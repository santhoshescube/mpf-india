﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="InterviewSchedule.aspx.cs" Inherits="Public_InterviewSchedule" Title="Interview Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Vacancy.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li class='selected'><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
            <li><a href="VisaTicketProcessing.aspx">
                <asp:Literal ID="Literal82" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></a></li>
            <li><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/C#">
.effect :'fadein'
.img {display: inline;}

    </script>

    <script type="text/javascript">
function ToggleDiv(sender)
{   
    if(sender != null)
    {
        sender.style.display = (sender.style.display == "block" ? "none" : "block");
        sender.style.visibility = (sender.style.visibility == "visible" ? "hidden" : "visible");
    }
}
    </script>

    <asp:UpdatePanel ID="upSchedule" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAddSchedule" runat="server" style="width: 100%; display: block;">
                <div style="width: 100%; padding-bottom: 10px;">
                    <div style="width: 100%; padding-bottom: 10px;">
                        <div id="Div1" style="color: #30a5d6">
                            <img src="../images/Interview Schedule.png" />
                            <b>
                                <asp:Literal ID="lit1" runat="server" meta:resourcekey="InterviewSchedule"></asp:Literal>
                            </b>
                        </div>
                    </div>
                </div>
                <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 20%; height: 29px">
                        <%--Job--%>
                        <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Job"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 75%">
                        <div style="float: left; width: 45%; height: 29px">
                            <asp:UpdatePanel ID="updJob" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlJob" runat="server" CssClass="dropdownlist_mandatory" Width="350px"
                                        AutoPostBack="true" DataValueField="JobID" DataTextField="Job" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:HiddenField ID="hfdJobScheduleID" runat="server" Value='<%# Eval("JobScheduleID") %>' />
                        </div>
                        <div style="float: right; width: 30%; height: 29px; padding-left: 3px">
                            <asp:RequiredFieldValidator ID="rfvJob" runat="server" ControlToValidate="ddlJob"
                                meta:resourcekey="SelectJob" CssClass="error" Display="Dynamic" ErrorMessage="Select Job"
                                ValidationGroup="ValJob" InitialValue="-1"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div style="height: auto">
                    <asp:UpdatePanel ID="updJobDetails" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divJobDetails" style="height: 50px; display: none;" runat="server">
                                <div class="trLeft" style="float: left; width: 20%; height: 50px">
                                </div>
                                <div class="trRight" style="display: block; float: left; width: 75%; height: 50px"
                                    runat="server" id="div3">
                                    <div style="float: left; width: 83%; max-height: 300px; overflow: auto;">
                                        <table>
                                            <tr>
                                                <td>
                                                    <%--Job Type :--%>
                                                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="JobType"></asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbkLevel" runat="server" CssClass="labeltext"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- Job Validity :--%>
                                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="JobValidity"></asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblExpiryDate" runat="server" CssClass="labeltext"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlJob" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 20%; height: 29px">
                        <%-- Job Level--%>
                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="JobLevel"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 75%">
                        <div style="float: left; width: 45%; height: 29px">
                            <asp:UpdatePanel ID="updJobLevel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlJobLevel" runat="server" CssClass="dropdownlist_mandatory"
                                        Width="350px" OnSelectedIndexChanged="ddlJobLevel_SelectedIndexChanged" AutoPostBack="true"
                                        DataValueField="JobLevelId" DataTextField="JobLevel">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hfdIsBorad" runat="server" Value="1" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlJob" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div style="float: right; width: 30%; height: 29px; padding-left: 3px">
                            <asp:CustomValidator ID="cusJobLevel" runat="server" ControlToValidate="ddlJobLevel"
                                Display="Dynamic" ClientValidationFunction="validateJobLevel" ValidationGroup="ValJob"
                                CssClass="error"></asp:CustomValidator>
                        </div>
                    </div>
                </div>
                <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 20%; height: 29px">
                        <%-- Interview Date--%>
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="InterviewDate"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 75%; height: 29px">
                        <div style="float: left; width: 25%; height: 29px">
                            <asp:TextBox ID="txtInterviewDate" runat="server" CssClass="textbox_mandatory" Width="90px"
                                onkeypress="return false" MaxLength="10" OnTextChanged="txtInterviewDate_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="ibtnInterviewDate" runat="server" CssClass="imagebutton" CausesValidation="False"
                                ImageUrl="~/images/Calendar_scheduleHS.png" style="margin-top: 5px;" />
                            <AjaxControlToolkit:CalendarExtender ID="AjaxCalDate" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="ibtnInterviewDate" TargetControlID="txtInterviewDate" Enabled="True">
                            </AjaxControlToolkit:CalendarExtender>
                        </div>
                        <div style="float: left; line-height:36px; padding-left:5px; height: 29px">
                            @
                        </div>
                        <div style="float: left; width: 20%; height: 29px">
                            <asp:TextBox ID="txtInterviewTime" runat="server" CssClass="textbox_mandatory" Width="80px"
                                MaxLength="7" Text='<%# Eval("InterviewTime") %>'></asp:TextBox>
                        </div>
                        <asp:CustomValidator ID="cvInteriewDate" runat="server" ValidateEmptyText="True"
                            ControlToValidate="txtInterviewDate" Display="Dynamic" ClientValidationFunction="validateInterviewDate"
                            ValidationGroup="ValJob" CssClass="error"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ID="rfvInterviewTime" runat="server" ControlToValidate="txtInterviewTime"
                            meta:resourcekey="Pleaseenterinterviewtime" ValidationGroup="ValJob" ErrorMessage="*"
                            CssClass="error"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 20%; height: 29px">
                        <%--Duration--%>
                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Duration"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 75%; height: 29px">
                        <div style="float: left; width: 40%; height: 29px">
                            <asp:TextBox ID="txtDuration" runat="server" MaxLength="3" Width="60px" CssClass="textbox"></asp:TextBox>
                            <AjaxControlToolkit:TextBoxWatermarkExtender ID="wmDuration" runat="server" TargetControlID="txtDuration"
                                WatermarkText="hh:mm" WatermarkCssClass="textWmcss" Enabled="True">
                            </AjaxControlToolkit:TextBoxWatermarkExtender>
                        </div>
                        <div style="float: right; width: 58%; height: 29px; padding-left: 3px">
                            <asp:RequiredFieldValidator ID="rfvDuration" runat="server" ControlToValidate="txtDuration"
                                meta:resourcekey="PleaseenterDuration" CssClass="error" Display="Dynamic" ErrorMessage="Please enter Duration"
                                ValidationGroup="ValJob"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 20%; height: 29px">
                        <%--Venue--%>
                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Venue"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 75%; height: 29px">
                        <div style="float: left; width: 30%; height: 29px">
                            <asp:TextBox ID="txtVenue" runat="server" MaxLength="50" Width="350px" CssClass="textbox"></asp:TextBox>
                        </div>
                        <div style="float: right; width: 35%; height: 29px; padding-left: 3px">
                            <asp:RequiredFieldValidator ID="rfvVenue" runat="server" ControlToValidate="txtVenue"
                                meta:resourcekey="Pleaseentervenue" CssClass="error" Display="Dynamic" ErrorMessage="Please enter venue"
                                ValidationGroup="ValJob"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                 <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 20%; height: 29px">
                        <%--Venue--%>
                        <asp:Literal ID="Literal22" runat="server" ></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 75%; height: 29px">
                        <div style="float: left; width: 53%; height: 29px">
                            <asp:CheckBox ID="chkEmployees" runat="server" 
                                         Text='<%$Resources:ControlsCommon,SelectAllEmployees%>' AutoPostBack="True" oncheckedchanged="chkEmployees_CheckedChanged" />
                        </div>
                        
                    </div>
                </div>
                <div style="height: auto; height: 250px;">
                    <div class="trLeft" style="float: left; width: 20%; height: 150px">
                        <%--Interviewers--%>
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Interviewers"></asp:Literal>
                    </div>
                    <div class="trRight" style="overflow: auto; float: left; width: 75%; height: 150px">
                        <div style="float: left; width: 75%; height: 29px">
                            <asp:UpdatePanel ID="updInterviewer" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:CheckBoxList ID="chlInterviewer" runat="server" RepeatColumns="2" DataTextField="Name"
                                        BorderStyle="None" BorderWidth="1px" GridLines="Both" DataValueField="InterviewerID">
                                    </asp:CheckBoxList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlJobLevel" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div style="float: right; width: 40%; height: 29px; padding-left: 3px">
                        </div>
                    </div>
                </div>
                <div style="height: 12px;">
                    <div class="trLeft" style="float: left; width: 20%; height: 12px">
                    </div>
                    <div class="trRight" style="float: left; width: 75%; height: 12px">
                        <asp:CustomValidator ID="CustomValidator1" runat="server" meta:resourcekey="Pleaseselectinterviewer"
                            ClientValidationFunction="valAddInterviewer" ValidationGroup="ValJob" CssClass="error"></asp:CustomValidator>
                    </div>
                </div>
                <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 20%; height: 29px">
                        <%--Todays Quota--%>
                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="TodaysQuota"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 75%; height: 29px">
                        <div style="float: left; width: 15%; height: 29px">
                            <asp:UpdatePanel ID="updResumeQuota" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtResumeQuota" runat="server" MaxLength="3" Width="60px" CssClass="textbox"></asp:TextBox>
                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtLocalPhone" runat="server"
                                        FilterType="Numbers,Custom" TargetControlID="txtResumeQuota" ValidChars="+ ()-">
                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div style="float: left; width: 30%; height: 29px; padding-left: 10px">
                            <asp:LinkButton ID="imgShowCandidates" CssClass="select_candidate" ToolTip="Select Candidates"
                                meta:resourcekey="SelectCandidates" runat="server" OnClick="imgShowCandidates_Click"
                                Text="Select Candidate"> </asp:LinkButton>
                        </div>
                        <div style="float: right; width: 58%; height: 29px; padding-left: 3px">
                            <asp:RequiredFieldValidator ID="rfvQuota" runat="server" ControlToValidate="txtResumeQuota"
                                meta:resourcekey="Pleaseentertodaysquota" CssClass="error" Display="Dynamic"
                                ErrorMessage="Please enter todays quota" ValidationGroup="ValJob"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div style="height: auto">
                    <asp:UpdatePanel ID="updCandidates" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="height: 300px; display: block;" runat="server">
                                <div class="trLeft" style="float: left; width: 20%; height: 300px">
                                    <%--Candidates--%>
                                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Candidates"></asp:Literal>
                                    <br />
                                    <asp:LinkButton ID="lnkMore" CausesValidation="false" runat="server" meta:resourcekey="MoreCandidates"
                                        OnClick="lnkMore_Click" ToolTip="More Candidates."></asp:LinkButton>
                                </div>
                                <div class="trRight" style="display: none; float: left; width: 75%; height: 300px"
                                    runat="server" id="divCandidate">
                                    <div style="float: left; width: 83%; max-height: 300px; overflow: auto;">
                                        <asp:DataList ID="dlCandidates" runat="server" Width="100%" CellPadding="3" BackColor="White"
                                            OnItemDataBound="dlCandidates_ItemDataBound" DataKeyField="CandidateId" BorderColor="#CCCCCC"
                                            BorderStyle="None" BorderWidth="1px" GridLines="Both">
                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                            <ItemStyle ForeColor="#000066" />
                                            <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            <HeaderTemplate>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 5%;" class="trLeft">
                                                            <%-- <asp:CheckBox ID="chkSelect" runat="server" />--%>
                                                        </td>
                                                        <td style="width: 5%;" class="trLeft">
                                                            <%--Sl.No--%>
                                                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="SlNo"></asp:Literal>
                                                        </td>
                                                        <td style="width: 70%;" align="left" class="trLeft">
                                                            <%--Candidate [English]--%>
                                                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="CandidateEnglish"></asp:Literal>
                                                        </td>
                                                        <td align="left" style="width: 10%;" class="trLeft">
                                                            <%-- Time--%>
                                                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Time"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <HeaderStyle BackColor="#17acf6" Font-Bold="True" ForeColor="White" />
                                            <ItemTemplate>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 5%;" class="trLeft" id="tdCheckbox" runat="server">
                                                            <asp:CheckBox ID="chkSelect" runat="server" Checked="true" />
                                                        </td>
                                                        <td style="width: 5%;" class="trLeft">
                                                            <%# Eval("Rowno")%>
                                                        </td>
                                                        <td style="width: 70%;" align="left" class="trLeft">
                                                            <asp:Label ID="lblNameEng" runat="server" Text='<%# Eval("NameEng")%>'></asp:Label>
                                                        </td>
                                                        <td align="left" style="width: 10%;" class="trLeft">
                                                            <asp:TextBox ID="txtTime" runat="server" Width="60px" MaxLength="7" Text=' <%# Eval("Time")%>'></asp:TextBox>
                                                            <asp:HiddenField ID="hfdIsCurrent" runat="server" Value=' <%# Eval("IsCurrent")%>' />
                                                            <asp:HiddenField ID="hfdIsAttended" runat="server" Value=' <%# Eval("IsAttended")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button ID="btnSetTime" runat="server" CssClass="popupbutton" meta:resourcekey="SetTime"
                                                    CausesValidation="false" />&nbsp;
                                                <asp:Button ID="brnClear" runat="server" meta:resourcekey="ClearTime" CssClass="popupbutton" />
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        </asp:DataList>
                                    </div>
                                    <div style="float: left; width: 75%; height: 100px; overflow: auto;">
                                    </div>
                                </div>
                                <div class="trRight" style="display: none; float: left; width: 75%; height: 29px"
                                    id="divNoCandidate" runat="server">
                                    <div style="float: left; width: 40%; height: 29px" class="error">
                                        <%--No Candidates--%>
                                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="NoCandidates"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txtResumeQuota" EventName="TextChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Submit%>'
                            CommandName="SUBMIT" Text='<%$Resources:ControlsCommon,Submit%>' ValidationGroup="ValJob"
                            Width="75px" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Cancel%>'
                            CausesValidation="false" OnClick="btnCancel_Click" Text='<%$Resources:ControlsCommon,Cancel%>'
                            Width="75px" />
                    </div>
                </div>
            </div>
            <div id="divViewSchedule" runat="server" style="width: 100%; display: none;">
                <div style="width: 100%; padding-bottom: 20px;">
                    <div style="width: 100%; padding-bottom: 20px;">
                        <div id="dashboardh5" style="color: #30a5d6">
                            <img src="../images/Interview_Shedule_list_Big.png" />
                            <b>
                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="ScheduleList"></asp:Literal></b>
                        </div>
                    </div>
                </div>
                <div style="width: 100%; padding-bottom: 10px; padding-left: 550px;" runat="server"
                    id="divSearch">
                    <asp:TextBox ID="txtScheduleSearchDate" runat="server" AutoPostBack="true" OnTextChanged="txtScheduleSearchDate_TextChanged"></asp:TextBox>
                    <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="false" ImageUrl="~/images/search.png"
                        meta:resourcekey="Clickheretosearch" ImageAlign="AbsMiddle" OnClick="ImageButton1_Click" />
                    <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
                        PopupButtonID="txtScheduleSearchDate" TargetControlID="txtScheduleSearchDate"
                        Enabled="True">
                    </AjaxControlToolkit:CalendarExtender>
                    <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                        TargetControlID="txtScheduleSearchDate" meta:resourcekey="Date" WatermarkCssClass="WmCss">
                    </AjaxControlToolkit:TextBoxWatermarkExtender>
                </div>
                <div style="width: 100%;">
                    <asp:DataList ID="dlViewSchedules" runat="server" Width="100%" CellPadding="3" BackColor="White"
                        OnItemCommand="dlViewSchedules_ItemCommand" OnItemDataBound="dlViewSchedules_ItemDataBound"
                        DataKeyField="JobScheduleID" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                        GridLines="Both">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <ItemStyle ForeColor="#000066" />
                        <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <HeaderTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 55%;" align="left" class="trLeft">
                                        <%--Schedule Title--%>
                                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="ScheduleTitle"></asp:Literal>
                                    </td>
                                    <td align="right" style="width: 10%;" class="trLeft">
                                        <%--Interviewers--%>
                                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Interviewers"></asp:Literal>
                                    </td>
                                    <td align="left" style="width: 10%;" class="trLeft">
                                        <%--Candidates--%>
                                        <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Candidates"></asp:Literal>
                                    </td>
                                    <td align="left" style="width: 5%;" class="trLeft">
                                    </td>
                                    <td style="width: 5%;" class="trLeft" align="left">
                                    </td>
                                    <td style="width: 5%;" class="trLeft" align="left">
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <HeaderStyle BackColor="#17acf6" Font-Bold="True" ForeColor="White" />
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td style="width: 55%;" align="left" colspan="2" class="trLeft">
                                        <asp:LinkButton ID="lnkScheduleTitle" runat="server" meta:resourcekey="ViewScheduledetailstooltip"
                                            Text='<%# Eval("ScheduleTitle")%>'></asp:LinkButton>
                                    </td>
                                    <td align="right" style="width: 10%;" class="trLeft">
                                        <%# Eval("NoOfInterviewers")%>
                                    </td>
                                    <td align="left" style="width: 10%;" class="trLeft">
                                        <%# Eval("MaximumCandidates")%>
                                        <asp:HiddenField ID="hfdProcessed" runat="server" Value=' <%# Eval("ProcessCount")%>' />
                                    </td>
                                    <td align="left" style="width: 5%;" class="trLeft" align="right">
                                        <asp:ImageButton ID="imgEdit" runat="server" CommandName="ALTER" Text='<%$Resources:ControlsCommon,Edit%>'
                                            CommandArgument='<%# Eval("JobScheduleID") %>' meta:resourcekey="TooltipEdit"
                                            ImageUrl="~/images/edit.png" />
                                    </td>
                                    <td style="width: 5%;" class="trLeft" align="right">
                                        <asp:ImageButton ID="imgDelete" runat="server" Enabled="false" CommandName="REMOVE"
                                            Text='<%$Resources:ControlsCommon,Delete%>' CommandArgument='<%# Eval("JobScheduleID") %>'
                                            ToolTip='<%$Resources:ControlsCommon,Delete%>' ImageUrl="~/images/delete_popup.png" />
                                        <AjaxControlToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                                            TargetControlID="imgDelete" meta:resourcekey="Areyousuretodeletethisschedule">
                                        </AjaxControlToolkit:ConfirmButtonExtender>
                                    </td>
                                    <td style="width: 5%;" class="trLeft" align="left">
                                        <asp:ImageButton ID="imgPrint" runat="server" CommandName="Print" Text='<%$Resources:ControlsCommon,Print%>'
                                            CommandArgument='<%# Eval("JobScheduleID") %>' ToolTip='<%$Resources:ControlsCommon,Print%>'
                                            ImageUrl="~/images/Print.PNG" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" style="width: 100%">
                                        <div id="divScheduleDetails" runat="server" style="width: 115%; display: none;">
                                            <div style="width: 100%;">
                                                <div style="height: auto; width: 100%;">
                                                    <div class="trLeft" style="float: left; width: 15%; height: 29px">
                                                        <%-- Venue--%>
                                                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Venue"></asp:Literal>
                                                    </div>
                                                    <div class="trLeft" style="float: left; width: 75%; height: 29px">
                                                        :
                                                        <%#Eval("Venue")%>
                                                    </div>
                                                </div>
                                                <div style="height: auto; width: 100%;">
                                                    <div class="trLeft" style="float: left; width: 15%; height: 29px">
                                                        <%--Job Level--%>
                                                        <asp:Literal ID="Literal14" runat="server" meta:resourcekey="JobLevel"></asp:Literal>
                                                    </div>
                                                    <div class="trLeft" style="float: left; width: 75%; height: 29px">
                                                        :
                                                        <%#Eval("Level")%>
                                                    </div>
                                                </div>
                                                <div style="height: auto; width: 100%;">
                                                    <div class="trLeft" style="float: left; width: 15%; height: 29px">
                                                        <%--Interviewers--%>
                                                        <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Interviewers"></asp:Literal>
                                                    </div>
                                                    <div class="trLeft" style="float: left; width: 75%; height: 29px">
                                                        :
                                                        <%#Eval("Interviewers")%>
                                                    </div>
                                                </div>
                                                <div style="height: auto; width: 100%;">
                                                    <div class="trRight" style="float: left; width: 75%">
                                                        <div style="float: left; width: 88%; height: 200px; padding-left: 3px; overflow: auto;">
                                                            <asp:DataList ID="dlViewCandidates" runat="server" Width="100%" CellPadding="4" ForeColor="#333333">
                                                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                                <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                                                <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                                <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                <HeaderTemplate>
                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                            <td style="width: 80%;">
                                                                                <%-- Candidate Name--%>
                                                                                <asp:Literal ID="Literal15" runat="server" meta:resourcekey="CandidateName"></asp:Literal>
                                                                            </td>
                                                                            <td style="width: 20%;">
                                                                                <%--Time--%>
                                                                                <asp:Literal ID="Literal16" runat="server" meta:resourcekey="Time"></asp:Literal>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <HeaderStyle Font-Bold="True" ForeColor="White" Height="15px" BackColor="#5D7B9D" />
                                                                <ItemTemplate>
                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                            <td style="width: 80%;">
                                                                                <%#Eval("NameEng")%>
                                                                            </td>
                                                                            <td style="width: 20%;">
                                                                                <%#Eval("Time")%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:DataList>
                    <uc:Pager ID="pgSchedule" runat="server" />
                </div>
                <div style="width: 100%; display: none;" id="divNoCandidates" runat="server">
                    <asp:Label ID="lblNoCandidates" runat="server" Text="No schedules" CssClass="error"></asp:Label>
                </div>
            </div>
            <asp:Panel ID="pnlPrint" runat="server">
            </asp:Panel>
            <%--  Message box--%>
            <div>
                <div style="display: none">
                    <asp:Button ID="btnProxy" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="upnlMessage" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
            <%--  Message box--%>
            <%-- More Candidates List--%>
            <asp:UpdatePanel ID="updMoreCandidate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none;">
                        <asp:Button ID="btnMore" runat="server" /></div>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMoreCandidates" runat="server" PopupControlID="pnlMoreCandidates"
                        TargetControlID="btnMore" BackgroundCssClass="modalBackground" Enabled="True"
                        PopupDragHandleControlID="tblDragControl" CancelControlID="ImageButton2">
                    </AjaxControlToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlMoreCandidates" runat="server" Width="800px" Style="display: none;">
                        <div id="popupmainwrap">
                            <div id="popupmain" style="width: 100%!important;">
                                <div id="header11">
                                    <div id="hbox1">
                                        <div id="headername" runat="server" style="color: White;">
                                            <%--Candidate List--%>
                                            <asp:Literal ID="Literal16" runat="server" meta:resourcekey="CandidateList"></asp:Literal>
                                        </div>
                                    </div>
                                    <div id="hbox2">
                                        <div id="close1">
                                            <a href="">
                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Controls/images/icon-close.png" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div id="contentwrap">
                                    <div id="content">
                                        <div style="height: 35px; margin-left: 10px; margin-top: 10px;">
                                            <asp:DropDownList ID="ddlSrchQualification" DataTextField="Qualification" Width="250px"
                                                DataValueField="DegreeID" CssClass="dropdownlist" runat="server">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSrchSkillSet" Width="200px" TextMode="MultiLine" CssClass="textbox"
                                                Height="20px" Style="margin-left: 10px;" runat="server" onchange="RestrictMulilineLength(this, 100);"
                                                onkeyup="RestrictMulilineLength(this, 100);"></asp:TextBox>
                                            <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                                TargetControlID="txtSrchSkillSet" meta:resourcekey="SearchBySkills" WatermarkCssClass="textWmcss"
                                                Enabled="True">
                                            </AjaxControlToolkit:TextBoxWatermarkExtender>
                                            <asp:UpdatePanel ID="updImgSearch" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:ImageButton ID="imgCanSearch" runat="server" CausesValidation="false" ImageUrl="~/images/search.png"
                                                        ToolTip="Click here to search" ImageAlign="AbsMiddle" OnClick="imgCanSearch_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div style="max-height: 350px; overflow: auto; margin: 5px;">
                                            <asp:UpdatePanel ID="updMoreCandidates" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:DataList ID="dlMoreCandidates" RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue"
                                                        DataKeyField="CandidateID" Width="98%" runat="server" RepeatColumns="4" BackColor="White"
                                                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both">
                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                        <ItemStyle ForeColor="#000066" />
                                                        <SeparatorStyle BackColor="AliceBlue" />
                                                        <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                        <HeaderTemplate>
                                                            <table style="width: 98%">
                                                                <tr>
                                                                    <td style="width: 20px; height: 15px; overflow: hidden; vertical-align: bottom">
                                                                    </td>
                                                                    <td style="width: 5%; overflow: hidden; vertical-align: top">
                                                                        <%-- <asp:CheckBox CssClass="labeltext" ForeColor="White" AutoPostBack="true" ID="chkSelectAll"
                                                                    OnCheckedChanged="chkSelectAll_CheckedChanged" Text="Select All" Height="15px"
                                                                    runat="server" />--%>
                                                                        <%--  Candidate--%>
                                                                        <asp:Literal ID="Literal16" runat="server" meta:resourcekey="Candidate"></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 90%; overflow: hidden; vertical-align: top; padding-left: 320px"
                                                                        align="left">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td style="width: 20px; height: 15px; overflow: hidden; vertical-align: bottom">
                                                                        <asp:CheckBox CssClass="labeltext" ForeColor="White" ID="chkMoreSelect" Height="15px"
                                                                            runat="server" />
                                                                    </td>
                                                                    <td style="width: 5px; height: 15px; overflow: hidden; vertical-align: bottom">
                                                                        <%-- <%#Eval("Rowno")%>--%>
                                                                    </td>
                                                                    <td style="vertical-align: top; width: 200px; height: 15px; overflow: hidden;" align="left">
                                                                        <asp:Label ID="lblCandidate" Height="15px" CssClass="labeltext" runat="server" ToolTip='<%# Eval("NameEng") %>'
                                                                            Text='<%# Eval("NameEng") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                <div id="footerwrapper">
                                    <div id="footer11">
                                        <div id="buttons">
                                            <a>
                                                <img src="../images/Add_Candidates.PNG" />
                                                <asp:LinkButton ID="lnkAddCandidates" runat="server" Text="Add to list" CausesValidation="False"
                                                    meta:resourcekey="Addtolist" OnClick="lnkAddCandidates_Click"></asp:LinkButton></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td width="20" height="51" align="left" valign="top" class="tdTopLeft">
                                    &nbsp;
                                </td>
                                <td height="51" width="660" align="left" valign="middle" class="tdTopCenter">
                                    <table cellpadding="0" border="0" cellspacing="0" width="100%" id="tblDragControl"
                                        runat="server">
                                        <tr>
                                            <td id="td3" runat="server" class="header_text" style="padding-top: 10px">
                                                <%--Candidate List--%>
                                                <asp:Literal ID="Literal17" runat="server" meta:resourcekey="CandidateList"></asp:Literal>
                                            </td>
                                            <td width="50px" align="right">
                                                <asp:ImageButton ID="imgMoreClose" runat="server" CssClass="imagebutton" SkinID="CloseIcon"
                                                    ToolTip="Close" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="20" height="51" class="tdTopRight">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" width="20" class="tdMiddleLeft">
                                    &nbsp;
                                </td>
                                <td height="100" width="660" style="background-color: White;" valign="top">
                                    <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0" align="center">
                                        <tr>
                                            <td width="100%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="padding: 10px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left" valign="top" width="20" class="tdMiddleRight">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="20" height="20" align="right" valign="top" class="tdBottomLeft">
                                    &nbsp;
                                </td>
                                <td height="20" width="660" align="left" valign="top" class="tdBottomCenter">
                                    &nbsp;
                                </td>
                                <td width="20" height="20" align="left" valign="top" class="tdBottomRight">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%-- More Candidates List--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgScheduleInterview" ImageUrl="~/images/Interview Schedule.png"
                        CausesValidation="false" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkSchedule" runat="server" CausesValidation="false" OnClick="lnkSchedule_Click"
                            meta:resourcekey="ScheduleInterview">
                            <asp:Literal ID="Literal18" runat="server" meta:resourcekey="InterviewSchedule"></asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgScheduleList" ImageUrl="~/images/Interview_Shedule_list_Big.png"
                        CausesValidation="false" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkScheduleList" runat="server" CausesValidation="false" OnClick="lnkScheduleList_Click"
                            meta:resourcekey="ViewSchedule">
                            <asp:Literal ID="Literal19" runat="server" meta:resourcekey="ScheduleList"></asp:Literal></asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
