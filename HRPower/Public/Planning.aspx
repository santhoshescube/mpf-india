﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Planning.aspx.cs" Inherits="Public_Planning" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>

        <script src="../js/jquery/jquery.ui.tabs.js" type="text/javascript"></script>

        <link href="../css/employee.css" rel="stylesheet" type="text/css" />
        <%-- <ul>
            <li class="selected"><a href='Employee.aspx'><span>Employee </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>Salary Structure </span></a></li>
            <li><a href="ViewPolicy.aspx">View Policies</a></li>--%>
        <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
        <%--</ul> --%>
        <ul>
            <li class="selected"><a href='Projects.aspx'><span>
                <%--  Employee --%>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Project %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>
                <%--    Salary Structure--%>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,Plan %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="ViewPolicy.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,Expense %>'>
                </asp:Literal>
            </a></li>
            <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
            
              <li><a href="ViewPolicy.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,Calender %>'>
                </asp:Literal>
            </a></li>
          
            
            
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
</asp:Content>
