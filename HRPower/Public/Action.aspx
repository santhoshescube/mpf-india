﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Action.aspx.cs" Inherits="Public_Action" Title="Performance Action" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .Left
        {
            float: left;
            padding-left: 10px;
            width: 25%;
            min-width: 25%;
            height: 30px;
        }
        .Middle
        {
            float: left;
            width: 3%;
            min-width: 3%;
            height: 30px;
        }
        .Right
        {
            float: left;
            width: 70%;
            color: Black;
            height: 30px;
            min-width: 70%;
        }
        .MainDivAction
        {
            width: 100%;
            min-height: 30px;
            height: auto;
            vertical-align: top;
            line-height: 25px;
            font-family: Verdana;
            font-size: 12px;
            font-weight: 200;
            text-align: left;
            padding-top: 3px;
        }
        .MainDivAction input[type="checkbox"]
        {
            margin-right: 10px;
            margin-top: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <%-- <ul>
            <li><a href="SalesPerformance.aspx">Target Based Performance</a></li>
            <li><a href="PerformanceTemplate.aspx"><span>Performance Template </span></a></li>
            <li><a href="PerformanceInitiation.aspx"><span>Performance Initiation</span></a></li>
            <li><a href="PerformanceEvaluation.aspx"><span>Performace Evaluation</span></a></li>
            <li class='selected'><a href="PerformanceAction.aspx">Performance Action</a></li>
        </ul>
        --%>
        <ul>
            <li><a href="SalesPerformance.aspx">
                <%--Target Performance--%>
                <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:MasterPageCommon,TargetPerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="AttendancePerformance.aspx">
                <%--Attendance Performance--%>
                <asp:Literal ID="Literal32" runat="server" Text='<%$Resources:MasterPageCommon,AttendancePerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="PerformanceTemplate.aspx"><span>
                <%--  Performance Template --%>
                <asp:Literal ID="Literal33" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceTemplate %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="PerformanceInitiation.aspx"><span>
                <%--        Performance Initiation--%>
                <asp:Literal ID="Literal34" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceInitiation %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="PerformanceEvaluation.aspx"><span>
                <%-- Performace Evaluation--%>
                <asp:Literal ID="Literal35" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation %>'>
                </asp:Literal>
            </span></a></li>
            <li class='selected'><a href="PerformanceAction.aspx">
                <%--        
            Performance Action--%>
                <asp:Literal ID="Literal36" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceAction %>'>
                </asp:Literal>
            </a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div id="divAdd" runat="server" style="width: 100%;">
        <div class="MainDivAction">
            <div class="Left">
                <%--   Employee--%>
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ControlsCommon,Employee %>'></asp:Literal>
            </div>
            <div class="Middle">
                :
            </div>
            <div class="Right">
                <asp:Label ID="lblEmployee" runat="server" />
            </div>
        </div>
        <div class="MainDivAction">
            <div class="Left">
                <%--Action Date--%>
                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="ActionDate"></asp:Literal>
            </div>
            <div class="Middle">
                :
            </div>
            <div class="Right">
                <asp:Label ID="lblActionDate" runat="server" />
            </div>
        </div>
        <div class="MainDivAction">
            <div class="Left">
                <%-- From Date--%>
                <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ControlsCommon,FromDate %>'></asp:Literal>
            </div>
            <div class="Middle">
                :
            </div>
            <div class="Right">
                <asp:Label ID="lblFromDate" runat="server" />
            </div>
        </div>
        <div class="MainDivAction">
            <div class="Left">
                <%--    To Date--%>
                <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ControlsCommon,ToDate %>'></asp:Literal>
            </div>
            <div class="Middle">
                :
            </div>
            <div class="Right">
                <asp:Label ID="lblToDate" runat="server" />
            </div>
        </div>
        <div class="MainDivAction">
            <div class="Left">
                <%--   With Effect Date--%>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ControlsCommon,WithEffectDate %>'></asp:Literal>
            </div>
            <div class="Middle">
                :
            </div>
            <div class="Right">
                <div class="Left" style="padding: 0px;">
                    <asp:TextBox ID="txtWithEffectDate" runat="server" Style="margin-right: 4px;" Width="80px"
                        onkeypress="return false;" />
                    <asp:ImageButton ID="ibtnWithEffectDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                        CausesValidation="false" />
                    <AjaxControlToolkit:CalendarExtender ID="ceWithEffectDate" runat="server" Format="dd/MM/yyyy"
                        TargetControlID="txtWithEffectDate" PopupButtonID="ibtnWithEffectDate" />
                    <AjaxControlToolkit:FilteredTextBoxExtender ID="fteWithEffectDate" TargetControlID="txtWithEffectDate"
                        runat="server" FilterType="Custom,Numbers" ValidChars="/" />
                </div>
                <div class="Right">
                    <asp:CustomValidator ID="cvWithEffectDate" runat="server" ValidateEmptyText="true"
                        ControlToValidate="txtWithEffectDate" Display="Dynamic" ClientValidationFunction="ValidateDocumentDate"
                        ValidationGroup="Submit" CssClass="error"></asp:CustomValidator>
                </div>
            </div>
        </div>
        <div class="MainDivAction" style="margin-top: 5px;">
            <asp:UpdatePanel ID="upnlJobPromotion" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="Left">
                        <asp:CheckBox ID="chkJobPromotion" runat="server" Checked="false" meta:resourcekey="JobPromotion"
                            AutoPostBack="true" OnCheckedChanged="chkJobPromotion_CheckedChanged" />
                    </div>
                    <div class="Right" id="divJobPromotion" runat="server" style="display: none; min-height: 60px;
                        height: auto;">
                        <div class="Left">
                            <%-- Designation--%>
                            <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ControlsCommon,Designation %>'></asp:Literal>
                        </div>
                        <div class="Right">
                            <asp:DropDownList ID="ddlDesignation" runat="server" Width="200px" />
                        </div>
                        <div class="Left">
                            <%-- Work Policy--%>
                            <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:ControlsCommon,WorkPolicy %>'></asp:Literal>
                        </div>
                        <div class="Right">
                            <asp:DropDownList ID="ddlWorkPolicy" runat="server" Width="200px" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="chkJobPromotion" EventName="CheckedChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="upnlSalary" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="MainDivAction">
                    <div class="Left" style="width: 100%; height: 30px;">
                        <asp:CheckBox ID="chkAlterSalary" runat="server" Checked="false" AutoPostBack="true"
                            meta:resourcekey="AlterSalary" OnCheckedChanged="chkAlterSalary_CheckedChanged" />
                    </div>
                </div>
                <div>
                    <asp:HiddenField ID="hdnEmployeeID" runat="server" />
                    <asp:HiddenField ID="hdnIsForApproval" runat="server" />
                    <asp:HiddenField ID="hdnPerformanceActionID" runat="server" />
                </div>
                <div style="clear: both;">
                </div>
                <div id="divSalaryStructure" runat="server" style="width: 91%; border: solid 1px #25c4ec;
                    border-radius: 8px; margin-left: 37px; margin-bottom: 20px; display: none;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td>
                                <div>
                                    <div style="display: none">
                                        <asp:Button ID="btnProxy" runat="server" Text="submit" />
                                    </div>
                                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                                        BorderStyle="None">
                                        <asp:UpdatePanel ID="upnlMessage" runat="server">
                                            <ContentTemplate>
                                                <uc:Message ID="ucMessage" runat="server" ModalPopupId="mpeMessage" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                                        PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                                        Drag="true">
                                    </AjaxControlToolkit:ModalPopupExtender>
                                </div>
                                <asp:UpdatePanel ID="upnlSalaryStructure" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="width: 100%;">
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="padding: 5px;">
                                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td width="100px" class="firstTrLeft">
                                                                    <%--   Company--%>
                                                                    <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:ControlsCommon,Company %>'></asp:Literal>
                                                                </td>
                                                                <td width="120px">
                                                                    <asp:DropDownList ID="ddlCompany" runat="server" Width="200px" Enabled="false" DataValueField="CompanyID"
                                                                        DataTextField="CompanyName">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td width="9%">
                                                                    &nbsp;
                                                                </td>
                                                                <td width="150px" class="firstTrLeft">
                                                                    <%--Payment Mode--%>
                                                                    <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ControlsCommon,PaymentMode %>'></asp:Literal>
                                                                </td>
                                                                <td class="firstTrRight">
                                                                    <asp:DropDownList ID="ddlPaymentMode" runat="server" Width="130px" DataValueField="PaymentModeID"
                                                                        DataTextField="Description" Enabled="False">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="firstTrLeft">
                                                                    <%-- Employee--%>
                                                                    <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:ControlsCommon,Employee %>'></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlEmployee" runat="server" Width="200px" Enabled="false" DataValueField="EmployeeID"
                                                                        DataTextField="EmployeeName">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="ddlEmployee"
                                                                        CssClass="error" Display="Dynamic" ErrorMessage='<%$Resources:ControlsCommon,PleaseSelectEmployee %>'
                                                                        ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td class="firstTrLeft">
                                                                    <%-- Payment Calculation--%>
                                                                    <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:ControlsCommon,PaymentCalculation %>'></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlPaymentCalculation" runat="server" Width="130px" DataValueField="PayCalculationTypeID"
                                                                        DataTextField="Description" Enabled="False">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="firstTrLeft">
                                                                    <%--  Currency--%>
                                                                    <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:ControlsCommon,Currency %>'></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlCurrency" runat="server" Width="200px" DataValueField="CurrencyID"
                                                                        DataTextField="Description" Enabled="false">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td class="firstTrLeft">
                                                                    <%-- Salary Process Day--%>
                                                                    <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:ControlsCommon,SalaryProcessDay %>'></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSalaryProcessDay" runat="server" Width="50px" Enabled="False">
                                                                        <asp:ListItem>1</asp:ListItem>
                                                                        <asp:ListItem>2</asp:ListItem>
                                                                        <asp:ListItem>3</asp:ListItem>
                                                                        <asp:ListItem>4</asp:ListItem>
                                                                        <asp:ListItem>5</asp:ListItem>
                                                                        <asp:ListItem>6</asp:ListItem>
                                                                        <asp:ListItem>7</asp:ListItem>
                                                                        <asp:ListItem>8</asp:ListItem>
                                                                        <asp:ListItem>9</asp:ListItem>
                                                                        <asp:ListItem>10</asp:ListItem>
                                                                        <asp:ListItem>11</asp:ListItem>
                                                                        <asp:ListItem>12</asp:ListItem>
                                                                        <asp:ListItem>13</asp:ListItem>
                                                                        <asp:ListItem>14</asp:ListItem>
                                                                        <asp:ListItem>15</asp:ListItem>
                                                                        <asp:ListItem>16</asp:ListItem>
                                                                        <asp:ListItem>17</asp:ListItem>
                                                                        <asp:ListItem>18</asp:ListItem>
                                                                        <asp:ListItem>19</asp:ListItem>
                                                                        <asp:ListItem>20</asp:ListItem>
                                                                        <asp:ListItem>21</asp:ListItem>
                                                                        <asp:ListItem>22</asp:ListItem>
                                                                        <asp:ListItem>23</asp:ListItem>
                                                                        <asp:ListItem>24</asp:ListItem>
                                                                        <asp:ListItem>25</asp:ListItem>
                                                                        <asp:ListItem>26</asp:ListItem>
                                                                        <asp:ListItem>27</asp:ListItem>
                                                                        <asp:ListItem>28</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                                                                    <asp:HiddenField ID="hdSalaryID" runat="server" Visible="false" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="firstTrRight" valign="top" style="height: 286px;">
                                                                    <div id="div1" class="TabbedPanels" style="font-size: 14px; width: 100%; float: left">
                                                                        <ul class="TabbedPanelsTabGroup">
                                                                            <li class="TabbedPanelsTabSelected" id="pnlpTab1" onclick="SetSalaryTab(this.id);">
                                                                                <%-- General--%>
                                                                                <asp:Literal ID="Literal30" runat="server" Text='<%$Resources:ControlsCommon,General %>'></asp:Literal>
                                                                            </li>
                                                                            <li class="TabbedPanelsTab" id="pnlpTab2" onclick="SetSalaryTab(this.id);">
                                                                                <%--  Other Remuneration--%>
                                                                                <asp:Literal ID="Literal31" runat="server" Text='<%$Resources:ControlsCommon,OtherRenumeration %>'></asp:Literal>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="TabbedPanelsContent" id="divpTab1" style="font-size: 13px; width: 100%;
                                                                            float: left; height: 200px">
                                                                            <div style="width: 100%; float: left">
                                                                                <div class="container_head" style="margin-top: 5px;">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td width="100px" align="left" class="labeltext">
                                                                                                <%-- Particulars--%>
                                                                                                <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:ControlsCommon,Particulars %>'></asp:Literal>
                                                                                            </td>
                                                                                            <td width="125px" align="left" class="labeltext">
                                                                                                <%--  Category--%>
                                                                                                <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:ControlsCommon,Category %>'></asp:Literal>
                                                                                            </td>
                                                                                            <td align="left" width="100px" class="labeltext">
                                                                                                <%--   Amount--%>
                                                                                                <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:ControlsCommon,Amount %>'></asp:Literal>
                                                                                            </td>
                                                                                            <td width="150px" align="left" class="labeltext">
                                                                                                <%--  Deduction Policy--%>
                                                                                                <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:ControlsCommon,DeductionPolicy %>'></asp:Literal>
                                                                                            </td>
                                                                                            <td width="25" align="left">
                                                                                                <asp:Button ID="btnAllowances" runat="server" CssClass="referencebutton" Text="..."
                                                                                                    ToolTip='<%$Resources:ControlsCommon,AddMoreParticulars %>' OnClick="btnAllowances_Click" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class="container_content" style="height: 200px; overflow: auto; border-top: 3px solid rgb(199, 194, 194);
                                                                                    background-color: rgb(238, 243, 243); width: 100%;">
                                                                                    <asp:UpdatePanel ID="upnlDesignationAllowance" runat="server" UpdateMode="Conditional">
                                                                                        <ContentTemplate>
                                                                                            <asp:DataList ID="dlDesignationAllowances" runat="server" Width="96%" DataKeyField="SalaryDetailID"
                                                                                                CssClass="labeltext" OnItemDataBound="dlDesignationAllowances_ItemDataBound"
                                                                                                OnItemCommand="dlDesignationAllowances_ItemCommand">
                                                                                                <ItemTemplate>
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td width="150">
                                                                                                                <asp:DropDownList ID="ddlParticulars" runat="server" AutoPostBack="true" Width="150"
                                                                                                                    onchange="chkDuplicateParticulars(this)" OnSelectedIndexChanged="ddlParticulars_SelectedIndexChanged">
                                                                                                                </asp:DropDownList>
                                                                                                                <asp:HiddenField ID="hdParticulars" runat="server" Value='<%# Eval("AddDedID") %>' />
                                                                                                            </td>
                                                                                                            <td width="100">
                                                                                                                <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" Width="100"
                                                                                                                    OnSelectedIndexChanged="ddlParticulars_SelectedIndexChanged">
                                                                                                                </asp:DropDownList>
                                                                                                                <asp:HiddenField ID="hfCatagoryID" runat="server" Value='<%# Eval("CategoryID") %>' />
                                                                                                            </td>
                                                                                                            <td width="100" align="left">
                                                                                                                <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right;" onchange="setVisibility(this);"
                                                                                                                    MaxLength="12" Text='<% # Eval("Amount") %>' onkeypress="return ValidateDecimal(event,this.value);"
                                                                                                                    AutoPostBack="True" OnTextChanged="txtAmount_TextChanged"></asp:TextBox>
                                                                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterMode="ValidChars"
                                                                                                                    FilterType="Numbers,Custom" TargetControlID="txtAmount" ValidChars=".">
                                                                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                                                                <asp:HiddenField ID="hdtxtAmount" runat="server" Visible="false" Value='<% # Eval("Amount") %>' />
                                                                                                            </td>
                                                                                                            <td width="130px" align="left">
                                                                                                                <asp:DropDownList ID="ddlDeductionPolicy" Width="160px" runat="server" AutoPostBack="True"
                                                                                                                    onchange="setVisibility2(this);" OnSelectedIndexChanged="ddlDeductionPolicy_SelectedIndexChanged">
                                                                                                                </asp:DropDownList>
                                                                                                                <asp:HiddenField ID="hdDeductionPolicy" runat="server" Value='<%# Eval("DeductionPolicyID") %>' />
                                                                                                                <asp:HiddenField ID="hdnDeductionAmount" runat="server" Value="0" />
                                                                                                            </td>
                                                                                                            <td width="15" align="right">
                                                                                                                <asp:ImageButton ID="btnRemove" runat="server" ToolTip="Delete Particular" SkinID="DeleteIconDatalist"
                                                                                                                    CommandArgument='<% # Eval("AddDedID") %>' CommandName="REMOVE_ALLOWANCE" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </ItemTemplate>
                                                                                            </asp:DataList>
                                                                                            <asp:CustomValidator ID="cvAllowances" runat="server" ClientValidationFunction="validateAllowanceDeduction"
                                                                                                ValidationGroup="Submit" Display="None" ErrorMessage='<%$Resources:ControlsCommon,PleaseSelectParticulars %>'
                                                                                                CssClass="error"></asp:CustomValidator>
                                                                                            <asp:CustomValidator ID="cvAmount" runat="server" ClientValidationFunction="validateAmount"
                                                                                                ValidationGroup="Submit" Display="None" ErrorMessage='<%$Resources:ControlsCommon,PleaseEnterTheAmountOrSelectDeductionPolicy %>'
                                                                                                CssClass="error"></asp:CustomValidator>
                                                                                            <asp:ValidationSummary ID="vsSummary" runat="server" ValidationGroup="Submit" ShowMessageBox="true"
                                                                                                ShowSummary="false" />
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="TabbedPanelsContent" id="divpTab2" style="display: none; height: 200px;">
                                                                            <div style="width: 100%; float: left">
                                                                                <div class="container_head" style="margin-top: 5px;">
                                                                                    <table width="100%" style="font-size: 11px">
                                                                                        <tr>
                                                                                            <td width="200px" align="left" class="labeltext">
                                                                                                <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:ControlsCommon,Particulars %>'></asp:Literal>
                                                                                            </td>
                                                                                            <td align="left" width="150px" class="labeltext">
                                                                                                <%--  Amount--%>
                                                                                                <asp:Literal ID="Literal19" runat="server" Text='<%$Resources:ControlsCommon,Amount %>'></asp:Literal>
                                                                                            </td>
                                                                                            <td width="25" align="left">
                                                                                                <asp:Button ID="btnOtherRemuneration" runat="server" CssClass="referencebutton" Text="..."
                                                                                                    ToolTip="Add more particulars" OnClick="btnOtherRemuneration_Click" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div class="container_content" style="height: 200px; overflow: auto; border-top: 3px solid rgb(199, 194, 194);
                                                                                    background-color: rgb(238, 243, 243); width: 100%;">
                                                                                    <asp:UpdatePanel ID="upnlOtherRemuneration" runat="server" UpdateMode="Conditional">
                                                                                        <ContentTemplate>
                                                                                            <asp:DataList ID="dlOtherRemuneration" runat="server" Width="96%" DataKeyField="SalaryID"
                                                                                                CssClass="labeltext" OnItemDataBound="dlOtherRemuneration_ItemDataBound" OnItemCommand="dlOtherRemuneration_ItemCommand">
                                                                                                <ItemTemplate>
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td width="200">
                                                                                                                <asp:DropDownList ID="ddlRemunerationParticulars" runat="server" AutoPostBack="true"
                                                                                                                    CssClass="dropdownlist" Width="200" onchange="chkDuplicateRemunerationParticulars(this)">
                                                                                                                </asp:DropDownList>
                                                                                                                <asp:HiddenField ID="hdRemunerationParticulars" runat="server" Value='<%# Eval("OtherRemunerationID") %>' />
                                                                                                            </td>
                                                                                                            <td width="145" align="left">
                                                                                                                <asp:TextBox ID="txtRemunerationAmount" runat="server" CssClass="textbox" Style="text-align: right;"
                                                                                                                    Text='<% # Eval("Amount") %>' MaxLength="9" onkeypress="return ValidateDecimal(event,this.value);"></asp:TextBox>
                                                                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtRemunerationAmount" runat="server"
                                                                                                                    FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtRemunerationAmount"
                                                                                                                    ValidChars=".">
                                                                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                                                                <asp:HiddenField ID="hdtxtRemunerationAmount" runat="server" Visible="false" Value='<% # Eval("Amount") %>' />
                                                                                                            </td>
                                                                                                            <td width="15" align="right">
                                                                                                                <asp:ImageButton ID="btnRemunerationRemove" runat="server" ToolTip="Delete Remunerationr"
                                                                                                                    SkinID="DeleteIconDatalist" CommandName="REMOVE_Remuneration" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </ItemTemplate>
                                                                                            </asp:DataList>
                                                                                            <asp:CustomValidator ID="cvRemuneration" runat="server" ClientValidationFunction="validateRemuneration"
                                                                                                ValidationGroup="Submit" Display="None" ErrorMessage='<%$Resources:ControlsCommon,PleaseEnterRemunerationParticulars %>'></asp:CustomValidator>
                                                                                            <asp:CustomValidator ID="cvRemunerationAmount" runat="server" ClientValidationFunction="validateRemunerationAmount"
                                                                                                ValidationGroup="Submit" Display="None" ErrorMessage='<%$Resources:ControlsCommon,PleaseEnterTheRemunerationAmount %>'></asp:CustomValidator>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="firstTrRight" valign="top" style="padding: 0px;">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 10%" class="firstTrLeft">
                                                                                <%--  Addition--%>
                                                                                <asp:Literal ID="Literal20" runat="server" Text='<%$Resources:ControlsCommon,Addition %>'></asp:Literal>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="txtAdditionAmt" Width="110px" runat="server" onkeydown="return false;"
                                                                                    Style="text-align: right; padding-right: 2px;" oncopy="return false;" oncut="return false;"
                                                                                    onpaste="return false;"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 13%" class="firstTrLeft">
                                                                                <%-- Deduction--%>
                                                                                <asp:Literal ID="Literal21" runat="server" Text='<%$Resources:ControlsCommon,Deduction %>'></asp:Literal>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="txtDeductionAmt" runat="server" Width="110px" onkeydown="return false;"
                                                                                    Style="text-align: right; padding-right: 2px;" oncopy="return false;" oncut="return false;"
                                                                                    onpaste="return false;"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 13%" class="firstTrLeft">
                                                                                <%--  NetAmount--%>
                                                                                <asp:Literal ID="Literal22" runat="server" Text='<%$Resources:ControlsCommon,NetAmount %>'></asp:Literal>
                                                                            </td>
                                                                            <td style="width: 17%">
                                                                                <asp:TextBox ID="txtNetAmount" runat="server" Width="110px" onkeydown="return false;"
                                                                                    Style="text-align: right; padding-right: 2px;" oncopy="return false;" oncut="return false;"
                                                                                    onpaste="return false;"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr style="padding-top: 25px;">
                                                                <td colspan="5" class="firstTrRight" valign="top">
                                                                    <table width="100%" style="padding-top: 5px;">
                                                                        <tr>
                                                                            <td class="firstTrLeft" style="width: 16.5%;">
                                                                                <%-- Remarks--%>
                                                                                <asp:Literal ID="Literal23" runat="server" Text='<%$Resources:ControlsCommon,Remarks %>'></asp:Literal>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="textbox" onchange="RestrictMulilineLength(this, 500);"
                                                                                    onkeyup="RestrictMulilineLength(this, 500);" TextMode="MultiLine" Width="94%"
                                                                                    Style="padding: 5px;" Height="50px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" valign="top" class="firstTrRight">
                                                                    <table style="font-size: 11px; height: 42px; width: 100%;">
                                                                        <tr>
                                                                            <td style="width: 15%;" class="firstTrLeft">
                                                                                <%-- Absent Policy--%>
                                                                                <asp:Literal ID="Literal24" runat="server" Text='<%$Resources:ControlsCommon,AbsentPolicy %>'></asp:Literal>
                                                                            </td>
                                                                            <td align="left" width="30%">
                                                                                <asp:UpdatePanel ID="upnlAbsentPolicy" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="ddlAbsentPolicy" runat="server" CssClass="dropdownlist" DataTextField="Description"
                                                                                                        Width="175px" DataValueField="AbsentPolicyID">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td width="18%" class="firstTrLeft">
                                                                                <%--  Encash Policy--%>
                                                                                <asp:Literal ID="Literal25" runat="server" Text='<%$Resources:ControlsCommon,EncashPolicy %>'></asp:Literal>
                                                                            </td>
                                                                            <td align="left" width="27%">
                                                                                <asp:UpdatePanel ID="upnlEncashPolicy" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="ddlEncashPolicy" Width="175px" runat="server" CssClass="dropdownlist"
                                                                                                        DataTextField="Description" DataValueField="EncashPolicyID">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="firstTrRight" valign="top">
                                                                    <table width="100%" style="font-size: 11px">
                                                                        <tr>
                                                                            <td width="15%" class="firstTrLeft">
                                                                                <%--Holiday Policy--%>
                                                                                <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:ControlsCommon,HolidayPolicy %>'></asp:Literal>
                                                                            </td>
                                                                            <td align="left" width="30%">
                                                                                <asp:UpdatePanel ID="upnlHolidayPolicy" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="ddlHolidayPolicy" runat="server" CssClass="dropdownlist" DataTextField="Description"
                                                                                                        Width="175px" DataValueField="HolidayPolicyID">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td valign="top" width="18%" class="firstTrLeft">
                                                                                <%-- Overtime Policy--%>
                                                                                <asp:Literal ID="Literal27" runat="server" Text='<%$Resources:ControlsCommon,OverTimePolicy %>'></asp:Literal>
                                                                            </td>
                                                                            <td align="left" width="27%">
                                                                                <asp:UpdatePanel ID="upnlOvertimePolicy" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="ddlOvertimePolicy" Width="175px" runat="server" CssClass="dropdownlist"
                                                                                                        DataTextField="Description" DataValueField="OTPolicyID">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div>
                                                <asp:CustomValidator ID="cvAddAllowances" runat="server" ClientValidationFunction="validateAddAllowanceDeduction"
                                                    ValidationGroup="Submit" Display="None" ErrorMessage='<%$Resources:ControlsCommon,PleaseAddBasicPay %>'
                                                    CssClass="error"></asp:CustomValidator>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="chkAlterSalary" EventName="CheckedChanged" />
            </Triggers>
        </asp:UpdatePanel>
        <div class="MainDivAction" style="height: 70px;">
            <div class="Left">
                <%-- Remarks--%>
                <asp:Literal ID="Literal28" runat="server" Text='<%$Resources:ControlsCommon,Remarks %>'></asp:Literal>
            </div>
            <div class="Middle">
                :
            </div>
            <div class="Right">
                <asp:TextBox ID="txtActionRemarks" runat="server" CssClass="textbox" onchange="RestrictMulilineLength(this, 500);"
                    onkeyup="RestrictMulilineLength(this, 500);" TextMode="MultiLine" Width="94%"
                    Height="50px" Style="padding: 5px;"></asp:TextBox>
            </div>
        </div>
        <div class="MainDivAction" style="height: 70px;">
            <div class="Left">
                <%--Feedback--%>
                <asp:Literal ID="Literal29" runat="server" Text='<%$Resources:ControlsCommon,Feedback %>'></asp:Literal>
            </div>
            <div class="Middle">
                :
            </div>
            <div class="Right">
                <asp:TextBox ID="txtFeedback" runat="server" CssClass="textbox" onchange="RestrictMulilineLength(this, 500);"
                    onkeyup="RestrictMulilineLength(this, 500);" TextMode="MultiLine" Width="94%"
                    Height="50px" Style="padding: 5px;"></asp:TextBox>
            </div>
        </div>
        <div id="divFooter" style="width: 96.5%; text-align: right;">
            <asp:Button ID="btnSubmit" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit %>'
                runat="server" ToolTip='<%$Resources:ControlsCommon,Submit %>' ValidationGroup="Submit" OnClick="btnSubmit_Click" />
            &nbsp;
            <asp:Button ID="btnCancel" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel %>'
                runat="server" ToolTip='<%$Resources:ControlsCommon,Cancel %>' OnClick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
</asp:Content>
