<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="InsuranceCard.aspx.cs" Inherits="Public_InsuranceCard"%>

<%@ Register Src="../Controls/DocumentReceiptIssue.ascx" TagName="DocumentReceiptIssue"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" /> <script src="../js/jquery/jquery.ui.tabs.js" type="text/javascript"></script>

   <div id='cssmenu'>
        <ul>
            <li ><a href="Passport.aspx"><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
            <li><a href="Visa.aspx"><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
            <li><a href="DrivingLicence.aspx"><asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
            <li><a href="Qualification.aspx"><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
            <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1"><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
            <li id="liLabour" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=2"><asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
            <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3"><asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
            <li class="selected"><a href="InsuranceCard.aspx"><asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
            <li><a href="TradeLicense.aspx"><asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
            <li><a href="LeaseAgreement.aspx"><asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
            <li><a href="Insurance.aspx"><asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
            <li><a href="Documents.aspx"><asp:Literal ID="Literal10" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
            <li><a href="ViewDocument.aspx"><asp:Literal ID="Literal11" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="updmain" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align: center; margin-top: 10px;">
                <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="lblNoData" runat="server" CssClass="error"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="dlInsuranceCard" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div>
                <div style="display: none">
                    <asp:Button ID="btnProxy" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
                <asp:Label ID="lblHeading" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
            </div>
            <div id="divAddEmployeeInsurance" style="width: 100%" runat="server">
                <div style="width: auto">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                        <p>
                           <asp:Literal ID="Literal14" runat ="server" Text='<%$Resources:DocumentsCommon,Employee%>'></asp:Literal>   
                        </p>
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:UpdatePanel ID="updEmployeeName" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: left; width: 45%; height: 29px">
                                    <asp:DropDownList ID="ddlEmployeeName" runat="server" CssClass="dropdownlist_mandatory"
                                        Width="190px" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                                <div style="float: right; width: 50%; height: 29px; padding-left: 3px; line-height: normal">
                                    <asp:RequiredFieldValidator ID="rfvEmployeeName" runat="server" ControlToValidate="ddlEmployeeName"
                                       Display="Dynamic" ErrorMessage="*" ValidationGroup="Insurance"
                                        InitialValue="-1"></asp:RequiredFieldValidator>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div style="width: auto">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                       <asp:Literal ID="Literal13" runat ="server" Text='<%$Resources:DocumentsCommon,CardNumber%>'></asp:Literal>    
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <div style="float: left; width: 45%; height: 29px">
                            <asp:TextBox ID="txtCardNumber" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                Width="182px"></asp:TextBox>
                        </div>
                        <div style="float: right; width: 50%; height: 29px; padding-left: 3px">
                            <asp:RequiredFieldValidator ID="rfvCardNumber" runat="server" ControlToValidate="txtCardNumber"
                                Display="Dynamic" ErrorMessage="*" ValidationGroup="Insurance"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div style="width: auto">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                          <asp:Literal ID="Literal15" runat ="server" meta:resourcekey="InsuranceCompany"></asp:Literal>    
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:UpdatePanel ID="updInsuranceCompany" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: left; width: 45%; height: 29px">
                                    <asp:DropDownList ID="ddlInsuranceCompany" runat="server" CssClass="dropdownlist_mandatory"
                                        Width="190px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnInsuranceCompany" runat="server" CausesValidation="False" CssClass="referencebutton"
                                        OnClick="btnInsuranceCompany_Click" Text="..." />
                                </div>
                                <div style="float: right; width: 50%; height: 29px; padding-left: 3px; line-height: normal">
                                    <asp:RequiredFieldValidator ID="rfvInsuranceCompany" runat="server" ControlToValidate="ddlInsuranceCompany"
                                        Display="Dynamic" ErrorMessage="*"
                                        ValidationGroup="Insurance" InitialValue="-1"></asp:RequiredFieldValidator>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div style="width: auto">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                          <asp:Literal ID="Literal16" runat ="server" meta:resourcekey="PolicyNumber"></asp:Literal>    
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:UpdatePanel ID="updPolicyNumber" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: left; width: 45%; height: 29px">
                                    <asp:TextBox ID="txtPolicyNumber" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                        Width="182px"></asp:TextBox>
                                </div>
                                <div style="float: right; width: 50%; height: 29px; padding-left: 3px; line-height: normal">
                                    <asp:RequiredFieldValidator ID="rfvPolicyNumber" runat="server" ControlToValidate="txtPolicyNumber"
                                       Display="Dynamic" ErrorMessage="*"
                                        ValidationGroup="Insurance"></asp:RequiredFieldValidator>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div style="width: auto">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                     <asp:Literal ID="Literal17" runat ="server" meta:resourcekey="PolicyName"></asp:Literal>   
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:UpdatePanel ID="updPolicyName" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: left; width: 45%; height: 29px">
                                    <asp:TextBox ID="txtPolicyName" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                        Width="182px"></asp:TextBox>
                                </div>
                                <div style="float: right; width: 50%; height: 29px; padding-left: 3px; line-height: normal">
                                    <asp:RequiredFieldValidator ID="rfvPolicyName" runat="server" ControlToValidate="txtPolicyName"
                                      Display="Dynamic" ErrorMessage="*" ValidationGroup="Insurance">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div style="width: auto">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                       <asp:Literal ID="Literal18" runat ="server" meta:resourcekey="CardIssued"></asp:Literal>  
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:UpdatePanel ID="updCardIssued" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: left; width: 45%; height: 29px">
                                    <asp:DropDownList ID="ddlCardIssued" runat="server" CssClass="dropdownlist_mandatory"
                                        Width="190px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnCardIssued" runat="server" CausesValidation="False" CssClass="referencebutton"
                                        OnClick="btnCardIssued_Click" Text="..." />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                       <asp:Literal ID="Literal19" runat ="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>    
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <div style="float: left; width: 30%; height: 29px">
                            <asp:TextBox ID="txtIssuedate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"></asp:TextBox>&nbsp
                            <asp:ImageButton ID="btnIssuedate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <AjaxControlToolkit:CalendarExtender ID="ceIssuedate" runat="server" Animated="true"
                                Format="dd/MM/yyyy" PopupButtonID="btnIssuedate" TargetControlID="txtIssuedate" />
                        </div>
                        <div style="float: right; width: 65%; height: 29px; padding-left: 3px; line-height: normal">
                            <asp:CustomValidator ID="cvIssuedate" runat="server" ControlToValidate="txtIssuedate"
                                ClientValidationFunction="validatepassportIssuedate" CssClass="error" ValidateEmptyText="True"
                                ValidationGroup="Insurance" Display="Dynamic"></asp:CustomValidator>
                        </div>
                    </div>
                </div>
                <div style="height: auto">
                    <div class="trLeft" style="float: left; width: 25%; height: 50px">
                         <asp:Literal ID="Literal20" runat ="server" Text='<%$Resources:DocumentsCommon, ExpiryDate%>'></asp:Literal> 
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 50px">
                        <div style="float: left; width: 30%; height: 29px">
                            <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"></asp:TextBox>&nbsp
                            <asp:ImageButton ID="btnExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <AjaxControlToolkit:CalendarExtender ID="ceExpiryDate" runat="server" Animated="true"
                                Format="dd/MM/yyyy" PopupButtonID="btnExpirydate" TargetControlID="txtExpiryDate" />
                        </div>
                        <div style="float: right; width: 65%; height: 29px; padding-left: 3px; line-height: normal">
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtExpiryDate"
                                CssClass="error" ValidateEmptyText="True" ValidationGroup="Insurance" ClientValidationFunction="validatepassportExpirydate"
                                Display="Dynamic"></asp:CustomValidator>
                        </div>
                    </div>
                </div>
                <div style="width: auto; height: auto; margin-top: 20px;">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                       <asp:Literal ID="Literal21" runat ="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal>    
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:UpdatePanel ID="updRemarks" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: left; width: 45%; height: 29px">
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="textbox" Width="300px" TextMode="MultiLine"
                                        MaxLength="200" Height="76px" onchange="RestrictMulilineLength(this, 200);" onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div style="width: auto; height: auto; margin-top: 25px;">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px; margin-top: 40px;">
                        <asp:Literal ID="Literal22" runat ="server" Text='<%$Resources:DocumentsCommon,CurrentStatus%>'></asp:Literal>   
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px; margin-top: 40px;">
                        <asp:Label ID="lblCurrentStatus" runat="server" Text="New" Width="30%"></asp:Label>
                    </div>
                </div>
                <div class="trLeft" style="float: left; width: 95%; height: 100px">
                    <div id="divParse">
                        <fieldset style="padding: 3px 1px;">
                            <legend> <asp:Literal ID="Literal23" runat ="server" Text='<%$Resources:DocumentsCommon,AddDocuments%>'></asp:Literal>  </legend>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-left: 10px">
                                <tr>
                                    <td valign="top">
                                      <asp:Literal ID="Literal24" runat ="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>     
                                    </td>
                                    <td valign="top" style="width: 3px">
                                    </td>
                                    <td valign="top">
                                       <asp:Literal ID="Literal25" runat ="server" Text='<%$Resources:DocumentsCommon,Choosefile%>'></asp:Literal>  
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" width="175px">
                                        <asp:TextBox ID="txtDocname" runat="server" CssClass="textbox_mandatory" Width="150px"
                                            MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDocname" runat="server" ControlToValidate="txtDocname"
                                          Display="Dynamic" ErrorMessage="*"
                                            ValidationGroup="AddDocs"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="cvotherdocnameduplicate" runat="server" ClientValidationFunction="validateotherdocnameduplicate"
                                            ControlToValidate="txtDocname" ValidationGroup="AddDocs" Display="Dynamic" ErrorMessage='<%$Resources:DocumentsCommon,Duplicatedocumentname%>'></asp:CustomValidator>
                                    </td>
                                    <td style="width: 2px">
                                    </td>
                                    <td valign="top" width="250px">
                                        <AjaxControlToolkit:AsyncFileUpload ID="fuDocument" runat="server" PersistFile="True"
                                            Width="250px" Visible="true" CompleteBackColor="White" ErrorBackColor="White"
                                            OnUploadedComplete="fuDocument_UploadedComplete" />
                                        <asp:CustomValidator ID="cvotherdocumentAttachment" runat="server" ErrorMessage='<%$Resources:DocumentsCommon,Pleaseattachfile%>'
                                            CssClass="error" ValidateEmptyText="True" ValidationGroup="AddDocs" ClientValidationFunction="validateotherdocumentAttachment"></asp:CustomValidator>
                                    </td>
                                    <td valign="top" width="70px">
                                        <asp:LinkButton ID="btnattachpassdoc"  Text='<%$Resources:DocumentsCommon,Addtolist%>' Enabled="true" runat="server"
                                            ValidationGroup="AddDocs" OnClick="btnattachpassdoc_Click"> </asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <div class="trLeft" style="float: left; width: 95%; height: 100px; margin-top: 20px;">
                    <div id="divOtherDoc" runat="server" class="container_content" style="display: none;
                        height: 150px; overflow: auto;">
                        <asp:UpdatePanel ID="updOtherDoc" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:DataList ID="dlOtherDoc" runat="server" Width="100%" DataKeyField="Node" GridLines="Horizontal"
                                    OnItemCommand="dlOtherDoc_ItemCommand">
                                    <HeaderStyle CssClass="datalistheader" />
                                    <HeaderTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td width="241">
                                                 <asp:Literal ID="Literal24" runat ="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal> 
                                                </td>
                                                <td width="110" align="left">
                                                  <asp:Literal ID="Literal26" runat ="server" Text='<%$Resources:DocumentsCommon,FileName%>'></asp:Literal> 
                                                </td>
                                                <td width="20" style="float: right;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td width="150">
                                                    <asp:Label ID="lblDocname" runat="server" Text='<%# Eval("Docname")%>' Width="100%"></asp:Label>
                                                </td>
                                                <td width="130" align="left">
                                                    <asp:Label ID="lblFilename" runat="server" Text='<%# Eval("Filename")%>' Width="100%"></asp:Label>
                                                </td>
                                                <td width="20" align="left">
                                                    <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/images/Delete_Icon_Datalist.png"  ToolTip='<%$Resources:ControlsCommon,Delete%>'
                                                        CommandArgument='<% # Eval("Node") %>' CommandName="REMOVE_ATTACHMENT" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="trRight" style="float: right; height: 29px; text-align: right; margin-top: 2px;">
                        <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' ToolTip='<%$Resources:ControlsCommon,Submit%>'
                            ValidationGroup="Insurance" OnClick="btnSubmit_Click" Width="75px" />
                        <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server"  Text='<%$Resources:ControlsCommon,Cancel%>' ToolTip='<%$Resources:ControlsCommon,Cancel%>' CausesValidation="false"
                           OnClick="btnCancel_Click" Width="75px" />
                        <asp:HiddenField ID="hdnInsuranceID" runat="server" />
                        <asp:HiddenField ID="hdnMode" runat="server" />
                    </div>
                </div>
                <!--Modal popup for card issued -->
                <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="Button1" runat="server" />
                        </div>
                        <asp:Panel ID="pnlModalPopUp" Style="display: none" runat="server">
                            <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                            PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="divDatalistView" style="width: 100%" runat="server">
                <asp:DataList ID="dlInsuranceCard" runat="server" Width="100%" DataKeyField="InsuranceCardID"
                    OnItemCommand="dlInsuranceCard_ItemCommand" CssClass="labeltext"
                    onitemdatabound="dlInsuranceCard_ItemDataBound">
                    <ItemStyle CssClass="item" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <table cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 5px">
                            <tr>
                                <td height="30" width="30">
                                    <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id, 'chkItem');" />
                                </td>
                                <td>
                                   <asp:Literal ID="Literal26" runat ="server"  Text='<%$Resources:ControlsCommon,SelectAll%>' ></asp:Literal>    
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <HeaderStyle CssClass="listItem" />
                    <ItemTemplate>
                        <table cellpadding="2" cellspacing="0" style="width: 100%; padding-left: 5px" id="tblDetails"
                            runat="server">
                            <tr valign="top">
                                <td valign="top" width="30" style="padding-top: 4px">
                                    <asp:CheckBox ID="chkItem" runat="server" />
                                </td>
                                <td valign="top">
                                    <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="lnkInsuranceCard" CausesValidation="false" runat="server" CommandArgument='<%# Eval("InsuranceCardID")%>'
                                                                CommandName="VIEW" Text='<%# string.Format("{0} {1}", Eval("SalutationName"), Eval("EmployeeFullName")) %>' CssClass="listHeader bold"></asp:LinkButton>
                                                                [<%# Eval("EmployeeNumber")%>] &nbsp <asp:Literal ID="Literal13" runat ="server" Text='<%$Resources:DocumentsCommon,CardNumber%>'></asp:Literal>  - <%# Eval("CardNumber") %>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td valign="top" align="right" width="55px">
                                                            <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="false" ImageUrl="~/images/edit.png"
                                                                CommandArgument='<%# Eval("InsuranceCardID")%>' CommandName="ALTER"  ToolTip='<%$Resources:ControlsCommon,Edit%>' />
                                                        </td>
                                                        <td style="visibility: hidden">
                                                            <asp:Label ID="lblIsDelete" runat="server" Text='<%# Eval("IsDelete")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 20px">
                                                <table width="100%">
                                                    <tr>
                                                        <td class="innerdivheader" width="150px">
                                                          <asp:Literal ID="Literal17" runat ="server" meta:resourcekey="InsuranceCompany"></asp:Literal>   
                                                        </td>
                                                        <td width="5">
                                                            :
                                                        </td>
                                                        <td class="innerdivheaderValue">
                                                            <%# Eval("Description")%>
                                                        </td>
                                                        <td style="color: #FF0000; font-weight: bold; float: right;">
                                                            <%# Eval("IsRenewedText")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="innerdivheader">
                                                          <asp:Literal ID="Literal27" runat ="server" meta:resourcekey="PolicyNumber"></asp:Literal>  
                                                        </td>
                                                        <td>
                                                            :
                                                        </td>
                                                        <td class="innerdivheaderValue">
                                                            <%# Eval("PolicyNumber")%>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="innerdivheader">
                                                           <asp:Literal ID="Literal28" runat ="server" meta:resourcekey="PolicyName"></asp:Literal> 
                                                        </td>
                                                        <td>
                                                            :
                                                        </td>
                                                        <td class="innerdivheaderValue">
                                                            <%# Eval("PolicyName")%>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="pgrInsuranceCards" runat="server" OnFill="BindPagerNumber" />
            </div>
            <div id="divViewSingleCards" style="width: 100%; margin-left: 50px; display: none;
                margin-top: 15px;" runat="server">
                <div class="innerdivheader" style="width: 100%; float: left; height: 20px; margin-top: 6px;">
                    <h4>
                         <asp:Literal ID="Literal37" runat ="server" meta:resourcekey="EmployeeInsuranceCard"></asp:Literal>  </h4>
                </div>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;">
                    <asp:Literal ID="Literal26" runat ="server"  Text='<%$Resources:DocumentsCommon,Employee%>'></asp:Literal>  
                </div>
                <div class="innerdivheaderValue" style="width: 60%; float: left; height: 20px; margin-top: 6px;"
                    id="divEmpName" runat="server">
                </div>
                <%--<div id="Div1" class="innerdivheaderValue" style="width: 15%; float: left; height: 20px;
                    margin-top: 6px;" runat="server">
                    <asp:ImageButton ID="ImgEdit" runat="server" CausesValidation="false" ImageUrl="~/images/edit.png"
                        OnClick="ImgEdit_OnClick" ToolTip="Edit" />
                </div>--%>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;"
                    id="divCardNo" runat="server">
                     <asp:Literal ID="Literal29" runat ="server"  Text='<%$Resources:DocumentsCommon,CardNumber%>'></asp:Literal>    
                </div>
                <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 6px;"
                    id="divCardNumber" runat="server">
                </div>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;">
                     <asp:Literal ID="Literal27" runat ="server" meta:resourcekey="PolicyNumber"></asp:Literal>   
                </div>
                <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 6px;"
                    id="divPolicyNumber" runat="server">
                </div>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;">
                      <asp:Literal ID="Literal30" runat ="server" meta:resourcekey="PolicyName"></asp:Literal>  
                </div>
                <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 6px;"
                    id="divPolicyName" runat="server">
                </div>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;">
                     <asp:Literal ID="Literal31" runat ="server" meta:resourcekey="InsuranceCompany"></asp:Literal>   
                </div>
                <div id="divInsuranceCompany" runat="server" class="innerdivheaderValue" style="width: 73%;
                    float: left; height: 20px; margin-top: 6px;">
                </div>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;">
                     <asp:Literal ID="Literal33" runat ="server"  Text='<%$Resources:DocumentsCommon, IssuedBy%>'></asp:Literal> 
                </div>
                <div id="divIssuedBy" runat="server" class="innerdivheaderValue" style="width: 73%;
                    float: left; height: 20px; margin-top: 6px;">
                </div>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;">
                      <asp:Literal ID="Literal32" runat ="server"  Text='<%$Resources:DocumentsCommon, IssueDate%>'></asp:Literal>   
                </div>
                <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 6px;"
                    id="divIssueDate" runat="server">
                </div>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;">
                    <asp:Literal ID="Literal34" runat ="server"  Text='<%$Resources:DocumentsCommon, ExpiryDate%>'></asp:Literal> 
                </div>
                <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 6px;"
                    id="divExpiryDate" runat="server">
                </div>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;"
                    id="divRenewStatus" runat="server">
                   <asp:Literal ID="Literal35" runat ="server"  Text='<%$Resources:DocumentsCommon,IsRenewed%>'></asp:Literal>   
                </div>
                <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 6px;"
                    id="divRenew1" runat="server">
                </div>
                <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 6px;">
                    <asp:Literal ID="Literal36" runat ="server"  Text='<%$Resources:DocumentsCommon, OtherInfo%>'></asp:Literal> 
                </div>
                <div class="innerdivheaderValue" style="width: 67%; float: left; min-height: 30px;
                    height: auto; margin-top: 6px; word-break: break-all;" id="divRemarks" runat="server">
                </div>
                <div id="divIsDelete" runat="server" style="visibility: hidden"></div>
            </div>
            
            <asp:UpdatePanel ID="upPrint" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div id="divPrint" runat="server" style="display: block">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upnlIssueReceipt" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none">
                        <asp:Button ID="btnIssueReceipt" runat="server" />
                    </div>
                    <asp:Panel ID="pnlIssueReceipt" Style="display: none" runat="server">
                        <uc1:DocumentReceiptIssue ID="ucDocIssueReceipt" runat="server" ModalPopupID="mpeIssueReceipt">
                        </uc1:DocumentReceiptIssue>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeIssueReceipt" runat="server" TargetControlID="btnIssueReceipt"
                        PopupControlID="pnlIssueReceipt" BackgroundCssClass="modalBackground">
                    </AjaxControlToolkit:ModalPopupExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 5px">
                    <tr>
                        <td style="width: 65%">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="WmCss" MaxLength="50"></asp:TextBox>
                        </td>
                        <td style="width: 35%; text-align: left; padding-left: 8px">
                            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" SkinID="GoButton"
                                ImageUrl="~/images/search.png" ToolTip="Click here to search" ImageAlign="AbsMiddle"
                                OnClick="btnSearch_Click" />
                            <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtSearch"
                                 WatermarkText='<%$Resources:DocumentsCommon,SearchBy%>' WatermarkCssClass="WmCss">
                            </AjaxControlToolkit:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddEmp" ImageUrl="~/images/Insurance_add.png" runat="server"
                        Enabled="false" CausesValidation="false" />
                </div>
                <div class="name">
                  <h5> <asp:LinkButton ID="lnkAddInsuranceCard" runat="server" OnClick="lnkAddInsuranceCard_Click" meta:resourcekey="AddInsuranceCard"> 
                           </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListEmp" ImageUrl="~/images/insurance_list.png" runat="server" 
                        Enabled="false"  CausesValidation="false"/>
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkListInsuranceCards" runat="server" OnClick="lnkListInsuranceCards_Click" meta:resourcekey="ViewInsuranceCard"> 
                          </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" Enabled="false"  CausesValidation="false"/>
                </div>
                <div class="name">
                    <h5><asp:LinkButton ID="lnkDelete" runat="server" OnClick="btnDelete_Click" Text='<%$Resources:ControlsCommon,Delete%>'> 
                           </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" Enabled="false"  CausesValidation="false"/>
                </div>
                <div class="name">
                    <h5><asp:LinkButton ID="lnkPrint" runat="server" OnClick="btnPrint_Click" Text='<%$Resources:ControlsCommon,Print%>'>
                           </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" Enabled="false"  CausesValidation="false"/>
                </div>
                <div class="name">
                     <h5><asp:LinkButton ID="lnkEmail" runat="server" OnClick="btnEmail_Click" Text='<%$Resources:ControlsCommon,Email%>'>
                        </asp:LinkButton></h5> 
                </div>
                <div id="divIssueReceipt" runat="server" style="display: block">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgDocIR" ImageUrl="~/images/document_reciept.png" runat="server" />
                    </div>
                    <div id="Div2" class="name" runat="server">
                       <h5>  <asp:LinkButton ID="lnkDocumentIR" runat="server" OnClick="lnkDocumentIR_Click" Text='<%$Resources:DocumentsCommon,Receipt%>'>
                                </asp:LinkButton></h5>  
                    </div>
                </div>
                
                <div id="divRenew" runat="server">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgRenew" ImageUrl="~/images/ShiftPolicy.png" runat="server" />
                    </div>
                    <div id="Div4" class="name" runat="server">
                          <h5><asp:LinkButton ID="lnkRenew" runat="server" OnClick="lnkRenew_Click" Text='<%$Resources:DocumentsCommon,Renew%>'>
                           
                                  </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
