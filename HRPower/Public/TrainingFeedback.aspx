﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="TrainingFeedback.aspx.cs" Inherits="Public_TrainingFeedback" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="../css/common.css" rel="Stylesheet" type="text/css" />
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">

<div id='cssmenu'>
        <ul>
            <li ><a href='TrainingFeedback.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text="Training Course"></asp:Literal></span></a></li>
            <li><a href='TrainingFeedback.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text="Training Schedule"></asp:Literal></span></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal92" runat="server" Text="Attendance"></asp:Literal></a></li>
            <li class='selected' ><a href="#">
                <asp:Literal ID="Literal93" runat="server" Text="Feedback"></asp:Literal></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal94" runat="server" Text="Employee Review"></asp:Literal></a></li>
        </ul>
    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">
 <div class="TabbedPanelsContent" id="divTab1" style="display: block;">
    <div class="trLeft" style="float: left; width: 25%; height: 29px">
       <asp:Literal ID="Literal1" runat="server" meta:resourcekey="TrainingScheduleTitle"></asp:Literal>
    </div>
     <div class="trRight" style="float: left; width: 70%; height: 29px">
         <asp:DropDownList ID="ddltraining" runat="server">
         </asp:DropDownList>
     </div>
      <div class="trLeft" style="float: left; width: 25%; height: 29px">
       <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Feedbackdate"></asp:Literal>
      </div>
       <div class="trRight" style="float: left; width: 70%; height: 29px">
           <asp:TextBox ID="txtcurrentdate" runat="server"></asp:TextBox>
       </div>
       <div class="trLeft" style="float: left; width: 25%; height: 29px">
       <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Rating"></asp:Literal>
      </div>
      <div class="trRight" style="float: left; width: 70%; height: 29px">
       <AjaxControlToolkit:Rating ID="ajrate" runat="server" EmptyStarCssClass ="emptyRatingStar" FilledStarCssClass="filledRatingStar" CurrentRating="5"
        MaxRating="10" StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar"  RatingAlign="Horizontal"  ReadOnly="true" >
       </AjaxControlToolkit:Rating>
      </div>
       <div class="trLeft" style="float: left; width: 25%; height: 29px">
       <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Remarks"></asp:Literal>
      </div>
       <div class="trRight" style="float: left; width: 70%; height: 70px">
           <asp:TextBox ID="txtremark" runat="server" TextMode="MultiLine" Height="70px" MaxLength="1000" Width="280px"></asp:TextBox>
       </div>
       <div class="trRight" style="float: left; width: 97%; height: 29px">
       </div>
     
      
      <div class="trRight" style="float: left; width: 70%; height: 29px">
          <p>
      <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,SUbmit %>' CommandName="SUBMIT"
          Text='<%$Resources:ControlsCommon,SUbmit %>'  Width="75px" />
       <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Cancel %>' CausesValidation="false"
                            Text='<%$Resources:ControlsCommon,Cancel %>' CommandName="_Cancel"  Width="75px" />
          </p>
      </div>
    
 </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
</asp:Content>

