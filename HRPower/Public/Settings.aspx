﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Settings.aspx.cs" Inherits="Public_Settings" Title="Users" %>

<%@ Register src="../Controls/AddUser.ascx" tagname="AddUser" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Company.aspx'>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'>
                <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>
            <li><a href="Bank.aspx">
                <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx">
                <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li class="selected"><a href="Settings.aspx">
                <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx">
                <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx">
                <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx">
                <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx">
                <asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx">
                <asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx">
                <asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div style="width: 100%; float: left;">
        <div style="width: 100%; float: left;">
            <div style="display: none">
                <asp:Button ID="btnSubmit" runat="server" Text="submit" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="upnlMessage" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage"  runat="server" TargetControlID="btnSubmit"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
        <div style="width: 100%; float: left;">
            <asp:UpdatePanel ID="upGridView" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView ID="gvUserSettings" runat="server" AutoGenerateColumns="False" DataKeyNames="UserID,EmployeeID"
                        OnRowCommand="gvUserSettings_RowCommand" Width="100%" CellPadding="1" CellSpacing="0"
                        GridLines="None" OnRowDataBound="gvUserSettings_RowDataBound" CssClass="labeltext"
                        OnSelectedIndexChanged="gvUserSettings_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id,'chkUserSettings')" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkUserSettings" runat="server" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    &nbsp;</EditItemTemplate>
                                <HeaderStyle Width="25px" />
                                <ItemStyle VerticalAlign="Top" CssClass="trLeft" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Employee</HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("EmployeeFullName") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlEmployee" runat="server" Width="150px" CssClass="dropdownlist_mandatory"
                                        DataTextField="EmployeeFullName" DataValueField="EmployeeID">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvddlEmployee" runat="server" ControlToValidate="ddlEmployee"
                                        ErrorMessage="Select employee" SetFocusOnError="True" Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <ItemStyle VerticalAlign="Top" CssClass="trLeft" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    User Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("UserName")%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                        Width="115px" Text='<%# Eval("UserName")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtUserName" runat="server" ControlToValidate="txtUserName"
                                        ErrorMessage="Enter Username" SetFocusOnError="True" ValidationGroup="submit"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <HeaderStyle Width="120px" />
                                <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPassword" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                        Width="90px"></asp:TextBox>
                                    <asp:HiddenField ID="hdpass" runat="server" Value='<%# bind("Password") %>' />
                                    <asp:RequiredFieldValidator ID="rfvtxtPassword" runat="server" ControlToValidate="txtPassword"
                                        Display="Dynamic" ErrorMessage="Enter password" SetFocusOnError="True" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <HeaderStyle Width="90px" />
                                <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                            </asp:TemplateField>
                         <%--   <asp:TemplateField>
                                <HeaderTemplate>
                                    Role
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("UserRole")%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlRole" runat="server" CssClass="dropdownlist_mandatory" DataTextField="RoleName"
                                        DataValueField="RoleID" Width="130px">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvddlRole" runat="server" ControlToValidate="ddlRole"
                                        InitialValue="-1" ErrorMessage="Select role" SetFocusOnError="True" ValidationGroup="submit"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <HeaderStyle Width="120px" />
                                <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                            </asp:TemplateField>--%>
                         
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnModify" runat="server" CommandName="Alter" CommandArgument='<%#Eval("EmployeeID") %>'
                                        ImageUrl="~/images/edit_icon_popup.jpg" ToolTip="edit" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div style="width: 100%; float: left;">
                                        <div style="width: 25px; float: left;">
                                            <asp:ImageButton ID="ibtnUpdate" runat="server" CssClass="imagebutton" CommandArgument='<%#Eval("UserID") %>'
                                                CommandName="Add" ValidationGroup="submit" ImageUrl="~/images/save_icon.jpg"
                                                ToolTip="save" />
                                        </div>
                                        <div style="float: left; width: auto;">
                                            <asp:ImageButton ID="ibtnCancel" runat="server" CssClass="imagebutton" CommandName="Resume"
                                                CommandArgument='<%#Eval("UserID") %>' ImageUrl="~/images/cancel_icon.jpg" ToolTip="cancel" />
                                        </div>
                                    </div>
                                </EditItemTemplate>
                                <HeaderStyle Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                        <HeaderStyle CssClass="datalistheader" />
                        <EditRowStyle HorizontalAlign="Left" />
                    </asp:GridView>
                    <uc:Pager ID="UserSettingsPager" runat="server" PageSize="10" />
                    <asp:Label ID="lblUserSettings" runat="server" Style="display: block;" CssClass="error"
                        Visible="False"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="updAddUser" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <AjaxControlToolkit:ModalPopupExtender PopupDragHandleControlID="dvAddUser" RepositionMode="None"
                    Drag="true" PopupControlID="dvAddUser" TargetControlID="btn" X="400" Y="200" ID="mdAddUser" runat="server">
                </AjaxControlToolkit:ModalPopupExtender>
                <asp:Button ID="btn" runat="server" Style="display: none" />
                <div id="dvAddUser" style="width: 100%; height: 80%; overflow: scroll">
              
                    <uc1:AddUser ID="AddUser1" runat="server" />
              
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <div id="search" style="vertical-align: top">
        <div id="text">
        </div>
        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
       
        <br />
        <div id="searchimg">
            <%-- <img src="../images/search.png">--%>
            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" ImageUrl="../images/search.png"
                ToolTip="Click here to search" ImageAlign="AbsMiddle" OnClick="btnSearch_Click" />
            <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtSearch"
                WatermarkText="Search By" WatermarkCssClass="WmCss">
            </AjaxControlToolkit:TextBoxWatermarkExtender>
        </div>
    </div>
    <asp:UpdatePanel ID="upnlMenus" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="sidebarsettings" style="width: 100%; float: left;">
                <div style="width: 100%; float: left;">
                    <div class="sideboxImage">
                        <img src="../images/User.PNG" />
                    </div>
                    <div class="name">
                        <asp:LinkButton ID="lnkAdd" runat="server" Text="Add User" CausesValidation="False"
                            CssClass="labeltext" OnClick="lnkAdd_Click"></asp:LinkButton>
                    </div>
                </div>
                <div style="width: 100%; float: left;">
                    <div class="sideboxImage">
                        <img src="../images/Delete.png" />
                    </div>
                    <div class="name">
                        <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete Users" CausesValidation="False"
                            CssClass="labeltext" OnClick="lnkDelete_Click"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
