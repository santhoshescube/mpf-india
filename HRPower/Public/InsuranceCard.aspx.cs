﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using System.Globalization;
using HRAutoComplete;
using System.IO;
using System.Text;

/// <Created By>Mehga</Created>
/// <created on ></created>
/// </summary>
/// <Modified by> Jisha Bijoy</Modified>
/// <Modified On> Aug 2 2013</Modified>
/// <Description> Insert alert message when creating an insurance card</Description>
public partial class Public_InsuranceCard : System.Web.UI.Page
{
    clsInsuranceCard objclsInsuranceCard;
    clsDocumentAlert objAlert;
    clsRoleSettings objRoleSettings;
    clsUserMaster objUser;
    private string CurrentSelectedValue { get; set; }
    private DateTime RenewDate { get; set; }
    private bool MblnUpdatePermission = true;  // FOR datalist Edit Button


    protected void Page_Load(object sender, EventArgs e)
    {
        // Auto complete
        this.RegisterAutoComplete();
        //
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        objclsInsuranceCard = new clsInsuranceCard();
        if (!IsPostBack)
        {


            CurrentSelectedValue = "-1";
            divAddEmployeeInsurance.Style["display"] = "none";
            divDatalistView.Style["display"] = "block";
            lblNoData.Style["display"] = "none";
            divViewSingleCards.Style["display"] = "none";
            divPrint.Style["display"] = "none";
            divIssueReceipt.Style["display"] = "none";
            divRenew.Style["display"] = "none";
            hdnMode.Value = "0";
            SetPermission();
            EnableMenus();
            

        }
        ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);

        //ReferenceControlNew1.onRefresh -= new controls_ReferenceControlNew.OnPageRefresh(ReferenceControlNew1_onRefresh);
        //ReferenceControlNew1.onRefresh += new controls_ReferenceControlNew.OnPageRefresh(ReferenceControlNew1_onRefresh);

        ucDocIssueReceipt.OnSave -= new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
        ucDocIssueReceipt.OnSave += new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
    }

    #region Right Side Bar

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        updmain.Update();
        divAddEmployeeInsurance.Style["display"] = "none";
        divDatalistView.Style["display"] = "block";
        lblNoData.Style["display"] = "none";
        divViewSingleCards.Style["display"] = "none";
        divPrint.Style["display"] = "none";
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        pgrInsuranceCards.CurrentPage = 0;
        EnableMenus();
        BindPagerNumber();

        this.RegisterAutoComplete();

        upMenu.Update();
        updmain.Update();
        upnlNoData.Update();
        upnlIssueReceipt.Update();
        //updOtherDoc.Update();
    }

    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.InsuranceCard, objUser.GetCompanyId());
    }

    protected void lnkAddInsuranceCard_Click(object sender, EventArgs e)
    {
        ControlEnableDisble(true);
        updmain.Update();
        pgrInsuranceCards.CurrentPage = 0;
        LoadInitials();
        ClearControls();
        lnkDelete.OnClientClick = lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return false;";
        lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = false;
        divAddEmployeeInsurance.Style["display"] = "block";
        divDatalistView.Style["display"] = "none";
        lblNoData.Style["display"] = "none";
        divViewSingleCards.Style["display"] = "none";
        divPrint.Style["display"] = "none";
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";

        upMenu.Update();
        updmain.Update();
        upnlNoData.Update();
        upnlIssueReceipt.Update();
        //updOtherDoc.Update();
    }
    protected void lnkListInsuranceCards_Click(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        updmain.Update();

        divAddEmployeeInsurance.Style["display"] = "none";
        divDatalistView.Style["display"] = "block";
        lblNoData.Style["display"] = "none";
        divViewSingleCards.Style["display"] = "none";
        divPrint.Style["display"] = "none";
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
         EnableMenus();
        lnkDelete.OnClientClick = lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return true;";
        lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = true;
        pgrInsuranceCards.CurrentPage = 0;
        BindDataList();
        BindPagerNumber();

        upMenu.Update();
        updmain.Update();
        upnlNoData.Update();

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        CheckBox chkItem;
        LinkButton lnkInsuranceCard;
        Label lblIsDelete;
        objclsInsuranceCard = new clsInsuranceCard();

        string strMessage = string.Empty;

        if (dlInsuranceCard.Items.Count > 0)
        {
            foreach (DataListItem item in dlInsuranceCard.Items)
            {
                chkItem = (CheckBox)item.FindControl("chkItem");
                lnkInsuranceCard = (LinkButton)item.FindControl("lnkInsuranceCard");
                lblIsDelete = (Label)item.FindControl("lblIsDelete");

                if (chkItem == null)
                    continue;

                if (chkItem.Checked == false)
                    continue;

                if (Convert.ToBoolean(lblIsDelete.Text.ToInt32()))
                {
                    this.objclsInsuranceCard.DeleteInsuranceCard(dlInsuranceCard.DataKeys[item.ItemIndex].ToInt32());
                    strMessage = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString();
                }
                else
                    strMessage = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();
            }            
        }
        else
        {
            if (Convert.ToBoolean(divIsDelete.InnerText.ToInt32()))
            {
                this.objclsInsuranceCard.DeleteInsuranceCard(hdnInsuranceID.Value.ToInt32());
                strMessage = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString(); ;
                
            }
            else
                strMessage = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();
        }

        if (strMessage == "")
            strMessage = GetGlobalResourceObject("DocumentsCommon", "Pleaseselectanitemfordelete").ToString();

        mcMessage.InformationalMessage(strMessage);
        mpeMessage.Show();

        ClearControls();

        divAddEmployeeInsurance.Style["display"] = "none";
        divDatalistView.Style["display"] = "block";
        lblNoData.Style["display"] = "none";
        divViewSingleCards.Style["display"] = "none";
        divPrint.Style["display"] = "none";

        BindDataList();
        BindPagerNumber();

        upMenu.Update();
        updmain.Update();
        upnlNoData.Update();
        //upnlIssueReceipt.Update();
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {

        upMenu.Update();
        if (hdnInsuranceID.Value.ToInt32() == 0)
        {
            CheckBox chkItem;

            if (dlInsuranceCard.Items.Count > 0)
            {
                string InsuranceCardIDs = string.Empty;

                foreach (DataListItem item in dlInsuranceCard.Items)
                {
                    chkItem = (CheckBox)item.FindControl("chkItem");

                    if (chkItem == null)
                        continue;

                    if (chkItem.Checked == false)
                        continue;

                    InsuranceCardIDs += "," + dlInsuranceCard.DataKeys[item.ItemIndex];
                }

                if (InsuranceCardIDs != string.Empty)
                    InsuranceCardIDs = InsuranceCardIDs.Remove(0, 1);

                objclsInsuranceCard = new clsInsuranceCard();
                if (InsuranceCardIDs.Contains(","))
                    divPrint.InnerHtml = PrintInsuranceCard(InsuranceCardIDs,false);
                else
                {
                    divPrint.InnerHtml = PrintInsuranceCard(InsuranceCardIDs,true);
                }
            }

        }
        else
        {

            divPrint.InnerHtml = PrintInsuranceCard(hdnInsuranceID.Value.ToString(),true);

        }

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);

        upMenu.Update();
        updmain.Update();
        //upnlNoData.Update();
        //upnlIssueReceipt.Update();
    }

    

    private string PrintInsuranceCard(string  InsuranceCardId, bool IsSingle)
    {
        objclsInsuranceCard = new clsInsuranceCard();
       
        DataTable dtPrint = objclsInsuranceCard.PrintSingleInsuranceCard(InsuranceCardId);

        StringBuilder sb = new StringBuilder();
        if (dtPrint != null && dtPrint.Rows.Count > 0)
        {
            string renew = "";
           
            if (IsSingle)
            {

                renew = dtPrint.Rows[0]["IsRenewed"].ToString();
                if (renew.ToBoolean())
                {
                    renew = clsGlobalization.IsArabicCulture() ? "نعم" : "Yes";
                }
                else
                {
                    renew = clsGlobalization.IsArabicCulture() ? "لا" : "No"; ;
                } 
                sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
                sb.Append("<tr><td><table border='0'>");
                sb.Append("<tr>");
                sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + dtPrint.Rows[0]["EmployeeFullName"].ToString() + " -" + Convert.ToString(dtPrint.Rows[0]["CardNumber"]) + "</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<tr><td style='padding-left:15px;'>");
                sb.Append("<table width='100%' style='font-size:11px;' align='left'>");

                sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'> " + GetLocalResourceObject("InsuranceCardDetails.Text").ToString() + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "CardNumber").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dtPrint.Rows[0]["CardNumber"]) + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("PolicyNumber.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dtPrint.Rows[0]["PolicyNumber"]) + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("PolicyName.Text").ToString() + "</td><td width='5px'>:</td><td>" + dtPrint.Rows[0]["PolicyName"].ToString() + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("InsuranceCompany.Text").ToString() + "</td><td width='5px'>:</td><td>" + dtPrint.Rows[0]["Description"].ToString() + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "IssuedBy").ToString() + "</td><td width='5px'>:</td><td>" + dtPrint.Rows[0]["IssuedBy"].ToString() + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td>:</td><td>" + dtPrint.Rows[0]["IssueDate"].ToDateTime().ToString("dd MMM yyyy") + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td>:</td><td>" + dtPrint.Rows[0]["ExpiryDate"].ToDateTime().ToString("dd MMM yyyy") + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td>:</td><td>" + renew + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td><td width='5px'>:</td><td style='word-break:break-all'>" + Convert.ToString(dtPrint.Rows[0]["Remarks"]) + "</td></tr>");

                sb.Append("</td>");
                sb.Append("</table>");
                sb.Append("</td>");
                sb.Append("</tr></table>");
            }
            else
            {
                sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
                sb.Append("<tr><td style='font-weight:bold;' align='center'> " + GetLocalResourceObject("InsuranceCardDetails.Text").ToString() + "</td></tr>");
                sb.Append("<tr><td>");
                sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
                sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + " </td><td width='100px'> " + GetGlobalResourceObject("DocumentsCommon", "CardNumber").ToString() + " </td><td width='150px'>" + GetLocalResourceObject("PolicyNumber.Text").ToString() + "</td><td width='150px'>" + GetLocalResourceObject("PolicyName.Text").ToString() + "</td><td width='150px'>" + GetLocalResourceObject("InsuranceCompany.Text").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon","IssuedBy").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td width='100px'> Is Renewed </td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
                sb.Append("<tr><td colspan='11'><hr></td></tr>");
                for (int i = 0; i < dtPrint.Rows.Count; i++)
                {
                    renew = dtPrint.Rows[i]["IsRenewed"].ToString();
                    if (renew.ToBoolean())
                    {
                        renew = clsGlobalization.IsArabicCulture() ? "نعم" : "Yes";
                    }
                    else
                    {
                        renew = clsGlobalization.IsArabicCulture() ? "لا" : "No"; ;
                    }
                    sb.Append("<td>" + dtPrint.Rows[i]["EmployeeFullName"].ToString() + "</td>");
                    sb.Append("<td>" + Convert.ToString(dtPrint.Rows[i]["CardNumber"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dtPrint.Rows[i]["PolicyNumber"]) + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["PolicyName"].ToString() + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["Description"].ToString() + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["IssuedBy"].ToString() + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["IssueDate"].ToString() + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["ExpiryDate"].ToString() + "</td>");
                    sb.Append("<td>" + renew + "</td>");
                    sb.Append("<td style='word-break:break-all'>" + dtPrint.Rows[i]["Remarks"].ToString() + "</td></tr>");

                }
                sb.Append("</td></tr>");
                sb.Append("</table>");
            }

        }
        return sb.ToString();
    }
    protected void btnEmail_Click(object sender, EventArgs e)
    {

        upMenu.Update();
        string InsuranceCardIDs = string.Empty;

        if (dlInsuranceCard.Items.Count > 0)
        {
            if (hdnInsuranceID.Value.ToInt32() == 0)
            {
                CheckBox chkItem;

                foreach (DataListItem item in dlInsuranceCard.Items)
                {
                    chkItem = (CheckBox)item.FindControl("chkItem");

                    if (chkItem == null)
                        continue;

                    if (chkItem.Checked == false)
                        continue;

                    InsuranceCardIDs += "," + dlInsuranceCard.DataKeys[item.ItemIndex];
                }
                if (InsuranceCardIDs != string.Empty)
                    InsuranceCardIDs = InsuranceCardIDs.Remove(0, 1);
            }
            else
            {
                InsuranceCardIDs = hdnInsuranceID.Value.ToString();
            }

        }
        else
        {
            InsuranceCardIDs = hdnInsuranceID.Value.ToString();
        }
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=InsuranceCard&InsuranceCardIDs=" + InsuranceCardIDs + "', 835, 585);", true);
        upMenu.Update();
        updmain.Update();
        //upnlNoData.Update();
        //upnlIssueReceipt.Update();
    }


    #endregion Right Side Bar

    #region ModalPopUp Events

    protected void btnInsuranceCompany_Click(object sender, EventArgs e)
    {
       // updmain.Update();
        if (updInsuranceCompany != null && ddlInsuranceCompany != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "InsuranceCompanyReference";
            ReferenceControlNew1.DataTextField = "Description";
            ReferenceControlNew1.DataTextFieldArabic = "DescriptionArb";
            ReferenceControlNew1.DataValueField = "InsuranceCompanyId";
            ReferenceControlNew1.FunctionName = "FillddlInsuranceCompany";
            ReferenceControlNew1.SelectedValue = ddlInsuranceCompany.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("InsuranceCompany.Text").ToString();
            //ReferenceControlNew1.PredefinedField = "Predefined";

            ReferenceControlNew1.PopulateData();
            mdlPopUpReference.Show();
            updModalPopUp.Update();
            //updInsuranceCompany.Update();



        }

    }

    protected void btnCardIssued_Click(object sender, EventArgs e)
    {
        updmain.Update();
        if (updCardIssued != null && ddlCardIssued != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "InsuranceCardIssueReference";
            ReferenceControlNew1.DataTextField = "IssuedBy";
            ReferenceControlNew1.DataValueField = "IssuedId";
            ReferenceControlNew1.DataTextFieldArabic = "IssuedByArb";
            ReferenceControlNew1.FunctionName = "FillddlCardIssued";
            ReferenceControlNew1.SelectedValue = ddlCardIssued.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("CardIssued.Text").ToString();
            //ReferenceControlNew1.PredefinedField = "Predefined";

            ReferenceControlNew1.PopulateData();
            mdlPopUpReference.Show();
            updModalPopUp.Update();

        }
    }

    #endregion ModalPopUp Events

    #region Events

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        upMenu.Update();
        hdnMode.Value = "0";
       // EnableMenus();
        lnkDelete.OnClientClick = lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return true;";
        lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = true;
        divAddEmployeeInsurance.Style["display"] = "none";
        divDatalistView.Style["display"] = "block";
        lblNoData.Style["display"] = "none";
        divViewSingleCards.Style["display"] = "none";
        divPrint.Style["display"] = "none";
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        pgrInsuranceCards.CurrentPage = 0;
        BindDataList();
        BindPagerNumber();
        upMenu.Update();
        updmain.Update();
        //upnlNoData.Update();
        //upnlIssueReceipt.Update();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        Save();
        hdnMode.Value = "1";
        upMenu.Update();
        updmain.Update();
        upnlNoData.Update();
        upnlIssueReceipt.Update();
        lnkListInsuranceCards_Click(sender, e);
    }

    protected void dlInsuranceCard_ItemCommand(object source, DataListCommandEventArgs e)
    {
        /// Retrieves the values selected from the DataList

        switch (e.CommandName)
        {
            case "ALTER": hdnInsuranceID.Value = e.CommandArgument.ToString();
                EditInsuranceCard(e.CommandArgument.ToInt32());
                EnableMenus();
                lnkDelete.OnClientClick = lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return false;";
                lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = false;
                break;

            case "VIEW": hdnInsuranceID.Value = e.CommandArgument.ToString();
                divViewSingleCards.Style["display"] = "block";

                DisplaySingleInsuranceCard(e.CommandArgument.ToInt32());

                break;
        }
        upMenu.Update();
        updmain.Update();
        upnlNoData.Update();
        upnlIssueReceipt.Update();
    }

    protected void ImgEdit_OnClick(object sender, EventArgs e)
    {
        EditInsuranceCard(hdnInsuranceID.Value.ToInt32());
        lnkDelete.OnClientClick = lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return false;";
        lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = false;
        updmain.Update();
        upMenu.Update();
    }

    #endregion Events

    #region Documents

    protected void lnkDocumentIR_Click(object sender, EventArgs e)
    {
        if (hdnMode.Value == "1")
        {
            DateTime ExpiryDate;
            DateTime.TryParseExact(txtExpiryDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ExpiryDate);
            
            int intOperationTypeID = (int)OperationType.Employee, intDocType;
            ucDocIssueReceipt.eOperationType = (OperationType)intOperationTypeID;
            ucDocIssueReceipt.DocumentID = hdnInsuranceID.Value.ToInt32();
            intDocType = (int)DocumentType.Insurance_Card;
            ucDocIssueReceipt.eDocumentType = (DocumentType)intDocType;
            ucDocIssueReceipt.DocumentNumber = txtCardNumber.Text.Trim();
            ucDocIssueReceipt.EmployeeID = ddlEmployeeName.SelectedValue.ToInt32();
            ucDocIssueReceipt.dtExpiryDate = ExpiryDate;
            ucDocIssueReceipt.Call();

            mpeIssueReceipt.Show();
            upnlIssueReceipt.Update();
            lnkListInsuranceCards_Click(sender, e);
        }
        else
            return;
    }

    protected void dlOtherDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        int intDocType, iOperationType = (int)OperationType.Employee;

        intDocType = (int)DocumentType.Insurance_Card;
        objclsInsuranceCard = new clsInsuranceCard();

        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":

                Label lblDocname, lblFilename;

                if (e.CommandArgument != string.Empty)
                {

                    lblFilename = (Label)e.Item.FindControl("lblFilename");

                    string strFilename = lblFilename.Text.Trim();
                    string strFolder = string.Empty;


                    clsDocumentMaster.SingleNodeDelete(e.CommandArgument.ToInt32());


                    strFolder = Convert.ToString((DocumentType)intDocType);

                    string strPath = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(strPath + "/" + Convert.ToString(strFilename)))
                        File.Delete(strPath + "/" + Convert.ToString(strFilename));

                }



                DataTable dtDocs = objclsInsuranceCard.GetAttachments(-1, -1, 0);
                DataRow drRow;
                foreach (DataListItem Item in dlOtherDoc.Items)
                {
                    if (e.Item.ItemIndex == Item.ItemIndex)
                        continue;

                    drRow = dtDocs.NewRow();

                    lblDocname = (Label)Item.FindControl("lblDocname");
                    lblFilename = (Label)Item.FindControl("lblFilename");

                    drRow["Node"] = dlOtherDoc.DataKeys[Item.ItemIndex];
                    drRow["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    drRow["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dtDocs.Rows.Add(drRow);
                }

                if (dtDocs.Rows.Count > 0)
                {
                    dlOtherDoc.DataSource = dtDocs;
                    dlOtherDoc.DataBind();

                    ViewState["OtherDoc"] = dtDocs;
                }
                else
                {

                    dlOtherDoc.DataSource = dtDocs;
                    dlOtherDoc.DataBind();
                    divOtherDoc.Style["display"] = "none";
                    ViewState["OtherDoc"] = dtDocs;
                }
                updOtherDoc.Update();
                updmain.Update();
                break;
        }
    }

    protected void fuDocument_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string strFilename = string.Empty;
        string strActualFilename = string.Empty;
        if (fuDocument.HasFile)
        {
            strActualFilename = fuDocument.FileName;
            strFilename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuDocument.FileName);
            Session["Filename"] = strFilename;
            fuDocument.SaveAs(Server.MapPath("~/Documents/temp/") + strFilename);

        }
    }

    protected void btnattachpassdoc_Click(object sender, EventArgs e)
    {
        divOtherDoc.Style["display"] = "block";

        DataTable dtDocs;
        DataRow drRow;

        if (ViewState["OtherDoc"] == null)
            dtDocs = objclsInsuranceCard.GetAttachments(-1, -1, 0);
        else
        {
            DataTable dtTemp = (DataTable)this.ViewState["OtherDoc"];
            dtDocs = dtTemp;
        }
        drRow = dtDocs.NewRow();

        drRow["Docname"] = Convert.ToString(txtDocname.Text.Trim());
        drRow["Filename"] = Session["Filename"];
        drRow["Actualfilename"] = Session["Filename"];
        dtDocs.Rows.Add(drRow);
        dlOtherDoc.DataSource = dtDocs;
        dlOtherDoc.DataBind();

        ViewState["OtherDoc"] = dtDocs;
        txtDocname.Text = string.Empty;
        updOtherDoc.Update();
        #region code for docname dup
        //if (dtDocs.Rows.Count > 0)
        //{
        //    int count = dtDocs.Rows.Count, flag = 0;
        //    for (int i = 0; i < count; i++)
        //    {

        //        if (dtDocs.Rows[i]["Docname"].ToString().Trim() == txtDocname.Text.Trim())
        //        {
        //            mcMessage.InformationalMessage("Document Name exists");
        //            mpeMessage.Show();
        //            flag = 1;

        //        }
        //    }
        //    if (flag != 1)
        //    {

        //        dtDocs.Rows.Add(drRow);
        //        dlOtherDoc.DataSource = dtDocs;
        //        dlOtherDoc.DataBind();

        //        ViewState["OtherDoc"] = dtDocs;
        //        txtDocname.Text = string.Empty;
        //        updOtherDoc.Update();

        //    }
        //    //}
        //}
        //else
        //{

        //    dtDocs.Rows.Add(drRow);
        //    dlOtherDoc.DataSource = dtDocs;
        //    dlOtherDoc.DataBind();
        //    ViewState["OtherDoc"] = dtDocs;
        //    txtDocname.Text = string.Empty;
        //    updOtherDoc.Update();
        //}
        //txtDocname.Text = string.Empty;
        //updOtherDoc.Update();
        #endregion code for docname dup
    }
    #endregion Documents

    #region Functions

    private void LoadInitials()
    {
        FillddlCardIssued();
        FillddlInsuranceCompany();
        FillddlEmployee();

        txtCardNumber.Text = "";
        txtPolicyNumber.Text = "";
        txtPolicyName.Text = "";
        txtRemarks.Text = "";
        txtIssuedate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        txtExpiryDate.Text = System.DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");// System.DateTime.Now.AddDays(1);
        updmain.Update();
    }

    public void FillddlCardIssued()
    {
        try
        {
            objclsInsuranceCard = new clsInsuranceCard();
            DataTable dtCardIssued = objclsInsuranceCard.GetInsuranceCardIssueReference();
            DataRow drRow = dtCardIssued.NewRow();
            drRow["IssuedID"] = "-1";
            drRow["IssuedBy"] = GetGlobalResourceObject("DocumentsCommon", "Select").ToString();
            dtCardIssued.Rows.InsertAt(drRow, 0);

            ddlCardIssued.DataSource = dtCardIssued;
            ddlCardIssued.DataTextField = "IssuedBy";
            ddlCardIssued.DataValueField = "IssuedID";
            ddlCardIssued.DataBind();
           
            ddlCardIssued.SelectedValue = CurrentSelectedValue;

            updCardIssued.Update();
            //updmain.Update();
        }
        finally
        { }
    }

    public void FillddlInsuranceCompany()
    {
        try
        {
            objclsInsuranceCard = new clsInsuranceCard();
            DataTable dtInsuranceCompany = objclsInsuranceCard.GetInsuranceCompanyReference();
            
            ddlInsuranceCompany.DataSource = dtInsuranceCompany;
            ddlInsuranceCompany.DataTextField = "Description";
            ddlInsuranceCompany.DataValueField = "InsuranceCompanyID";
            ddlInsuranceCompany.DataBind();
            ddlInsuranceCompany.Items.Insert(0, new ListItem(GetGlobalResourceObject("DocumentsCommon", "Select").ToString(), "-1"));
            if (ddlInsuranceCompany.Items.FindByValue(CurrentSelectedValue) != null)
                ddlInsuranceCompany.SelectedValue = CurrentSelectedValue;
            updInsuranceCompany.Update();
            //updmain.Update();
        }
        finally
        {

        }
    }

    private void FillddlEmployee()
    {
        objclsInsuranceCard = new clsInsuranceCard();
        objUser = new clsUserMaster();

        objclsInsuranceCard.CompanyID = objUser.GetCompanyId();
        DataTable dtEmployeeName = this.objclsInsuranceCard.GetEmployeeNames();
        ddlEmployeeName.DataSource = dtEmployeeName;
        ddlEmployeeName.DataTextField = "EmployeeName";
        ddlEmployeeName.DataValueField = "EmployeeID";
        ddlEmployeeName.DataBind();

        ddlEmployeeName.Items.Insert(0, new ListItem(GetGlobalResourceObject("DocumentsCommon", "Select").ToString(), "-1"));
        ddlEmployeeName.SelectedValue = CurrentSelectedValue;
        updEmployeeName.Update();
        updmain.Update();
    }

    private void BindDataList()
    {
        txtSearch.Text = string.Empty;
        clsUserMaster objUser = new clsUserMaster();
        objclsInsuranceCard = new clsInsuranceCard();
        objclsInsuranceCard.CompanyID = objUser.GetCompanyId();
        pgrInsuranceCards.Total = objclsInsuranceCard.GetCardCount();
        using (DataSet dtallcardsview = objclsInsuranceCard.GetAllInsuranceCardDetails())
        {
            if (dtallcardsview.Tables[0].Rows.Count > 0)
            {
                dlInsuranceCard.DataSource = dtallcardsview;
                dlInsuranceCard.DataBind();
                lblNoData.Style["display"] = "none";
                hdnInsuranceID.Value = "0";
                EnableMenus();
            }
            else
            {
                lblNoData.Style["display"] = "block";
                if (txtSearch.Text == string.Empty)
                    lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoDocuments").ToString();
                else
                    lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoSearchresultsfound").ToString();
                lnkPrint.OnClientClick = lnkEmail.OnClientClick = lnkDelete.OnClientClick = "return false;";
            }
        }
        upnlNoData.Update();
        updmain.Update();
    }

    /// <summary>
    /// to save
    /// </summary>
    private void Save()
    {
        DateTime IssuedDate, ExpiryDate, RenewDate;

        DateTime.TryParseExact(txtIssuedate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out IssuedDate);
        DateTime.TryParseExact(txtExpiryDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ExpiryDate);

        objAlert = new clsDocumentAlert();        
        objclsInsuranceCard = new clsInsuranceCard();
        objclsInsuranceCard.EmployeeID = ddlEmployeeName.SelectedValue.ToInt32();
        objclsInsuranceCard.CardNumber = txtCardNumber.Text.Trim();
        objclsInsuranceCard.InsuranceCompanyID = ddlInsuranceCompany.SelectedValue.ToInt32();
        objclsInsuranceCard.PolicyNumber = txtPolicyNumber.Text.Trim();
        objclsInsuranceCard.PolicyName = txtPolicyName.Text.Trim();
        if (ddlCardIssued.SelectedValue == "-1")
        {
            objclsInsuranceCard.IssueID = null;
        }
        else
        {
            objclsInsuranceCard.IssueID = ddlCardIssued.SelectedValue.ToInt32();
        }
        objclsInsuranceCard.IssueDate = IssuedDate;
        objclsInsuranceCard.ExpiryDate = ExpiryDate;

        objclsInsuranceCard.Remarks = txtRemarks.Text.Trim();
        objclsInsuranceCard.InsuranceCardID = hdnInsuranceID.Value.ToInt32();
        int iCardID = 0;

        //this fi
        if (this.objclsInsuranceCard.GetInsuranceCardByCNO())
        {
            mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "CardNumberexist").ToString());

        }
        else
        {
            if (this.objclsInsuranceCard.GetInsuranceCardByPNO())
            {
                mcMessage.InformationalMessage(GetLocalResourceObject("PolicyNumberExists.Text").ToString());

            }
            else
            {
                //for updation
                if (hdnInsuranceID.Value.ToInt32() > 0)
                {
                    hdnMode.Value = "1";

                    if (objclsInsuranceCard.UpdateInsuranceCard())
                    {
                        iCardID = objclsInsuranceCard.InsuranceCardID;
                        hdnInsuranceID.Value = objclsInsuranceCard.InsuranceCardID.ToString();
                        SaveToTreeMaster(iCardID);
                        mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "UpdatedSuccessfully").ToString());
                        AfterSave();
                    }
                    else
                    {
                        mcMessage.InformationalMessage("Failed"); ;
                    }
                }
                else
                {
                    //for insertion
                    hdnMode.Value = "0";
                    if (objclsInsuranceCard.CheckDuplication(hdnInsuranceID.Value.ToInt32(), txtCardNumber.Text.Trim()))
                    {
                        mcMessage.InformationalMessage( GetGlobalResourceObject("DocumentsCommon","CardNumberexist").ToString());

                    }
                    else
                    {
                        if (this.objclsInsuranceCard.SaveInsuranceCard())
                        {
                            iCardID = objclsInsuranceCard.InsuranceCardID;
                        
                            //code added by Jisha bijoy for alert settings 
                            objAlert.AlertMessage(Convert.ToInt32(DocumentType.Insurance_Card), DocumentType.Insurance_Card.ToString(), "Insurance Card Number", iCardID, txtCardNumber.Text, clsCommon.Convert2DateTime(txtExpiryDate.Text), "Emp", ddlEmployeeName.SelectedValue.ToInt32(), "", false);

                            SaveToTreeMaster(iCardID);
                            mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon","SavedSuccessfully").ToString());
                            hdnInsuranceID.Value = objclsInsuranceCard.InsuranceCardID.ToString();
                            AfterSave();
                        }
                        else
                        {
                            mcMessage.InformationalMessage("Failed"); ;
                        }
                    }

                }
            }
        }
        mpeMessage.Show();
        txtSearch.Text = string.Empty;
        upMenu.Update();
        updmain.Update();
        upnlIssueReceipt.Update();
        updOtherDoc.Update();
    }

    protected void lnkRenew_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdnInsuranceID.Value.ToInt32() > 0)
            {
                int id = 0;
                objAlert = new clsDocumentAlert();
                objclsInsuranceCard = new clsInsuranceCard();
                objclsInsuranceCard.InsuranceCardID = hdnInsuranceID.Value.ToInt32();
                try
                {
                    objclsInsuranceCard.BeginEmp();
                    id = objclsInsuranceCard.UpdateRenewEmployeeInsuranceCard();
                    objclsInsuranceCard.Commit();
                }
                catch (Exception ex)
                {
                    objclsInsuranceCard.RollBack();
                }

                objAlert.DeleteExpiryAlertMessage(Convert.ToInt32(DocumentType.Insurance_Card), id);

                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "RenewedSuccessfully").ToString());
                mpeMessage.Show();
                divIssueReceipt.Style["display"] = "block";
                divRenew.Style["display"] = "block";
                hdnInsuranceID.Value = objclsInsuranceCard.InsuranceCardID.ToStringCustom();
                EnableMenus();
                upMenu.Update();
                lnkListInsuranceCards_Click(sender, e);
            }
            else
                return;
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    private void SaveToTreeMaster(int iCardID)
    {
        int intDocType, iOperationType = (int)OperationType.Employee;
        intDocType = (int)DocumentType.Insurance_Card;
        Label lblDocname, lblActualfilename, lblFilename;

        /* Modified to Get Document Type */
        string strDoctypeName = clsDocumentMaster.GetDocumentType(intDocType);
        clsDocumentMaster.DeleteTreeMaster(iCardID, intDocType);

        foreach (DataListItem item in dlOtherDoc.Items)
        {

            lblDocname = (Label)item.FindControl("lblDocname");
            lblFilename = (Label)item.FindControl("lblFilename");
            lblActualfilename = (Label)item.FindControl("lblActualfilename");

            if (dlOtherDoc.DataKeys[item.ItemIndex] == DBNull.Value)
            {
                objclsInsuranceCard.SaveTreeMaster(iCardID, intDocType, ddlEmployeeName.SelectedValue.ToInt32(), ddlEmployeeName.SelectedItem.Text.Trim(), lblDocname.Text.Trim(), lblFilename.Text.Trim());

                string strPath = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";


                if (File.Exists(Server.MapPath("~/Documents/temp/" + lblFilename.Text.Trim().ToString())))
                {
                    if (!Directory.Exists(strPath))
                        Directory.CreateDirectory(strPath);

                    File.Move(Server.MapPath("~/Documents/temp/" + lblFilename.Text.Trim().ToString()), strPath + "/" + lblFilename.Text.Trim().ToString());
                }

            }
            else
            {
                objclsInsuranceCard.SaveTreeMaster(iCardID, intDocType, ddlEmployeeName.SelectedValue.ToInt32(), ddlEmployeeName.SelectedItem.Text.Trim(), lblDocname.Text.Trim(), lblFilename.Text.Trim());

                string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                {
                    if (!Directory.Exists(Path))
                        Directory.CreateDirectory(Path);

                    File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                }
            }
        }
    }
   
    private void AfterSave()
    {
        divAddEmployeeInsurance.Style["display"] = "block";
        divDatalistView.Style["display"] = "none";
        lblNoData.Style["display"] = "none";
        divPrint.Style["display"] = "none";
        lblNoData.Style["display"] = "none";
        divIssueReceipt.Style["display"] = "block";
        
        txtDocname.Text = string.Empty;
        upMenu.Update();
        updmain.Update();
        upnlIssueReceipt.Update();
        updOtherDoc.Update();
    }
    private void EditInsuranceCard(int CardID)
    {
        updmain.Update();
        int intDocType, iOperationType = (int)OperationType.Employee;

        intDocType = (int)DocumentType.Insurance_Card;

        objclsInsuranceCard = new clsInsuranceCard();

        hdnMode.Value = "1";

        divAddEmployeeInsurance.Style["display"] = "block";
        divDatalistView.Style["display"] = "none";
        divViewSingleCards.Style["display"] = "none";
        //divNoRecords.Style["display"] = "none";
        divPrint.Style["display"] = "none";


        FillddlCardIssued();
        FillddlInsuranceCompany();
        DataTable dteditcard = this.objclsInsuranceCard.GetInsuranceCardByID(hdnInsuranceID.Value.ToInt32(), intDocType);
        if (dteditcard != null && dteditcard.Rows.Count > 0)
        {
            objclsInsuranceCard.EmployeeID = dteditcard.Rows[0]["EmployeeID"].ToInt32();
            objclsInsuranceCard.InsuranceCardID = dteditcard.Rows[0]["InsuranceCardID"].ToInt32();
            FillddlEmployee();
            ddlEmployeeName.SelectedValue = dteditcard.Rows[0]["EmployeeID"].ToStringCustom();
            txtCardNumber.Text = dteditcard.Rows[0]["CardNumber"].ToStringCustom();

            if (ddlInsuranceCompany.Items.FindByValue(dteditcard.Rows[0]["InsuranceCompanyID"].ToStringCustom()) == null)
            {
                ddlInsuranceCompany.SelectedValue = CurrentSelectedValue;
            }
            else
            {
                ddlInsuranceCompany.SelectedValue = dteditcard.Rows[0]["InsuranceCompanyID"].ToStringCustom();
            }

            txtPolicyNumber.Text = dteditcard.Rows[0]["PolicyNumber"].ToStringCustom();
            txtPolicyName.Text = dteditcard.Rows[0]["PolicyName"].ToStringCustom();

            if (ddlCardIssued.Items.FindByValue(dteditcard.Rows[0]["IssuedID"].ToStringCustom()) == null)
            {
                ddlCardIssued.SelectedValue = CurrentSelectedValue;
            }
            else
            {
                ddlCardIssued.SelectedValue = dteditcard.Rows[0]["IssuedID"].ToStringCustom();
            }

            txtIssuedate.Text = dteditcard.Rows[0]["IssueDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtExpiryDate.Text = dteditcard.Rows[0]["ExpiryDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtRemarks.Text = dteditcard.Rows[0]["Remarks"].ToStringCustom();
            lblCurrentStatus.Text = Convert.ToString(dteditcard.Rows[0]["Status"]);
        }
        
        //if (clsDocumentMaster.IsReceipted(iOperationType, intDocType, hdnInsuranceID.Value.ToInt32()))
        //{
        //    imgDocIR.ImageUrl = "~/images/document_issue.png";
        //    lnkDocumentIR.Text = "<h5>Issue</h5>";
        //}
        //else
        //{
        //    imgDocIR.ImageUrl = "~/images/document_reciept.png";
        //    lnkDocumentIR.Text = "<h5>Receipt</h5>";
        //}
        DataTable datDocument = new DataTable();
        datDocument = objclsInsuranceCard.GetAttachments(hdnInsuranceID.Value.ToInt32(), ddlEmployeeName.SelectedValue.ToInt32(), intDocType);

        dlOtherDoc.DataSource = datDocument;
        dlOtherDoc.DataBind();

        ViewState["OtherDoc"] = datDocument;

        if (dlOtherDoc.Items.Count > 0)
            divOtherDoc.Style["display"] = "block";
        else
            divOtherDoc.Style["display"] = "none";
        upMenu.Update();
        updmain.Update();
        upnlIssueReceipt.Update();
        updOtherDoc.Update();

        if (Convert.ToInt32(dteditcard.Rows[0]["StatusID"]) == 1) // Receipt
            divIssueReceipt.Style["display"] = "none";
        else
            divIssueReceipt.Style["display"] = "block";

        if (Convert.ToBoolean(dteditcard.Rows[0]["IsRenewed"]) == true) // Renewed
        {
            divRenew.Style["display"] = "none";

            if (clsGlobalization.IsArabicCulture())
                lblCurrentStatus.Text += " ] " + GetGlobalResourceObject("DocumentsCommon", "renewed") + " ] ";
            else
                lblCurrentStatus.Text += " [ " + GetGlobalResourceObject("DocumentsCommon", "renewed") + " ] ";

            ControlEnableDisble(false);
        }
        else
        {
            divRenew.Style["display"] = "block";
            ControlEnableDisble(true);
        }

        upMenu.Update();
    }

    private void ControlEnableDisble(bool blnTemp)
    {
        ddlInsuranceCompany.Enabled = ddlEmployeeName.Enabled = ddlInsuranceCompany.Enabled = ddlCardIssued.Enabled = blnTemp;
        txtCardNumber.Enabled = txtPolicyNumber.Enabled = txtPolicyName.Enabled = blnTemp;
        txtIssuedate.Enabled = txtExpiryDate.Enabled = txtRemarks.Enabled = blnTemp;
    }

    private void DisplaySingleInsuranceCard(int CardID)
    {
        upMenu.Update();

        hdnMode.Value = "1";

        pgrInsuranceCards.Visible = false;
        dlInsuranceCard.DataSource = null;
        dlInsuranceCard.DataBind();

        divPrint.InnerHtml = this.objclsInsuranceCard.GetPrintText(hdnInsuranceID.Value.ToInt32());
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if(lnkPrint.Enabled )
        lnkPrint.OnClientClick = "return true;";//sPrinttbl;
        if (lnkEmail.Enabled)
        lnkEmail.OnClientClick = "return showMailDialog('Type=InsuranceCard&InsuranceCardIDs=" + hdnInsuranceID.Value.ToString() + "');";
        if (lnkDelete.Enabled)
        lnkDelete.OnClientClick = "return confirm('" +GetGlobalResourceObject("DocumentsCommon","Areyousuretodeletetheselecteditems").ToString() + "');" ;

        DataTable dtview = this.objclsInsuranceCard.GetInsuranceCardByID(CardID, 0);
        if (dtview != null && dtview.Rows.Count > 0)
        {
            divEmpName.InnerText = dtview.Rows[0]["EmployeeFullName"].ToStringCustom();
            divCardNumber.InnerText = dtview.Rows[0]["CardNumber"].ToStringCustom();
            divPolicyNumber.InnerText = dtview.Rows[0]["PolicyNumber"].ToStringCustom();
            divPolicyName.InnerText = dtview.Rows[0]["PolicyName"].ToStringCustom();
            divInsuranceCompany.InnerText = dtview.Rows[0]["Description"].ToStringCustom();
            divIssuedBy.InnerText = dtview.Rows[0]["IssuedBy"].ToStringCustom();
            divIssueDate.InnerText = dtview.Rows[0]["IssueDate"].ToDateTime().ToString("dd MMM yyyy");
            divExpiryDate.InnerText = dtview.Rows[0]["ExpiryDate"].ToDateTime().ToString("dd MMM yyyy");
            divRemarks.InnerText = dtview.Rows[0]["Remarks"].ToStringCustom();
            divIsDelete.InnerText = dtview.Rows[0]["IsDelete"].ToStringCustom();

            string renew = dtview.Rows[0]["IsRenewed"].ToString();
            if (renew.ToBoolean())
            {
                renew = clsGlobalization.IsArabicCulture() ? "نعم" : "Yes";
            }
            else
            {
                renew = clsGlobalization.IsArabicCulture() ? "لا" : "No"; ;
            }
            divRenew1.InnerText = renew;
            
        }

        divAddEmployeeInsurance.Style["display"] = "none";
        divDatalistView.Style["display"] = "none";
        divViewSingleCards.Style["display"] = "block";
        //divNoRecords.Style["display"] = "none";
        divPrint.Style["display"] = "none";
        updmain.Update();
        upMenu.Update();
    }

    protected void BindPagerNumber()
    {
        objUser = new clsUserMaster();
        objclsInsuranceCard = new clsInsuranceCard();
        objclsInsuranceCard.PageIndex = pgrInsuranceCards.CurrentPage + 1;
        objclsInsuranceCard.PageSize = pgrInsuranceCards.PageSize;
        objclsInsuranceCard.SearchKey = txtSearch.Text;
        objclsInsuranceCard.CompanyID = objUser.GetCompanyId();
        pgrInsuranceCards.Total = objclsInsuranceCard.GetCardCount();

        DataSet ds = objclsInsuranceCard.GetAllInsuranceCardDetails();
        if (ds.Tables[0].Rows.Count > 0)
        {
            dlInsuranceCard.DataSource = ds;
            dlInsuranceCard.DataBind();
            lblNoData.Style["display"] = "none";
            hdnInsuranceID.Value = "0";

            EnableMenus();
            pgrInsuranceCards.Visible = true;

        }
        else
        {
            dlInsuranceCard.DataSource = null;
            dlInsuranceCard.DataBind();
            pgrInsuranceCards.Visible = false;
            lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoSearchresultsfound").ToString();
            lblNoData.Style["display"] = "block";
            lnkPrint.OnClientClick = lnkEmail.OnClientClick = lnkDelete.OnClientClick = "return false;";
        }
        divAddEmployeeInsurance.Style["display"] = "none";
        divDatalistView.Style["display"] = "block";
        divViewSingleCards.Style["display"] = "none";
        divPrint.Style["display"] = "none";
        upnlNoData.Update();
        updmain.Update();
        upMenu.Update();

    }

    private void ClearControls()
    {
        hdnInsuranceID.Value = "0";
        hdnMode.Value = "0";

        txtCardNumber.Text = "";
        txtPolicyNumber.Text = "";
        txtPolicyName.Text = "";
        txtRemarks.Text = "";
        txtIssuedate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        txtExpiryDate.Text = System.DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");

        dlInsuranceCard.DataSource = null;
        dlInsuranceCard.DataBind();

        upMenu.Update();
        upnlNoData.Update();

        //EnableMenus();
        pgrInsuranceCards.CurrentPage = 0;
        pgrInsuranceCards.Visible = false;
        txtSearch.Text = string.Empty;
  
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        lblCurrentStatus.Text =  GetGlobalResourceObject("DocumentsCommon","New").ToString();
        ViewState["OtherDoc"] = null;
        dlOtherDoc.DataSource = null;
        dlOtherDoc.DataBind();

        upMenu.Update();
        updmain.Update();
        upnlIssueReceipt.Update();
        updOtherDoc.Update();
    }

    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        clsUserMaster objUser = new clsUserMaster();
        clsRoleSettings objRoleSettings = new clsRoleSettings();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.InsuranceCard);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;

        if (dt.Rows.Count > 0)
        {
            lnkRenew.Enabled = lnkAddInsuranceCard.Enabled = dt.Rows[0]["IsCreate"].ToBoolean();
            lnkListInsuranceCards.Enabled = dt.Rows[0]["IsView"].ToBoolean();
            lnkEmail.Enabled = lnkPrint.Enabled = dt.Rows[0]["IsPrintEmail"].ToBoolean();
            lnkDelete.Enabled = dt.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dt.Rows[0]["IsUpdate"].ToBoolean();
        }

        if (RoleId > 3)
        {
            if (lnkListInsuranceCards.Enabled)
            {
                BindDataList();
                BindPagerNumber();
            }
            else if (lnkAddInsuranceCard.Enabled)
                lnkAddInsuranceCard_Click(null, null);
            else
            {
                pgrInsuranceCards.Visible = false;
                dlInsuranceCard.DataSource = null;
                dlInsuranceCard.DataBind();
                btnSearch.Enabled = false;
                lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString();
                lblNoData.Style["display"] = "block";
            }
        }
        else
        {
            BindDataList();
            BindPagerNumber();
        }
    }

    public void EnableMenus()
    {
        DataTable dt = (DataTable)ViewState["Permission"];

        if (dt.Rows.Count > 0)
        {
            lnkRenew.Enabled = lnkAddInsuranceCard.Enabled = dt.Rows[0]["IsCreate"].ToBoolean();
            lnkListInsuranceCards.Enabled = dt.Rows[0]["IsView"].ToBoolean();
            lnkEmail.Enabled = lnkPrint.Enabled = dt.Rows[0]["IsPrintEmail"].ToBoolean();
            lnkDelete.Enabled = dt.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dt.Rows[0]["IsUpdate"].ToBoolean();
        }

        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlInsuranceCard.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";

        if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlInsuranceCard.ClientID + "');";
        else
            lnkPrint.OnClientClick = "return false;";

        if (lnkEmail.Enabled)
            lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlInsuranceCard.ClientID + "');";
        else
            lnkEmail.OnClientClick = "return false;";

        if (lnkRenew.Enabled)
            lnkRenew.OnClientClick = "return valRenewDatalist('" + dlInsuranceCard.ClientID + "');";
        else
            lnkRenew.OnClientClick = "return false;";
    }

    #endregion Functions

    #region ModalPopUp
    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);

    }
    void ReferenceControlNew1_onRefresh()
    {
        mdlPopUpReference.Show();
    }
    void ucDocIssueReceipt_OnSave(string Result)
    {
        if (Result != null)
        {
            lnkDocumentIR.Text = "<h5>" + Result + "</h5>";
            if (Result == "Issue")
            {
                imgDocIR.ImageUrl = "~/images/document_issue.png";
                lblCurrentStatus.Text = "Receipted";
            }
            else
            {
                imgDocIR.ImageUrl = "~/images/document_reciept.png";
                lblCurrentStatus.Text = "Issued";
            }


            upMenu.Update();
            updmain.Update();
        }
    }
    #endregion ModalPopUp


    protected void dlInsuranceCard_ItemDataBound(object sender, DataListItemEventArgs e)
    {
         ImageButton btnEdit=(ImageButton)e.Item.FindControl("btnEdit");
         if (btnEdit != null)
         {
             btnEdit.Enabled = MblnUpdatePermission;
         }

    }
}
