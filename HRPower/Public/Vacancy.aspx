﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Vacancy.aspx.cs" Inherits="Public_Vacancy" Title="Job Openings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li class='selected'><a href='Vacancy.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
            <li><a href="VisaTicketProcessing.aspx">
                <asp:Literal ID="Literal82" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></a></li>
            <li><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="updviewAll" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divViewAll" runat="server">
                <asp:DataList ID="dlAllJobs" runat="server" OnItemCommand="dlAllJobs_ItemCommand"
                    OnItemDataBound="dlAllJobs_ItemDataBound" Width="100%" DataKeyField="JobId" CssClass="labeltext">
                    <ItemStyle CssClass="item" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkSelectAll" runat="server" meta:resourcekey="SelectAll" onclick="selectAll(this.id, 'chkSelect');" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table cellpadding="0" cellspacing="0" style="width: 100%" id="tblUsers" runat="server">
                            <tr>
                                <td valign="top" width="30" style="padding-top: 4px">
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </td>
                                <td valign="top">
                                    <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table style="width: 100%; margin-top: 0px" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="btnJob" CausesValidation="false" runat="server" CommandArgument='<%# Eval("JobId")%>'
                                                                CommandName="VIEW" Text='<%# Eval("Title")%>' CssClass="listHeader bold"></asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td valign="top" align="right">
                                                            <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="false" ImageUrl="~/images/edit.png"
                                                                CommandArgument='<%# Eval("JobId")%>' CommandName="ALTER" ToolTip='<%$Resources:ControlsCommon,Edit%>' />
                                                            |
                                                            <asp:ImageButton ID="lnkClose" runat="server" CausesValidation="false" ImageUrl="~/images/Closed.png"
                                                                CommandArgument='<%# Eval("JobId")%>' CommandName="CLOSE" meta:resourcekey="ClicktoCloseJob" />
                                                            <AjaxControlToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                                                                TargetControlID="lnkClose" meta:resourcekey="Doyouwishtoclosethejob">
                                                            </AjaxControlToolkit:ConfirmButtonExtender>
                                                            <asp:HiddenField ID="hfJobID" runat="server" Value='<%# Eval("JobId")%>' />
                                                        </td>
                                                    </tr>
                                                    <tr style="padding-left: 20px; margin-top: 50px;">
                                                        <td valign="top" width="280px" style="padding-left: 20px;">
                                                            <table cellpadding="2" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="item_title" width="35%" align="left" style="color: Black;">
                                                                        <asp:Literal ID="Literal70" runat="server" meta:resourcekey="Company"></asp:Literal>
                                                                    </td>
                                                                    <td width="3%" style="color: Black;">
                                                                        :
                                                                    </td>
                                                                    <td class="trright" align="left" style="color: Black;">
                                                                        <%#Eval("CompanyName")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" align="right" style="width: 30%;" colspan="4">
                                                            <asp:Label ID="lblStatus" CssClass="statusspan" runat="server"></asp:Label>
                                                            <asp:HiddenField ID="hfclosed" runat="server" Value='<%#Convert.ToString(Eval("Status")) == "Closed" ? "Closed":"No"%>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="280px" style="padding-left: 20px">
                                                            <table cellpadding="2" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="item_title" width="35%" align="left" style="color: Black;">
                                                                        <asp:Literal ID="Literal71" runat="server" meta:resourcekey="Department"></asp:Literal>
                                                                    </td>
                                                                    <td width="3%" style="color: Black;">
                                                                        :
                                                                    </td>
                                                                    <td align="left" style="color: Black;">
                                                                        <%#Eval("Department")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="280px" style="padding-left: 20px">
                                                            <table cellpadding="2" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="item_title" width="35%" align="left" style="color: Black;">
                                                                        <asp:Literal ID="Literal72" runat="server" meta:resourcekey="Designation"></asp:Literal>
                                                                    </td>
                                                                    <td width="3%" style="color: Black;">
                                                                        :
                                                                    </td>
                                                                    <td align="left" style="color: Black;">
                                                                        <%#Eval("Designation")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="280px" style="padding-left: 20px">
                                                            <table cellpadding="2" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="item_title" width="35%" align="left" style="color: Black;">
                                                                        <asp:Literal ID="Literal73" runat="server" meta:resourcekey="EmploymentType"></asp:Literal>
                                                                    </td>
                                                                    <td width="3%" style="color: Black;">
                                                                        :
                                                                    </td>
                                                                    <td align="left" style="color: Black;">
                                                                        <%#Eval("EmploymentType")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="280px" style="padding-left: 20px">
                                                            <table cellpadding="2" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="item_title" width="35%" align="left" style="color: Black;">
                                                                        <asp:Literal ID="Literal74" runat="server" meta:resourcekey="InterviewType"></asp:Literal>
                                                                    </td>
                                                                    <td width="3%" style="color: Black;">
                                                                        :
                                                                    </td>
                                                                    <td align="left" style="color: Black;">
                                                                        <%--<%#Convert.ToBoolean(Eval("IsBoardInterview")) == true ? "Board" : "Multi-Level"%>--%>
                                                                        <%#Eval("IsBoardInterview")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="280px" style="padding-left: 20px">
                                                            <table cellpadding="2" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="item_title" width="35%" align="left" style="color: Black;">
                                                                        <asp:Literal ID="Literal75" runat="server" meta:resourcekey="RequisitionedBy"></asp:Literal>
                                                                    </td>
                                                                    <td width="3%" style="color: Black;">
                                                                        :
                                                                    </td>
                                                                    <td align="left" style="color: Black;">
                                                                        <%#Eval("EmployeeFullName")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="280px" style="padding-left: 20px">
                                                            <table cellpadding="2" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="item_title" width="35%" align="left" style="color: Black;">
                                                                        <asp:Literal ID="Literal79" runat="server" meta:resourcekey="DateofRequisition"></asp:Literal>
                                                                    </td>
                                                                    <td width="3%" style="color: Black;">
                                                                        :
                                                                    </td>
                                                                    <td align="left" style="color: Black;">
                                                                        <%#Eval("CreatedDate")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="280px" style="padding-left: 20px">
                                                            <table cellpadding="2" border="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td class="item_title" width="35%" align="left" style="color: Black;">
                                                                        <asp:Literal ID="Literal76" runat="server" meta:resourcekey="LastDate"></asp:Literal>
                                                                    </td>
                                                                    <td width="3%" style="color: Black;">
                                                                        :
                                                                    </td>
                                                                    <td align="left" style="color: Black;">
                                                                        <%#Eval("ExpiryDate")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="pgrJobs" runat="server" />
                <div runat="server" class="error" id="divNoData">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="always">
        <ContentTemplate>
            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 5px">
                    <tr>
                        <td style="width: 65%">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="WmCss" MaxLength="50"></asp:TextBox>
                        </td>
                        <td style="width: 35%; text-align: left; padding-left: 8px">
                            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" SkinID="GoButton"
                                ImageUrl="~/images/search.png" meta:resourcekey="Clickheretosearch" ImageAlign="AbsMiddle"
                                OnClick="btnSearch_Click" />
                            <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtSearch"
                                meta:resourcekey="SearchBy1" WatermarkCssClass="WmCss">
                            </AjaxControlToolkit:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgNewJob" ImageUrl="~/images/Active_Vacancies.png" CausesValidation="False"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkNewJob" runat="server" CausesValidation="False" PostBackUrl="~/Public/VacancyAddEdit.aspx"
                            meta:resourcekey="AddJob">    <%--  OnClick="lnkNewJob_Click"--%>                   
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListJob" CausesValidation="false" ImageUrl="~/images/Vacancy_ListBig.PNG"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkViewjob" runat="server" CausesValidation="False" PostBackUrl="~/Public/Vacancy.aspx"
                            meta:resourcekey="ViewJob">     <%-- OnClick="lnkViewjob_Click"        --%>          
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddJob" ImageUrl="~/images/delete.png" CausesValidation="False"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkDeleteJob" runat="server" Enabled="true" CausesValidation="False" OnClick="lnkDeleteJob_Click"
                            meta:resourcekey="Delete"> 
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgCriteria" CausesValidation="false" ImageUrl="~/images/add_criteria.png"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddCriteria" runat="server" CausesValidation="False" OnClick="lnkAddCriteria_Click"
                            meta:resourcekey="AddCriteria"> 
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddParticular0" CausesValidation="false" ImageUrl="~/images/add_addition_deduction.png"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddparticular" runat="server" CausesValidation="False" OnClick="lnkAddparticular_Click"
                            meta:resourcekey="AddParticulars"> 
                        </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
     <div style="height: auto">
        <div>
            <div style="display: none">
                <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
    </div>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:CriteriaReference ID="CriteriaReferenceControl" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlAdditionDeduction" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="FormView1" runat="server" DefaultMode="Edit">
                <InsertItemTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button3" runat="server" />
                    </div>
                    <asp:Panel ID="pnlAdditionDeduction" runat="server" Style="display: none">
                        <uc:AdditionDeduction ID="ucAdditionDeduction" runat="server" ModalPopupID="mpeAdditionDeduction" />
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeAdditionDeduction" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlAdditionDeduction" TargetControlID="Button3">
                    </AjaxControlToolkit:ModalPopupExtender>
                </InsertItemTemplate>
            </asp:FormView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
