﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Net;

public partial class Public_ViewImage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string RequestType = Request.QueryString["RequestType"].ToString().Trim();
        string FileName = Request.QueryString["FileName"].ToString().Trim();
        string FileLocation = "../Documents/"+RequestType +"/"+FileName;

        if (IsValidImage(Request.QueryString["FileName"].ToString()))
            imgImage.ImageUrl = FileLocation;
        else
        {

            string PhysicalLocation = Server.MapPath("../") + "Documents\\" + RequestType + "\\" + FileName;
            FileInfo file = new FileInfo(PhysicalLocation);
            if (file.Exists)
            {


                if (file.Extension == ".pdf")
                {

                    Response.ContentType = "application/pdf";

                    WebClient client = new WebClient();
                    Byte[] buffer = client.DownloadData(PhysicalLocation);

                    if (buffer != null)
                    {

                        Response.ContentType = "application/pdf";
                        //else if (file.Extension == ".doc" || file.Extension == ".docx")
                        //    Response.ContentType = "application/ms-word";
                        //else if (file.Extension == ".xls" || file.Extension == ".xlsx")
                        //    Response.ContentType = "application/vnd.ms-excel";
                        //else if (file.Extension == ".txt")
                        //    Response.ContentType = "application/text";



                        Response.AddHeader("content-length", buffer.Length.ToString());
                        Response.BinaryWrite(buffer);

                    }
                }

                else
                {





                    FileStream s = File.Open(PhysicalLocation, FileMode.Open);

                    int i = Convert.ToInt32(s.Length);

                    byte[] b = new byte[i];

                    s.Read(b, 0, i);

                    s.Close();


                    Response.Clear();


                    if (file.Extension == ".doc" || file.Extension == ".docx")
                        Response.ContentType = "application/word";
                    else if (file.Extension == ".xls" || file.Extension == ".xlsx")
                        Response.ContentType = "application/vnd.ms-excel";
                    else if (file.Extension == ".txt")
                        Response.ContentType = "application/text";
                    Response.AddHeader("content-disposition", "filename=" + FileName.Remove(0, FileName.LastIndexOf("\\") + 1));

                    Response.OutputStream.Write(b, 0, b.Length);
                    Response.OutputStream.Flush();
                    Response.OutputStream.Close();

                    Response.Flush();
                    Response.Close();
                }

            }


        }

    }



    private bool IsValidImage(string FileName)
    {
        try
        {
            string ext = FileName.Remove(0, FileName.LastIndexOf(".")).Trim();
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif" || ext.ToLower() == ".bmp" || ext.ToLower() == ".png" || ext.ToLower() == ".tif")
                return true;
            else
                return false;
        }
        catch (Exception) { return false; }
    }







}
