﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="PerformanceInitiation.aspx.cs" Inherits="Public_PerformanceInitiation"
    Title="Performance Initiation" %>

<%@ Register src="../Controls/AssignEvaluators.ascx" tagname="AssignEvaluators" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
<style type="text/css">
    select
    {
    	margin-left:0px;
    }
</style>

    <div id='cssmenu'>
        <ul>
            <li><a href="SalesPerformance.aspx">
                <%--Target Performance--%>
                <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:MasterPageCommon,TargetPerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="AttendancePerformance.aspx">
                <%--Attendance Performance--%>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,AttendancePerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="PerformanceTemplate.aspx"><span>
                <%--  Performance Template --%>
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceTemplate %>'>
                </asp:Literal>
            </span></a></li>
            <li class='selected'><a href="PerformanceInitiation.aspx"><span>
                <%--        Performance Initiation--%>
                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceInitiation %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="PerformanceEvaluation.aspx"><span>
                <%-- Performace Evaluation--%>
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="PerformanceAction.aspx">
                <%--        
            Performance Action--%>
                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceAction %>'>
                </asp:Literal>
            </a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">

    <script type ="text/javascript">
    
    function Save(IsSaveConfirmation)
    {
    debugger;
   
        var IsValid = true;
        var Message ="";
        
        Page_ClientValidate();
        if(Page_IsValid)
        {
            var FromDate = document.getElementById('<%=txtFromDate.ClientID%>');
            var ToDate = document.getElementById('<%=txtToDate.ClientID%>');
            var hfIsArabic = document.getElementById('<%=hfIsArabic.ClientID%>').value;
        
            if(FromDate && ToDate)
            {
                FromDate = FromDate.value;
                ToDate = ToDate.value;
                
                if(FromDate.length < 11 || FromDate.length > 11 || FromDate.split('-')[2] <1785 )
                {   
                    if(hfIsArabic == "True")
                        Message = 'غير صالحة من تنسيق التاريخ'
                    else
                        Message ='Invalid from date format';
                    IsValid = false;
                }
                  
                else if(ToDate.length < 11 || ToDate.length > 11  || ToDate.split('-')[2] <1785 )
                {
                    if(hfIsArabic == "True")
                        Message ='غير صالحة لتنسيق التاريخ'
                    else
                        Message ='Invalid to date format'
                    IsValid = false;
                }
                  
                if(IsValid)
                { 
                    Fromdate = new Date(FromDate.value.split('-')[1]+','+FromDate.value.split('-')[0]+','+FromDate.value.split('-')[2]); 
                    ToDate = new Date(ToDate.value.split('-')[1]+','+ToDate.value.split('-')[0]+','+ToDate.value.split('-')[2]); 

                    if(isNaN(Fromdate))
                    {
                        if(hfIsArabic == "True")
                            Message ='غير صالحة من تاريخ' 
                        else  
                            Message ='Invalid from date' 
                        IsValid = false;
                    }
                    else if(isNaN(ToDate))
                    {
                        if(hfIsArabic == "True")
                            Message ='غير صالحة حتى الآن'
                        else
                            Message ='Invalid to date'
                        IsValid = false;
                    }
                }
             }
             else IsValid  = false;
             
             
            
         }
         else IsValid  = false;
        
         if(IsValid)
         {  
            if(IsSaveConfirmation = true)
            {
                if(hfIsArabic == "True")
                    return confirm('هل أنت متأكد أنك تريد حفظ الشروع الأداء؟');
                else
                    return confirm('Are you sure you want to save performance initiation ?');
            }
             else
                return true;
         }
         else
         {  
            if(Message != '')
                alert(Message);
            return false;
          }
       
    }
    </script> 
    
    <asp:HiddenField ID="hfIsArabic" runat ="server" />
    <asp:UpdatePanel ID="updEmployees" runat ="server" UpdateMode ="Conditional"  >
    <ContentTemplate>
    
     <div id="dvRecord" runat ="server"  style="width: 100%">
        <div class="firstTrLeft" style="float: left; width: 20%; height: 29px">
          <%--  Company--%>
          
          
         
          
          
          <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:ControlsCommon,Company%>' >
          </asp:Literal>
          
          
          
          
          
          
            <asp:HiddenField ID="hfResult" runat ="server" />
              <asp:HiddenField ID="hfPerformanceInitiationID" runat ="server" />
        </div>
        <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
            <asp:DropDownList ID="ddlCompany" Width="200px" AutoPostBack ="true"  runat="server" CssClass="textbox_mandatory"
                MaxLength="25" onselectedindexchanged="Clear">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="ddlCompany"
                CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectACompany" SetFocusOnError="False"
                ValidationGroup ="SaveInitiation"></asp:RequiredFieldValidator>
        </div>
        <div class="firstTrLeft" style="float: left; width: 20%; height: 29px">
          <%--  Department--%>
          
                   
          <asp:Literal ID="Literal2" runat ="server" Text ='<%$Resources:ControlsCommon,Department%>' >
          </asp:Literal>
        </div>
        <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
            <asp:DropDownList AutoPostBack ="true"  ID="ddlDepartment" Width="200px" runat="server" CssClass="textbox_mandatory"
                MaxLength="25" onselectedindexchanged="Clear">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvDepartment" runat="server" ControlToValidate="ddlDepartment"
                CssClass="error" Display="Dynamic" InitialValue="-1" meta:resourcekey="PleaseSelectADepartment"
                SetFocusOnError="False"  ValidationGroup ="SaveInitiation"></asp:RequiredFieldValidator>
        </div>
      
        <div class="firstTrLeft" style="float: left; width: 20%; height: 29px">
          <%--  From Date--%>
          
                   
          <asp:Literal ID="Literal3" runat ="server" Text ='<%$Resources:ControlsCommon,FromDate%>' >
          </asp:Literal>
        </div>
        <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
            <asp:TextBox ID="txtFromDate" Width="100px" runat="server" 
                AutoPostBack ="true"  CssClass="textbox_mandatory"
                MaxLength="25" ontextchanged="Clear"></asp:TextBox>
            <AjaxControlToolkit:CalendarExtender runat="server" ID="ceFromDate" TargetControlID="txtFromDate" PopupButtonID ="imgFromDate"
                Format="dd-MMM-yyyy">
            </AjaxControlToolkit:CalendarExtender>
            
            <asp:Image ID="imgFromDate" style="margin-left :3px" runat ="server" ImageUrl ="~/images/Calendar_scheduleHS.png"/>
            
            <asp:RequiredFieldValidator ID="rfFromDate"     runat ="server" meta:resourcekey="PleaseEnterFromDate" ValidationGroup ="SaveInitiation" ControlToValidate ="txtFromDate">
            </asp:RequiredFieldValidator>
        </div>
        <div class="firstTrLeft" style="float: left; width: 20%; height: 29px">
          <%--  To Date--%>
          
                   
          <asp:Literal ID="Literal4" runat ="server" Text ='<%$Resources:ControlsCommon,ToDate%>' >
          </asp:Literal>
        </div>
        <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
            <asp:TextBox ID="txtToDate" Width="100px" runat="server" CssClass="textbox_mandatory" AutoPostBack ="true"  
                MaxLength="25" ontextchanged="Clear"></asp:TextBox>
            <AjaxControlToolkit:CalendarExtender runat="server" ID="ceToDate" TargetControlID="txtToDate" PopupButtonID ="imgToDate"
                Format="dd-MMM-yyyy">
            </AjaxControlToolkit:CalendarExtender>
            
            
            <asp:Image ID="imgToDate"  style="margin-left :3px" runat ="server" ImageUrl ="~/images/Calendar_scheduleHS.png"/>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat ="server"  ValidationGroup ="SaveInitiation" ControlToValidate ="txtToDate" meta:resourcekey="PleaseEnterToDate">
            </asp:RequiredFieldValidator>
        </div>
        <div>
            <div style="float: left; width: 80%">
               
            </div>
            <div style="float: right; width: 15%;height:50px" >
            <%--   <asp:Button ID="btnAddEmployee" runat="server" width="90px" Text="Add Employee" 
                  CssClass="btnsubmit" onclick="btnAddEmployee_Click1"  />--%>
                  
                  
                  <asp:ImageButton EnableViewState="true"  ID="imgAddEmployee" 
                    
                    
                    meta:resourcekey="AddEmployeeForPerformanceInitiation"
                    
                    
                     runat ="server" 
                    ImageUrl ="~/images/EmployeeBenefitsAdd_BIG.png" 
                    onclick="imgAddEmployee_Click"   ValidationGroup ="SaveInitiation"  OnClientClick ="return Save(false);"  />
                  <div style ="clear:both"></div>
                 <asp:Label ID="lblAdd" runat ="server" CssClass ="labeltext" meta:resourcekey="AddEmployeeForPerformanceInitiation"></asp:Label>
            </div>
        </div>
         <div style="margin-top: 5px; min-width: 100%; max-height: 450px; overflow: scroll">
             <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                 <ContentTemplate>
                     <asp:DataList SeparatorStyle-BackColor="AliceBlue" ID="dlEmployeeList" Width="100%"
                         runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px"
                         RepeatLayout="Table" CellPadding="3" GridLines="Both">
                         <FooterStyle BackColor="White" ForeColor="#000066" />
                         <ItemStyle ForeColor="#000066" />
                         <SeparatorStyle BackColor="AliceBlue" />
                         <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                         <HeaderTemplate>
                             <table>
                                 <tr>
                                     <td style="width: 60px; vertical-align: top">
                                         <asp:Label ID="lblSlNoHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                          Text='<%$Resources:ControlsCommon,SLNo %>' ></asp:Label>
                                     </td>
                                     <td style="width: 200px; vertical-align: top">
                                         <asp:Label ID="lblEmployeeHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                         Text='<%$Resources:ControlsCommon,Employee%>' 
                                           
                                           
                                           
                                           ></asp:Label>
                                             
                                             
                                          
                                     </td>
                                     <td style="width: 100px; vertical-align: top">
                                         <asp:Label ID="lblTemplateHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                            meta:resourcekey="Template"></asp:Label>
                                     </td>
                                     <td>
                                         <asp:Label ID="lblEvaluatorHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                             meta:resourcekey="Evaluators"></asp:Label>
                                     </td>
                                 </tr>
                             </table>
                         </HeaderTemplate>
                         <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                         <ItemTemplate>
                             <table>
                                 <tr>
                                     <td style="width: 60px; vertical-align: top">
                                         <asp:Label ID="lblSlNo" CssClass="labeltext" runat="server" ToolTip='<%# Eval("SLNo") %>'
                                             Text='<%# Eval("SLNo") %>'></asp:Label>
                                     </td>
                                     <td style="width: 200px; vertical-align: top">
                                         <asp:Label ID="lblEmployee" CssClass="labeltext" runat="server" ToolTip='<%# Eval("EmployeeFullName") %>'
                                             Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                         <asp:HiddenField ID="hfEmployeeID" runat="server" Value='<%# Eval("EmployeeID") %>'>
                                         </asp:HiddenField>
                                     </td>
                                     <td style="width: 100px; vertical-align: top; overflow: hidden">
                                         <asp:Label ID="lblTemplate" CssClass="labeltext" runat="server" ToolTip='<%# Eval("TemplateName") %>'
                                             Text='<%# Eval("TemplateName") %>'></asp:Label>
                                         <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("EmployeeID") %>'>
                                         </asp:HiddenField>
                                     </td>
                                     <td style="width: 331px">
                                         <asp:Label ID="lblEvaluator" CssClass="labeltext" runat="server" ToolTip='<%# Eval("EvaluatorFullName") %>'
                                             Text='<%# Eval("EvaluatorFullName") %>'></asp:Label>
                                     </td>
                                     <td style="width: 18px">
                                         <asp:ImageButton ID="imgDelete" ToolTip='<%$Resources:ControlsCommon,Delete%>' runat="server" ImageUrl="~/images/delete_popup.png"
                                             CommandArgument='<%# Eval("EmployeeID") %>' OnClick="imgDelete_Click" />
                                     </td>
                                 </tr>
                             </table>
                         </ItemTemplate>
                     </asp:DataList>
                     <div id="dvNoRecord" runat="server" style="text-align: center">
                         <asp:Label ID="lblNoRecord" CssClass="error "  runat="server" meta:resourcekey="NoEmployeesAdded"></asp:Label>
                     </div>
                 </ContentTemplate>
             </asp:UpdatePanel>
             
                    </div>
        
         <div style="float: right;margin-top:6px; width: 17%">
                <asp:Button ID="btnSave"  runat="server" Text='<%$Resources:ControlsCommon,Save %>'
                    CssClass="btnsubmit" onclick="btnSave_Click" ValidationGroup ="SaveInitiation"  OnClientClick ="return Save(true);"  />&nbsp
                       <asp:Button Width ="50px" ID="btnCancel" runat="server" Text='<%$Resources:ControlsCommon,Cancel %>'
                    CssClass="btnsubmit" onclick="btnCancel_Click"   />
            </div>
            
    </div>
    
    
    <div id="dvNoPermission" runat ="server"  style="margin-top:10px;display :none;text-align :center ; width: 100%">
    <asp:Label  ID="lblNoPermission" runat ="server" CssClass ="error" Text ="You have no permission to view this page">
    </asp:Label>
    </div> 
    
    
    
    </ContentTemplate>
    </asp:UpdatePanel>
        
           
   
    <asp:UpdatePanel ID="updEvaluators" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
           
         <AjaxControlToolkit:ModalPopupExtender Drag="true" 
                PopupControlID="dvEvaluators" TargetControlID="btn" ID="mdEvaluators"
                runat="server">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Button ID="btn" runat="server" Style="display: none" />
            <div id="dvEvaluators" style="width:85%;height:80%;overflow:scroll" >
                <uc1:AssignEvaluators ID="AssignEvaluators1" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img src="../images/add_performance_initiation.png" />
                </div>
                <div class="name">
                   <h5>  <asp:LinkButton ID="lnkAdd" runat="server" Text='<%$Resources:ControlsCommon,AddInitiation%>'  CausesValidation="false" 
                        onclick="lnkAdd_Click" >
		
	<%--Add Initiation--%>
	
	<%--<asp:Literal ID="Lit1234" runat ="server" meta:resourcekey="AddInitiation">
	</asp:Literal>--%>
<%--	<asp:Literal ID="Literal11" runat ="server"   Text='<%$Resources:ControlCommon,AddInitiation%>'  >
	</asp:Literal>
	--%>
</asp:LinkButton>	
	</h5>
                </div>
                
                   <div class="sideboxImage">
                    <img src="../images/view_performance_initiation.png" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkViewInitiation" runat="server"  Text='<%$Resources:ControlsCommon,ViewInitiation%>'  CausesValidation="false" 
                        onclick="lnkViewInitiation_Click" > 
		
	<%--View Initiation--%>
	<%--<asp:Literal ID="Literal10" runat ="server"   Text='<%$Resources:ControlCommon,ViewInitiation%>'  >
	</asp:Literal>--%>
	
	
	</asp:LinkButton></h5>
                </div>
                </a>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
