﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="TrainingCourse.aspx.cs" Inherits="Public_TrainingCourse" Title="Course" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li class='selected'><a href='TrainingCourse.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text="Training Course"></asp:Literal></span></a></li>
            <li><a href='TrainingSchedule.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text="Training Schedule"></asp:Literal></span></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal92" runat="server" Text="Attendance"></asp:Literal></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal93" runat="server" Text="Feedback"></asp:Literal></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal94" runat="server" Text="Employee Review"></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div>
        <div style="display: none">
            <asp:Button ID="btnSubmitMessage" runat="server" Text="submit" />
        </div>
        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
            BorderStyle="None">
            <asp:UpdatePanel ID="upnlMessage" runat="server">
                <ContentTemplate>
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmitMessage"
            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
            Drag="true">
        </AjaxControlToolkit:ModalPopupExtender>
    </div>
    <asp:UpdatePanel ID="updAdd" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="divAdd" runat="server" style="width: 100%; float: left; padding-top: 20px;">
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal1" runat="server" meta:ResourceKey="Code"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtCourseCode" runat="server" CssClass="textbox" Width="150px" Style="margin-right: 10px"
                        BackColor="Info" Enabled="False" MaxLength="20"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCourseCode" runat="server" ControlToValidate="txtCourseCode"
                        Display="Dynamic" SetFocusOnError="True" CssClass="error" ValidationGroup="Submit" ErrorMessage="*"
                       ></asp:RequiredFieldValidator>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal4" runat="server" meta:ResourceKey="Topic"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtTopic" runat="server" CssClass="textbox" Width="300px" Style="margin-right: 10px"
                        BackColor="Info" MaxLength="100"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTopic"
                        Display="Dynamic" SetFocusOnError="True" CssClass="error" ValidationGroup="Submit" ErrorMessage="*"
                       ></asp:RequiredFieldValidator>
                </div>
                <div class="trLeft" style="width: 25%; height: 60px; float: left;">
                    <asp:Literal ID="Literal2" runat="server" meta:ResourceKey="Description"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 60px; float: left;">
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="textbox" Width="300px"
                        TextMode="MultiLine" Rows="5" Height="70px" onchange="RestrictMulilineLength(this, 1000);"
                        onkeyup="RestrictMulilineLength(this, 1000);"></asp:TextBox>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal3" runat="server" meta:ResourceKey="Certification"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:RadioButtonList ID="rblIssued" RepeatColumns="2" RepeatDirection="Horizontal"
                        runat="server">
                        <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal6" runat="server" meta:ResourceKey="Status"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:DropDownList ID="ddlStatus" runat="server">
                        <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                        <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                    <%-- <asp:RadioButtonList ID="rblStatus" RepeatColumns="2" RepeatDirection="Horizontal"
                runat="server">
                <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
            </asp:RadioButtonList>--%>
                </div>
                <div style="clear: both; height: auto">
                </div>
                <div style="float: right; margin-top: 6px; width: 17%">
                    <asp:Button ID="btnSave" Width="50px" runat="server" Text='<%$Resources:ControlsCommon,Save %>'
                        CssClass="btnsubmit" ValidationGroup="Submit" OnClick="btnSave_Click" />&nbsp
                    <asp:Button Width="50px" ID="btnCancel" runat="server" Text='<%$Resources:ControlsCommon,Cancel %>'
                        CssClass="btnsubmit" OnClick="btnCancel_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="divSingleView" runat="server" style="width: 100%; float: left; padding-top: 20px;">
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal7" runat="server" meta:ResourceKey="Code"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:Label ID="lblCode" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal8" runat="server" meta:ResourceKey="Topic"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left; word-break: break-all;">
                    <asp:Label ID="lblTopic" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal10" runat="server" meta:ResourceKey="Certification"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:Label ID="lblCertification" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal13" runat="server" meta:ResourceKey="Status"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 60px; float: left;" style="word-break: break-all">
                    <asp:Literal ID="Literal9" runat="server" meta:ResourceKey="Description"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 60px; float: left;">
                    <asp:Label ID="lblDesc" runat="server"></asp:Label>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="divView" runat="server" style="width: 100%; float: left; padding-top: 20px;">
                <asp:DataList ID="dlCourse" runat="server" Width="100%" DataKeyField="CourseID" CssClass="labeltext"
                    OnItemCommand="dlCourse_ItemCommand">
                    <ItemStyle CssClass="item" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <table width="100%">
                            <tr>
                                <td width="25" valign="top" style="padding-left: 5px">
                                    <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkCourse')" />
                                </td>
                                <td style="padding-left: 7px">
                                    <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblDetails" runat="server"
                            onmouseover="showCancel(this);" onmouseout="hideCancel(this);">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td valign="top" rowspan="5" width="25" style="padding-left: 7px">
                                                <asp:CheckBox ID="chkCourse" runat="server" />
                                            </td>
                                            <td width="100%">
                                                <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="lnkCourse" runat="server" CssClass="listheader bold" CommandName="_View"
                                                                CausesValidation="False" Text='<%# Eval("Topic")%>' CommandArgument='<%# Eval("CourseID")%>'>
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td valign="top" align="right">
                                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandName="_Edit"
                                                                ImageUrl="~/images/edit.png" CommandArgument='<%# Eval("CourseID")%>' meta:resourcekey="Edit" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="padding-left: 25px">
                                                <table cellpadding="2" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td class="innerdivheader" width="30%" valign="top" align="left">
                                                            <asp:Literal ID="Literal11" runat="server" meta:Resourcekey="Code"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="2%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <%# Eval("CourseCode")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="innerdivheader" width="30%" valign="top" align="left">
                                                            <asp:Literal ID="Literal12" runat="server" meta:Resourcekey="Description"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="2%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <%# Eval("Description")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="innerdivheader" width="30%" valign="top" align="left">
                                                            <asp:Literal ID="Literal5" runat="server" meta:Resourcekey="Certification"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="2%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <%# Eval("Certification")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="CoursePager" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
         <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="txtSearch" WatermarkText='<%$Resources:DocumentsCommon,SearchBy%>'
                    WatermarkCssClass="WmCss" />
                <br />
                <div id="searchimg">
                    <asp:ImageButton ID="ImgSearch" ImageUrl="~/images/search.png " OnClick="btnSearch_Click"
                        runat="server" CausesValidation="false" />
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddCourse" ImageUrl="~/images/TrainingRequest_Big.png" runat="server"
                        CausesValidation="false" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" Text="Add Course"
                            OnClick="lnkAdd_Click">
                    
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/training_request_list.png"
                        runat="server" CausesValidation="false" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkView" runat="server" CausesValidation="false" Text="View Course"
                            OnClick="lnkView_Click">
                    
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="ImageButton3" ImageUrl="~/images/Delete.png" runat="server"
                        CausesValidation="false" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkDelete" runat="server" Enabled="true" CausesValidation="false"
                            Text="Delete" OnClick="lnkDelete_Click">
                        </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
