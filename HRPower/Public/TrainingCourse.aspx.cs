﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;

public partial class Public_TrainingCourse : System.Web.UI.Page
{
    clsTrainingCourse trainingCourse;
 
    protected void Page_Load(object sender, EventArgs e)
    {
        CoursePager.Fill += new controls_Pager.FillPager(BindDataList);
        if (!IsPostBack)
        {
         
            this.RegisterAutoComplete();
            GetMaxCourseID();

            BindDataList();
        }
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ClearAll();
        SetDivVisiblity(1);
    }
  
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AddUpdateTrainingCourse();
    }

    #region User Defined Functions

    private void ClearAll()
    {
        txtSearch.Text = txtTopic.Text = txtDescription.Text = string.Empty;
        ddlStatus.ClearSelection();
        rblIssued.ClearSelection();
        ViewState["CourseID"] = null;
        GetMaxCourseID();
    }
    private void RegisterAutoComplete()
    {
        clsUserMaster objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.TrainingCourse, -1);
    }
    /// <summary>
    /// Add training course
    /// </summary>
    private void AddUpdateTrainingCourse()
    {
        int CourseID = 0;

        if (ViewState["CourseID"] != null)
            CourseID = Convert.ToInt32(ViewState["CourseID"]);
        trainingCourse = new clsTrainingCourse();
        trainingCourse.CourseCode = txtCourseCode.Text;
        trainingCourse.Topic = txtTopic.Text;
        trainingCourse.Description = txtDescription.Text;
        trainingCourse.IsCertification = Convert.ToBoolean(rblIssued.SelectedValue.ToInt32());
        trainingCourse.Status = Convert.ToBoolean(ddlStatus.SelectedValue.ToInt32());
        if (CourseID > 0)
            trainingCourse.CourseID = CourseID;

       CourseID =  trainingCourse.SaveCourse();

       ViewState["CourseID"] = CourseID;

       BindDataList();
    }

    private void GetMaxCourseID()
    {
        trainingCourse = new clsTrainingCourse();
        txtCourseCode.Text = "TR" +  trainingCourse.GetCourseMaxNumber().ToString();
    }

    private void BindDataList()
    {
        trainingCourse = new clsTrainingCourse();
        trainingCourse.PageSize = CoursePager.PageSize;
        trainingCourse.PageIndex = CoursePager.CurrentPage + 1;
        trainingCourse.SearchKey = txtSearch.Text;
        DataSet ds = trainingCourse.GetCourse();
       
        DataTable dtCourse = ds.Tables[1];

        if (dtCourse.Rows.Count > 0)
        {
            CoursePager.Total = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            CoursePager.Visible = true;
            dlCourse.DataSource = dtCourse;
            dlCourse.DataBind();
            dlCourse.Visible = true;
            SetDivVisiblity(2);
        }
        else
        {
            dlCourse.Visible = false;
            SetDivVisiblity(1);
            CoursePager.Visible = false;
            ClearAll();
          
        }
        RegisterAutoComplete();


    }

    private void GetCourseDetails(int CourseID)
    {
        ViewState["CourseID"] = CourseID ;
        trainingCourse = new clsTrainingCourse();
        trainingCourse.CourseID = CourseID;
        DataTable dtCourseDetails = trainingCourse.GetCourseDetails();
        if (dtCourseDetails.Rows.Count > 0)
        {
            lblCode.Text = txtCourseCode.Text = dtCourseDetails.Rows[0]["Coursecode"].ToString();
            lblDesc.Text =  txtDescription.Text = dtCourseDetails.Rows[0]["Description"].ToString();
            lblTopic.Text = txtTopic.Text = dtCourseDetails.Rows[0]["Topic"].ToString();
            lblCertification.Text = dtCourseDetails.Rows[0]["Certification"].ToString();
            rblIssued.SelectedValue = dtCourseDetails.Rows[0]["IsCertification"].ToString() == "True" ?"1" :"0";
            ddlStatus.SelectedValue = dtCourseDetails.Rows[0]["Status"].ToString() == "True" ? "1" : "0";
            lblStatus.Text = dtCourseDetails.Rows[0]["StatusDesc"].ToString();
        }

        SetDivVisiblity(1);
    }

    /// <summary>
    /// Set Div Visibility
    /// </summary>
    /// <param name="intMode"></param>
    private void SetDivVisiblity(int intMode)
    {
        /* 1 -   Add New
         * 2 -   View
         * 3 -   Single View
       
         * */

        if (intMode == 1)
        {
            divAdd.Style["display"] = "block";
            divView.Style["display"] = divSingleView.Style["display"] = "none";
        }
        else if (intMode == 2)
        {
            divView.Style["display"] = "block";
            divAdd.Style["display"] = divSingleView.Style["display"] = "none";
       
        }
        else if (intMode == 3)
        {
            divSingleView.Style["display"] = "block";
            divAdd.Style["display"] = divView.Style["display"] = "none";
        }
      
    }

    private void DeleteCourse()
    {
        string message = string.Empty; 
        trainingCourse = new clsTrainingCourse();
        if (dlCourse.Items.Count > 0)
        {
            foreach (DataListItem item in dlCourse.Items)
            {
                CheckBox chkCourse = (CheckBox)item.FindControl("chkCourse");
                if (chkCourse == null)
                    continue;
                if (chkCourse.Checked)
                {
                   
                    trainingCourse.CourseID = Convert.ToInt32(dlCourse.DataKeys[item.ItemIndex]);
                    if (trainingCourse.DeleteCourse())
                        message = clsUserMaster.GetCulture() == "ar-AE" ? "Deleted successfully " : ("Deleted successfully");
                    else
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("Cannot delete reference exists.") : ("Cannot delete reference exists.");

                }
            }
        }
        else
        {
            trainingCourse.CourseID = Convert.ToInt32(ViewState["CourseID"]);
            if (trainingCourse.DeleteCourse())
                message = clsUserMaster.GetCulture() == "ar-AE" ? "Deleted successfully " : ("Deleted successfully");
            else
                message = clsUserMaster.GetCulture() == "ar-AE" ? ("Cannot delete reference exists.") : ("Cannot delete reference exists.");
        }

        msgs.InformationalMessage(message);
        mpeMessage.Show();

        BindDataList();

    }

    #endregion
    protected void dlCourse_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "_Edit":
                GetCourseDetails(Convert.ToInt32(e.CommandArgument));
                break;
            case "_View":
                GetCourseDetails(Convert.ToInt32(e.CommandArgument));
                SetDivVisiblity(3);
                break;

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        BindDataList();
        SetDivVisiblity(2);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        SetDivVisiblity(2);
        BindDataList();
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        DeleteCourse();
    }
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        CoursePager.CurrentPage = 0;
        BindDataList();    
    }
}
