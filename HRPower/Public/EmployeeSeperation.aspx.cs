﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;

public partial class Public_EmployeeSeperation : System.Web.UI.Page
{
    #region Fields
    private clsUserMaster objUser;
    private clsEmployeeSeperation objEmployeeSeperation;
    private clsEmployeeTakeOver objEmployeeTakeOver;
    private clsRoleSettings objRoleSettings;
    private clsJobPost objJobPost;
    private int CompanyId = 0;
    private bool AllCompanyPermission = false;

    private bool CanSave = false;
    private bool IsEdit = false;
    private bool CanCancel = false;
   
    private string CurrentSelectedValue
    {
        get;
        set;
    }
    #endregion
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {

        objUser = new clsUserMaster();
        TerminationPager.Fill += new controls_Pager.FillPager(TerminationPager_Fill);
        objEmployeeSeperation = new clsEmployeeSeperation();
        lblNoRecord.Visible = false;
        EnableMenus();

        CompanyId = objUser.GetCompanyId();


        // check if user has permission to manage all companies
        if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All))
        {
            AllCompanyPermission = true;
            CompanyId = 1;
        }

        Session["IsAllCompany"] = CompanyId;
        if (!IsPostBack)
        {


            if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Separation))
            {
                ViewSeperatedEmployee();
            }
            else if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.AddEdit_Separation))
            {
                TerminateEmployee();

            }
            else
            {
                dlTermination.ShowHeader = dlTermination.ShowFooter = TerminationPager.Visible = false;
                lblNoRecord.Text = "You dont have enough permission to view this page.";
                btnSearch.Enabled = false;
                lblNoRecord.Visible = true;
            }
           
        }
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    public void EnableOrDisablePrint(bool status)
    {
        if (!status)
        {
            lnkPrint.OnClientClick = "return true;";
            lnkEmail.OnClientClick = "return true;";
        }
        else
        {
            if (lnkPrint.Enabled)
                lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlTermination.ClientID + "');";
            else
                lnkPrint.OnClientClick = "return false;";

            if (lnkEmail.Enabled)
                lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlTermination.ClientID + "');";
            else
                lnkEmail.OnClientClick = "return false;";
        }


    }
    public void EnableMenus()
    {
        try
        {
            bool CanView = false;
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
            Button btnCancel = (Button)fvTerminationRequest.FindControl("btnCancel");
            lnkAdd.Enabled = CanSave = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.AddEdit_Separation);
            CanCancel = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Cancel_Separation);
            CanView = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Separation);
            lnkPrint.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Print_Separation);
            lnkEmail.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Email_Separation);

            if (btnCancel != null)
                btnCancel.Enabled = CanView;
            lnkView.Enabled = btnSearch.Enabled = CanView;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    void TerminationPager_Fill()
    {
        objEmployeeSeperation = new clsEmployeeSeperation();
        TerminationPager.Total = objEmployeeSeperation.GetTotalCount(CompanyId);
        GetSeperatedEmployees();
    }

    #endregion

    #region FormView_DataBound
    protected void fvTerminationRequest_DataBound(object sender, EventArgs e)
    {
        if (fvTerminationRequest.CurrentMode == FormViewMode.Insert | fvTerminationRequest.CurrentMode == FormViewMode.Edit)
        {
            try
            {
                FillComboCompany();
                FillComboAllEmployees();
                FillComboWorkingStatus();
                FillComboReasonForSeperation();
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        else if (fvTerminationRequest.CurrentMode == FormViewMode.ReadOnly)
        {
            if (((System.Data.DataRowView)(((System.Web.UI.WebControls.FormView)(sender)).DataItem)).Row.ItemArray[3] != null)
            {
                HtmlGenericControl DivReasonRight = (HtmlGenericControl)this.fvTerminationRequest.FindControl("DivReasonRight");
                HtmlGenericControl DivReasonLeft = (HtmlGenericControl)this.fvTerminationRequest.FindControl("DivReasonLeft");
                Label lblReasonCaption = (Label)this.fvTerminationRequest.FindControl("lblReasonCaption");
                Label lblReasonMessage = (Label)this.fvTerminationRequest.FindControl("lblReasonMessage");

                if (lblReasonCaption != null && lblReasonMessage != null)
                {
                    if (Convert.ToInt32(((System.Data.DataRowView)(((System.Web.UI.WebControls.FormView)(sender)).DataItem)).Row.ItemArray[12]) == 1)
                    {
                        DivReasonRight.Attributes.Add("style", "display:block");
                        DivReasonLeft.Attributes.Add("style", "display:block");
                        lblReasonCaption.Text = "Absconded Reason";
                    }
                    else if (Convert.ToInt32(((System.Data.DataRowView)(((System.Web.UI.WebControls.FormView)(sender)).DataItem)).Row.ItemArray[12]) == 5)
                    {
                        DivReasonRight.Attributes.Add("style", "display:block");
                        DivReasonLeft.Attributes.Add("style", "display:block");
                        lblReasonCaption.Text = "Terminated Reason";
                    }
                    else
                    {
                        DivReasonRight.Attributes.Add("style", "display:none");
                        DivReasonLeft.Attributes.Add("style", "display:none");
                    }
                }
            }
        }
    }
    #endregion

    #region FillComboWorkingStatus
    private void FillComboWorkingStatus()
    {
        try
        {
            objEmployeeSeperation = new clsEmployeeSeperation();
            DropDownList ddlWorkingStatus = (DropDownList)fvTerminationRequest.FindControl("ddlWorkStatus");
            if (ddlWorkingStatus != null)
            {
                DataTable dt = objEmployeeSeperation.GetWorkingStatus();
                DataRow dr = dt.NewRow();
                dr["WorkStatusID"] = "0";
                dr["WorkStatus"] = "Select";
                dt.Rows.InsertAt(dr, 0);

                ddlWorkingStatus.DataSource = dt;
                ddlWorkingStatus.DataValueField = "WorkStatusID";
                ddlWorkingStatus.DataTextField = "WorkStatus";
                ddlWorkingStatus.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objEmployeeSeperation = null;
        }
    }

    #endregion

    #region EnableOrDisableButtons

    private void EnableOrDisableButtons(bool Status)
    {
        Button btnSave = (Button)fvTerminationRequest.FindControl("btnSubmit");
        Button btnSaveAndSeperate = (Button)fvTerminationRequest.FindControl("btnSaveAndSeperate");
        btnSave.Enabled = btnSaveAndSeperate.Enabled = Status;
    }

    #endregion

    #region FillComboCompany
    private void FillComboCompany()
    {
        try
        {
            DropDownList ddlCompany = (DropDownList)fvTerminationRequest.FindControl("ddlCompany");
            objJobPost = new clsJobPost();
            ddlCompany.DataSource = objJobPost.FillCompany();
            ddlCompany.DataBind();
            Session["@IsAllCompany"] = CompanyId;
            if (!AllCompanyPermission)
            {
                ddlCompany.SelectedValue = CompanyId.ToString();
                ddlCompany.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objJobPost = null;
        }
    }
    #endregion

    #region FormView_ItemCommand
    protected void fvTerminationRequest_ItemCommand(object sender, FormViewCommandEventArgs e)
    {

    }
    #endregion

    private void TerminateEmployee()
    {
        EnableOrDisablePrint(false);
        fvTerminationRequest.ChangeMode(FormViewMode.Insert);
        fvTerminationRequest.DataSource = null;
        fvTerminationRequest.DataBind();

        DropDownList ddlCompany = (DropDownList)fvTerminationRequest.FindControl("ddlCompany");
        if (ddlCompany != null)
            Session["TerminateCompanyId"] = ddlCompany.SelectedValue;

        fvTerminationRequest.Visible = true;
        upForm.Update();
        dlTermination.DataSource = null;
        dlTermination.DataBind();
        upnlDatalist.Update();
        TerminationPager.Visible = false;
        if (ViewState["EmployeeSeperatedId"] != null)
        {
            GetEmployeeDetails(0);
            ViewState["EmployeeSeperatedId"] = null;
        }

    }


    #region Add_Click Event
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        try
        {
            txtSearch.Text = "";
            ClearAll(true);
            TerminateEmployee();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
        }

    }
    #endregion

    #region ViewSeperatedEmployee()
    private void ViewSeperatedEmployee()
    {
        try
        {
            objEmployeeSeperation = new clsEmployeeSeperation();
            TerminationPager.CurrentPage = 0;
            TerminationPager.Total = objEmployeeSeperation.GetTotalCount(CompanyId);
            ViewState["Search"] = null;
            txtSearch.Text = "";
            ShowOrHide(true);
            GetSeperatedEmployees();
            upnlDatalist.Update();
            upForm.Update();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        
    }

    #endregion

    #region View_Click Event
    protected void lnkView_Click(object sender, EventArgs e)
    {
        ViewSeperatedEmployee();
    }
    #endregion

    #region GetSeperatedEmployees
    private void GetSeperatedEmployees()
    {
        try
        {
            EnableOrDisablePrint(true);

            TerminationPager.Visible = true;

            fvTerminationRequest.ChangeMode(FormViewMode.ReadOnly);
            fvTerminationRequest.Visible = false;

            if (Convert.ToString(ViewState["Search"]) == "1")
            {
                objEmployeeSeperation.PageIndex = TerminationPager.CurrentPage;
                objEmployeeSeperation.PageSize = TerminationPager.PageSize;

                using (DataSet ds = objEmployeeSeperation.GetSeperatedEmployeesBySearch(txtSearch.Text.Trim(), CompanyId))
                {
                    TerminationPager.Total = Convert.ToInt32(ds.Tables[1].Rows[0]["Count"]);
                    dlTermination.DataSource = ds.Tables[0];
                    dlTermination.DataBind();
                    upnlDatalist.Update();
                    upForm.Update();
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        ShowOrHide(false);
                        
                    }
                }
            }
            else
            {
                objEmployeeSeperation.PageIndex = TerminationPager.CurrentPage;
                objEmployeeSeperation.PageSize = TerminationPager.PageSize;
                using (DataSet ds = objEmployeeSeperation.GetSeperatedEmployees(CompanyId))
                {
                    TerminationPager.Total = Convert.ToInt32(ds.Tables[1].Rows[0]["Count"]);
                    dlTermination.DataSource = ds.Tables[0];
                    dlTermination.DataBind();
                    upnlDatalist.Update();
                    upForm.Update();
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        ShowOrHide(false);
                        
                    }
                }
            }
            upnlDatalist.Update();
            upForm.Update();
            
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    #endregion

    #region GetSearchCount
    private int GetSearchCount()
    {
        return objEmployeeSeperation.GetSearchTotalCount(CompanyId);
    }

    #endregion

    #region GetEmployeeDetails()
    private void GetEmployeeDetails(int EmployeeId)
    {
        DataTable dt;
        try
        {
            objEmployeeTakeOver = new clsEmployeeTakeOver();
            fvTerminationRequest.ChangeMode(FormViewMode.Insert);
            TextBox txtEmployeeCode = (TextBox)fvTerminationRequest.FindControl("txtEmployeeCode");
            DropDownList ddlEmployeeName = (DropDownList)fvTerminationRequest.FindControl("ddlEmployees");

            if (txtEmployeeCode.Text != string.Empty || ViewState["EmployeeSeperatedId"] != null || EmployeeId != 0)
            {
                dt = objEmployeeTakeOver.GetEmployeeDetailsByEmployeeId(EmployeeId, txtEmployeeCode.Text.Trim(), ViewState["EmployeeSeperatedId"]);
                if (dt.Rows.Count > 0)
                {
                    TextBox txtEmployeeWorkingStatus = (TextBox)fvTerminationRequest.FindControl("txtWorkingStatus");
                    DropDownList ddlEmployee = (DropDownList)fvTerminationRequest.FindControl("ddlEmployees");
                    HiddenField hfEmployeeId = (HiddenField)fvTerminationRequest.FindControl("hfEmployeeId");
                    HiddenField hfCurrentDate = (HiddenField)fvTerminationRequest.FindControl("hfCurrentDate");
                    DropDownList ddlCompany = (DropDownList)fvTerminationRequest.FindControl("ddlCompany");

                    HiddenField hfEmployeeSeperatedId = (HiddenField)fvTerminationRequest.FindControl("hfEmployeeSeperatedId");
                    TextBox txtDateofjoining = (TextBox)fvTerminationRequest.FindControl("txtDateofjoining");
                    DropDownList ddlWorkStatus = (DropDownList)fvTerminationRequest.FindControl("ddlWorkStatus");
                    TextBox txtDateofSeperation = (TextBox)fvTerminationRequest.FindControl("txtDateofSepearation");
                    DropDownList ddlWorkStatus1 = (DropDownList)fvTerminationRequest.FindControl("ddlWorkStatus");
                    TextBox txtRemarks = (TextBox)fvTerminationRequest.FindControl("txtRemarks");
                    DropDownList ddlReasonForSeperation = (DropDownList)fvTerminationRequest.FindControl("ddlReasonForSeperation");
                    HtmlTableRow test = (HtmlTableRow)fvTerminationRequest.FindControl("test");
                    HtmlTableCell seperationDate = (HtmlTableCell)fvTerminationRequest.FindControl("seperationDate");
                    HtmlTableRow ReasonForTermination = (HtmlTableRow)fvTerminationRequest.FindControl("ReasonForSeperation");
                    HtmlTableCell lblReasonForSeperation = (HtmlTableCell)fvTerminationRequest.FindControl("lblReasonForSeperation");


                    if (dt.Rows[0]["WorkStatus"] == DBNull.Value || dt.Rows[0]["WorkStatus"] != null && Convert.ToInt32(dt.Rows[0]["CurrentWorkingStatusId"]) == 6)
                        EnableOrDisableButtons(true);
                    else
                        EnableOrDisableButtons(false);

                    if (hfCurrentDate != null)
                        hfCurrentDate.Value = objEmployeeSeperation.GetCurrentDate().ToString("dd/MM/yyyy");




                    if (IsEdit)
                    {
                        ddlCompany.SelectedValue = dt.Rows[0]["CompanyId"].ToString();
                        FillComboAllEmployees();
                        ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeId"].ToString();
                        txtEmployeeCode.Text = dt.Rows[0]["EmployeeNumber"].ToString();
                    }
                    else
                    {
                        if (EmployeeId == 0)
                        {
                            ddlEmployee.SelectedValue = dt.Rows[0]["EmployeeId"].ToString();
                        }
                        else
                        {
                            txtEmployeeCode.Text = dt.Rows[0]["EmployeeNumber"].ToString();

                        }
                    }
                    txtDateofjoining.Text = Convert.ToDateTime(dt.Rows[0]["DateOfJoining"]).ToString("dd/MM/yyyy");
                    hfEmployeeId.Value = dt.Rows[0]["EmployeeId"].ToString();
                    ddlEmployee.SelectedValue = hfEmployeeId.Value;
                    txtEmployeeWorkingStatus.Text = dt.Rows[0]["WorkStatus"].ToString();
                    ddlCompany.SelectedValue = dt.Rows[0]["CompanyId"].ToString();
                    if (dt.Rows[0]["SeperationType"] != DBNull.Value)
                    {
                        if (dt.Rows[0]["EmployeeNumber"] != DBNull.Value)
                        {
                            txtEmployeeCode.Text = dt.Rows[0]["EmployeeNumber"].ToString();
                            hfEmployeeSeperatedId.Value = dt.Rows[0]["EmployeeSeperatedId"].ToString();
                        }
                        test.Attributes.Add("style", "display:block");
                        ddlWorkStatus1.SelectedValue = dt.Rows[0]["SeperationType"].ToString();
                        seperationDate.InnerHtml = "Date of " + dt.Rows[0]["SeperationTypeS"].ToString();
                        if (Convert.ToInt32(dt.Rows[0]["SeperationType"]) == 1 || Convert.ToInt32(dt.Rows[0]["SeperationType"]) == 5)
                        {
                            ReasonForTermination.Attributes.Add("style", "display:block");

                            if (dt.Rows[0]["ReasonforSeperation"] != DBNull.Value)
                            {
                                FillComboReasonForSeperation();
                                ddlReasonForSeperation.SelectedValue = dt.Rows[0]["ReasonforSeperation"].ToStringCustom();
                            }
                            if (Convert.ToInt32(dt.Rows[0]["SeperationType"]) == 1)
                                lblReasonForSeperation.InnerHtml = "Reason for absconding";
                            else
                                lblReasonForSeperation.InnerHtml = "Reason for termination";
                        }
                        else
                            ReasonForTermination.Attributes.Add("style", "display:none");
                        txtDateofSeperation.Text = Convert.ToDateTime(dt.Rows[0]["SeperatedDate"]).ToString("dd/MM/yyyy");
                        txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                    }
                    else
                    {
                        ddlWorkStatus1.SelectedValue = "0";
                        test.Attributes.Add("style", "display:none");
                        ReasonForTermination.Attributes.Add("style", "display:none");
                    }

                    upForm.Update();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            dt = null;
            objEmployeeSeperation = null;
        }
    }
    #endregion

    #region Search_Click Event
    protected void imgSearch_Click(object sender, ImageClickEventArgs e)
    {
        GetEmployeeDetails(0); 
    }
    #endregion

    #region FillAllEmployees

    private void FillComboAllEmployees()
    {
        try
        {
            objEmployeeSeperation = new clsEmployeeSeperation();
            DropDownList ddlEmployees = (DropDownList)fvTerminationRequest.FindControl("ddlEmployees");
            DropDownList ddlCompany = (DropDownList)fvTerminationRequest.FindControl("ddlCompany");

            DataTable dt = objEmployeeSeperation.GetAllEmployees(Convert.ToInt32(ddlCompany.SelectedValue));
            DataRow dr = dt.NewRow();
            dr["EmployeeFullName"] = "Select";
            dr["EmployeeId"] = 0;
            dt.Rows.InsertAt(dr, 0);
            ddlEmployees.DataTextField = "EmployeeFullName";
            ddlEmployees.DataValueField = "EmployeeId";
            ddlEmployees.DataSource = dt;
            ddlEmployees.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objEmployeeSeperation = null;
        }
    }
    #endregion

    #region IsValid


    #endregion

    #region SaveSeparation

    private void SaveSeperation(bool Seperate)
    {
        if (objUser.GetEmployeeId() > 0)
        {
            HiddenField hfEmployeeId = (HiddenField)fvTerminationRequest.FindControl("hfEmployeeId");
            HiddenField hfEmployeeSeperatedId = (HiddenField)fvTerminationRequest.FindControl("hfEmployeeSeperatedId");
            DropDownList ddlWorkStatus1 = (DropDownList)fvTerminationRequest.FindControl("ddlWorkStatus");
            TextBox txtremarks = (TextBox)fvTerminationRequest.FindControl("txtRemarks");
            DropDownList ddlReasonForSeperation = (DropDownList)fvTerminationRequest.FindControl("ddlReasonForSeperation");
            TextBox txtDateofSepearation = (TextBox)fvTerminationRequest.FindControl("txtDateofSepearation");

            if (hfEmployeeSeperatedId.Value != string.Empty)
                objEmployeeSeperation.EmployeeSeperatedId = Convert.ToInt32(hfEmployeeSeperatedId.Value);
            objEmployeeSeperation.EmployeeId = Convert.ToInt64(hfEmployeeId.Value);
            objEmployeeSeperation.SeperatedBy = objUser.GetEmployeeId();
            objEmployeeSeperation.SeperatedDate = clsCommon.Convert2DateTime(txtDateofSepearation.Text);
            objEmployeeSeperation.SeperationType = Convert.ToInt32(ddlWorkStatus1.SelectedValue);
            if (ddlWorkStatus1.SelectedIndex == 1 || ddlWorkStatus1.SelectedIndex == 4)
                objEmployeeSeperation.SeperationReason = Convert.ToInt32(ddlReasonForSeperation.SelectedValue);
            else
                objEmployeeSeperation.SeperationReason = 0;
            objEmployeeSeperation.Remarks = txtremarks.Text.Trim();
            if (objEmployeeSeperation.SeperateEmployee(Seperate))
            {

                if (Seperate)
                    Message1.InformationalMessage("Employee seperated successfully");
                else
                    Message1.InformationalMessage("Saved successfully");
                PopUp.Attributes.Add("style", "display:block");
                ModalPopupExtender1.Show();
                ListTerminatedEmployee(null, Convert.ToInt32(hfEmployeeId.Value));
                ClearAll(true);
            }
        }
        else
        {
            Message1.InformationalMessage("Please log in as an employee");
            PopUp.Attributes.Add("style", "display:block");
            ModalPopupExtender1.Show();
        }
    }

    #endregion

    #region Submit_Click Event

    protected void btnSubmit_Click1(object sender, EventArgs e)
    {
        try
        {
            SaveSeperation(false);
        }
        catch (Exception ex)
        {
            throw ex;

        }

    }
    #endregion

    #region GetEmployeePhoto

    public string GetEmployeePhoto(object EmployeeID, int width)
    {
        return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }


    protected string GetEmployeeRecentPhoto(object EmployeeID, object RecentPhoto, int width)
    {
        if (RecentPhoto == DBNull.Value)
            return GetEmployeePassportPhoto(EmployeeID, width);
        else
            return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }

    protected string GetEmployeePassportPhoto(object EmployeeID, int width)
    {
        return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }

    #endregion

    #region ClearAll
    private void ClearAll(bool EmployeeNameShowStatus)
    {
        try
        {

            TextBox txtEmployeeCode = (TextBox)fvTerminationRequest.FindControl("txtEmployeeCode");
            TextBox txtEmployeeName = (TextBox)fvTerminationRequest.FindControl("txtEmployeeName");
            TextBox txtRemarks = (TextBox)fvTerminationRequest.FindControl("txtRemarks");
            TextBox txtDateOfJoining = (TextBox)fvTerminationRequest.FindControl("txtDateOfJoining");
            TextBox txtDateOfSeperation = (TextBox)fvTerminationRequest.FindControl("txtDateofSepearation");
            TextBox txtEmployeeWorkingStatus = (TextBox)fvTerminationRequest.FindControl("txtWorkingStatus");
            HiddenField hfEmployeeId = (HiddenField)fvTerminationRequest.FindControl("hfEmployeeId");
            HiddenField hfEmployeeSeperatedId = (HiddenField)fvTerminationRequest.FindControl("hfEmployeeSeperatedId");
            DropDownList ddlWorkStatus = (DropDownList)fvTerminationRequest.FindControl("ddlWorkStatus");
            DropDownList ddlEmployeeName = (DropDownList)fvTerminationRequest.FindControl("ddlEmployees");
            DropDownList ddlReasonForSeperation = (DropDownList)fvTerminationRequest.FindControl("ddlReasonForSeperation");

            if (txtEmployeeCode != null)
            {
                hfEmployeeId.Value = hfEmployeeSeperatedId.Value = "";
                txtEmployeeCode.Text = "";
                txtEmployeeWorkingStatus.Text = "";
                if (EmployeeNameShowStatus)
                    ddlEmployeeName.SelectedValue = "0";
                ddlWorkStatus.SelectedIndex = 0;
                txtDateOfJoining.Text = "";
                txtDateOfSeperation.Text = "";
                ddlReasonForSeperation.SelectedIndex = 0;
                txtRemarks.Text = "";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }


    }
    #endregion
   
    protected void dlTermination_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        Label lblSeperated = (Label)e.Item.FindControl("lblSeperatedReason");
        Label lblSeperated1 = (Label)e.Item.FindControl("lblSeperatedReason1");
        Label lblSeperatedColon = (Label)e.Item.FindControl("lblSeperatedColon");
        LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
        LinkButton btnCancel = (LinkButton)e.Item.FindControl("btnCancel");

        if (btnEdit != null)
        {
            btnEdit.Enabled = CanSave;
        }

        if (btnCancel != null)
        {
            btnCancel.Enabled = CanCancel;
        }
        if (lblSeperated != null && lblSeperated1 != null)
            if (((System.Data.DataRowView)(e.Item.DataItem)).Row.ItemArray[3] != null)
            {
                if (Convert.ToInt32(((System.Data.DataRowView)(e.Item.DataItem)).Row.ItemArray[4]) == 1)
                {
                    lblSeperated.Text = "Absconded reason";
                    lblSeperated1.Text = ((System.Data.DataRowView)(e.Item.DataItem)).Row.ItemArray[9].ToString();
                }
                else if (Convert.ToInt32(((System.Data.DataRowView)(e.Item.DataItem)).Row.ItemArray[4]) == 5)
                {
                    lblSeperated.Text = "Terminated reason";
                    lblSeperated1.Text = ((System.Data.DataRowView)(e.Item.DataItem)).Row.ItemArray[9].ToString();
                }
                else
                {
                    lblSeperated.Visible = false;
                    lblSeperated1.Visible = false;
                    lblSeperatedColon.Visible = false;
                }
            }
    }

    #region Clear
    private void ShowOrHide(bool status)
    {
        if (!status)
        {
            dlTermination.ShowHeader = dlTermination.ShowFooter = TerminationPager.Visible = false;
            lblNoRecord.Text = "No records found";
            lblNoRecord.Visible = true;
        }
        else
        {
            dlTermination.ShowHeader = dlTermination.ShowFooter = TerminationPager.Visible = true;
            lblNoRecord.Visible = false;
        }
        upnlDatalist.Update();
        upForm.Update();
    }


    #region GetSeperatedEmployeeBySearch()


    public void GetSeperatedEmployeeBySearch()
    {
        //if (txtSearch.Text != string.Empty)
        //{
            TerminationPager.CurrentPage = 0;
            ViewState["Search"] = 1;
            GetSeperatedEmployees();
            upnlDatalist.Update();
            upForm.Update();
        //}
    }
    #endregion

    #endregion
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetSeperatedEmployeeBySearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }


    }
   
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        IsEdit = true;
        if (((LinkButton)((sender))).CommandArgument != null)
        {
            ViewState["EmployeeSeperatedId"] = Convert.ToInt32(((LinkButton)((sender))).CommandArgument);
            if (ViewState["EmployeeSeperatedId"].ToString() != string.Empty)
            {
                /* Status Description 
                 * 0 -- Can Cancel Or Edit 
                 * 1 -- Take Over Details Exist
                 * 2 -- Take Over Details Exist And Employee Working Status Changed -- Cannot do any Changes                 */
                int Status = objEmployeeSeperation.GetEmployeeStatusByEmployeeSeperationId(Convert.ToInt32(ViewState["EmployeeSeperatedId"]));
                if (Status == 2)
                {
                    Message1.InformationalMessage("Sorry you cannot edit already seperated employee");
                    PopUp.Attributes.Add("style", "display:block");
                    ModalPopupExtender1.Show();
                }
                else
                {
                    TerminateEmployee();
                }
            }
        }
        IsEdit = false;

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        IsEdit = true;
        lblNoRecord.Visible = false;
        if (((LinkButton)((sender))).CommandArgument != null)
        {
            ViewState["EmployeeSeperatedId"] = Convert.ToInt32(((LinkButton)((sender))).CommandArgument);
            if (ViewState["EmployeeSeperatedId"].ToString() != string.Empty)
            {
                /* Status Description 
                 * 0 -- Can Cancel Or Edit 
                 * 1 -- Take Over Details Exist
                 * 2 -- Take Over Details Exist And Employee Working Status Changed -- Cannot do any Changes                 */
                int Status = objEmployeeSeperation.GetEmployeeStatusByEmployeeSeperationId(Convert.ToInt32(ViewState["EmployeeSeperatedId"]));
                if (Status == 1)
                {
                    Message1.InformationalMessage("Please delete all details from take over");
                    PopUp.Attributes.Add("style", "display:block");
                    ModalPopupExtender1.Show();

                }
                else if (Status == 2)
                {
                    Message1.InformationalMessage("Sorry you cannot cancel already seperated employee");
                    PopUp.Attributes.Add("style", "display:block");
                    ModalPopupExtender1.Show();
                }
                else
                {
                    if (objEmployeeSeperation.CancelEmployeeSeperation(Convert.ToInt32(ViewState["EmployeeSeperatedId"])))
                    {
                        Message1.InformationalMessage("Employee seperated has been cancelled successfully");
                        dlTermination.DataBind();
                        if (ViewState["Search"] != null)
                            GetSeperatedEmployeeBySearch();
                        else
                            ViewSeperatedEmployee();
                    }
                    else
                    {
                        Message1.InformationalMessage("Employee seperated cancellation failed");
                    }
                    ModalPopupExtender1.Show();

                }
            }


        }
        IsEdit = false;
    }
    protected void ddlEmployees_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddlEmployees_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (((System.Web.UI.WebControls.DropDownList)(sender)).SelectedIndex > 0)
        {

        } ClearAll(false);
        GetEmployeeDetails(Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)(sender)).SelectedValue));

    }
    protected void btnSaveAndSeperate_Click(object sender, EventArgs e)
    {
        SaveSeperation(true);
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillComboAllEmployees();
        Session["TerminateCompanyId"] = ((System.Web.UI.WebControls.DropDownList)(sender)).SelectedValue;
        ClearAll(false);
    }

    public bool ValidateDate()
    {
        TextBox txtDateOfJoining = (TextBox)fvTerminationRequest.FindControl("txtDateOfJoining");
        TextBox txtDateOfSeperation = (TextBox)fvTerminationRequest.FindControl("txtDateofSepearation");
        HtmlTableCell seperationDate = (HtmlTableCell)fvTerminationRequest.FindControl("seperationDate");

        if (txtDateOfJoining != null && txtDateOfSeperation != null)
        {

            if (Convert.ToDateTime(txtDateOfSeperation.Text) < Convert.ToDateTime(txtDateOfJoining.Text))
            {
                Message1.WarningMessage(seperationDate.InnerHtml + "should be greater than date of joining");
                ModalPopupExtender1.Show();
                return false;
            }
            else
                return true;
        }
        return false;

    }

    private void ListTerminatedEmployee(object EmployeeSeperatedId, int EmpId)
    {
        try
        {
            lblNoRecord.Visible = false;
            objEmployeeTakeOver = new clsEmployeeTakeOver();
            hdnEmployeeID.Value = EmpId.ToStringCustom();
            fvTerminationRequest.ChangeMode(FormViewMode.ReadOnly);

            if (EmployeeSeperatedId == null)
                fvTerminationRequest.DataSource = objEmployeeTakeOver.GetEmployeeDetailsByEmployeeId(EmpId, "", null);
            else
                fvTerminationRequest.DataSource = objEmployeeTakeOver.GetEmployeeDetailsByEmployeeId(0, "", EmployeeSeperatedId);

            fvTerminationRequest.DataBind();
            fvTerminationRequest.Visible = true;
            upForm.Update();
            dlTermination.DataSource = null;
            dlTermination.DataBind();
            upnlDatalist.Update();
            TerminationPager.Visible = false;
            
            EnableOrDisablePrint(false);
        }

        catch (Exception ex)
        {
            throw ex;
        }

        finally
        {
            objEmployeeTakeOver = null;
        }
    }

    protected void lnkCompany_Click(object sender, EventArgs e)
    {
        if (((LinkButton)((sender))).CommandArgument != null)
        {
            int EmployeeId = Convert.ToInt32(((LinkButton)((sender))).CommandArgument);
            ListTerminatedEmployee(null, EmployeeId);
        }

    }
    protected void btnCancel_Click1(object sender, EventArgs e)
    {
        ViewSeperatedEmployee();
    }


    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        string sEmployeeIds = string.Empty;
        objEmployeeSeperation = new clsEmployeeSeperation();

        if (dlTermination.Items.Count > 0)
        {
            foreach (DataListItem item in dlTermination.Items)
            {
                HiddenField hfEmployeeId = (HiddenField)item.FindControl("hfIsCompany");
                CheckBox chkEmployee;
                chkEmployee = (CheckBox)item.FindControl("chkEmployee");
                if (chkEmployee != null && chkEmployee.Checked && hfEmployeeId != null)
                {
                    sEmployeeIds = hfEmployeeId.Value + ',' + sEmployeeIds;
                }

            }

            if (sEmployeeIds.Length > 0)
                sEmployeeIds = sEmployeeIds.Substring(0, sEmployeeIds.Length - 1);

            if (sEmployeeIds.Contains(","))
            {
                divPrint.InnerHtml = objEmployeeSeperation.PrintMultipleItemSeperate(sEmployeeIds);
            }
            else
            {
                    divPrint.InnerHtml = objEmployeeSeperation.PrintSingleItemSeperate(Convert.ToInt32(sEmployeeIds));
            }

        }
        else
        {
            if (hdnEmployeeID.Value != null)
                sEmployeeIds = hdnEmployeeID.Value.ToStringCustom();
            divPrint.InnerHtml = objEmployeeSeperation.PrintSingleItemSeperate(Convert.ToInt32(sEmployeeIds));
        }
       

              
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);

    }

    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        string sEmployeeIds = string.Empty;
        if (dlTermination.Items.Count > 0)
        {
            objEmployeeSeperation = new clsEmployeeSeperation();

           

            foreach (DataListItem item in dlTermination.Items)
            {
                HiddenField hfEmployeeId = (HiddenField)item.FindControl("hfIsCompany");
                CheckBox chkEmployee;
                chkEmployee = (CheckBox)item.FindControl("chkEmployee");
                if (chkEmployee != null && chkEmployee.Checked && hfEmployeeId != null)
                {
                    sEmployeeIds = hfEmployeeId.Value + ',' + sEmployeeIds;
                }

            }
            if (sEmployeeIds.Length > 0)
                sEmployeeIds = sEmployeeIds.Substring(0, sEmployeeIds.Length - 1);

        }
        else
        {
            if (hdnEmployeeID.Value != null)
                sEmployeeIds = hdnEmployeeID.Value.ToStringCustom();
        }
        ScriptManager.RegisterStartupScript(lnkEmail, this.GetType(), "Email", "showMailDialog('Type=EmployeeSeperation&SEmployeeId=" + sEmployeeIds + "');", true);

    }
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
       GetSeperatedEmployeeBySearch();
       upnlDatalist.Update();
       upForm.Update();
       
    }
    protected void btnReasonForSeperation_Click(object sender, EventArgs e)
    {
        DropDownList ddlReasonForSeperation = (DropDownList)fvTerminationRequest.FindControl("ddlReasonForSeperation");
        UpdatePanel updReasonForSeperation = (UpdatePanel)fvTerminationRequest.FindControl("updReasonForSeperation");
        if (ddlReasonForSeperation != null && ddlReasonForSeperation != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "HRTerminationReasonReference";
            ReferenceControlNew1.DataTextField = "Reason";
            ReferenceControlNew1.DataValueField = "ReasonId";
            ReferenceControlNew1.FunctionName = "FillComboReasonForSeperation";
            ReferenceControlNew1.SelectedValue = ddlReasonForSeperation.SelectedValue;
            ReferenceControlNew1.DisplayName = "Reason For Seperation";
            ReferenceControlNew1.PredefinedField = "Predefined";
            ReferenceControlNew1.PopulateData();
            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    public void FillComboReasonForSeperation()
    {
        try
        {
            objEmployeeSeperation = new clsEmployeeSeperation();
            DropDownList ddlReasonForSeperation = (DropDownList)fvTerminationRequest.FindControl("ddlReasonForSeperation");
            UpdatePanel updReasonForSeperation = (UpdatePanel)fvTerminationRequest.FindControl("updReasonForSeperation");
            if (ddlReasonForSeperation != null && updReasonForSeperation != null)
            {
                DataTable dt = objEmployeeSeperation.GetReasonForSeperation();
                DataRow dr = dt.NewRow();
                dr["ReasonId"] = "0";
                dr["Reason"] = "Select";
                dt.Rows.InsertAt(dr, 0);

                ddlReasonForSeperation.DataSource = dt;
                ddlReasonForSeperation.DataValueField = "ReasonId";
                ddlReasonForSeperation.DataTextField = "Reason";
                ddlReasonForSeperation.DataBind();
                ddlReasonForSeperation.SelectedValue = CurrentSelectedValue;
                updReasonForSeperation.Update();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objEmployeeSeperation = null;
        }
    }

}
