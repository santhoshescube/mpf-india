﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;

public partial class Public_Templates : System.Web.UI.Page
{
    clsTemplates objTemplates;
    clsXML objXML;
    clsUserMaster objUserMaster;
    clsRoleSettings objRoleSettings;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Auto complete
        this.RegisterAutoComplete();
        // 

        pgTemplates.Fill += new controls_Pager.FillPager(BindTemplates);
        if (!IsPostBack)
        {
            objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            DataTable dt = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.TemplatesHR);  // Permission Such as view /add /delete / update 
            ViewState["Permission"] = dt;
            SetPermission();


            //if (objRoleSettings.IsMenuEnabled(objUserMaster.GetRoleId(), clsRoleSettings._MENU.Templates))
            //{
            //    btnGo.Enabled = true;
            //    lnkView.Enabled = true;
            //    lnkNew.Enabled = true;
            //    lnkDelete.Enabled = true;
            //    BindTemplates();
            //}
            //else
            //{
            //    fvTemplates.ChangeMode(FormViewMode.ReadOnly);
            //    fvTemplates.DataSource = null;
            //    fvTemplates.DataBind();
            //    pgTemplates.Visible = false;
            //    //lblHeading.Text = " Template List";
            //    //imgHeader.ImageUrl = "../images/Template_list_Big.png";
            //    lblNodata.Text = "You dont have enough permission to view this page.";
            //    btnGo.Enabled = false;
            //    lnkView.Enabled = false;
            //    lnkNew.Enabled = false;
            //    lnkDelete.Enabled = false;
            //}
        }
    }
    public void SetPermission()
    {
        clsUserMaster objUser = new clsUserMaster();
        clsRoleSettings objRole = new clsRoleSettings();
        int RoleId;
        RoleId = objUser.GetRoleId();
        if (RoleId != 1 && RoleId != 2 && RoleId != 3)
            {
            DataTable dtm = (DataTable)ViewState["Permission"];
            if (dtm.Rows.Count > 0)
            {
  

               

                    lnkView.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                    lnkNew.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                    lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();

                    if (dtm.Rows[0]["IsCreate"].ToBoolean() != true || dtm.Rows[0]["IsUpdate"].ToBoolean() != true)
                    {
                        Button btnSubmit = (Button)fvTemplates.FindControl("btnSubmit");
                        if (btnSubmit !=null)
                        {
                            btnSubmit.Enabled = false;
                            btnSubmit.OnClientClick = "return false;";
                        }
                    }

                    if (dtm.Rows[0]["IsCreate"].ToBoolean() == true || dtm.Rows[0]["IsUpdate"].ToBoolean() == true || dtm.Rows[0]["IsView"].ToBoolean() == true)
                    {
                        btnGo.Enabled = true;
                        BindTemplates();
                     
                    }
                    else
                    {  
                       // 
                        fvTemplates.ChangeMode(FormViewMode.ReadOnly);
                        
                        fvTemplates.DataSource = null;
                        fvTemplates.DataBind();
                        lblNodata.Text = "You dont have enough permission to view this page.";
                        lblNodata.Visible = true;
                        pgTemplates.Visible = false;
                    }
                    if (!dtm.Rows[0]["IsDelete"].ToBoolean())
                    {
                        lnkDelete.Enabled = false;
                        lnkDelete.OnClientClick = "return false;";
                    } 
                
                    
            }
            else
            {
                fvTemplates.ChangeMode(FormViewMode.ReadOnly);
                fvTemplates.DataSource = null;
                fvTemplates.DataBind();
                pgTemplates.Visible = false;
                lblNodata.Text = "You dont have enough permission to view this page.";
                btnGo.Enabled = false;
                lnkView.Enabled = false;
                lnkNew.Enabled = false;
                lnkDelete.Enabled = false;
            }
        }
        else
        {
            btnGo.Enabled = true;
            lnkView.Enabled = true;
            lnkNew.Enabled = true;
            lnkDelete.Enabled = true;
            BindTemplates();
        }
    }
    public void BindTemplates()
    {
        try
        {
            objTemplates = new clsTemplates();

            //lblHeading.Text = " Template List";
            //imgHeader.ImageUrl = "../images/Template_list_Big.png";
            txtSearch.Text = string.Empty;
            fvTemplates.ChangeMode(FormViewMode.ReadOnly);
            fvTemplates.DataSource = null;
            fvTemplates.DataBind();


            objTemplates.PageIndex = pgTemplates.CurrentPage + 1;
            objTemplates.PageSize = pgTemplates.PageSize;
            objTemplates.SortExpression = "TemplateId";
            objTemplates.SortOrder = "desc";
            objTemplates.Searchkey = txtSearch.Text;
            DataSet ds = objTemplates.GetAllTemplates();
            if (ds.Tables[0].Rows.Count != 0)
            {
                pgTemplates.Total = Convert.ToInt32(ds.Tables[0].Rows[0]["Total"]);

                dlTemplates.DataSource = ds;
                dlTemplates.DataBind();

                pgTemplates.Visible = true;
                lblNodata.Text = string.Empty;
                lnkDelete.Enabled = true;

                lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlTemplates.ClientID + "');";
                upDetails.Update();
                upMenu.Update();

            }
            else
            {
                dlTemplates.DataSource = null;
                dlTemplates.DataBind();
                lblNodata.Text = "No Templates Found";
                pgTemplates.Visible = false;
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
            }

        }
        catch
        {
        }


    } // binding the datalist 
    private void BindForm(int iTemplateId, FormViewMode fvMode)
    {
        objTemplates = new clsTemplates();
        txtSearch.Text = string.Empty;

        //lblHeading.Text = "Email Template Details";
        //imgHeader.ImageUrl = "../images/Add templates_Big.png";

        fvTemplates.ChangeMode(fvMode);
        objTemplates.TemplateId = iTemplateId;
        fvTemplates.DataSource = objTemplates.GetTemplatesDetails();
        fvTemplates.DataBind();
        dlTemplates.DataSource = null;
        dlTemplates.DataBind();
        pgTemplates.Visible = false;
    }
    protected void fvTemplates_DataBound(object sender, EventArgs e)
    {
        objTemplates = new clsTemplates();
        if (fvTemplates.CurrentMode == FormViewMode.Insert | fvTemplates.CurrentMode == FormViewMode.Edit)
        {
            DropDownList ddlTemplateType = (DropDownList)fvTemplates.FindControl("ddlTemplateType");
            RadioButton rbtnYes = (RadioButton)fvTemplates.FindControl("rbtnYes");
            RadioButton rbtnNo = (RadioButton)fvTemplates.FindControl("rbtnNo");


            ddlTemplateType.DataSource = objTemplates.GetTemplatesTypes();
            ddlTemplateType.DataBind();
            ddlTemplateType.Items.Insert(0, new ListItem("--Select--", "-1"));

            DropDownList ddlFields = (DropDownList)fvTemplates.FindControl("ddlFields");
            ddlFields.Items.Insert(0, new ListItem("--Select--", "-1"));
            if (fvTemplates.CurrentMode == FormViewMode.Edit)
            {
                rbtnYes.Checked = Convert.ToBoolean(fvTemplates.DataKey["IsCurrent"]);
                rbtnNo.Checked = !(Convert.ToBoolean(fvTemplates.DataKey["IsCurrent"]));
                ddlTemplateType.SelectedIndex = ddlTemplateType.Items.IndexOf(ddlTemplateType.Items.FindByValue(fvTemplates.DataKey["TemplateTypeId"].ToString()));
                ddlTemplateType.Enabled = false;
                if (ddlTemplateType.SelectedValue == "1" | ddlTemplateType.SelectedValue == "2" || ddlTemplateType.SelectedValue == "7")
                    rbtnYes.Enabled = rbtnNo.Enabled = false;
                else
                    rbtnYes.Enabled = rbtnNo.Enabled = true;
                objTemplates.TemplateTypeId = Convert.ToInt32(ddlTemplateType.SelectedValue);
                ddlFields.DataSource = objTemplates.GetFields();
                ddlFields.DataBind();
                ddlFields.Items.Insert(0, new ListItem("--Select--", "-1"));

            }
            Button btnAddFields = (Button)fvTemplates.FindControl("btnAddFields");
            btnAddFields.OnClientClick = "return CursorPosition('" + btnAddFields.ClientID + "');";

        }

    }
    protected void ddlTemplateType_SelectedIndexChanged(object sender, EventArgs e)
    {
        objTemplates = new clsTemplates();
        DropDownList ddlTemplateType = (DropDownList)sender;
        RadioButton rbtnYes = (RadioButton)fvTemplates.FindControl("rbtnYes");
        RadioButton rbtnNo = (RadioButton)fvTemplates.FindControl("rbtnNo");
        if (ddlTemplateType.SelectedIndex != -1)
        {
            DropDownList ddlFields = (DropDownList)fvTemplates.FindControl("ddlFields");
            objTemplates.TemplateTypeId = Convert.ToInt32(ddlTemplateType.SelectedValue);
            ddlFields.DataSource = objTemplates.GetFields();
            ddlFields.DataBind();
            ddlFields.Items.Insert(0, new ListItem("--Select--", "-1"));

            if (ddlTemplateType.SelectedValue == "1" | ddlTemplateType.SelectedValue == "2" || ddlTemplateType.SelectedValue == "7")
                rbtnYes.Enabled = rbtnNo.Enabled = false;
            else
                rbtnYes.Enabled = rbtnNo.Enabled = true;

        }
        else
            rbtnYes.Enabled = rbtnNo.Enabled = true;

    }
    protected void lnkNew_Click(object sender, EventArgs e)
    {
        fvTemplates.ChangeMode(FormViewMode.Insert);
        txtSearch.Text = string.Empty;
        //lblHeading.Text = "Email Template Details";
        //imgHeader.ImageUrl = "../images/Add templates_Big.png";
        lblNodata.Text = string.Empty;

        dlTemplates.DataSource = null;
        dlTemplates.DataBind();
        pgTemplates.Visible = false;
        upDetails.Update();
    }
    protected void fvTemplates_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        AjaxControlToolkit.HTMLEditor.Editor txtBody = (AjaxControlToolkit.HTMLEditor.Editor)fvTemplates.FindControl("txtBody");
        switch (e.CommandName)
        {

            case "_AddFields":
                DropDownList ddlFields = (DropDownList)fvTemplates.FindControl("ddlFields");
                if (ddlFields.SelectedValue != "-1")
                {
                    txtBody.Content = txtBody.Content + ddlFields.SelectedValue;
                }

                break;
            case "_Submit":

                DataTable dtm = (DataTable)ViewState["Permission"];
                if (dtm.Rows.Count > 0)
                {
                    if (dtm.Rows[0]["IsCreate"].ToBoolean() != true || dtm.Rows[0]["IsUpdate"].ToBoolean() != true)
                    {
                        Button btnSubmit = (Button)fvTemplates.FindControl("btnSubmit");
                        if (btnSubmit != null)
                        {
                            btnSubmit.Enabled = false;
                            btnSubmit.OnClientClick = "return false;";
                            break; 
                        }
                    }
                }

                if (txtBody.Content == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Message Content is empty')", true);
                    break;
                }
                objTemplates = new clsTemplates();
                objXML = new clsXML();
                DropDownList ddlTemplateType = (DropDownList)fvTemplates.FindControl("ddlTemplateType");
                TextBox txtTemplateTitle = (TextBox)fvTemplates.FindControl("txtTemplateTitle");
                TextBox txtSubject = (TextBox)fvTemplates.FindControl("txtSubject");
                RadioButton rbtnYes = (RadioButton)fvTemplates.FindControl("rbtnYes");
                objTemplates.TemplateId = (fvTemplates.CurrentMode == FormViewMode.Insert ? -1 : Convert.ToInt32(fvTemplates.DataKey["TemplateId"]));
                objTemplates.TemplateTypeId = Convert.ToInt32(ddlTemplateType.SelectedValue);
                objTemplates.Subject = txtSubject.Text.Trim();
                objTemplates.Title = txtTemplateTitle.Text.Trim();
                objTemplates.IsCurrent = rbtnYes.Checked;
                objTemplates.Message = txtBody.Content;

                bool IsEdit = this.fvTemplates.CurrentMode == FormViewMode.Edit;

                if (this.objTemplates.IsDuplicate(IsEdit))
                {
                    this.msgConfirm.InformationalMessage("Duplicate template");
                    this.mpeMessage.Show();
                    return;
                }
                

                int iTemplateId = objTemplates.InsertUpdateTemplate((fvTemplates.CurrentMode == FormViewMode.Insert ? true : false));
                if (iTemplateId != 0)
                {
                    objXML.AttributeId = iTemplateId;
                    objXML.Message = txtBody.Content;

                    // **** XML file writing Section ********//
                    if (fvTemplates.CurrentMode == FormViewMode.Insert)
                        objXML.InsertTemplateXml();
                    else
                        objXML.UpdateTemplateXml();
                    // **** ************************ ********//

                    msgConfirm.InformationalMessage("Templates " + (fvTemplates.CurrentMode == FormViewMode.Insert ? "saved" : "updated") + " successfully");
                    BindTemplates();
                    mpeMessage.Show();

                }

                break;
            case "_Reset":
                BindTemplates();
                break;
        }
    }
    protected void dlTemplates_ItemCommand(object source, DataListCommandEventArgs e)
    {
        objTemplates = new clsTemplates();
        switch (e.CommandName)
        {
            case "_EditTemplate":


                BindForm(Convert.ToInt32(e.CommandArgument), FormViewMode.Edit);

                break;
        }
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        BindTemplates();
        upDetails.Update();
    }
    public string GetXmlMessage(object oTemplateId)
    {
        string sMessage = string.Empty;
        if (oTemplateId != null)
        {
            objXML = new clsXML();
            objXML.AttributeId = Convert.ToInt32(oTemplateId);
            sMessage = objXML.GetTemplateMessage();
        }
        return sMessage;
    }

    protected void btnGo_Click(object sender, ImageClickEventArgs e)
    {
        pgTemplates.CurrentPage = 0;
        BindTemplates();
        this.RegisterAutoComplete();
        upDetails.Update();
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        objTemplates = new clsTemplates();
        objXML = new clsXML();
        int idelCout = 0;
        string sTemplates = string.Empty;

        foreach (DataListItem item in dlTemplates.Items)
        {
            CheckBox chkTemplates = (CheckBox)item.FindControl("chkTemplates");

            if (chkTemplates == null)
                continue;

            if (chkTemplates.Checked)
            {
                objTemplates.TemplateId = Convert.ToInt32(dlTemplates.DataKeys[item.ItemIndex]);

                if (!objTemplates.DeleteTemplates())
                {
                    sTemplates += "," + Convert.ToInt32(dlTemplates.DataKeys[item.ItemIndex]);
                    continue;
                }
                else
                {
                    objXML.AttributeId = Convert.ToInt32(dlTemplates.DataKeys[item.ItemIndex]);
                    objXML.DeleteTemplateXml();
                    idelCout++;
                }

            }
        }


        if (sTemplates == string.Empty)

            msgConfirm.InformationalMessage("Templates(s) deleted successfully.");
        else

            msgConfirm.WarningMessage((idelCout == 0 ? "" : Convert.ToString(idelCout) + " Template(s) deleted successfully.</br>") + "Cannot delete some of the selected templates,as they are predefined");

        BindTemplates();
        upDetails.Update();
        mpeMessage.Show();


    }

    private void RegisterAutoComplete()
    {
        objUserMaster = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Templates,objUserMaster.GetCompanyId());
    }

}
