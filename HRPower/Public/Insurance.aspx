﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Insurance.aspx.cs" Inherits="Public_Insurance" Title="Insurance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />
    <div id='cssmenu'>
        <ul>
            <li><a href="Passport.aspx">
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
            <li><a href="Visa.aspx">
                <asp:Literal ID="Literal30" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
            <li><a href="DrivingLicence.aspx">
                <asp:Literal ID="Literal31" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
            <li><a href="Qualification.aspx">
                <asp:Literal ID="Literal32" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
            <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1">
                <asp:Literal ID="Literal33" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
            <li id="liLabour" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=2">
                <asp:Literal ID="Literal34" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
            <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3">
                <asp:Literal ID="Literal35" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
            <li><a href="InsuranceCard.aspx">
                <asp:Literal ID="Literal36" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
            <li><a href="TradeLicense.aspx">
                <asp:Literal ID="Literal37" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
            <li><a href="LeaseAgreement.aspx">
                <asp:Literal ID="Literal38" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
            <li class="selected"><a href="Insurance.aspx">
                <asp:Literal ID="Literal39" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
            <li><a href="Documents.aspx">
                <asp:Literal ID="Literal40" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
            <li><a href="ViewDocument.aspx">
                <asp:Literal ID="Literal41" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="upAddInsurance" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAddInsurance" runat="server" style="width: 100%; min-height: 550px; height: auto;
                float: left; padding-top: 20px;">
                <asp:HiddenField ID="hfInsuranceID" runat="server" />
                <div id="divAddInsuranceDataPart" runat="server">
                    <div style="width: 100%; height: 40px; float: left; margin-top: 20px;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Company--%>
                            <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:DocumentsCommon,Company%>'></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:DropDownList ID="ddlCompany" runat="server" Width="207px" BackColor="#FFFFEC"
                                OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" CssClass="error" meta:resourcekey="Pleaseselectacompany"
                                Display="Dynamic" ControlToValidate="ddlCompany" ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Asset--%><asp:Literal ID="Literal1" runat="server" meta:resourcekey="Asset"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:UpdatePanel ID="upAsset" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlAsset" runat="server" Width="207px" BackColor="#FFFFEC">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:RequiredFieldValidator ID="rfvddlAsset" runat="server" CssClass="error" meta:resourcekey="PleaseselectanAsset"
                                Display="Dynamic" ControlToValidate="ddlAsset" ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Policy Number--%><asp:Literal ID="Literal2" runat="server" meta:resourcekey="PolicyNumber"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtPolicyNumber" runat="server" CssClass="textbox_mandatory" Width="200px"
                                Style="margin-right: 10px" BackColor="#FFFFEC"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtPolicyNumber" runat="server" meta:resourcekey="PleaseenterPolicyNumber"
                                ControlToValidate="txtPolicyNumber" Display="Dynamic" ValidationGroup="Submit"
                                CssClass="error"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Policy Name--%><asp:Literal ID="Literal3" runat="server" meta:resourcekey="PolicyName"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtPolicyName" runat="server" CssClass="textbox_mandatory" Width="200px"
                                Style="margin-right: 10px" BackColor="#FFFFEC"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtPolicyName" runat="server" meta:resourcekey="PleaseenterPolicyName"
                                ControlToValidate="txtPolicyName" Display="Dynamic" ValidationGroup="Submit"
                                CssClass="error"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Insurance Company--%><asp:Literal ID="Literal4" runat="server" meta:resourcekey="InsuranceCompany"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <div style="float: left;">
                                <asp:UpdatePanel ID="upInsuranceCompany" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlInsuranceCompany" runat="server" Width="207px" BackColor="#FFFFEC">
                                        </asp:DropDownList>
                                        <asp:Button ID="imgInsuranceCompany" runat="server" CausesValidation="False" CssClass="referencebutton"
                                            Text="..." OnClick="imgInsuranceCompany_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div style="float: left;">
                                <asp:RequiredFieldValidator ID="rfvddlInsuranceCompany" runat="server" CssClass="error"
                                    meta:resourcekey="PleaseselectaInsuranceCompany" Display="Dynamic" ControlToValidate="ddlInsuranceCompany"
                                    ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator></div>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Policy Amount--%><asp:Literal ID="Literal5" runat="server" meta:resourcekey="PolicyAmount"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtPolicyAmount" runat="server" CssClass="textbox" Width="200px"
                                Style="margin-right: 10px" BackColor="#FFFFEC"></asp:TextBox>
                            <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtPolicyAmountExtender" runat="server"
                                FilterType="Numbers,Custom" TargetControlID="txtPolicyAmount" ValidChars=".">
                            </AjaxControlToolkit:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvtxtPolicyAmount" runat="server" meta:resourcekey="PleaseenterPolicyAmount"
                                ControlToValidate="txtPolicyAmount" Display="Dynamic" ValidationGroup="Submit"
                                CssClass="error"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%-- Issue Date--%>
                            <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtIssuedate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                Style="margin-right: 10px" BackColor="#FFFFEC"></asp:TextBox>
                            <asp:ImageButton ID="ibtnIssuedate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                CausesValidation="false" CssClass="imagebutton" />
                            <AjaxControlToolkit:CalendarExtender ID="extenderIssuedate" runat="server" TargetControlID="txtIssuedate"
                                PopupButtonID="ibtnIssuedate" Format="dd/MM/yyyy" Enabled="true">
                            </AjaxControlToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rfvtxtIssuedate" runat="server" meta:resourcekey="PleaseenterIssueDate"
                                ControlToValidate="txtIssuedate" Display="Dynamic" ValidationGroup="Submit" CssClass="error"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvtxtIssuedate" runat="server" ClientValidationFunction="CheckFuturedate2"
                                CssClass="error" ControlToValidate="txtIssuedate" Display="Dynamic" ValidationGroup="Submit"></asp:CustomValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%-- Expiry date--%><asp:Literal ID="Literal7" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtExpirydate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                Style="margin-right: 10px" BackColor="#FFFFEC"></asp:TextBox>
                            <asp:ImageButton ID="ibtnExpirydate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                CssClass="imagebutton" CausesValidation="false" />
                            <AjaxControlToolkit:CalendarExtender ID="extendertExpirydate" runat="server" TargetControlID="txtExpirydate"
                                PopupButtonID="ibtnExpirydate" Format="dd/MM/yyyy">
                            </AjaxControlToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rfvtxtExpirydate" runat="server" meta:resourcekey="PleaseenterExpirydate"
                                ControlToValidate="txtExpirydate" Display="Dynamic" ValidationGroup="Submit"
                                CssClass="error"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvtxtExpirydate" runat="server" ClientValidationFunction="CheckValidDateAndExpiry"
                                CssClass="error" ControlToValidate="txtExpirydate" Display="Dynamic" ValidationGroup="Submit"></asp:CustomValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 80px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Remarks--%><asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ControlsCommon,Remarks%>'></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 80px; float: left;">
                            <asp:TextBox ID="txtRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                                onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="300px"
                                TextMode="MultiLine" Height="76px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Status--%><asp:Literal ID="Literal9" runat="server" meta:resourcekey="Status"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:Label ID="lblStatusAdd" runat="server" Text="New"></asp:Label>
                        </div>
                    </div>
                </div>
                <div id="divAddInsuranceDocumentPart" runat="server" style="width: 100%; height: auto;
                    float: left; margin-top: 20px;">
                    <fieldset style="width: 98%; padding: 1%;">
                        <legend>
                            <%--Add Documents--%>
                            <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:DocumentsCommon,AddDocuments%>'></asp:Literal>
                        </legend>
                        <div style="width: 100%; height: 30px; float: left; margin-top: 10px;">
                            <div style="width: 40%; height: 30px; float: left;">
                                <%-- Document Name--%>
                                <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                            </div>
                            <div style="width: 40%; height: 30px; float: left;">
                                <%-- Choose file..--%>
                                <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:DocumentsCommon,ChooseFile%>'></asp:Literal>
                            </div>
                            <div style="width: 15%; height: 30px; float: left;">
                            </div>
                        </div>
                        <div style="width: 100%; height: 40px; float: left;">
                            <div style="width: 40%; height: 40px; float: left;">
                                <asp:TextBox ID="txtDocname" runat="server" CssClass="textbox" MaxLength="30" Width="200px"></asp:TextBox>
                            </div>
                            <div style="width: 40%; height: 40px; float: left;">
                                <AjaxControlToolkit:AsyncFileUpload ID="fuInsurance" runat="server" CompleteBackColor="White"
                                    ErrorBackColor="White" OnUploadedComplete="fuInsurance_UploadedComplete" PersistFile="True"
                                    Visible="true" Width="250px" />
                            </div>
                            <div style="width: 15%; height: 40px; float: left;">
                                <asp:LinkButton ID="btnattachdoc" runat="server" Enabled="true" OnClick="btnattachdoc_Click"
                                    Text='<%$Resources:DocumentsCommon,Addtolist%>' ValidationGroup="VG1" ForeColor="#33a3d5">
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div style="width: 100%; height: 20px; float: left; margin-top: 5px;">
                            <div style="width: 40%; height: 20px; float: left;">
                                <asp:RequiredFieldValidator ID="rfvDocname" runat="server" ControlToValidate="txtDocname"
                                    CssClass="error" Display="Dynamic" ErrorMessage="*" ValidationGroup="VG1"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvInsurancedocnameduplicate" runat="server" ClientValidationFunction="validateInsurancedocnameduplicate"
                                    ControlToValidate="txtDocname" Display="Dynamic" ErrorMessage='<%$Resources:DocumentsCommon,Duplicatedocumentname%>'
                                    ValidationGroup="VG1"></asp:CustomValidator>
                            </div>
                            <div style="width: 40%; height: 20px; float: left;">
                                <asp:CustomValidator ID="cvAttachment" runat="server" ClientValidationFunction="validateInsuranceAttachment"
                                    CssClass="error" ErrorMessage='<%$Resources:DocumentsCommon,Pleaseattachfile%>'
                                    ValidateEmptyText="True" ValidationGroup="VG1"></asp:CustomValidator>
                            </div>
                            <div style="width: 15%; height: 20px; float: left;">
                            </div>
                        </div>
                    </fieldset>
                    <div id="divListDoc" runat="server" class="container_content" style="display: inline;
                        width: 100%; margin-top: 20px; height: auto; overflow: auto">
                        <asp:UpdatePanel ID="upListDoc" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:DataList ID="dlListDoc" runat="server" DataKeyField="Node" GridLines="Horizontal"
                                    OnItemCommand="dlListDoc_ItemCommand" Width="100%">
                                    <HeaderStyle CssClass="datalistheader" />
                                    <HeaderTemplate>
                                        <div style="width: 100%; height: 30px; float: left;">
                                            <div style="width: 40%; height: 30px; float: left;">
                                                <%-- Document Name--%>
                                                <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                            </div>
                                            <div style="width: 40%; height: 30px; float: left;">
                                                <%-- Document Name--%>
                                                <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                            </div>
                                            <div style="width: 15%; height: 30px; float: left;">
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="width: 100%; height: 30px; padding-top: 10px; float: left;">
                                            <div style="width: 40%; height: 30px; float: left;">
                                                <asp:Label ID="lblDocname" runat="server" Text='<%# Eval("Docname")%>' Width="150px"></asp:Label>
                                            </div>
                                            <div style="width: 40%; height: 30px; float: left;">
                                                <asp:Label ID="lblFilename" runat="server" Text='<%# Eval("Filename")%>' Width="150px"></asp:Label>
                                            </div>
                                            <div style="width: 15%; height: 30px; float: left;">
                                                <asp:ImageButton ID="btnRemove" runat="server" CommandArgument='<% # Eval("Node") %>'
                                                    CommandName="REMOVE_ATTACHMENT" SkinID="DeleteIconDatalist" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnattachdoc" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div style="width: 100%; height: 30px; float: left; text-align: right; margin-top: 20px;
                    margin-bottom: 20px;">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                        ToolTip='<%$Resources:ControlsCommon,Submit%>' Width="75px" ValidationGroup="Submit"
                        OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>'
                        ToolTip='<%$Resources:ControlsCommon,Cancel%>' Width="75px" Style="margin-left: 10px;
                        margin-right: 35%;" OnClick="btnCancel_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upSingleViewInsurance" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSingleViewInsurance" runat="server" style="width: 100%; min-height: 550px;
                height: auto; float: left; padding-top: 20px;">
                <div style="width: 100%; height: 40px; float: left; margin-top: 20px;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Company--%><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:DocumentsCommon,Company%>'></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 50%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblCompany" runat="server"></asp:Label>
                    </div>
                    <%--<div class="trRight" style="width: 20%; height: 40px; float: left; font-weight: bold;
                        color: #FF3300;">
                        <asp:Label ID="lblRenewed" runat="server"></asp:Label>
                    </div>--%>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Asset--%><asp:Literal ID="Literal17" runat="server" meta:resourcekey="Asset"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblAsset" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Policy Number--%><asp:Literal ID="Literal18" runat="server" meta:resourcekey="PolicyNumber"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblPolicyNumber" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Policy Name--%><asp:Literal ID="Literal19" runat="server" meta:resourcekey="PolicyName"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        <div style="width: 100%;">
                            :
                            <asp:Label ID="lblPolicyName" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Insurance Company--%><asp:Literal ID="Literal20" runat="server" meta:resourcekey="InsuranceCompany"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        <div style="width: 100%;">
                            :
                            <asp:Label ID="lblInsuranceCompany" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Policy Amount--%><asp:Literal ID="Literal21" runat="server" meta:resourcekey="PolicyAmount"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblPolicyAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Issue Date--%>
                        <asp:Literal ID="Literal22" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblIssueDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Expiry Date--%>
                        <asp:Literal ID="Literal23" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblExpiryDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; min-height: 55px; height: auto; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Remarks--%><asp:Literal ID="Literal24" runat="server" Text='<%$Resources:ControlsCommon,Remarks%>'></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; min-height: 55px; height: auto; float: left;
                        word-break: break-all;">
                        :
                        <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Is Renewed--%><asp:Literal ID="Literal42" runat="server" Text='<%$Resources:DocumentsCommon,IsRenewed%>'></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        <div style="width: 100%;">
                            :
                            <asp:Label ID="lblRenewed" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Status--%><asp:Literal ID="Literal25" runat="server" meta:resourcekey="Status"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblStatus" runat="server" Text="New"></asp:Label>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upViewInsurance" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divViewInsurance" runat="server" style="width: 100%; float: left; padding-top: 20px;">
                <asp:DataList ID="dlInsurance" runat="server" Width="100%" DataKeyField="InsuranceID"
                    CssClass="labeltext" OnItemCommand="dlInsurance_ItemCommand" OnItemDataBound="dlInsurance_ItemDataBound">
                    <ItemStyle CssClass="labeltext" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <div style="width: 100%; height: 30px; float: left;">
                            <div style="width: 5%; height: 30px; float: left; margin-left: 3.3%">
                                <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkInsurance')" />
                            </div>
                            <div style="width: 90%; height: 30px; float: left;">
                                <%--Select All--%>
                                <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="width: 100%; height: 50px; float: left;">
                            <div class="trLeft" style="width: 5%; height: 30px; float: left; margin-left: 2%">
                                <asp:CheckBox ID="chkInsurance" runat="server" /></div>
                            <div style="width: 80%; height: 30px; float: left;">
                                <asp:LinkButton ID="lnkInsurance" runat="server" CssClass="listHeader bold" CommandArgument='<%# Eval("InsuranceID") %>'
                                    meta:resourcekey="View" CommandName="_View" Text='<%# Eval("CompanyName")%>'
                                    CausesValidation="False">
                                </asp:LinkButton>
                                <%# Eval("Header")%>
                            </div>
                            <div style="width: 4%; height: 30px; float: left;">
                                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("InsuranceID") %>'
                                    CommandName="_Edit" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/edit.png"
                                    ToolTip='<%$Resources:ControlsCommon,Edit%>'></asp:ImageButton>
                            </div>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Policy Name--%><asp:Literal ID="Literal27" runat="server" meta:resourcekey="PolicyName"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 48%; height: 30px; float: left;">
                            :
                            <%# Eval("PolicyName")%>
                        </div>
                        <div class="trRight" style="width: 12%; height: 30px; float: left; font-weight: bold;
                            color: #FF3300;">
                            <%# Eval("Renewed")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Asset--%><asp:Literal ID="Literal28" runat="server" meta:resourcekey="Asset"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("Asset")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Policy Amount--%><asp:Literal ID="Literal29" runat="server" meta:resourcekey="PolicyAmount"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("PolicyAmount")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Issue Date--%>
                            <asp:Literal ID="Literal30" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("IssueDate")%>
                        </div>
                        <asp:HiddenField ID="hfIsRenewed" runat="server" Value='<%# Eval("IsRenewed")%>' />
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="InsurancePager" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upMessage" runat="server">
        <ContentTemplate>
            <div>
                <div style="display: none">
                    <asp:Button ID="btnsubmit1" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit1"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPrint" runat="server">
        <ContentTemplate>
            <div id="divPrint" runat="server" style="display: none">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upNoInformationFound" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divNoInformationFound" runat="server" style="display: none;  text-align: center; margin-top: 5px">
                <asp:Label ID="lblNoRecord" runat="server" CssClass="error" Text='<%$Resources:ControlsCommon,NoRecordFound%>'></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server" meta:resourcekey="SearchBy"></asp:TextBox>
                <br />
                <div id="searchimg">
                    <a href="">
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/search.png " OnClick="btnSearch_Click"
                            runat="server" />
                    </a>
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:Image ID="imgAdd" ImageUrl="~/images/Add transfer request.png" runat="server" />
                </div>
                <div class="name">
                    <h5><asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" meta:resourcekey="AddInsurance"> 
		
	<%--Add Insurance--%></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:Image ID="imgView" ImageUrl="~/images/View transfer request.png" runat="server" />
                </div>
                </a>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click" meta:resourcekey="ViewInsurance"> 
		
	<%--View Insurance--%></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:Image ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                     <h5><asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text='<%$Resources:ControlsCommon,Delete%>'>
		
	<%--Delete--%></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5><asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click" Text='<%$Resources:ControlsCommon,Print%>'> 
		
	              <%--Print--%></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                   <h5>  <asp:LinkButton ID="lnkEmail" runat="server" OnClick="lnkEmail_Click" Text='<%$Resources:ControlsCommon,Email%>'> 
	            <%--Email--%></asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
