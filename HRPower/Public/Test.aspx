﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Public_Test" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">
<div id="divSalaryStructure" runat="server" style="width: 91%; border: solid 1px #25c4ec;
            border-radius: 8px; margin-left: 37px; margin-bottom:20px; display: block; ">
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td>
                        <div>
                            <div style="display: none">
                                <asp:Button ID="btnProxy" runat="server" Text="submit" />
                            </div>
                            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                                BorderStyle="None">
                                <asp:UpdatePanel ID="upnlMessage" runat="server">
                                    <ContentTemplate>
                                        <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                                Drag="true">
                            </AjaxControlToolkit:ModalPopupExtender>
                        </div>
                        <asp:UpdatePanel ID="upnlSalaryStructure" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="width: 100%;">
                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="padding: 5px;">
                                                <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="100px" class="firstTrLeft">
                                                            Company
                                                        </td>
                                                        <td width="120px">
                                                            <asp:DropDownList ID="ddlCompany" runat="server" Width="200px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="CompanyID" DataTextField="CompanyName">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="9%">
                                                            &nbsp;
                                                        </td>
                                                        <td width="150px" class="firstTrLeft">
                                                            Payment Mode
                                                        </td>
                                                        <td class="firstTrRight">
                                                            <asp:DropDownList ID="ddlPaymentMode" runat="server" Width="130px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="PaymentModeID" DataTextField="Description" Enabled="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="firstTrLeft">
                                                            Employee
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlEmployee" runat="server" Width="200px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="EmployeeID" DataTextField="EmployeeName">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="ddlEmployee"
                                                                CssClass="error" Display="Dynamic" ErrorMessage="Please select employee" ValidationGroup="Employee"
                                                                InitialValue="-1"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="firstTrLeft">
                                                            Payment Calculation
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlPaymentCalculation" runat="server" Width="130px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="PayCalculationTypeID" DataTextField="Description" Enabled="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="firstTrLeft">
                                                            Currency
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlCurrency" runat="server" Width="200px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="CurrencyID" DataTextField="Description" Enabled="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="firstTrLeft">
                                                            Salary Process Day
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSalaryProcessDay" runat="server" CssClass="dropdownlist_mandatory"
                                                                Width="50px" Enabled="False">
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                                <asp:ListItem>5</asp:ListItem>
                                                                <asp:ListItem>6</asp:ListItem>
                                                                <asp:ListItem>7</asp:ListItem>
                                                                <asp:ListItem>8</asp:ListItem>
                                                                <asp:ListItem>9</asp:ListItem>
                                                                <asp:ListItem>10</asp:ListItem>
                                                                <asp:ListItem>11</asp:ListItem>
                                                                <asp:ListItem>12</asp:ListItem>
                                                                <asp:ListItem>13</asp:ListItem>
                                                                <asp:ListItem>14</asp:ListItem>
                                                                <asp:ListItem>15</asp:ListItem>
                                                                <asp:ListItem>16</asp:ListItem>
                                                                <asp:ListItem>17</asp:ListItem>
                                                                <asp:ListItem>18</asp:ListItem>
                                                                <asp:ListItem>19</asp:ListItem>
                                                                <asp:ListItem>20</asp:ListItem>
                                                                <asp:ListItem>21</asp:ListItem>
                                                                <asp:ListItem>22</asp:ListItem>
                                                                <asp:ListItem>23</asp:ListItem>
                                                                <asp:ListItem>24</asp:ListItem>
                                                                <asp:ListItem>25</asp:ListItem>
                                                                <asp:ListItem>26</asp:ListItem>
                                                                <asp:ListItem>27</asp:ListItem>
                                                                <asp:ListItem>28</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                                                            <asp:HiddenField ID="hdSalaryID" runat="server" Visible="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" class="firstTrRight" valign="top" style="height:286px;">
                                                            <div id="div1" class="TabbedPanels" style="font-size: 14px; width: 100%; float: left">
                                                                <ul class="TabbedPanelsTabGroup">
                                                                    <li class="TabbedPanelsTabSelected" id="pnlpTab1" onclick="SetSalaryTab(this.id);">General</li>
                                                                    <li class="TabbedPanelsTab" id="pnlpTab2" onclick="SetSalaryTab(this.id);">Other Remuneration</li>
                                                                </ul>
                                                                <div class="TabbedPanelsContent" id="divpTab1" style="font-size: 13px; width: 100%;
                                                                    float: left; height: 200px">
                                                                    <div style="width: 100%; float: left">
                                                                        <div class="container_head" style="margin-top: 5px;">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td width="200px" align="left" class="labeltext">
                                                                                        Particulars
                                                                                    </td>
                                                                                    <td align="left" width="150px" class="labeltext">
                                                                                        Amount
                                                                                    </td>
                                                                                    <td width="150px" align="left" class="labeltext">
                                                                                        Deduction Policy
                                                                                    </td>
                                                                                    <td width="25" align="left">
                                                                                        <asp:Button ID="btnAllowances" runat="server" CssClass="referencebutton" Text="..."
                                                                                            ToolTip="Add more particulars" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div class="container_content" style="height: 200px; overflow: auto; border-top: 3px solid rgb(199, 194, 194);
                                                                            background-color:rgb(238, 243, 243); width: 100%;">
                                                                            <asp:UpdatePanel ID="upnlDesignationAllowance" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:DataList ID="dlDesignationAllowances" runat="server" Width="96%" DataKeyField="SalaryDetailID"
                                                                                        CssClass="labeltext"
                                                                                       >
                                                                                        <ItemTemplate>
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td width="200">
                                                                                                        <asp:DropDownList ID="ddlParticulars" runat="server" DataTextField="Description"
                                                                                                            AutoPostBack="true" CssClass="dropdownlist" Width="200" DataValueField="AddDedID"
                                                                                                            onchange="chkDuplicateParticulars(this)">
                                                                                                        </asp:DropDownList>
                                                                                                        <asp:HiddenField ID="hdParticulars" runat="server" Value='<%# Eval("AddDedID") %>' />
                                                                                                    </td>
                                                                                                    <td width="145" align="left">
                                                                                                        <asp:TextBox ID="txtAmount" runat="server" CssClass="textbox" Style="text-align: right;"
                                                                                                            onblur="CalculateSalaryStructure(this.id)" onchange="setVisibility(this);" MaxLength="12"
                                                                                                            Text='<% # Eval("Amount") %>'></asp:TextBox>
                                                                                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterMode="ValidChars"
                                                                                                            FilterType="Numbers,Custom" TargetControlID="txtAmount" ValidChars=".">
                                                                                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                                                        <asp:HiddenField ID="hdtxtAmount" runat="server" Visible="false" Value='<% # Eval("Amount") %>' />
                                                                                                    </td>
                                                                                                    <td width="130px" align="left">
                                                                                                        <asp:DropDownList ID="ddlDeductionPolicy" Width="160px" DataTextField="PolicyName"
                                                                                                            DataValueField="DeductionPolicyID" CssClass="dropdownlist" runat="server" AutoPostBack="True"
                                                                                                            onchange="setVisibility2(this);">
                                                                                                        </asp:DropDownList>
                                                                                                        <asp:HiddenField ID="hdDeductionPolicy" runat="server" Value='<%# Eval("DeductionPolicyID") %>' />
                                                                                                    </td>
                                                                                                    <td width="15" align="right">
                                                                                                        <asp:ImageButton ID="btnRemove" runat="server" ToolTip="Delete Particular" SkinID="DeleteIconDatalist"
                                                                                                            CommandArgument='<% # Eval("AddDedID") %>' CommandName="REMOVE_ALLOWANCE" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                    <asp:CustomValidator ID="cvAllowances" runat="server" ClientValidationFunction="validateAllowanceDeduction"
                                                                                        ValidationGroup="Employee" Display="Dynamic" ErrorMessage="Please select particulars."></asp:CustomValidator>
                                                                                    <asp:CustomValidator ID="cvAmount" runat="server" ClientValidationFunction="validateAmount"
                                                                                        ValidationGroup="Employee" Display="Dynamic" ErrorMessage="Please enter the amount,or select deduction policy"></asp:CustomValidator>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="TabbedPanelsContent" id="divpTab2" style="display: none; height: 200px;">
                                                                    <div style="width: 100%; float: left">
                                                                        <div class="container_head" style="margin-top: 5px;">
                                                                            <table width="100%" style="font-size: 11px">
                                                                                <tr>
                                                                                    <td width="200px" align="left" class="labeltext">
                                                                                        Particulars
                                                                                    </td>
                                                                                    <td align="left" width="150px" class="labeltext">
                                                                                        Amount
                                                                                    </td>
                                                                                    <td width="25" align="left">
                                                                                        <asp:Button ID="btnOtherRemuneration" runat="server" CssClass="referencebutton" Text="..."
                                                                                            ToolTip="Add more particulars" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div class="container_content" style="height: 200px; overflow: auto; border-top: 3px solid rgb(199, 194, 194);
                                                                            background-color:rgb(238, 243, 243); width: 100%;">
                                                                            <asp:UpdatePanel ID="upnlOtherRemuneration" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:DataList ID="dlOtherRemuneration" runat="server" Width="96%" DataKeyField="SalaryID"
                                                                                        CssClass="labeltext">
                                                                                        <ItemTemplate>
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td width="200">
                                                                                                        <asp:DropDownList ID="ddlRemunerationParticulars" runat="server" AutoPostBack="true"
                                                                                                            DataTextField="OtherRemuneration" DataValueField="OtherRemunerationID" CssClass="dropdownlist"
                                                                                                            Width="200" onchange="chkDuplicateRemunerationParticulars(this)">
                                                                                                        </asp:DropDownList>
                                                                                                        <asp:HiddenField ID="hdRemunerationParticulars" runat="server" Value='<%# Eval("OtherRemunerationID") %>' />
                                                                                                    </td>
                                                                                                    <td width="145" align="left">
                                                                                                        <asp:TextBox ID="txtRemunerationAmount" runat="server" CssClass="textbox" Style="text-align: right;"
                                                                                                            Text='<% # Eval("Amount") %>' MaxLength="12"></asp:TextBox>
                                                                                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtRemunerationAmount" runat="server"
                                                                                                            FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtRemunerationAmount"
                                                                                                            ValidChars=".">
                                                                                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                                                        <asp:HiddenField ID="hdtxtRemunerationAmount" runat="server" Visible="false" Value='<% # Eval("Amount") %>' />
                                                                                                    </td>
                                                                                                    <td width="15" align="right">
                                                                                                        <asp:ImageButton ID="btnRemunerationRemove" runat="server" ToolTip="Delete Remunerationr"
                                                                                                            SkinID="DeleteIconDatalist" CommandName="REMOVE_Remuneration" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                    <asp:CustomValidator ID="cvRemuneration" runat="server" ClientValidationFunction="validateRemuneration"
                                                                                        ValidationGroup="Employee" Display="Dynamic" ErrorMessage="Please Remuneration particulars."></asp:CustomValidator>
                                                                                    <asp:CustomValidator ID="cvRemunerationAmount" runat="server" ClientValidationFunction="validateRemunerationAmount"
                                                                                        ValidationGroup="Employee" Display="Dynamic" ErrorMessage="Please enter the Remuneration amount"></asp:CustomValidator>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" class="firstTrRight" valign="top" style="padding:0px;">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="width:10%" class="firstTrLeft">
                                                                        Addition
                                                                    </td>
                                                                    <td style="width:20%">
                                                                        <asp:TextBox ID="txtAdditionAmt" Width="110px" runat="server" CssClass="textbox_disabled"
                                                                            Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width:13%" class="firstTrLeft">
                                                                        Deduction
                                                                    </td>
                                                                    <td style="width:20%">
                                                                        <asp:TextBox ID="txtDeductionAmt" runat="server" CssClass="textbox_disabled" Width="110px"
                                                                            Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width:13%" class="firstTrLeft">
                                                                        NetAmount
                                                                    </td>
                                                                    <td style="width:17%">
                                                                        <asp:TextBox ID="txtNetAmount" runat="server" CssClass="textbox_disabled" Width="110px"
                                                                            Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="padding-top: 25px;">
                                                        <td colspan="5" class="firstTrRight" valign="top">
                                                            <table width="100%" style="padding-top: 5px;">
                                                                <tr>
                                                                    <td class="firstTrLeft" style="width: 16.5%;">
                                                                        Remarks
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="textbox" onchange="RestrictMulilineLength(this, 500);"
                                                                            onkeyup="RestrictMulilineLength(this, 500);" TextMode="MultiLine" Width="94%" style="padding: 5px;"
                                                                            Height="50px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" valign="top" class="firstTrRight">
                                                            <table style="font-size: 11px; height: 42px; width: 100%;">
                                                                <tr>
                                                                    <td style="width: 15%;" class="firstTrLeft">
                                                                        Absent Policy
                                                                    </td>
                                                                    <td align="left" width="30%">
                                                                        <asp:UpdatePanel ID="upnlAbsentPolicy" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:DropDownList ID="ddlAbsentPolicy" runat="server" CssClass="dropdownlist" DataTextField="Description"
                                                                                                Width="175px" DataValueField="AbsentPolicyID">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                    <td width="18%" class="firstTrLeft">
                                                                        Encash Policy
                                                                    </td>
                                                                    <td align="left" width="27%">
                                                                        <asp:UpdatePanel ID="upnlEncashPolicy" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:DropDownList ID="ddlEncashPolicy" Width="175px" runat="server" CssClass="dropdownlist"
                                                                                                DataTextField="Description" DataValueField="EncashPolicyID">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" class="firstTrRight" valign="top">
                                                            <table width="100%" style="font-size: 11px">
                                                                <tr>
                                                                    <td width="15%" class="firstTrLeft">
                                                                        Holiday Policy
                                                                    </td>
                                                                    <td align="left" width="30%">
                                                                        <asp:UpdatePanel ID="upnlHolidayPolicy" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:DropDownList ID="ddlHolidayPolicy" runat="server" CssClass="dropdownlist" DataTextField="Description"
                                                                                                Width="175px" DataValueField="HolidayPolicyID">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                    <td valign="top" width="18%" class="firstTrLeft">
                                                                        Overtime Policy
                                                                    </td>
                                                                    <td align="left" width="27%">
                                                                        <asp:UpdatePanel ID="upnlOvertimePolicy" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:DropDownList ID="ddlOvertimePolicy" Width="175px" runat="server" CssClass="dropdownlist"
                                                                                                DataTextField="Description" DataValueField="OTPolicyID">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div>
                                        <asp:CustomValidator ID="cvAddAllowances" runat="server" ClientValidationFunction="validateAddAllowanceDeduction"
                                            ValidationGroup="Employee" Display="Dynamic" ErrorMessage="Please Add BasicPay."
                                            CssClass="error"></asp:CustomValidator>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
</asp:Content>

