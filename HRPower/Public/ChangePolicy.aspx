﻿<%@ Page Language="C#" MasterPageFile="~/Master/DocumentMasterPage.master" AutoEventWireup="true"
    CodeFile="ChangePolicy.aspx.cs" Inherits="Public_ChangePolicy" Title="Change Policy" %>

<%@ Register Src="../Controls/PayStructure.ascx" TagName="PayStructure" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div style="width: 100%; float: left;">
        <div style="width: 10%; height: 50px; margin-top: 10px; margin-bottom: 10px; padding-left: 1%;
            float: left;">
            <ul class="dark_menu">
                <li><a href="">Menu &nbsp; &#x25Bc</a>
                    <ul>
                        <li><a href="Employee.aspx">Employee</a></li>
                        <li><a href="SalaryStructure.aspx">Salary Structure</a></li>
                        <li><a href="ViewPolicy.aspx">View Policies</a></li>
                        <li class="selected"><a href="ChangePolicy.aspx">Change Policies</a></li>
                    </ul>
                </li>
            </ul>
        </div>
            <div style="width: 70%; float: left; margin-top: 25px; font-size: large; text-align: center;" class="name">
            Change Policies
            </div>
        <div style="width: 9%; float: left; margin-top: 25px;" class="name">
            <asp:LinkButton ID="btnAddPayStructure" runat="server" OnClick="btnAddPayStructure_Click"> <h5>Add Pay Structure</h5></asp:LinkButton>
        </div>
        <div style="width: 6%; float: left; margin-top: 15px;">
            <asp:ImageButton ID="imgAddPayStructure" runat="server" ToolTip="Add New PayStructure"
                ImageUrl="~/images/pay_structure.png" ValidationGroup="AddnewPayStructure" OnClick="imgAddPayStructure_Click" />
        </div>
    </div>
    <div style="width: 100%; float: left; margin-top: 10px; margin-bottom: 10px; padding-left: 1%;">
        <div style="width: 7%; float: left;">
            Company
        </div>
        <div style="width: 15%; float: left;">
            <asp:DropDownList ID="ddlCompany" runat="server" BorderStyle="Solid" BorderWidth="1px"
                Width="90%" DataTextField="Company" DataValueField="CompanyID">
            </asp:DropDownList>
        </div>
        <div style="width: 7%; float: left;">
            Department
        </div>
        <div style="width: 15%; float: left;">
            <asp:DropDownList ID="ddlDepartment" runat="server" BorderStyle="Solid" BorderWidth="1px"
                Width="90%" DataTextField="Department" DataValueField="DepartmentID">
            </asp:DropDownList>
        </div>
        <div style="width: 7%; float: left;">
            Designation
        </div>
        <div style="width: 15%; float: left;">
            <asp:DropDownList ID="ddlDesignation" runat="server" BorderStyle="Solid" BorderWidth="1px"
                Width="90%" DataTextField="Designation" DataValueField="DesignationID">
            </asp:DropDownList>
        </div>
        <div style="width: 7%; float: left;">
            Employee
        </div>
        <div style="width: 15%; float: left;">
            <asp:DropDownList ID="ddlEmployee" runat="server" BorderStyle="Solid" BorderWidth="1px"
                Width="90%" DataTextField="Employee" DataValueField="EmployeeID">
            </asp:DropDownList>
        </div>
        <div style="width: 7%; float: left;">
            <asp:Button ID="btnShow" CssClass="btnsubmit" runat="server" Text="Show" Width="100%"
                OnClick="btnShow_Click" />
        </div>
    </div>
    <div style="width: 100%; float: left;">
        <asp:UpdatePanel ID="upGridView" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="gvChangePolicy" runat="server" AutoGenerateColumns="False" DataKeyNames="EmployeeID,DesignationID"
                    OnRowCommand="gvChangePolicy_RowCommand" Width="100%" CellPadding="1" CellSpacing="0"
                    GridLines="None" OnRowDataBound="gvChangePolicy_RowDataBound" CssClass="labeltext">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Employee</HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("EmployeeName") %>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" CssClass="trLeft" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Designation
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("Designation")%>
                            </ItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                            <div style="width:100%; text-align:center;">Pay Structure </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("PayStructure")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlPayStructure" runat="server" DataTextField="PayStructure"
                                    DataValueField="PayStructureID" Width="130px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                              <div style="width:100%; text-align:center;">Work</div>
                                 
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("WorkPolicy")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlWorkPolicy" runat="server" CssClass="dropdownlist_mandatory"
                                    DataTextField="WorkPolicy" DataValueField="WorkPolicyID" Width="130px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvddlWorkPolicy" runat="server" ControlToValidate="ddlWorkPolicy"
                                    InitialValue="-1" ErrorMessage="Select Work Policy" SetFocusOnError="True" ValidationGroup="submit"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                  <div style="width:100%; text-align:center;">Leave</div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("PayStructure")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlLeavePolicy" runat="server" DataTextField="LeavePolicy"
                                    DataValueField="LeavePolicyID" Width="130px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>
                       <%-- <asp:TemplateField>
                            <HeaderTemplate>
                                Vacation Policy
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("VacationPolicy")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlVacationPolicy" runat="server" DataTextField="VacationPolicy"
                                    DataValueField="VacationPolicyID" Width="130px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Settlement Policy
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("SettlementPolicy")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSettlementPolicy" runat="server" DataTextField="SettlementPolicy"
                                    DataValueField="SettlementPolicyID" Width="130px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                  <div style="width:100%; text-align:center;">Absent</div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("AbsentPolicy")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlAbsentPolicy" runat="server" DataTextField="AbsentPolicy"
                                    DataValueField="AbsentPolicyID" Width="130px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>
                       <%-- <asp:TemplateField>
                            <HeaderTemplate>
                                  <div style="width:100%; text-align:center;">Encash</div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("EncashPolicy")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlEncashPolicy" runat="server" DataTextField="EncashPolicy"
                                    DataValueField="EncashPolicyID" Width="130px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                  <div style="width:100%; text-align:center;">Holiday</div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("HolidayPolicy")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlHolidayPolicy" runat="server" DataTextField="HolidayPolicy"
                                    DataValueField="HolidayPolicyID" Width="130px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                  <div style="width:100%; text-align:center;">Overtime</div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("OverTimePolicy")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlOvertimePolicy" runat="server" DataTextField="OvertimePolicy"
                                    DataValueField="OvertimePolicyID" Width="130px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle VerticalAlign="Top" CssClass="trRight" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnModify" runat="server" CommandName="Alter" CommandArgument='<%#Eval("EmployeeID") %>'
                                    ImageUrl="~/images/edit_icon_popup.jpg" ToolTip="edit" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 25px; float: left;">
                                        <asp:ImageButton ID="ibtnUpdate" runat="server" CssClass="imagebutton" CommandArgument='<%#Eval("EmployeeID") %>'
                                            CommandName="Add" ValidationGroup="submit" ImageUrl="~/images/save_icon.jpg"
                                            ToolTip="save" />
                                    </div>
                                    <div style="float: left; width: auto;">
                                        <asp:ImageButton ID="ibtnCancel" runat="server" CssClass="imagebutton" CommandName="Resume"
                                            CommandArgument='<%#Eval("EmployeeID") %>' ImageUrl="~/images/cancel_icon.jpg" ToolTip="cancel" />
                                    </div>
                                </div>
                            </EditItemTemplate>
                            <HeaderStyle Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderStyle CssClass="datalistheader" />
                    <EditRowStyle HorizontalAlign="Left" />
                </asp:GridView>
                <uc:Pager ID="ChangePolicyPager" runat="server" PageSize="10" />
                <asp:Label ID="lblgvChangePolicy" runat="server" Style="display: block;" CssClass="error"
                    Visible="False"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upnlPayStructure" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="fvPayStructure" runat="server" DefaultMode="Edit">
                <InsertItemTemplate>
                    <div style="display: none">
                        <asp:Button ID="btnPayStructure" runat="server" />
                    </div>
                    <asp:Panel ID="pnlPayStructure" runat="server" Style="display: none">
                        <uc1:PayStructure ID="ucPayStructure" runat="server" ModalPopupID="mpePayStructure" />
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpePayStructure" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlPayStructure" TargetControlID="btnPayStructure">
                    </AjaxControlToolkit:ModalPopupExtender>
                </InsertItemTemplate>
            </asp:FormView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
