﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using System.IO;
using AjaxControlToolkit;

public partial class Registration : System.Web.UI.Page
{
    clsCandidate objclsCandidate;
    private string CurrentSelectedValue
    {
        get;
        set;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadCombos();
             if(Request.QueryString["jobid"] != null)
            {
                
                int iJobId = Convert.ToInt32(Request.QueryString["jobid"]);
                ddlJob.SelectedIndex = ddlJob.Items.IndexOf(ddlJob.Items.FindByValue(iJobId.ToString()));
                lbjob.Text = ddlJob.SelectedItem.Text;
            }
        }
        ReferenceControl.OnSave -= new controls_ReferenceControl.saveHandler(ReferenceControl_OnSave);
        ReferenceControl.OnSave += new controls_ReferenceControl.saveHandler(ReferenceControl_OnSave);
        AgencyReference.OnSave += new Controls_AgencyReference.OnUpdate(AgencyReference_OnSave);


        if (fuResumeAttach.HasFile && Session["FileUpload"] != null)
        {
            //fResumeAttach.file
            fuResumeAttach = (AsyncFileUpload)this.Session["FileUpload"];


            //Session["FileUpload"] = fuResumeAttach;
        }
    }

    private void LoadCombos()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos(true);

        if (dsTemp != null)
        {
            if (dsTemp.Tables[0] != null)
            {
                ddlJob.DataSource = dsTemp.Tables[0];
                ddlJob.DataTextField = "JobCode";
                ddlJob.DataValueField = "JobId";
                ddlJob.DataBind();
                ddlJob.Items.Insert(0, new ListItem("Select", "-1"));
                updJob.Update();
            }

            if (dsTemp.Tables[2] != null)
            {
                rcSalutation.DataSource = dsTemp.Tables[2];
                rcSalutation.DataTextField = "Salutation";
                rcSalutation.DataValueField = "SalutationID";
                rcSalutation.DataBind();
                rcSalutation.Items.Insert(0, new ListItem("Select", "-1"));
                updSalutation.Update();
            }

           

           

            if (dsTemp.Tables[5] != null)
            {
                DataTable datTemp1 = dsTemp.Tables[5];
                rcCountry.DataSource = datTemp1;
                rcCountry.DataTextField = "CountryName";
                rcCountry.DataValueField = "CountryID";
                rcCountry.DataBind();
                rcCountry.Items.Insert(0, new ListItem("Select", "-1"));
                updCountry.Update();
              
            }

           
            if (dsTemp.Tables[7] != null)
            {
                rcDegree.DataSource = dsTemp.Tables[7];
                rcDegree.DataTextField = "Qualification";
                rcDegree.DataValueField = "DegreeID";
                rcDegree.DataBind();
                rcDegree.Items.Insert(0, new ListItem("Select", "-1"));
                uprcDegree.Update();
            }

            if (dsTemp.Tables[4] != null)
            {
                rcReligion.DataSource = dsTemp.Tables[4];
                rcReligion.DataTextField = "Religion";
                rcReligion.DataValueField = "ReligionID";
                rcReligion.DataBind();
                rcReligion.Items.Insert(0, new ListItem("Select", "-1"));
                updReligion.Update();
            }
            if (dsTemp.Tables[3] != null)
            {
                ddlMaritalStatus.DataSource = dsTemp.Tables[3];
                ddlMaritalStatus.DataTextField = "MaritalStatus";
                ddlMaritalStatus.DataValueField = "MaritalStatusID";
                ddlMaritalStatus.DataBind();
                ddlMaritalStatus.Items.Insert(0, new ListItem("Select", "-1"));
                updMaritalStatus.Update();
            }

            if (dsTemp.Tables[9] != null)
            {
                DataTable datTemp1 = dsTemp.Tables[9];

                ddlCurrentNationality.DataSource = datTemp1;
                ddlCurrentNationality.DataTextField = "Nationality";
                ddlCurrentNationality.DataValueField = "NationalityID";
                ddlCurrentNationality.DataBind();
                ddlCurrentNationality.Items.Insert(0, new ListItem("Select", "-1"));
                updCurrentNationality.Update();

            }

            if (dsTemp.Tables[10] != null)
            {
                ddlQualificationCategory.DataSource = dsTemp.Tables[10];
                ddlQualificationCategory.DataTextField = "QualificationCategory";
                ddlQualificationCategory.DataValueField = "QualificationCategoryID";
                ddlQualificationCategory.DataBind();
                ddlQualificationCategory.Items.Insert(0, new ListItem("Select", "-1"));
                updQualificationCategory.Update();
            }
            if (dsTemp.Tables[19] != null)
            {
                ddlReferraltype.DataSource = dsTemp.Tables[19];
                ddlReferraltype.DataTextField = "RefferalType";
                ddlReferraltype.DataValueField = "RefferalTypeID";
                ddlReferraltype.DataBind();

                ddlReferraltype.Items.Insert(0, new ListItem("Select", "-1"));
                ddlReferredBy.Items.Clear();
                ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));

            }
        }
    }
    protected void ddlReferraltype_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillComboRefferedBy();
    }
    protected void btnReferralType_Click(object sender, EventArgs e)
    {
        if (ddlReferraltype != null)
        {
            ReferenceControl.ClearAll();
            ReferenceControl.TableName = "HRCandidateRefferalTypeReference";
            ReferenceControl.DataTextField = "RefferalType";
            ReferenceControl.DataValueField = "RefferaltypeId";
            ReferenceControl.FunctionName = "FillComboRefferalType";
            ReferenceControl.SelectedValue = ddlReferraltype.SelectedValue;
            ReferenceControl.DisplayName = "Referral Type";
            //ReferenceControl.DataTextFieldArabic = "ReligionArb";
            ReferenceControl.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }
    protected void btnReferredBy_Click(object sender, EventArgs e)
    {
        if (ddlReferredBy != null)
        {

            switch (ddlReferraltype.SelectedValue)
            {
                case "1":
                    AgencyReference.BindDataList();
                    ModalPopoUpAgency.Show();
                    updAgency.Update();
                    updReferredBy.Update();
                    break;
                case "2":
                    ReferenceControl.ClearAll();
                    ReferenceControl.TableName = "HRJobPortalreference";
                    ReferenceControl.DataTextField = "Description";
                    //ReferenceControl.DataTextFieldArabic = "DescriptionArb";
                    ReferenceControl.DataValueField = "JobPortalId";
                    ReferenceControl.FunctionName = "FillComboRefferedBy";
                    ReferenceControl.SelectedValue = ddlReferredBy.SelectedValue;
                    ReferenceControl.DisplayName = "JobPortal";

                    //ReferenceControlNew1.DataTextFieldArabic = "RefferalTypeArb";
                    ReferenceControl.PopulateData();

                    mdlPopUpReference.Show();
                    updModalPopUp.Update();
                    updReferredBy.Update();
                    break;

            }
           


        }
    }
    public void FillComboRefferedBy()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();
       // string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        switch (ddlReferraltype.SelectedValue)
        {
            case "1":

                if (dsTemp.Tables[1] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[1];
                    ddlReferredBy.DataTextField = "Agency";
                    ddlReferredBy.DataValueField = "AgencyId";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));

                    this.SetSelectedIndex(this.ddlReferredBy);
                    btnReferredBy.Visible = true;


                }
                break;
            case "2":
                if (dsTemp.Tables[21] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[21];
                    ddlReferredBy.DataTextField = "Description";
                    ddlReferredBy.DataValueField = "JobPortalId";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));

                    this.SetSelectedIndex(this.ddlReferredBy);
                    btnReferredBy.Visible = true;
                }
                break;
            case "3":
                if (dsTemp.Tables[20] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[20];
                    ddlReferredBy.DataTextField = "EmployeeFullName";
                    ddlReferredBy.DataValueField = "EmployeeID";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));

                    this.SetSelectedIndex(this.ddlReferredBy);
                    btnReferredBy.Visible = false;

                }
                break;
            case "-1":
                ddlReferredBy.Items.Clear();
                ddlReferredBy.DataSource = null;
                ddlReferredBy.DataBind();
                ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));
                break;

        }
        updReferredBy.Update();
        updReferralType.Update();


    }
    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (clsCommon.Convert2DateTime(txtDateofBirth.Text.Trim()) > clsCommon.Convert2DateTime(DateTime.Now.Date.AddYears(-18).Date.ToString("dd/MM/yyyy")))
        {
            //lbMsg.Text = "Age must be greater than or equal to 18 years!!!";
            msgs.InformationalMessage("Age must be greater than or equal to 18 years!!!");
            mpeMessage.Show();
           
            txtDateofBirth.Focus();
            
        }
        else
        {
            RegisterCandidate();
        }
    }

    private void RegisterCandidate()
    {
         objclsCandidate = new clsCandidate();
        string sMessage = "";
        try
        {
            if (ddlJob.SelectedValue != "-1")
                objclsCandidate.JobID = ddlJob.SelectedValue.ToInt32();
            objclsCandidate.SalutationID = rcSalutation.SelectedValue.ToInt32();
            objclsCandidate.FirstNameEng = txtEnglishFirstName.Text.Trim();
            objclsCandidate.SecondNameEng = txtEnglishSecondName.Text.Trim();
            objclsCandidate.ThirdNameEng = txtEnglishThirdName.Text.Trim();
            objclsCandidate.PlaceofBirth = txtPlaceofbirth.Text.Trim();
            objclsCandidate.ReligionID = rcReligion.SelectedValue.ToInt32();
            objclsCandidate.MaritalStatusID = ddlMaritalStatus.SelectedValue.ToInt32();
            if (ddlReferraltype.SelectedValue != "-1")
                objclsCandidate.ReferralType = ddlReferraltype.SelectedValue.ToInt32();
            if (ddlReferredBy.SelectedValue != "-1")
                objclsCandidate.ReferredBy = ddlReferredBy.SelectedValue.ToInt32();
           

            if (txtDateofBirth.Text.Trim() != "")
                objclsCandidate.DateofBirth = clsCommon.Convert2DateTime(txtDateofBirth.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.DateofBirth = "";

            objclsCandidate.Gender = (rblGender.SelectedValue == "0" ? true : false);

            if (ddlCurrentNationality.SelectedValue != "-1")
                objclsCandidate.CurrentNationalityID = ddlCurrentNationality.SelectedValue.ToInt32();

            objclsCandidate.CurrentAddress = txtCurrentAddress.Text.Trim();
            
            objclsCandidate.CountryID = rcCountry.SelectedValue.ToInt32();
            objclsCandidate.CurrentTelephone = txtCurrentTelephoneNumber.Text.Trim();
            objclsCandidate.Skill = txtSkills.Text.Trim();
            objclsCandidate.ResumeAttachment = fuResumeAttach.FileName;
           if( ddlYear.SelectedValue != "-1")
                objclsCandidate.ExperienceYear = ddlYear.SelectedValue.ToInt32();
           if (ddlMonth.SelectedValue != "-1")
                objclsCandidate.ExperienceMonth = ddlMonth.SelectedValue.ToInt32();

            objclsCandidate.IsSelf = true;

            if (ddlQualificationCategory.SelectedValue != "-1")
                objclsCandidate.QualificationCategoryID = ddlQualificationCategory.SelectedValue.ToInt32();
            if (rcDegree.SelectedValue != "-1")
                objclsCandidate.QualificationID = rcDegree.SelectedValue.ToInt32();

            objclsCandidate.EmailID = txtEmail.Text.Trim();
            if (!objclsCandidate.CheckCandidateExists())
            {
                objclsCandidate.Begin();

                long lngTempCID = objclsCandidate.InsertCandidateBasicInfoEng(null, true);
               
                if (lngTempCID > 0)
                {
                    ClearControls();
                    msgs.InformationalMessage("Application No: AN " + lngTempCID + " <br> Congratulations, you have successfully registered for the vacancy");
                    mpeMessage.Show();
                    //lbMsg.Text = "Application No: AN " + lngTempCID + " <br> Congratulations, you have successfully registered for the vacancy";
                   
                }
                else
                {
                    
                    //lbMsg.Text = "Registration failed.";
                    msgs.InformationalMessage("Registration failed.");
                    mpeMessage.Show();
                   
                }
                objclsCandidate.Commit();

              
                if (Session["ResumeAttachment"] != null)
                {
                    System.IO.DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/documents/candidateresumes"));

                    foreach (System.IO.FileInfo file in dirInfo.GetFiles(lngTempCID + "_*"))
                    {
                        file.Delete();
                    }
                    File.Move(Server.MapPath("~/documents/temp/" + Session["ResumeAttachment"].ToString()), Server.MapPath("~/documents/candidateresumes/" + lngTempCID + "_" + fuResumeAttach.FileName));
                }

            }
            else
            {

                msgs.InformationalMessage("Registration failed.You have already registered .");
                mpeMessage.Show();
               // lbMsg.Text = "Registration failed.You have already registered .";
              
            }

           
        }
        catch (Exception )
        {

        }
    }

    protected void fuResumeAttach_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;   
      
        if (fuResumeAttach.HasFile)
        {
            filename = Session.SessionID + Path.GetExtension(fuResumeAttach.FileName);
            Session["ResumeAttachment"] = filename;
            fuResumeAttach.SaveAs(Server.MapPath("~/documents/temp/") + filename);

        }


        Session["FileUpload"] = fuResumeAttach;

    }

    private void ClearControls()
    {
        txtCurrentAddress.Text = "";
        txtCurrentTelephoneNumber.Text = "";
        txtDateofBirth.Text = "";
        txtEmail.Text = "";
        txtEnglishFirstName.Text = "";
        txtEnglishSecondName.Text = "";
        txtEnglishThirdName.Text = "";
        txtPlaceofbirth.Text = "";
        txtSkills.Text = "";
        ddlCurrentNationality.SelectedIndex = -1;
        ddlMonth.SelectedIndex = -1;
        ddlYear.SelectedIndex = -1;
        ddlQualificationCategory.SelectedIndex = -1;
        rcSalutation.SelectedIndex = -1;
        rcCountry.SelectedIndex = -1;
        rcDegree.SelectedIndex = -1;
        rcReligion.SelectedIndex = -1;
        ddlMaritalStatus.SelectedIndex = -1;
        ddlReferraltype.SelectedIndex = -1;
        ddlReferredBy.SelectedIndex = -1;
        rblGender.ClearSelection();

    }
    protected void btnCancelSubmit_Click(object sender, EventArgs e)
    {
        ClearControls();
        Response.Redirect("JobOpenings.aspx");
    }

    void AgencyReference_OnSave(DataTable dt)
    {
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod("FillComboRefferedBy");
        theMethod.Invoke(this, null);
    }
    void ReferenceControl_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }
}
