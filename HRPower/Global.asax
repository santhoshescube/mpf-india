﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Security.Principal" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Import Namespace="System.ComponentModel" %>

<script runat="server">

    public static BackgroundWorker bgworker = new BackgroundWorker();
    
    protected  void Application_BeginRequest(object sender, EventArgs e)
    {
        HttpCookie cookie = Request.Cookies["CurrentCulture"];

        if (cookie != null && cookie.Value == "ar-AE")
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ar-AE");
        }
        else
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-US");
        }
    }
    
    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        
        // Load a crystal report in a seperate thread on application start up event
        // to speed up report loading time.
        //clsCrysatlReprtThread.LoadReport();
        clsGlobalization.ProductSerial = "0";
        clsGlobalization.LicenseStatus = true;
        //bgworker.DoWork += new DoWorkEventHandler(DoWork);
        //bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(WorkerCompleted);
        //bgworker.RunWorkerAsync();
    }

    private void DoWork(object sender, DoWorkEventArgs e)
    {
        //clsProductValidation objPrd = new clsProductValidation();
        //clsGlobalization.LicenseStatus = objPrd.AlertAppInfo();
    }

    private void WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
        bgworker.Dispose();
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
        if (bgworker != null)
            bgworker.CancelAsync();
    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        //// Code that runs when an unhandled error occurs

        Exception ex = Server.GetLastError();

        string message = string.Empty;


        if (ex != null)
        {
            //ScriptManager.RegisterClientScriptBlock(new Control(), this.GetType(), new Guid().ToString(), "alert('" + ex.Message.ToString() + "');", true);
            message = ex.Message;

            clsCommon.WriteErrorLog(ex);

        }

        Server.ClearError();

        Page errorPage = HttpContext.Current.Handler as Page;

        MasterPage master = null;
        if (errorPage != null)
            master = errorPage.Master;

        if (master != null)
        {
            if (master.ToString() == "ASP.master_essmaster_master")
                Response.Redirect("~/UpgradeTips.html");
            else
                Response.Redirect("~/ManageTips.html");
        }
        else
            Response.Redirect("~/UpgradeTips.html");

        

    }
    protected void Application_OnAuthenticateRequest(Object sender, EventArgs e)
    {
        if (HttpContext.Current.User != null)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                if (HttpContext.Current.User.Identity is FormsIdentity)
                {
                    // Get Forms Identity From Current User
                    FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

                    // Get Forms Ticket From Identity object
                    FormsAuthenticationTicket ticket = id.Ticket;

                    // Retrieve stored user-data (role information is assigned 
                    // when the ticket is created, separate multiple roles 
                    // with commas) 
                    string[] userData = ticket.UserData.Split(',');
                    string[] roles = new string[1];
                    roles[0] = userData[0];//Extracting role from user data

                    // Create a new Generic Principal Instance and 
                    // assign to Current User
                    HttpContext.Current.User = new GenericPrincipal(id, roles);
                }
            }
        }
    }


    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }
    

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        System.IO.DirectoryInfo dirInfo = new System.IO.DirectoryInfo(Convert.ToString(ConfigurationManager.AppSettings["_TEMP_PHY_PATH"]));

        if (dirInfo.Exists)
        {
            foreach (System.IO.FileInfo file in dirInfo.GetFiles(Session.SessionID + ".*"))
            {
                file.Delete();
            }
        }    
       
        
        
    }

   

       
</script>
