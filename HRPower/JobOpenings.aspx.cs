﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class JobOpenings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BindJobs();

    }

    private void BindJobs()
     {
        DataTable dt = new clsHomePage().GetAllOpenJobs();
        if (dt.Rows.Count > 0)
        {
            dlJobopenings.DataSource = dt;
            dlJobopenings.DataBind();
            lbmsg.Text = "";
            lbmsg.Visible = false;
        }
        else
        {
            lbmsg.Text = "No vacancies found.";
            lbmsg.Visible = true;
        }
       

        
    }
    protected void dlJobopenings_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnkApplyNow = (LinkButton)e.Item.FindControl("lnkApplyNow");
            if (lnkApplyNow != null)
                lnkApplyNow.PostBackUrl = "Registration.aspx?jobid=" + dlJobopenings.DataKeys[e.Item.ItemIndex].ToString();
        }
    }
}
