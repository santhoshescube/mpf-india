﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JobOpenings.aspx.cs" Inherits="JobOpenings" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Current  Openings</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="HandheldFriendly" content="true"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/global.css"/>

    <script type="text/javascript" src="js/jquery.min.js"></script>

    <script type="text/javascript" language="javascript" charset="utf-8" src="js/bootstrap.min.js"></script>
<style type="text/css" >
html,
body {
  height: 100%;
  /* The html and body elements cannot have any padding or margin. */
}

/* Wrapper for page content to push down footer */
#wrap {
  min-height: 100%;
  height: auto;
  /* Negative indent footer by its height */
  margin: 0 auto -115px;
  /* Pad bottom by footer height */
  padding: 0px !important;
}

/* Set the fixed height of the footer here */
#footer1 {
  height: 60px;

}


/* Custom page CSS

</style>
</head>
<body>
<div id="wrap">
    <form id="form1" runat="server">
    <div class="row-fluid">
        <div class="span12" style="margin-top: 10px; padding-bottom: 5px; border-bottom: 1px solid #0FABEC;">
            <div class="span1">
            </div>
            <div class="span3">
                <div id="logo">
                    <img src="images/companylogo.png" />
                </div>
            </div>
            <div class="span5">
            </div>
            <div class="span3">
                <div id="logo1">
                    <img src="images/logohr power.png" />
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12" style="padding-top: 10px; padding-bottom: 40px; background-color: White;">
            <div class="span3" >  
            </div>
            <div class="span6">
               <asp:Label CssClass="error" ID="lbmsg" runat="server"></asp:Label>
               <div style="float: right; padding-right: 22px; font-weight: bold">
                                <a href="Login.aspx" class="btn btn-info ">⇦&nbsp;Back</a></div>
                <asp:DataList ID="dlJobopenings" runat="server" CellPadding="4" CellSpacing="2" 
                    CssClass="labeltext" DataKeyField="JobId" 
                    onitemdatabound="dlJobopenings_ItemDataBound">
                    <ItemTemplate>
                        <div id="divMyAttendance" runat="server" style="width: 100%; border: 1px solid #b8b8b8;
                            border-radius: 5px; height: auto; padding-left: 5px;padding-top:5px; background: white; float: left;
                            margin-top: 2%; margin-left: 12px; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
                            <label for="Job" class="text-info" style="margin-right: 12px; font-size: 16px;font-weight:bold">
                                <%#Eval("Job1")%>
                            </label>
                             <label for="Job" style="margin-right: 12px; font-size: 12px">
                                Posted on : 
                                <%#Eval("ApprovedDate")%>
                            </label>
                            <label for="Job" style="margin-right: 12px; font-size: 12px">
                                Position :
                                <%#Eval("Designation")%>
                            </label>
                            <label for="Job" style="margin-right: 12px; font-size: 12px">
                                Experience:
                                <%#Eval("Experience")%>
                            </label>
                            <label for="Job" style="margin-right: 12px; font-size: 12px; word-break: break-all">
                                Job description :
                                <%#Eval("JobDescription")%>
                            </label>
                            <label for="Job" style="margin-right: 12px; font-size: 12px;  word-break: break-all">
                                Skills:
                                <%#Eval("Skills")%>
                            </label>
                            <div style="float: right; padding-right: 5px;">
                                <asp:LinkButton ID="lnkApplyNow" runat="server">Apply Now</asp:LinkButton>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>
    </div>
    <div class="span12">
    </div>
    </div>
    
 <div id="footer1">  
    <div class="row-fluid">
        <div class="span12" style="border-top: 5px solid #00BE9C; background: Black; padding-bottom: 20px;">
            <div class="span3">
            </div>
            <div class="span8" style="padding: 20px;">
                <p style="color: grey;">
                    EPeople © 2009-21 Privacy policy All rights reserved. Powered by <a href="http://www.escubetech.com/">
                        ES3 Technovations LLP</a>.</p>
            </div>
            <div class="span1">
            </div>
        </div>
    </div>
    </div> 
    </form>
</body>
</html>
