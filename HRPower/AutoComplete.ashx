﻿<%@ WebHandler Language="C#" Class="AutoComplete" %>

using System;
using System.Web;
using HRAutoComplete;

public class AutoComplete : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        string Prefix = context.Request.QueryString["q"] == null ? string.Empty : context.Request.QueryString["q"].ToString();
        string PageEnumIndex = context.Request.QueryString["p"] == null ? "0" :  context.Request.QueryString["p"];
        string CompanyID = context.Request.QueryString["C"] == null ? "0" : context.Request.QueryString["C"]; 
        string suggestions = GlobalAutoComplete.GetSuggestions(Prefix.EscapeUnsafeChars(), PageEnumIndex , CompanyID.ToInt32());
        context.Response.ContentType = "text/plain";
        context.Response.Write(suggestions);
    }
  
    public bool IsReusable {
        get { return false; }
    }

}
