﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CandidateHome.aspx.cs" Inherits="Public_CandidateHome" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <title>My Home</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" media="all" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="all" href="../css/bootstrap-responsive.min.css">
    <link rel="stylesheet" type="text/css" media="all" href="../css/global.css">
    <link href="../css/popup.css" rel="stylesheet" type="text/css" />
    <%--<link href="../css/default_common.css" rel="stylesheet" type="text/css" />--%>
    <link href="../css/styless_common.css" rel="stylesheet" type="text/css" />
    <link href="../css/employee_menu.css" rel="stylesheet" type="text/css" />
<%--    <link href="../css/default_common.css" rel="stylesheet" type="text/css" />--%>
    <link href="../css/styless_common.css" rel="stylesheet" type="text/css" />
    <link href="../css/inettuts.css" rel="stylesheet" type="text/css" />
    <link href="../css/inettuts.js.css" rel="stylesheet" type="text/css" />
    <link href="../css/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
    <link href="../css/singleview.css" rel="stylesheet" type="text/css" />

    <script src="../js/HRPower.js" type="text/javascript"></script>

    <script src="../js/Controls.js" type="text/javascript"></script>

    <script src="../js/Common.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/jquery.min.js"></script>

    <script type="text/javascript" language="javascript" charset="utf-8" src="../js/bootstrap.min.js"></script>

    <style type="text/css">
        .style1
        {
            width: 410px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <%-- *******************/start/HEADER****************--%>
    <AjaxControlToolkit:ToolkitScriptManager EnablePageMethods="true" ID="ajxManager"
        runat="server">
    </AjaxControlToolkit:ToolkitScriptManager>
    <div class="row-fluid">
        <div class="span12" style="margin-top: 10px; padding-bottom: 5px; border-bottom: 1px solid #0FABEC;">
            <div class="span1">
            </div>
            <div class="span3">
                <div id="log">
                    <img src="../images/companylogo.png" />
                </div>
            </div>
            <div class="span4">
            </div>
            <div class="span4">
                <div class="span10">
                    <div id="logo1">
                        <img src="../images/logoeng.png" />
                    </div>
                </div>
                <div class="span2">
                    <a id="A1" href="~/Logout.aspx" meta:resourcekey="Logout" runat="server">
                        <%--title="Logout"--%>
                        <img src="../images/logout1.png" style="margin-top: 38px;" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <%-- *******************/end/HEADER****************--%>
    <%-- *******************/start/CONTENTS****************--%>
    <div class="row-fluid">
        <div class="span12" style="padding-top: 10px; padding-bottom: 40px; min-height: 433px;">
            <div class="span1">
            </div>
            <div class="span2">
                <div id='cssmenu' style="padding-top: 18px; cursor: pointer;">
                    <ul>
                        <li>
                            <asp:LinkButton ID="lnkHome" runat="server" meta:resourcekey="Home" OnClick="btnBack_OnClick"></asp:LinkButton>
                        </li>
                        <li>
                            <%--<a href="AttachDocuments_onClick"  runat="server">
                            <asp:Literal ID="Literal90" runat="server" Text="Attach Documents"></asp:Literal></a>--%>
                            <asp:LinkButton ID="lnkAttachDocuments" runat="server" meta:resourcekey="AttachDocuments" OnClick="AttachDocuments_onClick"></asp:LinkButton>
                        </li>
                        <li>
                            <%--<a href="DispTicketDetails_onClick" runat="server">
                            <asp:Literal ID="Literal12" runat="server" Text="Visa/Ticket Details"></asp:Literal></a>--%>
                            <asp:LinkButton ID="lnkTicketDetails" runat="server" meta:resourcekey="VisaTicketDetails" OnClick="DispTicketDetails_onClick"></asp:LinkButton>
                        </li>
                        <%-- <li>
                            <asp:LinkButton ID="lnkUpdateTicketDetails" runat="server" Text="Update Ticket Details"
                                OnClick="UpdateTicketDetails_onClick"></asp:LinkButton>
                        </li>--%>
                    </ul>
                </div>
            </div>
            <div class="span6">
                <div runat="server" class="error" id="divNoData">
                </div>
                <div>
                    <div style="display: none">
                        <asp:Button ID="btnSubmitMessage" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
                    </div>
                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                        BorderStyle="None">
                        <asp:UpdatePanel ID="upnlMessage" runat="server">
                            <ContentTemplate>
                                <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmitMessage"
                        PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                        Drag="true">
                    </AjaxControlToolkit:ModalPopupExtender>
                </div>
                <asp:UpdatePanel ID="updViewAll" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divViewAll" runat="server">
                            <asp:DataList ID="dlAllJobs" runat="server" CellPadding="4" CellSpacing="2" CssClass="labeltext"
                                OnItemCommand="dlAllJobs_ItemCommand" DataKeyField="OfferID">
                                <ItemTemplate>
                                    <div id="divViewJobs" runat="server" style="width: 615px; border: 1px solid #b8b8b8;
                                        border-radius: 5px; height: auto; padding-left: 5px; padding-top: 5px; background: white;
                                        float: left; margin-top: 2%; margin-left: 12px; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
                                        <label for="Job" class="text-info" style="margin-right: 12px; font-size: 16px; font-weight: bold">
                                            <%#Eval("Job1")%>
                                        </label>
                                        <label for="Job" style="margin-right: 12px; font-size: 12px">
                                            <%--Posted on :--%><asp:Literal ID="Literal1" runat="server" meta:resourcekey="Postedon"></asp:Literal>
                                            &nbsp;<%#Eval("ApprovedDate")%>
                                        </label>
                                        <label for="Job" style="margin-right: 12px; font-size: 12px">
                                            <%--Experience:--%><asp:Literal ID="Literal2" runat="server" meta:resourcekey="Experience"></asp:Literal>
                                            &nbsp;<%#Eval("Experience")%>
                                        </label>
                                        <label for="Job" style="margin-right: 12px; font-size: 12px">
                                            <%--Job description :--%><asp:Literal ID="Literal3" runat="server" meta:resourcekey="Jobdescription"></asp:Literal>
                                            <%#Eval("JobDescription")%>
                                        </label>
                                        <label for="Job" style="margin-right: 12px; font-size: 12px">
                                            <%--  Skills:--%><asp:Literal ID="Literal4" runat="server" meta:resourcekey="Skills"></asp:Literal>
                                            <%#Eval("Skills")%>
                                        </label>
                                        <div style="float: right; padding-right: 5px">
                                            <asp:Label ID="lblJobStatus" runat="server" Text='<%#Eval("CandidateStatus")%>' CssClass="statusspan"></asp:Label>
                                            <asp:HiddenField ID="hfStatusID" runat="server" Value='<%#Eval("CandidateStatusID")%>' />
                                        </div>
                                        <div style="float: right; padding-right: 5px">
                                            <asp:ImageButton ID="imgViewOffer" runat="server" ImageUrl="~/images/offer.png" CommandName="VIEW"
                                                meta:resourcekey="ViewOffer" CommandArgument='<%#Eval("OfferID")%>' CausesValidation="false" />
                                        </div>
                                        <%-- <div style="float: right; padding-right: 30px">
                                            <asp:Button ID="btnTicketDetails" runat="server" CssClass="btnsubmit" CausesValidation="false"
                                                Text="View Ticket Details" CommandArgument='<%Eval("CandidateID")%>' CommandName="TICKETDETAILS"
                                                Visible="false" />
                                        </div>--%>
                                        <br />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="updViewOffer" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="ViewOffer" runat="server" style="display: none;">
                            <div id="divViewOfferLetter" runat="server">
                            </div>
                            <div style="float: right; padding-right: 5px">
                                <asp:ImageButton ID="ancOfferAcceptance" runat="server" OnClick="lnkOfferAcceptance_Click"
                                    ImageUrl="~/images/offer_acceptance.png" CausesValidation="false" Height="27px"
                                    meta:resourcekey="ClicktoAcceptReject"></asp:ImageButton>
                                <asp:Button ID="btnBack" runat="server" meta:resourcekey="BackToHome" Class="btn btn-success"
                                    OnClick="btnBack_OnClick" />
                                <div id="divOfferAcceptance" runat="server" style="display: none; max-height: 250px">
                                    <fieldset style="padding-right: 5px; margin-left: 5px; width: 85%; margin-top: 5px;
                                        border-radius: 8px; float: left; margin-bottom: 14px; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 3px 2px rgba(208, 223, 226, 0.4) inset;">
                                        <table cellpadding="4" cellspacing="0" border="0" class="labeltext" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:RadioButtonList ID="rblOffer" runat="server" RepeatColumns="2" Width="150px"
                                                        sRepeatDirection="Horizontal" CellPadding="5" CellSpacing="5">
                                                        <asp:ListItem meta:resourcekey="Accept" Value="1" Selected="True"></asp:ListItem>
                                                        <asp:ListItem meta:resourcekey="Reject" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%--Remarks--%><asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ControlsCommon,Remarks%>'></asp:Literal>
                                                    <asp:TextBox ID="txtRejectRemarks" ValidationGroup="OfferNew" runat="server" TextMode="MultiLine"
                                                        Width="175px" Height="50px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ErrorMessage="*" ControlToValidate="txtRejectRemarks"
                                                        ValidationGroup="OfferNew"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnOfferAcceptance" runat="server" CssClass="btnsubmit" ValidationGroup="OfferNew"
                                                        Text='<%$Resources:ControlsCommon,Submit%>' OnClick="btnOfferAcceptance_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="updTicketDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divTicketDetails" runat="server" style="display: none;">
                            <div id="divSingleView" runat="server" style="width: 100%; height: auto; float: left;
                                padding-top: 2px;">
                                <asp:DataList ID="dlSingleView" runat="server" Width="100%" OnItemDataBound="dlSingleView_ItemDataBound">
                                    <ItemStyle CssClass="item" />
                                    <HeaderStyle CssClass="listItem" />
                                    <HeaderTemplate>
                                        <table width="95%" border="0" cellpadding="3" class="datalistheader">
                                            <tr>
                                                <td valign="top" width="100%" style="padding-left: 5px;">
                                                    <asp:Literal ID="Literal42" runat="server" meta:resourcekey="VisaTicketDetails"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                            <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Candidate"></asp:Literal>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                            :
                                        </div>
                                        <div class="trRight" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("Candidate")%>
                                        </div>
                                        <%----------------------------------MODIFICATIONS---------------------------------------  --%>
                                        <div>
                                            <table width="95%" border="0" cellpadding="3">
                                                <tr>
                                                    <td valign="top" width="100%" style="padding-left: 5px; color: #307296; font-weight: bold;
                                                        padding-top: 10px;">
                                                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="VisaDetails"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <asp:GridView ID="gvVisa" runat="server" AutoGenerateColumns="false" Width="96%"
                                            HeaderStyle-Height="10" BorderColor="#307296">
                                            <HeaderStyle CssClass="datalistheader" />
                                            <Columns>
                                                <asp:BoundField DataField="Status" meta:resourcekey="ProcessingStatus" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Size="Small" />
                                                <%--<asp:BoundField DataField="VisaNumber" HeaderText="Number" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Size="Small" />
                                                <asp:BoundField DataField="VisaType" HeaderText="Type" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Size="Small" />--%>
                                                <asp:BoundField DataField="VisaIssueDate" meta:resourcekey="IssueDate" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small"/>
                                                <%--<asp:BoundField DataField="VisaValidityDate" HeaderText="Validity Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small" />--%>
                                                <asp:BoundField DataField="VisaExpiryDate" meta:resourcekey="ExpiryDate" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small" />
                                                <asp:BoundField DataField="VisaAmount" meta:resourcekey="Amount" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small" />
                                            </Columns>
                                        </asp:GridView>
                                        <%--------------------------------------------------------------------------------------  --%>
                                        <div class="trLeft" style="float: left; width: 26%; height: 29px; padding-top: 15px;">
                                            <asp:Literal ID="Literal18" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 5%; height: 29px; padding-top: 15px;">
                                            :
                                        </div>
                                        <div class="trRight" style="float: left; width: 60%; height: auto; word-break: break-All;
                                            padding-top: 15px;">
                                            <%# Eval("VisaRemarks")%>
                                        </div>
                                        <div>
                                            <table width="95%" border="0" cellpadding="3">
                                                <tr>
                                                    <td valign="top" width="100%" style="padding-left: 5px; color: #307296; font-weight: bold;
                                                        padding-top: 20px;">
                                                        <asp:Literal ID="Literal36" runat="server" meta:resourcekey="TicketDetails"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                            <asp:Literal ID="Literal19" runat="server" meta:resourcekey="TicketSent"></asp:Literal>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                            :
                                        </div>
                                        <div class="trRight" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("TicketSent")%>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                            <asp:Literal ID="Literal20" runat="server" meta:resourcekey="TicketSentDate"></asp:Literal>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                            :
                                        </div>
                                        <div class="trRight" style="float: left; width: 60%; height: 29px; word-break: break-all;">
                                            <%# Eval("TicketSentDate")%>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Arrivaldate"></asp:Literal>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                            :
                                        </div>
                                        <div class="trRight" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("Arrivaldate")%>&nbsp; @ &nbsp;
                                            <%# Eval("ArrivalTime")%>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                            <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Venue"></asp:Literal>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                            :
                                        </div>
                                        <div class="trRight" style="float: left; width: 60%; height: 29px; word-break: break-all;">
                                            <%# Eval("Venue")%>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                            <asp:Literal ID="Literal23" runat="server" meta:resourcekey="PickUpRequired"></asp:Literal>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                            :
                                        </div>
                                        <div class="trRight" style="float: left; width: 60%; height: 29px; word-break: break-all;">
                                            <%# Eval("PickUpReqlbl")%>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                            <asp:Literal ID="Literal33" runat="server" meta:resourcekey="TicketAmount"></asp:Literal>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                            :
                                        </div>
                                        <div class="trRight" style="float: left; width: 60%; height: 29px;">
                                            <%# Eval("TicketAmount")%>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                            <asp:Literal ID="Literal22" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                        </div>
                                        <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                            :
                                        </div>
                                        <div class="trRight" style="float: left; width: 60%; height: auto; word-break: break-All;">
                                            <%# Eval("TicketRemarks")%>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlEditTicketDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divEditTicketDetails" runat="server" style="float: left; width: 96%; display: none;">
                            <fieldset id="fsTicket" runat="server" style="width: 92%; margin-left: 10px; color: #307296;
                                font-weight: bold;">
                                <legend >keyEdit Ticket Details</legend>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 26%; height: 29px">
                                        <asp:Literal ID="Literal16" runat="server" meta:resourcekey="ArrivalDate"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <div style="float: left;">
                                            <asp:TextBox ID="txtArrival" runat="server" CssClass="textbox_mandatory" Width="90px"></asp:TextBox>&nbsp;
                                            <asp:ImageButton ID="imgArrival" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                CausesValidation="false" CssClass="imagebutton" />
                                            <AjaxControlToolkit:CalendarExtender ID="ceArrival" runat="server" TargetControlID="txtArrival"
                                                Format="dd/MM/yyyy" PopupButtonID="imgArrival">
                                            </AjaxControlToolkit:CalendarExtender>
                                            <asp:CustomValidator ID="cvArrival" runat="server" CssClass="error" ControlToValidate="txtArrival"
                                                Display="Dynamic" ValidationGroup="submit"></asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="rfvArrivalDate" runat="server" ControlToValidate="txtArrival"
                                                ValidationGroup="submit" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                                        </div>
                                        <div style="float: left;">
                                            &nbsp; @&nbsp;
                                        </div>
                                        <div style="float: left;">
                                            <asp:TextBox ID="txtArrivalTime" runat="server" CssClass="textbox_mandatory" Width="80px"
                                                MaxLength="7" Text='<%# Eval("ArrivalTime") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtArrivalTime" runat="server" ControlToValidate="txtArrivalTime"
                                                ValidationGroup="submit" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="hfCandidateName" runat="server" />
                                </div>
                                <asp:UpdatePanel ID="upnlVenue" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 96%;">
                                            <div class="firstTrLeft" style="float: left; width: 26%; height: 29px">
                                                <asp:Literal ID="Literal18" runat="server" meta:resourcekey="Venue"></asp:Literal>
                                            </div>
                                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                                <asp:DropDownList ID="ddlVenue" runat="server" DataTextField="Venue" DataValueField="VenueID"
                                                    Width="250px" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <asp:Button ID="btnVenueReferenece" runat="server" CausesValidation="False" CommandName="Venue"
                                                    CssClass="referencebutton" OnClick="btnVenueReferenece_Click" Text="..." />
                                                <asp:RequiredFieldValidator ID="rfvVenue" runat="server" CssClass="error" Text="*"
                                                    Display="Dynamic" ControlToValidate="ddlVenue" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; vertical-align: top; width: 25%; height: 25px">
                                        <asp:Literal ID="Literal19" runat="server" meta:resourcekey="PickUpRequired"></asp:Literal><%--meta:resourcekey="PickUpRequired"--%>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                        <asp:RadioButtonList ID="rblPickUpReq" runat="server" RepeatDirection="Horizontal"
                                            Width="100px" AutoPostBack="True">
                                            <asp:ListItem Value="1" meta:resourcekey="Yes"></asp:ListItem>
                                            <asp:ListItem Selected="True" Value="0" meta:resourcekey="No"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 26%; height: 29px">
                                        <asp:Literal ID="Literal31" runat="server" meta:resourcekey="TicketAmount"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: auto;">
                                        <asp:TextBox ID="txtTicketAmount" runat="server" MaxLength="15" Width="120px" />
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteTicketAmount" runat="server" FilterType="Numbers,Custom"
                                            TargetControlID="txtTicketAmount" ValidChars=".">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 26%; height: 29px">
                                        <asp:Literal ID="Literal17" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: auto;">
                                        <asp:TextBox ID="txtTicketRemarks" runat="server" Height="70px" MaxLength="1000"
                                            TextMode="MultiLine" Width="280px" />
                                    </div>
                                </div>
                                <div id="tblSubmit" style="width: 44%; float: left; margin-left: 64px; text-align: right;
                                    padding-top: auto;">
                                    <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                                        ToolTip='<%$Resources:ControlsCommon,Submit%>' ValidationGroup="submit" Width="75px"
                                        OnClick="btnSubmit_Click" />
                                    <asp:Button ID="btnCancel" runat="server" Width="75px" Style="margin-left: 5px;"
                                        CssClass="btnsubmit" CausesValidation="false" Text='<%$Resources:ControlsCommon,Cancel%>'
                                        ToolTip='<%$Resources:ControlsCommon,Cancel%>' OnClick="btnCancel_Click" />
                                </div>
                            </fieldset>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlCandidateDocument" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <div id="divDocumentAttach" runat="server" style="float: left; display: none; width: 100%;">
                            <div style="float: left; width: 17%">
                                <asp:Label ID="lblDocumentCaption" runat="server" meta:resourcekey="DocumentName"></asp:Label>
                            </div>
                            <div style="float: left; width: 75%">
                                <asp:TextBox ID="txtCandidateDocumentName" runat="server" CssClass="textbox" MaxLength="30"
                                    Width="150px" Style="margin-right: 20px;"></asp:TextBox>
                                <AjaxControlToolkit:AsyncFileUpload ID="fuCandidate" runat="server" ErrorBackColor="White"
                                    OnUploadedComplete="fuCandidate_UploadedComplete" PersistFile="True" CompleteBackColor="Green"
                                    Visible="true" Width="215px" />
                                <asp:LinkButton ID="LinkButton1" runat="server" Enabled="true" OnClick="btnAttachCandidateDocument_Click"
                                    ValidationGroup="AttachedDocument" meta:resourcekey="Addtolist" ForeColor="#33a3d5">
                                </asp:LinkButton>
                            </div>
                            <div style="float: left; width: 70%; padding-left: 190px;">
                                <asp:RequiredFieldValidator ID="rfvDocumentName" Text="*" ValidationGroup="AttachedDocument"
                                    runat="server" ControlToValidate="txtCandidateDocumentName">
                                        
                                </asp:RequiredFieldValidator>
                            </div>
                            <div style="clear: both">
                            </div>
                            <div style="width: 70%; height: 200px; overflow: auto" id="dvAttachedDocuments" runat="server">
                                <asp:DataList ID="dlCandidateDocument" runat="server" GridLines="Horizontal" Width="90%"
                                    OnItemDataBound="dlCandidateDocument_ItemDataBound" BorderColor="#33a3d5">
                                    <HeaderStyle CssClass="datalistheader" />
                                    <HeaderTemplate>
                                        <div style="float: left; width: 100%;">
                                            <div style="float: left; width: 20%;" class="trLeft">
                                                <asp:Literal ID="Literal75" runat="server" meta:resourcekey="DocumentName"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 40%;" class="trLeft">
                                                <asp:Literal ID="Literal76" runat="server" meta:resourcekey="FileName"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 18%;" class="trRight">
                                                <asp:Literal ID="Literal77" runat="server" meta:resourcekey="Image"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 02%;" class="trRight">
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="float: left; width: 100%;">
                                            <asp:HiddenField ID="hfIsResume" runat="server" Value='<%#Eval("Type") %>' />
                                            <div style="float: left; width: 20%; word-break: break-all" class="trLeft">
                                                <asp:Label ID="lblDocumentName" runat="server" Text='<%# Eval("DocumentName")%>'
                                                    Width="120px" Style="word-break: break-all"></asp:Label>
                                            </div>
                                            <div style="float: left; width: 53%;" class="trLeft">
                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName")%>' Width="200px"></asp:Label>
                                            </div>
                                            <div style="float: LEFT; width: 12%;" class="trRight">
                                                <asp:ImageButton ID="img" Width="25px" Height="25px" runat="server" ImageUrl='<%# Eval("Location") %>'
                                                    CommandArgument='<%# Eval("Location") %>' OnClick="img_Click" CausesValidation="false"
                                                    CommandName="imgView" />
                                            </div>
                                            <div style="float: left; width: 2%;" class="trRight">
                                                <asp:ImageButton ID="imgDelete" ImageUrl="~/images/Delete18x18.png" runat="server"
                                                    CommandArgument='<%# Eval("FileName") %>' OnClick="imgDelete_Click" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                                <div style="width: 44%; float: left; margin-left: 64px; text-align: right; padding-top: auto;">
                                    <asp:Button ID="btnSaveDocuments" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                                        ToolTip='<%$Resources:ControlsCommon,Submit%>' Width="75px" OnClick="btnSaveDocuments_Click" />
                                    <asp:Button ID="btnCancelAttach" runat="server" Width="75px" Style="margin-left: 5px;"
                                        CssClass="btnsubmit" CausesValidation="false" Text='<%$Resources:ControlsCommon,Cancel%>'
                                        ToolTip='<%$Resources:ControlsCommon,Cancel%>' OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btn" Text="" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:ReferenceNew ID="ReferenceNew1" runat="server" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- *******************/end/CONTENT****************--%>
    <%-- *******************/start/FOOTER****************--%>
    <div class="row-fluid">
        <div class="span12" style="border-top: 5px solid #00BE9C; background: Black; padding-bottom: 20px;
            padding-top: 20px;">
            <div class="span3">
            </div>
            <div class="span8" style="padding: 20px;">
                <p style="color: grey;">
                    EPeople � 2009-21 Privacy policy All rights reserved. Powered by <a href="http://www.escubetech.com/">
                        ES3 Technovations LLP</a>.</p>
            </div>
            <div class="span1">
            </div>
        </div>
    </div>
    <%-- *******************/end/FOOTER****************--%>
    </div>
    </form>
</body>
</html>
