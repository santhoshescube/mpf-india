﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Specialized;
using System.Net.Mail;
using System.IO;
using System.Reflection;
using System.Globalization;

public partial class Public_CandidateHome : System.Web.UI.Page
{
    #region Declarations

    clsIndividualCandidateJobs objIndividualCandidateJobs;
    clsCandidate objclsCandidate;
    private string CurrentSelectedValue { get; set; }

    #endregion Declarations

    #region Events

    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        if (!IsPostBack)
        {
            BindInitials();
        }
        ReferenceNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }

    protected void dlAllJobs_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            objIndividualCandidateJobs = new clsIndividualCandidateJobs();
            switch (e.CommandName)
            {
                case "VIEW":        //Displays a selected job details
                    ViewOfferletter(e.Item);
                    break;

                case "TICKETDETAILS":
                    DisplayTicketDetails(ViewState["CandidateID"].ToInt32());
                    break;
            }
        }
        catch { }
    }

    protected void btnBack_OnClick(object sender, EventArgs e)
    {
        BindDetails();
    }

    protected void lnkOfferAcceptance_Click(object sender, ImageClickEventArgs e)
    {
        divOfferAcceptance.Style["display"] = "block";
        ancOfferAcceptance.Attributes.Add("onclick", "ClearAcceptance(this.id);return false;");
        ScriptManager.RegisterClientScriptBlock(ancOfferAcceptance, ancOfferAcceptance.GetType(), "Offer", "ClearAcceptance('" + ancOfferAcceptance.ClientID + "');", true);
    }

    protected void btnOfferAcceptance_Click(object sender, EventArgs e)
    {
        try
        {
            long CandidateID = 0;
            string sMessage = string.Empty;

            clsCandidate objCandidates = new clsCandidate();
            objCandidates.CandidateID = ViewState["CandidateID"].ToInt64();
            foreach (DataListItem item in dlAllJobs.Items)
            {
                Label lblJobStatus = (Label)item.FindControl("lblJobStatus");
                if (rblOffer.SelectedValue == "1")
                    objCandidates.OfferStatusID = (int)CandidateStatus.OfferAccepted;
                else
                    objCandidates.OfferStatusID = (int)CandidateStatus.OfferRejected;

                objCandidates.Remarks = txtRejectRemarks.Text;
                CandidateID = objCandidates.UpdateOfferStatus();

                if (CandidateID > 0)
                {
                    if (rblOffer.SelectedValue == "1")
                    {
                        ancOfferAcceptance.Enabled = false;
                        lblJobStatus.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("تقدم مقبول ") : ("Offer Accepted"); //"Offer Accepted";
                        sMessage = clsUserMaster.GetCulture() == "ar-AE" ? ("قبلت العرض بنجاح. ") : ("Offer accepted successfully."); //"Offer accepted successfully.";
                    }
                    else
                    {
                        ancOfferAcceptance.Enabled = false;
                        lblJobStatus.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("العرض مرفوض") : ("Offer Rejected"); //"Offer Rejected";
                        sMessage = clsUserMaster.GetCulture() == "ar-AE" ? ("رفض العرض بنجاح ") : ("Offer rejected successfully."); //"Offer rejected successfully.";
                    }
                    msgs.InformationalMessage(sMessage);
                    mpeMessage.Show();
                }
            }
            BindDetails();
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnVenueReferenece_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlVenue != null)
            {
                ReferenceNew1.ClearAll();
                ReferenceNew1.TableName = "HRVenueReference";
                ReferenceNew1.DataTextField = "Venue";
                ReferenceNew1.DataTextFieldArabic = "VenueArb";
                ReferenceNew1.DataValueField = "VenueID";
                ReferenceNew1.FunctionName = "LoadVenue";
                ReferenceNew1.SelectedValue = ddlVenue.SelectedValue;
                ReferenceNew1.DisplayName = GetLocalResourceObject("Venue.Text").ToString();
                ReferenceNew1.PopulateData();

                mdlPopUpReference.Show();
                updModalPopUp.Update();
            }
        }
        catch { }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Save();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        BindDetails();
    }

    #endregion Events

    #region Methods

    private void BindInitials()
    {
        objIndividualCandidateJobs = new clsIndividualCandidateJobs();
        ViewState["CandidateID"] = objIndividualCandidateJobs.GetCandidateID();
        BindDetails();
    }

    public void BindDetails()
    {
        try
        {
            objIndividualCandidateJobs = new clsIndividualCandidateJobs();

            objIndividualCandidateJobs.CandidateID = ViewState["CandidateID"].ToInt32();
            DataTable dt = objIndividualCandidateJobs.GetAllJobs();
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["JobId"].ToInt32() > 0)
                {
                    divNoData.Visible = false;
                    dlAllJobs.DataSource = dt;
                    dlAllJobs.DataBind();
                    if (dt.Rows[0]["CandidateStatusID"].ToInt32() == 6)
                    {
                        foreach (DataListItem item in dlAllJobs.Items)
                        {
                            ImageButton imgViewOffer = (ImageButton)item.FindControl("imgViewOffer");
                            //Button btnTicketDetails = (Button)item.FindControl("btnTicketDetails");
                            imgViewOffer.Visible = true;
                            //btnTicketDetails.Visible = false;
                            divOfferAcceptance.Style["display"] = divViewOfferLetter.Style["display"] = ViewOffer.Style["display"] = "none";
                            divTicketDetails.Style["display"] = divEditTicketDetails.Style["display"] = divDocumentAttach.Style["display"] = "none";
                            divViewAll.Style["display"] = "block";
                        }
                    }
                    else if (dt.Rows[0]["CandidateStatusID"].ToInt32() == 7 || dt.Rows[0]["CandidateStatusID"].ToInt32() == 9)
                    {
                        foreach (DataListItem item in dlAllJobs.Items)
                        {
                            ImageButton imgViewOffer = (ImageButton)item.FindControl("imgViewOffer");
                            //Button btnTicketDetails = (Button)item.FindControl("btnTicketDetails");
                            imgViewOffer.Visible = false;
                            //btnTicketDetails.Visible = true;
                            divOfferAcceptance.Style["display"] = divViewOfferLetter.Style["display"] = divDocumentAttach.Style["display"] = "none";
                            ViewOffer.Style["display"] = divTicketDetails.Style["display"] = divEditTicketDetails.Style["display"] = "none";
                            divViewAll.Style["display"] = "block";
                        }
                    }
                    else
                    {
                        foreach (DataListItem item in dlAllJobs.Items)
                        {
                            ImageButton imgViewOffer = (ImageButton)item.FindControl("imgViewOffer");
                            //Button btnTicketDetails = (Button)item.FindControl("btnTicketDetails");
                            imgViewOffer.Visible = false;//btnTicketDetails.Visible = 
                            divOfferAcceptance.Style["display"] = divViewOfferLetter.Style["display"] = divDocumentAttach.Style["display"] = "none";
                            ViewOffer.Style["display"] = divTicketDetails.Style["display"] = divEditTicketDetails.Style["display"] = "none";
                            divViewAll.Style["display"] = "block";
                        }
                    }
                }
                else
                {
                    dlAllJobs.Visible = false;
                    divNoData.InnerHtml = clsUserMaster.GetCulture() == "ar-AE" ? ("لديك لا وظائف في حسابك") : ("You have No Jobs in your Account"); //"You have No Jobs in your Account";
                    divNoData.Visible = true;
                }
            }
            else
            {
                dlAllJobs.Visible = false;
                divNoData.InnerHtml = clsUserMaster.GetCulture() == "ar-AE" ? ("لديك لا وظائف في حسابك") : ("No Jobs Found"); //"No Jobs Found";
                divNoData.Visible = true;
            }
            updViewAll.Update();
            updViewOffer.Update();
            updTicketDetails.Update();
            upnlEditTicketDetails.Update();
        }
        catch (Exception)
        { }
    }

    public void ViewOfferletter(DataListItem e)
    {
        objIndividualCandidateJobs = new clsIndividualCandidateJobs();
        ImageButton imgViewOffer = (ImageButton)e.FindControl("imgViewOffer");

        divViewOfferLetter.InnerHtml = objIndividualCandidateJobs.GetHeader() + OfferLetterView(Convert.ToInt32(dlAllJobs.DataKeys[e.ItemIndex]));
        divViewOfferLetter.Style["display"] = "block";
        ViewOffer.Style["display"] = "block";
        divViewAll.Style["display"] = "none";
        //updViewAll.Update();
        updViewOffer.Update();
    }

    public string OfferLetterView(int iOfferId)
    {
        clsOfferLetter objOfferLetter = new clsOfferLetter();
        clsCommon objCommon = new clsCommon();
        clsXML objXML = new clsXML();

        string sAllowances = string.Empty;
        string sDeductions = string.Empty;

        objXML.AttributeId = iOfferId;

        string sOfferletter = objXML.GetOfferLetterXml();

        if (sOfferletter == "Offer letter not available")
            ancOfferAcceptance.Visible = false;
        else
            ancOfferAcceptance.Visible = true;

        objOfferLetter.OfferId = iOfferId;

        DataSet ds = objOfferLetter.GetSavedOfferLetter();

        DataTable dtOfferletter = ds.Tables[0];
        DataTable dtOfferDetails = ds.Tables[1];
        DataTable dtJobDetails = ds.Tables[2];

        MailDefinition mdSendOffer = new MailDefinition();
        //ListDictionary ldReplacements = new ListDictionary();
        MailMessage mmContent;


        ListDictionary ldReplacements = new ListDictionary();

        ldReplacements.Add("<%Candidate Name%>", Convert.ToString(dtOfferletter.Rows[0]["Candidate"]));
        ldReplacements.Add("<%Company Name%>", Convert.ToString(dtOfferletter.Rows[0]["CompanyName"]));
        ldReplacements.Add("<%Job Title%>", Convert.ToString(dtOfferletter.Rows[0]["Vacancy"]));
        ldReplacements.Add("<%Gross Salary%>", Convert.ToString(dtOfferletter.Rows[0]["PerAnnum"]));
        ldReplacements.Add("<%Basic Pay%>", Convert.ToString(dtOfferletter.Rows[0]["BasicPay"]));
        ldReplacements.Add("<%Payment Mode%>", Convert.ToString(dtOfferletter.Rows[0]["PaymentMode"]));
        ldReplacements.Add("<%Sent Date%>", Convert.ToString(dtOfferletter.Rows[0]["SentDate"]));
        ldReplacements.Add("<%Expected Join Date%>", Convert.ToString(dtOfferletter.Rows[0]["DateofJoining"]));

        ldReplacements.Add("<%Place of Work%>", dtJobDetails.Rows[0]["Placeofwork"].ToString());
        ldReplacements.Add("<%Daily Working hours%>", dtJobDetails.Rows[0]["DailyWorkingHour"].ToString());
        ldReplacements.Add("<%Weekly working days%>", dtJobDetails.Rows[0]["Weeklyworkingdays"].ToString());
        ldReplacements.Add("<%Contract Period%>", dtJobDetails.Rows[0]["ContractPeriod"].ToString());
        ldReplacements.Add("<%Accomodation Available%>", dtJobDetails.Rows[0]["IsAccomodation"].ToString());
        ldReplacements.Add("<%Transportation Available%>", dtJobDetails.Rows[0]["Istransportation"].ToString());
        ldReplacements.Add("<%Air Ticket%>", dtJobDetails.Rows[0]["AirTicket"].ToString());
        ldReplacements.Add("<%Annual Leave days%>", dtJobDetails.Rows[0]["AnnualLeaveDays"].ToString());
        ldReplacements.Add("<%Legal Terms%>", dtOfferletter.Rows[0]["Terms"].ToString());
        ldReplacements.Add("<%Department%>", dtJobDetails.Rows[0]["Department"].ToString());
        ldReplacements.Add("<%Designation%>", dtOfferletter.Rows[0]["Designation"].ToString());
        ldReplacements.Add("<%EmploymentType%>", dtJobDetails.Rows[0]["EmploymentType"].ToString());

        mdSendOffer.From = "info@HrPower.com";
        mdSendOffer.Priority = MailPriority.High;

        string sMessage = sOfferletter.Replace("{$$", "<%");
        sMessage = sMessage.Replace("$$}", "%>");

        mmContent = mdSendOffer.CreateMailMessage(Convert.ToString(dtOfferletter.Rows[0]["Email"]), ldReplacements, sMessage, this);
        return mmContent.Body;
    }

    private void DisplayTicketDetails(int CandidateID)
    {
        objIndividualCandidateJobs = new clsIndividualCandidateJobs();
        DataSet ds = objIndividualCandidateJobs.GetTicketDetails(CandidateID);
        if (ds.Tables.Count > 0)
        {
            DataTable dtView = ds.Tables[0];
            ViewState["Ticket"] = dtView;
            if (dtView.Rows.Count > 0)
            {
                dlSingleView.DataSource = dtView;
                dlSingleView.DataBind();
                divViewAll.Style["display"] = divEditTicketDetails.Style["display"] = divDocumentAttach.Style["display"] = "none";
                divTicketDetails.Style["display"] = "block";
                updTicketDetails.Update();
            }
        }
    }

    public void LoadVenue()
    {
        string str = string.Empty;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        ddlVenue.DataSource = clsVisaTicketProcessing.GetVenue();
        ddlVenue.DataBind();
        ddlVenue.Items.Insert(0, new ListItem(str, "-1"));
        this.SetSelectedIndex(this.ddlVenue);
        upnlVenue.Update();
    }

    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    void ReferenceControlNew1_onRefresh()
    {
        mdlPopUpReference.Show();
    }

    private void Save()
    {
        clsVisaTicketProcessing objTicket = new clsVisaTicketProcessing();
        clsDocumentAlert objAlert = new clsDocumentAlert();
        string msg = string.Empty;

        objTicket.Arrivaldate = clsCommon.Convert2DateTime(txtArrival.Text.Trim());
        objTicket.ArrivalTime = txtArrivalTime.Text.Trim();
        objTicket.VenueID = ddlVenue.SelectedValue.ToInt32();
        objTicket.PickUpReq = rblPickUpReq.SelectedValue.ToInt32() == 1 ? true : false;
        objTicket.TicketAmount = Convert.ToDecimal(txtTicketAmount.Text.Trim());
        objTicket.TicketRemarks = txtTicketRemarks.Text.Trim();
        int Rowcount = objTicket.UpdateTicketDetails(ViewState["TID"].ToInt32());
        if (Rowcount > 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("المحفوظة بنجاح") : ("Successfully Saved");
            objAlert.AlertMessage(Convert.ToInt32(DocumentType.CandidateArrivalDate), "Candidate Arrival Date", "Candidate Arrival Date", ViewState["TID"].ToInt32(), "Ticket", clsCommon.Convert2DateTime(txtArrival.Text), "Candidate", ViewState["CandidateID"].ToInt32(), hfCandidateName.Value, false);
        }
        else
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("المحفوظة بنجاح") : ("Failed to Save");
        }
        msgs.InformationalMessage(msg);
        mpeMessage.Show();
    }

    protected void dlCandidateDocument_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Image img = (Image)e.Item.FindControl("img");
            Image imgDelete = (Image)e.Item.FindControl("imgDelete");
            HiddenField hfIsResume = (HiddenField)e.Item.FindControl("hfIsResume");

            if (img != null && hfIsResume != null)
            {
                imgDelete.Visible = (hfIsResume.Value != "Resume");
                if (!IsValidImage(img.ImageUrl))
                {
                    img.ImageUrl = "../Images/files.png";
                }
            }
        }
    }
    protected void fuCandidate_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        try
        {
            string FileName = string.Empty;
            string ActualFileName = string.Empty;

            if (!Directory.Exists(Server.MapPath("~/Documents/")))
                Directory.CreateDirectory(Server.MapPath("~/Documents/"));

            if (!Directory.Exists(Server.MapPath("~/Documents/Others/")))
                Directory.CreateDirectory(Server.MapPath("~/Documents/Others/"));

            if (fuCandidate.HasFile)
            {
                ActualFileName = fuCandidate.FileName;
                FileName = DateTime.Now.Ticks.ToString() + new Random().Next(0, 1000).ToString() + Path.GetExtension(fuCandidate.FileName);
                Session["FileName"] = FileName;
                fuCandidate.SaveAs(Server.MapPath("~/Documents/Others/") + FileName);
            }
        }
        catch (Exception)
        { }
    }

    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            DataTable dt = null;
            string FileName = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;

            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["FileName"].ToString() == FileName)
                {
                    if (File.Exists(Server.MapPath(dt.Rows[i]["Location"].ToString())))
                        File.Delete(Server.MapPath(dt.Rows[i]["Location"].ToString()));

                    dt.Rows.Remove(dt.Rows[i]);
                    new clsCandidate().DeleteCandidateAttachedDocuments(ViewState["CandidateID"].ToInt64(), FileName);
                }
            }
            dlCandidateDocument.DataSource = dt;
            dlCandidateDocument.DataBind();
            dlCandidateDocument.Visible = dlCandidateDocument.Items.Count > 0 ? true : false;
        }
        catch (Exception)
        { }
    }

    protected void img_Click(object sender, ImageClickEventArgs e)
    {
        string FileName = ((ImageButton)sender).CommandArgument.ToString();
        if (IsValidImage(FileName))
        {
            //string Page = "../Public/Download.aspx?FileName=" + ((ImageButton)sender).CommandArgument.ToString() + "&type=Candidate";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Open", "window.open('" + FileName + "');", true);
        }
        else
        {
            //string Page = "../Public/Download.aspx?FileName=" + ((ImageButton)sender).CommandArgument.ToString() + "&type=Candidate";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Open", "window.open('" + FileName + "','','width=400,height=600');", true);
        }
    }

    private bool IsValidImage(string FileName)
    {
        try
        {
            string ext = FileName.Remove(0, FileName.LastIndexOf(".")).Trim();
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif" || ext.ToLower() == ".bmp" || ext.ToLower() == ".png" || ext.ToLower() == ".tif")
                return true;
            else
                return false;
        }
        catch (Exception) { return false; }
    }

    protected void btnAttachCandidateDocument_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuCandidate.HasFile)
                fuCandidate.ClearAllFilesFromPersistedStore();
            else
            {
                string sMsg = clsGlobalization.IsArabicCulture() ? "يرجى إرفاق مستند" : "Please attach a document";
                msgs.InformationalMessage(sMsg);
                this.mpeMessage.Show();
                return;
            }

            DataTable dt = null;
            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);
            else
            {
                dt = new DataTable();
                dt.Columns.Add("FileName");
                dt.Columns.Add("DocumentName");
                dt.Columns.Add("Location");
                dt.Columns.Add("Type");
            }

            DataRow dr = dt.NewRow();
            dr["FileName"] = Session["FileName"];
            dr["DocumentName"] = txtCandidateDocumentName.Text;
            dr["Location"] = "../Documents/Others/" + Session["FileName"].ToString();
            dr["Type"] = "";
            dt.Rows.Add(dr);

            dlCandidateDocument.DataSource = dt;
            dlCandidateDocument.DataBind();
            dlCandidateDocument.Visible = dlCandidateDocument.Items.Count > 0 ? true : false;
            ViewState["Documents"] = dt;

            if (dt.Rows.Count > 0)
                dvAttachedDocuments.Visible = true;
            else
                dvAttachedDocuments.Visible = false;
            txtCandidateDocumentName.Text = "";
        }
        catch (Exception)
        { }
    }

    protected void btnSaveDocuments_Click(object sender, EventArgs e)
    {
        SaveCandidateDocuments();
    }

    private void SaveCandidateDocuments()
    {
        try
        {
            objclsCandidate = new clsCandidate();
            DataTable dt = null;
            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);

            if (dt != null && dt.Rows.Count > 0)
            {
                new clsCandidate().DeleteCandidateAttachedDocuments(ViewState["CandidateID"].ToInt64(), string.Empty);
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["Type"].ToString() == "")
                        new clsCandidate().SaveCandidateDocuments(dr["FileName"].ToString(), dr["DocumentName"].ToString(), ViewState["CandidateID"].ToInt64());
                }
            }
        }
        catch (Exception)
        { }
    }

    #endregion Methods

    protected void AttachDocuments_onClick(object sender, EventArgs e)
    {
        divViewAll.Style["display"] = divEditTicketDetails.Style["display"] = divTicketDetails.Style["display"] = "none";
        divDocumentAttach.Style["display"] = "block";
        GetCandidateAttachedDocuments(ViewState["CandidateID"].ToInt64());
    }
    private void GetCandidateAttachedDocuments(Int64 CandidateID)
    {
        try
        {
            DataTable dt = clsCandidate.GetCandidateAttachedDocuments(CandidateID);

            ViewState["Documents"] = dt;
            dlCandidateDocument.DataSource = dt;
            dlCandidateDocument.DataBind();
            if (dt.Rows.Count > 0)
                dvAttachedDocuments.Visible = true;
            else
                dvAttachedDocuments.Visible = false;
        }
        catch (Exception ex)
        { }
    }

    protected void DispTicketDetails_onClick(object sender, EventArgs e)
    {
        for (int i = 0; i < dlAllJobs.Items.Count; i++)
        {
            HiddenField hfStatusID = (HiddenField)dlAllJobs.Items[i].FindControl("hfStatusID");
            if (hfStatusID.Value == "7" || hfStatusID.Value == "9")
            {
                DisplayTicketDetails(ViewState["CandidateID"].ToInt32());
            }
        }
    }

    protected void dlSingleView_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Edit":
                ViewState["TID"] = e.CommandArgument.ToInt32();
                EditTicketDetails(e.CommandArgument.ToInt32());
                break;
        }
    }
    protected void dlSingleView_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            GridView gvVisa = (GridView)e.Item.FindControl("gvVisa");
            if (gvVisa != null)
            {
                objIndividualCandidateJobs = new clsIndividualCandidateJobs();
                DataSet ds = objIndividualCandidateJobs.GetTicketDetails(ViewState["CandidateID"].ToInt32());

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[1];
                    if (dt.Rows.Count > 0)
                    {
                        gvVisa.DataSource = dt;
                        gvVisa.DataBind();
                    }
                }
            }
        }
    }
    private void EditTicketDetails(int VisaTicketProcessID)
    {
        divTicketDetails.Style["display"] = "none";
        divEditTicketDetails.Style["display"] = "block";
        LoadVenue();
        BindTicketDetails(VisaTicketProcessID);
        upnlEditTicketDetails.Update();
    }

    private void BindTicketDetails(int VisaTicketProcessID)
    {
        DataTable dt = (DataTable)ViewState["Ticket"];
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["TicketSentBool"].ToBoolean())
            {
                hfCandidateName.Value = dt.Rows[0]["Candidate"].ToString();
                txtArrival.Text = dt.Rows[0]["ArrivalDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                txtArrivalTime.Text = dt.Rows[0]["ArrivalTime"].ToString();
                ddlVenue.SelectedValue = dt.Rows[0]["VenueID"].ToString();
                rblPickUpReq.SelectedValue = (dt.Rows[0]["PickUpReq"].ToString() == "True") ? "1" : "0";
                txtTicketAmount.Text = dt.Rows[0]["TicketAmount"].ToString();
                txtTicketRemarks.Text = dt.Rows[0]["TicketRemarks"].ToString();
            }
            else
            {
                hfCandidateName.Value = "";
                txtArrival.Text = string.Empty;
                txtArrivalTime.Text = string.Empty;
                ddlVenue.SelectedIndex = -1;
                rblPickUpReq.SelectedValue = "0";
                txtTicketAmount.Text = "0";
                txtTicketRemarks.Text = string.Empty;
            }
        }
    }
}
