﻿namespace MyPayfriend
{
    partial class NavigatorArb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("شركة", 5);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("عامل", 1);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NavigatorArb));
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("إعداد شركة", 0);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("إعداد الموظف", 1);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("تقارير", 2);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("تجهيز كشوف المرتبات", 3);
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.barTopSearch = new DevComponents.DotNetBar.Bar();
            this.txtTopSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.controlContainerItem1 = new DevComponents.DotNetBar.ControlContainerItem();
            this.btnTopDocumentSearch = new DevComponents.DotNetBar.ButtonItem();
            this.navigationPane1 = new DevComponents.DotNetBar.NavigationPane();
            this.navigationPanePanel10 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.lstDocumentsLeft = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader(0);
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader(1);
            this.ImgListLargeNavigator = new System.Windows.Forms.ImageList(this.components);
            this.btnDocuments = new DevComponents.DotNetBar.ButtonItem();
            this.navigationPanePanel1 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.AdvCompanyTree = new DevComponents.AdvTree.AdvTree();
            this.ElementStyle5 = new DevComponents.DotNetBar.ElementStyle();
            this.ImgListNavigator = new System.Windows.Forms.ImageList(this.components);
            this.NodeConnector2 = new DevComponents.AdvTree.NodeConnector();
            this.btnNavCompany = new DevComponents.DotNetBar.ButtonItem();
            this.navigationPanePanel2 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.AdvEmployeeTree = new DevComponents.AdvTree.AdvTree();
            this.elementStyle1 = new DevComponents.DotNetBar.ElementStyle();
            this.nodeConnector1 = new DevComponents.AdvTree.NodeConnector();
            this.btnClient = new DevComponents.DotNetBar.ButtonItem();
            this.navigationPanePanel4 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.lstEasyViewLeft = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader(0);
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader(1);
            this.btnEasyview = new DevComponents.DotNetBar.ButtonItem();
            this.nodeConnector6 = new DevComponents.AdvTree.NodeConnector();
            this.elementStyle4 = new DevComponents.DotNetBar.ElementStyle();
            this.btnNavTask = new DevComponents.DotNetBar.ButtonItem();
            this.ImageListDocuments = new System.Windows.Forms.ImageList(this.components);
            this.panelMiddle = new DevComponents.DotNetBar.PanelEx();
            this.AdvDocumentTree = new DevComponents.AdvTree.AdvTree();
            this.nodeConnector8 = new DevComponents.AdvTree.NodeConnector();
            this.elementStyle6 = new DevComponents.DotNetBar.ElementStyle();
            this.dgvList = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.clmImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.pnlCount = new DevComponents.DotNetBar.PanelEx();
            this.ExpEmployeeFilteration = new DevComponents.DotNetBar.ExpandablePanel();
            this.lblShowBy = new DevComponents.DotNetBar.LabelX();
            this.CboEmpFilter = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.CboEmpFilterTypes = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboEmpWorkstatus = new DevComponents.DotNetBar.Controls.ComboTree();
            this.ImageListEmployee = new System.Windows.Forms.ImageList(this.components);
            this.All = new DevComponents.AdvTree.Node();
            this.Filled = new DevComponents.AdvTree.Node();
            this.PartiallyFilled = new DevComponents.AdvTree.Node();
            this.Vacant = new DevComponents.AdvTree.Node();
            this.expandableSplitter1 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.panelRight = new DevComponents.DotNetBar.PanelEx();
            this.pnlUnSupportedFileTypes = new DevComponents.DotNetBar.PanelEx();
            this.LblMess2 = new System.Windows.Forms.Label();
            this.LblMess1 = new System.Windows.Forms.Label();
            this.btnOpenApplication = new System.Windows.Forms.Button();
            this.tabEmployee = new DevComponents.DotNetBar.TabControl();
            this.ImageControl = new Ic.ImageControl();
            this.webEasyview = new System.Windows.Forms.WebBrowser();
            this.navigationPanePanel3 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.advTree1 = new DevComponents.AdvTree.AdvTree();
            this.elementStyle2 = new DevComponents.DotNetBar.ElementStyle();
            this.nodeConnector3 = new DevComponents.AdvTree.NodeConnector();
            this.navigationPanePanel6 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.advTree2 = new DevComponents.AdvTree.AdvTree();
            this.elementStyle3 = new DevComponents.DotNetBar.ElementStyle();
            this.nodeConnector4 = new DevComponents.AdvTree.NodeConnector();
            this.navigationPanePanel7 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.advTree3 = new DevComponents.AdvTree.AdvTree();
            this.nodeConnector5 = new DevComponents.AdvTree.NodeConnector();
            this.navigationPanePanel8 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.advTree4 = new DevComponents.AdvTree.AdvTree();
            this.elementStyle7 = new DevComponents.DotNetBar.ElementStyle();
            this.nodeConnector7 = new DevComponents.AdvTree.NodeConnector();
            this.navigationPanePanel9 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader(0);
            this.navigationPanePanel11 = new DevComponents.DotNetBar.NavigationPanePanel();
            this.advTree6 = new DevComponents.AdvTree.AdvTree();
            this.elementStyle9 = new DevComponents.DotNetBar.ElementStyle();
            this.nodeConnector9 = new DevComponents.AdvTree.NodeConnector();
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite7 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.barTopSearch)).BeginInit();
            this.barTopSearch.SuspendLayout();
            this.navigationPane1.SuspendLayout();
            this.navigationPanePanel10.SuspendLayout();
            this.navigationPanePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AdvCompanyTree)).BeginInit();
            this.navigationPanePanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AdvEmployeeTree)).BeginInit();
            this.navigationPanePanel4.SuspendLayout();
            this.panelMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AdvDocumentTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.ExpEmployeeFilteration.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.pnlUnSupportedFileTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabEmployee)).BeginInit();
            this.navigationPanePanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advTree1)).BeginInit();
            this.navigationPanePanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advTree2)).BeginInit();
            this.navigationPanePanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advTree3)).BeginInit();
            this.navigationPanePanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advTree4)).BeginInit();
            this.navigationPanePanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advTree6)).BeginInit();
            this.dockSite7.SuspendLayout();
            this.SuspendLayout();
            // 
            // barTopSearch
            // 
            this.barTopSearch.AccessibleDescription = "DotNetBar Bar (barTopSearch)";
            this.barTopSearch.AccessibleName = "DotNetBar Bar";
            this.barTopSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.barTopSearch.ColorScheme.BarCaptionBackground = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.barTopSearch.ColorScheme.BarCaptionBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.barTopSearch.ColorScheme.ExplorerBarBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(172)))), ((int)(((byte)(89)))));
            this.barTopSearch.ColorScheme.ExplorerBarBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(121)))), ((int)(((byte)(0)))));
            this.barTopSearch.ColorScheme.ItemCheckedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
            this.barTopSearch.ColorScheme.ItemHotBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(125)))));
            this.barTopSearch.ColorScheme.ItemHotBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.barTopSearch.ColorScheme.ItemPressedBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.barTopSearch.ColorScheme.ItemPressedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.barTopSearch.Controls.Add(this.txtTopSearch);
            this.barTopSearch.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.barTopSearch.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.barTopSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.controlContainerItem1,
            this.btnTopDocumentSearch});
            this.barTopSearch.Location = new System.Drawing.Point(0, 0);
            this.barTopSearch.Name = "barTopSearch";
            this.barTopSearch.Size = new System.Drawing.Size(1303, 28);
            this.barTopSearch.Stretch = true;
            this.barTopSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.barTopSearch.TabIndex = 5;
            this.barTopSearch.TabStop = false;
            this.barTopSearch.Text = "Search";
            // 
            // txtTopSearch
            // 
            this.txtTopSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTopSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtTopSearch.Border.Class = "TextBoxBorder";
            this.txtTopSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTopSearch.Location = new System.Drawing.Point(986, 2);
            this.txtTopSearch.MaxLength = 100;
            this.txtTopSearch.Name = "txtTopSearch";
            this.txtTopSearch.Size = new System.Drawing.Size(307, 23);
            this.txtTopSearch.TabIndex = 1;
            this.txtTopSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtTopSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            this.txtTopSearch.TextChanged += new System.EventHandler(this.txtTopSearch_TextChanged);
            // 
            // controlContainerItem1
            // 
            this.controlContainerItem1.AllowItemResize = false;
            this.controlContainerItem1.Control = this.txtTopSearch;
            this.controlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem1.Name = "controlContainerItem1";
            // 
            // btnTopDocumentSearch
            // 
            this.btnTopDocumentSearch.Image = global::MyPayfriend.Properties.Resources.Go;
            this.btnTopDocumentSearch.Name = "btnTopDocumentSearch";
            this.btnTopDocumentSearch.Click += new System.EventHandler(this.btnDocumentSearch_Click);
            // 
            // navigationPane1
            // 
            this.navigationPane1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.navigationPane1.CanCollapse = true;
            this.navigationPane1.ConfigureAddRemoveVisible = false;
            this.navigationPane1.ConfigureItemVisible = false;
            this.navigationPane1.ConfigureNavOptionsVisible = false;
            this.navigationPane1.ConfigureShowHideVisible = false;
            this.navigationPane1.Controls.Add(this.navigationPanePanel10);
            this.navigationPane1.Controls.Add(this.navigationPanePanel1);
            this.navigationPane1.Controls.Add(this.navigationPanePanel2);
            this.navigationPane1.Controls.Add(this.navigationPanePanel4);
            this.navigationPane1.Dock = System.Windows.Forms.DockStyle.Left;
            this.navigationPane1.ItemPaddingBottom = 2;
            this.navigationPane1.ItemPaddingTop = 2;
            this.navigationPane1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnNavCompany,
            this.btnClient,
            this.btnEasyview,
            this.btnDocuments});
            this.navigationPane1.Location = new System.Drawing.Point(0, 29);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.NavigationBarHeight = 140;
            this.navigationPane1.Padding = new System.Windows.Forms.Padding(1);
            this.navigationPane1.Size = new System.Drawing.Size(245, 483);
            this.navigationPane1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPane1.TabIndex = 23;
            // 
            // 
            // 
            this.navigationPane1.TitlePanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPane1.TitlePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.navigationPane1.TitlePanel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navigationPane1.TitlePanel.Location = new System.Drawing.Point(1, 1);
            this.navigationPane1.TitlePanel.Name = "panelEx1";
            this.navigationPane1.TitlePanel.Size = new System.Drawing.Size(243, 24);
            this.navigationPane1.TitlePanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.navigationPane1.TitlePanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.navigationPane1.TitlePanel.Style.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.navigationPane1.TitlePanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPane1.TitlePanel.Style.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.navigationPane1.TitlePanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.navigationPane1.TitlePanel.Style.GradientAngle = 90;
            this.navigationPane1.TitlePanel.Style.MarginLeft = 4;
            this.navigationPane1.TitlePanel.TabIndex = 0;
            this.navigationPane1.TitlePanel.Text = " وثائق";
            this.navigationPane1.TitlePanel.Visible = false;
            // 
            // navigationPanePanel10
            // 
            this.navigationPanePanel10.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel10.Controls.Add(this.lstDocumentsLeft);
            this.navigationPanePanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel10.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel10.Name = "navigationPanePanel10";
            this.navigationPanePanel10.ParentItem = this.btnDocuments;
            this.navigationPanePanel10.Size = new System.Drawing.Size(243, 341);
            this.navigationPanePanel10.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel10.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel10.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel10.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel10.Style.GradientAngle = 90;
            this.navigationPanePanel10.TabIndex = 7;
            // 
            // lstDocumentsLeft
            // 
            this.lstDocumentsLeft.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            // 
            // 
            // 
            this.lstDocumentsLeft.Border.Class = "ListViewBorder";
            this.lstDocumentsLeft.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lstDocumentsLeft.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.lstDocumentsLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDocumentsLeft.GridLines = true;
            listViewItem1.ToolTipText = "شركة";
            listViewItem2.ToolTipText = "عامل";
            this.lstDocumentsLeft.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2});
            this.lstDocumentsLeft.LargeImageList = this.ImgListLargeNavigator;
            this.lstDocumentsLeft.Location = new System.Drawing.Point(0, 0);
            this.lstDocumentsLeft.Name = "lstDocumentsLeft";
            this.lstDocumentsLeft.Size = new System.Drawing.Size(243, 341);
            this.lstDocumentsLeft.TabIndex = 4;
            this.lstDocumentsLeft.UseCompatibleStateImageBehavior = false;
            this.lstDocumentsLeft.SelectedIndexChanged += new System.EventHandler(this.lstDocumentsLeft_SelectedIndexChanged);
            // 
            // ImgListLargeNavigator
            // 
            this.ImgListLargeNavigator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgListLargeNavigator.ImageStream")));
            this.ImgListLargeNavigator.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgListLargeNavigator.Images.SetKeyName(0, "Comapany_information.ico");
            this.ImgListLargeNavigator.Images.SetKeyName(1, "Employee Information.ico");
            this.ImgListLargeNavigator.Images.SetKeyName(2, "Account_Setup.ico");
            this.ImgListLargeNavigator.Images.SetKeyName(3, "Salary_setup.ico");
            this.ImgListLargeNavigator.Images.SetKeyName(4, "EasyView.ico");
            this.ImgListLargeNavigator.Images.SetKeyName(5, "Company.png");
            this.ImgListLargeNavigator.Images.SetKeyName(6, "Executive.png");
            // 
            // btnDocuments
            // 
            this.btnDocuments.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDocuments.Checked = true;
            this.btnDocuments.Image = global::MyPayfriend.Properties.Resources.document_register1;
            this.btnDocuments.Name = "btnDocuments";
            this.btnDocuments.OptionGroup = "navBar";
            this.btnDocuments.Text = " وثائق";
            this.btnDocuments.Tooltip = " وثائق";
            this.btnDocuments.Click += new System.EventHandler(this.btnNavigator_Click);
            // 
            // navigationPanePanel1
            // 
            this.navigationPanePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel1.Controls.Add(this.AdvCompanyTree);
            this.navigationPanePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel1.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel1.Name = "navigationPanePanel1";
            this.navigationPanePanel1.ParentItem = this.btnNavCompany;
            this.navigationPanePanel1.Size = new System.Drawing.Size(243, 341);
            this.navigationPanePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.navigationPanePanel1.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile;
            this.navigationPanePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.navigationPanePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel1.Style.GradientAngle = 90;
            this.navigationPanePanel1.StyleMouseDown.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel1.StyleMouseOver.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel1.TabIndex = 2;
            // 
            // AdvCompanyTree
            // 
            this.AdvCompanyTree.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.AdvCompanyTree.AllowDrop = true;
            this.AdvCompanyTree.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.AdvCompanyTree.BackgroundStyle.Class = "TreeBorderKey";
            this.AdvCompanyTree.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.AdvCompanyTree.ColumnsVisible = false;
            this.AdvCompanyTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdvCompanyTree.DragDropEnabled = false;
            this.AdvCompanyTree.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle;
            this.AdvCompanyTree.ExpandWidth = 14;
            this.AdvCompanyTree.GroupNodeStyle = this.ElementStyle5;
            this.AdvCompanyTree.ImageList = this.ImgListNavigator;
            this.AdvCompanyTree.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.AdvCompanyTree.Location = new System.Drawing.Point(0, 0);
            this.AdvCompanyTree.MultiSelectRule = DevComponents.AdvTree.eMultiSelectRule.AnyNode;
            this.AdvCompanyTree.Name = "AdvCompanyTree";
            this.AdvCompanyTree.NodesConnector = this.NodeConnector2;
            this.AdvCompanyTree.NodeStyle = this.ElementStyle5;
            this.AdvCompanyTree.PathSeparator = ";";
            this.AdvCompanyTree.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AdvCompanyTree.Size = new System.Drawing.Size(243, 341);
            this.AdvCompanyTree.TabIndex = 1;
            this.AdvCompanyTree.NodeMouseHover += new DevComponents.AdvTree.TreeNodeMouseEventHandler(this.AdvCompanyTree_NodeMouseHover);
            this.AdvCompanyTree.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AdvCompanyTree_MouseClick);
            this.AdvCompanyTree.NodeClick += new DevComponents.AdvTree.TreeNodeMouseEventHandler(this.AdvCompanyTree_NodeClick);
            this.AdvCompanyTree.NodeMouseEnter += new DevComponents.AdvTree.TreeNodeMouseEventHandler(this.AdvCompanyTree_NodeMouseEnter);
            // 
            // ElementStyle5
            // 
            this.ElementStyle5.Class = "";
            this.ElementStyle5.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ElementStyle5.Name = "ElementStyle5";
            this.ElementStyle5.PaddingTop = 5;
            this.ElementStyle5.TextColor = System.Drawing.SystemColors.ControlText;
            this.ElementStyle5.TextTrimming = DevComponents.DotNetBar.eStyleTextTrimming.None;
            this.ElementStyle5.WordWrap = true;
            // 
            // ImgListNavigator
            // 
            this.ImgListNavigator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgListNavigator.ImageStream")));
            this.ImgListNavigator.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgListNavigator.Images.SetKeyName(0, "Comapany_information.png");
            this.ImgListNavigator.Images.SetKeyName(1, "Company_branches.png");
            this.ImgListNavigator.Images.SetKeyName(2, "Easy_View.png");
            this.ImgListNavigator.Images.SetKeyName(3, "Employee_information.PNG");
            this.ImgListNavigator.Images.SetKeyName(4, "Leavepolicy.png");
            this.ImgListNavigator.Images.SetKeyName(5, "Shiftpolicy.png");
            this.ImgListNavigator.Images.SetKeyName(6, "Workpolicy.png");
            this.ImgListNavigator.Images.SetKeyName(7, "Policies.png");
            this.ImgListNavigator.Images.SetKeyName(8, "PreviousSalarystructure.png");
            this.ImgListNavigator.Images.SetKeyName(9, "SalaryStructure.png");
            // 
            // NodeConnector2
            // 
            this.NodeConnector2.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // btnNavCompany
            // 
            this.btnNavCompany.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnNavCompany.Image = global::MyPayfriend.Properties.Resources.Company_Information;
            this.btnNavCompany.Name = "btnNavCompany";
            this.btnNavCompany.OptionGroup = "navBar";
            this.btnNavCompany.SubItemsExpandWidth = 11;
            this.btnNavCompany.Text = "الشركة";
            this.btnNavCompany.Tooltip = "الشركة";
            this.btnNavCompany.Click += new System.EventHandler(this.btnNavigator_Click);
            // 
            // navigationPanePanel2
            // 
            this.navigationPanePanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel2.Controls.Add(this.AdvEmployeeTree);
            this.navigationPanePanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel2.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel2.Name = "navigationPanePanel2";
            this.navigationPanePanel2.ParentItem = this.btnClient;
            this.navigationPanePanel2.Size = new System.Drawing.Size(243, 341);
            this.navigationPanePanel2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.navigationPanePanel2.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile;
            this.navigationPanePanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.navigationPanePanel2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel2.Style.GradientAngle = 90;
            this.navigationPanePanel2.Style.WordWrap = true;
            this.navigationPanePanel2.StyleMouseDown.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel2.StyleMouseDown.WordWrap = true;
            this.navigationPanePanel2.StyleMouseOver.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel2.StyleMouseOver.WordWrap = true;
            this.navigationPanePanel2.TabIndex = 3;
            this.navigationPanePanel2.Text = "Drop your controls here and erase Text property";
            // 
            // AdvEmployeeTree
            // 
            this.AdvEmployeeTree.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.AdvEmployeeTree.AllowDrop = true;
            this.AdvEmployeeTree.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.AdvEmployeeTree.BackgroundStyle.Class = "TreeBorderKey";
            this.AdvEmployeeTree.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.AdvEmployeeTree.ColumnsVisible = false;
            this.AdvEmployeeTree.DisplayMembers = "CompanyID";
            this.AdvEmployeeTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdvEmployeeTree.DragDropEnabled = false;
            this.AdvEmployeeTree.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle;
            this.AdvEmployeeTree.ExpandWidth = 14;
            this.AdvEmployeeTree.GroupingMembers = "CompanyName,Branchname";
            this.AdvEmployeeTree.GroupNodeStyle = this.elementStyle1;
            this.AdvEmployeeTree.ImageList = this.ImgListNavigator;
            this.AdvEmployeeTree.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.AdvEmployeeTree.Location = new System.Drawing.Point(0, 0);
            this.AdvEmployeeTree.Name = "AdvEmployeeTree";
            this.AdvEmployeeTree.NodesConnector = this.nodeConnector1;
            this.AdvEmployeeTree.NodeStyle = this.elementStyle1;
            this.AdvEmployeeTree.PathSeparator = ";";
            this.AdvEmployeeTree.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AdvEmployeeTree.Size = new System.Drawing.Size(243, 341);
            this.AdvEmployeeTree.Styles.Add(this.elementStyle1);
            this.AdvEmployeeTree.TabIndex = 1;
            this.AdvEmployeeTree.ValueMember = "CompanyID";
            this.AdvEmployeeTree.NodeClick += new DevComponents.AdvTree.TreeNodeMouseEventHandler(this.AdvEmployeeTree_NodeClick);
            // 
            // elementStyle1
            // 
            this.elementStyle1.Class = "";
            this.elementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle1.Name = "elementStyle1";
            this.elementStyle1.PaddingTop = 5;
            this.elementStyle1.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // nodeConnector1
            // 
            this.nodeConnector1.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // btnClient
            // 
            this.btnClient.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnClient.Image = global::MyPayfriend.Properties.Resources.Employee_information;
            this.btnClient.Name = "btnClient";
            this.btnClient.OptionGroup = "navBar";
            this.btnClient.SubItemsExpandWidth = 11;
            this.btnClient.Text = "عامل";
            this.btnClient.Tooltip = "عامل";
            this.btnClient.Click += new System.EventHandler(this.btnNavigator_Click);
            // 
            // navigationPanePanel4
            // 
            this.navigationPanePanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel4.Controls.Add(this.lstEasyViewLeft);
            this.navigationPanePanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel4.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel4.Name = "navigationPanePanel4";
            this.navigationPanePanel4.ParentItem = this.btnEasyview;
            this.navigationPanePanel4.Size = new System.Drawing.Size(243, 341);
            this.navigationPanePanel4.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel4.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel4.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.navigationPanePanel4.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile;
            this.navigationPanePanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.navigationPanePanel4.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel4.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel4.Style.GradientAngle = 90;
            this.navigationPanePanel4.Style.WordWrap = true;
            this.navigationPanePanel4.StyleMouseDown.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel4.StyleMouseDown.WordWrap = true;
            this.navigationPanePanel4.StyleMouseOver.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel4.StyleMouseOver.WordWrap = true;
            this.navigationPanePanel4.TabIndex = 5;
            this.navigationPanePanel4.Text = "Drop your controls here and erase Text property";
            // 
            // lstEasyViewLeft
            // 
            this.lstEasyViewLeft.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            // 
            // 
            // 
            this.lstEasyViewLeft.Border.Class = "ListViewBorder";
            this.lstEasyViewLeft.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lstEasyViewLeft.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader5});
            this.lstEasyViewLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstEasyViewLeft.GridLines = true;
            listViewItem3.ToolTipText = "إعداد شركة";
            listViewItem4.ToolTipText = "إعداد الموظف";
            listViewItem5.ToolTipText = "تقارير";
            listViewItem6.ToolTipText = "تجهيز كشوف المرتبات";
            this.lstEasyViewLeft.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6});
            this.lstEasyViewLeft.LargeImageList = this.ImgListLargeNavigator;
            this.lstEasyViewLeft.Location = new System.Drawing.Point(0, 0);
            this.lstEasyViewLeft.Name = "lstEasyViewLeft";
            this.lstEasyViewLeft.Size = new System.Drawing.Size(243, 341);
            this.lstEasyViewLeft.TabIndex = 3;
            this.lstEasyViewLeft.UseCompatibleStateImageBehavior = false;
            this.lstEasyViewLeft.Click += new System.EventHandler(this.lstEasyViewLeft_Click);
            // 
            // btnEasyview
            // 
            this.btnEasyview.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnEasyview.Image = global::MyPayfriend.Properties.Resources.Easy_View;
            this.btnEasyview.Name = "btnEasyview";
            this.btnEasyview.OptionGroup = "navBar";
            this.btnEasyview.SubItemsExpandWidth = 11;
            this.btnEasyview.Text = "من السهل مشاهدة";
            this.btnEasyview.Tooltip = "Easy View";
            this.btnEasyview.Click += new System.EventHandler(this.btnNavigator_Click);
            // 
            // nodeConnector6
            // 
            this.nodeConnector6.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // elementStyle4
            // 
            this.elementStyle4.Class = "";
            this.elementStyle4.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle4.Name = "elementStyle4";
            this.elementStyle4.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // btnNavTask
            // 
            this.btnNavTask.Name = "btnNavTask";
            // 
            // ImageListDocuments
            // 
            this.ImageListDocuments.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListDocuments.ImageStream")));
            this.ImageListDocuments.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListDocuments.Images.SetKeyName(0, "trade16.ico");
            this.ImageListDocuments.Images.SetKeyName(1, "LeaseAgreement.ico");
            this.ImageListDocuments.Images.SetKeyName(2, "Insurance.ico");
            this.ImageListDocuments.Images.SetKeyName(3, "OtherDocument.png");
            // 
            // panelMiddle
            // 
            this.panelMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelMiddle.Controls.Add(this.AdvDocumentTree);
            this.panelMiddle.Controls.Add(this.dgvList);
            this.panelMiddle.Controls.Add(this.pnlCount);
            this.panelMiddle.Controls.Add(this.ExpEmployeeFilteration);
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMiddle.Location = new System.Drawing.Point(245, 29);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(224, 483);
            this.panelMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelMiddle.Style.GradientAngle = 90;
            this.panelMiddle.TabIndex = 24;
            this.panelMiddle.Text = "panelEx2";
            // 
            // AdvDocumentTree
            // 
            this.AdvDocumentTree.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.AdvDocumentTree.AllowDrop = true;
            this.AdvDocumentTree.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.AdvDocumentTree.BackgroundStyle.Class = "TreeBorderKey";
            this.AdvDocumentTree.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.AdvDocumentTree.ColumnsVisible = false;
            this.AdvDocumentTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdvDocumentTree.DragDropEnabled = false;
            this.AdvDocumentTree.DragDropNodeCopyEnabled = false;
            this.AdvDocumentTree.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle;
            this.AdvDocumentTree.ExpandWidth = 14;
            this.AdvDocumentTree.GridColumnLines = false;
            this.AdvDocumentTree.HotTracking = true;
            this.AdvDocumentTree.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.AdvDocumentTree.Location = new System.Drawing.Point(0, 97);
            this.AdvDocumentTree.Name = "AdvDocumentTree";
            this.AdvDocumentTree.NodesConnector = this.nodeConnector8;
            this.AdvDocumentTree.NodeStyle = this.elementStyle6;
            this.AdvDocumentTree.PathSeparator = ";";
            this.AdvDocumentTree.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AdvDocumentTree.Size = new System.Drawing.Size(224, 359);
            this.AdvDocumentTree.TabIndex = 34;
            this.AdvDocumentTree.Text = "AdvTree3";
            this.AdvDocumentTree.NodeDoubleClick += new DevComponents.AdvTree.TreeNodeMouseEventHandler(this.AdvDocumentTree_NodeDoubleClick);
            this.AdvDocumentTree.BeforeExpand += new DevComponents.AdvTree.AdvTreeNodeCancelEventHandler(this.AdvDocumentTree_BeforeExpand);
            this.AdvDocumentTree.NodeClick += new DevComponents.AdvTree.TreeNodeMouseEventHandler(this.AdvDocumentTree_NodeClick);
            // 
            // nodeConnector8
            // 
            this.nodeConnector8.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // elementStyle6
            // 
            this.elementStyle6.Class = "";
            this.elementStyle6.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle6.Name = "elementStyle6";
            this.elementStyle6.PaddingTop = 5;
            this.elementStyle6.TextColor = System.Drawing.SystemColors.ControlText;
            this.elementStyle6.TextTrimming = DevComponents.DotNetBar.eStyleTextTrimming.None;
            this.elementStyle6.WordWrap = true;
            // 
            // dgvList
            // 
            this.dgvList.AllowDrop = true;
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.BackgroundColor = System.Drawing.Color.White;
            this.dgvList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvList.ColumnHeadersVisible = false;
            this.dgvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmImage});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvList.Location = new System.Drawing.Point(0, 97);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowHeadersWidth = 10;
            this.dgvList.RowTemplate.Height = 40;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(224, 359);
            this.dgvList.TabIndex = 4;
            this.dgvList.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvList_CellMouseClick);
            this.dgvList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvList_CellMouseDoubleClick);
            this.dgvList.SelectionChanged += new System.EventHandler(this.dgvList_SelectionChanged);
            // 
            // clmImage
            // 
            this.clmImage.HeaderText = "";
            this.clmImage.Image = global::MyPayfriend.Properties.Resources.Executive;
            this.clmImage.Name = "clmImage";
            this.clmImage.Width = 20;
            // 
            // pnlCount
            // 
            this.pnlCount.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlCount.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlCount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlCount.Location = new System.Drawing.Point(0, 456);
            this.pnlCount.Name = "pnlCount";
            this.pnlCount.Size = new System.Drawing.Size(224, 27);
            this.pnlCount.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlCount.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlCount.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlCount.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlCount.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlCount.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlCount.Style.GradientAngle = 90;
            this.pnlCount.TabIndex = 35;
            // 
            // ExpEmployeeFilteration
            // 
            this.ExpEmployeeFilteration.CanvasColor = System.Drawing.SystemColors.Control;
            this.ExpEmployeeFilteration.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ExpEmployeeFilteration.Controls.Add(this.lblShowBy);
            this.ExpEmployeeFilteration.Controls.Add(this.CboEmpFilter);
            this.ExpEmployeeFilteration.Controls.Add(this.CboEmpFilterTypes);
            this.ExpEmployeeFilteration.Controls.Add(this.cboEmpWorkstatus);
            this.ExpEmployeeFilteration.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExpEmployeeFilteration.Location = new System.Drawing.Point(0, 0);
            this.ExpEmployeeFilteration.Name = "ExpEmployeeFilteration";
            this.ExpEmployeeFilteration.Size = new System.Drawing.Size(224, 97);
            this.ExpEmployeeFilteration.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpEmployeeFilteration.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpEmployeeFilteration.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpEmployeeFilteration.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ExpEmployeeFilteration.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.ExpEmployeeFilteration.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpEmployeeFilteration.Style.GradientAngle = 90;
            this.ExpEmployeeFilteration.TabIndex = 0;
            this.ExpEmployeeFilteration.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpEmployeeFilteration.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpEmployeeFilteration.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpEmployeeFilteration.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.ExpEmployeeFilteration.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.ExpEmployeeFilteration.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.ExpEmployeeFilteration.TitleStyle.GradientAngle = 90;
            this.ExpEmployeeFilteration.TitleText = "Search";
            this.ExpEmployeeFilteration.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.ExpEmployeeFilteration_ExpandedChanged);
            // 
            // lblShowBy
            // 
            // 
            // 
            // 
            this.lblShowBy.BackgroundStyle.Class = "";
            this.lblShowBy.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShowBy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShowBy.Location = new System.Drawing.Point(4, 31);
            this.lblShowBy.Name = "lblShowBy";
            this.lblShowBy.Size = new System.Drawing.Size(74, 23);
            this.lblShowBy.TabIndex = 13;
            this.lblShowBy.Text = "تظهر عن طريق";
            // 
            // CboEmpFilter
            // 
            this.CboEmpFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboEmpFilter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmpFilter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmpFilter.DisplayMember = "Text";
            this.CboEmpFilter.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboEmpFilter.DropDownHeight = 134;
            this.CboEmpFilter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboEmpFilter.FormattingEnabled = true;
            this.CboEmpFilter.IntegralHeight = false;
            this.CboEmpFilter.ItemHeight = 15;
            this.CboEmpFilter.Location = new System.Drawing.Point(6, 59);
            this.CboEmpFilter.Name = "CboEmpFilter";
            this.CboEmpFilter.Size = new System.Drawing.Size(212, 21);
            this.CboEmpFilter.TabIndex = 12;
            this.CboEmpFilter.SelectedIndexChanged += new System.EventHandler(this.cboEmpWorkstatus_SelectedIndexChanged);
            this.CboEmpFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboEmpFilter_KeyPress);
            // 
            // CboEmpFilterTypes
            // 
            this.CboEmpFilterTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CboEmpFilterTypes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmpFilterTypes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmpFilterTypes.DisplayMember = "Text";
            this.CboEmpFilterTypes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboEmpFilterTypes.DropDownHeight = 134;
            this.CboEmpFilterTypes.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboEmpFilterTypes.FormattingEnabled = true;
            this.CboEmpFilterTypes.IntegralHeight = false;
            this.CboEmpFilterTypes.ItemHeight = 15;
            this.CboEmpFilterTypes.Location = new System.Drawing.Point(84, 32);
            this.CboEmpFilterTypes.Name = "CboEmpFilterTypes";
            this.CboEmpFilterTypes.Size = new System.Drawing.Size(134, 21);
            this.CboEmpFilterTypes.TabIndex = 11;
            this.CboEmpFilterTypes.WatermarkText = "D";
            this.CboEmpFilterTypes.SelectedIndexChanged += new System.EventHandler(this.CboEmpFilterTypes_SelectedIndexChanged);
            this.CboEmpFilterTypes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboEmpFilterTypes_KeyPress);
            // 
            // cboEmpWorkstatus
            // 
            this.cboEmpWorkstatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboEmpWorkstatus.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.cboEmpWorkstatus.BackgroundStyle.Class = "TextBoxBorder";
            this.cboEmpWorkstatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cboEmpWorkstatus.ButtonDropDown.Visible = true;
            this.cboEmpWorkstatus.ColumnsVisible = false;
            this.cboEmpWorkstatus.FocusHighlightEnabled = true;
            this.cboEmpWorkstatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEmpWorkstatus.GridColumnLines = false;
            this.cboEmpWorkstatus.ImageIndex = 0;
            this.cboEmpWorkstatus.ImageList = this.ImageListEmployee;
            this.cboEmpWorkstatus.KeyboardSearchEnabled = false;
            this.cboEmpWorkstatus.KeyboardSearchNoSelectionAllowed = false;
            this.cboEmpWorkstatus.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.cboEmpWorkstatus.Location = new System.Drawing.Point(3, 3);
            this.cboEmpWorkstatus.Name = "cboEmpWorkstatus";
            this.cboEmpWorkstatus.Nodes.AddRange(new DevComponents.AdvTree.Node[] {
            this.All,
            this.Filled,
            this.PartiallyFilled,
            this.Vacant});
            this.cboEmpWorkstatus.Size = new System.Drawing.Size(190, 18);
            this.cboEmpWorkstatus.TabIndex = 10;
            this.cboEmpWorkstatus.Visible = false;
            this.cboEmpWorkstatus.WatermarkText = "الحالة العمل";
            this.cboEmpWorkstatus.SelectedIndexChanged += new System.EventHandler(this.cboEmpWorkstatus_SelectedIndexChanged);
            // 
            // ImageListEmployee
            // 
            this.ImageListEmployee.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListEmployee.ImageStream")));
            this.ImageListEmployee.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListEmployee.Images.SetKeyName(0, "Employee_Absconding.png");
            this.ImageListEmployee.Images.SetKeyName(1, "Employee_Expired.PNG");
            this.ImageListEmployee.Images.SetKeyName(2, "Employee_InService.PNG");
            this.ImageListEmployee.Images.SetKeyName(3, "Employee_Probation.PNG");
            this.ImageListEmployee.Images.SetKeyName(4, "Employee_Resigned.PNG");
            this.ImageListEmployee.Images.SetKeyName(5, "Employee_Retired.PNG");
            this.ImageListEmployee.Images.SetKeyName(6, "Employee_Terminated.png");
            this.ImageListEmployee.Images.SetKeyName(7, "female_Employee_Absconding.PNG");
            this.ImageListEmployee.Images.SetKeyName(8, "Employee_female_Expired.PNG");
            this.ImageListEmployee.Images.SetKeyName(9, "Employee_female_InService.png");
            this.ImageListEmployee.Images.SetKeyName(10, "Employee_female_Probation.png");
            this.ImageListEmployee.Images.SetKeyName(11, "Employee_female_Resigned.PNG");
            this.ImageListEmployee.Images.SetKeyName(12, "female_Employee_Retired.png");
            this.ImageListEmployee.Images.SetKeyName(13, "female_Employee_Terminated.PNG");
            this.ImageListEmployee.Images.SetKeyName(14, "Employee_All.PNG");
            this.ImageListEmployee.Images.SetKeyName(15, "Male_Others.PNG");
            // 
            // All
            // 
            this.All.Name = "All";
            // 
            // Filled
            // 
            this.Filled.Name = "Filled";
            // 
            // PartiallyFilled
            // 
            this.PartiallyFilled.Name = "PartiallyFilled";
            // 
            // Vacant
            // 
            this.Vacant.Name = "Vacant";
            // 
            // expandableSplitter1
            // 
            this.expandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter1.ExpandableControl = this.panelMiddle;
            this.expandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.Location = new System.Drawing.Point(469, 29);
            this.expandableSplitter1.Name = "expandableSplitter1";
            this.expandableSplitter1.Size = new System.Drawing.Size(6, 483);
            this.expandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitter1.TabIndex = 25;
            this.expandableSplitter1.TabStop = false;
            // 
            // panelRight
            // 
            this.panelRight.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelRight.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelRight.Controls.Add(this.pnlUnSupportedFileTypes);
            this.panelRight.Controls.Add(this.tabEmployee);
            this.panelRight.Controls.Add(this.ImageControl);
            this.panelRight.Controls.Add(this.webEasyview);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(475, 29);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(828, 483);
            this.panelRight.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelRight.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelRight.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelRight.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelRight.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelRight.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelRight.Style.GradientAngle = 90;
            this.panelRight.TabIndex = 26;
            this.panelRight.Text = "panelEx2";
            // 
            // pnlUnSupportedFileTypes
            // 
            this.pnlUnSupportedFileTypes.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlUnSupportedFileTypes.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlUnSupportedFileTypes.Controls.Add(this.LblMess2);
            this.pnlUnSupportedFileTypes.Controls.Add(this.LblMess1);
            this.pnlUnSupportedFileTypes.Controls.Add(this.btnOpenApplication);
            this.pnlUnSupportedFileTypes.Location = new System.Drawing.Point(133, 280);
            this.pnlUnSupportedFileTypes.Name = "pnlUnSupportedFileTypes";
            this.pnlUnSupportedFileTypes.Size = new System.Drawing.Size(673, 139);
            this.pnlUnSupportedFileTypes.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlUnSupportedFileTypes.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlUnSupportedFileTypes.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlUnSupportedFileTypes.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlUnSupportedFileTypes.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlUnSupportedFileTypes.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlUnSupportedFileTypes.Style.GradientAngle = 90;
            this.pnlUnSupportedFileTypes.TabIndex = 21;
            // 
            // LblMess2
            // 
            this.LblMess2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblMess2.AutoSize = true;
            this.LblMess2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.LblMess2.Location = new System.Drawing.Point(165, 58);
            this.LblMess2.Name = "LblMess2";
            this.LblMess2.Size = new System.Drawing.Size(485, 32);
            this.LblMess2.TabIndex = 19;
            this.LblMess2.Text = "\"المستند الذي تحاول عرض تتعلق بتطبيق خارجي. استخدم الزر أدناه لفتح هذا المستند \n\"" +
                "";
            this.LblMess2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblMess1
            // 
            this.LblMess1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblMess1.AutoSize = true;
            this.LblMess1.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.LblMess1.ForeColor = System.Drawing.Color.Maroon;
            this.LblMess1.Location = new System.Drawing.Point(468, 27);
            this.LblMess1.Name = "LblMess1";
            this.LblMess1.Size = new System.Drawing.Size(182, 18);
            this.LblMess1.TabIndex = 18;
            this.LblMess1.Text = "لا تتوفر معاينة لهذا المستند ";
            this.LblMess1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnOpenApplication
            // 
            this.btnOpenApplication.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenApplication.BackColor = System.Drawing.Color.AliceBlue;
            this.btnOpenApplication.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpenApplication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenApplication.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenApplication.Location = new System.Drawing.Point(506, 105);
            this.btnOpenApplication.Name = "btnOpenApplication";
            this.btnOpenApplication.Size = new System.Drawing.Size(145, 27);
            this.btnOpenApplication.TabIndex = 20;
            this.btnOpenApplication.Text = "فتح تطبيق ";
            this.btnOpenApplication.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenApplication.UseVisualStyleBackColor = false;
            this.btnOpenApplication.Click += new System.EventHandler(this.btnOpenApplication_Click);
            // 
            // tabEmployee
            // 
            this.tabEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(154)))));
            this.tabEmployee.CanReorderTabs = true;
            this.tabEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabEmployee.ImageList = this.ImgListNavigator;
            this.tabEmployee.Location = new System.Drawing.Point(0, 0);
            this.tabEmployee.Name = "tabEmployee";
            this.tabEmployee.SelectedTabFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabEmployee.SelectedTabIndex = 1;
            this.tabEmployee.Size = new System.Drawing.Size(828, 483);
            this.tabEmployee.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tabEmployee.TabIndex = 6;
            this.tabEmployee.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabEmployee.Text = "tabControl1";
            // 
            // ImageControl
            // 
            this.ImageControl.BackColor = System.Drawing.SystemColors.Window;
            this.ImageControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ImageControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImageControl.Image = null;
            this.ImageControl.initialimage = null;
            this.ImageControl.Location = new System.Drawing.Point(0, 0);
            this.ImageControl.Name = "ImageControl";
            this.ImageControl.Origin = new System.Drawing.Point(0, 0);
            this.ImageControl.PanButton = System.Windows.Forms.MouseButtons.Left;
            this.ImageControl.PanMode = true;
            this.ImageControl.ScrollbarsVisible = true;
            this.ImageControl.Size = new System.Drawing.Size(828, 483);
            this.ImageControl.StretchImageToFit = false;
            this.ImageControl.TabIndex = 15;
            this.ImageControl.ZoomFactor = 1;
            this.ImageControl.ZoomOnMouseWheel = true;
            // 
            // webEasyview
            // 
            this.webEasyview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webEasyview.Location = new System.Drawing.Point(0, 0);
            this.webEasyview.MinimumSize = new System.Drawing.Size(20, 20);
            this.webEasyview.Name = "webEasyview";
            this.webEasyview.Size = new System.Drawing.Size(828, 483);
            this.webEasyview.TabIndex = 7;
            // 
            // navigationPanePanel3
            // 
            this.navigationPanePanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel3.Controls.Add(this.advTree1);
            this.navigationPanePanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel3.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel3.Name = "navigationPanePanel3";
            this.navigationPanePanel3.ParentItem = null;
            this.navigationPanePanel3.Size = new System.Drawing.Size(223, 469);
            this.navigationPanePanel3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.navigationPanePanel3.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile;
            this.navigationPanePanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.navigationPanePanel3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel3.Style.GradientAngle = 90;
            this.navigationPanePanel3.Style.WordWrap = true;
            this.navigationPanePanel3.StyleMouseDown.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel3.StyleMouseDown.WordWrap = true;
            this.navigationPanePanel3.StyleMouseOver.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel3.StyleMouseOver.WordWrap = true;
            this.navigationPanePanel3.TabIndex = 6;
            this.navigationPanePanel3.Text = "Drop your controls here and erase Text property";
            // 
            // advTree1
            // 
            this.advTree1.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.advTree1.AllowDrop = true;
            this.advTree1.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.advTree1.BackgroundStyle.Class = "TreeBorderKey";
            this.advTree1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.advTree1.ColumnsVisible = false;
            this.advTree1.DisplayMembers = "fields";
            this.advTree1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advTree1.DragDropEnabled = false;
            this.advTree1.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle;
            this.advTree1.ExpandWidth = 14;
            this.advTree1.GroupingMembers = "ClientName";
            this.advTree1.GroupNodeStyle = this.elementStyle2;
            this.advTree1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.advTree1.Location = new System.Drawing.Point(0, 0);
            this.advTree1.MultiSelectRule = DevComponents.AdvTree.eMultiSelectRule.AnyNode;
            this.advTree1.Name = "advTree1";
            this.advTree1.NodesConnector = this.nodeConnector3;
            this.advTree1.NodeStyle = this.elementStyle2;
            this.advTree1.PathSeparator = ";";
            this.advTree1.Size = new System.Drawing.Size(223, 469);
            this.advTree1.Styles.Add(this.elementStyle2);
            this.advTree1.TabIndex = 2;
            this.advTree1.ValueMember = "ClientID";
            // 
            // elementStyle2
            // 
            this.elementStyle2.Class = "";
            this.elementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle2.Name = "elementStyle2";
            this.elementStyle2.PaddingTop = 5;
            this.elementStyle2.TextColor = System.Drawing.SystemColors.ControlText;
            this.elementStyle2.TextTrimming = DevComponents.DotNetBar.eStyleTextTrimming.None;
            this.elementStyle2.WordWrap = true;
            // 
            // nodeConnector3
            // 
            this.nodeConnector3.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // navigationPanePanel6
            // 
            this.navigationPanePanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel6.Controls.Add(this.advTree2);
            this.navigationPanePanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel6.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel6.Name = "navigationPanePanel6";
            this.navigationPanePanel6.Padding = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.navigationPanePanel6.ParentItem = null;
            this.navigationPanePanel6.Size = new System.Drawing.Size(223, 299);
            this.navigationPanePanel6.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel6.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel6.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.navigationPanePanel6.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile;
            this.navigationPanePanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.navigationPanePanel6.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel6.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel6.Style.GradientAngle = 90;
            this.navigationPanePanel6.StyleMouseDown.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel6.StyleMouseOver.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel6.TabIndex = 2;
            // 
            // advTree2
            // 
            this.advTree2.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.advTree2.AllowDrop = true;
            this.advTree2.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.advTree2.BackgroundStyle.Class = "TreeBorderKey";
            this.advTree2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.advTree2.ColumnsVisible = false;
            this.advTree2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advTree2.DragDropEnabled = false;
            this.advTree2.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle;
            this.advTree2.ExpandWidth = 14;
            this.advTree2.GroupNodeStyle = this.elementStyle3;
            this.advTree2.ImageList = this.ImgListNavigator;
            this.advTree2.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.advTree2.Location = new System.Drawing.Point(1, 1);
            this.advTree2.MultiSelectRule = DevComponents.AdvTree.eMultiSelectRule.AnyNode;
            this.advTree2.Name = "advTree2";
            this.advTree2.NodesConnector = this.nodeConnector4;
            this.advTree2.NodeStyle = this.elementStyle3;
            this.advTree2.PathSeparator = ";";
            this.advTree2.Size = new System.Drawing.Size(221, 298);
            this.advTree2.Styles.Add(this.elementStyle3);
            this.advTree2.TabIndex = 1;
            // 
            // elementStyle3
            // 
            this.elementStyle3.Class = "";
            this.elementStyle3.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle3.Name = "elementStyle3";
            this.elementStyle3.PaddingTop = 5;
            this.elementStyle3.TextColor = System.Drawing.SystemColors.ControlText;
            this.elementStyle3.TextTrimming = DevComponents.DotNetBar.eStyleTextTrimming.None;
            this.elementStyle3.WordWrap = true;
            // 
            // nodeConnector4
            // 
            this.nodeConnector4.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // navigationPanePanel7
            // 
            this.navigationPanePanel7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel7.Controls.Add(this.advTree3);
            this.navigationPanePanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel7.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel7.Name = "navigationPanePanel7";
            this.navigationPanePanel7.ParentItem = null;
            this.navigationPanePanel7.Size = new System.Drawing.Size(223, 299);
            this.navigationPanePanel7.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel7.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel7.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.navigationPanePanel7.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile;
            this.navigationPanePanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.navigationPanePanel7.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel7.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel7.Style.GradientAngle = 90;
            this.navigationPanePanel7.Style.WordWrap = true;
            this.navigationPanePanel7.StyleMouseDown.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel7.StyleMouseDown.WordWrap = true;
            this.navigationPanePanel7.StyleMouseOver.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel7.StyleMouseOver.WordWrap = true;
            this.navigationPanePanel7.TabIndex = 6;
            this.navigationPanePanel7.Text = "Drop your controls here and erase Text property";
            // 
            // advTree3
            // 
            this.advTree3.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.advTree3.AllowDrop = true;
            this.advTree3.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.advTree3.BackgroundStyle.Class = "TreeBorderKey";
            this.advTree3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.advTree3.ColumnsVisible = false;
            this.advTree3.DisplayMembers = "fields";
            this.advTree3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advTree3.DragDropEnabled = false;
            this.advTree3.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle;
            this.advTree3.ExpandWidth = 14;
            this.advTree3.GroupingMembers = "ClientName";
            this.advTree3.GroupNodeStyle = this.elementStyle6;
            this.advTree3.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.advTree3.Location = new System.Drawing.Point(0, 0);
            this.advTree3.MultiSelectRule = DevComponents.AdvTree.eMultiSelectRule.AnyNode;
            this.advTree3.Name = "advTree3";
            this.advTree3.NodesConnector = this.nodeConnector5;
            this.advTree3.NodeStyle = this.elementStyle6;
            this.advTree3.PathSeparator = ";";
            this.advTree3.Size = new System.Drawing.Size(223, 299);
            this.advTree3.Styles.Add(this.elementStyle6);
            this.advTree3.TabIndex = 2;
            this.advTree3.ValueMember = "ClientID";
            // 
            // nodeConnector5
            // 
            this.nodeConnector5.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // navigationPanePanel8
            // 
            this.navigationPanePanel8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel8.Controls.Add(this.advTree4);
            this.navigationPanePanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel8.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel8.Name = "navigationPanePanel8";
            this.navigationPanePanel8.ParentItem = null;
            this.navigationPanePanel8.Size = new System.Drawing.Size(223, 299);
            this.navigationPanePanel8.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel8.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel8.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.navigationPanePanel8.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile;
            this.navigationPanePanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.navigationPanePanel8.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel8.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel8.Style.GradientAngle = 90;
            this.navigationPanePanel8.Style.WordWrap = true;
            this.navigationPanePanel8.StyleMouseDown.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel8.StyleMouseDown.WordWrap = true;
            this.navigationPanePanel8.StyleMouseOver.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel8.StyleMouseOver.WordWrap = true;
            this.navigationPanePanel8.TabIndex = 3;
            this.navigationPanePanel8.Text = "Drop your controls here and erase Text property";
            // 
            // advTree4
            // 
            this.advTree4.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.advTree4.AllowDrop = true;
            this.advTree4.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.advTree4.BackgroundStyle.Class = "TreeBorderKey";
            this.advTree4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.advTree4.ColumnsVisible = false;
            this.advTree4.DisplayMembers = "CompanyID";
            this.advTree4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advTree4.DragDropEnabled = false;
            this.advTree4.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle;
            this.advTree4.ExpandWidth = 14;
            this.advTree4.GroupingMembers = "CompanyName,Branchname";
            this.advTree4.GroupNodeStyle = this.elementStyle7;
            this.advTree4.ImageList = this.ImgListNavigator;
            this.advTree4.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.advTree4.Location = new System.Drawing.Point(0, 0);
            this.advTree4.Name = "advTree4";
            this.advTree4.NodesConnector = this.nodeConnector7;
            this.advTree4.NodeStyle = this.elementStyle7;
            this.advTree4.PathSeparator = ";";
            this.advTree4.Size = new System.Drawing.Size(223, 299);
            this.advTree4.Styles.Add(this.elementStyle7);
            this.advTree4.TabIndex = 1;
            this.advTree4.ValueMember = "CompanyID";
            // 
            // elementStyle7
            // 
            this.elementStyle7.Class = "";
            this.elementStyle7.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle7.Name = "elementStyle7";
            this.elementStyle7.PaddingTop = 5;
            this.elementStyle7.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // nodeConnector7
            // 
            this.nodeConnector7.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // navigationPanePanel9
            // 
            this.navigationPanePanel9.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel9.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel9.Name = "navigationPanePanel9";
            this.navigationPanePanel9.ParentItem = null;
            this.navigationPanePanel9.Size = new System.Drawing.Size(223, 299);
            this.navigationPanePanel9.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel9.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel9.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.navigationPanePanel9.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile;
            this.navigationPanePanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.navigationPanePanel9.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel9.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel9.Style.GradientAngle = 90;
            this.navigationPanePanel9.Style.WordWrap = true;
            this.navigationPanePanel9.StyleMouseDown.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel9.StyleMouseDown.WordWrap = true;
            this.navigationPanePanel9.StyleMouseOver.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel9.StyleMouseOver.WordWrap = true;
            this.navigationPanePanel9.TabIndex = 5;
            this.navigationPanePanel9.Text = "Drop your controls here and erase Text property";
            // 
            // navigationPanePanel11
            // 
            this.navigationPanePanel11.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.navigationPanePanel11.Controls.Add(this.advTree6);
            this.navigationPanePanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navigationPanePanel11.Location = new System.Drawing.Point(1, 1);
            this.navigationPanePanel11.Name = "navigationPanePanel11";
            this.navigationPanePanel11.ParentItem = null;
            this.navigationPanePanel11.Size = new System.Drawing.Size(223, 386);
            this.navigationPanePanel11.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel11.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.navigationPanePanel11.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.navigationPanePanel11.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile;
            this.navigationPanePanel11.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.navigationPanePanel11.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.navigationPanePanel11.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.navigationPanePanel11.Style.GradientAngle = 90;
            this.navigationPanePanel11.Style.WordWrap = true;
            this.navigationPanePanel11.StyleMouseDown.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel11.StyleMouseDown.WordWrap = true;
            this.navigationPanePanel11.StyleMouseOver.Alignment = System.Drawing.StringAlignment.Center;
            this.navigationPanePanel11.StyleMouseOver.WordWrap = true;
            this.navigationPanePanel11.TabIndex = 6;
            this.navigationPanePanel11.Text = "Drop your controls here and erase Text property";
            // 
            // advTree6
            // 
            this.advTree6.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.advTree6.AllowDrop = true;
            this.advTree6.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.advTree6.BackgroundStyle.Class = "TreeBorderKey";
            this.advTree6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.advTree6.ColumnsVisible = false;
            this.advTree6.DisplayMembers = "fields";
            this.advTree6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advTree6.DragDropEnabled = false;
            this.advTree6.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle;
            this.advTree6.ExpandWidth = 14;
            this.advTree6.GroupingMembers = "ClientName";
            this.advTree6.GroupNodeStyle = this.elementStyle9;
            this.advTree6.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.advTree6.Location = new System.Drawing.Point(0, 0);
            this.advTree6.MultiSelectRule = DevComponents.AdvTree.eMultiSelectRule.AnyNode;
            this.advTree6.Name = "advTree6";
            this.advTree6.NodesConnector = this.nodeConnector9;
            this.advTree6.NodeStyle = this.elementStyle9;
            this.advTree6.PathSeparator = ";";
            this.advTree6.Size = new System.Drawing.Size(223, 386);
            this.advTree6.Styles.Add(this.elementStyle9);
            this.advTree6.TabIndex = 2;
            this.advTree6.ValueMember = "ClientID";
            // 
            // elementStyle9
            // 
            this.elementStyle9.Class = "";
            this.elementStyle9.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.elementStyle9.Name = "elementStyle9";
            this.elementStyle9.PaddingTop = 5;
            this.elementStyle9.TextColor = System.Drawing.SystemColors.ControlText;
            this.elementStyle9.TextTrimming = DevComponents.DotNetBar.eStyleTextTrimming.None;
            this.elementStyle9.WordWrap = true;
            // 
            // nodeConnector9
            // 
            this.nodeConnector9.LineColor = System.Drawing.SystemColors.ControlText;
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.ShowCustomizeContextMenu = false;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.ToolbarTopDockSite = this.dockSite7;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite4.Location = new System.Drawing.Point(0, 512);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1303, 0);
            this.dockSite4.TabIndex = 30;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(475, 29);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 483);
            this.dockSite1.TabIndex = 27;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite2.Location = new System.Drawing.Point(1303, 29);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(0, 483);
            this.dockSite2.TabIndex = 28;
            this.dockSite2.TabStop = false;
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(0, 512);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1303, 0);
            this.dockSite8.TabIndex = 33;
            this.dockSite8.TabStop = false;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(0, 0);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 512);
            this.dockSite5.TabIndex = 31;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1303, 0);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 512);
            this.dockSite6.TabIndex = 32;
            this.dockSite6.TabStop = false;
            // 
            // dockSite7
            // 
            this.dockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite7.Controls.Add(this.barTopSearch);
            this.dockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite7.Location = new System.Drawing.Point(0, 0);
            this.dockSite7.Name = "dockSite7";
            this.dockSite7.Size = new System.Drawing.Size(1303, 29);
            this.dockSite7.TabIndex = 7;
            this.dockSite7.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(0, 0);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1303, 0);
            this.dockSite3.TabIndex = 29;
            this.dockSite3.TabStop = false;
            // 
            // NavigatorArb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1303, 512);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.expandableSplitter1);
            this.Controls.Add(this.panelMiddle);
            this.Controls.Add(this.navigationPane1);
            this.Controls.Add(this.dockSite7);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite8);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "NavigatorArb";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.Load += new System.EventHandler(this.Navigator_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NavigatorArb_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.barTopSearch)).EndInit();
            this.barTopSearch.ResumeLayout(false);
            this.navigationPane1.ResumeLayout(false);
            this.navigationPanePanel10.ResumeLayout(false);
            this.navigationPanePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AdvCompanyTree)).EndInit();
            this.navigationPanePanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AdvEmployeeTree)).EndInit();
            this.navigationPanePanel4.ResumeLayout(false);
            this.panelMiddle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AdvDocumentTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ExpEmployeeFilteration.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.pnlUnSupportedFileTypes.ResumeLayout(false);
            this.pnlUnSupportedFileTypes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabEmployee)).EndInit();
            this.navigationPanePanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.advTree1)).EndInit();
            this.navigationPanePanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.advTree2)).EndInit();
            this.navigationPanePanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.advTree3)).EndInit();
            this.navigationPanePanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.advTree4)).EndInit();
            this.navigationPanePanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.advTree6)).EndInit();
            this.dockSite7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

       
        private DevComponents.DotNetBar.NavigationPane navigationPane1;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel4;
        private DevComponents.DotNetBar.ButtonItem btnEasyview;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel1;
        private DevComponents.DotNetBar.ButtonItem btnNavCompany;
        private DevComponents.DotNetBar.ButtonItem btnNavTask;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel2;
        private DevComponents.DotNetBar.ButtonItem btnClient;
        private DevComponents.DotNetBar.PanelEx panelMiddle;
        internal DevComponents.DotNetBar.Controls.DataGridViewX dgvList;
        private DevComponents.DotNetBar.ExpandablePanel ExpEmployeeFilteration;
        internal DevComponents.DotNetBar.Controls.ComboTree cboEmpWorkstatus;
        private DevComponents.AdvTree.Node All;
        private DevComponents.AdvTree.Node Filled;
        private DevComponents.AdvTree.Node PartiallyFilled;
        private DevComponents.AdvTree.Node Vacant;
        internal System.Windows.Forms.ImageList ImgListNavigator;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter1;
        private DevComponents.DotNetBar.PanelEx panelRight;
        internal DevComponents.AdvTree.AdvTree AdvEmployeeTree;
        internal DevComponents.DotNetBar.ElementStyle elementStyle1;
        internal DevComponents.AdvTree.NodeConnector nodeConnector1;
        internal DevComponents.DotNetBar.Controls.ListViewEx lstEasyViewLeft;
        internal System.Windows.Forms.ImageList ImgListLargeNavigator;
        internal System.Windows.Forms.ImageList ImageListEmployee;
        internal DevComponents.DotNetBar.LabelX lblShowBy;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx CboEmpFilter;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx CboEmpFilterTypes;
        internal DevComponents.DotNetBar.Bar barTopSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtTopSearch;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private DevComponents.DotNetBar.TabControl tabEmployee;
        private System.Windows.Forms.WebBrowser webEasyview;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel3;
        internal DevComponents.AdvTree.AdvTree advTree1;
        internal DevComponents.DotNetBar.ElementStyle elementStyle2;
        internal DevComponents.AdvTree.NodeConnector nodeConnector3;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel6;
        internal DevComponents.AdvTree.AdvTree advTree2;
        internal DevComponents.DotNetBar.ElementStyle elementStyle3;
        internal DevComponents.AdvTree.NodeConnector nodeConnector4;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel7;
        internal DevComponents.AdvTree.AdvTree advTree3;
        internal DevComponents.DotNetBar.ElementStyle elementStyle6;
        internal DevComponents.AdvTree.NodeConnector nodeConnector5;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel8;
        internal DevComponents.AdvTree.AdvTree advTree4;
        internal DevComponents.DotNetBar.ElementStyle elementStyle7;
        internal DevComponents.AdvTree.NodeConnector nodeConnector7;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel9;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel11;
        internal DevComponents.AdvTree.AdvTree advTree6;
        internal DevComponents.DotNetBar.ElementStyle elementStyle9;
        internal DevComponents.AdvTree.NodeConnector nodeConnector9;
        private DevComponents.DotNetBar.DotNetBarManager dotNetBarManager1;
        private DevComponents.DotNetBar.DockSite dockSite4;
        private DevComponents.DotNetBar.DockSite dockSite1;
        private DevComponents.DotNetBar.DockSite dockSite2;
        private DevComponents.DotNetBar.DockSite dockSite3;
        private DevComponents.DotNetBar.DockSite dockSite5;
        private DevComponents.DotNetBar.DockSite dockSite6;
        private DevComponents.DotNetBar.DockSite dockSite8;
        private DevComponents.DotNetBar.DockSite dockSite7;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private DevComponents.DotNetBar.NavigationPanePanel navigationPanePanel10;
        private DevComponents.DotNetBar.ButtonItem btnDocuments;
        internal System.Windows.Forms.ImageList ImageListDocuments;
        internal DevComponents.AdvTree.AdvTree AdvDocumentTree;
        internal DevComponents.AdvTree.NodeConnector nodeConnector8;
        internal Ic.ImageControl ImageControl;
        internal System.Windows.Forms.Button btnOpenApplication;
        internal System.Windows.Forms.Label LblMess2;
        internal System.Windows.Forms.Label LblMess1;
        internal DevComponents.DotNetBar.Controls.ListViewEx lstDocumentsLeft;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        internal DevComponents.DotNetBar.ButtonItem btnTopDocumentSearch;
        internal DevComponents.DotNetBar.ElementStyle ElementStyle5;
        private DevComponents.AdvTree.NodeConnector nodeConnector6;
        private DevComponents.DotNetBar.ElementStyle elementStyle4;
        internal DevComponents.AdvTree.AdvTree AdvCompanyTree;
        internal DevComponents.AdvTree.NodeConnector NodeConnector2;
        private System.Windows.Forms.DataGridViewImageColumn clmImage;
        private DevComponents.DotNetBar.PanelEx pnlUnSupportedFileTypes;
        private DevComponents.DotNetBar.PanelEx pnlCount;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;

        //private DevComponents.DotNetBar.DockSite dockSite7;
    }
}