﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Data;
using System.Data.SqlClient;
using DevComponents.DotNetBar;
using DevComponents.AdvTree;
using System.IO;


/*********************************************
* Author       : RAJESH.R
* Created On   : 27 AUG 2013
* ******************************************/

namespace MyPayfriend
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [System.Runtime.InteropServices.ComVisibleAttribute(true)] 
    public partial class NavigatorArb : DevComponents.DotNetBar.Office2007Form //Form 
    {

        #region Properties

        /// <summary>
        /// Gets or sets all the messages in the form
        /// </summary>
        private clsMessage MObjUserMessage = null;

        /// <summary>
        /// Gets or sets the documentsuggestion auto complete list for search-Documents and employee
        /// </summary>
        public System.Windows.Forms.AutoCompleteStringCollection DocumentSugessionList { get; set; }

        //For Permission
        private bool MblnViewPermission = false;
        private bool MblnPrintEmailPermission;
        private bool MblnAddPermission;
        private bool MblnUpdatePermission;
        private bool MblnDeletePermission;


        private bool CanLoad { get; set; }

        /// <summary>
        /// Gets or sets the documentId
        /// </summary>
        private int DocumentID { get; set; }

        /// <summary>
        /// Gets or sets the company id
        /// </summary>
        private int CompanyID { get; set; }

        /// <summary>
        /// Gets or sets the empployeeID
        /// </summary>
        private int EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the current row index for search
        /// </summary>
        private int CurrentRowIndex { get; set; }


        private int EmployeeSearchIndex { get; set; }
        /// <summary>
        /// Gets or sets the file path of the file
        /// </summary>
        private string FilePath { get; set; }


        private DataTable dtDocuments { get; set; }

        /// <summary>
        /// Gets or sets the DocumentType enum
        /// </summary>
        private eDocumentType DocumentTypeID { get; set; }

        /// <summary>
        /// Gets or sets the currently selected navigator type
        /// </summary>
        private eNavigator eCurrentType { get; set; }

        /// <summary>
        /// Gets or sets the current operation type
        /// </summary>
        private eOperationType eCurrentOperationType { get; set; }

        private eFilterTypes eCurrentFilterType { get; set; }

        private clsMessage UserMessage
        {
            get
            {
                if (this.MObjUserMessage == null)
                    this.MObjUserMessage = new clsMessage(FormID.Documents);
                return this.MObjUserMessage;
            }
        }

        /// <summary>
        /// Enum for navigator
        /// </summary>
        public enum eNavigator
        {
            COMPANY = 1,
            EMPLOYEE = 2,
            EASYVIEW = 3,
            DOCUMENTS = 4
        }

        /// <summary>
        /// Enum for employee webpage
        /// </summary>
        private enum eEmployeeWebPage
        {
            EmployeeInformation = 1,
            EmployeeSalaryStructure = 2,
            LeaveStructure = 3,
            WorkPolicy = 4,
            LeavePolicy = 5,
            VacationPolicy = 6
        }

        /// <summary>
        /// Enum for operation types
        /// </summary>
        private enum eOperationType
        {
            Company = 1,
            Employee = 2
        }

        /// <summary>
        /// Enum for document Type
        /// </summary>
        private enum eDocumentType
        {
            All = -1,
            None = 0,
            Passport = 1,
            Visa = 2,
            DrivingLicense = 3,
            HealthCard = 4,
            LabourCard = 5,
            EmiratesCard = 6,
            InsuranceDetails = 7,
            Qualification = 8,
            InsuranceCard = 9,
            Expense = 10,
            Deposit = 11,
            LeaseAgreement = 12,
            TradingLicense = 13,
            LeaveEntry = 14,
            Probation = 15,
            CompanyAsset = 16,
            PDC = 17,
            Option1 = 18,
            Option2 = 19,
            option3 = 20


        }

        #endregion

        #region Constructor
        public NavigatorArb()
        {
            InitializeComponent();
            SetArabicControls();
            
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.Navigator, this);
        }
        #endregion

        #region FormEvents
        private void Navigator_Load(object sender, EventArgs e)
        {
            Initialize();
        }
        #endregion

        #region ControlEvents
        /// <summary>
        /// Document Tree Node Click Event
        /// Showing the attached documents on clicking the selcected node
        /// </summary>
        private void AdvDocumentTree_NodeClick(object sender, TreeNodeMouseEventArgs e)
        {
            ImageControl.Image = null;
            string FileName = "";


            //if the selected node ends with attachedfile string 
            if (e.Node.Tag.ToString().EndsWith("AttachedFile"))
            {
                //Getting the file name of the from the selected attached document
                FileName = e.Node.Tag.ToString().Trim().Split('@')[0];

                //Constucting the file path based on the configuration file path 
                FilePath = string.Format(@"{0}\{1}\{2}", ClsCommonSettings.strServerPath, "DocumentImages", FileName);

                if (FileName != string.Empty)
                {
                    //Checking if the file exists in the path
                    if (File.Exists(FilePath))
                    {
                        //Display the file based on the file type
                        ShowFileContents(FilePath);
                    }

                }
            }

        }


        /// <summary>
        /// AdvDocumentTree Node DoubleClick Event
        /// Loads the current document when user double click the document
        /// </summary>
        private void AdvDocumentTree_NodeDoubleClick(object sender, TreeNodeMouseEventArgs e)
        {
            int DocumentTypeID = 0;
            int DocumentID = 0;
            string Temp = "";


            if (e.Node.Tag.ToString().EndsWith("Documents"))
            {
                Temp = e.Node.Tag.ToString().Split('@')[0].ToString();

                //getting the documenttypeid and documentid from the tag
                DocumentTypeID = Temp.Split('^')[1].ToInt32();
                DocumentID = Temp.Split('^')[0].ToInt32();

                switch (DocumentTypeID)
                {
                    //Driving license documenttype
                    case (int)DocumentType.Driving_License:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DrivingLicense, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new frmDrivingLicense() { StartPosition = FormStartPosition.CenterScreen, PiDrivingLicense = DocumentID }.ShowDialog();
                        break;

                    case (int)DocumentType.Expense:
                        break;

                    case (int)DocumentType.Health_Card:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.HealthCard, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new EmployeeHealthCard() { StartPosition = FormStartPosition.CenterScreen, MiHealthCardID = DocumentID }.ShowDialog();
                        break;

                    case (int)DocumentType.Insurance_Card:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.InsuranceCard, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new InsuranceCard() { StartPosition = FormStartPosition.CenterScreen, PintInsuranceCardID = DocumentID }.ShowDialog();
                        break;

                    case (int)DocumentType.Labour_Card:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.LabourCard, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new FrmEmployeeLabourCard() { StartPosition = FormStartPosition.CenterScreen, PLabourCardId = DocumentID }.ShowDialog();
                        break;


                    case (int)DocumentType.National_ID_Card:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.NationalIDCard, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new EmiratesHealthLabourCard() { StartPosition = FormStartPosition.CenterScreen, PiEmCardID = DocumentID }.ShowDialog();
                        break;

                    case (int)DocumentType.Passport:

                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.Passport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new frmPassport() { StartPosition = FormStartPosition.CenterScreen, PiPassportID = DocumentID }.ShowDialog();
                        break;

                    case (int)DocumentType.Qualification:

                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.Qualification, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new EmployeeQualification() { StartPosition = FormStartPosition.CenterScreen, PQualificationID = DocumentID }.ShowDialog();
                        break;


                    case (int)DocumentType.Visa:

                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.Visa, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new frmVisa() { StartPosition = FormStartPosition.CenterScreen, piVisaID = DocumentID }.ShowDialog();
                        break;

                    //Trading license document
                    case (int)DocumentType.Trading_License:

                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.TradeLicense, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new frmCompanyTradeLicense() { PintTradeLicenseID = DocumentID }.ShowDialog();
                        break;




                    //Lease agreement document
                    case (int)eDocumentType.LeaseAgreement:


                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.LeaseAgreement, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                        if (MblnViewPermission)
                            new frmCompanyLeaseAgreement() { LeaseID = DocumentID }.ShowDialog();
                        break;


                    case (int)eDocumentType.InsuranceDetails:


                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.Insurance, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                        if (MblnViewPermission)
                            new InsuranceDetails() { PintInsuranceID = DocumentID }.ShowDialog();
                        break;


                    //Company Asset Details
                    case (int)eDocumentType.CompanyAsset:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.CompanyAssets, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new FrmCompanyAssets() { AssetID = DocumentID }.ShowDialog();
                        break;

                    default:


                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.OtherDocuments, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                        if (MblnViewPermission)
                            new frmDocumentMasterNew() { StartPosition = FormStartPosition.CenterScreen, eOperationType = (OperationType)eCurrentOperationType, ePDocumentType = (DocumentType)DocumentTypeID, DocumentIDNav = DocumentID }.ShowDialog();
                        break;
                }
            }
        }


        /// <summary>
        /// Search button event
        /// </summary>
        private void btnDocumentSearch_Click(object sender, EventArgs e)
        {
            if (txtTopSearch.Text.Trim() == "")
                MessageBox.Show("Please enter an item to search ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            else if (eCurrentType == eNavigator.DOCUMENTS)
                SearchDocumentNavigator();
            else if (eCurrentType == eNavigator.EMPLOYEE)
                SearchEmployee();
            else if (eCurrentType == eNavigator.COMPANY)
                SearchCompanyDocuments();
 
        }

        /// <summary>
        /// Open the un supported file format with the help of installed applications that matches with the file type
        /// </summary>
        private void btnOpenApplication_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process objProc = new System.Diagnostics.Process();
                objProc.StartInfo.FileName = FilePath;
                objProc.Start();

                objProc = null;
            }
            catch (Exception)
            {
                MessageBox.Show("Windows can not open this file. Application Missing.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        /// <summary>
        /// This event will be fired on clicking the button in the navigator
        /// </summary>
        private void btnNavigator_Click(object sender, EventArgs e)
        {
            //Getting the currently selected navigator text
            string SelectedItem = ((DevComponents.DotNetBar.ButtonItem)(sender)).Text;

            if (SelectedItem == "الشركة")
                eCurrentType = eNavigator.COMPANY;
            else if (SelectedItem == "عامل")
                eCurrentType = eNavigator.EMPLOYEE;
            else if (SelectedItem == "من السهل مشاهدة")
            {
                eCurrentType = eNavigator.EASYVIEW;
            }
            else
                eCurrentType = eNavigator.DOCUMENTS;

            //Make panels visibility based on the currently selected navigatior button
            SetVisibility();
        }


        /// <summary>
        /// Event will be fired when user click any node in the company tree
        /// </summary>
        private void AdvCompanyTree_NodeClick(object sender, TreeNodeMouseEventArgs e)
        {
            //Taking the document id and document type id from the selected node
            //If the tag contains @ then it is a document type else it is a branch node

            CompanyID = 0;
            webEasyview.DocumentText = "";
            DocumentTypeID = eDocumentType.None;
            if (e.Node != null && e.Node.Tag != null && e.Node.Tag.ToString().Contains("@"))
            {
                DocumentTypeID = (eDocumentType)e.Node.Tag.ToString().Split('@')[0].ToInt32();
                CompanyID = e.Node.Tag.ToString().Split('@')[1].ToInt32();
                //Load the documents based on the document type id and display it to the grid
                FillCompanyDocumentsByDocumentTypeID();
            }
            else
            {
                dgvList.DataSource = null;
                webEasyview.DocumentText = ""; 
               
            }

        }


        /// <summary>
        /// eCurrentType == Company -->show the corresponding company documents details on the right web browser
        /// eCurrentType == Employee ->Show employee information ,leave structure,salary structure,leave policy ,work policy,vacation policy on the right employee tab
        /// 
        /// </summary>
        private void dgvList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           
        }

        /// <summary>
        /// dgvList_Cell mouse double click
        /// </summary>
        private void dgvList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if current type is employee then load the corresponding employee form 
            if (eCurrentType == eNavigator.EMPLOYEE)
            {
                EmployeeID = dgvList.Rows[e.RowIndex].Cells[1].Value.ToInt32();

                new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Employee, (Int32)eMenuID.Employee, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                MblnViewPermission = (ClsCommonSettings.RoleID <=3) ? true : MblnViewPermission;
                if (MblnViewPermission)
                    new FrmEmployee() { PintEmployeeID = EmployeeID }.ShowDialog();
            }
            //if current type is of company then show the corresponding company documents
            else if (eCurrentType == eNavigator.COMPANY)
            {
                DocumentID = dgvList.Rows[e.RowIndex].Cells[1].Value.ToInt32();
                DocumentTypeID = (eDocumentType)dgvList.Rows[e.RowIndex].Cells[2].Value.ToInt32();

                switch (DocumentTypeID)
                {
                    //Trading license document
                    case eDocumentType.TradingLicense:

                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.TradeLicense, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <=3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new frmCompanyTradeLicense() { PintTradeLicenseID = DocumentID }.ShowDialog();
                        break;

                    //Lease agreement document
                    case eDocumentType.LeaseAgreement:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.LeaseAgreement, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new frmCompanyLeaseAgreement() { LeaseID = DocumentID }.ShowDialog();
                        break;

                    case eDocumentType.InsuranceDetails:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.Insurance, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new InsuranceDetails() { PintInsuranceID = DocumentID }.ShowDialog();
                        break;

                    case eDocumentType.CompanyAsset:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.CompanyAssets, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <=3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new FrmCompanyAssets() { AssetID = DocumentID }.ShowDialog();
                        break;

                    //Other documents
                    default:
                        new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.OtherDocuments, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                        MblnViewPermission = (ClsCommonSettings.RoleID <=3) ? true : MblnViewPermission;

                        if (MblnViewPermission)
                            new frmDocumentMasterNew() { DocumentIDNav = DocumentID }.ShowDialog();
                        break;
                }
            }
        }

     

        /// <summary>
        /// Event will be fired when user click the tree advEmployeeTree
        /// </summary>

        private void AdvEmployeeTree_NodeClick(object sender, TreeNodeMouseEventArgs e)
        {
            CompanyID = 0;
            //Clear all the existing tabs from the employee
            tabEmployee.Tabs.Clear();

            if (e.Node != null && e.Node.Tag != null)
            {
                CompanyID = e.Node.Tag.ToInt32();
                //Load all employees based on the companyid
                FillAllEmployeesByCompany(string.Empty);

            }

        }


        /// <summary>
        /// Easy View Mouse Click Event
        /// </summary>
        private void lstEasyViewLeft_Click(object sender, EventArgs e)
        {
            //Display the correspnding easy view
            DisplayEasyView();
        }


        /// <summary>
        /// This Event will be fired when user click any of the employee or document type left expand icon in the advDocumentTree
        /// Here the corresponding document types and documents will be load when user click on the expand icon only 
        /// as loading everthing on first time is time consuming
        /// </summary>
        private void AdvDocumentTree_BeforeExpand(object sender, AdvTreeNodeCancelEventArgs e)
        {
            int ReferenceID = 0;
            DataTable dtDocuments;
            DataTable dtAttachedDocuments;
            if (e.Node != null)
            {
                /*Node Ends with 
                   1.Employee -->Selected Node is an employee Name (So need to add all the documents of the selected employee under the selected node)
                   2.Company -->Selected Node is an Company name so need to add all the document types under the selected company
                 */
                if (e.Node.Tag.ToString().EndsWith("Employee"))
                {
                    //ReferenceID -->Operation Type == Company (CompanyID)
                    //            -->OperationType  == Employee (EmployeeID)              
                    e.Node.Nodes.Clear();

                    ReferenceID = e.Node.Tag.ToString().Split('@')[0].ToInt32();

                    //Getting the distinct documents of the selected employee 
                    dtDocuments = clsNavigatorBLL.GetAllDocumentTypesByOperationTypeID((int)eCurrentOperationType, ReferenceID, -1, string.Empty, string.Empty)
                      .DefaultView.ToTable(true, "DocumentTypeID", "DocumentType", "DocumentNumber", "DocumentID");

                    //Iterate through the documents and add the documents under the selected node
                    foreach (DataRow dr in dtDocuments.Rows)
                    {
                        Node nodeDocuments = new Node();
                        nodeDocuments.Text = dr["DocumentType"].ToString() + "-" + dr["DocumentNumber"].ToString();
                        nodeDocuments.Tag = dr["DocumentID"].ToString() + "^" + dr["DocumentTypeID"].ToString() + "@@" + dr["DocumentNumber"].ToString() + "[" + dr["DocumentType"].ToString() + "]" + "@Documents";
                        e.Node.Nodes.Add(nodeDocuments);


                        //Getting all the attached documents of the current documents

                        dtAttachedDocuments = clsNavigatorBLL.GetAttachedDocuments((int)eCurrentOperationType, ReferenceID, dr["DocumentTypeID"].ToInt32(), dr["DocumentID"].ToInt32());

                        //Iterate through all the attached documents of the selected document and add those documents under the current document node
                        foreach (DataRow drAttachedFile in dtAttachedDocuments.Rows)
                        {
                            nodeDocuments.Nodes.Add(new Node()
                            {
                                Text = drAttachedFile["Description"].ToString(),
                                Tag = drAttachedFile["FileName"] + "@AttachedFile"

                            });
                        }
                    }
                }
            }
        }


        /// <summary>
        /// This event will be fired when the selected index of the list lstdocumentleft changed
        /// </summary>
        private void lstDocumentsLeft_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lstDocumentsLeft.Items[0].Selected)
                eCurrentOperationType = eOperationType.Company;
            else
                eCurrentOperationType = eOperationType.Employee;

            //Setting the search water mark text
            SetSearchTextBoxCaption();
            //Get all documents based on the current operation type
            GetAllDocumentsByOperationTypeID();


        }


        /// <summary>
        /// Setting the current row index to -1 when user enter search text
        /// </summary>
        private void txtTopSearch_TextChanged(object sender, EventArgs e)
        {
            CurrentRowIndex = -1;
        }


        /// <summary>
        /// shows the corresponding easy view details when index changes
        /// </summary>
        private void lstEasyViewLeft_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayEasyView();
        }

        #endregion

        #region Functions

        /// <summary>
        /// Loads the corresponding search suggestions
        /// </summary>
        private void FillDocumentSearchSuggestions()
        {
            //Distinct employee name or company name
            DataTable dtDistinctReference;
            DataTable dtDistinctDocumentNumber;


            DocumentSugessionList = new AutoCompleteStringCollection();
            if (dtDocuments.Rows.Count > 0)
            {
                //Taking the distinct reference (company name and employee name)
                dtDistinctReference = dtDocuments.DefaultView.ToTable(true, "Reference");
                foreach (DataRow dr in dtDistinctReference.Rows)
                {
                    DocumentSugessionList.Add(dr["Reference"].ToString());
                }

                //Taking the distinct document number from the datatable dtDocuments
                dtDistinctDocumentNumber = dtDocuments.DefaultView.ToTable(true, "DocumentNo");
                foreach (DataRow dr in dtDistinctDocumentNumber.Rows)
                {
                    DocumentSugessionList.Add(dr["DocumentNo"].ToString());
                }
            }

            txtTopSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtTopSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtTopSearch.AutoCompleteCustomSource = DocumentSugessionList;
        }

        /// <summary>
        /// Loads the employees auto complete suggestion
        /// </summary>
        private void FillEmployeeSearchSuggestions()
        {
            DataTable dtEmployeeAutoCompleteList = clsNavigatorBLL.GetAutoCompleteSuggestionsForEmployee(CompanyID); 

            DocumentSugessionList = new AutoCompleteStringCollection();
            if (dtEmployeeAutoCompleteList.Rows.Count > 0)
            {
                foreach (DataRow dr in dtEmployeeAutoCompleteList.Rows)
                {
                    DocumentSugessionList.Add(dr["Suggestions"].ToString());
                }
            }

            txtTopSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtTopSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtTopSearch.AutoCompleteCustomSource = DocumentSugessionList;
        }

        /// <summary>
        /// Fill Company auto complete suggestion
        /// </summary>
        private void FillCompanySearchSuggestions(DataTable dtDocuments)
        {
        
            DocumentSugessionList = new AutoCompleteStringCollection();
            if (dtDocuments.Rows.Count > 0)
            {
                foreach (DataRow dr in dtDocuments.Rows)
                {
                    DocumentSugessionList.Add(dr["DocumentNo"].ToString());
                }
            }

            txtTopSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtTopSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtTopSearch.AutoCompleteCustomSource = DocumentSugessionList;
        }


        #region EASY VIEW
        /// <summary>
        /// Display the corresponding html for easy view
        /// </summary>
        private void DisplayEasyView()
        {
            if (lstEasyViewLeft.SelectedItems[0] != null)
            {
                int intImageIndex = lstEasyViewLeft.SelectedItems[0].ImageIndex;
                switch (intImageIndex)
                {

                    case 0:
                        webEasyview.Navigate(Application.StartupPath + "\\HTML\\Company_setupArb.html");
                        break;
                    case 1:
                        webEasyview.Navigate(Application.StartupPath + "\\HTML\\Employee_setupArb.html");
                        break;
                    case 2:
                        webEasyview.Navigate(Application.StartupPath + "\\HTML\\ReportsArb.html");
                        break;
                    case 3:
                        webEasyview.Navigate(Application.StartupPath + "\\HTML\\payroll_processArb.html");
                        break;
                    default:
                        webEasyview.Navigate(Application.StartupPath + "\\HTML\\Company_setupArb.html");
                        break;
                }
            }
        }
        /// <summary>
        ///  Loads Company setup  ,for html page used in (Company_setup.html)
        /// </summary>
        /// <param name="Menu"></param>
        public void CallCompanyModule(string Menu)
        {
            switch (Menu)
            {
                case "company":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.NewCompany, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmCompany() { StartPosition = FormStartPosition.CenterScreen}.ShowDialog();
                    break;
                case "nHoliday":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.HolidayCalender, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmHolidayCalender() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "nLeave":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.LeavePolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmLeavePolicy() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "nCurrency":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.Currency, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmCurrencyReference() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "nVacation":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.VacationPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new frmVacationPolicy() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "nSettlement":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.SettlementPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmSettlementPolicy() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "Assets":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.CompanyAssets, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmCompanyAssets() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "nShift":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.ShiftPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmShiftPolicy() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "nWork":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.WorkPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmWorkPolicy() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "nUnearned":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.UnearnedPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmUnearnedPolicy() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
            }
        }
        /// <summary>
        ///  Loads employee setup ,for html page used in (Employee_setup.html)
        /// </summary>
        /// <param name="Menu"></param>
        public void CallEmployeeModule(string Menu)
        {
            switch (Menu)
            {
                case "eEmployee":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Employee, (Int32)eMenuID.Employee, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmEmployee() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eSalary":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Employee, (Int32)eMenuID.SalaryStructure, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmSalaryStructure() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eLeave":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Employee, (Int32)eMenuID.LeaveStructure, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmEmployeeLeaveStructure() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eDeposit":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.Deposit, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmDeposit() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eLoan":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Employee, (Int32)eMenuID.Loan, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmEmployeeLoan() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eLeaveEntry":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.LeaveEntry, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmLeaveEntry() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eLeaveExtension":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.LeaveExtension, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmLeaveExtension() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eShiftSchedule":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.ShiftSchedule, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmShiftSchedule() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eTransfer":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.EmployeeTransfer, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmEmployeeTransfer() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eSalaryAdvance":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.SalaryAdvance, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmSalaryAdvance() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eExpense":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.Expense, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmEmployeeExpense() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "eAssetHandOver":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.AssetHandover, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        ShowCompanyAssetOperations();
                    break;
                case "eLoanRepayment":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.LoanRepayment, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmLoanRepayment() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
            }
        }
        //For AttendanceForm
        private void ShowCompanyAssetOperations()
        {
            FrmCompanyAssetOperations objCompanyAssetOperations = null;
            try
            {
                objCompanyAssetOperations = new FrmCompanyAssetOperations();
                objCompanyAssetOperations.Text = "Asset HandOver";
                objCompanyAssetOperations.MdiParent = this.MdiParent;
                objCompanyAssetOperations.WindowState = FormWindowState.Maximized;
                objCompanyAssetOperations.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objCompanyAssetOperations.Dispose();
                System.GC.Collect();
                ShowCompanyAssetOperations();
            }
        }
        #region PayrollModule
        //For  Salary Relese
        public void CallSalaryRelease()
        {
            FrmSalaryRelease nwRelease = new FrmSalaryRelease();
            try
            {

                nwRelease.MdiParent = this;


                if (ClsCommonSettings.IsArabicView)
                    nwRelease.Text = "الإصدار المرتبات " + this.MdiChildren.Length.ToString();
                else
                    nwRelease.Text = "Salary Release " + this.MdiChildren.Length.ToString();

                nwRelease.WindowState = FormWindowState.Maximized;
                nwRelease.Show();
                nwRelease.Update();
            }
            catch (OutOfMemoryException ex)
            {
                nwRelease.Dispose();
                System.GC.Collect();
                CallSalaryRelease();
            }
        }
        //For  Salary Process
        public void CallSalaryProcess()
        {
            FrmSalaryProcessRelease nwRelease = new FrmSalaryProcessRelease();
            try
            {

                nwRelease.MdiParent = this;

                if (ClsCommonSettings.IsArabicView)
                    nwRelease.Text = "عملية التسجيل لل " + this.MdiChildren.Length.ToString();
                else
                    nwRelease.Text = "Salary Process " + this.MdiChildren.Length.ToString();

                nwRelease.WindowState = FormWindowState.Maximized;
                nwRelease.Show();
                nwRelease.Update();
            }
            catch (OutOfMemoryException ex)
            {
                nwRelease.Dispose();
                System.GC.Collect();
                CallSalaryProcess();
            }
        }
        //For AttendanceForm
        private void ShowAttendance()
        {
            FrmAttendance objAttendance = null;
            try
            {
                objAttendance = new FrmAttendance();
                objAttendance.Text = "Attendance";
                objAttendance.MdiParent = this.MdiParent;
                objAttendance.WindowState = FormWindowState.Maximized;
                objAttendance.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objAttendance.Dispose();
                System.GC.Collect();
                ShowAttendance();
            }
        }
        #endregion
        /// <summary>
        ///Loads payroll , for html page used in (payroll_process.html) 
        /// </summary>
        /// <param name="Menu"></param>
        public void CallPayrollModule(string Menu)
        {
           
            switch (Menu)
            {
               
                case "pAttendance":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.Attendance, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        ShowAttendance();
                    break;
                  
                case "pSalaryProcess":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SalaryProcess, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        CallSalaryProcess();
                    break;
                   
                case "pSalaryRelease":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SalaryRelease, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        CallSalaryRelease();
                    break;
                case "pvacationprocess":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.VacationProcess, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmVacationEntry() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "psettlementProcess":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SettlementProcess, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        new FrmSettlementEntry() { StartPosition = FormStartPosition.CenterScreen }.ShowDialog();
                    break;
                case "pvacationreport":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.VacationReport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        Program.objMain.ShowVacationReport();
                    break;
                case "psettlementreport":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SettlementReport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        Program.objMain.ShowSettlementReport();
                    break;
            }
        }
        /// <summary>
        /// Loads Reports,for html page used in (Reports.html) 
        /// </summary>
        /// <param name="Menu"></param>
        public void CallWinformReports(string Menu)
        {
            switch (Menu)
            {
                case "EmployeeProfile":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.EmployeeProfileReport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        Program.objMain.ShowEmployeeReport();
                    break;
                case "SalaryStructure":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.SalaryStructureReport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        Program.objMain.ShowSalStructureReport();
                    break;
                case "Attendance":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.AttendanceReport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        Program.objMain.ShowAttendanceReport();
                    break;
                case "Documents":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.DocumentReport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        Program.objMain.ShowDocument();
                    break;
                case "PaySlip":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.PaySlip, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        Program.objMain.ShowSalarySlip();
                    break;
                case "Payments":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.PaymentReport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        Program.objMain.ShowPaymetsReport();
                    break;
                case "Leave":
                    new clsBLLPermissionSettings().GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Reports, (Int32)eMenuID.LeaveSummaryReport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);
                    MblnViewPermission = (ClsCommonSettings.RoleID <= 3) ? true : MblnViewPermission;
                    if (MblnViewPermission)
                        Program.objMain.ShowLeaveSummaryReport();
                    break;
            }
        }
        #endregion

        /// <summary>
        /// Loading all the document types based on the operation type (company or employee)
        /// </summary>
        private void GetAllDocumentsByOperationTypeID()
        {
            //Clears all the existing nodes from the advdocumenttree
            AdvDocumentTree.Nodes.Clear();

            //Getting all the documents based on the operation type
            dtDocuments = clsNavigatorBLL.GetAllDocumentsByOperationTypeID((int)eCurrentOperationType, -1, (int)eDocumentType.All, string.Empty, string.Empty);

            //Filling the autocomplete suggestion text box with the complete document detail
            FillDocumentSearchSuggestions();

            //Getting all the distinct reference(company name or employee name) from dtDocuments(all documents)
            //if the currentOperation type is company then reference is the company name else employee name
            DataTable dtDistinctReference = dtDocuments.DefaultView.ToTable(true, "ReferenceID", "Reference", "Gender");

            //Iterate through distinct reference
            foreach (DataRow dr in dtDistinctReference.Rows)
            {
                AdvDocumentTree.Nodes.Add(new Node()
                {
                    Tag = dr["ReferenceID"].ToString() + "@Employee",
                    Text = dr["Reference"].ToString(),
                    ExpandVisibility = eNodeExpandVisibility.Visible,

                    //Constructing the image based on the operation type
                    //If operation type is employee then add image based on the employee gender
                    //else company image

                    Image = eCurrentOperationType == eOperationType.Employee ? (dr["Gender"].ToBoolean() == true ? ImageListEmployee.Images[2] : ImageListEmployee.Images[9]) : ImgListNavigator.Images[0]
                });
            }

            //Shows the total no of employees for operation type employee only at the bottom side
            if (eCurrentOperationType == eOperationType.Employee)
                pnlCount.Text = "Total Employees :" + AdvDocumentTree.Nodes.Count;
            else
                pnlCount.Text = "";
        }


        /// <summary>
        /// Return the appropriate name of the tab and loads the appopriate web page to the corresponding web browser
        /// based on the dtPoliciescount
        /// </summary>
        private string GetTabCaption(WebBrowser browser, int CurrentIndex, DataTable dtPoliciesCount)
        {
            string Result = string.Empty;

            //Here the result will be empty if there is no details exist against the corresponding type
            //to skip creating the corresponding type tab
            switch (CurrentIndex)
            {
                //Employee information tab
                case (int)eEmployeeWebPage.EmployeeInformation:
                     browser.DocumentText = new ClsWebform().GetWebFormData("Employee Information", EmailFormID.Employee, clsNavigatorBLL.GetEmployeeWebPage(EmployeeID));
                    //No need to check if data exists for the type employee information as it is a mandatory field
                    Result = "Employee Information";
                    break;

                //Employee Salary structure tab        
                case (int)eEmployeeWebPage.EmployeeSalaryStructure:

                    browser.DocumentText = new ClsWebform().GetWebFormData("Salary Structure", EmailFormID.SalaryStructure, clsBLLSalaryStructure.GetSalaryStructureEmail(EmployeeID));

                    //if there is no salary structure then return empty 
                    Result = dtPoliciesCount.Rows[0]["SalaryStructureID"] != DBNull.Value ? "Salary Structure" : "";
                    break;

                //Employee leave policy tab        
                case (int)eEmployeeWebPage.LeavePolicy:

                     browser.DocumentText = new ClsWebform().GetWebFormData("Leave Policy", EmailFormID.LeavePolicy, clsNavigatorBLL.GetLeavePolicyWebPage(EmployeeID));

                    //if there is no leave policy exists then return empty string
                    Result = dtPoliciesCount.Rows[0]["LeavePolicyID"] != DBNull.Value ? "Leave Policy" : "";
                    break;

                //Employee work policy tab
                case (int)eEmployeeWebPage.WorkPolicy:
                    browser.DocumentText = new ClsWebform().GetWebFormData("Leave Structure", EmailFormID.WorkPolicy, clsNavigatorBLL.GetWorkPolicyWebPage(EmployeeID));
                    Result = Result = dtPoliciesCount.Rows[0]["WorkPolicyID"] != DBNull.Value ? "Work Policy" : "";
                    break;

                //Emplyee leave structure tab
                case (int)eEmployeeWebPage.LeaveStructure:
                    //Getting the details of the leave structure of the selected employee
                    DataSet dtLeaveStructure = clsNavigatorBLL.GetLeaveStructureWebPage(EmployeeID);
                    browser.DocumentText = new ClsWebform().GetWebFormData("Leave Structure", EmailFormID.LeaveStructure, dtLeaveStructure);

                    //if there is no leave strucuture exists then return empty string
                    Result = dtLeaveStructure.Tables[1].Rows.Count > 0 ? "Leave Structure" : "";
                    break;

                //Employee vacation policy tab
                case (int)eEmployeeWebPage.VacationPolicy:
                    browser.DocumentText = new ClsWebform().GetWebFormData("Vacation Policy", EmailFormID.VacationPolicy, clsNavigatorBLL.GetVacationPolicyWebPage(EmployeeID));

                    //if there is no vacation policy exists then return empty string
                    Result = Result = Result = dtPoliciesCount.Rows[0]["VacationPolicyID"] != DBNull.Value ? "Vacation Policy" : "";
                    break;

                default:
                    Result = "";
                    break;
            }
            return Result;

        }


        /// <summary>
        /// Show the file based on the file extension
        /// </summary>
        private void ShowFileContents(string Filename)
        {
            pnlUnSupportedFileTypes.Visible = false;
            ImageControl.BringToFront();
            FileInfo fInfo = new FileInfo(Filename);

            try
            {
                //Checking if the file is a valid image file
                if (IsValidImage(Filename))
                {
                    ImageControl.Image = System.Drawing.Image.FromFile(Filename.ToString());
                    ImageControl.fittoscreen();
                }
                //if the file is of text type then display the details in the web browser
                else if (fInfo.Extension.ToLower() == ".txt")
                {
                    webEasyview.BringToFront();
                    webEasyview.Url = new Uri(Filename);
                }
                //if the file type is not an image file and text file then make panel pnlUnSupportedFileTypes visible
                else
                {
                    pnlUnSupportedFileTypes.Visible = true;
                    pnlUnSupportedFileTypes.BringToFront();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                fInfo = null;
            }
        }


        /// <summary>
        ///Checking if the file is a valid image file
        /// </summary>
        /// <param name="flName"></param>
        /// <returns></returns>
        private bool IsValidImage(string FileName)
        {
            try
            {
                string ext = FileName.Remove(0, FileName.LastIndexOf(".")).Trim();
                if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif" || ext.ToLower() == ".bmp" || ext.ToLower() == ".png" || ext.ToLower() == ".tif")
                    return true;
                else
                    return false;
            }
            catch (Exception) { return false; }
        }


        /// <summary>
        /// Load the middle datagrid(dgvList) based on the document navigator and employee navigator
        /// If it is employee then load all the employee else load all the documents of the currently selected document type of the selected company
        /// </summary>
        private void SetGrid(DataTable dt)
        {

            CanLoad = false;//PREVENT THE GRID FROM LOADING
            dgvList.DataSource = null;
            CanLoad = true;
            dgvList.DataSource = dt;
            int DocumentTypeID =0;

            eWorkStatusID eCurrentWorkStatus;
            if (eCurrentType == eNavigator.COMPANY)
            {

                //Filling auto complete suggestion for company documents
                FillCompanySearchSuggestions(dt);


                //DocumentID
                dgvList.Columns[1].Visible = false;
                //DocumentTypeID
                dgvList.Columns[2].Visible = false;
                //Document Number
                dgvList.Columns[3].Visible = true;

                dgvList.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;



                //Setting the grid image based on the document type
                for (int i = 0; i < dgvList.Rows.Count; i++)
                {
                    DocumentTypeID = dgvList.Rows[i].Cells[2].Value.ToInt32();


                    switch (DocumentTypeID)
                    {
                        case (int)eDocumentType.TradingLicense:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListDocuments.Images[0];
                            break;

                        case (int)eDocumentType.LeaseAgreement:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListDocuments.Images[1];
                            break;

                        case (int)eDocumentType.InsuranceDetails:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListDocuments.Images[2];
                            break;

                        default:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListDocuments.Images[3];
                            break;
                    }
                }
            }
            else if (eCurrentType == eNavigator.EMPLOYEE)
            {
                //EmployeeID
                dgvList.Columns[1].Visible = false;
                //Gender
                dgvList.Columns[2].Visible = false;
                //Employee Number
                dgvList.Columns[3].Visible = true;
                //First Name
                dgvList.Columns[4].Visible = true;

                dgvList.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                //WorkStatusID
                dgvList.Columns[5].Visible = false;

                //Setting the grid image based on the gender of the employee
                for (int i = 0; i < dgvList.Rows.Count; i++)
                {

                    eCurrentWorkStatus = ((eWorkStatusID)dgvList.Rows[i].Cells[5].Value.ToInt16());

                    switch (eCurrentWorkStatus)
                    {

                        case eWorkStatusID.Absconding:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListEmployee.Images[0];
                            break;

                        case eWorkStatusID.Expired:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListEmployee.Images[1];
                            break;

                        case eWorkStatusID.InService:

                            dgvList.Rows[i].Cells["clmImage"].Value = dgvList.Rows[i].Cells[2].Value.ToBoolean() == true ? ImageListEmployee.Images[2] : ImageListEmployee.Images[9];

                            break;

                        case eWorkStatusID.Probation:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListEmployee.Images[3];
                            break;

                        case eWorkStatusID.Resigned:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListEmployee.Images[4];
                            break;

                        case eWorkStatusID.Retired:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListEmployee.Images[5];
                            break;

                        case eWorkStatusID.Terminated:
                            dgvList.Rows[i].Cells["clmImage"].Value = ImageListEmployee.Images[6];
                            break;

                        default:
                            break;
                    }

                }

                pnlCount.Text = "Total Employees :" + dgvList.Rows.Count;
                if (dgvList.Rows.Count == 0)
                {
                    tabEmployee.Tabs.Clear();
                }
            }
            //if (dt.Rows.Count > 0)
            //    dgvList_CellMouseClick(new object(), new DataGridViewCellMouseEventArgs(0, 0, 0, 0, new MouseEventArgs(MouseButtons.Left, 0, 0, 0, 0)));

        }


        /// <summary>
        /// Getting all the documents of the currently selected document types of the company
        /// </summary>
        private void FillCompanyDocumentsByDocumentTypeID()
        {
            //Loading the grid with all document
            SetGrid(clsNavigatorBLL.GetAllDocuments((int)DocumentTypeID, CompanyID));
        }


        /// <summary>
        /// Fill all employees based on the currently selected companyid
        /// </summary>
        private void FillAllEmployeesByCompany(string SearchKey)
        {
            //Filling the auto complete suggestions for the employee
            FillEmployeeSearchSuggestions();
            //Loading the grid with all the employees
            SetGrid(clsNavigatorBLL.GetAllEmployeesByCompanyID(CompanyID, SearchKey, cboEmpWorkstatus.SelectedValue.ToInt32(), ((eFilterTypes)CboEmpFilterTypes.SelectedIndex), CboEmpFilter.SelectedValue.ToInt32()));
        }


        /// <summary>
        /// Shows the employee details on the webbrowser when user clicks any of the employee
        /// </summary>
        private void LoadEmployeeWebPage()
        {
            tabEmployee.Tabs.Clear();

            DevComponents.DotNetBar.TabControlPanel tabPanel;
            WebBrowser browser;
            DevComponents.DotNetBar.TabItem tabItem;

            /*For employee show the following webpages
                1.Employee personal information
                2.Employee Salary Structure
                3.Employee Leave Structure
                4.Employee Work Policy
            */

            //Getting all the policies assigned for the selected employee
            //used for deciding whether we need to create a tab for the selected policy or not
            //if there is no details for the selected policy then no need to create tab
            DataTable dtPoliciesCount = clsNavigatorBLL.GetAllExistingPolicies(EmployeeID);

            //Section for creating the dynamic tabs
            for (int i = 1; i <= 6; i++)
            {
                browser = new WebBrowser();
                browser.Dock = DockStyle.Fill;

                tabPanel = new TabControlPanel();
                tabPanel.Controls.Add(browser);
                tabPanel.Dock = System.Windows.Forms.DockStyle.Fill;


                tabItem = new TabItem();
                tabItem.AttachedControl = tabPanel;

                //Getting the tab text as well as load the appropriate web page in to the webbrowser
                tabItem.Text = GetTabCaption(browser, i, dtPoliciesCount);
                tabPanel.TabItem = tabItem;
                tabEmployee.Controls.Add(tabPanel);

                //if tabitem.text is empty then no need to create the tab for the selecte policy 
                //bcoz there is no details exist for the selected policy
                if (tabItem.Text != string.Empty)
                    tabEmployee.Tabs.Add(tabItem);
            }

        }


        /// <summary>
        /// Filling all the companies and branches in the company tree
        /// </summary>
        private void FillAllCompaniesAndBranch()
        {
            //Handles the company and branch creation for both company and employee
            //If th current selection is employee then no need to load the document types

            AdvTree advTree = eCurrentType == eNavigator.COMPANY ? AdvCompanyTree : AdvEmployeeTree;

            //Clear all the existing nodes from the company tree
            advTree.Nodes.Clear();
            //Getting all companies and branches
            DataTable dt = clsNavigatorBLL.GetAllCompaniesAndBranch();

            //Getting all companies document types
            DataTable dtDocumentTypes = eCurrentType == eNavigator.EMPLOYEE ? null : clsNavigatorBLL.GetAllCompaniesDocumentType();


            //Taking the companies from the dt collection
         var Companies = dt.AsEnumerable().Where(c => c.Field<int>("ParentID") == 0);
           // var Companies = dt.AsEnumerable();
            foreach (var company in Companies)
            {
                //Create Company node
                Node NodeCompany = new Node();
                NodeCompany.Tag = company.Field<int>("CompanyId");
                NodeCompany.Text = company.Field<string>("CompanyName");
                NodeCompany.ImageIndex = 0;
                NodeCompany.Selectable = false;
                advTree.Nodes.Add(NodeCompany);

                //Adding the same company under the company node
                Node NodeMasterCompany = new Node();
                NodeMasterCompany.Tag = company.Field<int>("CompanyId");
                NodeMasterCompany.Text = company.Field<string>("CompanyName");
                NodeMasterCompany.ImageIndex = 0;
                NodeMasterCompany.Selectable = eCurrentType == eNavigator.COMPANY ? false : true;
                NodeCompany.Nodes.Add(NodeMasterCompany);

                //Getting the document types of the main company
                //dtDocumentTypes will be null for employee
                if (dtDocumentTypes != null)
                {
                    var documentTypesCompany = dtDocumentTypes.AsEnumerable().Where(d => d.Field<int>("CompanyID") == company.Field<int>("CompanyId"));

                    //Adding the document types under the main company node

                    foreach (var documentType in documentTypesCompany)
                    {
                        NodeMasterCompany.Nodes.Add(new Node()
                        {
                            Tag = documentType.Field<int>("DocumentTypeID").ToString() + "@" + documentType.Field<int>("CompanyID").ToString(),
                            Text = documentType.Field<string>("DocumentType")
                        });
                    }
                }

                //Taking all the branches of the current company

                var Branches = dt.AsEnumerable().Where(b => b.Field<int>("ParentID") == company.Field<int>("CompanyID"));

                //Add branch node under the corresponding company node
                foreach (var Branch in Branches)
                {

                    Node NodeBranch = new Node();
                    NodeBranch.Tag = Branch.Field<int>("CompanyID");
                    NodeBranch.Text = Branch.Field<string>("CompanyName");
                    NodeBranch.ImageIndex = 1;
                    NodeCompany.Nodes.Add(NodeBranch);


                    //Getting the document types of the branch company
                    //dtDocumentTypes will be null for employee 
                    if (dtDocumentTypes != null)
                    {
                        var documentTypeBranch = dtDocumentTypes.AsEnumerable().Where(d => d.Field<int>("CompanyID") == Branch.Field<int>("CompanyID"));

                        //Adding the document types under the branch company node

                        foreach (var documentType in documentTypeBranch)
                        {
                            NodeBranch.Nodes.Add(new Node()
                            {
                                Tag = documentType.Field<int>("DocumentTypeID").ToString() + "@" + documentType.Field<int>("CompanyID").ToString(),
                                Text = documentType.Field<string>("DocumentType")
                            });
                        }
                    }

                }
            }

            advTree.ExpandAll();

          
        }


        /// <summary>
        /// Sets the permission of the form
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings ObjPermission = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                DataTable dt = ObjPermission.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID);
                if (dt.Rows.Count == 0)
                {
                    btnNavCompany.Enabled = btnClient.Enabled = btnEasyview.Enabled = btnDocuments.Enabled = btnNavTask.Enabled = false;
                }
                else
                {
                    /* -------------------------------- Masters -------------------------------------- */
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.NavigationCompany).ToString();
                    btnNavCompany.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.NavigationEmployee).ToString();
                    btnClient.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.NavigationEasyView).ToString();
                    btnEasyview.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.NavigationTasks).ToString();
                    //btnNavTask.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.NavigationDocuments).ToString();
                    btnDocuments.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                }
            }
        }

        /// <summary>
        /// Setting the water mark text of the search text box
        /// </summary>
        private void SetSearchTextBoxCaption()
        {
            txtTopSearch.Text = string.Empty;
            //setting current search row index to default value 
            CurrentRowIndex = -1;

            if (eCurrentType == eNavigator.COMPANY)
            {
                txtTopSearch.WatermarkText = "Search by document number";
            }
            else if (eCurrentType == eNavigator.EMPLOYEE)
            {
                txtTopSearch.WatermarkText = "Search by employee number/employee first name";
            }
            else if (eCurrentType == eNavigator.DOCUMENTS)
            {
                if(eCurrentOperationType == eOperationType.Company) 
                    txtTopSearch.WatermarkText = "Search by company name/document number";
                else
                    txtTopSearch.WatermarkText = "Search by employee first name/document number";
            }
            else
                txtTopSearch.WatermarkText = "";
        }

        /// <summary>
        /// Set Visibility According to Selected Pane
        /// </summary>
        private void SetVisibility()
        {
            //Clear all the existing controls values
            ClearAll();
            //Right side web browser for showing the web pages for employee and company
            tabEmployee.BringToFront();

            //AdvEmployeeTree is in front for company and employee
            AdvEmployeeTree.BringToFront();

            //AdvDocumentTree is in front for documents only
            AdvDocumentTree.SendToBack();

            //Middle Top panel -->Employee wise filteration for employee
            ExpEmployeeFilteration.Visible = false;

            //panel middle will be invisible for easy view
            panelMiddle.Visible = true;
            
            //Making top search panel visible
            SetSearchControlVisibility(true);
            expandableSplitter1.Visible = true;
            //Setting the watermark text
            SetSearchTextBoxCaption();
            switch (eCurrentType)
            {
                case eNavigator.COMPANY:

                    FillAllCompaniesAndBranch();
                    break;

                case eNavigator.DOCUMENTS:
                    //Showing the total no of employees
                    pnlCount.Visible = true;
                    //Bringing the image control to front
                    ImageControl.BringToFront();
                    AdvDocumentTree.BringToFront();

                    //Setting Company document as default
                    //Fetching all the documents based on the company and employee handles in the lstdocumentselected index change event
                    lstDocumentsLeft.Items[0].Selected = true;
                    lstDocumentsLeft.Select();
                    break;


                case eNavigator.EMPLOYEE:
                    //Showing the total no of employees
                    pnlCount.Visible = true;
                    FillAllCompaniesAndBranch();
                    //Middle employee wise filteration panel
                    ExpEmployeeFilteration.Visible = true;

                    break;
                case eNavigator.EASYVIEW:
                    expandableSplitter1.Visible = false ;
                    //No need of middle panel for easy view
                    panelMiddle.Visible = false;
                    
                    //Bringing the webeasyview to front
                    webEasyview.BringToFront();
                    webEasyview.ObjectForScripting = this;

                    //making the easyview first item as selected  and load the corresponding easy view
                    //loading the corresponding easyview handled in the selected index change event
                    lstEasyViewLeft.Items[0].Selected = true;
                    lstEasyViewLeft.Select();

                    //making top search panel invisible
                    SetSearchControlVisibility(false);
                    DisplayEasyView();
                    break;



                default:
                    break;
            }
        }


        /// <summary>
        /// Clear all the controls
        /// </summary>
        private void ClearAll()
        {
            //Clear all the employee tabs
            tabEmployee.Tabs.Clear();

            //setting the image control image to null
            ImageControl.Image = null;

            //clear all the nodes from the advtreedocuments
            AdvDocumentTree.Nodes.Clear();

            //clear the datagrid dgvlist
            dgvList.DataSource = null;

            //Making pnlcount invisible
            pnlCount.Visible = false;
            pnlCount.Text = "";

            //Clear the current selection from the list documents
            lstDocumentsLeft.SelectedItems.Clear();

            //Clear the current selection from the list easy view
            lstEasyViewLeft.SelectedItems.Clear();

            //clear all the suggestions
            txtTopSearch.AutoCompleteCustomSource = null;
        }


        /// <summary>
        /// Search Employee 
        /// </summary>
        private void SearchEmployee()
        {

            bool IsFound = false;
            int CurrentIndex =-2;
            int Iterator = 0;

            foreach (DataGridViewRow dr in dgvList.Rows)
            {
                if ((dr.Cells[3].Value.ToString().Contains(txtTopSearch.Text)) || (dr.Cells[4].Value.ToString().Contains(txtTopSearch.Text)))
                {
                    
                    CurrentIndex = CurrentIndex + 1;

                    if (CurrentIndex == CurrentRowIndex)
                    {
                        CurrentRowIndex = CurrentRowIndex + 1;
                        dgvList.CurrentCell = dgvList[0, dr.Cells[3].RowIndex];
                        dgvList_SelectionChanged(new object(), new EventArgs());
                        IsFound = true;
                        break;
                    }
                }
                Iterator = Iterator + 1;

                if (Iterator == dgvList.Rows.Count && CurrentRowIndex >=0)
                {
                    CurrentRowIndex = -1;
                    SearchEmployee();
                    IsFound = true;
                    break;
                }
            }

            if (!IsFound)
                MessageBox.Show("No data found", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);  
        }


        /// <summary>
        /// Search Company documents 
        /// </summary>
        private void SearchCompanyDocuments()
        {
            bool IsFound = false;
            int CurrentIndex = -2;
            int Iterator = 0;
            foreach (DataGridViewRow dr in dgvList.Rows)
            {
                if (dr.Cells[3].Value.ToString().Contains(txtTopSearch.Text))
                {
                    CurrentIndex = CurrentIndex + 1;

                    if (CurrentIndex == CurrentRowIndex)
                    {
                        CurrentRowIndex = CurrentRowIndex + 1;
                        dgvList.CurrentCell = dgvList[0, dr.Cells[3].RowIndex];
                        dgvList_SelectionChanged(new object(), new EventArgs());
                        IsFound = true;
                        break;
                    }
                }
                Iterator = Iterator + 1;
                if (Iterator == dgvList.Rows.Count && CurrentRowIndex >= 0)
                {
                    CurrentRowIndex = -1;
                    SearchCompanyDocuments();
                    IsFound = true;
                    break;
                }


            }

            if (!IsFound)
                MessageBox.Show("No data found", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);  
        }


        /// <summary>
        /// Search Documents
        /// </summary>
        private void SearchDocumentNavigator()
        {
            CurrentRowIndex = CurrentRowIndex + 1;
            string Reference = "";
            string DocumentNumber = "";

            bool CanContinue = true;

            //Getting all the documents based on the current search item (company name or employee name or document no)
            DataTable dt = clsNavigatorBLL.GetAllDocumentsByOperationTypeID((int)eCurrentOperationType, -1, (int)eDocumentType.All, string.Empty, txtTopSearch.Text.Trim());

            if (CurrentRowIndex >= dt.Rows.Count && dt.Rows.Count >0)
            {
                CurrentRowIndex = -1;
                SearchDocumentNavigator();
            }
            if (dt.Rows.Count > 0)
            {
                //Getting the employee name or company name based on the operation type
                Reference = dt.Rows[CurrentRowIndex]["Reference"].ToString();

                //Getting the document number from the current search type
                DocumentNumber = dt.Rows[CurrentRowIndex]["DocumentType"].ToString() + "-" + dt.Rows[CurrentRowIndex]["DocumentNumber"].ToString();
            }
            else
            {
                MessageBox.Show("No data found", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                AdvDocumentTree.DeselectNode(AdvDocumentTree.SelectedNode, eTreeAction.Code); 
            }


            //Iterate through all the nodes in the advDocumentTree
            foreach (Node node in AdvDocumentTree.Nodes)
            {
                if (!CanContinue)
                    break;
                //Iterate untill the main node text matches with the reference 

                if (node.Text.Trim().Equals(Reference))
                {
                    //if the reference is equal to the search text then it means search item is the main node
                    if (Reference == txtTopSearch.Text)
                    {
                        //setting the selected node as the main node
                        AdvDocumentTree.SelectedNode = node;
                        CanContinue = false;
                        break;
                    }
                    //otherwise the search text is of the documens of the current employee
                    else
                    {
                        //Filling all the documents of the current employee
                        AdvDocumentTree_BeforeExpand(node, new AdvTreeNodeCancelEventArgs(eTreeAction.Code, node));

                        //Iterate through all the documents untill it matches with the search text
                        node.Expand();
                        foreach (Node n1 in node.Nodes)
                        {
                            if (n1.Text.Trim() == DocumentNumber)
                            {
                               
                                AdvDocumentTree.SelectedNode = n1;
                                CanContinue = false;
                                break;
                            }
                        }
                    }
                }
            }
            AdvDocumentTree.Select();
        }


        /// <summary>
        /// Make Top Search controls visible or not based on the current selected navigation 
        /// </summary>
        /// <param name="IsVisible"></param>
        private void SetSearchControlVisibility(bool IsVisible)
        {
            txtTopSearch.Enabled = btnTopDocumentSearch.Enabled = IsVisible;
        }

        /// <summary>
        /// Initialize the form
        /// </summary>
        private void Initialize()
        {

            CanLoad = false;

            //setting easy view as default selection
            btnEasyview.Checked = true;
            eCurrentType = eNavigator.EASYVIEW;
            //set visibility based for the easy view
            SetVisibility();

            //set form permission
            SetPermissions();

            //Filling the employee workstatus for employee search
            FillEmployeeWorkStatus();

            FillAllFilterationTypes();

            
            cboEmpWorkstatus.Visible = true;
            CanLoad = true;
        }
        #endregion

        private void dgvList_SelectionChanged(object sender, EventArgs e)
        {
            if (CanLoad)
            {
                if (dgvList.CurrentCell.RowIndex >= 0)
                {
                    //Current Type is company
                    if (eCurrentType == eNavigator.COMPANY)
                    {
                        DocumentID = dgvList.Rows[dgvList.CurrentCell.RowIndex].Cells[1].Value.ToInt32();
                        DocumentTypeID = (eDocumentType)dgvList.Rows[dgvList.CurrentCell.RowIndex].Cells[2].Value.ToInt32();

                        webEasyview.DocumentText = "";
                        webEasyview.BringToFront();

                        switch (DocumentTypeID)
                        {
                            //Show the details of the trade license
                            case eDocumentType.TradingLicense:
                                webEasyview.DocumentText = new ClsWebform().GetWebFormData("Trade License", EmailFormID.TradeLicense, new clsBLLTradeLicense().DisplayTradeLicenseReport(DocumentID));
                                break;

                            //Show the details of lease agreement
                            case eDocumentType.LeaseAgreement:
                                webEasyview.DocumentText = new ClsWebform().GetWebFormData("Lease Agreement", EmailFormID.LeaseAgreement, new clsBLLLeaseAgreement().DisplayLeaseAgreementReport(DocumentID));
                                break;

                            //Insurance Details
                            case eDocumentType.InsuranceDetails :
                                webEasyview.DocumentText = new ClsWebform().GetWebFormData("Insurance Details", EmailFormID.InsuranceDetails, new clsBLLInsuranceDetails().DisplayInsuranceReport(DocumentID));
                                break;

                            //Company Asset Details
                            case eDocumentType.CompanyAsset:
                                webEasyview.DocumentText = new ClsWebform().GetWebFormData("Company Asset", EmailFormID.CompanyAsset, clsBLLCompanyAssets.GetCompanyAssetEmail(DocumentID)); 
                                break;



                            //Show other documents details(handles in default since there is only 3 document types for company)
                            default:
                                webEasyview.DocumentText = new ClsWebform().GetWebFormData("Other Documents", EmailFormID.Documents, new clsBLLDocumentMasterNew().DocumentMasterEmail(DocumentID));
                                break;
                        }
                    }
                    //Current Type is employee
                    else if (eCurrentType == eNavigator.EMPLOYEE)
                    {
                        EmployeeID = dgvList.Rows[dgvList.CurrentCell.RowIndex].Cells[1].Value.ToInt32();
                        //backgroundWorker1 = new BackgroundWorker();

                        //backgroundWorker1.DoWork -= new DoWorkEventHandler(backgroundWorker1_DoWork);
                        //backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);

                        //backgroundWorker1.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
                        //backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
                        //backgroundWorker1.RunWorkerAsync();


                        LoadEmployeeWebPage();

                        
                        dgvList.Focus();
                    }
                }
            }
        }



        private void FillAllFilterationTypes()
        {
            CboEmpFilterTypes.Items.Add("All");
            CboEmpFilterTypes.Items.Add("Department");
            CboEmpFilterTypes.Items.Add("Designation");
            CboEmpFilterTypes.Items.Add("Employment Type");
            CboEmpFilterTypes.SelectedIndex = 0;
        }


        private void FillAllFilterations()
        {
            int CurrentFilterType = CboEmpFilterTypes.SelectedIndex;

            eFilterTypes eCurrentType =(eFilterTypes) CboEmpFilterTypes.SelectedIndex;
            DataTable dt = null;
            DataRow dr = null;

            switch (eCurrentType)
            {
                case (eFilterTypes.All):
                    dt = new DataTable();
                    dt.Columns.Add("Data");
                    dt.Columns.Add("Value");

                    dr = dt.NewRow();
                    dr["Data"] = "All";
                    dr["Value"] = "0";

                    dt.Rows.Add(dr);
                    CboEmpFilter.DataSource = dt;
                    CboEmpFilter.DisplayMember = "Data";
                    CboEmpFilter.ValueMember = "Value";
                  

                    break;

                case (eFilterTypes.Department):
                    dt = clsBLLReportCommon.GetAllDepartments();
                    dt.Rows.RemoveAt(0);
                    dr = dt.NewRow();
                    dr["DepartmentID"] = "0";
                    dr["Department"] = "All";

                    dt.Rows.InsertAt(dr, 0);

                    CboEmpFilter.DataSource = dt;
                    CboEmpFilter.DisplayMember = "Department";
                    CboEmpFilter.ValueMember = "DepartmentID";
                    break;

                case (eFilterTypes.Designation):
                    dt = clsBLLReportCommon.GetAllDesignation();
                    dt.Rows.RemoveAt(0);
                    dr = dt.NewRow();

                    dr["DesignationID"] = "0";
                    dr["Designation"] = "All";

                    dt.Rows.InsertAt(dr, 0);
                    CboEmpFilter.DataSource = dt;
                    CboEmpFilter.DisplayMember = "Designation";
                    CboEmpFilter.ValueMember = "DesignationID";
                    break;

                case (eFilterTypes.EmploymentType):
                    dt = clsBLLReportCommon.GetAllEmploymentType();
                    dt.Rows.RemoveAt(0);
                    dr = dt.NewRow();


                    dr["EmploymentTypeID"] = "0";
                    dr["EmploymentType"] = "All";

                    dt.Rows.InsertAt(dr, 0);


                    CboEmpFilter.DataSource = dt;
                    CboEmpFilter.DisplayMember = "EmploymentType";
                    CboEmpFilter.ValueMember = "EmploymentTypeID";
                    break;


                default:
                    break;
            }
        }
        private void FillEmployeeWorkStatus()
        {
            DataTable dt = clsBLLReportCommon.GetAllWorkStatus();
            dt.Rows.RemoveAt(0);
            DataRow dr = dt.NewRow();
            dr["WorkStatus"] = "All";
            dr["WorkStatusID"] = 0;
            dt.Rows.InsertAt(dr, 0);

            cboEmpWorkstatus.DataSource = dt;
            cboEmpWorkstatus.DisplayMembers = "WorkStatus";
            cboEmpWorkstatus.ValueMember = "WorkStatusID";

            int i = cboEmpWorkstatus.Nodes.Count;

            for (int j = 0; j < i; j++)
            {
                Node node = cboEmpWorkstatus.Nodes[j];

                switch (j)
                {
                    case 0:
                        node.ImageIndex = 14;
                        break;

                    case 1:
                        node.ImageIndex = 0;
                        break;

                    case 2:
                        node.ImageIndex = 1;
                        break;

                    case 3:
                        node.ImageIndex = 5;
                        break;

                    case 4:
                        node.ImageIndex = 4;
                        break;

                    case 5:
                        node.ImageIndex = 6;
                        break;

                    case 6:
                        node.ImageIndex = 2;
                        break;

                    case 7:
                        node.ImageIndex = 3;
                        break;

                    default:
                        break;
                }
            }
        }




        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
           
        }

        void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = null;
           
        }

        private void AdvCompanyTree_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void AdvCompanyTree_NodeMouseHover(object sender, TreeNodeMouseEventArgs e)
        {
            
        }

        private void AdvCompanyTree_NodeMouseEnter(object sender, TreeNodeMouseEventArgs e)
        {
            //if (e.Node != null && e.Node.Tag != null && e.Node.Tag.ToString().Contains("@"))
            //{
            //    return;
            //}
        }

        private void ExpEmployeeFilteration_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            cboEmpWorkstatus.Visible = true;
          
        }

        private void CboEmpFilterTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllFilterations(); 
        }

        private void CboEmpFilterTypes_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboEmpFilterTypes.DroppedDown = false; 
        }

        private void CboEmpFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboEmpFilter.DroppedDown = false;
        }

        private void cboEmpWorkstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployeesByCompany(string.Empty);
        }

        private void NavigatorArb_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyData == Keys.F2)
                {
                    using (frmCompanySelection objCompanySelection = new frmCompanySelection())
                    {
                        objCompanySelection.BringToFront();
                        objCompanySelection.ShowDialog();

                        if (this.HasChildren)
                        {
                            foreach (Form f in this.MdiChildren)
                                f.Close();
                        }
                        btnNavigator_Click(sender, e);
                    }

                }
            }
            catch
            {
            }



        }
     


    }

    

    
}
