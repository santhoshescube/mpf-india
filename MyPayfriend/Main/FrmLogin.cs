﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using ProjectSettings;

namespace MyPayfriend
{
    public partial class FrmLogin : Form 
    {
        clsBLLLogin MobjClsBLLLogin;
        clsRegistry objRegistry;
        bool blnIsFromFormClose = true;
        public FrmLogin()
        {
            InitializeComponent();

            MobjClsBLLLogin = new clsBLLLogin();
            objRegistry = new clsRegistry();
            CheckArabicStatus();
        }

        private void CheckArabicStatus()
        {
            try
            {
               
                if (ClsCommonSettings.IsArabicViewEnabled)
                {
                    rBtnArabic.Visible = true;
                    rBtnEnglish.Visible = true;
                }
                else
                {
                    rBtnArabic.Visible = false;
                    rBtnEnglish.Visible = false;
                    rBtnEnglish.Checked = true;
                    this.BackgroundImage = global::MyPayfriend.Properties.Resources.EpeopleLogin;
                }
            }
            catch (Exception)
            { }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            ClsMainSettings.blnLoginStatus = false;
            txtUserName.Focus();
        }

        private void SetPassword()
        {
            try
            {
                MobjClsBLLLogin.UpdateRemember(txtUserName.Text.Trim(),chkRemember.Checked);
                if (txtUserName.Text.Trim() != "" && chkRemember.Checked)
                {
                    objRegistry.WriteToRegistry("SOFTWARE\\ES3Tech\\payLogin", "payLogin", txtUserName.Text.Trim(), txtUserName.Text.Trim());
                }
            }
            catch (Exception)
            {
            }
        }


        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                blnIsFromFormClose = false;
                if (ValidateForm())
                {

                    string strPassword = clsBLLCommonUtility.Encrypt(txtPassword.Text.Trim(), ClsCommonSettings.strEncryptionKey);
                    DataTable dtUserDetails = MobjClsBLLLogin.GetUserInfo(txtUserName.Text, strPassword);
                    if (dtUserDetails.Rows.Count > 0)
                    {
                        ClsCommonSettings.IsArabicView = rBtnArabic.Checked;
                       
                        ClsCommonSettings.UserID = Convert.ToInt32(dtUserDetails.Rows[0]["UserID"]);
                        ClsCommonSettings.RoleID = Convert.ToInt32(dtUserDetails.Rows[0]["RoleID"]);
                        ClsCommonSettings.strUserName = Convert.ToString(dtUserDetails.Rows[0]["UserName"]);
                        ClsCommonSettings.strEmployeeName = Convert.ToString(dtUserDetails.Rows[0]["EmployeeName"]);
                        ClsCommonSettings.intEmployeeID = Convert.ToInt32(dtUserDetails.Rows[0]["EmployeeID"]);
                        ClsCommonSettings.intDepartmentID = Convert.ToInt32(dtUserDetails.Rows[0]["DepartmentID"]);
                        ClsCommonSettings.ParentCompanyID = dtUserDetails.Rows[0]["CompanyID"].ToInt32();
                        ClsCommonSettings.CurrentCompanyID = Convert.ToInt32(dtUserDetails.Rows[0]["LoginCompanyID"]);
                        ClsCommonSettings.CurrentCompany = Convert.ToString(dtUserDetails.Rows[0]["LoginCompany"]);
                        ClsCommonSettings.strCompanyName = Convert.ToString(dtUserDetails.Rows[0]["Company"]);
                        ClsCommonSettings.CurrencyID = Convert.ToInt32(dtUserDetails.Rows[0]["CurrencyID"]);
                        ClsCommonSettings.Currency = Convert.ToString(dtUserDetails.Rows[0]["Currency"]);
                        SetPassword();

                        ClsCommonSettings.NationalityCard = MobjClsBLLLogin.GetNationalityIDDocType();
                       // SetCommonSettingsCompanyInfo();
                        SetCommonSettingsConfigurationSettingsInfo();

                        clsBLLCommonUtility objCommonUtility = new clsBLLCommonUtility();
                        DataTable datCurrencyDetails = objCommonUtility.FillCombos(new string[] { "CR.Scale", "CompanyMaster CM  Inner Join CurrencyReference CR On CM.CurrencyId = CR.CurrencyID", "CM.CompanyID = " + ClsCommonSettings.CurrentCompanyID });
                        if (datCurrencyDetails.Rows.Count > 0)
                            ClsCommonSettings.Scale = datCurrencyDetails.Rows[0]["Scale"].ToInt32();

                        ClsMainSettings.blnLoginStatus = true;

                        this.Close();
                    }
                    else
                    {
                        ClsMainSettings.blnLoginStatus = false;
                        lblLoginStatus.Text = "Invalid User Try Again";
                        txtUserName.Focus();
                        Timerlogin.Enabled = true;

                    }
                }
            }
            catch (Exception ex)
            {
                  MessageBox.Show(ex.Message);
            }
        }

        //private void SetCommonSettingsCompanyInfo()
        //{
        //    DataTable dtCompanyInfo = new DataTable();
        //    if(ClsCommonSettings.RoleID==1 || ClsCommonSettings.RoleID == 2)
        //     dtCompanyInfo= MobjClsBLLLogin.GetTopMostCompany();
        //    else
        //        dtCompanyInfo = MobjClsBLLLogin.GetUserCompany();

        //    if (dtCompanyInfo.Rows.Count > 0)
        //    {
        //        ClsCommonSettings.CompanyID = Convert.ToInt32(dtCompanyInfo.Rows[0]["CompanyID"]);
        //        ClsCommonSettings.strCompanyName = Convert.ToString(dtCompanyInfo.Rows[0]["CompanyName"]);
               
        //    }
        //}

       
        private bool ConvertStringToBoolean(string strValue)
        {
            if (strValue == "Yes")
                return true;
            return false;
        }

        public void SetCommonSettingsConfigurationSettingsInfo()
        {
            DataTable dtConfigurationSettings = MobjClsBLLLogin.GetDefaultConfigurationSettings();
            string strValue = "";

            ClsCommonSettings.MessageCaption = GetConfigurationValue("MessageCaption", dtConfigurationSettings);
            ClsCommonSettings.ReportFooter = GetConfigurationValue("ReportFooter", dtConfigurationSettings);

            //ClsCommonSettings.strEmployeeNumberPrefix = GetConfigurationValue("EmployeePrefix", dtConfigurationSettings);
            strValue = GetConfigurationValue("BackupInterval", dtConfigurationSettings);
            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.intBackupInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.intBackupInterval = 0;

            strValue = GetConfigurationValue("DefaultStatusBarMessageTime", dtConfigurationSettings);
            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.TimerInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.TimerInterval = 1000;



            if (Strings.UCase(GetConfigurationValue("SalaryDayIsEditable", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.GlbSalaryDayIsEditable = true;
            }
            else
            {
                ClsCommonSettings.GlbSalaryDayIsEditable = false;
            }


            if (Strings.UCase(GetConfigurationValue("SIFFile50Percentage", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.SIFFile50Percentage = true;
            }
            else
            {
                ClsCommonSettings.SIFFile50Percentage = false;
            }






            if (Strings.UCase(GetConfigurationValue("SalarySlipIsEditable", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.GlSalarySlipIsEditable = true;
            }
            else
            {
                ClsCommonSettings.GlSalarySlipIsEditable = false;
            }


            if (Strings.UCase(GetConfigurationValue("AmountRoundByZero", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.IsAmountRoundByZero = true;
            }
            else
            {
                ClsCommonSettings.IsAmountRoundByZero = false;
            }


            if (Strings.UCase(GetConfigurationValue("VacationBasedOnActualRejoinDate", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.IsVacationBasedOnActualRejoinDate = true;
            }
            else
            {
                ClsCommonSettings.IsVacationBasedOnActualRejoinDate = false;
            }



            if (Strings.UCase(GetConfigurationValue("24HourTimeFormat", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.Glb24HourFormat  = true;
            }
            else
            {
                ClsCommonSettings.Glb24HourFormat = false;
            }


            if (Strings.UCase(GetConfigurationValue("DisplayLeaveSummaryInSalarySlip", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip = true;
            }
            else
            {
                ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip = false;
            }

            if (Strings.UCase(GetConfigurationValue("EnableLiveAttendance", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.AttendanceLiveEnable = true;
            }
            else
            {
                ClsCommonSettings.AttendanceLiveEnable = false;
            }

            if (Strings.UCase(GetConfigurationValue("SendMailToAll", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.SendMailToAll = true;
            }
            else
            {
                ClsCommonSettings.SendMailToAll = false;
            }

           ClsCommonSettings.intLoanAmountPercentage = GetConfigurationValue("LoanInstallmentAmountPercentage", dtConfigurationSettings).ToInt32();

           if (Strings.UCase(GetConfigurationValue("HRPowerEnabled", dtConfigurationSettings)) == "YES")
               ClsCommonSettings.IsHrPowerEnabled = true;
           else
               ClsCommonSettings.IsHrPowerEnabled = false;



           if (Strings.UCase(GetConfigurationValue("AttendanceAutofillForSinglePunch", dtConfigurationSettings)) == "YES")
           {
               ClsCommonSettings.AttendanceAutofillForSinglePunch = true;
           }
           else
           {
               ClsCommonSettings.AttendanceAutofillForSinglePunch = false;
           }

           if (Strings.UCase(GetConfigurationValue("ThirdPartyIntegration", dtConfigurationSettings)) == "YES")
           {
               ClsCommonSettings.ThirdPartyIntegration = true;
           }
           else
           {
               ClsCommonSettings.ThirdPartyIntegration = false;
           }
           if (Strings.UCase(GetConfigurationValue("VacationBasedOnActualRejoinDate", dtConfigurationSettings)) == "YES")
           {
               ClsCommonSettings.IsVacationBasedOnActualRejoinDate = true;
           }
           else
           {
               ClsCommonSettings.IsVacationBasedOnActualRejoinDate = false;
           }
            
        }

        private string GetConfigurationValue(string strConfigurationItem,DataTable dtConfigurationSettings)
        {
            dtConfigurationSettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "'";
            if (dtConfigurationSettings.DefaultView.ToTable().Rows.Count > 0)
                return Convert.ToString(dtConfigurationSettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"]).Trim();
            return "";
        }

        public string GetCompanySettingsValue(string strConfigurationItem, string strSubItem, DataTable dtCompanySettings)
        {
            dtCompanySettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "' And SubItem = '" + strSubItem + "'";
            if (dtCompanySettings.DefaultView.ToTable().Rows.Count > 0)
                return Convert.ToString(dtCompanySettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"]).Trim();
            return "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClsMainSettings.blnLoginStatus = false;
            this.Close();
        }

        private void FrmLogin_Shown(object sender, EventArgs e)
        {
            txtUserName.Select();
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ProcessTabKey(true);
            }
        }

        private bool ValidateForm()
        {
            if (string.IsNullOrEmpty(txtUserName.Text.Trim()))
            {
                MessageBox.Show("Please enter user name");
                txtUserName.Focus();
                return false;
            }
            else if (string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                MessageBox.Show("Please enter password");
                txtPassword.Focus();
                return false;
            }
            return true;
        }

        private void Timerlogin_Tick(object sender, EventArgs e)
        {
             lblLoginStatus.Text = "";
             Timerlogin.Enabled = false;
        }


        private void txtPassword_Enter(object sender, EventArgs e)
        {
            try
            {
                if (txtUserName.Text.Trim() != "")
                {
                    txtPassword.Text = "";
                    chkRemember.Checked = MobjClsBLLLogin.GetRememberStatus(txtUserName.Text.Trim());
                    if (chkRemember.Checked)
                    {
                        string sUsername = "";
                        objRegistry.ReadFromRegistry("SOFTWARE\\ES3Tech\\payLogin", txtUserName.Text.Trim(), out sUsername);

                        if (sUsername != "")
                        {
                            string strPassWord = MobjClsBLLLogin.GetPassword(sUsername);
                            txtPassword.Text = clsBLLCommonUtility.Decrypt(strPassWord, ClsCommonSettings.strEncryptionKey);
                            chkRemember.Checked = true;
                        }
                        else
                        {
                            txtPassword.Text = "";
                            chkRemember.Checked = false;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (blnIsFromFormClose || ClsMainSettings.blnLoginStatus)
                e.Cancel = false;
            else
            {
                e.Cancel = true;
                blnIsFromFormClose = true;
            }
        }

       
    }
}
