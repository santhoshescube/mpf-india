using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Rendering;
using System.Text;
using DevComponents.AdvTree;
using System.Globalization;

namespace MyPayfriend
{
    /// <summary>
    /// Summary d escription for frmMain.
    /// </summary>
    public class FrmMain : DevComponents.DotNetBar.Office2007RibbonForm
    {
        

        #region Controls Declaration

        private DevComponents.DotNetBar.TabStrip tabStrip1;
        private System.ComponentModel.IContainer components;
        private BalloonSearch m_Search = null;
        private DevComponents.DotNetBar.RibbonControl rbnMenuBar;
        private RibbonTabItem Attendance;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private RibbonTabItem Company;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.DotNetBar.RibbonTabItemGroup ribbonTabItemGroup1;
        private DevComponents.DotNetBar.ButtonItem buttonChangeStyle;
        private DevComponents.DotNetBar.SuperTooltip superTooltip1;
        private DevComponents.DotNetBar.ButtonItem buttonStyleOffice2007Blue;
        private DevComponents.DotNetBar.ButtonItem buttonStyleOffice2007Black;
        private DevComponents.DotNetBar.QatCustomizeItem qatCustomizeItem1;
        private DevComponents.DotNetBar.ButtonItem buttonStyleOffice2007Silver;
        private DevComponents.DotNetBar.ColorPickerDropDown buttonStyleCustom;
        private Command AppCommandNew;
        private Command AppCommandTheme;
        private Command AppCommandExit;
        private DevComponents.DotNetBar.ButtonItem buttonItem60;
        private ButtonItem bMPFmenu;
        private RibbonPanel ribbonPanel9;
        private RibbonBar HomeBar;
        private ButtonItem btnEasyView;
        private RibbonTabItem General;

        private Command AppCmdSales;
        private Command AppCmdRoleSettings;
        private RibbonPanel ribbonPanel10;
        private RibbonTabItem Payroll;
        private ButtonItem buttonItem47;
        private ButtonItem buttonItem48;
        private ButtonItem buttonItem49;
        #endregion

        #region Variable Declaration


        private Command AppCmdPurchase;
        //Boolean MbTaxable;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private Command AppCommandReports;
        private Command AppCmdInventory;
        private ButtonItem btnHome;
        private Office2007StartButton office2007StartButton1;
        private ItemContainer itemContainer1;
        private ItemContainer itemContainer2;
        private ItemContainer itemContainer3;
        private ButtonItem mbtnSalaryStructure;
        private ItemContainer itemContainer5;
        private ButtonItem buttonItem12;
        private Office2007StartButton buttonFile;
        private ItemContainer menuFileContainer;
        private ItemContainer menuFileTwoColumnContainer;
        private ItemContainer menuFileItems;
        private ButtonItem btnAccountsMain;
        private ButtonItem buttonItem3;
        private ButtonItem btnProduction;
        private ButtonItem btnDocuments;
        private ButtonItem btnPayroll;
        private ButtonItem btnLogOut;
        private ButtonItem buttonItem25;
        private ItemContainer menuFileMRU;
        private LabelItem labelItem8;
        private ItemContainer menuFileBottomContainer;
        private ButtonItem buttonOptions;
        private ButtonItem buttonExit;
        private RibbonBar ribbonBar1;
        private ButtonItem btnNewCompany;
        private ButtonItem btnCompany;
        private RibbonBar ribbonBar2;
        private ButtonItem btnShift;
        private ButtonItem btnWorkPolicy;
        private ButtonItem btnLeavePolicy;
        private ButtonItem btnCalendar;
        private ButtonItem btnEmp;
        private ButtonItem btnSalary;
        private ButtonItem btnCurrency;
        private ButtonItem btnBank;
        private ButtonItem buttonItem2;
        private RibbonPanel ribbonPanel4;
        private RibbonTabItem ribbonTabItem2;
        internal ImageList ImgListNavigator;
        private RibbonPanel ribbonPanel5;
        private RibbonTabItem ribbonTabItem3;
        private RibbonBar ribbonBar6;
        private ButtonItem btnConfig;
        private ButtonItem btnMailSettings;
        private RibbonBar ribbonBar7;
        private ButtonItem btnUser;
        private ButtonItem btnUserRoles;
        private ButtonItem btnChangePassword;
        private ButtonItem btnAttendanceReport;
        private RibbonBar ribbonBarEmployee;
        private RibbonBar ribbonBar5;
        private ButtonItem btnBackup;
        private DockContainerItem dockContainerItem1;
        private DockContainerItem dockContainerItem2;
        private DockContainerItem dockContainerItem3;
        private DockContainerItem dockContainerItem4;
        private DockContainerItem dockRss;
        private ButtonItem btnRptAttendance;
        private ButtonItem btnPaymentsReport;
        private ButtonItem btnPaySlip;
        private ButtonItem mbtnEmployeePayment;
        private ButtonItem btnWizard;
        private ButtonItem btnWorkLocation;
        private ButtonItem btnRptEmployeeDeduction;
        private DockContainerItem dockContainerItem5;

        private Timer tmRssmain;
        private BackgroundWorker BackRssWorker;
        private Timer tmCheckStatus;
        private RibbonPanel ribbonPanel2;
        private RibbonBar ribbonBar16;
        private ButtonItem About;
        private RibbonTabItem ribbonTabItem1;
        private Bar bar2;

        private DockContainerItem dockContainerItem6;
        private DockContainerItem TaskPane1;
        private DockContainerItem TaskPane2;
        private ButtonItem mbtnCompany;
        private ButtonItem mbtnEmployee;
        private ButtonItem btnLogout1;
        private RibbonBar ribbonBar3;
        private ButtonItem btnEmployee;
        private RibbonPanel ribbonPanel6;
        private RibbonTabItem ribbonTabItem4;
        private ButtonItem btnSalaryStucture;
        private RibbonPanel ribbonPanel7;
        private RibbonBar ribbonBar4;
        private RibbonTabItem ribbonTabItem5;
        private RibbonBar rbLocation;
        private RibbonBar ribbonBarPayroll;
        private ButtonItem btnProcess;
        private ButtonItem btnRelease;
        private RibbonBar rbAttendance;
        private RibbonBar ribbonBar8;
        private ButtonItem Passport;
        private ButtonItem btnVisa;
        private ButtonItem DrivingLicense;
        private ButtonItem Qualification;
        private RibbonBar ribbonBar9;
        private ButtonItem EmiratesCard;
        private ButtonItem InsuranceCard;
        private ButtonItem LabourCard;
        private ButtonItem HealthCard;
        private RibbonBar ribbonBar10;
        private ButtonItem OtherDocuments;
        private ButtonItem StockRegister;
        private ButtonItem btnVacationPolicy;
        private ButtonItem btnSettlementPolicy;
        private ButtonItem buttonItem4;
        private ButtonItem btnLeaveStructure;
        private ButtonItem btnEmployeeLoan;
        private ButtonItem btnAttendance;
        private ButtonItem btnLeaveEntry;
        private ButtonItem btnLeaveExtension;
        private ButtonItem btnDocProcessTime;
        private ButtonItem btnLeaveOpening;
        private ButtonItem btnAssets;
        public Timer tmrAlert;
        private ButtonItem BtnShiftSchedule;
        private ButtonItem btnUnearnedPolicy;
        private RibbonBar rbLeave;
        private ButtonItem btnVacationProcess;
        private ButtonItem btnSettlementProcess;
        private RibbonBar ribbonBar11;
        private ButtonItem btnDeposit;
        private ButtonItem btnLoanRepayment;
        private ButtonItem btnSalaryAdvance;
        private ButtonItem btnExpense;
        private RibbonBar ribbonBar12;
        private ButtonItem btnAssetHandover;
        private ButtonItem btnEmployeeTransfer;
        private RibbonBar ribbonBar13;
        private ButtonItem btnTradeLicense;
        private DockSite dockSite2;
        private Bar bar1;
        private PanelDockContainer panelDockContainer1;
        private DockContainerItem DciUpdates;
        private DockSite dockSite1;
        private DockSite dockSite3;
        private DockSite dockSite4;
        private DockSite dockSite5;
        private DockSite dockSite6;
        private DockSite dockSite7;
        private DockSite dockSite8;
        private DotNetBarManager dotNetBarManager1;
        private Bar bar4;
        private ItemContainer itemContainer13;
        private WebBrowser WebRss;
        private PanelDockContainer panelDockContainer2;
        private DockContainerItem DciAlerts;
        private LabelX lblData;
        private BackgroundWorker bgwAlerts;
        private MessageBoxIcon MmessageIcon;

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private string MsMessageCommon;
        clsNavigatorBLL objNavigatorBLL;
        private clsBLLMain objclsBLLMain;
        ClsNotification MObjClsNotification;
        private int iMyAlertCount;
        private DevComponents.DotNetBar.Controls.ReflectionImage reflectionImage1;
        private ButtonItem btnLeaseAgreement;
        clsAlerts objAlerts;
        private ButtonX btnViewAlerts;
        private RibbonBar ribbonBar14;
        private ButtonItem Document;
        private ButtonItem btnAlert;
        private RibbonBar ribbonBar15;
        private ButtonItem btnExpenseReport;
        private ButtonItem btnSalaryAdvanceReport;
        private ButtonItem btnLeaveSummaryReport;
        private ButtonItem btnAssetHand;
        private ButtonItem btnHlpContents;
        private ButtonItem btnInsurance;
        private ButtonItem btnRptHoliday;
        private ButtonItem Transfer;
        private ButtonItem btnSupport;
        private ButtonItem btnRptSalesInvoice;
        private LabelItem lblLoginCompany;
        private ItemContainer itemContainer4;
        private LabelItem lblCurrency;
        private LabelItem labelStatus;
        private ItemContainer itemContainer6;
        private LabelItem lblUser;
        private ItemContainer itemContainer7;
        private LabelItem lblEmployee;
        private ButtonItem mbtnChangeCompany;
        private ButtonItem btnUpdateSalaryStructure;
        private ButtonItem btnRptAttendanceLive;
        private ButtonItem btnPaymentsProcessedReport;
        private ButtonItem btnAttendanceWorkSheet;
        private ButtonItem btnRptWorkSheet;
        private ButtonItem btnDutyRoaster;
        private RibbonPanel ribbonPanel8;
        private RibbonTabItem ribbonTabItem6;
        private RibbonBar ribbonBar17;
        private ButtonItem btnEmployeeReport;
        private ButtonItem btnRptSalaryStructureReport;
        private ButtonItem btnRptVacation;
        private ButtonItem btnSettlementReport;
        private ButtonItem btnRptLabourCost;
        private ButtonItem btnRptCTCReport;
        private ButtonItem btnOffDay;
        private ButtonItem btnCompanySettings;
        private ButtonItem btnGLCodeMapping;
        private ButtonItem btnAccrualReport;
        private ButtonItem btnRptEmployeeHistory;
        private RibbonBar rbIncentive;
        private ButtonItem btnProfitCenterTarget;
        private ButtonItem btnCommissionStructure;
        private ButtonItem btnSalesInvoice;
        private ButtonItem btnSalaryTemplate;
        private ButtonItem btnSettlementBatch;
        private ButtonItem btnVacationBatch;
        private BackgroundWorker bgwMSI;
        private RibbonBar rbAccupdate;
        private ButtonItem btnUpdateAccruals;
        private ButtonItem btnIntegration;
        private ButtonItem btnSalaryAmendment;
        private ButtonItem btnRptEmployeeLedger;
        bool IsCheckAlert;
        #endregion

        public FrmMain()
        {
            // Required for Windows Form Designer support
            InitializeComponent();

            iMyAlertCount = 0;   
            IsCheckAlert = false;
            objAlerts =  new clsAlerts();
            objNavigatorBLL = new clsNavigatorBLL();

            this.objclsBLLMain = new clsBLLMain();

            eOffice2007ColorScheme colorScheme = (eOffice2007ColorScheme)Enum.Parse(typeof(eOffice2007ColorScheme), "Silver");
            rbnMenuBar.Office2007ColorTable = colorScheme;

            ClsMainSettings.blnProductStatus = true;
           
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevComponents.DotNetBar.SuperTooltipInfo superTooltipInfo5 = new DevComponents.DotNetBar.SuperTooltipInfo();
            DevComponents.DotNetBar.SuperTooltipInfo superTooltipInfo1 = new DevComponents.DotNetBar.SuperTooltipInfo();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            DevComponents.DotNetBar.SuperTooltipInfo superTooltipInfo2 = new DevComponents.DotNetBar.SuperTooltipInfo();
            DevComponents.DotNetBar.SuperTooltipInfo superTooltipInfo3 = new DevComponents.DotNetBar.SuperTooltipInfo();
            this.tabStrip1 = new DevComponents.DotNetBar.TabStrip();
            this.rbnMenuBar = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel9 = new DevComponents.DotNetBar.RibbonPanel();
            this.HomeBar = new DevComponents.DotNetBar.RibbonBar();
            this.btnEasyView = new DevComponents.DotNetBar.ButtonItem();
            this.btnCompany = new DevComponents.DotNetBar.ButtonItem();
            this.btnEmp = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalary = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar2 = new DevComponents.DotNetBar.RibbonBar();
            this.btnShift = new DevComponents.DotNetBar.ButtonItem();
            this.btnWorkPolicy = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeavePolicy = new DevComponents.DotNetBar.ButtonItem();
            this.btnCalendar = new DevComponents.DotNetBar.ButtonItem();
            this.btnVacationPolicy = new DevComponents.DotNetBar.ButtonItem();
            this.btnSettlementPolicy = new DevComponents.DotNetBar.ButtonItem();
            this.btnUnearnedPolicy = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.btnNewCompany = new DevComponents.DotNetBar.ButtonItem();
            this.btnCurrency = new DevComponents.DotNetBar.ButtonItem();
            this.btnBank = new DevComponents.DotNetBar.ButtonItem();
            this.btnWorkLocation = new DevComponents.DotNetBar.ButtonItem();
            this.btnAssets = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.rbLocation = new DevComponents.DotNetBar.RibbonBar();
            this.btnLeaveStructure = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeaveOpening = new DevComponents.DotNetBar.ButtonItem();
            this.btnDutyRoaster = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalaryTemplate = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar3 = new DevComponents.DotNetBar.RibbonBar();
            this.btnEmployee = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalaryStucture = new DevComponents.DotNetBar.ButtonItem();
            this.btnUpdateSalaryStructure = new DevComponents.DotNetBar.ButtonItem();
            this.btnEmployeeLoan = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel10 = new DevComponents.DotNetBar.RibbonPanel();
            this.rbAccupdate = new DevComponents.DotNetBar.RibbonBar();
            this.btnUpdateAccruals = new DevComponents.DotNetBar.ButtonItem();
            this.rbLeave = new DevComponents.DotNetBar.RibbonBar();
            this.btnVacationProcess = new DevComponents.DotNetBar.ButtonItem();
            this.btnSettlementProcess = new DevComponents.DotNetBar.ButtonItem();
            this.btnSettlementBatch = new DevComponents.DotNetBar.ButtonItem();
            this.btnVacationBatch = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBarPayroll = new DevComponents.DotNetBar.RibbonBar();
            this.btnProcess = new DevComponents.DotNetBar.ButtonItem();
            this.btnRelease = new DevComponents.DotNetBar.ButtonItem();
            this.rbAttendance = new DevComponents.DotNetBar.RibbonBar();
            this.btnAttendance = new DevComponents.DotNetBar.ButtonItem();
            this.btnAttendanceWorkSheet = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalaryAmendment = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel4 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar17 = new DevComponents.DotNetBar.RibbonBar();
            this.btnEmployeeReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptSalaryStructureReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptVacation = new DevComponents.DotNetBar.ButtonItem();
            this.btnSettlementReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptLabourCost = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptCTCReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptEmployeeHistory = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptEmployeeLedger = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar15 = new DevComponents.DotNetBar.RibbonBar();
            this.btnAssetHand = new DevComponents.DotNetBar.ButtonItem();
            this.btnExpenseReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalaryAdvanceReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeaveSummaryReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptHoliday = new DevComponents.DotNetBar.ButtonItem();
            this.Transfer = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptSalesInvoice = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar14 = new DevComponents.DotNetBar.RibbonBar();
            this.Document = new DevComponents.DotNetBar.ButtonItem();
            this.btnAlert = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel8 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBarEmployee = new DevComponents.DotNetBar.RibbonBar();
            this.btnRptAttendance = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptAttendanceLive = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptWorkSheet = new DevComponents.DotNetBar.ButtonItem();
            this.btnOffDay = new DevComponents.DotNetBar.ButtonItem();
            this.btnPaymentsProcessedReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnPaymentsReport = new DevComponents.DotNetBar.ButtonItem();
            this.btnPaySlip = new DevComponents.DotNetBar.ButtonItem();
            this.btnRptEmployeeDeduction = new DevComponents.DotNetBar.ButtonItem();
            this.btnAccrualReport = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel7 = new DevComponents.DotNetBar.RibbonPanel();
            this.rbIncentive = new DevComponents.DotNetBar.RibbonBar();
            this.btnProfitCenterTarget = new DevComponents.DotNetBar.ButtonItem();
            this.btnCommissionStructure = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalesInvoice = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar12 = new DevComponents.DotNetBar.RibbonBar();
            this.btnAssetHandover = new DevComponents.DotNetBar.ButtonItem();
            this.btnEmployeeTransfer = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar11 = new DevComponents.DotNetBar.RibbonBar();
            this.btnSalaryAdvance = new DevComponents.DotNetBar.ButtonItem();
            this.btnExpense = new DevComponents.DotNetBar.ButtonItem();
            this.btnLoanRepayment = new DevComponents.DotNetBar.ButtonItem();
            this.btnDeposit = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar4 = new DevComponents.DotNetBar.RibbonBar();
            this.btnLeaveEntry = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeaveExtension = new DevComponents.DotNetBar.ButtonItem();
            this.BtnShiftSchedule = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel5 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar5 = new DevComponents.DotNetBar.RibbonBar();
            this.btnBackup = new DevComponents.DotNetBar.ButtonItem();
            this.btnWizard = new DevComponents.DotNetBar.ButtonItem();
            this.btnCompanySettings = new DevComponents.DotNetBar.ButtonItem();
            this.btnGLCodeMapping = new DevComponents.DotNetBar.ButtonItem();
            this.btnIntegration = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar7 = new DevComponents.DotNetBar.RibbonBar();
            this.btnUser = new DevComponents.DotNetBar.ButtonItem();
            this.btnUserRoles = new DevComponents.DotNetBar.ButtonItem();
            this.btnChangePassword = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar6 = new DevComponents.DotNetBar.RibbonBar();
            this.btnConfig = new DevComponents.DotNetBar.ButtonItem();
            this.btnMailSettings = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel6 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar10 = new DevComponents.DotNetBar.RibbonBar();
            this.OtherDocuments = new DevComponents.DotNetBar.ButtonItem();
            this.StockRegister = new DevComponents.DotNetBar.ButtonItem();
            this.btnDocProcessTime = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar9 = new DevComponents.DotNetBar.RibbonBar();
            this.EmiratesCard = new DevComponents.DotNetBar.ButtonItem();
            this.InsuranceCard = new DevComponents.DotNetBar.ButtonItem();
            this.LabourCard = new DevComponents.DotNetBar.ButtonItem();
            this.HealthCard = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar8 = new DevComponents.DotNetBar.RibbonBar();
            this.Passport = new DevComponents.DotNetBar.ButtonItem();
            this.btnVisa = new DevComponents.DotNetBar.ButtonItem();
            this.DrivingLicense = new DevComponents.DotNetBar.ButtonItem();
            this.Qualification = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar13 = new DevComponents.DotNetBar.RibbonBar();
            this.btnTradeLicense = new DevComponents.DotNetBar.ButtonItem();
            this.btnLeaseAgreement = new DevComponents.DotNetBar.ButtonItem();
            this.btnInsurance = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar16 = new DevComponents.DotNetBar.RibbonBar();
            this.About = new DevComponents.DotNetBar.ButtonItem();
            this.btnHlpContents = new DevComponents.DotNetBar.ButtonItem();
            this.btnSupport = new DevComponents.DotNetBar.ButtonItem();
            this.General = new DevComponents.DotNetBar.RibbonTabItem();
            this.Company = new DevComponents.DotNetBar.RibbonTabItem();
            this.Attendance = new DevComponents.DotNetBar.RibbonTabItem();
            this.Payroll = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem4 = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem5 = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem2 = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem6 = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem3 = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem1 = new DevComponents.DotNetBar.RibbonTabItem();
            this.buttonChangeStyle = new DevComponents.DotNetBar.ButtonItem();
            this.buttonStyleOffice2007Blue = new DevComponents.DotNetBar.ButtonItem();
            this.AppCommandTheme = new DevComponents.DotNetBar.Command(this.components);
            this.buttonStyleOffice2007Black = new DevComponents.DotNetBar.ButtonItem();
            this.buttonStyleOffice2007Silver = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem60 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonStyleCustom = new DevComponents.DotNetBar.ColorPickerDropDown();
            this.office2007StartButton1 = new DevComponents.DotNetBar.Office2007StartButton();
            this.itemContainer1 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer2 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer3 = new DevComponents.DotNetBar.ItemContainer();
            this.mbtnChangeCompany = new DevComponents.DotNetBar.ButtonItem();
            this.mbtnCompany = new DevComponents.DotNetBar.ButtonItem();
            this.mbtnEmployee = new DevComponents.DotNetBar.ButtonItem();
            this.mbtnSalaryStructure = new DevComponents.DotNetBar.ButtonItem();
            this.mbtnEmployeePayment = new DevComponents.DotNetBar.ButtonItem();
            this.btnLogout1 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer5 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.AppCommandExit = new DevComponents.DotNetBar.Command(this.components);
            this.btnHome = new DevComponents.DotNetBar.ButtonItem();
            this.bMPFmenu = new DevComponents.DotNetBar.ButtonItem();
            this.qatCustomizeItem1 = new DevComponents.DotNetBar.QatCustomizeItem();
            this.ribbonTabItemGroup1 = new DevComponents.DotNetBar.RibbonTabItemGroup();
            this.AppCommandNew = new DevComponents.DotNetBar.Command(this.components);
            this.buttonItem47 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem48 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem49 = new DevComponents.DotNetBar.ButtonItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.superTooltip1 = new DevComponents.DotNetBar.SuperTooltip();
            this.AppCmdPurchase = new DevComponents.DotNetBar.Command(this.components);
            this.AppCmdSales = new DevComponents.DotNetBar.Command(this.components);
            this.AppCmdRoleSettings = new DevComponents.DotNetBar.Command(this.components);
            this.AppCommandReports = new DevComponents.DotNetBar.Command(this.components);
            this.dockContainerItem5 = new DevComponents.DotNetBar.DockContainerItem();
            this.dockRss = new DevComponents.DotNetBar.DockContainerItem();
            this.AppCmdInventory = new DevComponents.DotNetBar.Command(this.components);
            this.buttonFile = new DevComponents.DotNetBar.Office2007StartButton();
            this.menuFileContainer = new DevComponents.DotNetBar.ItemContainer();
            this.menuFileTwoColumnContainer = new DevComponents.DotNetBar.ItemContainer();
            this.menuFileItems = new DevComponents.DotNetBar.ItemContainer();
            this.btnAccountsMain = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.btnProduction = new DevComponents.DotNetBar.ButtonItem();
            this.btnDocuments = new DevComponents.DotNetBar.ButtonItem();
            this.btnPayroll = new DevComponents.DotNetBar.ButtonItem();
            this.btnLogOut = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem25 = new DevComponents.DotNetBar.ButtonItem();
            this.menuFileMRU = new DevComponents.DotNetBar.ItemContainer();
            this.labelItem8 = new DevComponents.DotNetBar.LabelItem();
            this.menuFileBottomContainer = new DevComponents.DotNetBar.ItemContainer();
            this.buttonOptions = new DevComponents.DotNetBar.ButtonItem();
            this.buttonExit = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.ImgListNavigator = new System.Windows.Forms.ImageList(this.components);
            this.btnAttendanceReport = new DevComponents.DotNetBar.ButtonItem();
            this.dockContainerItem1 = new DevComponents.DotNetBar.DockContainerItem();
            this.dockContainerItem2 = new DevComponents.DotNetBar.DockContainerItem();
            this.dockContainerItem3 = new DevComponents.DotNetBar.DockContainerItem();
            this.dockContainerItem4 = new DevComponents.DotNetBar.DockContainerItem();
            this.tmRssmain = new System.Windows.Forms.Timer(this.components);
            this.BackRssWorker = new System.ComponentModel.BackgroundWorker();
            this.tmCheckStatus = new System.Windows.Forms.Timer(this.components);
            this.bar2 = new DevComponents.DotNetBar.Bar();
            this.dockContainerItem6 = new DevComponents.DotNetBar.DockContainerItem();
            this.TaskPane1 = new DevComponents.DotNetBar.DockContainerItem();
            this.TaskPane2 = new DevComponents.DotNetBar.DockContainerItem();
            this.tmrAlert = new System.Windows.Forms.Timer(this.components);
            this.dotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.dockSite4 = new DevComponents.DotNetBar.DockSite();
            this.dockSite1 = new DevComponents.DotNetBar.DockSite();
            this.dockSite2 = new DevComponents.DotNetBar.DockSite();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.panelDockContainer2 = new DevComponents.DotNetBar.PanelDockContainer();
            this.btnViewAlerts = new DevComponents.DotNetBar.ButtonX();
            this.reflectionImage1 = new DevComponents.DotNetBar.Controls.ReflectionImage();
            this.lblData = new DevComponents.DotNetBar.LabelX();
            this.panelDockContainer1 = new DevComponents.DotNetBar.PanelDockContainer();
            this.WebRss = new System.Windows.Forms.WebBrowser();
            this.DciAlerts = new DevComponents.DotNetBar.DockContainerItem();
            this.DciUpdates = new DevComponents.DotNetBar.DockContainerItem();
            this.dockSite8 = new DevComponents.DotNetBar.DockSite();
            this.bar4 = new DevComponents.DotNetBar.Bar();
            this.labelStatus = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer13 = new DevComponents.DotNetBar.ItemContainer();
            this.lblLoginCompany = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer4 = new DevComponents.DotNetBar.ItemContainer();
            this.lblCurrency = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer6 = new DevComponents.DotNetBar.ItemContainer();
            this.lblUser = new DevComponents.DotNetBar.LabelItem();
            this.itemContainer7 = new DevComponents.DotNetBar.ItemContainer();
            this.lblEmployee = new DevComponents.DotNetBar.LabelItem();
            this.dockSite5 = new DevComponents.DotNetBar.DockSite();
            this.dockSite6 = new DevComponents.DotNetBar.DockSite();
            this.dockSite7 = new DevComponents.DotNetBar.DockSite();
            this.dockSite3 = new DevComponents.DotNetBar.DockSite();
            this.bgwAlerts = new System.ComponentModel.BackgroundWorker();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bgwMSI = new System.ComponentModel.BackgroundWorker();
            this.rbnMenuBar.SuspendLayout();
            this.ribbonPanel9.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.ribbonPanel10.SuspendLayout();
            this.ribbonPanel4.SuspendLayout();
            this.ribbonPanel8.SuspendLayout();
            this.ribbonPanel7.SuspendLayout();
            this.ribbonPanel5.SuspendLayout();
            this.ribbonPanel6.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar2)).BeginInit();
            this.dockSite2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.bar1.SuspendLayout();
            this.panelDockContainer2.SuspendLayout();
            this.panelDockContainer1.SuspendLayout();
            this.dockSite8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar4)).BeginInit();
            this.SuspendLayout();
            // 
            // tabStrip1
            // 
            this.tabStrip1.AutoSelectAttachedControl = true;
            this.tabStrip1.CanReorderTabs = true;
            this.tabStrip1.CloseButtonOnTabsVisible = true;
            this.tabStrip1.CloseButtonVisible = false;
            this.tabStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabStrip1.Location = new System.Drawing.Point(5, 150);
            this.tabStrip1.MdiForm = this;
            this.tabStrip1.MdiTabbedDocuments = true;
            this.tabStrip1.Name = "tabStrip1";
            this.tabStrip1.SelectedTab = null;
            this.tabStrip1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabStrip1.Size = new System.Drawing.Size(1245, 33);
            this.tabStrip1.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tabStrip1.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Top;
            this.tabStrip1.TabIndex = 6;
            this.tabStrip1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabStrip1.Text = "tabStrip1";
            // 
            // rbnMenuBar
            // 
            this.rbnMenuBar.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.rbnMenuBar.BackgroundStyle.Class = "";
            this.rbnMenuBar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbnMenuBar.CaptionVisible = true;
            this.rbnMenuBar.Controls.Add(this.ribbonPanel9);
            this.rbnMenuBar.Controls.Add(this.ribbonPanel3);
            this.rbnMenuBar.Controls.Add(this.ribbonPanel1);
            this.rbnMenuBar.Controls.Add(this.ribbonPanel10);
            this.rbnMenuBar.Controls.Add(this.ribbonPanel4);
            this.rbnMenuBar.Controls.Add(this.ribbonPanel8);
            this.rbnMenuBar.Controls.Add(this.ribbonPanel7);
            this.rbnMenuBar.Controls.Add(this.ribbonPanel5);
            this.rbnMenuBar.Controls.Add(this.ribbonPanel6);
            this.rbnMenuBar.Controls.Add(this.ribbonPanel2);
            this.rbnMenuBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbnMenuBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnMenuBar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.General,
            this.Company,
            this.Attendance,
            this.Payroll,
            this.ribbonTabItem4,
            this.ribbonTabItem5,
            this.ribbonTabItem2,
            this.ribbonTabItem6,
            this.ribbonTabItem3,
            this.ribbonTabItem1,
            this.buttonChangeStyle});
            this.rbnMenuBar.KeyTipsFont = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnMenuBar.Location = new System.Drawing.Point(5, 1);
            this.rbnMenuBar.MdiSystemItemVisible = false;
            this.rbnMenuBar.Name = "rbnMenuBar";
            this.rbnMenuBar.Office2007ColorTable = DevComponents.DotNetBar.Rendering.eOffice2007ColorScheme.VistaGlass;
            this.rbnMenuBar.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.rbnMenuBar.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.office2007StartButton1,
            this.btnHome,
            this.bMPFmenu,
            this.qatCustomizeItem1});
            this.rbnMenuBar.Size = new System.Drawing.Size(1384, 149);
            this.rbnMenuBar.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbnMenuBar.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.rbnMenuBar.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.rbnMenuBar.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.rbnMenuBar.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.rbnMenuBar.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.rbnMenuBar.SystemText.QatDialogAddButton = "&Add >>";
            this.rbnMenuBar.SystemText.QatDialogCancelButton = "Cancel";
            this.rbnMenuBar.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.rbnMenuBar.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.rbnMenuBar.SystemText.QatDialogOkButton = "OK";
            this.rbnMenuBar.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.rbnMenuBar.SystemText.QatDialogRemoveButton = "&Remove";
            this.rbnMenuBar.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.rbnMenuBar.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.rbnMenuBar.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.rbnMenuBar.TabGroupHeight = 14;
            this.rbnMenuBar.TabGroups.AddRange(new DevComponents.DotNetBar.RibbonTabItemGroup[] {
            this.ribbonTabItemGroup1});
            this.rbnMenuBar.TabGroupsVisible = true;
            this.rbnMenuBar.TabIndex = 8;
            this.rbnMenuBar.Text = "MPF";
            this.rbnMenuBar.TitleTextMarkupLinkClick += new DevComponents.DotNetBar.MarkupLinkClickEventHandler(this.ribbonControl1_TitleTextMarkupLinkClick);
            // 
            // ribbonPanel9
            // 
            this.ribbonPanel9.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel9.Controls.Add(this.HomeBar);
            this.ribbonPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel9.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel9.Name = "ribbonPanel9";
            this.ribbonPanel9.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel9.Size = new System.Drawing.Size(1384, 90);
            // 
            // 
            // 
            this.ribbonPanel9.Style.Class = "";
            this.ribbonPanel9.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel9.StyleMouseDown.Class = "";
            this.ribbonPanel9.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel9.StyleMouseOver.Class = "";
            this.ribbonPanel9.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel9.TabIndex = 10;
            this.ribbonPanel9.Visible = true;
            // 
            // HomeBar
            // 
            this.HomeBar.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.HomeBar.BackgroundMouseOverStyle.Class = "";
            this.HomeBar.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.HomeBar.BackgroundStyle.Class = "";
            this.HomeBar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.HomeBar.ContainerControlProcessDialogKey = true;
            this.HomeBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.HomeBar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnEasyView,
            this.btnCompany,
            this.btnEmp,
            this.btnSalary,
            this.buttonItem4});
            this.HomeBar.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.HomeBar.Location = new System.Drawing.Point(3, 0);
            this.HomeBar.Name = "HomeBar";
            this.HomeBar.Size = new System.Drawing.Size(533, 87);
            this.HomeBar.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            superTooltipInfo5.FooterText = "test";
            superTooltipInfo5.HeaderText = "Easy View";
            this.superTooltip1.SetSuperTooltip(this.HomeBar, superTooltipInfo5);
            this.HomeBar.TabIndex = 0;
            this.HomeBar.Text = "General";
            // 
            // 
            // 
            this.HomeBar.TitleStyle.Class = "";
            this.HomeBar.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.HomeBar.TitleStyleMouseOver.Class = "";
            this.HomeBar.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.HomeBar.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnEasyView
            // 
            this.btnEasyView.FixedSize = new System.Drawing.Size(100, 10);
            this.btnEasyView.Image = global::MyPayfriend.Properties.Resources.EasyView;
            this.btnEasyView.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEasyView.Name = "btnEasyView";
            this.btnEasyView.SubItemsExpandWidth = 14;
            this.btnEasyView.Text = "&EasyView";
            this.btnEasyView.Tooltip = "Easy View";
            this.btnEasyView.Click += new System.EventHandler(this.btnEasyView_Click);
            // 
            // btnCompany
            // 
            this.btnCompany.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCompany.FixedSize = new System.Drawing.Size(100, 10);
            this.btnCompany.Image = global::MyPayfriend.Properties.Resources.New_company;
            this.btnCompany.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCompany.Name = "btnCompany";
            this.btnCompany.SubItemsExpandWidth = 14;
            this.btnCompany.Text = "&Company";
            this.btnCompany.Tooltip = "Company";
            this.btnCompany.Click += new System.EventHandler(this.btnCompany_Click);
            // 
            // btnEmp
            // 
            this.btnEmp.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnEmp.FixedSize = new System.Drawing.Size(100, 10);
            this.btnEmp.Image = global::MyPayfriend.Properties.Resources.Executive;
            this.btnEmp.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEmp.Name = "btnEmp";
            this.btnEmp.SubItemsExpandWidth = 14;
            this.btnEmp.Text = "&Employee";
            this.btnEmp.Tooltip = "Employee";
            this.btnEmp.Click += new System.EventHandler(this.btnEmp_Click);
            // 
            // btnSalary
            // 
            this.btnSalary.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalary.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSalary.Image = global::MyPayfriend.Properties.Resources.salarystructure;
            this.btnSalary.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalary.Name = "btnSalary";
            this.btnSalary.SubItemsExpandWidth = 14;
            this.btnSalary.Text = "&SalaryStructure";
            this.btnSalary.Tooltip = "SalaryStructure";
            this.btnSalary.Click += new System.EventHandler(this.btnSalary_Click);
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.FixedSize = new System.Drawing.Size(100, 10);
            this.buttonItem4.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem4.Image")));
            this.buttonItem4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.SubItemsExpandWidth = 14;
            this.buttonItem4.Text = "&Stock Register";
            this.buttonItem4.Tooltip = "Stock Register";
            this.buttonItem4.Click += new System.EventHandler(this.buttonItem4_Click);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel3.Controls.Add(this.ribbonBar2);
            this.ribbonPanel3.Controls.Add(this.ribbonBar1);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel3.Size = new System.Drawing.Size(1384, 90);
            // 
            // 
            // 
            this.ribbonPanel3.Style.Class = "";
            this.ribbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.Class = "";
            this.ribbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.Class = "";
            this.ribbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 3;
            this.ribbonPanel3.Visible = false;
            // 
            // ribbonBar2
            // 
            this.ribbonBar2.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundStyle.Class = "";
            this.ribbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.ContainerControlProcessDialogKey = true;
            this.ribbonBar2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnShift,
            this.btnWorkPolicy,
            this.btnLeavePolicy,
            this.btnCalendar,
            this.btnVacationPolicy,
            this.btnSettlementPolicy,
            this.btnUnearnedPolicy});
            this.ribbonBar2.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar2.Location = new System.Drawing.Point(419, 0);
            this.ribbonBar2.Name = "ribbonBar2";
            this.ribbonBar2.Size = new System.Drawing.Size(630, 87);
            this.ribbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar2.TabIndex = 5;
            this.ribbonBar2.Text = "Policies";
            // 
            // 
            // 
            this.ribbonBar2.TitleStyle.Class = "";
            this.ribbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.TitleStyleMouseOver.Class = "";
            this.ribbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnShift
            // 
            this.btnShift.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnShift.FixedSize = new System.Drawing.Size(70, 10);
            this.btnShift.Image = global::MyPayfriend.Properties.Resources.Shift;
            this.btnShift.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnShift.Name = "btnShift";
            this.btnShift.SubItemsExpandWidth = 14;
            this.btnShift.Text = "Shift";
            this.btnShift.Tooltip = "Shift Policy";
            this.btnShift.Click += new System.EventHandler(this.btnShift_Click);
            // 
            // btnWorkPolicy
            // 
            this.btnWorkPolicy.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnWorkPolicy.FixedSize = new System.Drawing.Size(70, 10);
            this.btnWorkPolicy.Image = global::MyPayfriend.Properties.Resources.Work_Policy;
            this.btnWorkPolicy.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnWorkPolicy.Name = "btnWorkPolicy";
            this.btnWorkPolicy.SubItemsExpandWidth = 14;
            this.btnWorkPolicy.Text = "Work";
            this.btnWorkPolicy.Tooltip = "Work Policy";
            this.btnWorkPolicy.Click += new System.EventHandler(this.btnWorkPolicy_Click);
            // 
            // btnLeavePolicy
            // 
            this.btnLeavePolicy.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLeavePolicy.FixedSize = new System.Drawing.Size(70, 10);
            this.btnLeavePolicy.Image = global::MyPayfriend.Properties.Resources.Leave;
            this.btnLeavePolicy.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeavePolicy.Name = "btnLeavePolicy";
            this.btnLeavePolicy.SubItemsExpandWidth = 14;
            this.btnLeavePolicy.Text = "Leave";
            this.btnLeavePolicy.Tooltip = "Leave Policy";
            this.btnLeavePolicy.Click += new System.EventHandler(this.btnLeavePolicy_Click);
            // 
            // btnCalendar
            // 
            this.btnCalendar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCalendar.FixedSize = new System.Drawing.Size(100, 10);
            this.btnCalendar.Image = global::MyPayfriend.Properties.Resources.holiday_Policy;
            this.btnCalendar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCalendar.Name = "btnCalendar";
            this.btnCalendar.SubItemsExpandWidth = 14;
            this.btnCalendar.Text = "Holiday Calendar";
            this.btnCalendar.Tooltip = "Holiday Calendar";
            this.btnCalendar.Click += new System.EventHandler(this.btnCalendar_Click);
            // 
            // btnVacationPolicy
            // 
            this.btnVacationPolicy.FixedSize = new System.Drawing.Size(100, 10);
            this.btnVacationPolicy.Image = ((System.Drawing.Image)(resources.GetObject("btnVacationPolicy.Image")));
            this.btnVacationPolicy.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVacationPolicy.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnVacationPolicy.Name = "btnVacationPolicy";
            this.btnVacationPolicy.SubItemsExpandWidth = 14;
            this.btnVacationPolicy.Text = "Vacation Policy";
            this.btnVacationPolicy.Tooltip = "Vacation Policy";
            this.btnVacationPolicy.Click += new System.EventHandler(this.btnVacationPolicy_Click);
            // 
            // btnSettlementPolicy
            // 
            this.btnSettlementPolicy.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSettlementPolicy.Image = ((System.Drawing.Image)(resources.GetObject("btnSettlementPolicy.Image")));
            this.btnSettlementPolicy.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSettlementPolicy.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnSettlementPolicy.Name = "btnSettlementPolicy";
            this.btnSettlementPolicy.SubItemsExpandWidth = 14;
            this.btnSettlementPolicy.Text = "Settlement Policy";
            this.btnSettlementPolicy.Tooltip = "Settlement Policy";
            this.btnSettlementPolicy.Click += new System.EventHandler(this.btnSettlementPolicy_Click);
            // 
            // btnUnearnedPolicy
            // 
            this.btnUnearnedPolicy.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUnearnedPolicy.FixedSize = new System.Drawing.Size(100, 10);
            this.btnUnearnedPolicy.Image = global::MyPayfriend.Properties.Resources.unerned_pilcy32x32;
            this.btnUnearnedPolicy.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnUnearnedPolicy.Name = "btnUnearnedPolicy";
            this.btnUnearnedPolicy.SubItemsExpandWidth = 14;
            this.btnUnearnedPolicy.Text = "Unearned Policy";
            this.btnUnearnedPolicy.Tooltip = "UnEarned Policy";
            this.btnUnearnedPolicy.Click += new System.EventHandler(this.btnUnearnedPolicy_Click);
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundStyle.Class = "";
            this.ribbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.ContainerControlProcessDialogKey = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnNewCompany,
            this.btnCurrency,
            this.btnBank,
            this.btnWorkLocation,
            this.btnAssets});
            this.ribbonBar1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar1.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(416, 87);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            superTooltipInfo1.BodyImage = ((System.Drawing.Image)(resources.GetObject("superTooltipInfo1.BodyImage")));
            superTooltipInfo1.Color = DevComponents.DotNetBar.eTooltipColor.Cyan;
            superTooltipInfo1.FooterText = "Add,Edit,Delete company information";
            superTooltipInfo1.HeaderText = "Company Information";
            this.superTooltip1.SetSuperTooltip(this.ribbonBar1, superTooltipInfo1);
            this.ribbonBar1.TabIndex = 2;
            this.ribbonBar1.Text = "&Company";
            // 
            // 
            // 
            this.ribbonBar1.TitleStyle.Class = "";
            this.ribbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.TitleStyleMouseOver.Class = "";
            this.ribbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnNewCompany
            // 
            this.btnNewCompany.FixedSize = new System.Drawing.Size(80, 10);
            this.btnNewCompany.GlobalName = "NewCompany";
            this.btnNewCompany.Image = global::MyPayfriend.Properties.Resources.New_company;
            this.btnNewCompany.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnNewCompany.Name = "btnNewCompany";
            this.btnNewCompany.SplitButton = true;
            this.btnNewCompany.Text = "&Company";
            this.btnNewCompany.Tooltip = "Company";
            this.btnNewCompany.Click += new System.EventHandler(this.btnNewCompany_Click);
            // 
            // btnCurrency
            // 
            this.btnCurrency.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCurrency.FixedSize = new System.Drawing.Size(80, 10);
            this.btnCurrency.Image = global::MyPayfriend.Properties.Resources.Currency___Exchange_Rate2;
            this.btnCurrency.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCurrency.Name = "btnCurrency";
            this.btnCurrency.SubItemsExpandWidth = 14;
            this.btnCurrency.Text = "&Currency";
            this.btnCurrency.Tooltip = "Currency";
            this.btnCurrency.Click += new System.EventHandler(this.btnCurrency_Click);
            // 
            // btnBank
            // 
            this.btnBank.FixedSize = new System.Drawing.Size(80, 10);
            this.btnBank.Image = global::MyPayfriend.Properties.Resources.Bank1;
            this.btnBank.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBank.Name = "btnBank";
            this.btnBank.SubItemsExpandWidth = 14;
            this.btnBank.Text = " &Bank";
            this.btnBank.Tooltip = "Bank";
            this.btnBank.Click += new System.EventHandler(this.btnBank_Click);
            // 
            // btnWorkLocation
            // 
            this.btnWorkLocation.FixedSize = new System.Drawing.Size(80, 10);
            this.btnWorkLocation.GlobalName = "btnWorkLocation";
            this.btnWorkLocation.Image = global::MyPayfriend.Properties.Resources.work_location;
            this.btnWorkLocation.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnWorkLocation.Name = "btnWorkLocation";
            this.btnWorkLocation.SplitButton = true;
            this.btnWorkLocation.Text = "&Work Location";
            this.btnWorkLocation.Tooltip = "Work Location";
            this.btnWorkLocation.Click += new System.EventHandler(this.btnWorkLocation_Click);
            // 
            // btnAssets
            // 
            this.btnAssets.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAssets.FixedSize = new System.Drawing.Size(80, 10);
            this.btnAssets.Image = global::MyPayfriend.Properties.Resources.assets32x32;
            this.btnAssets.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAssets.Name = "btnAssets";
            this.btnAssets.SubItemsExpandWidth = 14;
            this.btnAssets.Text = "Asset";
            this.btnAssets.Tooltip = "Asset";
            this.btnAssets.Click += new System.EventHandler(this.btnAssets_Click);
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel1.Controls.Add(this.rbLocation);
            this.ribbonPanel1.Controls.Add(this.ribbonBar3);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(1384, 90);
            // 
            // 
            // 
            this.ribbonPanel1.Style.Class = "";
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.Class = "";
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.Class = "";
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 1;
            this.ribbonPanel1.Visible = false;
            // 
            // rbLocation
            // 
            this.rbLocation.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbLocation.BackgroundMouseOverStyle.Class = "";
            this.rbLocation.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbLocation.BackgroundStyle.Class = "";
            this.rbLocation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbLocation.ContainerControlProcessDialogKey = true;
            this.rbLocation.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbLocation.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnLeaveStructure,
            this.btnLeaveOpening,
            this.btnDutyRoaster,
            this.btnSalaryTemplate});
            this.rbLocation.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbLocation.Location = new System.Drawing.Point(445, 0);
            this.rbLocation.Name = "rbLocation";
            this.rbLocation.Size = new System.Drawing.Size(416, 87);
            this.rbLocation.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbLocation.TabIndex = 11;
            this.rbLocation.Text = "Leave";
            // 
            // 
            // 
            this.rbLocation.TitleStyle.Class = "";
            this.rbLocation.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbLocation.TitleStyleMouseOver.Class = "";
            this.rbLocation.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbLocation.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnLeaveStructure
            // 
            this.btnLeaveStructure.FixedSize = new System.Drawing.Size(100, 10);
            this.btnLeaveStructure.Image = ((System.Drawing.Image)(resources.GetObject("btnLeaveStructure.Image")));
            this.btnLeaveStructure.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeaveStructure.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnLeaveStructure.Name = "btnLeaveStructure";
            this.btnLeaveStructure.SubItemsExpandWidth = 14;
            this.btnLeaveStructure.Text = "Leave Structure";
            this.btnLeaveStructure.Tooltip = "Leave Structure";
            this.btnLeaveStructure.Click += new System.EventHandler(this.btnLeaveStructure_Click);
            // 
            // btnLeaveOpening
            // 
            this.btnLeaveOpening.FixedSize = new System.Drawing.Size(100, 10);
            this.btnLeaveOpening.Image = global::MyPayfriend.Properties.Resources.Leaveopening;
            this.btnLeaveOpening.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeaveOpening.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnLeaveOpening.Name = "btnLeaveOpening";
            this.btnLeaveOpening.SubItemsExpandWidth = 14;
            this.btnLeaveOpening.Text = "Leave Opening";
            this.btnLeaveOpening.Tooltip = "Leave Opening";
            this.btnLeaveOpening.Click += new System.EventHandler(this.btnLeaveOpening_Click);
            // 
            // btnDutyRoaster
            // 
            this.btnDutyRoaster.FixedSize = new System.Drawing.Size(100, 10);
            this.btnDutyRoaster.Image = global::MyPayfriend.Properties.Resources.off;
            this.btnDutyRoaster.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDutyRoaster.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnDutyRoaster.Name = "btnDutyRoaster";
            this.btnDutyRoaster.SubItemsExpandWidth = 14;
            this.btnDutyRoaster.Text = "DutyRoaster";
            this.btnDutyRoaster.Tooltip = "DutyRoaster";
            this.btnDutyRoaster.Click += new System.EventHandler(this.btnOffDayMarking_Click);
            // 
            // btnSalaryTemplate
            // 
            this.btnSalaryTemplate.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSalaryTemplate.Image = global::MyPayfriend.Properties.Resources.SalaryTemplate;
            this.btnSalaryTemplate.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalaryTemplate.Name = "btnSalaryTemplate";
            this.btnSalaryTemplate.SubItemsExpandWidth = 14;
            this.btnSalaryTemplate.Text = "Salary Template";
            this.btnSalaryTemplate.Tooltip = "Salary Template";
            this.btnSalaryTemplate.Click += new System.EventHandler(this.btnSalaryTemplate_Click);
            // 
            // ribbonBar3
            // 
            this.ribbonBar3.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar3.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundStyle.Class = "";
            this.ribbonBar3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.ContainerControlProcessDialogKey = true;
            this.ribbonBar3.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar3.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnEmployee,
            this.btnSalaryStucture,
            this.btnUpdateSalaryStructure,
            this.btnEmployeeLoan});
            this.ribbonBar3.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar3.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar3.Name = "ribbonBar3";
            this.ribbonBar3.Size = new System.Drawing.Size(442, 87);
            this.ribbonBar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            superTooltipInfo2.BodyText = "Assigning the Super Tooltip to the Ribbon Bar control will display it when mouse " +
                "hovers over the Dialog Launcher button.";
            superTooltipInfo2.HeaderText = "SuperTooltip for Dialog Launcher Button";
            this.superTooltip1.SetSuperTooltip(this.ribbonBar3, superTooltipInfo2);
            this.ribbonBar3.TabIndex = 9;
            this.ribbonBar3.Text = "&Employee";
            // 
            // 
            // 
            this.ribbonBar3.TitleStyle.Class = "";
            this.ribbonBar3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.TitleStyleMouseOver.Class = "";
            this.ribbonBar3.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnEmployee
            // 
            this.btnEmployee.FixedSize = new System.Drawing.Size(100, 10);
            this.btnEmployee.GlobalName = "Executives";
            this.btnEmployee.Image = ((System.Drawing.Image)(resources.GetObject("btnEmployee.Image")));
            this.btnEmployee.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEmployee.Name = "btnEmployee";
            this.btnEmployee.Text = "&Employees";
            this.btnEmployee.Tooltip = "Employees";
            this.btnEmployee.Click += new System.EventHandler(this.btnEmployee_Click);
            // 
            // btnSalaryStucture
            // 
            this.btnSalaryStucture.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalaryStucture.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSalaryStucture.Image = global::MyPayfriend.Properties.Resources.salarystructure;
            this.btnSalaryStucture.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalaryStucture.Name = "btnSalaryStucture";
            this.btnSalaryStucture.SubItemsExpandWidth = 14;
            this.btnSalaryStucture.Text = "&SalaryStructure";
            this.btnSalaryStucture.Tooltip = "SalaryStructure";
            this.btnSalaryStucture.Click += new System.EventHandler(this.btnSalaryStructure_Click);
            // 
            // btnUpdateSalaryStructure
            // 
            this.btnUpdateSalaryStructure.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUpdateSalaryStructure.FixedSize = new System.Drawing.Size(120, 10);
            this.btnUpdateSalaryStructure.Image = global::MyPayfriend.Properties.Resources.Salary_Structure;
            this.btnUpdateSalaryStructure.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnUpdateSalaryStructure.Name = "btnUpdateSalaryStructure";
            this.btnUpdateSalaryStructure.SubItemsExpandWidth = 14;
            this.btnUpdateSalaryStructure.Text = "&Update Salary Structure";
            this.btnUpdateSalaryStructure.Tooltip = "Update Salary Structure";
            this.btnUpdateSalaryStructure.Click += new System.EventHandler(this.btnUpdateSalaryStructure_Click);
            // 
            // btnEmployeeLoan
            // 
            this.btnEmployeeLoan.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnEmployeeLoan.FixedSize = new System.Drawing.Size(100, 10);
            this.btnEmployeeLoan.Image = global::MyPayfriend.Properties.Resources.loans;
            this.btnEmployeeLoan.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEmployeeLoan.Name = "btnEmployeeLoan";
            this.btnEmployeeLoan.SubItemsExpandWidth = 14;
            this.btnEmployeeLoan.Text = "&Loan";
            this.btnEmployeeLoan.Tooltip = "Loan";
            this.btnEmployeeLoan.Click += new System.EventHandler(this.btnEmployeeLoan_Click);
            // 
            // ribbonPanel10
            // 
            this.ribbonPanel10.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel10.Controls.Add(this.rbAccupdate);
            this.ribbonPanel10.Controls.Add(this.rbLeave);
            this.ribbonPanel10.Controls.Add(this.ribbonBarPayroll);
            this.ribbonPanel10.Controls.Add(this.rbAttendance);
            this.ribbonPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel10.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel10.Name = "ribbonPanel10";
            this.ribbonPanel10.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel10.Size = new System.Drawing.Size(1384, 90);
            // 
            // 
            // 
            this.ribbonPanel10.Style.Class = "";
            this.ribbonPanel10.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel10.StyleMouseDown.Class = "";
            this.ribbonPanel10.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel10.StyleMouseOver.Class = "";
            this.ribbonPanel10.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel10.TabIndex = 11;
            this.ribbonPanel10.Visible = false;
            // 
            // rbAccupdate
            // 
            this.rbAccupdate.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbAccupdate.BackgroundMouseOverStyle.Class = "";
            this.rbAccupdate.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbAccupdate.BackgroundStyle.Class = "";
            this.rbAccupdate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbAccupdate.ContainerControlProcessDialogKey = true;
            this.rbAccupdate.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbAccupdate.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnUpdateAccruals});
            this.rbAccupdate.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbAccupdate.Location = new System.Drawing.Point(750, 0);
            this.rbAccupdate.Name = "rbAccupdate";
            this.rbAccupdate.Size = new System.Drawing.Size(107, 87);
            this.rbAccupdate.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbAccupdate.TabIndex = 17;
            this.rbAccupdate.Text = "Accruals";
            // 
            // 
            // 
            this.rbAccupdate.TitleStyle.Class = "";
            this.rbAccupdate.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbAccupdate.TitleStyleMouseOver.Class = "";
            this.rbAccupdate.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbAccupdate.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnUpdateAccruals
            // 
            this.btnUpdateAccruals.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUpdateAccruals.FixedSize = new System.Drawing.Size(95, 10);
            this.btnUpdateAccruals.Image = global::MyPayfriend.Properties.Resources.unerned_pilcy32x32;
            this.btnUpdateAccruals.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnUpdateAccruals.Name = "btnUpdateAccruals";
            this.btnUpdateAccruals.SubItemsExpandWidth = 14;
            this.btnUpdateAccruals.Text = "Update Accruals";
            this.btnUpdateAccruals.Tooltip = "Update Accruals";
            this.btnUpdateAccruals.Click += new System.EventHandler(this.btnUpdateAccruals_Click);
            // 
            // rbLeave
            // 
            this.rbLeave.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbLeave.BackgroundMouseOverStyle.Class = "";
            this.rbLeave.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbLeave.BackgroundStyle.Class = "";
            this.rbLeave.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbLeave.ContainerControlProcessDialogKey = true;
            this.rbLeave.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbLeave.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnVacationProcess,
            this.btnSettlementProcess,
            this.btnSettlementBatch,
            this.btnVacationBatch});
            this.rbLeave.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbLeave.Location = new System.Drawing.Point(542, 0);
            this.rbLeave.Name = "rbLeave";
            this.rbLeave.Size = new System.Drawing.Size(208, 87);
            this.rbLeave.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbLeave.TabIndex = 11;
            this.rbLeave.Text = "Process";
            // 
            // 
            // 
            this.rbLeave.TitleStyle.Class = "";
            this.rbLeave.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbLeave.TitleStyleMouseOver.Class = "";
            this.rbLeave.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbLeave.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnVacationProcess
            // 
            this.btnVacationProcess.FixedSize = new System.Drawing.Size(100, 10);
            this.btnVacationProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnVacationProcess.Image")));
            this.btnVacationProcess.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVacationProcess.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnVacationProcess.Name = "btnVacationProcess";
            this.btnVacationProcess.SubItemsExpandWidth = 14;
            this.btnVacationProcess.Text = "Vacation Process";
            this.btnVacationProcess.Tooltip = "Vacation Process";
            this.btnVacationProcess.Click += new System.EventHandler(this.btnVacationProcess_Click);
            // 
            // btnSettlementProcess
            // 
            this.btnSettlementProcess.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSettlementProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnSettlementProcess.Image")));
            this.btnSettlementProcess.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSettlementProcess.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnSettlementProcess.Name = "btnSettlementProcess";
            this.btnSettlementProcess.SubItemsExpandWidth = 14;
            this.btnSettlementProcess.Text = "Settlement Process";
            this.btnSettlementProcess.Tooltip = "Settlement Process";
            this.btnSettlementProcess.Click += new System.EventHandler(this.btnSettlementProcess_Click);
            // 
            // btnSettlementBatch
            // 
            this.btnSettlementBatch.BeginGroup = true;
            this.btnSettlementBatch.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSettlementBatch.Image = ((System.Drawing.Image)(resources.GetObject("btnSettlementBatch.Image")));
            this.btnSettlementBatch.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSettlementBatch.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnSettlementBatch.Name = "btnSettlementBatch";
            this.btnSettlementBatch.SubItemsExpandWidth = 14;
            this.btnSettlementBatch.Text = "Settlement Batchwise";
            this.btnSettlementBatch.Tooltip = "Settlement Batchwise";
            this.btnSettlementBatch.Visible = false;
            this.btnSettlementBatch.Click += new System.EventHandler(this.btnSettlementBatch_Click);
            // 
            // btnVacationBatch
            // 
            this.btnVacationBatch.FixedSize = new System.Drawing.Size(90, 10);
            this.btnVacationBatch.Image = ((System.Drawing.Image)(resources.GetObject("btnVacationBatch.Image")));
            this.btnVacationBatch.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVacationBatch.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnVacationBatch.Name = "btnVacationBatch";
            this.btnVacationBatch.SubItemsExpandWidth = 14;
            this.btnVacationBatch.Text = "Vacation Batchwise";
            this.btnVacationBatch.Tooltip = "Vacation BatchWise";
            this.btnVacationBatch.Visible = false;
            // 
            // ribbonBarPayroll
            // 
            this.ribbonBarPayroll.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBarPayroll.BackgroundMouseOverStyle.Class = "";
            this.ribbonBarPayroll.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarPayroll.BackgroundStyle.Class = "";
            this.ribbonBarPayroll.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarPayroll.ContainerControlProcessDialogKey = true;
            this.ribbonBarPayroll.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBarPayroll.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnProcess,
            this.btnRelease});
            this.ribbonBarPayroll.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBarPayroll.Location = new System.Drawing.Point(327, 0);
            this.ribbonBarPayroll.Name = "ribbonBarPayroll";
            this.ribbonBarPayroll.Size = new System.Drawing.Size(215, 87);
            this.ribbonBarPayroll.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBarPayroll.TabIndex = 6;
            this.ribbonBarPayroll.Text = "Payroll  Processing";
            // 
            // 
            // 
            this.ribbonBarPayroll.TitleStyle.Class = "";
            this.ribbonBarPayroll.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarPayroll.TitleStyleMouseOver.Class = "";
            this.ribbonBarPayroll.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarPayroll.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnProcess
            // 
            this.btnProcess.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnProcess.FixedSize = new System.Drawing.Size(100, 10);
            this.btnProcess.Image = global::MyPayfriend.Properties.Resources.Salary_processing1;
            this.btnProcess.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.SubItemsExpandWidth = 14;
            this.btnProcess.Text = "Processing";
            this.btnProcess.Tooltip = "Salary Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click_1);
            // 
            // btnRelease
            // 
            this.btnRelease.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRelease.FixedSize = new System.Drawing.Size(100, 10);
            this.btnRelease.Image = ((System.Drawing.Image)(resources.GetObject("btnRelease.Image")));
            this.btnRelease.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRelease.Name = "btnRelease";
            this.btnRelease.SubItemsExpandWidth = 14;
            this.btnRelease.Text = "Releasing";
            this.btnRelease.Tooltip = "Salary Release";
            this.btnRelease.Click += new System.EventHandler(this.btnRelease_Click_1);
            // 
            // rbAttendance
            // 
            this.rbAttendance.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbAttendance.BackgroundMouseOverStyle.Class = "";
            this.rbAttendance.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbAttendance.BackgroundStyle.Class = "";
            this.rbAttendance.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbAttendance.ContainerControlProcessDialogKey = true;
            this.rbAttendance.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbAttendance.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnAttendance,
            this.btnAttendanceWorkSheet,
            this.btnSalaryAmendment});
            this.rbAttendance.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbAttendance.Location = new System.Drawing.Point(3, 0);
            this.rbAttendance.Name = "rbAttendance";
            this.rbAttendance.Size = new System.Drawing.Size(324, 87);
            this.rbAttendance.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbAttendance.TabIndex = 5;
            this.rbAttendance.Text = "Attendance && Amendment";
            // 
            // 
            // 
            this.rbAttendance.TitleStyle.Class = "";
            this.rbAttendance.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbAttendance.TitleStyleMouseOver.Class = "";
            this.rbAttendance.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbAttendance.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnAttendance
            // 
            this.btnAttendance.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAttendance.FixedSize = new System.Drawing.Size(100, 10);
            this.btnAttendance.Image = global::MyPayfriend.Properties.Resources.attendance;
            this.btnAttendance.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAttendance.Name = "btnAttendance";
            this.btnAttendance.SubItemsExpandWidth = 14;
            this.btnAttendance.Text = "Attendance";
            this.btnAttendance.Tooltip = "Attendance";
            this.btnAttendance.Click += new System.EventHandler(this.btnAttendance_Click);
            // 
            // btnAttendanceWorkSheet
            // 
            this.btnAttendanceWorkSheet.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAttendanceWorkSheet.FixedSize = new System.Drawing.Size(100, 10);
            this.btnAttendanceWorkSheet.Image = global::MyPayfriend.Properties.Resources.worksheet1;
            this.btnAttendanceWorkSheet.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAttendanceWorkSheet.Name = "btnAttendanceWorkSheet";
            this.btnAttendanceWorkSheet.SubItemsExpandWidth = 14;
            this.btnAttendanceWorkSheet.Text = "Work Sheet";
            this.btnAttendanceWorkSheet.Tooltip = "Work Sheet";
            this.btnAttendanceWorkSheet.Click += new System.EventHandler(this.btnAttendanceWorkSheet_Click);
            // 
            // btnSalaryAmendment
            // 
            this.btnSalaryAmendment.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalaryAmendment.FixedSize = new System.Drawing.Size(100, 10);
            this.btnSalaryAmendment.Image = global::MyPayfriend.Properties.Resources.SalaryAmendment32;
            this.btnSalaryAmendment.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalaryAmendment.Name = "btnSalaryAmendment";
            this.btnSalaryAmendment.SubItemsExpandWidth = 14;
            this.btnSalaryAmendment.Text = "&Salary Amendment";
            this.btnSalaryAmendment.Tooltip = "Salary Amendment";
            this.btnSalaryAmendment.Click += new System.EventHandler(this.btnSalaryAmendment_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel4.Controls.Add(this.ribbonBar17);
            this.ribbonPanel4.Controls.Add(this.ribbonBar15);
            this.ribbonPanel4.Controls.Add(this.ribbonBar14);
            this.ribbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel4.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel4.Size = new System.Drawing.Size(1384, 90);
            // 
            // 
            // 
            this.ribbonPanel4.Style.Class = "";
            this.ribbonPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseDown.Class = "";
            this.ribbonPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseOver.Class = "";
            this.ribbonPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel4.TabIndex = 13;
            this.ribbonPanel4.Visible = false;
            // 
            // ribbonBar17
            // 
            this.ribbonBar17.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar17.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar17.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar17.BackgroundStyle.Class = "";
            this.ribbonBar17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar17.ContainerControlProcessDialogKey = true;
            this.ribbonBar17.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar17.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnEmployeeReport,
            this.btnRptSalaryStructureReport,
            this.btnRptVacation,
            this.btnSettlementReport,
            this.btnRptLabourCost,
            this.btnRptCTCReport,
            this.btnRptEmployeeHistory,
            this.btnRptEmployeeLedger});
            this.ribbonBar17.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar17.Location = new System.Drawing.Point(621, 0);
            this.ribbonBar17.Name = "ribbonBar17";
            this.ribbonBar17.Size = new System.Drawing.Size(561, 87);
            this.ribbonBar17.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar17.TabIndex = 6;
            this.ribbonBar17.Text = "MIS";
            // 
            // 
            // 
            this.ribbonBar17.TitleStyle.Class = "";
            this.ribbonBar17.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar17.TitleStyleMouseOver.Class = "";
            this.ribbonBar17.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar17.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnEmployeeReport
            // 
            this.btnEmployeeReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnEmployeeReport.FixedSize = new System.Drawing.Size(65, 10);
            this.btnEmployeeReport.Image = global::MyPayfriend.Properties.Resources.EmployeeProfileReport;
            this.btnEmployeeReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEmployeeReport.Name = "btnEmployeeReport";
            this.btnEmployeeReport.SubItemsExpandWidth = 14;
            this.btnEmployeeReport.Text = "Profile";
            this.btnEmployeeReport.Tooltip = "Employee Profile";
            this.btnEmployeeReport.Click += new System.EventHandler(this.btnEmployeeReport_Click);
            // 
            // btnRptSalaryStructureReport
            // 
            this.btnRptSalaryStructureReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptSalaryStructureReport.FixedSize = new System.Drawing.Size(65, 10);
            this.btnRptSalaryStructureReport.Image = ((System.Drawing.Image)(resources.GetObject("btnRptSalaryStructureReport.Image")));
            this.btnRptSalaryStructureReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptSalaryStructureReport.Name = "btnRptSalaryStructureReport";
            this.btnRptSalaryStructureReport.SubItemsExpandWidth = 14;
            this.btnRptSalaryStructureReport.Text = "Salary Structure";
            this.btnRptSalaryStructureReport.Tooltip = "Salary Structure";
            this.btnRptSalaryStructureReport.Click += new System.EventHandler(this.btnSalaryStructureReport_Click);
            // 
            // btnRptVacation
            // 
            this.btnRptVacation.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptVacation.FixedSize = new System.Drawing.Size(65, 10);
            this.btnRptVacation.Image = ((System.Drawing.Image)(resources.GetObject("btnRptVacation.Image")));
            this.btnRptVacation.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptVacation.Name = "btnRptVacation";
            this.btnRptVacation.SubItemsExpandWidth = 14;
            this.btnRptVacation.Text = "Vacation";
            this.btnRptVacation.Click += new System.EventHandler(this.btnRptVacation_Click);
            // 
            // btnSettlementReport
            // 
            this.btnSettlementReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSettlementReport.FixedSize = new System.Drawing.Size(65, 10);
            this.btnSettlementReport.Image = ((System.Drawing.Image)(resources.GetObject("btnSettlementReport.Image")));
            this.btnSettlementReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSettlementReport.Name = "btnSettlementReport";
            this.btnSettlementReport.SubItemsExpandWidth = 14;
            this.btnSettlementReport.Text = "Settlement";
            this.btnSettlementReport.Click += new System.EventHandler(this.btnSettlementReport_Click);
            // 
            // btnRptLabourCost
            // 
            this.btnRptLabourCost.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptLabourCost.FixedSize = new System.Drawing.Size(65, 10);
            this.btnRptLabourCost.Image = ((System.Drawing.Image)(resources.GetObject("btnRptLabourCost.Image")));
            this.btnRptLabourCost.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptLabourCost.Name = "btnRptLabourCost";
            this.btnRptLabourCost.SubItemsExpandWidth = 14;
            this.btnRptLabourCost.Text = "Labour Cost ";
            this.btnRptLabourCost.Click += new System.EventHandler(this.btnRptLabourCost_Click);
            // 
            // btnRptCTCReport
            // 
            this.btnRptCTCReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptCTCReport.FixedSize = new System.Drawing.Size(65, 10);
            this.btnRptCTCReport.Image = global::MyPayfriend.Properties.Resources.Employee_Reports1;
            this.btnRptCTCReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptCTCReport.Name = "btnRptCTCReport";
            this.btnRptCTCReport.SubItemsExpandWidth = 14;
            this.btnRptCTCReport.Text = "CTC";
            this.btnRptCTCReport.Tooltip = "CTC Report";
            this.btnRptCTCReport.Click += new System.EventHandler(this.btnRptCTCReport_Click);
            // 
            // btnRptEmployeeHistory
            // 
            this.btnRptEmployeeHistory.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptEmployeeHistory.FixedSize = new System.Drawing.Size(65, 10);
            this.btnRptEmployeeHistory.Image = global::MyPayfriend.Properties.Resources.EmployeeHistory;
            this.btnRptEmployeeHistory.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptEmployeeHistory.Name = "btnRptEmployeeHistory";
            this.btnRptEmployeeHistory.SubItemsExpandWidth = 14;
            this.btnRptEmployeeHistory.Text = "Employee History";
            this.btnRptEmployeeHistory.Tooltip = "Employee History";
            this.btnRptEmployeeHistory.Click += new System.EventHandler(this.btnRptEmployeeHistory_Click);
            // 
            // btnRptEmployeeLedger
            // 
            this.btnRptEmployeeLedger.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptEmployeeLedger.FixedSize = new System.Drawing.Size(65, 10);
            this.btnRptEmployeeLedger.Image = global::MyPayfriend.Properties.Resources.employee_deduction;
            this.btnRptEmployeeLedger.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptEmployeeLedger.Name = "btnRptEmployeeLedger";
            this.btnRptEmployeeLedger.SubItemsExpandWidth = 14;
            this.btnRptEmployeeLedger.Text = "Employee Ledger";
            this.btnRptEmployeeLedger.Tooltip = "Employee Ledger";
            this.btnRptEmployeeLedger.Click += new System.EventHandler(this.btnRptEmployeeLedger_Click);
            // 
            // ribbonBar15
            // 
            this.ribbonBar15.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar15.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar15.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar15.BackgroundStyle.Class = "";
            this.ribbonBar15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar15.ContainerControlProcessDialogKey = true;
            this.ribbonBar15.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar15.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnAssetHand,
            this.btnExpenseReport,
            this.btnSalaryAdvanceReport,
            this.btnLeaveSummaryReport,
            this.btnRptHoliday,
            this.Transfer,
            this.btnRptSalesInvoice});
            this.ribbonBar15.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar15.Location = new System.Drawing.Point(148, 0);
            this.ribbonBar15.Name = "ribbonBar15";
            this.ribbonBar15.Size = new System.Drawing.Size(473, 87);
            this.ribbonBar15.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar15.TabIndex = 5;
            this.ribbonBar15.Text = "Tasks";
            // 
            // 
            // 
            this.ribbonBar15.TitleStyle.Class = "";
            this.ribbonBar15.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar15.TitleStyleMouseOver.Class = "";
            this.ribbonBar15.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar15.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnAssetHand
            // 
            this.btnAssetHand.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAssetHand.FixedSize = new System.Drawing.Size(65, 10);
            this.btnAssetHand.Image = ((System.Drawing.Image)(resources.GetObject("btnAssetHand.Image")));
            this.btnAssetHand.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAssetHand.Name = "btnAssetHand";
            this.btnAssetHand.SubItemsExpandWidth = 14;
            this.btnAssetHand.Text = "Asset";
            this.btnAssetHand.Tooltip = "Asset";
            this.btnAssetHand.Click += new System.EventHandler(this.btnAssetHand_Click);
            // 
            // btnExpenseReport
            // 
            this.btnExpenseReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnExpenseReport.FixedSize = new System.Drawing.Size(65, 10);
            this.btnExpenseReport.Image = ((System.Drawing.Image)(resources.GetObject("btnExpenseReport.Image")));
            this.btnExpenseReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnExpenseReport.Name = "btnExpenseReport";
            this.btnExpenseReport.SubItemsExpandWidth = 14;
            this.btnExpenseReport.Text = "Expense";
            this.btnExpenseReport.Tooltip = "Expense";
            this.btnExpenseReport.Click += new System.EventHandler(this.btnExpenseReport_Click);
            // 
            // btnSalaryAdvanceReport
            // 
            this.btnSalaryAdvanceReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalaryAdvanceReport.FixedSize = new System.Drawing.Size(65, 10);
            this.btnSalaryAdvanceReport.Image = ((System.Drawing.Image)(resources.GetObject("btnSalaryAdvanceReport.Image")));
            this.btnSalaryAdvanceReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalaryAdvanceReport.Name = "btnSalaryAdvanceReport";
            this.btnSalaryAdvanceReport.SubItemsExpandWidth = 14;
            this.btnSalaryAdvanceReport.Text = "Loan && Advance";
            this.btnSalaryAdvanceReport.Tooltip = "Loan && Salary Advance";
            this.btnSalaryAdvanceReport.Click += new System.EventHandler(this.btnSalaryAdvanceReport_Click);
            // 
            // btnLeaveSummaryReport
            // 
            this.btnLeaveSummaryReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLeaveSummaryReport.FixedSize = new System.Drawing.Size(65, 10);
            this.btnLeaveSummaryReport.Image = ((System.Drawing.Image)(resources.GetObject("btnLeaveSummaryReport.Image")));
            this.btnLeaveSummaryReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeaveSummaryReport.Name = "btnLeaveSummaryReport";
            this.btnLeaveSummaryReport.SubItemsExpandWidth = 14;
            this.btnLeaveSummaryReport.Text = "Leave ";
            this.btnLeaveSummaryReport.Tooltip = "Leave";
            this.btnLeaveSummaryReport.Click += new System.EventHandler(this.btnLeaveSummaryReport_Click);
            // 
            // btnRptHoliday
            // 
            this.btnRptHoliday.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptHoliday.FixedSize = new System.Drawing.Size(65, 10);
            this.btnRptHoliday.Image = ((System.Drawing.Image)(resources.GetObject("btnRptHoliday.Image")));
            this.btnRptHoliday.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptHoliday.Name = "btnRptHoliday";
            this.btnRptHoliday.SubItemsExpandWidth = 14;
            this.btnRptHoliday.Text = "Holiday";
            this.btnRptHoliday.Tooltip = "Holiday";
            this.btnRptHoliday.Click += new System.EventHandler(this.btnRptHoliday_Click);
            // 
            // Transfer
            // 
            this.Transfer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Transfer.FixedSize = new System.Drawing.Size(65, 10);
            this.Transfer.Image = global::MyPayfriend.Properties.Resources.Transfers;
            this.Transfer.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Transfer.Name = "Transfer";
            this.Transfer.SubItemsExpandWidth = 14;
            this.Transfer.Text = "Transfer";
            this.Transfer.Tooltip = "Transfer";
            this.Transfer.Click += new System.EventHandler(this.Transfer_Click);
            // 
            // btnRptSalesInvoice
            // 
            this.btnRptSalesInvoice.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptSalesInvoice.FixedSize = new System.Drawing.Size(65, 10);
            this.btnRptSalesInvoice.Image = global::MyPayfriend.Properties.Resources.add_particulars32x32;
            this.btnRptSalesInvoice.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptSalesInvoice.Name = "btnRptSalesInvoice";
            this.btnRptSalesInvoice.SubItemsExpandWidth = 14;
            this.btnRptSalesInvoice.Text = "Incentive";
            this.btnRptSalesInvoice.Tooltip = "Incentive";
            this.btnRptSalesInvoice.Click += new System.EventHandler(this.btnRptSalesInvoice_Click);
            // 
            // ribbonBar14
            // 
            this.ribbonBar14.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar14.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar14.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar14.BackgroundStyle.Class = "";
            this.ribbonBar14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar14.ContainerControlProcessDialogKey = true;
            this.ribbonBar14.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar14.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.Document,
            this.btnAlert});
            this.ribbonBar14.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar14.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar14.Name = "ribbonBar14";
            this.ribbonBar14.Size = new System.Drawing.Size(145, 87);
            this.ribbonBar14.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar14.TabIndex = 4;
            this.ribbonBar14.Text = "Documents";
            // 
            // 
            // 
            this.ribbonBar14.TitleStyle.Class = "";
            this.ribbonBar14.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar14.TitleStyleMouseOver.Class = "";
            this.ribbonBar14.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar14.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // Document
            // 
            this.Document.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Document.FixedSize = new System.Drawing.Size(65, 10);
            this.Document.Image = global::MyPayfriend.Properties.Resources.document_register;
            this.Document.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Document.Name = "Document";
            this.Document.SubItemsExpandWidth = 14;
            this.Document.Text = "Document";
            this.Document.Tooltip = "Document";
            this.Document.Click += new System.EventHandler(this.Document_Click);
            // 
            // btnAlert
            // 
            this.btnAlert.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAlert.FixedSize = new System.Drawing.Size(65, 10);
            this.btnAlert.Image = global::MyPayfriend.Properties.Resources.Alearts;
            this.btnAlert.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAlert.Name = "btnAlert";
            this.btnAlert.SubItemsExpandWidth = 14;
            this.btnAlert.Text = "Alerts";
            this.btnAlert.Tooltip = "Alerts";
            this.btnAlert.Click += new System.EventHandler(this.btnAlert_Click);
            // 
            // ribbonPanel8
            // 
            this.ribbonPanel8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel8.Controls.Add(this.ribbonBarEmployee);
            this.ribbonPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel8.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel8.Name = "ribbonPanel8";
            this.ribbonPanel8.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel8.Size = new System.Drawing.Size(1384, 90);
            // 
            // 
            // 
            this.ribbonPanel8.Style.Class = "";
            this.ribbonPanel8.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel8.StyleMouseDown.Class = "";
            this.ribbonPanel8.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel8.StyleMouseOver.Class = "";
            this.ribbonPanel8.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel8.TabIndex = 18;
            this.ribbonPanel8.Visible = false;
            // 
            // ribbonBarEmployee
            // 
            this.ribbonBarEmployee.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBarEmployee.BackgroundMouseOverStyle.Class = "";
            this.ribbonBarEmployee.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarEmployee.BackgroundStyle.Class = "";
            this.ribbonBarEmployee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarEmployee.ContainerControlProcessDialogKey = true;
            this.ribbonBarEmployee.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBarEmployee.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnRptAttendance,
            this.btnRptAttendanceLive,
            this.btnRptWorkSheet,
            this.btnOffDay,
            this.btnPaymentsProcessedReport,
            this.btnPaymentsReport,
            this.btnPaySlip,
            this.btnRptEmployeeDeduction,
            this.btnAccrualReport});
            this.ribbonBarEmployee.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBarEmployee.Location = new System.Drawing.Point(3, 0);
            this.ribbonBarEmployee.Name = "ribbonBarEmployee";
            this.ribbonBarEmployee.Size = new System.Drawing.Size(720, 87);
            this.ribbonBarEmployee.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBarEmployee.TabIndex = 3;
            this.ribbonBarEmployee.Text = "Payroll Reports";
            // 
            // 
            // 
            this.ribbonBarEmployee.TitleStyle.Class = "";
            this.ribbonBarEmployee.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBarEmployee.TitleStyleMouseOver.Class = "";
            this.ribbonBarEmployee.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBarEmployee.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            this.ribbonBarEmployee.ItemClick += new System.EventHandler(this.ribbonBarEmployee_ItemClick);
            // 
            // btnRptAttendance
            // 
            this.btnRptAttendance.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptAttendance.FixedSize = new System.Drawing.Size(76, 10);
            this.btnRptAttendance.Image = global::MyPayfriend.Properties.Resources.Attendance_Report;
            this.btnRptAttendance.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptAttendance.Name = "btnRptAttendance";
            this.btnRptAttendance.SubItemsExpandWidth = 14;
            this.btnRptAttendance.Text = "Attendance";
            this.btnRptAttendance.Click += new System.EventHandler(this.btnRptAttendance_Click);
            // 
            // btnRptAttendanceLive
            // 
            this.btnRptAttendanceLive.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptAttendanceLive.FixedSize = new System.Drawing.Size(90, 10);
            this.btnRptAttendanceLive.Image = global::MyPayfriend.Properties.Resources.AttendanceReportlive;
            this.btnRptAttendanceLive.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptAttendanceLive.Name = "btnRptAttendanceLive";
            this.btnRptAttendanceLive.SubItemsExpandWidth = 14;
            this.btnRptAttendanceLive.Text = "Live Attendance";
            this.btnRptAttendanceLive.Tooltip = "Live Attendance";
            this.btnRptAttendanceLive.Click += new System.EventHandler(this.btnRptAttendanceLive_Click);
            // 
            // btnRptWorkSheet
            // 
            this.btnRptWorkSheet.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptWorkSheet.FixedSize = new System.Drawing.Size(76, 10);
            this.btnRptWorkSheet.Image = global::MyPayfriend.Properties.Resources.worksheet_report1;
            this.btnRptWorkSheet.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptWorkSheet.Name = "btnRptWorkSheet";
            this.btnRptWorkSheet.SubItemsExpandWidth = 14;
            this.btnRptWorkSheet.Text = "Work Sheet";
            this.btnRptWorkSheet.Tooltip = "Work Sheet";
            this.btnRptWorkSheet.Click += new System.EventHandler(this.btnRptWorkSheet_Click);
            // 
            // btnOffDay
            // 
            this.btnOffDay.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnOffDay.FixedSize = new System.Drawing.Size(76, 10);
            this.btnOffDay.Image = global::MyPayfriend.Properties.Resources.offreport;
            this.btnOffDay.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnOffDay.Name = "btnOffDay";
            this.btnOffDay.SubItemsExpandWidth = 14;
            this.btnOffDay.Text = "DutyRoaster";
            this.btnOffDay.Tooltip = "DutyRoaster";
            this.btnOffDay.Click += new System.EventHandler(this.btnOffDay_Click);
            // 
            // btnPaymentsProcessedReport
            // 
            this.btnPaymentsProcessedReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPaymentsProcessedReport.FixedSize = new System.Drawing.Size(76, 10);
            this.btnPaymentsProcessedReport.Image = global::MyPayfriend.Properties.Resources.Payments2;
            this.btnPaymentsProcessedReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPaymentsProcessedReport.Name = "btnPaymentsProcessedReport";
            this.btnPaymentsProcessedReport.SubItemsExpandWidth = 14;
            this.btnPaymentsProcessedReport.Text = "Processed";
            this.btnPaymentsProcessedReport.Click += new System.EventHandler(this.btnPaymentsProcessedReport_Click);
            // 
            // btnPaymentsReport
            // 
            this.btnPaymentsReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPaymentsReport.FixedSize = new System.Drawing.Size(76, 10);
            this.btnPaymentsReport.Image = global::MyPayfriend.Properties.Resources.Payment1;
            this.btnPaymentsReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPaymentsReport.Name = "btnPaymentsReport";
            this.btnPaymentsReport.SubItemsExpandWidth = 14;
            this.btnPaymentsReport.Text = "Payments";
            this.btnPaymentsReport.Click += new System.EventHandler(this.btnPaymentsReport_Click);
            // 
            // btnPaySlip
            // 
            this.btnPaySlip.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPaySlip.FixedSize = new System.Drawing.Size(76, 10);
            this.btnPaySlip.Image = global::MyPayfriend.Properties.Resources.PaySlipReport;
            this.btnPaySlip.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnPaySlip.Name = "btnPaySlip";
            this.btnPaySlip.SubItemsExpandWidth = 14;
            this.btnPaySlip.Text = "Pay Slip";
            this.btnPaySlip.Tooltip = "Pay Slip";
            this.btnPaySlip.Click += new System.EventHandler(this.btnPaySlip_Click);
            this.btnPaySlip.MouseHover += new System.EventHandler(this.btnPaySlip_MouseHover);
            // 
            // btnRptEmployeeDeduction
            // 
            this.btnRptEmployeeDeduction.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnRptEmployeeDeduction.FixedSize = new System.Drawing.Size(76, 10);
            this.btnRptEmployeeDeduction.Image = global::MyPayfriend.Properties.Resources.employee_deduction;
            this.btnRptEmployeeDeduction.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRptEmployeeDeduction.Name = "btnRptEmployeeDeduction";
            this.btnRptEmployeeDeduction.SubItemsExpandWidth = 14;
            this.btnRptEmployeeDeduction.Text = "Deduction";
            this.btnRptEmployeeDeduction.Tooltip = "Employee Deduction";
            this.btnRptEmployeeDeduction.Click += new System.EventHandler(this.btnRptEmployeeDeduction_Click);
            // 
            // btnAccrualReport
            // 
            this.btnAccrualReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAccrualReport.FixedSize = new System.Drawing.Size(76, 10);
            this.btnAccrualReport.Image = global::MyPayfriend.Properties.Resources.add_particulars32x32;
            this.btnAccrualReport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAccrualReport.Name = "btnAccrualReport";
            this.btnAccrualReport.SubItemsExpandWidth = 14;
            this.btnAccrualReport.Text = "Accrual";
            this.btnAccrualReport.Tooltip = "Employee Deduction";
            this.btnAccrualReport.Click += new System.EventHandler(this.btnAccrualReport_Click);
            // 
            // ribbonPanel7
            // 
            this.ribbonPanel7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel7.Controls.Add(this.rbIncentive);
            this.ribbonPanel7.Controls.Add(this.ribbonBar12);
            this.ribbonPanel7.Controls.Add(this.ribbonBar11);
            this.ribbonPanel7.Controls.Add(this.ribbonBar4);
            this.ribbonPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel7.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel7.Name = "ribbonPanel7";
            this.ribbonPanel7.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel7.Size = new System.Drawing.Size(1384, 90);
            // 
            // 
            // 
            this.ribbonPanel7.Style.Class = "";
            this.ribbonPanel7.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel7.StyleMouseDown.Class = "";
            this.ribbonPanel7.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel7.StyleMouseOver.Class = "";
            this.ribbonPanel7.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel7.TabIndex = 17;
            this.ribbonPanel7.Visible = false;
            // 
            // rbIncentive
            // 
            this.rbIncentive.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbIncentive.BackgroundMouseOverStyle.Class = "";
            this.rbIncentive.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbIncentive.BackgroundStyle.Class = "";
            this.rbIncentive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbIncentive.ContainerControlProcessDialogKey = true;
            this.rbIncentive.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbIncentive.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnProfitCenterTarget,
            this.btnCommissionStructure,
            this.btnSalesInvoice});
            this.rbIncentive.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.rbIncentive.Location = new System.Drawing.Point(898, 0);
            this.rbIncentive.Name = "rbIncentive";
            this.rbIncentive.Size = new System.Drawing.Size(325, 87);
            this.rbIncentive.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.rbIncentive.TabIndex = 14;
            this.rbIncentive.Text = "Incentive";
            // 
            // 
            // 
            this.rbIncentive.TitleStyle.Class = "";
            this.rbIncentive.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbIncentive.TitleStyleMouseOver.Class = "";
            this.rbIncentive.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbIncentive.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            this.rbIncentive.Visible = false;
            // 
            // btnProfitCenterTarget
            // 
            this.btnProfitCenterTarget.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnProfitCenterTarget.FixedSize = new System.Drawing.Size(100, 10);
            this.btnProfitCenterTarget.Image = global::MyPayfriend.Properties.Resources.unerned_pilcy32x32;
            this.btnProfitCenterTarget.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnProfitCenterTarget.Name = "btnProfitCenterTarget";
            this.btnProfitCenterTarget.SubItemsExpandWidth = 14;
            this.btnProfitCenterTarget.Text = "Profit Center Target";
            this.btnProfitCenterTarget.Tooltip = "Profit Center Target";
            this.btnProfitCenterTarget.Click += new System.EventHandler(this.btnProfitCenterTarget_Click);
            // 
            // btnCommissionStructure
            // 
            this.btnCommissionStructure.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCommissionStructure.FixedSize = new System.Drawing.Size(120, 10);
            this.btnCommissionStructure.Image = global::MyPayfriend.Properties.Resources.User_Heirarchy;
            this.btnCommissionStructure.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCommissionStructure.Name = "btnCommissionStructure";
            this.btnCommissionStructure.SubItemsExpandWidth = 14;
            this.btnCommissionStructure.Text = "Commission Structure";
            this.btnCommissionStructure.Tooltip = "Commission Structure";
            this.btnCommissionStructure.Click += new System.EventHandler(this.btnCommissionStructure_Click);
            // 
            // btnSalesInvoice
            // 
            this.btnSalesInvoice.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalesInvoice.FixedSize = new System.Drawing.Size(95, 10);
            this.btnSalesInvoice.Image = global::MyPayfriend.Properties.Resources.add_particulars32x32;
            this.btnSalesInvoice.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalesInvoice.Name = "btnSalesInvoice";
            this.btnSalesInvoice.SubItemsExpandWidth = 14;
            this.btnSalesInvoice.Text = "Sales Invoice";
            this.btnSalesInvoice.Tooltip = "Sales Invoice";
            this.btnSalesInvoice.Click += new System.EventHandler(this.btnSalesInvoice_Click);
            // 
            // ribbonBar12
            // 
            this.ribbonBar12.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar12.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar12.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar12.BackgroundStyle.Class = "";
            this.ribbonBar12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar12.ContainerControlProcessDialogKey = true;
            this.ribbonBar12.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar12.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnAssetHandover,
            this.btnEmployeeTransfer});
            this.ribbonBar12.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar12.Location = new System.Drawing.Point(696, 0);
            this.ribbonBar12.Name = "ribbonBar12";
            this.ribbonBar12.Size = new System.Drawing.Size(202, 87);
            this.ribbonBar12.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar12.TabIndex = 13;
            this.ribbonBar12.Text = "Transfer";
            // 
            // 
            // 
            this.ribbonBar12.TitleStyle.Class = "";
            this.ribbonBar12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar12.TitleStyleMouseOver.Class = "";
            this.ribbonBar12.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar12.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnAssetHandover
            // 
            this.btnAssetHandover.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAssetHandover.FixedSize = new System.Drawing.Size(95, 10);
            this.btnAssetHandover.Image = global::MyPayfriend.Properties.Resources.AddAssect1;
            this.btnAssetHandover.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAssetHandover.Name = "btnAssetHandover";
            this.btnAssetHandover.SubItemsExpandWidth = 14;
            this.btnAssetHandover.Text = "Asset HandOver";
            this.btnAssetHandover.Click += new System.EventHandler(this.btnAssetHandover_Click);
            // 
            // btnEmployeeTransfer
            // 
            this.btnEmployeeTransfer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnEmployeeTransfer.FixedSize = new System.Drawing.Size(95, 10);
            this.btnEmployeeTransfer.Image = global::MyPayfriend.Properties.Resources.Transfers;
            this.btnEmployeeTransfer.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnEmployeeTransfer.Name = "btnEmployeeTransfer";
            this.btnEmployeeTransfer.SubItemsExpandWidth = 14;
            this.btnEmployeeTransfer.Text = "Employee Transfer";
            this.btnEmployeeTransfer.Tooltip = "Employee Transfer";
            this.btnEmployeeTransfer.Click += new System.EventHandler(this.btnEmployeeTransfer_Click);
            // 
            // ribbonBar11
            // 
            this.ribbonBar11.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar11.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar11.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar11.BackgroundStyle.Class = "";
            this.ribbonBar11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar11.ContainerControlProcessDialogKey = true;
            this.ribbonBar11.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar11.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSalaryAdvance,
            this.btnExpense,
            this.btnLoanRepayment,
            this.btnDeposit});
            this.ribbonBar11.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar11.Location = new System.Drawing.Point(300, 0);
            this.ribbonBar11.Name = "ribbonBar11";
            this.ribbonBar11.Size = new System.Drawing.Size(396, 87);
            this.ribbonBar11.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar11.TabIndex = 12;
            this.ribbonBar11.Text = "Receipts && Payments";
            // 
            // 
            // 
            this.ribbonBar11.TitleStyle.Class = "";
            this.ribbonBar11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar11.TitleStyleMouseOver.Class = "";
            this.ribbonBar11.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar11.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnSalaryAdvance
            // 
            this.btnSalaryAdvance.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSalaryAdvance.FixedSize = new System.Drawing.Size(95, 10);
            this.btnSalaryAdvance.Image = ((System.Drawing.Image)(resources.GetObject("btnSalaryAdvance.Image")));
            this.btnSalaryAdvance.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSalaryAdvance.Name = "btnSalaryAdvance";
            this.btnSalaryAdvance.SubItemsExpandWidth = 14;
            this.btnSalaryAdvance.Text = "&Salary Advance";
            this.btnSalaryAdvance.Tooltip = "Salary Advance";
            this.btnSalaryAdvance.Click += new System.EventHandler(this.btnSalaryAdvance_Click);
            // 
            // btnExpense
            // 
            this.btnExpense.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnExpense.FixedSize = new System.Drawing.Size(95, 10);
            this.btnExpense.Image = ((System.Drawing.Image)(resources.GetObject("btnExpense.Image")));
            this.btnExpense.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnExpense.Name = "btnExpense";
            this.btnExpense.SubItemsExpandWidth = 14;
            this.btnExpense.Text = "Expense";
            this.btnExpense.Tooltip = "Expense";
            this.btnExpense.Click += new System.EventHandler(this.btnExpense_Click);
            // 
            // btnLoanRepayment
            // 
            this.btnLoanRepayment.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLoanRepayment.FixedSize = new System.Drawing.Size(95, 10);
            this.btnLoanRepayment.Image = ((System.Drawing.Image)(resources.GetObject("btnLoanRepayment.Image")));
            this.btnLoanRepayment.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLoanRepayment.Name = "btnLoanRepayment";
            this.btnLoanRepayment.SubItemsExpandWidth = 14;
            this.btnLoanRepayment.Text = "Loan Repayment";
            this.btnLoanRepayment.Click += new System.EventHandler(this.btnLoanRepayment_Click);
            // 
            // btnDeposit
            // 
            this.btnDeposit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDeposit.FixedSize = new System.Drawing.Size(95, 10);
            this.btnDeposit.Image = ((System.Drawing.Image)(resources.GetObject("btnDeposit.Image")));
            this.btnDeposit.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDeposit.Name = "btnDeposit";
            this.btnDeposit.SubItemsExpandWidth = 14;
            this.btnDeposit.Text = "Deposit";
            this.btnDeposit.Click += new System.EventHandler(this.btnDeposit_Click);
            // 
            // ribbonBar4
            // 
            this.ribbonBar4.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar4.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundStyle.Class = "";
            this.ribbonBar4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar4.ContainerControlProcessDialogKey = true;
            this.ribbonBar4.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar4.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnLeaveEntry,
            this.btnLeaveExtension,
            this.BtnShiftSchedule});
            this.ribbonBar4.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar4.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar4.Name = "ribbonBar4";
            this.ribbonBar4.Size = new System.Drawing.Size(297, 87);
            this.ribbonBar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar4.TabIndex = 0;
            this.ribbonBar4.Text = "Leave";
            // 
            // 
            // 
            this.ribbonBar4.TitleStyle.Class = "";
            this.ribbonBar4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.TitleStyleMouseOver.Class = "";
            this.ribbonBar4.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar4.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnLeaveEntry
            // 
            this.btnLeaveEntry.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLeaveEntry.FixedSize = new System.Drawing.Size(95, 10);
            this.btnLeaveEntry.Image = global::MyPayfriend.Properties.Resources.leave_entry;
            this.btnLeaveEntry.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeaveEntry.Name = "btnLeaveEntry";
            this.btnLeaveEntry.SubItemsExpandWidth = 14;
            this.btnLeaveEntry.Text = "Leave Entry";
            this.btnLeaveEntry.Tooltip = "Leave Entry";
            this.btnLeaveEntry.Click += new System.EventHandler(this.btnLeaveEntry_Click);
            // 
            // btnLeaveExtension
            // 
            this.btnLeaveExtension.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLeaveExtension.FixedSize = new System.Drawing.Size(95, 10);
            this.btnLeaveExtension.Image = global::MyPayfriend.Properties.Resources.leave_extention1;
            this.btnLeaveExtension.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeaveExtension.Name = "btnLeaveExtension";
            this.btnLeaveExtension.SubItemsExpandWidth = 14;
            this.btnLeaveExtension.Text = "Leave Extension";
            this.btnLeaveExtension.Tooltip = "Leave Extension";
            this.btnLeaveExtension.Click += new System.EventHandler(this.btnLeaveExtension_Click);
            // 
            // BtnShiftSchedule
            // 
            this.BtnShiftSchedule.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.BtnShiftSchedule.FixedSize = new System.Drawing.Size(95, 10);
            this.BtnShiftSchedule.Image = ((System.Drawing.Image)(resources.GetObject("BtnShiftSchedule.Image")));
            this.BtnShiftSchedule.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BtnShiftSchedule.Name = "BtnShiftSchedule";
            this.BtnShiftSchedule.SubItemsExpandWidth = 14;
            this.BtnShiftSchedule.Text = "Shift Schedule";
            this.BtnShiftSchedule.Click += new System.EventHandler(this.BtnShiftSchedule_Click_1);
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel5.Controls.Add(this.ribbonBar5);
            this.ribbonPanel5.Controls.Add(this.ribbonBar7);
            this.ribbonPanel5.Controls.Add(this.ribbonBar6);
            this.ribbonPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel5.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel5.Name = "ribbonPanel5";
            this.ribbonPanel5.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel5.Size = new System.Drawing.Size(1384, 90);
            // 
            // 
            // 
            this.ribbonPanel5.Style.Class = "";
            this.ribbonPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseDown.Class = "";
            this.ribbonPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseOver.Class = "";
            this.ribbonPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel5.TabIndex = 14;
            this.ribbonPanel5.Visible = false;
            // 
            // ribbonBar5
            // 
            this.ribbonBar5.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundStyle.Class = "";
            this.ribbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.ContainerControlProcessDialogKey = true;
            this.ribbonBar5.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar5.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnBackup,
            this.btnWizard,
            this.btnCompanySettings,
            this.btnGLCodeMapping,
            this.btnIntegration});
            this.ribbonBar5.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar5.Location = new System.Drawing.Point(533, 0);
            this.ribbonBar5.Name = "ribbonBar5";
            this.ribbonBar5.Size = new System.Drawing.Size(423, 87);
            this.ribbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar5.TabIndex = 5;
            this.ribbonBar5.Text = "Settings";
            // 
            // 
            // 
            this.ribbonBar5.TitleStyle.Class = "";
            this.ribbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.TitleStyleMouseOver.Class = "";
            this.ribbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnBackup
            // 
            this.btnBackup.FixedSize = new System.Drawing.Size(100, 10);
            this.btnBackup.Image = global::MyPayfriend.Properties.Resources.Backup1;
            this.btnBackup.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Text = "Backup ";
            this.btnBackup.Tooltip = "Backup Database";
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // btnWizard
            // 
            this.btnWizard.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnWizard.FixedSize = new System.Drawing.Size(75, 10);
            this.btnWizard.Image = global::MyPayfriend.Properties.Resources.Attendance_Manual;
            this.btnWizard.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnWizard.Name = "btnWizard";
            this.btnWizard.SubItemsExpandWidth = 14;
            this.btnWizard.Text = "Attendance Wizard";
            this.btnWizard.Tooltip = "Attendance Wizard";
            this.btnWizard.Click += new System.EventHandler(this.btnWizard_Click);
            // 
            // btnCompanySettings
            // 
            this.btnCompanySettings.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnCompanySettings.FixedSize = new System.Drawing.Size(75, 10);
            this.btnCompanySettings.Image = global::MyPayfriend.Properties.Resources.Company_Settings;
            this.btnCompanySettings.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnCompanySettings.Name = "btnCompanySettings";
            this.btnCompanySettings.SubItemsExpandWidth = 14;
            this.btnCompanySettings.Text = "Company Settings";
            this.btnCompanySettings.Tooltip = "Company Settings";
            this.btnCompanySettings.Click += new System.EventHandler(this.btnCompanySettings_Click);
            // 
            // btnGLCodeMapping
            // 
            this.btnGLCodeMapping.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnGLCodeMapping.FixedSize = new System.Drawing.Size(75, 10);
            this.btnGLCodeMapping.Image = global::MyPayfriend.Properties.Resources.unerned_pilcy32x32;
            this.btnGLCodeMapping.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnGLCodeMapping.Name = "btnGLCodeMapping";
            this.btnGLCodeMapping.SubItemsExpandWidth = 14;
            this.btnGLCodeMapping.Text = "GL Code Mapping";
            this.btnGLCodeMapping.Tooltip = "GL Code Mapping";
            this.btnGLCodeMapping.Visible = false;
            this.btnGLCodeMapping.Click += new System.EventHandler(this.btnGLCodeMapping_Click);
            // 
            // btnIntegration
            // 
            this.btnIntegration.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnIntegration.FixedSize = new System.Drawing.Size(75, 10);
            this.btnIntegration.Image = global::MyPayfriend.Properties.Resources.settings;
            this.btnIntegration.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnIntegration.Name = "btnIntegration";
            this.btnIntegration.SubItemsExpandWidth = 14;
            this.btnIntegration.Text = "Integration";
            this.btnIntegration.Tooltip = "Company Settings";
            this.btnIntegration.Click += new System.EventHandler(this.btnIntegration_Click);
            // 
            // ribbonBar7
            // 
            this.ribbonBar7.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar7.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundStyle.Class = "";
            this.ribbonBar7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar7.ContainerControlProcessDialogKey = true;
            this.ribbonBar7.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar7.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnUser,
            this.btnUserRoles,
            this.btnChangePassword});
            this.ribbonBar7.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar7.Location = new System.Drawing.Point(218, 0);
            this.ribbonBar7.Name = "ribbonBar7";
            this.ribbonBar7.Size = new System.Drawing.Size(315, 87);
            this.ribbonBar7.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar7.TabIndex = 4;
            this.ribbonBar7.Text = "User";
            // 
            // 
            // 
            this.ribbonBar7.TitleStyle.Class = "";
            this.ribbonBar7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.TitleStyleMouseOver.Class = "";
            this.ribbonBar7.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar7.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnUser
            // 
            this.btnUser.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUser.FixedSize = new System.Drawing.Size(100, 10);
            this.btnUser.Image = global::MyPayfriend.Properties.Resources.User1;
            this.btnUser.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnUser.Name = "btnUser";
            this.btnUser.SubItemsExpandWidth = 14;
            this.btnUser.Text = "User";
            this.btnUser.Tooltip = "User";
            this.btnUser.Click += new System.EventHandler(this.btnUser_Click);
            // 
            // btnUserRoles
            // 
            this.btnUserRoles.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnUserRoles.FixedSize = new System.Drawing.Size(100, 10);
            this.btnUserRoles.Image = global::MyPayfriend.Properties.Resources.RoleSettings;
            this.btnUserRoles.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnUserRoles.Name = "btnUserRoles";
            this.btnUserRoles.SubItemsExpandWidth = 14;
            this.btnUserRoles.Text = "Role Settings";
            this.btnUserRoles.Tooltip = "Role Settings";
            this.btnUserRoles.Click += new System.EventHandler(this.btnUserRoles_Click);
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnChangePassword.FixedSize = new System.Drawing.Size(100, 10);
            this.btnChangePassword.Image = global::MyPayfriend.Properties.Resources.ChangePassword1;
            this.btnChangePassword.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.SubItemsExpandWidth = 14;
            this.btnChangePassword.Text = "Change Password";
            this.btnChangePassword.Tooltip = "Change Password";
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // ribbonBar6
            // 
            this.ribbonBar6.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar6.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundStyle.Class = "";
            this.ribbonBar6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar6.ContainerControlProcessDialogKey = true;
            this.ribbonBar6.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar6.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnConfig,
            this.btnMailSettings});
            this.ribbonBar6.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar6.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar6.Name = "ribbonBar6";
            this.ribbonBar6.Size = new System.Drawing.Size(215, 87);
            this.ribbonBar6.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar6.TabIndex = 3;
            this.ribbonBar6.Text = "Configuration";
            // 
            // 
            // 
            this.ribbonBar6.TitleStyle.Class = "";
            this.ribbonBar6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.TitleStyleMouseOver.Class = "";
            this.ribbonBar6.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar6.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnConfig
            // 
            this.btnConfig.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnConfig.FixedSize = new System.Drawing.Size(100, 10);
            this.btnConfig.Image = global::MyPayfriend.Properties.Resources.Configuration1;
            this.btnConfig.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.SubItemsExpandWidth = 14;
            this.btnConfig.Text = "Con&figuration";
            this.btnConfig.Tooltip = "Configuration";
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // btnMailSettings
            // 
            this.btnMailSettings.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMailSettings.FixedSize = new System.Drawing.Size(100, 10);
            this.btnMailSettings.Image = global::MyPayfriend.Properties.Resources.EmailSettings;
            this.btnMailSettings.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMailSettings.Name = "btnMailSettings";
            this.btnMailSettings.SubItemsExpandWidth = 14;
            this.btnMailSettings.Text = "&Email";
            this.btnMailSettings.Tooltip = "Email Configuration";
            this.btnMailSettings.Click += new System.EventHandler(this.btnMailSettings_Click);
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel6.Controls.Add(this.ribbonBar10);
            this.ribbonPanel6.Controls.Add(this.ribbonBar9);
            this.ribbonPanel6.Controls.Add(this.ribbonBar8);
            this.ribbonPanel6.Controls.Add(this.ribbonBar13);
            this.ribbonPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel6.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel6.Name = "ribbonPanel6";
            this.ribbonPanel6.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel6.Size = new System.Drawing.Size(1270, 90);
            // 
            // 
            // 
            this.ribbonPanel6.Style.Class = "";
            this.ribbonPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseDown.Class = "";
            this.ribbonPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseOver.Class = "";
            this.ribbonPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel6.TabIndex = 16;
            this.ribbonPanel6.Visible = false;
            // 
            // ribbonBar10
            // 
            this.ribbonBar10.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar10.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar10.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar10.BackgroundStyle.Class = "";
            this.ribbonBar10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar10.ContainerControlProcessDialogKey = true;
            this.ribbonBar10.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar10.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.OtherDocuments,
            this.StockRegister,
            this.btnDocProcessTime});
            this.ribbonBar10.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar10.Location = new System.Drawing.Point(958, 0);
            this.ribbonBar10.Name = "ribbonBar10";
            this.ribbonBar10.Size = new System.Drawing.Size(288, 87);
            this.ribbonBar10.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar10.TabIndex = 2;
            this.ribbonBar10.Text = "StockRegister";
            // 
            // 
            // 
            this.ribbonBar10.TitleStyle.Class = "";
            this.ribbonBar10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar10.TitleStyleMouseOver.Class = "";
            this.ribbonBar10.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar10.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // OtherDocuments
            // 
            this.OtherDocuments.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.OtherDocuments.FixedSize = new System.Drawing.Size(90, 10);
            this.OtherDocuments.Image = ((System.Drawing.Image)(resources.GetObject("OtherDocuments.Image")));
            this.OtherDocuments.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.OtherDocuments.Name = "OtherDocuments";
            this.OtherDocuments.SubItemsExpandWidth = 14;
            this.OtherDocuments.Text = "Other Documents";
            this.OtherDocuments.Tooltip = "Other Documents";
            this.OtherDocuments.Click += new System.EventHandler(this.OtherDocuments_Click);
            // 
            // StockRegister
            // 
            this.StockRegister.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.StockRegister.FixedSize = new System.Drawing.Size(90, 10);
            this.StockRegister.Image = ((System.Drawing.Image)(resources.GetObject("StockRegister.Image")));
            this.StockRegister.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.StockRegister.Name = "StockRegister";
            this.StockRegister.SubItemsExpandWidth = 14;
            this.StockRegister.Text = "Stock Register";
            this.StockRegister.Tooltip = "Stock Register";
            this.StockRegister.Click += new System.EventHandler(this.StockRegister_Click);
            // 
            // btnDocProcessTime
            // 
            this.btnDocProcessTime.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDocProcessTime.FixedSize = new System.Drawing.Size(90, 10);
            this.btnDocProcessTime.Image = ((System.Drawing.Image)(resources.GetObject("btnDocProcessTime.Image")));
            this.btnDocProcessTime.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDocProcessTime.Name = "btnDocProcessTime";
            this.btnDocProcessTime.SubItemsExpandWidth = 14;
            this.btnDocProcessTime.Text = "Processing Time";
            this.btnDocProcessTime.Tooltip = "Processing Time";
            this.btnDocProcessTime.Click += new System.EventHandler(this.btnDocProcessTime_Click);
            // 
            // ribbonBar9
            // 
            this.ribbonBar9.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar9.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundStyle.Class = "";
            this.ribbonBar9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar9.ContainerControlProcessDialogKey = true;
            this.ribbonBar9.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar9.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.EmiratesCard,
            this.InsuranceCard,
            this.LabourCard,
            this.HealthCard});
            this.ribbonBar9.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar9.Location = new System.Drawing.Point(622, 0);
            this.ribbonBar9.Name = "ribbonBar9";
            this.ribbonBar9.Size = new System.Drawing.Size(336, 87);
            this.ribbonBar9.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar9.TabIndex = 1;
            this.ribbonBar9.Text = "Employee Cards";
            // 
            // 
            // 
            this.ribbonBar9.TitleStyle.Class = "";
            this.ribbonBar9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.TitleStyleMouseOver.Class = "";
            this.ribbonBar9.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar9.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // EmiratesCard
            // 
            this.EmiratesCard.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.EmiratesCard.FixedSize = new System.Drawing.Size(80, 10);
            this.EmiratesCard.Image = ((System.Drawing.Image)(resources.GetObject("EmiratesCard.Image")));
            this.EmiratesCard.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.EmiratesCard.Name = "EmiratesCard";
            this.EmiratesCard.SubItemsExpandWidth = 14;
            this.EmiratesCard.Text = "Emirates Card";
            this.EmiratesCard.Tooltip = "Emirates Card";
            this.EmiratesCard.Click += new System.EventHandler(this.EmiratesCard_Click);
            // 
            // InsuranceCard
            // 
            this.InsuranceCard.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.InsuranceCard.FixedSize = new System.Drawing.Size(80, 10);
            this.InsuranceCard.Image = ((System.Drawing.Image)(resources.GetObject("InsuranceCard.Image")));
            this.InsuranceCard.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.InsuranceCard.Name = "InsuranceCard";
            this.InsuranceCard.SubItemsExpandWidth = 14;
            this.InsuranceCard.Text = "Insurance Card";
            this.InsuranceCard.Tooltip = "Insurance Card";
            this.InsuranceCard.Click += new System.EventHandler(this.InsuranceCard_Click);
            // 
            // LabourCard
            // 
            this.LabourCard.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.LabourCard.FixedSize = new System.Drawing.Size(80, 10);
            this.LabourCard.Image = ((System.Drawing.Image)(resources.GetObject("LabourCard.Image")));
            this.LabourCard.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.LabourCard.Name = "LabourCard";
            this.LabourCard.SubItemsExpandWidth = 14;
            this.LabourCard.Text = "Labour Card";
            this.LabourCard.Tooltip = "Labour Card";
            this.LabourCard.Click += new System.EventHandler(this.LabourCard_Click);
            // 
            // HealthCard
            // 
            this.HealthCard.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.HealthCard.FixedSize = new System.Drawing.Size(80, 10);
            this.HealthCard.Image = ((System.Drawing.Image)(resources.GetObject("HealthCard.Image")));
            this.HealthCard.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.HealthCard.Name = "HealthCard";
            this.HealthCard.SubItemsExpandWidth = 14;
            this.HealthCard.Text = "Health Card";
            this.HealthCard.Tooltip = "Health Card";
            this.HealthCard.Click += new System.EventHandler(this.HealthCard_Click);
            // 
            // ribbonBar8
            // 
            this.ribbonBar8.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar8.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundStyle.Class = "";
            this.ribbonBar8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar8.ContainerControlProcessDialogKey = true;
            this.ribbonBar8.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar8.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.Passport,
            this.btnVisa,
            this.DrivingLicense,
            this.Qualification});
            this.ribbonBar8.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar8.Location = new System.Drawing.Point(284, 0);
            this.ribbonBar8.Name = "ribbonBar8";
            this.ribbonBar8.Size = new System.Drawing.Size(338, 87);
            this.ribbonBar8.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar8.TabIndex = 0;
            this.ribbonBar8.Text = "Employee Documents";
            // 
            // 
            // 
            this.ribbonBar8.TitleStyle.Class = "";
            this.ribbonBar8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.TitleStyleMouseOver.Class = "";
            this.ribbonBar8.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar8.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // Passport
            // 
            this.Passport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Passport.FixedSize = new System.Drawing.Size(80, 10);
            this.Passport.Image = ((System.Drawing.Image)(resources.GetObject("Passport.Image")));
            this.Passport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Passport.Name = "Passport";
            this.Passport.SubItemsExpandWidth = 14;
            this.Passport.Text = "Passport";
            this.Passport.Tooltip = "Passport";
            this.Passport.Click += new System.EventHandler(this.Passport_Click);
            // 
            // btnVisa
            // 
            this.btnVisa.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnVisa.FixedSize = new System.Drawing.Size(80, 10);
            this.btnVisa.Image = ((System.Drawing.Image)(resources.GetObject("btnVisa.Image")));
            this.btnVisa.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnVisa.Name = "btnVisa";
            this.btnVisa.SubItemsExpandWidth = 14;
            this.btnVisa.Text = "Visa";
            this.btnVisa.Tooltip = "Visa";
            this.btnVisa.Click += new System.EventHandler(this.Visas_Click);
            // 
            // DrivingLicense
            // 
            this.DrivingLicense.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.DrivingLicense.FixedSize = new System.Drawing.Size(80, 10);
            this.DrivingLicense.Image = ((System.Drawing.Image)(resources.GetObject("DrivingLicense.Image")));
            this.DrivingLicense.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.DrivingLicense.Name = "DrivingLicense";
            this.DrivingLicense.SubItemsExpandWidth = 14;
            this.DrivingLicense.Text = "Driving License";
            this.DrivingLicense.Tooltip = "Driving License";
            this.DrivingLicense.Click += new System.EventHandler(this.DrivingLicense_Click);
            // 
            // Qualification
            // 
            this.Qualification.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.Qualification.FixedSize = new System.Drawing.Size(80, 10);
            this.Qualification.Image = ((System.Drawing.Image)(resources.GetObject("Qualification.Image")));
            this.Qualification.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Qualification.Name = "Qualification";
            this.Qualification.SubItemsExpandWidth = 14;
            this.Qualification.Text = "Qualification";
            this.Qualification.Tooltip = "Qualification";
            this.Qualification.Click += new System.EventHandler(this.Qualification_Click);
            // 
            // ribbonBar13
            // 
            this.ribbonBar13.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar13.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar13.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar13.BackgroundStyle.Class = "";
            this.ribbonBar13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar13.ContainerControlProcessDialogKey = true;
            this.ribbonBar13.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar13.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnTradeLicense,
            this.btnLeaseAgreement,
            this.btnInsurance});
            this.ribbonBar13.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar13.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar13.Name = "ribbonBar13";
            this.ribbonBar13.Size = new System.Drawing.Size(281, 87);
            this.ribbonBar13.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar13.TabIndex = 3;
            this.ribbonBar13.Text = "Company Documents";
            // 
            // 
            // 
            this.ribbonBar13.TitleStyle.Class = "";
            this.ribbonBar13.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar13.TitleStyleMouseOver.Class = "";
            this.ribbonBar13.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar13.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // btnTradeLicense
            // 
            this.btnTradeLicense.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnTradeLicense.FixedSize = new System.Drawing.Size(90, 10);
            this.btnTradeLicense.Image = global::MyPayfriend.Properties.Resources.TradeLicence;
            this.btnTradeLicense.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnTradeLicense.Name = "btnTradeLicense";
            this.btnTradeLicense.SubItemsExpandWidth = 14;
            this.btnTradeLicense.Text = "TradeLicense";
            this.btnTradeLicense.Tooltip = "TradeLicense";
            this.btnTradeLicense.Click += new System.EventHandler(this.btnTradeLicense_Click);
            // 
            // btnLeaseAgreement
            // 
            this.btnLeaseAgreement.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLeaseAgreement.FixedSize = new System.Drawing.Size(90, 10);
            this.btnLeaseAgreement.Image = global::MyPayfriend.Properties.Resources.LeaseAgreement1;
            this.btnLeaseAgreement.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLeaseAgreement.Name = "btnLeaseAgreement";
            this.btnLeaseAgreement.SubItemsExpandWidth = 14;
            this.btnLeaseAgreement.Text = "Lease Agreement";
            this.btnLeaseAgreement.Tooltip = "Lease Agreement";
            this.btnLeaseAgreement.Click += new System.EventHandler(this.btnLeaseAgreement_Click);
            // 
            // btnInsurance
            // 
            this.btnInsurance.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnInsurance.FixedSize = new System.Drawing.Size(90, 10);
            this.btnInsurance.Image = global::MyPayfriend.Properties.Resources.Insurance;
            this.btnInsurance.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnInsurance.Name = "btnInsurance";
            this.btnInsurance.SubItemsExpandWidth = 14;
            this.btnInsurance.Text = "Insurance";
            this.btnInsurance.Tooltip = "Insurance";
            this.btnInsurance.Click += new System.EventHandler(this.btnInsurance_Click);
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonPanel2.Controls.Add(this.ribbonBar16);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 57);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel2.Size = new System.Drawing.Size(1262, 90);
            // 
            // 
            // 
            this.ribbonPanel2.Style.Class = "";
            this.ribbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseDown.Class = "";
            this.ribbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseOver.Class = "";
            this.ribbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel2.TabIndex = 15;
            this.ribbonPanel2.Visible = false;
            // 
            // ribbonBar16
            // 
            this.ribbonBar16.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar16.BackgroundMouseOverStyle.Class = "";
            this.ribbonBar16.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar16.BackgroundStyle.Class = "";
            this.ribbonBar16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar16.ContainerControlProcessDialogKey = true;
            this.ribbonBar16.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar16.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.About,
            this.btnHlpContents,
            this.btnSupport});
            this.ribbonBar16.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ribbonBar16.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar16.Name = "ribbonBar16";
            this.ribbonBar16.Size = new System.Drawing.Size(347, 87);
            this.ribbonBar16.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonBar16.TabIndex = 1;
            this.ribbonBar16.Text = "Help";
            // 
            // 
            // 
            this.ribbonBar16.TitleStyle.Class = "";
            this.ribbonBar16.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar16.TitleStyleMouseOver.Class = "";
            this.ribbonBar16.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar16.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // About
            // 
            this.About.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.About.FixedSize = new System.Drawing.Size(110, 10);
            this.About.Image = global::MyPayfriend.Properties.Resources.About;
            this.About.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.About.Name = "About";
            this.About.SubItemsExpandWidth = 14;
            this.About.Text = "About";
            this.About.Click += new System.EventHandler(this.About_Click);
            // 
            // btnHlpContents
            // 
            this.btnHlpContents.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnHlpContents.FixedSize = new System.Drawing.Size(110, 10);
            this.btnHlpContents.Image = global::MyPayfriend.Properties.Resources.help1;
            this.btnHlpContents.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnHlpContents.Name = "btnHlpContents";
            this.btnHlpContents.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.btnHlpContents.SubItemsExpandWidth = 14;
            this.btnHlpContents.Text = "Contents";
            this.btnHlpContents.Tooltip = "Contents";
            this.btnHlpContents.Click += new System.EventHandler(this.btnHlpContents_Click);
            // 
            // btnSupport
            // 
            this.btnSupport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSupport.FixedSize = new System.Drawing.Size(110, 10);
            this.btnSupport.Image = global::MyPayfriend.Properties.Resources.support;
            this.btnSupport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSupport.Name = "btnSupport";
            this.btnSupport.SubItemsExpandWidth = 14;
            this.btnSupport.Text = "Support";
            this.btnSupport.Tooltip = "Support";
            this.btnSupport.Click += new System.EventHandler(this.btnSupport_Click);
            // 
            // General
            // 
            this.General.Checked = true;
            this.General.HotFontBold = true;
            this.General.Name = "General";
            this.General.Panel = this.ribbonPanel9;
            this.General.Text = "&Home";
            this.General.Tooltip = "Home Module";
            this.General.Click += new System.EventHandler(this.General_Click);
            // 
            // Company
            // 
            this.Company.GlobalName = "Company";
            this.Company.HotFontBold = true;
            this.Company.Name = "Company";
            this.Company.Panel = this.ribbonPanel3;
            this.Company.Text = "&Masters";
            this.Company.Tooltip = "Master Module";
            // 
            // Attendance
            // 
            this.Attendance.GlobalName = "Attendance";
            this.Attendance.HotFontBold = true;
            this.Attendance.Name = "Attendance";
            this.Attendance.Panel = this.ribbonPanel1;
            this.Attendance.Text = "&Employee";
            this.Attendance.Tooltip = "Employee";
            this.Attendance.Click += new System.EventHandler(this.Attendance_Click);
            // 
            // Payroll
            // 
            this.Payroll.HotFontBold = true;
            this.Payroll.Name = "Payroll";
            this.Payroll.Panel = this.ribbonPanel10;
            this.Payroll.Text = "Payroll";
            this.Payroll.Tooltip = "Payroll Module";
            // 
            // ribbonTabItem4
            // 
            this.ribbonTabItem4.Name = "ribbonTabItem4";
            this.ribbonTabItem4.Panel = this.ribbonPanel6;
            this.ribbonTabItem4.Text = "Documents";
            this.ribbonTabItem4.Tooltip = "Documents";
            this.ribbonTabItem4.Click += new System.EventHandler(this.ribbonTabItem4_Click);
            // 
            // ribbonTabItem5
            // 
            this.ribbonTabItem5.Name = "ribbonTabItem5";
            this.ribbonTabItem5.Panel = this.ribbonPanel7;
            this.ribbonTabItem5.Text = "Task";
            this.ribbonTabItem5.Tooltip = "Task";
            this.ribbonTabItem5.Click += new System.EventHandler(this.ribbonTabItem5_Click);
            // 
            // ribbonTabItem2
            // 
            this.ribbonTabItem2.Name = "ribbonTabItem2";
            this.ribbonTabItem2.Panel = this.ribbonPanel4;
            this.ribbonTabItem2.Text = "Reports";
            this.ribbonTabItem2.Tooltip = "Reports Module";
            // 
            // ribbonTabItem6
            // 
            this.ribbonTabItem6.Name = "ribbonTabItem6";
            this.ribbonTabItem6.Panel = this.ribbonPanel8;
            this.ribbonTabItem6.Text = "Payroll Reports";
            // 
            // ribbonTabItem3
            // 
            this.ribbonTabItem3.Name = "ribbonTabItem3";
            this.ribbonTabItem3.Panel = this.ribbonPanel5;
            this.ribbonTabItem3.Text = "Settings";
            // 
            // ribbonTabItem1
            // 
            this.ribbonTabItem1.Name = "ribbonTabItem1";
            this.ribbonTabItem1.Panel = this.ribbonPanel2;
            this.ribbonTabItem1.Text = "Help";
            // 
            // buttonChangeStyle
            // 
            this.buttonChangeStyle.AutoExpandOnClick = true;
            this.buttonChangeStyle.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far;
            this.buttonChangeStyle.Name = "buttonChangeStyle";
            this.buttonChangeStyle.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonStyleOffice2007Blue,
            this.buttonStyleOffice2007Black,
            this.buttonStyleOffice2007Silver,
            this.buttonItem60,
            this.buttonStyleCustom});
            superTooltipInfo3.BodyText = "Change the style of all DotNetBar User Interface elements.";
            superTooltipInfo3.HeaderText = "Change the style";
            this.superTooltip1.SetSuperTooltip(this.buttonChangeStyle, superTooltipInfo3);
            this.buttonChangeStyle.Text = "Style";
            // 
            // buttonStyleOffice2007Blue
            // 
            this.buttonStyleOffice2007Blue.Command = this.AppCommandTheme;
            this.buttonStyleOffice2007Blue.CommandParameter = "Blue";
            this.buttonStyleOffice2007Blue.Name = "buttonStyleOffice2007Blue";
            this.buttonStyleOffice2007Blue.OptionGroup = "style";
            this.buttonStyleOffice2007Blue.Text = "Office 2007 <font color=\"Blue\"><b>Blue</b></font>";
            // 
            // AppCommandTheme
            // 
            this.AppCommandTheme.Name = "AppCommandTheme";
            this.AppCommandTheme.Executed += new System.EventHandler(this.AppCommandTheme_Executed);
            // 
            // buttonStyleOffice2007Black
            // 
            this.buttonStyleOffice2007Black.Command = this.AppCommandTheme;
            this.buttonStyleOffice2007Black.CommandParameter = "Black";
            this.buttonStyleOffice2007Black.Name = "buttonStyleOffice2007Black";
            this.buttonStyleOffice2007Black.OptionGroup = "style";
            this.buttonStyleOffice2007Black.Text = "Office 2007 <font color=\"black\"><b>Black</b></font>";
            // 
            // buttonStyleOffice2007Silver
            // 
            this.buttonStyleOffice2007Silver.Command = this.AppCommandTheme;
            this.buttonStyleOffice2007Silver.CommandParameter = "Silver";
            this.buttonStyleOffice2007Silver.Name = "buttonStyleOffice2007Silver";
            this.buttonStyleOffice2007Silver.OptionGroup = "style";
            this.buttonStyleOffice2007Silver.Text = "Office 2007 <font color=\"Silver\"><b>Silver</b></font>";
            // 
            // buttonItem60
            // 
            this.buttonItem60.Checked = true;
            this.buttonItem60.Command = this.AppCommandTheme;
            this.buttonItem60.CommandParameter = "VistaGlass";
            this.buttonItem60.Name = "buttonItem60";
            this.buttonItem60.OptionGroup = "style";
            this.buttonItem60.Text = "Mindsoft";
            // 
            // buttonStyleCustom
            // 
            this.buttonStyleCustom.BeginGroup = true;
            this.buttonStyleCustom.Command = this.AppCommandTheme;
            this.buttonStyleCustom.Name = "buttonStyleCustom";
            this.buttonStyleCustom.Text = "Custom scheme";
            this.buttonStyleCustom.Tooltip = "Custom color scheme is created based on currently selected color table. Try selec" +
                "ting Silver or Blue color table and then creating custom color scheme.";
            this.buttonStyleCustom.SelectedColorChanged += new System.EventHandler(this.buttonStyleCustom_SelectedColorChanged);
            this.buttonStyleCustom.ColorPreview += new DevComponents.DotNetBar.ColorPreviewEventHandler(this.buttonStyleCustom_ColorPreview);
            this.buttonStyleCustom.ExpandChange += new System.EventHandler(this.buttonStyleCustom_ExpandChange);
            // 
            // office2007StartButton1
            // 
            this.office2007StartButton1.AutoExpandOnClick = true;
            this.office2007StartButton1.CanCustomize = false;
            this.office2007StartButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.office2007StartButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("office2007StartButton1.Icon")));
            this.office2007StartButton1.Image = ((System.Drawing.Image)(resources.GetObject("office2007StartButton1.Image")));
            this.office2007StartButton1.ImagePaddingHorizontal = 2;
            this.office2007StartButton1.ImagePaddingVertical = 2;
            this.office2007StartButton1.Name = "office2007StartButton1";
            this.office2007StartButton1.ShowSubItems = false;
            this.office2007StartButton1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer1});
            this.office2007StartButton1.Text = "&File";
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.itemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer2,
            this.itemContainer5});
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer";
            this.itemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer2.BackgroundStyle.PaddingBottom = 2;
            this.itemContainer2.BackgroundStyle.PaddingLeft = 2;
            this.itemContainer2.BackgroundStyle.PaddingRight = 2;
            this.itemContainer2.BackgroundStyle.PaddingTop = 2;
            this.itemContainer2.ItemSpacing = 0;
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer3});
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.itemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer3.ItemSpacing = 5;
            this.itemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer3.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.mbtnChangeCompany,
            this.mbtnCompany,
            this.mbtnEmployee,
            this.mbtnSalaryStructure,
            this.mbtnEmployeePayment,
            this.btnLogout1});
            // 
            // mbtnChangeCompany
            // 
            this.mbtnChangeCompany.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.mbtnChangeCompany.Image = global::MyPayfriend.Properties.Resources.Attendance_automatic;
            this.mbtnChangeCompany.Name = "mbtnChangeCompany";
            this.mbtnChangeCompany.Text = "Change Company";
            this.mbtnChangeCompany.Tooltip = "Change Company";
            this.mbtnChangeCompany.Visible = false;
            this.mbtnChangeCompany.Click += new System.EventHandler(this.mbtnChangeCompany_Click);
            // 
            // mbtnCompany
            // 
            this.mbtnCompany.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.mbtnCompany.Image = global::MyPayfriend.Properties.Resources.Company;
            this.mbtnCompany.Name = "mbtnCompany";
            this.mbtnCompany.SubItemsExpandWidth = 24;
            this.mbtnCompany.Text = "&Company";
            this.mbtnCompany.Tooltip = "Company";
            this.mbtnCompany.Click += new System.EventHandler(this.mbtnCompany_Click);
            // 
            // mbtnEmployee
            // 
            this.mbtnEmployee.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.mbtnEmployee.Image = global::MyPayfriend.Properties.Resources.Executive;
            this.mbtnEmployee.Name = "mbtnEmployee";
            this.mbtnEmployee.SubItemsExpandWidth = 24;
            this.mbtnEmployee.Text = "&Employee";
            this.mbtnEmployee.Tooltip = "Employee";
            this.mbtnEmployee.Click += new System.EventHandler(this.mbtnEmployee_Click);
            // 
            // mbtnSalaryStructure
            // 
            this.mbtnSalaryStructure.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.mbtnSalaryStructure.Image = global::MyPayfriend.Properties.Resources.salarystructure;
            this.mbtnSalaryStructure.Name = "mbtnSalaryStructure";
            this.mbtnSalaryStructure.Text = "Salary Structure";
            this.mbtnSalaryStructure.Tooltip = "Salary Structure";
            this.mbtnSalaryStructure.Click += new System.EventHandler(this.mbtnSalaryStructure_Click);
            // 
            // mbtnEmployeePayment
            // 
            this.mbtnEmployeePayment.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.mbtnEmployeePayment.Image = global::MyPayfriend.Properties.Resources.Payment1;
            this.mbtnEmployeePayment.Name = "mbtnEmployeePayment";
            this.mbtnEmployeePayment.Text = "Employee Payment";
            this.mbtnEmployeePayment.Tooltip = "Employee Payment";
            this.mbtnEmployeePayment.Click += new System.EventHandler(this.mbtnEmployeePayment_Click);
            // 
            // btnLogout1
            // 
            this.btnLogout1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLogout1.Image = global::MyPayfriend.Properties.Resources.Logout1;
            this.btnLogout1.Name = "btnLogout1";
            this.btnLogout1.Text = "Log Out";
            this.btnLogout1.Click += new System.EventHandler(this.btnLogout1_Click);
            // 
            // itemContainer5
            // 
            // 
            // 
            // 
            this.itemContainer5.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.itemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer5.Name = "itemContainer5";
            this.itemContainer5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem12});
            // 
            // buttonItem12
            // 
            this.buttonItem12.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem12.Command = this.AppCommandExit;
            this.buttonItem12.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem12.Image")));
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.SubItemsExpandWidth = 24;
            this.buttonItem12.Text = "E&xit MyPayfriend";
            // 
            // AppCommandExit
            // 
            this.AppCommandExit.Name = "AppCommandExit";
            this.AppCommandExit.Executed += new System.EventHandler(this.AppCommandExit_Executed);
            // 
            // btnHome
            // 
            this.btnHome.Name = "btnHome";
            this.btnHome.Text = "Home";
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // bMPFmenu
            // 
            this.bMPFmenu.Image = global::MyPayfriend.Properties.Resources.Minimize;
            this.bMPFmenu.Name = "bMPFmenu";
            this.bMPFmenu.Text = "Minimize Menu";
            this.bMPFmenu.Tooltip = "Minimize Menu";
            this.bMPFmenu.Click += new System.EventHandler(this.bMPFmenu_Click);
            // 
            // qatCustomizeItem1
            // 
            this.qatCustomizeItem1.Name = "qatCustomizeItem1";
            this.qatCustomizeItem1.Visible = false;
            // 
            // ribbonTabItemGroup1
            // 
            this.ribbonTabItemGroup1.Color = DevComponents.DotNetBar.eRibbonTabGroupColor.Orange;
            this.ribbonTabItemGroup1.GroupTitle = "Smart";
            this.ribbonTabItemGroup1.Name = "ribbonTabItemGroup1";
            // 
            // 
            // 
            this.ribbonTabItemGroup1.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(158)))), ((int)(((byte)(159)))));
            this.ribbonTabItemGroup1.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(226)))));
            this.ribbonTabItemGroup1.Style.BackColorGradientAngle = 90;
            this.ribbonTabItemGroup1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderBottomWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(58)))), ((int)(((byte)(59)))));
            this.ribbonTabItemGroup1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderLeftWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderRightWidth = 1;
            this.ribbonTabItemGroup1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.ribbonTabItemGroup1.Style.BorderTopWidth = 1;
            this.ribbonTabItemGroup1.Style.Class = "";
            this.ribbonTabItemGroup1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonTabItemGroup1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.ribbonTabItemGroup1.Style.TextColor = System.Drawing.Color.Black;
            this.ribbonTabItemGroup1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // AppCommandNew
            // 
            this.AppCommandNew.Name = "AppCommandNew";
            this.AppCommandNew.Executed += new System.EventHandler(this.AppCommandNew_Executed);
            // 
            // buttonItem47
            // 
            this.buttonItem47.BeginGroup = true;
            this.buttonItem47.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem47.Image")));
            this.buttonItem47.Name = "buttonItem47";
            this.buttonItem47.Text = "Search for Templates Online...";
            // 
            // buttonItem48
            // 
            this.buttonItem48.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem48.Image")));
            this.buttonItem48.Name = "buttonItem48";
            this.buttonItem48.Text = "Browse for Templates...";
            // 
            // buttonItem49
            // 
            this.buttonItem49.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem49.Image")));
            this.buttonItem49.Name = "buttonItem49";
            this.buttonItem49.Text = "Save Current Template...";
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "6";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "7";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "8";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "9";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "10";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "11";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "12";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "13";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "14";
            // 
            // superTooltip1
            // 
            this.superTooltip1.DefaultFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.superTooltip1.MinimumTooltipSize = new System.Drawing.Size(150, 50);
            // 
            // AppCmdPurchase
            // 
            this.AppCmdPurchase.Name = "AppCmdPurchase";
            // 
            // AppCmdSales
            // 
            this.AppCmdSales.Name = "AppCmdSales";
            // 
            // AppCmdRoleSettings
            // 
            this.AppCmdRoleSettings.Name = "AppCmdRoleSettings";
            // 
            // AppCommandReports
            // 
            this.AppCommandReports.Name = "AppCommandReports";
            // 
            // dockContainerItem5
            // 
            this.dockContainerItem5.DefaultFloatingSize = new System.Drawing.Size(256, 196);
            this.dockContainerItem5.GlobalItem = true;
            this.dockContainerItem5.GlobalName = "dockSearchResults";
            this.dockContainerItem5.Image = global::MyPayfriend.Properties.Resources.rss;
            this.dockContainerItem5.MinimumSize = new System.Drawing.Size(64, 64);
            this.dockContainerItem5.Name = "dockContainerItem5";
            this.dockContainerItem5.Text = "Product Updates";
            // 
            // dockRss
            // 
            this.dockRss.DefaultFloatingSize = new System.Drawing.Size(256, 196);
            this.dockRss.GlobalItem = true;
            this.dockRss.GlobalName = "dockSearchResults";
            this.dockRss.Image = global::MyPayfriend.Properties.Resources.rss;
            this.dockRss.MinimumSize = new System.Drawing.Size(64, 64);
            this.dockRss.Name = "dockRss";
            this.dockRss.Text = "Product Updates";
            // 
            // AppCmdInventory
            // 
            this.AppCmdInventory.Name = "AppCmdInventory";
            // 
            // buttonFile
            // 
            this.buttonFile.AutoExpandOnClick = true;
            this.buttonFile.CanCustomize = false;
            this.buttonFile.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.buttonFile.Icon = ((System.Drawing.Icon)(resources.GetObject("buttonFile.Icon")));
            this.buttonFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonFile.Image")));
            this.buttonFile.ImagePaddingHorizontal = 2;
            this.buttonFile.ImagePaddingVertical = 2;
            this.buttonFile.Name = "buttonFile";
            this.buttonFile.ShowSubItems = false;
            this.buttonFile.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.menuFileContainer});
            this.buttonFile.Text = "&File";
            // 
            // menuFileContainer
            // 
            // 
            // 
            // 
            this.menuFileContainer.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.menuFileContainer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.menuFileContainer.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.menuFileContainer.Name = "menuFileContainer";
            this.menuFileContainer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.menuFileTwoColumnContainer,
            this.menuFileBottomContainer});
            // 
            // menuFileTwoColumnContainer
            // 
            // 
            // 
            // 
            this.menuFileTwoColumnContainer.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer";
            this.menuFileTwoColumnContainer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.menuFileTwoColumnContainer.BackgroundStyle.PaddingBottom = 2;
            this.menuFileTwoColumnContainer.BackgroundStyle.PaddingLeft = 2;
            this.menuFileTwoColumnContainer.BackgroundStyle.PaddingRight = 2;
            this.menuFileTwoColumnContainer.BackgroundStyle.PaddingTop = 2;
            this.menuFileTwoColumnContainer.ItemSpacing = 0;
            this.menuFileTwoColumnContainer.Name = "menuFileTwoColumnContainer";
            this.menuFileTwoColumnContainer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.menuFileItems,
            this.menuFileMRU});
            // 
            // menuFileItems
            // 
            // 
            // 
            // 
            this.menuFileItems.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.menuFileItems.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.menuFileItems.ItemSpacing = 5;
            this.menuFileItems.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.menuFileItems.MinimumSize = new System.Drawing.Size(120, 0);
            this.menuFileItems.Name = "menuFileItems";
            this.menuFileItems.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnAccountsMain,
            this.buttonItem3,
            this.btnProduction,
            this.btnDocuments,
            this.btnPayroll,
            this.btnLogOut,
            this.buttonItem25});
            // 
            // btnAccountsMain
            // 
            this.btnAccountsMain.Name = "btnAccountsMain";
            // 
            // buttonItem3
            // 
            this.buttonItem3.Name = "buttonItem3";
            // 
            // btnProduction
            // 
            this.btnProduction.Name = "btnProduction";
            // 
            // btnDocuments
            // 
            this.btnDocuments.Name = "btnDocuments";
            // 
            // btnPayroll
            // 
            this.btnPayroll.BeginGroup = true;
            this.btnPayroll.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnPayroll.Image = global::MyPayfriend.Properties.Resources.Executive;
            this.btnPayroll.Name = "btnPayroll";
            this.btnPayroll.SubItemsExpandWidth = 24;
            this.btnPayroll.Text = "&Payroll";
            // 
            // btnLogOut
            // 
            this.btnLogOut.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLogOut.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLogOut.Icon")));
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Text = "LogOut";
            this.btnLogOut.Tooltip = "Log Out";
            // 
            // buttonItem25
            // 
            this.buttonItem25.BeginGroup = true;
            this.buttonItem25.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem25.Command = this.AppCommandExit;
            this.buttonItem25.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem25.Image")));
            this.buttonItem25.Name = "buttonItem25";
            this.buttonItem25.SubItemsExpandWidth = 24;
            this.buttonItem25.Text = "&Close";
            this.buttonItem25.Visible = false;
            // 
            // menuFileMRU
            // 
            // 
            // 
            // 
            this.menuFileMRU.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.menuFileMRU.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.menuFileMRU.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.menuFileMRU.MinimumSize = new System.Drawing.Size(225, 0);
            this.menuFileMRU.Name = "menuFileMRU";
            this.menuFileMRU.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem8});
            // 
            // labelItem8
            // 
            this.labelItem8.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom;
            this.labelItem8.BorderType = DevComponents.DotNetBar.eBorderType.Etched;
            this.labelItem8.Name = "labelItem8";
            this.labelItem8.PaddingBottom = 2;
            this.labelItem8.PaddingTop = 2;
            this.labelItem8.Stretch = true;
            this.labelItem8.Text = "Recent Documents";
            // 
            // menuFileBottomContainer
            // 
            // 
            // 
            // 
            this.menuFileBottomContainer.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.menuFileBottomContainer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.menuFileBottomContainer.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.menuFileBottomContainer.Name = "menuFileBottomContainer";
            this.menuFileBottomContainer.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonOptions,
            this.buttonExit});
            // 
            // buttonOptions
            // 
            this.buttonOptions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonOptions.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonOptions.Image = ((System.Drawing.Image)(resources.GetObject("buttonOptions.Image")));
            this.buttonOptions.Name = "buttonOptions";
            this.buttonOptions.SubItemsExpandWidth = 24;
            this.buttonOptions.Text = "Smart Trade Opt&ions";
            // 
            // buttonExit
            // 
            this.buttonExit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonExit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonExit.Command = this.AppCommandExit;
            this.buttonExit.Image = ((System.Drawing.Image)(resources.GetObject("buttonExit.Image")));
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.SubItemsExpandWidth = 24;
            this.buttonExit.Text = "E&xit MindSoftERP";
            // 
            // buttonItem2
            // 
            this.buttonItem2.Name = "buttonItem2";
            // 
            // ImgListNavigator
            // 
            this.ImgListNavigator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgListNavigator.ImageStream")));
            this.ImgListNavigator.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgListNavigator.Images.SetKeyName(0, "Company.PNG");
            this.ImgListNavigator.Images.SetKeyName(1, "Client.png");
            this.ImgListNavigator.Images.SetKeyName(2, "Employee.png");
            this.ImgListNavigator.Images.SetKeyName(3, "MaleEmployee.PNG");
            this.ImgListNavigator.Images.SetKeyName(4, "FemaleEmployee.PNG");
            this.ImgListNavigator.Images.SetKeyName(5, "CurrentEmployee.PNG");
            this.ImgListNavigator.Images.SetKeyName(6, "PreviousEmployee.png");
            // 
            // btnAttendanceReport
            // 
            this.btnAttendanceReport.FixedSize = new System.Drawing.Size(100, 10);
            this.btnAttendanceReport.Image = global::MyPayfriend.Properties.Resources.About;
            this.btnAttendanceReport.Name = "btnAttendanceReport";
            this.btnAttendanceReport.SubItemsExpandWidth = 14;
            this.btnAttendanceReport.Text = "Attendance";
            // 
            // dockContainerItem1
            // 
            this.dockContainerItem1.Name = "dockContainerItem1";
            this.dockContainerItem1.Text = "dockContainerItem1";
            // 
            // dockContainerItem2
            // 
            this.dockContainerItem2.Name = "dockContainerItem2";
            this.dockContainerItem2.Text = "dockContainerItem2";
            // 
            // dockContainerItem3
            // 
            this.dockContainerItem3.Name = "dockContainerItem3";
            this.dockContainerItem3.Text = "dockContainerItem3";
            // 
            // dockContainerItem4
            // 
            this.dockContainerItem4.Name = "dockContainerItem4";
            this.dockContainerItem4.Text = "dockContainerItem4";
            // 
            // tmRssmain
            // 
            this.tmRssmain.Tick += new System.EventHandler(this.tmRssmain_Tick);
            // 
            // BackRssWorker
            // 
            this.BackRssWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackRssWorker_DoWork);
            // 
            // tmCheckStatus
            // 
            this.tmCheckStatus.Enabled = true;
            this.tmCheckStatus.Interval = 330000;
            this.tmCheckStatus.Tick += new System.EventHandler(this.tmCheckStatus_Tick);
            // 
            // bar2
            // 
            this.bar2.AccessibleDescription = "DotNetBar Bar (bar2)";
            this.bar2.AccessibleName = "DotNetBar Bar";
            this.bar2.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.bar2.AutoSyncBarCaption = true;
            this.bar2.CloseSingleTab = true;
            this.bar2.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bar2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar2.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Caption;
            this.bar2.LayoutType = DevComponents.DotNetBar.eLayoutType.DockContainer;
            this.bar2.Location = new System.Drawing.Point(8, 8);
            this.bar2.Name = "bar2";
            this.bar2.Size = new System.Drawing.Size(38, 310);
            this.bar2.Stretch = true;
            this.bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.bar2.TabIndex = 2;
            this.bar2.TabStop = false;
            this.bar2.Text = "Product Updates";
            // 
            // dockContainerItem6
            // 
            this.dockContainerItem6.Name = "dockContainerItem6";
            this.dockContainerItem6.Text = "Product Updates";
            // 
            // TaskPane1
            // 
            this.TaskPane1.DefaultFloatingSize = new System.Drawing.Size(193, 290);
            this.TaskPane1.GlobalItem = true;
            this.TaskPane1.GlobalName = "TaskPane1";
            this.TaskPane1.Name = "TaskPane1";
            this.TaskPane1.Text = "Getting Started";
            // 
            // TaskPane2
            // 
            this.TaskPane2.DefaultFloatingSize = new System.Drawing.Size(193, 290);
            this.TaskPane2.GlobalItem = true;
            this.TaskPane2.GlobalName = "TaskPane2";
            this.TaskPane2.Name = "TaskPane2";
            this.TaskPane2.Text = "Research";
            // 
            // tmrAlert
            // 
            this.tmrAlert.Interval = 9000;
            this.tmrAlert.Tick += new System.EventHandler(this.tmrAlert_Tick);
            // 
            // dotNetBarManager1
            // 
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.dotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.dotNetBarManager1.BottomDockSite = this.dockSite4;
            this.dotNetBarManager1.EnableFullSizeDock = false;
            this.dotNetBarManager1.LeftDockSite = this.dockSite1;
            this.dotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.dotNetBarManager1.ParentForm = this;
            this.dotNetBarManager1.RightDockSite = this.dockSite2;
            this.dotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.dotNetBarManager1.ToolbarBottomDockSite = this.dockSite8;
            this.dotNetBarManager1.ToolbarLeftDockSite = this.dockSite5;
            this.dotNetBarManager1.ToolbarRightDockSite = this.dockSite6;
            this.dotNetBarManager1.ToolbarTopDockSite = this.dockSite7;
            this.dotNetBarManager1.TopDockSite = this.dockSite3;
            // 
            // dockSite4
            // 
            this.dockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite4.Location = new System.Drawing.Point(5, 464);
            this.dockSite4.Name = "dockSite4";
            this.dockSite4.Size = new System.Drawing.Size(1384, 0);
            this.dockSite4.TabIndex = 25;
            this.dockSite4.TabStop = false;
            // 
            // dockSite1
            // 
            this.dockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite1.Location = new System.Drawing.Point(5, 150);
            this.dockSite1.Name = "dockSite1";
            this.dockSite1.Size = new System.Drawing.Size(0, 314);
            this.dockSite1.TabIndex = 22;
            this.dockSite1.TabStop = false;
            // 
            // dockSite2
            // 
            this.dockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite2.Controls.Add(this.bar1);
            this.dockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer(new DevComponents.DotNetBar.DocumentBaseContainer[] {
            ((DevComponents.DotNetBar.DocumentBaseContainer)(new DevComponents.DotNetBar.DocumentBarContainer(this.bar1, 136, 314)))}, DevComponents.DotNetBar.eOrientation.Horizontal);
            this.dockSite2.Location = new System.Drawing.Point(1250, 150);
            this.dockSite2.Name = "dockSite2";
            this.dockSite2.Size = new System.Drawing.Size(139, 314);
            this.dockSite2.TabIndex = 23;
            this.dockSite2.TabStop = false;
            // 
            // bar1
            // 
            this.bar1.AccessibleDescription = "DotNetBar Bar (bar1)";
            this.bar1.AccessibleName = "DotNetBar Bar";
            this.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.bar1.AutoSyncBarCaption = true;
            this.bar1.CloseSingleTab = true;
            this.bar1.Controls.Add(this.panelDockContainer2);
            this.bar1.Controls.Add(this.panelDockContainer1);
            this.bar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bar1.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Caption;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.DciAlerts,
            this.DciUpdates});
            this.bar1.LayoutType = DevComponents.DotNetBar.eLayoutType.DockContainer;
            this.bar1.Location = new System.Drawing.Point(3, 0);
            this.bar1.Name = "bar1";
            this.bar1.SelectedDockTab = 0;
            this.bar1.Size = new System.Drawing.Size(136, 314);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.bar1.TabIndex = 0;
            this.bar1.TabStop = false;
            this.bar1.Text = "Alerts";
            // 
            // panelDockContainer2
            // 
            this.panelDockContainer2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelDockContainer2.Controls.Add(this.btnViewAlerts);
            this.panelDockContainer2.Controls.Add(this.reflectionImage1);
            this.panelDockContainer2.Controls.Add(this.lblData);
            this.panelDockContainer2.Location = new System.Drawing.Point(3, 23);
            this.panelDockContainer2.Name = "panelDockContainer2";
            this.panelDockContainer2.Size = new System.Drawing.Size(130, 263);
            this.panelDockContainer2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelDockContainer2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.panelDockContainer2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.panelDockContainer2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.panelDockContainer2.Style.GradientAngle = 90;
            this.panelDockContainer2.TabIndex = 2;
            this.panelDockContainer2.Visible = true;
            // 
            // btnViewAlerts
            // 
            this.btnViewAlerts.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnViewAlerts.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnViewAlerts.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnViewAlerts.Location = new System.Drawing.Point(0, 240);
            this.btnViewAlerts.Name = "btnViewAlerts";
            this.btnViewAlerts.Size = new System.Drawing.Size(130, 23);
            this.btnViewAlerts.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnViewAlerts.TabIndex = 5;
            this.btnViewAlerts.Text = "View Alerts";
            this.btnViewAlerts.Tooltip = "View Alerts";
            this.btnViewAlerts.Click += new System.EventHandler(this.btnViewAlerts_Click);
            // 
            // reflectionImage1
            // 
            this.reflectionImage1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.reflectionImage1.BackgroundStyle.Class = "";
            this.reflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.reflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.reflectionImage1.Image = ((System.Drawing.Image)(resources.GetObject("reflectionImage1.Image")));
            this.reflectionImage1.Location = new System.Drawing.Point(30, 73);
            this.reflectionImage1.Name = "reflectionImage1";
            this.reflectionImage1.Size = new System.Drawing.Size(71, 112);
            this.reflectionImage1.TabIndex = 4;
            this.reflectionImage1.Visible = false;
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            // 
            // 
            // 
            this.lblData.BackgroundStyle.BackColorGradientAngle = 90;
            this.lblData.BackgroundStyle.Class = "";
            this.lblData.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblData.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.Location = new System.Drawing.Point(7, 10);
            this.lblData.Name = "lblData";
            this.lblData.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.lblData.TabIndex = 1;
            // 
            // panelDockContainer1
            // 
            this.panelDockContainer1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelDockContainer1.Controls.Add(this.WebRss);
            this.panelDockContainer1.Location = new System.Drawing.Point(3, 23);
            this.panelDockContainer1.Name = "panelDockContainer1";
            this.panelDockContainer1.Size = new System.Drawing.Size(130, 263);
            this.panelDockContainer1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelDockContainer1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.panelDockContainer1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.panelDockContainer1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.panelDockContainer1.Style.GradientAngle = 90;
            this.panelDockContainer1.TabIndex = 0;
            this.panelDockContainer1.Visible = true;
            // 
            // WebRss
            // 
            this.WebRss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WebRss.Location = new System.Drawing.Point(0, 0);
            this.WebRss.MinimumSize = new System.Drawing.Size(20, 20);
            this.WebRss.Name = "WebRss";
            this.WebRss.ScrollBarsEnabled = false;
            this.WebRss.Size = new System.Drawing.Size(130, 263);
            this.WebRss.TabIndex = 2;
            // 
            // DciAlerts
            // 
            this.DciAlerts.Control = this.panelDockContainer2;
            this.DciAlerts.Image = global::MyPayfriend.Properties.Resources.Alert;
            this.DciAlerts.Name = "DciAlerts";
            this.DciAlerts.Text = "Alerts";
            // 
            // DciUpdates
            // 
            this.DciUpdates.Control = this.panelDockContainer1;
            this.DciUpdates.Image = global::MyPayfriend.Properties.Resources.rss;
            this.DciUpdates.Name = "DciUpdates";
            this.DciUpdates.Text = "Product Updates";
            // 
            // dockSite8
            // 
            this.dockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite8.Controls.Add(this.bar4);
            this.dockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dockSite8.Location = new System.Drawing.Point(5, 464);
            this.dockSite8.Name = "dockSite8";
            this.dockSite8.Size = new System.Drawing.Size(1384, 20);
            this.dockSite8.TabIndex = 29;
            this.dockSite8.TabStop = false;
            // 
            // bar4
            // 
            this.bar4.AccessibleDescription = "DotNetBar Bar (bar4)";
            this.bar4.AccessibleName = "DotNetBar Bar";
            this.bar4.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.bar4.AntiAlias = true;
            this.bar4.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar4.DockSide = DevComponents.DotNetBar.eDockSide.Bottom;
            this.bar4.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.bar4.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelStatus,
            this.itemContainer13,
            this.itemContainer4,
            this.itemContainer6,
            this.itemContainer7});
            this.bar4.ItemSpacing = 2;
            this.bar4.Location = new System.Drawing.Point(0, 1);
            this.bar4.Name = "bar4";
            this.bar4.Size = new System.Drawing.Size(1384, 19);
            this.bar4.Stretch = true;
            this.bar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.bar4.TabIndex = 9;
            this.bar4.TabStop = false;
            this.bar4.Text = "barStatus";
            // 
            // labelStatus
            // 
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.PaddingLeft = 2;
            this.labelStatus.PaddingRight = 2;
            this.labelStatus.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.labelStatus.Stretch = true;
            // 
            // itemContainer13
            // 
            // 
            // 
            // 
            this.itemContainer13.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer13.Name = "itemContainer13";
            this.itemContainer13.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblLoginCompany});
            // 
            // lblLoginCompany
            // 
            this.lblLoginCompany.Name = "lblLoginCompany";
            this.lblLoginCompany.PaddingLeft = 2;
            this.lblLoginCompany.PaddingRight = 2;
            this.lblLoginCompany.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.lblLoginCompany.Stretch = true;
            this.lblLoginCompany.Width = 200;
            // 
            // itemContainer4
            // 
            // 
            // 
            // 
            this.itemContainer4.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer4.Name = "itemContainer4";
            this.itemContainer4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblCurrency});
            // 
            // lblCurrency
            // 
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.PaddingLeft = 2;
            this.lblCurrency.PaddingRight = 2;
            this.lblCurrency.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.lblCurrency.Stretch = true;
            this.lblCurrency.Width = 50;
            // 
            // itemContainer6
            // 
            // 
            // 
            // 
            this.itemContainer6.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer6.Name = "itemContainer6";
            this.itemContainer6.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblUser});
            // 
            // lblUser
            // 
            this.lblUser.Name = "lblUser";
            this.lblUser.PaddingLeft = 2;
            this.lblUser.PaddingRight = 2;
            this.lblUser.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.lblUser.Stretch = true;
            this.lblUser.Width = 100;
            // 
            // itemContainer7
            // 
            // 
            // 
            // 
            this.itemContainer7.BackgroundStyle.Class = "Office2007StatusBarBackground2";
            this.itemContainer7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemContainer7.Name = "itemContainer7";
            this.itemContainer7.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblEmployee});
            // 
            // lblEmployee
            // 
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.PaddingLeft = 2;
            this.lblEmployee.PaddingRight = 2;
            this.lblEmployee.SingleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.lblEmployee.Stretch = true;
            this.lblEmployee.Width = 200;
            // 
            // dockSite5
            // 
            this.dockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.dockSite5.Location = new System.Drawing.Point(5, 1);
            this.dockSite5.Name = "dockSite5";
            this.dockSite5.Size = new System.Drawing.Size(0, 463);
            this.dockSite5.TabIndex = 26;
            this.dockSite5.TabStop = false;
            // 
            // dockSite6
            // 
            this.dockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockSite6.Location = new System.Drawing.Point(1389, 1);
            this.dockSite6.Name = "dockSite6";
            this.dockSite6.Size = new System.Drawing.Size(0, 463);
            this.dockSite6.TabIndex = 27;
            this.dockSite6.TabStop = false;
            // 
            // dockSite7
            // 
            this.dockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite7.Location = new System.Drawing.Point(5, 1);
            this.dockSite7.Name = "dockSite7";
            this.dockSite7.Size = new System.Drawing.Size(1384, 0);
            this.dockSite7.TabIndex = 28;
            this.dockSite7.TabStop = false;
            // 
            // dockSite3
            // 
            this.dockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.dockSite3.Location = new System.Drawing.Point(5, 1);
            this.dockSite3.Name = "dockSite3";
            this.dockSite3.Size = new System.Drawing.Size(1384, 0);
            this.dockSite3.TabIndex = 24;
            this.dockSite3.TabStop = false;
            // 
            // bgwAlerts
            // 
            this.bgwAlerts.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwAlerts_DoWork);
            this.bgwAlerts.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwAlerts_RunWorkerCompleted);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Alert Message";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // bgwMSI
            // 
            this.bgwMSI.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwMSI_DoWork);
            // 
            // FrmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(1394, 486);
            this.Controls.Add(this.tabStrip1);
            this.Controls.Add(this.dockSite2);
            this.Controls.Add(this.dockSite1);
            this.Controls.Add(this.rbnMenuBar);
            this.Controls.Add(this.dockSite3);
            this.Controls.Add(this.dockSite4);
            this.Controls.Add(this.dockSite5);
            this.Controls.Add(this.dockSite6);
            this.Controls.Add(this.dockSite7);
            this.Controls.Add(this.dockSite8);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EPeople";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MdiChildActivate += new System.EventHandler(this.MdiChildActivated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmMain_KeyDown);
            this.rbnMenuBar.ResumeLayout(false);
            this.rbnMenuBar.PerformLayout();
            this.ribbonPanel9.ResumeLayout(false);
            this.ribbonPanel3.ResumeLayout(false);
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonPanel10.ResumeLayout(false);
            this.ribbonPanel4.ResumeLayout(false);
            this.ribbonPanel8.ResumeLayout(false);
            this.ribbonPanel7.ResumeLayout(false);
            this.ribbonPanel5.ResumeLayout(false);
            this.ribbonPanel6.ResumeLayout(false);
            this.ribbonPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar2)).EndInit();
            this.dockSite2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.bar1.ResumeLayout(false);
            this.panelDockContainer2.ResumeLayout(false);
            this.panelDockContainer2.PerformLayout();
            this.panelDockContainer1.ResumeLayout(false);
            this.dockSite8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bar4)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        #region AppCreation
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new FrmMain());
        }
        #endregion


        /// <summary>
        /// Verifies current context and enables/disables menu items...
        /// </summary>
        /// 

        private void LoadMessage()
        {
            MObjClsNotification = new ClsNotification();
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.MainFormRibbon, 4);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.MainFormRibbon, 4);
        }

        private void EnableFileItems()
        {
            // Accessing items through the Items collection and setting the properties on them
            // will propagate certain properties to all items with the same name...
            if (this.ActiveMdiChild == null)
            {
                //AppCommandSave.Enabled=false;
                //AppCommandSaveAs.Enabled = false;
            }
            else
            {
                //AppCommandSave.Enabled = true;
                //AppCommandSaveAs.Enabled = true;
                //if (this.ActiveMdiChild is Navigator)
               //((Navigator)this.ActiveMdiChild);
            }
        }

        private void MdiChildActivated(object sender, System.EventArgs e)
        {
            EnableFileItems();
            UpdateTitle();

        }

        private void UnloadPopup(object sender, System.EventArgs e)
        {
            ButtonItem item = sender as ButtonItem;
            if (item.Name == "bTabColor")
            {
                DevComponents.DotNetBar.PopupContainerControl container = item.PopupContainerControl as PopupContainerControl;
                ColorPicker clr = container.Controls[0] as ColorPicker;
                if (clr.SelectedColor != Color.Empty)
                {
                    tabStrip1.ColorScheme.TabBackground = ControlPaint.LightLight(clr.SelectedColor);
                    tabStrip1.ColorScheme.TabBackground2 = clr.SelectedColor;
                    tabStrip1.Refresh();
                }
                // Close popup menu, since it is not closed when Popup Container is closed...
                item.Parent.Expanded = false;
            }
        }
        #region Automatic Color Scheme creation based on the selected color table
        private bool m_ColorSelected = false;
        private eOffice2007ColorScheme m_BaseColorScheme = eOffice2007ColorScheme.Blue;
        private void buttonStyleCustom_ExpandChange(object sender, System.EventArgs e)
        {
            if (buttonStyleCustom.Expanded)
            {
                // Remember the starting color scheme to apply if no color is selected during live-preview
                m_ColorSelected = false;
                m_BaseColorScheme = ((Office2007Renderer)GlobalManager.Renderer).ColorTable.InitialColorScheme;
            }
            else
            {
                if (!m_ColorSelected)
                {
                    rbnMenuBar.Office2007ColorTable = m_BaseColorScheme;
                }
            }
        }

        private void buttonStyleCustom_ColorPreview(object sender, DevComponents.DotNetBar.ColorPreviewEventArgs e)
        {
            RibbonPredefinedColorSchemes.ChangeOffice2007ColorTable(this, m_BaseColorScheme, e.Color);
        }

        private void buttonStyleCustom_SelectedColorChanged(object sender, System.EventArgs e)
        {
            m_ColorSelected = true; // Indicate that color was selected for buttonStyleCustom_ExpandChange method
            buttonStyleCustom.CommandParameter = buttonStyleCustom.SelectedColor;
        }
        #endregion

        /// <summary>
        /// Updates the title displayed on Ribbon using the rich text markup
        /// </summary>
        private void UpdateTitle()
        {
            string t = "<b>EPeople</b>";
            if (ClsCommonSettings.IsArabicView)
                t = "<b>ماي بايفراند</b>";

            if (this.ActiveMdiChild != null)
            {
                // Note the usage of SysCaptionTextExtra for as the value of color attribute.
                // It specifies the system color that changes based on selected color table.
                t += "<font color=\"SysCaptionTextExtra\">" + this.ActiveMdiChild.Text + "</font> ";
            }
            //t+= "<b>Smart Trade</b>";
            t += " <b><a name=\"tip\"><font color=\"SysCaptionTextExtra\"></font></a></b>";
            rbnMenuBar.TitleText = t;

            lblLoginCompany.Text = ClsCommonSettings.CurrentCompany;
            lblCurrency.Text = ClsCommonSettings.Currency;
            lblUser.Text = "USER : " + ClsCommonSettings.strUserName;
            lblEmployee.Text = ClsCommonSettings.strEmployeeName;
        }

        private void frmMain_Load(object sender, System.EventArgs e)
        {
            Application.CurrentCulture = new CultureInfo("en-US");
            ClsCommonSettings.IsArabicViewEnabled = false;
            ClsCommonSettings.IsArabicView = false; 

            SetInitials();

            FrmLogin objFrmLogin = new FrmLogin();
            objFrmLogin.ShowDialog();
            objFrmLogin = null;            

            if (ClsMainSettings.blnLoginStatus)
            {
                SetArabicControls();
                
                SetPermissions();

                SetEmiratesCardType();

                LoadMessage();

                EnableFileItems();

                AppCommandNew.Execute();

                DisplayMyAlerts(true);

                StatusCheck();

                LoadInitialRssdetails();

                tmRssmain.Enabled = true;
                tmrAlert.Enabled = true;
                
                //CheckBackupStatus();
            }
            else
            {
                Application.Exit();
            }
        }

        private void SetInitials()
        {
            //----------------------------------------------------------------------------------
            //FrmSplash objSplash = new FrmSplash();
            //objSplash.ShowDialog();
            //objSplash = null;

            //ProjectSettings.clsRegistry objRegistry = new ProjectSettings.clsRegistry();
            //string strTemp = "";
            //objRegistry.ReadFromRegistry("SOFTWARE\\Epeopleserver", "Epeopleservername", out strTemp);
            //ClsCommonSettings.ServerName = strTemp;

            //objRegistry.ReadFromRegistry("SOFTWARE\\Epeopleserver", "Epeopleserverpath", out strTemp);
            //ClsCommonSettings.strServerPath = strTemp;

            ClsCommonSettings.ServerName = "ESCUBE_2\\SQLEXPRESS";
            ClsCommonSettings.DbName = "GoldenAnchor";
            MsDb.ConnectionString = "Data Source=" + ClsCommonSettings.ServerName + ";Initial Catalog=" + ClsCommonSettings.DbName + ";uid=msadmin;pwd=msSoft!234;Connect Timeout=0;";

            //---------------------------------------------------------------------------------

            //ClsCommonSettings.ServerName = "TECH-LAP\\SQL2012";
            //ClsCommonSettings.strServerPath = @"D:\TestApps\TestH\Regal\Docs";
            //ClsCommonSettings.DbName = "Regal";
            //MsDb.ConnectionString = "Data Source=" + ClsCommonSettings.ServerName + ";Initial Catalog=" + ClsCommonSettings.DbName + ";uid=sa;pwd=admin@2015;Connect Timeout=0;";


            ClsCommonSettings.TimerInterval = 1000;
        }

        public void SetEmiratesCardType()
        {
            EmiratesCard.Text = ClsCommonSettings.NationalityCard;
            EmiratesCard.Tooltip = ClsCommonSettings.NationalityCard;
        }

        private void SetArabicControls()
        {
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                rbIncentive.Visible =btnRptSalesInvoice.Visible= false;
                
                HomeBar.Text = "عام";
                ribbonBar1.Text = "شركة";
                ribbonBar2.Text = "السياسات";
                ribbonBar3.Text = "عامل";
                rbLocation.Text = "ترك";
                rbAttendance.Text = "الحضور";
                ribbonBarPayroll.Text = "تجهيز كشوف المرتبات";
                rbLeave.Text = "عملية";
                ribbonBar13.Text = "وثائق الشركة";
                ribbonBar8.Text = "المستندات موظف";
                ribbonBar9.Text = "بطاقات موظف";
                ribbonBar10.Text = "سجل الأسهم";
                ribbonBar4.Text = "ترك";
                ribbonBar11.Text = "الإيصالات والمدفوعات";
                ribbonBar12.Text = "نقل";
                ribbonBarEmployee.Text = "تقارير الرواتب";
                ribbonBar14.Text = "وثائق";
                ribbonBar15.Text = "المهام";
                ribbonBar6.Text = "ترتيب";
                ribbonBar7.Text = "المستخدم";
                ribbonBar5.Text = "إعدادات";
                ribbonBar16.Text = "مساعدة";
                bar1.Text = "التنبيهات";
                btnViewAlerts.Text = "عرض التنبيهات";
                DciAlerts.Text = "التنبيهات";
                DciUpdates.Text = "تحديثات المنتج";
                buttonChangeStyle.Text = "أسلوب";
                buttonStyleOffice2007Blue.Text = "مكتب الأزرق 2007";
                buttonStyleOffice2007Black.Text = "مكتب 2007 أسود";
                buttonStyleOffice2007Silver.Text = "أوفيس 2007 فضة";
                buttonItem60.Text = "ميندسوفت";
                buttonStyleCustom.Text = "مخطط مخصص";
                btnHome.Text = "منزل";
                mbtnCompany.Text = "شركة";
                mbtnEmployee.Text = "عامل";
                mbtnSalaryStructure.Text = "هيكل المرتبات";
                mbtnEmployeePayment.Text = "موظف الدفع";
                btnLogout1.Text = "تسجيل الخروج";
                buttonItem12.Text = "خروج راتبي صديق";
                mbtnChangeCompany.Text = "تغيير الشركة";
                btnGLCodeMapping.Text = "GL رسم الخرائط مدونة";
                btnRptEmployeeHistory.Text = "عامل تاريخ";
                rbIncentive.Text = "حافز";
                DataTable datTemp = objAlerts.FillCombos(new string[] { "ModuleID,ModuleNameArb", "ModuleReference", "" });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        General.Text = datTemp.Rows[8]["ModuleNameArb"].ToString(); // Home
                        Company.Text = datTemp.Rows[0]["ModuleNameArb"].ToString(); // Masters
                        Attendance.Text = datTemp.Rows[1]["ModuleNameArb"].ToString(); // Employee
                        Payroll.Text = datTemp.Rows[2]["ModuleNameArb"].ToString(); // Payroll
                        ribbonTabItem4.Text = datTemp.Rows[5]["ModuleNameArb"].ToString(); // Documents
                        ribbonTabItem5.Text = datTemp.Rows[6]["ModuleNameArb"].ToString(); // Task

                        ribbonTabItem2.Text = datTemp.Rows[4]["ModuleNameArb"].ToString(); // Report
                        ribbonTabItem6.Text = datTemp.Rows[4]["ModuleNameArb"].ToString(); // Report

                        ribbonTabItem3.Text = datTemp.Rows[3]["ModuleNameArb"].ToString(); // Settings
                        ribbonTabItem1.Text = "مساعدة"; // Help
                    }
                }

                datTemp = null;
                datTemp = objAlerts.FillCombos(new string[] { "MenuID,MenuNameArb", "MenuMaster", "" });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        // Home
                        btnEasyView.Text = datTemp.Rows[87]["MenuNameArb"].ToString();
                        btnCompany.Text = datTemp.Rows[0]["MenuNameArb"].ToString();
                        btnEmp.Text = datTemp.Rows[19]["MenuNameArb"].ToString();
                        btnSalary.Text = datTemp.Rows[21]["MenuNameArb"].ToString();
                        buttonItem4.Text = datTemp.Rows[62]["MenuNameArb"].ToString();
                        
                        // Masters
                        btnNewCompany.Text = datTemp.Rows[0]["MenuNameArb"].ToString();
                        btnCurrency.Text = datTemp.Rows[8]["MenuNameArb"].ToString();
                        btnBank.Text = datTemp.Rows[1]["MenuNameArb"].ToString();
                        btnWorkLocation.Text = datTemp.Rows[7]["MenuNameArb"].ToString();
                        btnAssets.Text = datTemp.Rows[13]["MenuNameArb"].ToString();
                        btnShift.Text = datTemp.Rows[4]["MenuNameArb"].ToString();
                        btnWorkPolicy.Text = datTemp.Rows[3]["MenuNameArb"].ToString();
                        btnLeavePolicy.Text = datTemp.Rows[5]["MenuNameArb"].ToString();
                        btnCalendar.Text = datTemp.Rows[6]["MenuNameArb"].ToString();
                        btnVacationPolicy.Text = datTemp.Rows[14]["MenuNameArb"].ToString();
                        btnSettlementPolicy.Text = datTemp.Rows[15]["MenuNameArb"].ToString();
                        btnUnearnedPolicy.Text = datTemp.Rows[18]["MenuNameArb"].ToString();

                        // Employee
                        btnEmployee.Text = datTemp.Rows[19]["MenuNameArb"].ToString();
                        btnSalaryStucture.Text = datTemp.Rows[21]["MenuNameArb"].ToString();
                        btnUpdateSalaryStructure.Text = datTemp.Rows[144]["MenuNameArb"].ToString();
                        btnEmployeeLoan.Text = datTemp.Rows[23]["MenuNameArb"].ToString();
                        btnLeaveStructure.Text = datTemp.Rows[20]["MenuNameArb"].ToString();
                        btnLeaveOpening.Text = datTemp.Rows[22]["MenuNameArb"].ToString();

                        // Payroll
                        btnAttendance.Text = datTemp.Rows[24]["MenuNameArb"].ToString();
                        btnProcess.Text = datTemp.Rows[25]["MenuNameArb"].ToString();
                        btnRelease.Text = datTemp.Rows[26]["MenuNameArb"].ToString();
                        btnVacationProcess.Text = datTemp.Rows[29]["MenuNameArb"].ToString();
                        btnSettlementProcess.Text = datTemp.Rows[28]["MenuNameArb"].ToString();

                        // Documents
                        btnTradeLicense.Text = datTemp.Rows[70]["MenuNameArb"].ToString();
                        btnLeaseAgreement.Text = datTemp.Rows[71]["MenuNameArb"].ToString();
                        btnInsurance.Text = datTemp.Rows[72]["MenuNameArb"].ToString();
                        Passport.Text = datTemp.Rows[63]["MenuNameArb"].ToString();
                        btnVisa.Text = datTemp.Rows[58]["MenuNameArb"].ToString();
                        DrivingLicense.Text = datTemp.Rows[64]["MenuNameArb"].ToString();
                        Qualification.Text = datTemp.Rows[57]["MenuNameArb"].ToString();
                        EmiratesCard.Text = datTemp.Rows[67]["MenuNameArb"].ToString();
                        InsuranceCard.Text = datTemp.Rows[68]["MenuNameArb"].ToString();
                        LabourCard.Text = datTemp.Rows[66]["MenuNameArb"].ToString();
                        HealthCard.Text = datTemp.Rows[65]["MenuNameArb"].ToString();
                        OtherDocuments.Text = datTemp.Rows[60]["MenuNameArb"].ToString();
                        StockRegister.Text = datTemp.Rows[62]["MenuNameArb"].ToString();
                        btnDocProcessTime.Text = datTemp.Rows[69]["MenuNameArb"].ToString();

                        // Task
                        btnLeaveEntry.Text = datTemp.Rows[75]["MenuNameArb"].ToString();
                        btnLeaveExtension.Text = datTemp.Rows[74]["MenuNameArb"].ToString();
                        BtnShiftSchedule.Text = datTemp.Rows[79]["MenuNameArb"].ToString();
                        btnSalaryAdvance.Text = datTemp.Rows[76]["MenuNameArb"].ToString();
                        btnExpense.Text = datTemp.Rows[80]["MenuNameArb"].ToString();
                        btnLoanRepayment.Text = datTemp.Rows[82]["MenuNameArb"].ToString();
                        btnDeposit.Text = datTemp.Rows[81]["MenuNameArb"].ToString();
                        btnAssetHandover.Text = datTemp.Rows[78]["MenuNameArb"].ToString();
                        btnEmployeeTransfer.Text = datTemp.Rows[77]["MenuNameArb"].ToString();

                        // Reports
                        btnEmployeeReport.Text = datTemp.Rows[46]["MenuNameArb"].ToString();
                        btnRptSalaryStructureReport.Text = datTemp.Rows[39]["MenuNameArb"].ToString();
                        btnRptAttendance.Text = datTemp.Rows[40]["MenuNameArb"].ToString();
                        btnPaymentsReport.Text = datTemp.Rows[41]["MenuNameArb"].ToString();
                        btnPaySlip.Text = datTemp.Rows[43]["MenuNameArb"].ToString();
                        btnRptEmployeeDeduction.Text = datTemp.Rows[44]["MenuNameArb"].ToString();
                        btnRptVacation.Text = datTemp.Rows[50]["MenuNameArb"].ToString();
                        btnSettlementReport.Text = datTemp.Rows[51]["MenuNameArb"].ToString();
                        btnRptLabourCost.Text = datTemp.Rows[49]["MenuNameArb"].ToString();
                        btnRptCTCReport.Text = datTemp.Rows[53]["MenuNameArb"].ToString();
                        Document.Text = datTemp.Rows[47]["MenuNameArb"].ToString();
                        btnAlert.Text = datTemp.Rows[54]["MenuNameArb"].ToString();
                        btnAssetHand.Text = datTemp.Rows[48]["MenuNameArb"].ToString();
                        btnExpenseReport.Text = datTemp.Rows[52]["MenuNameArb"].ToString();
                        btnSalaryAdvanceReport.Text = datTemp.Rows[122]["MenuNameArb"].ToString();
                        btnLeaveSummaryReport.Text = datTemp.Rows[42]["MenuNameArb"].ToString();
                        btnRptHoliday.Text = datTemp.Rows[55]["MenuNameArb"].ToString();
                        Transfer.Text = datTemp.Rows[56]["MenuNameArb"].ToString();
                        btnAccrualReport.Text = datTemp.Rows[147]["MenuNameArb"].ToString();
                        //btnRptSalesInvoice.Text = datTemp.Rows[154]["MenuNameArb"].ToString();

                        // Settings
                        btnConfig.Text = datTemp.Rows[33]["MenuNameArb"].ToString();
                        btnMailSettings.Text = datTemp.Rows[34]["MenuNameArb"].ToString();
                        btnUser.Text = datTemp.Rows[36]["MenuNameArb"].ToString();
                        btnUserRoles.Text = datTemp.Rows[35]["MenuNameArb"].ToString();
                        btnChangePassword.Text = datTemp.Rows[32]["MenuNameArb"].ToString();
                        btnBackup.Text = datTemp.Rows[37]["MenuNameArb"].ToString();
                        btnWizard.Text = datTemp.Rows[38]["MenuNameArb"].ToString();


                        btnDutyRoaster.Text = datTemp.Rows[138]["MenuNameArb"].ToString();
                        btnAttendanceWorkSheet.Text = datTemp.Rows[139]["MenuNameArb"].ToString();
                        btnRptAttendanceLive.Text = datTemp.Rows[140]["MenuNameArb"].ToString();
                        btnRptWorkSheet.Text = datTemp.Rows[141]["MenuNameArb"].ToString();
                        btnOffDay.Text = datTemp.Rows[142]["MenuNameArb"].ToString();
                        btnPaymentsProcessedReport.Text = datTemp.Rows[143]["MenuNameArb"].ToString();
                        btnCompanySettings.Text = datTemp.Rows[145]["MenuNameArb"].ToString();
 


                        // Help
                        About.Text = "حول";
                        btnHlpContents.Text = "محتويات";
                        btnSupport.Text = "دعم";
                    }
                }
            }
        }

        private void btnViewAlerts_Click(object sender, EventArgs e)
        {
            ShowAlertReport();
            IsCheckAlert = true;
        }

        private void tmrAlert_Tick(object sender, EventArgs e)
        {
            tmrAlert.Enabled = false;

            if (!bgwAlerts.IsBusy)
                bgwAlerts.RunWorkerAsync();
        }

        private void bgwAlerts_DoWork(object sender, DoWorkEventArgs e)
        {
            DisplayMyAlerts(false);
        }

        private void bgwAlerts_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            tmrAlert.Enabled = true;
        }

        private void DisplayMyAlerts(bool IsFirst)
        {
            try
            {
                if (IsFirst)
                {
                    iMyAlertCount = objAlerts.GetUserAlertCount();
                    FillAlerts(objAlerts.GetUserAlerts());
                    if (ClsCommonSettings.IsArabicView)
                        AddAlertCount("التنبيهات(" + iMyAlertCount.ToString() + ")");
                    else
                        AddAlertCount("Alerts(" + iMyAlertCount.ToString() + ")");
                }
                else
                {
                    int iCount = objAlerts.GetUserAlertCount();
                    if (iCount != iMyAlertCount)
                    {
                        FillAlerts(objAlerts.GetUserAlerts());
                        iMyAlertCount = iCount;
                        if (ClsCommonSettings.IsArabicView)
                            AddAlertCount("التنبيهات(" + iMyAlertCount.ToString() + ")");
                        else
                            AddAlertCount("Alerts(" + iMyAlertCount.ToString() + ")");
                        System.Media.SystemSounds.Asterisk.Play();
                        IsCheckAlert = false;
                    }
                }
            }
            catch { }
            
        }

        public delegate void AddnewTextDelegate(string str);
        public void AddnewText(string str)
        {
            if (this.lblData.InvokeRequired)
            {
                this.Invoke(new AddnewTextDelegate(AddnewText), str);
            }
            else
            {
                this.lblData.Text =  str;
                this.lblData.Refresh();
            }
        }

        public delegate void AddAlertCountDelegate(string str);
        public void AddAlertCount(string str)
        {
            if (this.DciAlerts.InvokeRequired)
            {
                this.Invoke(new AddAlertCountDelegate(AddAlertCount), str);
            }
            else
            {
                this.DciAlerts.Text = str;
                this.DciAlerts.Refresh();
            }
        }

        private void FillAlerts(DataTable dtAlerts)
        {
            if (dtAlerts.Rows.Count > 0)
            {
                reflectionImage1.Visible = false;
                StringBuilder oString = new StringBuilder();
                for (int i = 0; i < dtAlerts.Rows.Count; i++)
                {
                    string sDes = Convert.ToString(dtAlerts.Rows[i][1]).Remove(0, Convert.ToString(dtAlerts.Rows[i][1]).IndexOf(" "));
                    if (sDes.IndexOf("(s)") > 15 && sDes.Length > 41)
                    {
                        string sFirst = sDes.Substring(0, 15) + "...";
                        sDes = sFirst + sDes.Remove(0, sDes.IndexOf("(s)") + 3);
                    }
                    if (sDes.Length > 41)
                        sDes = sDes.Substring(0, 41) + "...";
                    if (Convert.ToString(dtAlerts.Rows[i][2]) == "Y")
                        oString.Append("<br><font size='+4' color='black'><b>" + Convert.ToString(dtAlerts.Rows[i][1]).Substring(0, Convert.ToString(dtAlerts.Rows[i][1]).IndexOf(" ")) + "</b></font><font color='red'>" + sDes + "</font></br>");
                    else
                        oString.Append("<br><font size='+4' color='green'><b>" + Convert.ToString(dtAlerts.Rows[i][1]).Substring(0, Convert.ToString(dtAlerts.Rows[i][1]).IndexOf(" ")) + "</b></font>" + sDes + "</br>");
                }
                AddnewText(oString.ToString());
            }
            else
            {
                AddnewText("");
                
            }
        }

        private void StatusCheck()
        {
            try
            {
                //labelStatus.Text = "You are using a UAT version of EPeople[ES3Tech Technologies (I) Pvt Ltd]";
                //ClsMainSettings.strProductSerial = ClsMainSettings.strProductSerial == null ? "0" : ClsMainSettings.strProductSerial;
                //clsFiles objFiles = new clsFiles();
                //if (!objFiles.Checksysfile(Application.StartupPath))
                //{
                //    labelStatus.Text = "You are using a cracked software of ES3Tech Technologies (I) Pvt Ltd.";
                //}
            }
            catch (Exception)
            {
                labelStatus.Text = "You are using a cracked software of ES3 Technovations LLP.";
            }
        }

        private void tmRssmain_Tick(object sender, EventArgs e)
        {
            tmRssmain.Stop();
            tmRssmain.Enabled = false;
            tmRssmain.Dispose();

            BackRssWorker.RunWorkerAsync(1);
        }

        private bool LoadInitialRssdetails()
        {
            WebRss.DocumentText = new clsRss().GetRssFeedInitial();
            return true;
        }

        private bool LoadRssfromWeb()
        {
            try
            {
                ClsInternet objINet = new ClsInternet();
                if (objINet.IsInternetConnected())
                {
                    string sRssUrlLive = "http://msg.escubetech.com/rssimages/Payprdupdate.jpg";
                    if (objINet.IsUrlAvailable(sRssUrlLive))
                    {
                        string sRssFedd = new clsRss().GetRssFeedLive();
                        if (sRssFedd != "")
                        {
                            WebRss.DocumentText = sRssFedd;
                            string sRssLocalfile = Application.StartupPath + "\\rss\\rssimages\\Payprdupdate.jpg";
                            System.Net.WebClient wbclient = new System.Net.WebClient();
                            wbclient.DownloadFileAsync(new Uri(sRssUrlLive), sRssLocalfile);
                        }
                    }
                }
                

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void tmCheckStatus_Tick(object sender, EventArgs e)
        {
            if (!BackRssWorker.IsBusy)
                BackRssWorker.RunWorkerAsync(0);
        }

        private void CheckProductStatus()
        {
            try
            {
                if (ClsMainSettings.blnProductStatus)
                {
                    string dt = "30-Dec-2021";
                    if (ClsMainSettings.strProductSerial == "" || ClsMainSettings.strProductSerial == null)
                        dt = "01-Jan-2021";

                    clsFiles objSet = new clsFiles();
                    ClsMainSettings.blnProductStatus = objSet.CheckProductMaxUsage(dt);
                }
            }
            catch (Exception)
            {

            }
        }

        void wbclient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //BackRssWorker.Dispose();
        }
        private void bgwMSI_DoWork(object sender, DoWorkEventArgs e)
        {
            //MsiPorts.SetPorts();
        }
        private void BackRssWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //if (e.Argument.ToInt32() == 1)
            //{
            //    if (ClsMainSettings.blnProductStatus)
            //        new clsBulksetup().ProductdbStatus();

            //    //new clsBulksetup().UpdateInfo();

            //    //new clsConfig().AlertStatusUpdation(ClsMainSettings.strProductSerial);
            //}
            CheckProductStatus();
        }

        private void ribbonControl1_TitleTextMarkupLinkClick(object sender, DevComponents.DotNetBar.MarkupLinkClickEventArgs e)
        {
            //MessageBoxEx.Show("TitleText property on Ribbon fully supports our text-markup as demonstrated here. However, try keeping things simpler in title bar, perhaps using hyperlinks is pushing it too far ;-) <br/><br/>See <b>UpdateTitle()</b> method in source for details.", "RibbonControl.TitleText property tip");
        }

        #region Commands Implementation
        private void BindDocumentCommands()
        {
            Navigator document = this.ActiveMdiChild as Navigator;
            if (document == null)
            {
                // Note that when Command is set to null the button will be automatically 
                // disabled if it had command associated previously
                //buttonCopy.Command = null;
                //buttonCut.Command = null;
                //buttonPaste.Command = null;
                //buttonUndo.Command = null;
                //buttonAlignCenter.Command = null;
                //buttonAlignLeft.Command = null;
                //buttonAlignRight.Command = null;
                //buttonFind.Command = null;
                //comboFont.Command = null;
                //buttonFontBold.Command = null;
                //buttonFontItalic.Command = null;
                //comboFontSize.Command = null;
                //buttonFontStrike.Command = null;
                //buttonFontUnderline.Command = null;
                //buttonTextColor.Command = null;
                //zoomSlider.Command = null;
                //labelStatus.Command = null;
                //bCopy.Command = null;
                //bCut.Command = null;
                //bPaste.Command = null;
            }
            else
            {
                //buttonCopy.Command = document.CommandCopy;
                //bCopy.Command = document.CommandCopy;
                //buttonCut.Command = document.CommandCut;
                //bCut.Command = document.CommandCut;
                //buttonPaste.Command = document.CommandPaste;
                //bPaste.Command = document.CommandPaste;
                //buttonUndo.Command = document.CommandUndo;
                //buttonAlignCenter.Command = document.CommandAlignCenter;
                //buttonAlignLeft.Command = document.CommandAlignLeft;
                //buttonAlignRight.Command = document.CommandAlignRight;
                //buttonFind.Command = document.CommandFind;
                //comboFont.Command = document.CommandFont;
                //buttonFontBold.Command = document.CommandFontBold;
                //buttonFontItalic.Command = document.CommandFontItalic;
                //comboFontSize.Command = document.CommandFontSize;
                //buttonFontStrike.Command = document.CommandFontStrike;
                //buttonFontUnderline.Command = document.CommandFontUnderline;
                //buttonTextColor.Command = document.CommandTextColor;
                //zoomSlider.Command = document.CommandZoom;
                //labelStatus.Command = document.CommandStatus;
            }
        }

        private void AppCommandNew_Executed(object sender, EventArgs e)
        {
            ShowEasyview();
        }

        private void ShowEasyview()
        {

            NavigatorArb docArb = null;
            Navigator doc = null;
            try
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    docArb = new NavigatorArb();
                    docArb.Text = "من السهل مشاهدة" + (this.MdiChildren.Length + 1);
                    docArb.MdiParent = this;
                    docArb.WindowState = FormWindowState.Maximized;
                    docArb.Show();
                    docArb.Update();
                }
                else
                {
                    doc = new Navigator();
                    doc.Text = " Easy View " + (this.MdiChildren.Length + 1);
                    doc.MdiParent = this;
                    doc.WindowState = FormWindowState.Maximized;
                    //doc.GCompanyID = ClsCommonSettings.CompanyID;
                    doc.Show();
                    doc.Update();
                }
            }
            catch (OutOfMemoryException)
            {
                doc.Dispose();
                docArb.Dispose();
                System.GC.Collect();
                ShowEasyview();
            }
        }

        private void AppCommandSave_Executed(object sender, EventArgs e)
        {
            if (ClsCommonSettings.IsArabicView)
            {
                NavigatorArb docArb = this.ActiveMdiChild as NavigatorArb;
                if (docArb == null)
                    return;
            }
            else
            {
                Navigator doc = this.ActiveMdiChild as Navigator;
                if (doc == null)
                    return;
            }

        }

        private void AppCommandTheme_Executed(object sender, EventArgs e)
        {
            ICommandSource source = sender as ICommandSource;
            if (source.CommandParameter is string)
            {
                eOffice2007ColorScheme colorScheme = (eOffice2007ColorScheme)Enum.Parse(typeof(eOffice2007ColorScheme), source.CommandParameter.ToString());
                // This is all that is needed to change the color table for all controls on the form
                rbnMenuBar.Office2007ColorTable = colorScheme;
            }
            else if (source.CommandParameter is Color)
            {
                RibbonPredefinedColorSchemes.ChangeOffice2007ColorTable(this, m_BaseColorScheme, (Color)source.CommandParameter);
            }
            this.Invalidate();
        }

        private void AppCommandExit_Executed(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
                


        #region PayrollModuleEvents

        private bool SetPermissions()
        {
            btnGLCodeMapping.Visible = ClsCommonSettings.ThirdPartyIntegration;

            clsBLLPermissionSettings ObjPermission = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                DataTable dt = ObjPermission.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID);
                if (dt.Rows.Count == 0)
                {
                    //-----------------------------------------------------------Documents Module----------------------------
                    Passport.Enabled = btnVisa.Enabled = DrivingLicense.Enabled = Qualification.Enabled = StockRegister.Enabled = EmiratesCard.Enabled = HealthCard.Enabled = InsuranceCard.Enabled = LabourCard.Enabled = OtherDocuments.Enabled = btnDocProcessTime.Enabled = false;
                    //Company Documents
                    btnTradeLicense.Enabled = btnLeaseAgreement.Enabled = btnInsurance.Enabled = false;
                    //-----------------------------------------------------------****************----------------------------
                    btnWizard.Enabled = btnAttendance.Enabled = btnLeaveOpening.Enabled = btnSalaryStucture.Enabled = btnEasyView.Enabled = btnCompany.Enabled = btnEmp.Enabled = btnSalary.Enabled = btnAssets.Enabled = false;
                    btnNewCompany.Enabled = btnCurrency.Enabled = btnBank.Enabled = btnShift.Enabled = btnWorkPolicy.Enabled = btnLeavePolicy.Enabled = btnCalendar.Enabled = btnEmployee.Enabled = false;
                    btnVacationPolicy.Enabled = btnSettlementPolicy.Enabled = btnWorkLocation.Enabled = buttonItem4.Enabled = false;
                    btnProcess.Enabled = btnRelease.Enabled = btnSettlementProcess.Enabled = btnVacationProcess.Enabled = btnEmployeeLoan.Enabled = btnUnearnedPolicy.Enabled = false;
                    mbtnCompany.Enabled = mbtnEmployee.Enabled = mbtnSalaryStructure.Enabled = mbtnEmployeePayment.Enabled = false;
                    btnLeaveStructure.Enabled = false;
                    // btnUserRoles.Enabled = false;
                    btnAssetHand.Enabled = btnConfig.Enabled = btnMailSettings.Enabled = btnUser.Enabled = btnChangePassword.Enabled = btnBackup.Enabled = btnUserRoles.Enabled = false;
                    btnCompanySettings.Enabled = btnUpdateSalaryStructure.Enabled = btnDutyRoaster.Enabled= btnRptWorkSheet.Enabled=btnAttendanceWorkSheet.Enabled=btnOffDay.Enabled= btnPaymentsProcessedReport.Enabled = btnRptAttendanceLive.Enabled =  Document.Enabled = btnEmployeeReport.Enabled = btnRptSalaryStructureReport.Enabled = btnRptAttendance.Enabled = btnPaymentsReport.Enabled = btnPaySlip.Enabled = false;
                    btnLeaveSummaryReport.Enabled = btnSalaryAdvanceReport.Enabled = btnRptEmployeeDeduction.Enabled = Document.Enabled = btnRptLabourCost.Enabled = btnExpenseReport.Enabled = btnSettlementReport.Enabled = btnRptVacation.Enabled = btnAlert.Enabled = btnRptCTCReport.Enabled = btnRptHoliday.Enabled = Transfer.Enabled = false;

                    // Task Module
                    btnLeaveEntry.Enabled = btnLeaveExtension.Enabled = BtnShiftSchedule.Enabled = false;
                    btnDeposit.Enabled = btnSalaryAdvance.Enabled = btnExpense.Enabled = btnLoanRepayment.Enabled = false;
                    btnAssetHandover.Enabled = btnEmployeeTransfer.Enabled = false;
                    btnProfitCenterTarget.Enabled = btnCommissionStructure.Enabled = btnSalesInvoice.Enabled = false;
                    // Settings Module
                    btnGLCodeMapping.Enabled = false;
                    return false;
                }
                else
                {

                    //-----------------------------------------------------------Documents Module----------------------------

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Passport).ToString(); // Passport
                    Passport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Visa).ToString(); // visa
                    btnVisa.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DrivingLicense).ToString(); // Driving license
                    DrivingLicense.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Qualification).ToString(); // Qualification
                    Qualification.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DocumentRegister).ToString();
                    StockRegister.Enabled = buttonItem4.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.NationalIDCard).ToString(); // Emirates Card
                    EmiratesCard.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.HealthCard).ToString(); //Health card
                    HealthCard.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.InsuranceCard).ToString(); // Insurance Card
                    InsuranceCard.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LabourCard).ToString(); // labour card
                    LabourCard.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.OtherDocuments).ToString(); // Other documents
                    OtherDocuments.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ProcessingTime).ToString(); // Processing time
                    btnDocProcessTime.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    // Company Documents
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.TradeLicense).ToString(); // Trade license
                    btnTradeLicense.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaseAgreement).ToString(); // Lease agreement
                    btnLeaseAgreement.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Insurance).ToString(); // Insurance details
                    btnInsurance.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //-----------------------------------------------------------****************----------------------------

                    //-----------------------------------------------------------Masters

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.NewCompany).ToString();
                    btnNewCompany.Enabled = btnCompany.Enabled = mbtnCompany.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.VacationPolicy).ToString();
                    btnVacationPolicy.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);


                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SettlementPolicy).ToString();
                    btnSettlementPolicy.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Currency).ToString();
                    btnCurrency.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Bank).ToString();
                    btnBank.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ShiftPolicy).ToString();
                    btnShift.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.WorkPolicy).ToString();
                    btnWorkPolicy.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeavePolicy).ToString();
                    btnLeavePolicy.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);


                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaveExtension).ToString();
                    btnLeaveExtension.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.HolidayCalender).ToString();
                    btnCalendar.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Employee).ToString();
                    btnEmployee.Enabled = btnEmp.Enabled = mbtnEmployee.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.WorkLocation).ToString();
                    btnWorkLocation.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.CompanyAssets).ToString();
                    btnAssets.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.UnearnedPolicy).ToString();
                    btnUnearnedPolicy.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    //-------------------------------------------------------------------Employee (MenuID = 2)

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.VacationProcess).ToString();
                    btnVacationProcess.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SettlementProcess).ToString();
                    btnSettlementProcess.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Loan).ToString();
                    btnEmployeeLoan.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);



                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DocumentReport).ToString();
                    Document.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);



                    //----------------------------------------------------------------------
                    //-----------------------------------------------------------Attendance
                    //dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AttendanceDeviceSettings).ToString();
                    //btnDeviceSettings.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AttendanceMapping).ToString();
                    //BtnAttnMapping.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Attendance).ToString();
                    btnAttendance.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LocationSchedule).ToString();
                    //btnLocationSchedule.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaveOpening).ToString();
                    btnLeaveOpening.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaveStructure).ToString();
                    btnLeaveStructure.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //-----------------------------------------------------------Payroll
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalaryStructure).ToString();
                    btnSalaryStucture.Enabled = btnSalary.Enabled = mbtnSalaryStructure.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalaryProcess).ToString();
                    btnProcess.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalaryRelease).ToString();
                    btnRelease.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    // --------------------------------------------------------- Task
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaveEntry).ToString();
                    btnLeaveEntry.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaveExtension).ToString();
                    btnLeaveExtension.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ShiftSchedule).ToString();
                    BtnShiftSchedule.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Deposit).ToString(); // Deposit
                    btnDeposit.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalaryAdvance).ToString();
                    btnSalaryAdvance.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Expense).ToString(); // Expense
                    btnExpense.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LoanRepayment).ToString(); // Loan Repayment
                    btnLoanRepayment.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AssetHandover).ToString(); // Company Asset Operation (Asset HandOver)
                    btnAssetHandover.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.EmployeeTransfer).ToString(); // Employee Qualification
                    btnEmployeeTransfer.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ProfitCenterTarget).ToString(); // Profit Center Target
                    btnProfitCenterTarget.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.CommissionStructure).ToString(); // Commission Structure
                    btnCommissionStructure.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalesInvoice).ToString(); // Sales Invoice
                    btnSalesInvoice.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    //-----------------------------------------------------------Settings

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ConfigurationSetting).ToString();
                    btnConfig.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.EmailSettings).ToString();
                    btnMailSettings.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.User).ToString();
                    btnUser.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Role).ToString();
                    btnUserRoles.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ChangePassword).ToString();
                    btnChangePassword.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);


                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Backup).ToString();
                    btnBackup.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AttendanceWizard).ToString();
                    btnWizard.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ConfigurationSetting).ToString(); // has Persmission in Configuration
                    btnGLCodeMapping.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    
                    // ---------------------------------------------------------Reports


                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.EmployeeProfileReport).ToString(); // Employee Profile 
                    btnEmployeeReport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalaryStructureReport).ToString(); // salary structure and history
                    btnRptSalaryStructureReport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AttendanceReport).ToString(); // Attendance report
                    btnRptAttendance.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AttendanceReportLive).ToString(); // Attendance Live  report
                    btnRptAttendanceLive.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PaymentReportProcess).ToString(); // Payment report
                     btnPaymentsProcessedReport.Enabled = mbtnEmployeePayment.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PaymentReport).ToString(); // Payment report
                    btnPaymentsReport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);


                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaveSummaryReport).ToString(); // Leavesummary
                    btnLeaveSummaryReport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalaryAdvanceSummaryReport).ToString(); // AdvanceSummary
                    btnSalaryAdvanceReport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.PaySlip).ToString(); // Pay slip
                    btnPaySlip.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AlertReport).ToString(); // Alert Report
                    btnAlert.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.EmployeeDeduction).ToString(); // Employee Deduction
                    btnRptEmployeeDeduction.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DocumentReport).ToString(); // document report
                    Document.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AssetReport).ToString(); // asset report
                    btnAssetHand.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.VacationReport).ToString(); //vacation  Report 
                    btnRptVacation.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.ExpenseReport).ToString(); //Expense  Report 
                    btnExpenseReport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SettlementReport).ToString(); //settlement  Report 
                    btnSettlementReport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LabourCostReport).ToString(); // Labour Cost
                    btnRptLabourCost.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.CTCReport).ToString(); //CTC
                    btnRptCTCReport.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.HolidayReport).ToString(); // Holiday
                    btnRptHoliday.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.EmployeeTranferReport).ToString(); // Holiday
                    Transfer.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DutyRoast).ToString(); // Duty Roast
                    btnDutyRoaster.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.WorkSheetRpt).ToString(); // Work Sheet Rpt
                    btnRptWorkSheet.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.WorkSheet).ToString(); // Work Sheet
                    btnAttendanceWorkSheet.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.OffDayMarkReport).ToString(); // Off Day Mark Report
                    btnOffDay.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalaryStructureList).ToString(); // SalaryStructure List Update
                    btnUpdateSalaryStructure.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.CompanySettings).ToString(); // Company Settings
                    btnCompanySettings.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    


                    return true;


                }
            }
            else
            {
                Passport.Enabled = btnVisa.Enabled = DrivingLicense.Enabled = Qualification.Enabled = StockRegister.Enabled = EmiratesCard.Enabled = HealthCard.Enabled = InsuranceCard.Enabled = LabourCard.Enabled = OtherDocuments.Enabled = btnDocProcessTime.Enabled = true;
                //Company Documents
                btnTradeLicense.Enabled = btnLeaseAgreement.Enabled = btnInsurance.Enabled = true;
                //-----------------------------------------------------------****************----------------------------
                btnWizard.Enabled = btnAttendance.Enabled = btnLeaveOpening.Enabled = btnSalaryStucture.Enabled = btnEasyView.Enabled = btnCompany.Enabled = btnEmp.Enabled = btnSalary.Enabled = btnAssets.Enabled = true;
                btnNewCompany.Enabled = btnCurrency.Enabled = btnBank.Enabled = btnShift.Enabled = btnWorkPolicy.Enabled = btnLeavePolicy.Enabled = btnCalendar.Enabled = btnEmployee.Enabled = true;
                btnVacationPolicy.Enabled = btnSettlementPolicy.Enabled = btnWorkLocation.Enabled = buttonItem4.Enabled = true;
                btnProcess.Enabled = btnRelease.Enabled = btnSettlementProcess.Enabled = btnVacationProcess.Enabled = btnEmployeeLoan.Enabled = btnUnearnedPolicy.Enabled = true;
                mbtnCompany.Enabled = mbtnEmployee.Enabled = mbtnSalaryStructure.Enabled = mbtnEmployeePayment.Enabled = true;
                btnLeaveStructure.Enabled = true;
                // btnUserRoles.Enabled = false;
                btnAssetHand.Enabled = btnConfig.Enabled = btnMailSettings.Enabled = btnUser.Enabled = btnChangePassword.Enabled = btnBackup.Enabled = btnUserRoles.Enabled = true;
               btnCompanySettings.Enabled = btnUpdateSalaryStructure.Enabled = btnDutyRoaster.Enabled = btnRptWorkSheet.Enabled = btnAttendanceWorkSheet.Enabled = btnOffDay.Enabled = btnPaymentsProcessedReport.Enabled = btnRptAttendanceLive.Enabled = Document.Enabled = btnEmployeeReport.Enabled = btnRptSalaryStructureReport.Enabled = btnRptAttendance.Enabled = btnPaymentsReport.Enabled = btnPaySlip.Enabled = true;
                btnLeaveSummaryReport.Enabled = btnSalaryAdvanceReport.Enabled = btnRptEmployeeDeduction.Enabled = Document.Enabled = btnRptLabourCost.Enabled = btnExpenseReport.Enabled = btnSettlementReport.Enabled = btnRptVacation.Enabled = btnAlert.Enabled = btnRptCTCReport.Enabled = btnRptHoliday.Enabled = Transfer.Enabled = true;

                // Task Module
                btnLeaveEntry.Enabled = btnLeaveExtension.Enabled = BtnShiftSchedule.Enabled = true;
                btnDeposit.Enabled = btnSalaryAdvance.Enabled = btnExpense.Enabled = btnLoanRepayment.Enabled = true;
                btnAssetHandover.Enabled = btnEmployeeTransfer.Enabled = true;
                return true;
            }

        }

        private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            ShowEasyview();
        }

        private void btnNewCompany_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmCompany(), true);
        }

        private void btnCurrency_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmCurrencyReference(), true);
        }

        private void btnBank_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmBankDetails(0), true);
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmShiftPolicy(), true);
        }

        private void btnWorkPolicy_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmWorkPolicy(), true);
        }

        private void btnLeavePolicy_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmLeavePolicy(), true);
        }

        private void btnCalendar_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmHolidayCalender(), true);
        }

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmEmployee(), true);
        }

        //private void BtnAttnMapping_Click(object sender, EventArgs e)
        //{
        //    Program.objMain.ShowDialogForm(new FrmAttendanceMapping(), true);
        //}

        private void btnNewAttendance_Click(object sender, EventArgs e)
        {
            ShowAttendance();
        }

        private void btnSalaryStructure_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmSalaryStructure(), true);
        }

        private void btnUpdateSalaryStructure_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmSalayStructureList(), true);
        }

        
       

        private void ShowAttendance()
        {
            FrmAttendanceManual objAttendance = null;
            try
            {
                objAttendance = new FrmAttendanceManual();
                objAttendance.Text = "Attendance ";
                objAttendance.MdiParent = this;
                objAttendance.WindowState = FormWindowState.Maximized;
                objAttendance.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objAttendance.Dispose();
                System.GC.Collect();
                ShowAttendance();
            }
        }



        private void btnCompany_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmCompany(), true);
        }

        private void btnEmp_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmEmployee(), true);
        }

        private void btnSalary_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmSalaryStructure(), true);
        }

        private void AdditionDeduction_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmAdditionDeduction(), true);
        }

        private void btnSalaryAdvance_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmSalaryAdvance(), true);
        }

        private void btnDeviceSettings_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmAttendanceDeviceSettings(), true);
        }


        private void btnSalaryAdvance_Click_1(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmSalaryAdvance(), true);
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            FrmConfigurationSetting objConfigurationSetting = new FrmConfigurationSetting();
            objConfigurationSetting.ModuleID = 4;// (int)ModuleID.Company;
            objConfigurationSetting.Show();
        }

        private void btnMailSettings_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmEmailSettings(), true);
        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmUser(), true);
        }

        private void btnUserRoles_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new frmRole(), true);
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmChangePassword(), true);
        }

        public void btnSalaryStructureReport_Click(object sender, EventArgs e)
        {
            ShowSalStructureReport();

        }
        public void ShowSalStructureReport()
        {
            FrmRptSalaryStructure objFrmRptSalaryStructure = null;
            try
            {
                objFrmRptSalaryStructure = new FrmRptSalaryStructure();
                if (ClsCommonSettings.IsArabicView)
                    objFrmRptSalaryStructure.Text = "هيكل تقرير الراتب";
                else
                    objFrmRptSalaryStructure.Text = "Salary Structure Report";
                objFrmRptSalaryStructure.MdiParent = this;
                objFrmRptSalaryStructure.WindowState = FormWindowState.Maximized;
                objFrmRptSalaryStructure.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptSalaryStructure.Dispose();
                System.GC.Collect();
                ShowSalStructureReport();
            }
        }

        private void btnFileMnuAccounts_Click(object sender, EventArgs e)
        {

        }


        private void btnFileMnuTrading_Click(object sender, EventArgs e)
        {

        }


        #endregion

        private void btnEmployeeReport_Click(object sender, EventArgs e)
        {
            ShowEmployeeReport();
        }
        public void ShowEmployeeReport()
        {
            FrmRptEmployeeProfile objFrmRptEmployeeProfile = null;
            try
            {
                objFrmRptEmployeeProfile = new FrmRptEmployeeProfile();
                if (ClsCommonSettings.IsArabicView)
                    objFrmRptEmployeeProfile.Text = "الموظف الشخصي تقرير";
                else
                    objFrmRptEmployeeProfile.Text = "Employee Profile Report";
                objFrmRptEmployeeProfile.MdiParent = this;
                objFrmRptEmployeeProfile.WindowState = FormWindowState.Maximized;
                objFrmRptEmployeeProfile.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptEmployeeProfile.Dispose();
                System.GC.Collect();
                ShowEmployeeReport();
            }
        }


        private void btnBackup_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmBackup(), true);
        }

        public void ShowAttendanceReport()
        {
            FrmRptAttendanceManual objAttendance = null;
            try
            {
                objAttendance = new FrmRptAttendanceManual();
                if (ClsCommonSettings.IsArabicView)
                    objAttendance.Text = "تقرير الحضور";
                else
                    objAttendance.Text = "Attendance Report";
                objAttendance.MdiParent = this;
                objAttendance.WindowState = FormWindowState.Maximized;
                objAttendance.Show();
            }
            catch (OutOfMemoryException)
            {
                objAttendance.Dispose();
                System.GC.Collect();
                ShowAttendanceReport();
            }
        }

        private void btnRptAttendance_Click(object sender, EventArgs e)
        {
            ShowAttendanceReport();
        }

        private void btnPaymentsReport_Click(object sender, EventArgs e)
        {
            ShowPaymetsReport();

        }
         

        public void ShowPaymetsProcessedReport()
        {

            FrmRptUnreleased objFrmRptPayments = null;
            try
            {
                objFrmRptPayments = new FrmRptUnreleased();
                if (ClsCommonSettings.IsArabicView)
                    objFrmRptPayments.Text = "المدفوعات المجهزة تقرير";
                else
                    objFrmRptPayments.Text = "Payments Processed Report";
                objFrmRptPayments.MdiParent = this;
                objFrmRptPayments.WindowState = FormWindowState.Maximized;
                objFrmRptPayments.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptPayments.Dispose();
                System.GC.Collect();
                ShowPaymetsProcessedReport();
            }
        }


        public void ShowPaymetsReport()
        {

            FrmRptPayments objFrmRptPayments = null;
            try
            {
                objFrmRptPayments = new FrmRptPayments();
                if(ClsCommonSettings.IsArabicView)
                    objFrmRptPayments.Text = "تقرير الدفع";
                else
                    objFrmRptPayments.Text = "Payments Report";
                objFrmRptPayments.MdiParent = this;
                objFrmRptPayments.WindowState = FormWindowState.Maximized;
                objFrmRptPayments.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptPayments.Dispose();
                System.GC.Collect();
                ShowPaymetsReport();
            }
        }
      
        private void ShowLocationScheduleReport()
        {

            FrmRptLocationSchedule objFrmRptLocationSchedule = null;
            try
            {
                objFrmRptLocationSchedule = new FrmRptLocationSchedule();
                objFrmRptLocationSchedule.Text = "Location Schedule Report";
                objFrmRptLocationSchedule.MdiParent = this;
                objFrmRptLocationSchedule.WindowState = FormWindowState.Maximized;
                objFrmRptLocationSchedule.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptLocationSchedule.Dispose();
                System.GC.Collect();
                ShowPaymetsReport();
            }
        }
        private void btnLeaveSummaryReport_Click(object sender, EventArgs e)
        {
            ShowLeaveSummaryReport();
        }

        public void ShowLeaveSummaryReport()
        {

            FrmRptLeaveSummary objFrmRptLeaveSummary = null;
            try
            {
                objFrmRptLeaveSummary = new FrmRptLeaveSummary();
                if (ClsCommonSettings.IsArabicView)
                    objFrmRptLeaveSummary.Text = "ترك تقرير موجز";
                else
                    objFrmRptLeaveSummary.Text = "Leave Report";
                objFrmRptLeaveSummary.MdiParent = this;
                objFrmRptLeaveSummary.WindowState = FormWindowState.Maximized;
                objFrmRptLeaveSummary.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptLeaveSummary.Dispose();
                System.GC.Collect();
                ShowLeaveSummaryReport();
            }

        }

        private void btnPaySlip_Click(object sender, EventArgs e)
        {
            ShowSalarySlip();
        }
        public void ShowSalarySlip()
        {
            FrmSalarySlip objSalarySlip = null;

            try
            {
                objSalarySlip = new FrmSalarySlip();
                if (ClsCommonSettings.IsArabicView)
                    objSalarySlip.Text = "قسيمة الدفع";
                else
                    objSalarySlip.Text = "Pay Slip";
                objSalarySlip.MdiParent = this;
                objSalarySlip.WindowState = FormWindowState.Maximized;
                objSalarySlip.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objSalarySlip.Dispose();
                System.GC.Collect();
                ShowSalarySlip();
            }
        }

        private void btnEasyView_Click(object sender, EventArgs e)
        {
            ShowEasyview();
        }

        private void btnSalaryAdvanceReport_Click(object sender, EventArgs e)
        {

            FrmRptSalaryAdvance objSalaryAdvance = null;

            try
            {
                objSalaryAdvance = new FrmRptSalaryAdvance();
                if (ClsCommonSettings.IsArabicView)
                    objSalaryAdvance.Text = "تقرير الراتب موجز مسبق";
                else
                    objSalaryAdvance.Text = "Salary Advance & Loan Report";
                objSalaryAdvance.MdiParent = this;
                objSalaryAdvance.WindowState = FormWindowState.Maximized;
                objSalaryAdvance.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objSalaryAdvance.Dispose();
                System.GC.Collect();
            }

        }


        private void btnPaySlip_MouseHover(object sender, EventArgs e)
        {
            btnPaySlip.Style = StyleManager.GetEffectiveStyle();
        }

        private void bMPFmenu_Click(object sender, EventArgs e)
        {
            rbnMenuBar.Expanded = !rbnMenuBar.Expanded;

            if (rbnMenuBar.Expanded)
            {
                bMPFmenu.Tooltip = "Minimize Menu";
                bMPFmenu.Image = global::MyPayfriend.Properties.Resources.Minimize;
            }
            else
            {
                rbnMenuBar.Expanded = false;
                bMPFmenu.Tooltip = "Maximize Menu";
                bMPFmenu.Image = global::MyPayfriend.Properties.Resources.Maximize;
            }
        }
        private void btnShiftSchedule_Click(object sender, EventArgs e)
        {

            using (FrmShiftSchedule objShiftSchedule = new FrmShiftSchedule())
            {
                objShiftSchedule.ShowDialog();
            }

        }
        public void ShowDialogForm(Form f, bool disposeForm)
        {
            try
            {
                f.FormBorderStyle = FormBorderStyle.FixedSingle;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.MaximizeBox = false;
                f.ShowDialog();
            }
            finally
            {
                if (disposeForm)
                    f.Dispose();
            }
        }

        private void btnWorkLocation_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmWorkLocation(), true);
        }

        private void btnLeaveEntry_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmLeaveEntry(), true);
        }


        private void btnLeaveStructure_Click(object sender, EventArgs e)
        {
            ShowLeaveStructure();
        }
        private void ShowLeaveStructure()
        {
            FrmEmployeeLeaveStructure f = new FrmEmployeeLeaveStructure();
            try
            {

                f.FormBorderStyle = FormBorderStyle.FixedSingle;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.MaximizeBox = false;
                f.ShowDialog();
                if (f.PblnShowReport)
                {
                    ShowLeaveSummaryReport();
                }
            }
            finally
            {
                f.Dispose();
            }
        }
        private void btnLeaveExtension_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmLeaveExtension(), true);
        }

        private void btnRptEmployeeDeduction_Click(object sender, EventArgs e)
        {
            FrmRptEmployeeDeduction objEmployeeDeduction = null;

            try
            {
                objEmployeeDeduction = new FrmRptEmployeeDeduction();
                if (ClsCommonSettings.IsArabicView)
                    objEmployeeDeduction.Text = "خصم الموظف";
                else
                    objEmployeeDeduction.Text = "Employee Deduction";
                objEmployeeDeduction.MdiParent = this;
                objEmployeeDeduction.WindowState = FormWindowState.Maximized;
                objEmployeeDeduction.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objEmployeeDeduction.Dispose();
                System.GC.Collect();
                ShowSalarySlip();
            }
        }

        private void About_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new frmAbout(), true);
        }

        private void mbtnCompany_Click(object sender, EventArgs e)
        {
            btnCompany_Click(sender, e);
        }

        private void mbtnEmployee_Click(object sender, EventArgs e)
        {
            btnEmployee_Click(sender, e);
        }

        private void mbtnSalaryStructure_Click(object sender, EventArgs e)
        {
            btnSalaryStructure_Click(sender, e);
        }

        private void mbtnEmployeePayment_Click(object sender, EventArgs e)
        {
            btnPaymentsReport_Click(sender, e);
        }


        private void btnLogout1_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void btnRptLoactionSchedule_Click(object sender, EventArgs e)
        {
            ShowLocationScheduleReport();
        }

        private void ribbonTabItem4_Click(object sender, EventArgs e)
        {

        }

        private void btnVacationPolicy_Click(object sender, EventArgs e)
        {
            using (frmVacationPolicy objWizard = new frmVacationPolicy())
            {
                objWizard.ShowDialog();
            }
        }

        private void Passport_Click(object sender, EventArgs e)
        {
            new frmPassport().ShowDialog();
        }

        private void Visas_Click(object sender, EventArgs e)
        {
            using (frmVisa objfrmVisa = new frmVisa())
            {
                objfrmVisa.ShowDialog();
            }

        }

        private void btnAttendance_Click(object sender, EventArgs e)
        {
            FrmAttendance objFrmAttendance = null;
            try
            {
                objFrmAttendance = new FrmAttendance();
                if (ClsCommonSettings.IsArabicView)
                    objFrmAttendance.Text = "الحضور";
                else
                    objFrmAttendance.Text = "Attendance";
                objFrmAttendance.MdiParent = this;
                objFrmAttendance.objFrmMain = this; 
                objFrmAttendance.WindowState = FormWindowState.Maximized;
                objFrmAttendance.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmAttendance.Dispose();
                System.GC.Collect();
            }
        }

        private void btnNewAttendance_Click_1(object sender, EventArgs e)
        {
            FrmAttendanceManual objFrmAttendance = null;

            try
            {
                objFrmAttendance = new FrmAttendanceManual();
                objFrmAttendance.Text = "Attendance Manual";
                objFrmAttendance.MdiParent = this;
                objFrmAttendance.WindowState = FormWindowState.Maximized;
                objFrmAttendance.Show();
            }
            catch (OutOfMemoryException)
            {
                objFrmAttendance.Dispose();
                System.GC.Collect();
            }
        }

        private void btnAssets_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmCompanyAssets(), true);

        }

        private void btnEmployeeTransfer_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmEmployeeTransfer(), true);
        }

        private void btnSettlementPolicy_Click(object sender, EventArgs e)
        {
            using (FrmSettlementPolicy objPolicy = new FrmSettlementPolicy())
            {
                objPolicy.ShowDialog();
            }
        }

      

        private void btnWizard_Click(object sender, EventArgs e)
        {

            if (ClsCommonSettings.IsArabicView)
            {
                using (FrmAttendanceWizardArb objWizard = new FrmAttendanceWizardArb())
                {
                    objWizard.ShowDialog();
                }
            }
            else
            {
                using (FrmAttendancewizard objWizard = new FrmAttendancewizard())
                {
                    objWizard.ShowDialog();
                }
            }
        }

        private void DrivingLicense_Click(object sender, EventArgs e)
        {
            using (frmDrivingLicense objDrivingLicense = new frmDrivingLicense())
            {
                objDrivingLicense.ShowDialog();
            }
        }

        private void OtherDocuments_Click(object sender, EventArgs e)
        {
            using (frmDocumentMasterNew objDocument = new frmDocumentMasterNew())
            {
                objDocument.MainFlag = true;  
                objDocument.ShowDialog();
            }
        }

        private void btnEmployeeLoan_Click(object sender, EventArgs e)
        {
            using (FrmEmployeeLoan objFrmEmployeeLoan = new FrmEmployeeLoan())
            {
                objFrmEmployeeLoan.ShowDialog();
            }

        }


      

        private void Qualification_Click(object sender, EventArgs e)
        {
            using (EmployeeQualification objfrmEmployeeQualifiaction = new EmployeeQualification())
            {
                objfrmEmployeeQualifiaction.StartPosition = FormStartPosition.CenterScreen;
                objfrmEmployeeQualifiaction.FormBorderStyle = FormBorderStyle.FixedSingle;
                objfrmEmployeeQualifiaction.MaximizeBox = false;
                objfrmEmployeeQualifiaction.MinimizeBox = false;
                objfrmEmployeeQualifiaction.ShowDialog();
            }
        }

        private void EmiratesCard_Click(object sender, EventArgs e)
        {
            using (EmiratesHealthLabourCard objEmiratesHealthLabourCard = new EmiratesHealthLabourCard(166)) //166->Emirates FormID
            {
                objEmiratesHealthLabourCard.StartPosition = FormStartPosition.CenterScreen;
                objEmiratesHealthLabourCard.FormBorderStyle = FormBorderStyle.FixedSingle;
                objEmiratesHealthLabourCard.MaximizeBox = false;
                objEmiratesHealthLabourCard.MinimizeBox = false;
                objEmiratesHealthLabourCard.ShowDialog();
            }
        }

        private void InsuranceCard_Click(object sender, EventArgs e)
        {
            using (InsuranceCard objInsuranceCard = new InsuranceCard())
            {
                objInsuranceCard.StartPosition = FormStartPosition.CenterScreen;
                objInsuranceCard.FormBorderStyle = FormBorderStyle.FixedSingle;
                objInsuranceCard.MaximizeBox = false;
                objInsuranceCard.MinimizeBox = false;
                objInsuranceCard.ShowDialog();

            }
        }

        private void btnLeaveOpening_Click(object sender, EventArgs e)
        {
            using (FrmLeaveOpening objFrmLeaveOpening = new FrmLeaveOpening())
            {
                objFrmLeaveOpening.ShowDialog();
            }
        }

        //private void btnLeaveExtension_Click(object sender, EventArgs e)
        //{
        //    using (FrmLeaveExtension objFrmLeaveExtension = new FrmLeaveExtension())
        //    {
        //        objFrmLeaveExtension.ShowDialog();
        //    }
        //}

        private void StockRegister_Click(object sender, EventArgs e)
        {
            ShowStockRegister();
        }


        private void ShowStockRegister()
        {
            frmStockRegister objStockRegister = null;

            try
            {
                objStockRegister = new frmStockRegister();
                if (ClsCommonSettings.IsArabicView)
                    objStockRegister.Text = "سجل توثيق";
                else
                    objStockRegister.Text = "Document Register";
                objStockRegister.MdiParent = this;
                objStockRegister.WindowState = FormWindowState.Maximized;
                objStockRegister.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objStockRegister.Dispose();
                System.GC.Collect();
                ShowStockRegister();
            }
        }

        private void btnDocProcessTime_Click(object sender, EventArgs e)
        {
            using (frmProcessingTimeMaster objProcessing = new frmProcessingTimeMaster())
            {
                objProcessing.StartPosition = FormStartPosition.CenterScreen;
                objProcessing.FormBorderStyle = FormBorderStyle.FixedSingle;
                objProcessing.MaximizeBox = false;
                objProcessing.MinimizeBox = false;

                objProcessing.ShowDialog();
            }
        }

        private void btnProcess_Click_1(object sender, EventArgs e)
        {
            CallSalaryProcess();
        }

        private void btnRelease_Click_1(object sender, EventArgs e)
        {
            CallSalaryRelease();
        }

        public void CallSalaryRelease()
        {
            FrmSalaryRelease nwRelease = new FrmSalaryRelease();
            try
            {
              
                nwRelease.MdiParent = this;
             
                
                    if (ClsCommonSettings.IsArabicView)
                        nwRelease.Text = "الإصدار المرتبات " + this.MdiChildren.Length.ToString();
                    else
                        nwRelease.Text = "Salary Release " + this.MdiChildren.Length.ToString();                    
                
                nwRelease.WindowState = FormWindowState.Maximized;
                nwRelease.Show();
                nwRelease.Update();
            }
            catch (OutOfMemoryException ex)
            {
                nwRelease.Dispose();
                System.GC.Collect();
                CallSalaryRelease();
            }
        }

        public void CallSalaryProcess()
        {
            FrmSalaryProcessRelease nwRelease = new FrmSalaryProcessRelease();
            try
            {

                nwRelease.MdiParent = this;

                if (ClsCommonSettings.IsArabicView)
                    nwRelease.Text = "عملية التسجيل لل " + this.MdiChildren.Length.ToString();
                else
                    nwRelease.Text = "Salary Process " + this.MdiChildren.Length.ToString();

                nwRelease.WindowState = FormWindowState.Maximized;
                nwRelease.Show();
                nwRelease.Update();
            }
            catch (OutOfMemoryException ex)
            {
                nwRelease.Dispose();
                System.GC.Collect();
                CallSalaryProcess();
            }
        }
        public void CallSettlementBatchwise()
        {
            //FrmSettlementProcessGroup nwRelease = new FrmSettlementProcessGroup();
            //try
            //{

            //    nwRelease.MdiParent = this;

            //    if (ClsCommonSettings.IsArabicView)
            //        nwRelease.Text = "حفظ البيانات " + this.MdiChildren.Length.ToString();
            //    else
            //        nwRelease.Text = "Settlement Process - Batchwise " + this.MdiChildren.Length.ToString();

            //    nwRelease.WindowState = FormWindowState.Maximized;
            //    nwRelease.Show();
            //    nwRelease.Update();
            //}
            //catch (OutOfMemoryException ex)
            //{
            //    nwRelease.Dispose();
            //    System.GC.Collect();
            //    CallSalaryProcess();
            //}
        }
        private void HealthCard_Click(object sender, EventArgs e)
        {
            using (EmployeeHealthCard objHealthCard = new EmployeeHealthCard())
            {
                objHealthCard.StartPosition = FormStartPosition.CenterScreen;
                objHealthCard.FormBorderStyle = FormBorderStyle.FixedSingle;
                objHealthCard.MaximizeBox = false;
                objHealthCard.MinimizeBox = false;

                objHealthCard.ShowDialog();
            }
        }

        private void buttonItem4_Click(object sender, EventArgs e)
        {
            ShowStockRegister();
        }

        private void LabourCard_Click(object sender, EventArgs e)
        {
            using (FrmEmployeeLabourCard objFrmEmployeeLabourCard = new FrmEmployeeLabourCard())
            {
                objFrmEmployeeLabourCard.StartPosition = FormStartPosition.CenterScreen;
                objFrmEmployeeLabourCard.FormBorderStyle = FormBorderStyle.FixedSingle;
                objFrmEmployeeLabourCard.MaximizeBox = false;
                objFrmEmployeeLabourCard.MinimizeBox = false;
                objFrmEmployeeLabourCard.ShowDialog();
            }
        }


        public void ShowDocument()
        {
            frmDocumentReport objDocumentReport = null;

            try
            {
                objDocumentReport = new frmDocumentReport();
                if (ClsCommonSettings.IsArabicView)
                    objDocumentReport.Text = "تقرير الوثيقة";
                else
                    objDocumentReport.Text = "Document Report";
                objDocumentReport.MdiParent = this;
                objDocumentReport.WindowState = FormWindowState.Maximized;
                objDocumentReport.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objDocumentReport.Dispose();
                System.GC.Collect();
                ShowDocument();
            }
        }
        private void Document_Click(object sender, EventArgs e)
        {
            ShowDocument();

        }

        private void btnAssetHandover_Click(object sender, EventArgs e)
        {
            CallAssetHandOver();
        }

        public void CallAssetHandOver()
        {
            FrmCompanyAssetOperations objFrmCompanyAssetOperations = new FrmCompanyAssetOperations();
            try
            {
                objFrmCompanyAssetOperations = new FrmCompanyAssetOperations();
                if (ClsCommonSettings.IsArabicView)
                    objFrmCompanyAssetOperations.Text = "الأصول تسليم";
                else
                    objFrmCompanyAssetOperations.Text = "Asset Handover";
                objFrmCompanyAssetOperations.MdiParent = this;
                objFrmCompanyAssetOperations.WindowState = FormWindowState.Maximized;
                objFrmCompanyAssetOperations.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmCompanyAssetOperations.Dispose();
                System.GC.Collect();
                CallAssetHandOver();
            }
        }

        private void Showdocument(Int32 intAlertID)
        {
            if (intAlertID > 0)
            {
                DataTable dtAlertData = objNavigatorBLL.GetAlertData(intAlertID);

                if (dtAlertData.Rows.Count > 0)
                {
                    switch (dtAlertData.Rows[0]["DocumentTypeID"].ToInt32())
                    {
                        case (int)DocumentType.Passport:
                            using (frmPassport objfrmPassport = new frmPassport())
                            {
                                objfrmPassport.StartPosition = FormStartPosition.CenterScreen;
                                objfrmPassport.PiPassportID = dtAlertData.Rows[0]["CommonID"].ToInt32();
                                objfrmPassport.ShowDialog();
                            }
                            break;

                        case (int)DocumentType.Visa:
                            using (frmVisa objfrmVisa = new frmVisa())
                            {
                                objfrmVisa.StartPosition = FormStartPosition.CenterScreen;
                                objfrmVisa.piVisaID = dtAlertData.Rows[0]["CommonID"].ToInt32();
                                objfrmVisa.ShowDialog();
                            }
                            break;
                        case (int)DocumentType.Driving_License:
                            using (frmDrivingLicense objDrivingLicense = new frmDrivingLicense())
                            {
                                objDrivingLicense.StartPosition = FormStartPosition.CenterScreen;
                                objDrivingLicense.PiDrivingLicense = dtAlertData.Rows[0]["CommonID"].ToInt32();
                                objDrivingLicense.ShowDialog();
                            }
                            break;
                        case (int)DocumentType.National_ID_Card:
                            using (EmiratesHealthLabourCard objEmiratesHealthLabourCard = new EmiratesHealthLabourCard(166)) //166->Emirates FormID
                            {
                                objEmiratesHealthLabourCard.StartPosition = FormStartPosition.CenterScreen;
                                objEmiratesHealthLabourCard.PiEmCardID = dtAlertData.Rows[0]["CommonID"].ToInt32();
                                objEmiratesHealthLabourCard.ShowDialog();
                            }
                            break;
                        case (int)DocumentType.Insurance_Card:
                            using (InsuranceCard objInsuranceCard = new InsuranceCard())
                            {
                                objInsuranceCard.StartPosition = FormStartPosition.CenterScreen;
                                objInsuranceCard.PintInsuranceCardID = dtAlertData.Rows[0]["CommonID"].ToInt32();
                                objInsuranceCard.ShowDialog();
                            }
                            break;
                        case (int)DocumentType.Labour_Card:
                            using (FrmEmployeeLabourCard objFrmEmployeeLabourCard = new FrmEmployeeLabourCard())
                            {
                                objFrmEmployeeLabourCard.StartPosition = FormStartPosition.CenterScreen;
                                objFrmEmployeeLabourCard.PLabourCardId = dtAlertData.Rows[0]["CommonID"].ToInt32();
                                objFrmEmployeeLabourCard.ShowDialog();
                            }
                            break;
                        case (int)DocumentType.Health_Card:
                            using (EmployeeHealthCard objHealthCard = new EmployeeHealthCard())
                            {
                                objHealthCard.StartPosition = FormStartPosition.CenterScreen;
                                objHealthCard.MiHealthCardID = dtAlertData.Rows[0]["CommonID"].ToInt32();
                                objHealthCard.ShowDialog();
                            }
                            break;
                        case (int)DocumentType.Qualification:
                            using (EmployeeQualification objEmployeeQualification = new EmployeeQualification())
                            {
                                objEmployeeQualification.StartPosition = FormStartPosition.CenterScreen;
                                objEmployeeQualification.PQualificationID = dtAlertData.Rows[0]["CommonID"].ToInt32();
                                objEmployeeQualification.ShowDialog();
                            }
                            break;
                        default:
                            using (frmDocumentMasterNew objfrmDocumentMasterNew = new frmDocumentMasterNew())
                            {
                                objfrmDocumentMasterNew.StartPosition = FormStartPosition.CenterScreen;
                                objfrmDocumentMasterNew.DocumentID = dtAlertData.Rows[0]["CommonID"].ToInt32();
                                objfrmDocumentMasterNew.ePDocumentType = (DocumentType)dtAlertData.Rows[0]["DocumentTypeID"].ToInt32();
                                objfrmDocumentMasterNew.ShowDialog();
                            }
                            break;
                    }
                }

            }
        }

        private void btnAlert_Click(object sender, EventArgs e)
        {
            ShowAlertReport();
        }

        private void ShowAlertReport()
        {
            frmAlert objAlert = null;
            try
            {
                objAlert = new frmAlert();
                if (ClsCommonSettings.IsArabicView)
                    objAlert.Text = "تنبيه تقرير";
                else
                    objAlert.Text = "Alert Report";
                objAlert.MdiParent = this;
                objAlert.WindowState = FormWindowState.Maximized;
                objAlert.Show();
            }
            catch (Exception ex)
            {
                objAlert.Dispose();
                System.GC.Collect();
                ShowAlertReport();
            }
        }

        private void buttonItem5_Click(object sender, EventArgs e)
        {
            frmAlertreport objAlertreport = null;

            try
            {
                objAlertreport = new frmAlertreport();

                objAlertreport.MdiParent = this;
                objAlertreport.WindowState = FormWindowState.Maximized;
                objAlertreport.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objAlertreport.Dispose();
                System.GC.Collect();
                ShowDocument();
            }

        }

        private void General_Click(object sender, EventArgs e)
        {

        }

        private void BtnShiftSchedule_Click_1(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmShiftSchedule(), true);
        }

        private void btnAssetHand_Click(object sender, EventArgs e)
        {
            ShowAssetHandOverReport();
        }


        public void ShowAssetHandOverReport()
        {
            FrmRptCompanyAsset objFrmRptCompanyAsset = null;
            try
            {
                objFrmRptCompanyAsset = new FrmRptCompanyAsset();
                if (ClsCommonSettings.IsArabicView)
                    objFrmRptCompanyAsset.Text = "تقرير الأصول";
                else
                    objFrmRptCompanyAsset.Text = "Asset Report";
                objFrmRptCompanyAsset.MdiParent = this;
                objFrmRptCompanyAsset.WindowState = FormWindowState.Maximized;
                objFrmRptCompanyAsset.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptCompanyAsset.Dispose();
                System.GC.Collect();
                ShowAssetHandOverReport();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            tmrAlert_Tick(null, null);
        }

        private void btnExpense_Click(object sender, EventArgs e)
        {
            new FrmEmployeeExpense().ShowDialog();
        }

        private void btnUnearnedPolicy_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmUnearnedPolicy(), true);
        }

        private void btnDeposit_Click(object sender, EventArgs e)
        {
            new FrmDeposit().ShowDialog();
        }

        private void btnLoanRepayment_Click(object sender, EventArgs e)
        {
            new FrmLoanRepayment().ShowDialog();
        }

        private void btnVacationProcess_Click(object sender, EventArgs e)
        {
            using (FrmVacationEntry objWizard = new FrmVacationEntry())
            {
                objWizard.ShowDialog();
            }
        }

        private void btnSettlementProcess_Click(object sender, EventArgs e)
        {
            using (FrmSettlementEntry objSettlementEntry = new FrmSettlementEntry())
            {
                objSettlementEntry.ShowDialog();
            }
        }

        private void Attendance_Click(object sender, EventArgs e)
        {

        }

        private void btnRptVacation_Click(object sender, EventArgs e)
        {
            ShowVacationReport();
        }
        public void ShowVacationReport()
        {
            FrmVacationReport objVacationReport = null;
            try
            {
               objVacationReport = new FrmVacationReport();
                if (ClsCommonSettings.IsArabicView)
                    objVacationReport.Text = "تقرير اجازة";
                else
                    objVacationReport.Text = "Vacation Report";
                objVacationReport.MdiParent = this;
                objVacationReport.WindowState = FormWindowState.Maximized;
                objVacationReport.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objVacationReport.Dispose();
                System.GC.Collect();
                ShowVacationReport();
            }
        }
        public void ShowExpenseReport()
        {
            FrmExpenseReport objFrmRptExpenseReport = null;
            try
            {
                objFrmRptExpenseReport = new FrmExpenseReport();
                if (ClsCommonSettings.IsArabicView)
                    objFrmRptExpenseReport.Text = "تقرير المصاريف";
                else
                    objFrmRptExpenseReport.Text = "Expense Report";
                objFrmRptExpenseReport.MdiParent = this;
                objFrmRptExpenseReport.WindowState = FormWindowState.Maximized;
                objFrmRptExpenseReport.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptExpenseReport.Dispose();
                System.GC.Collect();
                ShowExpenseReport();
            }
        }

        private void btnExpenseReport_Click(object sender, EventArgs e)
        {
            ShowExpenseReport();
        }

        private void btnTradeLicense_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new frmCompanyTradeLicense(), true);
        }

        private void btnSettlementReport_Click(object sender, EventArgs e)
        {
            ShowSettlementReport();
        }
        public void ShowSettlementReport()
        {
            FrmSettlementReport objFrmSettlementReport = null;
            try
            {
                objFrmSettlementReport = new FrmSettlementReport();
                if (ClsCommonSettings.IsArabicView)
                    objFrmSettlementReport.Text = "تقرير التسوية";
                else
                    objFrmSettlementReport.Text = "Settlement Report";
                objFrmSettlementReport.MdiParent = this;
                objFrmSettlementReport.WindowState = FormWindowState.Maximized;
                objFrmSettlementReport.Show();
            }
            catch (OutOfMemoryException)
            {
                objFrmSettlementReport.Dispose();
                System.GC.Collect();
                //
            }
        }
        private void btnRptLabourCost_Click(object sender, EventArgs e)
        {
            FrmRptLabourCost objFrmRptLabourCost = null;
            try
            {
                objFrmRptLabourCost = new FrmRptLabourCost();
                if (ClsCommonSettings.IsArabicView)
                    objFrmRptLabourCost.Text = "تقرير تكلفة العمل";
                else
                    objFrmRptLabourCost.Text = "Labour Cost Report";
                objFrmRptLabourCost.MdiParent = this;
                objFrmRptLabourCost.WindowState = FormWindowState.Maximized;
                objFrmRptLabourCost.Show();
            }
            catch (OutOfMemoryException)
            {
                objFrmRptLabourCost.Dispose();
                System.GC.Collect();
            }
        }

        private void btnLeaseAgreement_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new frmCompanyLeaseAgreement (), true);
        }

        private void btnRptCTCReport_Click(object sender, EventArgs e)
        {
            FrmRptCTC objCTCReport = null;
            try
            {
                objCTCReport = new FrmRptCTC();
                if (ClsCommonSettings.IsArabicView)
                    objCTCReport.Text = "تقرير لجنة مكافحة الإرهاب";
                else
                    objCTCReport.Text = "CTC Report";
                objCTCReport.MdiParent = this;
                objCTCReport.WindowState = FormWindowState.Maximized;
                objCTCReport.Show();
            }
            catch (OutOfMemoryException)
            {
                objCTCReport.Dispose();
                System.GC.Collect();
            }

        }

        private void btnHlpContents_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void btnInsurance_Click(object sender, EventArgs e)
        {
            InsuranceDetails objInsurance = new InsuranceDetails();
            objInsurance.ShowDialog();
            objInsurance = null;
        }

        private void btnRptHoliday_Click(object sender, EventArgs e)
        {
            ShowHolidayReport();
        }
        private void ShowHolidayReport()
        {
            FrmRptHoliday objRptHoliday = null;
            try
            {
                objRptHoliday = new FrmRptHoliday();
                if (ClsCommonSettings.IsArabicView)
                    objRptHoliday.Text = "عطلة تقرير";
                else
                    objRptHoliday.Text = "Holiday Report";
                objRptHoliday.MdiParent = this;
                objRptHoliday.WindowState = FormWindowState.Maximized;
                objRptHoliday.Show();
            }
            catch (OutOfMemoryException)
            {
                objRptHoliday.Dispose();
                System.GC.Collect();
                ShowHolidayReport();
            }
        }

        private void Transfer_Click(object sender, EventArgs e)
        {
            FrmTransferReport objRptTransferReport = null;
            try
            {
                objRptTransferReport = new FrmTransferReport();
                if (ClsCommonSettings.IsArabicView)
                    objRptTransferReport.Text = "يمكنك نقل الموظف تقرير";
                else
                    objRptTransferReport.Text = "Employee Transfer Report";
                objRptTransferReport.MdiParent = this;
                objRptTransferReport.WindowState = FormWindowState.Maximized;
                objRptTransferReport.Show();
            }
            catch (OutOfMemoryException)
            {
                objRptTransferReport.Dispose();
                System.GC.Collect();
                
            }
        }

        private void btnSupport_Click(object sender, EventArgs e)
        {
            using (FrmSupport objSupport = new FrmSupport())
            {
                objSupport.ShowDialog();
            }
        }

        public void ShowProjectCostReport()
        {
            FrmRptEmployeeExpense objFrmRptPayments = null;
            try
            {
                objFrmRptPayments = new FrmRptEmployeeExpense();
                if (ClsCommonSettings.IsArabicView)
                    objFrmRptPayments.Text = "تقرير الدفع";
                else
                    objFrmRptPayments.Text = "Project Cost Report";
                objFrmRptPayments.MdiParent = this;
                objFrmRptPayments.WindowState = FormWindowState.Maximized;
                objFrmRptPayments.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmRptPayments.Dispose();
                System.GC.Collect();
                ShowPaymetsReport();
            }
        }

        private void btnPettyCash_Click(object sender, EventArgs e)
        {
            new FrmEmployeePettyCash().ShowDialog();
        }

        private void ribbonTabItem5_Click(object sender, EventArgs e)
        {

        }

        private void FrmMain_KeyDown(object sender, KeyEventArgs e)
        {
            //try
            //{
            //    if (e.KeyData == Keys.F2)
            //    {


      
            //            using (frmCompanySelection objCompanySelection = new frmCompanySelection())
            //            {
            //                objCompanySelection.BringToFront();
            //                objCompanySelection.ShowDialog();

            //                if (this.HasChildren)
            //                {
            //                    foreach (Form f in this.MdiChildren)
            //                        f.Close();
            //                }
            //                UpdateTitle();
            //                ShowEasyview();
            //                SetPermissions();
            //            }
            //        }

    
            //}
            //catch
            //{
            //}
        }

        private void mbtnChangeCompany_Click(object sender, EventArgs e)
        {
            try
            {
                using (frmCompanySelection objCompanySelection = new frmCompanySelection())
                {
                    objCompanySelection.BringToFront();
                    objCompanySelection.ShowDialog();

                    if (this.HasChildren)
                    {
                        foreach (Form f in this.MdiChildren)
                            f.Close();
                    }
                    UpdateTitle();
                    ShowEasyview();
                    SetPermissions();
                }
            }
            catch
            {
            }
        }

        private void ribbonBarEmployee_ItemClick(object sender, EventArgs e)
        {

        }

        private void btnRptAttendanceLive_Click(object sender, EventArgs e)
        {
            ShowAttendanceReportLive();
        }

        public void ShowAttendanceReportLive()
        {
            FrmRptAttendanceLive  objAttendance = null;
            try
            {
                objAttendance = new FrmRptAttendanceLive();
                if (ClsCommonSettings.IsArabicView)
                    objAttendance.Text = "تقرير الحضور";
                else
                    objAttendance.Text = "Live Attendance";
                objAttendance.MdiParent = this;
                objAttendance.WindowState = FormWindowState.Maximized;
                objAttendance.Show();
            }
            catch (OutOfMemoryException)
            {
                objAttendance.Dispose();
                System.GC.Collect();
                ShowAttendanceReport();
            }
        }

        private void btnPaymentsProcessedReport_Click(object sender, EventArgs e)
        {
            ShowPaymetsProcessedReport();
        }

        private void btnAttendanceWorkSheet_Click(object sender, EventArgs e)
        {
            FrmWorkSheet objFrmWorkSheet = null;
            try
            {
                objFrmWorkSheet = new FrmWorkSheet();
                if (ClsCommonSettings.IsArabicView)
                    objFrmWorkSheet.Text = "الحضور";
                else
                    objFrmWorkSheet.Text = "WorkSheet";
                objFrmWorkSheet.MdiParent = this;
                objFrmWorkSheet.WindowState = FormWindowState.Maximized;
                objFrmWorkSheet.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objFrmWorkSheet.Dispose();
                System.GC.Collect();
            }

        }



        public void ShowWorkSheetReport()
        {
            FrmRptWorkSheet objAttendance = null;
            try
            {
                objAttendance = new FrmRptWorkSheet();
                if (ClsCommonSettings.IsArabicView)
                    objAttendance.Text = "ورقة العمل ";
                else
                    objAttendance.Text = "Work Sheet";
                objAttendance.MdiParent = this;
                objAttendance.WindowState = FormWindowState.Maximized;
                objAttendance.Show();
            }
            catch (OutOfMemoryException)
            {
                objAttendance.Dispose();
                System.GC.Collect();
                ShowAttendanceReport();
            }
        }

        private void btnRptWorkSheet_Click(object sender, EventArgs e)
        {
            ShowWorkSheetReport();
        }

        private void btnOffDayMarking_Click(object sender, EventArgs e)
        {
            using (DutyRoaster objFrmOffDayMark = new DutyRoaster())
            {
                objFrmOffDayMark.ShowDialog();
            }
        }

        private void btnOffDay_Click(object sender, EventArgs e)
        {
            ShowRptOffDayMark();
        }



        public void ShowRptOffDayMark()
        {
            FrmRptOffDayMark objAttendance = null;
            try
            {
                objAttendance = new FrmRptOffDayMark();
                if (ClsCommonSettings.IsArabicView)
                    objAttendance.Text = "من يوم تفاصيل ";
                else
                    objAttendance.Text = "Off Day Details";
                objAttendance.MdiParent = this;
                objAttendance.WindowState = FormWindowState.Maximized;
                objAttendance.Show();
            }
            catch (OutOfMemoryException)
            {
                objAttendance.Dispose();
                System.GC.Collect();
                ShowAttendanceReport();
            }
        }

        private void btnCompanySettings_Click(object sender, EventArgs e)
        {
            using (FrmCompanySettings ObjFrm = new FrmCompanySettings())
            {
                ObjFrm.ShowDialog();  
            }
        }

        private void btnGLCodeMapping_Click(object sender, EventArgs e)
        {
            using (frmGLCodeMapping objGLCodeMapping = new frmGLCodeMapping())
                objGLCodeMapping.ShowDialog();
        }

        private void btnAccrualReport_Click(object sender, EventArgs e)
        {
            ShowRptAccrualReport();
        }
        public void ShowRptAccrualReport()
        {
            frmRptAccrual objAccrual = null;
            try
            {
                objAccrual = new frmRptAccrual();
                if (ClsCommonSettings.IsArabicView)
                    objAccrual.Text = "من يوم تفاصيل ";
              
                objAccrual.MdiParent = this;
                objAccrual.WindowState = FormWindowState.Maximized;
                objAccrual.Show();
            }
            catch (OutOfMemoryException)
            {
                objAccrual.Dispose();
                System.GC.Collect();
                ShowRptAccrualReport();
            }
        }

        private void btnRptEmployeeHistory_Click(object sender, EventArgs e)
        {


            FrmRptEmployeeHistory objEmployeeHistory = null;
            try
            {
                objEmployeeHistory = new FrmRptEmployeeHistory();
                if (ClsCommonSettings.IsArabicView)
                    objEmployeeHistory.Text = "عامل تاريخ";
                else
                    objEmployeeHistory.Text = "Employee History Report";
                objEmployeeHistory.MdiParent = this;
                objEmployeeHistory.WindowState = FormWindowState.Maximized;
                objEmployeeHistory.Show();
            }
            catch (OutOfMemoryException)
            {
                objEmployeeHistory.Dispose();
                System.GC.Collect();
            }

        }

        private void btnProfitCenterTarget_Click(object sender, EventArgs e)
        {
            using (frmProfitCenterTarget obj = new frmProfitCenterTarget())
            {
                obj.ShowDialog();
            }
        }

        private void btnCommissionStructure_Click(object sender, EventArgs e)
        {
            using (frmCommissionStructure obj = new frmCommissionStructure())
            {
                obj.ShowDialog();
            }
        }

        private void btnSalesInvoice_Click(object sender, EventArgs e)
        {
            using (frmSalesInvoice obj = new frmSalesInvoice())
            {
                obj.ShowDialog();
            }
        }

        private void btnRptSalesInvoice_Click(object sender, EventArgs e)
        {
            ShowSalesIncentiveReport();
        }

        public void ShowSalesIncentiveReport()
        {
            frmSalesInvoiceSummary objSalesInvoiceSummary = null;
            try
            {
                objSalesInvoiceSummary = new frmSalesInvoiceSummary();
                if (ClsCommonSettings.IsArabicView)
                    objSalesInvoiceSummary.Text = "تقرير الدفع";
                else
                    objSalesInvoiceSummary.Text = "Sales Invoice Summary";
                objSalesInvoiceSummary.MdiParent = this;
                objSalesInvoiceSummary.WindowState = FormWindowState.Maximized;
                objSalesInvoiceSummary.Show();
            }
            catch (OutOfMemoryException ex)
            {
                objSalesInvoiceSummary.Dispose();
                System.GC.Collect();
                ShowSalesIncentiveReport();
            }
        }

        private void btnSalaryTemplate_Click(object sender, EventArgs e)
        {
            using (FrmSalaryTemplate obj = new FrmSalaryTemplate())
            {
                obj.ShowDialog();
            }
        }

        private void btnSettlementBatch_Click(object sender, EventArgs e)
        {
            CallSettlementBatchwise();
            using (FrmSettlementProcessGroup objSettlementProcessBatchwise = new FrmSettlementProcessGroup())
            {
                objSettlementProcessBatchwise.ShowDialog();
            }

        }

        private void btnUpdateAccruals_Click(object sender, EventArgs e)
        {
            using (FrmUpdateAccruals objUpdateAccruals = new FrmUpdateAccruals())
            {
                objUpdateAccruals.ShowDialog();
            }
        }

        private void btnSalaryAmendment_Click(object sender, EventArgs e)
        {
            using (FrmSalaryAmendment frmSalaryAmendment = new FrmSalaryAmendment())
            {
                frmSalaryAmendment.ShowDialog();
            }
        }

        private void btnIntegration_Click(object sender, EventArgs e)
        {
            using (FrmCSV objFrmCSV = new FrmCSV())
            {
                objFrmCSV.ShowDialog();
            }
        }

        private void btnRptEmployeeLedger_Click(object sender, EventArgs e)
        {
            FrmRptEmployeeLedger objEmployeeLedger = null;
            try
            {
                objEmployeeLedger = new FrmRptEmployeeLedger();
                if (ClsCommonSettings.IsArabicView)
                    objEmployeeLedger.Text = "عامل تاريخ";
                else
                    objEmployeeLedger.Text = "Employee Ledger Report";
                objEmployeeLedger.MdiParent = this;
                objEmployeeLedger.WindowState = FormWindowState.Maximized;
                objEmployeeLedger.Show();
            }
            catch (OutOfMemoryException)
            {
                objEmployeeLedger.Dispose();
                System.GC.Collect();
            }
        }

        

     
    }
}