﻿namespace MyPayfriend
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chkRemember = new System.Windows.Forms.CheckBox();
            this.lblLoginStatus = new System.Windows.Forms.Label();
            this.Timerlogin = new System.Windows.Forms.Timer(this.components);
            this.rBtnArabic = new System.Windows.Forms.RadioButton();
            this.rBtnEnglish = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnLogin.Location = new System.Drawing.Point(134, 208);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(82, 23);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(134, 131);
            this.txtPassword.MaxLength = 50;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(183, 20);
            this.txtPassword.TabIndex = 1;
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyDown);
            this.txtPassword.Enter += new System.EventHandler(this.txtPassword_Enter);
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(134, 104);
            this.txtUserName.MaxLength = 50;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(183, 20);
            this.txtUserName.TabIndex = 0;
            this.txtUserName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyDown);
            this.txtUserName.Enter += new System.EventHandler(this.txtPassword_Enter);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(235, 208);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(82, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chkRemember
            // 
            this.chkRemember.AutoSize = true;
            this.chkRemember.BackColor = System.Drawing.Color.Transparent;
            this.chkRemember.ForeColor = System.Drawing.Color.White;
            this.chkRemember.Location = new System.Drawing.Point(134, 158);
            this.chkRemember.Name = "chkRemember";
            this.chkRemember.Size = new System.Drawing.Size(175, 17);
            this.chkRemember.TabIndex = 2;
            this.chkRemember.TabStop = false;
            this.chkRemember.Text = "Remember me on this computer";
            this.chkRemember.UseVisualStyleBackColor = false;
            this.chkRemember.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyDown);
            // 
            // lblLoginStatus
            // 
            this.lblLoginStatus.AutoSize = true;
            this.lblLoginStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblLoginStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoginStatus.ForeColor = System.Drawing.Color.Navy;
            this.lblLoginStatus.Location = new System.Drawing.Point(12, 227);
            this.lblLoginStatus.Name = "lblLoginStatus";
            this.lblLoginStatus.Size = new System.Drawing.Size(0, 14);
            this.lblLoginStatus.TabIndex = 7;
            // 
            // Timerlogin
            // 
            this.Timerlogin.Interval = 2000;
            this.Timerlogin.Tick += new System.EventHandler(this.Timerlogin_Tick);
            // 
            // rBtnArabic
            // 
            this.rBtnArabic.AutoSize = true;
            this.rBtnArabic.BackColor = System.Drawing.Color.Transparent;
            this.rBtnArabic.ForeColor = System.Drawing.Color.White;
            this.rBtnArabic.Location = new System.Drawing.Point(235, 181);
            this.rBtnArabic.Name = "rBtnArabic";
            this.rBtnArabic.Size = new System.Drawing.Size(55, 17);
            this.rBtnArabic.TabIndex = 4;
            this.rBtnArabic.Text = "العربية";
            this.rBtnArabic.UseVisualStyleBackColor = false;
            // 
            // rBtnEnglish
            // 
            this.rBtnEnglish.AutoSize = true;
            this.rBtnEnglish.BackColor = System.Drawing.Color.Transparent;
            this.rBtnEnglish.Checked = true;
            this.rBtnEnglish.ForeColor = System.Drawing.Color.White;
            this.rBtnEnglish.Location = new System.Drawing.Point(134, 181);
            this.rBtnEnglish.Name = "rBtnEnglish";
            this.rBtnEnglish.Size = new System.Drawing.Size(59, 17);
            this.rBtnEnglish.TabIndex = 3;
            this.rBtnEnglish.TabStop = true;
            this.rBtnEnglish.Text = "English";
            this.rBtnEnglish.UseVisualStyleBackColor = false;
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MyPayfriend.Properties.Resources.EpeopleLogin;
            this.ClientSize = new System.Drawing.Size(449, 250);
            this.Controls.Add(this.rBtnEnglish);
            this.Controls.Add(this.rBtnArabic);
            this.Controls.Add(this.lblLoginStatus);
            this.Controls.Add(this.chkRemember);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLogin";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.Shown += new System.EventHandler(this.FrmLogin_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLogin_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.CheckBox chkRemember;
        internal System.Windows.Forms.Label lblLoginStatus;
        internal System.Windows.Forms.Timer Timerlogin;
        private System.Windows.Forms.RadioButton rBtnArabic;
        private System.Windows.Forms.RadioButton rBtnEnglish;
    }
}