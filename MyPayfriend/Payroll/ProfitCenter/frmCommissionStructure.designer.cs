﻿namespace MyPayfriend
{
    partial class frmCommissionStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCommissionStructure));
            this.ssProfitCenter = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.expLeft = new DevComponents.DotNetBar.ExpandablePanel();
            this.dgvSearch = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.CommissionStructureID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StructureName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bnProfitCenter = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.txtCommissionStructure = new System.Windows.Forms.TextBox();
            this.lblCommissionStructure = new System.Windows.Forms.Label();
            this.lblDeductionPercentage = new System.Windows.Forms.Label();
            this.diDeductionPercentage = new DevComponents.Editors.DoubleInput();
            this.tcCommissionStructure = new System.Windows.Forms.TabControl();
            this.tpGroup = new System.Windows.Forms.TabPage();
            this.dgvGroupTarget = new System.Windows.Forms.DataGridView();
            this.FromPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpIndividual = new System.Windows.Forms.TabPage();
            this.dgvExcessTarget = new System.Windows.Forms.DataGridView();
            this.ExcessParameterID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ExcessTarget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Percentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.tmrProfitCenter = new System.Windows.Forms.Timer(this.components);
            this.errProfitCenter = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.ssProfitCenter.SuspendLayout();
            this.expLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnProfitCenter)).BeginInit();
            this.bnProfitCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.diDeductionPercentage)).BeginInit();
            this.tcCommissionStructure.SuspendLayout();
            this.tpGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroupTarget)).BeginInit();
            this.tpIndividual.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcessTarget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProfitCenter)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // ssProfitCenter
            // 
            this.ssProfitCenter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.ssProfitCenter.Location = new System.Drawing.Point(0, 452);
            this.ssProfitCenter.Name = "ssProfitCenter";
            this.ssProfitCenter.Size = new System.Drawing.Size(794, 22);
            this.ssProfitCenter.TabIndex = 1048;
            this.ssProfitCenter.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // expLeft
            // 
            this.expLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.expLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expLeft.Controls.Add(this.dgvSearch);
            this.expLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.expLeft.ExpandButtonVisible = false;
            this.expLeft.Location = new System.Drawing.Point(0, 0);
            this.expLeft.Name = "expLeft";
            this.expLeft.Size = new System.Drawing.Size(243, 452);
            this.expLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expLeft.Style.GradientAngle = 90;
            this.expLeft.TabIndex = 1049;
            this.expLeft.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expLeft.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expLeft.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expLeft.TitleStyle.GradientAngle = 90;
            this.expLeft.TitleText = "Search";
            // 
            // dgvSearch
            // 
            this.dgvSearch.AllowUserToAddRows = false;
            this.dgvSearch.AllowUserToDeleteRows = false;
            this.dgvSearch.AllowUserToResizeColumns = false;
            this.dgvSearch.AllowUserToResizeRows = false;
            this.dgvSearch.BackgroundColor = System.Drawing.Color.White;
            this.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CommissionStructureID,
            this.StructureName});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSearch.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSearch.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSearch.Location = new System.Drawing.Point(0, 26);
            this.dgvSearch.Name = "dgvSearch";
            this.dgvSearch.ReadOnly = true;
            this.dgvSearch.RowHeadersVisible = false;
            this.dgvSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSearch.Size = new System.Drawing.Size(243, 426);
            this.dgvSearch.TabIndex = 1;
            this.dgvSearch.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSearch_CellDoubleClick);
            this.dgvSearch.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // CommissionStructureID
            // 
            this.CommissionStructureID.DataPropertyName = "CommissionStructureID";
            this.CommissionStructureID.HeaderText = "CommissionStructureID";
            this.CommissionStructureID.Name = "CommissionStructureID";
            this.CommissionStructureID.ReadOnly = true;
            this.CommissionStructureID.Visible = false;
            // 
            // StructureName
            // 
            this.StructureName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StructureName.DataPropertyName = "StructureName";
            this.StructureName.HeaderText = "Commission Structure";
            this.StructureName.Name = "StructureName";
            this.StructureName.ReadOnly = true;
            // 
            // bnProfitCenter
            // 
            this.bnProfitCenter.AddNewItem = null;
            this.bnProfitCenter.CountItem = null;
            this.bnProfitCenter.DeleteItem = null;
            this.bnProfitCenter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.btnSaveItem,
            this.btnDelete,
            this.btnClear});
            this.bnProfitCenter.Location = new System.Drawing.Point(243, 0);
            this.bnProfitCenter.MoveFirstItem = null;
            this.bnProfitCenter.MoveLastItem = null;
            this.bnProfitCenter.MoveNextItem = null;
            this.bnProfitCenter.MovePreviousItem = null;
            this.bnProfitCenter.Name = "bnProfitCenter";
            this.bnProfitCenter.PositionItem = null;
            this.bnProfitCenter.Size = new System.Drawing.Size(551, 25);
            this.bnProfitCenter.TabIndex = 1064;
            this.bnProfitCenter.Text = "BindingNavigator1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.RightToLeftAutoMirrorImage = true;
            this.btnAdd.Size = new System.Drawing.Size(23, 22);
            this.btnAdd.Text = "Add new";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save Data";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtCommissionStructure
            // 
            this.txtCommissionStructure.BackColor = System.Drawing.SystemColors.Info;
            this.txtCommissionStructure.Location = new System.Drawing.Point(344, 34);
            this.txtCommissionStructure.Name = "txtCommissionStructure";
            this.txtCommissionStructure.Size = new System.Drawing.Size(250, 20);
            this.txtCommissionStructure.TabIndex = 1065;
            this.txtCommissionStructure.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // lblCommissionStructure
            // 
            this.lblCommissionStructure.AutoSize = true;
            this.lblCommissionStructure.Location = new System.Drawing.Point(265, 37);
            this.lblCommissionStructure.Name = "lblCommissionStructure";
            this.lblCommissionStructure.Size = new System.Drawing.Size(35, 13);
            this.lblCommissionStructure.TabIndex = 1066;
            this.lblCommissionStructure.Text = "Name";
            // 
            // lblDeductionPercentage
            // 
            this.lblDeductionPercentage.AutoSize = true;
            this.lblDeductionPercentage.Location = new System.Drawing.Point(265, 63);
            this.lblDeductionPercentage.Name = "lblDeductionPercentage";
            this.lblDeductionPercentage.Size = new System.Drawing.Size(73, 13);
            this.lblDeductionPercentage.TabIndex = 1068;
            this.lblDeductionPercentage.Text = "Deduction (%)";
            // 
            // diDeductionPercentage
            // 
            // 
            // 
            // 
            this.diDeductionPercentage.BackgroundStyle.Class = "DateTimeInputBackground";
            this.diDeductionPercentage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.diDeductionPercentage.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.diDeductionPercentage.Increment = 1;
            this.diDeductionPercentage.Location = new System.Drawing.Point(344, 60);
            this.diDeductionPercentage.Name = "diDeductionPercentage";
            this.diDeductionPercentage.Size = new System.Drawing.Size(106, 20);
            this.diDeductionPercentage.TabIndex = 1067;
            this.diDeductionPercentage.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // tcCommissionStructure
            // 
            this.tcCommissionStructure.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tcCommissionStructure.Controls.Add(this.tpGroup);
            this.tcCommissionStructure.Controls.Add(this.tpIndividual);
            this.tcCommissionStructure.Location = new System.Drawing.Point(246, 86);
            this.tcCommissionStructure.Name = "tcCommissionStructure";
            this.tcCommissionStructure.SelectedIndex = 0;
            this.tcCommissionStructure.Size = new System.Drawing.Size(545, 334);
            this.tcCommissionStructure.TabIndex = 1069;
            // 
            // tpGroup
            // 
            this.tpGroup.Controls.Add(this.dgvGroupTarget);
            this.tpGroup.Location = new System.Drawing.Point(4, 22);
            this.tpGroup.Name = "tpGroup";
            this.tpGroup.Padding = new System.Windows.Forms.Padding(3);
            this.tpGroup.Size = new System.Drawing.Size(537, 308);
            this.tpGroup.TabIndex = 0;
            this.tpGroup.Text = "Group Target";
            this.tpGroup.UseVisualStyleBackColor = true;
            // 
            // dgvGroupTarget
            // 
            this.dgvGroupTarget.BackgroundColor = System.Drawing.Color.White;
            this.dgvGroupTarget.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGroupTarget.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FromPercentage,
            this.ToPercentage,
            this.Amount});
            this.dgvGroupTarget.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGroupTarget.Location = new System.Drawing.Point(3, 3);
            this.dgvGroupTarget.Name = "dgvGroupTarget";
            this.dgvGroupTarget.Size = new System.Drawing.Size(531, 302);
            this.dgvGroupTarget.TabIndex = 1059;
            this.dgvGroupTarget.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGroupTarget_CellValueChanged);
            this.dgvGroupTarget.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_EditingControlShowing);
            this.dgvGroupTarget.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_CurrentCellDirtyStateChanged);
            this.dgvGroupTarget.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // FromPercentage
            // 
            this.FromPercentage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FromPercentage.DataPropertyName = "FromPercentage";
            this.FromPercentage.HeaderText = "From";
            this.FromPercentage.MaxInputLength = 6;
            this.FromPercentage.Name = "FromPercentage";
            this.FromPercentage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ToPercentage
            // 
            this.ToPercentage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ToPercentage.DataPropertyName = "ToPercentage";
            this.ToPercentage.HeaderText = "To";
            this.ToPercentage.MaxInputLength = 6;
            this.ToPercentage.Name = "ToPercentage";
            this.ToPercentage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Amount
            // 
            this.Amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Amount.DataPropertyName = "Amount";
            this.Amount.HeaderText = "Amount";
            this.Amount.MaxInputLength = 20;
            this.Amount.Name = "Amount";
            this.Amount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tpIndividual
            // 
            this.tpIndividual.Controls.Add(this.dgvExcessTarget);
            this.tpIndividual.Location = new System.Drawing.Point(4, 22);
            this.tpIndividual.Name = "tpIndividual";
            this.tpIndividual.Padding = new System.Windows.Forms.Padding(3);
            this.tpIndividual.Size = new System.Drawing.Size(537, 308);
            this.tpIndividual.TabIndex = 1;
            this.tpIndividual.Text = "Excess Target";
            this.tpIndividual.UseVisualStyleBackColor = true;
            // 
            // dgvExcessTarget
            // 
            this.dgvExcessTarget.BackgroundColor = System.Drawing.Color.White;
            this.dgvExcessTarget.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvExcessTarget.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ExcessParameterID,
            this.ExcessTarget,
            this.Percentage});
            this.dgvExcessTarget.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExcessTarget.Location = new System.Drawing.Point(3, 3);
            this.dgvExcessTarget.Name = "dgvExcessTarget";
            this.dgvExcessTarget.Size = new System.Drawing.Size(531, 302);
            this.dgvExcessTarget.TabIndex = 1060;
            this.dgvExcessTarget.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvExcessTarget_CellValueChanged);
            this.dgvExcessTarget.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_EditingControlShowing);
            this.dgvExcessTarget.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_CurrentCellDirtyStateChanged);
            this.dgvExcessTarget.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // ExcessParameterID
            // 
            this.ExcessParameterID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExcessParameterID.DataPropertyName = "ExcessParameterID";
            this.ExcessParameterID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExcessParameterID.HeaderText = "Parameter";
            this.ExcessParameterID.Name = "ExcessParameterID";
            this.ExcessParameterID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ExcessParameterID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ExcessTarget
            // 
            this.ExcessTarget.DataPropertyName = "ExcessTarget";
            this.ExcessTarget.HeaderText = "Excess Target";
            this.ExcessTarget.MaxInputLength = 20;
            this.ExcessTarget.Name = "ExcessTarget";
            this.ExcessTarget.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Percentage
            // 
            this.Percentage.DataPropertyName = "Percentage";
            this.Percentage.HeaderText = "Percentage";
            this.Percentage.MaxInputLength = 6;
            this.Percentage.Name = "Percentage";
            this.Percentage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(249, 426);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1070;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(712, 426);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1072;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(631, 426);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1071;
            this.btnOK.Text = "Ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // tmrProfitCenter
            // 
            this.tmrProfitCenter.Interval = 1000;
            this.tmrProfitCenter.Tick += new System.EventHandler(this.tmrProfitCenter_Tick);
            // 
            // errProfitCenter
            // 
            this.errProfitCenter.ContainerControl = this;
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMain.Controls.Add(this.bnProfitCenter);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.btnOK);
            this.pnlMain.Controls.Add(this.btnSave);
            this.pnlMain.Controls.Add(this.expLeft);
            this.pnlMain.Controls.Add(this.ssProfitCenter);
            this.pnlMain.Controls.Add(this.tcCommissionStructure);
            this.pnlMain.Controls.Add(this.txtCommissionStructure);
            this.pnlMain.Controls.Add(this.lblDeductionPercentage);
            this.pnlMain.Controls.Add(this.lblCommissionStructure);
            this.pnlMain.Controls.Add(this.diDeductionPercentage);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(794, 474);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 1073;
            // 
            // frmCommissionStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 474);
            this.Controls.Add(this.pnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCommissionStructure";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Commission Structure";
            this.Load += new System.EventHandler(this.frmCommissionStructure_Load);
            this.ssProfitCenter.ResumeLayout(false);
            this.ssProfitCenter.PerformLayout();
            this.expLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnProfitCenter)).EndInit();
            this.bnProfitCenter.ResumeLayout(false);
            this.bnProfitCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.diDeductionPercentage)).EndInit();
            this.tcCommissionStructure.ResumeLayout(false);
            this.tpGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroupTarget)).EndInit();
            this.tpIndividual.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExcessTarget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProfitCenter)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.StatusStrip ssProfitCenter;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        private DevComponents.DotNetBar.ExpandablePanel expLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSearch;
        internal System.Windows.Forms.BindingNavigator bnProfitCenter;
        internal System.Windows.Forms.ToolStripButton btnAdd;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripButton btnClear;
        private System.Windows.Forms.TextBox txtCommissionStructure;
        internal System.Windows.Forms.Label lblCommissionStructure;
        internal System.Windows.Forms.Label lblDeductionPercentage;
        private DevComponents.Editors.DoubleInput diDeductionPercentage;
        private System.Windows.Forms.TabControl tcCommissionStructure;
        private System.Windows.Forms.TabPage tpGroup;
        private System.Windows.Forms.TabPage tpIndividual;
        private System.Windows.Forms.DataGridView dgvGroupTarget;
        private System.Windows.Forms.DataGridView dgvExcessTarget;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Timer tmrProfitCenter;
        private System.Windows.Forms.ErrorProvider errProfitCenter;
        private System.Windows.Forms.DataGridViewTextBoxColumn FromPercentage;
        private System.Windows.Forms.DataGridViewTextBoxColumn ToPercentage;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewComboBoxColumn ExcessParameterID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExcessTarget;
        private System.Windows.Forms.DataGridViewTextBoxColumn Percentage;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommissionStructureID;
        private System.Windows.Forms.DataGridViewTextBoxColumn StructureName;
        private DevComponents.DotNetBar.PanelEx pnlMain;
    }
}