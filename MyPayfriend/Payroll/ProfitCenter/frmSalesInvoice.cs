﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MyPayfriend
{
    public partial class frmSalesInvoice : DevComponents.DotNetBar.Office2007Form
    {
        clsBLLSalesInvoice objclsBLLSalesInvoice;
        ClsLogWriter mObjLogs;

        private string MsMessageCaption;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private ArrayList MaMessageArr;
        ClsNotification mObjNotification;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool mblnAddStatus = true;

        public frmSalesInvoice()
        {
            InitializeComponent();
            objclsBLLSalesInvoice = new clsBLLSalesInvoice();
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
        }

        private void frmSalesInvoice_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            AddNew();
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.SalesInvoice, 2);
            MsMessageCaption = ClsCommonSettings.MessageCaption;
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.SalesInvoice,
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void AddNew()
        {
            mblnAddStatus = true;
            LoadCombos(0);
            errProfitCenter.Clear();
            cboCompany.Tag = 0; // SalesInvoiceID
            cboCompany.SelectedIndex = cboProfitCenter.SelectedIndex = -1;
            dtpMonth.Value = ClsCommonSettings.GetServerDate();
            txtRemarks.Text = string.Empty;
            dgvInvoice.Rows.Clear();
            SearchInfo();

            btnClear.Enabled = true;
            btnAdd.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = btnDelete.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false;
        }

        private void LoadCombos(int intType)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0 || intType == 1)
            {
                cboCompany.DataSource = null;
                cboCompany.Text = string.Empty;

                datTemp = objclsBLLSalesInvoice.FillCombos(new string[] { "Distinct C.CompanyID,C.CompanyName", 
                    "CompanyMaster AS C INNER JOIN UserCompanyDetails AS U ON C.CompanyID = U.CompanyID", "U.UserID =" + ClsCommonSettings.UserID });

                cboCompany.DataSource = datTemp;
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";

                // Search Company
                cboSearchCompany.DataSource = null;
                cboSearchCompany.Text = string.Empty;

                DataTable datNew = datTemp.Copy();
                DataRow dr = datNew.NewRow();
                dr["CompanyID"] = 0;

                if (ClsCommonSettings.IsArabicView)
                    dr["CompanyName"] = "أي";
                else
                    dr["CompanyName"] = "All";

                datNew.Rows.InsertAt(dr, 0);
                cboSearchCompany.DataSource = datNew;
                cboSearchCompany.ValueMember = "CompanyID";
                cboSearchCompany.DisplayMember = "CompanyName";
            }

            if (intType == 0 || intType == 2)
            {
                cboProfitCenter.DataSource = null;
                cboProfitCenter.Text = string.Empty;

                datTemp = objclsBLLSalesInvoice.FillCombos(new string[] { "ProfitCenterID," + 
                    (ClsCommonSettings.IsArabicView ? "ProfitCenterArb" : "ProfitCenter") + " AS ProfitCenter", "ProfitCenterReference", "" });

                cboProfitCenter.DataSource = datTemp;
                cboProfitCenter.ValueMember = "ProfitCenterID";
                cboProfitCenter.DisplayMember = "ProfitCenter";

                // Search Profit Center
                DataTable datNew = datTemp.Copy();
                cboSearchProfitCenter.DataSource = null;
                cboSearchProfitCenter.Text = string.Empty;
                cboSearchProfitCenter.DataSource = datNew;
                cboSearchProfitCenter.ValueMember = "ProfitCenterID";
                cboSearchProfitCenter.DisplayMember = "ProfitCenter";
            }

            if (intType == 3)
            {
                datTemp = objclsBLLSalesInvoice.FillCombos(new string[] { 
                    "DISTINCT EM.EmployeeID," + 
                    (ClsCommonSettings.IsArabicView ? "EM.EmployeeFullNameArb" : "EM.EmployeeFullName") + " + ' - ' + EM.EmployeeNumber AS Name", 
                    "EmployeeMaster AS EM INNER JOIN UserCompanyDetails AS U ON EM.CompanyID = U.CompanyID " +
                    "INNER JOIN ProfitCenterTargetDetails PCTD ON EM.EmployeeID = PCTD.EmployeeID " +
                    "INNER JOIN ProfitCenterTargetMaster PCTM ON PCTD.TargetID = PCTM.TargetID", 
                    "EM.EmployeeID > 0 and U.UserID =" + ClsCommonSettings.UserID + " AND " +
                    "EM.CompanyID = " + cboCompany.SelectedValue.ToInt32() + " AND " +
                    "PCTM.ProfitCenterID = " + cboProfitCenter.SelectedValue.ToInt32() + " AND " +
                    "CONVERT(DATETIME,'01 " + dtpMonth.Value.ToString("MMM yyyy") + "',106) " +
			        "BETWEEN CONVERT(DATETIME,'01 ' + dbo.fnGetMonthDescription(MONTH(MonthFrom)) + ' ' + CAST(YEAR(MonthFrom) AS VARCHAR),106) AND " +
                    "CONVERT(DATETIME,'01 ' + dbo.fnGetMonthDescription(MONTH(MonthTo)) + ' ' + CAST(YEAR(MonthTo) AS VARCHAR),106) ORDER BY Name" });
                
                EmployeeID.ValueMember = "EmployeeID";
                EmployeeID.DisplayMember = "Name";
                EmployeeID.DataSource = datTemp;
            }

            if (intType == 0 || intType == 4)
            {
                cboSearchEmployee.DataSource = null;
                cboSearchEmployee.Text = string.Empty;

                datTemp = objclsBLLSalesInvoice.FillCombos(new string[] { 
                    "DISTINCT EM.EmployeeID," + 
                    (ClsCommonSettings.IsArabicView ? "EM.EmployeeFullNameArb" : "EM.EmployeeFullName") + " + ' - ' + EM.EmployeeNumber AS Name", 
                    "EmployeeMaster AS EM INNER JOIN UserCompanyDetails AS U ON EM.CompanyID = U.CompanyID", 
                    "EM.EmployeeID > 0 and U.UserID =" + ClsCommonSettings.UserID + " AND " +
                    "(EM.CompanyID = " + cboSearchCompany.SelectedValue.ToInt32() + 
                    " OR " + cboSearchCompany.SelectedValue.ToInt32() + " = 0) ORDER BY Name" });

                cboSearchEmployee.ValueMember = "EmployeeID";
                cboSearchEmployee.DisplayMember = "Name";
                cboSearchEmployee.DataSource = datTemp;
            }
        }

        private void SearchInfo()
        {
            dgvSearch.DataSource = objclsBLLSalesInvoice.GetSearchSalesInvoice(cboSearchCompany.SelectedValue.ToInt32(),
                cboSearchProfitCenter.SelectedValue.ToInt32(), cboSearchEmployee.SelectedValue.ToInt32(),
                dtpSearchFromDate.Value.ToString("MMM yyyy").ToDateTime(), dtpSearchToDate.Value.ToString("MMM yyyy").ToDateTime());
        }

        private void cbo_SelectedIndexChanged(object sender, EventArgs e) // for filtering employee
        {
            LoadCombos(3);
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            LoadCombos(3);
            ChangeControlStatus(sender, e);
        }

        private void cboSearchCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombos(4);
        }

        private void dgvInvoice_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvInvoice.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgvInvoice_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == EmployeeID.Index)
                {
                    int iEmpID = dgvInvoice.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToInt32();

                    if (iEmpID > 0)
                        dgvInvoice.Rows[e.RowIndex].Cells["EmployeeID"].Tag = dgvInvoice.Rows[e.RowIndex].Cells["EmployeeID"].Value;
                }

                ChangeControlStatus(sender, e);
            }
        }

        private void dgvInvoice_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvInvoice.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvInvoice.CurrentCell.OwningColumn.Index == Amount.Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                    e.Handled = true;
                else
                {
                    int dotIndex = -1;

                    if (txt.Text.Contains("."))
                        dotIndex = txt.Text.IndexOf('.');

                    if (e.KeyChar == '.')
                    {
                        if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            e.Handled = true;
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                bool blnTempSave = false;

                if (mblnAddStatus)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }

                if (blnTempSave)
                {
                    if (SaveForm())
                    {
                        if (mblnAddStatus)
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        else
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);

                        lblstatus.Text = MsMessageCommon.Split('#').Last();
                        tmrProfitCenter.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        AddNew();
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            btnSaveItem_Click(sender, e);
            this.Close();
        }

        private bool FormValidation()
        {
            errProfitCenter.Clear();
            bool blnValid = true;
            int MessageID = 0;
            Control ctrl = new Control();

            if (cboCompany.SelectedIndex == -1)
            {
                MessageID = 14;
                blnValid = false;
                errProfitCenter.SetError(cboCompany, "Mandatory");
            }

            if (blnValid && cboProfitCenter.SelectedIndex == -1)
            {
                MessageID = 21601;
                blnValid = false;
                errProfitCenter.SetError(cboProfitCenter, "Mandatory");
            }

            if (blnValid && dgvInvoice.Rows.Count == 1)
            {
                MessageID = 9124;
                blnValid = false;
            }

            if (blnValid && dgvInvoice.Rows.Count > 1)
            {
                for (int i = 0; i <= dgvInvoice.Rows.Count - 2; i++)
                {
                    if (blnValid && dgvInvoice.Rows[i].Cells["EmployeeID"].Tag.ToInt32() == 0)
                    {
                        MessageID = 9124;
                        blnValid = false;
                        dgvInvoice.CurrentCell = dgvInvoice[EmployeeID.Index, i];
                    }

                    if (blnValid && dgvInvoice.Rows[i].Cells["InvoiceNo"].Value.ToStringCustom() == string.Empty)
                    {
                        MessageID = 21602;
                        blnValid = false;
                        dgvInvoice.CurrentCell = dgvInvoice[InvoiceNo.Index, i];
                    }

                    if (blnValid && dgvInvoice.Rows[i].Cells["InvoiceDate"].Value.ToStringCustom() == string.Empty)
                    {
                        MessageID = 21603;
                        blnValid = false;
                        dgvInvoice.CurrentCell = dgvInvoice[InvoiceDate.Index, i];
                    }

                    if (blnValid && dgvInvoice.Rows[i].Cells["InvoiceDate"].Value.ToStringCustom() != string.Empty)
                    {
                        DateTime dtInvDate = dgvInvoice.Rows[i].Cells["InvoiceDate"].Value.ToDateTime();

                        if (!(dtpMonth.Value.Month == dtInvDate.Month && dtpMonth.Value.Year == dtInvDate.Year))
                        {
                            MessageID = 21605;
                            blnValid = false;
                            dgvInvoice.CurrentCell = dgvInvoice[InvoiceDate.Index, i];
                        }
                    }

                    if (blnValid && dgvInvoice.Rows[i].Cells["Amount"].Value.ToDouble() == 0)
                    {
                        MessageID = 21604;
                        blnValid = false;
                        dgvInvoice.CurrentCell = dgvInvoice[Amount.Index, i];
                    }
                }
            }

            if (!blnValid)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, MessageID, out MmessageIcon);
                lblstatus.Text = MsMessageCommon.Split('#').Last();
                tmrProfitCenter.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                return false;
            }
            else
                return true;
        }

        private bool SaveForm()
        {
            FillParameters();
            return objclsBLLSalesInvoice.SaveForm();
        }

        private void FillParameters()
        {
            objclsBLLSalesInvoice.PobjclsDTOSalesInvoice.SalesInvoiceID = cboCompany.Tag.ToInt32();
            objclsBLLSalesInvoice.PobjclsDTOSalesInvoice.CompanyID = cboCompany.SelectedValue.ToInt32();
            objclsBLLSalesInvoice.PobjclsDTOSalesInvoice.ProfitCenterID = cboProfitCenter.SelectedValue.ToInt32();
            objclsBLLSalesInvoice.PobjclsDTOSalesInvoice.Month = dtpMonth.Value.ToString("MMM yyyy").ToDateTime();
            objclsBLLSalesInvoice.PobjclsDTOSalesInvoice.Remarks = txtRemarks.Text.Trim();
            objclsBLLSalesInvoice.PobjclsDTOSalesInvoice.SalesInvoiceDetails = new List<clsDTOSalesInvoiceDetails>();

            foreach (DataGridViewRow dr in dgvInvoice.Rows)
            {
                if (dr.Cells["EmployeeID"].Tag.ToInt32() > 0)
                {
                    clsDTOSalesInvoiceDetails objSalesInvoiceDetails = new clsDTOSalesInvoiceDetails();
                    objSalesInvoiceDetails.EmployeeID = dr.Cells["EmployeeID"].Tag.ToInt32();
                    objSalesInvoiceDetails.InvoiceNo = dr.Cells["InvoiceNo"].Value.ToStringCustom();
                    objSalesInvoiceDetails.InvoiceDate = dr.Cells["InvoiceDate"].Value.ToDateTime().ToString("dd MMM yyyy").ToDateTime();
                    objSalesInvoiceDetails.Amount = dr.Cells["Amount"].Value.ToDouble();
                    objSalesInvoiceDetails.Remarks = dr.Cells["Remarks"].Value.ToStringCustom();
                    objclsBLLSalesInvoice.PobjclsDTOSalesInvoice.SalesInvoiceDetails.Add(objSalesInvoiceDetails);
                }
            }
        }

        private void tmrProfitCenter_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = string.Empty;
            tmrProfitCenter.Enabled = false;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            objclsBLLSalesInvoice.PobjclsDTOSalesInvoice.SalesInvoiceID = cboCompany.Tag.ToInt32();

            if (objclsBLLSalesInvoice.PobjclsDTOSalesInvoice.SalesInvoiceID > 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    if (objclsBLLSalesInvoice.DeleteForm())
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                        lblstatus.Text = MsMessageCommon.Split('#').Last();
                        tmrProfitCenter.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        AddNew();
                    }
                }
            }
        }

        private void dgvSearch_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int iSalesInvoiceID = dgvSearch.Rows[e.RowIndex].Cells["SalesInvoiceID"].Value.ToInt32();
                DataTable datTemp = objclsBLLSalesInvoice.DisplayInfo(iSalesInvoiceID);

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        mblnAddStatus = false;
                        cboCompany.Tag = iSalesInvoiceID;
                        cboCompany.SelectedValue = datTemp.Rows[0]["CompanyID"].ToInt32();                        
                        cboProfitCenter.SelectedValue = datTemp.Rows[0]["ProfitCenterID"].ToInt32();                        
                        dtpMonth.Value = datTemp.Rows[0]["Month"].ToDateTime();
                        LoadCombos(3);
                        txtRemarks.Text = datTemp.Rows[0]["Remarks"].ToStringCustom();
                        dgvInvoice.Rows.Clear();

                        foreach (DataRow dr in datTemp.Rows)
                        {
                            dgvInvoice.Rows.Add();
                            dgvInvoice.Rows[dgvInvoice.Rows.Count - 2].Cells["EmployeeID"].Tag = dr["EmployeeID"].ToInt32();
                            dgvInvoice.Rows[dgvInvoice.Rows.Count - 2].Cells["EmployeeID"].Value = dr["Employee"].ToStringCustom();
                            dgvInvoice.Rows[dgvInvoice.Rows.Count - 2].Cells["InvoiceNo"].Value = dr["InvoiceNo"].ToStringCustom();
                            dgvInvoice.Rows[dgvInvoice.Rows.Count - 2].Cells["InvoiceDate"].Value = dr["InvoiceDate"].ToDateTime();
                            dgvInvoice.Rows[dgvInvoice.Rows.Count - 2].Cells["Amount"].Value = dr["Amount"].ToDouble();
                            dgvInvoice.Rows[dgvInvoice.Rows.Count - 2].Cells["Remarks"].Value = dr["EmployeeRemarks"].ToStringCustom();
                        }

                        ChangeStatus();
                        btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = false; // disable in update mode. Enable these controls when edit started
                    }
                }
            }
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchInfo();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox obj = (ComboBox)sender;
            obj.DroppedDown = false;
        }

        private void ChangeControlStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void ChangeStatus()
        {
            if (mblnAddStatus)
            {
                btnAdd.Enabled = false;
                btnClear.Enabled = true;
                btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnAddPermission;
                btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            }
            else
            {
                btnAdd.Enabled = MblnAddPermission;
                btnClear.Enabled = false;
                btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnUpdatePermission;
                btnDelete.Enabled = MblnDeletePermission;
                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
            }

            errProfitCenter.Clear();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            using (FrmReportviewer obj = new FrmReportviewer())
            {
                obj.PiRecId = cboCompany.Tag.ToInt32();
                obj.PsFormName = this.Text;
                obj.PiFormID = (int)FormID.SalesInvoice;
                obj.ShowDialog();
            }
        }
    }
}