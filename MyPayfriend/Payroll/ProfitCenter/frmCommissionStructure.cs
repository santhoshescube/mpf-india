﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MyPayfriend
{
    public partial class frmCommissionStructure : DevComponents.DotNetBar.Office2007Form
    {
        clsBLLCommissionStructure objclsBLLCommissionStructure;
        ClsLogWriter mObjLogs;

        private string MsMessageCaption;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private ArrayList MaMessageArr;
        ClsNotification mObjNotification;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool mblnAddStatus = true;

        public frmCommissionStructure()
        {
            InitializeComponent();
            objclsBLLCommissionStructure = new clsBLLCommissionStructure();
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
        }

        private void frmCommissionStructure_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            AddNew();
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.CommissionStructure, 2);
            MsMessageCaption = ClsCommonSettings.MessageCaption;
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.CommissionStructure,
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void AddNew()
        {
            mblnAddStatus = true;
            LoadCombos();
            errProfitCenter.Clear();
            txtCommissionStructure.Tag = 0; // CommissionStructureID
            txtCommissionStructure.Text = string.Empty;
            diDeductionPercentage.Text = null;
            dgvGroupTarget.Rows.Clear();
            dgvExcessTarget.Rows.Clear();
            SearchInfo();

            btnClear.Enabled = true;
            btnAdd.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = btnDelete.Enabled = false;
        }

        private void LoadCombos()
        {
            ExcessParameterID.DataSource = objclsBLLCommissionStructure.FillCombos(new string[] { "ParameterID," + 
                    (ClsCommonSettings.IsArabicView ? "ParameterNameArb" : "ParameterName") + " AS Parameter", "PayParameters", "ParameterID NOT IN (6,7)" });
            ExcessParameterID.ValueMember = "ParameterID";
            ExcessParameterID.DisplayMember = "Parameter";
        }

        private void SearchInfo()
        {
            dgvSearch.DataSource = objclsBLLCommissionStructure.GetSearchCommissionStructure();
        }

        private void dgv_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            dgv.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgvGroupTarget_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
                ChangeControlStatus(sender, e);
        }

        private void dgvExcessTarget_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == ExcessParameterID.Index)
                {
                    int iParmeterID = dgvExcessTarget.Rows[e.RowIndex].Cells["ExcessParameterID"].Value.ToInt32();

                    if (iParmeterID > 0)
                        dgvExcessTarget.Rows[e.RowIndex].Cells["ExcessParameterID"].Tag = dgvExcessTarget.Rows[e.RowIndex].Cells["ExcessParameterID"].Value;                    
                }

                ChangeControlStatus(sender, e);
            }
        }

        private void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            dgv.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvGroupTarget.CurrentCell.OwningColumn.Index == FromPercentage.Index ||
                dgvGroupTarget.CurrentCell.OwningColumn.Index == ToPercentage.Index ||
                dgvGroupTarget.CurrentCell.OwningColumn.Index == Amount.Index ||
                dgvExcessTarget.CurrentCell.OwningColumn.Index == ExcessTarget.Index ||
                dgvExcessTarget.CurrentCell.OwningColumn.Index == Percentage.Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                    e.Handled = true;
                else
                {
                    int dotIndex = -1;

                    if (txt.Text.Contains("."))
                        dotIndex = txt.Text.IndexOf('.');

                    if (e.KeyChar == '.')
                    {
                        if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            e.Handled = true;
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                bool blnTempSave = false;

                if (mblnAddStatus)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }

                if (blnTempSave)
                {
                    if (SaveForm())
                    {
                        if (mblnAddStatus)
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        else
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);

                        lblstatus.Text = MsMessageCommon.Split('#').Last();
                        tmrProfitCenter.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        AddNew();
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            btnSaveItem_Click(sender, e);
            this.Close();
        }

        private bool FormValidation()
        {
            errProfitCenter.Clear();
            bool blnValid = true;
            int MessageID = 0;
            Control ctrl = new Control();

            if (txtCommissionStructure.Text.Trim() == string.Empty)
            {
                MessageID = 21501;
                blnValid = false;
                errProfitCenter.SetError(txtCommissionStructure, "Mandatory");
            }

            if (blnValid && dgvGroupTarget.Rows.Count == 1)
            {
                MessageID = 21502;
                blnValid = false;
            }

            if (blnValid && dgvGroupTarget.Rows.Count > 1)
            {
                for (int i = 0; i <= dgvGroupTarget.Rows.Count - 2; i++)
                {
                    if (blnValid && dgvGroupTarget.Rows[i].Cells["FromPercentage"].Value.ToDouble() == 0)
                    {
                        MessageID = 21503;
                        blnValid = false;
                        dgvGroupTarget.CurrentCell = dgvGroupTarget[FromPercentage.Index, i];
                    }

                    if (blnValid && dgvGroupTarget.Rows[i].Cells["ToPercentage"].Value.ToDouble() == 0)
                    {
                        MessageID = 21503;
                        blnValid = false;
                        dgvGroupTarget.CurrentCell = dgvGroupTarget[ToPercentage.Index, i];
                    }

                    if (blnValid && dgvGroupTarget.Rows[i].Cells["Amount"].Value.ToDouble() == 0)
                    {
                        MessageID = 21504;
                        blnValid = false;
                        dgvGroupTarget.CurrentCell = dgvGroupTarget[Amount.Index, i];
                    }
                }
            }

            if (blnValid && dgvExcessTarget.Rows.Count == 1)
            {
                MessageID = 21506;
                blnValid = false;
            }

            if (blnValid && dgvExcessTarget.Rows.Count > 1)
            {
                for (int i = 0; i <= dgvExcessTarget.Rows.Count - 2; i++)
                {
                    if (blnValid && dgvExcessTarget.Rows[i].Cells["ExcessParameterID"].Tag.ToInt32() == 0)
                    {
                        MessageID = 21505;
                        blnValid = false;
                        dgvExcessTarget.CurrentCell = dgvExcessTarget[ExcessParameterID.Index, i];
                    }

                    if (blnValid && dgvExcessTarget.Rows[i].Cells["ExcessTarget"].Value.ToDouble() == 0)
                    {
                        MessageID = 21506;
                        blnValid = false;
                        dgvExcessTarget.CurrentCell = dgvExcessTarget[ExcessTarget.Index, i];
                    }

                    if (blnValid && dgvExcessTarget.Rows[i].Cells["Percentage"].Value.ToDouble() == 0)
                    {
                        MessageID = 21507;
                        blnValid = false;
                        dgvExcessTarget.CurrentCell = dgvExcessTarget[Percentage.Index, i];
                    }
                }
            }

            if (!blnValid)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, MessageID, out MmessageIcon);
                lblstatus.Text = MsMessageCommon.Split('#').Last();
                tmrProfitCenter.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                return false;
            }
            else
                return true;
        }

        private bool SaveForm()
        {
            FillParameters();
            return objclsBLLCommissionStructure.SaveForm();
        }

        private void FillParameters()
        {
            objclsBLLCommissionStructure.PobjclsDTOCommissionStructure.CommissionStructureID = txtCommissionStructure.Tag.ToInt32();
            objclsBLLCommissionStructure.PobjclsDTOCommissionStructure.StructureName = txtCommissionStructure.Text.Trim();
            objclsBLLCommissionStructure.PobjclsDTOCommissionStructure.Deduction = diDeductionPercentage.Value;
            objclsBLLCommissionStructure.PobjclsDTOCommissionStructure.CommissionStructureGroupDetails = new List<clsDTOCommissionStructureGroupDetails>();
            objclsBLLCommissionStructure.PobjclsDTOCommissionStructure.CommissionStructureExcessTarget = new List<clsDTOCommissionStructureExcessTarget>();

            foreach (DataGridViewRow dr in dgvGroupTarget.Rows)
            {
                if (dr.Cells["FromPercentage"].Value.ToDouble() > 0)
                {
                    clsDTOCommissionStructureGroupDetails objCommissionStructureGroupDetails = new clsDTOCommissionStructureGroupDetails();
                    objCommissionStructureGroupDetails.FromPercentage = dr.Cells["FromPercentage"].Value.ToDouble();
                    objCommissionStructureGroupDetails.ToPercentage = dr.Cells["ToPercentage"].Value.ToDouble();
                    objCommissionStructureGroupDetails.Amount = dr.Cells["Amount"].Value.ToDouble();
                    objclsBLLCommissionStructure.PobjclsDTOCommissionStructure.CommissionStructureGroupDetails.Add(objCommissionStructureGroupDetails);
                }
            }

            foreach (DataGridViewRow dr in dgvExcessTarget.Rows)
            {
                if (dr.Cells["ExcessParameterID"].Tag.ToInt32() > 0)
                {
                    clsDTOCommissionStructureExcessTarget objCommissionStructureExcessTarget = new clsDTOCommissionStructureExcessTarget();
                    objCommissionStructureExcessTarget.ParameterID = dr.Cells["ExcessParameterID"].Tag.ToInt32();
                    objCommissionStructureExcessTarget.ExcessTarget = dr.Cells["ExcessTarget"].Value.ToDouble();
                    objCommissionStructureExcessTarget.Percentage = dr.Cells["Percentage"].Value.ToDouble();
                    objclsBLLCommissionStructure.PobjclsDTOCommissionStructure.CommissionStructureExcessTarget.Add(objCommissionStructureExcessTarget);
                }
            }
        }

        private void tmrProfitCenter_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = string.Empty;
            tmrProfitCenter.Enabled = false;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            objclsBLLCommissionStructure.PobjclsDTOCommissionStructure.CommissionStructureID = txtCommissionStructure.Tag.ToInt32();

            if (objclsBLLCommissionStructure.PobjclsDTOCommissionStructure.CommissionStructureID > 0)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    if (objclsBLLCommissionStructure.DeleteForm())
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                        lblstatus.Text = MsMessageCommon.Split('#').Last();
                        tmrProfitCenter.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        AddNew();
                    }
                }
            }
        }

        private void dgvSearch_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int iCommissionStructureID = dgvSearch.Rows[e.RowIndex].Cells["CommissionStructureID"].Value.ToInt32();
                DataSet dsTemp = objclsBLLCommissionStructure.DisplayInfo(iCommissionStructureID);

                if (dsTemp != null)
                {
                    if (dsTemp.Tables.Count > 0)
                    {
                        mblnAddStatus = false;

                        if (dsTemp.Tables[0] != null)
                        {
                            if (dsTemp.Tables[0].Rows.Count > 0)
                            {                                
                                txtCommissionStructure.Tag = iCommissionStructureID;
                                txtCommissionStructure.Text = dsTemp.Tables[0].Rows[0]["StructureName"].ToStringCustom();
                                diDeductionPercentage.Value = dsTemp.Tables[0].Rows[0]["Deduction"].ToDouble();                                
                            }
                        }

                        dgvGroupTarget.Rows.Clear();

                        if (dsTemp.Tables[1] != null)
                        {
                            if (dsTemp.Tables[1].Rows.Count > 0)
                            {
                                foreach (DataRow dr in dsTemp.Tables[1].Rows)
                                {
                                    dgvGroupTarget.Rows.Add();
                                    dgvGroupTarget.Rows[dgvGroupTarget.Rows.Count - 2].Cells["FromPercentage"].Value = dr["FromPercentage"].ToDouble();
                                    dgvGroupTarget.Rows[dgvGroupTarget.Rows.Count - 2].Cells["ToPercentage"].Value = dr["ToPercentage"].ToDouble();
                                    dgvGroupTarget.Rows[dgvGroupTarget.Rows.Count - 2].Cells["Amount"].Value = dr["Amount"].ToDouble();
                                }
                            }
                        }

                        dgvExcessTarget.Rows.Clear();

                        if (dsTemp.Tables[2] != null)
                        {
                            if (dsTemp.Tables[2].Rows.Count > 0)
                            {
                                foreach (DataRow dr in dsTemp.Tables[2].Rows)
                                {
                                    dgvExcessTarget.Rows.Add();
                                    dgvExcessTarget.Rows[dgvExcessTarget.Rows.Count - 2].Cells["ExcessParameterID"].Tag = dr["ParameterID"].ToInt32();
                                    dgvExcessTarget.Rows[dgvExcessTarget.Rows.Count - 2].Cells["ExcessParameterID"].Value = dr["ParameterName"].ToStringCustom();
                                    dgvExcessTarget.Rows[dgvExcessTarget.Rows.Count - 2].Cells["ExcessTarget"].Value = dr["ExcessTarget"].ToDouble();
                                    dgvExcessTarget.Rows[dgvExcessTarget.Rows.Count - 2].Cells["Percentage"].Value = dr["Percentage"].ToDouble();
                                }
                            }
                        }

                        ChangeStatus();
                        btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = false; // disable in update mode. Enable these controls when edit started
                    }
                }
            }
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangeControlStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void ChangeStatus()
        {
            if (mblnAddStatus)
            {
                btnAdd.Enabled = false;
                btnClear.Enabled = true;
                btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnAddPermission;
                btnDelete.Enabled = false;
            }
            else
            {
                btnAdd.Enabled = MblnAddPermission;
                btnClear.Enabled = false;
                btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnUpdatePermission;
                btnDelete.Enabled = MblnDeletePermission;
            }

            errProfitCenter.Clear();
        }
    }
}