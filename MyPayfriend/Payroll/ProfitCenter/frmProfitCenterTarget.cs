﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MyPayfriend
{
    public partial class frmProfitCenterTarget : DevComponents.DotNetBar.Office2007Form
    {
        clsBLLProfitCenterTarget objclsBLLProfitCenterTarget;
        ClsLogWriter mObjLogs;
        private string MsMessageCaption;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private ArrayList MaMessageArr;
        ClsNotification mObjNotification;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool mblnAddStatus = true;

        public frmProfitCenterTarget()
        {
            InitializeComponent();
            objclsBLLProfitCenterTarget = new clsBLLProfitCenterTarget();
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
        }

        private void frmProfitCenterTarget_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            AddNew();
            diTarget.DisplayFormat = new string('#', 14) + "." + new string('#', ClsCommonSettings.Scale);
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.ProfitCenterTarget, 2);
            MsMessageCaption = ClsCommonSettings.MessageCaption;
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.ProfitCenterTarget,
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void AddNew()
        {
            mblnAddStatus = true;
            LoadCombos(0);
            errProfitCenter.Clear();
            cboCompany.Tag = 0; // TargetID
            cboCompany.SelectedIndex = cboProfitCenter.SelectedIndex = -1;
            dtpPeriodFrom.Value = dtpPeriodTo.Value = ClsCommonSettings.GetServerDate();
            diTarget.Text = null;
            txtRemarks.Text = string.Empty;
            dgvEmployee.Rows.Clear();
            SearchInfo();

            btnClear.Enabled = true;
            btnAdd.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = btnDelete.Enabled = false;
        }

        private void LoadCombos(int intType)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0 || intType == 1)
            {
                cboCompany.DataSource = null;
                cboCompany.Text = string.Empty;

                datTemp = objclsBLLProfitCenterTarget.FillCombos(new string[] { "Distinct C.CompanyID,C.CompanyName", 
                    "CompanyMaster AS C INNER JOIN UserCompanyDetails AS U ON C.CompanyID = U.CompanyID", "U.UserID =" + ClsCommonSettings.UserID });

                cboCompany.DataSource = datTemp;                
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";

                // Search Company
                cboSearchCompany.DataSource = null;
                cboSearchCompany.Text = string.Empty;

                DataTable datNew = datTemp.Copy();
                DataRow dr = datNew.NewRow();
                dr["CompanyID"] = 0;

                if (ClsCommonSettings.IsArabicView)
                    dr["CompanyName"] = "أي";
                else
                    dr["CompanyName"] = "All";

                datNew.Rows.InsertAt(dr, 0);
                cboSearchCompany.DataSource = datNew;                
                cboSearchCompany.ValueMember = "CompanyID";
                cboSearchCompany.DisplayMember = "CompanyName";
            }

            if (intType == 0 || intType == 2)
            {
                cboProfitCenter.DataSource = null;
                cboProfitCenter.Text = string.Empty;

                datTemp = objclsBLLProfitCenterTarget.FillCombos(new string[] { "ProfitCenterID," + 
                    (ClsCommonSettings.IsArabicView ? "ProfitCenterArb" : "ProfitCenter") + " AS ProfitCenter", "ProfitCenterReference", "" });

                cboProfitCenter.DataSource = datTemp;
                cboProfitCenter.ValueMember = "ProfitCenterID";
                cboProfitCenter.DisplayMember = "ProfitCenter";

                // Search Profit Center
                DataTable datNew = datTemp.Copy();
                cboSearchProfitCenter.DataSource = null;
                cboSearchProfitCenter.Text = string.Empty;
                cboSearchProfitCenter.DataSource = datNew;
                cboSearchProfitCenter.ValueMember = "ProfitCenterID";
                cboSearchProfitCenter.DisplayMember = "ProfitCenter";
            }

            if (intType == 3)
            {
                datTemp = objclsBLLProfitCenterTarget.FillCombos(new string[] { 
                    "DISTINCT EM.EmployeeID," + 
                    (ClsCommonSettings.IsArabicView ? "EM.EmployeeFullNameArb" : "EM.EmployeeFullName") + " + ' - ' + EM.EmployeeNumber AS Name", 
                    "EmployeeMaster AS EM INNER JOIN UserCompanyDetails AS U ON EM.CompanyID = U.CompanyID", 
                    "EM.EmployeeID > 0 and U.UserID =" + ClsCommonSettings.UserID + " AND " +
                    "EM.CompanyID = " + cboCompany.SelectedValue.ToInt32() + " ORDER BY Name" });

                EmployeeID.ValueMember = "EmployeeID";
                EmployeeID.DisplayMember = "Name";
                EmployeeID.DataSource = datTemp;
            }

            if (intType == 0 || intType == 4)
            {
                cboSearchEmployee.DataSource = null;
                cboSearchEmployee.Text = string.Empty;

                datTemp = objclsBLLProfitCenterTarget.FillCombos(new string[] { 
                    "DISTINCT EM.EmployeeID," + 
                    (ClsCommonSettings.IsArabicView ? "EM.EmployeeFullNameArb" : "EM.EmployeeFullName") + " + ' - ' + EM.EmployeeNumber AS Name", 
                    "EmployeeMaster AS EM INNER JOIN UserCompanyDetails AS U ON EM.CompanyID = U.CompanyID", 
                    "EM.EmployeeID > 0 and U.UserID =" + ClsCommonSettings.UserID + " AND " +
                    "(EM.CompanyID = " + cboSearchCompany.SelectedValue.ToInt32() + 
                    " OR " + cboSearchCompany.SelectedValue.ToInt32() + " = 0) ORDER BY Name" });

                cboSearchEmployee.ValueMember = "EmployeeID";
                cboSearchEmployee.DisplayMember = "Name";
                cboSearchEmployee.DataSource = datTemp;
            }
        }

        private void SearchInfo()
        {
            dgvSearch.DataSource = objclsBLLProfitCenterTarget.GetSearchProfitCenterTarget(cboSearchCompany.SelectedValue.ToInt32(),
                cboSearchProfitCenter.SelectedValue.ToInt32(), cboSearchEmployee.SelectedValue.ToInt32(), 
                dtpSearchFromDate.Value.ToString("MMM yyyy").ToDateTime(), dtpSearchToDate.Value.ToString("MMM yyyy").ToDateTime());
        }

        private void btnProfitCenter_Click(object sender, EventArgs e)
        {
            try
            {
                int ComboID = Convert.ToInt32(cboProfitCenter.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("ProfitCenter", new int[] { 1, 0, 0 }, 
                    "ProfitCenterID,ProfitCenter,ProfitCenterArb", "ProfitCenterReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(2);

                if (objCommon.NewID != 0)
                    cboProfitCenter.SelectedValue = objCommon.NewID;
                else
                    cboProfitCenter.SelectedValue = ComboID;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on btnProfitCenter_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnProfitCenter_Click" + ex.Message.ToString());
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombos(3);
        }

        private void cboSearchCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombos(4);
        }

        private void dgvEmployee_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgvEmployee_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == EmployeeID.Index)
                {
                    int iEmpID = dgvEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToInt32();

                    if (iEmpID > 0)
                        dgvEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Tag = dgvEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Value;

                    dgvEmployee.Rows[e.RowIndex].Cells["Target"].Value = diTarget.Value;                    
                }

                ChangeControlStatus(sender, e);
            }
        }

        private void dgvEmployee_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvEmployee.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvEmployee.CurrentCell.OwningColumn.Index == Target.Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;

                if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                    e.Handled = true;
                else
                {
                    int dotIndex = -1;

                    if (txt.Text.Contains("."))
                        dotIndex = txt.Text.IndexOf('.');

                    if (e.KeyChar == '.')
                    {
                        if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                            e.Handled = true;
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                bool blnTempSave = false;

                if (mblnAddStatus)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        blnTempSave = true;
                }

                if (blnTempSave)
                {
                    if (SaveForm())
                    {
                        if (mblnAddStatus)
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        else
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);

                        lblstatus.Text = MsMessageCommon.Split('#').Last();
                        tmrProfitCenter.Enabled = true;
                        MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        AddNew();
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            btnSaveItem_Click(sender, e);
            this.Close();
        }

        private bool FormValidation()
        {
            errProfitCenter.Clear();
            bool blnValid = true;
            int MessageID = 0;
            Control ctrl = new Control();

            if (!mblnAddStatus)
            {
                if (objclsBLLProfitCenterTarget.CheckProfitCenterExists(cboCompany.Tag.ToInt32()))
                {
                    MessageID = 21404;
                    blnValid = false;
                }
            }

            if (blnValid && cboCompany.SelectedIndex == -1)
            {
                MessageID = 14;
                blnValid = false;
                errProfitCenter.SetError(cboCompany, "Mandatory");
            }

            if (blnValid && cboProfitCenter.SelectedIndex == -1)
            {
                MessageID = 21401;
                blnValid = false;
                errProfitCenter.SetError(cboProfitCenter, "Mandatory");
            }

            if (blnValid && diTarget.Value == 0)
            {
                MessageID = 21402;
                blnValid = false;
                errProfitCenter.SetError(diTarget, "Mandatory");
            }

            if (blnValid && dgvEmployee.Rows.Count == 1)
            {
                MessageID = 9124;
                blnValid = false;
            }

            if (blnValid && dgvEmployee.Rows.Count > 1)
            {
                for (int i = 0; i <= dgvEmployee.Rows.Count - 2; i++)
                {
                    if (blnValid && dgvEmployee.Rows[i].Cells["EmployeeID"].Tag.ToInt32() == 0)
                    {
                        MessageID = 9124;
                        blnValid = false;
                        dgvEmployee.CurrentCell = dgvEmployee[EmployeeID.Index, i];
                    }

                    if (blnValid && dgvEmployee.Rows[i].Cells["Target"].Value.ToDouble() == 0)
                    {
                        MessageID = 21402;
                        blnValid = false;
                        dgvEmployee.CurrentCell = dgvEmployee[Target.Index, i];
                    }

                    // Employee duplication
                    for (int j = 0; j <= dgvEmployee.Rows.Count - 2; j++)
                    {
                        if (j != i)
                        {
                            if (dgvEmployee.Rows[j].Cells["EmployeeID"].Tag.ToInt32() == dgvEmployee.Rows[i].Cells["EmployeeID"].Tag.ToInt32())
                            {
                                MessageID = 31;
                                blnValid = false;
                                dgvEmployee.CurrentCell = dgvEmployee[EmployeeID.Index, i];
                            }
                        }
                    }
                }
            }

            if (!blnValid)
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, MessageID, out MmessageIcon);
                lblstatus.Text = MsMessageCommon.Split('#').Last();
                tmrProfitCenter.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);                
                return false;
            }
            else
                return true;
        }

        private bool SaveForm()
        {
            FillParameters();
            return objclsBLLProfitCenterTarget.SaveForm();
        }

        private void FillParameters()
        {
            objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.TargetID = cboCompany.Tag.ToInt32();
            objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.CompanyID = cboCompany.SelectedValue.ToInt32();
            objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.ProfitCenterID = cboProfitCenter.SelectedValue.ToInt32();
            objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.MonthFrom = dtpPeriodFrom.Value.ToString("MMM yyyy").ToDateTime();
            objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.MonthTo = dtpPeriodTo.Value.ToString("MMM yyyy").ToDateTime();
            objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.Target = diTarget.Value;
            objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.Remarks = txtRemarks.Text.Trim();
            objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.ProfitCenterTargetDetails = new List<clsDTOProfitCenterTargetDetails>();

            foreach (DataGridViewRow dr in dgvEmployee.Rows)
            {
                if (dr.Cells["EmployeeID"].Tag.ToInt32() > 0)
                {
                    clsDTOProfitCenterTargetDetails objProfitCenterTargetDetails = new clsDTOProfitCenterTargetDetails();
                    objProfitCenterTargetDetails.EmployeeID = dr.Cells["EmployeeID"].Tag.ToInt32();
                    objProfitCenterTargetDetails.Target = dr.Cells["Target"].Value.ToDouble();
                    objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.ProfitCenterTargetDetails.Add(objProfitCenterTargetDetails);
                }
            }
        }

        private void tmrProfitCenter_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = string.Empty;
            tmrProfitCenter.Enabled = false;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.TargetID = cboCompany.Tag.ToInt32();

            if (objclsBLLProfitCenterTarget.CheckProfitCenterExists(objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.TargetID))
            {
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 21405, out MmessageIcon);
                lblstatus.Text = MsMessageCommon.Split('#').Last();
                tmrProfitCenter.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            }
            else
            {
                if (objclsBLLProfitCenterTarget.PobjclsDTOProfitCenterTarget.TargetID > 0)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        if (objclsBLLProfitCenterTarget.DeleteForm())
                        {
                            MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                            lblstatus.Text = MsMessageCommon.Split('#').Last();
                            tmrProfitCenter.Enabled = true;
                            MessageBox.Show(MsMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            AddNew();
                        }
                    }
                }
            }
        }

        private void dgvSearch_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int iTargetID = dgvSearch.Rows[e.RowIndex].Cells["TargetID"].Value.ToInt32();
                DataTable datTemp = objclsBLLProfitCenterTarget.DisplayInfo(iTargetID);

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        mblnAddStatus = false;
                        cboCompany.Tag = iTargetID;
                        cboCompany.SelectedValue = datTemp.Rows[0]["CompanyID"].ToInt32();
                        LoadCombos(3);
                        cboProfitCenter.SelectedValue = datTemp.Rows[0]["ProfitCenterID"].ToInt32();
                        dtpPeriodFrom.Value = datTemp.Rows[0]["MonthFrom"].ToDateTime();
                        dtpPeriodTo.Value = datTemp.Rows[0]["MonthTo"].ToDateTime();
                        diTarget.Value = datTemp.Rows[0]["Target"].ToDouble();
                        txtRemarks.Text = datTemp.Rows[0]["Remarks"].ToStringCustom();
                        dgvEmployee.Rows.Clear();

                        foreach (DataRow dr in datTemp.Rows)
                        {
                            dgvEmployee.Rows.Add();
                            dgvEmployee.Rows[dgvEmployee.Rows.Count - 2].Cells["EmployeeID"].Tag = dr["EmployeeID"].ToInt32();
                            dgvEmployee.Rows[dgvEmployee.Rows.Count - 2].Cells["EmployeeID"].Value = dr["Employee"].ToStringCustom();
                            dgvEmployee.Rows[dgvEmployee.Rows.Count - 2].Cells["Target"].Value = dr["EmployeeTarget"].ToDouble();
                        }

                        ChangeStatus();
                        btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = false; // disable in update mode. Enable these controls when edit started
                    }
                }
            }
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchInfo();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox obj = (ComboBox)sender;
            obj.DroppedDown = false;
        }

        private void ChangeControlStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void ChangeStatus()
        {
            if (mblnAddStatus)
            {
                btnAdd.Enabled = false;
                btnClear.Enabled = true;
                btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnAddPermission;
                btnDelete.Enabled = false;
            }
            else
            {
                btnAdd.Enabled = MblnAddPermission;
                btnClear.Enabled = false;
                btnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnUpdatePermission;
                btnDelete.Enabled = MblnDeletePermission;
            }

            errProfitCenter.Clear();
        }
    }
}