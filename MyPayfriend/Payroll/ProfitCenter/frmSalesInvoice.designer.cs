﻿namespace MyPayfriend
{
    partial class frmSalesInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSalesInvoice));
            this.ssProfitCenter = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.expLeft = new DevComponents.DotNetBar.ExpandablePanel();
            this.dgvSearch = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.SalesInvoiceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProfitCenterID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProfitCenter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Month = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.pnlTopSearch = new DevComponents.DotNetBar.PanelEx();
            this.lblSearchCompany = new System.Windows.Forms.Label();
            this.cboSearchCompany = new System.Windows.Forms.ComboBox();
            this.lblSearchEmployee = new System.Windows.Forms.Label();
            this.cboSearchEmployee = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblSearchTo = new System.Windows.Forms.Label();
            this.lblSearchFrom = new System.Windows.Forms.Label();
            this.dtpSearchToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpSearchFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblSearchProfitCenter = new System.Windows.Forms.Label();
            this.cboSearchProfitCenter = new System.Windows.Forms.ComboBox();
            this.bnProfitCenter = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.lblMonth = new System.Windows.Forms.Label();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.lblProfitCenter = new System.Windows.Forms.Label();
            this.cboProfitCenter = new System.Windows.Forms.ComboBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.dgvInvoice = new System.Windows.Forms.DataGridView();
            this.EmployeeID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.InvoiceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceDate = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblCompany = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.tmrProfitCenter = new System.Windows.Forms.Timer(this.components);
            this.errProfitCenter = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.ssProfitCenter.SuspendLayout();
            this.expLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).BeginInit();
            this.pnlTopSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnProfitCenter)).BeginInit();
            this.bnProfitCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProfitCenter)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // ssProfitCenter
            // 
            this.ssProfitCenter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.ssProfitCenter.Location = new System.Drawing.Point(0, 502);
            this.ssProfitCenter.Name = "ssProfitCenter";
            this.ssProfitCenter.Size = new System.Drawing.Size(1254, 22);
            this.ssProfitCenter.TabIndex = 1048;
            this.ssProfitCenter.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // expLeft
            // 
            this.expLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.expLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expLeft.Controls.Add(this.dgvSearch);
            this.expLeft.Controls.Add(this.pnlTopSearch);
            this.expLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.expLeft.ExpandButtonVisible = false;
            this.expLeft.Location = new System.Drawing.Point(0, 0);
            this.expLeft.Name = "expLeft";
            this.expLeft.Size = new System.Drawing.Size(266, 502);
            this.expLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expLeft.Style.GradientAngle = 90;
            this.expLeft.TabIndex = 1049;
            this.expLeft.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expLeft.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expLeft.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expLeft.TitleStyle.GradientAngle = 90;
            this.expLeft.TitleText = "Search";
            // 
            // dgvSearch
            // 
            this.dgvSearch.AllowUserToAddRows = false;
            this.dgvSearch.AllowUserToDeleteRows = false;
            this.dgvSearch.AllowUserToResizeColumns = false;
            this.dgvSearch.AllowUserToResizeRows = false;
            this.dgvSearch.BackgroundColor = System.Drawing.Color.White;
            this.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SalesInvoiceID,
            this.ProfitCenterID,
            this.ProfitCenter,
            this.Month});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSearch.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSearch.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSearch.Location = new System.Drawing.Point(0, 163);
            this.dgvSearch.Name = "dgvSearch";
            this.dgvSearch.ReadOnly = true;
            this.dgvSearch.RowHeadersVisible = false;
            this.dgvSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSearch.Size = new System.Drawing.Size(266, 339);
            this.dgvSearch.TabIndex = 1;
            this.dgvSearch.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSearch_CellDoubleClick);
            this.dgvSearch.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // SalesInvoiceID
            // 
            this.SalesInvoiceID.DataPropertyName = "SalesInvoiceID";
            this.SalesInvoiceID.HeaderText = "SalesInvoiceID";
            this.SalesInvoiceID.Name = "SalesInvoiceID";
            this.SalesInvoiceID.ReadOnly = true;
            this.SalesInvoiceID.Visible = false;
            // 
            // ProfitCenterID
            // 
            this.ProfitCenterID.DataPropertyName = "ProfitCenterID";
            this.ProfitCenterID.HeaderText = "ProfitCenterID";
            this.ProfitCenterID.Name = "ProfitCenterID";
            this.ProfitCenterID.ReadOnly = true;
            this.ProfitCenterID.Visible = false;
            // 
            // ProfitCenter
            // 
            this.ProfitCenter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProfitCenter.DataPropertyName = "ProfitCenter";
            this.ProfitCenter.HeaderText = "Profit Center";
            this.ProfitCenter.Name = "ProfitCenter";
            this.ProfitCenter.ReadOnly = true;
            // 
            // Month
            // 
            this.Month.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            // 
            // 
            // 
            this.Month.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.Month.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Month.CustomFormat = "MMM yyyy";
            this.Month.DataPropertyName = "Month";
            this.Month.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.Month.HeaderText = "Month";
            this.Month.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.Month.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.Month.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.Month.MonthCalendar.BackgroundStyle.Class = "";
            this.Month.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.Month.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.Month.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Month.MonthCalendar.DisplayMonth = new System.DateTime(2014, 12, 1, 0, 0, 0, 0);
            this.Month.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.Month.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.Month.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.Month.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Month.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.Month.Name = "Month";
            this.Month.ReadOnly = true;
            this.Month.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // pnlTopSearch
            // 
            this.pnlTopSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlTopSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlTopSearch.Controls.Add(this.lblSearchCompany);
            this.pnlTopSearch.Controls.Add(this.cboSearchCompany);
            this.pnlTopSearch.Controls.Add(this.lblSearchEmployee);
            this.pnlTopSearch.Controls.Add(this.cboSearchEmployee);
            this.pnlTopSearch.Controls.Add(this.btnSearch);
            this.pnlTopSearch.Controls.Add(this.lblSearchTo);
            this.pnlTopSearch.Controls.Add(this.lblSearchFrom);
            this.pnlTopSearch.Controls.Add(this.dtpSearchToDate);
            this.pnlTopSearch.Controls.Add(this.dtpSearchFromDate);
            this.pnlTopSearch.Controls.Add(this.lblSearchProfitCenter);
            this.pnlTopSearch.Controls.Add(this.cboSearchProfitCenter);
            this.pnlTopSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopSearch.Location = new System.Drawing.Point(0, 26);
            this.pnlTopSearch.Name = "pnlTopSearch";
            this.pnlTopSearch.Size = new System.Drawing.Size(266, 137);
            this.pnlTopSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlTopSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlTopSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlTopSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlTopSearch.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlTopSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlTopSearch.Style.GradientAngle = 90;
            this.pnlTopSearch.TabIndex = 2;
            // 
            // lblSearchCompany
            // 
            this.lblSearchCompany.AutoSize = true;
            this.lblSearchCompany.Location = new System.Drawing.Point(8, 7);
            this.lblSearchCompany.Name = "lblSearchCompany";
            this.lblSearchCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSearchCompany.TabIndex = 1068;
            this.lblSearchCompany.Text = "Company";
            // 
            // cboSearchCompany
            // 
            this.cboSearchCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchCompany.DropDownHeight = 134;
            this.cboSearchCompany.FormattingEnabled = true;
            this.cboSearchCompany.IntegralHeight = false;
            this.cboSearchCompany.Location = new System.Drawing.Point(78, 4);
            this.cboSearchCompany.Name = "cboSearchCompany";
            this.cboSearchCompany.Size = new System.Drawing.Size(181, 21);
            this.cboSearchCompany.TabIndex = 1067;
            this.cboSearchCompany.SelectedIndexChanged += new System.EventHandler(this.cboSearchCompany_SelectedIndexChanged);
            this.cboSearchCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblSearchEmployee
            // 
            this.lblSearchEmployee.AutoSize = true;
            this.lblSearchEmployee.Location = new System.Drawing.Point(7, 61);
            this.lblSearchEmployee.Name = "lblSearchEmployee";
            this.lblSearchEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblSearchEmployee.TabIndex = 225;
            this.lblSearchEmployee.Text = "Employee";
            // 
            // cboSearchEmployee
            // 
            this.cboSearchEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchEmployee.DropDownHeight = 134;
            this.cboSearchEmployee.FormattingEnabled = true;
            this.cboSearchEmployee.IntegralHeight = false;
            this.cboSearchEmployee.Location = new System.Drawing.Point(78, 58);
            this.cboSearchEmployee.Name = "cboSearchEmployee";
            this.cboSearchEmployee.Size = new System.Drawing.Size(181, 21);
            this.cboSearchEmployee.TabIndex = 224;
            this.cboSearchEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(199, 110);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(60, 23);
            this.btnSearch.TabIndex = 223;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblSearchTo
            // 
            this.lblSearchTo.AutoSize = true;
            this.lblSearchTo.Location = new System.Drawing.Point(8, 115);
            this.lblSearchTo.Name = "lblSearchTo";
            this.lblSearchTo.Size = new System.Drawing.Size(20, 13);
            this.lblSearchTo.TabIndex = 218;
            this.lblSearchTo.Text = "To";
            // 
            // lblSearchFrom
            // 
            this.lblSearchFrom.AutoSize = true;
            this.lblSearchFrom.Location = new System.Drawing.Point(7, 89);
            this.lblSearchFrom.Name = "lblSearchFrom";
            this.lblSearchFrom.Size = new System.Drawing.Size(30, 13);
            this.lblSearchFrom.TabIndex = 217;
            this.lblSearchFrom.Text = "From";
            // 
            // dtpSearchToDate
            // 
            this.dtpSearchToDate.CustomFormat = "MMM yyyy";
            this.dtpSearchToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSearchToDate.Location = new System.Drawing.Point(78, 111);
            this.dtpSearchToDate.Name = "dtpSearchToDate";
            this.dtpSearchToDate.Size = new System.Drawing.Size(106, 20);
            this.dtpSearchToDate.TabIndex = 216;
            // 
            // dtpSearchFromDate
            // 
            this.dtpSearchFromDate.CustomFormat = "MMM yyyy";
            this.dtpSearchFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSearchFromDate.Location = new System.Drawing.Point(78, 85);
            this.dtpSearchFromDate.Name = "dtpSearchFromDate";
            this.dtpSearchFromDate.Size = new System.Drawing.Size(106, 20);
            this.dtpSearchFromDate.TabIndex = 215;
            // 
            // lblSearchProfitCenter
            // 
            this.lblSearchProfitCenter.AutoSize = true;
            this.lblSearchProfitCenter.Location = new System.Drawing.Point(7, 34);
            this.lblSearchProfitCenter.Name = "lblSearchProfitCenter";
            this.lblSearchProfitCenter.Size = new System.Drawing.Size(65, 13);
            this.lblSearchProfitCenter.TabIndex = 3;
            this.lblSearchProfitCenter.Text = "Profit Center";
            // 
            // cboSearchProfitCenter
            // 
            this.cboSearchProfitCenter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchProfitCenter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchProfitCenter.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchProfitCenter.DropDownHeight = 134;
            this.cboSearchProfitCenter.FormattingEnabled = true;
            this.cboSearchProfitCenter.IntegralHeight = false;
            this.cboSearchProfitCenter.Location = new System.Drawing.Point(78, 31);
            this.cboSearchProfitCenter.Name = "cboSearchProfitCenter";
            this.cboSearchProfitCenter.Size = new System.Drawing.Size(181, 21);
            this.cboSearchProfitCenter.TabIndex = 2;
            this.cboSearchProfitCenter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // bnProfitCenter
            // 
            this.bnProfitCenter.AddNewItem = null;
            this.bnProfitCenter.CountItem = null;
            this.bnProfitCenter.DeleteItem = null;
            this.bnProfitCenter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.btnSaveItem,
            this.btnDelete,
            this.btnClear,
            this.ToolStripSeparator3,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnHelp});
            this.bnProfitCenter.Location = new System.Drawing.Point(266, 0);
            this.bnProfitCenter.MoveFirstItem = null;
            this.bnProfitCenter.MoveLastItem = null;
            this.bnProfitCenter.MoveNextItem = null;
            this.bnProfitCenter.MovePreviousItem = null;
            this.bnProfitCenter.Name = "bnProfitCenter";
            this.bnProfitCenter.PositionItem = null;
            this.bnProfitCenter.Size = new System.Drawing.Size(988, 25);
            this.bnProfitCenter.TabIndex = 1064;
            this.bnProfitCenter.Text = "BindingNavigator1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.RightToLeftAutoMirrorImage = true;
            this.btnAdd.Size = new System.Drawing.Size(23, 22);
            this.btnAdd.Text = "Add new";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save Data";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.ToolTipText = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnEmail.Image")));
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.ToolTipText = "Email";
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.Location = new System.Drawing.Point(285, 88);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(37, 13);
            this.lblMonth.TabIndex = 1068;
            this.lblMonth.Text = "Month";
            // 
            // dtpMonth
            // 
            this.dtpMonth.CustomFormat = "MMM yyyy";
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Location = new System.Drawing.Point(363, 84);
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.Size = new System.Drawing.Size(106, 20);
            this.dtpMonth.TabIndex = 1067;
            this.dtpMonth.ValueChanged += new System.EventHandler(this.dtpMonth_ValueChanged);
            // 
            // lblProfitCenter
            // 
            this.lblProfitCenter.AutoSize = true;
            this.lblProfitCenter.Location = new System.Drawing.Point(286, 60);
            this.lblProfitCenter.Name = "lblProfitCenter";
            this.lblProfitCenter.Size = new System.Drawing.Size(65, 13);
            this.lblProfitCenter.TabIndex = 1066;
            this.lblProfitCenter.Text = "Profit Center";
            // 
            // cboProfitCenter
            // 
            this.cboProfitCenter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboProfitCenter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboProfitCenter.BackColor = System.Drawing.SystemColors.Info;
            this.cboProfitCenter.DropDownHeight = 134;
            this.cboProfitCenter.FormattingEnabled = true;
            this.cboProfitCenter.IntegralHeight = false;
            this.cboProfitCenter.Location = new System.Drawing.Point(363, 57);
            this.cboProfitCenter.Name = "cboProfitCenter";
            this.cboProfitCenter.Size = new System.Drawing.Size(234, 21);
            this.cboProfitCenter.TabIndex = 1065;
            this.cboProfitCenter.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            this.cboProfitCenter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboProfitCenter.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(627, 33);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 1070;
            this.lblRemarks.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(630, 49);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(359, 56);
            this.txtRemarks.TabIndex = 1069;
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // dgvInvoice
            // 
            this.dgvInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvInvoice.BackgroundColor = System.Drawing.Color.White;
            this.dgvInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmployeeID,
            this.InvoiceNo,
            this.InvoiceDate,
            this.Amount,
            this.Remarks});
            this.dgvInvoice.Location = new System.Drawing.Point(269, 110);
            this.dgvInvoice.Name = "dgvInvoice";
            this.dgvInvoice.Size = new System.Drawing.Size(982, 360);
            this.dgvInvoice.TabIndex = 1071;
            this.dgvInvoice.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInvoice_CellValueChanged);
            this.dgvInvoice.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvInvoice_EditingControlShowing);
            this.dgvInvoice.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvInvoice_CurrentCellDirtyStateChanged);
            this.dgvInvoice.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // EmployeeID
            // 
            this.EmployeeID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EmployeeID.DataPropertyName = "EmployeeID";
            this.EmployeeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EmployeeID.HeaderText = "Employee";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // InvoiceNo
            // 
            this.InvoiceNo.DataPropertyName = "InvoiceNo";
            this.InvoiceNo.HeaderText = "InvoiceNo";
            this.InvoiceNo.Name = "InvoiceNo";
            this.InvoiceNo.Width = 150;
            // 
            // InvoiceDate
            // 
            // 
            // 
            // 
            this.InvoiceDate.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.InvoiceDate.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.InvoiceDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.InvoiceDate.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText;
            this.InvoiceDate.CustomFormat = "dd/MMM/yyyy";
            this.InvoiceDate.DataPropertyName = "InvoiceDate";
            this.InvoiceDate.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.InvoiceDate.HeaderText = "Date";
            this.InvoiceDate.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.InvoiceDate.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.InvoiceDate.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.InvoiceDate.MonthCalendar.BackgroundStyle.Class = "";
            this.InvoiceDate.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.InvoiceDate.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.InvoiceDate.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.InvoiceDate.MonthCalendar.DisplayMonth = new System.DateTime(2014, 12, 1, 0, 0, 0, 0);
            this.InvoiceDate.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.InvoiceDate.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.InvoiceDate.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.InvoiceDate.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.InvoiceDate.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.InvoiceDate.Name = "InvoiceDate";
            this.InvoiceDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            this.Amount.HeaderText = "Amount";
            this.Amount.MaxInputLength = 20;
            this.Amount.Name = "Amount";
            this.Amount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Remarks
            // 
            this.Remarks.DataPropertyName = "Remarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.MaxInputLength = 500;
            this.Remarks.Name = "Remarks";
            this.Remarks.Width = 150;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(272, 476);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1072;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(1176, 476);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1074;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(1095, 476);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1073;
            this.btnOK.Text = "Ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(286, 33);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 1076;
            this.lblCompany.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.Location = new System.Drawing.Point(363, 30);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(234, 21);
            this.cboCompany.TabIndex = 1075;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboCompany.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // tmrProfitCenter
            // 
            this.tmrProfitCenter.Interval = 1000;
            this.tmrProfitCenter.Tick += new System.EventHandler(this.tmrProfitCenter_Tick);
            // 
            // errProfitCenter
            // 
            this.errProfitCenter.ContainerControl = this;
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMain.Controls.Add(this.bnProfitCenter);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.btnOK);
            this.pnlMain.Controls.Add(this.btnSave);
            this.pnlMain.Controls.Add(this.lblCompany);
            this.pnlMain.Controls.Add(this.expLeft);
            this.pnlMain.Controls.Add(this.cboCompany);
            this.pnlMain.Controls.Add(this.lblRemarks);
            this.pnlMain.Controls.Add(this.dgvInvoice);
            this.pnlMain.Controls.Add(this.txtRemarks);
            this.pnlMain.Controls.Add(this.ssProfitCenter);
            this.pnlMain.Controls.Add(this.cboProfitCenter);
            this.pnlMain.Controls.Add(this.lblProfitCenter);
            this.pnlMain.Controls.Add(this.dtpMonth);
            this.pnlMain.Controls.Add(this.lblMonth);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1254, 524);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 1077;
            // 
            // frmSalesInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1254, 524);
            this.Controls.Add(this.pnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSalesInvoice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sales Invoice";
            this.Load += new System.EventHandler(this.frmSalesInvoice_Load);
            this.ssProfitCenter.ResumeLayout(false);
            this.ssProfitCenter.PerformLayout();
            this.expLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            this.pnlTopSearch.ResumeLayout(false);
            this.pnlTopSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnProfitCenter)).EndInit();
            this.bnProfitCenter.ResumeLayout(false);
            this.bnProfitCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProfitCenter)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.StatusStrip ssProfitCenter;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        private DevComponents.DotNetBar.ExpandablePanel expLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSearch;
        private DevComponents.DotNetBar.PanelEx pnlTopSearch;
        internal System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.Label lblSearchTo;
        internal System.Windows.Forms.Label lblSearchFrom;
        internal System.Windows.Forms.DateTimePicker dtpSearchToDate;
        internal System.Windows.Forms.DateTimePicker dtpSearchFromDate;
        internal System.Windows.Forms.Label lblSearchProfitCenter;
        internal System.Windows.Forms.ComboBox cboSearchProfitCenter;
        internal System.Windows.Forms.BindingNavigator bnProfitCenter;
        internal System.Windows.Forms.ToolStripButton btnAdd;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.Label lblMonth;
        internal System.Windows.Forms.DateTimePicker dtpMonth;
        internal System.Windows.Forms.Label lblProfitCenter;
        internal System.Windows.Forms.ComboBox cboProfitCenter;
        internal System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.DataGridView dgvInvoice;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.Label lblSearchEmployee;
        internal System.Windows.Forms.ComboBox cboSearchEmployee;
        internal System.Windows.Forms.Label lblCompany;
        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.Label lblSearchCompany;
        internal System.Windows.Forms.ComboBox cboSearchCompany;
        private System.Windows.Forms.Timer tmrProfitCenter;
        private System.Windows.Forms.ErrorProvider errProfitCenter;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesInvoiceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProfitCenterID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProfitCenter;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn Month;
        private DevComponents.DotNetBar.PanelEx pnlMain;
        private System.Windows.Forms.DataGridViewComboBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceNo;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn InvoiceDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
    }
}