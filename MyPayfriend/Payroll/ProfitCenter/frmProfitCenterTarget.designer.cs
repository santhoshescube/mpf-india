﻿namespace MyPayfriend
{
    partial class frmProfitCenterTarget
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfitCenterTarget));
            this.expLeft = new DevComponents.DotNetBar.ExpandablePanel();
            this.dgvSearch = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.TargetID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProfitCenterID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProfitCenter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MonthFrom = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.MonthTo = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.pnlTopSearch = new DevComponents.DotNetBar.PanelEx();
            this.lblSearchCompany = new System.Windows.Forms.Label();
            this.cboSearchCompany = new System.Windows.Forms.ComboBox();
            this.lblSearchEmployee = new System.Windows.Forms.Label();
            this.cboSearchEmployee = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblSearchTo = new System.Windows.Forms.Label();
            this.lblSearchFrom = new System.Windows.Forms.Label();
            this.dtpSearchToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpSearchFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblSearchProfitCenter = new System.Windows.Forms.Label();
            this.cboSearchProfitCenter = new System.Windows.Forms.ComboBox();
            this.bnProfitCenter = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnProfitCenter = new System.Windows.Forms.ToolStripButton();
            this.ssProfitCenter = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblProfitCenter = new System.Windows.Forms.Label();
            this.cboProfitCenter = new System.Windows.Forms.ComboBox();
            this.lblPeriodTo = new System.Windows.Forms.Label();
            this.lblPeriodFrom = new System.Windows.Forms.Label();
            this.dtpPeriodTo = new System.Windows.Forms.DateTimePicker();
            this.dtpPeriodFrom = new System.Windows.Forms.DateTimePicker();
            this.diTarget = new DevComponents.Editors.DoubleInput();
            this.lblTarget = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.dgvEmployee = new System.Windows.Forms.DataGridView();
            this.EmployeeID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Target = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblCompany = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.tmrProfitCenter = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errProfitCenter = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.expLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).BeginInit();
            this.pnlTopSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnProfitCenter)).BeginInit();
            this.bnProfitCenter.SuspendLayout();
            this.ssProfitCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.diTarget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProfitCenter)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // expLeft
            // 
            this.expLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.expLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expLeft.Controls.Add(this.dgvSearch);
            this.expLeft.Controls.Add(this.pnlTopSearch);
            this.expLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.expLeft.ExpandButtonVisible = false;
            this.expLeft.Location = new System.Drawing.Point(0, 0);
            this.expLeft.Name = "expLeft";
            this.expLeft.Size = new System.Drawing.Size(328, 452);
            this.expLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expLeft.Style.GradientAngle = 90;
            this.expLeft.TabIndex = 1045;
            this.expLeft.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expLeft.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expLeft.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expLeft.TitleStyle.GradientAngle = 90;
            this.expLeft.TitleText = "Search";
            // 
            // dgvSearch
            // 
            this.dgvSearch.AllowUserToAddRows = false;
            this.dgvSearch.AllowUserToDeleteRows = false;
            this.dgvSearch.AllowUserToResizeColumns = false;
            this.dgvSearch.AllowUserToResizeRows = false;
            this.dgvSearch.BackgroundColor = System.Drawing.Color.White;
            this.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TargetID,
            this.ProfitCenterID,
            this.ProfitCenter,
            this.MonthFrom,
            this.MonthTo});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSearch.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSearch.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSearch.Location = new System.Drawing.Point(0, 162);
            this.dgvSearch.Name = "dgvSearch";
            this.dgvSearch.ReadOnly = true;
            this.dgvSearch.RowHeadersVisible = false;
            this.dgvSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSearch.Size = new System.Drawing.Size(328, 290);
            this.dgvSearch.TabIndex = 1;
            this.dgvSearch.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSearch_CellDoubleClick);
            this.dgvSearch.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // TargetID
            // 
            this.TargetID.DataPropertyName = "TargetID";
            this.TargetID.HeaderText = "TargetID";
            this.TargetID.Name = "TargetID";
            this.TargetID.ReadOnly = true;
            this.TargetID.Visible = false;
            // 
            // ProfitCenterID
            // 
            this.ProfitCenterID.DataPropertyName = "ProfitCenterID";
            this.ProfitCenterID.HeaderText = "ProfitCenterID";
            this.ProfitCenterID.Name = "ProfitCenterID";
            this.ProfitCenterID.ReadOnly = true;
            this.ProfitCenterID.Visible = false;
            // 
            // ProfitCenter
            // 
            this.ProfitCenter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProfitCenter.DataPropertyName = "ProfitCenter";
            this.ProfitCenter.HeaderText = "Profit Center";
            this.ProfitCenter.Name = "ProfitCenter";
            this.ProfitCenter.ReadOnly = true;
            // 
            // MonthFrom
            // 
            // 
            // 
            // 
            this.MonthFrom.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.MonthFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.MonthFrom.CustomFormat = "MMM yyyy";
            this.MonthFrom.DataPropertyName = "MonthFrom";
            this.MonthFrom.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.MonthFrom.HeaderText = "From";
            this.MonthFrom.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.MonthFrom.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.MonthFrom.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.MonthFrom.MonthCalendar.BackgroundStyle.Class = "";
            this.MonthFrom.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.MonthFrom.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.MonthFrom.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.MonthFrom.MonthCalendar.DisplayMonth = new System.DateTime(2014, 12, 1, 0, 0, 0, 0);
            this.MonthFrom.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.MonthFrom.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.MonthFrom.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.MonthFrom.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.MonthFrom.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.MonthFrom.Name = "MonthFrom";
            this.MonthFrom.ReadOnly = true;
            this.MonthFrom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // MonthTo
            // 
            // 
            // 
            // 
            this.MonthTo.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.MonthTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.MonthTo.CustomFormat = "MMM yyyy";
            this.MonthTo.DataPropertyName = "MonthTo";
            this.MonthTo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.MonthTo.HeaderText = "To";
            this.MonthTo.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            // 
            // 
            // 
            this.MonthTo.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.MonthTo.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.MonthTo.MonthCalendar.BackgroundStyle.Class = "";
            this.MonthTo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.MonthTo.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.MonthTo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.MonthTo.MonthCalendar.DisplayMonth = new System.DateTime(2014, 12, 1, 0, 0, 0, 0);
            this.MonthTo.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.MonthTo.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.MonthTo.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.MonthTo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.MonthTo.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.MonthTo.Name = "MonthTo";
            this.MonthTo.ReadOnly = true;
            this.MonthTo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // pnlTopSearch
            // 
            this.pnlTopSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlTopSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlTopSearch.Controls.Add(this.lblSearchCompany);
            this.pnlTopSearch.Controls.Add(this.cboSearchCompany);
            this.pnlTopSearch.Controls.Add(this.lblSearchEmployee);
            this.pnlTopSearch.Controls.Add(this.cboSearchEmployee);
            this.pnlTopSearch.Controls.Add(this.btnSearch);
            this.pnlTopSearch.Controls.Add(this.lblSearchTo);
            this.pnlTopSearch.Controls.Add(this.lblSearchFrom);
            this.pnlTopSearch.Controls.Add(this.dtpSearchToDate);
            this.pnlTopSearch.Controls.Add(this.dtpSearchFromDate);
            this.pnlTopSearch.Controls.Add(this.lblSearchProfitCenter);
            this.pnlTopSearch.Controls.Add(this.cboSearchProfitCenter);
            this.pnlTopSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopSearch.Location = new System.Drawing.Point(0, 26);
            this.pnlTopSearch.Name = "pnlTopSearch";
            this.pnlTopSearch.Size = new System.Drawing.Size(328, 136);
            this.pnlTopSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlTopSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlTopSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlTopSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlTopSearch.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlTopSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlTopSearch.Style.GradientAngle = 90;
            this.pnlTopSearch.TabIndex = 2;
            // 
            // lblSearchCompany
            // 
            this.lblSearchCompany.AutoSize = true;
            this.lblSearchCompany.Location = new System.Drawing.Point(11, 8);
            this.lblSearchCompany.Name = "lblSearchCompany";
            this.lblSearchCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSearchCompany.TabIndex = 1066;
            this.lblSearchCompany.Text = "Company";
            // 
            // cboSearchCompany
            // 
            this.cboSearchCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchCompany.DropDownHeight = 134;
            this.cboSearchCompany.FormattingEnabled = true;
            this.cboSearchCompany.IntegralHeight = false;
            this.cboSearchCompany.Location = new System.Drawing.Point(81, 5);
            this.cboSearchCompany.Name = "cboSearchCompany";
            this.cboSearchCompany.Size = new System.Drawing.Size(236, 21);
            this.cboSearchCompany.TabIndex = 1065;
            this.cboSearchCompany.SelectedIndexChanged += new System.EventHandler(this.cboSearchCompany_SelectedIndexChanged);
            this.cboSearchCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblSearchEmployee
            // 
            this.lblSearchEmployee.AutoSize = true;
            this.lblSearchEmployee.Location = new System.Drawing.Point(11, 62);
            this.lblSearchEmployee.Name = "lblSearchEmployee";
            this.lblSearchEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblSearchEmployee.TabIndex = 227;
            this.lblSearchEmployee.Text = "Employee";
            // 
            // cboSearchEmployee
            // 
            this.cboSearchEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchEmployee.DropDownHeight = 134;
            this.cboSearchEmployee.FormattingEnabled = true;
            this.cboSearchEmployee.IntegralHeight = false;
            this.cboSearchEmployee.Location = new System.Drawing.Point(81, 59);
            this.cboSearchEmployee.Name = "cboSearchEmployee";
            this.cboSearchEmployee.Size = new System.Drawing.Size(236, 21);
            this.cboSearchEmployee.TabIndex = 226;
            this.cboSearchEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(257, 109);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(60, 23);
            this.btnSearch.TabIndex = 223;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblSearchTo
            // 
            this.lblSearchTo.AutoSize = true;
            this.lblSearchTo.Location = new System.Drawing.Point(11, 116);
            this.lblSearchTo.Name = "lblSearchTo";
            this.lblSearchTo.Size = new System.Drawing.Size(20, 13);
            this.lblSearchTo.TabIndex = 218;
            this.lblSearchTo.Text = "To";
            // 
            // lblSearchFrom
            // 
            this.lblSearchFrom.AutoSize = true;
            this.lblSearchFrom.Location = new System.Drawing.Point(11, 90);
            this.lblSearchFrom.Name = "lblSearchFrom";
            this.lblSearchFrom.Size = new System.Drawing.Size(30, 13);
            this.lblSearchFrom.TabIndex = 217;
            this.lblSearchFrom.Text = "From";
            // 
            // dtpSearchToDate
            // 
            this.dtpSearchToDate.CustomFormat = "MMM yyyy";
            this.dtpSearchToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSearchToDate.Location = new System.Drawing.Point(81, 112);
            this.dtpSearchToDate.Name = "dtpSearchToDate";
            this.dtpSearchToDate.Size = new System.Drawing.Size(106, 20);
            this.dtpSearchToDate.TabIndex = 216;
            // 
            // dtpSearchFromDate
            // 
            this.dtpSearchFromDate.CustomFormat = "MMM yyyy";
            this.dtpSearchFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSearchFromDate.Location = new System.Drawing.Point(81, 86);
            this.dtpSearchFromDate.Name = "dtpSearchFromDate";
            this.dtpSearchFromDate.Size = new System.Drawing.Size(106, 20);
            this.dtpSearchFromDate.TabIndex = 215;
            // 
            // lblSearchProfitCenter
            // 
            this.lblSearchProfitCenter.AutoSize = true;
            this.lblSearchProfitCenter.Location = new System.Drawing.Point(11, 35);
            this.lblSearchProfitCenter.Name = "lblSearchProfitCenter";
            this.lblSearchProfitCenter.Size = new System.Drawing.Size(65, 13);
            this.lblSearchProfitCenter.TabIndex = 3;
            this.lblSearchProfitCenter.Text = "Profit Center";
            // 
            // cboSearchProfitCenter
            // 
            this.cboSearchProfitCenter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchProfitCenter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchProfitCenter.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchProfitCenter.DropDownHeight = 134;
            this.cboSearchProfitCenter.FormattingEnabled = true;
            this.cboSearchProfitCenter.IntegralHeight = false;
            this.cboSearchProfitCenter.Location = new System.Drawing.Point(81, 32);
            this.cboSearchProfitCenter.Name = "cboSearchProfitCenter";
            this.cboSearchProfitCenter.Size = new System.Drawing.Size(236, 21);
            this.cboSearchProfitCenter.TabIndex = 2;
            this.cboSearchProfitCenter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // bnProfitCenter
            // 
            this.bnProfitCenter.AddNewItem = null;
            this.bnProfitCenter.CountItem = null;
            this.bnProfitCenter.DeleteItem = null;
            this.bnProfitCenter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.btnSaveItem,
            this.btnDelete,
            this.btnClear,
            this.ToolStripSeparator3,
            this.btnProfitCenter});
            this.bnProfitCenter.Location = new System.Drawing.Point(328, 0);
            this.bnProfitCenter.MoveFirstItem = null;
            this.bnProfitCenter.MoveLastItem = null;
            this.bnProfitCenter.MoveNextItem = null;
            this.bnProfitCenter.MovePreviousItem = null;
            this.bnProfitCenter.Name = "bnProfitCenter";
            this.bnProfitCenter.PositionItem = null;
            this.bnProfitCenter.Size = new System.Drawing.Size(466, 25);
            this.bnProfitCenter.TabIndex = 1046;
            this.bnProfitCenter.Text = "BindingNavigator1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.RightToLeftAutoMirrorImage = true;
            this.btnAdd.Size = new System.Drawing.Size(23, 22);
            this.btnAdd.Text = "Add new";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save Data";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnProfitCenter
            // 
            this.btnProfitCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnProfitCenter.Image = global::MyPayfriend.Properties.Resources._16x16Executive;
            this.btnProfitCenter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnProfitCenter.Name = "btnProfitCenter";
            this.btnProfitCenter.Size = new System.Drawing.Size(23, 22);
            this.btnProfitCenter.Text = "Profit Center";
            this.btnProfitCenter.Click += new System.EventHandler(this.btnProfitCenter_Click);
            // 
            // ssProfitCenter
            // 
            this.ssProfitCenter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.ssProfitCenter.Location = new System.Drawing.Point(0, 452);
            this.ssProfitCenter.Name = "ssProfitCenter";
            this.ssProfitCenter.Size = new System.Drawing.Size(794, 22);
            this.ssProfitCenter.TabIndex = 1047;
            this.ssProfitCenter.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lblProfitCenter
            // 
            this.lblProfitCenter.AutoSize = true;
            this.lblProfitCenter.Location = new System.Drawing.Point(346, 61);
            this.lblProfitCenter.Name = "lblProfitCenter";
            this.lblProfitCenter.Size = new System.Drawing.Size(65, 13);
            this.lblProfitCenter.TabIndex = 1049;
            this.lblProfitCenter.Text = "Profit Center";
            // 
            // cboProfitCenter
            // 
            this.cboProfitCenter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboProfitCenter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboProfitCenter.BackColor = System.Drawing.SystemColors.Info;
            this.cboProfitCenter.DropDownHeight = 134;
            this.cboProfitCenter.FormattingEnabled = true;
            this.cboProfitCenter.IntegralHeight = false;
            this.cboProfitCenter.Location = new System.Drawing.Point(423, 58);
            this.cboProfitCenter.Name = "cboProfitCenter";
            this.cboProfitCenter.Size = new System.Drawing.Size(234, 21);
            this.cboProfitCenter.TabIndex = 1048;
            this.cboProfitCenter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboProfitCenter.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // lblPeriodTo
            // 
            this.lblPeriodTo.AutoSize = true;
            this.lblPeriodTo.Location = new System.Drawing.Point(535, 89);
            this.lblPeriodTo.Name = "lblPeriodTo";
            this.lblPeriodTo.Size = new System.Drawing.Size(10, 13);
            this.lblPeriodTo.TabIndex = 1053;
            this.lblPeriodTo.Text = "-";
            // 
            // lblPeriodFrom
            // 
            this.lblPeriodFrom.AutoSize = true;
            this.lblPeriodFrom.Location = new System.Drawing.Point(346, 89);
            this.lblPeriodFrom.Name = "lblPeriodFrom";
            this.lblPeriodFrom.Size = new System.Drawing.Size(37, 13);
            this.lblPeriodFrom.TabIndex = 1052;
            this.lblPeriodFrom.Text = "Period";
            // 
            // dtpPeriodTo
            // 
            this.dtpPeriodTo.CustomFormat = "MMM yyyy";
            this.dtpPeriodTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPeriodTo.Location = new System.Drawing.Point(551, 85);
            this.dtpPeriodTo.Name = "dtpPeriodTo";
            this.dtpPeriodTo.Size = new System.Drawing.Size(106, 20);
            this.dtpPeriodTo.TabIndex = 1051;
            this.dtpPeriodTo.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // dtpPeriodFrom
            // 
            this.dtpPeriodFrom.CustomFormat = "MMM yyyy";
            this.dtpPeriodFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPeriodFrom.Location = new System.Drawing.Point(423, 85);
            this.dtpPeriodFrom.Name = "dtpPeriodFrom";
            this.dtpPeriodFrom.Size = new System.Drawing.Size(106, 20);
            this.dtpPeriodFrom.TabIndex = 1050;
            this.dtpPeriodFrom.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // diTarget
            // 
            // 
            // 
            // 
            this.diTarget.BackgroundStyle.Class = "DateTimeInputBackground";
            this.diTarget.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.diTarget.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.diTarget.Increment = 1;
            this.diTarget.Location = new System.Drawing.Point(423, 111);
            this.diTarget.Name = "diTarget";
            this.diTarget.Size = new System.Drawing.Size(106, 20);
            this.diTarget.TabIndex = 1054;
            this.diTarget.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // lblTarget
            // 
            this.lblTarget.AutoSize = true;
            this.lblTarget.Location = new System.Drawing.Point(346, 115);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(38, 13);
            this.lblTarget.TabIndex = 1055;
            this.lblTarget.Text = "Target";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(423, 137);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(359, 35);
            this.txtRemarks.TabIndex = 1056;
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(346, 140);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 1057;
            this.lblRemarks.Text = "Remarks";
            // 
            // dgvEmployee
            // 
            this.dgvEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEmployee.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmployeeID,
            this.Target});
            this.dgvEmployee.Location = new System.Drawing.Point(331, 179);
            this.dgvEmployee.Name = "dgvEmployee";
            this.dgvEmployee.Size = new System.Drawing.Size(460, 241);
            this.dgvEmployee.TabIndex = 1058;
            this.dgvEmployee.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployee_CellValueChanged);
            this.dgvEmployee.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvEmployee_EditingControlShowing);
            this.dgvEmployee.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvEmployee_CurrentCellDirtyStateChanged);
            this.dgvEmployee.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // EmployeeID
            // 
            this.EmployeeID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EmployeeID.DataPropertyName = "EmployeeID";
            this.EmployeeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EmployeeID.HeaderText = "Employee";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Target
            // 
            this.Target.DataPropertyName = "Target";
            this.Target.HeaderText = "Target";
            this.Target.MaxInputLength = 20;
            this.Target.Name = "Target";
            this.Target.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(331, 426);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1059;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(713, 426);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1061;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(632, 426);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1060;
            this.btnOK.Text = "Ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(346, 34);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 1064;
            this.lblCompany.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.Location = new System.Drawing.Point(423, 31);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(234, 21);
            this.cboCompany.TabIndex = 1063;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboCompany.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // tmrProfitCenter
            // 
            this.tmrProfitCenter.Interval = 1000;
            this.tmrProfitCenter.Tick += new System.EventHandler(this.tmrProfitCenter_Tick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Target";
            this.dataGridViewTextBoxColumn1.HeaderText = "Target";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ProfitCenterID";
            this.dataGridViewTextBoxColumn2.HeaderText = "ProfitCenterID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ProfitCenter";
            this.dataGridViewTextBoxColumn3.HeaderText = "Profit Center";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PeriodFrom";
            this.dataGridViewTextBoxColumn4.HeaderText = "From";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PeriodTo";
            this.dataGridViewTextBoxColumn5.HeaderText = "To";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // errProfitCenter
            // 
            this.errProfitCenter.ContainerControl = this;
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMain.Controls.Add(this.bnProfitCenter);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.btnOK);
            this.pnlMain.Controls.Add(this.btnSave);
            this.pnlMain.Controls.Add(this.lblCompany);
            this.pnlMain.Controls.Add(this.expLeft);
            this.pnlMain.Controls.Add(this.cboCompany);
            this.pnlMain.Controls.Add(this.ssProfitCenter);
            this.pnlMain.Controls.Add(this.dgvEmployee);
            this.pnlMain.Controls.Add(this.txtRemarks);
            this.pnlMain.Controls.Add(this.cboProfitCenter);
            this.pnlMain.Controls.Add(this.lblRemarks);
            this.pnlMain.Controls.Add(this.lblProfitCenter);
            this.pnlMain.Controls.Add(this.dtpPeriodFrom);
            this.pnlMain.Controls.Add(this.lblTarget);
            this.pnlMain.Controls.Add(this.dtpPeriodTo);
            this.pnlMain.Controls.Add(this.diTarget);
            this.pnlMain.Controls.Add(this.lblPeriodFrom);
            this.pnlMain.Controls.Add(this.lblPeriodTo);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(794, 474);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 1065;
            // 
            // frmProfitCenterTarget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 474);
            this.Controls.Add(this.pnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProfitCenterTarget";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Profit Center Target";
            this.Load += new System.EventHandler(this.frmProfitCenterTarget_Load);
            this.expLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            this.pnlTopSearch.ResumeLayout(false);
            this.pnlTopSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnProfitCenter)).EndInit();
            this.bnProfitCenter.ResumeLayout(false);
            this.bnProfitCenter.PerformLayout();
            this.ssProfitCenter.ResumeLayout(false);
            this.ssProfitCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.diTarget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProfitCenter)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSearch;
        private DevComponents.DotNetBar.PanelEx pnlTopSearch;
        internal System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.Label lblSearchTo;
        internal System.Windows.Forms.Label lblSearchFrom;
        internal System.Windows.Forms.DateTimePicker dtpSearchToDate;
        internal System.Windows.Forms.DateTimePicker dtpSearchFromDate;
        internal System.Windows.Forms.Label lblSearchProfitCenter;
        internal System.Windows.Forms.ComboBox cboSearchProfitCenter;
        internal System.Windows.Forms.BindingNavigator bnProfitCenter;
        internal System.Windows.Forms.ToolStripButton btnAdd;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.StatusStrip ssProfitCenter;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Label lblProfitCenter;
        internal System.Windows.Forms.ComboBox cboProfitCenter;
        internal System.Windows.Forms.Label lblPeriodTo;
        internal System.Windows.Forms.Label lblPeriodFrom;
        internal System.Windows.Forms.DateTimePicker dtpPeriodTo;
        internal System.Windows.Forms.DateTimePicker dtpPeriodFrom;
        private DevComponents.Editors.DoubleInput diTarget;
        internal System.Windows.Forms.Label lblTarget;
        private System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.DataGridView dgvEmployee;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.Label lblSearchEmployee;
        internal System.Windows.Forms.ComboBox cboSearchEmployee;
        private System.Windows.Forms.ToolStripButton btnProfitCenter;
        internal System.Windows.Forms.Label lblCompany;
        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.Label lblSearchCompany;
        internal System.Windows.Forms.ComboBox cboSearchCompany;
        private System.Windows.Forms.DataGridViewComboBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Target;
        private System.Windows.Forms.Timer tmrProfitCenter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn TargetID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProfitCenterID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProfitCenter;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn MonthFrom;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn MonthTo;
        private System.Windows.Forms.ErrorProvider errProfitCenter;
        private DevComponents.DotNetBar.PanelEx pnlMain;
    }
}