﻿namespace MyPayfriend
{
    partial class FrmAttendanceManual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAttendanceManual));
            this.lblSearchIn = new DevComponents.DotNetBar.LabelX();
            this.DotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.DockSite4 = new DevComponents.DotNetBar.DockSite();
            this.DockSite1 = new DevComponents.DotNetBar.DockSite();
            this.DockSite2 = new DevComponents.DotNetBar.DockSite();
            this.DockSite8 = new DevComponents.DotNetBar.DockSite();
            this.DockSite5 = new DevComponents.DotNetBar.DockSite();
            this.DockSite6 = new DevComponents.DotNetBar.DockSite();
            this.DockSite7 = new DevComponents.DotNetBar.DockSite();
            this.BarView = new DevComponents.DotNetBar.Bar();
            this.btnSave = new DevComponents.DotNetBar.ButtonItem();
            this.btnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.btnClear = new DevComponents.DotNetBar.ButtonItem();
            this.btnAutoFill = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.CustomizeItem1 = new DevComponents.DotNetBar.CustomizeItem();
            this.DockSite3 = new DevComponents.DotNetBar.DockSite();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.grpSearchLeft = new System.Windows.Forms.GroupBox();
            this.btnSearch = new DevComponents.DotNetBar.ButtonX();
            this.cboEmpSearch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.EmployeeName = new DevComponents.Editors.ComboItem();
            this.EmployeeNo = new DevComponents.Editors.ComboItem();
            this.ExpandablepnlSearch = new DevComponents.DotNetBar.ExpandablePanel();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.lblLocation = new DevComponents.DotNetBar.LabelX();
            this.cboLocation = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblMonth = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnLastGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.TlSProgressBarAttendance = new System.Windows.Forms.ToolStripProgressBar();
            this.PanelBottom = new DevComponents.DotNetBar.PanelEx();
            this.lblInfo = new DevComponents.DotNetBar.LabelX();
            this.lblDayStatus = new DevComponents.DotNetBar.LabelX();
            this.lblShortageTime = new DevComponents.DotNetBar.LabelX();
            this.lblWorkTime = new DevComponents.DotNetBar.LabelX();
            this.lblConseqDisplay = new DevComponents.DotNetBar.LabelX();
            this.lblConsequence = new DevComponents.DotNetBar.LabelX();
            this.lblShiftName = new DevComponents.DotNetBar.LabelX();
            this.lblShift = new DevComponents.DotNetBar.LabelX();
            this.StatusStripBottom = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblAttendanceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblExcessTime = new DevComponents.DotNetBar.LabelX();
            this.lblWork = new DevComponents.DotNetBar.LabelX();
            this.lblShortage = new DevComponents.DotNetBar.LabelX();
            this.lblExcess = new DevComponents.DotNetBar.LabelX();
            this.btnNextGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.ContextMenuStripAutoFill = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AutoFillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuStripStatus = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnPresent = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRest = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLeave = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAbsent = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.FromTimeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.LabelItem26 = new DevComponents.DotNetBar.LabelItem();
            this.lblPageNumber = new DevComponents.DotNetBar.LabelItem();
            this.PanelRightMain = new DevComponents.DotNetBar.PanelEx();
            this.dgvAttendance = new ClsDataGirdViewX();
            this.colDgvEmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDgvCompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDgvPolicyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDgvOTHour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDgvOTDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMiddileBottom = new DevComponents.DotNetBar.PanelEx();
            this.ItemPanelMiddileBottom = new DevComponents.DotNetBar.ItemPanel();
            this.ItemContainer5 = new DevComponents.DotNetBar.ItemContainer();
            this.cboCountItem = new DevComponents.DotNetBar.ComboBoxItem();
            this.cboItem25 = new DevComponents.Editors.ComboItem();
            this.cboItem50 = new DevComponents.Editors.ComboItem();
            this.cboItem100 = new DevComponents.Editors.ComboItem();
            this.cboItem200 = new DevComponents.Editors.ComboItem();
            this.cboItem500 = new DevComponents.Editors.ComboItem();
            this.cboItemALL = new DevComponents.Editors.ComboItem();
            this.btnFirstGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.btnPreviousGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.LabelItem24 = new DevComponents.DotNetBar.LabelItem();
            this.DockSite7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarView)).BeginInit();
            this.grpSearchLeft.SuspendLayout();
            this.ExpandablepnlSearch.SuspendLayout();
            this.grpMain.SuspendLayout();
            this.PanelBottom.SuspendLayout();
            this.StatusStripBottom.SuspendLayout();
            this.ContextMenuStripAutoFill.SuspendLayout();
            this.ContextMenuStripStatus.SuspendLayout();
            this.PanelRightMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttendance)).BeginInit();
            this.pnlMiddileBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSearchIn
            // 
            this.lblSearchIn.AutoSize = true;
            // 
            // 
            // 
            this.lblSearchIn.BackgroundStyle.Class = "";
            this.lblSearchIn.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSearchIn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchIn.Location = new System.Drawing.Point(6, 42);
            this.lblSearchIn.Name = "lblSearchIn";
            this.lblSearchIn.Size = new System.Drawing.Size(13, 16);
            this.lblSearchIn.TabIndex = 9;
            this.lblSearchIn.Text = "In";
            // 
            // DotNetBarManager1
            // 
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.DotNetBarManager1.BottomDockSite = this.DockSite4;
            this.DotNetBarManager1.ColorScheme.BarCaptionBackground = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.BarCaptionBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.BarCaptionInactiveBackground = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(157)))), ((int)(((byte)(145)))));
            this.DotNetBarManager1.ColorScheme.ExplorerBarBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(172)))), ((int)(((byte)(89)))));
            this.DotNetBarManager1.ColorScheme.ExplorerBarBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(121)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemCheckedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemDisabledText = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(157)))), ((int)(((byte)(145)))));
            this.DotNetBarManager1.ColorScheme.ItemHotBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(125)))));
            this.DotNetBarManager1.ColorScheme.ItemHotBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemPressedBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.DotNetBarManager1.ColorScheme.ItemPressedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(67)))));
            this.DotNetBarManager1.EnableFullSizeDock = false;
            this.DotNetBarManager1.LeftDockSite = this.DockSite1;
            this.DotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.DotNetBarManager1.ParentForm = this;
            this.DotNetBarManager1.RightDockSite = this.DockSite2;
            this.DotNetBarManager1.ShowCustomizeContextMenu = false;
            this.DotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.DotNetBarManager1.ToolbarBottomDockSite = this.DockSite8;
            this.DotNetBarManager1.ToolbarLeftDockSite = this.DockSite5;
            this.DotNetBarManager1.ToolbarRightDockSite = this.DockSite6;
            this.DotNetBarManager1.ToolbarTopDockSite = this.DockSite7;
            this.DotNetBarManager1.TopDockSite = this.DockSite3;
            // 
            // DockSite4
            // 
            this.DockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite4.Location = new System.Drawing.Point(0, 470);
            this.DockSite4.Name = "DockSite4";
            this.DockSite4.Size = new System.Drawing.Size(1208, 0);
            this.DockSite4.TabIndex = 155;
            this.DockSite4.TabStop = false;
            // 
            // DockSite1
            // 
            this.DockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.DockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite1.Location = new System.Drawing.Point(0, 0);
            this.DockSite1.Name = "DockSite1";
            this.DockSite1.Size = new System.Drawing.Size(0, 470);
            this.DockSite1.TabIndex = 152;
            this.DockSite1.TabStop = false;
            // 
            // DockSite2
            // 
            this.DockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.DockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite2.Location = new System.Drawing.Point(1208, 0);
            this.DockSite2.Name = "DockSite2";
            this.DockSite2.Size = new System.Drawing.Size(0, 470);
            this.DockSite2.TabIndex = 153;
            this.DockSite2.TabStop = false;
            // 
            // DockSite8
            // 
            this.DockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DockSite8.Location = new System.Drawing.Point(0, 470);
            this.DockSite8.Name = "DockSite8";
            this.DockSite8.Size = new System.Drawing.Size(1208, 0);
            this.DockSite8.TabIndex = 159;
            this.DockSite8.TabStop = false;
            // 
            // DockSite5
            // 
            this.DockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.DockSite5.Location = new System.Drawing.Point(0, 0);
            this.DockSite5.Name = "DockSite5";
            this.DockSite5.Size = new System.Drawing.Size(0, 470);
            this.DockSite5.TabIndex = 157;
            this.DockSite5.TabStop = false;
            // 
            // DockSite6
            // 
            this.DockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.DockSite6.Location = new System.Drawing.Point(1208, 0);
            this.DockSite6.Name = "DockSite6";
            this.DockSite6.Size = new System.Drawing.Size(0, 470);
            this.DockSite6.TabIndex = 158;
            this.DockSite6.TabStop = false;
            // 
            // DockSite7
            // 
            this.DockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite7.Controls.Add(this.BarView);
            this.DockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite7.Location = new System.Drawing.Point(0, 0);
            this.DockSite7.Name = "DockSite7";
            this.DockSite7.Size = new System.Drawing.Size(1208, 25);
            this.DockSite7.TabIndex = 150;
            this.DockSite7.TabStop = false;
            // 
            // BarView
            // 
            this.BarView.AccessibleDescription = "DotNetBar Bar (BarView)";
            this.BarView.AccessibleName = "DotNetBar Bar";
            this.BarView.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.BarView.CanUndock = false;
            this.BarView.ColorScheme.BarCaptionBackground = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.BarCaptionBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ExplorerBarBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(172)))), ((int)(((byte)(89)))));
            this.BarView.ColorScheme.ExplorerBarBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(121)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ItemCheckedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ItemHotBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(125)))));
            this.BarView.ColorScheme.ItemHotBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ItemPressedBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.BarView.ColorScheme.ItemPressedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.BarView.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.BarView.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSave,
            this.btnDelete,
            this.btnClear,
            this.btnAutoFill,
            this.BtnPrint,
            this.CustomizeItem1});
            this.BarView.Location = new System.Drawing.Point(0, 0);
            this.BarView.Name = "BarView";
            this.BarView.Size = new System.Drawing.Size(147, 25);
            this.BarView.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.BarView.TabIndex = 2;
            this.BarView.TabStop = false;
            this.BarView.Text = "Bar2";
            // 
            // btnSave
            // 
            this.btnSave.Image = global::MyPayfriend.Properties.Resources.save;
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "Save";
            this.btnSave.Tooltip = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "Delete";
            this.btnDelete.Tooltip = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.Name = "btnClear";
            this.btnClear.Text = "Clear";
            this.btnClear.Tooltip = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAutoFill
            // 
            this.btnAutoFill.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.btnAutoFill.BeginGroup = true;
            this.btnAutoFill.Image = global::MyPayfriend.Properties.Resources.Autofill;
            this.btnAutoFill.Name = "btnAutoFill";
            this.btnAutoFill.Text = "AutoFill";
            this.btnAutoFill.Tooltip = "Auto Fill";
            this.btnAutoFill.Click += new System.EventHandler(this.btnAutoFill_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.BeginGroup = true;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Tooltip = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // CustomizeItem1
            // 
            this.CustomizeItem1.CustomizeItemVisible = false;
            this.CustomizeItem1.Name = "CustomizeItem1";
            this.CustomizeItem1.Text = "&Add or Remove Buttons";
            this.CustomizeItem1.Tooltip = "Bar Options";
            // 
            // DockSite3
            // 
            this.DockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite3.Location = new System.Drawing.Point(0, 0);
            this.DockSite3.Name = "DockSite3";
            this.DockSite3.Size = new System.Drawing.Size(1208, 0);
            this.DockSite3.TabIndex = 154;
            this.DockSite3.TabStop = false;
            // 
            // txtSearch
            // 
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(6, 14);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(188, 20);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // grpSearchLeft
            // 
            this.grpSearchLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSearchLeft.Controls.Add(this.lblSearchIn);
            this.grpSearchLeft.Controls.Add(this.txtSearch);
            this.grpSearchLeft.Controls.Add(this.btnSearch);
            this.grpSearchLeft.Controls.Add(this.cboEmpSearch);
            this.grpSearchLeft.Location = new System.Drawing.Point(998, 31);
            this.grpSearchLeft.Name = "grpSearchLeft";
            this.grpSearchLeft.Size = new System.Drawing.Size(204, 63);
            this.grpSearchLeft.TabIndex = 1;
            this.grpSearchLeft.TabStop = false;
            this.grpSearchLeft.Text = "Search";
            // 
            // btnSearch
            // 
            this.btnSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Location = new System.Drawing.Point(160, 38);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(34, 20);
            this.btnSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cboEmpSearch
            // 
            this.cboEmpSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmpSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmpSearch.DisplayMember = "Text";
            this.cboEmpSearch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmpSearch.FormattingEnabled = true;
            this.cboEmpSearch.ItemHeight = 14;
            this.cboEmpSearch.Items.AddRange(new object[] {
            this.EmployeeName,
            this.EmployeeNo});
            this.cboEmpSearch.Location = new System.Drawing.Point(23, 38);
            this.cboEmpSearch.Name = "cboEmpSearch";
            this.cboEmpSearch.Size = new System.Drawing.Size(135, 20);
            this.cboEmpSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboEmpSearch.TabIndex = 1;
            this.cboEmpSearch.SelectedIndexChanged += new System.EventHandler(this.cboEmpSearch_SelectedIndexChanged);
            this.cboEmpSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmpSearch_KeyDown);
            // 
            // EmployeeName
            // 
            this.EmployeeName.Text = "Employee Name";
            // 
            // EmployeeNo
            // 
            this.EmployeeNo.Text = "Employee No";
            // 
            // ExpandablepnlSearch
            // 
            this.ExpandablepnlSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.ExpandablepnlSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ExpandablepnlSearch.Controls.Add(this.grpSearchLeft);
            this.ExpandablepnlSearch.Controls.Add(this.grpMain);
            this.ExpandablepnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExpandablepnlSearch.Location = new System.Drawing.Point(0, 25);
            this.ExpandablepnlSearch.Name = "ExpandablepnlSearch";
            this.ExpandablepnlSearch.Size = new System.Drawing.Size(1208, 101);
            this.ExpandablepnlSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpandablepnlSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpandablepnlSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpandablepnlSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ExpandablepnlSearch.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(67)))));
            this.ExpandablepnlSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpandablepnlSearch.Style.GradientAngle = 90;
            this.ExpandablepnlSearch.TabIndex = 151;
            this.ExpandablepnlSearch.TitleHeight = 30;
            this.ExpandablepnlSearch.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpandablepnlSearch.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpandablepnlSearch.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpandablepnlSearch.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.ExpandablepnlSearch.TitleStyle.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(67)))));
            this.ExpandablepnlSearch.TitleStyle.BorderWidth = 2;
            this.ExpandablepnlSearch.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.ExpandablepnlSearch.TitleStyle.GradientAngle = 90;
            this.ExpandablepnlSearch.TitleText = "Advanced Search";
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.lblLocation);
            this.grpMain.Controls.Add(this.cboLocation);
            this.grpMain.Controls.Add(this.dtpFromDate);
            this.grpMain.Controls.Add(this.lblMonth);
            this.grpMain.Controls.Add(this.lblCompany);
            this.grpMain.Controls.Add(this.cboCompany);
            this.grpMain.Controls.Add(this.btnShow);
            this.grpMain.Location = new System.Drawing.Point(4, 30);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(846, 66);
            this.grpMain.TabIndex = 0;
            this.grpMain.TabStop = false;
            this.grpMain.Text = "General info";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            // 
            // 
            // 
            this.lblLocation.BackgroundStyle.Class = "";
            this.lblLocation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLocation.Location = new System.Drawing.Point(296, 30);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(73, 15);
            this.lblLocation.TabIndex = 32;
            this.lblLocation.Text = "Work Location";
            // 
            // cboLocation
            // 
            this.cboLocation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLocation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLocation.DisplayMember = "Text";
            this.cboLocation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboLocation.FormattingEnabled = true;
            this.cboLocation.ItemHeight = 14;
            this.cboLocation.Location = new System.Drawing.Point(373, 28);
            this.cboLocation.Name = "cboLocation";
            this.cboLocation.Size = new System.Drawing.Size(210, 20);
            this.cboLocation.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboLocation.TabIndex = 31;
            this.cboLocation.WatermarkText = "Company";
            this.cboLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboLocation_KeyDown);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(649, 28);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowUpDown = true;
            this.dtpFromDate.Size = new System.Drawing.Size(105, 20);
            this.dtpFromDate.TabIndex = 3;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            this.dtpFromDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpFromDate_KeyDown);
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            // 
            // 
            // 
            this.lblMonth.BackgroundStyle.Class = "";
            this.lblMonth.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMonth.Location = new System.Drawing.Point(601, 30);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(33, 15);
            this.lblMonth.TabIndex = 30;
            this.lblMonth.Text = "Month";
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(19, 30);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 27;
            this.lblCompany.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(73, 28);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(210, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 0;
            this.cboCompany.WatermarkText = "Company";
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCompany_KeyDown);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(761, 28);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(56, 22);
            this.btnShow.TabIndex = 4;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnLastGridCollection
            // 
            this.btnLastGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowRightStart;
            this.btnLastGridCollection.Name = "btnLastGridCollection";
            this.btnLastGridCollection.Text = "Last";
            this.btnLastGridCollection.Click += new System.EventHandler(this.btnLastGridCollection_Click);
            // 
            // TlSProgressBarAttendance
            // 
            this.TlSProgressBarAttendance.Name = "TlSProgressBarAttendance";
            this.TlSProgressBarAttendance.Size = new System.Drawing.Size(100, 16);
            this.TlSProgressBarAttendance.Visible = false;
            // 
            // PanelBottom
            // 
            this.PanelBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.PanelBottom.Controls.Add(this.lblInfo);
            this.PanelBottom.Controls.Add(this.lblDayStatus);
            this.PanelBottom.Controls.Add(this.lblShortageTime);
            this.PanelBottom.Controls.Add(this.lblWorkTime);
            this.PanelBottom.Controls.Add(this.lblConseqDisplay);
            this.PanelBottom.Controls.Add(this.lblConsequence);
            this.PanelBottom.Controls.Add(this.lblShiftName);
            this.PanelBottom.Controls.Add(this.lblShift);
            this.PanelBottom.Controls.Add(this.StatusStripBottom);
            this.PanelBottom.Controls.Add(this.lblExcessTime);
            this.PanelBottom.Controls.Add(this.lblWork);
            this.PanelBottom.Controls.Add(this.lblShortage);
            this.PanelBottom.Controls.Add(this.lblExcess);
            this.PanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelBottom.Location = new System.Drawing.Point(0, 393);
            this.PanelBottom.Name = "PanelBottom";
            this.PanelBottom.Size = new System.Drawing.Size(1208, 77);
            this.PanelBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelBottom.Style.GradientAngle = 90;
            this.PanelBottom.TabIndex = 153;
            // 
            // lblInfo
            // 
            this.lblInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblInfo.AutoSize = true;
            // 
            // 
            // 
            this.lblInfo.BackgroundStyle.Class = "";
            this.lblInfo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblInfo.Location = new System.Drawing.Point(4, 37);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(197, 15);
            this.lblInfo.TabIndex = 34;
            this.lblInfo.Text = "**Time should be entered in hour format";
            // 
            // lblDayStatus
            // 
            this.lblDayStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDayStatus.AutoSize = true;
            // 
            // 
            // 
            this.lblDayStatus.BackgroundStyle.Class = "";
            this.lblDayStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDayStatus.Location = new System.Drawing.Point(849, 27);
            this.lblDayStatus.Name = "lblDayStatus";
            this.lblDayStatus.Size = new System.Drawing.Size(40, 15);
            this.lblDayStatus.TabIndex = 33;
            this.lblDayStatus.Text = "Status :";
            this.lblDayStatus.Visible = false;
            // 
            // lblShortageTime
            // 
            this.lblShortageTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblShortageTime.AutoSize = true;
            // 
            // 
            // 
            this.lblShortageTime.BackgroundStyle.Class = "";
            this.lblShortageTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShortageTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShortageTime.Location = new System.Drawing.Point(1021, 6);
            this.lblShortageTime.Name = "lblShortageTime";
            this.lblShortageTime.Size = new System.Drawing.Size(47, 15);
            this.lblShortageTime.TabIndex = 32;
            this.lblShortageTime.Text = "00:00:00";
            // 
            // lblWorkTime
            // 
            this.lblWorkTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWorkTime.AutoSize = true;
            // 
            // 
            // 
            this.lblWorkTime.BackgroundStyle.Class = "";
            this.lblWorkTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWorkTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorkTime.Location = new System.Drawing.Point(907, 6);
            this.lblWorkTime.Name = "lblWorkTime";
            this.lblWorkTime.Size = new System.Drawing.Size(47, 15);
            this.lblWorkTime.TabIndex = 23;
            this.lblWorkTime.Text = "00:00:00";
            // 
            // lblConseqDisplay
            // 
            this.lblConseqDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConseqDisplay.AutoSize = true;
            // 
            // 
            // 
            this.lblConseqDisplay.BackgroundStyle.Class = "";
            this.lblConseqDisplay.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblConseqDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConseqDisplay.Location = new System.Drawing.Point(363, 37);
            this.lblConseqDisplay.Name = "lblConseqDisplay";
            this.lblConseqDisplay.Size = new System.Drawing.Size(73, 15);
            this.lblConseqDisplay.TabIndex = 29;
            this.lblConseqDisplay.Text = "Consequence";
            this.lblConseqDisplay.Visible = false;
            // 
            // lblConsequence
            // 
            this.lblConsequence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsequence.AutoSize = true;
            // 
            // 
            // 
            this.lblConsequence.BackgroundStyle.Class = "";
            this.lblConsequence.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblConsequence.Location = new System.Drawing.Point(284, 37);
            this.lblConsequence.Name = "lblConsequence";
            this.lblConsequence.Size = new System.Drawing.Size(73, 15);
            this.lblConsequence.TabIndex = 28;
            this.lblConsequence.Text = " Consequence";
            this.lblConsequence.Visible = false;
            // 
            // lblShiftName
            // 
            this.lblShiftName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblShiftName.AutoSize = true;
            // 
            // 
            // 
            this.lblShiftName.BackgroundStyle.Class = "";
            this.lblShiftName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShiftName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShiftName.Location = new System.Drawing.Point(41, 17);
            this.lblShiftName.Name = "lblShiftName";
            this.lblShiftName.Size = new System.Drawing.Size(59, 15);
            this.lblShiftName.TabIndex = 17;
            this.lblShiftName.Text = "Shift Name";
            this.lblShiftName.Visible = false;
            // 
            // lblShift
            // 
            this.lblShift.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblShift.AutoSize = true;
            // 
            // 
            // 
            this.lblShift.BackgroundStyle.Class = "";
            this.lblShift.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShift.Location = new System.Drawing.Point(7, 17);
            this.lblShift.Name = "lblShift";
            this.lblShift.Size = new System.Drawing.Size(25, 15);
            this.lblShift.TabIndex = 16;
            this.lblShift.Text = "Shift";
            this.lblShift.Visible = false;
            // 
            // StatusStripBottom
            // 
            this.StatusStripBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.StatusStripBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lblAttendanceStatus,
            this.TlSProgressBarAttendance});
            this.StatusStripBottom.Location = new System.Drawing.Point(0, 55);
            this.StatusStripBottom.Name = "StatusStripBottom";
            this.StatusStripBottom.Size = new System.Drawing.Size(1208, 22);
            this.StatusStripBottom.TabIndex = 0;
            this.StatusStripBottom.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(42, 17);
            this.lblStatus.Text = "Status:";
            // 
            // lblAttendanceStatus
            // 
            this.lblAttendanceStatus.Name = "lblAttendanceStatus";
            this.lblAttendanceStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lblExcessTime
            // 
            this.lblExcessTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExcessTime.AutoSize = true;
            // 
            // 
            // 
            this.lblExcessTime.BackgroundStyle.Class = "";
            this.lblExcessTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblExcessTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExcessTime.Location = new System.Drawing.Point(1158, 6);
            this.lblExcessTime.Name = "lblExcessTime";
            this.lblExcessTime.Size = new System.Drawing.Size(47, 15);
            this.lblExcessTime.TabIndex = 27;
            this.lblExcessTime.Text = "00:00:00";
            // 
            // lblWork
            // 
            this.lblWork.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWork.AutoSize = true;
            // 
            // 
            // 
            this.lblWork.BackgroundStyle.Class = "";
            this.lblWork.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWork.Location = new System.Drawing.Point(849, 6);
            this.lblWork.Name = "lblWork";
            this.lblWork.Size = new System.Drawing.Size(56, 15);
            this.lblWork.TabIndex = 22;
            this.lblWork.Text = "Work Time";
            // 
            // lblShortage
            // 
            this.lblShortage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblShortage.AutoSize = true;
            // 
            // 
            // 
            this.lblShortage.BackgroundStyle.Class = "";
            this.lblShortage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblShortage.Location = new System.Drawing.Point(968, 6);
            this.lblShortage.Name = "lblShortage";
            this.lblShortage.Size = new System.Drawing.Size(47, 15);
            this.lblShortage.TabIndex = 24;
            this.lblShortage.Text = "Shortage";
            // 
            // lblExcess
            // 
            this.lblExcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExcess.AutoSize = true;
            // 
            // 
            // 
            this.lblExcess.BackgroundStyle.Class = "";
            this.lblExcess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblExcess.Location = new System.Drawing.Point(1087, 6);
            this.lblExcess.Name = "lblExcess";
            this.lblExcess.Size = new System.Drawing.Size(65, 15);
            this.lblExcess.TabIndex = 26;
            this.lblExcess.Text = "Excess Time";
            // 
            // btnNextGridCollection
            // 
            this.btnNextGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowRight;
            this.btnNextGridCollection.Name = "btnNextGridCollection";
            this.btnNextGridCollection.Text = "Next";
            this.btnNextGridCollection.Click += new System.EventHandler(this.btnNextGridCollection_Click);
            // 
            // ContextMenuStripAutoFill
            // 
            this.ContextMenuStripAutoFill.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AutoFillToolStripMenuItem});
            this.ContextMenuStripAutoFill.Name = "ContextMenuStripAutoFill";
            this.ContextMenuStripAutoFill.Size = new System.Drawing.Size(116, 26);
            // 
            // AutoFillToolStripMenuItem
            // 
            this.AutoFillToolStripMenuItem.Name = "AutoFillToolStripMenuItem";
            this.AutoFillToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.AutoFillToolStripMenuItem.Text = "AutoFill";
            this.AutoFillToolStripMenuItem.Click += new System.EventHandler(this.AutoFillToolStripMenuItem_Click);
            // 
            // ContextMenuStripStatus
            // 
            this.ContextMenuStripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPresent,
            this.btnRest,
            this.btnLeave,
            this.btnAbsent});
            this.ContextMenuStripStatus.Name = "statusContextMenuStrip";
            this.ContextMenuStripStatus.Size = new System.Drawing.Size(114, 92);
            // 
            // btnPresent
            // 
            this.btnPresent.Name = "btnPresent";
            this.btnPresent.Size = new System.Drawing.Size(113, 22);
            this.btnPresent.Text = "Present";
            this.btnPresent.Click += new System.EventHandler(this.btnPresent_Click);
            // 
            // btnRest
            // 
            this.btnRest.Name = "btnRest";
            this.btnRest.Size = new System.Drawing.Size(113, 22);
            this.btnRest.Text = "Rest";
            this.btnRest.Click += new System.EventHandler(this.btnRest_Click);
            // 
            // btnLeave
            // 
            this.btnLeave.Name = "btnLeave";
            this.btnLeave.Size = new System.Drawing.Size(113, 22);
            this.btnLeave.Text = "Leave";
            this.btnLeave.Click += new System.EventHandler(this.btnLeave_Click);
            // 
            // btnAbsent
            // 
            this.btnAbsent.Name = "btnAbsent";
            this.btnAbsent.Size = new System.Drawing.Size(113, 22);
            this.btnAbsent.Text = "Absent";
            this.btnAbsent.Click += new System.EventHandler(this.btnAbsent_Click);
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // FromTimeDateTimePicker
            // 
            this.FromTimeDateTimePicker.CustomFormat = "HH:mm";
            this.FromTimeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FromTimeDateTimePicker.Location = new System.Drawing.Point(611, 153);
            this.FromTimeDateTimePicker.Name = "FromTimeDateTimePicker";
            this.FromTimeDateTimePicker.ShowUpDown = true;
            this.FromTimeDateTimePicker.Size = new System.Drawing.Size(68, 20);
            this.FromTimeDateTimePicker.TabIndex = 1001;
            // 
            // LabelItem26
            // 
            this.LabelItem26.Name = "LabelItem26";
            this.LabelItem26.Text = "|";
            // 
            // lblPageNumber
            // 
            this.lblPageNumber.BeginGroup = true;
            this.lblPageNumber.Name = "lblPageNumber";
            this.lblPageNumber.Text = "{1} of {1}";
            // 
            // PanelRightMain
            // 
            this.PanelRightMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelRightMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.PanelRightMain.Controls.Add(this.dgvAttendance);
            this.PanelRightMain.Controls.Add(this.pnlMiddileBottom);
            this.PanelRightMain.Controls.Add(this.PanelBottom);
            this.PanelRightMain.Controls.Add(this.ExpandablepnlSearch);
            this.PanelRightMain.Controls.Add(this.FromTimeDateTimePicker);
            this.PanelRightMain.Controls.Add(this.DockSite7);
            this.PanelRightMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelRightMain.Location = new System.Drawing.Point(0, 0);
            this.PanelRightMain.Name = "PanelRightMain";
            this.PanelRightMain.Size = new System.Drawing.Size(1208, 470);
            this.PanelRightMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelRightMain.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.PanelRightMain.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(234)))), ((int)(((byte)(245)))));
            this.PanelRightMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelRightMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelRightMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelRightMain.Style.GradientAngle = 90;
            this.PanelRightMain.TabIndex = 156;
            this.PanelRightMain.Text = "PanelEx1";
            // 
            // dgvAttendance
            // 
            this.dgvAttendance.AddNewRow = false;
            this.dgvAttendance.AllowUserToAddRows = false;
            this.dgvAttendance.AllowUserToDeleteRows = false;
            this.dgvAttendance.AlphaNumericCols = new int[0];
            this.dgvAttendance.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvAttendance.CapsLockCols = new int[0];
            this.dgvAttendance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttendance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDgvEmployeeID,
            this.colDgvCompanyID,
            this.colDgvPolicyID,
            this.colDgvOTHour,
            this.colDgvOTDays});
            this.dgvAttendance.DecimalCols = new int[0];
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAttendance.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAttendance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAttendance.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvAttendance.HasSlNo = false;
            this.dgvAttendance.LastRowIndex = 0;
            this.dgvAttendance.Location = new System.Drawing.Point(0, 126);
            this.dgvAttendance.Name = "dgvAttendance";
            this.dgvAttendance.NegativeValueCols = new int[0];
            this.dgvAttendance.NumericCols = new int[0];
            this.dgvAttendance.Size = new System.Drawing.Size(1208, 236);
            this.dgvAttendance.TabIndex = 1003;
            this.dgvAttendance.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvAttendance_CellMouseClick);
            this.dgvAttendance.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvAttendance_CellBeginEdit);
            this.dgvAttendance.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAttendance_CellEndEdit);
            this.dgvAttendance.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAttendance_CellClick);
            this.dgvAttendance.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvAttendance_EditingControlShowing);
            this.dgvAttendance.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvAttendance_CurrentCellDirtyStateChanged);
            this.dgvAttendance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAttendance_KeyDown);
            // 
            // colDgvEmployeeID
            // 
            this.colDgvEmployeeID.HeaderText = "Employee";
            this.colDgvEmployeeID.Name = "colDgvEmployeeID";
            this.colDgvEmployeeID.ReadOnly = true;
            // 
            // colDgvCompanyID
            // 
            this.colDgvCompanyID.HeaderText = "Company";
            this.colDgvCompanyID.Name = "colDgvCompanyID";
            this.colDgvCompanyID.Visible = false;
            // 
            // colDgvPolicyID
            // 
            this.colDgvPolicyID.HeaderText = "PolicyID";
            this.colDgvPolicyID.Name = "colDgvPolicyID";
            this.colDgvPolicyID.Visible = false;
            // 
            // colDgvOTHour
            // 
            this.colDgvOTHour.HeaderText = "OT Hour";
            this.colDgvOTHour.Name = "colDgvOTHour";
            // 
            // colDgvOTDays
            // 
            this.colDgvOTDays.HeaderText = "OT Days";
            this.colDgvOTDays.Name = "colDgvOTDays";
            // 
            // pnlMiddileBottom
            // 
            this.pnlMiddileBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMiddileBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMiddileBottom.Controls.Add(this.ItemPanelMiddileBottom);
            this.pnlMiddileBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMiddileBottom.Location = new System.Drawing.Point(0, 362);
            this.pnlMiddileBottom.Name = "pnlMiddileBottom";
            this.pnlMiddileBottom.Size = new System.Drawing.Size(1208, 31);
            this.pnlMiddileBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMiddileBottom.Style.BackColor1.Color = System.Drawing.Color.White;
            this.pnlMiddileBottom.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(154)))));
            this.pnlMiddileBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMiddileBottom.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(154)))));
            this.pnlMiddileBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMiddileBottom.Style.GradientAngle = 90;
            this.pnlMiddileBottom.TabIndex = 1002;
            // 
            // ItemPanelMiddileBottom
            // 
            // 
            // 
            // 
            this.ItemPanelMiddileBottom.BackgroundStyle.Class = "ItemPanel";
            this.ItemPanelMiddileBottom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ItemPanelMiddileBottom.ContainerControlProcessDialogKey = true;
            this.ItemPanelMiddileBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemPanelMiddileBottom.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ItemContainer5});
            this.ItemPanelMiddileBottom.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.ItemPanelMiddileBottom.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.ItemPanelMiddileBottom.Location = new System.Drawing.Point(0, 0);
            this.ItemPanelMiddileBottom.Name = "ItemPanelMiddileBottom";
            this.ItemPanelMiddileBottom.Size = new System.Drawing.Size(1208, 31);
            this.ItemPanelMiddileBottom.TabIndex = 0;
            this.ItemPanelMiddileBottom.Text = "ItemPanel1";
            // 
            // ItemContainer5
            // 
            // 
            // 
            // 
            this.ItemContainer5.BackgroundStyle.Class = "";
            this.ItemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ItemContainer5.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.ItemContainer5.Name = "ItemContainer5";
            this.ItemContainer5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cboCountItem,
            this.btnFirstGridCollection,
            this.btnPreviousGridCollection,
            this.LabelItem24,
            this.lblPageNumber,
            this.LabelItem26,
            this.btnNextGridCollection,
            this.btnLastGridCollection});
            // 
            // cboCountItem
            // 
            this.cboCountItem.Caption = "ComboBoxItem1";
            this.cboCountItem.DropDownHeight = 106;
            this.cboCountItem.Items.AddRange(new object[] {
            this.cboItem25,
            this.cboItem50,
            this.cboItem100,
            this.cboItem200,
            this.cboItem500,
            this.cboItemALL});
            this.cboCountItem.Name = "cboCountItem";
            this.cboCountItem.SelectedIndexChanged += new System.EventHandler(this.cboCountItem_SelectedIndexChanged);
            // 
            // cboItem25
            // 
            this.cboItem25.Text = "25";
            // 
            // cboItem50
            // 
            this.cboItem50.Text = "50";
            // 
            // cboItem100
            // 
            this.cboItem100.Text = "100";
            // 
            // cboItem200
            // 
            this.cboItem200.Text = "200";
            // 
            // cboItem500
            // 
            this.cboItem500.Text = "500";
            // 
            // cboItemALL
            // 
            this.cboItemALL.Text = "ALL";
            // 
            // btnFirstGridCollection
            // 
            this.btnFirstGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowLeftStart;
            this.btnFirstGridCollection.Name = "btnFirstGridCollection";
            this.btnFirstGridCollection.Text = "First";
            this.btnFirstGridCollection.Click += new System.EventHandler(this.btnFirstGridCollection_Click);
            // 
            // btnPreviousGridCollection
            // 
            this.btnPreviousGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowLeft;
            this.btnPreviousGridCollection.Name = "btnPreviousGridCollection";
            this.btnPreviousGridCollection.Text = "Previous";
            this.btnPreviousGridCollection.Click += new System.EventHandler(this.btnPreviousGridCollection_Click);
            // 
            // LabelItem24
            // 
            this.LabelItem24.Name = "LabelItem24";
            this.LabelItem24.Text = "|";
            // 
            // FrmAttendanceManual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1208, 470);
            this.Controls.Add(this.PanelRightMain);
            this.Controls.Add(this.DockSite3);
            this.Controls.Add(this.DockSite4);
            this.Controls.Add(this.DockSite1);
            this.Controls.Add(this.DockSite2);
            this.Controls.Add(this.DockSite5);
            this.Controls.Add(this.DockSite6);
            this.Controls.Add(this.DockSite8);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmAttendanceManual";
            this.Text = "Attendance";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmAttendanceManual_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAttendanceManual_FormClosing);
            this.DockSite7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarView)).EndInit();
            this.grpSearchLeft.ResumeLayout(false);
            this.grpSearchLeft.PerformLayout();
            this.ExpandablepnlSearch.ResumeLayout(false);
            this.grpMain.ResumeLayout(false);
            this.grpMain.PerformLayout();
            this.PanelBottom.ResumeLayout(false);
            this.PanelBottom.PerformLayout();
            this.StatusStripBottom.ResumeLayout(false);
            this.StatusStripBottom.PerformLayout();
            this.ContextMenuStripAutoFill.ResumeLayout(false);
            this.ContextMenuStripStatus.ResumeLayout(false);
            this.PanelRightMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttendance)).EndInit();
            this.pnlMiddileBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.LabelX lblSearchIn;
        internal DevComponents.DotNetBar.DockSite DockSite4;
        internal DevComponents.DotNetBar.DockSite DockSite1;
        internal DevComponents.DotNetBar.DockSite DockSite2;
        internal DevComponents.DotNetBar.DockSite DockSite8;
        internal DevComponents.DotNetBar.DockSite DockSite5;
        internal DevComponents.DotNetBar.DockSite DockSite6;
        internal DevComponents.DotNetBar.Bar BarView;
        internal DevComponents.DotNetBar.ButtonItem btnSave;
        internal DevComponents.DotNetBar.ButtonItem btnDelete;
        internal DevComponents.DotNetBar.ButtonItem btnClear;
        internal DevComponents.DotNetBar.ButtonItem btnAutoFill;
        internal DevComponents.DotNetBar.ButtonItem BtnPrint;
        internal DevComponents.DotNetBar.CustomizeItem CustomizeItem1;
        internal DevComponents.DotNetBar.DockSite DockSite3;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        internal System.Windows.Forms.GroupBox grpSearchLeft;
        internal DevComponents.DotNetBar.ButtonX btnSearch;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboEmpSearch;
        internal DevComponents.Editors.ComboItem EmployeeName;
        internal DevComponents.Editors.ComboItem EmployeeNo;
        internal DevComponents.DotNetBar.ExpandablePanel ExpandablepnlSearch;
        internal System.Windows.Forms.GroupBox grpMain;
        internal System.Windows.Forms.DateTimePicker dtpFromDate;
        internal DevComponents.DotNetBar.LabelX lblMonth;
        internal DevComponents.DotNetBar.LabelX lblCompany;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        internal System.Windows.Forms.Button btnShow;
        internal DevComponents.DotNetBar.ButtonItem btnLastGridCollection;
        internal System.Windows.Forms.ToolStripProgressBar TlSProgressBarAttendance;
        internal DevComponents.DotNetBar.PanelEx PanelBottom;
        internal DevComponents.DotNetBar.LabelX lblInfo;
        internal DevComponents.DotNetBar.LabelX lblDayStatus;
        internal DevComponents.DotNetBar.LabelX lblShortageTime;
        internal DevComponents.DotNetBar.LabelX lblWorkTime;
        internal DevComponents.DotNetBar.LabelX lblConseqDisplay;
        internal DevComponents.DotNetBar.LabelX lblConsequence;
        internal DevComponents.DotNetBar.LabelX lblShiftName;
        internal DevComponents.DotNetBar.LabelX lblShift;
        internal System.Windows.Forms.StatusStrip StatusStripBottom;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblAttendanceStatus;
        internal DevComponents.DotNetBar.LabelX lblExcessTime;
        internal DevComponents.DotNetBar.LabelX lblWork;
        internal DevComponents.DotNetBar.LabelX lblShortage;
        internal DevComponents.DotNetBar.LabelX lblExcess;
        internal DevComponents.DotNetBar.ButtonItem btnNextGridCollection;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStripAutoFill;
        internal System.Windows.Forms.ToolStripMenuItem AutoFillToolStripMenuItem;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStripStatus;
        internal System.Windows.Forms.ToolStripMenuItem btnPresent;
        internal System.Windows.Forms.ToolStripMenuItem btnRest;
        internal System.Windows.Forms.ToolStripMenuItem btnLeave;
        internal System.Windows.Forms.ToolStripMenuItem btnAbsent;
        internal System.Windows.Forms.Timer tmrClear;
        internal System.Windows.Forms.DateTimePicker FromTimeDateTimePicker;
        internal DevComponents.DotNetBar.LabelItem LabelItem26;
        internal DevComponents.DotNetBar.LabelItem lblPageNumber;
        internal DevComponents.DotNetBar.PanelEx PanelRightMain;
        internal DevComponents.DotNetBar.PanelEx pnlMiddileBottom;
        internal DevComponents.DotNetBar.ItemPanel ItemPanelMiddileBottom;
        internal DevComponents.DotNetBar.ItemContainer ItemContainer5;
        internal DevComponents.DotNetBar.ComboBoxItem cboCountItem;
        internal DevComponents.Editors.ComboItem cboItem25;
        internal DevComponents.Editors.ComboItem cboItem50;
        internal DevComponents.Editors.ComboItem cboItem100;
        internal DevComponents.Editors.ComboItem cboItem200;
        internal DevComponents.Editors.ComboItem cboItem500;
        internal DevComponents.Editors.ComboItem cboItemALL;
        internal DevComponents.DotNetBar.ButtonItem btnFirstGridCollection;
        internal DevComponents.DotNetBar.ButtonItem btnPreviousGridCollection;
        internal DevComponents.DotNetBar.LabelItem LabelItem24;
        private ClsDataGirdViewX dgvAttendance;
        private DevComponents.DotNetBar.DockSite DockSite7;
        private DevComponents.DotNetBar.DotNetBarManager DotNetBarManager1;
        internal DevComponents.DotNetBar.LabelX lblLocation;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDgvEmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDgvCompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDgvPolicyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDgvOTHour;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDgvOTDays;
    }
}