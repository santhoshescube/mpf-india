﻿namespace MyPayfriend
{
    partial class DutyRoaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DutyRoaster));
            this.lblSearchIn = new DevComponents.DotNetBar.LabelX();
            this.DotNetBarManager1 = new DevComponents.DotNetBar.DotNetBarManager(this.components);
            this.DockSite4 = new DevComponents.DotNetBar.DockSite();
            this.DockSite1 = new DevComponents.DotNetBar.DockSite();
            this.DockSite2 = new DevComponents.DotNetBar.DockSite();
            this.DockSite8 = new DevComponents.DotNetBar.DockSite();
            this.DockSite5 = new DevComponents.DotNetBar.DockSite();
            this.DockSite6 = new DevComponents.DotNetBar.DockSite();
            this.DockSite7 = new DevComponents.DotNetBar.DockSite();
            this.BarView = new DevComponents.DotNetBar.Bar();
            this.btnSave = new DevComponents.DotNetBar.ButtonItem();
            this.btnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.btnClear = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.btnShift = new DevComponents.DotNetBar.ButtonItem();
            this.CustomizeItem1 = new DevComponents.DotNetBar.CustomizeItem();
            this.DockSite3 = new DevComponents.DotNetBar.DockSite();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.grpSearchLeft = new System.Windows.Forms.GroupBox();
            this.btnSearch = new DevComponents.DotNetBar.ButtonX();
            this.cboEmpSearch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.EmployeeName = new DevComponents.Editors.ComboItem();
            this.ExpandablepnlSearch = new DevComponents.DotNetBar.ExpandablePanel();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.CboDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblMonth = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnShow = new System.Windows.Forms.Button();
            this.ContextMenuStripAutoFill = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AutoFillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.FromTimeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.PanelRightMain = new DevComponents.DotNetBar.PanelEx();
            this.dgvRoster = new ClsDataGirdViewX();
            this.colDgvEmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDgvCompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMiddileBottom = new DevComponents.DotNetBar.PanelEx();
            this.StatusStripBottom = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblAttendanceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.TlSProgressBarAttendance = new System.Windows.Forms.ToolStripProgressBar();
            this.cboItem25 = new DevComponents.Editors.ComboItem();
            this.cboItem50 = new DevComponents.Editors.ComboItem();
            this.cboItem100 = new DevComponents.Editors.ComboItem();
            this.cboItem200 = new DevComponents.Editors.ComboItem();
            this.cboItem500 = new DevComponents.Editors.ComboItem();
            this.cboItemALL = new DevComponents.Editors.ComboItem();
            this.ItemContainer5 = new DevComponents.DotNetBar.ItemContainer();
            this.btnLastGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.DockSite7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarView)).BeginInit();
            this.grpSearchLeft.SuspendLayout();
            this.ExpandablepnlSearch.SuspendLayout();
            this.grpMain.SuspendLayout();
            this.ContextMenuStripAutoFill.SuspendLayout();
            this.PanelRightMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoster)).BeginInit();
            this.pnlMiddileBottom.SuspendLayout();
            this.StatusStripBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSearchIn
            // 
            this.lblSearchIn.AutoSize = true;
            // 
            // 
            // 
            this.lblSearchIn.BackgroundStyle.Class = "";
            this.lblSearchIn.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSearchIn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchIn.Location = new System.Drawing.Point(6, 42);
            this.lblSearchIn.Name = "lblSearchIn";
            this.lblSearchIn.Size = new System.Drawing.Size(13, 16);
            this.lblSearchIn.TabIndex = 9;
            this.lblSearchIn.Text = "In";
            // 
            // DotNetBarManager1
            // 
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del);
            this.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins);
            this.DotNetBarManager1.BottomDockSite = this.DockSite4;
            this.DotNetBarManager1.ColorScheme.BarCaptionBackground = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.BarCaptionBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.BarCaptionInactiveBackground = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(157)))), ((int)(((byte)(145)))));
            this.DotNetBarManager1.ColorScheme.ExplorerBarBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(172)))), ((int)(((byte)(89)))));
            this.DotNetBarManager1.ColorScheme.ExplorerBarBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(121)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemCheckedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemDisabledText = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(157)))), ((int)(((byte)(145)))));
            this.DotNetBarManager1.ColorScheme.ItemHotBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(125)))));
            this.DotNetBarManager1.ColorScheme.ItemHotBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemPressedBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.DotNetBarManager1.ColorScheme.ItemPressedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DotNetBarManager1.ColorScheme.ItemSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(67)))));
            this.DotNetBarManager1.EnableFullSizeDock = false;
            this.DotNetBarManager1.LeftDockSite = this.DockSite1;
            this.DotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.DotNetBarManager1.ParentForm = this;
            this.DotNetBarManager1.RightDockSite = this.DockSite2;
            this.DotNetBarManager1.ShowCustomizeContextMenu = false;
            this.DotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.DotNetBarManager1.ToolbarBottomDockSite = this.DockSite8;
            this.DotNetBarManager1.ToolbarLeftDockSite = this.DockSite5;
            this.DotNetBarManager1.ToolbarRightDockSite = this.DockSite6;
            this.DotNetBarManager1.ToolbarTopDockSite = this.DockSite7;
            this.DotNetBarManager1.TopDockSite = this.DockSite3;
            // 
            // DockSite4
            // 
            this.DockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DockSite4.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite4.Location = new System.Drawing.Point(0, 558);
            this.DockSite4.Name = "DockSite4";
            this.DockSite4.Size = new System.Drawing.Size(1164, 0);
            this.DockSite4.TabIndex = 155;
            this.DockSite4.TabStop = false;
            // 
            // DockSite1
            // 
            this.DockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite1.Dock = System.Windows.Forms.DockStyle.Left;
            this.DockSite1.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite1.Location = new System.Drawing.Point(0, 0);
            this.DockSite1.Name = "DockSite1";
            this.DockSite1.Size = new System.Drawing.Size(0, 558);
            this.DockSite1.TabIndex = 152;
            this.DockSite1.TabStop = false;
            // 
            // DockSite2
            // 
            this.DockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite2.Dock = System.Windows.Forms.DockStyle.Right;
            this.DockSite2.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite2.Location = new System.Drawing.Point(1164, 0);
            this.DockSite2.Name = "DockSite2";
            this.DockSite2.Size = new System.Drawing.Size(0, 558);
            this.DockSite2.TabIndex = 153;
            this.DockSite2.TabStop = false;
            // 
            // DockSite8
            // 
            this.DockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DockSite8.Location = new System.Drawing.Point(0, 558);
            this.DockSite8.Name = "DockSite8";
            this.DockSite8.Size = new System.Drawing.Size(1164, 0);
            this.DockSite8.TabIndex = 159;
            this.DockSite8.TabStop = false;
            // 
            // DockSite5
            // 
            this.DockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite5.Dock = System.Windows.Forms.DockStyle.Left;
            this.DockSite5.Location = new System.Drawing.Point(0, 0);
            this.DockSite5.Name = "DockSite5";
            this.DockSite5.Size = new System.Drawing.Size(0, 558);
            this.DockSite5.TabIndex = 157;
            this.DockSite5.TabStop = false;
            // 
            // DockSite6
            // 
            this.DockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite6.Dock = System.Windows.Forms.DockStyle.Right;
            this.DockSite6.Location = new System.Drawing.Point(1164, 0);
            this.DockSite6.Name = "DockSite6";
            this.DockSite6.Size = new System.Drawing.Size(0, 558);
            this.DockSite6.TabIndex = 158;
            this.DockSite6.TabStop = false;
            // 
            // DockSite7
            // 
            this.DockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite7.Controls.Add(this.BarView);
            this.DockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite7.Location = new System.Drawing.Point(0, 0);
            this.DockSite7.Name = "DockSite7";
            this.DockSite7.Size = new System.Drawing.Size(1164, 25);
            this.DockSite7.TabIndex = 150;
            this.DockSite7.TabStop = false;
            // 
            // BarView
            // 
            this.BarView.AccessibleDescription = "DotNetBar Bar (BarView)";
            this.BarView.AccessibleName = "DotNetBar Bar";
            this.BarView.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.BarView.CanUndock = false;
            this.BarView.ColorScheme.BarCaptionBackground = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.BarCaptionBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ExplorerBarBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(172)))), ((int)(((byte)(89)))));
            this.BarView.ColorScheme.ExplorerBarBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(121)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ItemCheckedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ItemHotBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(125)))));
            this.BarView.ColorScheme.ItemHotBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ItemPressedBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.BarView.ColorScheme.ItemPressedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.BarView.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.BarView.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSave,
            this.btnDelete,
            this.btnClear,
            this.BtnPrint,
            this.btnShift,
            this.CustomizeItem1});
            this.BarView.Location = new System.Drawing.Point(0, 0);
            this.BarView.Name = "BarView";
            this.BarView.Size = new System.Drawing.Size(144, 25);
            this.BarView.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.BarView.TabIndex = 2;
            this.BarView.TabStop = false;
            this.BarView.Text = "Bar2";
            // 
            // btnSave
            // 
            this.btnSave.Image = global::MyPayfriend.Properties.Resources.save;
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "Save";
            this.btnSave.Tooltip = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "Delete";
            this.btnDelete.Tooltip = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.Name = "btnClear";
            this.btnClear.Text = "Clear";
            this.btnClear.Tooltip = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.BeginGroup = true;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Tooltip = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // btnShift
            // 
            this.btnShift.Image = global::MyPayfriend.Properties.Resources.Shiftpolicy;
            this.btnShift.Name = "btnShift";
            this.btnShift.Text = "Shift";
            this.btnShift.Click += new System.EventHandler(this.btnShift_Click);
            // 
            // CustomizeItem1
            // 
            this.CustomizeItem1.CustomizeItemVisible = false;
            this.CustomizeItem1.Name = "CustomizeItem1";
            this.CustomizeItem1.Text = "&Add or Remove Buttons";
            this.CustomizeItem1.Tooltip = "Bar Options";
            // 
            // DockSite3
            // 
            this.DockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite3.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite3.DocumentDockContainer = new DevComponents.DotNetBar.DocumentDockContainer();
            this.DockSite3.Location = new System.Drawing.Point(0, 0);
            this.DockSite3.Name = "DockSite3";
            this.DockSite3.Size = new System.Drawing.Size(1164, 0);
            this.DockSite3.TabIndex = 154;
            this.DockSite3.TabStop = false;
            // 
            // txtSearch
            // 
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(6, 14);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(188, 20);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // grpSearchLeft
            // 
            this.grpSearchLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSearchLeft.Controls.Add(this.lblSearchIn);
            this.grpSearchLeft.Controls.Add(this.txtSearch);
            this.grpSearchLeft.Controls.Add(this.btnSearch);
            this.grpSearchLeft.Controls.Add(this.cboEmpSearch);
            this.grpSearchLeft.Location = new System.Drawing.Point(954, 31);
            this.grpSearchLeft.Name = "grpSearchLeft";
            this.grpSearchLeft.Size = new System.Drawing.Size(204, 63);
            this.grpSearchLeft.TabIndex = 1;
            this.grpSearchLeft.TabStop = false;
            this.grpSearchLeft.Text = "Search";
            // 
            // btnSearch
            // 
            this.btnSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSearch.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Location = new System.Drawing.Point(160, 38);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(34, 20);
            this.btnSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cboEmpSearch
            // 
            this.cboEmpSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmpSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmpSearch.DisplayMember = "Text";
            this.cboEmpSearch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmpSearch.FormattingEnabled = true;
            this.cboEmpSearch.ItemHeight = 14;
            this.cboEmpSearch.Items.AddRange(new object[] {
            this.EmployeeName});
            this.cboEmpSearch.Location = new System.Drawing.Point(23, 38);
            this.cboEmpSearch.Name = "cboEmpSearch";
            this.cboEmpSearch.Size = new System.Drawing.Size(135, 20);
            this.cboEmpSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboEmpSearch.TabIndex = 1;
            this.cboEmpSearch.SelectedIndexChanged += new System.EventHandler(this.cboEmpSearch_SelectedIndexChanged);
            this.cboEmpSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmpSearch_KeyDown);
            // 
            // EmployeeName
            // 
            this.EmployeeName.Text = "Employee Name";
            // 
            // ExpandablepnlSearch
            // 
            this.ExpandablepnlSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.ExpandablepnlSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ExpandablepnlSearch.Controls.Add(this.grpSearchLeft);
            this.ExpandablepnlSearch.Controls.Add(this.grpMain);
            this.ExpandablepnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExpandablepnlSearch.ExpandButtonVisible = false;
            this.ExpandablepnlSearch.Location = new System.Drawing.Point(0, 25);
            this.ExpandablepnlSearch.Name = "ExpandablepnlSearch";
            this.ExpandablepnlSearch.Size = new System.Drawing.Size(1164, 101);
            this.ExpandablepnlSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpandablepnlSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpandablepnlSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpandablepnlSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ExpandablepnlSearch.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(67)))));
            this.ExpandablepnlSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpandablepnlSearch.Style.GradientAngle = 90;
            this.ExpandablepnlSearch.TabIndex = 151;
            this.ExpandablepnlSearch.TitleHeight = 30;
            this.ExpandablepnlSearch.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpandablepnlSearch.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpandablepnlSearch.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpandablepnlSearch.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.ExpandablepnlSearch.TitleStyle.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(67)))));
            this.ExpandablepnlSearch.TitleStyle.BorderWidth = 2;
            this.ExpandablepnlSearch.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.ExpandablepnlSearch.TitleStyle.GradientAngle = 90;
            this.ExpandablepnlSearch.TitleText = "Title Bar";
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.CboDepartment);
            this.grpMain.Controls.Add(this.labelX1);
            this.grpMain.Controls.Add(this.dtpFromDate);
            this.grpMain.Controls.Add(this.lblMonth);
            this.grpMain.Controls.Add(this.lblCompany);
            this.grpMain.Controls.Add(this.cboCompany);
            this.grpMain.Controls.Add(this.btnShow);
            this.grpMain.Location = new System.Drawing.Point(4, 30);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(950, 66);
            this.grpMain.TabIndex = 0;
            this.grpMain.TabStop = false;
            this.grpMain.Text = "General info";
            // 
            // CboDepartment
            // 
            this.CboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDepartment.DisplayMember = "Text";
            this.CboDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CboDepartment.FormattingEnabled = true;
            this.CboDepartment.ItemHeight = 14;
            this.CboDepartment.Location = new System.Drawing.Point(381, 30);
            this.CboDepartment.Name = "CboDepartment";
            this.CboDepartment.Size = new System.Drawing.Size(210, 20);
            this.CboDepartment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CboDepartment.TabIndex = 32;
            this.CboDepartment.WatermarkText = "Company";
            this.CboDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboDepartment_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(315, 31);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(60, 15);
            this.labelX1.TabIndex = 31;
            this.labelX1.Text = "Department";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(677, 31);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowUpDown = true;
            this.dtpFromDate.Size = new System.Drawing.Size(105, 20);
            this.dtpFromDate.TabIndex = 3;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            this.dtpFromDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpFromDate_KeyDown);
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            // 
            // 
            // 
            this.lblMonth.BackgroundStyle.Class = "";
            this.lblMonth.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMonth.Location = new System.Drawing.Point(629, 33);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(33, 15);
            this.lblMonth.TabIndex = 30;
            this.lblMonth.Text = "Month";
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(19, 30);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 27;
            this.lblCompany.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(73, 28);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(210, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 0;
            this.cboCompany.WatermarkText = "Company";
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCompany_KeyDown);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(811, 31);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(56, 22);
            this.btnShow.TabIndex = 4;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // ContextMenuStripAutoFill
            // 
            this.ContextMenuStripAutoFill.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AutoFillToolStripMenuItem});
            this.ContextMenuStripAutoFill.Name = "ContextMenuStripAutoFill";
            this.ContextMenuStripAutoFill.Size = new System.Drawing.Size(116, 26);
            // 
            // AutoFillToolStripMenuItem
            // 
            this.AutoFillToolStripMenuItem.Name = "AutoFillToolStripMenuItem";
            this.AutoFillToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.AutoFillToolStripMenuItem.Text = "AutoFill";
            this.AutoFillToolStripMenuItem.Click += new System.EventHandler(this.AutoFillToolStripMenuItem_Click);
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // FromTimeDateTimePicker
            // 
            this.FromTimeDateTimePicker.CustomFormat = "HH:mm";
            this.FromTimeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FromTimeDateTimePicker.Location = new System.Drawing.Point(611, 153);
            this.FromTimeDateTimePicker.Name = "FromTimeDateTimePicker";
            this.FromTimeDateTimePicker.ShowUpDown = true;
            this.FromTimeDateTimePicker.Size = new System.Drawing.Size(68, 20);
            this.FromTimeDateTimePicker.TabIndex = 1001;
            // 
            // PanelRightMain
            // 
            this.PanelRightMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelRightMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005;
            this.PanelRightMain.Controls.Add(this.dgvRoster);
            this.PanelRightMain.Controls.Add(this.pnlMiddileBottom);
            this.PanelRightMain.Controls.Add(this.ExpandablepnlSearch);
            this.PanelRightMain.Controls.Add(this.FromTimeDateTimePicker);
            this.PanelRightMain.Controls.Add(this.DockSite7);
            this.PanelRightMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelRightMain.Location = new System.Drawing.Point(0, 0);
            this.PanelRightMain.Name = "PanelRightMain";
            this.PanelRightMain.Size = new System.Drawing.Size(1164, 558);
            this.PanelRightMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelRightMain.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.PanelRightMain.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(234)))), ((int)(((byte)(245)))));
            this.PanelRightMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelRightMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelRightMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelRightMain.Style.GradientAngle = 90;
            this.PanelRightMain.TabIndex = 156;
            this.PanelRightMain.Text = "PanelEx1";
            // 
            // dgvRoster
            // 
            this.dgvRoster.AddNewRow = false;
            this.dgvRoster.AllowUserToAddRows = false;
            this.dgvRoster.AllowUserToDeleteRows = false;
            this.dgvRoster.AlphaNumericCols = new int[0];
            this.dgvRoster.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvRoster.CapsLockCols = new int[0];
            this.dgvRoster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRoster.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDgvEmployeeID,
            this.colDgvCompanyID});
            this.dgvRoster.DecimalCols = new int[0];
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRoster.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvRoster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRoster.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvRoster.HasSlNo = false;
            this.dgvRoster.LastRowIndex = 0;
            this.dgvRoster.Location = new System.Drawing.Point(0, 126);
            this.dgvRoster.Name = "dgvRoster";
            this.dgvRoster.NegativeValueCols = new int[0];
            this.dgvRoster.NumericCols = new int[0];
            this.dgvRoster.Size = new System.Drawing.Size(1164, 401);
            this.dgvRoster.TabIndex = 1003;
            this.dgvRoster.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRoster_CellValueChanged);
            this.dgvRoster.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvRoster_CellMouseClick);
            this.dgvRoster.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvOffDayMark_CellBeginEdit);
            this.dgvRoster.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvRoster_MouseClick);
            this.dgvRoster.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRoster_CellEndEdit);
            this.dgvRoster.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvOffDayMark_CurrentCellDirtyStateChanged);
            this.dgvRoster.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvRoster_DataError);
            this.dgvRoster.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRoster_CellEnter);
            // 
            // colDgvEmployeeID
            // 
            this.colDgvEmployeeID.HeaderText = "Employee";
            this.colDgvEmployeeID.Name = "colDgvEmployeeID";
            this.colDgvEmployeeID.ReadOnly = true;
            // 
            // colDgvCompanyID
            // 
            this.colDgvCompanyID.HeaderText = "Company";
            this.colDgvCompanyID.Name = "colDgvCompanyID";
            this.colDgvCompanyID.Visible = false;
            // 
            // pnlMiddileBottom
            // 
            this.pnlMiddileBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMiddileBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMiddileBottom.Controls.Add(this.StatusStripBottom);
            this.pnlMiddileBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMiddileBottom.Location = new System.Drawing.Point(0, 527);
            this.pnlMiddileBottom.Name = "pnlMiddileBottom";
            this.pnlMiddileBottom.Size = new System.Drawing.Size(1164, 31);
            this.pnlMiddileBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMiddileBottom.Style.BackColor1.Color = System.Drawing.Color.White;
            this.pnlMiddileBottom.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(154)))));
            this.pnlMiddileBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMiddileBottom.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(181)))), ((int)(((byte)(154)))));
            this.pnlMiddileBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMiddileBottom.Style.GradientAngle = 90;
            this.pnlMiddileBottom.TabIndex = 1002;
            // 
            // StatusStripBottom
            // 
            this.StatusStripBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.StatusStripBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lblAttendanceStatus,
            this.TlSProgressBarAttendance});
            this.StatusStripBottom.Location = new System.Drawing.Point(0, 9);
            this.StatusStripBottom.Name = "StatusStripBottom";
            this.StatusStripBottom.Size = new System.Drawing.Size(1164, 22);
            this.StatusStripBottom.TabIndex = 0;
            this.StatusStripBottom.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(42, 17);
            this.lblStatus.Text = "Status:";
            // 
            // lblAttendanceStatus
            // 
            this.lblAttendanceStatus.Name = "lblAttendanceStatus";
            this.lblAttendanceStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // TlSProgressBarAttendance
            // 
            this.TlSProgressBarAttendance.Name = "TlSProgressBarAttendance";
            this.TlSProgressBarAttendance.Size = new System.Drawing.Size(100, 16);
            this.TlSProgressBarAttendance.Visible = false;
            // 
            // cboItem25
            // 
            this.cboItem25.Text = "25";
            // 
            // cboItem50
            // 
            this.cboItem50.Text = "50";
            // 
            // cboItem100
            // 
            this.cboItem100.Text = "100";
            // 
            // cboItem200
            // 
            this.cboItem200.Text = "200";
            // 
            // cboItem500
            // 
            this.cboItem500.Text = "500";
            // 
            // cboItemALL
            // 
            this.cboItemALL.Text = "ALL";
            // 
            // ItemContainer5
            // 
            // 
            // 
            // 
            this.ItemContainer5.BackgroundStyle.Class = "";
            this.ItemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ItemContainer5.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.ItemContainer5.Name = "ItemContainer5";
            this.ItemContainer5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnLastGridCollection});
            // 
            // btnLastGridCollection
            // 
            this.btnLastGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowRightStart;
            this.btnLastGridCollection.Name = "btnLastGridCollection";
            this.btnLastGridCollection.Text = "Last";
            // 
            // DutyRoaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1164, 558);
            this.Controls.Add(this.PanelRightMain);
            this.Controls.Add(this.DockSite3);
            this.Controls.Add(this.DockSite4);
            this.Controls.Add(this.DockSite1);
            this.Controls.Add(this.DockSite2);
            this.Controls.Add(this.DockSite5);
            this.Controls.Add(this.DockSite6);
            this.Controls.Add(this.DockSite8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DutyRoaster";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Duty Roaster";
            this.Load += new System.EventHandler(this.FrmOffDayMark_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOffDayMark_FormClosing);
            this.DockSite7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarView)).EndInit();
            this.grpSearchLeft.ResumeLayout(false);
            this.grpSearchLeft.PerformLayout();
            this.ExpandablepnlSearch.ResumeLayout(false);
            this.grpMain.ResumeLayout(false);
            this.grpMain.PerformLayout();
            this.ContextMenuStripAutoFill.ResumeLayout(false);
            this.PanelRightMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoster)).EndInit();
            this.pnlMiddileBottom.ResumeLayout(false);
            this.pnlMiddileBottom.PerformLayout();
            this.StatusStripBottom.ResumeLayout(false);
            this.StatusStripBottom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.LabelX lblSearchIn;
        internal DevComponents.DotNetBar.DockSite DockSite4;
        internal DevComponents.DotNetBar.DockSite DockSite1;
        internal DevComponents.DotNetBar.DockSite DockSite2;
        internal DevComponents.DotNetBar.DockSite DockSite8;
        internal DevComponents.DotNetBar.DockSite DockSite5;
        internal DevComponents.DotNetBar.DockSite DockSite6;
        internal DevComponents.DotNetBar.Bar BarView;
        internal DevComponents.DotNetBar.ButtonItem btnSave;
        internal DevComponents.DotNetBar.ButtonItem btnDelete;
        internal DevComponents.DotNetBar.ButtonItem btnClear;
        internal DevComponents.DotNetBar.ButtonItem BtnPrint;
        internal DevComponents.DotNetBar.CustomizeItem CustomizeItem1;
        internal DevComponents.DotNetBar.DockSite DockSite3;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        internal System.Windows.Forms.GroupBox grpSearchLeft;
        internal DevComponents.DotNetBar.ButtonX btnSearch;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboEmpSearch;
        internal DevComponents.Editors.ComboItem EmployeeName;
        internal DevComponents.DotNetBar.ExpandablePanel ExpandablepnlSearch;
        internal System.Windows.Forms.GroupBox grpMain;
        internal System.Windows.Forms.DateTimePicker dtpFromDate;
        internal DevComponents.DotNetBar.LabelX lblMonth;
        internal DevComponents.DotNetBar.LabelX lblCompany;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        internal System.Windows.Forms.Button btnShow;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStripAutoFill;
        internal System.Windows.Forms.ToolStripMenuItem AutoFillToolStripMenuItem;
        internal System.Windows.Forms.Timer tmrClear;
        internal System.Windows.Forms.DateTimePicker FromTimeDateTimePicker;
        internal DevComponents.DotNetBar.PanelEx PanelRightMain;
        internal DevComponents.Editors.ComboItem cboItem25;
        internal DevComponents.Editors.ComboItem cboItem50;
        internal DevComponents.Editors.ComboItem cboItem100;
        internal DevComponents.Editors.ComboItem cboItem200;
        internal DevComponents.Editors.ComboItem cboItem500;
        internal DevComponents.Editors.ComboItem cboItemALL;
        private ClsDataGirdViewX dgvRoster;
        private DevComponents.DotNetBar.DockSite DockSite7;
        private DevComponents.DotNetBar.DotNetBarManager DotNetBarManager1;
        internal DevComponents.DotNetBar.ItemContainer ItemContainer5;
        internal DevComponents.DotNetBar.ButtonItem btnLastGridCollection;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx CboDepartment;
        internal DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDgvEmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDgvCompanyID;
        internal System.Windows.Forms.StatusStrip StatusStripBottom;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblAttendanceStatus;
        internal System.Windows.Forms.ToolStripProgressBar TlSProgressBarAttendance;
        internal DevComponents.DotNetBar.PanelEx pnlMiddileBottom;
        private DevComponents.DotNetBar.ButtonItem btnShift;
    }
}