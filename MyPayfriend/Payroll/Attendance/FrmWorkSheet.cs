﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;


namespace MyPayfriend
{
    public partial class FrmWorkSheet : Form
    {


               
        #region Declarations
        bool MblnViewPermission;
        bool MblnAddPermission;
        bool MblnUpdatePermission;
        bool MblnDeletePermission;
        bool MblnAddUpdatePermission;
        bool MblnPrintEmailPermission;
        bool FlagLoop = false;
        int MintSearchIndex = 0;

        private MessageBoxIcon MmessageIcon;
        private ArrayList MsarMessageArr;
        string MstrMessCommon = "";


        private clsBLLWorkSheet MobjClsWorkSheet;
        private ClsNotification MobjNotification;
        private ClsLogWriter MobjLog;
        private clsConnection MobjClsConnection;

 


        #endregion



        public FrmWorkSheet()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }


        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.WorkSheet, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            btnDelete.Enabled = MblnDeletePermission;
            btnSave.Enabled = MblnUpdatePermission;
            BtnPrint.Enabled = MblnPrintEmailPermission;
        }
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.WorkSheet, this);

            grpMain.Text = "معلومات عامة";
        }
        private void LoadMessage()
        {
            // Loading Message
            MsarMessageArr = new ArrayList();
            try
            {
                MsarMessageArr = MobjNotification.FillMessageArray(203, 9);
            }
            catch (Exception Ex)
            {
                MobjLog.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }
        private void ClearAllControls()
        {
            //Clearing All Controls
            lblColor.BackColor = Color.DarkSeaGreen; 
            cboCompany.SelectedIndex = -1;
            cboCompany.Text = "";
            dtpFromDate.Value = ClsCommonSettings.GetServerDate();

        }

        private void btnShow_Click(object sender, EventArgs e)
        {


            FillParameters(0); 
            FillGrid();

        }


        private void FillGrid()
        {

            DataTable dtEmployeeWorkSheet = MobjClsWorkSheet.GetWorkSheetDetails();
            GrdWorkSheet.Rows.Clear();



            for (int iCounter = 0; iCounter <= dtEmployeeWorkSheet.Rows.Count -1 ; iCounter++)
            {
                GrdWorkSheet.RowCount = GrdWorkSheet.RowCount + 1;
                GrdWorkSheet.Rows[iCounter].Cells["EmployeeID"].Value = dtEmployeeWorkSheet.Rows[iCounter]["EmployeeID"];
                GrdWorkSheet.Rows[iCounter].Cells["Employee"].Value = dtEmployeeWorkSheet.Rows[iCounter]["Employee"];
                GrdWorkSheet.Rows[iCounter].Cells["TotalDays"].Value = dtEmployeeWorkSheet.Rows[iCounter]["TotalDays"];
                GrdWorkSheet.Rows[iCounter].Cells["WorkedDays"].Value = dtEmployeeWorkSheet.Rows[iCounter]["WorkedDays"];
                GrdWorkSheet.Rows[iCounter].Cells["AbsentDays"].Value = dtEmployeeWorkSheet.Rows[iCounter]["AbsentDays"];
                GrdWorkSheet.Rows[iCounter].Cells["OTHour"].Value = dtEmployeeWorkSheet.Rows[iCounter]["OTHour"];
                GrdWorkSheet.Rows[iCounter].Cells["Remarks"].Value = dtEmployeeWorkSheet.Rows[iCounter]["Remarks"];
                GrdWorkSheet.Rows[iCounter].Cells["ToDate"].Value = dtEmployeeWorkSheet.Rows[iCounter]["ToDate"] ;
                GrdWorkSheet.Rows[iCounter].Cells["HolidayWorked"].Value = dtEmployeeWorkSheet.Rows[iCounter]["HolidayWorkCnt"].ToDecimal() ;
                GrdWorkSheet.Rows[iCounter].Cells["DateRange"].Value = dtEmployeeWorkSheet.Rows[iCounter]["DateRange"];

                GrdWorkSheet.Rows[iCounter].Cells["DateOfJoining"].Value = dtEmployeeWorkSheet.Rows[iCounter]["DateOfJoining"];

                if (dtEmployeeWorkSheet.Rows[iCounter]["IsSalary"].ToInt32() > 0)
                {
                    GrdWorkSheet.Rows[iCounter].Cells["TotalDays"].ReadOnly = true;
                    GrdWorkSheet.Rows[iCounter].Cells["WorkedDays"].ReadOnly = true;
                    GrdWorkSheet.Rows[iCounter].Cells["AbsentDays"].ReadOnly = true;
                    GrdWorkSheet.Rows[iCounter].Cells["OTHour"].ReadOnly = true;
                    GrdWorkSheet.Rows[iCounter].Cells["ToDate"].ReadOnly = true;
                    GrdWorkSheet.Rows[iCounter].DefaultCellStyle.BackColor = Color.DarkSeaGreen; 
                }
            }
        }

        private void FrmWorkSheet_Load(object sender, EventArgs e)
        {
            MobjClsWorkSheet = new clsBLLWorkSheet();
            MobjNotification = new ClsNotification();
            MobjLog = new ClsLogWriter(Application.StartupPath);
            MobjClsConnection = new clsConnection();
            //GrdWorkSheet.RowDefaultCellStyle.BackColor = Color.Bisque;
            GrdWorkSheet.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke; 
            SetPermissions();
            LoadMessage();
            LoadCombo(0);
        }
        private bool LoadCombo(int iType)
        {
            DataTable datCombos;
            if (iType == 0 || iType == 1)
            {
                datCombos = MobjClsWorkSheet.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;
                datCombos = null;
                cboCompany.SelectedValue = (ClsCommonSettings.CurrentCompanyID > 0 ? ClsCommonSettings.CurrentCompanyID : 0);

            }
            if (iType == 0 || iType == 2)
            {
                cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
                cboDepartment.DisplayMember = "Department";
                cboDepartment.ValueMember = "DepartmentID";
            }
            return true;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCombo(2);
                GrdWorkSheet.Rows.Clear();
            }
            catch
            {
            }
        }
        private void FillParameters(int Mode)
        {
            try
            {
                MobjClsWorkSheet.clsDTOWorkSheet.iCompanyID = cboCompany.SelectedValue.ToInt32();
                MobjClsWorkSheet.clsDTOWorkSheet.strFromDate = GetStratDate();
                if (cboDepartment.SelectedIndex > 0)
                {
                    MobjClsWorkSheet.clsDTOWorkSheet.iDepartmentID = cboDepartment.SelectedValue.ToInt32();
                }
                else
                {
                    MobjClsWorkSheet.clsDTOWorkSheet.iDepartmentID = 0;
                }
                if (Mode > 0)
                {
                    FillDetailParameters();
                }
            }
            catch
            {
            }

        }
        private void FillDetailParameters()
        {
            MobjClsWorkSheet.clsDTOWorkSheet.lstDTOWorkSheetDetail  = new System.Collections.Generic.List<clsDTOWorkSheetDetail>();
            try
            {
                for (int i = 0; i < GrdWorkSheet.Rows.Count; i++)
                {
                    clsDTOWorkSheetDetail objWorkSheetDetail = new clsDTOWorkSheetDetail();
                    objWorkSheetDetail.iEmployeeID = GrdWorkSheet["EmployeeID", i].Value.ToInt32();
                    objWorkSheetDetail.dblWorkedDays = GrdWorkSheet["WorkedDays", i].Value.ToDouble();
                    objWorkSheetDetail.decAbsentDays = GrdWorkSheet["AbsentDays", i].Value.ToDecimal();
                    objWorkSheetDetail.decOTHour = GrdWorkSheet["OTHour", i].Value.ToDecimal();
                    objWorkSheetDetail.strRemarks =Convert.ToString(GrdWorkSheet["Remarks", i].Value).ToString();
                    objWorkSheetDetail.strToDate = Convert.ToString(GrdWorkSheet["ToDate", i].Value).ToString();
                    objWorkSheetDetail.strDateOfJoining = Convert.ToString(GrdWorkSheet["DateOfJoining", i].Value).ToString();
                    objWorkSheetDetail.strDateRange = Convert.ToString(GrdWorkSheet["DateRange", i].Value).ToString();
                    objWorkSheetDetail.iDepartmentID = MobjClsWorkSheet.clsDTOWorkSheet.iDepartmentID;
                    objWorkSheetDetail.iCompanyID = MobjClsWorkSheet.clsDTOWorkSheet.iCompanyID;
                    objWorkSheetDetail.decTotalDays = GrdWorkSheet["TotalDays", i].Value.ToDecimal();
                    objWorkSheetDetail.strFromDate =  GetStratDate();
                    objWorkSheetDetail.decHolidayWorked = GrdWorkSheet["HolidayWorked", i].Value.ToDecimal();
                    MobjClsWorkSheet.clsDTOWorkSheet.lstDTOWorkSheetDetail.Add(objWorkSheetDetail);
                }
            }
            catch (Exception Ex)
            {
                MobjLog.WriteLog("Error on FillPolicyDetailParameters " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }

        private string GetStratDate()
        {
            //'Getting Start date
            string sMonth = "";
            switch (dtpFromDate.Value.Month)
            {
                case 1:
                    sMonth = "Jan";
                    break;
                case 2:
                    sMonth = "Feb";
                    break;
                case 3:
                    sMonth = "Mar";
                    break;
                case 4:
                    sMonth = "Apr";
                    break;
                case 5:
                    sMonth = "May";
                    break;
                case 6:
                    sMonth = "Jun";
                    break;
                case 7:
                    sMonth = "Jul";
                    break;
                case 8:
                    sMonth = "Aug";
                    break;
                case 9:
                    sMonth = "Sep";
                    break;
                case 10:
                    sMonth = "Oct";
                    break;
                case 11:
                    sMonth = "Nov";
                    break;
                case 12:
                    sMonth = "Dec";
                    break;
            }
            return Convert.ToDateTime("01-" + sMonth + "-" + dtpFromDate.Value.Year).ToString("dd MMM yyyy");
        }
        private bool FormValidation()
        {
            try
            {
                GrdWorkSheet.EndEdit(); 
                for (int i = 0; i < GrdWorkSheet.Rows.Count; i++)
                {
                    if (GrdWorkSheet.Rows[i].Cells["WorkedDays"].Value.ToInt32() + GrdWorkSheet.Rows[i].Cells["HolidayWorked"].Value.ToInt32() + GrdWorkSheet.Rows[i].Cells["AbsentDays"].Value.ToInt32() > GrdWorkSheet.Rows[i].Cells["TotalDays"].Value.ToInt32())
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 20050, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GrdWorkSheet.CurrentCell = GrdWorkSheet["WorkedDays", i];
                        return false; 
                    }
                }
                return true;
            }
            catch
            {
                return false; 
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (FormValidation() == true)
                {
                    FillParameters(1);
                    MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr,1, out MmessageIcon); ;
                    if (MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }
                    MobjClsWorkSheet.DeleteWorkSheet();
                    MobjClsWorkSheet.SaveWorkSheet();
                    MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MobjClsWorkSheet.DeleteWorkSheetEmpty();
                }
            }
            catch (Exception Ex)
            {
                MobjLog.WriteLog("Error on BarView_ItemClick " + this.Name + " " + Ex.Message.ToString(), 1);
            }
        }

        private void dtpFromDate_KeyDown(object sender, KeyEventArgs e)
        {
            dtpFromDate.Value = GetStratDate().ToDateTime();
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                dtpFromDate.Value = GetStratDate().ToDateTime();
                btnShow_Click(sender, e);
            }
            catch
            {
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void btnClear_Click(object sender, EventArgs e)
        {

            GrdWorkSheet.Rows.Clear();  

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon);
                if (MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {

                    FillParameters(0);
                    MobjClsWorkSheet.DeleteWorkSheet();
                    btnShow_Click(sender, e);
                }
            }
            catch
            {
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            FrmRptWorkSheet objAttenSum = new FrmRptWorkSheet();
            try
            {
                objAttenSum.Text = "Work Sheet"; ;
                objAttenSum.MdiParent = this.MdiParent;
                objAttenSum.WindowState = FormWindowState.Maximized;
                objAttenSum.Show();
                objAttenSum.Update();
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                MobjLog.WriteLog("Error on " + this.Name + " - Method : " + System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error : " + ex.Message.ToString(), 1);
                objAttenSum.Dispose();
                objAttenSum = null;
                GC.Collect();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);
            this.Close(); 
        }

        private void btnSaveB_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);  
        }

        private void GrdWorkSheet_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void GrdWorkSheet_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (GrdWorkSheet.IsCurrentCellDirty)
                {
                    if (GrdWorkSheet.CurrentCell != null)
                        GrdWorkSheet.CommitEdit(DataGridViewDataErrorContexts.Commit);

                }
            }
            catch
            {
            }
        }


        private void btnAutoFill_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GrdWorkSheet.Rows.Count; i++)
            {
                GrdWorkSheet["WorkedDays", i].Value = GrdWorkSheet["TotalDays", i].Value.ToDouble();
            }
        }

      


        private void GrdWorkSheet_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
             try
            {
                if (GrdWorkSheet.Rows[e.RowIndex].Cells["DateRange"].Value != null)
                {
                    if (e.RowIndex.ToInt32() >= 0)
                    {
                        lblStatus.Text = GrdWorkSheet.Rows[e.RowIndex].Cells["DateRange"].Value.ToString();
                    }
                }
            }
            catch
            {
            }
        }

        private void applyToAllToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                int Mrowindex = GrdWorkSheet.CurrentRow.Index;
                int MColindex = GrdWorkSheet.CurrentCell.ColumnIndex;
                if (Mrowindex < 0) return;
                if (GrdWorkSheet.Rows[Mrowindex].Cells[MColindex].Value != DBNull.Value)
                {
                    for (int i = Mrowindex; i < GrdWorkSheet.Rows.Count; i++)
                    {
                            if (GrdWorkSheet.Rows[Mrowindex].Cells[MColindex].Value.ToString() != null)
                            {
                                GrdWorkSheet.Rows[i].Cells[MColindex].Value = GrdWorkSheet.Rows[Mrowindex].Cells[MColindex].Value;
                            }
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjLog.WriteLog("Error on applyToAllToolStripMenuItem_Click " + this.Name + " " + Ex.Message.ToString(), 1);
            }



        }

        private void GrdWorkSheet_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            try
            {
                int Mrowindex = GrdWorkSheet.CurrentRow.Index;
                int MColindex = GrdWorkSheet.CurrentCell.ColumnIndex;

                if (GrdWorkSheet.CurrentRow.Cells[MColindex].Value != DBNull.Value)
                {
                        if (Mrowindex != -1 && MColindex >= 0)
                        {
                            if (MColindex >2)
                            {
                                if (e.Button == MouseButtons.Right)
                                {
                                    this.CopyDetailsContextMenuStrip.Show(this.GrdWorkSheet, this.GrdWorkSheet.PointToClient(Cursor.Position));
                                }
                            }
                        }
                }
            }
            catch (Exception Ex)
            {
                MobjLog.WriteLog("Error on GrdWorkSheet_CellMouseClick " + this.Name + " " + Ex.Message.ToString(), 1);
            }



        }

        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }
        void txtInt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}.{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0))
            {
                e.Handled = true;
            }
        }

        private void GrdWorkSheet_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
              
                if (GrdWorkSheet.CurrentCell != null)
                {
                    TextBox txt = (TextBox)(e.Control);
                    if (GrdWorkSheet.CurrentCell.ColumnIndex == WorkedDays.Index || GrdWorkSheet.CurrentCell.ColumnIndex == OTHour.Index ||
                        GrdWorkSheet.CurrentCell.ColumnIndex == AbsentDays.Index)
                    {
                        
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                    else
                    {
                        txt.KeyPress -= new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }

        private void GrdWorkSheet_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                int iType = 0;
                if (e.RowIndex > -1 && (e.ColumnIndex > EmployeeID.Index))
                {
                    if (IsCellEditable(e.RowIndex, e.ColumnIndex, out iType) == false)
                    {
                        //sALARY
                        if (iType == Convert.ToInt32(CellEditableTypes.SalaryIsReleased))
                        {
                            MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10089, out MmessageIcon);
                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }
            catch
            {
            }
        }
        public bool IsCellEditable(int iRowIndex, int iColumnIndex, out int iType)
        {
            //Checking the current cell is editable or not

            //iType = 1 Holiday
            //iType = 2 halfday leave
            //iType = 3 Full day leave
            //iType = 4 Attendance Auto Exists
            //iType = 5 Salary Is released
            //iType = 6 Vacation Exists
            iType = 0;
            if (GrdWorkSheet.Rows[iRowIndex].Cells["EmployeeID"].Tag != null)
            {
                int iEmpID = GrdWorkSheet.Rows[iRowIndex].Cells["EmployeeID"].Tag.ToInt32();
                string strDate = GrdWorkSheet.Columns[iColumnIndex].Name.Trim() + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year;
                bool blnRetValue = true;

                string strDayInfo = GrdWorkSheet.Rows[iRowIndex].Cells[iColumnIndex].Tag.ToStringCustom();

                bool blnIsSalaryReleased = this.MobjClsWorkSheet.IsSalaryReleased(iEmpID, strDate);

                if (blnIsSalaryReleased)
                {
                    iType = Convert.ToInt32(CellEditableTypes.SalaryIsReleased);
                    //5 Salary Is released
                    blnRetValue = false;
                }

                return blnRetValue;

            }
            else
            {
                return false;
            }
        }


        private enum CellEditableTypes
        {
            Holiday = 1,
            HalfDayLeave = 2,
            FullDayLeave = 3,
            AttendanceAutoExists = 4,
            SalaryIsReleased = 5,
            VacationExists = 6
        }


        private void cboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }


        private void cboDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            cboDepartment.DroppedDown = false; 
        }

        private void BarView_ItemClick(object sender, EventArgs e)
        {

        }




    }
}
