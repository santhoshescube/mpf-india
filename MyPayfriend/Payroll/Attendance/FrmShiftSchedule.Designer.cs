﻿namespace MyPayfriend
{
    partial class FrmShiftSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label ToTimeLabel;
            System.Windows.Forms.Label lblShiftTime;
            System.Windows.Forms.Label FromTimeLabel;
            System.Windows.Forms.Label lblShiftPolicy;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmShiftSchedule));
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.txtScheduleName = new System.Windows.Forms.TextBox();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.lblScheduleName = new System.Windows.Forms.Label();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnAddNew = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.ShiftSheduleBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnShift = new System.Windows.Forms.Button();
            this.dgvShift = new System.Windows.Forms.DataGridView();
            this.IsCheckedDB = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IsChecked = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DesignationID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblShiftTimeDisplay = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.cboShiftPolicy = new System.Windows.Forms.ComboBox();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboFilterCompany = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboFilterDesignation = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboFilterDepartment = new System.Windows.Forms.ComboBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatusShow = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.errValidate = new System.Windows.Forms.ErrorProvider(this.components);
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            ToTimeLabel = new System.Windows.Forms.Label();
            lblShiftTime = new System.Windows.Forms.Label();
            FromTimeLabel = new System.Windows.Forms.Label();
            lblShiftPolicy = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftSheduleBindingNavigator)).BeginInit();
            this.ShiftSheduleBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShift)).BeginInit();
            this.GrpMain.SuspendLayout();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToTimeLabel
            // 
            ToTimeLabel.AutoSize = true;
            ToTimeLabel.Location = new System.Drawing.Point(238, 78);
            ToTimeLabel.Name = "ToTimeLabel";
            ToTimeLabel.Size = new System.Drawing.Size(46, 13);
            ToTimeLabel.TabIndex = 17;
            ToTimeLabel.Text = "To Date";
            // 
            // lblShiftTime
            // 
            lblShiftTime.AutoSize = true;
            lblShiftTime.Location = new System.Drawing.Point(13, 51);
            lblShiftTime.Name = "lblShiftTime";
            lblShiftTime.Size = new System.Drawing.Size(54, 13);
            lblShiftTime.TabIndex = 13;
            lblShiftTime.Text = "Shift Time";
            // 
            // FromTimeLabel
            // 
            FromTimeLabel.AutoSize = true;
            FromTimeLabel.Location = new System.Drawing.Point(13, 78);
            FromTimeLabel.Name = "FromTimeLabel";
            FromTimeLabel.Size = new System.Drawing.Size(56, 13);
            FromTimeLabel.TabIndex = 16;
            FromTimeLabel.Text = "From Date";
            // 
            // lblShiftPolicy
            // 
            lblShiftPolicy.AutoSize = true;
            lblShiftPolicy.Location = new System.Drawing.Point(238, 25);
            lblShiftPolicy.Name = "lblShiftPolicy";
            lblShiftPolicy.Size = new System.Drawing.Size(59, 13);
            lblShiftPolicy.TabIndex = 12;
            lblShiftPolicy.Text = "Shift Policy";
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // txtScheduleName
            // 
            this.txtScheduleName.BackColor = System.Drawing.SystemColors.Info;
            this.txtScheduleName.Location = new System.Drawing.Point(102, 22);
            this.txtScheduleName.Name = "txtScheduleName";
            this.txtScheduleName.Size = new System.Drawing.Size(130, 20);
            this.txtScheduleName.TabIndex = 0;
            this.txtScheduleName.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // lblScheduleName
            // 
            this.lblScheduleName.AutoSize = true;
            this.lblScheduleName.Location = new System.Drawing.Point(13, 25);
            this.lblScheduleName.Name = "lblScheduleName";
            this.lblScheduleName.Size = new System.Drawing.Size(83, 13);
            this.lblScheduleName.TabIndex = 11;
            this.lblScheduleName.Text = "Schedule Name";
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(23, 176);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(15, 14);
            this.chkSelectAll.TabIndex = 22;
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddNew.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNew.Image")));
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.RightToLeftAutoMirrorImage = true;
            this.btnAddNew.Size = new System.Drawing.Size(23, 22);
            this.btnAddNew.Text = "Add new";
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save Data";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Cancel";
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // ShiftSheduleBindingNavigator
            // 
            this.ShiftSheduleBindingNavigator.AddNewItem = null;
            this.ShiftSheduleBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.ShiftSheduleBindingNavigator.DeleteItem = null;
            this.ShiftSheduleBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.btnAddNew,
            this.btnSaveItem,
            this.btnDelete,
            this.btnClear,
            this.ToolStripSeparator1,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator3,
            this.btnHelp});
            this.ShiftSheduleBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ShiftSheduleBindingNavigator.MoveFirstItem = null;
            this.ShiftSheduleBindingNavigator.MoveLastItem = null;
            this.ShiftSheduleBindingNavigator.MoveNextItem = null;
            this.ShiftSheduleBindingNavigator.MovePreviousItem = null;
            this.ShiftSheduleBindingNavigator.Name = "ShiftSheduleBindingNavigator";
            this.ShiftSheduleBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.ShiftSheduleBindingNavigator.Size = new System.Drawing.Size(514, 25);
            this.ShiftSheduleBindingNavigator.TabIndex = 4;
            this.ShiftSheduleBindingNavigator.Text = "BindingNavigator1";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(7, 422);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(432, 422);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(350, 422);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnShift
            // 
            this.btnShift.Location = new System.Drawing.Point(455, 20);
            this.btnShift.Name = "btnShift";
            this.btnShift.Size = new System.Drawing.Size(32, 23);
            this.btnShift.TabIndex = 2;
            this.btnShift.Text = "...";
            this.btnShift.UseVisualStyleBackColor = true;
            this.btnShift.Click += new System.EventHandler(this.btnShift_Click);
            // 
            // dgvShift
            // 
            this.dgvShift.AllowUserToAddRows = false;
            this.dgvShift.AllowUserToDeleteRows = false;
            this.dgvShift.AllowUserToResizeColumns = false;
            this.dgvShift.AllowUserToResizeRows = false;
            this.dgvShift.BackgroundColor = System.Drawing.Color.White;
            this.dgvShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShift.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IsCheckedDB,
            this.IsChecked,
            this.CompanyID,
            this.EmployeeID,
            this.EmployeeName,
            this.DepartmentID,
            this.DesignationID});
            this.dgvShift.Location = new System.Drawing.Point(16, 171);
            this.dgvShift.Name = "dgvShift";
            this.dgvShift.RowHeadersVisible = false;
            this.dgvShift.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShift.Size = new System.Drawing.Size(470, 181);
            this.dgvShift.TabIndex = 8;
            this.dgvShift.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvShift_CellClick);
            this.dgvShift.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvShift_DataError);
            // 
            // IsCheckedDB
            // 
            this.IsCheckedDB.HeaderText = "IsCheckedDB";
            this.IsCheckedDB.Name = "IsCheckedDB";
            this.IsCheckedDB.ReadOnly = true;
            this.IsCheckedDB.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsCheckedDB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IsCheckedDB.Visible = false;
            // 
            // IsChecked
            // 
            this.IsChecked.DataPropertyName = "IsChecked";
            this.IsChecked.HeaderText = "";
            this.IsChecked.Name = "IsChecked";
            this.IsChecked.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IsChecked.Width = 25;
            // 
            // CompanyID
            // 
            this.CompanyID.DataPropertyName = "CompanyID";
            this.CompanyID.HeaderText = "CompanyID";
            this.CompanyID.Name = "CompanyID";
            this.CompanyID.Visible = false;
            // 
            // EmployeeID
            // 
            this.EmployeeID.DataPropertyName = "EmployeeID";
            this.EmployeeID.HeaderText = "EmployeeID";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.ReadOnly = true;
            this.EmployeeID.Visible = false;
            // 
            // EmployeeName
            // 
            this.EmployeeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EmployeeName.DataPropertyName = "EmployeeName";
            this.EmployeeName.HeaderText = "Employee Name";
            this.EmployeeName.Name = "EmployeeName";
            this.EmployeeName.ReadOnly = true;
            // 
            // DepartmentID
            // 
            this.DepartmentID.DataPropertyName = "DepartmentID";
            this.DepartmentID.HeaderText = "DepartmentID";
            this.DepartmentID.Name = "DepartmentID";
            this.DepartmentID.ReadOnly = true;
            this.DepartmentID.Visible = false;
            // 
            // DesignationID
            // 
            this.DesignationID.DataPropertyName = "DesignationID";
            this.DesignationID.HeaderText = "DesignationID";
            this.DesignationID.Name = "DesignationID";
            this.DesignationID.ReadOnly = true;
            this.DesignationID.Visible = false;
            // 
            // lblShiftTimeDisplay
            // 
            this.lblShiftTimeDisplay.AutoSize = true;
            this.lblShiftTimeDisplay.Location = new System.Drawing.Point(115, 51);
            this.lblShiftTimeDisplay.Name = "lblShiftTimeDisplay";
            this.lblShiftTimeDisplay.Size = new System.Drawing.Size(54, 13);
            this.lblShiftTimeDisplay.TabIndex = 15;
            this.lblShiftTimeDisplay.Text = "Shift Time";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(303, 75);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(106, 20);
            this.dtpToDate.TabIndex = 4;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(102, 75);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(106, 20);
            this.dtpFromDate.TabIndex = 3;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // cboShiftPolicy
            // 
            this.cboShiftPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboShiftPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboShiftPolicy.BackColor = System.Drawing.SystemColors.Info;
            this.cboShiftPolicy.DropDownHeight = 134;
            this.cboShiftPolicy.FormattingEnabled = true;
            this.cboShiftPolicy.IntegralHeight = false;
            this.cboShiftPolicy.Location = new System.Drawing.Point(303, 22);
            this.cboShiftPolicy.Name = "cboShiftPolicy";
            this.cboShiftPolicy.Size = new System.Drawing.Size(146, 21);
            this.cboShiftPolicy.TabIndex = 1;
            this.cboShiftPolicy.SelectedIndexChanged += new System.EventHandler(this.cboShiftName_SelectedIndexChanged);
            this.cboShiftPolicy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboShiftPolicy.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.label1);
            this.GrpMain.Controls.Add(this.cboFilterCompany);
            this.GrpMain.Controls.Add(this.label5);
            this.GrpMain.Controls.Add(this.btnShift);
            this.GrpMain.Controls.Add(lblShiftPolicy);
            this.GrpMain.Controls.Add(this.cboShiftPolicy);
            this.GrpMain.Controls.Add(this.lblScheduleName);
            this.GrpMain.Controls.Add(this.txtScheduleName);
            this.GrpMain.Controls.Add(this.chkSelectAll);
            this.GrpMain.Controls.Add(this.label4);
            this.GrpMain.Controls.Add(this.dgvShift);
            this.GrpMain.Controls.Add(this.label3);
            this.GrpMain.Controls.Add(this.cboFilterDesignation);
            this.GrpMain.Controls.Add(this.label2);
            this.GrpMain.Controls.Add(this.cboFilterDepartment);
            this.GrpMain.Controls.Add(this.Label7);
            this.GrpMain.Controls.Add(this.lblShiftTimeDisplay);
            this.GrpMain.Controls.Add(ToTimeLabel);
            this.GrpMain.Controls.Add(this.dtpToDate);
            this.GrpMain.Controls.Add(FromTimeLabel);
            this.GrpMain.Controls.Add(this.dtpFromDate);
            this.GrpMain.Controls.Add(lblShiftTime);
            this.GrpMain.Controls.Add(this.shapeContainer1);
            this.GrpMain.Location = new System.Drawing.Point(7, 56);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(500, 360);
            this.GrpMain.TabIndex = 0;
            this.GrpMain.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(99, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = ":";
            // 
            // cboFilterCompany
            // 
            this.cboFilterCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterCompany.DropDownHeight = 134;
            this.cboFilterCompany.FormattingEnabled = true;
            this.cboFilterCompany.IntegralHeight = false;
            this.cboFilterCompany.Location = new System.Drawing.Point(19, 144);
            this.cboFilterCompany.Name = "cboFilterCompany";
            this.cboFilterCompany.Size = new System.Drawing.Size(195, 21);
            this.cboFilterCompany.TabIndex = 5;
            this.cboFilterCompany.SelectedIndexChanged += new System.EventHandler(this.cboFilter_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Company";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Employee Info";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(350, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Designation";
            // 
            // cboFilterDesignation
            // 
            this.cboFilterDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterDesignation.DropDownHeight = 134;
            this.cboFilterDesignation.FormattingEnabled = true;
            this.cboFilterDesignation.IntegralHeight = false;
            this.cboFilterDesignation.Location = new System.Drawing.Point(356, 144);
            this.cboFilterDesignation.Name = "cboFilterDesignation";
            this.cboFilterDesignation.Size = new System.Drawing.Size(130, 21);
            this.cboFilterDesignation.TabIndex = 7;
            this.cboFilterDesignation.SelectedIndexChanged += new System.EventHandler(this.cboFilter_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Department";
            // 
            // cboFilterDepartment
            // 
            this.cboFilterDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterDepartment.DropDownHeight = 134;
            this.cboFilterDepartment.FormattingEnabled = true;
            this.cboFilterDepartment.IntegralHeight = false;
            this.cboFilterDepartment.Location = new System.Drawing.Point(220, 144);
            this.cboFilterDepartment.Name = "cboFilterDepartment";
            this.cboFilterDepartment.Size = new System.Drawing.Size(130, 21);
            this.cboFilterDepartment.TabIndex = 6;
            this.cboFilterDepartment.SelectedIndexChanged += new System.EventHandler(this.cboFilter_SelectedIndexChanged);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(7, 0);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(84, 13);
            this.Label7.TabIndex = 9;
            this.Label7.Text = "Schedule Info";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(494, 341);
            this.shapeContainer1.TabIndex = 10;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.Color.LightGray;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 43;
            this.lineShape2.X2 = 482;
            this.lineShape2.Y1 = 94;
            this.lineShape2.Y2 = 94;
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusShow,
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 452);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(514, 22);
            this.ssStatus.TabIndex = 5;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblStatusShow
            // 
            this.lblStatusShow.Name = "lblStatusShow";
            this.lblStatusShow.Size = new System.Drawing.Size(48, 17);
            this.lblStatusShow.Text = "Status : ";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // tmrClear
            // 
            this.tmrClear.Interval = 2000;
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // errValidate
            // 
            this.errValidate.ContainerControl = this;
            this.errValidate.RightToLeft = true;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(514, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 1024;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // FrmShiftSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 474);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.ShiftSheduleBindingNavigator);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmShiftSchedule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shift Schedule";
            this.Load += new System.EventHandler(this.FrmShiftSchedule_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmShiftSchedule_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmShiftSchedule_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ShiftSheduleBindingNavigator)).EndInit();
            this.ShiftSheduleBindingNavigator.ResumeLayout(false);
            this.ShiftSheduleBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShift)).EndInit();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.TextBox txtScheduleName;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.Label lblScheduleName;
        internal System.Windows.Forms.CheckBox chkSelectAll;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.BindingNavigator ShiftSheduleBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton btnAddNew;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.DataGridView dgvShift;
        internal System.Windows.Forms.Label lblShiftTimeDisplay;
        internal System.Windows.Forms.Button btnShift;
        internal System.Windows.Forms.DateTimePicker dtpToDate;
        internal System.Windows.Forms.DateTimePicker dtpFromDate;
        internal System.Windows.Forms.ComboBox cboShiftPolicy;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.ComboBox cboFilterDesignation;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.ComboBox cboFilterDepartment;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        internal System.Windows.Forms.Label label4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatusShow;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.ErrorProvider errValidate;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.ComboBox cboFilterCompany;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsCheckedDB;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsChecked;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartmentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DesignationID;
    }
}