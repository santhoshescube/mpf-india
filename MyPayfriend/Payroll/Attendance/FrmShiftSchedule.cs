﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Microsoft.VisualBasic;
/*
 * Created By       : Tijo
 * Created Date     : 22 Aug 2013
 * Purpose          : For Shift Schedule
*/
namespace MyPayfriend
{
    public partial class FrmShiftSchedule : Form
    {
        clsBLLShiftSchedule MobjclsBLLShiftSchedule = new clsBLLShiftSchedule();
        long TotalRecordCnt, CurrentRecCnt;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool mblnAddStatus = true;
        private clsMessage objUserMessage = null;
        DataTable datEmployee = new DataTable();
        string strBindingOf = "Of ";

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.ShiftSchedule);

                return this.objUserMessage;
            }
        }

        public FrmShiftSchedule()
        {
            InitializeComponent();
            tmrClear.Interval = ClsCommonSettings.TimerInterval;

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.ShiftSchedule, this);

            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            lblStatusShow.Text = "حالة :";
            strBindingOf = "من ";
        }

        private void FrmShiftSchedule_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos(0);
            AddNew();
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.ShiftSchedule,
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadCombos(int intType)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0 || intType == 1)
            {
                datTemp = MobjclsBLLShiftSchedule.FillCombos(new string[] { "ShiftID,ShiftName", "PayShiftReference", "" });
                cboShiftPolicy.ValueMember = "ShiftID";
                cboShiftPolicy.DisplayMember = "ShiftName";
                cboShiftPolicy.DataSource = datTemp;
            }

            if (intType == 0)
            {
                datTemp = MobjclsBLLShiftSchedule.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });
                cboFilterCompany.ValueMember = "CompanyID";
                cboFilterCompany.DisplayMember = "CompanyName";
                cboFilterCompany.DataSource = datTemp;

                if (ClsCommonSettings.IsArabicView)
                {
                    datTemp = MobjclsBLLShiftSchedule.FillCombos(new string[] { "0 AS DepartmentID,'كل' AS Department UNION " +
                    "SELECT DepartmentID,DepartmentArb AS Department", "DepartmentReference", "" });
                }
                else
                {
                    datTemp = MobjclsBLLShiftSchedule.FillCombos(new string[] { "0 AS DepartmentID,'All' AS Department UNION " +
                    "SELECT DepartmentID,Department", "DepartmentReference", "" });
                }
                cboFilterDepartment.ValueMember = "DepartmentID";
                cboFilterDepartment.DisplayMember = "Department";
                cboFilterDepartment.DataSource = datTemp;

                if (ClsCommonSettings.IsArabicView)
                {
                    datTemp = MobjclsBLLShiftSchedule.FillCombos(new string[] { "0 AS DesignationID,'كل' AS Designation UNION " +
                        "SELECT DesignationID,DesignationArb AS Designation", "DesignationReference", "" });
                }
                else
                {
                    datTemp = MobjclsBLLShiftSchedule.FillCombos(new string[] { "0 AS DesignationID,'All' AS Designation UNION " +
                    "SELECT DesignationID,Designation", "DesignationReference", "" });
                }
                cboFilterDesignation.ValueMember = "DesignationID";
                cboFilterDesignation.DisplayMember = "Designation";
                cboFilterDesignation.DataSource = datTemp;
            }
        }

        private void AddNew()
        {
            mblnAddStatus = true;
            txtScheduleName.Tag = 0;
            LoadCombos(0);
            cboShiftPolicy.SelectedIndex = -1;
            txtScheduleName.Text = lblShiftTimeDisplay.Text = txtSearch.Text = "";
            dtpFromDate.Value = dtpToDate.Value = ClsCommonSettings.GetServerDate();
            dtpFromDate.Tag = dtpToDate.Tag = ClsCommonSettings.GetServerDate();          
            dgvShift.Rows.Clear();
            datEmployee = MobjclsBLLShiftSchedule.getEmployeeDetails(0, 0, 0); // get Employee Details mapped into a common table
            cboFilterCompany.SelectedIndex = cboFilterDepartment.SelectedIndex = cboFilterDesignation.SelectedIndex = -1;
            cboShiftPolicy.Tag = 0;
            chkSelectAll.Checked = false;
            lblstatus.Text = "";
            tmrClear.Enabled = false;
            errValidate.Clear();

            SetAutoCompleteList();
            RecordCount();
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt + 1).ToString();
            BindingNavigatorPositionItem.Text = (TotalRecordCnt + 1).ToString();

            GrpMain.Enabled = cboShiftPolicy.Enabled = true;

            btnClear.Enabled = true;
            btnAddNew.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false;

            txtScheduleName.Focus();
        }
        
        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "", "PayShiftScheduleDetails");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }

        private void RecordCount()
        {
            MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.strSearchKey = txtSearch.Text.Trim(); // Is any search text filteration will be taken that also
            TotalRecordCnt = MobjclsBLLShiftSchedule.GetRecordCount();
            CurrentRecCnt = TotalRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }

            BindingEnableDisable();
        }

        private void BindingEnableDisable() // Navigator visibility settings
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

        private void cboShiftName_SelectedIndexChanged(object sender, EventArgs e)
        {
            getShiftTimeDisplay();
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            // For new Shift entry
            int intTempID = cboShiftPolicy.SelectedValue.ToInt32(); // For setting selected Shift after reference form closing
            FrmShiftPolicy objShiftPolicy = new FrmShiftPolicy();
            objShiftPolicy.ShowDialog();
            LoadCombos(1);

            if (intTempID != 0)
                cboShiftPolicy.SelectedValue = intTempID;

            objShiftPolicy.Dispose();
            getShiftTimeDisplay();
        }

        private void getShiftTimeDisplay()
        {
            try
            {
                lblShiftTimeDisplay.Text = "";

                if (cboShiftPolicy.SelectedIndex >= 0)
                {
                    DataTable datTemp = MobjclsBLLShiftSchedule.FillCombos(new string[] { "FromTime,ToTime,Duration," +
                        "Replace(MinWorkingHours,'.',':') AS MinWorkingHours", "" +
                        "PayShiftReference", "ShiftID = " + cboShiftPolicy.SelectedValue.ToInt32() + "" });

                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            if (ClsCommonSettings.Glb24HourFormat)
                            {
                                lblShiftTimeDisplay.Text = Strings.Format(Convert.ToDateTime(datTemp.Rows[0]["FromTime"]), "HH:mm ") + " To " +
                                 "" + Strings.Format(Convert.ToDateTime(datTemp.Rows[0]["ToTime"]), "HH:mm tt") +
                                 " [Duration = " + datTemp.Rows[0]["Duration"] + ", Min.WorkHrs = " + datTemp.Rows[0]["MinWorkingHours"] + "]";
                            }
                            else
                            {
                                lblShiftTimeDisplay.Text = Strings.Format(Convert.ToDateTime(datTemp.Rows[0]["FromTime"]), "hh:mm tt") + " To " +
                                  "" + Strings.Format(Convert.ToDateTime(datTemp.Rows[0]["ToTime"]), "hh:mm tt") +
                                  " [Duration = " + datTemp.Rows[0]["Duration"] + ", Min.WorkHrs = " + datTemp.Rows[0]["MinWorkingHours"] + "]";
                            }
                        }
                    }
                }
            }
            catch { }
        }

        private void getFilterEmployee()
        {
            bool isFiltered = true;
            DataTable datTemp = datEmployee;

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (cboFilterCompany.SelectedValue.ToInt32() > 0 && cboFilterDepartment.SelectedValue.ToInt32() > 0 && cboFilterDesignation.SelectedValue.ToInt32() > 0)
                        datTemp.DefaultView.RowFilter = "CompanyID = " + cboFilterCompany.SelectedValue.ToInt32() + " AND " +
                            "DepartmentID = " + cboFilterDepartment.SelectedValue.ToInt32() + " AND " +
                            "DesignationID = " + cboFilterDesignation.SelectedValue.ToInt32();
                    else if (cboFilterCompany.SelectedValue.ToInt32() > 0 && cboFilterDepartment.SelectedValue.ToInt32() == 0 && cboFilterDesignation.SelectedValue.ToInt32() == 0)
                        datTemp.DefaultView.RowFilter = "CompanyID = " + cboFilterCompany.SelectedValue.ToInt32();
                    else if (cboFilterCompany.SelectedValue.ToInt32() == 0 && cboFilterDepartment.SelectedValue.ToInt32() > 0 && cboFilterDesignation.SelectedValue.ToInt32() == 0)
                        datTemp.DefaultView.RowFilter = "DepartmentID = " + cboFilterDepartment.SelectedValue.ToInt32();
                    else if (cboFilterCompany.SelectedValue.ToInt32() == 0 && cboFilterDepartment.SelectedValue.ToInt32() == 0 && cboFilterDesignation.SelectedValue.ToInt32() > 0)
                        datTemp.DefaultView.RowFilter = "DesignationID = " + cboFilterDesignation.SelectedValue.ToInt32();
                    else if (cboFilterCompany.SelectedValue.ToInt32() > 0 && cboFilterDepartment.SelectedValue.ToInt32() > 0 && cboFilterDesignation.SelectedValue.ToInt32() == 0)
                        datTemp.DefaultView.RowFilter = "CompanyID = " + cboFilterCompany.SelectedValue.ToInt32() + " AND " +
                            "DepartmentID = " + cboFilterDepartment.SelectedValue.ToInt32();
                    else if (cboFilterCompany.SelectedValue.ToInt32() > 0 && cboFilterDepartment.SelectedValue.ToInt32() == 0 && cboFilterDesignation.SelectedValue.ToInt32() > 0)
                        datTemp.DefaultView.RowFilter = "CompanyID = " + cboFilterCompany.SelectedValue.ToInt32() + " AND " +
                            "DesignationID = " + cboFilterDesignation.SelectedValue.ToInt32();
                    else if (cboFilterCompany.SelectedValue.ToInt32() == 0 && cboFilterDepartment.SelectedValue.ToInt32() > 0 && cboFilterDesignation.SelectedValue.ToInt32() > 0)
                        datTemp.DefaultView.RowFilter = "DepartmentID = " + cboFilterDepartment.SelectedValue.ToInt32() + " AND " +
                            "DesignationID = " + cboFilterDesignation.SelectedValue.ToInt32();
                    else
                        isFiltered = false;

                    DataTable datTempEmployee = new DataTable();
                    if (isFiltered)
                        datTempEmployee = datTemp.DefaultView.ToTable();
                    else
                        datTempEmployee = datEmployee;

                    FillGrid(datTempEmployee);
                }
            }
        }

        private void FillGrid(DataTable datTemp)
        {
            dgvShift.Rows.Clear();

            for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
            {
                dgvShift.Rows.Add();
                dgvShift.Rows[iCounter].Cells["IsCheckedDB"].Value = dgvShift.Rows[iCounter].Cells["IsChecked"].Value = Convert.ToBoolean(datTemp.Rows[iCounter]["IsChecked"].ToInt32());
                dgvShift.Rows[iCounter].Cells["CompanyID"].Value = datTemp.Rows[iCounter]["CompanyID"].ToInt32();
                dgvShift.Rows[iCounter].Cells["EmployeeID"].Value = datTemp.Rows[iCounter]["EmployeeID"].ToInt64();
                dgvShift.Rows[iCounter].Cells["EmployeeName"].Value = datTemp.Rows[iCounter]["EmployeeName"].ToString();
                dgvShift.Rows[iCounter].Cells["DepartmentID"].Value = datTemp.Rows[iCounter]["DepartmentID"].ToInt32();
                dgvShift.Rows[iCounter].Cells["DesignationID"].Value = datTemp.Rows[iCounter]["DesignationID"].ToInt32();
            }

            dgvShift.ClearSelection();
            txtScheduleName.Focus();
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int iCounter = 0; iCounter <= dgvShift.Rows.Count - 1; iCounter++)
                dgvShift.Rows[iCounter].Cells["IsChecked"].Value = chkSelectAll.Checked;

            ChangeControlStatus(sender, e);
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            SaveShiftSchedule();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveShiftSchedule())
            {
                btnSave.Enabled = false;
                this.Close();
            }
        }

        private bool SaveShiftSchedule()
        {
            if (FormValidation())
            {
                bool blnSave = false;

                if (mblnAddStatus)
                {
                    if (this.UserMessage.ShowMessage(1) == true) // Save
                        blnSave = true;
                }
                else
                {
                    if (this.UserMessage.ShowMessage(3) == true) // Update
                        blnSave = true;
                }

                if (blnSave)
                {
                    FillParameters(); // fill parameters for saving the content
                    FillDetailParameters(); // fill parameters Details for saving the content

                    int intRetValue = MobjclsBLLShiftSchedule.SaveShiftScheduleMaster();

                    if (intRetValue > 0)
                    {
                        if (mblnAddStatus)
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(2);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(2); // Saved Successfully
                        }
                        else
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(21);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(21); // Updated Successfully
                        }

                        DisplayShiftSheduleAgainstRecordID(intRetValue); // to display the current saved or updated content
                        SetAutoCompleteList();
                        return true;
                    }
                }
            }

            return false;
        }

        private bool FormValidation()
        {
            btnShift.Focus();
            errValidate.Clear();

            if (txtScheduleName.Text.Trim() == "") // if Schedule Name not entered
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(7108);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(7108, txtScheduleName, errValidate);
                return false;
            }

            // Check Shcedulae Name Exists
            if (MobjclsBLLShiftSchedule.CheckShiftScheduleNameExists(txtScheduleName.Tag.ToInt32(), txtScheduleName.Text.Trim()))
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(7109);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(7109, txtScheduleName, errValidate);
                return false;
            }

            if (cboShiftPolicy.SelectedIndex == -1) // if Shift Policy is not selected properly
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(7101);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(7101, cboShiftPolicy, errValidate);
                return false;
            }

            if (dtpFromDate.Value.Date > dtpToDate.Value.Date) // From and To dates
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(7107);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(7107, dtpToDate, errValidate);
                return false;
            }

            if (dgvShift.RowCount == 0)  // If there is no employee in the grid
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(7103);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(7103);
                return false;
            }

            if (dgvShift.RowCount > 0)  // If employee not selected
            {
                bool blnSelected = false;

                for (int iCounter = 0; iCounter <= dgvShift.Rows.Count - 1; iCounter++)
                {
                    if (Convert.ToBoolean(dgvShift.Rows[iCounter].Cells["IsChecked"].Value) == true)
                    {
                        blnSelected = true;
                        break;
                    }
                }

                if (!blnSelected)
                {
                    lblstatus.Text = this.UserMessage.GetMessageByCode(7102);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(7102);
                    return false;
                }
                else
                {
                    for (int iCounter = 0; iCounter <= dgvShift.Rows.Count - 1; iCounter++)
                    {
                        bool bShiftExists = false;
                        bool bPaymentExists = false;
                        bool bAttendanceExists = false;

                        if (Convert.ToBoolean(dgvShift.Rows[iCounter].Cells["IsCheckedDB"].Value) == true || // Check Employee in DB and Currently selected
                            Convert.ToBoolean(dgvShift.Rows[iCounter].Cells["IsChecked"].Value) == true)
                        {
                            bool blnDataChanged = false;

                            if (datEmployee.Rows[iCounter]["EmployeeID"].ToInt64() == dgvShift.Rows[iCounter].Cells["EmployeeID"].Value.ToInt64())
                            {
                                if (dtpFromDate.Value.Date.ToString("dd MMM yyyy") != dtpFromDate.Tag.ToDateTime().ToString("dd MMM yyyy") ||
                                    dtpToDate.Value.Date.ToString("dd MMM yyyy") != dtpToDate.Tag.ToDateTime().ToString("dd MMM yyyy"))
                                    blnDataChanged = true;

                                if (Convert.ToBoolean(datEmployee.Rows[iCounter]["IsChecked"]) != 
                                    Convert.ToBoolean(dgvShift.Rows[iCounter].Cells["IsChecked"].Value))
                                    blnDataChanged = true;

                                if (cboShiftPolicy.Tag.ToInt32() != cboShiftPolicy.SelectedValue.ToInt32())
                                    blnDataChanged = true;
                            }

                            if (blnDataChanged)
                            {
                                MobjclsBLLShiftSchedule.CheckShiftExists((dtpFromDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime(),
                                    (dtpToDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime(),
                                    txtScheduleName.Tag.ToInt32(), txtScheduleName.Text.Trim(), dgvShift.Rows[iCounter].Cells["EmployeeID"].Value.ToInt64(),
                                    ref bShiftExists, ref bPaymentExists, ref bAttendanceExists);

                                if (bShiftExists == true)     // Shift already exists for an employee in the given dates
                                {
                                    lblstatus.Text = this.UserMessage.GetMessageByCode(7104);
                                    tmrClear.Enabled = true;
                                    this.UserMessage.ShowMessage(7104);
                                    return false;
                                }
                                else if (bPaymentExists == true)  // Salary has already released for an employee in the given dates
                                {
                                    lblstatus.Text = this.UserMessage.GetMessageByCode(7105);
                                    tmrClear.Enabled = true;
                                    this.UserMessage.ShowMessage(7105);
                                    return false;
                                }
                                else if (bAttendanceExists == true)   // Attendance has already entered for an employee in the given dates
                                {
                                    lblstatus.Text = this.UserMessage.GetMessageByCode(7106);
                                    tmrClear.Enabled = true;
                                    this.UserMessage.ShowMessage(7106);
                                    return false;
                                }
                            }
                        }
                    }
                }
            }            

            return true;
        }

        private void FillParameters()
        {
            MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.intShiftScheduleID = txtScheduleName.Tag.ToInt32();
            MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.strScheduleName = txtScheduleName.Text;
            MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.intShiftID = cboShiftPolicy.SelectedValue.ToInt32();
            MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.dtFromDate = (dtpFromDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.dtToDate = (dtpToDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
        }

        private void FillDetailParameters()
        {
            MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.IlstclsDTOShiftScheduleDetails = new System.Collections.Generic.List<clsDTOShiftScheduleDetails>();
            
            for (int iCounter = 0; iCounter <= dgvShift.Rows.Count - 1; iCounter++)
            {
                if (Convert.ToBoolean(dgvShift.Rows[iCounter].Cells["IsChecked"].Value) == true)
                {
                    clsDTOShiftScheduleDetails objclsDTOShiftScheduleDetails = new clsDTOShiftScheduleDetails();
                    objclsDTOShiftScheduleDetails.lngEmployeeID = dgvShift.Rows[iCounter].Cells["EmployeeID"].Value.ToInt64();
                    objclsDTOShiftScheduleDetails.intCompanyID = dgvShift.Rows[iCounter].Cells["CompanyID"].Value.ToInt32();
                    MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.IlstclsDTOShiftScheduleDetails.Add(objclsDTOShiftScheduleDetails);
                }
            }
        }

        private void DisplayShiftSheduleAgainstRecordID(int intRetValue)
        {
            RecordCount();
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt).ToString();
            BindingNavigatorPositionItem.Text = MobjclsBLLShiftSchedule.getRecordRowNumber(intRetValue).ToString(); // get record no. against the Deposit ID

            DisplayShiftScheduleDetails();
            BindingEnableDisable();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (FormValidationForDelete())
            {
                if (this.UserMessage.ShowMessage(13) == true)
                {
                    MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.intShiftScheduleID = txtScheduleName.Tag.ToInt32();
                    if (MobjclsBLLShiftSchedule.DeleteShiftScheduleMaster() == true)
                    {
                        lblstatus.Text = this.UserMessage.GetMessageByCode(4); // Deleted Successfully
                        tmrClear.Enabled = true;
                        this.UserMessage.ShowMessage(4);

                        AddNew();
                    }
                }
            }
        }

        private bool FormValidationForDelete()
        {
            bool blnExists = false;

            for (int iCounter = 0; iCounter <= dgvShift.Rows.Count - 1; iCounter++)
            {
                if (Convert.ToBoolean(dgvShift.Rows[iCounter].Cells["IsCheckedDB"].Value) == true) // Checking with database value
                {
                    if (MobjclsBLLShiftSchedule.CheckIsSalaryExistsForDelete(dgvShift.Rows[iCounter].Cells["EmployeeID"].Value.ToInt64(),
                        (dtpFromDate.Tag.ToDateTime().ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime(),
                        (dtpToDate.Tag.ToDateTime().ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime()))
                    {
                        blnExists = true;
                        break;
                    }
                    else
                    {
                        if (MobjclsBLLShiftSchedule.CheckIsShiftScheduleExistsForDelete(txtScheduleName.Tag.ToInt32(),
                            dgvShift.Rows[iCounter].Cells["EmployeeID"].Value.ToInt64(),
                            (dtpFromDate.Tag.ToDateTime().ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime(),
                            (dtpToDate.Tag.ToDateTime().ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime()))
                        {
                            blnExists = true;
                            break;
                        }
                    }
                }
            }

            if (blnExists)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(7111);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(7111);
                return false;
            }

            return true;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (txtScheduleName.Tag.ToInt32() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = txtScheduleName.Tag.ToInt64();
                ObjViewer.PintCompany = ClsCommonSettings.CurrentCompanyID;
                ObjViewer.PiFormID = (int)FormID.ShiftSchedule;
                ObjViewer.ShowDialog();
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtScheduleName.Tag.ToInt32() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Shift Schedule";
                        ObjEmailPopUp.EmailFormType = EmailFormID.ShiftSchedule;
                        ObjEmailPopUp.EmailSource = MobjclsBLLShiftSchedule.GetPrintShiftSchedule(txtScheduleName.Tag.ToInt32());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch { }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "ShiftSchedule";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayShiftScheduleDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();

            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
                BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayShiftScheduleDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();

            if (TotalRecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                else
                    BindingNavigatorPositionItem.Text = "1";
            }

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayShiftScheduleDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                BindingNavigatorCountItem.Text = strBindingOf + "1";
            }

            DisplayShiftScheduleDetails();
            BindingEnableDisable();
        }

        private void DisplayShiftScheduleDetails()
        {
            cboFilterCompany.SelectedIndex = cboFilterDepartment.SelectedIndex = cboFilterDesignation.SelectedIndex = 0;
            MobjclsBLLShiftSchedule.PobjclsDTOShiftSchedule.strSearchKey = txtSearch.Text.Trim();
            DataTable datTemp = MobjclsBLLShiftSchedule.DisplayShiftScheduleMaster(BindingNavigatorPositionItem.Text.ToInt32()); // Get Shift Schedule Details

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    mblnAddStatus = false;
                    btnClear.Enabled = false;

                    txtScheduleName.Tag = datTemp.Rows[0]["ShiftScheduleID"].ToInt32();
                    txtScheduleName.Text = datTemp.Rows[0]["ScheduleName"].ToString();
                    cboShiftPolicy.SelectedValue = datTemp.Rows[0]["ShiftID"].ToInt32();
                    cboShiftPolicy.Tag = cboShiftPolicy.SelectedValue.ToInt32();
                    getShiftTimeDisplay();
                    dtpFromDate.Value = datTemp.Rows[0]["FromDate"].ToDateTime();
                    dtpFromDate.Tag = dtpFromDate.Value.Date;
                    dtpToDate.Value = datTemp.Rows[0]["ToDate"].ToDateTime();
                    dtpToDate.Tag = dtpToDate.Value.Date;

                    datEmployee = MobjclsBLLShiftSchedule.DisplayShiftScheduleDetails(txtScheduleName.Tag.ToInt32(), 0, 0, 0);
                    FillGrid(datEmployee);

                    ChangeStatus();
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false; // disable in update mode. Enable these controls when edit started
                }
            }
        }

        private void ChangeStatus()
        {
            if (mblnAddStatus)
            {
                btnAddNew.Enabled = false;
                btnClear.Enabled = true;
                btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            }
            else
            {
                btnAddNew.Enabled = MblnAddPermission;
                btnClear.Enabled = false;
                btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                btnDelete.Enabled = MblnDeletePermission;
                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
            }
            errValidate.Clear();
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox obj = (ComboBox)sender;
            obj.DroppedDown = false;
        }

        private void ChangeControlStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dgvShift_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void dgvShift_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvShift.Columns["IsChecked"].Index)
                    ChangeControlStatus(sender, e);
            }
        }

        private void FrmShiftSchedule_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "ShiftSchedule";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (MblnAddPermission)
                            btnAddNew_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (MblnDeletePermission)
                            btnDelete_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (MblnPrintEmailPermission)
                            btnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (MblnPrintEmailPermission)
                            btnEmail_Click(sender, new EventArgs());//Email
                        break;
                }
            }
            catch { }
        }

        private void FrmShiftSchedule_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {
                if (this.UserMessage.ShowMessage(8) == true) // Your changes will be lost. Please confirm if you wish to cancel?
                {
                    MobjclsBLLShiftSchedule = null;
                    objUserMessage = null;
                    e.Cancel = false;
                }
                else
                    e.Cancel = true;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.txtSearch.Text.Trim() == string.Empty)
                return;

            RecordCount(); // get record count

            if (TotalRecordCnt == 0) // No records
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(40);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(40);
                txtSearch.Text = string.Empty;
                btnAddNew_Click(sender, e);
            }
            else if (TotalRecordCnt > 0)
                BindingNavigatorMoveFirstItem_Click(sender, e); // if record found in search then automatically navigate to first item
        }

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = "";
            tmrClear.Enabled = false;
        }

        private void cboFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            getFilterEmployee();
        }
    }
}