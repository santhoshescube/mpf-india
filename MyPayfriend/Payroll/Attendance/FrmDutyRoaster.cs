﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;

namespace MyPayfriend
{
    public partial class DutyRoaster : DevComponents.DotNetBar.Office2007Form
    {

        #region Declarations

        bool MblnAddPermission;
        bool MblnUpdatePermission;
        bool MblnDeletePermission;
        bool MblnPrintEmailPermission;
        bool FlagLoop = false;
        int MintSearchIndex = 0;
        private string MstrMessageCaption;              //Message caption
        private MessageBoxIcon MmessageIcon;
        private ArrayList MsarMessageArr;
        string MstrMessCommon = "";


        private clsBLLOffDayMark MobjClsOffDayMark;
        private ClsNotification MobjNotification;
        private ClsLogWriter MobjLog;
        private clsConnection MobjClsConnection;

        int intCurrentPage;
        int intPageCount;
        int intRowCount;
        bool blnNotFromButtonClick =true;
        string strCurrentShift;

        #endregion

        #region Properties

        #endregion

        #region Constructor
        public void New()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            tmrClear.Interval = (ClsCommonSettings.TimerInterval > 0 ? ClsCommonSettings.TimerInterval : 2000);

            // Add any initialization after the InitializeComponent() call.
        }
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.Attendance, this);
        }
        #endregion SetArabicControls
        public DutyRoaster()
        {
            InitializeComponent();
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            MmessageIcon = MessageBoxIcon.Information;
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                //SetArabicControls();
            }
        }
        #endregion

        #region Methods
       
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.DutyRoast, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

                if (MblnAddPermission || MblnUpdatePermission || MblnAddPermission)
                MblnUpdatePermission = true;
            btnDelete.Enabled = MblnDeletePermission;
            btnSave.Enabled = MblnUpdatePermission;
            BtnPrint.Enabled = MblnPrintEmailPermission;


        }
        private void LoadMessage()
        {
            // Loading Message
            MsarMessageArr = new ArrayList();
            try
            {
                MsarMessageArr = MobjNotification.FillMessageArray(121, 9);
            }
            catch (Exception Ex)
            {
                MobjLog.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }
        private void ClearAllControls()
        {
            //Clearing All Controls
            cboEmpSearch.SelectedIndex = -1;
            txtSearch.Text = "";
            cboCompany.SelectedIndex = -1;
            cboCompany.Text = "";
            dtpFromDate.Value = ClsCommonSettings.GetServerDate();
            cboEmpSearch.SelectedIndex = -1;
            txtSearch.Text = "";
            dgvRoster.Rows.Clear();
            dgvRoster.ColumnHeadersVisible = false;
            intPageCount = 0;
            intCurrentPage = 0;
            //setPageNumberText();
        }
      
    
       
        private void InitializeProgressBar(int iRowCount)
        {
            //Initializing the progress bar
            TlSProgressBarAttendance.Visible = true;
            TlSProgressBarAttendance.Value = 0;
            TlSProgressBarAttendance.Minimum = 0;
            if (iRowCount > 0)
            {
                TlSProgressBarAttendance.Maximum = iRowCount + 1;
            }
        }
        private void ResetProgressBar()
        {
            //Reset the progressbar
            TlSProgressBarAttendance.Value = TlSProgressBarAttendance.Maximum;
            TlSProgressBarAttendance.Visible = false;
            TlSProgressBarAttendance.Value = 0;
            TlSProgressBarAttendance.Minimum = 0;
            TlSProgressBarAttendance.Maximum = 0;
        }

      
       
        private bool LoadCombo(int iType)
        {
            //'Loading Combo
            DataTable datCombos;
            if (iType == 0 || iType == 1)
            {
                //'Load Comapny Combo
                datCombos = MobjClsOffDayMark.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;
                datCombos = null;
                cboCompany.SelectedValue = (ClsCommonSettings.CurrentCompanyID > 0 ? ClsCommonSettings.CurrentCompanyID : 0);



                CboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
                CboDepartment.DisplayMember = "Department";
                CboDepartment.ValueMember = "DepartmentID";



                //datCombos = MobjClsOffDayMark.FillCombos(new string[] { "DepartmentID,Department", "DepartmentReference", "" });
                //CboDepartment.ValueMember = "DepartmentID";
                //CboDepartment.DisplayMember = "Department";
                //CboDepartment.DataSource = datCombos;
                //datCombos = null;



            }
        
            return true;
        }
        private string GetStratDate()
        {
            //'Getting Start date
            string sMonth = "";
            switch (dtpFromDate.Value.Month)
            {
                case 1:
                    sMonth = "Jan";
                    break;
                case 2:
                    sMonth = "Feb";
                    break;
                case 3:
                    sMonth = "Mar";
                    break;
                case 4:
                    sMonth = "Apr";
                    break;
                case 5:
                    sMonth = "May";
                    break;
                case 6:
                    sMonth = "Jun";
                    break;
                case 7:
                    sMonth = "Jul";
                    break;
                case 8:
                    sMonth = "Aug";
                    break;
                case 9:
                    sMonth = "Sep";
                    break;
                case 10:
                    sMonth = "Oct";
                    break;
                case 11:
                    sMonth = "Nov";
                    break;
                case 12:
                    sMonth = "Dec";
                    break;
            }
            return Convert.ToDateTime("01-" + sMonth + "-" + dtpFromDate.Value.Year).ToString("dd MMM yyyy");
        }
        private void ResetGrid()
        {
            try
            {
                //'Resetting the Grid
                dgvRoster.Rows.Clear();
                dgvRoster.ColumnHeadersVisible = false;
                if (dgvRoster.Columns["colDgvCompanyID"] != null)
                {
                    int iColIndex = dgvRoster.Columns["colDgvCompanyID"].Index - 1;
                    int iLimit = 0;
                    iLimit = colDgvEmployeeID.Index + 1;
                    for (int iCounter = iColIndex; iCounter >= iLimit; iCounter--)
                    {
                        dgvRoster.Columns.RemoveAt(iCounter);
                        dgvRoster.Columns[iCounter].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                }
            }
            catch
            {
            }
        }
        private bool InitilizeGrid()
        {
            //Initializing the grid with date columns

            try
            {
                dgvRoster.ColumnHeadersVisible = true;
                int iDaycount = 30;
                System.DateTime dpDate = default(System.DateTime);

                dpDate = GetStratDate().ToDateTime();
                //iDaycount =DateDiffDay(dpDate, dpDate.AddMonths(1));
                iDaycount = (dpDate.AddMonths(1) - dpDate).TotalDays.ToInt32();
                int iColIndex = dgvRoster.Columns["colDgvCompanyID"].Index - 1;
                int iLimit = 0;
                iLimit = colDgvEmployeeID.Index + 1;
                for (int iCounter = iColIndex; iCounter >= iLimit; iCounter += -1)
                {
                    dgvRoster.Columns.RemoveAt(iCounter);
                    dgvRoster.Columns[iCounter].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                int iStart = dgvRoster.Columns["colDgvEmployeeID"].Index + 1;
                iLimit = 1;
                for (int z = iStart; z <= iDaycount; z++)
                {
                    int iLoc = z;
                    DataGridViewComboBoxColumn adcolDay = new DataGridViewComboBoxColumn();
                     
                    string strDayID = iLimit.ToString().Trim();
                    strDayID = (strDayID.Length > 1 ? strDayID.Trim() : ("0" + strDayID).Trim());
                    {
                        adcolDay.Name = iLimit.ToString();
                        adcolDay.HeaderText = strDayID;
                        adcolDay.FlatStyle = FlatStyle.Flat;  
                        adcolDay.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox;  
                        adcolDay.SortMode = DataGridViewColumnSortMode.NotSortable;
                        adcolDay.Width = 65;
                        adcolDay.ToolTipText = strDayID + " " + dtpFromDate.Text;
                    }

                    DataTable datCombos;
                    datCombos = MobjClsOffDayMark.FillCombos("select 0 as ShiftID,'None' as ShiftName Union select -1 ShiftID,'Rest' ShiftName Union select ShiftID,ShiftName from PayShiftReference");
                    adcolDay.DisplayMember = "ShiftName";
                    adcolDay.ValueMember = "ShiftID";
                    adcolDay.DataSource = datCombos;
    
                    dgvRoster.Columns.Insert(iLoc, adcolDay);

                    iLimit += 1;
                }

                dgvRoster.Columns["colDgvEmployeeID"].Width = 260;
                dgvRoster.Columns["colDgvEmployeeID"].MinimumWidth = 260;
                dgvRoster.Columns["colDgvEmployeeID"].Resizable = DataGridViewTriState.False;
                dgvRoster.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;

                return true;
            }
            catch (Exception ex)
            {
                MobjLog.WriteLog("Error on " + this.Name + " - Method : " + System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error : " + ex.Message.ToString(), 1);
                return false;
            }
        }
        private bool FormValidations()
        {

            if (cboCompany.SelectedIndex <= -1)
            {
                MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 14, out  MmessageIcon);
                lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                tmrClear.Enabled = true;
                return false;
            }

            return true;
        }
        private void FillParameters()
        {
            MobjClsOffDayMark.clsDTOOffDayMark.intCompanyID = cboCompany.SelectedValue.ToInt32();
            MobjClsOffDayMark.clsDTOOffDayMark.strRosterDate = dtpFromDate.Value.ToString("dd MMM yyyy");
            MobjClsOffDayMark.clsDTOOffDayMark.intDepartmentID = CboDepartment.SelectedValue.ToInt32();  
        }

        private object DispalyOffDayMarkInfo()
        {
            //Display The Employees to grid

            try
            {
                if (FormValidations() == false)
                    return false;


                bool blnRetValue = false;
                InitilizeGrid();

                FillParameters();
                DataTable dtEmployeeOffDayMark = MobjClsOffDayMark.GetOffDayMarkDetails();

                dgvRoster.DataSource = null;


                dgvRoster.Rows.Clear();
                if (dtEmployeeOffDayMark.Rows.Count > 0)
                {
                    InitializeProgressBar(dtEmployeeOffDayMark.Rows.Count);

                    var EmpIDs = (from DataRow r in dtEmployeeOffDayMark.AsEnumerable() orderby r["EmployeeID"] ascending select r.Field<Int32>("EmployeeID")).Distinct();
                    FlagLoop = true;

                    while (FlagLoop)
                    {
                        for (int iCounter = 0; iCounter <= EmpIDs.Count() - 1; iCounter++)
                        {
                            int iEmpID = EmpIDs.ElementAt(iCounter);//Convert.ToInt32(dr["EmployeeID"]);
                            dtEmployeeOffDayMark.DefaultView.RowFilter = "EmployeeID =" + (iEmpID).ToString();
                            if (dtEmployeeOffDayMark.DefaultView.ToTable().Rows.Count > 0)
                            {
                                dgvRoster.RowCount = dgvRoster.RowCount + 1;
                                int iRowIndex = dgvRoster.RowCount - 1;
                                if (FlagLoop == false)
                                {
                                    return false;
                                }
                                dgvRoster.Rows[iRowIndex].Cells["colDgvEmployeeID"].Value = dtEmployeeOffDayMark.DefaultView.ToTable().Rows[0]["Employee"];
                                dgvRoster.Rows[iRowIndex].Cells["colDgvEmployeeID"].Tag = dtEmployeeOffDayMark.DefaultView.ToTable().Rows[0]["EmployeeID"];
                                dgvRoster.Rows[iRowIndex].Cells["colDgvCompanyID"].Value = dtEmployeeOffDayMark.DefaultView.ToTable().Rows[0]["CompanyID"];
                                foreach (DataRow dataRow in dtEmployeeOffDayMark.DefaultView.ToTable().Rows)
                                {
                                    int intDayID = dataRow["DayID"].ToInt32();

                                    if (intDayID > 0)
                                    {
                                        dgvRoster.Rows[iRowIndex].Cells[intDayID.ToString().Trim()].Tag = dtEmployeeOffDayMark.DefaultView.ToTable().Rows[0]["Stat"].ToInt32();
                                        dgvRoster.Rows[iRowIndex].Cells[intDayID.ToString().Trim()].Value = dataRow["StatusDes"].ToInt32(); 
                                        TlSProgressBarAttendance.Value += 1;
                                    }

                                }


                                dgvRoster.CurrentCell = dgvRoster[colDgvEmployeeID.Index, iRowIndex];
                                Application.DoEvents();
                                dgvRoster.Refresh();
                                //--------------------Rows----------------------------------------------------
                            }
                        }
                        FlagLoop = false;
                        TlSProgressBarAttendance.Value = dtEmployeeOffDayMark.Rows.Count + 1;
                    }
                }

                FlagLoop = true;
                InitializeProgressBar(0);
                return blnRetValue;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private bool SaveOffDayMarkInfo()
        {
            //Saving the OffDayMark info

            try
            {
                if (dgvRoster.Rows.Count > 0)
                {
                    dgvRoster.CurrentCell = dgvRoster[colDgvEmployeeID.Index, 0];
                    FillParameters();
                    //MobjClsOffDayMark.DeleteOffDayMarks();
                    InitializeProgressBar(dgvRoster.Rows.Count-1);
                    SaveDetails();
                    return true;
                    //FillParameters(); 

                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }




            Cursor.Current = Cursors.WaitCursor;
            int iProgressMaximum = (dgvRoster.RowCount * DateTime.DaysInMonth(dtpFromDate.Value.Year, dtpFromDate.Value.Month));
            // + 1
            InitializeProgressBar(iProgressMaximum);

            bool blnRetValue = false;
            if (SaveOffDayMarkNew())
            {
                blnRetValue = true;
            }

            ResetProgressBar();
            Cursor.Current = Cursors.Default;
            return blnRetValue;
        }
        private bool SaveOffDayMarkNew()
        {
            GC.Collect();
            bool blnRetValue = false;

            foreach (DataGridViewRow DgvRow in dgvRoster.Rows)
            {
                for (int ICounter = colDgvEmployeeID.Index + 1; ICounter <= colDgvCompanyID.Index - 1; ICounter++)
                {
                    string strDayInfo = DgvRow.Cells[ICounter].Tag.ToStringCustom();
       
                }
                 //MobjClsOffDayMark.SaveManualAttendanceOT(iEmpID, dtpFromDate.Value.ToString("dd-MMM-yyyy"), sOTHr, dOTDays);
            }
            return blnRetValue;
        }
        private bool SaveDetails()
        {
            GC.Collect();
            bool blnRetValue = false;
            foreach (DataGridViewRow DgvRow in dgvRoster.Rows)
            {
                MobjClsOffDayMark.clsDTOOffDayMark.intCompanyID = cboCompany.SelectedValue.ToInt32();
                MobjClsOffDayMark.clsDTOOffDayMark.intEmployeeID = DgvRow.Cells["colDgvEmployeeID"].Tag.ToInt32();  
    
                for (int ICounter = colDgvEmployeeID.Index + 1; ICounter <= colDgvCompanyID.Index - 1; ICounter++)
                {
                    //if (DgvRow.Cells[ICounter].Tag.ToInt32() > 0)
                    //{
                        MobjClsOffDayMark.clsDTOOffDayMark.intRosterStatusID = DgvRow.Cells[DgvRow.Cells[ICounter].ColumnIndex].Value.ToInt32();  
                        MobjClsOffDayMark.clsDTOOffDayMark.intEmployeeID = DgvRow.Cells["colDgvEmployeeID"].Tag.ToInt32();  
                        MobjClsOffDayMark.clsDTOOffDayMark.strRosterDate = dgvRoster.Columns[DgvRow.Cells[ICounter].ColumnIndex].Name.Trim().ToString()  + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year.ToString() ;
                        MobjClsOffDayMark.SaveOffDayMarks();
                    //}
                    blnRetValue = true;
               
                }


                try
                {
                    TlSProgressBarAttendance.Value += 1;
                    StatusStripBottom.Refresh();
                }
                catch
                {
                }

            }
            return blnRetValue;
        }
       
        private bool SearchEmp()
        {
            //Seraching Employee's in Grid
            try
            {
                if ((!string.IsNullOrEmpty(txtSearch.Text)))
                {
                    if ((dgvRoster.Rows.Count > 0))
                    {
                        string strEmpName = "";
                        if ((MintSearchIndex == dgvRoster.RowCount))
                            MintSearchIndex = 0;

                        MintSearchIndex = ((MintSearchIndex == 0) ? 0 : MintSearchIndex);
                        if (MintSearchIndex > 0)
                        {
                            dgvRoster.Rows[MintSearchIndex - 1].Selected = false;
                        }
                        else
                        {
                            dgvRoster.Rows[MintSearchIndex].Selected = false;
                        }


                        for (int i = MintSearchIndex; i <= dgvRoster.Rows.Count - 1; i++)
                        {
                            strEmpName = "";
                            if ((cboEmpSearch.Text == "Employee Name"))
                            {
                                strEmpName = Convert.ToString(dgvRoster.Rows[i].Cells["colDgvEmployeeID"].Value);
                                strEmpName = strEmpName.TrimStart().TrimEnd();
                                if ((strEmpName.ToLower().StartsWith(txtSearch.Text.Trim().ToLower())))
                                {
                                    dgvRoster.CurrentCell = dgvRoster[colDgvEmployeeID.Index, i];
                                    dgvRoster.CurrentCell.Selected = false;
                                    dgvRoster.Rows[i].Selected = true;
                                    MintSearchIndex = i + 1;
                                    return false;
                                }
                            }
                            else if ((cboEmpSearch.Text == "Employee No"))
                            {
                                strEmpName = Convert.ToString(dgvRoster.Rows[i].Cells["colDgvEmployeeID"].Value);
                                strEmpName = strEmpName.Substring(strEmpName.IndexOf("-") + 1);
                                strEmpName = strEmpName.TrimStart().TrimEnd();
                                if ((strEmpName.ToLower().StartsWith(txtSearch.Text.Trim().ToLower())))
                                {
                                    dgvRoster.CurrentCell = dgvRoster[colDgvEmployeeID.Index, i];
                                    dgvRoster.CurrentCell.Selected = false;
                                    dgvRoster.Rows[i].Selected = true;
                                    MintSearchIndex = i + 1;
                                    return false;
                                }
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MobjLog.WriteLog("Error on " + this.Name + " - Method : " + System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error : " + ex.Message.ToString(), 1);
            }
            return true;
        }

        private void FillGrid()
        {
            this.btnShow.Enabled = false;
            FlagLoop = false;
            if (DispalyOffDayMarkInfo().ToBoolean())
            {
                ResetProgressBar();
            }
            FlagLoop = false;
            this.btnShow.Enabled = true;
        }
        #endregion

        #region Control Events
        private void btnShow_Click(object sender, EventArgs e)
        {
            blnNotFromButtonClick = false;
            intPageCount = 0;
            intCurrentPage = 1;
            intRowCount = 25;
            FillGrid();
            blnNotFromButtonClick = true;
        }

        private void FrmOffDayMark_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                FlagLoop = false;

            }
            catch (Exception ex)
            {
            }
        }
        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }
        private void FrmOffDayMark_Load(object sender, EventArgs e)
        {
            try
            {
                MobjClsOffDayMark = new clsBLLOffDayMark();
                MobjNotification = new ClsNotification();
                MobjLog = new ClsLogWriter(Application.StartupPath);
                MobjClsConnection = new clsConnection();
                SetPermissions();

                LoadMessage();
                LoadCombo(1);
                dtpFromDate.Value = ("01/" + GetCurrentMonth(dtpFromDate.Value.Month) + "/" + dtpFromDate.Value.Year.ToString() + "").ToDateTime();
            }
            catch
            { }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombo(2);
            LoadCombo(3);
            ResetGrid();
        }
      
        private void dgvOffDayMark_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1 && (e.ColumnIndex > colDgvEmployeeID.Index && e.ColumnIndex < colDgvCompanyID.Index))
                {

                    MobjClsOffDayMark.clsDTOOffDayMark.intEmployeeID = dgvRoster.Rows[e.RowIndex].Cells["colDgvEmployeeID"].Tag.ToInt32();
                    MobjClsOffDayMark.clsDTOOffDayMark.strRosterDate = dgvRoster.Columns[e.ColumnIndex].Name.Trim().ToString() + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year.ToString();
                    if (MobjClsOffDayMark.IsAttendance())
                    {
                        //e.Cancel = true;
                        //dgvRoster.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                        //return; 

                        e.Cancel = true; 
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 20015, out  MmessageIcon);
                        lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        tmrClear.Enabled = true;


                    }

                }
            }
            catch
            {
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveOffDayMarkInfo())
            {
                MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                tmrClear.Enabled = true;
                //ClearAllControls();
                lblStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1);
            }
            ResetProgressBar();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            if (dgvRoster.Rows.Count > 0)
            {

                MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 13, out  MmessageIcon);
                foreach (DataGridViewRow DgvRow in (dgvRoster.Rows))
                {
                    int iEmpID = DgvRow.Cells["colDgvEmployeeID"].Tag.ToInt32();
                    string strDate = GetStratDate();

                }
                int iProgressMaximum = dgvRoster.RowCount * DateTime.DaysInMonth(dtpFromDate.Value.Year, dtpFromDate.Value.Month);


                if (DeleteValidating())
                {
                    return; 
                }

                if (MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    ResetProgressBar();
                    return;
                }
                FillParameters();
                MobjClsOffDayMark.DeleteOffDayMarks();
                btnShow_Click(sender,e);
                InitializeProgressBar(0);
                ResetProgressBar();
            }
        }

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            tmrClear.Enabled = false;
            lblStatus.Text = "";
        }

        //private void btnAutoFill_Click(object sender, EventArgs e)
        //{
        //    AutoFillAllEmployees();
        //}

        private void cboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            dgvRoster.ClearSelection();
            MintSearchIndex = 0;
        }

        private void cboEmpSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvRoster.ClearSelection();
            MintSearchIndex = 0;
        }

        private void cboEmpSearch_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmpSearch.DroppedDown = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            string strMessage = "No Items Found !";
            dgvRoster.ClearSelection();
            if ((!string.IsNullOrEmpty(txtSearch.Text) && (cboEmpSearch.Text == "Employee Name" || cboEmpSearch.Text == "Employee No")))
            {

                if ((dgvRoster.Rows.Count > 0))
                {
                    if ((SearchEmp() == true))
                    {
                        if ((MintSearchIndex == 0))
                        {
                            MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dgvRoster.Refresh();
                        }
                        MintSearchIndex = 0;
                    }
                }
            }
            else
            {
                dgvRoster.ClearSelection();
                MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            dtpFromDate.Value = GetStratDate().ToDateTime();
            ResetGrid();
        }

        private void dtpFromDate_KeyDown(object sender, KeyEventArgs e)
        {
            dtpFromDate.Value = GetStratDate().ToDateTime();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearAllControls();
            //ClearBottomPanel();
        }
        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }
       
        #endregion

      

        private void dgvOffDayMark_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvRoster.IsCurrentCellDirty)
                {
                    dgvRoster.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }


        }

        private void dgvRoster_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                    if (e.RowIndex > -1 && (e.ColumnIndex > colDgvEmployeeID.Index && e.ColumnIndex < colDgvCompanyID.Index))
                    {
                        if (blnNotFromButtonClick == true)
                        {
                            dgvRoster.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag = 1;
                        }

                        if (dgvRoster.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToBoolean() == true)
                        {
                            dgvRoster.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.RosyBrown;
                        }
                        else
                        {
                            dgvRoster.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.White; 
                        }
                        lblStatus.Text = dgvRoster.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();  
                    }
            }
            catch
            {
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                using (FrmRptOffDayMark ObjViewer = new FrmRptOffDayMark())
                {
                    ObjViewer.strMonthYearDate = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                    ObjViewer.PiDepartmentID = CboDepartment.SelectedValue.ToInt32();
                    ObjViewer.PiCompanyID = cboCompany.SelectedValue.ToInt32();
                    ObjViewer.ShowDialog();
                }
         
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }


        private void CboDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            CboDepartment.DroppedDown = false; 
        }

        private bool DeleteValidating()
        {
            try
            {
                   FillParameters();  
                   DataTable DT=MobjClsOffDayMark.DeleteValid();
                    if(DT.Rows.Count>0)
                    {
                    if (DT.DefaultView.ToTable().Rows[0]["AttendanceID"].ToInt32()>0)
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 20015, out  MmessageIcon);
                        lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        tmrClear.Enabled = true;
                        return true; 
                    }
                    }
                    return false; 
            }
            catch
            {
                return false;
            }
        }

        private void dgvRoster_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void dgvRoster_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1 && (e.ColumnIndex > colDgvEmployeeID.Index && e.ColumnIndex < colDgvCompanyID.Index))
                {
                    lblStatus.Text = dgvRoster.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
                }
            }
            catch
            {
            }
        }

        private void btnShift_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmShiftPolicy objShiftPolicy = new FrmShiftPolicy())
                {
                    objShiftPolicy.ShowDialog();
                }
                //btnShow_Click(sender,e) ;
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
        }

        private void AutoFillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                    int Mrowindex = dgvRoster.CurrentRow.Index;
                    int MColindex = dgvRoster.CurrentCell.ColumnIndex;
                    if (Mrowindex < 0) return;
                    if (dgvRoster.Rows[Mrowindex].Cells[MColindex].Value != DBNull.Value)
                    {
                        for (int i = Mrowindex; i < dgvRoster.Rows.Count; i++)
                        {

                            MobjClsOffDayMark.clsDTOOffDayMark.intEmployeeID = dgvRoster.Rows[i].Cells["colDgvEmployeeID"].Tag.ToInt32();
                            MobjClsOffDayMark.clsDTOOffDayMark.strRosterDate = dgvRoster.Columns[MColindex].Name.Trim().ToString() + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year.ToString();
                            if (MobjClsOffDayMark.IsAttendance() ==false)
                            {
                                if (Mrowindex != i)
                                {
                                    dgvRoster.Rows[i].Cells[MColindex].Value = dgvRoster.Rows[Mrowindex].Cells[MColindex].Value;
                                }

                            }


                        }
                    }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
        }

        private void dgvRoster_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void dgvRoster_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Mrowindex = dgvRoster.CurrentRow.Index;
                int MColindex = dgvRoster.CurrentCell.ColumnIndex;



                if (Mrowindex < 0 || MColindex<1) return;

                if (dgvRoster.CurrentRow.Cells[MColindex].Value != DBNull.Value)
                {

                        if (Mrowindex != -1 && MColindex >= 0)
                        {

                                if (e.Button == MouseButtons.Right)
                                {
                                    this.ContextMenuStripAutoFill.Show(this.dgvRoster, this.dgvRoster.PointToClient(Cursor.Position));
                                }

                        }

                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
        }

        private void dgvRoster_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

        }


    }
}