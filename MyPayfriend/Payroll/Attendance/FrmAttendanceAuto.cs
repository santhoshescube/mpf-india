﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Linq;
using Microsoft.VisualBasic;
using System.Collections;
using System.Threading;
using System.IO;
using System.Data.OleDb;

namespace MyPayfriend
{
    public partial class FrmAttendanceAuto : DevComponents.DotNetBar.Office2007Form
    {

        double MdblMonthlyleave = 0;
        double MdblTakenLeaves = 0;
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission
        bool Glbln24HrFormat = ClsCommonSettings.Glb24HourFormat;
        bool MblDeleteMeassage = false;
        int MintDetailCount = 0;
        private string MstrMessCommon;
        private MessageBoxIcon MmessageIcon;
        private ArrayList MsarMessageArr;                  // Error Message display
        ComboBox CboStatusComboBox;
        int MintTimingsColumnCnt = 0;
        int MintDisplayMode = 2;
        clsBLLAttendance MobjclsBLLAttendance;
        ClsLogWriter MobjClsLogs;
        ClsNotification MobjClsNotification;
        ClsExportDatagridviewToExcel MobjExportToExcel;
        int iProCount;
        public FrmAttendanceAuto()
        {
            InitializeComponent();
            dtpMonthSelect.Value = ClsCommonSettings.GetServerDate();  
            MobjclsBLLAttendance = new clsBLLAttendance();
            MobjClsLogs = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotification();
            MobjExportToExcel = new ClsExportDatagridviewToExcel();
            LoadCombo(0);
            BarRecordCnt.Visible = false;
            LoadMessage();
        }
        private void LoadMessage()
        {
            // Loading Message
            MsarMessageArr = new ArrayList();
            try
            {
                MsarMessageArr = MobjClsNotification.FillMessageArray(121, 2);
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }
        private void SetPermissions()
        {
            // Function for setting permissions

            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.Attendance, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                clsBLLPermissionSettings ObjPermission = new clsBLLPermissionSettings();
                DataTable dt = ObjPermission.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID);
                dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.AttendanceMapping).ToString();

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            if (MblnAddPermission || MblnUpdatePermission || MblnAddUpdatePermission)
                MblnUpdatePermission = true;
            //btnDelete.Enabled = MblnDeletePermission;
            btnSave.Enabled = MblnUpdatePermission;
        }
        private void btnShow_Click(object sender, EventArgs e)
        {
            lblPrgressPercentage.Text = "";
            lblPrgressPercentage.Refresh();
            DgvAttendance.RowCount = 0;
            BarRecordCnt.Visible = true;
            barProgressBarAttendance.Value = 0;
            barProgressBarAttendance.Visible = true;

            iProCount = 1;
            GetAllEmployeeAttendance();
            iProCount = 0;
            BarRecordCnt.Visible = false;
        }

        private void GetAllEmployeeAttendance()
        {
            try
            {

                int EmpID = 0;
                DataTable datLiveData;
                DataTable Employees;
                int IncludeCompany = 0;
                if (chkIncludeCompany.Checked==true)
                {
                    IncludeCompany = 1;
                }
                else
                {
                    IncludeCompany = 0;
                }
                Employees = MobjclsBLLAttendance.FillEmployes(cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), CboCompany.SelectedValue.ToInt32(), CboBranch.SelectedValue.ToInt32(), IncludeCompany);
                if (Employees.Rows.Count > 0 && CboEmployee.SelectedIndex <= 0)
                {
                    barProgressBarAttendance.Minimum = 0;
                    barProgressBarAttendance.Maximum = (Employees.Rows.Count * (dtpMonthSelect.MaxDate.Day.ToInt32())) +1;
                    barProgressBarAttendance.Visible = true;
                    barProgressBarAttendance.Refresh();
                }
                if (CboEmployee.SelectedIndex  > 0)
                {
                    datLiveData = MobjclsBLLAttendance.getAttendanceLive(CboEmployee.SelectedValue.ToInt32(), 1, dtpMonthSelect.Value, 2,cboDepartment.SelectedValue.ToInt32(),cboDesignation.SelectedValue.ToInt32());
                    if (datLiveData.Rows.Count > 0)
                    {
                        barProgressBarAttendance.Minimum = 0;
                        barProgressBarAttendance.Maximum = (1 * (dtpMonthSelect.MaxDate.Day.ToInt32())) + 1;
                        barProgressBarAttendance.Visible = true;
                        barProgressBarAttendance.Refresh();

                        ConvertLiveData(datLiveData, "dd/MM/yyyy", true, 0);
                    }
                }
                else
                {
                    for (int RntCnt = 0; RntCnt <= Employees.Rows.Count - 1; ++RntCnt)
                    {
                        EmpID = Employees.Rows[RntCnt][0].ToInt32();
                        datLiveData = MobjclsBLLAttendance.getAttendanceLive(EmpID, 1, dtpMonthSelect.Value, 2, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32());
                        if (datLiveData.Rows.Count > 0)
                        {
                            ConvertLiveData(datLiveData, "dd/MM/yyyy", true, 0);
                        }
                    }
                }
                if (Employees.Rows.Count > 0)
                {
                    barProgressBarAttendance.Maximum = (Employees.Rows.Count * (dtpMonthSelect.MaxDate.Day.ToInt32())) + 1;
                    barProgressBarAttendance.Refresh();
                }
                for (int iRw = 0; iRw <= DgvAttendance.Rows.Count - 1; iRw++)
                {
                    if (DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Value.ToInt32() != 1)
                    {
                        DataTable datLeaveRemarks = MobjclsBLLAttendance.GetLeaveRemarks(DgvAttendance.Rows[iRw].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[iRw].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value.ToDateTime());
                        if (datLeaveRemarks.Rows.Count > 0)
                        {
                            DgvAttendance.Rows[iRw].Cells["ColDgvRemarks"].Value = datLeaveRemarks.Rows[0]["Remarks"].ToStringCustom();
                            DgvAttendance.Rows[iRw].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Leave;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveType"].Value = datLeaveRemarks.Rows[0]["LeaveTypeID"].ToInt32();
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveInfo"].Value = datLeaveRemarks.Rows[0]["HalfDay"].ToInt32();
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveType"].Tag = datLeaveRemarks.Rows[0]["LeaveTypeID"].ToInt32();
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Tag = 1;
                            DgvAttendance.Rows[iRw].Cells["ColDgvLeaveFlag"].Value = 1;
                            DgvAttendance.Rows[iRw].DefaultCellStyle.ForeColor = Color.Red;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Value = 0;
                            DgvAttendance.Rows[iRw].Cells["ColDgvHolidayFlag"].Tag = 0;
                        }
                    }
                }
                Employees = null;
                datLiveData = null;
            }
            catch
            {
            }
        }
        private bool ConvertLiveData(DataTable dtFetchData, string sdateFormat, bool bdatetime, int iTemplateMasterId)//To read punch data direct from table log
        {
            //Sorting And HANDLING Night Shift
            bool blnRetValue = false;
            try
            {

                DataTable dtData1;
                DataTable dtData2;
                dtData1 = dtFetchData;
                var drID = (from r in dtFetchData.AsEnumerable()
                            select r["EmployeeID"]).Distinct();
                var drDate = (from r in dtFetchData.AsEnumerable()
                              orderby r["AtnDate"] ascending
                              select r["AtnDate"].ToDateTime().ToString("dd MMM yyyy")).Distinct();
                int gCount = 0;
                int iStatus = 0;
                lblPrgressPercentage.Refresh();
                lblProgressShow.Visible = true;
                lblPrgressPercentage.Visible = true;
                lblPrgressPercentage.BringToFront();
                lblProgressShow.BringToFront();
                lblProgressShow.Refresh();
                BarRecordCnt.Refresh();
                dtData2 = dtFetchData;
                Application.DoEvents();
                foreach (Int64 ID in drID)
                {

                    foreach (string dteDate in drDate)
                    {

                        int iEmpId = 0;
                        int iShiftId = 0;
                        int iLate = 0;
                        int iEarly = 0;
                        int ishiftType = 0;
                        int iCmpId = 0;
                        int iBuffer = 0;
                        int iNoOfTimings = 0;
                        string sFromTime = "";
                        string sToTime = "";
                        string sDuration = "";
                        string sAllowedBreakTime = "0";
                        string sEmpName = "";
                        string sShiftName = "";
                        string sMinWorkHours = "0";//
                        int OffDay=0;
                        iStatus = 0;
                        int iShiftMode = 0;
                        if (GetEmployeeInfo(Convert.ToString(ID).Trim(), Convert.ToDateTime(dteDate), ref iEmpId, ref sEmpName, ref iShiftId, ref sShiftName,
                                                            ref sFromTime, ref  sToTime, ref  sDuration, ref ishiftType, ref  sAllowedBreakTime, ref  iLate, ref iEarly, ref iCmpId, iTemplateMasterId,
                                                            ref  iNoOfTimings, ref  sMinWorkHours, ref iBuffer, ref iShiftMode,false))
                        {


                            if (iShiftId == 0)
                            {
                                GetEmployeeInfo(Convert.ToString(ID).Trim(), Convert.ToDateTime(dteDate), ref iEmpId, ref sEmpName, ref iShiftId, ref sShiftName,
                                           ref sFromTime, ref  sToTime, ref  sDuration, ref ishiftType, ref  sAllowedBreakTime, ref  iLate, ref iEarly, ref iCmpId, iTemplateMasterId,
                                           ref  iNoOfTimings, ref  sMinWorkHours, ref iBuffer, ref iShiftMode, true);
                            }


                            int iTempEmpId = 0;
                            int iTempShiftId = 0;
                            int iTempLate = 0;
                            int iTempEarly = 0;
                            int iTempCmpId = 0;
                            int iTempBuffer = 0;
                            int iTempNoOfTimings = 0;
                            string sTempFromTime = "";
                            string sTempToTime = "";
                            string sTempDuration = "";
                            string sTempAllowedBreakTime = "0";
                            string sTempEmpName = "";
                            string sTempShiftName = "";
                            string sTempMinWorkHours = "0";
                            string sFilterExpression = "";
                            string sTempFilterExpression = "";
                            DataRow[] DataRow;
                            DataRow[] TempDataRow;
                            if (iBuffer > 0 && iShiftMode !=2 )
                            {
                                string sTemp1FromTime = dteDate;
                                string sTemp2FromTime = "";

                                if (GetEmployeeInfo(Convert.ToString(ID).Trim(), Convert.ToDateTime(dteDate), ref iTempEmpId, ref sTempEmpName, ref iTempShiftId, ref sTempShiftName, ref sTempFromTime,
                                        ref sTempToTime, ref sTempDuration, ref iTempShiftId, ref sTempAllowedBreakTime, ref iTempLate, ref iTempEarly, ref iTempCmpId, iTemplateMasterId, ref iTempNoOfTimings,
                                        ref sTempMinWorkHours, ref iTempBuffer, ref iShiftMode,false))
                                {
                                    sTemp1FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + " " + sTempFromTime).ToString("dd MMM yyyy HH:mm tt");
                                    sTemp2FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + " " + sTempToTime).ToString("dd MMM yyyy HH:mm tt");

                                }
                                else
                                {
                                    sTemp1FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + " " + sTempFromTime).ToString("dd MMM yyyy HH:mm tt");
                                    sTemp2FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + " " + sFromTime).ToString(" dd MMM yyyy HH:mm tt");

                                }

                                sTemp1FromTime = Convert.ToDateTime(sTemp1FromTime).AddMinutes(-iBuffer).ToString();
                                sTemp2FromTime = Convert.ToDateTime(sTemp2FromTime).AddMinutes(iBuffer).ToString();
                                sFilterExpression = "EmployeeID='" + Convert.ToString(ID).Trim() + "' and AtnDate>='" + sTemp1FromTime + "' and AtnDate<'" + sTemp2FromTime + "'";
                                DataRow = dtFetchData.Select(sFilterExpression);

                            }
                            else if (iShiftMode == 2)
                            {
                                string sTemp1FromTime = dteDate;
                                string sTemp2FromTime = "";
                                //if (iBuffer == 0)
                                //{
                                    iBuffer = 360;
                                //}
                                if (GetEmployeeInfo(Convert.ToString(ID).Trim(), Convert.ToDateTime(dteDate), ref iTempEmpId, ref sTempEmpName, ref iTempShiftId, ref sTempShiftName, ref sTempFromTime,
                                    ref sTempToTime, ref sTempDuration, ref iTempShiftId, ref sTempAllowedBreakTime, ref iTempLate, ref iTempEarly, ref iTempCmpId, iTemplateMasterId, ref iTempNoOfTimings,
                                    ref sTempMinWorkHours, ref iTempBuffer, ref iShiftMode,false))
                                {
                                    sTemp2FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + " " + sTempToTime).ToString("dd MMM yyyy HH:mm tt");
                                    sTemp1FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + " " + sTempFromTime).ToString("dd MMM yyyy HH:mm tt");

                                }
                                else
                                {
                                    sTemp2FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + " " + sToTime).ToString(" dd MMM yyyy HH:mm tt");
                                    sTemp1FromTime = Convert.ToDateTime(Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + " " + sFromTime).ToString(" dd MMM yyyy HH:mm tt");

                                }
                                sTemp1FromTime = Convert.ToDateTime(sTemp1FromTime).AddMinutes(-iBuffer).ToString();
                                sTemp2FromTime = Convert.ToDateTime(sTemp2FromTime).AddMinutes(iBuffer).ToString();
                                sFilterExpression = "EmployeeID='" + Convert.ToString(ID).Trim() + "' and AtnDate>='" + sTemp1FromTime + "' and AtnDate<'" + sTemp2FromTime + "'";
                                dtFetchData.DefaultView.Sort = "AtnDate";
                                dtFetchData.DefaultView.RowFilter = sFilterExpression;
                                DataRow = dtFetchData.DefaultView.Table.Select(sFilterExpression);

                            }
                            else
                            {
                                sFilterExpression = "EmployeeID='" + ID + "' and AtnDate >='" + Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + "' and AtnDate <='" + Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + "'";
                                sTempFilterExpression = "EmployeeID='" + ID + "' and AtnDate >='" + Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + "' and AtnDate <='" + Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + "' and isnull(StatCnt,0)=0";
                                DataRow = dtData1.DefaultView.Table.Select(sFilterExpression);
                                TempDataRow = dtData2.DefaultView.Table.Select(sTempFilterExpression);
                                if (DataRow.Count() > 0)
                                {
                                    if (DataRow.Count() != TempDataRow.Count())
                                    {
                                        sFilterExpression = "EmployeeID='" + ID + "' and AtnDate >='" + Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + "' and AtnDate <='" + Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + "' and isnull(StatCnt,0)=1";
                                    }
                                    else
                                    {
                                        sFilterExpression = "EmployeeID='" + ID + "' and AtnDate >='" + Convert.ToDateTime(dteDate).ToString("dd MMM yyyy") + "' and AtnDate <='" + Convert.ToDateTime(dteDate).AddDays(1).ToString("dd MMM yyyy") + "'";
                                    }
                                        dtFetchData.DefaultView.Sort = "AtnDate";
                                        dtFetchData.DefaultView.RowFilter = sFilterExpression;
                                        DataRow = dtFetchData.DefaultView.Table.Select(sFilterExpression);
                                }
                                
                            }
                            dtData1.Dispose();
                            dtData2.Dispose();




                            if (DataRow.Count() > 0 && iShiftId>0)
                            {
                                iStatus = DataRow[0]["AtnStatus"].ToInt32();
                                lblPrgressPercentage.Text = iProCount.ToString() + " Records processed";
                                barProgressBarAttendance.Value += 1;
                                //barProgressBarAttendance.Visible = true;
                                barProgressBarAttendance.Refresh(); 
                                iProCount = iProCount + 1;
                                DgvAttendance.RowCount += 1;
                                gCount = DgvAttendance.RowCount - 1;


                                //int iType = 0;
                                //if (CheckIfValidDate(DgvAttendance.CurrentCell.RowIndex.ToInt32(), ref iType) == false)
                                //{
                                //    if (iType == 3 || iType == 2 || iType == 4)// 'off day and off day without shift and Holiday
                                //    {
                                //        //DgvAttendance.Rows[gCount].DefaultCellStyle.ForeColor = Color.Red;
                                //        //DgvAttendance.Rows[gCount].Cells["ColDgvHolidayFlag"].Value = 1;
                                //        //DgvAttendance.Rows[gCount].Cells["ColDgvHolidayFlag"].Tag = 1;
                                //        //DgvAttendance.Rows[gCount].Cells["ColDgvLeaveFlag"].Tag = 0;
                                //        //DgvAttendance.Rows[gCount].Cells["ColDgvLeaveFlag"].Value = 0;
                                //        //DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value = -1;
                                //        OffDay = 1;
                                //    }
                                //    else if (iType == 1)//Then 'leave
                                //    {
                                //        DgvAttendance.Rows[gCount].DefaultCellStyle.ForeColor = Color.Red;
                                //        DgvAttendance.Rows[gCount].Cells["ColDgvLeaveFlag"].Tag = 1;
                                //        DgvAttendance.Rows[gCount].Cells["ColDgvLeaveFlag"].Value = 1;
                                //        DgvAttendance.Rows[gCount].Cells["ColDgvHolidayFlag"].Value = 0;
                                //        DgvAttendance.Rows[gCount].Cells["ColDgvHolidayFlag"].Tag = 0;
                                //    }

                                //}



                                if (OffDay != 1)
                                {
                                    if (iStatus == 1)
                                    {
                                        DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value = Convert.ToInt32(AttendanceStatus.Present);
                                    }
                                    else if (iStatus == 2)
                                    {
                                        DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value = Convert.ToInt32(AttendanceStatus.Rest);
                                    }
                                    else
                                    {
                                        DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value = Convert.ToInt32(AttendanceStatus.Absent);
                                    }
                                }
                                DgvAttendance.Rows[gCount].Cells["ColDgvAtnDate"].Value = dteDate.ToString();
                                DgvAttendance.Rows[gCount].Cells["ColDgvDay"].Value = Convert.ToDateTime(dteDate).DayOfWeek;
                                ColDgvAtnDate.Visible = true;
                                ColDgvEmployee.Visible = true;
                                DgvAttendance.Rows[gCount].Cells["ColDgvEmployee"].Tag = iEmpId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvEmployee"].Value = sEmpName;
                                DgvAttendance.Rows[gCount].Cells["ColDgvCompanyID"].Value = iCmpId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvShift"].Tag = iShiftId;
                                DgvAttendance.Rows[gCount].Cells["ColDgvShift"].Value = sShiftName;
                                DgvAttendance.Rows[gCount].Cells["ColDgvPunchID"].Value = ID.ToString();//Workid Saved for deleting handpunch logs
                                if (OffDay != 1)
                                {
                                    if (Glbln24HrFormat == false)
                                    {
                                        sFromTime = string.Format("{0:hh:mm tt}", Convert.ToDateTime(sFromTime));
                                        sToTime = string.Format("{0:hh:mm tt}", Convert.ToDateTime(sToTime));
                                        DgvAttendance.Rows[gCount].Cells["ColDgvShiftTimeDisplay"].Value = Convert.ToString(sFromTime + " To " + sToTime + "(Duration:" + sDuration + " )");
                                    }
                                    else
                                    {
                                        DgvAttendance.Rows[gCount].Cells["ColDgvShiftTimeDisplay"].Value = Convert.ToString(sFromTime + " To " + sToTime + "(Duration:" + sDuration + " )");
                                    }
                                }
                                else
                                {
                                    DgvAttendance.Rows[gCount].DefaultCellStyle.ForeColor = Color.Red;
                                    DgvAttendance.Rows[gCount].Cells["ColDgvValidFlag"].Value = 1;//if Zero then Invalid Data

                                }
                                sAllowedBreakTime = Convert.ToString(sAllowedBreakTime == "" ? 0 : Convert.ToDouble(sAllowedBreakTime));

                                DgvAttendance.Rows[gCount].Cells["ColDgvAllowedBreakTime"].Tag = sAllowedBreakTime == "" ? 0 : Convert.ToDouble(sAllowedBreakTime);

                                sAllowedBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today));
                                sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                                DgvAttendance.Rows[gCount].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;

                                int intStart = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;
                                //if (DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value.ToInt32() != 4 && DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value.ToInt32() != 0)
                                //{
                                    for (int i = intStart; i <= DataRow.Count() + 1; i += 2)
                                    {
                                        int intLoc = i;
                                        int intCnt = i / 2;
                                        DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                                        adcolIn.Name = "TimeIn" + intCnt.ToString();
                                        adcolIn.HeaderText = "TimeIn" + intCnt.ToString();
                                        adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                                        adcolIn.Resizable = DataGridViewTriState.False;
                                        adcolIn.Width = 70;
                                        adcolIn.MaxInputLength = 9;

                                        DataGridViewTextBoxColumn adcolout = new DataGridViewTextBoxColumn();
                                        adcolout.Name = "TimeOut" + intCnt.ToString();
                                        adcolout.HeaderText = "TimeOut" + intCnt.ToString();
                                        adcolout.SortMode = DataGridViewColumnSortMode.NotSortable;
                                        adcolout.Resizable = DataGridViewTriState.False;
                                        adcolout.Width = 70;
                                        adcolout.MaxInputLength = 9;

                                        DgvAttendance.Columns.Insert(intLoc + 1, adcolIn);
                                        DgvAttendance.Columns.Insert(intLoc + 2, adcolout);
                                        MintTimingsColumnCnt += 2;

                                    }
                                    if (DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value.ToInt32() != 2 && DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value.ToInt32() != 4 && DgvAttendance.Rows[gCount].Cells["ColDgvStatus"].Value.ToInt32() != 0)
                                    {
                                         WorkTimeCalculationLive(DataRow, sFromTime, sdateFormat, bdatetime, iLate, sToTime, gCount, ishiftType, iNoOfTimings, sMinWorkHours, sDuration, iEarly);
                                    }
                                DgvAttendance.Refresh();
                                blnRetValue = true;
                            }//data row
                        }//get employeeinfo



                    }//for date

                    
                    //iProCount += 1;
                    lblPrgressPercentage.Refresh();
         
                    //barProgressBarAttendance.Visible = true;
                    barProgressBarAttendance.Refresh();

                }//for id 

          
              

                lblProgressShow.Visible = false;
                //lblPrgressPercentage.Visible = false;
                //barProgressBarAttendance.Visible = false;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

                MobjClsLogs.WriteLog("ConvertFetchToValidData--" + Ex.ToString(), 2);
                blnRetValue = false;
            }
            return blnRetValue;
        }
        private bool GetEmployeeInfo(string sEmpNo, DateTime atnDate, ref  int iEmpId, ref string sEmpName, ref int iShiftId, ref string sShiftName, ref string sFromtime,
                                        ref string sTotime, ref string sDuration, ref int ishiftType, ref string sAllowedBreakTime, ref int iLate, ref int iEarly, ref int iCmpId, int iTemplateId,
                                          ref int iNoOfTimings, ref string sMinWorkHours, ref int iBuffer, ref int OffDay, bool isLiveAtn)
        {


            //Get employee shift related information
            DataTable dtEmployeeInfo; int EmptyFlg = 0;
            iEmpId = 0;
            sEmpName = "";
            iShiftId = 0;
            sShiftName = "";
            sFromtime = "";
            sTotime = "";
            sDuration = "";
            ishiftType = 0;
            sAllowedBreakTime = "";
            iLate = 0;
            iEarly = 0;
            iBuffer = 0;
            iCmpId = 0;
            iNoOfTimings = 0;
            sMinWorkHours = "0";
            int intDayID = 0;
            intDayID = Convert.ToInt32(atnDate.DayOfWeek) + 1;
            string sFromDate = atnDate.ToString("dd MMM yyyy");
            string sToDate = atnDate.ToString("dd MMM yyyy");

            try
            {

                dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeInfo(1, sEmpNo, iTemplateId, intDayID, sFromDate, sToDate, iCmpId, iEmpId);

                if (dtEmployeeInfo.Rows.Count > 0)
                {
                    iEmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EmployeeID"]);
                    iCmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["CompanyID"]);
                    iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                    iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                    iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                    ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                    iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                    sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                    sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                    sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                    sEmpName = Convert.ToString(dtEmployeeInfo.Rows[0]["Employee"]);
                    sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                    sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                    sAllowedBreakTime = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                    iBuffer = Convert.ToInt32(dtEmployeeInfo.Rows[0]["Buffer"]);

                    OffDay = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NightShift"]);
                    if (dtEmployeeInfo.Rows[0]["TimeDiff"].ToInt32() < 0)
                    {
                        OffDay = 2;
                    }


                }

                if (iEmpId == 0)
                {
                    dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeInfo(2, sEmpNo, iTemplateId, intDayID, sFromDate, sToDate, iCmpId, iEmpId);
                    if (dtEmployeeInfo.Rows.Count > 0)
                    {
                        iEmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EmployeeID"]);
                        iCmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["CompanyID"]);
                        iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftId"]);
                        iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                        iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                        ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                        iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                        sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                        sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                        sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                        sEmpName = Convert.ToString(dtEmployeeInfo.Rows[0]["Employee"]);
                        sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                        sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                        sAllowedBreakTime = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                        iBuffer = Convert.ToInt32(dtEmployeeInfo.Rows[0]["Buffer"]);
                        OffDay = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NightShift"]);
                        if (dtEmployeeInfo.Rows[0]["TimeDiff"].ToInt32() < 0)
                        {
                            OffDay = 2;
                        }


                    }
                }
                
                if (iEmpId >= 0)
                {
                    if (iEmpId == 0)
                    {
                        iEmpId = sEmpNo.ToInt32();
                        EmptyFlg = 1;
                    }

                    if (isLiveAtn)
                        dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeInfo(25, sEmpNo, iTemplateId, intDayID, sFromDate, sToDate, iCmpId, iEmpId);
                    else
                        dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeInfo(3, sEmpNo, iTemplateId, intDayID, sFromDate, sToDate, iCmpId, iEmpId);
                    if (dtEmployeeInfo.Rows.Count > 0)
                    {
                        ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                        iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                        sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                        iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                        sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                        sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                        sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                        sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                        iBuffer = Convert.ToInt32(dtEmployeeInfo.Rows[0]["Buffer"]);
                        OffDay = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NightShift"]);
                        iEmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EmployeeID"]);
                        iCmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["CompanyID"]);
                        iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                        iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                        sEmpName = Convert.ToString(dtEmployeeInfo.Rows[0]["Employee"]);
                        sAllowedBreakTime = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);

                        if (dtEmployeeInfo.Rows[0]["TimeDiff"].ToInt32() < 0)
                        {
                            OffDay = 2;
                        }

                    }
                    else if (dtEmployeeInfo.Rows.Count <= 0 && EmptyFlg==1)
                    {
                        iEmpId = 0;
                    }

                    return true;
                }


                if (iEmpId == 0)
                {

                    OffDay = 1;
                    iTemplateId = 99999;
                    dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeInfo(1, sEmpNo, iTemplateId, intDayID, sFromDate, sToDate, iCmpId, sEmpNo.ToInt32());

                    if (dtEmployeeInfo.Rows.Count > 0)
                    {
                        iEmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EmployeeID"]);
                        iCmpId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["CompanyID"]);
                        iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                        //iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                        //iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                        ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                        iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                        //sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                        sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                        sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                        sEmpName = Convert.ToString(dtEmployeeInfo.Rows[0]["Employee"]);
                        sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                        sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                        sAllowedBreakTime = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                        iBuffer = Convert.ToInt32(dtEmployeeInfo.Rows[0]["Buffer"]);
                        return true;
                    }
                    return true;
                }


                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("GetEmployeeInfo--" + Ex.ToString());
                return false;
            }
        }

        private int GetSplitShiftTotalDuration(int ishiftID)
        {
            //'in split Shift Total Duration is Sum of  details shift duration
            int intTotalSplitShiftDurInMinutes = 0;
            try
            {

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(ishiftID);
                if (DtShiftDetails.Rows.Count > 0)
                {
                    for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                    {
                        string strDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                        int intDurInMin = GetDurationInMinutes(strDuration);
                        intTotalSplitShiftDurInMinutes += intDurInMin;
                    }
                }
                return intTotalSplitShiftDurInMinutes;
            }
            catch (Exception ex)
            {
                MobjClsLogs.WriteLog("Error on GetSplitShiftTotalDuration  :" + this.Name + " --" + ex.Message.ToString(), 1);
                return 0;
            }
        }
        private int GetDayOfWeek(DateTime Day)
        {
            switch (Day.DayOfWeek)
            {

                case DayOfWeek.Sunday:
                    return 1;
                case DayOfWeek.Monday:
                    return 2;
                case DayOfWeek.Tuesday:
                    return 3;
                case DayOfWeek.Wednesday:
                    return 4;
                case DayOfWeek.Thursday:
                    return 5;
                case DayOfWeek.Friday:
                    return 6;
                case DayOfWeek.Saturday:
                    return 7;
            }
            return 0;
        }
        private bool WorkTimeCalculationLive(DataRow[] datarow, string sFromTime, string sdateFormat, bool bdatetime,
                                int iLate, string sToTime, int intRowCount, int iShiftType, int iNoOfTimings, string sMinWorkHours
                               , string sDuration, int iEarly)
        {
            try
            {
                string sWorkTime = "0";
                string sBreakTime = "0";
                string sOTBreakTime = "0";
                string sOt = "0";
                string sPreviousTime = "";
                bool bShiftChangeTime = true;
                bool bShiftChangeTimeBrkOT = true;
                bool bShiftChangeTimeEarlyWRk = true; //'Special condition
                bool bShiftChangeTimeEarlyBRK = true;
                int intDurationInMinutes = 0;
                int intMinWorkHrsInMinutes = 0;
                int intAdditionalMinutes = 0;
                int intAbsentTime = 0;
                int iShiftOrderNo = 0;
                bool blnIsMinWrkHRsAsOT = false;
                bool blnNightShiftFlag = false;
                DateTime atnDate = Convert.ToDateTime(DgvAttendance.Rows[intRowCount].Cells["ColDgvAtnDate"].Value);
                int intDayID = Convert.ToInt32(atnDate.DayOfWeek) + 1;
                if (datarow.Length % 2 == 1)
                {
                    DgvAttendance.Rows[intRowCount].DefaultCellStyle.ForeColor = Color.IndianRed;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvValidFlag"].Value = 0;//if Zero then Invalid Data
                }
                else
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvValidFlag"].Value = 1;//if One then ValidData

                }

                //Leave Extension Additional Minutes
                bool bEarly = true;
                bool bLate = true;
                //if (MblnPayExists)//pay friend exists
                //{
                intAdditionalMinutes = MobjclsBLLAttendance.GetLeaveExAddMinutes(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvEmployee"].Tag), Convert.ToDateTime((DgvAttendance.Rows[intRowCount].Cells["ColDgvAtnDate"].Value)));
                //}

                DgvAttendance.Rows[intRowCount].Cells["ColDgvShiftOrderNo"].Value = 0;//Dynamic Shift

                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Value = "00:00:00";
                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Tag = 0;

                //lblLateComing.Visible = false;
                DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 0;
                //lblEarlyGoing.Visible = false;

                //--------------------------------------------------
                if (iShiftType == 3) //Then 'For dynamic shift
                {
                    string sFirstTime = datarow[0]["AtnDate"].ToString();
                    sFirstTime = Convert.ToDateTime(sFirstTime).ToString("HH:mm");
                    iShiftOrderNo = GetShiftOrderNo(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), sFirstTime, ref sFromTime, ref sToTime, ref sDuration);
                    string sAllowedBreakTime = "0";
                    MobjclsBLLAttendance.GetDynamicShiftDuration(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), iShiftOrderNo, ref sMinWorkHours, ref sAllowedBreakTime);
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToInt32(sAllowedBreakTime);
                    sAllowedBreakTime = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAllowedBreakTime), DateTime.Today).ToString();
                    sAllowedBreakTime = Convert.ToDateTime(sAllowedBreakTime).ToString("HH:mm:ss");
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Value = sAllowedBreakTime;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvShiftOrderNo"].Value = iShiftOrderNo;
                }

                if (intAdditionalMinutes > 0)
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + Convert.ToInt32(intAdditionalMinutes);

                    bEarly = false;
                    bLate = false;
                }

                intDurationInMinutes = GetDurationInMinutes(sDuration);//Getting Duration In mintes

                int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(intDurationInMinutes), Convert.ToDateTime(sToTime));
                if (tmDurationDiff < 0) //For Night Shift
                {
                    blnNightShiftFlag = false;
                    sFromTime = (Convert.ToDateTime(sFromTime)).ToString();
                    sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                }

                blnIsMinWrkHRsAsOT = MobjclsBLLAttendance.isAfterMinWrkHrsAsOT(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag));
                intMinWorkHrsInMinutes = GetDurationInMinutes(sMinWorkHours);//Setting Min Work hrs


                int iBufferForOT = 0;
                int iMinOtMinutes = 0;
                bool blnConsiderBufferForOT = false;
                blnConsiderBufferForOT = MobjclsBLLAttendance.IsConsiderBufferTimeForOT(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), ref iBufferForOT, ref iMinOtMinutes);

                //--------------------------------Calculation Starting------------------
                if (iShiftType <= 3)
                {

                    for (int j = 0; j <= datarow.Length - 1; j++)
                    {
                        string sTimeValue = datarow[j]["AtnDate"].ToString();
                        sTimeValue = Convert.ToDateTime(sTimeValue).ToString("HH:mm");

                        if (iShiftType == 3)//'Modified for Dynamic Shift
                        {
                            if (blnNightShiftFlag == true)
                            {
                                if (Convert.ToDateTime(sTimeValue) >= Convert.ToDateTime("12:00 AM") &&
                                    Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime("12:00 PM"))
                                { sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt"); }
                            }
                        }


                        if (j == 0) //first time' setting LateComing 
                        {
                            //--------------------------------- Attendnace Check first Punch in  next day -----
                            string sFrm = Convert.ToDateTime(sFromTime).ToString("HH:mm tt");
                            string sto = Convert.ToDateTime(sToTime).ToString("HH:mm tt");
                            if (blnNightShiftFlag && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sto))
                            {
                                if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm) && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm).AddMinutes(-120))
                                    sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt");
                            }
                            //---

                            //'---------------------------------Late Coming
                            if ((Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate)) && (iShiftType != 2) && (bLate == true))
                            {

                                int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                if (il > 0)
                                {
                                    string sLate = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today));
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                }

                                //lblLateComing.Tag = 1;
                                //lblLateComing.Visible = true;
                                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes
                            }
                            else
                            {
                                //lblLateComing.Tag = 0;
                                //lblLateComing.Visible = false;
                                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Value = "00:00:00"; //'if 1 then LateComing=yes
                                DgvAttendance.Rows[intRowCount].Cells["ColDgvLateComing"].Tag = 0;
                            }


                        }
                        else //Work and Brek Time Calculation
                        {

                            int intTmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));

                            string sPrevCurrent = intTmDiff.ToString();

                            if (intTmDiff < 0)
                            {
                                intTmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                            }



                            if (j % 2 == 1)// 'if mode 1 then wrk time else Break time
                            {
                                //'------------------------------------------------Early going Case Checking

                                if ((Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly)) && (iShiftType != 2) && (bEarly = true))
                                {
                                    int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                    if (iE > 0)
                                    {
                                        string sEarly = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today));
                                        DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                    }
                                    //lblEarlyGoing.Tag = 1;

                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes
                                }


                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                {


                                    if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                    {
                                        bShiftChangeTime = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                    }
                                    else
                                    {
                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(intTmDiff)); //'WorkTime Calculation 
                                    }

                                }
                                else
                                {
                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime))
                                    {
                                        bflag = true;
                                    } //'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                    if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime))
                                    {
                                        bShiftChangeTimeEarlyWRk = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sTimeValue));
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(intTmDiff));
                                    }
                                }

                            }
                            else //Break time Calculation
                            {
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                {
                                    //lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00"; //'Earlygoing No
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 0;

                                    if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                    {
                                        bShiftChangeTimeBrkOT = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));// 'Break Time Calculation
                                        sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                            sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(intTmDiff));// 'Break Time Calculation
                                    }
                                }
                                else
                                {
                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bflag = true;
                                    }
                                    if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime).AddMinutes(iLate) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bShiftChangeTimeEarlyBRK = false;
                                        int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(intTmDiff));
                                    }


                                    //lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00"; //'Earlygoing No
                                    DgvAttendance.Rows[intRowCount].Cells["ColDgvEarlyGoing"].Tag = 0;
                                    //'-------------------------------------Early Going 

                                }
                            }
                        }
                        sPreviousTime = sTimeValue;
                        if (Glbln24HrFormat == false)
                        {
                            sTimeValue = Convert.ToDateTime(sTimeValue).ToString("hh:mm tt");
                            DgvAttendance.Rows[intRowCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("hh:mm tt");
                        }
                        else
                        {
                            DgvAttendance.Rows[intRowCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("HH:mm");
                        }
                    }//for
                }
                else if (iShiftType == 4)
                {
                    sWorkTime = "0";
                    sBreakTime = "0";
                    sOt = "0";
                    int intTotalSplitAbsentTm = 0;
                    int intTotalAbsentTm = 0;


                    intDurationInMinutes = GetSplitShiftTotalDuration(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag));
                    WorktimeCalculationForSplitShift(datarow, Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvShift"].Tag), intRowCount, sdateFormat, bdatetime,
                                                    sFromTime, sToTime, iLate, iEarly, bLate, bEarly, "", blnIsMinWrkHRsAsOT, blnConsiderBufferForOT, iBufferForOT, ref sWorkTime, ref sBreakTime, ref sOt, ref intTotalSplitAbsentTm, ref intTotalAbsentTm);

                    if (intDurationInMinutes > intMinWorkHrsInMinutes)  //'Split shift Absent           
                    {
                        if (intAdditionalMinutes > 0)
                            intAbsentTime = intMinWorkHrsInMinutes - (Convert.ToInt32(sWorkTime) + intAdditionalMinutes);
                        else
                            intAbsentTime = intMinWorkHrsInMinutes - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        if (intTotalSplitAbsentTm > (Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate))
                        {
                            intAbsentTime = intTotalAbsentTm + (intTotalSplitAbsentTm - (Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate));
                        }
                        else
                        {
                            intAbsentTime = intTotalAbsentTm;
                        }
                    }

                }
                if (blnIsMinWrkHRsAsOT == true)
                {
                    if (Convert.ToDouble(sWorkTime) == Convert.ToDouble(intMinWorkHrsInMinutes))
                    { }//'
                    else if (Convert.ToDouble(sWorkTime) < Convert.ToDouble(intMinWorkHrsInMinutes))
                    { //'sOt = "0"
                        if (intDurationInMinutes > intMinWorkHrsInMinutes)
                        { sOt = "0"; }
                        else// 'duration and minWorkhours are same then Consider Convert.ToInt32(sAllowedBreak) + iEarly + iLate)
                        {
                            if (Convert.ToInt32(sWorkTime) + Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate < intMinWorkHrsInMinutes)
                            { sOt = "0"; }
                        }
                    }
                    else if (Convert.ToDouble(sWorkTime) > Convert.ToDouble(intMinWorkHrsInMinutes))
                    {
                        int Ot = Convert.ToInt32(sWorkTime) - Convert.ToInt32(intMinWorkHrsInMinutes);
                        sOt = Convert.ToString(Convert.ToInt32(sOt) + Ot);
                        sWorkTime = intMinWorkHrsInMinutes.ToString();
                    }
                }

                if (Convert.ToInt32(sOt) < iMinOtMinutes)
                    sOt = "0";
                if (sOt != "" && sOt != "0")//Ot Template
                    sOt = GetOt(sOt);

                bool blnIsHoliday = false;
                blnIsHoliday = MobjclsBLLAttendance.IsHoliday(Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvCompanyID"].Value), atnDate, intDayID); //'cheking current date is holiday
                if (iShiftType <= 3)
                {
                    if (intAdditionalMinutes > 0)
                        intMinWorkHrsInMinutes = intMinWorkHrsInMinutes - intAdditionalMinutes;
                    if (intDurationInMinutes > intMinWorkHrsInMinutes)//Absent Time Calculation
                    {
                        intAbsentTime = intMinWorkHrsInMinutes - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        intAbsentTime = intDurationInMinutes - (Convert.ToInt32(sWorkTime) + Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag) + iEarly + iLate);
                    }
                }

                int iShortage = 0;
                if (iShiftType == 2 || iShiftType == 4)
                { }
                else
                {
                    if (blnIsHoliday == false)
                    {
                        iShortage = Convert.ToInt32(sBreakTime == "0" ? "0" : sBreakTime) - Convert.ToInt32(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Tag);
                        if (iShortage > intAbsentTime)
                        {
                            intAbsentTime = iShortage;
                        }
                    }
                }

                DgvAttendance.Rows[intRowCount].Cells["ColDgvAbsentTime"].Value = intAbsentTime > 0 ? intAbsentTime : 0;
                int iAbsent = intAbsentTime > 0 ? intAbsentTime : 0;
                //lblShortageTime.ForeColor = iAbsent > 0 ? Color.DarkRed : System.Drawing.SystemColors.ControlText;
                string sShortage = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iAbsent), DateTime.Today));
                //lblShortageTime.Text = Convert.ToDateTime(sShortage).ToString("HH:mm:ss");

                //---------------------------------------------------------------------

                //lblAllowedBreakTime.Text = Convert.ToString(DgvAttendance.Rows[intRowCount].Cells["ColDgvAllowedBreakTime"].Value);

                DgvAttendance.Rows[intRowCount].Cells["ColDgvWorkTime"].Tag = Convert.ToDouble(sWorkTime);
                sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                DgvAttendance.Rows[intRowCount].Cells["ColDgvWorkTime"].Value = sWorkTime;
                //lblWorkTime.Text = sWorkTime;

                if (sBreakTime != "" && sBreakTime != "0")
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Tag = sBreakTime;
                    sBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sBreakTime), DateTime.Today));
                    sBreakTime = Convert.ToDateTime(sBreakTime).ToString("HH:mm:ss");
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Value = sBreakTime;
                    //lblBreakTime.Text = sBreakTime;
                }
                else
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Tag = 0;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvBreakTime"].Value = "";
                    //lblBreakTime.Text = "00:00:00";
                }

                if (Convert.ToString(DgvAttendance.Rows[intRowCount].Cells["ColDgvWorkTime"].Value) != "" && Convert.ToDouble(sOt) > 0)
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Tag = sOt;
                    sOt = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sOt), DateTime.Today));
                    sOt = Convert.ToDateTime(sOt).ToString("HH:mm:ss");
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Value = sOt;
                    //lblExcessTime.Text = sOt;
                }
                else
                {
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Tag = 0;
                    DgvAttendance.Rows[intRowCount].Cells["ColDgvExcessTime"].Value = "";
                    //lblExcessTime.Text = "00:00:00";
                }

                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("WorktimeCalculation datarow[]" + Ex.ToString(), 2);
                //MessageBox.Show("WorkTimeCalculation[]--" + Ex.ToString());
                return false;
            }
        }

        private string GetOt(string StrOt)
        {
            string StrOtReturn = "0";
            try
            {
                int iOt = 0;
                if (Int32.TryParse(StrOt, out iOt) == false)
                    StrOt = "0";

                StrOtReturn = StrOt;
                string strStartMin = "";
                string strEndMin = "";
                string strOTMin = "";

                DataTable dtOtDetails = MobjclsBLLAttendance.GetOtDetails();
                if (dtOtDetails != null)
                {
                    if (dtOtDetails.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtOtDetails.Rows)
                        {
                            strStartMin = (row["StartMin"] == DBNull.Value ? "0" : Convert.ToString(row["StartMin"]));
                            strEndMin = (row["EndMin"] == DBNull.Value ? "0" : Convert.ToString(row["EndMin"]));
                            strOTMin = (row["OTMin"] == DBNull.Value ? "0" : Convert.ToString(row["OTMin"]));
                            if (iOt >= Convert.ToInt32(strStartMin) && iOt <= Convert.ToInt32(strEndMin))
                            {
                                StrOtReturn = strOTMin;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
            return StrOtReturn;
        }

        private int GetShiftOrderNo(int iShiftId, string sFirstTime, ref string sFromTime, ref string sToTime, ref string sDur) // 'Finding shift OrderNo from First Punch time
        {
            int iShiftOrderNo = 0;
            sFromTime = "";
            sToTime = "";
            bool blnNightshiftFlag = false;
            try
            {

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftId);
                if (DtShiftDetails.Rows.Count > 0)
                {
                    if (sFirstTime != "")
                    {
                        int iBufferTime = 0;

                        int iDuration = 0;
                        for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                        {

                            iShiftOrderNo = 0;

                            sDur = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                            iBufferTime = Convert.ToInt32(DtShiftDetails.Rows[i]["BufferTime"]);
                            sFromTime = Convert.ToString((DtShiftDetails.Rows[i]["FromTime"]));
                            sToTime = Convert.ToString((DtShiftDetails.Rows[i]["ToTime"]));

                            int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iDuration), Convert.ToDateTime(sToTime));
                            if (tmDurationDiff < 0)
                            {
                                blnNightshiftFlag = true;
                                sFromTime = Convert.ToString(Convert.ToDateTime(sFromTime));
                                sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                            }

                            if (Convert.ToDateTime(sFirstTime) >= Convert.ToDateTime(sFromTime).AddMinutes(-iBufferTime) &&
                                Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime(sFromTime).AddMinutes(iBufferTime))
                            {
                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                break;
                            }

                        } //for i

                        if (iShiftOrderNo <= 0)
                        {
                            int iTime = 0;
                            iShiftOrderNo = 0;
                            string sFrom = "";
                            string sTo = "";

                            for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                            {

                                iDuration = 0;
                                blnNightshiftFlag = false;
                                sDur = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                                iBufferTime = Convert.ToInt32(DtShiftDetails.Rows[i]["BufferTime"]);
                                sFromTime = Convert.ToString((DtShiftDetails.Rows[i]["FromTime"]));
                                sToTime = Convert.ToString((DtShiftDetails.Rows[i]["ToTime"]));

                                int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iDuration), Convert.ToDateTime(sToTime));
                                if (tmDurationDiff < 0)
                                {
                                    blnNightshiftFlag = true;
                                    sFromTime = Convert.ToString(Convert.ToDateTime(sFromTime));
                                    sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                                }
                                if (i == 0)
                                {
                                    iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                    iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                    sFrom = sFromTime;
                                    sTo = sToTime;
                                }
                                else
                                {
                                    if (blnNightshiftFlag)
                                    {
                                        bool flag = false;
                                        if (Convert.ToDateTime(sFirstTime) >= Convert.ToDateTime("12:00 AM") && Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime("12:00 PM"))
                                        {
                                            flag = true;
                                        }

                                        if (Convert.ToDateTime(sFirstTime) >= Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sFirstTime) <= Convert.ToDateTime("11:59 PM"))
                                        {
                                            if (Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))) < iTime)
                                            {
                                                iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                                sFrom = sFromTime;
                                                sTo = sToTime;
                                            }
                                        }
                                        else if (Convert.ToDateTime(sFirstTime) < Convert.ToDateTime(sFromTime))
                                        {
                                            if (Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))) < iTime)
                                            {
                                                iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                                sFrom = sFromTime;
                                                sTo = sToTime;
                                            }
                                        }
                                        else
                                        {
                                            if (Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)))) < iTime)
                                            {
                                                iTime = Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))));
                                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                                sFrom = sFromTime;
                                                sTo = sToTime;
                                            }
                                        }

                                        if (flag == true)
                                        {
                                            if (Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)))) < iTime)
                                            {
                                                iTime = Math.Abs(1440 - Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))));
                                                iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                                sFrom = sFromTime;
                                                sTo = sToTime;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime))) < iTime)
                                        {
                                            iTime = Math.Abs(System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sFirstTime)));
                                            iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[i]["OrderNo"]);
                                            sFrom = sFromTime;
                                            sTo = sToTime;
                                        }
                                    }
                                }

                            }//For 
                            sFromTime = sFrom;
                            sToTime = sTo;
                        }

                    }
                    else
                    {
                        iShiftOrderNo = Convert.ToInt32(DtShiftDetails.Rows[0]["OrderNo"]);
                        if (Glbln24HrFormat == false)
                        {
                            sFromTime = ConvertDate12(Convert.ToDateTime(DtShiftDetails.Rows[0]["FromTime"]));
                            sToTime = ConvertDate12(Convert.ToDateTime(DtShiftDetails.Rows[0]["ToTime"]));

                        }
                        else
                        {
                            sFromTime = ConvertDate24(Convert.ToDateTime(DtShiftDetails.Rows[0]["FromTime"]));
                            sToTime = ConvertDate24(Convert.ToDateTime(DtShiftDetails.Rows[0]["ToTime"]));
                        }
                        sDur = Convert.ToString(DtShiftDetails.Rows[0]["Duration"]);
                    }
                    if (blnNightshiftFlag)
                    {
                        if (Glbln24HrFormat == false)
                        {

                            sFromTime = ConvertDate12(Convert.ToDateTime(sFromTime));
                            sToTime = ConvertDate12(Convert.ToDateTime(sToTime));
                        }
                        else
                        {
                            sFromTime = ConvertDate24(Convert.ToDateTime(sFromTime));
                            sToTime = ConvertDate24(Convert.ToDateTime(sToTime));

                        }
                    }

                }

                return iShiftOrderNo;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString());
                return iShiftOrderNo;
            }
        }
        private string ConvertDate12(DateTime dateString)//Getting time format
        {
            try
            {
                DTPFormatTime.CustomFormat = "hh:mm tt";
                DTPFormatTime.Text = dateString.ToString();
                return DTPFormatTime.Text;
            }
            catch (Exception)
            {
                return " ";
            }
        }
        private string ConvertDate24(DateTime dateString)
        {
            try
            {
                DTPFormatTime.CustomFormat = "HH:mm";
                DTPFormatTime.Text = dateString.ToString();
                return DTPFormatTime.Text;
            }
            catch (Exception)
            {
                return " ";
            }
        }

        private string Convert24HrFormat(string StrValue)
        {
            try
            {
                StrValue = FormatString(StrValue);

                if (StrValue.IndexOf(".") != -1)
                    StrValue = StrValue.Replace(".", ":");

                string ReturnTime = "";
                if (StrValue.IndexOf(":") != -1)
                {
                    string LeftTime = "";
                    string RightTime = "";
                    string[] strar = new string[2];
                    strar = StrValue.Split(':');
                    LeftTime = strar[0];
                    RightTime = strar[1];
                    if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                    {
                        if ((RightTime.Length) > 2)
                            return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                    }
                }
                if (StrValue.IndexOf(":") != -1)
                {
                    string LeftTime = "";
                    string RightTime = "";
                    string Time1 = "";
                    string time2 = "";
                    string[] strar = new string[2];
                    strar = StrValue.Split(':');
                    LeftTime = strar[0];
                    RightTime = strar[1];
                    if (Microsoft.VisualBasic.Information.IsNumeric(LeftTime))
                    {
                        if (Convert.ToInt32(LeftTime) > 23)
                            return StrValue;
                        else
                        {
                            if (LeftTime.Length == 2)
                                Time1 = LeftTime;
                            else if (LeftTime.Length == 1)
                                Time1 = "0" + LeftTime;

                        }
                    }
                    else
                        return "10p";

                    if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                        if (Convert.ToInt32(RightTime) > 59)
                            return StrValue;
                        else
                        {
                            if (RightTime.Length == 2)
                                time2 = RightTime;
                            else if (RightTime.Length == 1)
                                time2 = "0" + RightTime;

                        }
                    else
                        return "10p";

                    ReturnTime = Time1 + ":" + time2;
                    return ReturnTime;
                }
                else
                {

                    if (Microsoft.VisualBasic.Information.IsNumeric(StrValue))
                    {
                        if (Convert.ToInt32(StrValue) > 23)
                            return StrValue;
                        else
                        {
                            if (StrValue.Length == 2)
                            {
                                ReturnTime = StrValue + ":" + "00";
                                return ReturnTime;
                            }
                            else if (StrValue.Length == 1)
                            {
                                ReturnTime = "0" + StrValue + ":" + "00";
                                return ReturnTime;
                            }
                            else
                                return StrValue;

                        }
                    }
                    else
                    {
                        return StrValue;
                    }

                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return StrValue;
            }
        }

        private int GetDurationInMinutes(string sDuration)// 'Getting Duration In minutes
        {
            int iDurationInMinutes = 0;
            try
            {
                string[] sHourMinute = new string[2];

                if (sDuration.Contains('.'))
                {
                    sHourMinute = sDuration.Split('.');
                    iDurationInMinutes = Convert.ToInt32(sHourMinute[0]) * 60 + Convert.ToInt32(sHourMinute[1]);

                }
                else if (sDuration.Contains(':'))
                {
                    sHourMinute = sDuration.Split(':');
                    iDurationInMinutes = Convert.ToInt32(sHourMinute[0]) * 60 + Convert.ToInt32(sHourMinute[1]);
                }
                else if (sDuration != "")
                {
                    iDurationInMinutes = Convert.ToInt32(sDuration) * 60;
                }
                return iDurationInMinutes;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return iDurationInMinutes;
            }
        }
        private bool WorktimeCalculationForSplitShift(DataRow[] datarow, int iShiftID, int gCount, string sdateFormat, bool bdatetime,
                                                 string sFromTime, string sToTime, int iLate, int iEarly, bool bLate,
                                                bool bEarly, string sEmpName, bool isAfterMinWrkHrsAsOT, bool blnConsiderBufferForOT, int iBufferForOT,
                                                ref string sWorkTime, ref string sBreakTime, ref string sOt, ref int intTotalSplitAbsentTm, ref int intTotalAbsentTm)
        {
            //'Worktime Calculation for Split shift  

            try
            {
                DgvAttendance.Rows[gCount].Cells["ColDgvShiftOrderNo"].Value = 0;//Dynamic Shift
                DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Value = "00:00:00";
                DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Tag = 0;
                //lblLateComing.Visible = false;
                DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Tag = 0;
                //lblEarlyGoing.Visible = false;

                sWorkTime = "0";
                sBreakTime = "0";
                sOt = "0";
                string sOvertime = "0";
                bool nightFlag = false;

                int intWorkTm = 0;
                int intBreakTm = 0;
                int intAbsentTm = 0;
                int intSplitAbsentTm = 0;
                intTotalSplitAbsentTm = 0;
                intTotalAbsentTm = 0;

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftID);

                if (DtShiftDetails.Rows.Count > 0)
                {
                    for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                    {

                        string sSplitFromTime = Convert.ToString(DtShiftDetails.Rows[i]["FromTime"]);
                        string sSplitToTime = Convert.ToString(DtShiftDetails.Rows[i]["ToTime"]);
                        string sDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                        int iMinWorkingHrsMin = GetDurationInMinutes(sDuration);
                        //'Modified for Split Shift
                        if (nightFlag == true)
                        {
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).AddDays(1).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }

                        int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime).AddMinutes(iMinWorkingHrsMin), Convert.ToDateTime(sSplitToTime));
                        if (tmDurationDiff < 0)
                        {
                            nightFlag = true;
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }
                        string sPreviousTime = "";
                        bool bShiftChangeTime = true;
                        bool bShiftChangeOvertime = true;
                        bool bShiftChangeTimeBrkOT = true;
                        bool bShiftChangeTimeEarlyWRk = true;
                        bool bShiftChangeTimeEarlyBRK = true;
                        string sOTBreakTime = "0";

                        for (int j = 0; j <= datarow.Length - 1; j++)
                        {
                            string sTimeValue = datarow[j]["column3"].ToString();
                            string dResult = "";
                            if (sTimeValue == "")
                            {
                                DgvAttendance.Rows[gCount].DefaultCellStyle.ForeColor = Color.IndianRed;
                                DgvAttendance.Rows[gCount].Cells["ColDgvValidFlag"].Value = 0;
                                continue;
                            }

                            if (Datetimeformat(sTimeValue, sdateFormat, bdatetime, ref dResult))
                            { sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm"); }

                            if (j == 0)// ''first time' Grater than Shift time Checking eg:09:00AM to 06:00PM
                            {//'---------------------------------Late Coming
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate) && bLate == true)// Then 'bFlexi = False
                                {
                                    int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                    if (il > 0)
                                    {
                                        string sLate = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today).ToString();
                                        DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                    }
                                    //lblLateComing.Tag = 1;
                                    //lblLateComing.Visible = true;
                                    DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes
                                }
                                else
                                {
                                    //lblLateComing.Tag = 0;
                                    //lblLateComing.Visible = false;
                                    DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Value = "00:00:00"; //'if 1 then LateComing=yes
                                    DgvAttendance.Rows[gCount].Cells["ColDgvLateComing"].Tag = 0;
                                }
                                //'---------------------------------Late Coming
                            }
                            else
                            {
                                if (sPreviousTime != "")
                                {
                                    int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));
                                    string sPrevCurrent = tmDiff.ToString();

                                    if (tmDiff < 0)// 'if date diff less than Zero then adding day to the StimeValue
                                    {
                                        tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                        sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                                    }
                                    if (j % 2 == 1) //Then           'if mode 1 then wrk time else Break time
                                    {
                                        //'------------------------------------------------Early going Case Checking
                                        if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly) && bEarly == true)//Then 'bFlexi = False
                                        {
                                            int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                            if (iE > 0)
                                            {
                                                string sEarly = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today).ToString();
                                                DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                            }

                                            //lblEarlyGoing.Visible = true;
                                            DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes
                                        } //'------------------------------------------------Early going 
                                        //'---------------------------------------------Split Over Time-------------------------------------------

                                        if (i == DtShiftDetails.Rows.Count - 1)
                                        {
                                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))//Then
                                            {
                                                if (bShiftChangeOvertime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                                {
                                                    bShiftChangeOvertime = false;
                                                    int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                                    if (i == DtShiftDetails.Rows.Count - 1)
                                                    {
                                                        sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + idiffCurtShiftOT);

                                                    }
                                                }
                                                else
                                                {
                                                    if (i == DtShiftDetails.Rows.Count - 1)
                                                    {

                                                        sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + Convert.ToDouble(tmDiff));

                                                    } //'WorkTime Calculation 
                                                }
                                            }
                                        }// '---------------------------------------------Split Over Time-------------------------------------------

                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime)) //Then
                                        {
                                            if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                            {
                                                bShiftChangeTime = false;
                                                int idiffPrevShitWork = 0;
                                                if (Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime)) //Then 'modified 28 dec
                                                {
                                                    idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sSplitToTime));
                                                }
                                                else
                                                {
                                                    idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));
                                                }
                                                int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                {
                                                    sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                                }

                                                sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                            }
                                            else
                                            {
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                {
                                                    sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff));

                                                }// 'WorkTime Calculation 
                                            }
                                        }
                                        else
                                        {
                                            bool bflag = false;
                                            if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                            {
                                                bflag = true;
                                            }

                                            if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                            {
                                                bShiftChangeTimeEarlyWRk = false;
                                                int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                                sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                                bflag = true;
                                            }
                                            if (bflag == false)
                                            {
                                                sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));
                                            }
                                        }

                                    }
                                    else //'-------------------------------------------------------Break Time Calculation 
                                    {
                                        if (i == DtShiftDetails.Rows.Count - 1)
                                        {
                                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                            {
                                                if (bEarly)
                                                {

                                                    //lblEarlyGoing.Visible = false;
                                                    DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Tag = 0; //'Earlygoing yes
                                                    DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                                }

                                            }
                                        }

                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                        {
                                            if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                            {
                                                bShiftChangeTimeBrkOT = false;
                                                int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));
                                                int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));
                                                sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));// 'Break Time Calculation
                                                sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                            }
                                            else
                                            {
                                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime)) { sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff)); } //'Then ot Break Time Calculation
                                            }

                                        }
                                        else
                                        {
                                            bool bflag = false;
                                            if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                            { bflag = true; }
                                            if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                            {
                                                bShiftChangeTimeEarlyBRK = false;
                                                int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                                sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                                bflag = true;

                                            }
                                            if (bflag == false)
                                            {
                                                sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                            }
                                            //'-------------------------------------Early Going validating

                                            //lblEarlyGoing.Visible = false;
                                            DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Tag = 0; //'Earlygoing yes
                                            DgvAttendance.Rows[gCount].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                            //'-------------------------------------Early Going 
                                        }
                                    }//else breaktime Cal
                                }//if Previous Time
                            } //'j=0

                            sPreviousTime = sTimeValue;
                            if (Glbln24HrFormat == false)
                                DgvAttendance.Rows[gCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("hh:mm tt");
                            else
                                DgvAttendance.Rows[gCount].Cells[j + 3].Value = Convert.ToDateTime(sTimeValue).ToString("HH:mm");
                        } //'j = 0 To datarow.Length - 1 '

                        //'------------------------------------Shoratge Calculation starts----------------------------------
                        intSplitAbsentTm = 0;

                        intAbsentTm = 0;
                        if (intWorkTm == Convert.ToInt32(sWorkTime) && intBreakTm == Convert.ToInt32(sBreakTime))
                        { intAbsentTm = iMinWorkingHrsMin; }
                        else
                        {
                            intSplitAbsentTm = iMinWorkingHrsMin - (Convert.ToInt32(sWorkTime) - intWorkTm);
                            if (intSplitAbsentTm > 0)
                            { intTotalSplitAbsentTm += intSplitAbsentTm; }
                        }
                        if (intAbsentTm > 0)
                        { intTotalAbsentTm += intAbsentTm; }


                        intWorkTm = Convert.ToInt32(sWorkTime);
                        intBreakTm = Convert.ToInt32(sBreakTime);
                        //'------------------------------------Shoratge Calculation Ends------------------------------------

                    } //'For i As Integer = 0 To DtShiftDetails.Rows.Count - 1
                    if (isAfterMinWrkHrsAsOT == false)
                    {
                        sOt = sOvertime;
                    }
                }//'  If DtShiftDetails.Rows.Count > 0
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on WorktimeCalculationForSplitShiftDataRow():Attendance Auto." + Ex.Message.ToString() + "", 3);
                return false;
            }
        }
        private bool Datetimeformat(string sDate, string sFormat, bool bDateTime, ref string dDate)
        {

            try
            {
                int iMonth = 0;
                int iYear = 0;
                int iDay = 0;
                string sMonth = "";
                string[] sSplit = new string[3];

                char cSeparator = '/';

                if (sDate.IndexOf('/') != -1)
                {
                    cSeparator = '/';
                }
                else if (sDate.IndexOf('-') != -1)
                {
                    cSeparator = '-';
                }
                else if (sDate.IndexOf(' ') != -1)
                {
                    cSeparator = ' ';
                }
                if (bDateTime)
                {
                    string sDatePart = sDate.Substring(0, sDate.IndexOf(" "));
                    string stimePart = sDate.Substring(sDate.IndexOf(" "), sDate.Length - sDate.IndexOf(" "));


                    if (sFormat == "dd/MM/yyyy" || sFormat == "dd-MM-yyyy")
                    {
                        //ThenOr sFormat = "dd\MM\yyyy"
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[0]);
                    }
                    else if (sFormat == "MM/dd/yyyy" || sFormat == "MM-dd-yyyy")
                    {//Or sFormat = "MM\dd\yyyy" Then
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[0]);
                        iDay = Convert.ToInt32(sSplit[1]);

                    }
                    else if (sFormat == "dd/MM/yy" || sFormat == "dd-MM-yy")// Or sFormat = "dd\MM\yy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[0]);
                    }
                    else if (sFormat == "MM/dd/yy" || sFormat == "MM-dd-yy")//Or sFormat = "MM\dd\yy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[0]);
                        iDay = Convert.ToInt32(sSplit[1]);
                    }
                    else if (sFormat == "d/M/yyyy" || sFormat == "d-M-yyyy")//Or sFormat = "d\M\yyyy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[0]);
                    }
                    else if (sFormat == "M/d/yyyy" || sFormat == "M-d-yyyy")//Or sFormat = "M\d\yyyy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[0]);
                        iDay = Convert.ToInt32(sSplit[1]);
                    }
                    else if (sFormat == "d/M/yy" || sFormat == "d-M-yy")//|| sFormat = "d\M\yy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[0]);
                    }
                    else if (sFormat == "M/d/yy" || sFormat == "M-d-yy")//|| sFormat = "M\d\yy" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[2]);
                        iMonth = Convert.ToInt32(sSplit[0]);
                        iDay = Convert.ToInt32(sSplit[1]);
                    }
                    else if (sFormat == "yyyy/MM/dd" || sFormat == "yyyy-MM-dd")//|| sFormat = "yyyy\MM\dd" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    else if (sFormat == "yyyy/M/d" || sFormat == "yyyy-M-d")// || sFormat = "yyyy\M\d" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    else if (sFormat == "yy/MM/dd" || sFormat == "yy-MM-dd")// || sFormat = "yy\MM\dd" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    else if (sFormat == "yy/M/d" || sFormat == "yy-M-d")//|| sFormat = "yy\M\d" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    else if (sFormat == "dd MMM yyyy" || sFormat == "dd-MMM-yyyy")//|| sFormat = "yy\M\d" Then
                    {
                        sSplit = sDatePart.Split(cSeparator);
                        iYear = Convert.ToInt32(sSplit[0]);
                        iMonth = Convert.ToInt32(sSplit[1]);
                        iDay = Convert.ToInt32(sSplit[2]);
                    }
                    sMonth = GetMonth(iMonth);
                    sDate = iDay.ToString().PadLeft(2, '0') + " " + sMonth + " " + iYear.ToString().PadLeft(3, '0').PadLeft(4, '2') + stimePart;

                }
                else
                {
                    sDate = "01 Jan 2000 " + sDate;
                }
                dDate = sDate;
                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }
        private string GetMonth(int iMonth)
        {
            //Get month name of a year
            string sMonth = "";
            if (iMonth >= 1 && iMonth <= 12)
                return Microsoft.VisualBasic.DateAndTime.MonthName(iMonth, true);

            return sMonth;
        }

        private string FormatString(string strTime)
        {
            if (strTime.ToStringCustom() == string.Empty)
                return "00:00";

            string strTT = "";
            if (strTime.ToLower().Contains('a'))
                strTT = "AM";
            else if (strTime.ToLower().Contains('p'))
                strTT = "PM";

            strTime = strTime.ToLower();
            strTime = strTime.Replace(":", ".");
            strTime = strTime.Replace("a", "");
            strTime = strTime.Replace("p", "");
            strTime = strTime.Replace("m", "");

            string strOutput = "00:00";
            decimal decOutput = 0M;

            if (Decimal.TryParse(strTime, out decOutput))
            {
                strOutput = String.Format("{0:N2}", decOutput).Replace(".", ":").Replace(",", "");

                if (decOutput > 0)
                {
                    string[] arr = strOutput.Split(':');
                    if (arr.Length >= 2)
                    {
                        int intLeft = Convert.ToInt32(arr[0]);
                        int intRight = Convert.ToInt32(arr[1]);
                        intLeft = intLeft > 23 ? 0 : intLeft;
                        intRight = intRight > 59 ? 0 : intRight;
                        if (intRight < 10)
                        {
                            string strRight = "0" + intRight.ToString();
                            strOutput = String.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + "." + strRight)).Replace(".", ":").Replace(",", "");

                        }
                        else
                        {
                            strOutput = String.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + "." + intRight.ToString())).Replace(".", ":").Replace(",", "");

                        }

                    }
                }
            }
            strOutput = strOutput + " " + strTT;
            strOutput = strOutput.TrimEnd().TrimStart();
            return strOutput;

        }
        private void ClearAllControls()
        {

            try
            {
                InitializeGrid();
                DgvAttendance.Rows.Clear();
                DgvAttendance.Refresh();
                barProgressBarAttendance.Visible = false;
                barProgressBarAttendance.Maximum = 0;
                ClearBottomPanel();

            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
            }
        }
        private bool InitializeGrid()
        {
            try
            {

                int iColIndex = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;
                int iCounter;
                int iLimit;
                iLimit = ColDgvStatus.Index + 1;
                for (iCounter = iColIndex; iCounter >= iLimit; iCounter -= 1)
                {
                    if (DgvAttendance.Columns[iCounter].Name.IndexOf("Time") != -1)
                    {
                        DgvAttendance.Columns.RemoveAt(iCounter);
                        DgvAttendance.Columns[iCounter].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                }
                MintTimingsColumnCnt = 0;
                if (MintDisplayMode == Convert.ToInt16(AttendanceMode.AttendanceManual))
                {
                    DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                    DataGridViewTextBoxColumn adcolOut = new DataGridViewTextBoxColumn();

                    adcolIn.Name = "TimeIn1";
                    adcolIn.HeaderText = "TimeIn1";
                    adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolIn.Resizable = DataGridViewTriState.False;
                    adcolIn.Width = 70;
                    adcolIn.MaxInputLength = 9;

                    adcolOut.Name = "TimeOut1";
                    adcolOut.HeaderText = "TimeOut1";
                    adcolOut.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolOut.Resizable = DataGridViewTriState.False;
                    adcolOut.Width = 70;
                    adcolIn.MaxInputLength = 9;

                    DgvAttendance.Columns.Insert(iLimit, adcolIn);
                    DgvAttendance.Columns.Insert(iLimit + 1, adcolOut);
                    MintTimingsColumnCnt = 2;
 
                    ColDgvEmployee.Visible = true;
                    ColDgvAtnDate.Visible = false;
                }
                else
                {
                    ColDgvEmployee.Visible = true;
                    ColDgvAtnDate.Visible = true;
                }
                DgvAttendance.Columns["ColDgvAtnDate"].Width = 75;
                DgvAttendance.Columns["ColDgvAddMore"].Width = 35;
                DgvAttendance.Columns["ColDgvAddMore"].MinimumWidth = 35;
                DgvAttendance.Columns["ColDgvAddMore"].Resizable = DataGridViewTriState.False;
                DgvAttendance.Refresh();
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (DgvAttendance.Rows.Count > 0)
                {

                    if (SaveAttendanceInfo())
                    {
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1600, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        barProgressBarAttendance.Visible = false;
                        barProgressBarAttendance.Value = 0;
                        ClearAllControls();
                    }

                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }
        private void FillCompany()
        {
            CboCompany.DisplayMember = "Company";
            CboCompany.ValueMember = "CompanyID";
            CboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Load Branches by Company
        /// </summary>
        private void FillAllBranches()
        {
            CboBranch.DisplayMember = "Branch";
            CboBranch.ValueMember = "BranchID";
            CboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(CboCompany.SelectedValue.ToInt32());
        }


        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }



        /// <summary>
        /// Load All Designations
        /// </summary>
        private void FillAllDesignations()
        {
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation();
        }

        /// <summary>
        /// Load All Departments
        /// </summary>



        /// <summary>
        /// Load Employees by Company,Department,Designation
        /// </summary>
        private void FillAllEmployees()
        {
            CboEmployee.DisplayMember = "EmployeeFullName";
            CboEmployee.ValueMember = "EmployeeID";
            CboEmployee.DataSource = clsBLLReportCommon.GetAllEmployeesCurrent(CboCompany.SelectedValue.ToInt32(), CboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), -1, -1, -1, 1);
        }
        /// <summary>
        /// Load Month 
        /// </summary>

        private bool LoadCombo(int iType)
        {
            try
            {

                DataTable datCombos;
                if (iType == 0 || iType == 1) //Then 'Loading Attendnace Status
                {
                    datCombos = new DataTable();
                    datCombos = MobjclsBLLAttendance.FillCombos(new string[] { "AttendenceStatusID,AttendanceStatus", "PayAttendenceStatusReference", "", "AttendenceStatusID", "AttendanceStatus" });
                    ColDgvStatus.ValueMember = "AttendenceStatusID";
                    ColDgvStatus.DisplayMember = "AttendanceStatus";
                    ColDgvStatus.DataSource = datCombos;
                }

                if (iType == 0 || iType == 2) 
                {
                    FillAllDepartments();
                }

                if (iType == 0 || iType == 3) 
                {
                    FillAllDesignations(); 
                }

                if (iType == 0 || iType == 4) 
                {
                    FillAllEmployees(); 
                }

                if (iType == 0 || iType == 5)
                {

                    FillCompany();
                }

                if (iType == 0 || iType == 6)
                {

                    FillAllDesignations();
                }

                if (iType == 0 || iType == 7)
                {
                    FillAllBranches(); 
                }
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on LoadCombo " + this.Name + " " + Ex.Message.ToString(), 1);
                //MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private void dtpMonthSelect_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                dtpMonthSelect.Value = ("01/" + GetCurrentMonth(dtpMonthSelect.Value.Month) + "/" + (dtpMonthSelect.Value.Year) + "").ToDateTime();
            }
            catch
            {
            }
        }

        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {

            //lblAttendnaceStatus.Text = "";
            lblPrgressPercentage.Text = "";
            lblPrgressPercentage.Refresh();
            DgvAttendance.RowCount = 0;
        }

        private void DgvAttendance_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (DgvAttendance.IsCurrentCellDirty)
                {
                    if (DgvAttendance.CurrentCell != null)
                        DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);

                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }

        private void DgvAttendance_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (DgvAttendance.CurrentCell != null)
                {
                    if (DgvAttendance.CurrentCell.ColumnIndex == ColDgvStatus.Index)
                    {
                        CboStatusComboBox = (ComboBox)e.Control;
                        if (CboStatusComboBox != null)
                        {
                            CboStatusComboBox.KeyPress -= new KeyPressEventHandler(ComboBox_KeyPress);
                            CboStatusComboBox.KeyPress += new KeyPressEventHandler(ComboBox_KeyPress);
                        }

                        //DgvAttendance.NotifyCurrentCellDirty(true);
                    }
                    if (DgvAttendance.CurrentCell.ColumnIndex > ColDgvStatus.Index && DgvAttendance.CurrentCell.ColumnIndex < ColDgvAddMore.Index)
                    {
                        if (DgvAttendance.CurrentCell is DataGridViewTextBoxCell)
                        {
                            TextBox txt = (TextBox)(e.Control);
                            txt.KeyPress -= new KeyPressEventHandler(txtInt_KeyPress);
                            txt.KeyPress += new KeyPressEventHandler(txtInt_KeyPress);

                        }
                    }
                    if (DgvAttendance.CurrentCell.ColumnIndex == ColDgvRemarks.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress -= new KeyPressEventHandler(txtInt_KeyPress);
                    }

                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }



        private void txtInt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " bcdefghijklnoqrstuvwxyz~!@#$%^&*()_+|}{?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
                return;
            }
            string strValidChars = " amp.:";
            if ((strValidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ((TextBox)sender).Text.ToLower().Contains(e.KeyChar.ToString())))
            {
                e.Handled = true;
                return;
            }
            if ((strValidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ((TextBox)sender).Text.ToLower().Equals("") && ((TextBox)sender).SelectedText.Equals("")))
            {
                e.Handled = true;
                return;
            }
            if ((strValidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ((TextBox)sender).Text.Length == ((TextBox)sender).SelectedText.Length))
            {
                e.Handled = true;
                return;
            }
            if ((e.KeyChar.ToString().ToLower() == "p" && ((TextBox)sender).Text.ToLower().Contains('a')))
            {
                e.Handled = true;
                return;
            }
            if ((e.KeyChar.ToString().ToLower() == "a" && ((TextBox)sender).Text.ToLower().Contains('p')))
            {
                e.Handled = true;
                return;
            }
        }

        private void ComboBox_KeyPress(object Sender, KeyPressEventArgs e)
        {
            CboStatusComboBox.DroppedDown = false;
        }

        private void DgvAttendance_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }

        }

        private void DgvAttendance_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (DgvAttendance.RowCount <= 0)//Then Exit Sub
                { return; }
                int iGridRowIndex = DgvAttendance.CurrentRow.Index;
                int iGridColIndex = DgvAttendance.CurrentCell.ColumnIndex;
                switch (e.KeyCode)
                {
                    case Keys.Enter:
                        if (DgvAttendance.Columns[iGridColIndex].Name.IndexOf("ColDgvAddMore") != -1)
                        {
                            if ((DgvAttendance.Rows[iGridRowIndex].Cells[iGridColIndex - 2].Value != null) && DgvAttendance.Rows[iGridRowIndex].Cells[iGridColIndex - 1].Value != null)
                            {
                                if (Convert.ToString(DgvAttendance.Rows[iGridRowIndex].Cells[iGridColIndex - 2].Value).Trim() == "" || Convert.ToString(DgvAttendance.Rows[iGridRowIndex].Cells[iGridColIndex - 1].Value).Trim() == "")
                                    return;
                                else
                                    AddNewColumns(3);

                            }
                            else
                            {
                                return;
                            }
                        }
                        break;
                    case Keys.Delete:
                        Application.DoEvents();
                        if (MblnDeletePermission)
                        {
                            if (MintDisplayMode == Convert.ToInt32(AttendanceMode.AttendanceAuto))
                            {
                                if (DgvAttendance.CurrentRow.Selected)
                                    return;
                                int iUpto = DgvAttendance.Columns["ColDgvAddMore"].Index;
                                bool bChange = false;
                                foreach (DataGridViewCell cell in DgvAttendance.SelectedCells)
                                {
                                    for (int i = cell.ColumnIndex; i <= iUpto - 1; i++)
                                    {
                                        if (cell.ColumnIndex > 2)
                                        {
                                            if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value != null && DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex + 1].Value != null)
                                            {
                                                if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value != DBNull.Value && DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex + 1].Value != DBNull.Value)
                                                {
                                                    if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value.ToStringCustom() != string.Empty)
                                                    {
                                                        if (i == iUpto - 1)
                                                        { DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = ""; }
                                                        else
                                                        {
                                                            DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = DgvAttendance.Rows[cell.RowIndex].Cells[i + 1].Value;
                                                            bChange = true;
                                                        }
                                                    }

                                                }
                                                else if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value == DBNull.Value && DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex + 1].Value != DBNull.Value)
                                                {
                                                    if (i == iUpto - 1)
                                                    { DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = ""; }
                                                    else
                                                    {
                                                        DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = DgvAttendance.Rows[cell.RowIndex].Cells[i + 1].Value;
                                                        bChange = true;
                                                    }
                                                }
                                                else if (DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex].Value != DBNull.Value && DgvAttendance.Rows[cell.RowIndex].Cells[cell.ColumnIndex + 1].Value == DBNull.Value)
                                                {
                                                    if (i == iUpto - 1)
                                                    {
                                                        DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = "";
                                                    }
                                                    else
                                                    {
                                                        DgvAttendance.Rows[cell.RowIndex].Cells[i].Value = DgvAttendance.Rows[cell.RowIndex].Cells[i + 1].Value;
                                                        bChange = true;
                                                    }
                                                }

                                            }
                                        }
                                    }//for
                                }//for each
                                ClearBottomPanel();
                                if (bChange)
                                    WorkTimeCalculation(DgvAttendance.CurrentRow.Index, false);
                            }
                            if (iGridRowIndex >= 0)
                            {
                                bool blnDelete = false;
                                foreach (DataGridViewRow DgvRow in DgvAttendance.SelectedRows)
                                {
                                    int iGridSelectedRowIndex = DgvRow.Index;
                                    if (iGridSelectedRowIndex >= 0)
                                    {
                                        if (DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag != null)
                                        {

                                            DateTime dtDate = Convert.ToDateTime(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvAtnDate"].Value);

                                            if (MobjclsBLLAttendance.IsSalaryReleased(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), dtDate) == false) //Then 'If CheckReleasedSalary(Convert.ToInt32(AttendanceDataGridView.Rows(iGridSelectedRowIndex).Cells("Employee").Tag), dtDate) = False Then
                                            {

                                                MobjclsBLLAttendance.DeleteLeaveEntry(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), dtDate);
                                                if (Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvLeaveId"].Value) > 0)
                                                {
                                                    MobjclsBLLAttendance.DeleteLeaveFromAttendance(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), dtDate, Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvLeaveId"].Value));
                                                }
                                                MobjclsBLLAttendance.DeleteAttendance(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), dtDate);
                                                blnDelete = true;
                                            }
                                            else
                                            {

                                                if (MobjclsBLLAttendance.IsAttendanceExistsManual(Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvCompanyID"].Value), Convert.ToInt32(DgvAttendance.Rows[iGridSelectedRowIndex].Cells["ColDgvEmployee"].Tag), dtDate) != true)
                                                {
                                                    //MessageBox.Show("Cannot delete employee attendance. Salary released.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1599, out MmessageIcon);
                                                    //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                    //tmrClearlabels.Enabled = true;
                                                    e.Handled = true;
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (blnDelete)
                                {
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                                    //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    //tmrClearlabels.Enabled = true;
                                }

                            }
                        }
                        else
                        {
                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1599, out MmessageIcon).Replace("#", "").Trim();

                            // MessageBox.Show("You Don't have enough permission to delete attendance", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            MessageBox.Show(MstrMessCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                            //tmrClearlabels.Enabled = true;
                            e.Handled = true;
                            return;
                        }
                        break;
                    case Keys.Up:
                        ClearBottomPanel();
                        if (iGridRowIndex > 0)
                        {
                            WorkTimeCalculation(iGridRowIndex - 1, false);

                            //ShowConsequenceAndShiftInfo(iGridRowIndex - 1);
                        }
                        break;
                    case Keys.Down:
                        ClearBottomPanel();
                        if (iGridRowIndex < DgvAttendance.RowCount - 1)
                        {
                            WorkTimeCalculation(iGridRowIndex + 1, false);
                            //ShowConsequenceAndShiftInfo(iGridRowIndex + 1);
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }
        private bool WorkTimeCalculation(int iRowIndex, bool bInOutOnly)
        {
            try
            {
                string sWorkTime = "0";
                string sBreakTime = "0";
                string sOTBreakTime = "0";
                int iAbsentTime = 0;
                string sOt = "0";
                string sPreviousTime = "";
                bool bShiftChangeTime = true;
                bool bShiftChangeTimeBrkOT = true;
                bool bShiftChangeTimeEarlyWRk = true;
                bool bShiftChangeTimeEarlyBRK = true;
                bool blnNightShiftFlag = false;
                int intDurationInMinutes = 0;
                int intMinWorkHrsInMinutes = 0;
                bool blnIsMinWrkHRsAsOT = false;
                int iCountIndex = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;
                if (bInOutOnly)
                    iCountIndex = 4;

                int k = 0;
                for (int j = 3; j <= iCountIndex; j++)
                {
                    if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value).Trim() == "")
                        break;
                    if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value).Trim() != "")
                        k = k + 1;
                }

                if (k % 2 == 1)
                {
                    DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.IndianRed;
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvValidFlag"].Value = 0;
                }
                else if (k == 0)
                {
                    return false;
                }
                else
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvValidFlag"].Value = 1;
                    if (DgvAttendance.Rows[iRowIndex].Cells["ColDgvHolidayFlag"].Value.ToInt32() == 1 || DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1) //Then 'HOLIDAY CHEKINGG
                    {
                        if (DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveInfo"].Value.ToInt32() == 1)
                        {

                            DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.Black;
                        }
                        else
                        {
                            DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.Black;
                    }
                }

                DateTime atnDate = Convert.ToDateTime(DgvAttendance.Rows[iRowIndex].Cells["ColDgvAtnDate"].Value);
                string strAtnDate = atnDate.ToString("dd MMM yyyy");
                int iEmpId = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvEmployee"].Tag);
                int iCmpId = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvCompanyID"].Value);

                int iShiftId = Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvShift"].Tag);

                string sAllowedBreakTime = DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Tag != null && DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Tag.ToStringCustom() != string.Empty ? Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Tag) : "0";
                int iDayID = Convert.ToInt32(atnDate.DayOfWeek) + 1;
                string sFromDate = "";
                string sToDate = "";
                string sFromTime = "";
                string sToTime = "";
                string sDuration = "";

                int iShiftType = 0;
                int iNoOfTimings = 0;
                string sMinWorkHours = "0";
                int iLate = 0;
                int iEarly = 0;
                bool bOffDay = true;
                int iShiftOrderNo = 0;
                DataTable dtShiftInfo;
                dtShiftInfo = MobjclsBLLAttendance.GetShiftInfo(4, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                if (dtShiftInfo.Rows.Count > 0)
                {
                    bOffDay = false;
                    sFromTime = Convert.ToString(dtShiftInfo.Rows[0]["FromTime"]);
                    sToTime = Convert.ToString(dtShiftInfo.Rows[0]["ToTime"]);
                    sDuration = Convert.ToString(dtShiftInfo.Rows[0]["Duration"]);
                    iShiftType = Convert.ToInt32(dtShiftInfo.Rows[0]["ShiftTypeID"]);
                    iNoOfTimings = Convert.ToInt32(dtShiftInfo.Rows[0]["NoOfTimings"]);
                    sMinWorkHours = Convert.ToString(dtShiftInfo.Rows[0]["MinWorkingHours"]);
                    iLate = Convert.ToInt32(dtShiftInfo.Rows[0]["LateAfter"]);
                    iEarly = Convert.ToInt32(dtShiftInfo.Rows[0]["EarlyBefore"]);
                    sAllowedBreakTime = Convert.ToString(dtShiftInfo.Rows[0]["AllowedBreakTime"]);
                }
                if (bOffDay == true)
                {
                    dtShiftInfo = MobjclsBLLAttendance.GetShiftInfo(5, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                    if (dtShiftInfo.Rows.Count > 0)
                    {
                        sFromTime = Convert.ToString(dtShiftInfo.Rows[0]["FromTime"]);
                        sToTime = Convert.ToString(dtShiftInfo.Rows[0]["ToTime"]);
                        sDuration = Convert.ToString(dtShiftInfo.Rows[0]["Duration"]);
                        iShiftType = Convert.ToInt32(dtShiftInfo.Rows[0]["ShiftTypeID"]);
                        iNoOfTimings = Convert.ToInt32(dtShiftInfo.Rows[0]["NoOfTimings"]);
                        sMinWorkHours = Convert.ToString(dtShiftInfo.Rows[0]["MinWorkingHours"]);
                        sAllowedBreakTime = Convert.ToString(dtShiftInfo.Rows[0]["AllowedBreakTime"]);
                    }
                }
                if (iShiftType == 3)// Then 'for Dynamic Shift
                {
                    string dFirstTime = Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[3].Value);
                    string sFirstTime = Convert.ToDateTime(dFirstTime).ToString("HH:mm");

                    iShiftOrderNo = GetShiftOrderNo(iShiftId, sFirstTime, ref sFromTime, ref  sToTime, ref sDuration);
                    MobjclsBLLAttendance.GetDynamicShiftDuration(iShiftId, iShiftOrderNo, ref sMinWorkHours, ref sAllowedBreakTime);
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvShiftOrderNo"].Value = iShiftOrderNo;
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToDouble(sAllowedBreakTime);
                    string sAlldBreakT = sAllowedBreakTime;
                    sAlldBreakT = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAlldBreakT), DateTime.Today).ToString();
                    sAlldBreakT = Convert.ToDateTime(sAlldBreakT).ToString("HH:mm:ss");
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Value = sAlldBreakT;

                }

                dtShiftInfo = MobjclsBLLAttendance.GetShiftInfo(3, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                blnIsMinWrkHRsAsOT = MobjclsBLLAttendance.isAfterMinWrkHrsAsOT(iShiftId);
                intDurationInMinutes = GetDurationInMinutes(sDuration);//Getting Duration In mintes
                int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(intDurationInMinutes), Convert.ToDateTime(sToTime));
                if (tmDurationDiff < 0) //For Night Shift
                {
                    blnNightShiftFlag = true;
                    sFromTime = (Convert.ToDateTime(sFromTime)).ToString();
                    sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
                }

                int iAdditionalMinutes = 0;
                bool bEarly = true;
                bool bLate = true;
                iAdditionalMinutes = MobjclsBLLAttendance.GetLeaveExAddMinutes(iEmpId, Convert.ToDateTime(strAtnDate));
                if (iAdditionalMinutes > 0)
                {
                    sAllowedBreakTime = Convert.ToString(Convert.ToInt32(sAllowedBreakTime) + Convert.ToInt32(iAdditionalMinutes));
                    bEarly = false;
                    bLate = false;
                }
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                //lblLateComing.Visible = false;
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                //lblEarlyGoing.Visible = false;

                int iBufferForOT = 0;
                bool blnConsiderBufferForOT = false;
                int iMinOtMinutes = 0;
                blnConsiderBufferForOT = MobjclsBLLAttendance.IsConsiderBufferTimeForOT(iShiftId, ref iBufferForOT, ref iMinOtMinutes);
                intMinWorkHrsInMinutes = GetDurationInMinutes(sMinWorkHours);//Minwork hrs Setting

                if (iShiftType <= 3)
                {

                    for (int j = 3; j <= k + 2; j++)
                    {

                        DateTime dResult = Convert.ToDateTime(DgvAttendance.Rows[iRowIndex].Cells[j].Value);
                        string sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm");
                        if (iShiftType == 3)//'Modified for Dynamic Shift
                        {
                            if (blnNightShiftFlag == true)
                            {
                                if (Convert.ToDateTime(sTimeValue) >= Convert.ToDateTime("12:00 AM") &&
                                    Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime("12:00 PM"))
                                { sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt"); }

                            }
                        }

                        if (j == 3)
                        {
                            string sFrm = Convert.ToDateTime(sFromTime).ToString("HH:mm tt");
                            string sto = Convert.ToDateTime(sToTime).ToString("HH:mm tt");

                            if (Convert.ToDateTime(sFromTime) == Convert.ToDateTime("00:00"))
                            {
                                blnNightShiftFlag = true;
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFrm))
                                    sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(-1).ToString("dd MMM yyyy HH:mm tt");
                            }


                            if (blnNightShiftFlag && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sto))
                            {
                                if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm) && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm).AddMinutes(-120))
                                    sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt");
                            }
                            if (bLate)
                            {
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate) && iShiftType != 2)
                                {

                                    int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                    if (il > 0)
                                    {
                                        string sLate = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today));
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                    }

                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes

                                }
                                else
                                {
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                                }
                            }
                        }
                        else
                        {

                            int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));

                            if (tmDiff < 0)
                            {
                                tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                            }
                            if (j % 2 == 0)
                            {
                                if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly) && iShiftType != 2 && bEarly == true)
                                {
                                    int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                    if (iE > 0)
                                    {
                                        string sEarly = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today));
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                    }

                                    //lblEarlyGoing.Visible = true;
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes

                                }

                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                {

                                    if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                    {
                                        bShiftChangeTime = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));

                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                    }

                                    else
                                    {

                                        sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff));

                                    }
                                }
                                else
                                {
                                    bool bflag = false;

                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime))
                                        bflag = true;

                                    if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime))
                                    {
                                        bShiftChangeTimeEarlyWRk = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sTimeValue));
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork.ToString());
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));

                                }

                            }
                            else//brek time
                            {


                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                {
                                    //lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;

                                    if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                    {
                                        bShiftChangeTimeBrkOT = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));
                                        sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                            sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff));
                                    }
                                }
                                else
                                {

                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bflag = true;
                                    }
                                    if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime).AddMinutes(iLate) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                    {
                                        bShiftChangeTimeEarlyBRK = false;
                                        int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                    }

                                    //lblEarlyGoing.Visible = false;
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                }

                            }
                        }

                        sPreviousTime = sTimeValue;
                        if (Glbln24HrFormat == false)
                        {
                            sTimeValue = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                            DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                        }
                        else
                        {
                            DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("HH:mm");
                        }


                    }//for
                }
                else if (iShiftType == 4)
                {
                    int intMinWorkHrsInMinutesIf = 0;
                    intDurationInMinutes = GetSplitShiftTotalDuration(iShiftId);
                    sWorkTime = "0";
                    sBreakTime = "0";
                    sOt = "0";
                    int intTotalSplitAbsentTm = 0;
                    int intTotalAbsentTm = 0;
                    WorktimeCalculationForSplitShift(iShiftId, k, iRowIndex, sFromTime, sToTime, iLate, iEarly, bLate,
                                                     bEarly, "", blnIsMinWrkHRsAsOT, blnConsiderBufferForOT, iBufferForOT, ref sWorkTime, ref sBreakTime,
                                                     ref sOt, ref intTotalSplitAbsentTm, ref intTotalAbsentTm);


                    if (intDurationInMinutes > intMinWorkHrsInMinutes)  //'Split shift Absent           
                    {

                        if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave)) //Then 'for halfday
                        {
                            intMinWorkHrsInMinutesIf = intMinWorkHrsInMinutes / 2;
                        }
                        else
                        {
                            intMinWorkHrsInMinutesIf = intMinWorkHrsInMinutes;
                        }

                        if (iAdditionalMinutes > 0)
                            iAbsentTime = intMinWorkHrsInMinutesIf - (Convert.ToInt32(sWorkTime) + iAdditionalMinutes);
                        else
                            iAbsentTime = intMinWorkHrsInMinutesIf - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        if (intTotalSplitAbsentTm > (Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate))
                        {
                            iAbsentTime = intTotalAbsentTm + (intTotalSplitAbsentTm - (Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate));
                        }
                        else
                        {
                            iAbsentTime = intTotalAbsentTm;
                        }
                    }
                }

                if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave)) //Then 'for halfday
                {
                    if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveInfo"].Value) == "1")
                    {
                        if (Convert.ToDouble(sWorkTime) >= (intMinWorkHrsInMinutes / 2))
                        {

                            if (blnIsMinWrkHRsAsOT == true)// Then 'Checking whether calculation is based on minimum working hour 
                            {
                                int ihalfday = (intMinWorkHrsInMinutes / 2);
                                int addOt = Convert.ToInt32(sWorkTime) - ihalfday;
                                sWorkTime = ihalfday.ToString();
                                sOt = Convert.ToString(Convert.ToInt32(sOt) + addOt);
                            }
                        }
                        if (intDurationInMinutes > 0)
                            intDurationInMinutes = intDurationInMinutes / 2;
                        if (intMinWorkHrsInMinutes > 0)
                            intMinWorkHrsInMinutes = intMinWorkHrsInMinutes / 2;
                    }
                }
                else
                {

                    if (blnIsMinWrkHRsAsOT == true)
                    {
                        if (Convert.ToDouble(sWorkTime) == Convert.ToDouble(intMinWorkHrsInMinutes))
                        { }//'
                        else if (Convert.ToDouble(sWorkTime) < Convert.ToDouble(intMinWorkHrsInMinutes))
                        { //'sOt = "0"
                            if (intDurationInMinutes > intMinWorkHrsInMinutes)
                            { sOt = "0"; }
                            else// 'duration and minWorkhours are same then Consider Convert.ToInt32(sAllowedBreak) + iEarly + iLate)
                            {
                                if (Convert.ToInt32(sWorkTime) + Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate < intMinWorkHrsInMinutes)
                                { sOt = "0"; }
                            }
                        }
                        else if (Convert.ToDouble(sWorkTime) > Convert.ToDouble(intMinWorkHrsInMinutes))
                        {
                            int Ot = Convert.ToInt32(sWorkTime) - Convert.ToInt32(intMinWorkHrsInMinutes);
                            sOt = Convert.ToString(Convert.ToInt32(sOt) + Ot);
                            sWorkTime = intMinWorkHrsInMinutes.ToString();
                        }
                    }

                }
                if (Convert.ToInt32(sOt) < iMinOtMinutes)
                    sOt = "0";
                if (sOt != "" && sOt != "0")//Ot Template
                    sOt = GetOt(sOt);

                bool blnIsHoliday = false;
                blnIsHoliday = MobjclsBLLAttendance.IsHoliday(iEmpId, iCmpId, atnDate, iDayID); //'cheking current date is holiday

                if (iShiftType <= 3)
                {

                    if (iAdditionalMinutes > 0)
                        intMinWorkHrsInMinutes = intMinWorkHrsInMinutes - iAdditionalMinutes;

                    if (intDurationInMinutes > intMinWorkHrsInMinutes)
                    {
                        iAbsentTime = intMinWorkHrsInMinutes - Convert.ToInt32(sWorkTime);
                    }
                    else
                    {
                        iAbsentTime = intMinWorkHrsInMinutes - (Convert.ToInt32(sWorkTime) + Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate);
                    }
                }

                int iShortage = 0;
                if (iShiftType == 2 || iShiftType == 4)
                { }
                else
                {
                    if (blnIsHoliday == false)
                    {
                        iShortage = Convert.ToInt32(sBreakTime == "0" ? "0" : sBreakTime) - Convert.ToInt32(sAllowedBreakTime);
                        if (iShortage > iAbsentTime)
                        {
                            iAbsentTime = iShortage;
                        }
                    }
                }
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvAbsentTime"].Value = iAbsentTime > 0 ? iAbsentTime : 0;

                int iAbsent = iAbsentTime > 0 ? iAbsentTime : 0;
                //lblShortageTime.ForeColor = iAbsent > 0 ? Color.DarkRed : System.Drawing.SystemColors.ControlText;
                string sShortage = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iAbsent), DateTime.Today));
                //lblShortageTime.Text = Convert.ToDateTime(sShortage).ToString("HH:mm:ss");
                //------------------------------------

                //lblAllowedBreakTime.Text = Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Value);

                DgvAttendance.Rows[iRowIndex].Cells["ColDgvWorkTime"].Tag = Convert.ToDouble(sWorkTime);
                sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
                sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvWorkTime"].Value = sWorkTime;
                //lblWorkTime.Text = sWorkTime;
                if (sBreakTime != "" && sBreakTime != "0")
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Tag = sBreakTime;
                    sBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sBreakTime), DateTime.Today));
                    sBreakTime = Convert.ToDateTime(sBreakTime).ToString("HH:mm:ss");
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Value = sBreakTime;
                    //lblBreakTime.Text = sBreakTime;
                }
                else
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Tag = 0;
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Value = "";
                    //lblBreakTime.Text = "00:00:00";
                }

                if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvWorkTime"].Value) != "" && Convert.ToDouble(sOt) > 0)
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Tag = sOt;
                    sOt = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sOt), DateTime.Today));
                    sOt = Convert.ToDateTime(sOt).ToString("HH:mm:ss");
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Value = sOt;
                    //lblExcessTime.Text = sOt;
                }
                else
                {
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Tag = 0;
                    DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Value = "";
                    //lblExcessTime.Text = "00:00:00";
                }

                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }

        }
        private bool WorktimeCalculationForSplitShift(int iShiftID, int k, int iRowIndex, string sFromTime, string sToTime, int iLate, int iEarly,
                                                      bool bLate, bool bEarly, string sEmpName, bool isAfterMinWrkHrsAsOT, bool blnConsiderBufferForOT, int iBufferForOT,
                                                      ref string sWorkTime, ref  string sBreakTime, ref string sOt, ref int intTotalSplitAbsentTm, ref int intTotalAbsentTm)
        {//'Worktime Calculation for Split shift 
            try
            {
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvShiftOrderNo"].Value = 0;//Dynamic Shift
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                //lblLateComing.Visible = false;
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                //lblEarlyGoing.Visible = false;

                sWorkTime = "0";
                sBreakTime = "0";
                sOt = "0";
                string sOvertime = "0";
                bool nightFlag = false;

                int intWorkTm = 0;
                int intBreakTm = 0;
                int intAbsentTm = 0;
                int intSplitAbsentTm = 0;
                intTotalSplitAbsentTm = 0;
                intTotalAbsentTm = 0;

                DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftID);

                if (DtShiftDetails.Rows.Count > 0)
                {
                    for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                    {

                        string sSplitFromTime = Convert.ToString(DtShiftDetails.Rows[i]["FromTime"]);
                        string sSplitToTime = Convert.ToString(DtShiftDetails.Rows[i]["ToTime"]);
                        string sDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                        int iMinWorkingHrsMin = GetDurationInMinutes(sDuration);
                        //'Modified for Split Shift
                        if (nightFlag == true)
                        {
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).AddDays(1).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }

                        int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime).AddMinutes(iMinWorkingHrsMin), Convert.ToDateTime(sSplitToTime));
                        if (tmDurationDiff < 0)
                        {
                            nightFlag = true;
                            sSplitFromTime = Convert.ToDateTime(sSplitFromTime).ToString();
                            sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                        }

                        string sPreviousTime = "";
                        bool bShiftChangeTime = true;
                        bool bShiftChangeOvertime = true;
                        bool bShiftChangeTimeBrkOT = true;
                        bool bShiftChangeTimeEarlyWRk = true;
                        bool bShiftChangeTimeEarlyBRK = true;
                        string sOTBreakTime = "0";

                        for (int j = 3; j <= k + 2; j++)
                        {
                            string dResult = Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value);

                            string sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm");

                            if (j == 3)// Then ''first time
                            {
                                if (bLate)
                                {
                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate)) //'bFlexi = False
                                    {
                                        int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                        if (il > 0)
                                        {
                                            string sLate = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today).ToString();
                                            DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                        }
                                        //lblLateComing.Tag = 1;
                                        //lblLateComing.Visible = true;
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes
                                    }
                                    else
                                    {
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                                        //lblLateComing.Tag = 0;
                                        //lblLateComing.Visible = false;
                                    }
                                }
                            }
                            else
                            {
                                int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));
                                //' If tmDiff < 0 Then tmDiff = DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1))
                                if (tmDiff < 0)//Then 'if date diff less than Zero then adding day to the StimeValue
                                {
                                    tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                    sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                                }
                                //'------------------------------------------------------------------------------------------------
                                //' workTime Time Calculation-
                                if (j % 2 == 0) //Then 'Mode=0 for workTime
                                { //'-------------------------------------Early Going validating 
                                    if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly) && bEarly == true)//Then 'bFlexi = False
                                    {
                                        int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                        if (iE > 0)
                                        {
                                            string sEarly = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today).ToString();
                                            DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                        }

                                        //lblEarlyGoing.Visible = true;
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes
                                    } //'-------------------------------------Early Going 
                                    //'---------------------------------------------Split Over Time-------------------------------------------

                                    if (i == DtShiftDetails.Rows.Count - 1)
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                        {
                                            if (bShiftChangeOvertime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                            {
                                                bShiftChangeOvertime = false;
                                                int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                {
                                                    sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + idiffCurtShiftOT);
                                                }
                                            }
                                            else
                                            {
                                                if (i == DtShiftDetails.Rows.Count - 1)
                                                {
                                                    sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + Convert.ToDouble(tmDiff));//'WorkTime Calculation 
                                                }
                                            }
                                        }
                                    }
                                    //'--------------------------------------------------------------------------------------------------------
                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                    { //' DgvAttendanceFetch.Rows(iRowIndex).Cells("EarlyGoing").Value = 0
                                        if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                        {
                                            bShiftChangeTime = false;
                                            int idiffPrevShitWork = 0;
                                            if (Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime))// 'modified 28 dec
                                            {
                                                idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sSplitToTime));
                                            }
                                            else
                                            {
                                                idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));
                                            }
                                            int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));
                                            if (i == DtShiftDetails.Rows.Count - 1)
                                            {
                                                sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                            }
                                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                        }
                                        else
                                        {
                                            if (i == DtShiftDetails.Rows.Count - 1)
                                            {
                                                sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //'Modification -----------------------------12-11-2010
                                        bool bflag = false;
                                        if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bflag = true;
                                        } //'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                        if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bShiftChangeTimeEarlyWRk = false;
                                            int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                            bflag = true;
                                        }
                                        if (bflag == false)
                                        {
                                            sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));
                                        }
                                        //'Modification -----------------------------End 12-11-2010
                                    }
                                    //'---------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                }
                                else //'Break Time Calculation-where mode=1
                                {
                                    if (i == DtShiftDetails.Rows.Count - 1)
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                        {
                                            if (bEarly)
                                            {

                                                //lblEarlyGoing.Visible = false;
                                                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                                DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                            }
                                        }
                                    }

                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                    {
                                        if (bShiftChangeTimeBrkOT == true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                        {
                                            bShiftChangeTimeBrkOT = false;
                                            int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));//'break time= before ToTime- Early giong minutes
                                            int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));// 'Ot Break time = after To time
                                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));//;'Break Time Calculation
                                            sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                        }
                                        else
                                        {
                                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime)) { sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff)); } //'Break Time Calculation Ot
                                        }
                                    }
                                    else
                                    {
                                        //'Modification -----------------------------12-11-2010
                                        bool bflag = false;
                                        if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bflag = true;
                                        } //'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                        if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                        {
                                            bShiftChangeTimeEarlyBRK = false;
                                            int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                            bflag = true;
                                        }
                                        if (bflag == false)
                                        {
                                            sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                        }


                                        //lblEarlyGoing.Tag = 0;
                                        //lblEarlyGoing.Visible = false;
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                        DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";

                                    }
                                }
                            }
                            sPreviousTime = sTimeValue;
                            if (Glbln24HrFormat == false)
                            {
                                sTimeValue = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                                DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                            }
                            else
                            {
                                DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("HH:mm");
                            }

                        } //'For j = 3 To k + 2 'Time selecting filter wit date and id 

                        //'------------------------------------Shoratge Calculation starts----------------------------------
                        intSplitAbsentTm = 0;

                        intAbsentTm = 0;
                        if (intWorkTm == Convert.ToInt32(sWorkTime) && intBreakTm == Convert.ToInt32(sBreakTime))
                        { intAbsentTm = iMinWorkingHrsMin; }
                        else
                        {
                            intSplitAbsentTm = iMinWorkingHrsMin - (Convert.ToInt32(sWorkTime) - intWorkTm);
                            if (intSplitAbsentTm > 0)
                            { intTotalSplitAbsentTm += intSplitAbsentTm; }
                        }
                        if (intAbsentTm > 0)
                        { intTotalAbsentTm += intAbsentTm; }


                        intWorkTm = Convert.ToInt32(sWorkTime);
                        intBreakTm = Convert.ToInt32(sBreakTime);
                        //'------------------------------------Shoratge Calculation Ends------------------------------------

                    } //'For i As Integer = 0 To DtShiftDetails.Rows.Count - 1

                    if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave) || DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1)
                    {
                        intTotalSplitAbsentTm = 0;
                        intTotalAbsentTm = 0;
                    }
                    if (isAfterMinWrkHrsAsOT == false)
                    {
                        sOt = sOvertime;
                    }
                } //'  If DtShiftDetails.Rows.Count > 0

                return true;
            }

            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on WorktimeCalculationForSplitShift:Attendance auto." + Ex.Message.ToString() + "", 3);
                return false;
            }
        }
        private void AddNewColumns(int iColCounts)
        {
            try
            {

                if (MintTimingsColumnCnt >= 2)
                {
                    DgvAttendance.Columns["ColDgvAddMore"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    DgvAttendance.Columns["ColDgvAddMore"].Width = 35;
                    DgvAttendance.Columns["ColDgvAddMore"].MinimumWidth = 35;
                    int cnt = MintTimingsColumnCnt + 2;

                    cnt = Convert.ToInt32(Math.Floor(Convert.ToDecimal((cnt + 1) / 2)));
                    DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                    DataGridViewTextBoxColumn adcolOut = new DataGridViewTextBoxColumn();

                    adcolIn.Name = "TimeIn" + cnt;
                    adcolIn.HeaderText = "TimeIn" + cnt;
                    adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolIn.Resizable = DataGridViewTriState.False;
                    adcolIn.Width = 70;
                    adcolIn.MaxInputLength = 9;

                    adcolOut.Name = "TimeOut" + cnt;
                    adcolOut.HeaderText = "TimeOut" + cnt;
                    adcolOut.SortMode = DataGridViewColumnSortMode.NotSortable;
                    adcolOut.Resizable = DataGridViewTriState.False;
                    adcolOut.Width = 70;
                    adcolOut.MaxInputLength = 9;

                    DgvAttendance.Columns.Insert(MintTimingsColumnCnt + 3, adcolIn);
                    DgvAttendance.Columns.Insert(MintTimingsColumnCnt + 4, adcolOut);
                    MintTimingsColumnCnt += 2;
                    DgvAttendance.CurrentCell = DgvAttendance[DgvAttendance.CurrentCell.ColumnIndex - iColCounts, DgvAttendance.CurrentCell.RowIndex];
                }
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
        }

        private void ClearBottomPanel()
        {

            //lblAllowedBreakTime.Text = "00:00:00";
            //lblBreakTime.Text = "00:00:00";
            //lblWorkTime.Text = "00:00:00";
            //lblShortageTime.Text = "00:00:00";
            //lblShortageTime.ForeColor = System.Drawing.SystemColors.ControlText;
            //lblExcessTime.Text = "00:00:00";
            //lblShiftName.Text = "";
            //lblConsequence.Text = "";
            //lblConsequence.Visible = false;
            //lblEarlyGoing.Visible = false;
            //lblLateComing.Visible = false;

        }

        private void DgvAttendance_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                string date2 = "";
                DateTime ConvertTime;
                if (e.RowIndex != -1)
                {


                    //if (e.ColumnIndex ==  ColDgvStatus.Index)
                    //{
                    //    if (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value.ToInt32() != (int)AttendanceStatus.Leave)
                    //    {
                    //        DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
                    //        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvValidFlag"].Value = 1;
                    //        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvRemarks"].Value = "";
                    //        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Value = "";
                    //        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveInfo"].Value = "";
                    //        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Tag = "";
                    //        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveFlag"].Tag = 0;
                    //        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveFlag"].Value = 0;
                    //        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvHolidayFlag"].Value = 0;
                    //        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvHolidayFlag"].Tag = 0;
                    //    }
                    //}


                    if (e.ColumnIndex > ColDgvStatus.Index && e.ColumnIndex <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1)
                    {
                        if (Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value) == "" || DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
                            return;
                        else
                        {

                            if (Glbln24HrFormat == false)
                            {
                                DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = AMPMConvert(Microsoft.VisualBasic.Strings.UCase(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()));
                            }
                            else
                            {
                                DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Convert24HrFormat(Microsoft.VisualBasic.Strings.UCase(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()));

                            }
                            date2 = DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                        }
                        if (IsValidTime(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) == false)
                        {
                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1556, out  MmessageIcon);
                            if (Glbln24HrFormat == false)
                            {
                                //message
                                string strSecondMsg = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1557, out  MmessageIcon);
                                MstrMessCommon = MstrMessCommon + strSecondMsg.Remove(0, strSecondMsg.IndexOf("#") + 1).Trim();
                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                //tmrClearlabels.Enabled = true;

                            }
                            else
                            {

                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                //tmrClearlabels.Enabled = true;

                            }
                            DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                            e.Cancel = true;
                            return;
                        }
                        else
                        {
                            ConvertTime = Convert.ToDateTime(date2);
                        }

                        if (IsValidTime(DgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) == true)
                        {
                            string STimeIn = DgvAttendance.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
                            if (Glbln24HrFormat == false)
                            {
                                STimeIn = ConvertDate24New(STimeIn);

                            }
                        }
                        string CurrentDateDisplay = "";
                        string FirstOne = "";
                        //MblnCorrectTime = true;

                        if (e.ColumnIndex >= 3 && e.ColumnIndex <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1)
                        {
                            //'CurrentDateDisplay = DTPDate.Value.ToString("dd-MMM-yyyy")
                            CurrentDateDisplay = Microsoft.VisualBasic.DateAndTime.Now.Date.ToString("dd MMM yyyy");
                            if (DgvAttendance.Rows[e.RowIndex].Cells[3].Value != null)
                            {
                                if (Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[3].Value) != "")
                                {
                                    FirstOne = CurrentDateDisplay.Trim() + " " + DgvAttendance.Rows[e.RowIndex].Cells[3].FormattedValue.ToString();
                                }
                            }

                            string strStart = FirstOne;
                            string strLast = Convert.ToDateTime(strStart).AddDays(1).ToString();
                            string DayChange = "";
                            bool flag = false;
                            string strCurrent = "";
                            string sPreviousTime = "";
                            int iDiff = 0;

                            for (int k = 3; k <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; k++)
                            {
                                if (Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[k].Value) == "")
                                {
                                    break;
                                }
                                if (k > 3)
                                {
                                    if (flag == true)
                                    {
                                        strCurrent = DayChange + " " + DgvAttendance.Rows[e.RowIndex].Cells[k].FormattedValue;
                                        iDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(strCurrent));
                                        if (iDiff < 0)
                                        {
                                            strCurrent = Convert.ToDateTime(strCurrent).AddDays(1).ToString();
                                            DayChange = Strings.Format(Convert.ToDateTime(strCurrent), "dd MMM yyyy");
                                            if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(strStart), Convert.ToDateTime(strCurrent)) >= 1440)
                                            {
                                                //MblnCorrectTime = false;
                                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1562, out MmessageIcon);
                                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                //tmrClearlabels.Enabled = true;
                                                e.Cancel = true;
                                                return;
                                            }
                                        }
                                        if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(strStart), Convert.ToDateTime(strCurrent)) >= 1440)
                                        {
                                            //MblnCorrectTime = false;
                                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1562, out MmessageIcon);
                                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                            //tmrClearlabels.Enabled = true;
                                            e.Cancel = true;
                                            return;
                                        }
                                    }
                                    else
                                    {

                                        strCurrent = Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[k].FormattedValue);
                                        iDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(strCurrent));
                                        if (iDiff < 0)
                                        {
                                            strCurrent = Convert.ToDateTime(strCurrent).AddDays(1).ToString();
                                            DayChange = Strings.Format(Convert.ToDateTime(strCurrent), "dd MMM yyyy");
                                            flag = true;
                                            if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(strStart), Convert.ToDateTime(strCurrent)) >= 1440)
                                            {
                                                //MblnCorrectTime = false;
                                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1562, out MmessageIcon);
                                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                //tmrClearlabels.Enabled = true;
                                                e.Cancel = true;
                                                return;
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    strCurrent = Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells[3].Value);
                                }
                                sPreviousTime = strCurrent;
                            }

                        }//(e.ColumnIndex >= 3 )
                    }

                }//rowindex =-1

            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }
        private string ConvertDate24New(string dateString)
        {
            try
            {
                DTPFormatTime.CustomFormat = "HH:mm";
                DTPFormatTime.Text = dateString;
                return DTPFormatTime.Text.ToString();
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
                return DTPFormatTime.Text.ToString();
            }

        }
        private bool IsValidTime(string sTime)
        {
            try
            {
                //'Sample  Format  "13:14"
                //DateTime TheDate ;
                //TheDate = sTime;
                string Format1 = "^*(1[0-2]|[1-9]):[0-5][0-9]*(a|p|A|P)(m|M)*$";
                System.Text.RegularExpressions.Regex TryValidateFormat1 = new System.Text.RegularExpressions.Regex(Format1);
                string Format2 = "([0-1][0-9]|2[0-3]):([0-5][0-9])";
                System.Text.RegularExpressions.Regex TryValidateFormat2 = new System.Text.RegularExpressions.Regex(Format2);
                return TryValidateFormat2.IsMatch(sTime) || TryValidateFormat1.IsMatch(sTime);
            }
            catch (Exception)
            {

                return false;
            }
        }
        private string AMPMConvert(string StrValue)
        {

            try
            {
                StrValue = FormatString(StrValue);
                if (StrValue.IndexOf(".") != -1)
                    StrValue = StrValue.Replace('.', ':');

                if (Microsoft.VisualBasic.Information.IsNumeric(StrValue))
                {
                    if ((StrValue).Length == 1)
                    {
                        if (Convert.ToInt32(StrValue) < 12)
                            StrValue = StrValue + "A";
                        else
                            StrValue = StrValue + "P";

                    }
                    else if (StrValue.Length == 2)
                    {
                        if (Convert.ToInt32(StrValue) < 12)
                            StrValue = StrValue + "A";
                        else
                            StrValue = StrValue + "P";

                    }
                }

                if (StrValue.IndexOf(":") != -1)
                {
                    string LeftTime = "";
                    string RightTime = "";
                    string[] strar = new string[2];

                    strar = StrValue.Split(':');
                    LeftTime = strar[0];
                    RightTime = strar[1];
                    if (Microsoft.VisualBasic.Information.IsNumeric(RightTime))
                    {
                        if (RightTime.Length > 2)
                            return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                    }
                }


                DateTime CForDate;
                if (StrValue.IndexOf("AM") != -1)
                {

                    StrValue = StrValue.Substring(0, StrValue.IndexOf("M") + 1);

                    CForDate = Convert.ToDateTime(StrValue.Replace("AM", "AM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    // return AMPMConvert("");
                }
                else if (StrValue.IndexOf("A") != -1)
                {
                    StrValue = StrValue.Substring(0, StrValue.IndexOf("A") + 1);
                    CForDate = Convert.ToDateTime(StrValue.Replace("A", "AM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    //Return AMPMConvert
                }
                else if (StrValue.IndexOf("PM") != -1)
                {
                    StrValue = StrValue.Substring(0, StrValue.IndexOf("M") + 1);
                    CForDate = Convert.ToDateTime(StrValue.Replace("PM", "PM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    //Return AMPMConvert
                }
                else if (StrValue.IndexOf("P") != -1)
                {

                    StrValue = StrValue.Substring(0, StrValue.IndexOf("P") + 1);
                    CForDate = Convert.ToDateTime(StrValue.Replace("P", "PM"));
                    if (Glbln24HrFormat == false)
                        return CForDate.ToString("hh:mm tt").Trim();
                    else
                        return CForDate.ToString("HH:mm").Trim();

                    //Return AMPMConvert
                }
                else
                {
                    if (StrValue.Length > 3)
                        return Microsoft.VisualBasic.Strings.Left(StrValue, 2);

                    return StrValue;
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return StrValue;
            }

        }


        private bool SaveAttendanceInfo()
        {
            bool rtnValue = false;
            int EmployeeID = 0;
            if (DgvAttendance.CurrentCell != null)
            {

                try
                {
                    DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    DgvAttendance.CurrentCell = DgvAttendance[ColDgvEmployee.Index, DgvAttendance.CurrentCell.RowIndex];
                }
                catch
                {
                    return false;
                }
            }

            try
            {

                this.Cursor = Cursors.WaitCursor;
                if (DgvAttendance.RowCount > 0)
                {

                    if (FormValidation())
                    {
                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1601, out  MmessageIcon);
                        if (MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        {
                            this.Cursor = Cursors.Default;
                            return false;
                        }

                        if (DeleteAttendenceDetails() == false)
                        {
                            this.Cursor = Cursors.Default;
                            return false;
                        }
                        DgvAttendance.Focus();
                        barProgressBarAttendance.Visible = true;
                        barProgressBarAttendance.Minimum = 0;
                        barProgressBarAttendance.Maximum = DgvAttendance.Rows.Count + 1;

                        int iCount = 0;
                        DataTable DtEmployee = new DataTable();// For Absent Marking
                        DtEmployee.Columns.Add("EmployeeID");
                        DtEmployee.Columns.Add("Date");
                        DtEmployee.Columns.Add("Month");
                        string[] sRows = new string[3];

                        for (int i = 0; i <= DgvAttendance.Rows.Count - 1; i++)
                        {
                            rtnValue = false;

                            if (ClsCommonSettings.AttendanceAutofillForSinglePunch && (DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value.ToInt32() != Convert.ToInt32(AttendanceStatus.Leave)) && (DgvAttendance.Rows[i].Cells["TimeIn1"].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[i].Cells["TimeOut1"].Value.ToStringCustom() == string.Empty))
                            {
                                //option to autofill the row if only the first column is entered 
                                fillAttendanceRow(i);
                                DgvAttendance.Rows[i].DefaultCellStyle.ForeColor = Color.Black;
                                DgvAttendance.Rows[i].Cells["ColDgvValidFlag"].Value = 1;
                            }

                            if (DgvAttendance.Rows[i].DefaultCellStyle.ForeColor == Color.IndianRed)
                                continue;



                            int iStatus = 0;
                            if (DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value != DBNull.Value && DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value != null && Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value))
                                iStatus = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value);

                            if (iStatus == Convert.ToInt32(AttendanceStatus.Present) && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvValidFlag"].Value) == 0)
                                continue;
                            else if (iStatus == Convert.ToInt32(AttendanceStatus.Present) && (DgvAttendance.Rows[i].Cells[ColDgvStatus.Index + 1].Value == DBNull.Value || DgvAttendance.Rows[i].Cells[ColDgvStatus.Index + 1].Value == null || Convert.ToString(DgvAttendance.Rows[i].Cells[ColDgvStatus.Index + 1].Value) == ""))
                                continue;
                            else if (iStatus == 0)
                            { continue; }
                            else if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
                            {
                                if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveType"].Value) <= 0)
                                    continue;

                            }
                            DataTable datLeaveRemarks = MobjclsBLLAttendance.GetLeaveRemarks(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime());
                            if (datLeaveRemarks.Rows.Count > 0)
                            {
                                if (datLeaveRemarks.Rows[0]["HalfDay"].ToInt32() == 0)//if leave from leave entry and full day leave
                                {
                                    continue;
                                }
                                else
                                {
                                    if (DgvAttendance.Rows[i].Cells["TimeIn1"].Value.ToStringCustom() == string.Empty || DgvAttendance.Rows[i].Cells["TimeOut1"].Value.ToStringCustom() == string.Empty)
                                    {
                                        continue;
                                    }
                                }
                            }
                            MobjclsBLLAttendance.ClearAllPools();// Bulk Data So conncettion pools are cleared
                            string sCurr = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value).ToString("dd MMM yyyy");
                            string sNow = Strings.Format(Microsoft.VisualBasic.DateAndTime.Now.Date, "dd MMM yyyy");
                            string sworkID = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvPunchID"].Value).TrimEnd().TrimStart();
                            string sDate = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value).ToString("dd MMM yyyy");
                            int iCompanyID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
                            int iEmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
                            int iShiftID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag);

                            if (iEmpID <= 0 || iShiftID <= 0)
                                continue;
                            string sJoiningDate = MobjclsBLLAttendance.GetJoiningDate(iEmpID);
                            DateTime dtJoiningDate = Convert.ToDateTime(sJoiningDate);
                            //'Cheking With Current Date
                            ////////if (Convert.ToDateTime(sCurr) > Convert.ToDateTime(sNow))
                            ////////    continue;
                            if (Convert.ToDateTime(sCurr).Date < dtJoiningDate.Date)
                                continue;

                            if (MobjclsBLLAttendance.IsEmployeeInService(iEmpID) == false)
                                continue;
                            if (MobjclsBLLAttendance.IsSalaryReleased(iEmpID, iCompanyID, Convert.ToDateTime(sCurr).Date) == true)
                                continue;

                            if (FillParameterAtnMaster(i) == false)
                                continue;

                            rtnValue = MobjclsBLLAttendance.SaveAttendanceInfo();

                            if (rtnValue == true)
                            {
                                EmployeeID = iEmpID;
                                iCount += 1;
                                sRows[0] = iEmpID.ToString();
                                sRows[1] = sDate;
                                int iMonth = Convert.ToDateTime(sDate).Month;
                                string smonthname = Microsoft.VisualBasic.DateAndTime.MonthName(iMonth, true);
                                string smonth = smonthname + " " + Convert.ToDateTime(sDate).Year.ToString();
                                sRows[2] = smonth;
                                DtEmployee.Rows.Add(sRows);
                                barProgressBarAttendance.Value += 1;
                                ////////////////////////////CHECKING REQUIRED
                                //if (!rbtnMonthlyIndividual.Checked)
                                //{
                                MobjclsBLLAttendance.UpdateAbsent(EmployeeID, Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value).ToString("dd MMM yyyy"), 0);
                                //}

                            }


                        }//for

                        if (iCount > 0)
                        {
                             
                            // Modified By Laxmi for Attendance summary updation
                            MobjclsBLLAttendance.SaveAttendanceSummaryConsequence(EmployeeID, dtpMonthSelect.Value.Month, dtpMonthSelect.Value.Year);


                            if (DgvAttendance.Rows.Count > 0)
                            {
                                SetLeaveEntryFromConsequence(EmployeeID);
                            }

                            //
                            //if (rbtnMonthlyIndividual.Checked)
                            //{
                            //    MobjclsBLLAttendance.UpdateAbsent(EmployeeID, dtpFromDate.Value.ToString("dd-MMM-yyyy"), 1);
                            //}WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWww
                            if (DtEmployee.Rows.Count > 0)
                            {
                                Absentmarking(DtEmployee);//Absent Marking
                            }


                            rtnValue = true;
                        }
                        else
                        {
                            barProgressBarAttendance.Visible = false;
                        }

                        barProgressBarAttendance.Value = DgvAttendance.Rows.Count + 1;
                    }
                }//If rowcount >0
                else
                {
                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1602, out  MmessageIcon);
                    MessageBox.Show(MstrMessCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
            catch (Exception Ex)
            {

                this.Cursor = Cursors.Default;

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
            this.Cursor = Cursors.Default;
            return rtnValue;
        }


        private bool Absentmarking(DataTable dtEmployee)
        {
            var drID = (from r in dtEmployee.AsEnumerable() select r["EmployeeID"]).Distinct();
            var drDate = (from r in dtEmployee.AsEnumerable() select r["Month"]).Distinct();

            try
            {
                foreach (string sEmpID in drID)
                {
                    foreach (string sDate in drDate)
                    {
                        string sFilterExpression = "EmployeeID='" + sEmpID + "' and Month='" + sDate + "'";
                        DataRow[] datarow = dtEmployee.Select(sFilterExpression);
                        dtEmployee.DefaultView.RowFilter = sFilterExpression;
                        DataTable dt = dtEmployee.DefaultView.ToTable().Copy();
    
                            var drSDate = (from r in dt.AsEnumerable() select r["Date"]).Distinct();
                            foreach (string sNewDate in drSDate)
                            {

                                string sStartdate = Convert.ToDateTime(sNewDate).ToString("dd MMM yyyy");
                                string sEndDate = sStartdate;
                                DataTable dtAbsentdays = MobjclsBLLAttendance.GetAbsentDays(sStartdate, sEndDate, Convert.ToInt32(sEmpID));
                                if (dtAbsentdays.Rows.Count > 0)
                                {
                                    for (int l = 0; l <= dtAbsentdays.Rows.Count - 1; l++)
                                    {
                                        int iShiftID = 0;
                                        int iPolicyId = 0;
                                        int IcomID = 0;
                                        string strDate = Convert.ToDateTime(dtAbsentdays.Rows[l][0]).ToString("dd MMM yyyy");
                                        DateTime dtDates = Convert.ToDateTime(strDate);
                                        int iDayId = Convert.ToInt32(dtDates.DayOfWeek) + 2 > 7 ? 1 : Convert.ToInt32(dtDates.DayOfWeek) + 2;
                                        string sDuration = "0";
                                        string sMinWorkHrs = "0";
                                        int iShiftType = 0;
                                        string sAllowedBreak = "0";
                                        int intAbsentTime = 0;
                                        MobjclsBLLAttendance.GetEmployeeInfoForAbsentMarking(Convert.ToInt32(sEmpID), iDayId, strDate, ref iShiftID, ref iPolicyId, ref IcomID, ref sDuration,
                                                                                               ref sMinWorkHrs, ref iShiftType, ref sAllowedBreak);

                                        intAbsentTime = GetDurationInMinutes(sMinWorkHrs);
                                        MobjclsBLLAttendance.AbsentSaving(Convert.ToInt32(sEmpID), strDate, iShiftID, 0, intAbsentTime, iPolicyId, IcomID);//' Absent Marking

                                    }//for  l
                                }//if(dtAbsentdays
                            }//for j sMonth
                        //}
                    } //For k sDate
                } //For (i) SID
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString());
                return false;
            }
        }

        private void SetLeaveEntryFromConsequence(int EmployeeID)
        {
            int LateAllowedDays = 0;
            int EarlyAllowedDays = 0;
            int iCmpID = Convert.ToInt32(DgvAttendance.Rows[0].Cells["ColDgvCompanyID"].Value);
            int Late = 0;
            int Early = 0;

            DateTime dtAtnDate = Convert.ToDateTime(DgvAttendance.Rows[0].Cells["ColDgvAtnDate"].Value);//.ToString("dd MMM yyyy")
            DataTable dtConsequence = MobjclsBLLAttendance.CheckConsequence(Convert.ToInt32(DgvAttendance.Rows[0].Cells["ColDgvEmployee"].Tag), dtAtnDate.Month, dtAtnDate.Year);

            if (dtConsequence.Rows.Count > 0)
            {
                LateAllowedDays = Convert.ToInt32(dtConsequence.Rows[0]["AllowdLateCt"]);
                EarlyAllowedDays = Convert.ToInt32(dtConsequence.Rows[0]["AllowedEarlyCt"]);
            }

            if (Convert.ToInt32(dtConsequence.Rows[0]["LeaveCount"]) > 0)
            {
                foreach (DataRow Row in dtConsequence.Rows)
                {
                    dtAtnDate = Convert.ToDateTime(Row["Date"]);

                    if (Late < LateAllowedDays)
                    {
                        if (Convert.ToInt32(Row["IsLate"]) == 0 && Row["LeaveID"].ToInt32() > 0)
                        {
                            DateTime dtDate = Convert.ToDateTime(Row["Date"]);

                            MobjclsBLLAttendance.DeleteLeaveEntry(EmployeeID, iCmpID, dtDate);
                            Late++;
                        }

                    }
                    if (Early < EarlyAllowedDays)
                    {
                        if (Convert.ToInt32(Row["IsEarly"]) == 0 && Row["LeaveID"].ToInt32() > 0)
                        {
                            DateTime dtDate = Convert.ToDateTime(Row["Date"]);

                            MobjclsBLLAttendance.DeleteLeaveEntry(EmployeeID, iCmpID, dtDate);
                            Early++;

                        }
                    }
                }
            }
        }  
        private bool FormValidation()
        {

            if (DgvAttendance.RowCount <= 0)
            {

                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1551, out  MmessageIcon);
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }


            if (CheckTimeInTimeOut() == false)
            {
                return false;
            }

            return true;
        }
        private bool CheckTimeInTimeOut()
        {

            DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);

            foreach (DataGridViewRow DgvRow in DgvAttendance.Rows)
            {

                for (int iCounter = ColDgvStatus.Index; iCounter <= MintTimingsColumnCnt; iCounter += 2)
                {
                    int intLoc = iCounter;
                    int intCnt = iCounter / 2;
                    string strCount = intCnt.ToString().Trim();
                    string strTimeInColName = "TimeIn" + strCount; ;
                    string strTimeInColOutName = "TimeOut" + strCount;
                    if (DgvAttendance.Columns.Contains(strTimeInColOutName))
                    {
                        if (DgvRow.Cells[strTimeInColOutName].Value.ToStringCustom() != string.Empty)
                        {
                            try
                            {
                                if (Glbln24HrFormat == false)
                                {
                                    DgvRow.Cells[strTimeInColOutName].Value = AMPMConvert(Microsoft.VisualBasic.Strings.UCase(DgvRow.Cells[strTimeInColOutName].Value.ToStringCustom()));
                                }
                                else
                                {
                                    DgvRow.Cells[strTimeInColOutName].Value = Convert24HrFormat(Microsoft.VisualBasic.Strings.UCase(DgvRow.Cells[strTimeInColOutName].Value.ToStringCustom()));
                                }
                            }
                            catch
                            {
                            }

                            if (IsValidTime(DgvRow.Cells[strTimeInColOutName].Value.ToString()) == false)
                            {
                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1556, out  MmessageIcon);
                                if (Glbln24HrFormat == false)
                                {
                                    string strSecondMsg = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1557, out  MmessageIcon);
                                    MstrMessCommon = MstrMessCommon + strSecondMsg.Remove(0, strSecondMsg.IndexOf("#") + 1).Trim();
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }

                                DgvAttendance.CurrentCell = DgvAttendance[strTimeInColOutName, DgvRow.Index];
                                return false;
                            }
                        }
                    }
                    if (DgvAttendance.Columns.Contains(strTimeInColName))
                    {
                        if (DgvRow.Cells[strTimeInColName].Value.ToStringCustom() != string.Empty)
                        {
                            try
                            {
                                if (Glbln24HrFormat == false)
                                {
                                    DgvRow.Cells[strTimeInColName].Value = AMPMConvert(Microsoft.VisualBasic.Strings.UCase(DgvRow.Cells[strTimeInColName].Value.ToStringCustom()));
                                }
                                else
                                {
                                    DgvRow.Cells[strTimeInColName].Value = Convert24HrFormat(Microsoft.VisualBasic.Strings.UCase(DgvRow.Cells[strTimeInColName].Value.ToStringCustom()));

                                }
                            }
                            catch
                            {
                            }
                            if (IsValidTime(DgvRow.Cells[strTimeInColName].Value.ToString()) == false)
                            {
                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1556, out  MmessageIcon);
                                if (Glbln24HrFormat == false)
                                {
                                    string strSecondMsg = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1557, out  MmessageIcon);
                                    MstrMessCommon = MstrMessCommon + strSecondMsg.Remove(0, strSecondMsg.IndexOf("#") + 1).Trim();
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }

                                DgvAttendance.CurrentCell = DgvAttendance[strTimeInColName, DgvRow.Index];
                                return false;
                            }

                        }
                    }
                }//For icounter
            }//For Each
            return true;
        }

        private bool DeleteAttendenceDetails()
        {
            bool blnReturn = false;
            try
            {
                int iEmpID = 0;
                int iCmpID = 0;
                MblDeleteMeassage = false; 
                for (int i = 0; i <= DgvAttendance.Rows.Count - 1; i++)
                {
                    //bool blnIsValid = false;
                    //if (blnIsValid)
                    //{
                        MblDeleteMeassage = true;
                        DateTime dtDate = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value);
                        iEmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
                        iCmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
                        int ileave = 0;
                        if (Int32.TryParse(Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value), out ileave))
                        {
                            ileave = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value);
                        }
                        if (MobjclsBLLAttendance.DeleteAttendanceWithLeave(iEmpID, iCmpID, dtDate, ileave))
                        {
                            blnReturn = true;
                        }
                        else
                        {
                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1573, out  MmessageIcon).Replace("period", "date").Trim();
                        }
                    //}
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                blnReturn = false;
            }
            return blnReturn;
        }
        private void fillAttendanceRow(int i)
        {
            int iEmp = 0;
            DateTime? dJoiningDate = null;
            string StrDate = "";
            iEmp = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
            string strJoining = MobjclsBLLAttendance.GetEmployeeJoiningDate(iEmp);
            if (strJoining != "")
            {
                dJoiningDate = Convert.ToDateTime(strJoining);
            }

            DateTime day = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value);
            if (day.Date < Convert.ToDateTime(dJoiningDate).Date)
            {
                //continue;
                return;
            }

            bool blnCheckDay = false;
            int iShiftID = 0;
            string sShiftName = "";
            string sFromtime = "";
            string sTotime = "";

            string sDuration = "";
            int ishiftType = 0;
            int iNoOfTimings = 0;
            string sMinWorkHours = "";
            int iShiftOrderNO = 0;
            int iAllowedBreaktime = 0;
            int iDayID = GetDayOfWeek(day);
            int iPolicyID = 0;
            int IsConsequenceRequired=0;
            string sAllowedBreaktime="0";
            int iEarly = 0;
            int iLate = 0;


            if (GetEmployeeShiftInfo(iEmp, Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value), Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value),
                                        ref iShiftID, ref sShiftName, ref sFromtime, ref sTotime, ref sDuration, ref ishiftType, ref iNoOfTimings, ref sMinWorkHours, ref sAllowedBreaktime, ref iLate, ref iEarly) == true)
            {

                MobjclsBLLAttendance.GetEmployeePolicy(iEmp, iDayID, ref iPolicyID, ref IsConsequenceRequired);
                if (ishiftType == 3)
                {
                    iShiftOrderNO = GetShiftOrderNo(iShiftID, "", ref sFromtime, ref sTotime, ref sDuration);
                    MobjclsBLLAttendance.GetDynamicShiftDuration(iShiftID, iShiftOrderNO, ref sMinWorkHours, ref sAllowedBreaktime);
                }
                blnCheckDay = MobjclsBLLAttendance.CheckDay(iEmp, GetDayOfWeek(day), DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime());
                iAllowedBreaktime = Convert.ToInt32(sAllowedBreaktime);

                DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Tag = iAllowedBreaktime;
                string sAlBreak = Convert.ToString(iAllowedBreaktime);
                sAlBreak = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAlBreak), DateTime.Today).ToString();
                sAlBreak = Convert.ToDateTime(sAlBreak).ToString("HH:mm:ss");
                DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Value = sAlBreak;

                DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag = iShiftID;
                DgvAttendance.Rows[i].Cells["ColDgvShift"].Value = sShiftName;
                DgvAttendance.Rows[i].Cells["ColDgvShiftOrderNo"].Value = iShiftOrderNO;

                int iType = 0;
                if (CheckIfValidDate(i, ref iType))
                {
                    DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value = 0;
                    DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 0;
                }

                else
                {
                    if (iType == 1)
                    {
                        DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 1;
                        DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value = 0;
                    }
                    else if (iType == 2 || iType == 3 || iType == 4)
                    {
                        DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value = 1;
                        DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 0;
                    }
                }
                if (DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value != null && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                {
                    if (MobjclsBLLAttendance.CheckHalfdayLeave(Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value), iEmp, Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value)))
                        DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 1;
                    else
                        DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value = 0;

                }

                if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value) == 0 && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                {
                    if (ishiftType <= 3)
                    {
                        DgvAttendance.Rows[i].Cells["TimeIn1"].Value = sFromtime;
                    }
                    DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value = 1;
                }
                DateTime dtDuration;
                int iDurationInMinutes = GetDurationInMinutes(sMinWorkHours);
                dtDuration = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, iDurationInMinutes, sFromtime.ToDateTime());

                if (Convert.ToInt32(ishiftType) == 2)//'flexi =true
                {

                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value) == 0 && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                        DgvAttendance.Rows[i].Cells["TimeOut1"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();

                }
                else if (ishiftType == 4)//Then 'Flexi
                {
                    DataTable DtShiftDetails = MobjclsBLLAttendance.GetDynamicShiftDetails(iShiftID);
                    if (DtShiftDetails.Rows.Count > 0)
                    {
                        int ICount = DtShiftDetails.Rows.Count;
                        int iStrat = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;
                        for (int ij = iStrat; ij <= ICount * 2 + 1; ij += 2)
                        {
                            int iColumnCount = Convert.ToInt32(Math.Floor(Convert.ToDouble(ij / 2)));
                            DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                            DataGridViewTextBoxColumn adcolOut = new DataGridViewTextBoxColumn();

                            adcolIn.Name = "TimeIn" + iColumnCount.ToString();
                            adcolIn.HeaderText = "TimeIn" + iColumnCount.ToString();
                            adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                            adcolIn.Resizable = DataGridViewTriState.False;
                            adcolIn.Width = 70;
                            adcolIn.MaxInputLength = 9;

                            adcolOut.Name = "TimeOut" + iColumnCount.ToString();
                            adcolOut.HeaderText = "TimeOut" + iColumnCount.ToString();
                            adcolOut.SortMode = DataGridViewColumnSortMode.NotSortable;
                            adcolOut.Resizable = DataGridViewTriState.False;
                            adcolOut.Width = 70;
                            adcolOut.MaxInputLength = 9;

                            DgvAttendance.Columns.Insert(ij + 1, adcolIn);
                            DgvAttendance.Columns.Insert(ij + 2, adcolOut);

                            MintTimingsColumnCnt += 2;
                        }//For 
                        for (int intCount = 0; intCount <= DtShiftDetails.Rows.Count - 1; intCount++)
                        {
                            string sSplitFromTime = Convert.ToString(DtShiftDetails.Rows[intCount]["FromTime"]);
                            string sSplitToTime = Convert.ToString(DtShiftDetails.Rows[intCount]["ToTime"]);
                            if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value) == 0 && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                            {
                                DgvAttendance.Rows[i].Cells["TimeIn" + (intCount + 1)].Value = sSplitFromTime;
                                DgvAttendance.Rows[i].Cells["TimeOut" + (intCount + 1)].Value = sSplitToTime;
                            }
                        }

                    }
                }
                else
                {
                    if (Convert.ToString(sTotime).Trim() != "")
                    {
                        if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvHolidayFlag"].Value) == 0 && Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveFlag"].Value) == 0)
                        {
                            //DgvAttendance.Rows[i].Cells["TimeOut1"].Value = sTotime;
                            if (iAllowedBreaktime > 0)
                            {
                                int intDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromtime), Convert.ToDateTime(sTotime));

                                if (intDurationDiff < 0)
                                {
                                    intDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromtime), Convert.ToDateTime(sTotime).AddDays(1));
                                }

                                if ((intDurationDiff - iAllowedBreaktime) < iDurationInMinutes)
                                {
                                    DgvAttendance.Rows[i].Cells["TimeOut1"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();//'' Totime

                                }
                                else
                                {
                                    int ICount = 3;
                                    int iStrat = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;

                                    for (int ij = iStrat; ij <= ICount + 2; ij = ij + 2)
                                    {
                                        DataGridViewTextBoxColumn adcolIn = new DataGridViewTextBoxColumn();
                                        DataGridViewTextBoxColumn adcolOut = new DataGridViewTextBoxColumn();

                                        adcolIn.Name = "TimeIn" + (ij - 2);
                                        adcolIn.HeaderText = "TimeIn" + (ij - 2);
                                        adcolIn.SortMode = DataGridViewColumnSortMode.NotSortable;
                                        adcolIn.Resizable = DataGridViewTriState.False;
                                        adcolIn.Width = 70;
                                        adcolIn.MaxInputLength = 9;

                                        adcolOut.Name = "TimeOut" + (ij - 2);
                                        adcolOut.HeaderText = "TimeOut" + (ij - 2);
                                        adcolOut.SortMode = DataGridViewColumnSortMode.NotSortable;
                                        adcolOut.Resizable = DataGridViewTriState.False;
                                        adcolOut.Width = 70;
                                        adcolOut.MaxInputLength = 9;

                                        DgvAttendance.Columns.Insert(ij + 1, adcolIn);
                                        DgvAttendance.Columns.Insert(ij + 2, adcolOut);
                                        MintTimingsColumnCnt += 2;
                                    }

                                    dtDuration = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, 30, Convert.ToDateTime(sFromtime)); //''initial default break
                                    DgvAttendance.Rows[i].Cells["TimeOut1"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();//'' Totime
                                    dtDuration = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, iAllowedBreaktime, Convert.ToDateTime(dtDuration));
                                    DgvAttendance.Rows[i].Cells["TimeIn2"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();// '' Totime
                                    dtDuration = Convert.ToDateTime(sTotime);
                                    DgvAttendance.Rows[i].Cells["TimeOut2"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();// '' Totime
                                }
                            }
                            else
                            {
                                DgvAttendance.Rows[i].Cells["TimeOut1"].Value = dtDuration.Hour.ToString() + ":" + dtDuration.Minute.ToString() + ":" + dtDuration.Second.ToString();// '' Totime
                            }

                        }

                    }

                }

                for (int j = 1; j <= Math.Floor(Convert.ToDouble((MintTimingsColumnCnt + 1) / 2)); j++)
                {
                    if (Glbln24HrFormat == false)
                    {
                        if ((Microsoft.VisualBasic.Information.IsNothing(DgvAttendance.Rows[i].Cells["TimeIn" + j].Value.ToStringCustom()) ? "" : DgvAttendance.Rows[i].Cells["TimeIn" + j].Value.ToStringCustom()) != "")
                        {
                            StrDate = ConvertDate12(Convert.ToDateTime(DgvAttendance.Rows[i].Cells["TimeIn" + j].Value));
                            if (Microsoft.VisualBasic.Strings.Right(DTPFormatTime.Text.Trim(), 2) == "AM")
                                DgvAttendance.Rows[i].Cells["TimeIn" + j].Value = DTPFormatTime.Text.Replace(":AM", " AM");
                            else
                                DgvAttendance.Rows[i].Cells["TimeIn" + j].Value = DTPFormatTime.Text.Replace(":PM", " PM");

                        }
                        if ((Microsoft.VisualBasic.Information.IsNothing(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value.ToStringCustom()) ? "" : DgvAttendance.Rows[i].Cells["TimeOut" + j].Value.ToStringCustom()) != "")
                        {
                            StrDate = ConvertDate12(Convert.ToDateTime(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value));
                            if (Microsoft.VisualBasic.Strings.Right(DTPFormatTime.Text.Trim(), 2) == "AM")
                                DgvAttendance.Rows[i].Cells["TimeOut" + j].Value = DTPFormatTime.Text.Replace(":AM", " AM");
                            else
                                DgvAttendance.Rows[i].Cells["TimeOut" + j].Value = DTPFormatTime.Text.Replace(":PM", " PM");

                        }
                    }
                    else
                    {
                        if ((Microsoft.VisualBasic.Information.IsNothing(DgvAttendance.Rows[i].Cells["TimeIn" + j].Value.ToStringCustom()) ? "" : DgvAttendance.Rows[i].Cells["TimeIn" + j].Value.ToStringCustom()) != "")
                        {
                            StrDate = ConvertDate24(Convert.ToDateTime(DgvAttendance.Rows[i].Cells["TimeIn" + j].Value));
                            DgvAttendance.Rows[i].Cells["TimeIn" + j].Value = DTPFormatTime.Text;
                        }
                        if ((Microsoft.VisualBasic.Information.IsNothing(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value) ? "" : Convert.ToString(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value)) != "")
                        {
                            StrDate = ConvertDate24(Convert.ToDateTime(DgvAttendance.Rows[i].Cells["TimeOut" + j].Value));
                            DgvAttendance.Rows[i].Cells["TimeOut" + j].Value = DTPFormatTime.Text;
                        }
                    }
                }// For 

            }
            if (Convert.ToString(DgvAttendance.Rows[i].Cells["TimeIn1"].Value) != "" && Convert.ToString(DgvAttendance.Rows[i].Cells["TimeOut1"].Value) != "" &&
            DgvAttendance.Rows[i].DefaultCellStyle.ForeColor != Color.IndianRed)
            {
                WorkTimeCalculation(i, false);
            }
        }

        private bool GetEmployeeShiftInfo(int intEmpID, int intCmpID, DateTime atnDate, ref int iShiftId, ref string sShiftName, ref string sFromtime,
                                      ref string sTotime, ref string sDuration, ref int ishiftType,
                                        ref int iNoOfTimings, ref string sMinWorkHours, ref string sAllowedBreak, ref int iLate, ref int iEarly)
        {
            DataTable dtEmployeeInfo = new DataTable();

            iShiftId = 0;
            sShiftName = "";
            sFromtime = "";
            sTotime = "";
            sDuration = "";
            ishiftType = 0;
            iNoOfTimings = 0;
            sMinWorkHours = "0";
            sAllowedBreak = "0";
            iLate = 0;
            iEarly = 0;

            int intDayID = 0;
            intDayID = (int)atnDate.DayOfWeek + 1;
            string sFromDate = atnDate.ToString("dd MMM yyyy");
            string sToDate = atnDate.ToString("dd MMM yyyy");

            try
            {
                dtEmployeeInfo = MobjclsBLLAttendance.GetEmployeeShiftInfo(intEmpID, intCmpID, sFromDate);//Shift Schedule fromDAte and toDAte are same
                if (dtEmployeeInfo.Rows.Count > 0)
                {

                    ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                    iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                    sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                    iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                    sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                    sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                    sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                    sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                    sAllowedBreak = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                    iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                    iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                }
                else
                {
                    bool dailyShift = MobjclsBLLAttendance.CheckDay(intEmpID, intDayID, atnDate);

                    if (dailyShift == true)
                    {

                        dtEmployeeInfo = MobjclsBLLAttendance.GetDailyShift(intEmpID, intDayID, atnDate);
                        if (dtEmployeeInfo.Rows.Count > 0)
                        {
                            iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                            sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["Shift"]);
                            sFromtime = Convert.ToString(ConvertDate24(Convert.ToDateTime(dtEmployeeInfo.Rows[0]["TimeIn1"])));
                            sTotime = Convert.ToString(ConvertDate24(Convert.ToDateTime(dtEmployeeInfo.Rows[0]["TimeOut1"])));
                            ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                            sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                            iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                            sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                            sAllowedBreak = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                            iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                            iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                        }

                    }
                    else
                    {
                        dtEmployeeInfo = MobjclsBLLAttendance.GetOffDayShift(intEmpID, intDayID, atnDate);
                        if (dtEmployeeInfo.Rows.Count > 0)
                        {
                            iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                            sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["Shift"]);
                            sFromtime = Convert.ToString(ConvertDate24(Convert.ToDateTime(dtEmployeeInfo.Rows[0]["TimeIn1"])));
                            sTotime = Convert.ToString(ConvertDate24(Convert.ToDateTime((dtEmployeeInfo.Rows[0]["TimeOut1"]))));
                            ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                            sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                            iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                            sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                            sAllowedBreak = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                            iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                            iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                        }

                    }
                }
                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public int GetAbsentTime(int iShiftid, int iWorkPolicy, int dayid)
        {
            int iAbsentTime = 0;
            string sBreakTime = "0";
            string sMinWorkHours = "0";
            MobjclsBLLAttendance.GetAbsentTime(iShiftid, iWorkPolicy, dayid, ref sBreakTime, ref sMinWorkHours);
            iAbsentTime = GetDurationInMinutes(sMinWorkHours);
            return iAbsentTime;
        }

        private bool FillParameterAtnMaster(int i)
        {
            MobjclsBLLAttendance.clsDTOAttendance.lstclsDTOAttendanceDetails = null;
            int iWorkPolicyID = 0;
            int iStatus = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value);
            int iempId = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
            int iCmpID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
            DateTime dtDates = Convert.ToDateTime(DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value);
            string sDtDates = dtDates.ToString("dd MMM yyyy");
            int iShiftID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag);
            int iShiftOrderNo = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShiftOrderNo"].Value);
            int DayID = GetDayOfWeek(dtDates);
            int iLeaveID = 0;
            short bHalfDay = 0;
            short bLOP = 0;
            double dConseqAmt = 0;
            int iConseqID = 0;
            int iPolicyid = 0;

            iWorkPolicyID = MobjclsBLLAttendance.GetEmpPolicyID(Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag));

            //if (MbAutoFill == false)
            //{
                if (MobjclsBLLAttendance.IsHoliday(iempId, iCmpID, dtDates, DayID) == false)
                {
                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Present))
                    {
                        ApplyPolicyConsequence(iCmpID, sDtDates, iempId, iShiftID, DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Tag == null ? 0 : Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Tag), DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Tag == null ? 0 : Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Tag),
                            DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Tag == null ? 0 : Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvAllowedBreakTime"].Tag), DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Tag == null ? false : Convert.ToBoolean(DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Tag) == false ? false : true,
                            DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Tag == null ? false : Convert.ToBoolean(DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Tag) == false ? false : true, ref bHalfDay, ref bLOP, ref iConseqID, ref iPolicyid, ref dConseqAmt, iShiftOrderNo);
                    }
                    else if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave)) //Then 'Half Day Leave
                    {
                        iLeaveID = MobjclsBLLAttendance.LeaveEntry(iCmpID, iempId, Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value), dtDates, Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveType"].Value),
                           DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Tag.ToInt32());
                        if (iLeaveID == 0)
                            return false;
                    }
                }

            //}
            MobjclsBLLAttendance.clsDTOAttendance.strDate = DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToString();
            MobjclsBLLAttendance.clsDTOAttendance.intEmployeeID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag);
            MobjclsBLLAttendance.clsDTOAttendance.intShiftID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag);

            MobjclsBLLAttendance.clsDTOAttendance.intProjectID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvProjectID"].Value);

            if (iStatus == Convert.ToInt32(AttendanceStatus.Rest) || iStatus == Convert.ToInt32(AttendanceStatus.Absent)) //Then 'full day leave and absent
            {
                MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = "00:00:00";
                MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = "";//"00:00:00";
                MobjclsBLLAttendance.clsDTOAttendance.strLate = "00:00:00";
                MobjclsBLLAttendance.clsDTOAttendance.strEarly = "00:00:00";
                MobjclsBLLAttendance.clsDTOAttendance.strOT = "";//"00:00:00";

                if (iStatus == Convert.ToInt32(AttendanceStatus.Absent))//absent
                {
                    int iabsent = GetAbsentTime(Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvShift"].Tag), iWorkPolicyID, DayID); //'if employee is absent shift duration is saving as absent time
                    MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = iabsent;
                }
                else//full day leave
                {
                    MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = 0;
                }
            }
            else if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
            {
                if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "0")
                {
                    MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = "00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = "";//"00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.strLate = "00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.strEarly = "00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.strOT = "";//"00:00:00";
                    MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = 0;
                }
                else
                {
                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Tag) > 0)
                    {
                        MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Value.ToStringCustom();
                        MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Value.ToString();
                        MobjclsBLLAttendance.clsDTOAttendance.strLate = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Value);
                        MobjclsBLLAttendance.clsDTOAttendance.strEarly = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Value);
                        MobjclsBLLAttendance.clsDTOAttendance.strOT = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvExcessTime"].Value);
                        MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvAbsentTime"].Value);
                    }
                    else
                    {
                        MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = "00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = "";//"00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.strLate = "00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.strEarly = "00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.strOT = "";//"00:00:00";
                        MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = 0;
                        bLOP = 1;

                    }
                }

            }
            else //'normal
            {
                MobjclsBLLAttendance.clsDTOAttendance.strWorkTime = DgvAttendance.Rows[i].Cells["ColDgvWorkTime"].Value.ToStringCustom();
                if (DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Value != null)
                {
                    MobjclsBLLAttendance.clsDTOAttendance.strBreakTime = DgvAttendance.Rows[i].Cells["ColDgvBreakTime"].Value.ToString();
                }
                if (DgvAttendance.Rows[i].Cells["ColDgvLateComing"] != null)
                {
                    MobjclsBLLAttendance.clsDTOAttendance.strLate = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLateComing"].Value);
                }
                MobjclsBLLAttendance.clsDTOAttendance.strEarly = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvEarlyGoing"].Value);
                MobjclsBLLAttendance.clsDTOAttendance.strOT = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvExcessTime"].Value);
                MobjclsBLLAttendance.clsDTOAttendance.intAbsentTime = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvAbsentTime"].Value);

            }

            MobjclsBLLAttendance.clsDTOAttendance.blnIsHalfDay = Convert.ToBoolean(bHalfDay);
            MobjclsBLLAttendance.clsDTOAttendance.blnIsLOP = Convert.ToBoolean(bLOP);
            MobjclsBLLAttendance.clsDTOAttendance.intCompanyId = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value);
            MobjclsBLLAttendance.clsDTOAttendance.intConsequenceID = iConseqID;
            MobjclsBLLAttendance.clsDTOAttendance.intPolicyID = iWorkPolicyID;
            MobjclsBLLAttendance.clsDTOAttendance.decAmountDed = Convert.ToDecimal(dConseqAmt);
            MobjclsBLLAttendance.clsDTOAttendance.intAttendenceStatusID = Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value);//Present
            if (iLeaveID > 0)
            {
                DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value = iLeaveID;
                MobjclsBLLAttendance.clsDTOAttendance.intLeaveID = iLeaveID;
            }
            else
            {
                MobjclsBLLAttendance.clsDTOAttendance.intLeaveID = 0;
            }

            if (iShiftOrderNo > 0)
            { MobjclsBLLAttendance.clsDTOAttendance.intShiftOrderID = iShiftOrderNo; }
            else { MobjclsBLLAttendance.clsDTOAttendance.intShiftOrderID = 0; }

            MobjclsBLLAttendance.clsDTOAttendance.strRemarks = Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvRemarks"].Value);
            if (iStatus == Convert.ToInt32(AttendanceStatus.Absent) || iStatus == Convert.ToInt32(AttendanceStatus.Rest))
            {
                return true;
            }
            if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
            {
                if (DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value.ToStringCustom() == "0")
                {
                    return true;
                }

            }
            FillParmeterAtnDetails(i);
            return true;
        }
        private bool FillParmeterAtnDetails(int iRow)
        {
            try
            {
                MobjclsBLLAttendance.clsDTOAttendance.lstclsDTOAttendanceDetails = new List<clsDTOAttendanceDetails>();
                MintDetailCount = 0;
                for (int j = 3; j < DgvAttendance.Columns["ColDgvAddMore"].Index; j++)
                {
                    if (DgvAttendance.Rows[iRow].Cells[j].Value.ToStringCustom() == string.Empty)
                        break;

                    clsDTOAttendanceDetails MobjclsDTOAttendanceDetails = new clsDTOAttendanceDetails();
                    MobjclsDTOAttendanceDetails.intOrderNo = j - 2;
                    MobjclsDTOAttendanceDetails.strTime = DgvAttendance.Rows[iRow].Cells[j].Value.ToString();
                    MobjclsBLLAttendance.clsDTOAttendance.lstclsDTOAttendanceDetails.Add(MobjclsDTOAttendanceDetails);
                    MintDetailCount += 1;
                }
                return true;
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }
        private bool ApplyPolicyConsequence(int iCompanyId, string sDate, int iEmpId, int iEmpShiftID, int iWorkTime, int iBreakTime,
                                                   int iAllowedBreak, bool bLate, bool bEarly, ref short bHalfDay, ref short bLOP, ref int iConseqID,
                                                   ref int iPolicyId, ref  double dConseqAmt, int iShiftOrderNo)
        {
            try
            {
                bHalfDay = 0;
                bLOP = 0;
                dConseqAmt = 0;
                iConseqID = 0;

                double iMnLeave = 0;
                double iTakenLeave = 0;
                int iLeaveid = 0;
                int iDayId = 0;
                int IsConsequenceRequired = 0;
                int iLop = 3; // 'none
                int iCasual = 3; //'none
                int iPolicyBreakAllowed = 0;
                int iLateMin = 0;
                int iEarlyMin = 0;
                int iAdditionalMinutes = 0;
                int intShiftType = 0;
                string sMinWorkHours = "0";
                string sDuration = "";
                string sAllowedBreak = "0";
                bool bHalfTaken = false;
                DateTime dt = Convert.ToDateTime(sDate);
                iDayId = (Convert.ToInt32(dt.DayOfWeek) + 1);
                iMnLeave = MobjclsBLLAttendance.GetEmployeeLeaveStatus(iEmpId, iCompanyId, sDate);
                iAdditionalMinutes = MobjclsBLLAttendance.GetLeaveExAddMinutes(iEmpId, dt);
                bHalfTaken = MobjclsBLLAttendance.GetEmployeeIsHalfDay(iEmpId, sDate);
                if (iMnLeave > 0)
                    iTakenLeave = MobjclsBLLAttendance.GetTakenleaves(iEmpId, dt, 2);//intLeveType=3 Casual
                else
                    iMnLeave = 0;
                MobjclsBLLAttendance.GetShiftInfo(iEmpShiftID, ref intShiftType, ref sDuration, ref sMinWorkHours, ref iPolicyBreakAllowed, ref iLateMin, ref iEarlyMin);//getting shiftInfo
                MobjclsBLLAttendance.GetEmployeePolicy(iEmpId, iDayId, ref iPolicyId, ref IsConsequenceRequired);
                if (intShiftType == 3)// only For Dynamic Shift
                {
                    sDuration = MobjclsBLLAttendance.GetDynamicShiftDuration(iEmpShiftID, iShiftOrderNo, ref sMinWorkHours, ref sAllowedBreak);
                    iPolicyBreakAllowed = Convert.ToInt32(sAllowedBreak);
                }

                if (iAdditionalMinutes > 0)
                {
                    bEarly = false;
                    bLate = false;
                }


                if (bHalfTaken == true)
                {
                    bEarly = true;
                    bLate = true;
                }
                int iDurationInMinute = 0;
                if (intShiftType == 4)
                {
                    iDurationInMinute = GetSplitShiftTotalDuration(iEmpShiftID);
                }
                else
                {
                    iDurationInMinute = GetDurationInMinutes(sDuration);
                }
                int iMinWorkingHrsMin = GetDurationInMinutes(sMinWorkHours);


                if (sDuration != "")
                {

                    if (intShiftType == 1 || intShiftType == 3 || intShiftType == 4)//'bFlexi = False
                    {


                        if (iDurationInMinute > iMinWorkingHrsMin)
                        {
                            sDuration = Convert.ToString(iMinWorkingHrsMin);
                        }
                        else
                        {
                            sDuration = Convert.ToString(iDurationInMinute);
                            iWorkTime += iPolicyBreakAllowed + iLateMin + iEarlyMin;
                        }
                    }
                    else
                    {

                        sDuration = Convert.ToString(iMinWorkingHrsMin);
                    }

                    iWorkTime += iAdditionalMinutes;

                    if (bHalfTaken)
                        sDuration = Convert.ToString(Convert.ToDouble(sDuration) / 2);

                    if (iWorkTime < Convert.ToDouble(sDuration))
                    {
                        MobjclsBLLAttendance.GetPolicyConsequenceWorktime(iEmpId, ref iLop, ref iCasual, ref dConseqAmt, ref iConseqID);

                        if (bHalfTaken && iLop == 2)
                            iLop = 1;
                        if (bHalfTaken && iCasual == 2)
                            iCasual = 1;
                        if (iLop == 1)//    'half day lop
                        {
                            bHalfDay = 1;
                            bLOP = 1;
                        }
                        else if (iLop == 2)//   'full day lop
                        {
                            bHalfDay = 0;
                            bLOP = 1;
                        }
                        else if (iCasual == 1)// 'half day casual
                        {
                            bHalfDay = 1;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                            }
                            else
                            {
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, 1, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;

                                }

                            }
                        }
                        else if (iCasual == 2)
                        {
                            bHalfDay = 0;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                                return true;
                            }
                            else
                            {
                                int ihalfday;
                                if (iMnLeave - iTakenLeave == 0.5)
                                {
                                    bHalfDay = 1;
                                    bLOP = 1;

                                    ihalfday = 1;
                                }
                                else
                                {
                                    bHalfDay = 0;
                                    bLOP = 0;
                                    ihalfday = 0;

                                }
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, ihalfday, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    if (iMnLeave - iTakenLeave == 0.5)
                                    { return true; }
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }

                            }
                        }
                        else if (dConseqAmt > 0)
                        {
                            bHalfDay = 0;
                            bLOP = 0;
                        }
                        else
                        {
                            iConseqID = 0;
                        }//else if s

                    }//iWorkTime < Convert.ToDouble(sDuration)

                }//sduration


                if (intShiftType == 2)//  ''Flexi then Exit after worktime consequence, Dynamic shift only worktime consequence
                {
                    return true;
                }

                if (iLeaveid <= 0)//Then 'break time
                {
                    if (iBreakTime > 0 && iConseqID == 0)
                    {
                        if (iBreakTime > iPolicyBreakAllowed + iAdditionalMinutes)
                        {
                            MobjclsBLLAttendance.GetPolicyConsequenceBreaktime(iPolicyId, ref iLop, ref  iCasual, ref dConseqAmt, ref iConseqID);

                            if (bHalfTaken && iLop == 2)
                                iLop = 1;
                            if (bHalfTaken && iCasual == 2)
                                iCasual = 1;
                            if (iLop == 1) //Then        'half day lop
                            {
                                bHalfDay = 1;
                                bLOP = 1;
                            }
                            else if (iLop == 2) //Then    'full day lop
                            {
                                bHalfDay = 0;
                                bLOP = 1;
                            }

                            else if (iCasual == 1) //Then 'half day casual
                            {
                                bHalfDay = 1;

                                if (iTakenLeave >= iMnLeave)
                                {
                                    bLOP = 1;
                                }
                                else
                                {
                                    iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, 1, dt, 2);

                                    if (iLeaveid > 0)
                                    {
                                        bLOP = 0;
                                        return true;
                                    }
                                    else
                                    {
                                        bLOP = 1;
                                    }

                                }

                            }
                            else if (iCasual == 2)//Then 'full day casual
                            {

                                bHalfDay = 0;
                                if (iTakenLeave >= iMnLeave)
                                {
                                    bLOP = 1;
                                    return true;
                                }
                                else
                                {


                                    int HalfDay;
                                    if (iMnLeave - iTakenLeave == 0.5)
                                    {
                                        bHalfDay = 1;
                                        bLOP = 1;
                                        HalfDay = 1;
                                    }
                                    else
                                    {
                                        bHalfDay = 0;
                                        bLOP = 0;
                                        HalfDay = 1;
                                    }
                                    iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, HalfDay, dt, 2);
                                    if (iLeaveid > 0)
                                    {
                                        if (iMnLeave - iTakenLeave == 0.5)
                                        { return true; }
                                        bLOP = 0;
                                        return true;
                                    }
                                    else
                                    {
                                        bLOP = 1;
                                    }

                                }
                            }
                            else if (dConseqAmt > 0)
                            {

                                bHalfDay = 0;
                                bLOP = 0;
                            }
                            else
                            {
                                iConseqID = 0;
                            }//else if's

                        }//( iBreakTime > iPolicyBreakAllowed + iAdditionalMinutes )
                    }//(iBreakTime > 0)
                }//(iLeaveid <= 0 )

                if (iLeaveid <= 0)//Then 'Late Coming
                {
                    if (bLate && iConseqID == 0)
                    {

                        MobjclsBLLAttendance.GetPolicyConsequenceLateComing(iEmpId, ref iLop, ref iCasual, ref dConseqAmt, ref iConseqID);
                        if (bHalfTaken && iLop == 2)
                            iLop = 1;
                        if (bHalfTaken && iCasual == 2)
                            iCasual = 1;
                        if (iLop == 1) //Then        'half day lop
                        {
                            bHalfDay = 1;
                            bLOP = 1;
                        }
                        else if (iLop == 2)   //Then 'full day lop
                        {
                            bHalfDay = 0;
                            bLOP = 1;
                        }
                        else if (iCasual == 1) //Then 'half day casual
                        {

                            bHalfDay = 1;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                            }
                            else
                            {

                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, 1, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }

                            }
                        }
                        else if (iCasual == 2) //Then 'full day casual
                        {
                            bHalfDay = 0;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                                return true;
                            }
                            else
                            {
                                int HalfDay;

                                if (iMnLeave - iTakenLeave == 0.5)
                                {
                                    bHalfDay = 1;
                                    bLOP = 1;
                                    HalfDay = 1;

                                }
                                else
                                {
                                    bHalfDay = 0;
                                    bLOP = 0;
                                    HalfDay = 0;

                                }
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, HalfDay, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    if (iMnLeave - iTakenLeave == 0.5) { return true; }
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }

                            }
                        }
                        else if (dConseqAmt > 0)
                        {
                            bHalfDay = 0;
                            bLOP = 0;
                        }
                        else
                        {
                            iConseqID = 0;
                        }// else if's


                    }//( bLate)

                }//( iLeaveid <= 0 )//Then 'Late Coming

                if (iLeaveid <= 0)//Then early Going
                {
                    if (bEarly && iConseqID == 0)
                    {
                        MobjclsBLLAttendance.GetPolicyConsequenceEarlyGoing(iEmpId, ref iLop, ref  iCasual, ref dConseqAmt, ref iConseqID);

                        if (bHalfTaken && iLop == 2) { iLop = 1; }
                        if (bHalfTaken && iCasual == 2) { iCasual = 1; }
                        if (iLop == 1)//Then        'half day lop
                        {
                            bHalfDay = 1;
                            bLOP = 1;
                        }
                        else if (iLop == 2)// Then    'full day lop
                        {
                            bHalfDay = 0;
                            bLOP = 1;
                        }
                        else if (iCasual == 1)//Then 'half day casual
                        {
                            bHalfDay = 1;
                            if (iTakenLeave >= iMnLeave)
                                bLOP = 1;
                            else
                            {
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, 1, dt, 2);
                                if (iLeaveid > 0)
                                {
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }

                            }
                        }
                        else if (iCasual == 2)//Then 'full day casual
                        {
                            bHalfDay = 0;
                            if (iTakenLeave >= iMnLeave)
                            {
                                bLOP = 1;
                                return true;
                            }
                            else
                            {

                                int HalfDay;

                                if (iMnLeave - iTakenLeave == 0.5)
                                {
                                    bHalfDay = 1;
                                    bLOP = 1;
                                    HalfDay = 1;

                                }
                                else
                                {
                                    bHalfDay = 0;
                                    bLOP = 0;
                                    HalfDay = 0;
                                }
                                iLeaveid = MobjclsBLLAttendance.LeaveFromPolicyConsequence(iCompanyId, iEmpId, HalfDay, dt, 2);

                                if (iLeaveid > 0)
                                {
                                    if (iMnLeave - iTakenLeave == 0.5) { return true; }
                                    bLOP = 0;
                                    return true;
                                }
                                else
                                {
                                    bLOP = 1;
                                }
                            }

                        }
                        else if (dConseqAmt > 0)
                        {
                            bHalfDay = 0;
                            bLOP = 0;
                        }
                        else
                        {
                            iConseqID = 0;
                        }//else if's

                    }//(bEarly)
                }//( iLeaveid <= 0 )Then early Going


                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }

        private bool CheckIfValidDate(int rowindex, ref  int Type)
        {
            try
            {

                int iCompID = Convert.ToInt32(DgvAttendance.Rows[rowindex].Cells["ColDgvCompanyID"].Value);
                int iEmpId = Convert.ToInt32(DgvAttendance.Rows[rowindex].Cells["ColDgvEmployee"].Tag);
                DateTime dCurrentDate = Convert.ToDateTime(DgvAttendance.Rows[rowindex].Cells["ColDgvAtnDate"].Value);
                int intDayID = Convert.ToInt32(dCurrentDate.DayOfWeek) + 1;
                if (MobjclsBLLAttendance.IsEmployeeOnLeave(iEmpId, iCompID, dCurrentDate) == true)
                {
                    Type = 1;
                    return false;
                }
                if (MobjclsBLLAttendance.CheckDay(iEmpId, intDayID, dCurrentDate) == false)
                {
                    Type = 3;
                    if (MobjclsBLLAttendance.CheckOffDayShift(iEmpId, intDayID, dCurrentDate) == false)
                    {
                        Type = 2;       //''offday without shift
                        return false;
                    }
                    return false;
                }
                if (MobjclsBLLAttendance.CheckHoliday(iCompID, dCurrentDate.ToString("dd MMM yyyy")) == true)
                {
                    Type = 4;//       ''Holiday
                    return false;
                }
                return true;

            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void DgvAttendance_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex != -1 && e.RowIndex != -1)
                {

                    //----------CELL Value Change------------------------------------------------------------------------------------------------------------------------
                    if (DgvAttendance.Rows.Count > 0)
                    {
                        if (Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value))
                            if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) != Convert.ToInt32(AttendanceStatus.Present))
                                //MbAutoFill = false;

                        if (e.ColumnIndex == ColDgvStatus.Index)
                        {
                            int iStatus = DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value != DBNull.Value && !Convert.ToString(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value).Trim().Equals(String.Empty) && Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value) == true ? Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value) : 0;
                            if (iStatus == Convert.ToInt32(AttendanceStatus.Absent) || iStatus == Convert.ToInt32(AttendanceStatus.Rest))// Then '--Or iStatus = 5
                            {
                                for (int i = ColDgvStatus.Index + 1; i <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; i++)
                                {
                                    DgvAttendance.Rows[e.RowIndex].Cells[i].Value = "";
                                }
                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Value = "";
                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Tag = 0;
                                if (Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value) <= 0)
                                {
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Tag = "";
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Value = "";
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveInfo"].Value = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";

                                }
                                if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvHolidayFlag"].Value) == 1)
                                {
                                    DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = 0;
                                    DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = "";
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1555, out MmessageIcon);
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    //tmrClearlabels.Enabled = true;
                                    DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                    DgvAttendance.CancelEdit();
                                    return;
                                }
                                if (DgvAttendance.CurrentRow.Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1)
                                {

                                    if (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0)
                                    {
                                        DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = 0;
                                        DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = "";
                                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1586, out MmessageIcon);
                                        //MstrMessCommon = MstrMessCommon.Replace("leave on", "Half day leave on this day");
                                        MstrMessCommon = MstrMessCommon.Replace("leave on", "leave on this day");
                                        if (MobjclsBLLAttendance.CheckHalfdayLeave(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[e.RowIndex].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAtnDate"].Value.ToDateTime()))
                                        {
                                            MstrMessCommon = MstrMessCommon.Replace("leave on", "Half day leave on this day");
                                        }
                                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                        //tmrClearlabels.Enabled = true;
                                        DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                        DgvAttendance.CancelEdit();
                                        return;
                                    }
                                }
                                DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";

                                //if (iStatus == Convert.ToInt32(AttendanceStatus.Absent))// Then '--Or iStatus = 5
                                //{
                                //    DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "Employee is absent from Attendance";
                                //}
                                //if (iStatus == Convert.ToInt32(AttendanceStatus.Rest))// Then '--Or iStatus = 5
                                //{
                                //    DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "Employee is Rest from Attendance";
                                //}

                            }
                            else if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
                            {
                                if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvHolidayFlag"].Value) == 1)
                                {
                                    DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = 0;
                                    DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = "";
                                    MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1555, out MmessageIcon);
                                    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                    //tmrClearlabels.Enabled = true;
                                    DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                    DgvAttendance.CancelEdit();
                                    return;
                                }

                                if (DgvAttendance.CurrentRow.Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1)
                                {
                                    int iLeaveType = MobjclsBLLAttendance.CheckHalfdayLeaveNew(DgvAttendance.CurrentRow.Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.CurrentRow.Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.CurrentRow.Cells["ColDgvAtnDate"].Value.ToDateTime());

                                    if (iLeaveType > 0 && (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0))
                                    {
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = iLeaveType;
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = 1;
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Tag = iLeaveType;
                                        return;
                                    }
                                    else
                                    {
                                        if (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0)
                                            return;
                                    }
                                }

                                int iEmpId = Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvEmployee"].Tag);
                                int iCmpID = Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvCompanyID"].Value);
                                string StrEmp = Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvEmployee"].Value);
                                string strDate = Convert.ToString(DgvAttendance.CurrentRow.Cells["ColDgvAtnDate"].Value);
                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Tag = "";
                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";

                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Value = "";
                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvWorkTime"].Tag = 0;
                                FrmLeaveFromAttendance objFrmLeaveFromAttendance = new FrmLeaveFromAttendance(iEmpId, StrEmp, iCmpID, strDate);

                                objFrmLeaveFromAttendance.ShowDialog();
                                if (objFrmLeaveFromAttendance.PblnBtnOk)
                                {
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = objFrmLeaveFromAttendance.PintLeaveType;
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = objFrmLeaveFromAttendance.PblnHalfday ? 1 : 0; //'Contains Haldfday or fullday
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = objFrmLeaveFromAttendance.PblnPaid ? 0 : 1; //'paid or unpaid
                                    DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = objFrmLeaveFromAttendance.PstrRemarks;

                                    GetEmployeeLeaveStatus(iEmpId, iCmpID, Convert.ToDateTime(strDate), objFrmLeaveFromAttendance.PintLeaveType);
                                    int iType = 0;
                                    if (!CheckingYearlyLeaves(DgvAttendance.CurrentRow.Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.CurrentRow.Cells["ColDgvEmployee"].Tag.ToInt32(), objFrmLeaveFromAttendance.PintLeaveType, DgvAttendance.CurrentRow.Cells["ColDgvAtnDate"].Value.ToDateTime()))
                                    {
                                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1588, out MmessageIcon);
                                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                        //tmrClearlabels.Enabled = true;
                                        DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                        DgvAttendance.CancelEdit();
                                        DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Absent;
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                        DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                        DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";
                                    }
                                    else
                                    {
                                        CheckingLeaves(objFrmLeaveFromAttendance.PintLeaveType, ref  iType);
                                        if (iType == 1)
                                        {
                                            //'half day only
                                            if (objFrmLeaveFromAttendance.PblnHalfday == false)
                                            {
                                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1588, out MmessageIcon);
                                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                                //tmrClearlabels.Enabled = true;
                                                DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                                DgvAttendance.CancelEdit();
                                                DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Absent;
                                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                                DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                                DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";
                                            }
                                        }
                                        else if (iType == 2)
                                        {
                                            //code later
                                        }
                                        else
                                        {
                                            MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1588, out MmessageIcon);
                                            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                            //tmrClearlabels.Enabled = true;
                                            DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                            DgvAttendance.CancelEdit();
                                            DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Absent;
                                            DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                            DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                            DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                            DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";
                                        }
                                    }

                                    if (objFrmLeaveFromAttendance.PblnHalfday)
                                    {
                                        WorkTimeCalculation(e.RowIndex, false);
                                    }
                                    else
                                    {

                                        for (int i = ColDgvStatus.Index + 1; i <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; i++)
                                        {
                                            DgvAttendance.Rows[e.RowIndex].Cells[i].Value = "";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = ColDgvStatus.Index + 1; i <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1; i++)
                                    {
                                        DgvAttendance.Rows[e.RowIndex].Cells[i].Value = "";
                                    }
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value = (int)AttendanceStatus.Absent;
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Value = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveType"].Tag = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Value = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";
                                }

                            }
                            else
                            {
                                if (Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value) <= 0)
                                {
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Tag = "";
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveType"].Value = "";
                                    DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveInfo"].Value = "";
                                    DgvAttendance.CurrentRow.Cells["ColDgvLeaveInfo"].Tag = "";

                                }
                                DgvAttendance.CurrentRow.Cells["ColDgvRemarks"].Value = "";

                                if (iStatus == (int)AttendanceStatus.Present && DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1)
                                {

                                    if (DgvAttendance.Rows[e.RowIndex].Cells["ColDgvLeaveId"].Value.ToInt32() <= 0)
                                    {
                                        DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = 0;
                                        DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value = "";
                                        MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1586, out MmessageIcon);
                                        MstrMessCommon = MstrMessCommon.Replace("leave on", "leave on this day");
                                        if (MobjclsBLLAttendance.CheckHalfdayLeave(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[e.RowIndex].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[e.RowIndex].Cells["ColDgvAtnDate"].Value.ToDateTime()))
                                        {
                                            MstrMessCommon = MstrMessCommon.Replace("leave on", "Half day leave on this day");
                                        }
                                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim() + " ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                        //tmrClearlabels.Enabled = true;
                                        DgvAttendance.CurrentCell = DgvAttendance[e.ColumnIndex, e.RowIndex];
                                        DgvAttendance.CancelEdit();
                                    }
                                }
                            }
                        }

                    }//Dgv.row.count>0

                    if (DgvAttendance.IsCurrentCellDirty)
                    {
                        if (DgvAttendance.CurrentCell != null)
                        {
                            DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                        }
                    }
                    if (e.ColumnIndex == ColDgvStatus.Index)
                    {
                        if (DgvAttendance.CurrentCell.Value != DBNull.Value && !Convert.ToString(DgvAttendance.CurrentCell.Value).Trim().Equals(String.Empty))
                        {
                            if (!Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.CurrentCell.Value))
                            {
                                MstrMessCommon = MobjClsNotification.GetErrorMessage(MsarMessageArr, 1594, out MmessageIcon);
                                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //lblAttendnaceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                                DgvAttendance.CurrentCell = DgvAttendance[ColDgvStatus.Index, e.RowIndex];
                                //tmrClearlabels.Enabled = true;
                                return;
                            }
                        }
                    }

                    if (DgvAttendance.CurrentRow.Cells["TimeIn1"].Value.ToStringCustom() != string.Empty)
                    {
                        FormatGrid(e.RowIndex);
                    }
                    else
                    {
                        if (Microsoft.VisualBasic.Information.IsNumeric(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value))
                        {
                            if (Convert.ToInt32(DgvAttendance.Rows[e.RowIndex].Cells["ColDgvStatus"].Value) != Convert.ToInt32(AttendanceStatus.Present))
                            {
                                if (DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor != Color.Red)
                                {
                                    DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
                                }
                            }
                        }

                    }

                    //if (e.ColumnIndex >= 3 && e.ColumnIndex <= DgvAttendance.Columns["ColDgvAddMore"].Index - 1)
                    //{
                    //    if (MbAutoFill == true)
                    //    {
                    //        if (Convert.ToString(DgvAttendance.CurrentRow.Cells[e.ColumnIndex].Value).Trim() != MstrCellTime)
                    //        {
                    //            MbAutoFill = false;
                    //        }
                    //        if (Convert.ToInt32(DgvAttendance.CurrentRow.Cells["ColDgvStatus"].Value) != 1)
                    //            MbAutoFill = false;
                    //    }
                    //}
                    //MintRowIndexformessge = e.RowIndex;

                    if (e.ColumnIndex >= 1)
                    {
                        if (DgvAttendance.Rows[e.RowIndex].Cells["TimeIn1"].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[e.RowIndex].Cells["TimeOut1"].Value.ToStringCustom() != string.Empty &&
                                     DgvAttendance.Rows[e.RowIndex].DefaultCellStyle.ForeColor != Color.IndianRed)
                            WorkTimeCalculation(e.RowIndex, false);

                    }

                }

            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);
            }

        }
        private void FormatGrid(int iRow)
        {
            Int32 icolName;
            DgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
            if (Convert.ToInt32(DgvAttendance.Rows[iRow].Cells["ColDgvHolidayFlag"].Value) == 1 || Convert.ToInt32(DgvAttendance.Rows[iRow].Cells["ColDgvLeaveFlag"].Value) == 1) //Then 'HOLIDAY CHEKINGG
            {
                //bool isHalfDay = MobjclsBLLAttendance.GetEmployeeIsHalfDay(DgvAttendance.Rows[iRow].Cells["ColDgvEmployee"].Tag.ToInt32(), Convert.ToDateTime(DgvAttendance.Rows[iRow].Cells["ColDgvAtnDate"].Value).ToString("dd MMM yyyy"));
                bool isHalfDay = (DgvAttendance.Rows[iRow].Cells["ColDgvLeaveInfo"].Value.ToInt32() == 1) ? true : false;
                if (DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Value == DBNull.Value)
                {
                    DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                    DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                }
                else
                {
                    if (Convert.ToString(DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Value) == "")
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                    }
                    else
                    {
                        if (isHalfDay)
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                        }
                        else
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Red;

                        }
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                    }
                }
                for (Int16 k = 2; k <= MintTimingsColumnCnt; k += 2)
                {
                    icolName = Convert.ToInt16(Math.Floor(Convert.ToDouble(k / 2)));

                    if (DgvAttendance.Rows[iRow].Cells["TimeIn1"].Value.ToStringCustom() == string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut1"].Value.ToStringCustom() == string.Empty)
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                        break;
                    }
                    else if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut" + icolName].Value.ToStringCustom() == string.Empty)
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                        break;
                    }
                    else if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() == string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut" + icolName].Value.ToStringCustom() != string.Empty)
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                        break;
                    }
                    else
                    {
                        if (isHalfDay)
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                        }
                        else
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Red;

                        }

                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                    }
                }
            }
            else// 'normal case
            {

                if (DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Value == DBNull.Value)
                {
                    DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                    DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                }
                else
                {
                    if (Convert.ToString(DgvAttendance.Rows[iRow].Cells["ColDgvWorkTime"].Value) == "")
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                    }
                    else
                    {
                        DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                        DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                    }
                }
                for (Int16 k = 2; k <= MintTimingsColumnCnt; k += 2)
                {
                    icolName = Convert.ToInt16(Math.Floor(Convert.ToDouble(k / 2)));
                    if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() != string.Empty)
                    {
                        if (DgvAttendance.Rows[iRow].Cells["TimeIn1"].Value.ToStringCustom() == string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut1"].Value.ToStringCustom() == string.Empty)
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                            break;
                        }
                        else if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() != string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut" + icolName].Value.ToStringCustom() == string.Empty)
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                            break;
                        }
                        else if (DgvAttendance.Rows[iRow].Cells["TimeIn" + icolName].Value.ToStringCustom() == string.Empty && DgvAttendance.Rows[iRow].Cells["TimeOut" + icolName].Value.ToStringCustom() != string.Empty) //Then 'modified
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.IndianRed;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 0;
                            break;
                        }
                        else
                        {
                            DgvAttendance.Rows[iRow].DefaultCellStyle.ForeColor = Color.Black;
                            DgvAttendance.Rows[iRow].Cells["ColDgvValidFlag"].Value = 1;
                        }
                    }
                }
            }
        }
        private bool GetEmployeeLeaveStatus(int iEmpId, int iCmpID, DateTime dtDate, int iLeaveType)
        {
            try
            {
                MdblMonthlyleave = 0;
                MdblTakenLeaves = 0;

                MdblMonthlyleave = MobjclsBLLAttendance.GetEmployeeLeaveStatus(iEmpId, iCmpID, dtDate.Date.ToString("dd MMM yyyy"), iLeaveType);

                if (MdblMonthlyleave > 0)
                {
                    string strdate = dtDate.Date.ToString("dd MMM yyyy");
                    MdblTakenLeaves = MobjclsBLLAttendance.GetTakenleaves(iEmpId, Convert.ToDateTime(strdate), iLeaveType);
                }
                else
                {
                    MdblMonthlyleave = 0;
                }
                return true;
            }
            catch (Exception Ex)
            {

                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                // MessageBox.Show("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //;
                return false;
            }
        }
        private bool CheckingYearlyLeaves(int intCompanyID, int intEmployee, int intLeaveType, DateTime dteDate)
        {
            try
            {
                double dblHalfdayLeave = 0;
                double intFulldayleave = 0;
                for (int i = 0; i <= DgvAttendance.Rows.Count - 1; i++)
                {
                    if (DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value.ToInt32() == Convert.ToInt32(AttendanceStatus.Leave))
                    {
                        if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveType"].Value) == intLeaveType)
                        {
                            if (DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value.ToInt32() == 0)
                            {

                                DataTable datLeaveRemarks = MobjclsBLLAttendance.GetLeaveRemarks(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime());
                                if (datLeaveRemarks.Rows.Count <= 0)
                                {

                                    if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "1")
                                    {
                                        dblHalfdayLeave += 0.5;

                                    }
                                    else if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "0")
                                    {
                                        intFulldayleave += 1;

                                    }
                                }
                            }
                        }
                    }
                }
                if ((intFulldayleave + dblHalfdayLeave) > 0)
                {
                    DataSet dtsLeave = MobjclsBLLAttendance.GetBalanceAndHolidaysdetails(intCompanyID, intEmployee, intLeaveType, dteDate.ToString("dd MMM yyyy"));
                    if (dtsLeave.Tables.Count > 0)
                    {
                        if (dtsLeave.Tables[0].Rows.Count > 0)
                        {
                            double dblBalanceLeave = dtsLeave.Tables[0].Rows[0][0].ToDouble();
                            if ((intFulldayleave + dblHalfdayLeave) <= dblBalanceLeave)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }

                }
                return false;
            }
            catch (Exception Ex)
            {
                return false;
            }
        }
        private bool CheckingLeaves(int intLeaveType, ref  int Type)
        {

            double dblHalfdayLeave = 0;
            double intFulldayleave = 0;
            double TotalLeaves = 0;
            double dblTakenLeavesFrmAtten = 0;
            try
            {
                //if (MblnPayExists)
                //{


                for (int i = 0; i <= DgvAttendance.Rows.Count - 1; i++)
                {
                    if (DgvAttendance.Rows[i].Cells["ColDgvStatus"].Value.ToInt32() == Convert.ToInt32(AttendanceStatus.Leave))
                    {
                        if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveType"].Value) == intLeaveType)
                        {
                            DataTable datLeaveRemarks = MobjclsBLLAttendance.GetLeaveRemarks(DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag.ToInt32(), DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime());
                            if (datLeaveRemarks.Rows.Count <= 0)
                            {

                                if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "1")
                                {
                                    dblHalfdayLeave += 0.5;
                                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value) > 0)
                                    {
                                        dblTakenLeavesFrmAtten += 0.5;
                                    }
                                }
                                else if (Convert.ToString(DgvAttendance.Rows[i].Cells["ColDgvLeaveInfo"].Value) == "0")
                                {
                                    intFulldayleave += 1;
                                    if (Convert.ToInt32(DgvAttendance.Rows[i].Cells["ColDgvLeaveId"].Value) > 0)
                                    {
                                        dblTakenLeavesFrmAtten += 1;
                                    }
                                }
                            }
                        }
                    }
                }
                TotalLeaves = intFulldayleave + dblHalfdayLeave - dblTakenLeavesFrmAtten;

                if (MdblMonthlyleave > 0)
                {
                    if ((MdblMonthlyleave - MdblTakenLeaves) >= TotalLeaves)
                    {
                        if ((MdblMonthlyleave - MdblTakenLeaves) == 0.5)
                        //'half day casual
                        { Type = 1; }//'half day allowed
                        else if ((MdblMonthlyleave - MdblTakenLeaves) >= 1)
                        {//'full day casual or halfday
                            Type = 2;
                        }
                        else
                        {
                            Type = 0;
                        }
                    }
                    else
                    {
                        Type = 0;
                    }
                }
                else
                {
                    Type = 0;
                }
                // }
                return true;

            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
                //MessageBox.Show("Error on :" + sMethod + " " + this.Name.ToString() + " " + Ex.Message.ToString(), GlStrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //return false;
                return false;
            }
        }


        private void btnExcelExport_Click(object sender, EventArgs e)
        {
            string sFileName = "";

            if (DgvAttendance.RowCount <= 0)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";

                if (sfd.ShowDialog() == DialogResult.OK)
                    sFileName = sfd.FileName;
                else
                    return;
            }

            try
            {
                if (sFileName != "")
                {
                    if (DgvAttendance.RowCount > 0)
                        MobjExportToExcel.ExportToExcel(DgvAttendance, sFileName, 0, DgvAttendance.Columns.Count - 1, true);
                }
                else
                    MessageBox.Show("Please Enter Name");
            }
            catch { }
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCombo(4); 
            }
            catch
            {
            }
        }

        private void cboDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCombo(4);
            }
            catch
            {
            }
        }

        private void BtnAutoFill_Click(object sender, EventArgs e)
        {
            try
            {
                int MaxDay;
                if (txtCount.Text.Trim() == "")
                {
                    txtCount.Text = "0";
                }
                MaxDay = DateTime.DaysInMonth(dtpMonthSelect.Value.Year, dtpMonthSelect.Value.Month);

                if (MaxDay == txtCount.Text.ToInt32())
                {
                    MaxDay = txtCount.Text.ToInt32();
                }
                else
                {
                    MaxDay = MaxDay - txtCount.Text.ToInt32();
                }
                if (MaxDay > 0 && MaxDay != txtCount.Text.ToInt32())
                {
                    for (int i = 0 ; i <= DgvAttendance.RowCount - 1; i++)
                    {
                        DateTime dtDate = DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime();
                        if (dtDate.Day > MaxDay)
                        {
                            int EmpID = 0;
                            int ComID = 0;
                            ComID = DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value.ToInt32();
                            EmpID = DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag.ToInt32();
                            if (EmpID > 0 && ComID > 0)
                            {
                                fillAttendanceRow(i);
                            }
                        }
                    }
                }
                else if (MaxDay == txtCount.Text.ToInt32() && txtCount.Text.ToInt32()>0)
                {
                    for (int i = 0; i <= DgvAttendance.RowCount - 1; i++)
                    {
                        DateTime dtDate = DgvAttendance.Rows[i].Cells["ColDgvAtnDate"].Value.ToDateTime();

                            int EmpID = 0;
                            int ComID = 0;
                            ComID = DgvAttendance.Rows[i].Cells["ColDgvCompanyID"].Value.ToInt32();
                            EmpID = DgvAttendance.Rows[i].Cells["ColDgvEmployee"].Tag.ToInt32();
                            if (EmpID > 0 && ComID > 0)
                            {
                                fillAttendanceRow(i);
                            }
                    }
                }

            }
            catch
            {
            }
        }

        private void txtCount_Leave(object sender, EventArgs e)
        {

            //if (txtCount.Text.ToDecimal() > 15)
            //{
            //    txtCount.Text = "15";
            //}

        }

        private void CboEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            CboEmployee.DroppedDown = false; 
        }

        private void cboDesignation_KeyDown(object sender, KeyEventArgs e)
        {
            cboDesignation.DroppedDown = false;
        }

        private void cboDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            cboDepartment.DroppedDown = false; 
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void CboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
            FillAllEmployees();
        }

        private void CboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

       



    }
}
