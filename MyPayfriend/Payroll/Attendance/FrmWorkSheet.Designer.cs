﻿namespace MyPayfriend
{
    partial class FrmWorkSheet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWorkSheet));
            this.DockSite7 = new DevComponents.DotNetBar.DockSite();
            this.BarView = new DevComponents.DotNetBar.Bar();
            this.btnSave = new DevComponents.DotNetBar.ButtonItem();
            this.btnDelete = new DevComponents.DotNetBar.ButtonItem();
            this.btnClear = new DevComponents.DotNetBar.ButtonItem();
            this.btnAutoFill = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.CustomizeItem1 = new DevComponents.DotNetBar.CustomizeItem();
            this.StatusStripBottom = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblAttendanceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.TlSProgressBarAttendance = new System.Windows.Forms.ToolStripProgressBar();
            this.btnSaveB = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GrdWorkSheet = new System.Windows.Forms.DataGridView();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.cboDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblMonth = new DevComponents.DotNetBar.LabelX();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnShow = new System.Windows.Forms.Button();
            this.lblColor = new DevComponents.DotNetBar.LabelX();
            this.CboVendor = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CboEmployee = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CboJob = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CboUnit = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.NoOfUnits = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HolidayUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OvertimeHrs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorksheetdetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JobID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CopyDetailsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.applyToAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Employee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateOfJoining = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkedDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AbsentDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OTHour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HolidayWorked = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateRange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DockSite7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarView)).BeginInit();
            this.StatusStripBottom.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdWorkSheet)).BeginInit();
            this.grpMain.SuspendLayout();
            this.CopyDetailsContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // DockSite7
            // 
            this.DockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite7.Controls.Add(this.BarView);
            this.DockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite7.Location = new System.Drawing.Point(0, 0);
            this.DockSite7.Name = "DockSite7";
            this.DockSite7.Size = new System.Drawing.Size(1003, 25);
            this.DockSite7.TabIndex = 151;
            this.DockSite7.TabStop = false;
            // 
            // BarView
            // 
            this.BarView.AccessibleDescription = "DotNetBar Bar (BarView)";
            this.BarView.AccessibleName = "DotNetBar Bar";
            this.BarView.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.BarView.CanUndock = false;
            this.BarView.ColorScheme.BarCaptionBackground = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.BarCaptionBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ExplorerBarBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(172)))), ((int)(((byte)(89)))));
            this.BarView.ColorScheme.ExplorerBarBackground2 = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(121)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ItemCheckedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(97)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ItemHotBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(125)))));
            this.BarView.ColorScheme.ItemHotBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.ColorScheme.ItemPressedBackground = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.BarView.ColorScheme.ItemPressedBorder = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BarView.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.BarView.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.BarView.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSave,
            this.btnDelete,
            this.btnClear,
            this.btnAutoFill,
            this.BtnPrint,
            this.CustomizeItem1});
            this.BarView.Location = new System.Drawing.Point(0, 0);
            this.BarView.Name = "BarView";
            this.BarView.Size = new System.Drawing.Size(147, 25);
            this.BarView.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.BarView.TabIndex = 2;
            this.BarView.TabStop = false;
            this.BarView.Text = "Bar2";
            this.BarView.ItemClick += new System.EventHandler(this.BarView_ItemClick);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::MyPayfriend.Properties.Resources.save;
            this.btnSave.Name = "btnSave";
            this.btnSave.Text = "Save";
            this.btnSave.Tooltip = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Text = "Delete";
            this.btnDelete.Tooltip = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.Name = "btnClear";
            this.btnClear.Text = "Clear";
            this.btnClear.Tooltip = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAutoFill
            // 
            this.btnAutoFill.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.btnAutoFill.BeginGroup = true;
            this.btnAutoFill.Image = global::MyPayfriend.Properties.Resources.Autofill;
            this.btnAutoFill.Name = "btnAutoFill";
            this.btnAutoFill.Text = "AutoFill";
            this.btnAutoFill.Tooltip = "Auto Fill";
            this.btnAutoFill.Click += new System.EventHandler(this.btnAutoFill_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.BeginGroup = true;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Tooltip = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // CustomizeItem1
            // 
            this.CustomizeItem1.CustomizeItemVisible = false;
            this.CustomizeItem1.Name = "CustomizeItem1";
            this.CustomizeItem1.Text = "&Add or Remove Buttons";
            this.CustomizeItem1.Tooltip = "Bar Options";
            // 
            // StatusStripBottom
            // 
            this.StatusStripBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.StatusStripBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lblAttendanceStatus,
            this.TlSProgressBarAttendance});
            this.StatusStripBottom.Location = new System.Drawing.Point(0, 530);
            this.StatusStripBottom.Name = "StatusStripBottom";
            this.StatusStripBottom.Size = new System.Drawing.Size(1003, 22);
            this.StatusStripBottom.TabIndex = 152;
            this.StatusStripBottom.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(42, 17);
            this.lblStatus.Text = "Status:";
            // 
            // lblAttendanceStatus
            // 
            this.lblAttendanceStatus.Name = "lblAttendanceStatus";
            this.lblAttendanceStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // TlSProgressBarAttendance
            // 
            this.TlSProgressBarAttendance.Name = "TlSProgressBarAttendance";
            this.TlSProgressBarAttendance.Size = new System.Drawing.Size(100, 16);
            this.TlSProgressBarAttendance.Visible = false;
            // 
            // btnSaveB
            // 
            this.btnSaveB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveB.Location = new System.Drawing.Point(9, 497);
            this.btnSaveB.Name = "btnSaveB";
            this.btnSaveB.Size = new System.Drawing.Size(75, 23);
            this.btnSaveB.TabIndex = 153;
            this.btnSaveB.Text = "&Save";
            this.btnSaveB.UseVisualStyleBackColor = true;
            this.btnSaveB.Click += new System.EventHandler(this.btnSaveB_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(916, 497);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 155;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(835, 497);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 154;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.GrdWorkSheet);
            this.groupBox1.Controls.Add(this.grpMain);
            this.groupBox1.Location = new System.Drawing.Point(1, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(990, 460);
            this.groupBox1.TabIndex = 156;
            this.groupBox1.TabStop = false;
            // 
            // GrdWorkSheet
            // 
            this.GrdWorkSheet.AllowUserToAddRows = false;
            this.GrdWorkSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GrdWorkSheet.BackgroundColor = System.Drawing.Color.White;
            this.GrdWorkSheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdWorkSheet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmployeeID,
            this.Employee,
            this.DateOfJoining,
            this.TotalDays,
            this.WorkedDays,
            this.AbsentDays,
            this.OTHour,
            this.HolidayWorked,
            this.Remarks,
            this.ToDate,
            this.DateRange});
            this.GrdWorkSheet.Location = new System.Drawing.Point(4, 71);
            this.GrdWorkSheet.Name = "GrdWorkSheet";
            this.GrdWorkSheet.RowHeadersWidth = 24;
            this.GrdWorkSheet.Size = new System.Drawing.Size(980, 377);
            this.GrdWorkSheet.TabIndex = 2;
            this.GrdWorkSheet.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.GrdWorkSheet_CellMouseClick);
            this.GrdWorkSheet.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.GrdWorkSheet_CellBeginEdit);
            this.GrdWorkSheet.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.GrdWorkSheet_EditingControlShowing);
            this.GrdWorkSheet.CurrentCellDirtyStateChanged += new System.EventHandler(this.GrdWorkSheet_CurrentCellDirtyStateChanged);
            this.GrdWorkSheet.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.GrdWorkSheet_DataError);
            this.GrdWorkSheet.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrdWorkSheet_CellEnter);
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.cboDepartment);
            this.grpMain.Controls.Add(this.labelX1);
            this.grpMain.Controls.Add(this.dtpFromDate);
            this.grpMain.Controls.Add(this.lblMonth);
            this.grpMain.Controls.Add(this.lblCompany);
            this.grpMain.Controls.Add(this.cboCompany);
            this.grpMain.Controls.Add(this.btnShow);
            this.grpMain.ForeColor = System.Drawing.Color.Black;
            this.grpMain.Location = new System.Drawing.Point(4, 0);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(986, 65);
            this.grpMain.TabIndex = 1;
            this.grpMain.TabStop = false;
            this.grpMain.Text = "General info";
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.DisplayMember = "Text";
            this.cboDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.ItemHeight = 14;
            this.cboDepartment.Location = new System.Drawing.Point(411, 27);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(256, 20);
            this.cboDepartment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDepartment.TabIndex = 36;
            this.cboDepartment.WatermarkText = "Company";
            this.cboDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboDepartment_KeyDown);
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(345, 32);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(60, 15);
            this.labelX1.TabIndex = 35;
            this.labelX1.Text = "Department";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(730, 27);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowUpDown = true;
            this.dtpFromDate.Size = new System.Drawing.Size(105, 20);
            this.dtpFromDate.TabIndex = 33;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            this.dtpFromDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpFromDate_KeyDown);
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            // 
            // 
            // 
            this.lblMonth.BackgroundStyle.Class = "";
            this.lblMonth.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMonth.Location = new System.Drawing.Point(681, 32);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(33, 15);
            this.lblMonth.TabIndex = 34;
            this.lblMonth.Text = "Month";
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(19, 32);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 27;
            this.lblCompany.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(75, 27);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(256, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 0;
            this.cboCompany.WatermarkText = "Company";
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCompany_KeyDown);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(849, 25);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(56, 22);
            this.btnShow.TabIndex = 4;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // lblColor
            // 
            this.lblColor.BackColor = System.Drawing.Color.DarkSeaGreen;
            // 
            // 
            // 
            this.lblColor.BackgroundStyle.Class = "";
            this.lblColor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblColor.ForeColor = System.Drawing.Color.White;
            this.lblColor.Location = new System.Drawing.Point(90, 485);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(92, 19);
            this.lblColor.TabIndex = 37;
            this.lblColor.Text = "Salary Processed";
            // 
            // CboVendor
            // 
            this.CboVendor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboVendor.HeaderText = "Vendors";
            this.CboVendor.Name = "CboVendor";
            this.CboVendor.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CboVendor.Width = 150;
            // 
            // CboEmployee
            // 
            this.CboEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboEmployee.HeaderText = "Employee";
            this.CboEmployee.Name = "CboEmployee";
            this.CboEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CboEmployee.Width = 150;
            // 
            // CboJob
            // 
            this.CboJob.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboJob.HeaderText = "Job";
            this.CboJob.Name = "CboJob";
            this.CboJob.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CboJob.Width = 125;
            // 
            // CboUnit
            // 
            this.CboUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CboUnit.HeaderText = "Unit";
            this.CboUnit.Name = "CboUnit";
            this.CboUnit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CboUnit.Width = 125;
            // 
            // NoOfUnits
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.NoOfUnits.DefaultCellStyle = dataGridViewCellStyle1;
            this.NoOfUnits.HeaderText = "No of Units";
            this.NoOfUnits.MaxInputLength = 5;
            this.NoOfUnits.Name = "NoOfUnits";
            this.NoOfUnits.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.NoOfUnits.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.NoOfUnits.Width = 90;
            // 
            // HolidayUnit
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.HolidayUnit.DefaultCellStyle = dataGridViewCellStyle2;
            this.HolidayUnit.HeaderText = "HolidayUnit";
            this.HolidayUnit.Name = "HolidayUnit";
            this.HolidayUnit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // OvertimeHrs
            // 
            this.OvertimeHrs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.OvertimeHrs.DefaultCellStyle = dataGridViewCellStyle3;
            this.OvertimeHrs.HeaderText = "Overtime Hrs";
            this.OvertimeHrs.MaxInputLength = 5;
            this.OvertimeHrs.Name = "OvertimeHrs";
            this.OvertimeHrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // WorksheetdetailID
            // 
            this.WorksheetdetailID.HeaderText = "WorksheetdetailID";
            this.WorksheetdetailID.Name = "WorksheetdetailID";
            this.WorksheetdetailID.Visible = false;
            this.WorksheetdetailID.Width = 5;
            // 
            // EditStatus
            // 
            this.EditStatus.HeaderText = "EditStatus";
            this.EditStatus.Name = "EditStatus";
            this.EditStatus.ReadOnly = true;
            this.EditStatus.Visible = false;
            // 
            // Valid
            // 
            this.Valid.HeaderText = "Valid";
            this.Valid.Name = "Valid";
            this.Valid.ReadOnly = true;
            this.Valid.Visible = false;
            // 
            // JobID
            // 
            this.JobID.HeaderText = "JobID";
            this.JobID.Name = "JobID";
            this.JobID.Visible = false;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn1.HeaderText = "Vendors";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn1.Width = 150;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn2.HeaderText = "Employee";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn2.Width = 150;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn3.HeaderText = "Job";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn3.Width = 125;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn4.HeaderText = "Unit";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn4.Width = 125;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn1.HeaderText = "No of Units";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 90;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn2.HeaderText = "HolidayUnit";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn3.HeaderText = "Overtime Hrs";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "WorksheetdetailID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            this.dataGridViewTextBoxColumn4.Width = 5;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "EditStatus";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Valid";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "JobID";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // CopyDetailsContextMenuStrip
            // 
            this.CopyDetailsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applyToAllToolStripMenuItem});
            this.CopyDetailsContextMenuStrip.Name = "CopyDetailsContextMenuStrip";
            this.CopyDetailsContextMenuStrip.Size = new System.Drawing.Size(140, 26);
            // 
            // applyToAllToolStripMenuItem
            // 
            this.applyToAllToolStripMenuItem.Name = "applyToAllToolStripMenuItem";
            this.applyToAllToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.applyToAllToolStripMenuItem.Text = "Apply To All";
            this.applyToAllToolStripMenuItem.Click += new System.EventHandler(this.applyToAllToolStripMenuItem_Click);
            // 
            // EmployeeID
            // 
            this.EmployeeID.HeaderText = "EmployeeID";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EmployeeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EmployeeID.Visible = false;
            this.EmployeeID.Width = 5;
            // 
            // Employee
            // 
            this.Employee.HeaderText = "Employee";
            this.Employee.Name = "Employee";
            this.Employee.ReadOnly = true;
            this.Employee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Employee.Width = 350;
            // 
            // DateOfJoining
            // 
            this.DateOfJoining.HeaderText = "DateOfJoining";
            this.DateOfJoining.Name = "DateOfJoining";
            this.DateOfJoining.ReadOnly = true;
            this.DateOfJoining.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TotalDays
            // 
            this.TotalDays.HeaderText = "TotalDays";
            this.TotalDays.Name = "TotalDays";
            this.TotalDays.ReadOnly = true;
            this.TotalDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // WorkedDays
            // 
            this.WorkedDays.HeaderText = "WorkedDays";
            this.WorkedDays.Name = "WorkedDays";
            this.WorkedDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AbsentDays
            // 
            this.AbsentDays.HeaderText = "Absent Days";
            this.AbsentDays.Name = "AbsentDays";
            this.AbsentDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // OTHour
            // 
            this.OTHour.HeaderText = "OTHour";
            this.OTHour.Name = "OTHour";
            this.OTHour.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // HolidayWorked
            // 
            this.HolidayWorked.HeaderText = "Holiday Worked";
            this.HolidayWorked.Name = "HolidayWorked";
            this.HolidayWorked.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Remarks
            // 
            this.Remarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.MaxInputLength = 500;
            this.Remarks.Name = "Remarks";
            this.Remarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ToDate
            // 
            this.ToDate.HeaderText = "ToDate";
            this.ToDate.Name = "ToDate";
            this.ToDate.Visible = false;
            // 
            // DateRange
            // 
            this.DateRange.HeaderText = "DateRange";
            this.DateRange.Name = "DateRange";
            this.DateRange.Visible = false;
            // 
            // FrmWorkSheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 552);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblColor);
            this.Controls.Add(this.btnSaveB);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.StatusStripBottom);
            this.Controls.Add(this.DockSite7);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmWorkSheet";
            this.Text = "Work Sheet";
            this.Load += new System.EventHandler(this.FrmWorkSheet_Load);
            this.DockSite7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarView)).EndInit();
            this.StatusStripBottom.ResumeLayout(false);
            this.StatusStripBottom.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GrdWorkSheet)).EndInit();
            this.grpMain.ResumeLayout(false);
            this.grpMain.PerformLayout();
            this.CopyDetailsContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.DockSite DockSite7;
        internal DevComponents.DotNetBar.Bar BarView;
        internal DevComponents.DotNetBar.ButtonItem btnSave;
        internal DevComponents.DotNetBar.ButtonItem btnDelete;
        internal DevComponents.DotNetBar.ButtonItem btnClear;
        internal DevComponents.DotNetBar.ButtonItem btnAutoFill;
        internal DevComponents.DotNetBar.ButtonItem BtnPrint;
        internal DevComponents.DotNetBar.CustomizeItem CustomizeItem1;
        internal System.Windows.Forms.StatusStrip StatusStripBottom;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblAttendanceStatus;
        internal System.Windows.Forms.ToolStripProgressBar TlSProgressBarAttendance;
        internal System.Windows.Forms.Button btnSaveB;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.DataGridViewComboBoxColumn CboVendor;
        internal System.Windows.Forms.DataGridViewComboBoxColumn CboEmployee;
        internal System.Windows.Forms.DataGridViewComboBoxColumn CboJob;
        internal System.Windows.Forms.DataGridViewComboBoxColumn CboUnit;
        internal System.Windows.Forms.DataGridViewTextBoxColumn NoOfUnits;
        internal System.Windows.Forms.DataGridViewTextBoxColumn HolidayUnit;
        internal System.Windows.Forms.DataGridViewTextBoxColumn OvertimeHrs;
        internal System.Windows.Forms.DataGridViewTextBoxColumn WorksheetdetailID;
        internal System.Windows.Forms.DataGridViewTextBoxColumn EditStatus;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Valid;
        internal System.Windows.Forms.DataGridViewTextBoxColumn JobID;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridView GrdWorkSheet;
        internal System.Windows.Forms.GroupBox grpMain;
        internal DevComponents.DotNetBar.LabelX lblCompany;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        internal System.Windows.Forms.Button btnShow;
        internal System.Windows.Forms.DateTimePicker dtpFromDate;
        internal DevComponents.DotNetBar.LabelX lblMonth;
        private System.Windows.Forms.ContextMenuStrip CopyDetailsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem applyToAllToolStripMenuItem;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboDepartment;
        internal DevComponents.DotNetBar.LabelX labelX1;
        internal DevComponents.DotNetBar.LabelX lblColor;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Employee;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateOfJoining;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkedDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn AbsentDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn OTHour;
        private System.Windows.Forms.DataGridViewTextBoxColumn HolidayWorked;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn ToDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateRange;
    }
}