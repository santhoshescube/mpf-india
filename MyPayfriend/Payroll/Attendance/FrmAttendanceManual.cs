﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;

namespace MyPayfriend
{
    public partial class FrmAttendanceManual : DevComponents.DotNetBar.Office2007Form
    {

        #region Enums
        private enum AttendanceStatus
        {
            Present = 1,
            Rest = 2,
            Leave = 3,
            Absent = 4
        }
        private enum TagInfo
        {
            AttendanceID = 1,
            AttendanceStatusID = 2,
            OT = 3,
            ShiftID = 4,
            LeaveTypeID = 5,
            HalfdayLeave = 6,
            PaidLeave = 7,
            LeaveID = 8,
            ConsequenceID = 9,
            AbsentTime = 10,
            MinWorkingHours = 11,
            IsHoliday = 12,
            IsOnLeave = 13,
            IsAtnAutoExists = 14,
            ShiftName = 15,
            WorkLocation =16
        }

        private enum CellEditableTypes
        {
            Holiday = 1,
            HalfDayLeave = 2,
            FullDayLeave = 3,
            AttendanceAutoExists = 4,
            SalaryIsReleased = 5,
            VacationExists = 6
        }

        #endregion

        #region Declarations
        bool MblnViewPermission;
        bool MblnAddPermission;
        bool MblnUpdatePermission;
        bool MblnDeletePermission;
        bool MblnAddUpdatePermission;
        bool MblnPrintEmailPermission;
        bool FlagLoop = false;
        int MintSearchIndex = 0;

        private MessageBoxIcon MmessageIcon;
        private ArrayList MsarMessageArr;
        string MstrMessCommon = "";


        private clsBLLAttendanceManual MobjClsAttendanceManual;
        private ClsNotification MobjNotification;
        private ClsLogWriter MobjLog;
        private clsConnection MobjClsConnection;

        int intCurrentPage;
        int intPageCount;
        int intRowCount;
        bool blnNotFromButtonClick;
        string strCurrentShift;
        bool IsWorkLocationCurrent;
        #endregion

        #region Properties
        //private clsConnection Connetion { get; set; }
        //private ClsNotification MobjNotification { get; set; }
        //private ClsLogWriter MobjLog { get; set; }
        //private clsBLLAttendanceManual MobjClsAttendanceManual { get; set; }
        //private clsDTOAttendanceManual objclsDTOAttendanceManual { get; set; }

        #endregion

        #region Constructor
        public void New()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            tmrClear.Interval = (ClsCommonSettings.TimerInterval > 0 ? ClsCommonSettings.TimerInterval : 2000);
            // Add any initialization after the InitializeComponent() call.
        }

        public FrmAttendanceManual()
        {
            InitializeComponent();
            
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
       
        }
        #endregion


        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.AttendanceManual, this);
        }
        #endregion SetArabicControls



        #region Methods
        //private void SetPermissions()
        //{
        //    //Permission Settings
        //    try
        //    {
        //        ClsPermissions objPermission = new ClsPermissions();
        //        objPermission.GetPermissions(GlUserId, this.Name, MblnAddPermission, MblnUpdatePermission, MblnDeletePermission);
        //        if (MblnAddPermission = True || MblnUpdatePermission = True)
        //        {
        //            MblnViewPermission = True;
        //        }
        //        if (MblnAddPermission = True || MblnUpdatePermission = True)
        //        {
        //            MblnAddUpdatePermission = True;
        //        }
        //        btnSave.Enabled = MblnUpdatePermission;
        //        btnDelete.Enabled = MblnDeletePermission;
        //        btnShow.Enabled = MblnViewPermission;

        //    }
        //    catch (Exception ex)
        //    {
        //        MobjLog.WriteLog("Error on " && this.Name + " - Method : " + System.Reflection.MethodBase.GetCurrentMethod.Name + " - Error : " && ex.Message.ToString(), 1);
        //    }
        //}
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.Attendance, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            if (MblnAddPermission || MblnUpdatePermission || MblnAddUpdatePermission)
                MblnUpdatePermission = true;
            btnDelete.Enabled = MblnDeletePermission;
            btnSave.Enabled = MblnUpdatePermission;
            BtnPrint.Enabled = MblnPrintEmailPermission;


        }
        private void LoadMessage()
        {
            // Loading Message
            MsarMessageArr = new ArrayList();
            try
            {
                //if (ClsCommonSettings.payExists)
                MsarMessageArr = MobjNotification.FillMessageArray(121, 9);
                //else
                //    MsarMessageArr = MobjNotification.FillMessageArray(30, 10);


            }
            catch (Exception Ex)
            {
                MobjLog.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + ".Form-" + this.Name.ToString() + ".Error- " + Ex.Message.ToString(), 1);

            }
        }
        private void ClearAllControls()
        {
            //Clearing All Controls
            cboEmpSearch.SelectedIndex = -1;
            txtSearch.Text = "";
            cboCompany.SelectedIndex = -1;
            cboCompany.Text = "";
            dtpFromDate.Value = ClsCommonSettings.GetServerDate();
            cboEmpSearch.SelectedIndex = -1;
            txtSearch.Text = "";
            dgvAttendance.Rows.Clear();
            dgvAttendance.ColumnHeadersVisible = false;
            intPageCount = 0;
            intCurrentPage = 0;
            setPageNumberText();
        }
        private void ClearBottomPanel()
        {
            //Reset the bottom panel
            lblDayStatus.Visible = false;
            lblWorkTime.Text = "00:00:00";
            lblShortageTime.Text = "00:00:00";
            lblExcessTime.Text = "00:00:00";
            lblShiftName.Text = "";
            lblConseqDisplay.Text = "";
            lblConseqDisplay.Visible = false;
            lblConsequence.Visible = false;
            lblShift.Visible = false;
            lblShiftName.Visible = false;
        }
        //private void ShowCellInfo(int iRowindex, int iColumnIndex)
        //{
        //    //Showing cell info in bottom Panel

        //    if (dgvAttendance.Rows[iRowindex].Cells[iColumnIndex].Tag == null) { return; }
        //    string strDayInfo = dgvAttendance.Rows[iRowIndex].Cells[iColumnIndex].Tag;

        //    int iStatus = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceStatusID));
        //    if (iStatus == Convert.ToInt32(AttendanceStatus.Absent))
        //    {
        //        lblDayStatus.Text = "Status : Absent";
        //    }
        //    else if (iStatus == Convert.ToInt32(AttendanceStatus.Present))
        //    {
        //        lblDayStatus.Text = "Status : Present";
        //    }

        //    else if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
        //    {
        //        lblDayStatus.Text = "Status : Leave";
        //    }
        //    else if (iStatus == Convert.ToInt32(AttendanceStatus.Rest))
        //    {
        //        lblDayStatus.Text = "Status : Rest";
        //    }

        //    lblDayStatus.Visible = (iStatus > 0);
        //    string sOT = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.OT));
        //    string strWorkTime = dgvAttendance.Rows[iRowIndex].Cells[iColumnIndex].Value;
        //    if (strWorkTime != "H" && strWorkTime != "")
        //    {
        //        strWorkTime = GetDurationInMinutes(strWorkTime);
        //        strWorkTime = Math.Abs(Convert.ToDouble(strWorkTime) - Convert.ToDouble(sOT));
        //        strWorkTime = DateAdd(DateInterval.Minute, Convert.ToDouble(strWorkTime), DateTime.Today);
        //        strWorkTime = Convert.ToDateTime(strWorkTime).ToString("HH:mm");
        //        lblWorkTime.Text = strWorkTime;
        //    }
        //    else
        //    {
        //        lblWorkTime.Text = "00:00";
        //    }

        //    sOT = DateAdd(DateInterval.Minute, Convert.ToDouble(sOT), DateTime.Today);
        //    sOT = Convert.ToDateTime(sOT).ToString("HH:mm");
        //    string sAbsent = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AbsentTime));
        //    sAbsent = DateAdd(DateInterval.Minute, Convert.ToDouble(sAbsent), DateTime.Today);
        //    sAbsent = Convert.ToDateTime(sAbsent).ToString("HH:mm");
        //    lblExcessTime.Text = sOT;
        //    lblShortageTime.Text = sAbsent;
        //    string strShiftInfo = "";
        //    GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.ShiftName), strShiftInfo);

        //    lblShiftName.Text = strShiftInfo;
        //    lblShiftName.Visible = true;
        //    lblShift.Visible = true;
        //}
        private void ShowCellInfo(int iRowindex, int iColumnIndex)
        {
            //Showing cell info in bottom Panel

            if (dgvAttendance.Rows[iRowindex].Cells[iColumnIndex].Tag == null)
            {
                return;
            }
            string strDayInfo = dgvAttendance.Rows[iRowindex].Cells[iColumnIndex].Tag.ToStringCustom();

            int iStatus = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceStatusID), out strCurrentShift);
            int iIsHalfdayLeave = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.HalfdayLeave), out strCurrentShift);







            if (iStatus == Convert.ToInt32(AttendanceStatus.Absent))
            {
                lblDayStatus.Text = "Status : Absent";
            }
            else if (iStatus == Convert.ToInt32(AttendanceStatus.Present))
            {
                lblDayStatus.Text = "Status : Present";
            }
            else if (iStatus == Convert.ToInt32(AttendanceStatus.Leave))
            {
                if (iIsHalfdayLeave == 1)
                {
                    lblDayStatus.Text = "Status : Half Day Leave";
                }
                else
                {
                    lblDayStatus.Text = "Status : Leave";
                }
            }
            else if (iStatus == Convert.ToInt32(AttendanceStatus.Rest))
            {
                lblDayStatus.Text = "Status : Rest";
            }


            lblDayStatus.Visible = (iStatus > 0);
            string sOT = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.OT), out strCurrentShift).ToStringCustom();
            string strWorkTime = dgvAttendance.Rows[iRowindex].Cells[iColumnIndex].Value.ToStringCustom();
            if (strWorkTime != "H" && !string.IsNullOrEmpty(strWorkTime))
            {
                strWorkTime = GetDurationInMinutes(strWorkTime).ToStringCustom();
                strWorkTime = Math.Abs(Convert.ToDouble(strWorkTime) - Convert.ToDouble(sOT)).ToStringCustom();
                //strWorkTime = DateAdd(DateInterval.Minute, Convert.ToDouble(strWorkTime), DateTime.Today);
                strWorkTime = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(strWorkTime)).ToStringCustom();
                strWorkTime = Convert.ToDateTime(strWorkTime).ToString("HH:mm");
                lblWorkTime.Text = strWorkTime;
            }
            else
            {
                lblWorkTime.Text = "00:00";
            }


            //  IIf(, strWorkTime, "00:00")



            //sOT = DateAdd(DateInterval.Minute, Convert.ToDouble(sOT), DateTime.Today);
            sOT = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(sOT)).ToStringCustom();
            sOT = Convert.ToDateTime(sOT).ToString("HH:mm");
            string sAbsent = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AbsentTime), out strCurrentShift).ToStringCustom();
            //sAbsent = DateAdd(DateInterval.Minute, Convert.ToDouble(sAbsent), DateTime.Today);
            sAbsent = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(sAbsent)).ToStringCustom();
            sAbsent = Convert.ToDateTime(sAbsent).ToString("HH:mm");
            lblExcessTime.Text = sOT;
            lblShortageTime.Text = sAbsent;
            string strShiftInfo = "";
            GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.ShiftName), out strShiftInfo);

            lblShiftName.Text = strShiftInfo;
            lblShiftName.Visible = true;
            lblShift.Visible = true;
        }
        private void InitializeProgressBar(int iRowCount)
        {
            //Initializing the progress bar
            TlSProgressBarAttendance.Visible = true;
            TlSProgressBarAttendance.Value = 0;
            TlSProgressBarAttendance.Minimum = 0;
            if (iRowCount > 0)
            {
                TlSProgressBarAttendance.Maximum = iRowCount + 1;
            }
        }
        private void ResetProgressBar()
        {
            //Reset the progressbar
            TlSProgressBarAttendance.Value = TlSProgressBarAttendance.Maximum;
            TlSProgressBarAttendance.Visible = false;
            TlSProgressBarAttendance.Value = 0;
            TlSProgressBarAttendance.Minimum = 0;
            TlSProgressBarAttendance.Maximum = 0;
        }
        private void AutoFillAllEmployees()
        {
            int iProgressMaximum = dgvAttendance.RowCount * DateTime.DaysInMonth(dtpFromDate.Value.Year, dtpFromDate.Value.Month);
            InitializeProgressBar(iProgressMaximum);

            foreach (DataGridViewRow DgvRow in dgvAttendance.Rows)
            {
                AutoFill(DgvRow.Index);
                dgvAttendance.Refresh();
                TlSProgressBarAttendance.Value += 1;
            }
            ResetProgressBar();
            Cursor.Current = Cursors.Default;
        }
        private void SetEmployeeInfoInTag(int iRowIndex, int iColumIndex, int intStatus)
        {
            //Setting the particular day info to the Cell
            if (iRowIndex > -1 && (iColumIndex > colDgvEmployeeID.Index && iColumIndex < colDgvCompanyID.Index))
            {
                string strDayInfo = "AttendanceID =0@" +
                                        "AttendanceStatusID =0@" +
                                        "OT = 0@" +
                                        "ShiftID = 0@" +
                                        "LeaveTypeID = 0@" +
                                        "HalfdayLeave = 0@" +
                                        "PaidLeave = 0@" +
                                        "LeaveID = 0@" +
                                        "ConsequenceID = 0@" +
                                        "AbsentTime = 0@" +
                                        "MinWorkingHours = 0@" +
                                        "IsHoliday = 0@" +
                                        "IsOnLeave = 0@" +
                                        "IsAtnAutoExists =0@" +
                                        "ShiftName = 0  " +
                                        "WorkLocation = 0@"; 

                if (dgvAttendance.Rows[iRowIndex].Cells[iColumIndex].Tag == null)
                {
                    dgvAttendance.Rows[iRowIndex].Cells[iColumIndex].Tag = strDayInfo;
                }
                strDayInfo = dgvAttendance.Rows[iRowIndex].Cells[iColumIndex].Tag.ToStringCustom();
                string strCurrentShift;
                int intShiftId = GetTagInfo(dgvAttendance.Rows[iRowIndex].Cells[iColumIndex].Tag.ToStringCustom(), Convert.ToInt32(TagInfo.ShiftID), out strCurrentShift);
                string strDate = dgvAttendance.Columns[iColumIndex].Name + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year;
                int iMinWrkHrs = 0;
                int iWorkID = 0;
                string strTemp;
                if (intShiftId <= 0)
                {
                    intShiftId = this.GetShift(dgvAttendance.Rows[iRowIndex].Cells["colDgvEmployeeID"].Tag.ToInt32(), strDate, out iMinWrkHrs, out strTemp).ToInt32();
                }
                else
                {
                    iMinWrkHrs = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.MinWorkingHours), out strCurrentShift);
                }
                strDayInfo = SetTagInfo(strDayInfo, intShiftId.ToString(), Convert.ToInt32(TagInfo.ShiftID));
                strDayInfo = SetTagInfo(strDayInfo, intStatus.ToString(), Convert.ToInt32(TagInfo.AttendanceStatusID));
                strDayInfo = SetTagInfo(strDayInfo, iMinWrkHrs.ToString(), Convert.ToInt32(TagInfo.MinWorkingHours));
                //strDayInfo = SetTagInfo(strDayInfo, iWorkID.ToString(), Convert.ToInt32(TagInfo.WorkLocation));
                dgvAttendance.Rows[iRowIndex].Cells[iColumIndex].Tag = strDayInfo;

            }
        }
        private void SetStatusButtonVisibilty(int iRowIndex, int iColumnIndex)
        {
            //'Setting the Attendance Status(present,rest,leave,absent) buttons visibility
            if ((iColumnIndex > colDgvEmployeeID.Index) && (iColumnIndex < colDgvCompanyID.Index))
            {

                int iType = 0;
                btnAbsent.Enabled = true;
                btnPresent.Enabled = true;
                btnLeave.Enabled = true;
                btnRest.Enabled = true;
                int iLeaveID = 0;
                int iEmpID = dgvAttendance.Rows[iRowIndex].Cells["colDgvEmployeeID"].Tag.ToInt32();
                int iAttendanceID = 0;
                string strDate = Convert.ToDateTime(dgvAttendance.Columns[iColumnIndex].Name.Trim() + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year).ToString("dd MMM yyyy");
                string strDayInfo = dgvAttendance.Rows[iRowIndex].Cells[iColumnIndex].Tag.ToStringCustom();
                iAttendanceID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceID), out strCurrentShift);

                if (IsCellEditable(iRowIndex, iColumnIndex, out iType) == false)
                {
                    if (iType == Convert.ToInt32(CellEditableTypes.Holiday))
                    {
                        btnAbsent.Enabled = false;
                        btnLeave.Enabled = false;
                        btnRest.Enabled = false;
                    }
                    else if (iType == Convert.ToInt32(CellEditableTypes.HalfDayLeave) || iType == Convert.ToInt32(CellEditableTypes.FullDayLeave))
                    {
                        iLeaveID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.LeaveID), out strCurrentShift);
                        if (iLeaveID <= 0)
                        {
                            btnAbsent.Enabled = false;
                            btnPresent.Enabled = false;
                            btnLeave.Enabled = false;
                            btnRest.Enabled = false;
                        }
                        else if (iType == Convert.ToInt32(CellEditableTypes.AttendanceAutoExists) || iType == Convert.ToInt32(CellEditableTypes.SalaryIsReleased) || iType == Convert.ToInt32(CellEditableTypes.VacationExists))
                        {
                            btnAbsent.Enabled = false;
                            btnPresent.Enabled = false;
                            btnLeave.Enabled = false;
                            btnRest.Enabled = false;
                        }
                        if (btnAbsent.Enabled)// 'if time exists in a particular day then status cannot be absent
                        {
                            if ((MobjClsAttendanceManual.IsWorkTimeExists(iEmpID, strDate, iAttendanceID)))
                            {
                                btnAbsent.Enabled = false;
                            }
                        }
                    }
                }
            }
        }
        private bool LoadCombo(int iType)
        {
            //'Loading Combo
            DataTable datCombos;
            if (iType == 0 || iType == 1)
            {
                //'Load Comapny Combo
                datCombos = MobjClsAttendanceManual.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;
                datCombos = null;
                cboCompany.SelectedValue = (ClsCommonSettings.CurrentCompanyID > 0 ? ClsCommonSettings.CurrentCompanyID : 0);

            }
            if (iType == 0 || iType == 2)
            {
                //'Load work location Combo
                datCombos = MobjClsAttendanceManual.FillCombos(new string[] { "WorkLocationID,LocationName", "WorkLocationReference", "" });
                cboLocation.ValueMember = "WorkLocationID";
                cboLocation.DisplayMember = "LocationName";
                //DataRow dr = datCombos.NewRow();
                //dr["WorkLocationID"] = 0;
                //dr["LocationName"] = "ALL";
                //datCombos.Rows.InsertAt(dr, 0);
                cboLocation.DataSource = datCombos;
                datCombos = null;

            }
            return true;
        }
        private string GetStratDate()
        {
            //'Getting Start date
            string sMonth = "";
            switch (dtpFromDate.Value.Month)
            {
                case 1:
                    sMonth = "Jan";
                    break;
                case 2:
                    sMonth = "Feb";
                    break;
                case 3:
                    sMonth = "Mar";
                    break;
                case 4:
                    sMonth = "Apr";
                    break;
                case 5:
                    sMonth = "May";
                    break;
                case 6:
                    sMonth = "Jun";
                    break;
                case 7:
                    sMonth = "Jul";
                    break;
                case 8:
                    sMonth = "Aug";
                    break;
                case 9:
                    sMonth = "Sep";
                    break;
                case 10:
                    sMonth = "Oct";
                    break;
                case 11:
                    sMonth = "Nov";
                    break;
                case 12:
                    sMonth = "Dec";
                    break;
            }
            return Convert.ToDateTime("01-" + sMonth + "-" + dtpFromDate.Value.Year).ToString("dd MMM yyyy");
        }
        private void ResetGrid()
        {
            //'Resetting the Grid
            dgvAttendance.Rows.Clear();
            dgvAttendance.ColumnHeadersVisible = false;
            if (dgvAttendance.Columns["colDgvCompanyID"] != null)
            {
                int iColIndex = dgvAttendance.Columns["colDgvCompanyID"].Index - 1;
                int iLimit = 0;
                iLimit = colDgvEmployeeID.Index + 1;
                for (int iCounter = iColIndex; iCounter >= iLimit; iCounter--)
                {
                    dgvAttendance.Columns.RemoveAt(iCounter);
                    dgvAttendance.Columns[iCounter].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }
        //private bool InitilizeGrid()
        //{
        //    //'Initializing the grid with date columns
        //    try
        //    {
        //        dgvAttendance.ColumnHeadersVisible = true;
        //        int iDaycount = 30;
        //        DateTime dpDate;

        //        dpDate = GetStratDate().ToDateTime();
        //        iDaycount = DateDiffDay(dpDate, dpDate.AddMonths(1));

        //        int iColIndex = dgvAttendance.Columns["colDgvCompanyID"].Index - 1;
        //        int iLimit = 0;
        //        iLimit = colDgvEmployeeID.Index + 1;
        //        for (int iCounter = iColIndex; iCounter >= iLimit; iCounter--)
        //        {
        //            dgvAttendance.Columns.RemoveAt(iCounter);
        //            dgvAttendance.Columns[iCounter].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        //        }

        //        int iStart = dgvAttendance.Columns["colDgvEmployeeID"].Index + 1;
        //        iLimit = 1;
        //        for (int z = iStart; z <= iDaycount; z++)
        //        {
        //            int iLoc = z;
        //            DataGridViewTextBoxColumn adcolDay = new DataGridViewTextBoxColumn();

        //            string strDayID = iLimit.ToString().Trim();
        //            strDayID = (strDayID.Length > 1 ? strDayID.Trim() : ("0" + strDayID).Trim());
        //            new adcolDay
        //            {
        //                Name = iLimit.ToString(),
        //                HeaderText = strDayID,
        //                SortMode = DataGridViewColumnSortMode.NotSortable,
        //                Width = 65,
        //                ToolTipText = strDayID + " " + dtpFromDate.Text
        //            };
        //            dgvAttendance.Columns.Insert(iLoc, adcolDay);
        //            iLimit += 1;
        //        }
        //        dgvAttendance.Columns["colDgvEmployeeID"].Width = 260;
        //        dgvAttendance.Columns["colDgvEmployeeID"].MinimumWidth = 260;
        //        dgvAttendance.Columns["colDgvEmployeeID"].Resizable = DataGridViewTriState.False;
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        MobjLog.WriteLog("Error on " + Me.Name + " - Method : " + System.Reflection.MethodBase.GetCurrentMethod.Name & " - Error : " & ex.Message.ToString(), 1);
        //        return false;
        //    }
        //}
        private bool InitilizeGrid()
        {
            //Initializing the grid with date columns

            try
            {
                dgvAttendance.ColumnHeadersVisible = true;
                int iDaycount = 30;
                System.DateTime dpDate = default(System.DateTime);

                dpDate = GetStratDate().ToDateTime();
                //iDaycount =DateDiffDay(dpDate, dpDate.AddMonths(1));
                iDaycount = (dpDate.AddMonths(1) - dpDate).TotalDays.ToInt32();
                int iColIndex = dgvAttendance.Columns["colDgvCompanyID"].Index - 1;
                int iLimit = 0;
                iLimit = colDgvEmployeeID.Index + 1;
                for (int iCounter = iColIndex; iCounter >= iLimit; iCounter += -1)
                {
                    dgvAttendance.Columns.RemoveAt(iCounter);
                    dgvAttendance.Columns[iCounter].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                int iStart = dgvAttendance.Columns["colDgvEmployeeID"].Index + 1;
                iLimit = 1;
                for (int z = iStart; z <= iDaycount; z++)
                {
                    int iLoc = z;
                    DataGridViewTextBoxColumn adcolDay = new DataGridViewTextBoxColumn();

                    string strDayID = iLimit.ToString().Trim();
                    strDayID = (strDayID.Length > 1 ? strDayID.Trim() : ("0" + strDayID).Trim());
                    {
                        adcolDay.Name = iLimit.ToString();
                        adcolDay.HeaderText = strDayID;
                        adcolDay.SortMode = DataGridViewColumnSortMode.NotSortable;
                        adcolDay.Width = 65;
                        adcolDay.ToolTipText = strDayID + " " + dtpFromDate.Text;
                    }
                    dgvAttendance.Columns.Insert(iLoc, adcolDay);
                    iLimit += 1;
                }
                
                dgvAttendance.Columns["colDgvEmployeeID"].Width = 260;
                dgvAttendance.Columns["colDgvEmployeeID"].MinimumWidth = 260;
                dgvAttendance.Columns["colDgvEmployeeID"].Resizable = DataGridViewTriState.False;
                return true;
            }
            catch (Exception ex)
            {
                MobjLog.WriteLog("Error on " + this.Name + " - Method : " + System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error : " + ex.Message.ToString(), 1);
                return false;
            }
        }
        private int GetDurationInMinutes(string sDuration)
        {// 'Getting Duration In minutes
            int iDurationInMinutes = 0;
            try
            {
                string[] sHourMinute;
                if (sDuration.Contains("."))
                {
                    sHourMinute = sDuration.Split(Convert.ToChar("."));
                    iDurationInMinutes = sHourMinute[0].ToInt32() * 60 + sHourMinute[1].ToInt32();
                }
                else if (sDuration.Contains(":"))
                {
                    sHourMinute = sDuration.Split(Convert.ToChar(":"));
                    iDurationInMinutes = sHourMinute[0].ToInt32() * 60 + sHourMinute[1].ToInt32();
                }
                else if (sDuration != "")
                {

                    iDurationInMinutes = (Convert.ToInt32(sDuration) * 60l).ToInt32();
                }
                return iDurationInMinutes;
            }
            catch (Exception ex)
            {
                MobjLog.WriteLog("Error on " + this.Name + " - Method : " + System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error : " + ex.Message.ToString(), 1);
                return iDurationInMinutes;
            }
        }

        private string GetHourFromMinutes(int iTime)
        {
            //getting Hours from minutes
            string strTime = "00:00";
            string strHR = "00";
            string strMin = "00";
            if (iTime >= 60)
            {
                strHR = Convert.ToString(iTime / 60);
                string[] strSplitTime = strHR.Split('.');
                strHR = strSplitTime[0];
                strMin = (iTime % 60).ToString();
            }
            else
            {
                strMin = iTime.ToString();
            }

            if (strHR.Length == 1)
            {
                strHR = "0" + strHR;
            }
            if (strMin.Length == 1)
            {
                strMin = "0" + strMin;
            }
            strTime = strHR + ":" + strMin;
            return strTime;
        }
        private bool FormValidations()
        {
            //Form validations
            if (cboLocation.SelectedIndex <= -1)
            {
                return false;
            }

            if (cboCompany.SelectedIndex <= -1)
            {
                MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 14, out  MmessageIcon);
                lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                tmrClear.Enabled = true;
                return false;
            }

            return true;
        }
        private object DispalyAttendanceInfo()
        {
            //Display The Employees to grid

            try
            {
                if (FormValidations() == false)
                    return false;


                bool blnRetValue = false;
                InitilizeGrid();

                MobjClsAttendanceManual.clsDTOAttendanceManual.intCompanyID = cboCompany.SelectedValue.ToInt32();
                MobjClsAttendanceManual.clsDTOAttendanceManual.intWorkLocationID = cboLocation.SelectedValue.ToInt32();

                MobjClsAttendanceManual.clsDTOAttendanceManual.strDate = dtpFromDate.Value.ToString("dd MMM yyyy");
                intPageCount = MobjClsAttendanceManual.GetAttendanceDetailsPageCount(intCurrentPage, intRowCount).Rows[0]["EmpCount"].ToInt32();
                setPageNumberText();
                DataTable dtEmployeeAttendance = MobjClsAttendanceManual.GetAttendanceDetails(intCurrentPage, intRowCount);

                //dgvAttendance.RowCount = 0;
                dgvAttendance.Rows.Clear();

                if (dtEmployeeAttendance.Rows.Count > 0)
                {
                    InitializeProgressBar(dtEmployeeAttendance.Rows.Count);
                    //TlSProgressBarAttendance.Visible = True
                    //TlSProgressBarAttendance.Value = 0
                    //TlSProgressBarAttendance.Minimum = 0
                    //TlSProgressBarAttendance.Maximum = dtEmployeeAttendance.Rows.Count + 1

                    //dtEmployeeAttendance.AsEnumerable ().Select(t=>t.Field<Int32>("EmployeeID").Distinct();

                    var EmpIDs = (from DataRow r in dtEmployeeAttendance.AsEnumerable() orderby r["EmployeeID"] ascending select r.Field<Int32>("EmployeeID")).Distinct();

                    //var obj = dtEmployeeAttendance; //dt.AsEnumerable().Where(p => p.Field<int>("PlanTypeID") == cboPlanType.SelectedValue.ToInt32());




                    //object EmpIDs = (from r in dtEmployeeAttendance.AsEnumerable() orderby r["EmployeeID"] ascending select r("EmployeeID")).Distinct();
                    //var ObjEmpIDs = dtEmployeeAttendance.AsEnumerable ().Select(t=>t.Field<Int32>("EmployeeID")).Distinct();
                    //DataTable EmpIDs = ObjEmpIDs.tod;
                    //DataRow[] EmpIDs =dtEmployeeAttendance.Select("")
                    FlagLoop = true;

                    while (FlagLoop)
                    {


                        for (int iCounter = 0; iCounter <= EmpIDs.Count() - 1; iCounter++)
                        {


                            //foreach (DataRow dr in EmpIDs)
                            //{

                            int iEmpID = EmpIDs.ElementAt(iCounter);//Convert.ToInt32(dr["EmployeeID"]);
                            dtEmployeeAttendance.DefaultView.RowFilter = "EmployeeID =" + (iEmpID).ToString();
                            if (dtEmployeeAttendance.DefaultView.ToTable().Rows.Count > 0)
                            {
                                dgvAttendance.RowCount = dgvAttendance.RowCount + 1;
                                int iRowIndex = dgvAttendance.RowCount - 1;
                                if (FlagLoop == false)
                                {
                                    return false;
                                }
                                dgvAttendance.Rows[iRowIndex].Cells["colDgvEmployeeID"].Value = dtEmployeeAttendance.DefaultView.ToTable().Rows[0]["Employee"];
                                dgvAttendance.Rows[iRowIndex].Cells["colDgvEmployeeID"].Tag = dtEmployeeAttendance.DefaultView.ToTable().Rows[0]["EmployeeID"];
                                dgvAttendance.Rows[iRowIndex].Cells["colDgvPolicyID"].Value = dtEmployeeAttendance.DefaultView.ToTable().Rows[0]["PolicyID"];
                                dgvAttendance.Rows[iRowIndex].Cells["colDgvCompanyID"].Value = dtEmployeeAttendance.DefaultView.ToTable().Rows[0]["CompanyID"];
                                dgvAttendance.Rows[iRowIndex].Cells["colDgvOTDays"].Value = dtEmployeeAttendance.DefaultView.ToTable().Rows[0]["OTDays"];
                                dgvAttendance.Rows[iRowIndex].Cells["colDgvOTHour"].Value = dtEmployeeAttendance.DefaultView.ToTable().Rows[0]["OTHour"];


                                int iWorkTime = 0;
                                foreach (DataRow dataRow in dtEmployeeAttendance.DefaultView.ToTable().Rows)
                                {
                                    int intDayID = dataRow["DayID"].ToInt32();
                                    string strDayInfo = "";

                                    if (intDayID > 0)
                                    {

                                        strDayInfo = " AttendanceID =" + dataRow["AttendanceID"] + "@" + "AttendanceStatusID = " + dataRow["AttendenceStatusID"] + "@" + "OT = " + GetDurationInMinutes(dataRow["OT"].ToStringCustom()) + "@" + "ShiftID = " + dataRow["ShiftID"] + "@" + "LeaveTypeID =" + dataRow["LeaveTypeID"] + "@" + "HalfdayLeave = " + dataRow["IsHalfdayLeave"] + "@" + "PaidLeave = " + dataRow["IsPaidLeave"] + "@" + "LeaveID = " + dataRow["LeaveID"] + "@" + "ConsequenceID = " + dataRow["ConsequenceID"] + "@" + "AbsentTime =" + dataRow["AbsentTime"] + "@" + "MinWorkingHours = " + GetDurationInMinutes(dataRow["MinWorkHrs"].ToStringCustom()) + "@" + "IsHoliday = " + dataRow["IsHoliday"] + "@" + "IsOnLeave = " + dataRow["IsOnLeave"] + "@" + "IsAtnAutoExists =" + dataRow["IsAtnAutoExists"] + "@" + "ShiftName = " + dataRow["ShiftName"] + "@" + "WorkLocationID =" + dataRow["WorkLocationID"] + " ";

                                        string sWrkTime = "00:00";
                                        if (!string.IsNullOrEmpty(dataRow["WorkTime"].ToStringCustom()))
                                        {
                                            iWorkTime = GetDurationInMinutes(dataRow["WorkTime"].ToStringCustom());
                                            iWorkTime = Math.Abs(iWorkTime + GetDurationInMinutes(dataRow["OT"].ToStringCustom()));
                                            //sWrkTime = DateAdd(DateInterval.Minute, Convert.ToDouble(iWorkTime), DateTime.Today);
                                            sWrkTime = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(iWorkTime)).ToStringCustom();
                                            sWrkTime = Convert.ToDateTime(sWrkTime).ToString("HH:mm");
                                            dgvAttendance.Rows[iRowIndex].Cells[intDayID.ToString().Trim()].Value = sWrkTime;
                                            //dataRow("WorkTime") '

                                        }

                                        dgvAttendance.Rows[iRowIndex].Cells[intDayID.ToString().Trim()].Tag = strDayInfo;
                                        TlSProgressBarAttendance.Value += 1;
                                        blnRetValue = true;

                                        if (Convert.ToInt32(dataRow["IsHoliday"]) > 0)
                                        {
                                            dgvAttendance.Rows[iRowIndex].Cells[intDayID.ToString().Trim()].Style.ForeColor = Color.Red;
                                            if (string.IsNullOrEmpty(dgvAttendance.Rows[iRowIndex].Cells[intDayID.ToString().Trim()].Value.ToStringCustom()))
                                            {
                                                dgvAttendance.Rows[iRowIndex].Cells[intDayID.ToString().Trim()].Style.Alignment = DataGridViewContentAlignment.BottomRight;
                                                dgvAttendance.Rows[iRowIndex].Cells[intDayID.ToString().Trim()].Value = "H";
                                            }

                                        }

                                    }

                                }


                                dgvAttendance.CurrentCell = dgvAttendance[colDgvEmployeeID.Index, iRowIndex];
                                Application.DoEvents();
                                dgvAttendance.Refresh();


                                //--------------------Rows----------------------------------------------------
                            }
                        }
                        FlagLoop = false;
                        TlSProgressBarAttendance.Value = dtEmployeeAttendance.Rows.Count + 1;
                    }
                }


                FlagLoop = true;
                return blnRetValue;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private bool SaveAttendanceInfo()
        {
            //Saving the Attendance info

            try
            {
                if (dgvAttendance.Rows.Count > 0)
                {
                    dgvAttendance.CurrentCell = dgvAttendance[colDgvEmployeeID.Index, 0];
                }
            }
            catch (Exception ex)
            {
            }




            Cursor.Current = Cursors.WaitCursor;
            //TlSProgressBarAttendance.Visible = True
            //TlSProgressBarAttendance.Value = 0
            //TlSProgressBarAttendance.Minimum = 0
            // TlSProgressBarAttendance.Maximum = iProgressMaximum
            int iProgressMaximum = (dgvAttendance.RowCount * DateTime.DaysInMonth(dtpFromDate.Value.Year, dtpFromDate.Value.Month));
            // + 1
            InitializeProgressBar(iProgressMaximum);

            bool blnRetValue = false;

            //If FillParameters() And MobjClsAttendanceManual.SaveAttendanceManual() Then
            //    blnRetValue = True
            //End If
            if (SaveAttendanceManualNew())
            {
                blnRetValue = true;
            }
            //TlSProgressBarAttendance.Value = iProgressMaximum
            ResetProgressBar();
            Cursor.Current = Cursors.Default;
            return blnRetValue;
        }
        private bool SaveAttendanceManualNew()
        {
            GC.Collect();
            bool blnRetValue = false;
            DateTime dJoiningDate = default(DateTime);
            DateTime dtDates = default(DateTime);
            int iWorkLocationSel = Convert.ToInt32(cboLocation.SelectedValue);    
            decimal dOTDays = 0.0M;
            string sOTHr = "";

            foreach (DataGridViewRow DgvRow in dgvAttendance.Rows)
            {
                int iEmpID = DgvRow.Cells["colDgvEmployeeID"].Tag.ToInt32();
                dJoiningDate = MobjClsAttendanceManual.GetEmployeeJoiningDate(iEmpID);

                for (int ICounter = colDgvEmployeeID.Index + 1; ICounter <= colDgvCompanyID.Index - 1; ICounter++)
                {
                    if (DgvRow.Cells[ICounter].Tag == null)
                        continue;
                    dtDates = Convert.ToDateTime(dgvAttendance.Columns[DgvRow.Cells[ICounter].ColumnIndex].Name.Trim() + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year);
                    string sCurrGridDate = dtDates.Date.ToString("dd MMM yyyy");
                    string sNowDate = ClsCommonSettings.GetServerDate().ToString("");

                    if (Convert.ToDateTime(sCurrGridDate) > Convert.ToDateTime(sNowDate))
                        continue;
                    //SysDate
                    if (dtDates.Date < dJoiningDate.Date)
                        continue;
                    //cheking of joining date 

                    string strDayInfo = DgvRow.Cells[ICounter].Tag.ToStringCustom();
                    int iAttendanceID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceID), out strCurrentShift);
                    int iAttendanceStatus = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceStatusID), out strCurrentShift);
                    int iShiftID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.ShiftID), out strCurrentShift);
                    int iIsHoliday = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.IsHoliday), out strCurrentShift);
                    int iWorkLocationCurrent = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.WorkLocation), out strCurrentShift);

                    if (iWorkLocationCurrent == 0 || iWorkLocationSel != iWorkLocationCurrent)
                    {
                        continue;
                    }


                    if ((DgvRow.Cells[ICounter].Value.ToStringCustom() == "00:00" && iIsHoliday == 1))
                    {
                        DgvRow.Cells[ICounter].Value = "H";
                        iAttendanceStatus = 0;
                    }
                    string sOT = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.OT), out strCurrentShift).ToStringCustom();
                    string sWrkTime = "00:00";
                    int iWrkTime = 0;
                    if (DgvRow.Cells[ICounter].Value == null || string.IsNullOrEmpty(DgvRow.Cells[ICounter].Value.ToStringCustom()) || DgvRow.Cells[ICounter].Value == "H")
                    {
                    }
                    else
                    {
                        sWrkTime = Convert.ToString(DgvRow.Cells[ICounter].Value);
                        iWrkTime = GetDurationInMinutes(sWrkTime);
                        if (iWrkTime == 0 && MobjClsAttendanceManual.IsWorkTimeExists(iEmpID, sCurrGridDate, iAttendanceID) == false && iAttendanceStatus == Convert.ToInt32(AttendanceStatus.Present))
                        {
                            iAttendanceStatus = Convert.ToInt32(AttendanceStatus.Absent);
                        }
                    }
                    if (Convert.ToDouble(sOT) > 0)
                    {
                        iWrkTime = Math.Abs(iWrkTime - Convert.ToDouble(sOT)).ToInt32();
                        //sWrkTime = DateAdd(DateInterval.Minute, Convert.ToDouble(iWrkTime), DateTime.Today);
                        sWrkTime = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(iWrkTime)).ToStringCustom();
                        sWrkTime = Convert.ToDateTime(sWrkTime).ToString("HH:mm");
                    }
                    //sOT = DateAdd(DateInterval.Minute, Convert.ToDouble(sOT), DateTime.Today);


                    sOT = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(sOT)).ToStringCustom();
                    sOT = Convert.ToDateTime(sOT).ToString("HH:mm");
                    int iLeaveTypeID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.LeaveTypeID), out strCurrentShift);
                    int iIsHalfdayLeave = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.HalfdayLeave), out strCurrentShift);
                    int iIsPaidLeave = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.PaidLeave), out strCurrentShift);
                    int iLeaveID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.LeaveID), out strCurrentShift);
                    int iConsequenceID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.ConsequenceID), out strCurrentShift);
                    int iAbsentTime = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AbsentTime), out strCurrentShift);


                    if (iAttendanceStatus > 0 || iAttendanceID > 0)
                    {
                        MobjClsAttendanceManual.SaveManualAttendanceNew(iAttendanceID, iEmpID, DgvRow.Cells["colDgvCompanyID"].Value.ToInt32(), sCurrGridDate, DgvRow.Cells["colDgvPolicyID"].Value.ToInt32(), iShiftID, sWrkTime, sOT, iAbsentTime, iAttendanceStatus,
                        iLeaveID, iLeaveTypeID, iIsHalfdayLeave, iIsPaidLeave, iConsequenceID, iWorkLocationSel);
                        TlSProgressBarAttendance.Value += 1;
                        StatusStripBottom.Refresh();
                        blnRetValue = true;
                    }


                }

                 dOTDays = 0; sOTHr = "";
                 if (DgvRow.Cells["colDgvOTDays"].Value == System.DBNull.Value)
                 {
                     dOTDays = 0;
                 }
                 else
                 {
                     dOTDays = Convert.ToDecimal(DgvRow.Cells["colDgvOTDays"].Value);
                 }

                 if (DgvRow.Cells["colDgvOTHour"].Value == System.DBNull.Value)
                 {
                     sOTHr = "";
                 }
                 else
                 {
                     sOTHr = Convert.ToString(DgvRow.Cells["colDgvOTHour"].Value);
                 }


                MobjClsAttendanceManual.SaveManualAttendanceOT(iEmpID, dtpFromDate.Value.ToString("dd-MMM-yyyy"), sOTHr, dOTDays);

            }
            return blnRetValue;
        }
        private bool FillParameters()
        {
            //Filling parameters for save

            //MobjClsAttendanceManual.AttendanceManualDetails = New List(Of ClsAttendanceManualDetails)

            GC.Collect();

            MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = "";
            bool blnRetValue = false;
            DateTime dJoiningDate = default(DateTime);
            DateTime dtDates = default(DateTime);

            MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = "<Root>";

            foreach (DataGridViewRow DgvRow in dgvAttendance.Rows)
            {
                int iEmpID = DgvRow.Cells["colDgvEmployeeID"].Tag.ToInt32();
                dJoiningDate = MobjClsAttendanceManual.GetEmployeeJoiningDate(iEmpID);

                for (int ICounter = colDgvEmployeeID.Index + 1; ICounter <= colDgvCompanyID.Index - 1; ICounter++)
                {
                    if (DgvRow.Cells[ICounter].Tag == null)
                        continue;
                    dtDates = Convert.ToDateTime(dgvAttendance.Columns[DgvRow.Cells[ICounter].ColumnIndex].Name.Trim() + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year);
                    //string sCurrGridDate = Strings.Format(dtDates.Date, "dd MMM yyyy");
                    string sCurrGridDate = dtDates.Date.ToString("dd MMM yyyy");
                    string sNowDate = ClsCommonSettings.GetServerDate().ToString("dd MMM yyyy");

                    if (Convert.ToDateTime(sCurrGridDate) > Convert.ToDateTime(sNowDate))
                        continue;
                    //SysDate
                    if (dtDates.Date < dJoiningDate.Date)
                        continue;
                    //cheking of joining date 

                    string strDayInfo = DgvRow.Cells[ICounter].Tag.ToStringCustom();
                    int iAttendanceID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceID), out strCurrentShift);
                    int iAttendanceStatus = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceStatusID), out strCurrentShift);
                    int iShiftID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.ShiftID), out strCurrentShift);

                    string sOT = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.OT), out strCurrentShift).ToStringCustom();
                    string sWrkTime = "00:00";
                    int iWrkTime = 0;
                    if (DgvRow.Cells[ICounter].Value == null || string.IsNullOrEmpty(DgvRow.Cells[ICounter].Value.ToStringCustom()) || DgvRow.Cells[ICounter].Value.ToStringCustom() == "H")
                    {
                    }
                    else
                    {
                        sWrkTime = Convert.ToString(DgvRow.Cells[ICounter].Value);
                        iWrkTime = GetDurationInMinutes(sWrkTime);
                        if (iWrkTime == 0 && MobjClsAttendanceManual.IsWorkTimeExists(iEmpID, sCurrGridDate, iAttendanceID) == false && iAttendanceStatus == Convert.ToInt32(AttendanceStatus.Present))
                        {
                            iAttendanceStatus = Convert.ToInt32(AttendanceStatus.Absent);
                        }
                    }
                    if (Convert.ToDouble(sOT) > 0)
                    {
                        iWrkTime = Math.Abs(iWrkTime - Convert.ToDouble(sOT)).ToInt32();
                        //sWrkTime = DateAdd(DateInterval.Minute, Convert.ToDouble(iWrkTime), DateTime.Today);
                        sWrkTime = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(iWrkTime)).ToStringCustom();
                        sWrkTime = Convert.ToDateTime(sWrkTime).ToString("HH:mm");
                    }


                    //sOT = DateAdd(DateInterval.Minute, Convert.ToDouble(sOT), DateTime.Today);
                    sOT = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(sOT)).ToStringCustom();
                    sOT = Convert.ToDateTime(sOT).ToString("HH:mm");
                    int iLeaveTypeID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.LeaveTypeID), out strCurrentShift);
                    int iIsHalfdayLeave = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.LeaveTypeID), out strCurrentShift);
                    int iIsPaidLeave = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.PaidLeave), out strCurrentShift);
                    int iLeaveID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.LeaveID), out strCurrentShift);
                    int iConsequenceID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.ConsequenceID), out strCurrentShift);
                    int iAbsentTime = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AbsentTime), out strCurrentShift);
                    int iProjectID = DgvRow.Cells["colDgvProjectID"].Value.ToInt32();


                    if (iAttendanceStatus > 0 || iAttendanceID > 0)
                    {
                        MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc + "<ClsAttendanceManualDetails> " + "<AttendanceID>" + iAttendanceID + " </AttendanceID>" + "<EmployeeID>" + iEmpID + " </EmployeeID>" + "<CompanyID>" + DgvRow.Cells["colDgvCompanyID"].Value + "</CompanyID>\t" + "<Date>" + sCurrGridDate + "</Date>" + "<PolicyID>" + DgvRow.Cells["colDgvPolicyID"].Value + "</PolicyID>" + "<ShiftID>" + iShiftID + "</ShiftID>\t\t" + "<WorkTime>" + sWrkTime + "</WorkTime>" + "<OT>" + sOT + "</OT>" + "<AttendenceStatusID>" + iAttendanceStatus + "</AttendenceStatusID>" + "<ProjectID>" + iProjectID + "</ProjectID>\t\t" + "<LeaveID>" + iLeaveID + "</LeaveID>" + "<LeaveType>" + iLeaveTypeID + "</LeaveType>" + "<IsHalfDayLeave>" + iIsHalfdayLeave + "</IsHalfDayLeave>" + "<IsPaidLeave>" + iIsPaidLeave + "</IsPaidLeave>" + "<ConsequenceID>" + iConsequenceID + "</ConsequenceID>" + "<AbsentTime>" + iAbsentTime + "</AbsentTime> </ClsAttendanceManualDetails>\t";
                        blnRetValue = true;
                    }

                    TlSProgressBarAttendance.Value += 1;
                    StatusStripBottom.Refresh();
                }
            }
            MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc + "</Root>";
            return blnRetValue;
        }
        private bool FillDeleteParameters(bool blnKeyDel)
        {
            //Filling parametrs for delete
            //  MobjClsAttendanceManual.AttendanceManualDetails = New List(Of ClsAttendanceManualDetails)
            bool blnRetValue = false;
            MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = "";
            MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = "<Root>";

            MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10093, out  MmessageIcon);
            MstrMessCommon = MstrMessCommon.Substring(MstrMessCommon.IndexOf("#") + 1);
            //MstrMessCommon = MstrMessCommon.Replace("edit", "delete")


            //foreach (DataGridViewRow DgvRow in ((blnKeyDel ? dgvAttendance.SelectedRows : dgvAttendance.Rows)))
            //{
            //    int iEmpID = DgvRow.Cells["colDgvEmployeeID"].Tag.ToInt32();
            //    string strDate = GetStratDate();
            //    if (MobjClsAttendanceManual.IsSalaryReleased(iEmpID, strDate))
            //    {
            //        lblAttendanceStatus.Text = MstrMessCommon;
            //        continue;
            //    }



            //    for (int ICounter = colDgvEmployeeID.Index + 1; ICounter <= colDgvCompanyID.Index - 1; ICounter++)
            //    {

            //        if (DgvRow.Cells[ICounter].Tag == null)
            //            continue;

            //        string strDayInfo = DgvRow.Cells[ICounter).Tag;
            //        int iAttendanceID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceID),"");
            //        int iAttendanceStatus = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceStatusID),"");
            //        int iLeaveID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.LeaveID),"");
            //        if (iAttendanceID > 0)
            //        {
            //            MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc + "<ClsAttendanceManualDetails> " + "<AttendanceID>" + iAttendanceID + " </AttendanceID>" + "<LeaveID>" + iLeaveID + "</LeaveID>" + "</ClsAttendanceManualDetails>\t";

            //            blnRetValue = true;
            //        }
            //        TlSProgressBarAttendance.Value += 1;
            //        StatusStripBottom.Refresh();
            //    }
            //}
            if (blnKeyDel)
            {
                foreach (DataGridViewRow DgvRow in (dgvAttendance.SelectedRows))
                {
                    int iEmpID = DgvRow.Cells["colDgvEmployeeID"].Tag.ToInt32();
                    string strDate = GetStratDate();
                    if (MobjClsAttendanceManual.IsSalaryReleased(iEmpID, strDate))
                    {
                        lblAttendanceStatus.Text = MstrMessCommon;
                        continue;
                    }



                    for (int ICounter = colDgvEmployeeID.Index + 1; ICounter <= colDgvCompanyID.Index - 1; ICounter++)
                    {

                        if (DgvRow.Cells[ICounter].Tag == null)
                            continue;

                        string strDayInfo = DgvRow.Cells[ICounter].Tag.ToStringCustom();
                        int iAttendanceID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceID), out strCurrentShift);
                        int iAttendanceStatus = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceStatusID), out strCurrentShift);
                        int iLeaveID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.LeaveID), out strCurrentShift);
                        int iWorkLocationID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.WorkLocation), out strCurrentShift);

                        if (iAttendanceID > 0)
                        {
                            MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc + "<ClsAttendanceManualDetails> " + "<AttendanceID>" + iAttendanceID + " </AttendanceID>" + "<LeaveID>" + iLeaveID + "</LeaveID>" + "<WorkLocationID>" + iWorkLocationID + " </WorkLocationID>" + "</ClsAttendanceManualDetails>\t";

                            blnRetValue = true;
                        }
                        TlSProgressBarAttendance.Value += 1;
                        StatusStripBottom.Refresh();
                    }
                }
            }
            else
            {
                foreach (DataGridViewRow DgvRow in (dgvAttendance.Rows))
                {
                    int iEmpID = DgvRow.Cells["colDgvEmployeeID"].Tag.ToInt32();
                    string strDate = GetStratDate();
                    if (MobjClsAttendanceManual.IsSalaryReleased(iEmpID, strDate))
                    {
                        lblAttendanceStatus.Text = MstrMessCommon;
                        continue;
                    }



                    for (int ICounter = colDgvEmployeeID.Index + 1; ICounter <= colDgvCompanyID.Index - 1; ICounter++)
                    {

                        if (DgvRow.Cells[ICounter].Tag == null)
                            continue;

                        string strDayInfo = DgvRow.Cells[ICounter].Tag.ToStringCustom();
                        int iAttendanceID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceID), out strCurrentShift);
                        int iAttendanceStatus = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceStatusID), out strCurrentShift);
                        int iLeaveID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.LeaveID), out strCurrentShift);
                        int iWorkLocationID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.WorkLocation), out strCurrentShift);
                        if (iAttendanceID > 0)
                        {
                            MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc + "<ClsAttendanceManualDetails> " + "<AttendanceID>" + iAttendanceID + " </AttendanceID>" + "<LeaveID>" + iLeaveID + "</LeaveID>" + "<WorkLocationID>" + iWorkLocationID + " </WorkLocationID>" + "</ClsAttendanceManualDetails>\t";

                            blnRetValue = true;
                        }
                        TlSProgressBarAttendance.Value += 1;
                        StatusStripBottom.Refresh();
                    }
                }
            }
            MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc = MobjClsAttendanceManual.clsDTOAttendanceManual.strXmlDoc + "</Root>";
            return blnRetValue;
        }
        private string SetTagInfo(string strDayInfo, string strValue, int iFilter)
        {
            //Setting the tag info
            if (string.IsNullOrEmpty(strValue.Trim()))
            {
                strValue = "0";
            }
            int intRetValue = 0;
            int iIndex = -1;
            string[] strDay = strDayInfo.Split('@');

            switch (iFilter)
            {
                case (int)TagInfo.AttendanceID:
                    strDay[0] = ("AttendanceID = " + strValue).Trim();
                    break;
                case (int)TagInfo.AttendanceStatusID:
                    strDay[1] = ("AttendanceStatusID = " + strValue).Trim();
                    break;
                case (int)TagInfo.OT:
                    strDay[2] = ("OT =  " + strValue).Trim();
                    break;
                case (int)TagInfo.ShiftID:
                    strDay[3] = ("ShiftID =  " + strValue).Trim();
                    break;
                case (int)TagInfo.LeaveTypeID:
                    strDay[4] = ("LeaveTypeID =  " + strValue).Trim();
                    break;
                case (int)TagInfo.HalfdayLeave:
                    strDay[5] = ("HalfdayLeave =  " + strValue).Trim();
                    break;
                case (int)TagInfo.PaidLeave:
                    strDay[6] = ("PaidLeave =  " + strValue).Trim();
                    break;
                case (int)TagInfo.LeaveID:
                    strDay[7] = ("LeaveID =  " + strValue).Trim();
                    break;
                case (int)TagInfo.ConsequenceID:
                    strDay[8] = ("ConsequenceID =  " + strValue).Trim();
                    break;
                case (int)TagInfo.AbsentTime:
                    strDay[9] = ("AbsentTime =  " + strValue).Trim();
                    break;
                case (int)TagInfo.MinWorkingHours:
                    strDay[10] = ("MinWorkingHours =  " + strValue).Trim();
                    break;
                case (int)TagInfo.IsHoliday:
                    strDay[11] = ("IsHoliday =  " + strValue).Trim();
                    break;
                case (int)TagInfo.IsOnLeave:
                    strDay[12] = ("IsOnLeave =  " + strValue).Trim();
                    break;
                case (int)TagInfo.IsAtnAutoExists:
                    strDay[13] = ("IsAtnAutoExists =  " + strValue).Trim();
                    break;
                case (int)TagInfo.ShiftName:
                    strDay[14] = ("ShiftName =  " + strValue).Trim();
                    break;
                case (int)TagInfo.WorkLocation:
                    strDay[15] = ("WorkLocation =  " + strValue).Trim();
                    break;
            }
            strDayInfo = strDay[0] + "@" + strDay[1] + "@" + strDay[2] + "@" + strDay[3] + "@" + strDay[4] + "@" + strDay[5] + "@" + strDay[6] + "@" + strDay[7] + "@" + strDay[8] + "@" + strDay[9] + "@" + strDay[10] + "@" + strDay[11] + "@" + strDay[12] + "@" + strDay[13] + "@" + strDay[14] + "@" + strDay[15];
            return strDayInfo;
        }
        private int GetTagInfo(string strDayInfo, int iFilter, out string strShiftName)
        {

            strShiftName = "";
            //Get info from Tag 
            //AttendanceID = 1
            //AttendanceStatusID = 2
            //OT = 3
            //ShiftID = 4
            //LeaveTypeID = 5
            //HalfdayLeave = 6
            //PaidLeave = 7
            //LeaveID = 8
            //ConsequenceID = 9
            //AbsentTime = 10
            //MinWorkingHours = 11
            //IsHoliday = 12
            //IsOnLeave = 13
            //IsAtnAutoExists = 14
            //ShiftName = 15

            string intRetValue = "0";
            int iIndex = -1;
            string[] strDay = strDayInfo.Split('@');

            switch (iFilter)
            {
                case (int)TagInfo.AttendanceID:
                    iIndex = 0;
                    break;
                case (int)TagInfo.AttendanceStatusID:
                    iIndex = 1;
                    break;
                case (int)TagInfo.OT:
                    iIndex = 2;
                    break;
                case (int)TagInfo.ShiftID:
                    iIndex = 3;
                    break;
                case (int)TagInfo.LeaveTypeID:
                    iIndex = 4;
                    break;
                case (int)TagInfo.HalfdayLeave:
                    iIndex = 5;
                    break;
                case (int)TagInfo.PaidLeave:
                    iIndex = 6;
                    break;
                case (int)TagInfo.LeaveID:
                    iIndex = 7;
                    break;
                case (int)TagInfo.ConsequenceID:
                    iIndex = 8;
                    break;
                case (int)TagInfo.AbsentTime:
                    iIndex = 9;
                    break;
                case (int)TagInfo.MinWorkingHours:
                    iIndex = 10;
                    break;
                case (int)TagInfo.IsHoliday:
                    iIndex = 11;
                    break;
                case (int)TagInfo.IsOnLeave:
                    iIndex = 12;
                    break;
                case (int)TagInfo.IsAtnAutoExists:
                    iIndex = 13;
                    break;
                case (int)TagInfo.ShiftName:
                    iIndex = 14;
                    break;
                case (int)TagInfo.WorkLocation:
                    iIndex = 15;
                    break;
            }

            if (strDay.Count() >= iIndex)
            {
                if (iFilter == Convert.ToInt32(TagInfo.ShiftName))
                {
                    strShiftName = strDay[iIndex].Substring(strDay[iIndex].IndexOf("=") + 1);
                }
                else
                {
                    intRetValue = strDay[iIndex].Substring(strDay[iIndex].IndexOf("=") + 1).ToStringCustom();
                }

            }
            return intRetValue.ToInt32();
        }
        private bool IsValidTime(string sTime)
        {
            //Validate time fromat
            try
            {
                //Sample  Format  "13:14"
                if (string.IsNullOrEmpty(sTime) || sTime == null)
                    return false;
                DateTime TheDate = default(DateTime);
                TheDate = sTime.ToDateTime();
                string Format1 = "^*(1[0-2]|[1-9]):[0-5][0-9]*(a|p|A|P)(m|M)*$";
                System.Text.RegularExpressions.Regex TryValidateFormat1 = new System.Text.RegularExpressions.Regex(Format1);
                string Format2 = "([0-1][0-9]|2[0-3]):([0-5][0-9])";
                System.Text.RegularExpressions.Regex TryValidateFormat2 = new System.Text.RegularExpressions.Regex(Format2);
                return TryValidateFormat2.IsMatch(sTime) || TryValidateFormat1.IsMatch(sTime);
            }
            catch (Exception ex)
            {
                //MObjLog.WriteLog("Error on " & System.Reflection.MethodBase.GetCurrentMethod.Name & " : " & Me.Name & " " & ex.Message.ToString(), 1)
                return false;
            }
        }
        public string FormatString(string strTime)
        {
            //Formatting time value
            if ((strTime == null))
            {
                return "00:00";
            }
            if ((string.IsNullOrEmpty(strTime.Trim())))
            {
                return "00:00";
            }
            strTime = strTime.Replace(":", ".");
            string strOutput = "00:00";
            decimal decOutput = 0M;
            if ((decimal.TryParse(strTime, out decOutput)))
            {
                if (decOutput > 23.59M)
                    return "00:00";

                strOutput = string.Format("{0:N2}", decOutput).Replace(".", ":").Replace(",", "");

                if ((decOutput > 0))
                {
                    string[] arr = strOutput.Split(':');
                    if ((arr.Length >= 2))
                    {
                        int intLeft = Convert.ToInt32(arr[0]);
                        int intRight = Convert.ToInt32(arr[1]);
                        intLeft += (intRight > 59) ? Convert.ToInt32(Math.Floor(intRight / 60M).ToString()) : 0;
                        intRight = (intRight > 59) ? (intRight % 60) : intRight;

                        if (intRight.ToString().Length == 1)
                            strOutput = string.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + ".0" + intRight.ToString())).Replace(".", ":").Replace(",", "");
                        else
                            strOutput = string.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + "." + intRight.ToString())).Replace(".", ":").Replace(",", "");

                        arr = strOutput.Split(':');
                        string strLeft = arr[0];
                        if (strLeft.Trim().Length == 1)
                        {
                            strLeft = "0" + strLeft;
                        }
                        strOutput = strLeft + ":" + arr[1];
                    }
                }
                else
                {
                    return "00:00";
                }
            }

            return strOutput;
        }


        public string FormatStringMonth(string strTime)
        {
            //Formatting time value
            if ((strTime == null))
            {
                return "000:00";
            }
            if ((string.IsNullOrEmpty(strTime.Trim())))
            {
                return "000:00";
            }
            strTime = strTime.Replace(":", ".");
            string strOutput = "000:00";
            decimal decOutput = 0M;
            if ((decimal.TryParse(strTime, out decOutput)))
            {
                if (decOutput > 999.59M)
                    return "000:00";

                strOutput = string.Format("{0:N2}", decOutput).Replace(".", ":").Replace(",", "");

                if ((decOutput > 0))
                {
                    string[] arr = strOutput.Split(':');
                    if ((arr.Length >= 2))
                    {
                        int intLeft = Convert.ToInt32(arr[0]);
                        int intRight = Convert.ToInt32(arr[1]);
                        intLeft += (intRight > 59) ? Convert.ToInt32(Math.Floor(intRight / 60M).ToString()) : 0;
                        intRight = (intRight > 59) ? (intRight % 60) : intRight;

                        if (intRight.ToString().Length == 1)
                            strOutput = string.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + ".0" + intRight.ToString())).Replace(".", ":").Replace(",", "");
                        else
                            strOutput = string.Format("{0:N2}", Convert.ToDecimal(intLeft.ToString() + "." + intRight.ToString())).Replace(".", ":").Replace(",", "");

                        arr = strOutput.Split(':');
                        string strLeft = arr[0];
                        if (strLeft.Trim().Length == 1)
                        {
                            strLeft = "0" + strLeft;
                        }
                        if (strLeft.Trim().Length == 2)
                        {
                            strLeft = "0" + strLeft;
                        }
                        strOutput = strLeft + ":" + arr[1];
                    }
                }
                else
                {
                    return "000:00";
                }
            }

            return strOutput;
        }


        public bool IsCellEditable(int iRowIndex, int iColumnIndex, out int iType)
        {
            //Checking the current cell is editable or not

            //iType = 1 Holiday
            //iType = 2 halfday leave
            //iType = 3 Full day leave
            //iType = 4 Attendance Auto Exists
            //iType = 5 Salary Is released
            //iType = 6 Vacation Exists
            iType = 0;
            int iEmpID = dgvAttendance.Rows[iRowIndex].Cells["colDgvEmployeeID"].Tag.ToInt32();
            string strDate = dgvAttendance.Columns[iColumnIndex].Name.Trim() + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year;
            bool blnRetValue = true;

            string strDayInfo = dgvAttendance.Rows[iRowIndex].Cells[iColumnIndex].Tag.ToStringCustom();
            int iIsOnLeave = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.IsOnLeave), out strCurrentShift);
            int iIsHoliday = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.IsHoliday), out strCurrentShift);
            int iIsHalfDay = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.HalfdayLeave), out strCurrentShift);
            int iIsAttendanceAutoExists = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.IsAtnAutoExists), out strCurrentShift);
            bool blnIsSalaryReleased = this.MobjClsAttendanceManual.IsSalaryReleased(iEmpID, strDate);
            //bool blnIsVactionExists = this.MobjClsAttendanceManual.IsVacationExists(iEmpID, strDate);

            if (iIsHoliday > 0)
            {
                iType = Convert.ToInt32(CellEditableTypes.Holiday);
                //1 Holiday
                blnRetValue = false;
            }
            if (iIsOnLeave > 0)
            {
                if (iIsHalfDay > 0)
                {
                    iType = Convert.ToInt32(CellEditableTypes.HalfDayLeave);
                    //2 halfday leave
                }
                else
                {
                    iType = Convert.ToInt32(CellEditableTypes.FullDayLeave);
                    //3 Full day leave
                }
                blnRetValue = false;
            }

            if (iIsAttendanceAutoExists > 0)
            {
                iType = Convert.ToInt32(CellEditableTypes.AttendanceAutoExists);
                //4 Attendance Auto Exists
                blnRetValue = false;
            }

            if (blnIsSalaryReleased)
            {
                iType = Convert.ToInt32(CellEditableTypes.SalaryIsReleased);
                //5 Salary Is released
                blnRetValue = false;
            }

            //if (blnIsVactionExists)
            //{
            //    iType = Convert.ToInt32(CellEditableTypes.VacationExists);
            //    //6 Vacation Exists
            //    blnRetValue = false;
            //}

            return blnRetValue;
        }
        private int GetShift(int iEmpId, string strDate, out int iMinWorkhrs, out string strShiftName)
        {
            //Getting Shift of an employee for a corresponding date
            DataTable dtShift = MobjClsAttendanceManual.GetEmployeeShiftInfo(dgvAttendance.CurrentRow.Cells["colDgvEmployeeID"].Tag.ToInt32(), strDate);
            int iShiftID = 0;
            iMinWorkhrs = 0;
            strShiftName = "";
            if (dtShift.Rows.Count > 0)
            {
                iShiftID = dtShift.Rows[0]["ShiftID"].ToInt32();
                iMinWorkhrs = GetDurationInMinutes(dtShift.Rows[0]["MinWorkingHours"].ToStringCustom());
                strShiftName = dtShift.Rows[0]["ShiftName"].ToStringCustom();
            }
            return iShiftID;
        }
        private bool AutoFill(int iRowIndex)
        {
            //Auto Filling A row with minmum working hours
            bool blnRetValue = false;
            int iWorkLocationSel = Convert.ToInt32(cboLocation.SelectedValue);   
            int iEmpID = dgvAttendance.Rows[iRowIndex].Cells["colDgvEmployeeID"].Tag.ToInt32();
            int iCmpID = dgvAttendance.Rows[iRowIndex].Cells["colDgvCompanyID"].Value.ToInt32();
            string strDate = "";
            DateTime dJoiningDate = default(DateTime);
            dJoiningDate = MobjClsAttendanceManual.GetEmployeeJoiningDate(iEmpID);
            string sNowDate = ClsCommonSettings.GetServerDate().ToString("dd MMM yyyy");

            for (int iCounter = colDgvEmployeeID.Index + 1; iCounter <= colDgvCompanyID.Index - 1; iCounter++)
            {
                strDate = iCounter.ToString() + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year.ToString();

                if (dgvAttendance.Rows[iRowIndex].Cells[iCounter].Tag == null)
                {
                    SetEmployeeInfoInTag(iRowIndex, iCounter, 1);
                }
                if (Convert.ToDateTime(strDate) > Convert.ToDateTime(sNowDate))
                {
                    break; // TODO: might not be correct. Was : Exit For
                }

                if (Convert.ToDateTime(strDate).Date < dJoiningDate.Date)
                    continue;
                //cheking of joining date 


               

                string strDayInfo = dgvAttendance.Rows[iRowIndex].Cells[iCounter].Tag.ToStringCustom();
                //Dim iIsOnLeave As Integer = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.IsOnLeave))
                //Dim iIsOnHoliday As Integer = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.IsHoliday))
                //Dim iIsOnHalfDayLeave As Integer = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.HalfdayLeave))
                int iShiftID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.ShiftID), out strCurrentShift);
                int iMinWorkHrs = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.MinWorkingHours), out strCurrentShift);
                int iAttendanceID = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.AttendanceID), out strCurrentShift);
                int iWorkLocationCurrent = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.WorkLocation), out strCurrentShift);


       

                if (iWorkLocationCurrent == 0 || (iWorkLocationSel != iWorkLocationCurrent))
                {
                    continue;
                }
         

                int iType = 0;
                if (IsCellEditable(iRowIndex, iCounter, out iType) == false)
                {
                    if ((iType == Convert.ToInt32(CellEditableTypes.Holiday) || iType == Convert.ToInt32(CellEditableTypes.FullDayLeave) || iType == Convert.ToInt32(CellEditableTypes.AttendanceAutoExists) || iType == Convert.ToInt32(CellEditableTypes.SalaryIsReleased) || iType == Convert.ToInt32(CellEditableTypes.VacationExists)))
                    {
                        continue;
                    }
                    else if (iType == Convert.ToInt32(CellEditableTypes.HalfDayLeave))
                    {
                        iMinWorkHrs = iMinWorkHrs / 2;
                    }
                }

                string strWorkTime = "00:00";
                int iMaxWorkTime = 0;

                //strWorkTime = DateAdd(DateInterval.Minute, Convert.ToDouble(iMinWorkHrs), DateTime.Today);
                strWorkTime = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(iMinWorkHrs)).ToString();
                strWorkTime = Convert.ToDateTime(strWorkTime).ToString("HH:mm");

                if (IsTimeGreaterThan24(iEmpID, strDate, iAttendanceID, strWorkTime, iMaxWorkTime))
                {
                    strWorkTime = "00:00";
                    if (iMaxWorkTime > 0)
                    {
                        iMaxWorkTime = iMaxWorkTime - 1;
                        //strWorkTime = DateAdd(DateInterval.Minute, Convert.ToDouble(iMinWorkHrs), DateTime.Today);
                        strWorkTime = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(iMinWorkHrs)).ToString();
                        strWorkTime = Convert.ToDateTime(strWorkTime).ToString("HH:mm");
                    }
                }


                int intTotalWorkTime = GetTotalWorkTime(dgvAttendance.CurrentRow.Cells["colDgvEmployeeID"].Tag.ToInt32(), strDate, iAttendanceID);
                int iCurrentWorkTime = GetDurationInMinutes(strWorkTime);
                string strOt = "0";

                if (MobjClsAttendanceManual.IsOTExists(dgvAttendance.CurrentRow.Cells["colDgvEmployeeID"].Tag.ToInt32(), strDate, iAttendanceID))
                {
                    if (intTotalWorkTime < iMinWorkHrs)
                    {
                        strOt = Math.Abs(iMinWorkHrs - (intTotalWorkTime + iCurrentWorkTime)).ToString();
                    }
                    else
                    {
                        strOt = iCurrentWorkTime.ToString();
                    }
                    //strOt = GetDurationInMinutes(strWorkTime)
                }
                else
                {
                    strOt = GetOTHrs((intTotalWorkTime + iCurrentWorkTime).ToStringCustom(), iMinWorkHrs.ToStringCustom()).ToStringCustom();
                }

                int iMinmumTimeForOt = 0;
                GetOtDetails(iShiftID, ref iMinmumTimeForOt, 0, false);
                //If Ot is less Than minimumTimeForOt then ot =0
                if (Convert.ToDouble(strOt) < iMinmumTimeForOt)
                {
                    strOt = "0";
                }

                strDayInfo = SetTagInfo(strDayInfo, strOt, Convert.ToInt32(TagInfo.OT));
                strDayInfo = SetTagInfo(strDayInfo, Convert.ToInt32(AttendanceStatus.Present).ToString(), Convert.ToInt32(TagInfo.AttendanceStatusID));
                dgvAttendance.Rows[iRowIndex].Cells[iCounter].Value = strWorkTime;
                dgvAttendance.Rows[iRowIndex].Cells[iCounter].Tag = strDayInfo;
                blnRetValue = true;
                Application.DoEvents();
            }
            return blnRetValue;
        }
        private string GetOTHrs(string strWorkTime, string strMinWrkHrs)
        {
            //Calculating Ot Hours = WorkTime - MinWorking hours
            int Ot = 0;
            string strOt = "00:00";
            //flexi true And iShiftType = 2
            if (Convert.ToDouble(strWorkTime) > Convert.ToDouble(strMinWrkHrs))
            {
                Ot = Convert.ToInt32(strWorkTime) - Convert.ToInt32(strMinWrkHrs);
            }
            return Ot.ToStringCustom();
        }
        private object GetAbsentTime(int iMinWrkHrs, string strWorkHrs)
        {
            //Calculating Absent time ,Minworking Hours- Worktime
            int iMinWorkHrs = iMinWrkHrs;
            int iWorkhrs = GetDurationInMinutes(strWorkHrs);
            int iAbsentTime = iMinWorkHrs - iWorkhrs;
            iAbsentTime = (iAbsentTime > 0 ? iAbsentTime : 0);
            return iAbsentTime;
        }
        private bool SearchEmp()
        {
            //Seraching Employee's in Grid
            try
            {
                if ((!string.IsNullOrEmpty(txtSearch.Text)))
                {
                    if ((dgvAttendance.Rows.Count > 0))
                    {
                        string strEmpName = "";
                        if ((MintSearchIndex == dgvAttendance.RowCount))
                            MintSearchIndex = 0;

                        MintSearchIndex = ((MintSearchIndex == 0) ? 0 : MintSearchIndex);
                        if (MintSearchIndex > 0)
                        {
                            dgvAttendance.Rows[MintSearchIndex - 1].Selected = false;
                        }
                        else
                        {
                            dgvAttendance.Rows[MintSearchIndex].Selected = false;
                        }


                        for (int i = MintSearchIndex; i <= dgvAttendance.Rows.Count - 1; i++)
                        {
                            strEmpName = "";
                            if ((cboEmpSearch.Text == "Employee Name"))
                            {
                                strEmpName = Convert.ToString(dgvAttendance.Rows[i].Cells["colDgvEmployeeID"].Value);
                                strEmpName = strEmpName.TrimStart().TrimEnd();
                                if ((strEmpName.ToLower().StartsWith(txtSearch.Text.Trim().ToLower())))
                                {
                                    dgvAttendance.CurrentCell = dgvAttendance[colDgvEmployeeID.Index, i];
                                    dgvAttendance.CurrentCell.Selected = false;
                                    dgvAttendance.Rows[i].Selected = true;
                                    MintSearchIndex = i + 1;
                                    return false;
                                }
                            }
                            else if ((cboEmpSearch.Text == "Employee No"))
                            {
                                strEmpName = Convert.ToString(dgvAttendance.Rows[i].Cells["colDgvEmployeeID"].Value);
                                strEmpName = strEmpName.Substring(strEmpName.IndexOf("-") + 1);
                                strEmpName = strEmpName.TrimStart().TrimEnd();
                                if ((strEmpName.ToLower().StartsWith(txtSearch.Text.Trim().ToLower())))
                                {
                                    dgvAttendance.CurrentCell = dgvAttendance[colDgvEmployeeID.Index, i];
                                    dgvAttendance.CurrentCell.Selected = false;
                                    dgvAttendance.Rows[i].Selected = true;
                                    MintSearchIndex = i + 1;
                                    return false;
                                }
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MobjLog.WriteLog("Error on " + this.Name + " - Method : " + System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error : " + ex.Message.ToString(), 1);
            }
            return true;
        }
        private int GetTotalWorkTime(int iEmpID, string strDate, int iAttendanceID)
        {
            //getting total worktime for a particular day
            int iTotalWorkTime = MobjClsAttendanceManual.GetTotalWorkTime(iEmpID, strDate, iAttendanceID);
            return iTotalWorkTime;
        }
        private bool IsTimeGreaterThan24(int iEmpID, string strDate, int iAttendanceID, string strWorkTime, int iMaxWorkTime)
        {
            //Checking Worktime is greater than 24 hrs in a day
            bool blnRetValue = false;

            int iWorkTime = GetDurationInMinutes(strWorkTime);
            int iTotalTime = MobjClsAttendanceManual.GetTotalTime(iEmpID, strDate, iAttendanceID);

            //Checking Time is greater than 24Hr
            if (iWorkTime + iTotalTime >= 1440)
            {
                blnRetValue = true;
            }
            iMaxWorkTime = 1440 - iTotalTime;
            if (iMaxWorkTime < 0)
                iMaxWorkTime = 0;
            return blnRetValue;
        }
        private bool GetOtDetails(int iShiftID, ref int iMinTimeForOt, int iBufferTime, bool blnIsConsiderBufferTimeForOT)
        {
            //Get The Ot details from Shift Table 
            //Now buffertime and Isbufferforot is not cosider for ot Calculation
            //only minimumot field is taking for ot Calculation . if Calculated ot is greater than minimumot then only ot is calculated otherwise ot =0
            iMinTimeForOt = 0;
            bool blnRetValue = false;
            DataTable DtOtDetails = MobjClsAttendanceManual.GetShiftOTDetails(iShiftID);
            if (DtOtDetails != null)
            {
                if (DtOtDetails.Rows.Count > 0)
                {
                    iMinTimeForOt = Convert.ToInt32(DtOtDetails.Rows[0]["MinimumOT"]);
                    iBufferTime = Convert.ToInt32(DtOtDetails.Rows[0]["BufferTime"]);
                    blnIsConsiderBufferTimeForOT = Convert.ToBoolean(DtOtDetails.Rows[0]["IsBufferForOT"]);
                    blnRetValue = true;
                }
            }
            return blnRetValue;
        }
        public void setPageNumberText()
        {
            lblPageNumber.Text = "";
            if (intCurrentPage >= 1 && intPageCount >= 1)
            {
                lblPageNumber.Text = "{" + Convert.ToString(intCurrentPage) + "}" + "of" + "{" + Convert.ToString(intPageCount) + "}";
            }
            else if (intCurrentPage < 1 && intPageCount < 1)
            {
                lblPageNumber.Text = "{1}of{1}";
            }
            else if (intPageCount < 1)
            {
                lblPageNumber.Text = "{1}of{1}";
            }
            else if (intCurrentPage < 1 && intPageCount >= 1)
            {
                lblPageNumber.Text = "{1}" + "of" + "{" + Convert.ToString(intPageCount) + "}";
            }

        }
        private void FillGrid()
        {
            this.btnShow.Enabled = false;
            FlagLoop = false;
            if (DispalyAttendanceInfo().ToBoolean())
            {
                ResetProgressBar();
            }
            FlagLoop = false;
            this.btnShow.Enabled = true;
        }
        #endregion

        #region Control Events
        private void btnShow_Click(object sender, EventArgs e)
        {
            blnNotFromButtonClick = false;
            intPageCount = 0;
            intCurrentPage = 1;
            intRowCount = 25;
            cboCountItem.SelectedIndex = 0;
            FillGrid();
            blnNotFromButtonClick = true;
        }

        private void FrmAttendanceManual_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                FlagLoop = false;

            }
            catch (Exception ex)
            {
            }
        }

        private void FrmAttendanceManual_Load(object sender, EventArgs e)
        {
            MobjClsAttendanceManual = new clsBLLAttendanceManual();
            MobjNotification = new ClsNotification();
            MobjLog = new ClsLogWriter(Application.StartupPath);
            MobjClsConnection = new clsConnection();
            SetPermissions();
            LoadMessage();
            LoadCombo(1);
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombo(2);
            LoadCombo(3);
            ResetGrid();
        }

        private void btnPresent_Click(object sender, EventArgs e)
        {
            int iType = 0;
            if (dgvAttendance.CurrentCell != null)
            {



                SetEmployeeInfoInTag(dgvAttendance.CurrentCell.RowIndex, dgvAttendance.CurrentCell.ColumnIndex, Convert.ToInt32(AttendanceStatus.Present));

                if (IsCellEditable(dgvAttendance.CurrentCell.RowIndex, dgvAttendance.CurrentCell.ColumnIndex, out iType) == false)
                {
                    if (iType == Convert.ToInt32(CellEditableTypes.SalaryIsReleased))
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10089, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClear.Enabled = true;
                        return;
                    }
                }

                if (dgvAttendance.CurrentCell.Value.ToStringCustom() == string.Empty || string.IsNullOrEmpty(dgvAttendance.CurrentCell.Value.ToStringCustom()))
                {
                    dgvAttendance.CurrentCell.Value = "00:00";
                    string strDayInfo = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), 0.ToString(), Convert.ToInt32(TagInfo.OT));
                    dgvAttendance.CurrentCell.Tag = strDayInfo;
                }
                else if (dgvAttendance.CurrentCell.Value.ToStringCustom() == "H")
                {
                    dgvAttendance.CurrentCell.Value = "00:00";
                    //dgvAttendance.CurrentCell.Style.ForeColor = Color.Black
                    dgvAttendance.CurrentCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
            }
        }

        private void btnRest_Click(object sender, EventArgs e)
        {
            int iType = 0;
            if (dgvAttendance.CurrentCell != null)
            {
                if (IsCellEditable(dgvAttendance.CurrentCell.RowIndex, dgvAttendance.CurrentCell.ColumnIndex, out iType) == false)
                {
                    if (iType == Convert.ToInt32(CellEditableTypes.SalaryIsReleased))
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10089, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClear.Enabled = true;
                        return;
                    }
                }
                SetEmployeeInfoInTag(dgvAttendance.CurrentCell.RowIndex, dgvAttendance.CurrentCell.ColumnIndex, Convert.ToInt32(AttendanceStatus.Rest));
                dgvAttendance.CurrentCell.Value = "00:00";
                string strDayInfo = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), 0.ToString(), Convert.ToInt32(TagInfo.OT));
                dgvAttendance.CurrentCell.Tag = strDayInfo;
            }
        }

        private void btnLeave_Click(object sender, EventArgs e)
        {
            int iType = 0;
            if (dgvAttendance.CurrentCell != null)
            {
                if (IsCellEditable(dgvAttendance.CurrentCell.RowIndex, dgvAttendance.CurrentCell.ColumnIndex, out iType) == false)
                {
                    if (iType == Convert.ToInt32(CellEditableTypes.SalaryIsReleased))
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10089, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClear.Enabled = true;
                        return;
                    }
                }
                if (dgvAttendance.CurrentCell.Tag == null)
                    SetEmployeeInfoInTag(dgvAttendance.CurrentCell.RowIndex, dgvAttendance.CurrentCell.ColumnIndex, 0);

                int iEmpId = Convert.ToInt32(dgvAttendance.CurrentRow.Cells["colDgvEmployeeID"].Tag.ToInt32());
                int iCmpID = Convert.ToInt32(dgvAttendance.CurrentRow.Cells["colDgvCompanyID"].Value.ToInt32());
                string StrEmpName = dgvAttendance.CurrentRow.Cells["colDgvEmployeeID"].Value.ToStringCustom();
                string strDate = dgvAttendance.Columns[dgvAttendance.CurrentCell.ColumnIndex].Name + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year;
                FrmLeaveFromAttendance objFrmLeaveFromAttendance = new FrmLeaveFromAttendance(iEmpId, StrEmpName, iCmpID, strDate);
                objFrmLeaveFromAttendance.ShowDialog();

                if ((objFrmLeaveFromAttendance.PblnBtnOk))
                {
                    dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), Convert.ToString((int)AttendanceStatus.Leave), Convert.ToInt32(TagInfo.AttendanceStatusID));
                    dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), objFrmLeaveFromAttendance.PintLeaveType.ToString(), Convert.ToInt32(TagInfo.LeaveTypeID));

                    dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), (objFrmLeaveFromAttendance.PblnHalfday ? 1 : 0).ToString(), Convert.ToInt32(TagInfo.HalfdayLeave));
                    //Contains Haldfday or fullday

                    //if (!objFrmLeaveFromAttendance.PblnHalfday)
                    //{
                    dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), 0.ToString(), Convert.ToInt32(TagInfo.AbsentTime));
                    //}

                    dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), (objFrmLeaveFromAttendance.PblnPaid ? 0 : 1).ToString(), Convert.ToInt32(TagInfo.PaidLeave));
                    //paid or unpaid
                }
                else
                {
                    dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), Convert.ToString((int)AttendanceStatus.Absent), Convert.ToInt32(TagInfo.AttendanceStatusID));

                }
                dgvAttendance.CurrentCell.Value = "";
                string strDayInfo = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToString(), 0.ToString().ToStringCustom(), Convert.ToInt32(TagInfo.OT));
                dgvAttendance.CurrentCell.Tag = strDayInfo;
            }
        }

        private void btnAbsent_Click(object sender, EventArgs e)
        {
            int iType = 0;

            if (dgvAttendance.CurrentCell != null)
            {
                if (IsCellEditable(dgvAttendance.CurrentCell.RowIndex, dgvAttendance.CurrentCell.ColumnIndex, out iType) == false)
                {
                    if (iType == Convert.ToInt32(CellEditableTypes.SalaryIsReleased))
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10089, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClear.Enabled = true;
                        return;
                    }
                }

                SetEmployeeInfoInTag(dgvAttendance.CurrentCell.RowIndex, dgvAttendance.CurrentCell.ColumnIndex, Convert.ToInt32(AttendanceStatus.Absent));
                dgvAttendance.CurrentCell.Value = "00:00";
                string strDayInfo = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToString(), 0.ToString(), Convert.ToInt32(TagInfo.OT));
                int iMinWorkingHours = GetTagInfo(strDayInfo, Convert.ToInt32(TagInfo.MinWorkingHours), out strCurrentShift);
                strDayInfo = SetTagInfo(strDayInfo, iMinWorkingHours.ToString(), Convert.ToInt32(TagInfo.AbsentTime));
                dgvAttendance.CurrentCell.Tag = strDayInfo;
            }
        }
        private void AutoFillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvAttendance.CurrentCell != null)
            {
                AutoFill(dgvAttendance.CurrentRow.Index);
            }
        }

        private void dgvAttendance_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.RowIndex > -1 && (e.ColumnIndex > colDgvEmployeeID.Index && e.ColumnIndex < colDgvCompanyID.Index))
            {
                if (dgvAttendance.CurrentCell.Tag == null)
                    SetEmployeeInfoInTag(e.RowIndex, e.ColumnIndex, 0);
                int iType = 0;
                if (IsCellEditable(e.RowIndex, e.ColumnIndex, out iType) == false)
                {
                    //sALARY
                    if (iType == Convert.ToInt32(CellEditableTypes.SalaryIsReleased))
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10089, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClear.Enabled = true;
                        e.Cancel = true;
                        return;
                    }
                    ////vACTION
                    //if (iType == Convert.ToInt32(CellEditableTypes.VacationExists))
                    //{
                    //    MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10091, out MmessageIcon);
                    //    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                    //    tmrClear.Enabled = true;
                    //    e.Cancel = true;
                    //    return;
                    //}
                    //HOLIDAY
                    if (iType == Convert.ToInt32(CellEditableTypes.Holiday))
                    {
                        if (dgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToStringCustom() == "H")
                        {
                            dgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "";
                        }
                        //dgvAttendance.Rows[e.RowIndex).Cells[e.ColumnIndex).Style.ForeColor = Color.Black
                        dgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.Alignment = DataGridViewContentAlignment.BottomLeft;
                    }

                    //FULL DAY LEAVE
                    if (iType == Convert.ToInt32(CellEditableTypes.FullDayLeave))
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10085, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClear.Enabled = true;
                        e.Cancel = true;
                        return;
                    }

                    //ATTENDANCE aUTO
                    if (iType == Convert.ToInt32(CellEditableTypes.AttendanceAutoExists))
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10087, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClear.Enabled = true;
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    //Checking to avoid over write the attendance status from leave to present (if error occur please avoid the if checking)
                    if (GetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), Convert.ToInt32(TagInfo.AttendanceStatusID), out strCurrentShift) != (int)AttendanceStatus.Leave)
                    {
                        SetEmployeeInfoInTag(e.RowIndex, e.ColumnIndex, 1);
                    }
                }
            }
        }

        private void dgvAttendance_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            string strOt = "";

            double ColValOT= 0;


            if (e.RowIndex > -1 && (e.ColumnIndex == colDgvOTHour.Index ))
            {
                if (dgvAttendance.CurrentCell.Value != null && dgvAttendance.CurrentCell.Value.ToStringCustom() != string.Empty)
                {
                    dgvAttendance.CurrentCell.Value = FormatStringMonth(dgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Trim());
                    dgvAttendance.RefreshEdit();
                   
                }

            }

            if (e.RowIndex > -1 && (e.ColumnIndex > colDgvEmployeeID.Index && e.ColumnIndex < colDgvCompanyID.Index))
            {
                if (dgvAttendance.CurrentCell.Value != null && dgvAttendance.CurrentCell.Value.ToStringCustom() != string.Empty)
                {
                    dgvAttendance.CurrentCell.Value = FormatString(dgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Trim());
                    dgvAttendance.RefreshEdit();
                    if (IsValidTime(dgvAttendance.CurrentCell.Value.ToStringCustom()) == false)
                    {
                        MessageBox.Show(MobjNotification.GetErrorMessage(MsarMessageArr, 10086, out MmessageIcon), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgvAttendance.CurrentCell.Value = "00:00";
                        return;
                    }
                    string strDate = dgvAttendance.Columns[dgvAttendance.CurrentCell.ColumnIndex].Name + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year;
                    int iMinWrkHrs = 0;
                    string strTemp;
                    int intShiftId = this.GetShift(dgvAttendance.CurrentRow.Cells["colDgvEmployeeID"].Tag.ToInt32(), strDate, out iMinWrkHrs, out  strTemp);

                    int iAttendanceID = GetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), Convert.ToInt32(TagInfo.AttendanceID), out strCurrentShift);
                    int intIsHoiliday = GetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), Convert.ToInt32(TagInfo.IsHoliday), out strCurrentShift);

                    int iMaxWorkTime = 0;
                    if ((IsTimeGreaterThan24(dgvAttendance.CurrentRow.Cells["colDgvEmployeeID"].Tag.ToInt32(), strDate, iAttendanceID, dgvAttendance.CurrentCell.Value.ToStringCustom(), iMaxWorkTime)))
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10090, out MmessageIcon);
                        MstrMessCommon = MstrMessCommon.Replace("#", " ");
                        if (iMaxWorkTime > 0)
                        {
                            //string sWrk = DateAdd(DateInterval.Minute, Convert.ToDouble(iMaxWorkTime), DateTime.Today);
                            string sWrk = ClsCommonSettings.GetServerDate().AddMinutes(Convert.ToDouble(iMaxWorkTime)).ToString();
                            sWrk = Convert.ToDateTime(sWrk).ToString("HH:mm");
                            MstrMessCommon = MstrMessCommon + " .Please Enter Worktime less than " + sWrk;
                        }
                        MessageBox.Show(MstrMessCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgvAttendance.CurrentCell.Value = "00:00";
                        return;
                    }
                    string strWorkTime = "00:00";
                    strOt = "00:00";
                    int intTotalWorkTime = 0;
                    intTotalWorkTime = GetTotalWorkTime(dgvAttendance.CurrentRow.Cells["colDgvEmployeeID"].Tag.ToInt32(), strDate, iAttendanceID);
                    int iCurrentWorkTime = GetDurationInMinutes(dgvAttendance.CurrentCell.Value.ToStringCustom());

                    if (MobjClsAttendanceManual.IsOTExists(dgvAttendance.CurrentRow.Cells["colDgvEmployeeID"].Tag.ToInt32(), strDate, iAttendanceID))
                    {
                        if (intTotalWorkTime < iMinWrkHrs)
                        {
                            strOt = Math.Abs(iMinWrkHrs - (intTotalWorkTime + iCurrentWorkTime)).ToStringCustom();
                        }
                        else
                        {
                            strOt = iCurrentWorkTime.ToString();
                        }
                    }
                    else
                    {
                        strWorkTime = (intTotalWorkTime + iCurrentWorkTime).ToStringCustom();
                        strOt = GetOTHrs(strWorkTime, iMinWrkHrs.ToStringCustom());
                    }

                    int iMinmumTimeForOt = 0;
                    this.GetOtDetails(intShiftId, ref iMinmumTimeForOt, 0, false);
                    //If Ot is less Than minimumTimeForOt then ot =0
                    if ((Convert.ToDouble(strOt) < iMinmumTimeForOt) || intIsHoiliday == 1)
                    {
                        strOt = "0";
                    }

                    string strTotalWorkTime = GetHourFromMinutes(intTotalWorkTime + iCurrentWorkTime);
                    int iAbsentTime = 0;
                    if (GetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), Convert.ToInt32(TagInfo.HalfdayLeave), out strCurrentShift) == 1)
                    {
                        iAbsentTime = GetAbsentTime(iMinWrkHrs / 2, strTotalWorkTime).ToInt32();
                    }
                    else
                    {
                        iAbsentTime = GetAbsentTime(iMinWrkHrs, strTotalWorkTime).ToInt32();
                    }
                    //int iAbsentTime = GetAbsentTime(iMinWrkHrs, strTotalWorkTime).ToInt32();
                    //Checking to avoid over write the attendance status from leave to present (if error occur please avoid the if checking)

                    if (GetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), Convert.ToInt32(TagInfo.AttendanceStatusID), out strCurrentShift) != (int)AttendanceStatus.Leave)
                    {
                        dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), 1.ToStringCustom(), Convert.ToInt32(TagInfo.AttendanceStatusID));
                    }
                    dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), strOt, Convert.ToInt32(TagInfo.OT));
                    dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), iAbsentTime.ToStringCustom(), Convert.ToInt32(TagInfo.AbsentTime));
                }
                else
                {
                    int iStatus = GetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), Convert.ToInt32(TagInfo.AttendanceStatusID), out strCurrentShift);
                    int iIsHoiliday = GetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), Convert.ToInt32(TagInfo.IsHoliday), out strCurrentShift);

                    if (iStatus == Convert.ToInt32(AttendanceStatus.Present))
                    {
                        dgvAttendance.CurrentCell.Tag = SetTagInfo(dgvAttendance.CurrentCell.Tag.ToStringCustom(), 0.ToStringCustom(), Convert.ToInt32(TagInfo.AttendanceStatusID));
                    }
                    if (iIsHoiliday > 0)
                    {
                        dgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "H";
                        //dgvAttendance.Rows[e.RowIndex).Cells[e.ColumnIndex).Style.ForeColor = Color.Black
                        dgvAttendance.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.Alignment = DataGridViewContentAlignment.BottomRight;
                    }

                }

              
                if (strOt!="")
                {
                    if (Convert.ToDouble(strOt) > 0)
                    {
                        if (Convert.ToString(dgvAttendance.Rows[e.RowIndex].Cells["colDgvOTHour"].Value).Trim().Equals(String.Empty))
                        {
                            dgvAttendance.Rows[e.RowIndex].Cells["colDgvOTHour"].Value = "0";
                        }


                        ColValOT = GetTimeFromHrMinutes(Convert.ToDouble(dgvAttendance.Rows[e.RowIndex].Cells["colDgvOTHour"].Value.ToString().Replace(":","."))).ToDouble();
                        dgvAttendance.Rows[e.RowIndex].Cells["colDgvOTHour"].Value = GetTimeFromMinHour(ColValOT.ToDouble() + strOt.ToDouble());
                        dgvAttendance.Rows[e.RowIndex].Cells["colDgvOTHour"].Value = FormatStringMonth(dgvAttendance.Rows[e.RowIndex].Cells["colDgvOTHour"].Value.ToString());
                        dgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                        dgvAttendance.EndEdit(); 
                    }
                }
 
            }
        }

        private string GetTimeFromHrMinutes(double strHour)
        {
            try
            {
                TimeSpan t = TimeSpan.FromHours(strHour);
                string GetTm = t.TotalMinutes.ToString(); 
              return GetTm;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        private string GetTimeFromMinHour(double strMinutes)
        {
            try
            {
                TimeSpan t = TimeSpan.FromMinutes(strMinutes);
                string GetTm =  t.Hours.ToString() + ":" + t.Minutes.ToString() ;     

                

                return GetTm;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        private void dgvAttendance_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvAttendance.CurrentCell != null)
            {
                dgvAttendance.CurrentCell.ContextMenuStrip = null;
                if (e.RowIndex > -1 && e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    if ((e.ColumnIndex > colDgvEmployeeID.Index && e.ColumnIndex < colDgvCompanyID.Index) )
                    {

                        if (dgvAttendance.CurrentCell.ColumnIndex > colDgvEmployeeID.Index && dgvAttendance.CurrentCell.ColumnIndex < colDgvCompanyID.Index)
                        {
                            SetStatusButtonVisibilty(e.RowIndex, e.ColumnIndex);
                            dgvAttendance.CurrentCell.ContextMenuStrip = this.ContextMenuStripStatus;
                            this.ContextMenuStripStatus.Show(this.dgvAttendance, this.dgvAttendance.PointToClient(System.Windows.Forms.Cursor.Position));

                        }
                    }
                    else if (e.ColumnIndex == colDgvEmployeeID.Index)
                    {
                        if (dgvAttendance.CurrentCell.ColumnIndex == e.ColumnIndex && dgvAttendance.CurrentCell.RowIndex == e.RowIndex)
                        {
                            dgvAttendance.CurrentCell.ContextMenuStrip = this.ContextMenuStripAutoFill;
                            this.ContextMenuStripAutoFill.Show(this.dgvAttendance, this.dgvAttendance.PointToClient(System.Windows.Forms.Cursor.Position));
                        }
                    }
                }
            }
        }

        private void dgvAttendance_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ClearBottomPanel();
            if (dgvAttendance.CurrentCell != null)
            {
                if (e.RowIndex >= 0 && e.ColumnIndex > colDgvEmployeeID.Index && e.ColumnIndex < colDgvCompanyID.Index)
                {
                    ShowCellInfo(e.RowIndex, e.ColumnIndex);
                }
            }
        }

        private void dgvAttendance_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                e.Handled = true;
                return;
                //if (dgvAttendance.SelectedRows.Count > 0)
                //{
                //    int iProgressMaximum = dgvAttendance.RowCount * DateTime.DaysInMonth(dtpFromDate.Value.Year, dtpFromDate.Value.Month);
                //    // + 1
                //    //TlSProgressBarAttendance.Maximum = iProgressMaximum
                //    if (MessageBox.Show(new ClsNotification().GetErrorMessage(MsarMessageArr, 13, out MmessageIcon).Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                //    {
                //        e.Handled = true;
                //        return;
                //    }
                //    InitializeProgressBar(iProgressMaximum);
                //    if (FillDeleteParameters(true))
                //    {
                //        if (MobjClsAttendanceManual.DeleteAttendanceManual())
                //        {
                //            MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                //            MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //            lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                //            tmrClear.Enabled = true;
                //            TlSProgressBarAttendance.Value = iProgressMaximum;
                //        }
                //    }
                //    ResetProgressBar();
                //}
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveAttendanceInfo())
            {
                MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                ClearAllControls();
            }
            ResetProgressBar();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            if (dgvAttendance.Rows.Count > 0)
            {

                MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 13, out  MmessageIcon);
                foreach (DataGridViewRow DgvRow in (dgvAttendance.Rows))
                {
                    int iEmpID = DgvRow.Cells["colDgvEmployeeID"].Tag.ToInt32();
                    string strDate = GetStratDate();
                    if (MobjClsAttendanceManual.IsSalaryReleased(iEmpID, strDate))
                    {
                        //if (MessageBox.Show(new ClsNotification().GetErrorMessage(MsarMessageArr, 10094, out MmessageIcon).Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        //{

                        //    return;
                        //}
                        //else
                        //{
                        //    break;
                        //}
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10094, out  MmessageIcon);
                        break;
                    }
                }
                int iProgressMaximum = dgvAttendance.RowCount * DateTime.DaysInMonth(dtpFromDate.Value.Year, dtpFromDate.Value.Month);
                // + 1
                //MstrMessCommon = MstrMessCommon.Substring(MstrMessCommon.IndexOf("#") + 1);

                if (MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    ResetProgressBar();
                    return;
                }
                InitializeProgressBar(iProgressMaximum);
                if (FillDeleteParameters(false))
                {
                    if (MobjClsAttendanceManual.DeleteAttendanceManual())
                    {
                        MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                        MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
                        tmrClear.Enabled = true;
                        TlSProgressBarAttendance.Value = iProgressMaximum;
                        ClearAllControls();
                        ClearBottomPanel();
                    }
                }
                else
                {
                    tmrClear.Enabled = true;
                }
                ResetProgressBar();
            }
        }

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            tmrClear.Enabled = false;
            lblAttendanceStatus.Text = "";
        }

        private void btnAutoFill_Click(object sender, EventArgs e)
        {
            AutoFillAllEmployees();
        }

        private void cboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            dgvAttendance.ClearSelection();
            MintSearchIndex = 0;
        }

        private void cboEmpSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvAttendance.ClearSelection();
            MintSearchIndex = 0;
        }

        private void cboEmpSearch_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmpSearch.DroppedDown = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            string strMessage = "No Items Found !";
            dgvAttendance.ClearSelection();
            if ((!string.IsNullOrEmpty(txtSearch.Text) && (cboEmpSearch.Text == "Employee Name" || cboEmpSearch.Text == "Employee No")))
            {

                if ((dgvAttendance.Rows.Count > 0))
                {
                    if ((SearchEmp() == true))
                    {
                        if ((MintSearchIndex == 0))
                        {
                            MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //dgvEmployeeLeaveOpening.CurrentCell = dgvEmployeeLeaveOpening(EmployeeName.Index, 0)
                            dgvAttendance.Refresh();
                        }
                        MintSearchIndex = 0;
                    }
                }
            }
            else
            {
                dgvAttendance.ClearSelection();
                MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            dtpFromDate.Value = GetStratDate().ToDateTime();
            ResetGrid();
        }

        private void dtpFromDate_KeyDown(object sender, KeyEventArgs e)
        {
            dtpFromDate.Value = GetStratDate().ToDateTime();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearAllControls();
            ClearBottomPanel();
        }
        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            //if (dgvAttendance.Rows.Count > 0)
            //{
            //Int32 companyId = default(Int16);
            //Int32 branchid = default(Int16);


            //companyId = Convert.ToInt32(cboCompany.SelectedValue);
            //DateTime dteDate = default(DateTime);
            //dteDate = Convert.ToDateTime(" 01 " + " " + dtpFromDate.Value.ToString("MMM") + " " + dtpFromDate.Value.Year.ToString());
            //if (MobjClsAttendanceManual.IsAttendanceManualExists(companyId, dteDate.ToString("dd MMM yyyy"), 0) == false)
            //{
            //    MstrMessCommon = MobjNotification.GetErrorMessage(MsarMessageArr, 10092, out MmessageIcon);
            //    MessageBox.Show(MstrMessCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    this.lblAttendanceStatus.Text = MstrMessCommon.Remove(0, MstrMessCommon.IndexOf("#") + 1).Trim();
            //    tmrClear.Enabled = true;
            //    return;
            //}
            FrmRptOffDayMark objAttenSum = new FrmRptOffDayMark();
            try
            {
                //branchid = -2;
                //objAttenSum.mCompanyID = companyId;
                //objAttenSum.mBranchID = branchid;
                //objAttenSum.mFromDate = dteDate;
                //objAttenSum.mToDate = DateAdd(DateInterval.Day, -1, (DateAdd(DateInterval.Month, 1, dteDate)));
                //objAttenSum.mIncludeComp = 1;
                //objAttenSum.mEmployeeID = 0;
                objAttenSum.Text = "Attendance Manual Report" + this.MdiParent.MdiChildren.Length.ToString();
                objAttenSum.MdiParent = this.MdiParent;
                objAttenSum.WindowState = FormWindowState.Maximized;
                objAttenSum.Show();
                objAttenSum.Update();
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                MobjLog.WriteLog("Error on " + this.Name + " - Method : " + System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error : " + ex.Message.ToString(), 1);
                objAttenSum.Dispose();
                objAttenSum = null;
                GC.Collect();
            }
            //}
        }

        private void cboCountItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (Convert.ToString(cboCountItem.SelectedItem))
            {
                case "25":
                    intRowCount = 25;
                    break;
                case "50":
                    intRowCount = 50;
                    break;
                case "100":
                    intRowCount = 100;
                    break;
                case "200":
                    intRowCount = 200;
                    break;
                case "500":
                    intRowCount = 500;
                    break;
                case "ALL":
                    intRowCount = 0;
                    break;
                default:
                    intRowCount = 0;
                    break;
            }
            if (blnNotFromButtonClick)
            {
                intCurrentPage = 1;
                FillGrid();
            }
        }

        private void btnNextGridCollection_Click(object sender, EventArgs e)
        {
            if ((intCurrentPage + 1) <= intPageCount && intPageCount > 0)
            {
                intCurrentPage = intCurrentPage + 1;
                FillGrid();
            }
            setPageNumberText();
        }

        private void btnPreviousGridCollection_Click(object sender, EventArgs e)
        {
            if ((intCurrentPage - 1) >= 1 && intPageCount > 0)
            {
                intCurrentPage = intCurrentPage - 1;
                FillGrid();
            }
            setPageNumberText();
        }

        private void btnFirstGridCollection_Click(object sender, EventArgs e)
        {
            if (intPageCount > 0)
            {
                intCurrentPage = 1;
                FillGrid();
            }
            else
            {
                intCurrentPage = 1;
            }
            setPageNumberText();
        }

        private void btnLastGridCollection_Click(object sender, EventArgs e)
        {
            if (intPageCount > 0)
            {
                intCurrentPage = intPageCount;
                FillGrid();
            }
            else
            {
                intCurrentPage = 1;
            }
            setPageNumberText();
        }
        #endregion

        private void dgvAttendance_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (dgvAttendance.CurrentCell != null)
                {
                    if (dgvAttendance.CurrentCell.ColumnIndex == colDgvOTDays.Index || dgvAttendance.CurrentCell.ColumnIndex == colDgvOTHour.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
        }

        private void dgvAttendance_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvAttendance.IsCurrentCellDirty)
                {
                    dgvAttendance.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }


        }

        private void cboLocation_KeyDown(object sender, KeyEventArgs e)
        {
            cboLocation.DroppedDown = false;
        }
    }
}