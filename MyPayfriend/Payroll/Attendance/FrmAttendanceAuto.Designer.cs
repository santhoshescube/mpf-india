﻿namespace MyPayfriend
{
    partial class FrmAttendanceAuto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAttendanceAuto));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnExcelExport = new DevComponents.DotNetBar.ButtonX();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.BtnAutoFill = new DevComponents.DotNetBar.ButtonItem();
            this.BtnClear = new DevComponents.DotNetBar.ButtonItem();
            this.txtCount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpMonthSelect = new System.Windows.Forms.DateTimePicker();
            this.CboEmployee = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.cboDesignation = new System.Windows.Forms.ComboBox();
            this.cboDepartment = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.BarRecordCnt = new DevComponents.DotNetBar.Bar();
            this.lblPrgressPercentage = new DevComponents.DotNetBar.LabelX();
            this.lblProgressShow = new DevComponents.DotNetBar.LabelX();
            this.barProgressBarAttendance = new DevComponents.DotNetBar.ProgressBarItem();
            this.controlContainerItem2 = new DevComponents.DotNetBar.ControlContainerItem();
            this.controlContainerItem3 = new DevComponents.DotNetBar.ControlContainerItem();
            this.dockSite7 = new DevComponents.DotNetBar.DockSite();
            this.DgvAttendance = new ClsDataGirdViewX();
            this.ColDgvAtnDate = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvEmployee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvStatus = new DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn();
            this.ColDgvAddMore = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.ColDgvRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvShift = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvDay = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvBreakTime = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvWorkTime = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvLateComing = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvEarlyGoing = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvExcessTime = new DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn();
            this.ColDgvCompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvConsequenceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvPolicyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvConseqAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvValidFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvShiftTimeDisplay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvAllowedBreakTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvHolidayFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvLeaveFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvAbsentTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvShiftOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvLeaveInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvLeaveId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvLeaveType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvPunchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDgvProjectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTPFormatTime = new System.Windows.Forms.DateTimePicker();
            this.CboCompany = new System.Windows.Forms.ComboBox();
            this.CboBranch = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PanelTop = new DevComponents.DotNetBar.PanelEx();
            this.chkIncludeCompany = new DevComponents.DotNetBar.Controls.CheckBoxX();
            ((System.ComponentModel.ISupportInitialize)(this.BarRecordCnt)).BeginInit();
            this.BarRecordCnt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendance)).BeginInit();
            this.PanelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExcelExport
            // 
            this.btnExcelExport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExcelExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcelExport.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnExcelExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcelExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExcelExport.Image")));
            this.btnExcelExport.ImageTextSpacing = 5;
            this.btnExcelExport.Location = new System.Drawing.Point(893, 67);
            this.btnExcelExport.Name = "btnExcelExport";
            this.btnExcelExport.Size = new System.Drawing.Size(16, 16);
            this.btnExcelExport.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnExcelExport.TabIndex = 162;
            this.btnExcelExport.Tooltip = "Export to Excel";
            this.btnExcelExport.Click += new System.EventHandler(this.btnExcelExport_Click);
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(771, 63);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(55, 24);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BtnAutoFill,
            this.BtnClear});
            this.btnShow.TabIndex = 22;
            this.btnShow.Text = "Show";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // BtnAutoFill
            // 
            this.BtnAutoFill.GlobalItem = false;
            this.BtnAutoFill.Name = "BtnAutoFill";
            this.BtnAutoFill.Text = "AutoFill";
            this.BtnAutoFill.Click += new System.EventHandler(this.BtnAutoFill_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.GlobalItem = false;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Text = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtCount
            // 
            this.txtCount.Location = new System.Drawing.Point(852, 35);
            this.txtCount.Name = "txtCount";
            this.txtCount.Size = new System.Drawing.Size(57, 20);
            this.txtCount.TabIndex = 21;
            this.txtCount.Leave += new System.EventHandler(this.txtCount_Leave);
            this.txtCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInt_KeyPress);
            // 
            // label4
            // 
            this.label4.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(748, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Last Auto Fill Count";
            // 
            // label3
            // 
            this.label3.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(748, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Month";
            // 
            // dtpMonthSelect
            // 
            this.dtpMonthSelect.CustomFormat = "MMM yyyy";
            this.dtpMonthSelect.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonthSelect.Location = new System.Drawing.Point(820, 9);
            this.dtpMonthSelect.Name = "dtpMonthSelect";
            this.dtpMonthSelect.ShowUpDown = true;
            this.dtpMonthSelect.Size = new System.Drawing.Size(89, 20);
            this.dtpMonthSelect.TabIndex = 0;
            this.dtpMonthSelect.ValueChanged += new System.EventHandler(this.dtpMonthSelect_ValueChanged);
            // 
            // CboEmployee
            // 
            this.CboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmployee.DropDownHeight = 125;
            this.CboEmployee.FormattingEnabled = true;
            this.CboEmployee.IntegralHeight = false;
            this.CboEmployee.Location = new System.Drawing.Point(459, 62);
            this.CboEmployee.Name = "CboEmployee";
            this.CboEmployee.Size = new System.Drawing.Size(247, 21);
            this.CboEmployee.TabIndex = 18;
            this.CboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboEmployee_KeyDown);
            // 
            // label1
            // 
            this.label1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(378, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Employee";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(832, 63);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(55, 24);
            this.btnSave.TabIndex = 16;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cboDesignation
            // 
            this.cboDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDesignation.FormattingEnabled = true;
            this.cboDesignation.Location = new System.Drawing.Point(459, 35);
            this.cboDesignation.Name = "cboDesignation";
            this.cboDesignation.Size = new System.Drawing.Size(247, 21);
            this.cboDesignation.TabIndex = 13;
            this.cboDesignation.SelectedIndexChanged += new System.EventHandler(this.cboDesignation_SelectedIndexChanged);
            this.cboDesignation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboDesignation_KeyDown);
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.Location = new System.Drawing.Point(459, 8);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(247, 21);
            this.cboDepartment.TabIndex = 12;
            this.cboDepartment.SelectedIndexChanged += new System.EventHandler(this.cboDepartment_SelectedIndexChanged);
            this.cboDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboDepartment_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(376, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Designation";
            // 
            // lblDepartment
            // 
            this.lblDepartment.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Location = new System.Drawing.Point(376, 12);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(62, 13);
            this.lblDepartment.TabIndex = 10;
            this.lblDepartment.Text = "Department";
            // 
            // BarRecordCnt
            // 
            this.BarRecordCnt.AntiAlias = true;
            this.BarRecordCnt.Controls.Add(this.lblPrgressPercentage);
            this.BarRecordCnt.Controls.Add(this.lblProgressShow);
            this.BarRecordCnt.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BarRecordCnt.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.barProgressBarAttendance,
            this.controlContainerItem2,
            this.controlContainerItem3});
            this.BarRecordCnt.Location = new System.Drawing.Point(0, 490);
            this.BarRecordCnt.Name = "BarRecordCnt";
            this.BarRecordCnt.Size = new System.Drawing.Size(1250, 23);
            this.BarRecordCnt.Stretch = true;
            this.BarRecordCnt.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BarRecordCnt.TabIndex = 9;
            this.BarRecordCnt.TabStop = false;
            this.BarRecordCnt.Text = "bar1";
            // 
            // lblPrgressPercentage
            // 
            this.lblPrgressPercentage.AutoSize = true;
            // 
            // 
            // 
            this.lblPrgressPercentage.BackgroundStyle.Class = "";
            this.lblPrgressPercentage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPrgressPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrgressPercentage.Location = new System.Drawing.Point(99, 3);
            this.lblPrgressPercentage.Name = "lblPrgressPercentage";
            this.lblPrgressPercentage.Size = new System.Drawing.Size(137, 17);
            this.lblPrgressPercentage.TabIndex = 1035;
            this.lblPrgressPercentage.Text = "0 Records processed ";
            this.lblPrgressPercentage.Visible = false;
            // 
            // lblProgressShow
            // 
            this.lblProgressShow.AutoSize = true;
            // 
            // 
            // 
            this.lblProgressShow.BackgroundStyle.Class = "";
            this.lblProgressShow.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblProgressShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgressShow.Location = new System.Drawing.Point(240, 3);
            this.lblProgressShow.Name = "lblProgressShow";
            this.lblProgressShow.Size = new System.Drawing.Size(17, 17);
            this.lblProgressShow.TabIndex = 1034;
            this.lblProgressShow.Text = "....";
            this.lblProgressShow.Visible = false;
            // 
            // barProgressBarAttendance
            // 
            // 
            // 
            // 
            this.barProgressBarAttendance.BackStyle.Class = "";
            this.barProgressBarAttendance.BackStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.barProgressBarAttendance.ChunkGradientAngle = 0F;
            this.barProgressBarAttendance.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.barProgressBarAttendance.Name = "barProgressBarAttendance";
            this.barProgressBarAttendance.RecentlyUsed = false;
            // 
            // controlContainerItem2
            // 
            this.controlContainerItem2.AllowItemResize = false;
            this.controlContainerItem2.Control = this.lblPrgressPercentage;
            this.controlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem2.Name = "controlContainerItem2";
            // 
            // controlContainerItem3
            // 
            this.controlContainerItem3.AllowItemResize = false;
            this.controlContainerItem3.Control = this.lblProgressShow;
            this.controlContainerItem3.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.controlContainerItem3.Name = "controlContainerItem3";
            // 
            // dockSite7
            // 
            this.dockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockSite7.Location = new System.Drawing.Point(0, 0);
            this.dockSite7.Name = "dockSite7";
            this.dockSite7.Size = new System.Drawing.Size(277, 0);
            this.dockSite7.TabIndex = 7;
            this.dockSite7.TabStop = false;
            // 
            // DgvAttendance
            // 
            this.DgvAttendance.AddNewRow = false;
            this.DgvAttendance.AllowUserToAddRows = false;
            this.DgvAttendance.AllowUserToResizeColumns = false;
            this.DgvAttendance.AllowUserToResizeRows = false;
            this.DgvAttendance.AlphaNumericCols = new int[0];
            this.DgvAttendance.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvAttendance.CapsLockCols = new int[0];
            this.DgvAttendance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvAttendance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColDgvAtnDate,
            this.ColDgvEmployee,
            this.ColDgvStatus,
            this.ColDgvAddMore,
            this.ColDgvRemarks,
            this.ColDgvShift,
            this.ColDgvDay,
            this.ColDgvBreakTime,
            this.ColDgvWorkTime,
            this.ColDgvLateComing,
            this.ColDgvEarlyGoing,
            this.ColDgvExcessTime,
            this.ColDgvCompanyID,
            this.ColDgvConsequenceID,
            this.ColDgvPolicyID,
            this.ColDgvConseqAmt,
            this.ColDgvValidFlag,
            this.ColDgvShiftTimeDisplay,
            this.ColDgvAllowedBreakTime,
            this.ColDgvHolidayFlag,
            this.ColDgvLeaveFlag,
            this.ColDgvAbsentTime,
            this.ColDgvShiftOrderNo,
            this.ColDgvLeaveInfo,
            this.ColDgvLeaveId,
            this.ColDgvLeaveType,
            this.ColDgvPunchID,
            this.ColDgvProjectID});
            this.DgvAttendance.DecimalCols = new int[0];
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvAttendance.DefaultCellStyle = dataGridViewCellStyle1;
            this.DgvAttendance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvAttendance.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.DgvAttendance.HasSlNo = false;
            this.DgvAttendance.LastRowIndex = 0;
            this.DgvAttendance.Location = new System.Drawing.Point(0, 97);
            this.DgvAttendance.Name = "DgvAttendance";
            this.DgvAttendance.NegativeValueCols = new int[0];
            this.DgvAttendance.NumericCols = new int[0];
            this.DgvAttendance.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DgvAttendance.Size = new System.Drawing.Size(1250, 393);
            this.DgvAttendance.TabIndex = 2;
            this.DgvAttendance.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DgvAttendance_CellValidating);
            this.DgvAttendance.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvAttendance_CellEndEdit);
            this.DgvAttendance.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DgvAttendance_EditingControlShowing);
            this.DgvAttendance.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvAttendance_CurrentCellDirtyStateChanged);
            this.DgvAttendance.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvAttendance_DataError);
            this.DgvAttendance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvAttendance_KeyDown);
            // 
            // ColDgvAtnDate
            // 
            this.ColDgvAtnDate.FillWeight = 80F;
            this.ColDgvAtnDate.Frozen = true;
            this.ColDgvAtnDate.HeaderText = "Date";
            this.ColDgvAtnDate.Name = "ColDgvAtnDate";
            this.ColDgvAtnDate.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ColDgvAtnDate.Width = 80;
            // 
            // ColDgvEmployee
            // 
            this.ColDgvEmployee.FillWeight = 250F;
            this.ColDgvEmployee.Frozen = true;
            this.ColDgvEmployee.HeaderText = "Employee";
            this.ColDgvEmployee.Name = "ColDgvEmployee";
            this.ColDgvEmployee.ReadOnly = true;
            this.ColDgvEmployee.Visible = false;
            this.ColDgvEmployee.Width = 250;
            // 
            // ColDgvStatus
            // 
            this.ColDgvStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ColDgvStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ColDgvStatus.DisplayMember = "Text";
            this.ColDgvStatus.DropDownHeight = 106;
            this.ColDgvStatus.DropDownWidth = 121;
            this.ColDgvStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ColDgvStatus.Frozen = true;
            this.ColDgvStatus.HeaderText = "Status";
            this.ColDgvStatus.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ColDgvStatus.IntegralHeight = false;
            this.ColDgvStatus.ItemHeight = 15;
            this.ColDgvStatus.Name = "ColDgvStatus";
            this.ColDgvStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ColDgvStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColDgvStatus.WatermarkText = "Status";
            // 
            // ColDgvAddMore
            // 
            this.ColDgvAddMore.FillWeight = 35F;
            this.ColDgvAddMore.HeaderText = "";
            this.ColDgvAddMore.Name = "ColDgvAddMore";
            this.ColDgvAddMore.Text = null;
            this.ColDgvAddMore.Width = 35;
            // 
            // ColDgvRemarks
            // 
            this.ColDgvRemarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColDgvRemarks.FillWeight = 150F;
            this.ColDgvRemarks.HeaderText = "Remarks";
            this.ColDgvRemarks.MaxInputLength = 190;
            this.ColDgvRemarks.MinimumWidth = 150;
            this.ColDgvRemarks.Name = "ColDgvRemarks";
            this.ColDgvRemarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColDgvShift
            // 
            this.ColDgvShift.HeaderText = "Shift";
            this.ColDgvShift.Name = "ColDgvShift";
            this.ColDgvShift.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // ColDgvDay
            // 
            this.ColDgvDay.HeaderText = "Day";
            this.ColDgvDay.Name = "ColDgvDay";
            this.ColDgvDay.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ColDgvDay.Visible = false;
            // 
            // ColDgvBreakTime
            // 
            this.ColDgvBreakTime.HeaderText = "BreakTime";
            this.ColDgvBreakTime.Name = "ColDgvBreakTime";
            this.ColDgvBreakTime.TextAlignment = System.Drawing.StringAlignment.Center;
            this.ColDgvBreakTime.Visible = false;
            // 
            // ColDgvWorkTime
            // 
            this.ColDgvWorkTime.HeaderText = "WorkTime";
            this.ColDgvWorkTime.Name = "ColDgvWorkTime";
            this.ColDgvWorkTime.Visible = false;
            // 
            // ColDgvLateComing
            // 
            this.ColDgvLateComing.HeaderText = "LateComing";
            this.ColDgvLateComing.Name = "ColDgvLateComing";
            this.ColDgvLateComing.Visible = false;
            // 
            // ColDgvEarlyGoing
            // 
            this.ColDgvEarlyGoing.HeaderText = "EarlyGoing";
            this.ColDgvEarlyGoing.Name = "ColDgvEarlyGoing";
            this.ColDgvEarlyGoing.Visible = false;
            // 
            // ColDgvExcessTime
            // 
            this.ColDgvExcessTime.HeaderText = "ExcessTime";
            this.ColDgvExcessTime.Name = "ColDgvExcessTime";
            this.ColDgvExcessTime.Visible = false;
            // 
            // ColDgvCompanyID
            // 
            this.ColDgvCompanyID.HeaderText = "CompanyID";
            this.ColDgvCompanyID.Name = "ColDgvCompanyID";
            this.ColDgvCompanyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColDgvCompanyID.Visible = false;
            // 
            // ColDgvConsequenceID
            // 
            this.ColDgvConsequenceID.HeaderText = "ConsequenceID";
            this.ColDgvConsequenceID.Name = "ColDgvConsequenceID";
            this.ColDgvConsequenceID.Visible = false;
            // 
            // ColDgvPolicyID
            // 
            this.ColDgvPolicyID.HeaderText = "PolicyID";
            this.ColDgvPolicyID.Name = "ColDgvPolicyID";
            this.ColDgvPolicyID.Visible = false;
            // 
            // ColDgvConseqAmt
            // 
            this.ColDgvConseqAmt.HeaderText = "ConseqAmt";
            this.ColDgvConseqAmt.Name = "ColDgvConseqAmt";
            this.ColDgvConseqAmt.Visible = false;
            // 
            // ColDgvValidFlag
            // 
            this.ColDgvValidFlag.HeaderText = "ValidFlag";
            this.ColDgvValidFlag.Name = "ColDgvValidFlag";
            this.ColDgvValidFlag.Visible = false;
            // 
            // ColDgvShiftTimeDisplay
            // 
            this.ColDgvShiftTimeDisplay.HeaderText = "ShiftTimeDisplay";
            this.ColDgvShiftTimeDisplay.Name = "ColDgvShiftTimeDisplay";
            this.ColDgvShiftTimeDisplay.Visible = false;
            // 
            // ColDgvAllowedBreakTime
            // 
            this.ColDgvAllowedBreakTime.HeaderText = "AllowedBreakTime";
            this.ColDgvAllowedBreakTime.Name = "ColDgvAllowedBreakTime";
            this.ColDgvAllowedBreakTime.Visible = false;
            // 
            // ColDgvHolidayFlag
            // 
            this.ColDgvHolidayFlag.HeaderText = "HolidayFlag";
            this.ColDgvHolidayFlag.Name = "ColDgvHolidayFlag";
            this.ColDgvHolidayFlag.Visible = false;
            // 
            // ColDgvLeaveFlag
            // 
            this.ColDgvLeaveFlag.HeaderText = "LeaveFlag";
            this.ColDgvLeaveFlag.Name = "ColDgvLeaveFlag";
            this.ColDgvLeaveFlag.Visible = false;
            // 
            // ColDgvAbsentTime
            // 
            this.ColDgvAbsentTime.HeaderText = "AbsentTime";
            this.ColDgvAbsentTime.Name = "ColDgvAbsentTime";
            this.ColDgvAbsentTime.Visible = false;
            // 
            // ColDgvShiftOrderNo
            // 
            this.ColDgvShiftOrderNo.HeaderText = "ShiftOrderNo";
            this.ColDgvShiftOrderNo.Name = "ColDgvShiftOrderNo";
            this.ColDgvShiftOrderNo.Visible = false;
            // 
            // ColDgvLeaveInfo
            // 
            this.ColDgvLeaveInfo.HeaderText = "LeaveInfo";
            this.ColDgvLeaveInfo.Name = "ColDgvLeaveInfo";
            this.ColDgvLeaveInfo.Visible = false;
            // 
            // ColDgvLeaveId
            // 
            this.ColDgvLeaveId.HeaderText = "LeaveId";
            this.ColDgvLeaveId.Name = "ColDgvLeaveId";
            this.ColDgvLeaveId.Visible = false;
            // 
            // ColDgvLeaveType
            // 
            this.ColDgvLeaveType.HeaderText = "LeaveType";
            this.ColDgvLeaveType.Name = "ColDgvLeaveType";
            this.ColDgvLeaveType.Visible = false;
            // 
            // ColDgvPunchID
            // 
            this.ColDgvPunchID.HeaderText = "PunchID";
            this.ColDgvPunchID.Name = "ColDgvPunchID";
            this.ColDgvPunchID.Visible = false;
            // 
            // ColDgvProjectID
            // 
            this.ColDgvProjectID.HeaderText = "ProjectID";
            this.ColDgvProjectID.Name = "ColDgvProjectID";
            this.ColDgvProjectID.Visible = false;
            // 
            // DTPFormatTime
            // 
            this.DTPFormatTime.CustomFormat = "HH:mm";
            this.DTPFormatTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPFormatTime.Location = new System.Drawing.Point(1170, 121);
            this.DTPFormatTime.Name = "DTPFormatTime";
            this.DTPFormatTime.ShowUpDown = true;
            this.DTPFormatTime.Size = new System.Drawing.Size(68, 20);
            this.DTPFormatTime.TabIndex = 4;
            this.DTPFormatTime.TabStop = false;
            // 
            // CboCompany
            // 
            this.CboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompany.FormattingEnabled = true;
            this.CboCompany.Location = new System.Drawing.Point(90, 8);
            this.CboCompany.Name = "CboCompany";
            this.CboCompany.Size = new System.Drawing.Size(247, 21);
            this.CboCompany.TabIndex = 163;
            this.CboCompany.SelectedIndexChanged += new System.EventHandler(this.CboCompany_SelectedIndexChanged);
            // 
            // CboBranch
            // 
            this.CboBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboBranch.FormattingEnabled = true;
            this.CboBranch.Location = new System.Drawing.Point(90, 35);
            this.CboBranch.Name = "CboBranch";
            this.CboBranch.Size = new System.Drawing.Size(247, 21);
            this.CboBranch.TabIndex = 164;
            this.CboBranch.SelectedIndexChanged += new System.EventHandler(this.CboBranch_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 165;
            this.label5.Text = "Branch";
            // 
            // label6
            // 
            this.label6.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 166;
            this.label6.Text = "Company";
            // 
            // PanelTop
            // 
            this.PanelTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.PanelTop.Controls.Add(this.chkIncludeCompany);
            this.PanelTop.Controls.Add(this.CboCompany);
            this.PanelTop.Controls.Add(this.label6);
            this.PanelTop.Controls.Add(this.label5);
            this.PanelTop.Controls.Add(this.CboBranch);
            this.PanelTop.Controls.Add(this.btnExcelExport);
            this.PanelTop.Controls.Add(this.txtCount);
            this.PanelTop.Controls.Add(this.btnShow);
            this.PanelTop.Controls.Add(this.btnSave);
            this.PanelTop.Controls.Add(this.cboDepartment);
            this.PanelTop.Controls.Add(this.label4);
            this.PanelTop.Controls.Add(this.lblDepartment);
            this.PanelTop.Controls.Add(this.cboDesignation);
            this.PanelTop.Controls.Add(this.label2);
            this.PanelTop.Controls.Add(this.label3);
            this.PanelTop.Controls.Add(this.CboEmployee);
            this.PanelTop.Controls.Add(this.label1);
            this.PanelTop.Controls.Add(this.dtpMonthSelect);
            this.PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTop.Location = new System.Drawing.Point(0, 0);
            this.PanelTop.Name = "PanelTop";
            this.PanelTop.Size = new System.Drawing.Size(1250, 97);
            this.PanelTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelTop.Style.GradientAngle = 90;
            this.PanelTop.TabIndex = 167;
            // 
            // chkIncludeCompany
            // 
            this.chkIncludeCompany.AutoSize = true;
            // 
            // 
            // 
            this.chkIncludeCompany.BackgroundStyle.Class = "";
            this.chkIncludeCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkIncludeCompany.Checked = true;
            this.chkIncludeCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeCompany.CheckValue = "Y";
            this.chkIncludeCompany.Location = new System.Drawing.Point(90, 65);
            this.chkIncludeCompany.Name = "chkIncludeCompany";
            this.chkIncludeCompany.Size = new System.Drawing.Size(108, 15);
            this.chkIncludeCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkIncludeCompany.TabIndex = 167;
            this.chkIncludeCompany.Text = "Include Company";
            this.chkIncludeCompany.CheckedChanged += new System.EventHandler(this.chkIncludeCompany_CheckedChanged);
            // 
            // FrmAttendanceAuto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1250, 513);
            this.Controls.Add(this.DgvAttendance);
            this.Controls.Add(this.DTPFormatTime);
            this.Controls.Add(this.PanelTop);
            this.Controls.Add(this.BarRecordCnt);
            this.Name = "FrmAttendanceAuto";
            this.Text = "Attendance Auto";
            ((System.ComponentModel.ISupportInitialize)(this.BarRecordCnt)).EndInit();
            this.BarRecordCnt.ResumeLayout(false);
            this.BarRecordCnt.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAttendance)).EndInit();
            this.PanelTop.ResumeLayout(false);
            this.PanelTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.DockSite dockSite7;
        private ClsDataGirdViewX DgvAttendance;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvAtnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvEmployee;
        private DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn ColDgvStatus;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn ColDgvAddMore;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvRemarks;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvShift;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvDay;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvBreakTime;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvWorkTime;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvLateComing;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvEarlyGoing;
        private DevComponents.DotNetBar.Controls.DataGridViewLabelXColumn ColDgvExcessTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvCompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvConsequenceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvPolicyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvConseqAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvValidFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvShiftTimeDisplay;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvAllowedBreakTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvHolidayFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvLeaveFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvAbsentTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvShiftOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvLeaveInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvLeaveId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvLeaveType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvPunchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDgvProjectID;
        private System.Windows.Forms.DateTimePicker dtpMonthSelect;
        private DevComponents.DotNetBar.LabelX lblPrgressPercentage;
        private DevComponents.DotNetBar.LabelX lblProgressShow;
        internal System.Windows.Forms.DateTimePicker DTPFormatTime;
        private DevComponents.DotNetBar.Bar BarRecordCnt;
        private DevComponents.DotNetBar.ProgressBarItem barProgressBarAttendance;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem2;
        private DevComponents.DotNetBar.ControlContainerItem controlContainerItem3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cboDesignation;
        private System.Windows.Forms.ComboBox cboDepartment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CboEmployee;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private System.Windows.Forms.TextBox txtCount;
        private System.Windows.Forms.Label label4;
        private DevComponents.DotNetBar.ButtonX btnExcelExport;
        private DevComponents.DotNetBar.ButtonItem BtnAutoFill;
        private DevComponents.DotNetBar.ButtonItem BtnClear;
        private System.Windows.Forms.ComboBox CboBranch;
        private System.Windows.Forms.ComboBox CboCompany;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private DevComponents.DotNetBar.PanelEx PanelTop;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkIncludeCompany;
    }
}