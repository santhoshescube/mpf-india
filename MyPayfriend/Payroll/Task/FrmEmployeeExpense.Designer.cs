﻿namespace MyPayfriend
{
    partial class FrmEmployeeExpense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label DateLabel;
            System.Windows.Forms.Label AmountLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeeExpense));
            this.EmployeeExpenseBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAddNew = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDocument = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatusShow = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblJoiningDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDocumentAttached = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.lblReturnAmount = new System.Windows.Forms.Label();
            this.dtpExpenseFrom = new System.Windows.Forms.DateTimePicker();
            this.txtActualAmount = new System.Windows.Forms.TextBox();
            this.rdbNone = new System.Windows.Forms.RadioButton();
            this.rdbReimbursement = new System.Windows.Forms.RadioButton();
            this.dtpExpenseTo = new System.Windows.Forms.DateTimePicker();
            this.rdbDeduction = new System.Windows.Forms.RadioButton();
            this.btnExpenseType = new System.Windows.Forms.Button();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cboExpenseType = new System.Windows.Forms.ComboBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.errValidate = new System.Windows.Forms.ErrorProvider(this.components);
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            label4 = new System.Windows.Forms.Label();
            DateLabel = new System.Windows.Forms.Label();
            AmountLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeExpenseBindingNavigator)).BeginInit();
            this.EmployeeExpenseBindingNavigator.SuspendLayout();
            this.ssStatus.SuspendLayout();
            this.GrpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(13, 232);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(49, 13);
            label4.TabIndex = 24;
            label4.Text = "Remarks";
            // 
            // DateLabel
            // 
            DateLabel.AutoSize = true;
            DateLabel.Location = new System.Drawing.Point(13, 131);
            DateLabel.Name = "DateLabel";
            DateLabel.Size = new System.Drawing.Size(74, 13);
            DateLabel.TabIndex = 20;
            DateLabel.Text = "Expense Date";
            // 
            // AmountLabel
            // 
            AmountLabel.AutoSize = true;
            AmountLabel.Location = new System.Drawing.Point(13, 157);
            AmountLabel.Name = "AmountLabel";
            AmountLabel.Size = new System.Drawing.Size(43, 13);
            AmountLabel.TabIndex = 21;
            AmountLabel.Text = "Amount";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(13, 78);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(31, 13);
            label1.TabIndex = 17;
            label1.Text = "Type";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(13, 104);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(30, 13);
            label5.TabIndex = 18;
            label5.Text = "From";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(209, 104);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(20, 13);
            label6.TabIndex = 19;
            label6.Text = "To";
            // 
            // EmployeeExpenseBindingNavigator
            // 
            this.EmployeeExpenseBindingNavigator.AddNewItem = null;
            this.EmployeeExpenseBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeeExpenseBindingNavigator.DeleteItem = null;
            this.EmployeeExpenseBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.btnAddNew,
            this.btnSaveItem,
            this.btnDelete,
            this.btnClear,
            this.toolStripSeparator1,
            this.btnDocument,
            this.toolStripSeparator3,
            this.btnPrint,
            this.btnEmail,
            this.toolStripSeparator2,
            this.btnHelp});
            this.EmployeeExpenseBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeExpenseBindingNavigator.MoveFirstItem = null;
            this.EmployeeExpenseBindingNavigator.MoveLastItem = null;
            this.EmployeeExpenseBindingNavigator.MoveNextItem = null;
            this.EmployeeExpenseBindingNavigator.MovePreviousItem = null;
            this.EmployeeExpenseBindingNavigator.Name = "EmployeeExpenseBindingNavigator";
            this.EmployeeExpenseBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeeExpenseBindingNavigator.Size = new System.Drawing.Size(444, 25);
            this.EmployeeExpenseBindingNavigator.TabIndex = 4;
            this.EmployeeExpenseBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAddNew
            // 
            this.btnAddNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddNew.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNew.Image")));
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.RightToLeftAutoMirrorImage = true;
            this.btnAddNew.Size = new System.Drawing.Size(23, 22);
            this.btnAddNew.Text = "Add new";
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save Data";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDocument
            // 
            this.btnDocument.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDocument.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.btnDocument.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDocument.Name = "btnDocument";
            this.btnDocument.Size = new System.Drawing.Size(23, 22);
            this.btnDocument.Text = "Attach Document";
            this.btnDocument.Click += new System.EventHandler(this.btnDocument_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusShow,
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 412);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(444, 22);
            this.ssStatus.TabIndex = 5;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblStatusShow
            // 
            this.lblStatusShow.Name = "lblStatusShow";
            this.lblStatusShow.Size = new System.Drawing.Size(48, 17);
            this.lblStatusShow.Text = "Status : ";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.label7);
            this.GrpMain.Controls.Add(this.lblJoiningDate);
            this.GrpMain.Controls.Add(this.label3);
            this.GrpMain.Controls.Add(this.lblDocumentAttached);
            this.GrpMain.Controls.Add(this.lblCurrency);
            this.GrpMain.Controls.Add(this.lblEmployee);
            this.GrpMain.Controls.Add(this.lblReturnAmount);
            this.GrpMain.Controls.Add(this.dtpExpenseFrom);
            this.GrpMain.Controls.Add(label5);
            this.GrpMain.Controls.Add(this.txtActualAmount);
            this.GrpMain.Controls.Add(label6);
            this.GrpMain.Controls.Add(this.rdbNone);
            this.GrpMain.Controls.Add(this.rdbReimbursement);
            this.GrpMain.Controls.Add(this.dtpExpenseTo);
            this.GrpMain.Controls.Add(this.rdbDeduction);
            this.GrpMain.Controls.Add(AmountLabel);
            this.GrpMain.Controls.Add(DateLabel);
            this.GrpMain.Controls.Add(this.btnExpenseType);
            this.GrpMain.Controls.Add(this.dtpDate);
            this.GrpMain.Controls.Add(label1);
            this.GrpMain.Controls.Add(this.cboExpenseType);
            this.GrpMain.Controls.Add(this.txtAmount);
            this.GrpMain.Controls.Add(this.txtRemarks);
            this.GrpMain.Controls.Add(label4);
            this.GrpMain.Controls.Add(this.Label2);
            this.GrpMain.Controls.Add(this.cboEmployee);
            this.GrpMain.Location = new System.Drawing.Point(7, 57);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(430, 320);
            this.GrpMain.TabIndex = 0;
            this.GrpMain.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(94, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = ":";
            // 
            // lblJoiningDate
            // 
            this.lblJoiningDate.AutoSize = true;
            this.lblJoiningDate.Location = new System.Drawing.Point(110, 52);
            this.lblJoiningDate.Name = "lblJoiningDate";
            this.lblJoiningDate.Size = new System.Drawing.Size(66, 13);
            this.lblJoiningDate.TabIndex = 16;
            this.lblJoiningDate.Text = "Joining Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Joining Date";
            // 
            // lblDocumentAttached
            // 
            this.lblDocumentAttached.AutoSize = true;
            this.lblDocumentAttached.Location = new System.Drawing.Point(13, 298);
            this.lblDocumentAttached.Name = "lblDocumentAttached";
            this.lblDocumentAttached.Size = new System.Drawing.Size(130, 13);
            this.lblDocumentAttached.TabIndex = 25;
            this.lblDocumentAttached.Text = "No Document(s) Attached";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrency.Location = new System.Drawing.Point(209, 157);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(28, 13);
            this.lblCurrency.TabIndex = 22;
            this.lblCurrency.Text = "CUR";
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(13, 25);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblEmployee.TabIndex = 13;
            this.lblEmployee.Text = "Employee";
            // 
            // lblReturnAmount
            // 
            this.lblReturnAmount.AutoSize = true;
            this.lblReturnAmount.Location = new System.Drawing.Point(13, 206);
            this.lblReturnAmount.Name = "lblReturnAmount";
            this.lblReturnAmount.Size = new System.Drawing.Size(76, 13);
            this.lblReturnAmount.TabIndex = 23;
            this.lblReturnAmount.Text = "Actual Amount";
            // 
            // dtpExpenseFrom
            // 
            this.dtpExpenseFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpenseFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpenseFrom.Location = new System.Drawing.Point(97, 102);
            this.dtpExpenseFrom.Name = "dtpExpenseFrom";
            this.dtpExpenseFrom.Size = new System.Drawing.Size(106, 20);
            this.dtpExpenseFrom.TabIndex = 3;
            this.dtpExpenseFrom.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // txtActualAmount
            // 
            this.txtActualAmount.Location = new System.Drawing.Point(97, 203);
            this.txtActualAmount.MaxLength = 12;
            this.txtActualAmount.Name = "txtActualAmount";
            this.txtActualAmount.Size = new System.Drawing.Size(106, 20);
            this.txtActualAmount.TabIndex = 10;
            this.txtActualAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtActualAmount.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            this.txtActualAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // rdbNone
            // 
            this.rdbNone.AutoSize = true;
            this.rdbNone.Checked = true;
            this.rdbNone.Location = new System.Drawing.Point(97, 180);
            this.rdbNone.Name = "rdbNone";
            this.rdbNone.Size = new System.Drawing.Size(51, 17);
            this.rdbNone.TabIndex = 7;
            this.rdbNone.TabStop = true;
            this.rdbNone.Text = "None";
            this.rdbNone.UseVisualStyleBackColor = true;
            this.rdbNone.CheckedChanged += new System.EventHandler(this.rdbNone_CheckedChanged);
            // 
            // rdbReimbursement
            // 
            this.rdbReimbursement.AutoSize = true;
            this.rdbReimbursement.Location = new System.Drawing.Point(154, 180);
            this.rdbReimbursement.Name = "rdbReimbursement";
            this.rdbReimbursement.Size = new System.Drawing.Size(98, 17);
            this.rdbReimbursement.TabIndex = 8;
            this.rdbReimbursement.Text = "Reimbursement";
            this.rdbReimbursement.UseVisualStyleBackColor = true;
            this.rdbReimbursement.CheckedChanged += new System.EventHandler(this.rdbReimbursement_CheckedChanged);
            // 
            // dtpExpenseTo
            // 
            this.dtpExpenseTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpenseTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpenseTo.Location = new System.Drawing.Point(234, 102);
            this.dtpExpenseTo.Name = "dtpExpenseTo";
            this.dtpExpenseTo.Size = new System.Drawing.Size(106, 20);
            this.dtpExpenseTo.TabIndex = 4;
            this.dtpExpenseTo.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // rdbDeduction
            // 
            this.rdbDeduction.AutoSize = true;
            this.rdbDeduction.Location = new System.Drawing.Point(258, 180);
            this.rdbDeduction.Name = "rdbDeduction";
            this.rdbDeduction.Size = new System.Drawing.Size(74, 17);
            this.rdbDeduction.TabIndex = 9;
            this.rdbDeduction.Text = "Deduction";
            this.rdbDeduction.UseVisualStyleBackColor = true;
            this.rdbDeduction.CheckedChanged += new System.EventHandler(this.rdbDeduction_CheckedChanged);
            // 
            // btnExpenseType
            // 
            this.btnExpenseType.Location = new System.Drawing.Point(308, 74);
            this.btnExpenseType.Name = "btnExpenseType";
            this.btnExpenseType.Size = new System.Drawing.Size(32, 23);
            this.btnExpenseType.TabIndex = 2;
            this.btnExpenseType.Text = "...";
            this.btnExpenseType.UseVisualStyleBackColor = true;
            this.btnExpenseType.Click += new System.EventHandler(this.btnExpenseType_Click);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(97, 128);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(106, 20);
            this.dtpDate.TabIndex = 5;
            this.dtpDate.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // cboExpenseType
            // 
            this.cboExpenseType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboExpenseType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboExpenseType.BackColor = System.Drawing.SystemColors.Info;
            this.cboExpenseType.DropDownHeight = 134;
            this.cboExpenseType.FormattingEnabled = true;
            this.cboExpenseType.IntegralHeight = false;
            this.cboExpenseType.Location = new System.Drawing.Point(96, 75);
            this.cboExpenseType.Name = "cboExpenseType";
            this.cboExpenseType.Size = new System.Drawing.Size(206, 21);
            this.cboExpenseType.TabIndex = 1;
            this.cboExpenseType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboExpenseType.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtAmount.Location = new System.Drawing.Point(97, 154);
            this.txtAmount.MaxLength = 12;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(106, 20);
            this.txtAmount.TabIndex = 6;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(97, 229);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRemarks.Size = new System.Drawing.Size(320, 60);
            this.txtRemarks.TabIndex = 11;
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(7, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(80, 13);
            this.Label2.TabIndex = 12;
            this.Label2.Text = "Expense Info";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(97, 22);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(320, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboEmployee.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(7, 383);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(362, 383);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(281, 383);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // tmrClear
            // 
            this.tmrClear.Interval = 1000;
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // errValidate
            // 
            this.errValidate.ContainerControl = this;
            this.errValidate.RightToLeft = true;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(444, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 90;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // FrmEmployeeExpense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 434);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.EmployeeExpenseBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEmployeeExpense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expense";
            this.Load += new System.EventHandler(this.FrmEmployeeExpense_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEmployeeExpense_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmEmployeeExpense_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeExpenseBindingNavigator)).EndInit();
            this.EmployeeExpenseBindingNavigator.ResumeLayout(false);
            this.EmployeeExpenseBindingNavigator.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator EmployeeExpenseBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton btnAddNew;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatusShow;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.TextBox txtActualAmount;
        internal System.Windows.Forms.DateTimePicker dtpDate;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.ComboBox cboExpenseType;
        internal System.Windows.Forms.DateTimePicker dtpExpenseTo;
        internal System.Windows.Forms.DateTimePicker dtpExpenseFrom;
        private System.Windows.Forms.RadioButton rdbDeduction;
        private System.Windows.Forms.RadioButton rdbReimbursement;
        private System.Windows.Forms.RadioButton rdbNone;
        private System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.ErrorProvider errValidate;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.ToolStripButton btnClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.Button btnExpenseType;
        private System.Windows.Forms.Label lblReturnAmount;
        private System.Windows.Forms.Label lblEmployee;
        internal System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.ToolStripButton btnDocument;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Label lblDocumentAttached;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblJoiningDate;
        private System.Windows.Forms.Label label3;
    }
}