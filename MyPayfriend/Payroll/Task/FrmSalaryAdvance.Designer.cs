﻿namespace MyPayfriend
{
    partial class FrmSalaryAdvance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label DateLabel;
            System.Windows.Forms.Label AmountLabel;
            System.Windows.Forms.Label label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalaryAdvance));
            this.SalaryAdvanceBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAddNew = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblJoiningDate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTotalSalaryAdvance = new System.Windows.Forms.Label();
            this.lblTotalAdvancePeriod = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblWarning = new System.Windows.Forms.Label();
            this.lblSalAdvPercentAmt = new System.Windows.Forms.Label();
            this.lblSalAdvPercent = new System.Windows.Forms.Label();
            this.lblBasicPayAmount = new System.Windows.Forms.Label();
            this.lblBasicPay = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.Label2 = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatusShow = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.errValidate = new System.Windows.Forms.ErrorProvider(this.components);
            DateLabel = new System.Windows.Forms.Label();
            AmountLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SalaryAdvanceBindingNavigator)).BeginInit();
            this.SalaryAdvanceBindingNavigator.SuspendLayout();
            this.GrpMain.SuspendLayout();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).BeginInit();
            this.SuspendLayout();
            // 
            // DateLabel
            // 
            DateLabel.AutoSize = true;
            DateLabel.Location = new System.Drawing.Point(13, 104);
            DateLabel.Name = "DateLabel";
            DateLabel.Size = new System.Drawing.Size(30, 13);
            DateLabel.TabIndex = 3;
            DateLabel.Text = "Date";
            // 
            // AmountLabel
            // 
            AmountLabel.AutoSize = true;
            AmountLabel.Location = new System.Drawing.Point(13, 156);
            AmountLabel.Name = "AmountLabel";
            AmountLabel.Size = new System.Drawing.Size(43, 13);
            AmountLabel.TabIndex = 7;
            AmountLabel.Text = "Amount";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(13, 182);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(49, 13);
            label1.TabIndex = 1041;
            label1.Text = "Remarks";
            // 
            // SalaryAdvanceBindingNavigator
            // 
            this.SalaryAdvanceBindingNavigator.AddNewItem = null;
            this.SalaryAdvanceBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.SalaryAdvanceBindingNavigator.DeleteItem = null;
            this.SalaryAdvanceBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.btnAddNew,
            this.btnSaveItem,
            this.btnDelete,
            this.btnClear,
            this.toolStripSeparator1,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnHelp});
            this.SalaryAdvanceBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.SalaryAdvanceBindingNavigator.MoveFirstItem = null;
            this.SalaryAdvanceBindingNavigator.MoveLastItem = null;
            this.SalaryAdvanceBindingNavigator.MoveNextItem = null;
            this.SalaryAdvanceBindingNavigator.MovePreviousItem = null;
            this.SalaryAdvanceBindingNavigator.Name = "SalaryAdvanceBindingNavigator";
            this.SalaryAdvanceBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.SalaryAdvanceBindingNavigator.Size = new System.Drawing.Size(404, 25);
            this.SalaryAdvanceBindingNavigator.TabIndex = 302;
            this.SalaryAdvanceBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAddNew
            // 
            this.btnAddNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddNew.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNew.Image")));
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.RightToLeftAutoMirrorImage = true;
            this.btnAddNew.Size = new System.Drawing.Size(23, 22);
            this.btnAddNew.Text = "Add new";
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save Data";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(7, 310);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(322, 310);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(241, 310);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.label5);
            this.GrpMain.Controls.Add(this.lblJoiningDate);
            this.GrpMain.Controls.Add(this.label6);
            this.GrpMain.Controls.Add(this.lblEmployee);
            this.GrpMain.Controls.Add(this.label4);
            this.GrpMain.Controls.Add(this.lblTotalSalaryAdvance);
            this.GrpMain.Controls.Add(this.lblTotalAdvancePeriod);
            this.GrpMain.Controls.Add(this.lblCurrency);
            this.GrpMain.Controls.Add(this.label3);
            this.GrpMain.Controls.Add(this.label13);
            this.GrpMain.Controls.Add(label1);
            this.GrpMain.Controls.Add(this.lblWarning);
            this.GrpMain.Controls.Add(this.lblSalAdvPercentAmt);
            this.GrpMain.Controls.Add(this.lblSalAdvPercent);
            this.GrpMain.Controls.Add(this.lblBasicPayAmount);
            this.GrpMain.Controls.Add(this.lblBasicPay);
            this.GrpMain.Controls.Add(this.dtpDate);
            this.GrpMain.Controls.Add(DateLabel);
            this.GrpMain.Controls.Add(this.Label2);
            this.GrpMain.Controls.Add(this.cboEmployee);
            this.GrpMain.Controls.Add(this.txtAmount);
            this.GrpMain.Controls.Add(AmountLabel);
            this.GrpMain.Controls.Add(this.txtRemarks);
            this.GrpMain.Location = new System.Drawing.Point(7, 56);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(390, 248);
            this.GrpMain.TabIndex = 0;
            this.GrpMain.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(92, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 1055;
            this.label5.Text = ":";
            // 
            // lblJoiningDate
            // 
            this.lblJoiningDate.AutoSize = true;
            this.lblJoiningDate.Location = new System.Drawing.Point(108, 52);
            this.lblJoiningDate.Name = "lblJoiningDate";
            this.lblJoiningDate.Size = new System.Drawing.Size(66, 13);
            this.lblJoiningDate.TabIndex = 1054;
            this.lblJoiningDate.Text = "Joining Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 1053;
            this.label6.Text = "Joining Date";
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(13, 25);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblEmployee.TabIndex = 1048;
            this.lblEmployee.Text = "Employee";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(274, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 1047;
            this.label4.Text = ":";
            // 
            // lblTotalSalaryAdvance
            // 
            this.lblTotalSalaryAdvance.AutoSize = true;
            this.lblTotalSalaryAdvance.Location = new System.Drawing.Point(290, 130);
            this.lblTotalSalaryAdvance.Name = "lblTotalSalaryAdvance";
            this.lblTotalSalaryAdvance.Size = new System.Drawing.Size(43, 13);
            this.lblTotalSalaryAdvance.TabIndex = 1046;
            this.lblTotalSalaryAdvance.Text = "Amount";
            // 
            // lblTotalAdvancePeriod
            // 
            this.lblTotalAdvancePeriod.AutoSize = true;
            this.lblTotalAdvancePeriod.Location = new System.Drawing.Point(13, 130);
            this.lblTotalAdvancePeriod.Name = "lblTotalAdvancePeriod";
            this.lblTotalAdvancePeriod.Size = new System.Drawing.Size(249, 13);
            this.lblTotalAdvancePeriod.TabIndex = 1045;
            this.lblTotalAdvancePeriod.Text = "Total Salary Advance (01 Aug 2013 - 31 Aug 2013)";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrency.Location = new System.Drawing.Point(207, 156);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(28, 13);
            this.lblCurrency.TabIndex = 1044;
            this.lblCurrency.Text = "CUR";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(274, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 1043;
            this.label3.Text = ":";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(92, 78);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 13);
            this.label13.TabIndex = 1042;
            this.label13.Text = ":";
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarning.Location = new System.Drawing.Point(8, 288);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(67, 11);
            this.lblWarning.TabIndex = 1040;
            this.lblWarning.Text = "*** Basic Pay";
            // 
            // lblSalAdvPercentAmt
            // 
            this.lblSalAdvPercentAmt.AutoSize = true;
            this.lblSalAdvPercentAmt.Location = new System.Drawing.Point(290, 78);
            this.lblSalAdvPercentAmt.Name = "lblSalAdvPercentAmt";
            this.lblSalAdvPercentAmt.Size = new System.Drawing.Size(61, 13);
            this.lblSalAdvPercentAmt.TabIndex = 1039;
            this.lblSalAdvPercentAmt.Text = "Advance %";
            // 
            // lblSalAdvPercent
            // 
            this.lblSalAdvPercent.AutoSize = true;
            this.lblSalAdvPercent.Location = new System.Drawing.Point(207, 78);
            this.lblSalAdvPercent.Name = "lblSalAdvPercent";
            this.lblSalAdvPercent.Size = new System.Drawing.Size(61, 13);
            this.lblSalAdvPercent.TabIndex = 1038;
            this.lblSalAdvPercent.Text = "Advance %";
            // 
            // lblBasicPayAmount
            // 
            this.lblBasicPayAmount.AutoSize = true;
            this.lblBasicPayAmount.Location = new System.Drawing.Point(108, 78);
            this.lblBasicPayAmount.Name = "lblBasicPayAmount";
            this.lblBasicPayAmount.Size = new System.Drawing.Size(34, 13);
            this.lblBasicPayAmount.TabIndex = 1037;
            this.lblBasicPayAmount.Text = "Gross";
            // 
            // lblBasicPay
            // 
            this.lblBasicPay.AutoSize = true;
            this.lblBasicPay.Location = new System.Drawing.Point(13, 78);
            this.lblBasicPay.Name = "lblBasicPay";
            this.lblBasicPay.Size = new System.Drawing.Size(73, 13);
            this.lblBasicPay.TabIndex = 1036;
            this.lblBasicPay.Text = "Gross Amount";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(95, 101);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(106, 20);
            this.dtpDate.TabIndex = 1;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(7, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(82, 13);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Advance Info";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(95, 22);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(282, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboEmployee.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtAmount.Location = new System.Drawing.Point(95, 153);
            this.txtAmount.MaxLength = 12;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(106, 20);
            this.txtAmount.TabIndex = 2;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(95, 179);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRemarks.Size = new System.Drawing.Size(282, 60);
            this.txtRemarks.TabIndex = 7;
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusShow,
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 342);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(404, 22);
            this.ssStatus.TabIndex = 311;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblStatusShow
            // 
            this.lblStatusShow.Name = "lblStatusShow";
            this.lblStatusShow.Size = new System.Drawing.Size(48, 17);
            this.lblStatusShow.Text = "Status : ";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(404, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 312;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // errValidate
            // 
            this.errValidate.ContainerControl = this;
            this.errValidate.RightToLeft = true;
            // 
            // FrmSalaryAdvance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 364);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.SalaryAdvanceBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSalaryAdvance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Salary Advance";
            this.Load += new System.EventHandler(this.FrmSalaryAdvance_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSalaryAdvance_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSalaryAdvance_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.SalaryAdvanceBindingNavigator)).EndInit();
            this.SalaryAdvanceBindingNavigator.ResumeLayout(false);
            this.SalaryAdvanceBindingNavigator.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator SalaryAdvanceBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton btnAddNew;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.DateTimePicker dtpDate;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatusShow;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        private System.Windows.Forms.Timer tmrClear;
        internal System.Windows.Forms.Label lblSalAdvPercentAmt;
        internal System.Windows.Forms.Label lblSalAdvPercent;
        internal System.Windows.Forms.Label lblBasicPayAmount;
        internal System.Windows.Forms.Label lblBasicPay;
        internal System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label lblCurrency;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label lblTotalSalaryAdvance;
        internal System.Windows.Forms.Label lblTotalAdvancePeriod;
        private System.Windows.Forms.Label lblEmployee;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblJoiningDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ErrorProvider errValidate;
    }
}