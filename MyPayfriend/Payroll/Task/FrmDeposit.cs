﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
/*
 * Created By       : Tijo
 * Created Date     : 14 Aug 2013
 * Purpose          : For Employee Deposit
*/
namespace MyPayfriend
{    
    public partial class FrmDeposit : Form
    {
        clsBLLDeposit MobjclsBLLDeposit = new clsBLLDeposit();
        long TotalRecordCnt, CurrentRecCnt;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission, MblnRefundPermission;
        bool mblnAddStatus = true;
        private clsMessage objUserMessage = null;
        string strBindingOf = "Of ";

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.Deposit);

                return this.objUserMessage;
            }
        }

        public FrmDeposit()
        {
            InitializeComponent();
            tmrClear.Interval = ClsCommonSettings.TimerInterval;

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.Deposit, this);

            bnMoreActions.Text = "الإجراءات";
            bnMoreActions.ToolTipText = "الإجراءات";
            btnDocument.Text = "وثائق";
            btnRefund.Text = "رد";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            lblStatusShow.Text = "حالة :";
            strBindingOf = "من ";
        }

        private void FrmDeposit_Load(object sender, EventArgs e)
        {
            SetPermissions();
            AddNew();
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.DepositRefund,
                    out MblnPrintEmailPermission, out MblnRefundPermission, out MblnUpdatePermission, out MblnDeletePermission);

                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.Deposit,
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = MblnRefundPermission = true;
        }

        private void LoadCombos(int intType)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0 || intType == 1)
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    if (mblnAddStatus) // In Add New Mode not showing the Absconding, Expired, Retired, Resigned, Terminated Employees
                    {
                        datTemp = MobjclsBLLDeposit.FillCombos(new string[] { "EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullNameArb + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullNameArb + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") ORDER BY EmployeeName" });
                    }
                    else
                    {
                        datTemp = MobjclsBLLDeposit.FillCombos(new string[] { "EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullNameArb + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullNameArb + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "1=1 ORDER BY EmployeeName" });
                    }
                }
                else
                {
                    if (mblnAddStatus) // In Add New Mode not showing the Absconding, Expired, Retired, Resigned, Terminated Employees
                    {
                        datTemp = MobjclsBLLDeposit.FillCombos(new string[] { "EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']'  ELSE EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID  + ") ORDER BY EmployeeName" });
                    }
                    else
                    {
                        datTemp = MobjclsBLLDeposit.FillCombos(new string[] { "EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']'  ELSE EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "1=1 ORDER BY EmployeeName" });
                    }
                }

                cboEmployee.DataSource = datTemp;
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DisplayMember = "EmployeeName";
            }

            if (intType == 0 || intType == 2)
            {
                if (ClsCommonSettings.IsArabicView)
                    datTemp = MobjclsBLLDeposit.FillCombos(new string[] { "DepositTypeID,DepositTypeArb AS DepositType", "PayDepositTypeReference", "" });
                else
                    datTemp = MobjclsBLLDeposit.FillCombos(new string[] { "DepositTypeID,DepositType", "PayDepositTypeReference", "" });

                cboDepositType.DataSource = datTemp;
                cboDepositType.ValueMember = "DepositTypeID";
                cboDepositType.DisplayMember = "DepositType";
            }
        }

        private void AddNew()
        {
            mblnAddStatus = true;
            cboEmployee.Tag = 0;
            LoadCombos(0);
            cboEmployee.SelectedIndex = -1;
            cboDepositType.SelectedIndex = -1;
            txtAmount.Text = txtRemarks.Text = txtSearch.Text = lblCurrency.Text = lblRefundStatus.Text = lblJoiningDate.Text = "";
            dtpDate.Value = dtpRefundableDate.Value = ClsCommonSettings.GetServerDate();

            if (ClsCommonSettings.IsArabicView)
                lblDocumentAttached.Text = "ليس وثائق تعلق";
            else
                lblDocumentAttached.Text = "No Document(s) Attached";

            chkIsRefundable.Checked = false;
            lblstatus.Text = "";
            tmrClear.Enabled = false;
            errValidate.Clear();
            
            SetAutoCompleteList();
            RecordCount();
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt + 1).ToString();
            BindingNavigatorPositionItem.Text = (TotalRecordCnt + 1).ToString();

            cboEmployee.Enabled = true;
            dtpRefundableDate.Enabled = false;
            lblRefundStatus.Visible = false;

            btnClear.Enabled = true;
            btnAddNew.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
            bnMoreActions.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false;

            cboEmployee.Focus();
        }

        private long getDepositNo()
        {
            Random objRan = new Random();
            return objRan.Next(9999).ToInt64();
        }

        private void RecordCount()
        {
            MobjclsBLLDeposit.PobjclsDTODeposit.strSearchKey = txtSearch.Text.Trim(); // Is any search text filteration will be taken that also
            TotalRecordCnt = MobjclsBLLDeposit.GetRecordCount();
            CurrentRecCnt = TotalRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }

            BindingEnableDisable();
        }

        private void BindingEnableDisable() // Navigator visibility settings
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "", "PayEmployeeDepositDetails");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }

        private void btnDepositType_Click(object sender, EventArgs e)
        {
            // For new deposit type entry
            int intTempID = cboDepositType.SelectedValue.ToInt32(); // For setting selected deposit type after reference form closing
            FrmCommonRef objFrmCommonRef = new FrmCommonRef("Deposit Type", new int[] { 1, 0 }, "DepositTypeID,DepositType,DepositTypeArb", "PayDepositTypeReference", "");
            objFrmCommonRef.ShowDialog();
            LoadCombos(2);

            if (objFrmCommonRef.NewID != 0)
                cboDepositType.SelectedValue = objFrmCommonRef.NewID; // If new Reference is added that will be loaded
            else
                cboDepositType.SelectedValue = intTempID;

            objFrmCommonRef.Dispose();
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            SaveDeposit();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveDeposit())
            {
                btnSave.Enabled = false;
                this.Close();
            }
        }

        private bool SaveDeposit()
        {
            if (FormValidation())
            {
                bool blnSave = false;

                if (mblnAddStatus)
                {
                    if (this.UserMessage.ShowMessage(1) == true) // Save
                        blnSave = true;
                }
                else
                {
                    if (this.UserMessage.ShowMessage(3) == true) // Update
                        blnSave = true;
                }

                if (blnSave)
                {
                    FillParameters(); // fill parameters for saving the content

                    long lngRetValue = MobjclsBLLDeposit.SaveDepositDetails();

                    if (lngRetValue > 0)
                    {
                        if (ClsCommonSettings.ThirdPartyIntegration)
                            new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpDate.Value);

                        if (mblnAddStatus)
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(2);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(2); // Saved Successfully
                        }
                        else
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(21);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(21); // Updated Successfully
                        }

                        DisplayDepositAgainstRecordID(lngRetValue); // to display the current saved or updated content
                        SetAutoCompleteList();
                        return true;
                    }
                }
            }

            return false;
        }

        private bool FormValidation()
        {
            btnDepositType.Focus();
            errValidate.Clear();

            if (cboEmployee.SelectedIndex == -1) // if employee is not selected properly
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(9124);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(9124, cboEmployee, errValidate);
                return false;
            }

            if (cboDepositType.SelectedIndex == -1) // if Deposit type is not selected properly
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(1801);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1801, cboDepositType, errValidate);
                return false;
            }

            if (txtAmount.Text.ToDecimal() == 0) // if Deposit Amount is not enter
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(1802);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1802, txtAmount, errValidate);
                return false;
            }

            if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate()) // if deposit date is greater than current date
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(1803);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1803, dtpDate, errValidate);
                return false;
            }

            if (dtpDate.Value.Date > dtpRefundableDate.Value.Date) // id refundable date is less than deposit date
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(1806);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1806, dtpRefundableDate, errValidate);
                return false;
            }

            if (dtpDate.Value.Date < lblJoiningDate.Text.ToDateTime().Date) // if deposit date is less than Employee Joining Date
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(3081);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(3081, dtpDate, errValidate);
                return false;
            }

            return true;
        }

        private void FillParameters()
        {
            MobjclsBLLDeposit.PobjclsDTODeposit.lngDepositID = cboEmployee.Tag.ToInt64();
            MobjclsBLLDeposit.PobjclsDTODeposit.lngDepositNo = getDepositNo();
            MobjclsBLLDeposit.PobjclsDTODeposit.lngEmployeeID = cboEmployee.SelectedValue.ToInt64();
            MobjclsBLLDeposit.PobjclsDTODeposit.intDepositTypeID = cboDepositType.SelectedValue.ToInt32();
            MobjclsBLLDeposit.PobjclsDTODeposit.decDepositAmount = txtAmount.Text.ToDecimal();

            if (mblnAddStatus)
            {
                DataTable datTemp = MobjclsBLLDeposit.FillCombos(new string[] { "CM.CurrencyId", "EmployeeMaster EM " +
                    "INNER JOIN CompanyMaster CM ON EM.CompanyID = CM.CompanyID", "EM.EmployeeID = " + MobjclsBLLDeposit.PobjclsDTODeposit.lngEmployeeID + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        MobjclsBLLDeposit.PobjclsDTODeposit.intCurrencyID = datTemp.Rows[0]["CurrencyId"].ToInt32();
                }
            }

            MobjclsBLLDeposit.PobjclsDTODeposit.dtDepositDate = (dtpDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            MobjclsBLLDeposit.PobjclsDTODeposit.decDepositAmount = MobjclsBLLDeposit.PobjclsDTODeposit.decCompanyAmount = txtAmount.Text.ToDecimal();

            if (chkIsRefundable.Checked)
            {
                MobjclsBLLDeposit.PobjclsDTODeposit.blnIsRefundable = true;
                MobjclsBLLDeposit.PobjclsDTODeposit.dtRefundableDate = (dtpRefundableDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            }
            else
                MobjclsBLLDeposit.PobjclsDTODeposit.blnIsRefundable = false;

            MobjclsBLLDeposit.PobjclsDTODeposit.blnIsRefunded = false;
            MobjclsBLLDeposit.PobjclsDTODeposit.strRemarks = txtRemarks.Text;
        }

        private void DisplayDepositAgainstRecordID(long lngRetValue)
        {
            RecordCount();
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt).ToString();
            BindingNavigatorPositionItem.Text = MobjclsBLLDeposit.getRecordRowNumber(lngRetValue).ToString(); // get record no. against the Deposit ID

            DisplayDepositDetails();
            BindingEnableDisable();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.UserMessage.ShowMessage(13) == true)
            {
                MobjclsBLLDeposit.PobjclsDTODeposit.lngDepositID = cboEmployee.Tag.ToInt64();
                if (MobjclsBLLDeposit.DeleteDepositDetails() == true)
                {
                    if (ClsCommonSettings.ThirdPartyIntegration)
                        new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpDate.Value);

                    lblstatus.Text = this.UserMessage.GetMessageByCode(4); // Deleted Successfully
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(4);
                    AddNew();
                }
            }
        }

        private void btnRefund_Click(object sender, EventArgs e)
        {
            if (!mblnAddStatus)
            {
                if (this.UserMessage.ShowMessage(1804) == true) // confirmation message for refunded
                {
                    MobjclsBLLDeposit.PobjclsDTODeposit.lngDepositID = cboEmployee.Tag.ToInt64();
                    MobjclsBLLDeposit.PobjclsDTODeposit.blnIsRefunded = true;

                    long lngRetValue = MobjclsBLLDeposit.UpdateDepositRefundDetails();

                    if (lngRetValue > 0)
                    {
                        lblstatus.Text = this.UserMessage.GetMessageByCode(1805);
                        tmrClear.Enabled = true;
                        this.UserMessage.ShowMessage(1805); // Refunded Successfully

                        if (ClsCommonSettings.ThirdPartyIntegration)
                            new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpDate.Value);

                        DisplayDepositAgainstRecordID(lngRetValue);
                    }
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboEmployee.Tag.ToInt64() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = cboEmployee.Tag.ToInt64();

                DataTable datTemp = MobjclsBLLDeposit.FillCombos(new string[] { "TOP 1 CompanyID AS CompanyID", "EmployeeMaster", "" +
                    "EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        ObjViewer.PintCompany = datTemp.Rows[0]["CompanyID"].ToInt32();
                }
                                
                ObjViewer.PiFormID = (int)FormID.Deposit;
                ObjViewer.ShowDialog();
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboEmployee.Tag.ToInt64() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Deposit";
                        ObjEmailPopUp.EmailFormType = EmailFormID.Deposit;
                        ObjEmailPopUp.EmailSource = MobjclsBLLDeposit.GetPrintDepositDetails(cboEmployee.Tag.ToInt64());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch { }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Deposit";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayDepositDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();

            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
                BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayDepositDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();

            if (TotalRecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                else
                    BindingNavigatorPositionItem.Text = "1";
            }

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayDepositDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                BindingNavigatorCountItem.Text = strBindingOf + "1";
            }

            DisplayDepositDetails();
            BindingEnableDisable();
        }

        private void DisplayDepositDetails()
        {
            MobjclsBLLDeposit.PobjclsDTODeposit.strSearchKey = txtSearch.Text.Trim();
            DataTable datTemp = MobjclsBLLDeposit.DisplayDepositDetails(BindingNavigatorPositionItem.Text.ToInt64()); // Get Deposit Details

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    mblnAddStatus = false;
                    cboEmployee.Enabled = btnClear.Enabled = false;                    
                    LoadCombos(1);

                    cboEmployee.Tag = datTemp.Rows[0]["DepositID"].ToInt64();
                    cboEmployee.SelectedValue = datTemp.Rows[0]["EmployeeID"].ToInt64();
                    cboDepositType.SelectedValue = datTemp.Rows[0]["DepositTypeID"].ToInt32();
                    txtAmount.Text = datTemp.Rows[0]["DepositAmount"].ToString();

                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        txtAmount.Text = txtAmount.Text.ToDecimal().ToString("F" + 0);
                    }



                    if (ClsCommonSettings.IsArabicView)
                        lblCurrency.Text = datTemp.Rows[0]["CurrencyNameArb"].ToString();
                    else
                        lblCurrency.Text = datTemp.Rows[0]["CurrencyName"].ToString();

                    dtpDate.Value = datTemp.Rows[0]["DepositDate"].ToDateTime();
                    txtRemarks.Text = datTemp.Rows[0]["Remarks"].ToString();

                    string strTemp = datTemp.Rows[0]["DocumentAttached"].ToString();
                    if (ClsCommonSettings.IsArabicView)
                    {
                        if (strTemp.Contains("No"))
                            strTemp = strTemp.Replace("No", "ليس");

                        strTemp = strTemp.Replace("Document(s) Attached", "وثائق تعلق");
                    }

                    lblDocumentAttached.Text = strTemp;

                    MobjclsBLLDeposit.PobjclsDTODeposit.lngDepositNo = datTemp.Rows[0]["DepositNo"].ToInt64();
                    MobjclsBLLDeposit.PobjclsDTODeposit.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();

                    chkIsRefundable.Checked = datTemp.Rows[0]["IsRefundable"].ToBoolean();

                    if (datTemp.Rows[0]["RefundableDate"].ToStringCustom() == "") // if deposit is refundable then want to show the refundable date
                        dtpRefundableDate.Value = ClsCommonSettings.GetServerDate();
                    else
                        dtpRefundableDate.Value = datTemp.Rows[0]["RefundableDate"].ToDateTime();

                    MobjclsBLLDeposit.PobjclsDTODeposit.blnIsRefunded = datTemp.Rows[0]["IsRefunded"].ToBoolean();

                    if (MobjclsBLLDeposit.PobjclsDTODeposit.blnIsRefunded) // if deposit is refunded want to show the refund status and date
                    {
                        lblRefundStatus.Visible = true;
                        strTemp = datTemp.Rows[0]["RefundStatus"].ToString();

                        if (ClsCommonSettings.IsArabicView)
                        {
                            if (strTemp.Contains("Refunded"))
                                strTemp = strTemp.Replace("Refunded", "ردها");

                            if (strTemp.Contains("Refund Date"))
                                strTemp = strTemp.Replace("Refund Date", "رد التسجيل");
                        }

                        lblRefundStatus.Text = strTemp;
                    }
                    else
                    {
                        lblRefundStatus.Visible = false;
                        lblRefundStatus.Text = "";
                    }

                    if (datTemp.Rows[0]["RefundDate"].ToStringCustom() == "")
                        MobjclsBLLDeposit.PobjclsDTODeposit.dtRefundDate = ClsCommonSettings.GetServerDate();
                    else
                        MobjclsBLLDeposit.PobjclsDTODeposit.dtRefundDate = datTemp.Rows[0]["RefundDate"].ToDateTime();
                    
                    ChangeStatus();
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false; // disable in update mode. Enable these controls when edit started
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox obj = (ComboBox)sender;
            obj.DroppedDown = false;
        }

        private void ChangeControlStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void chkIsRefundable_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus();
            btnRefund.Enabled = false; // if Is Refundable is checked / unchecked in update mode, don't want to enable the refund button

            if (chkIsRefundable.Checked)
                dtpRefundableDate.Enabled = true;
            else
                dtpRefundableDate.Enabled = false;
        }

        private void ChangeStatus()
        {
            if (mblnAddStatus)
            {
                btnAddNew.Enabled = bnMoreActions.Enabled = false;
                btnClear.Enabled = true;
                btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            }
            else
            {
                btnAddNew.Enabled = MblnAddPermission;
                btnClear.Enabled = false;

                // if employee is settled don't want to update / delete the deposit details
                if (MobjclsBLLDeposit.CheckEmployeeExistance(cboEmployee.SelectedValue.ToInt64()) == 1)
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
                else
                {                    
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                    btnDelete.Enabled = MblnDeletePermission;                    
                }
                // END

                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;

                bnMoreActions.Enabled = true;
                btnDocument.Enabled = (MblnAddPermission || MblnUpdatePermission) ? true : false;
                                
                if (chkIsRefundable.Checked == true && MobjclsBLLDeposit.PobjclsDTODeposit.blnIsRefunded == false)
                    btnRefund.Enabled = MblnRefundPermission;
                else
                    btnRefund.Enabled = false;
            }
            errValidate.Clear();
        }

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = "";
            tmrClear.Enabled = false;            
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datTemp = MobjclsBLLDeposit.FillCombos(new string[] { "CONVERT(VARCHAR,EM.DateofJoining,106) AS DateofJoining," +
                        "CR.Code AS CurrencyName,CR.CurrencyNameArb", "EmployeeMaster EM INNER JOIN CompanyMaster CM ON EM.CompanyID = CM.CompanyID " +
                        "INNER JOIN CurrencyReference CR ON CM.CurrencyId = CR.CurrencyID", "" +
                        "EM.EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + "" });

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    lblJoiningDate.Text = datTemp.Rows[0]["DateofJoining"].ToString(); // For Checking Employee Date of Joining

                    if (mblnAddStatus)
                    {
                        if (ClsCommonSettings.IsArabicView)
                            lblCurrency.Text = datTemp.Rows[0]["CurrencyNameArb"].ToStringCustom();
                        else
                            lblCurrency.Text = datTemp.Rows[0]["CurrencyName"].ToStringCustom();
                    }
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.txtSearch.Text.Trim() == string.Empty)
                return;

            RecordCount(); // get record count

            if (TotalRecordCnt == 0) // No records
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(40);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(40);
                txtSearch.Text = string.Empty;
                btnAddNew_Click(sender, e);
            }
            else if (TotalRecordCnt > 0)
                BindingNavigatorMoveFirstItem_Click(sender, e); // if record found in search then automatically navigate to first item
        }

        private void FrmDeposit_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "Deposit";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (MblnAddPermission)
                            btnAddNew_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (MblnDeletePermission)
                            btnDelete_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (MblnPrintEmailPermission)
                            btnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (MblnPrintEmailPermission)
                            btnEmail_Click(sender, new EventArgs());//Email
                        break;
                }
            }
            catch { }
        }

        private void FrmDeposit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {
                if (this.UserMessage.ShowMessage(8) == true) // Your changes will be lost. Please confirm if you wish to cancel?
                {
                    MobjclsBLLDeposit = null;
                    objUserMessage = null;
                    e.Cancel = false;
                }
                else
                    e.Cancel = true;
            }
        }

        private void btnDocument_Click(object sender, EventArgs e)
        {
            using (FrmScanning objScanning = new FrmScanning())
            {
                objScanning.MintDocumentTypeid = (int)DocumentType.Deposit;
                objScanning.MlngReferenceID = cboEmployee.SelectedValue.ToInt64();
                objScanning.MintOperationTypeID = (int)OperationType.Employee;
                objScanning.MstrReferenceNo = dtpDate.Value.ToString("dd MMM yyyy") + " [" + MobjclsBLLDeposit.PobjclsDTODeposit.lngDepositNo.ToString() + "]";
                objScanning.PblnEditable = true;
                objScanning.MintDocumentID = cboEmployee.Tag.ToInt32();
                objScanning.ShowDialog();

                // get attached Document Count
                string strTemp = MobjclsBLLDeposit.getAttchaedDocumentCount(cboEmployee.Tag.ToInt64());
                if (ClsCommonSettings.IsArabicView)
                {
                    if (strTemp.Contains("No"))
                        strTemp = strTemp.Replace("No", "ليس");

                    strTemp = strTemp.Replace("Document(s) Attached", "وثائق تعلق");
                }

                lblDocumentAttached.Text = strTemp;
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;
            e.Handled = false;
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            }
            else
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            }

            if (ClsCommonSettings.IsAmountRoundByZero == false && ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }
    }
}