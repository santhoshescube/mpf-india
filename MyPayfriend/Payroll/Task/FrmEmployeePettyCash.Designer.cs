﻿namespace MyPayfriend
{
    partial class FrmEmployeePettyCash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label DateLabel;
            System.Windows.Forms.Label EmployeeIDLabel;
            System.Windows.Forms.Label AmountLabel;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeePettyCash));
            this.EmployeePettyCashBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAddNew = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDocument = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.txtPettyCashNo = new System.Windows.Forms.TextBox();
            this.lblDocumentAttached = new System.Windows.Forms.Label();
            this.txtEmployeeRemarks = new System.Windows.Forms.TextBox();
            this.txtActualAmount = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.Label2 = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatusShow = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.errPettyCash = new System.Windows.Forms.ErrorProvider(this.components);
            DateLabel = new System.Windows.Forms.Label();
            EmployeeIDLabel = new System.Windows.Forms.Label();
            AmountLabel = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeePettyCashBindingNavigator)).BeginInit();
            this.EmployeePettyCashBindingNavigator.SuspendLayout();
            this.GrpMain.SuspendLayout();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errPettyCash)).BeginInit();
            this.SuspendLayout();
            // 
            // DateLabel
            // 
            DateLabel.AutoSize = true;
            DateLabel.Location = new System.Drawing.Point(9, 47);
            DateLabel.Name = "DateLabel";
            DateLabel.Size = new System.Drawing.Size(30, 13);
            DateLabel.TabIndex = 3;
            DateLabel.Text = "Date";
            // 
            // EmployeeIDLabel
            // 
            EmployeeIDLabel.AutoSize = true;
            EmployeeIDLabel.Location = new System.Drawing.Point(9, 19);
            EmployeeIDLabel.Name = "EmployeeIDLabel";
            EmployeeIDLabel.Size = new System.Drawing.Size(53, 13);
            EmployeeIDLabel.TabIndex = 1;
            EmployeeIDLabel.Text = "Employee";
            // 
            // AmountLabel
            // 
            AmountLabel.AutoSize = true;
            AmountLabel.Location = new System.Drawing.Point(9, 72);
            AmountLabel.Name = "AmountLabel";
            AmountLabel.Size = new System.Drawing.Size(43, 13);
            AmountLabel.TabIndex = 7;
            AmountLabel.Text = "Amount";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(9, 164);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(76, 13);
            label3.TabIndex = 76;
            label3.Text = "Actual Amount";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(9, 190);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(98, 13);
            label4.TabIndex = 77;
            label4.Text = "Employee Remarks";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(9, 98);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(60, 13);
            label1.TabIndex = 79;
            label1.Text = "Description";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(9, 255);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(108, 13);
            label6.TabIndex = 81;
            label6.Text = "Document Attached :";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(251, 47);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(24, 13);
            label5.TabIndex = 83;
            label5.Text = "No.";
            // 
            // EmployeePettyCashBindingNavigator
            // 
            this.EmployeePettyCashBindingNavigator.AddNewItem = null;
            this.EmployeePettyCashBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeePettyCashBindingNavigator.DeleteItem = null;
            this.EmployeePettyCashBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.btnAddNew,
            this.btnSaveItem,
            this.btnDelete,
            this.toolStripSeparator1,
            this.btnDocument,
            this.toolStripSeparator2,
            this.btnPrint,
            this.btnEmail});
            this.EmployeePettyCashBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeePettyCashBindingNavigator.MoveFirstItem = null;
            this.EmployeePettyCashBindingNavigator.MoveLastItem = null;
            this.EmployeePettyCashBindingNavigator.MoveNextItem = null;
            this.EmployeePettyCashBindingNavigator.MovePreviousItem = null;
            this.EmployeePettyCashBindingNavigator.Name = "EmployeePettyCashBindingNavigator";
            this.EmployeePettyCashBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeePettyCashBindingNavigator.Size = new System.Drawing.Size(408, 25);
            this.EmployeePettyCashBindingNavigator.TabIndex = 303;
            this.EmployeePettyCashBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAddNew
            // 
            this.btnAddNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddNew.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNew.Image")));
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.RightToLeftAutoMirrorImage = true;
            this.btnAddNew.Size = new System.Drawing.Size(23, 22);
            this.btnAddNew.Text = "Add new";
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save Data";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDocument
            // 
            this.btnDocument.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDocument.Image = global::MyPayfriend.Properties.Resources.Document_handover;
            this.btnDocument.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDocument.Name = "btnDocument";
            this.btnDocument.Size = new System.Drawing.Size(23, 22);
            this.btnDocument.Text = "Document";
            this.btnDocument.Click += new System.EventHandler(this.btnDocument_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.txtPettyCashNo);
            this.GrpMain.Controls.Add(label5);
            this.GrpMain.Controls.Add(this.lblDocumentAttached);
            this.GrpMain.Controls.Add(label6);
            this.GrpMain.Controls.Add(label1);
            this.GrpMain.Controls.Add(this.txtEmployeeRemarks);
            this.GrpMain.Controls.Add(label4);
            this.GrpMain.Controls.Add(this.txtActualAmount);
            this.GrpMain.Controls.Add(label3);
            this.GrpMain.Controls.Add(this.dtpDate);
            this.GrpMain.Controls.Add(DateLabel);
            this.GrpMain.Controls.Add(this.Label2);
            this.GrpMain.Controls.Add(this.cboEmployee);
            this.GrpMain.Controls.Add(this.txtAmount);
            this.GrpMain.Controls.Add(EmployeeIDLabel);
            this.GrpMain.Controls.Add(AmountLabel);
            this.GrpMain.Controls.Add(this.txtDescription);
            this.GrpMain.Location = new System.Drawing.Point(4, 28);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(400, 279);
            this.GrpMain.TabIndex = 304;
            this.GrpMain.TabStop = false;
            // 
            // txtPettyCashNo
            // 
            this.txtPettyCashNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtPettyCashNo.Location = new System.Drawing.Point(281, 43);
            this.txtPettyCashNo.MaxLength = 10;
            this.txtPettyCashNo.Name = "txtPettyCashNo";
            this.txtPettyCashNo.ReadOnly = true;
            this.txtPettyCashNo.Size = new System.Drawing.Size(110, 20);
            this.txtPettyCashNo.TabIndex = 84;
            // 
            // lblDocumentAttached
            // 
            this.lblDocumentAttached.AutoSize = true;
            this.lblDocumentAttached.Location = new System.Drawing.Point(126, 254);
            this.lblDocumentAttached.Name = "lblDocumentAttached";
            this.lblDocumentAttached.Size = new System.Drawing.Size(21, 13);
            this.lblDocumentAttached.TabIndex = 82;
            this.lblDocumentAttached.Text = "No";
            // 
            // txtEmployeeRemarks
            // 
            this.txtEmployeeRemarks.Location = new System.Drawing.Point(126, 187);
            this.txtEmployeeRemarks.MaxLength = 450;
            this.txtEmployeeRemarks.Multiline = true;
            this.txtEmployeeRemarks.Name = "txtEmployeeRemarks";
            this.txtEmployeeRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtEmployeeRemarks.Size = new System.Drawing.Size(265, 60);
            this.txtEmployeeRemarks.TabIndex = 78;
            this.txtEmployeeRemarks.TextChanged += new System.EventHandler(this.txtEmployeeRemarks_TextChanged);
            // 
            // txtActualAmount
            // 
            this.txtActualAmount.Location = new System.Drawing.Point(126, 161);
            this.txtActualAmount.MaxLength = 10;
            this.txtActualAmount.Name = "txtActualAmount";
            this.txtActualAmount.Size = new System.Drawing.Size(106, 20);
            this.txtActualAmount.TabIndex = 75;
            this.txtActualAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtActualAmount.TextChanged += new System.EventHandler(this.txtActualAmount_TextChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(126, 43);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(106, 20);
            this.dtpDate.TabIndex = 1;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(2, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(94, 13);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Petty Cash Info";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(126, 16);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(265, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboEmployee_KeyPress);
            this.cboEmployee.TextChanged += new System.EventHandler(this.cboEmployee_TextChanged);
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtAmount.Location = new System.Drawing.Point(126, 69);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(106, 20);
            this.txtAmount.TabIndex = 2;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(126, 95);
            this.txtDescription.MaxLength = 450;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDescription.Size = new System.Drawing.Size(265, 60);
            this.txtDescription.TabIndex = 7;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusShow,
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 342);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(408, 22);
            this.ssStatus.TabIndex = 315;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblStatusShow
            // 
            this.lblStatusShow.Name = "lblStatusShow";
            this.lblStatusShow.Size = new System.Drawing.Size(48, 17);
            this.lblStatusShow.Text = "Status : ";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(9, 313);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 312;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(329, 313);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 314;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(248, 313);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 313;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // tmrClear
            // 
            this.tmrClear.Interval = 1000;
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // errPettyCash
            // 
            this.errPettyCash.ContainerControl = this;
            this.errPettyCash.RightToLeft = true;
            // 
            // FrmEmployeePettyCash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 364);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.EmployeePettyCashBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(424, 400);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(424, 400);
            this.Name = "FrmEmployeePettyCash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Petty Cash";
            this.Load += new System.EventHandler(this.FrmEmployeePettyCash_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeePettyCashBindingNavigator)).EndInit();
            this.EmployeePettyCashBindingNavigator.ResumeLayout(false);
            this.EmployeePettyCashBindingNavigator.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errPettyCash)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator EmployeePettyCashBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton btnAddNew;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.TextBox txtActualAmount;
        internal System.Windows.Forms.DateTimePicker dtpDate;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.TextBox txtDescription;
        internal System.Windows.Forms.TextBox txtEmployeeRemarks;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatusShow;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ToolStripButton btnDocument;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label lblDocumentAttached;
        private System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.ErrorProvider errPettyCash;
        internal System.Windows.Forms.TextBox txtPettyCashNo;
    }
}