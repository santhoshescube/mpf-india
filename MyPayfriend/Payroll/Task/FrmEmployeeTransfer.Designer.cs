﻿namespace MyPayfriend
{
    partial class FrmEmployeeTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label TransferDateLabel;
            System.Windows.Forms.Label Label11;
            System.Windows.Forms.Label TransactionTypeIDLabel;
            System.Windows.Forms.Label PolicyIDLabel;
            System.Windows.Forms.Label BankNameIDLabel;
            System.Windows.Forms.Label ComBankAccIdLabel;
            System.Windows.Forms.Label lblEmployee;
            System.Windows.Forms.Label label13;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeeTransfer));
            this.lblEmployeeAcntNoText = new DevComponents.DotNetBar.LabelX();
            this.LabelX9 = new DevComponents.DotNetBar.LabelX();
            this.Label16 = new System.Windows.Forms.Label();
            this.lblEmployeeText = new DevComponents.DotNetBar.LabelX();
            this.WizardEmployeeInfo = new DevComponents.DotNetBar.WizardPage();
            this.lblLocationText = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.cboWorkLocation = new System.Windows.Forms.ComboBox();
            this.LabelX8 = new DevComponents.DotNetBar.LabelX();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.lblEmployeeBankNameText = new DevComponents.DotNetBar.LabelX();
            this.Label14 = new System.Windows.Forms.Label();
            this.lblEmployerBankAntText = new DevComponents.DotNetBar.LabelX();
            this.Label5 = new System.Windows.Forms.Label();
            this.lblEmployerBankNameText = new DevComponents.DotNetBar.LabelX();
            this.lblLeavePolicyText = new DevComponents.DotNetBar.LabelX();
            this.cboLeavePolicy = new System.Windows.Forms.ComboBox();
            this.lblWorkPolicyText = new DevComponents.DotNetBar.LabelX();
            this.lblTransactionTypeText = new DevComponents.DotNetBar.LabelX();
            this.LabelX6 = new DevComponents.DotNetBar.LabelX();
            this.cboTransactionType = new System.Windows.Forms.ComboBox();
            this.LabelX5 = new DevComponents.DotNetBar.LabelX();
            this.cboEmployerBankAct = new System.Windows.Forms.ComboBox();
            this.LabelX4 = new DevComponents.DotNetBar.LabelX();
            this.cboEmployeeBank = new System.Windows.Forms.ComboBox();
            this.LabelX3 = new DevComponents.DotNetBar.LabelX();
            this.cboEmployerBank = new System.Windows.Forms.ComboBox();
            this.LabelX2 = new DevComponents.DotNetBar.LabelX();
            this.cboWorkPolicy = new System.Windows.Forms.ComboBox();
            this.LabelX1 = new DevComponents.DotNetBar.LabelX();
            this.txtEmployeeAntNo = new System.Windows.Forms.TextBox();
            this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.LineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lblTransferTo = new System.Windows.Forms.Label();
            this.lblTransferFrom = new System.Windows.Forms.Label();
            this.cboTransferTo = new System.Windows.Forms.ComboBox();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.lblComment = new System.Windows.Forms.Label();
            this.TmrEmployeeTransfer = new System.Windows.Forms.Timer(this.components);
            this.lblFinishComment = new System.Windows.Forms.Label();
            this.epEmployeeTransfer = new System.Windows.Forms.ErrorProvider(this.components);
            this.Label4 = new System.Windows.Forms.Label();
            this.txtOtherInfo = new System.Windows.Forms.TextBox();
            this.WizardFinishPage = new DevComponents.DotNetBar.WizardPage();
            this.btnSalaryStructure = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblLocationT = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCompanyT = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblWorkPolicyT = new System.Windows.Forms.Label();
            this.lblEmployeeT = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblLeavePolicyT = new System.Windows.Forms.Label();
            this.lblDesignationT = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblDepartmentT = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblEmploymentTypeT = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.lblEmployerBankACT = new System.Windows.Forms.Label();
            this.lblEmployerBankT = new System.Windows.Forms.Label();
            this.lblTransactionTypeT = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblEmployeeBankACT = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.lblEmployeeBankT = new System.Windows.Forms.Label();
            this.lblWait = new System.Windows.Forms.Label();
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.dtpTransferDate = new System.Windows.Forms.DateTimePicker();
            this.cboTransferFrom = new System.Windows.Forms.ComboBox();
            this.Wizard1 = new DevComponents.DotNetBar.Wizard();
            this.WizardWelcomePage = new DevComponents.DotNetBar.WizardPage();
            this.PanelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.ChkTemp = new System.Windows.Forms.CheckBox();
            this.pnlRequest = new DevComponents.DotNetBar.PanelEx();
            this.rdbDirect = new System.Windows.Forms.RadioButton();
            this.rdbFromRequest = new System.Windows.Forms.RadioButton();
            this.rbEmployment = new System.Windows.Forms.RadioButton();
            this.rbDesignation = new System.Windows.Forms.RadioButton();
            this.rbDepartment = new System.Windows.Forms.RadioButton();
            this.rbCompanyBranch = new System.Windows.Forms.RadioButton();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.WizardEmployeeTransferInfo = new DevComponents.DotNetBar.WizardPage();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.tmrProcess = new System.Windows.Forms.Timer(this.components);
            TransferDateLabel = new System.Windows.Forms.Label();
            Label11 = new System.Windows.Forms.Label();
            TransactionTypeIDLabel = new System.Windows.Forms.Label();
            PolicyIDLabel = new System.Windows.Forms.Label();
            BankNameIDLabel = new System.Windows.Forms.Label();
            ComBankAccIdLabel = new System.Windows.Forms.Label();
            lblEmployee = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            this.WizardEmployeeInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epEmployeeTransfer)).BeginInit();
            this.WizardFinishPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Wizard1.SuspendLayout();
            this.WizardWelcomePage.SuspendLayout();
            this.PanelEx1.SuspendLayout();
            this.pnlRequest.SuspendLayout();
            this.WizardEmployeeTransferInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // TransferDateLabel
            // 
            TransferDateLabel.AutoSize = true;
            TransferDateLabel.Location = new System.Drawing.Point(48, 189);
            TransferDateLabel.Name = "TransferDateLabel";
            TransferDateLabel.Size = new System.Drawing.Size(74, 13);
            TransferDateLabel.TabIndex = 74;
            TransferDateLabel.Text = "Transfer Date";
            // 
            // Label11
            // 
            Label11.AutoSize = true;
            Label11.Location = new System.Drawing.Point(242, 205);
            Label11.Name = "Label11";
            Label11.Size = new System.Drawing.Size(69, 13);
            Label11.TabIndex = 919;
            Label11.Text = "Leave Policy ";
            // 
            // TransactionTypeIDLabel
            // 
            TransactionTypeIDLabel.AutoSize = true;
            TransactionTypeIDLabel.Location = new System.Drawing.Point(19, 205);
            TransactionTypeIDLabel.Name = "TransactionTypeIDLabel";
            TransactionTypeIDLabel.Size = new System.Drawing.Size(93, 13);
            TransactionTypeIDLabel.TabIndex = 918;
            TransactionTypeIDLabel.Text = "Transaction Type ";
            // 
            // PolicyIDLabel
            // 
            PolicyIDLabel.AutoSize = true;
            PolicyIDLabel.Location = new System.Drawing.Point(19, 233);
            PolicyIDLabel.Name = "PolicyIDLabel";
            PolicyIDLabel.Size = new System.Drawing.Size(65, 13);
            PolicyIDLabel.TabIndex = 911;
            PolicyIDLabel.Text = "Work Policy ";
            // 
            // BankNameIDLabel
            // 
            BankNameIDLabel.AutoSize = true;
            BankNameIDLabel.Location = new System.Drawing.Point(19, 260);
            BankNameIDLabel.Name = "BankNameIDLabel";
            BankNameIDLabel.Size = new System.Drawing.Size(107, 13);
            BankNameIDLabel.TabIndex = 914;
            BankNameIDLabel.Text = "Employer Bank Name";
            // 
            // ComBankAccIdLabel
            // 
            ComBankAccIdLabel.AutoSize = true;
            ComBankAccIdLabel.Location = new System.Drawing.Point(19, 286);
            ComBankAccIdLabel.Name = "ComBankAccIdLabel";
            ComBankAccIdLabel.Size = new System.Drawing.Size(96, 13);
            ComBankAccIdLabel.TabIndex = 917;
            ComBankAccIdLabel.Text = "Employer Bank A/c";
            // 
            // lblEmployee
            // 
            lblEmployee.AutoSize = true;
            lblEmployee.Location = new System.Drawing.Point(48, 109);
            lblEmployee.Name = "lblEmployee";
            lblEmployee.Size = new System.Drawing.Size(53, 13);
            lblEmployee.TabIndex = 68;
            lblEmployee.Text = "Employee";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(243, 233);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(47, 13);
            label13.TabIndex = 931;
            label13.Text = "Location";
            // 
            // lblEmployeeAcntNoText
            // 
            this.lblEmployeeAcntNoText.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployeeAcntNoText.BackgroundStyle.Class = "";
            this.lblEmployeeAcntNoText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployeeAcntNoText.Location = new System.Drawing.Point(145, 167);
            this.lblEmployeeAcntNoText.Name = "lblEmployeeAcntNoText";
            this.lblEmployeeAcntNoText.Size = new System.Drawing.Size(27, 16);
            this.lblEmployeeAcntNoText.TabIndex = 930;
            this.lblEmployeeAcntNoText.Text = "1234";
            // 
            // LabelX9
            // 
            this.LabelX9.AutoSize = true;
            // 
            // 
            // 
            this.LabelX9.BackgroundStyle.Class = "";
            this.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX9.Location = new System.Drawing.Point(22, 167);
            this.LabelX9.Name = "LabelX9";
            this.LabelX9.Size = new System.Drawing.Size(86, 16);
            this.LabelX9.TabIndex = 929;
            this.LabelX9.Text = "Employee A/c No";
            // 
            // Label16
            // 
            this.Label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label16.BackColor = System.Drawing.Color.Transparent;
            this.Label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label16.Location = new System.Drawing.Point(365, 341);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(120, 13);
            this.Label16.TabIndex = 928;
            this.Label16.Text = "To continue, click Next.";
            // 
            // lblEmployeeText
            // 
            this.lblEmployeeText.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployeeText.BackgroundStyle.Class = "";
            this.lblEmployeeText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployeeText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeText.Location = new System.Drawing.Point(115, 26);
            this.lblEmployeeText.Name = "lblEmployeeText";
            this.lblEmployeeText.Size = new System.Drawing.Size(40, 16);
            this.lblEmployeeText.TabIndex = 927;
            this.lblEmployeeText.Text = "AAAAA";
            // 
            // WizardEmployeeInfo
            // 
            this.WizardEmployeeInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WizardEmployeeInfo.AntiAlias = false;
            this.WizardEmployeeInfo.BackColor = System.Drawing.Color.Transparent;
            this.WizardEmployeeInfo.Controls.Add(this.lblLocationText);
            this.WizardEmployeeInfo.Controls.Add(this.labelX10);
            this.WizardEmployeeInfo.Controls.Add(this.cboWorkLocation);
            this.WizardEmployeeInfo.Controls.Add(label13);
            this.WizardEmployeeInfo.Controls.Add(this.lblEmployeeAcntNoText);
            this.WizardEmployeeInfo.Controls.Add(this.LabelX9);
            this.WizardEmployeeInfo.Controls.Add(this.Label16);
            this.WizardEmployeeInfo.Controls.Add(this.lblEmployeeText);
            this.WizardEmployeeInfo.Controls.Add(this.LabelX8);
            this.WizardEmployeeInfo.Controls.Add(this.Label9);
            this.WizardEmployeeInfo.Controls.Add(this.Label8);
            this.WizardEmployeeInfo.Controls.Add(this.Label6);
            this.WizardEmployeeInfo.Controls.Add(this.lblEmployeeBankNameText);
            this.WizardEmployeeInfo.Controls.Add(this.Label14);
            this.WizardEmployeeInfo.Controls.Add(this.lblEmployerBankAntText);
            this.WizardEmployeeInfo.Controls.Add(this.Label5);
            this.WizardEmployeeInfo.Controls.Add(this.lblEmployerBankNameText);
            this.WizardEmployeeInfo.Controls.Add(Label11);
            this.WizardEmployeeInfo.Controls.Add(this.lblLeavePolicyText);
            this.WizardEmployeeInfo.Controls.Add(this.cboLeavePolicy);
            this.WizardEmployeeInfo.Controls.Add(this.lblWorkPolicyText);
            this.WizardEmployeeInfo.Controls.Add(TransactionTypeIDLabel);
            this.WizardEmployeeInfo.Controls.Add(this.lblTransactionTypeText);
            this.WizardEmployeeInfo.Controls.Add(this.LabelX6);
            this.WizardEmployeeInfo.Controls.Add(this.cboTransactionType);
            this.WizardEmployeeInfo.Controls.Add(this.LabelX5);
            this.WizardEmployeeInfo.Controls.Add(this.cboEmployerBankAct);
            this.WizardEmployeeInfo.Controls.Add(this.LabelX4);
            this.WizardEmployeeInfo.Controls.Add(this.cboEmployeeBank);
            this.WizardEmployeeInfo.Controls.Add(this.LabelX3);
            this.WizardEmployeeInfo.Controls.Add(this.cboEmployerBank);
            this.WizardEmployeeInfo.Controls.Add(this.LabelX2);
            this.WizardEmployeeInfo.Controls.Add(this.cboWorkPolicy);
            this.WizardEmployeeInfo.Controls.Add(this.LabelX1);
            this.WizardEmployeeInfo.Controls.Add(PolicyIDLabel);
            this.WizardEmployeeInfo.Controls.Add(BankNameIDLabel);
            this.WizardEmployeeInfo.Controls.Add(ComBankAccIdLabel);
            this.WizardEmployeeInfo.Controls.Add(this.txtEmployeeAntNo);
            this.WizardEmployeeInfo.Controls.Add(this.ShapeContainer2);
            this.WizardEmployeeInfo.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.WizardEmployeeInfo.InteriorPage = false;
            this.WizardEmployeeInfo.Location = new System.Drawing.Point(0, 0);
            this.WizardEmployeeInfo.Name = "WizardEmployeeInfo";
            this.WizardEmployeeInfo.PageTitle = "Employee Information";
            this.WizardEmployeeInfo.Size = new System.Drawing.Size(513, 357);
            // 
            // 
            // 
            this.WizardEmployeeInfo.Style.Class = "";
            this.WizardEmployeeInfo.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WizardEmployeeInfo.StyleMouseDown.Class = "";
            this.WizardEmployeeInfo.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WizardEmployeeInfo.StyleMouseOver.Class = "";
            this.WizardEmployeeInfo.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WizardEmployeeInfo.TabIndex = 9;
            this.WizardEmployeeInfo.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WizardEmployeeInfo_NextButtonClick);
            this.WizardEmployeeInfo.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WizardEmployeeInfo_AfterPageDisplayed);
            // 
            // lblLocationText
            // 
            this.lblLocationText.AutoSize = true;
            // 
            // 
            // 
            this.lblLocationText.BackgroundStyle.Class = "";
            this.lblLocationText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLocationText.Location = new System.Drawing.Point(315, 83);
            this.lblLocationText.Name = "lblLocationText";
            this.lblLocationText.Size = new System.Drawing.Size(49, 16);
            this.lblLocationText.TabIndex = 934;
            this.lblLocationText.Text = "Location1";
            // 
            // labelX10
            // 
            this.labelX10.AutoSize = true;
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.Class = "";
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Location = new System.Drawing.Point(242, 83);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(43, 16);
            this.labelX10.TabIndex = 933;
            this.labelX10.Text = "Location";
            // 
            // cboWorkLocation
            // 
            this.cboWorkLocation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkLocation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkLocation.BackColor = System.Drawing.SystemColors.Window;
            this.cboWorkLocation.DropDownHeight = 85;
            this.cboWorkLocation.FormattingEnabled = true;
            this.cboWorkLocation.IntegralHeight = false;
            this.cboWorkLocation.Location = new System.Drawing.Point(311, 230);
            this.cboWorkLocation.Name = "cboWorkLocation";
            this.cboWorkLocation.Size = new System.Drawing.Size(166, 21);
            this.cboWorkLocation.TabIndex = 932;
            this.cboWorkLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboWorkLocation_KeyDown);
            // 
            // LabelX8
            // 
            this.LabelX8.AutoSize = true;
            // 
            // 
            // 
            this.LabelX8.BackgroundStyle.Class = "";
            this.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX8.Location = new System.Drawing.Point(22, 26);
            this.LabelX8.Name = "LabelX8";
            this.LabelX8.Size = new System.Drawing.Size(50, 16);
            this.LabelX8.TabIndex = 926;
            this.LabelX8.Text = "Employee";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(2, 2);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(173, 18);
            this.Label9.TabIndex = 925;
            this.Label9.Text = "Employee Information";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(3, 186);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(88, 13);
            this.Label8.TabIndex = 924;
            this.Label8.Text = "Employee Info";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(3, 45);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(76, 13);
            this.Label6.TabIndex = 922;
            this.Label6.Text = "Current Info";
            // 
            // lblEmployeeBankNameText
            // 
            this.lblEmployeeBankNameText.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployeeBankNameText.BackgroundStyle.Class = "";
            this.lblEmployeeBankNameText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployeeBankNameText.Location = new System.Drawing.Point(145, 146);
            this.lblEmployeeBankNameText.Name = "lblEmployeeBankNameText";
            this.lblEmployeeBankNameText.Size = new System.Drawing.Size(19, 16);
            this.lblEmployeeBankNameText.TabIndex = 23;
            this.lblEmployeeBankNameText.Text = "SBI";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(19, 312);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(109, 13);
            this.Label14.TabIndex = 921;
            this.Label14.Text = "Employee Bank Name";
            // 
            // lblEmployerBankAntText
            // 
            this.lblEmployerBankAntText.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployerBankAntText.BackgroundStyle.Class = "";
            this.lblEmployerBankAntText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployerBankAntText.Location = new System.Drawing.Point(145, 125);
            this.lblEmployerBankAntText.Name = "lblEmployerBankAntText";
            this.lblEmployerBankAntText.Size = new System.Drawing.Size(32, 16);
            this.lblEmployerBankAntText.TabIndex = 22;
            this.lblEmployerBankAntText.Text = "ACT 1";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(19, 337);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(118, 13);
            this.Label5.TabIndex = 920;
            this.Label5.Text = "Employee Bank A/c No.";
            // 
            // lblEmployerBankNameText
            // 
            this.lblEmployerBankNameText.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployerBankNameText.BackgroundStyle.Class = "";
            this.lblEmployerBankNameText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployerBankNameText.Location = new System.Drawing.Point(145, 104);
            this.lblEmployerBankNameText.Name = "lblEmployerBankNameText";
            this.lblEmployerBankNameText.Size = new System.Drawing.Size(19, 16);
            this.lblEmployerBankNameText.TabIndex = 21;
            this.lblEmployerBankNameText.Text = "SBI";
            // 
            // lblLeavePolicyText
            // 
            this.lblLeavePolicyText.AutoSize = true;
            // 
            // 
            // 
            this.lblLeavePolicyText.BackgroundStyle.Class = "";
            this.lblLeavePolicyText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblLeavePolicyText.Location = new System.Drawing.Point(315, 62);
            this.lblLeavePolicyText.Name = "lblLeavePolicyText";
            this.lblLeavePolicyText.Size = new System.Drawing.Size(40, 16);
            this.lblLeavePolicyText.TabIndex = 20;
            this.lblLeavePolicyText.Text = "Policy 1";
            // 
            // cboLeavePolicy
            // 
            this.cboLeavePolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLeavePolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLeavePolicy.BackColor = System.Drawing.SystemColors.Window;
            this.cboLeavePolicy.DropDownHeight = 85;
            this.cboLeavePolicy.FormattingEnabled = true;
            this.cboLeavePolicy.IntegralHeight = false;
            this.cboLeavePolicy.Location = new System.Drawing.Point(311, 201);
            this.cboLeavePolicy.Name = "cboLeavePolicy";
            this.cboLeavePolicy.Size = new System.Drawing.Size(166, 21);
            this.cboLeavePolicy.TabIndex = 2;
            this.cboLeavePolicy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboLeavePolicy_KeyDown);
            // 
            // lblWorkPolicyText
            // 
            this.lblWorkPolicyText.AutoSize = true;
            // 
            // 
            // 
            this.lblWorkPolicyText.BackgroundStyle.Class = "";
            this.lblWorkPolicyText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWorkPolicyText.Location = new System.Drawing.Point(145, 83);
            this.lblWorkPolicyText.Name = "lblWorkPolicyText";
            this.lblWorkPolicyText.Size = new System.Drawing.Size(40, 16);
            this.lblWorkPolicyText.TabIndex = 19;
            this.lblWorkPolicyText.Text = "Policy 1";
            // 
            // lblTransactionTypeText
            // 
            this.lblTransactionTypeText.AutoSize = true;
            // 
            // 
            // 
            this.lblTransactionTypeText.BackgroundStyle.Class = "";
            this.lblTransactionTypeText.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTransactionTypeText.Location = new System.Drawing.Point(145, 62);
            this.lblTransactionTypeText.Name = "lblTransactionTypeText";
            this.lblTransactionTypeText.Size = new System.Drawing.Size(26, 16);
            this.lblTransactionTypeText.TabIndex = 18;
            this.lblTransactionTypeText.Text = "Cash";
            // 
            // LabelX6
            // 
            this.LabelX6.AutoSize = true;
            // 
            // 
            // 
            this.LabelX6.BackgroundStyle.Class = "";
            this.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX6.Location = new System.Drawing.Point(22, 146);
            this.LabelX6.Name = "LabelX6";
            this.LabelX6.Size = new System.Drawing.Size(77, 16);
            this.LabelX6.TabIndex = 17;
            this.LabelX6.Text = "Employee Bank";
            // 
            // cboTransactionType
            // 
            this.cboTransactionType.BackColor = System.Drawing.SystemColors.Info;
            this.cboTransactionType.FormattingEnabled = true;
            this.cboTransactionType.Location = new System.Drawing.Point(140, 201);
            this.cboTransactionType.Name = "cboTransactionType";
            this.cboTransactionType.Size = new System.Drawing.Size(89, 21);
            this.cboTransactionType.TabIndex = 1;
            this.cboTransactionType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboTransactionType_KeyDown);
            // 
            // LabelX5
            // 
            this.LabelX5.AutoSize = true;
            // 
            // 
            // 
            this.LabelX5.BackgroundStyle.Class = "";
            this.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX5.Location = new System.Drawing.Point(22, 125);
            this.LabelX5.Name = "LabelX5";
            this.LabelX5.Size = new System.Drawing.Size(67, 16);
            this.LabelX5.TabIndex = 16;
            this.LabelX5.Text = "Employer A/c";
            // 
            // cboEmployerBankAct
            // 
            this.cboEmployerBankAct.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployerBankAct.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployerBankAct.BackColor = System.Drawing.SystemColors.Window;
            this.cboEmployerBankAct.DropDownHeight = 85;
            this.cboEmployerBankAct.FormattingEnabled = true;
            this.cboEmployerBankAct.IntegralHeight = false;
            this.cboEmployerBankAct.Location = new System.Drawing.Point(140, 282);
            this.cboEmployerBankAct.Name = "cboEmployerBankAct";
            this.cboEmployerBankAct.Size = new System.Drawing.Size(338, 21);
            this.cboEmployerBankAct.TabIndex = 5;
            this.cboEmployerBankAct.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployerBankAct_KeyDown);
            // 
            // LabelX4
            // 
            this.LabelX4.AutoSize = true;
            // 
            // 
            // 
            this.LabelX4.BackgroundStyle.Class = "";
            this.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX4.Location = new System.Drawing.Point(22, 104);
            this.LabelX4.Name = "LabelX4";
            this.LabelX4.Size = new System.Drawing.Size(75, 16);
            this.LabelX4.TabIndex = 15;
            this.LabelX4.Text = "Employer Bank";
            // 
            // cboEmployeeBank
            // 
            this.cboEmployeeBank.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployeeBank.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployeeBank.BackColor = System.Drawing.SystemColors.Window;
            this.cboEmployeeBank.DropDownHeight = 85;
            this.cboEmployeeBank.FormattingEnabled = true;
            this.cboEmployeeBank.IntegralHeight = false;
            this.cboEmployeeBank.Location = new System.Drawing.Point(140, 308);
            this.cboEmployeeBank.Name = "cboEmployeeBank";
            this.cboEmployeeBank.Size = new System.Drawing.Size(338, 21);
            this.cboEmployeeBank.TabIndex = 6;
            this.cboEmployeeBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployeeBank_KeyDown);
            // 
            // LabelX3
            // 
            this.LabelX3.AutoSize = true;
            // 
            // 
            // 
            this.LabelX3.BackgroundStyle.Class = "";
            this.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX3.Location = new System.Drawing.Point(242, 62);
            this.LabelX3.Name = "LabelX3";
            this.LabelX3.Size = new System.Drawing.Size(62, 16);
            this.LabelX3.TabIndex = 14;
            this.LabelX3.Text = "Leave Policy";
            // 
            // cboEmployerBank
            // 
            this.cboEmployerBank.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployerBank.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployerBank.BackColor = System.Drawing.SystemColors.Window;
            this.cboEmployerBank.DropDownHeight = 85;
            this.cboEmployerBank.FormattingEnabled = true;
            this.cboEmployerBank.IntegralHeight = false;
            this.cboEmployerBank.Location = new System.Drawing.Point(140, 256);
            this.cboEmployerBank.Name = "cboEmployerBank";
            this.cboEmployerBank.Size = new System.Drawing.Size(337, 21);
            this.cboEmployerBank.TabIndex = 4;
            this.cboEmployerBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployerBank_KeyDown);
            // 
            // LabelX2
            // 
            this.LabelX2.AutoSize = true;
            // 
            // 
            // 
            this.LabelX2.BackgroundStyle.Class = "";
            this.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX2.Location = new System.Drawing.Point(22, 83);
            this.LabelX2.Name = "LabelX2";
            this.LabelX2.Size = new System.Drawing.Size(59, 16);
            this.LabelX2.TabIndex = 13;
            this.LabelX2.Text = "Work Policy";
            // 
            // cboWorkPolicy
            // 
            this.cboWorkPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkPolicy.BackColor = System.Drawing.SystemColors.Info;
            this.cboWorkPolicy.DropDownHeight = 85;
            this.cboWorkPolicy.FormattingEnabled = true;
            this.cboWorkPolicy.IntegralHeight = false;
            this.cboWorkPolicy.Location = new System.Drawing.Point(140, 229);
            this.cboWorkPolicy.Name = "cboWorkPolicy";
            this.cboWorkPolicy.Size = new System.Drawing.Size(89, 21);
            this.cboWorkPolicy.TabIndex = 3;
            this.cboWorkPolicy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboWorkPolicy_KeyDown);
            // 
            // LabelX1
            // 
            this.LabelX1.AutoSize = true;
            // 
            // 
            // 
            this.LabelX1.BackgroundStyle.Class = "";
            this.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX1.Location = new System.Drawing.Point(22, 62);
            this.LabelX1.Name = "LabelX1";
            this.LabelX1.Size = new System.Drawing.Size(86, 16);
            this.LabelX1.TabIndex = 12;
            this.LabelX1.Text = "Transaction Type";
            // 
            // txtEmployeeAntNo
            // 
            this.txtEmployeeAntNo.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmployeeAntNo.Location = new System.Drawing.Point(140, 333);
            this.txtEmployeeAntNo.MaxLength = 40;
            this.txtEmployeeAntNo.Name = "txtEmployeeAntNo";
            this.txtEmployeeAntNo.Size = new System.Drawing.Size(172, 21);
            this.txtEmployeeAntNo.TabIndex = 7;
            // 
            // ShapeContainer2
            // 
            this.ShapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer2.Name = "ShapeContainer2";
            this.ShapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape3,
            this.LineShape2});
            this.ShapeContainer2.Size = new System.Drawing.Size(513, 357);
            this.ShapeContainer2.TabIndex = 923;
            this.ShapeContainer2.TabStop = false;
            // 
            // LineShape3
            // 
            this.LineShape3.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape3.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape3.Name = "LineShape3";
            this.LineShape3.X1 = 70;
            this.LineShape3.X2 = 489;
            this.LineShape3.Y1 = 194;
            this.LineShape3.Y2 = 194;
            // 
            // LineShape2
            // 
            this.LineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape2.Name = "LineShape2";
            this.LineShape2.X1 = 72;
            this.LineShape2.X2 = 491;
            this.LineShape2.Y1 = 53;
            this.LineShape2.Y2 = 53;
            // 
            // lblTransferTo
            // 
            this.lblTransferTo.AutoSize = true;
            this.lblTransferTo.Location = new System.Drawing.Point(48, 164);
            this.lblTransferTo.Name = "lblTransferTo";
            this.lblTransferTo.Size = new System.Drawing.Size(63, 13);
            this.lblTransferTo.TabIndex = 73;
            this.lblTransferTo.Text = "Transfer To";
            // 
            // lblTransferFrom
            // 
            this.lblTransferFrom.AutoSize = true;
            this.lblTransferFrom.Location = new System.Drawing.Point(48, 136);
            this.lblTransferFrom.Name = "lblTransferFrom";
            this.lblTransferFrom.Size = new System.Drawing.Size(75, 13);
            this.lblTransferFrom.TabIndex = 72;
            this.lblTransferFrom.Text = "Transfer From";
            // 
            // cboTransferTo
            // 
            this.cboTransferTo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTransferTo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTransferTo.BackColor = System.Drawing.SystemColors.Info;
            this.cboTransferTo.DropDownHeight = 100;
            this.cboTransferTo.FormattingEnabled = true;
            this.cboTransferTo.IntegralHeight = false;
            this.cboTransferTo.Location = new System.Drawing.Point(155, 159);
            this.cboTransferTo.Name = "cboTransferTo";
            this.cboTransferTo.Size = new System.Drawing.Size(278, 21);
            this.cboTransferTo.TabIndex = 3;
            this.cboTransferTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboTransferTo_KeyDown);
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 100;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(155, 106);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(278, 21);
            this.cboEmployee.TabIndex = 1;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployee_KeyDown);
            // 
            // lblComment
            // 
            this.lblComment.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblComment.Location = new System.Drawing.Point(6, 219);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(468, 20);
            this.lblComment.TabIndex = 80;
            this.lblComment.Text = "Employee will be transfered from ";
            // 
            // lblFinishComment
            // 
            this.lblFinishComment.AutoSize = true;
            this.lblFinishComment.Location = new System.Drawing.Point(360, 257);
            this.lblFinishComment.Name = "lblFinishComment";
            this.lblFinishComment.Size = new System.Drawing.Size(118, 13);
            this.lblFinishComment.TabIndex = 79;
            this.lblFinishComment.Text = "To continue,  click Next";
            this.lblFinishComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // epEmployeeTransfer
            // 
            this.epEmployeeTransfer.ContainerControl = this;
            this.epEmployeeTransfer.RightToLeft = true;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.Label4.Location = new System.Drawing.Point(6, 182);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(134, 13);
            this.Label4.TabIndex = 78;
            this.Label4.Text = "Other Related Information";
            // 
            // txtOtherInfo
            // 
            this.txtOtherInfo.Location = new System.Drawing.Point(146, 182);
            this.txtOtherInfo.MaxLength = 200;
            this.txtOtherInfo.Multiline = true;
            this.txtOtherInfo.Name = "txtOtherInfo";
            this.txtOtherInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtOtherInfo.Size = new System.Drawing.Size(353, 31);
            this.txtOtherInfo.TabIndex = 77;
            // 
            // WizardFinishPage
            // 
            this.WizardFinishPage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WizardFinishPage.AntiAlias = false;
            this.WizardFinishPage.BackColor = System.Drawing.Color.Transparent;
            this.WizardFinishPage.Controls.Add(this.btnSalaryStructure);
            this.WizardFinishPage.Controls.Add(this.groupBox2);
            this.WizardFinishPage.Controls.Add(this.groupBox1);
            this.WizardFinishPage.Controls.Add(this.lblComment);
            this.WizardFinishPage.Controls.Add(this.lblFinishComment);
            this.WizardFinishPage.Controls.Add(this.Label4);
            this.WizardFinishPage.Controls.Add(this.txtOtherInfo);
            this.WizardFinishPage.Controls.Add(this.lblWait);
            this.WizardFinishPage.Controls.Add(this.ProgressBar1);
            this.WizardFinishPage.FinishButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.False;
            this.WizardFinishPage.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.WizardFinishPage.Location = new System.Drawing.Point(7, 72);
            this.WizardFinishPage.Name = "WizardFinishPage";
            this.WizardFinishPage.NextButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.True;
            this.WizardFinishPage.NextButtonVisible = DevComponents.DotNetBar.eWizardButtonState.True;
            this.WizardFinishPage.Size = new System.Drawing.Size(499, 273);
            // 
            // 
            // 
            this.WizardFinishPage.Style.Class = "";
            this.WizardFinishPage.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WizardFinishPage.StyleMouseDown.Class = "";
            this.WizardFinishPage.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WizardFinishPage.StyleMouseOver.Class = "";
            this.WizardFinishPage.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WizardFinishPage.TabIndex = 10;
            this.WizardFinishPage.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WizardFinishPage_NextButtonClick);
            this.WizardFinishPage.FinishButtonClick += new System.ComponentModel.CancelEventHandler(this.WizardFinishPage_FinishButtonClick);
            this.WizardFinishPage.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WizardFinishPage_AfterPageDisplayed);
            // 
            // btnSalaryStructure
            // 
            this.btnSalaryStructure.BackColor = System.Drawing.Color.Transparent;
            this.btnSalaryStructure.Location = new System.Drawing.Point(9, 248);
            this.btnSalaryStructure.Name = "btnSalaryStructure";
            this.btnSalaryStructure.Size = new System.Drawing.Size(130, 23);
            this.btnSalaryStructure.TabIndex = 3;
            this.btnSalaryStructure.Text = "Edit Salary Structure";
            this.btnSalaryStructure.UseVisualStyleBackColor = false;
            this.btnSalaryStructure.Click += new System.EventHandler(this.btnSalaryStructure_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblLocationT);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.lblCompanyT);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.lblWorkPolicyT);
            this.groupBox2.Controls.Add(this.lblEmployeeT);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.lblLeavePolicyT);
            this.groupBox2.Controls.Add(this.lblDesignationT);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.lblDepartmentT);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.lblEmploymentTypeT);
            this.groupBox2.Location = new System.Drawing.Point(0, -3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(499, 100);
            this.groupBox2.TabIndex = 107;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Employee Summary";
            // 
            // lblLocationT
            // 
            this.lblLocationT.AutoSize = true;
            this.lblLocationT.Location = new System.Drawing.Point(360, 81);
            this.lblLocationT.Name = "lblLocationT";
            this.lblLocationT.Size = new System.Drawing.Size(41, 13);
            this.lblLocationT.TabIndex = 98;
            this.lblLocationT.Text = "label29";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(260, 81);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 13);
            this.label17.TabIndex = 97;
            this.label17.Text = "Location                :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 81;
            this.label7.Text = "Company      :";
            // 
            // lblCompanyT
            // 
            this.lblCompanyT.AutoSize = true;
            this.lblCompanyT.Location = new System.Drawing.Point(98, 15);
            this.lblCompanyT.Name = "lblCompanyT";
            this.lblCompanyT.Size = new System.Drawing.Size(41, 13);
            this.lblCompanyT.TabIndex = 82;
            this.lblCompanyT.Text = "label13";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 36);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 13);
            this.label18.TabIndex = 83;
            this.label18.Text = "Employee      :";
            // 
            // lblWorkPolicyT
            // 
            this.lblWorkPolicyT.AutoSize = true;
            this.lblWorkPolicyT.Location = new System.Drawing.Point(358, 60);
            this.lblWorkPolicyT.Name = "lblWorkPolicyT";
            this.lblWorkPolicyT.Size = new System.Drawing.Size(41, 13);
            this.lblWorkPolicyT.TabIndex = 96;
            this.lblWorkPolicyT.Text = "label29";
            // 
            // lblEmployeeT
            // 
            this.lblEmployeeT.AutoSize = true;
            this.lblEmployeeT.Location = new System.Drawing.Point(98, 36);
            this.lblEmployeeT.Name = "lblEmployeeT";
            this.lblEmployeeT.Size = new System.Drawing.Size(41, 13);
            this.lblEmployeeT.TabIndex = 84;
            this.lblEmployeeT.Text = "label17";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(260, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(108, 13);
            this.label30.TabIndex = 95;
            this.label30.Text = "Work Policy           :   ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 59);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 13);
            this.label20.TabIndex = 85;
            this.label20.Text = "Designation   :";
            // 
            // lblLeavePolicyT
            // 
            this.lblLeavePolicyT.AutoSize = true;
            this.lblLeavePolicyT.Location = new System.Drawing.Point(358, 36);
            this.lblLeavePolicyT.Name = "lblLeavePolicyT";
            this.lblLeavePolicyT.Size = new System.Drawing.Size(41, 13);
            this.lblLeavePolicyT.TabIndex = 94;
            this.lblLeavePolicyT.Text = "label27";
            // 
            // lblDesignationT
            // 
            this.lblDesignationT.AutoSize = true;
            this.lblDesignationT.Location = new System.Drawing.Point(98, 59);
            this.lblDesignationT.Name = "lblDesignationT";
            this.lblDesignationT.Size = new System.Drawing.Size(41, 13);
            this.lblDesignationT.TabIndex = 86;
            this.lblDesignationT.Text = "label19";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(260, 36);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 13);
            this.label28.TabIndex = 93;
            this.label28.Text = "Leave Policy          :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 81);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 13);
            this.label22.TabIndex = 87;
            this.label22.Text = "Department   :";
            // 
            // lblDepartmentT
            // 
            this.lblDepartmentT.AutoSize = true;
            this.lblDepartmentT.Location = new System.Drawing.Point(98, 81);
            this.lblDepartmentT.Name = "lblDepartmentT";
            this.lblDepartmentT.Size = new System.Drawing.Size(41, 13);
            this.lblDepartmentT.TabIndex = 88;
            this.lblDepartmentT.Text = "label21";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(261, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(99, 13);
            this.label24.TabIndex = 89;
            this.label24.Text = "Employment Type :";
            // 
            // lblEmploymentTypeT
            // 
            this.lblEmploymentTypeT.AutoSize = true;
            this.lblEmploymentTypeT.Location = new System.Drawing.Point(359, 15);
            this.lblEmploymentTypeT.Name = "lblEmploymentTypeT";
            this.lblEmploymentTypeT.Size = new System.Drawing.Size(41, 13);
            this.lblEmploymentTypeT.TabIndex = 90;
            this.lblEmploymentTypeT.Text = "label23";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.lblEmployerBankACT);
            this.groupBox1.Controls.Add(this.lblEmployerBankT);
            this.groupBox1.Controls.Add(this.lblTransactionTypeT);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.lblEmployeeBankACT);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.lblEmployeeBankT);
            this.groupBox1.Location = new System.Drawing.Point(0, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(499, 81);
            this.groupBox1.TabIndex = 106;
            this.groupBox1.TabStop = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 36);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(111, 13);
            this.label32.TabIndex = 98;
            this.label32.Text = "Employer Bank Name:";
            // 
            // lblEmployerBankACT
            // 
            this.lblEmployerBankACT.AutoSize = true;
            this.lblEmployerBankACT.Location = new System.Drawing.Point(376, 36);
            this.lblEmployerBankACT.Name = "lblEmployerBankACT";
            this.lblEmployerBankACT.Size = new System.Drawing.Size(41, 13);
            this.lblEmployerBankACT.TabIndex = 105;
            this.lblEmployerBankACT.Text = "label38";
            // 
            // lblEmployerBankT
            // 
            this.lblEmployerBankT.AutoSize = true;
            this.lblEmployerBankT.Location = new System.Drawing.Point(118, 36);
            this.lblEmployerBankT.Name = "lblEmployerBankT";
            this.lblEmployerBankT.Size = new System.Drawing.Size(41, 13);
            this.lblEmployerBankT.TabIndex = 99;
            this.lblEmployerBankT.Text = "label33";
            // 
            // lblTransactionTypeT
            // 
            this.lblTransactionTypeT.AutoSize = true;
            this.lblTransactionTypeT.Location = new System.Drawing.Point(118, 16);
            this.lblTransactionTypeT.Name = "lblTransactionTypeT";
            this.lblTransactionTypeT.Size = new System.Drawing.Size(41, 13);
            this.lblTransactionTypeT.TabIndex = 92;
            this.lblTransactionTypeT.Text = "label25";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(272, 36);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(102, 13);
            this.label39.TabIndex = 104;
            this.label39.Text = "Employer Bank A/C:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(112, 13);
            this.label26.TabIndex = 91;
            this.label26.Text = "Transaction Type      :";
            // 
            // lblEmployeeBankACT
            // 
            this.lblEmployeeBankACT.AutoSize = true;
            this.lblEmployeeBankACT.Location = new System.Drawing.Point(376, 60);
            this.lblEmployeeBankACT.Name = "lblEmployeeBankACT";
            this.lblEmployeeBankACT.Size = new System.Drawing.Size(41, 13);
            this.lblEmployeeBankACT.TabIndex = 103;
            this.lblEmployeeBankACT.Text = "label36";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 58);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(113, 13);
            this.label35.TabIndex = 100;
            this.label35.Text = "Employee Bank Name:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(272, 60);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(101, 13);
            this.label37.TabIndex = 102;
            this.label37.Text = "Employe Bank A/C :";
            // 
            // lblEmployeeBankT
            // 
            this.lblEmployeeBankT.AutoSize = true;
            this.lblEmployeeBankT.Location = new System.Drawing.Point(118, 58);
            this.lblEmployeeBankT.Name = "lblEmployeeBankT";
            this.lblEmployeeBankT.Size = new System.Drawing.Size(41, 13);
            this.lblEmployeeBankT.TabIndex = 101;
            this.lblEmployeeBankT.Text = "label34";
            // 
            // lblWait
            // 
            this.lblWait.Location = new System.Drawing.Point(60, 255);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(381, 23);
            this.lblWait.TabIndex = 5;
            this.lblWait.Text = "Please wait...";
            this.lblWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblWait.Visible = false;
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(49, 235);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(392, 10);
            this.ProgressBar1.TabIndex = 4;
            this.ProgressBar1.Visible = false;
            // 
            // dtpTransferDate
            // 
            this.dtpTransferDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTransferDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransferDate.Location = new System.Drawing.Point(154, 186);
            this.dtpTransferDate.Name = "dtpTransferDate";
            this.dtpTransferDate.Size = new System.Drawing.Size(102, 21);
            this.dtpTransferDate.TabIndex = 4;
            // 
            // cboTransferFrom
            // 
            this.cboTransferFrom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTransferFrom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTransferFrom.BackColor = System.Drawing.SystemColors.Info;
            this.cboTransferFrom.DropDownHeight = 100;
            this.cboTransferFrom.Enabled = false;
            this.cboTransferFrom.FormattingEnabled = true;
            this.cboTransferFrom.IntegralHeight = false;
            this.cboTransferFrom.Location = new System.Drawing.Point(154, 133);
            this.cboTransferFrom.Name = "cboTransferFrom";
            this.cboTransferFrom.Size = new System.Drawing.Size(279, 21);
            this.cboTransferFrom.TabIndex = 2;
            // 
            // Wizard1
            // 
            this.Wizard1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(229)))), ((int)(((byte)(253)))));
            this.Wizard1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Wizard1.BackgroundImage")));
            this.Wizard1.ButtonStyle = DevComponents.DotNetBar.eWizardStyle.Office2007;
            this.Wizard1.CancelButtonText = "Cancel";
            this.Wizard1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Wizard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Wizard1.FinishButtonTabIndex = 3;
            this.Wizard1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // 
            // 
            this.Wizard1.FooterStyle.BackColor = System.Drawing.Color.Transparent;
            this.Wizard1.FooterStyle.Class = "";
            this.Wizard1.FooterStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Wizard1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            this.Wizard1.HeaderCaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Wizard1.HeaderDescriptionIndent = 16;
            // 
            // 
            // 
            this.Wizard1.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(215)))), ((int)(((byte)(243)))));
            this.Wizard1.HeaderStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(241)))), ((int)(((byte)(254)))));
            this.Wizard1.HeaderStyle.BackColorGradientAngle = 90;
            this.Wizard1.HeaderStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.Wizard1.HeaderStyle.BorderBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(157)))), ((int)(((byte)(182)))));
            this.Wizard1.HeaderStyle.BorderBottomWidth = 1;
            this.Wizard1.HeaderStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.Wizard1.HeaderStyle.BorderLeftWidth = 1;
            this.Wizard1.HeaderStyle.BorderRightWidth = 1;
            this.Wizard1.HeaderStyle.BorderTopWidth = 1;
            this.Wizard1.HeaderStyle.Class = "";
            this.Wizard1.HeaderStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Wizard1.HeaderStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.Wizard1.HeaderStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.Wizard1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.Wizard1.Location = new System.Drawing.Point(0, 0);
            this.Wizard1.Name = "Wizard1";
            this.Wizard1.NextButtonTabIndex = 6;
            this.Wizard1.Size = new System.Drawing.Size(513, 403);
            this.Wizard1.TabIndex = 2;
            this.Wizard1.WizardPages.AddRange(new DevComponents.DotNetBar.WizardPage[] {
            this.WizardWelcomePage,
            this.WizardEmployeeTransferInfo,
            this.WizardEmployeeInfo,
            this.WizardFinishPage});
            this.Wizard1.HelpButtonClick += new System.ComponentModel.CancelEventHandler(this.Wizard1_HelpButtonClick);
            // 
            // WizardWelcomePage
            // 
            this.WizardWelcomePage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WizardWelcomePage.AntiAlias = false;
            this.WizardWelcomePage.BackColor = System.Drawing.Color.Transparent;
            this.WizardWelcomePage.Controls.Add(this.PanelEx1);
            this.WizardWelcomePage.Controls.Add(this.Label10);
            this.WizardWelcomePage.Controls.Add(this.Label1);
            this.WizardWelcomePage.Controls.Add(this.Label2);
            this.WizardWelcomePage.Controls.Add(this.Label3);
            this.WizardWelcomePage.InteriorPage = false;
            this.WizardWelcomePage.Location = new System.Drawing.Point(0, 0);
            this.WizardWelcomePage.Name = "WizardWelcomePage";
            this.WizardWelcomePage.Size = new System.Drawing.Size(513, 357);
            // 
            // 
            // 
            this.WizardWelcomePage.Style.Class = "";
            this.WizardWelcomePage.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WizardWelcomePage.StyleMouseDown.Class = "";
            this.WizardWelcomePage.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WizardWelcomePage.StyleMouseOver.Class = "";
            this.WizardWelcomePage.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WizardWelcomePage.TabIndex = 7;
            this.WizardWelcomePage.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WizardWelcomePage_NextButtonClick);
            // 
            // PanelEx1
            // 
            this.PanelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.PanelEx1.Controls.Add(this.ChkTemp);
            this.PanelEx1.Controls.Add(this.pnlRequest);
            this.PanelEx1.Controls.Add(this.rbEmployment);
            this.PanelEx1.Controls.Add(this.rbDesignation);
            this.PanelEx1.Controls.Add(this.rbDepartment);
            this.PanelEx1.Controls.Add(this.rbCompanyBranch);
            this.PanelEx1.Location = new System.Drawing.Point(75, 202);
            this.PanelEx1.Name = "PanelEx1";
            this.PanelEx1.Size = new System.Drawing.Size(349, 122);
            this.PanelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelEx1.Style.GradientAngle = 90;
            this.PanelEx1.TabIndex = 9;
            // 
            // ChkTemp
            // 
            this.ChkTemp.AutoSize = true;
            this.ChkTemp.Location = new System.Drawing.Point(258, 15);
            this.ChkTemp.Name = "ChkTemp";
            this.ChkTemp.Size = new System.Drawing.Size(78, 17);
            this.ChkTemp.TabIndex = 12;
            this.ChkTemp.Text = "Temporary";
            this.ChkTemp.UseVisualStyleBackColor = true;
            this.ChkTemp.CheckedChanged += new System.EventHandler(this.ChkTemp_CheckedChanged);
            // 
            // pnlRequest
            // 
            this.pnlRequest.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlRequest.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlRequest.Controls.Add(this.rdbDirect);
            this.pnlRequest.Controls.Add(this.rdbFromRequest);
            this.pnlRequest.Location = new System.Drawing.Point(11, 28);
            this.pnlRequest.Name = "pnlRequest";
            this.pnlRequest.Size = new System.Drawing.Size(111, 69);
            this.pnlRequest.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlRequest.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlRequest.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlRequest.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlRequest.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlRequest.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlRequest.Style.GradientAngle = 90;
            this.pnlRequest.TabIndex = 11;
            // 
            // rdbDirect
            // 
            this.rdbDirect.AutoSize = true;
            this.rdbDirect.Location = new System.Drawing.Point(13, 41);
            this.rdbDirect.Name = "rdbDirect";
            this.rdbDirect.Size = new System.Drawing.Size(53, 17);
            this.rdbDirect.TabIndex = 1;
            this.rdbDirect.Text = "Direct";
            this.rdbDirect.UseVisualStyleBackColor = true;
            this.rdbDirect.CheckedChanged += new System.EventHandler(this.rdbDirect_CheckedChanged);
            // 
            // rdbFromRequest
            // 
            this.rdbFromRequest.AutoSize = true;
            this.rdbFromRequest.Checked = true;
            this.rdbFromRequest.Location = new System.Drawing.Point(13, 14);
            this.rdbFromRequest.Name = "rdbFromRequest";
            this.rdbFromRequest.Size = new System.Drawing.Size(92, 17);
            this.rdbFromRequest.TabIndex = 0;
            this.rdbFromRequest.TabStop = true;
            this.rdbFromRequest.Text = "From Request";
            this.rdbFromRequest.UseVisualStyleBackColor = true;
            this.rdbFromRequest.CheckedChanged += new System.EventHandler(this.rdbFromRequest_CheckedChanged);
            // 
            // rbEmployment
            // 
            this.rbEmployment.AutoSize = true;
            this.rbEmployment.Location = new System.Drawing.Point(128, 92);
            this.rbEmployment.Name = "rbEmployment";
            this.rbEmployment.Size = new System.Drawing.Size(83, 17);
            this.rbEmployment.TabIndex = 3;
            this.rbEmployment.Text = "Employment";
            this.rbEmployment.UseVisualStyleBackColor = true;
            this.rbEmployment.CheckedChanged += new System.EventHandler(this.rbEmployment_CheckedChanged);
            // 
            // rbDesignation
            // 
            this.rbDesignation.AutoSize = true;
            this.rbDesignation.Location = new System.Drawing.Point(128, 67);
            this.rbDesignation.Name = "rbDesignation";
            this.rbDesignation.Size = new System.Drawing.Size(81, 17);
            this.rbDesignation.TabIndex = 2;
            this.rbDesignation.Text = "Designation";
            this.rbDesignation.UseVisualStyleBackColor = true;
            this.rbDesignation.CheckedChanged += new System.EventHandler(this.rbDesignation_CheckedChanged);
            // 
            // rbDepartment
            // 
            this.rbDepartment.AutoSize = true;
            this.rbDepartment.Location = new System.Drawing.Point(128, 42);
            this.rbDepartment.Name = "rbDepartment";
            this.rbDepartment.Size = new System.Drawing.Size(82, 17);
            this.rbDepartment.TabIndex = 1;
            this.rbDepartment.Text = "Department";
            this.rbDepartment.UseVisualStyleBackColor = true;
            this.rbDepartment.CheckedChanged += new System.EventHandler(this.rbDepartment_CheckedChanged);
            // 
            // rbCompanyBranch
            // 
            this.rbCompanyBranch.AutoSize = true;
            this.rbCompanyBranch.Checked = true;
            this.rbCompanyBranch.Location = new System.Drawing.Point(128, 15);
            this.rbCompanyBranch.Name = "rbCompanyBranch";
            this.rbCompanyBranch.Size = new System.Drawing.Size(113, 17);
            this.rbCompanyBranch.TabIndex = 0;
            this.rbCompanyBranch.TabStop = true;
            this.rbCompanyBranch.Text = "Company / Branch";
            this.rbCompanyBranch.UseVisualStyleBackColor = true;
            this.rbCompanyBranch.CheckedChanged += new System.EventHandler(this.rbCompanyBranch_CheckedChanged);
            // 
            // Label10
            // 
            this.Label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Label10.BackColor = System.Drawing.Color.Transparent;
            this.Label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.Location = new System.Drawing.Point(72, 178);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(288, 21);
            this.Label10.TabIndex = 10;
            this.Label10.Text = "Please select transfer type";
            // 
            // Label1
            // 
            this.Label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 16F);
            this.Label1.Location = new System.Drawing.Point(162, 18);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(337, 66);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Welcome to Employee Transfer Wizard";
            // 
            // Label2
            // 
            this.Label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(210, 72);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(288, 44);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "\r\nThis wizard will guide you through certain steps for \r\ntransfering the employee" +
                "\r\n\r\n\r\n";
            // 
            // Label3
            // 
            this.Label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(250, 334);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(120, 13);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "To continue, click Next.";
            // 
            // WizardEmployeeTransferInfo
            // 
            this.WizardEmployeeTransferInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WizardEmployeeTransferInfo.AntiAlias = false;
            this.WizardEmployeeTransferInfo.BackColor = System.Drawing.Color.Transparent;
            this.WizardEmployeeTransferInfo.Controls.Add(this.Label15);
            this.WizardEmployeeTransferInfo.Controls.Add(this.Label12);
            this.WizardEmployeeTransferInfo.Controls.Add(this.cboTransferFrom);
            this.WizardEmployeeTransferInfo.Controls.Add(TransferDateLabel);
            this.WizardEmployeeTransferInfo.Controls.Add(this.dtpTransferDate);
            this.WizardEmployeeTransferInfo.Controls.Add(this.lblTransferTo);
            this.WizardEmployeeTransferInfo.Controls.Add(this.lblTransferFrom);
            this.WizardEmployeeTransferInfo.Controls.Add(this.cboTransferTo);
            this.WizardEmployeeTransferInfo.Controls.Add(this.cboEmployee);
            this.WizardEmployeeTransferInfo.Controls.Add(lblEmployee);
            this.WizardEmployeeTransferInfo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.WizardEmployeeTransferInfo.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.WizardEmployeeTransferInfo.Location = new System.Drawing.Point(7, 72);
            this.WizardEmployeeTransferInfo.Name = "WizardEmployeeTransferInfo";
            this.WizardEmployeeTransferInfo.PageTitle = "Specify Employee Transfer Info";
            this.WizardEmployeeTransferInfo.Size = new System.Drawing.Size(499, 273);
            // 
            // 
            // 
            this.WizardEmployeeTransferInfo.Style.Class = "";
            this.WizardEmployeeTransferInfo.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WizardEmployeeTransferInfo.Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // 
            // 
            this.WizardEmployeeTransferInfo.StyleMouseDown.Class = "";
            this.WizardEmployeeTransferInfo.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.WizardEmployeeTransferInfo.StyleMouseOver.Class = "";
            this.WizardEmployeeTransferInfo.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.WizardEmployeeTransferInfo.TabIndex = 8;
            this.WizardEmployeeTransferInfo.NextButtonClick += new System.ComponentModel.CancelEventHandler(this.WizardEmployeeTransferInfo_NextButtonClick);
            this.WizardEmployeeTransferInfo.AfterPageDisplayed += new DevComponents.DotNetBar.WizardPageChangeEventHandler(this.WizardEmployeeTransferInfo_AfterPageDisplayed);
            // 
            // Label15
            // 
            this.Label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label15.BackColor = System.Drawing.Color.Transparent;
            this.Label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label15.Location = new System.Drawing.Point(253, 260);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(120, 13);
            this.Label15.TabIndex = 79;
            this.Label15.Text = "To continue, click Next.";
            // 
            // Label12
            // 
            this.Label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Label12.BackColor = System.Drawing.Color.Transparent;
            this.Label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(159, 50);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(288, 30);
            this.Label12.TabIndex = 78;
            this.Label12.Text = "Please specify employee and transfer details";
            // 
            // FrmEmployeeTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 403);
            this.ControlBox = false;
            this.Controls.Add(this.Wizard1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEmployeeTransfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmployeeTransfer";
            this.Load += new System.EventHandler(this.FrmEmployeeTransfer_Load);
            this.WizardEmployeeInfo.ResumeLayout(false);
            this.WizardEmployeeInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epEmployeeTransfer)).EndInit();
            this.WizardFinishPage.ResumeLayout(false);
            this.WizardFinishPage.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Wizard1.ResumeLayout(false);
            this.WizardWelcomePage.ResumeLayout(false);
            this.PanelEx1.ResumeLayout(false);
            this.PanelEx1.PerformLayout();
            this.pnlRequest.ResumeLayout(false);
            this.pnlRequest.PerformLayout();
            this.WizardEmployeeTransferInfo.ResumeLayout(false);
            this.WizardEmployeeTransferInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.LabelX lblEmployeeAcntNoText;
        internal DevComponents.DotNetBar.LabelX LabelX9;
        internal System.Windows.Forms.Label Label16;
        internal DevComponents.DotNetBar.LabelX lblEmployeeText;
        internal DevComponents.DotNetBar.WizardPage WizardEmployeeInfo;
        internal DevComponents.DotNetBar.LabelX LabelX8;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label6;
        internal DevComponents.DotNetBar.LabelX lblEmployeeBankNameText;
        internal System.Windows.Forms.Label Label14;
        internal DevComponents.DotNetBar.LabelX lblEmployerBankAntText;
        internal System.Windows.Forms.Label Label5;
        internal DevComponents.DotNetBar.LabelX lblEmployerBankNameText;
        internal DevComponents.DotNetBar.LabelX lblLeavePolicyText;
        internal System.Windows.Forms.ComboBox cboLeavePolicy;
        internal DevComponents.DotNetBar.LabelX lblWorkPolicyText;
        internal DevComponents.DotNetBar.LabelX lblTransactionTypeText;
        internal DevComponents.DotNetBar.LabelX LabelX6;
        internal System.Windows.Forms.ComboBox cboTransactionType;
        internal DevComponents.DotNetBar.LabelX LabelX5;
        internal System.Windows.Forms.ComboBox cboEmployerBankAct;
        internal DevComponents.DotNetBar.LabelX LabelX4;
        internal System.Windows.Forms.ComboBox cboEmployeeBank;
        internal DevComponents.DotNetBar.LabelX LabelX3;
        internal System.Windows.Forms.ComboBox cboEmployerBank;
        internal DevComponents.DotNetBar.LabelX LabelX2;
        internal System.Windows.Forms.ComboBox cboWorkPolicy;
        internal DevComponents.DotNetBar.LabelX LabelX1;
        internal System.Windows.Forms.TextBox txtEmployeeAntNo;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape3;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape2;
        internal System.Windows.Forms.Label lblTransferTo;
        internal System.Windows.Forms.Label lblTransferFrom;
        internal System.Windows.Forms.ComboBox cboTransferTo;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.Label lblComment;
        internal System.Windows.Forms.Timer TmrEmployeeTransfer;
        internal System.Windows.Forms.Label lblFinishComment;
        internal System.Windows.Forms.ErrorProvider epEmployeeTransfer;
        internal DevComponents.DotNetBar.Wizard Wizard1;
        internal DevComponents.DotNetBar.WizardPage WizardWelcomePage;
        internal DevComponents.DotNetBar.PanelEx PanelEx1;
        internal System.Windows.Forms.RadioButton rbEmployment;
        internal System.Windows.Forms.RadioButton rbDesignation;
        internal System.Windows.Forms.RadioButton rbDepartment;
        internal System.Windows.Forms.RadioButton rbCompanyBranch;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label3;
        internal DevComponents.DotNetBar.WizardPage WizardEmployeeTransferInfo;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.ComboBox cboTransferFrom;
        internal System.Windows.Forms.DateTimePicker dtpTransferDate;
        internal DevComponents.DotNetBar.WizardPage WizardFinishPage;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox txtOtherInfo;
        internal System.Windows.Forms.Label lblWait;
        internal System.Windows.Forms.ProgressBar ProgressBar1;
        internal System.Windows.Forms.Timer tmrProcess;
        private System.Windows.Forms.Label lblWorkPolicyT;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lblLeavePolicyT;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblTransactionTypeT;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblEmploymentTypeT;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblDepartmentT;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblDesignationT;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblEmployeeT;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblCompanyT;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblEmployerBankACT;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label lblEmployeeBankACT;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lblEmployeeBankT;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lblEmployerBankT;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSalaryStructure;
        internal System.Windows.Forms.ComboBox cboWorkLocation;
        internal DevComponents.DotNetBar.LabelX lblLocationText;
        internal DevComponents.DotNetBar.LabelX labelX10;
        private System.Windows.Forms.Label lblLocationT;
        private System.Windows.Forms.Label label17;
        internal DevComponents.DotNetBar.PanelEx pnlRequest;
        internal System.Windows.Forms.RadioButton rdbDirect;
        internal System.Windows.Forms.RadioButton rdbFromRequest;
        private System.Windows.Forms.CheckBox ChkTemp;
    }
}