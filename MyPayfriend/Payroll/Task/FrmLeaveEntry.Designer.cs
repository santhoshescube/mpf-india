﻿namespace MyPayfriend
{
    partial class FrmLeaveEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label LeaveTypeIDLabel;
            System.Windows.Forms.Label lblEmployee;
            System.Windows.Forms.Label lblCode;
            System.Windows.Forms.Label lblCompany;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLeaveEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BtnSave = new System.Windows.Forms.Button();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.BtnOk = new System.Windows.Forms.Button();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorCancelItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.TmFocus = new System.Windows.Forms.Timer(this.components);
            this.ErrorProviderEmpLeave = new System.Windows.Forms.ErrorProvider(this.components);
            this.TimerLeave = new System.Windows.Forms.Timer(this.components);
            this.EmployeeLeaveDetailsBindingSourceRpt = new System.Windows.Forms.BindingSource(this.components);
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.EmployeeLeaveDetailsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnLeaveExtension = new System.Windows.Forms.ToolStripMenuItem();
            this.RemarksTextBox = new System.Windows.Forms.TextBox();
            this.CboLeaveType = new System.Windows.Forms.ComboBox();
            this.HalfDayCheckBox = new System.Windows.Forms.CheckBox();
            this.LblBalLeave = new System.Windows.Forms.Label();
            this.LblDateTo = new System.Windows.Forms.Label();
            this.LblDateFrom = new System.Windows.Forms.Label();
            this.LblRejoinDate = new System.Windows.Forms.Label();
            this.LeaveDateFromDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.lblJoiningDateTag = new System.Windows.Forms.Label();
            this.LblDOJ = new System.Windows.Forms.Label();
            this.LeaveDateToDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.lblnoofholidays = new System.Windows.Forms.Label();
            this.chkPaidUnPaid = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblEmploee = new System.Windows.Forms.Label();
            this.LblBalLeaveColn = new System.Windows.Forms.Label();
            this.lblnoofholidaysColn = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblholidaysValue = new System.Windows.Forms.Label();
            this.lblBalLeaveValue = new System.Windows.Forms.Label();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.PanelLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvLeave = new ClsDataGirdViewX();
            this.Employee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FromDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeaveID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpPanelEmployee = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblTo = new System.Windows.Forms.Label();
            this.dtpSTo = new System.Windows.Forms.DateTimePicker();
            this.dtpSFrom = new System.Windows.Forms.DateTimePicker();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cboCode = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboEmployeeName = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.GrpMain = new DevComponents.DotNetBar.PanelEx();
            this.RejoinDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            LeaveTypeIDLabel = new System.Windows.Forms.Label();
            lblEmployee = new System.Windows.Forms.Label();
            lblCode = new System.Windows.Forms.Label();
            lblCompany = new System.Windows.Forms.Label();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderEmpLeave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLeaveDetailsBindingSourceRpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLeaveDetailsBindingNavigator)).BeginInit();
            this.EmployeeLeaveDetailsBindingNavigator.SuspendLayout();
            this.PanelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeave)).BeginInit();
            this.ExpPanelEmployee.SuspendLayout();
            this.GrpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // LeaveTypeIDLabel
            // 
            LeaveTypeIDLabel.AutoSize = true;
            LeaveTypeIDLabel.Location = new System.Drawing.Point(12, 91);
            LeaveTypeIDLabel.Name = "LeaveTypeIDLabel";
            LeaveTypeIDLabel.Size = new System.Drawing.Size(64, 13);
            LeaveTypeIDLabel.TabIndex = 50;
            LeaveTypeIDLabel.Text = "Leave Type";
            // 
            // lblEmployee
            // 
            lblEmployee.AutoSize = true;
            lblEmployee.Location = new System.Drawing.Point(7, 70);
            lblEmployee.Name = "lblEmployee";
            lblEmployee.Size = new System.Drawing.Size(53, 13);
            lblEmployee.TabIndex = 4;
            lblEmployee.Text = "Employee";
            // 
            // lblCode
            // 
            lblCode.AutoSize = true;
            lblCode.Location = new System.Drawing.Point(8, 96);
            lblCode.Name = "lblCode";
            lblCode.Size = new System.Drawing.Size(32, 13);
            lblCode.TabIndex = 6;
            lblCode.Text = "Code";
            // 
            // lblCompany
            // 
            lblCompany.AutoSize = true;
            lblCompany.Location = new System.Drawing.Point(7, 43);
            lblCompany.Name = "lblCompany";
            lblCompany.Size = new System.Drawing.Size(51, 13);
            lblCompany.TabIndex = 7;
            lblCompany.Text = "Company";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(346, 366);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 1;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(621, 366);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 3;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(702, 366);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // EmployeeLeaveDetailsBindingNavigatorSaveItem
            // 
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("EmployeeLeaveDetailsBindingNavigatorSaveItem.Image")));
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Name = "EmployeeLeaveDetailsBindingNavigatorSaveItem";
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Text = "Save Data";
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem.Click += new System.EventHandler(this.EmployeeLeaveDetailsBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorCancelItem
            // 
            this.BindingNavigatorCancelItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorCancelItem.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.BindingNavigatorCancelItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BindingNavigatorCancelItem.Name = "BindingNavigatorCancelItem";
            this.BindingNavigatorCancelItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorCancelItem.Text = "Clear";
            this.BindingNavigatorCancelItem.Click += new System.EventHandler(this.BindingNavigatorCancelItem_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 399);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(788, 22);
            this.StatusStrip1.TabIndex = 84;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(45, 17);
            this.StatusLabel.Text = "Status :";
            this.StatusLabel.ToolTipText = "Status";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // TmFocus
            // 
            this.TmFocus.Tick += new System.EventHandler(this.TmFocus_Tick);
            // 
            // ErrorProviderEmpLeave
            // 
            this.ErrorProviderEmpLeave.ContainerControl = this;
            this.ErrorProviderEmpLeave.RightToLeft = true;
            // 
            // TimerLeave
            // 
            this.TimerLeave.Interval = 2000;
            this.TimerLeave.Tick += new System.EventHandler(this.TimerLeave_Tick);
            // 
            // EmployeeLeaveDetailsBindingSourceRpt
            // 
            this.EmployeeLeaveDetailsBindingSourceRpt.DataMember = "EmployeeLeaveDetails";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // EmployeeLeaveDetailsBindingNavigator
            // 
            this.EmployeeLeaveDetailsBindingNavigator.AddNewItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.CountItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.DeleteItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewItem,
            this.EmployeeLeaveDetailsBindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorCancelItem,
            this.ToolStripSeparator1,
            this.bnMoreActions,
            this.ToolStripSeparator2,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator3,
            this.BtnHelp});
            this.EmployeeLeaveDetailsBindingNavigator.Location = new System.Drawing.Point(339, 0);
            this.EmployeeLeaveDetailsBindingNavigator.MoveFirstItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.MoveLastItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.MoveNextItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.MovePreviousItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.Name = "EmployeeLeaveDetailsBindingNavigator";
            this.EmployeeLeaveDetailsBindingNavigator.PositionItem = null;
            this.EmployeeLeaveDetailsBindingNavigator.Size = new System.Drawing.Size(449, 25);
            this.EmployeeLeaveDetailsBindingNavigator.TabIndex = 79;
            this.EmployeeLeaveDetailsBindingNavigator.Text = "BindingNavigator1";
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDocument,
            this.BtnLeaveExtension});
            this.bnMoreActions.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.bnMoreActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "Actions";
            this.bnMoreActions.ToolTipText = "Actions";
            // 
            // btnDocument
            // 
            this.btnDocument.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.btnDocument.Name = "btnDocument";
            this.btnDocument.Size = new System.Drawing.Size(157, 22);
            this.btnDocument.Text = "Documents";
            this.btnDocument.ToolTipText = "Documents";
            this.btnDocument.Click += new System.EventHandler(this.btnDocument_Click);
            // 
            // BtnLeaveExtension
            // 
            this.BtnLeaveExtension.Image = global::MyPayfriend.Properties.Resources.leave_extention;
            this.BtnLeaveExtension.Name = "BtnLeaveExtension";
            this.BtnLeaveExtension.Size = new System.Drawing.Size(157, 22);
            this.BtnLeaveExtension.Text = "Leave Extension";
            this.BtnLeaveExtension.ToolTipText = "Leave Extension";
            this.BtnLeaveExtension.Click += new System.EventHandler(this.BtnLeaveExtension_Click);
            // 
            // RemarksTextBox
            // 
            this.RemarksTextBox.Location = new System.Drawing.Point(105, 193);
            this.RemarksTextBox.MaxLength = 500;
            this.RemarksTextBox.Multiline = true;
            this.RemarksTextBox.Name = "RemarksTextBox";
            this.RemarksTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RemarksTextBox.Size = new System.Drawing.Size(323, 122);
            this.RemarksTextBox.TabIndex = 6;
            this.RemarksTextBox.TextChanged += new System.EventHandler(this.RemarksTextBox_TextChanged);
            // 
            // CboLeaveType
            // 
            this.CboLeaveType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboLeaveType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboLeaveType.BackColor = System.Drawing.SystemColors.Info;
            this.CboLeaveType.DropDownHeight = 134;
            this.CboLeaveType.FormattingEnabled = true;
            this.CboLeaveType.IntegralHeight = false;
            this.CboLeaveType.Location = new System.Drawing.Point(108, 87);
            this.CboLeaveType.Name = "CboLeaveType";
            this.CboLeaveType.Size = new System.Drawing.Size(198, 21);
            this.CboLeaveType.TabIndex = 1;
            this.CboLeaveType.SelectionChangeCommitted += new System.EventHandler(this.CboLeaveType_SelectionChangeCommitted);
            this.CboLeaveType.SelectedIndexChanged += new System.EventHandler(this.CboLeaveType_SelectedIndexChanged);
            this.CboLeaveType.Leave += new System.EventHandler(this.CboLeaveType_Leave);
            this.CboLeaveType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboLeaveType_KeyPress);
            // 
            // HalfDayCheckBox
            // 
            this.HalfDayCheckBox.AutoSize = true;
            this.HalfDayCheckBox.Location = new System.Drawing.Point(108, 127);
            this.HalfDayCheckBox.Name = "HalfDayCheckBox";
            this.HalfDayCheckBox.Size = new System.Drawing.Size(67, 17);
            this.HalfDayCheckBox.TabIndex = 2;
            this.HalfDayCheckBox.Text = "Half Day";
            this.HalfDayCheckBox.UseVisualStyleBackColor = true;
            this.HalfDayCheckBox.CheckedChanged += new System.EventHandler(this.HalfDayCheckBox_CheckedChanged);
            // 
            // LblBalLeave
            // 
            this.LblBalLeave.AutoSize = true;
            this.LblBalLeave.Location = new System.Drawing.Point(315, 90);
            this.LblBalLeave.Name = "LblBalLeave";
            this.LblBalLeave.Size = new System.Drawing.Size(79, 13);
            this.LblBalLeave.TabIndex = 52;
            this.LblBalLeave.Text = "Balance Leave";
            this.LblBalLeave.VisibleChanged += new System.EventHandler(this.LblBalLeave_VisibleChanged);
            // 
            // LblDateTo
            // 
            this.LblDateTo.AutoSize = true;
            this.LblDateTo.Location = new System.Drawing.Point(236, 163);
            this.LblDateTo.Name = "LblDateTo";
            this.LblDateTo.Size = new System.Drawing.Size(46, 13);
            this.LblDateTo.TabIndex = 54;
            this.LblDateTo.Text = "Date To";
            // 
            // LblDateFrom
            // 
            this.LblDateFrom.AutoSize = true;
            this.LblDateFrom.Location = new System.Drawing.Point(12, 163);
            this.LblDateFrom.Name = "LblDateFrom";
            this.LblDateFrom.Size = new System.Drawing.Size(56, 13);
            this.LblDateFrom.TabIndex = 53;
            this.LblDateFrom.Text = "Date From";
            // 
            // LblRejoinDate
            // 
            this.LblRejoinDate.AutoSize = true;
            this.LblRejoinDate.Location = new System.Drawing.Point(434, 341);
            this.LblRejoinDate.Name = "LblRejoinDate";
            this.LblRejoinDate.Size = new System.Drawing.Size(63, 13);
            this.LblRejoinDate.TabIndex = 55;
            this.LblRejoinDate.Text = "Rejoin Date";
            this.LblRejoinDate.Visible = false;
            // 
            // LeaveDateFromDateTimePicker
            // 
            this.LeaveDateFromDateTimePicker.CustomFormat = "dd-MMM-yyyy";
            this.LeaveDateFromDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.LeaveDateFromDateTimePicker.Location = new System.Drawing.Point(105, 159);
            this.LeaveDateFromDateTimePicker.Name = "LeaveDateFromDateTimePicker";
            this.LeaveDateFromDateTimePicker.Size = new System.Drawing.Size(106, 20);
            this.LeaveDateFromDateTimePicker.TabIndex = 4;
            this.LeaveDateFromDateTimePicker.ValueChanged += new System.EventHandler(this.LeaveDateFromDateTimePicker_ValueChanged);
            // 
            // lblJoiningDateTag
            // 
            this.lblJoiningDateTag.AutoSize = true;
            this.lblJoiningDateTag.Location = new System.Drawing.Point(12, 59);
            this.lblJoiningDateTag.Name = "lblJoiningDateTag";
            this.lblJoiningDateTag.Size = new System.Drawing.Size(66, 13);
            this.lblJoiningDateTag.TabIndex = 75;
            this.lblJoiningDateTag.Text = "Joining Date";
            // 
            // LblDOJ
            // 
            this.LblDOJ.AutoSize = true;
            this.LblDOJ.Location = new System.Drawing.Point(121, 59);
            this.LblDOJ.Name = "LblDOJ";
            this.LblDOJ.Size = new System.Drawing.Size(28, 13);
            this.LblDOJ.TabIndex = 1;
            this.LblDOJ.Text = "DOJ";
            // 
            // LeaveDateToDateTimePicker
            // 
            this.LeaveDateToDateTimePicker.CustomFormat = "dd-MMM-yyyy";
            this.LeaveDateToDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.LeaveDateToDateTimePicker.Location = new System.Drawing.Point(289, 159);
            this.LeaveDateToDateTimePicker.Name = "LeaveDateToDateTimePicker";
            this.LeaveDateToDateTimePicker.Size = new System.Drawing.Size(102, 20);
            this.LeaveDateToDateTimePicker.TabIndex = 5;
            this.LeaveDateToDateTimePicker.ValueChanged += new System.EventHandler(this.LeaveDateToDateTimePicker_ValueChanged);
            // 
            // lblnoofholidays
            // 
            this.lblnoofholidays.AutoSize = true;
            this.lblnoofholidays.Location = new System.Drawing.Point(315, 128);
            this.lblnoofholidays.Name = "lblnoofholidays";
            this.lblnoofholidays.Size = new System.Drawing.Size(78, 13);
            this.lblnoofholidays.TabIndex = 77;
            this.lblnoofholidays.Text = "No Of Holidays";
            this.lblnoofholidays.Visible = false;
            this.lblnoofholidays.VisibleChanged += new System.EventHandler(this.lblnoofholidays_VisibleChanged);
            // 
            // chkPaidUnPaid
            // 
            this.chkPaidUnPaid.AutoSize = true;
            this.chkPaidUnPaid.Location = new System.Drawing.Point(199, 124);
            this.chkPaidUnPaid.Name = "chkPaidUnPaid";
            this.chkPaidUnPaid.Size = new System.Drawing.Size(60, 17);
            this.chkPaidUnPaid.TabIndex = 3;
            this.chkPaidUnPaid.Text = "Unpaid";
            this.chkPaidUnPaid.UseVisualStyleBackColor = true;
            this.chkPaidUnPaid.CheckedChanged += new System.EventHandler(this.chkPaidUnPaid_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(105, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 89;
            this.label8.Text = ":";
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.Location = new System.Drawing.Point(121, 28);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(53, 13);
            this.lblEmployeeName.TabIndex = 87;
            this.lblEmployeeName.Text = "Employee";
            // 
            // lblEmploee
            // 
            this.lblEmploee.AutoSize = true;
            this.lblEmploee.Location = new System.Drawing.Point(12, 28);
            this.lblEmploee.Name = "lblEmploee";
            this.lblEmploee.Size = new System.Drawing.Size(53, 13);
            this.lblEmploee.TabIndex = 88;
            this.lblEmploee.Text = "Employee";
            // 
            // LblBalLeaveColn
            // 
            this.LblBalLeaveColn.AutoSize = true;
            this.LblBalLeaveColn.Location = new System.Drawing.Point(399, 90);
            this.LblBalLeaveColn.Name = "LblBalLeaveColn";
            this.LblBalLeaveColn.Size = new System.Drawing.Size(10, 13);
            this.LblBalLeaveColn.TabIndex = 83;
            this.LblBalLeaveColn.Text = ":";
            // 
            // lblnoofholidaysColn
            // 
            this.lblnoofholidaysColn.AutoSize = true;
            this.lblnoofholidaysColn.Location = new System.Drawing.Point(399, 128);
            this.lblnoofholidaysColn.Name = "lblnoofholidaysColn";
            this.lblnoofholidaysColn.Size = new System.Drawing.Size(10, 13);
            this.lblnoofholidaysColn.TabIndex = 82;
            this.lblnoofholidaysColn.Text = ":";
            this.lblnoofholidaysColn.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(105, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 81;
            this.label2.Text = ":";
            // 
            // lblholidaysValue
            // 
            this.lblholidaysValue.AutoSize = true;
            this.lblholidaysValue.Location = new System.Drawing.Point(415, 128);
            this.lblholidaysValue.Name = "lblholidaysValue";
            this.lblholidaysValue.Size = new System.Drawing.Size(13, 13);
            this.lblholidaysValue.TabIndex = 80;
            this.lblholidaysValue.Text = "0";
            this.lblholidaysValue.Visible = false;
            // 
            // lblBalLeaveValue
            // 
            this.lblBalLeaveValue.AutoSize = true;
            this.lblBalLeaveValue.Location = new System.Drawing.Point(416, 90);
            this.lblBalLeaveValue.Name = "lblBalLeaveValue";
            this.lblBalLeaveValue.Size = new System.Drawing.Size(13, 13);
            this.lblBalLeaveValue.TabIndex = 79;
            this.lblBalLeaveValue.Text = "0";
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(12, 193);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 78;
            this.lblRemarks.Text = "Remarks";
            // 
            // PanelLeft
            // 
            this.PanelLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.PanelLeft.Controls.Add(this.dgvLeave);
            this.PanelLeft.Controls.Add(this.ExpPanelEmployee);
            this.PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(339, 399);
            this.PanelLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelLeft.Style.GradientAngle = 90;
            this.PanelLeft.TabIndex = 85;
            this.PanelLeft.Text = "panelEx1";
            // 
            // dgvLeave
            // 
            this.dgvLeave.AddNewRow = false;
            this.dgvLeave.AllowUserToAddRows = false;
            this.dgvLeave.AllowUserToDeleteRows = false;
            this.dgvLeave.AllowUserToResizeColumns = false;
            this.dgvLeave.AllowUserToResizeRows = false;
            this.dgvLeave.AlphaNumericCols = new int[0];
            this.dgvLeave.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvLeave.CapsLockCols = new int[0];
            this.dgvLeave.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLeave.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Employee,
            this.FromDate,
            this.ToDate,
            this.LeaveID});
            this.dgvLeave.DecimalCols = new int[0];
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLeave.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLeave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLeave.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvLeave.HasSlNo = false;
            this.dgvLeave.LastRowIndex = 0;
            this.dgvLeave.Location = new System.Drawing.Point(0, 178);
            this.dgvLeave.Name = "dgvLeave";
            this.dgvLeave.NegativeValueCols = new int[0];
            this.dgvLeave.NumericCols = new int[0];
            this.dgvLeave.ReadOnly = true;
            this.dgvLeave.RowHeadersWidth = 30;
            this.dgvLeave.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLeave.Size = new System.Drawing.Size(339, 221);
            this.dgvLeave.TabIndex = 1;
            this.dgvLeave.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLeave_CellDoubleClick);
            this.dgvLeave.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvLeave_DataError);
            // 
            // Employee
            // 
            this.Employee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Employee.DataPropertyName = "Employee";
            this.Employee.FillWeight = 245.4227F;
            this.Employee.HeaderText = "Employee";
            this.Employee.Name = "Employee";
            this.Employee.ReadOnly = true;
            // 
            // FromDate
            // 
            this.FromDate.DataPropertyName = "FromDate";
            this.FromDate.FillWeight = 42.139F;
            this.FromDate.HeaderText = "FromDate";
            this.FromDate.Name = "FromDate";
            this.FromDate.ReadOnly = true;
            this.FromDate.Width = 80;
            // 
            // ToDate
            // 
            this.ToDate.DataPropertyName = "ToDate";
            this.ToDate.FillWeight = 34.76012F;
            this.ToDate.HeaderText = "ToDate";
            this.ToDate.Name = "ToDate";
            this.ToDate.ReadOnly = true;
            this.ToDate.Width = 80;
            // 
            // LeaveID
            // 
            this.LeaveID.DataPropertyName = "LeaveID";
            this.LeaveID.HeaderText = "LeaveID";
            this.LeaveID.Name = "LeaveID";
            this.LeaveID.ReadOnly = true;
            this.LeaveID.Visible = false;
            // 
            // ExpPanelEmployee
            // 
            this.ExpPanelEmployee.CanvasColor = System.Drawing.SystemColors.Control;
            this.ExpPanelEmployee.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ExpPanelEmployee.Controls.Add(this.cboCompany);
            this.ExpPanelEmployee.Controls.Add(this.btnSearch);
            this.ExpPanelEmployee.Controls.Add(this.lblTo);
            this.ExpPanelEmployee.Controls.Add(this.dtpSTo);
            this.ExpPanelEmployee.Controls.Add(this.dtpSFrom);
            this.ExpPanelEmployee.Controls.Add(this.lblFrom);
            this.ExpPanelEmployee.Controls.Add(lblCompany);
            this.ExpPanelEmployee.Controls.Add(lblCode);
            this.ExpPanelEmployee.Controls.Add(this.cboCode);
            this.ExpPanelEmployee.Controls.Add(lblEmployee);
            this.ExpPanelEmployee.Controls.Add(this.cboEmployeeName);
            this.ExpPanelEmployee.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExpPanelEmployee.Location = new System.Drawing.Point(0, 0);
            this.ExpPanelEmployee.Name = "ExpPanelEmployee";
            this.ExpPanelEmployee.Size = new System.Drawing.Size(339, 178);
            this.ExpPanelEmployee.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpPanelEmployee.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpPanelEmployee.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpPanelEmployee.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.ExpPanelEmployee.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.ExpPanelEmployee.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.ExpPanelEmployee.Style.GradientAngle = 90;
            this.ExpPanelEmployee.TabIndex = 0;
            this.ExpPanelEmployee.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.ExpPanelEmployee.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.ExpPanelEmployee.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.ExpPanelEmployee.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.ExpPanelEmployee.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.ExpPanelEmployee.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.ExpPanelEmployee.TitleStyle.GradientAngle = 90;
            this.ExpPanelEmployee.TitleText = "Search";
            // 
            // cboCompany
            // 
            this.cboCompany.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 15;
            this.cboCompany.Location = new System.Drawing.Point(73, 37);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(199, 21);
            this.cboCompany.TabIndex = 59;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboKeyDown);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(202, 138);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 58;
            this.btnSearch.Text = "Searc&h";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(8, 143);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 57;
            this.lblTo.Text = "To";
            // 
            // dtpSTo
            // 
            this.dtpSTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpSTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSTo.Location = new System.Drawing.Point(73, 143);
            this.dtpSTo.Name = "dtpSTo";
            this.dtpSTo.Size = new System.Drawing.Size(85, 20);
            this.dtpSTo.TabIndex = 56;
            // 
            // dtpSFrom
            // 
            this.dtpSFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpSFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSFrom.Location = new System.Drawing.Point(73, 117);
            this.dtpSFrom.Name = "dtpSFrom";
            this.dtpSFrom.Size = new System.Drawing.Size(82, 20);
            this.dtpSFrom.TabIndex = 54;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(7, 120);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 55;
            this.lblFrom.Text = "From";
            // 
            // cboCode
            // 
            this.cboCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCode.DisplayMember = "Text";
            this.cboCode.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCode.DropDownHeight = 134;
            this.cboCode.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCode.FormattingEnabled = true;
            this.cboCode.IntegralHeight = false;
            this.cboCode.ItemHeight = 15;
            this.cboCode.Location = new System.Drawing.Point(73, 90);
            this.cboCode.Name = "cboCode";
            this.cboCode.Size = new System.Drawing.Size(202, 21);
            this.cboCode.TabIndex = 5;
            this.cboCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboKeyDown);
            // 
            // cboEmployeeName
            // 
            this.cboEmployeeName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cboEmployeeName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployeeName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployeeName.DisplayMember = "Text";
            this.cboEmployeeName.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmployeeName.DropDownHeight = 134;
            this.cboEmployeeName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEmployeeName.FormattingEnabled = true;
            this.cboEmployeeName.IntegralHeight = false;
            this.cboEmployeeName.ItemHeight = 15;
            this.cboEmployeeName.Location = new System.Drawing.Point(73, 64);
            this.cboEmployeeName.Name = "cboEmployeeName";
            this.cboEmployeeName.Size = new System.Drawing.Size(202, 21);
            this.cboEmployeeName.TabIndex = 3;
            this.cboEmployeeName.SelectedIndexChanged += new System.EventHandler(this.cboEmployeeName_SelectedIndexChanged);
            this.cboEmployeeName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboKeyDown);
            // 
            // GrpMain
            // 
            this.GrpMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.GrpMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.GrpMain.Controls.Add(this.label8);
            this.GrpMain.Controls.Add(this.lblEmploee);
            this.GrpMain.Controls.Add(this.lblRemarks);
            this.GrpMain.Controls.Add(this.RejoinDateDateTimePicker);
            this.GrpMain.Controls.Add(this.RemarksTextBox);
            this.GrpMain.Controls.Add(this.lblEmployeeName);
            this.GrpMain.Controls.Add(LeaveTypeIDLabel);
            this.GrpMain.Controls.Add(this.CboLeaveType);
            this.GrpMain.Controls.Add(this.LblBalLeaveColn);
            this.GrpMain.Controls.Add(this.HalfDayCheckBox);
            this.GrpMain.Controls.Add(this.LblBalLeave);
            this.GrpMain.Controls.Add(this.LblDateTo);
            this.GrpMain.Controls.Add(this.lblnoofholidaysColn);
            this.GrpMain.Controls.Add(this.LblDateFrom);
            this.GrpMain.Controls.Add(this.label2);
            this.GrpMain.Controls.Add(this.LeaveDateFromDateTimePicker);
            this.GrpMain.Controls.Add(this.lblholidaysValue);
            this.GrpMain.Controls.Add(this.lblJoiningDateTag);
            this.GrpMain.Controls.Add(this.lblBalLeaveValue);
            this.GrpMain.Controls.Add(this.LblDOJ);
            this.GrpMain.Controls.Add(this.chkPaidUnPaid);
            this.GrpMain.Controls.Add(this.LeaveDateToDateTimePicker);
            this.GrpMain.Controls.Add(this.lblnoofholidays);
            this.GrpMain.Location = new System.Drawing.Point(339, 25);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(449, 335);
            this.GrpMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.GrpMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.GrpMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.GrpMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.GrpMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GrpMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GrpMain.Style.GradientAngle = 90;
            this.GrpMain.TabIndex = 86;
            // 
            // RejoinDateDateTimePicker
            // 
            this.RejoinDateDateTimePicker.CustomFormat = "dd-MMM-yyyy";
            this.RejoinDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RejoinDateDateTimePicker.Location = new System.Drawing.Point(481, 193);
            this.RejoinDateDateTimePicker.Name = "RejoinDateDateTimePicker";
            this.RejoinDateDateTimePicker.Size = new System.Drawing.Size(82, 20);
            this.RejoinDateDateTimePicker.TabIndex = 54;
            this.RejoinDateDateTimePicker.Visible = false;
            // 
            // FrmLeaveEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 421);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.EmployeeLeaveDetailsBindingNavigator);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.LblRejoinDate);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLeaveEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Leave Entry";
            this.Load += new System.EventHandler(this.FrmLeaveEntry_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLeaveEntry_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLeaveEntry_KeyDown);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderEmpLeave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLeaveDetailsBindingSourceRpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLeaveDetailsBindingNavigator)).EndInit();
            this.EmployeeLeaveDetailsBindingNavigator.ResumeLayout(false);
            this.EmployeeLeaveDetailsBindingNavigator.PerformLayout();
            this.PanelLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeave)).EndInit();
            this.ExpPanelEmployee.ResumeLayout(false);
            this.ExpPanelEmployee.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripButton EmployeeLeaveDetailsBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorCancelItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Timer TmFocus;
        internal System.Windows.Forms.ErrorProvider ErrorProviderEmpLeave;
        internal System.Windows.Forms.BindingNavigator EmployeeLeaveDetailsBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.Timer TimerLeave;
        internal System.Windows.Forms.BindingSource EmployeeLeaveDetailsBindingSourceRpt;
        internal System.Windows.Forms.CheckBox chkPaidUnPaid;
        internal System.Windows.Forms.Label lblnoofholidays;
        internal System.Windows.Forms.DateTimePicker LeaveDateToDateTimePicker;
        internal System.Windows.Forms.Label LblDOJ;
        internal System.Windows.Forms.Label lblJoiningDateTag;
        internal System.Windows.Forms.DateTimePicker LeaveDateFromDateTimePicker;
        internal System.Windows.Forms.Label LblDateFrom;
        internal System.Windows.Forms.Label LblDateTo;
        internal System.Windows.Forms.Label LblBalLeave;
        internal System.Windows.Forms.CheckBox HalfDayCheckBox;
        internal System.Windows.Forms.ComboBox CboLeaveType;
        internal System.Windows.Forms.TextBox RemarksTextBox;
        internal System.Windows.Forms.Label LblRejoinDate;
        internal System.Windows.Forms.Label lblRemarks;
        internal System.Windows.Forms.Label lblholidaysValue;
        internal System.Windows.Forms.Label lblBalLeaveValue;
        private System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        private System.Windows.Forms.ToolStripMenuItem btnDocument;
        private System.Windows.Forms.ToolStripMenuItem BtnLeaveExtension;
        internal System.Windows.Forms.Label LblBalLeaveColn;
        internal System.Windows.Forms.Label lblnoofholidaysColn;
        internal System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.PanelEx PanelLeft;
        private ClsDataGirdViewX dgvLeave;
        private DevComponents.DotNetBar.ExpandablePanel ExpPanelEmployee;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboEmployeeName;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboCode;
        internal System.Windows.Forms.Label lblTo;
        internal System.Windows.Forms.DateTimePicker dtpSTo;
        internal System.Windows.Forms.DateTimePicker dtpSFrom;
        internal System.Windows.Forms.Label lblFrom;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label lblEmployeeName;
        internal System.Windows.Forms.Label lblEmploee;
        internal System.Windows.Forms.Button btnSearch;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.PanelEx GrpMain;
        internal System.Windows.Forms.DateTimePicker RejoinDateDateTimePicker;
        private System.Windows.Forms.DataGridViewTextBoxColumn Employee;
        private System.Windows.Forms.DataGridViewTextBoxColumn FromDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ToDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn LeaveID;
    }
}