﻿namespace MyPayfriend
{
    partial class FrmEmployeeLoan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label10;
            System.Windows.Forms.Label NoOfInstalmentLabel;
            System.Windows.Forms.Label DeductEndDateLabel;
            System.Windows.Forms.Label DeductStartDateLabel;
            System.Windows.Forms.Label InterestRateLabel;
            System.Windows.Forms.Label LoanNumberLabel;
            System.Windows.Forms.Label EmployeeIDLabel;
            System.Windows.Forms.Label LoanTypeIDLabel;
            System.Windows.Forms.Label LoanDateLabel;
            System.Windows.Forms.Label AmountLabel;
            System.Windows.Forms.Label InterestTypeIDLabel;
            System.Windows.Forms.Label PaymentAmountLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeeLoan));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.LoanBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.lblBalanceAmount = new System.Windows.Forms.Label();
            this.lblDeductEndDate = new System.Windows.Forms.Label();
            this.chkClosed = new System.Windows.Forms.CheckBox();
            this.dgvInstallmentDetails = new System.Windows.Forms.DataGridView();
            this.LoanID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InstallmentNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InstallmentDate = new DemoClsDataGridview.CalendarColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InterestAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsPaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountDecimal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Lblbasicpay = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.LblPayClassification = new System.Windows.Forms.Label();
            this.LblPayment = new System.Windows.Forms.Label();
            this.txtNoOfInstallments = new System.Windows.Forms.TextBox();
            this.dtpDeductStartDate = new System.Windows.Forms.DateTimePicker();
            this.txtInterestRate = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.LblDOJ = new System.Windows.Forms.Label();
            this.LblJoinDate = new System.Windows.Forms.Label();
            this.TextBoxNumricInstalment = new System.Windows.Forms.TextBox();
            this.TextboxNumeric = new System.Windows.Forms.TextBox();
            this.btnLoanType = new System.Windows.Forms.Button();
            this.cboInterestType = new System.Windows.Forms.ComboBox();
            this.cboLoanType = new System.Windows.Forms.ComboBox();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.txtLoanNumber = new System.Windows.Forms.TextBox();
            this.dtpLoanDate = new System.Windows.Forms.DateTimePicker();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtPaymentAmount = new System.Windows.Forms.TextBox();
            this.txtReason = new System.Windows.Forms.TextBox();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errLoan = new System.Windows.Forms.ErrorProvider(this.components);
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            Label10 = new System.Windows.Forms.Label();
            NoOfInstalmentLabel = new System.Windows.Forms.Label();
            DeductEndDateLabel = new System.Windows.Forms.Label();
            DeductStartDateLabel = new System.Windows.Forms.Label();
            InterestRateLabel = new System.Windows.Forms.Label();
            LoanNumberLabel = new System.Windows.Forms.Label();
            EmployeeIDLabel = new System.Windows.Forms.Label();
            LoanTypeIDLabel = new System.Windows.Forms.Label();
            LoanDateLabel = new System.Windows.Forms.Label();
            AmountLabel = new System.Windows.Forms.Label();
            InterestTypeIDLabel = new System.Windows.Forms.Label();
            PaymentAmountLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LoanBindingNavigator)).BeginInit();
            this.LoanBindingNavigator.SuspendLayout();
            this.GrpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstallmentDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errLoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label10
            // 
            Label10.AutoSize = true;
            Label10.Location = new System.Drawing.Point(13, 98);
            Label10.Name = "Label10";
            Label10.Size = new System.Drawing.Size(49, 13);
            Label10.TabIndex = 1028;
            Label10.Text = "Currency";
            // 
            // NoOfInstalmentLabel
            // 
            NoOfInstalmentLabel.AutoSize = true;
            NoOfInstalmentLabel.Location = new System.Drawing.Point(347, 99);
            NoOfInstalmentLabel.Name = "NoOfInstalmentLabel";
            NoOfInstalmentLabel.Size = new System.Drawing.Size(93, 13);
            NoOfInstalmentLabel.TabIndex = 2;
            NoOfInstalmentLabel.Text = "No Of Installments";
            // 
            // DeductEndDateLabel
            // 
            DeductEndDateLabel.AutoSize = true;
            DeductEndDateLabel.Location = new System.Drawing.Point(347, 177);
            DeductEndDateLabel.Name = "DeductEndDateLabel";
            DeductEndDateLabel.Size = new System.Drawing.Size(90, 13);
            DeductEndDateLabel.TabIndex = 0;
            DeductEndDateLabel.Text = "Deduct End Date";
            // 
            // DeductStartDateLabel
            // 
            DeductStartDateLabel.AutoSize = true;
            DeductStartDateLabel.Location = new System.Drawing.Point(347, 73);
            DeductStartDateLabel.Name = "DeductStartDateLabel";
            DeductStartDateLabel.Size = new System.Drawing.Size(93, 13);
            DeductStartDateLabel.TabIndex = 0;
            DeductStartDateLabel.Text = "Deduct Start Date";
            // 
            // InterestRateLabel
            // 
            InterestRateLabel.AutoSize = true;
            InterestRateLabel.Location = new System.Drawing.Point(347, 47);
            InterestRateLabel.Name = "InterestRateLabel";
            InterestRateLabel.Size = new System.Drawing.Size(68, 13);
            InterestRateLabel.TabIndex = 0;
            InterestRateLabel.Text = "Interest Rate";
            // 
            // LoanNumberLabel
            // 
            LoanNumberLabel.AutoSize = true;
            LoanNumberLabel.Location = new System.Drawing.Point(13, 124);
            LoanNumberLabel.Name = "LoanNumberLabel";
            LoanNumberLabel.Size = new System.Drawing.Size(71, 13);
            LoanNumberLabel.TabIndex = 2;
            LoanNumberLabel.Text = "Loan Number";
            // 
            // EmployeeIDLabel
            // 
            EmployeeIDLabel.AutoSize = true;
            EmployeeIDLabel.Location = new System.Drawing.Point(13, 21);
            EmployeeIDLabel.Name = "EmployeeIDLabel";
            EmployeeIDLabel.Size = new System.Drawing.Size(84, 13);
            EmployeeIDLabel.TabIndex = 4;
            EmployeeIDLabel.Text = "Employee Name";
            // 
            // LoanTypeIDLabel
            // 
            LoanTypeIDLabel.AutoSize = true;
            LoanTypeIDLabel.Location = new System.Drawing.Point(13, 150);
            LoanTypeIDLabel.Name = "LoanTypeIDLabel";
            LoanTypeIDLabel.Size = new System.Drawing.Size(61, 13);
            LoanTypeIDLabel.TabIndex = 6;
            LoanTypeIDLabel.Text = "Loan Type ";
            // 
            // LoanDateLabel
            // 
            LoanDateLabel.AutoSize = true;
            LoanDateLabel.Location = new System.Drawing.Point(13, 176);
            LoanDateLabel.Name = "LoanDateLabel";
            LoanDateLabel.Size = new System.Drawing.Size(57, 13);
            LoanDateLabel.TabIndex = 8;
            LoanDateLabel.Text = "Loan Date";
            // 
            // AmountLabel
            // 
            AmountLabel.AutoSize = true;
            AmountLabel.Location = new System.Drawing.Point(347, 125);
            AmountLabel.Name = "AmountLabel";
            AmountLabel.Size = new System.Drawing.Size(43, 13);
            AmountLabel.TabIndex = 10;
            AmountLabel.Text = "Amount";
            // 
            // InterestTypeIDLabel
            // 
            InterestTypeIDLabel.AutoSize = true;
            InterestTypeIDLabel.Location = new System.Drawing.Point(347, 21);
            InterestTypeIDLabel.Name = "InterestTypeIDLabel";
            InterestTypeIDLabel.Size = new System.Drawing.Size(69, 13);
            InterestTypeIDLabel.TabIndex = 12;
            InterestTypeIDLabel.Text = "Interest Type";
            // 
            // PaymentAmountLabel
            // 
            PaymentAmountLabel.AutoSize = true;
            PaymentAmountLabel.Location = new System.Drawing.Point(347, 151);
            PaymentAmountLabel.Name = "PaymentAmountLabel";
            PaymentAmountLabel.Size = new System.Drawing.Size(87, 13);
            PaymentAmountLabel.TabIndex = 14;
            PaymentAmountLabel.Text = "Payment Amount";
            // 
            // LoanBindingNavigator
            // 
            this.LoanBindingNavigator.AddNewItem = null;
            this.LoanBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.LoanBindingNavigator.DeleteItem = null;
            this.LoanBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.btnClear,
            this.ToolStripSeparator1,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.LoanBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.LoanBindingNavigator.MoveFirstItem = null;
            this.LoanBindingNavigator.MoveLastItem = null;
            this.LoanBindingNavigator.MoveNextItem = null;
            this.LoanBindingNavigator.MovePreviousItem = null;
            this.LoanBindingNavigator.Name = "LoanBindingNavigator";
            this.LoanBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.LoanBindingNavigator.Size = new System.Drawing.Size(620, 25);
            this.LoanBindingNavigator.TabIndex = 1;
            this.LoanBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current Position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.lblBalanceAmount);
            this.GrpMain.Controls.Add(this.lblDeductEndDate);
            this.GrpMain.Controls.Add(this.chkClosed);
            this.GrpMain.Controls.Add(this.dgvInstallmentDetails);
            this.GrpMain.Controls.Add(Label10);
            this.GrpMain.Controls.Add(this.cboCurrency);
            this.GrpMain.Controls.Add(this.Label4);
            this.GrpMain.Controls.Add(this.Lblbasicpay);
            this.GrpMain.Controls.Add(this.Label2);
            this.GrpMain.Controls.Add(this.LblPayClassification);
            this.GrpMain.Controls.Add(this.LblPayment);
            this.GrpMain.Controls.Add(NoOfInstalmentLabel);
            this.GrpMain.Controls.Add(this.txtNoOfInstallments);
            this.GrpMain.Controls.Add(DeductEndDateLabel);
            this.GrpMain.Controls.Add(DeductStartDateLabel);
            this.GrpMain.Controls.Add(this.dtpDeductStartDate);
            this.GrpMain.Controls.Add(InterestRateLabel);
            this.GrpMain.Controls.Add(this.txtInterestRate);
            this.GrpMain.Controls.Add(this.Label1);
            this.GrpMain.Controls.Add(this.LblDOJ);
            this.GrpMain.Controls.Add(this.LblJoinDate);
            this.GrpMain.Controls.Add(this.TextBoxNumricInstalment);
            this.GrpMain.Controls.Add(this.TextboxNumeric);
            this.GrpMain.Controls.Add(this.btnLoanType);
            this.GrpMain.Controls.Add(this.cboInterestType);
            this.GrpMain.Controls.Add(this.cboLoanType);
            this.GrpMain.Controls.Add(this.cboEmployee);
            this.GrpMain.Controls.Add(LoanNumberLabel);
            this.GrpMain.Controls.Add(this.txtLoanNumber);
            this.GrpMain.Controls.Add(EmployeeIDLabel);
            this.GrpMain.Controls.Add(LoanTypeIDLabel);
            this.GrpMain.Controls.Add(LoanDateLabel);
            this.GrpMain.Controls.Add(this.dtpLoanDate);
            this.GrpMain.Controls.Add(AmountLabel);
            this.GrpMain.Controls.Add(this.txtAmount);
            this.GrpMain.Controls.Add(InterestTypeIDLabel);
            this.GrpMain.Controls.Add(PaymentAmountLabel);
            this.GrpMain.Controls.Add(this.txtPaymentAmount);
            this.GrpMain.Controls.Add(this.txtReason);
            this.GrpMain.Controls.Add(this.ShapeContainer1);
            this.GrpMain.Location = new System.Drawing.Point(7, 53);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(606, 451);
            this.GrpMain.TabIndex = 2;
            this.GrpMain.TabStop = false;
            // 
            // lblBalanceAmount
            // 
            this.lblBalanceAmount.AutoSize = true;
            this.lblBalanceAmount.Location = new System.Drawing.Point(13, 396);
            this.lblBalanceAmount.Name = "lblBalanceAmount";
            this.lblBalanceAmount.Size = new System.Drawing.Size(76, 13);
            this.lblBalanceAmount.TabIndex = 1031;
            this.lblBalanceAmount.Text = "Balance Amt : ";
            // 
            // lblDeductEndDate
            // 
            this.lblDeductEndDate.AutoSize = true;
            this.lblDeductEndDate.Location = new System.Drawing.Point(451, 178);
            this.lblDeductEndDate.Name = "lblDeductEndDate";
            this.lblDeductEndDate.Size = new System.Drawing.Size(35, 13);
            this.lblDeductEndDate.TabIndex = 1030;
            this.lblDeductEndDate.Text = "label3";
            // 
            // chkClosed
            // 
            this.chkClosed.AutoSize = true;
            this.chkClosed.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkClosed.Location = new System.Drawing.Point(220, 176);
            this.chkClosed.Name = "chkClosed";
            this.chkClosed.Size = new System.Drawing.Size(105, 17);
            this.chkClosed.TabIndex = 5;
            this.chkClosed.TabStop = false;
            this.chkClosed.Text = "Closed/Write-Off";
            this.chkClosed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkClosed.UseVisualStyleBackColor = true;
            this.chkClosed.CheckedChanged += new System.EventHandler(this.chkClosed_CheckedChanged);
            // 
            // dgvInstallmentDetails
            // 
            this.dgvInstallmentDetails.AllowUserToAddRows = false;
            this.dgvInstallmentDetails.AllowUserToDeleteRows = false;
            this.dgvInstallmentDetails.AllowUserToResizeColumns = false;
            this.dgvInstallmentDetails.AllowUserToResizeRows = false;
            this.dgvInstallmentDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvInstallmentDetails.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInstallmentDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInstallmentDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInstallmentDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LoanID,
            this.InstallmentNo,
            this.InstallmentDate,
            this.Amount,
            this.InterestAmount,
            this.IsPaid,
            this.AmountDecimal});
            this.dgvInstallmentDetails.Location = new System.Drawing.Point(16, 214);
            this.dgvInstallmentDetails.Name = "dgvInstallmentDetails";
            this.dgvInstallmentDetails.RowHeadersVisible = false;
            this.dgvInstallmentDetails.Size = new System.Drawing.Size(578, 155);
            this.dgvInstallmentDetails.TabIndex = 1029;
            this.dgvInstallmentDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstallmentDetails_CellValueChanged);
            this.dgvInstallmentDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstallmentDetails_CellEndEdit);
            this.dgvInstallmentDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvInstallmentDetails_EditingControlShowing);
            this.dgvInstallmentDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvInstallmentDetails_CurrentCellDirtyStateChanged);
            // 
            // LoanID
            // 
            this.LoanID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LoanID.DataPropertyName = "LoanID";
            this.LoanID.HeaderText = "LoanID";
            this.LoanID.Name = "LoanID";
            this.LoanID.ReadOnly = true;
            this.LoanID.Visible = false;
            // 
            // InstallmentNo
            // 
            this.InstallmentNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.InstallmentNo.DataPropertyName = "InstallmentNo";
            this.InstallmentNo.FillWeight = 178.7234F;
            this.InstallmentNo.HeaderText = "Installment No";
            this.InstallmentNo.MinimumWidth = 200;
            this.InstallmentNo.Name = "InstallmentNo";
            this.InstallmentNo.ReadOnly = true;
            // 
            // InstallmentDate
            // 
            this.InstallmentDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.InstallmentDate.DataPropertyName = "InstallmentDate";
            dataGridViewCellStyle2.Format = "dd MMM yyyy";
            this.InstallmentDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.InstallmentDate.HeaderText = "Installment Date";
            this.InstallmentDate.Name = "InstallmentDate";
            this.InstallmentDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Amount
            // 
            this.Amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle3;
            this.Amount.HeaderText = "Amount";
            this.Amount.MaxInputLength = 12;
            this.Amount.MinimumWidth = 150;
            this.Amount.Name = "Amount";
            this.Amount.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // InterestAmount
            // 
            this.InterestAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.InterestAmount.DataPropertyName = "InterestAmount";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.InterestAmount.DefaultCellStyle = dataGridViewCellStyle4;
            this.InterestAmount.HeaderText = "InterestAmount";
            this.InterestAmount.MaxInputLength = 12;
            this.InterestAmount.Name = "InterestAmount";
            this.InterestAmount.ReadOnly = true;
            this.InterestAmount.Visible = false;
            // 
            // IsPaid
            // 
            this.IsPaid.HeaderText = "IsPaid";
            this.IsPaid.Name = "IsPaid";
            this.IsPaid.Visible = false;
            // 
            // AmountDecimal
            // 
            this.AmountDecimal.HeaderText = "AmountDecimal";
            this.AmountDecimal.Name = "AmountDecimal";
            this.AmountDecimal.Visible = false;
            // 
            // cboCurrency
            // 
            this.cboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCurrency.BackColor = System.Drawing.SystemColors.Info;
            this.cboCurrency.DropDownHeight = 134;
            this.cboCurrency.Enabled = false;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.IntegralHeight = false;
            this.cboCurrency.Location = new System.Drawing.Point(105, 95);
            this.cboCurrency.MaxDropDownItems = 10;
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(220, 21);
            this.cboCurrency.TabIndex = 1;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            this.cboCurrency.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCurrency_KeyPress);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(3, -1);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(78, 13);
            this.Label4.TabIndex = 1006;
            this.Label4.Text = "Loan Details";
            // 
            // Lblbasicpay
            // 
            this.Lblbasicpay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lblbasicpay.Location = new System.Drawing.Point(209, 71);
            this.Lblbasicpay.Name = "Lblbasicpay";
            this.Lblbasicpay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Lblbasicpay.Size = new System.Drawing.Size(116, 15);
            this.Lblbasicpay.TabIndex = 1002;
            this.Lblbasicpay.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(3, 198);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(143, 13);
            this.Label2.TabIndex = 139;
            this.Label2.Text = "Loan Installment Details";
            // 
            // LblPayClassification
            // 
            this.LblPayClassification.AutoSize = true;
            this.LblPayClassification.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPayClassification.Location = new System.Drawing.Point(151, 72);
            this.LblPayClassification.Name = "LblPayClassification";
            this.LblPayClassification.Size = new System.Drawing.Size(0, 13);
            this.LblPayClassification.TabIndex = 2;
            // 
            // LblPayment
            // 
            this.LblPayment.AutoSize = true;
            this.LblPayment.Location = new System.Drawing.Point(13, 72);
            this.LblPayment.Name = "LblPayment";
            this.LblPayment.Size = new System.Drawing.Size(142, 13);
            this.LblPayment.TabIndex = 137;
            this.LblPayment.Text = "Payment Mode/Gross Salary";
            // 
            // txtNoOfInstallments
            // 
            this.txtNoOfInstallments.BackColor = System.Drawing.SystemColors.Info;
            this.txtNoOfInstallments.Location = new System.Drawing.Point(450, 97);
            this.txtNoOfInstallments.MaxLength = 2;
            this.txtNoOfInstallments.Name = "txtNoOfInstallments";
            this.txtNoOfInstallments.Size = new System.Drawing.Size(144, 20);
            this.txtNoOfInstallments.TabIndex = 9;
            this.txtNoOfInstallments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoOfInstallments.TextChanged += new System.EventHandler(this.txtNoOfInstallments_TextChanged);
            this.txtNoOfInstallments.Leave += new System.EventHandler(this.txtNoOfInstallments_Leave);
            this.txtNoOfInstallments.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoOfInstallments_KeyPress);
            // 
            // dtpDeductStartDate
            // 
            this.dtpDeductStartDate.CustomFormat = "MMM-yyyy";
            this.dtpDeductStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDeductStartDate.Location = new System.Drawing.Point(450, 71);
            this.dtpDeductStartDate.Name = "dtpDeductStartDate";
            this.dtpDeductStartDate.Size = new System.Drawing.Size(106, 20);
            this.dtpDeductStartDate.TabIndex = 8;
            this.dtpDeductStartDate.ValueChanged += new System.EventHandler(this.dtpDeductStartDate_ValueChanged);
            // 
            // txtInterestRate
            // 
            this.txtInterestRate.BackColor = System.Drawing.SystemColors.Info;
            this.txtInterestRate.Location = new System.Drawing.Point(450, 45);
            this.txtInterestRate.MaxLength = 5;
            this.txtInterestRate.Name = "txtInterestRate";
            this.txtInterestRate.Size = new System.Drawing.Size(144, 20);
            this.txtInterestRate.TabIndex = 7;
            this.txtInterestRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInterestRate.TextChanged += new System.EventHandler(this.txtInterestRate_TextChanged);
            this.txtInterestRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterestRate_KeyPress);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(189, 380);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(64, 13);
            this.Label1.TabIndex = 985;
            this.Label1.Text = "Other Info";
            // 
            // LblDOJ
            // 
            this.LblDOJ.AutoSize = true;
            this.LblDOJ.Location = new System.Drawing.Point(105, 48);
            this.LblDOJ.Name = "LblDOJ";
            this.LblDOJ.Size = new System.Drawing.Size(0, 13);
            this.LblDOJ.TabIndex = 1;
            // 
            // LblJoinDate
            // 
            this.LblJoinDate.AutoSize = true;
            this.LblJoinDate.Location = new System.Drawing.Point(13, 48);
            this.LblJoinDate.Name = "LblJoinDate";
            this.LblJoinDate.Size = new System.Drawing.Size(66, 13);
            this.LblJoinDate.TabIndex = 997;
            this.LblJoinDate.Text = "Joining Date";
            // 
            // TextBoxNumricInstalment
            // 
            this.TextBoxNumricInstalment.Location = new System.Drawing.Point(644, 214);
            this.TextBoxNumricInstalment.Name = "TextBoxNumricInstalment";
            this.TextBoxNumricInstalment.Size = new System.Drawing.Size(22, 20);
            this.TextBoxNumricInstalment.TabIndex = 998;
            this.TextBoxNumricInstalment.Visible = false;
            // 
            // TextboxNumeric
            // 
            this.TextboxNumeric.Location = new System.Drawing.Point(644, 159);
            this.TextboxNumeric.Name = "TextboxNumeric";
            this.TextboxNumeric.Size = new System.Drawing.Size(12, 20);
            this.TextboxNumeric.TabIndex = 999;
            this.TextboxNumeric.Visible = false;
            // 
            // btnLoanType
            // 
            this.btnLoanType.Location = new System.Drawing.Point(293, 146);
            this.btnLoanType.Name = "btnLoanType";
            this.btnLoanType.Size = new System.Drawing.Size(32, 23);
            this.btnLoanType.TabIndex = 4;
            this.btnLoanType.Text = "...";
            this.btnLoanType.UseVisualStyleBackColor = true;
            this.btnLoanType.Click += new System.EventHandler(this.btnLoanType_Click);
            // 
            // cboInterestType
            // 
            this.cboInterestType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInterestType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInterestType.BackColor = System.Drawing.SystemColors.Info;
            this.cboInterestType.DropDownHeight = 134;
            this.cboInterestType.FormattingEnabled = true;
            this.cboInterestType.IntegralHeight = false;
            this.cboInterestType.Location = new System.Drawing.Point(450, 18);
            this.cboInterestType.Name = "cboInterestType";
            this.cboInterestType.Size = new System.Drawing.Size(144, 21);
            this.cboInterestType.TabIndex = 6;
            this.cboInterestType.SelectedIndexChanged += new System.EventHandler(this.cboInterestType_SelectedIndexChanged);
            this.cboInterestType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboInterestType_KeyPress);
            // 
            // cboLoanType
            // 
            this.cboLoanType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLoanType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLoanType.BackColor = System.Drawing.SystemColors.Info;
            this.cboLoanType.DropDownHeight = 134;
            this.cboLoanType.FormattingEnabled = true;
            this.cboLoanType.IntegralHeight = false;
            this.cboLoanType.Location = new System.Drawing.Point(105, 147);
            this.cboLoanType.Name = "cboLoanType";
            this.cboLoanType.Size = new System.Drawing.Size(182, 21);
            this.cboLoanType.TabIndex = 3;
            this.cboLoanType.SelectedIndexChanged += new System.EventHandler(this.cboLoanType_SelectedIndexChanged);
            this.cboLoanType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboLoanType_KeyPress);
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(105, 18);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(220, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboEmployee_KeyPress);
            // 
            // txtLoanNumber
            // 
            this.txtLoanNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtLoanNumber.Location = new System.Drawing.Point(105, 121);
            this.txtLoanNumber.MaxLength = 20;
            this.txtLoanNumber.Name = "txtLoanNumber";
            this.txtLoanNumber.Size = new System.Drawing.Size(220, 20);
            this.txtLoanNumber.TabIndex = 2;
            this.txtLoanNumber.TextChanged += new System.EventHandler(this.txtLoanNumber_TextChanged);
            // 
            // dtpLoanDate
            // 
            this.dtpLoanDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpLoanDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLoanDate.Location = new System.Drawing.Point(105, 174);
            this.dtpLoanDate.Name = "dtpLoanDate";
            this.dtpLoanDate.Size = new System.Drawing.Size(107, 20);
            this.dtpLoanDate.TabIndex = 4;
            this.dtpLoanDate.ValueChanged += new System.EventHandler(this.dtpLoanDate_ValueChanged);
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtAmount.Location = new System.Drawing.Point(450, 123);
            this.txtAmount.MaxLength = 12;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(144, 20);
            this.txtAmount.TabIndex = 10;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            this.txtAmount.Leave += new System.EventHandler(this.txtAmount_Leave);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // txtPaymentAmount
            // 
            this.txtPaymentAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtPaymentAmount.Enabled = false;
            this.txtPaymentAmount.Location = new System.Drawing.Point(450, 149);
            this.txtPaymentAmount.MaxLength = 14;
            this.txtPaymentAmount.Name = "txtPaymentAmount";
            this.txtPaymentAmount.Size = new System.Drawing.Size(144, 20);
            this.txtPaymentAmount.TabIndex = 11;
            this.txtPaymentAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPaymentAmount.TextChanged += new System.EventHandler(this.txtPaymentAmount_TextChanged);
            // 
            // txtReason
            // 
            this.txtReason.Location = new System.Drawing.Point(192, 396);
            this.txtReason.MaxLength = 500;
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReason.Size = new System.Drawing.Size(402, 47);
            this.txtReason.TabIndex = 24;
            this.txtReason.TextChanged += new System.EventHandler(this.txtReason_TextChanged);
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.AutoScroll = true;
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape2,
            this.LineShape1});
            this.ShapeContainer1.Size = new System.Drawing.Size(600, 432);
            this.ShapeContainer1.TabIndex = 136;
            this.ShapeContainer1.TabStop = false;
            // 
            // LineShape2
            // 
            this.LineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape2.Name = "LineShape2";
            this.LineShape2.X1 = 26;
            this.LineShape2.X2 = 592;
            this.LineShape2.Y1 = 190;
            this.LineShape2.Y2 = 190;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 198;
            this.LineShape1.X2 = 592;
            this.LineShape1.Y1 = 371;
            this.LineShape1.Y2 = 369;
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Location = new System.Drawing.Point(0, 543);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(620, 22);
            this.StatusStrip1.TabIndex = 62;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(7, 512);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 63;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(458, 512);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 64;
            this.btnOK.Text = "&Ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(539, 512);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 65;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "LoanID";
            this.dataGridViewTextBoxColumn1.HeaderText = "LoanID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "InstalmentNo";
            this.dataGridViewTextBoxColumn2.FillWeight = 178.7234F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Installment No";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 200;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Amount";
            this.dataGridViewTextBoxColumn3.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "InterestAmount";
            this.dataGridViewTextBoxColumn4.HeaderText = "InterestAmount";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // errLoan
            // 
            this.errLoan.ContainerControl = this;
            this.errLoan.RightToLeft = true;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(620, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 90;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Tooltip = "Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // FrmEmployeeLoan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 565);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.LoanBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEmployeeLoan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loans";
            this.Load += new System.EventHandler(this.FrmEmployeeLoan_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEmployeeLoan_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmEmployeeLoan_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.LoanBindingNavigator)).EndInit();
            this.LoanBindingNavigator.ResumeLayout(false);
            this.LoanBindingNavigator.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstallmentDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errLoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator LoanBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.ComboBox cboCurrency;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Lblbasicpay;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label LblPayClassification;
        internal System.Windows.Forms.Label LblPayment;
        internal System.Windows.Forms.TextBox txtNoOfInstallments;
        internal System.Windows.Forms.DateTimePicker dtpDeductStartDate;
        internal System.Windows.Forms.TextBox txtInterestRate;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label LblDOJ;
        internal System.Windows.Forms.Label LblJoinDate;
        internal System.Windows.Forms.TextBox TextBoxNumricInstalment;
        internal System.Windows.Forms.TextBox TextboxNumeric;
        internal System.Windows.Forms.Button btnLoanType;
        internal System.Windows.Forms.ComboBox cboInterestType;
        internal System.Windows.Forms.ComboBox cboLoanType;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.TextBox txtLoanNumber;
        internal System.Windows.Forms.DateTimePicker dtpLoanDate;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.TextBox txtPaymentAmount;
        internal System.Windows.Forms.TextBox txtReason;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.DataGridView dgvInstallmentDetails;
        internal System.Windows.Forms.ErrorProvider errLoan;
        private System.Windows.Forms.CheckBox chkClosed;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.Label lblDeductEndDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanID;
        private System.Windows.Forms.DataGridViewTextBoxColumn InstallmentNo;
        private DemoClsDataGridview.CalendarColumn InstallmentDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn InterestAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsPaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountDecimal;
        private System.Windows.Forms.Label lblBalanceAmount;
    }
}