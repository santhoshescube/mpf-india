﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
/*
 * Created By       : Tijo
 * Created Date     : 19 Aug 2013
 * Purpose          : For Loan Repayment
*/
namespace MyPayfriend
{
    public partial class FrmLoanRepayment : Form
    {
        clsBLLLoanRepayment MobjclsBLLLoanRepayment = new clsBLLLoanRepayment();
        long TotalRecordCnt, CurrentRecCnt;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool mblnAddStatus = true;
        private clsMessage objUserMessage = null;
        string strBindingOf = "Of ";

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.LoanRepayment);

                return this.objUserMessage;
            }
        }

        public FrmLoanRepayment()
        {
            InitializeComponent();
            tmrClear.Interval = ClsCommonSettings.TimerInterval;

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.LoanRepayment, this);

            strBindingOf = "من ";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            lblStatusShow.Text = "حالة :";
        }


        private void FrmLoanRepayment_Load(object sender, EventArgs e)
        {
            SetPermissions();
            AddNew(); 
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.LoanRepayment,
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadCombos(int intType)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0)
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    if (mblnAddStatus) // In Add New Mode not showing the Absconding, Expired, Retired, Resigned, Terminated Employees
                    {
                        datTemp = MobjclsBLLLoanRepayment.FillCombos(new string[] { "DISTINCT EM.EmployeeID," +
                            "CASE WHEN ISNULL(EM.EmployeeNumber,'')<> '' THEN EM.EmployeeFullNameArb + ' - ' + EM.EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EM.EmployeeFullNameArb + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster EM INNER JOIN Loan L ON EM.EmployeeID = L.EmployeeID INNER JOIN CompanyMaster C ON EM.CompanyID=C.CompanyID", "" +
                            "EM.WorkStatusID >= 6 AND L.Closed <> 1 AND EM.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") ORDER BY EmployeeName" });
                    }
                    else
                    {
                        datTemp = MobjclsBLLLoanRepayment.FillCombos(new string[] { "EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullNameArb + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullNameArb + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "1=1 ORDER BY EmployeeName" });
                    }
                }
                else
                {
                    if (mblnAddStatus) // In Add New Mode not showing the Absconding, Expired, Retired, Resigned, Terminated Employees
                    {
                        datTemp = MobjclsBLLLoanRepayment.FillCombos(new string[] { "DISTINCT EM.EmployeeID," +
                            "CASE WHEN ISNULL(EM.EmployeeNumber,'')<> '' THEN EM.EmployeeFullName + ' - ' + EM.EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EM.EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster EM INNER JOIN Loan L ON EM.EmployeeID = L.EmployeeID INNER JOIN CompanyMaster C ON EM.CompanyID=C.CompanyID", "" +
                            "EM.WorkStatusID >= 6 AND L.Closed <> 1 AND EM.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") ORDER BY EmployeeName" });
                    }
                    else
                    {
                        datTemp = MobjclsBLLLoanRepayment.FillCombos(new string[] { "EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "1=1 ORDER BY EmployeeName" });
                    }
                }
                cboEmployee.DataSource = datTemp;
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DisplayMember = "EmployeeName";
            }

            if (intType == 1)
            {
                if (mblnAddStatus) // In Add New Mode not showing Closed Loans
                {
                    datTemp = MobjclsBLLLoanRepayment.FillCombos(new string[] { "LoanID,LoanNumber", "Loan", "" +
                        "EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + " AND Closed = 0" });
                }
                else
                {
                    datTemp = MobjclsBLLLoanRepayment.FillCombos(new string[] { "LoanID,LoanNumber", "Loan", "" +
                        "EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + "" });
                }

                cboLoanNumber.DataSource = datTemp;
                cboLoanNumber.ValueMember = "LoanID";
                cboLoanNumber.DisplayMember = "LoanNumber";
            }
        }
  
        private void AddNew()
        {
            mblnAddStatus = true;
            cboEmployee.Tag = 0;
            LoadCombos(0);
            cboEmployee.SelectedIndex = cboLoanNumber.SelectedIndex = -1;
            cboLoanNumber.Text = "";
            txtPrincipleAmount.Text = txtRemarks.Text = lblCurrency.Text = txtSearch.Text = "";
            lblLoanDate.Text = lblTotalLoanAmount.Text = lblRepaidAmount.Text = lblBalanceAmountDue.Text = "";

            if (ClsCommonSettings.IsArabicView)
                lblDeductedFromSalary.Text = "يثيعؤفثي بخقة سشمشقغ : ىخ";
            else
                lblDeductedFromSalary.Text = "Deducted From Salary : No";

            dtpDate.Value = ClsCommonSettings.GetServerDate();
            dtpDate.Tag = ClsCommonSettings.GetServerDate();
            dgvInstallments.Rows.Clear();
            chkLoanClosed.Checked = chkSelectAll.Checked = false;
            txtPrincipleAmount.Tag = 0;
            lblEmployee.Tag = "";
            lblstatus.Text = "";
            tmrClear.Enabled = false;
            errValidate.Clear();

            SetAutoCompleteList();
            RecordCount();
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt + 1).ToString();
            BindingNavigatorPositionItem.Text = (TotalRecordCnt + 1).ToString();

            GrpMain.Enabled = cboEmployee.Enabled = cboLoanNumber.Enabled = true;            

            btnClear.Enabled = true;
            btnAddNew.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false;

            cboEmployee.Focus();
        }

        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "", "LoanRepayment");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }

        private void RecordCount()
        {
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.strSearchKey = txtSearch.Text.Trim(); // Is any search text filteration will be taken that also
            TotalRecordCnt = MobjclsBLLLoanRepayment.GetRecordCount();
            CurrentRecCnt = TotalRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }

            BindingEnableDisable();
        }

        private void BindingEnableDisable() // Navigator visibility settings
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboEmployee.SelectedIndex >= 0)
            {
                LoadCombos(1);

                DataTable datTemp = MobjclsBLLLoanRepayment.FillCombos(new string[] { "CONVERT(VARCHAR,EM.DateofJoining,106) AS DateofJoining," +
                        "CR.Code AS CurrencyName,CR.CurrencyNameArb", "EmployeeMaster EM INNER JOIN CompanyMaster CM ON EM.CompanyID = CM.CompanyID " +
                        "INNER JOIN CurrencyReference CR ON CM.CurrencyId = CR.CurrencyID", "" +
                        "EM.EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                    {
                        lblEmployee.Tag = datTemp.Rows[0]["DateofJoining"].ToString(); // For Checking Employee Date of Joining

                        if (mblnAddStatus)
                        {
                            if (ClsCommonSettings.IsArabicView)
                                lblCurrency.Text = datTemp.Rows[0]["CurrencyNameArb"].ToStringCustom();
                            else
                                lblCurrency.Text = datTemp.Rows[0]["CurrencyName"].ToStringCustom();
                        }
                    }
                }
            }
        }

        private void cboLoanNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboLoanNumber.SelectedIndex >= 0)
                getLoanDetails();
        }

        private void getLoanDetails()
        {
            int intLoanID = cboLoanNumber.SelectedValue.ToInt32();
            DataTable datTemp = MobjclsBLLLoanRepayment.getLoanDetails(intLoanID);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    lblLoanDate.Text = datTemp.Rows[0]["LoanDate"].ToString();
                    lblTotalLoanAmount.Text = datTemp.Rows[0]["PaymentAmount"].ToString();
                    lblRepaidAmount.Text = datTemp.Rows[0]["RepaidAmount"].ToString();
                    lblBalanceAmountDue.Text = datTemp.Rows[0]["BalanceAmount"].ToString();
                    chkLoanClosed.Checked = Convert.ToBoolean(datTemp.Rows[0]["Closed"].ToString());
                    dtpDate.Tag = MobjclsBLLLoanRepayment.getLastLoanRepaymentDate(0, intLoanID);
                }
            }

            FillGrid();
        }

        private void FillGrid() // to display the loan installment details
        {
            dgvInstallments.Rows.Clear();
            DataTable datTemp = MobjclsBLLLoanRepayment.getLoanInstallmentDetails(cboLoanNumber.SelectedValue.ToInt32(), cboEmployee.Tag.ToInt64());

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
                    {
                        dgvInstallments.Rows.Add();
                        dgvInstallments.Rows[iCounter].Cells["IsChecked"].Value = Convert.ToBoolean(datTemp.Rows[iCounter]["IsChecked"].ToInt32());
                        dgvInstallments.Rows[iCounter].Cells["InstalmentNo"].Value = datTemp.Rows[iCounter]["InstalmentNo"].ToString();
                        dgvInstallments.Rows[iCounter].Cells["InstalmentNoAndDate"].Value = datTemp.Rows[iCounter]["InstalmentNoAndDate"].ToString();
                        dgvInstallments.Rows[iCounter].Cells["BalanceAmount"].Value = datTemp.Rows[iCounter]["BalanceAmount"].ToDecimal();

                        decimal decPrincipleAmount = datTemp.Rows[iCounter]["PrincipleAmount"].ToDecimal();

                        if (decPrincipleAmount > 0)
                        {
                            dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value = 
                                dgvInstallments.Rows[iCounter].Cells["EnteredAmount"].Value = decPrincipleAmount;
                        }

                        dgvInstallments.Rows[iCounter].Cells["RepaidAmount"].Value = datTemp.Rows[iCounter]["RepaidAmount"].ToDecimal();
                        dgvInstallments.Rows[iCounter].Cells["TotalPrincipleAmount"].Value = datTemp.Rows[iCounter]["TotalPrincipleAmount"].ToDecimal();

                        if (ClsCommonSettings.IsAmountRoundByZero)
                        {
                            dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value = dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value.ToDecimal().ToString("F" + 0);
                        }


                    }
                }
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            SaveLoanRepayment();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveLoanRepayment())
            {
                btnSave.Enabled = false;
                this.Close();
            }
        }

        private bool SaveLoanRepayment()
        {
            if (FormValidation())
            {
                bool blnSave = false;

                if (chkLoanClosed.Checked)
                {
                    if (this.UserMessage.ShowMessage(18111) == true) // Save
                        blnSave = true;
                }
                else
                {
                    if (mblnAddStatus)
                    {
                        if (this.UserMessage.ShowMessage(1) == true) // Save
                            blnSave = true;
                    }
                    else
                    {
                        if (this.UserMessage.ShowMessage(3) == true) // Update
                            blnSave = true;
                    }
                }

                if (blnSave)
                {
                    FillParameters(); // fill parameters for saving the content
                    DataTable datTemp = FillParametersDetails();

                    long lngRetValue = MobjclsBLLLoanRepayment.SaveLoanRepaymentMaster(datTemp);

                    if (lngRetValue > 0)
                    {
                        if (ClsCommonSettings.ThirdPartyIntegration)
                            new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpDate.Value);

                        if (mblnAddStatus)
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(2);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(2); // Saved Successfully
                        }
                        else
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(21);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(21); // Updated Successfully
                        }

                        chkSelectAll.Checked = false;
                        chkSelectAll.Focus();
                        DisplayLoanRepaymentAgainstRecordID(lngRetValue); // to display the current saved or updated content
                        SetAutoCompleteList();
                        
                        return true;
                    }
                }
            }

            return false;
        }

        private bool FormValidation()
        {
            txtPrincipleAmount.Focus();
            errValidate.Clear();

            if (cboEmployee.SelectedIndex == -1) // if employee is not selected properly
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(9124);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(9124, cboEmployee, errValidate);
                return false;
            }

            if (MobjclsBLLLoanRepayment.CheckSalaryProcessedAgainstEmployee(cboEmployee.SelectedValue.ToInt64()) == 1) // Check Salary is Processed
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(18112);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(18112);
                return false;
            }

            if (MobjclsBLLLoanRepayment.CheckSettlementProcessed(cboEmployee.SelectedValue.ToInt64()) == 1) // Check Settlement
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(20012);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(20012);
                return false;
            }

            if (cboLoanNumber.SelectedIndex == -1) // if Loan Number is not selected properly
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(1811);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1811, cboLoanNumber, errValidate);
                return false;
            }

            // if principle Amount should not be zero
            if (txtPrincipleAmount.Text.ToDecimal() == 0)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(1816);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1816, txtPrincipleAmount, errValidate);
                return false;
            }

            // if principle Amount should not be greater than Balance Due Amount
            if (txtPrincipleAmount.Text.ToDecimal() > txtPrincipleAmount.Tag.ToDecimal() + lblBalanceAmountDue.Text.ToDecimal())
            {
               //decimal x = txtPrincipleAmount.Tag.ToDecimal() + lblBalanceAmountDue.Text.ToDecimal();
                lblstatus.Text = this.UserMessage.GetMessageByCode(1814);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1814, txtPrincipleAmount, errValidate);
                return false;
            }

            if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate()) // if Loan Repayment date is greater than current date
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(1813);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1813, dtpDate, errValidate);
                return false;
            }

            if (dtpDate.Value.Date < lblLoanDate.Text.ToDateTime().Date) // if Loan Repayment date is less than Loan Date
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(1815);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1815, dtpDate, errValidate);
                return false;
            }

            if (dtpDate.Value.Date < (dtpDate.Tag.ToString()).ToDateTime().Date) // if Loan Repayment date should be greater than last Loan repayment Date
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(18110);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(18110, dtpDate, errValidate);
                return false;
            }

            if (dgvInstallments.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dgvInstallments.Rows[0].Cells["IsChecked"].Value) == false) // if you are not selecting the first row
                {
                    lblstatus.Text = this.UserMessage.GetMessageByCode(1818);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(1818);
                    return false;
                }

                for (int iCounter = 0; iCounter <= dgvInstallments.Rows.Count - 1; iCounter++)
                {
                    if (Convert.ToBoolean(dgvInstallments.Rows[iCounter].Cells["IsChecked"].Value))
                    {
                        // Principle Amount should greater than total principle amount.
                        if (dgvInstallments.Rows[iCounter].Cells["EnteredAmount"].Value.ToDecimal() !=
                            dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value.ToDecimal())
                        {
                            if (dgvInstallments.Rows[iCounter].Cells["BalanceAmount"].Value.ToDecimal() <
                                    (dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value.ToDecimal() -
                                    dgvInstallments.Rows[iCounter].Cells["EnteredAmount"].Value.ToDecimal()))
                            {
                                //lblstatus.Text = this.UserMessage.GetMessageByCode(1817);
                                //tmrClear.Enabled = true;
                                //this.UserMessage.ShowMessage(1817);

                                lblstatus.Text = "Amount should not greater than " +
                                    (dgvInstallments.Rows[iCounter].Cells["BalanceAmount"].Value.ToDecimal() +
                                    dgvInstallments.Rows[iCounter].Cells["EnteredAmount"].Value.ToDecimal()).ToStringCustom() + ".";
                                tmrClear.Enabled = true;
                                MessageBox.Show(lblstatus.Text, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }

                        // Please close the previous installment(s) properly.
                        if (iCounter > 0)
                        {
                            // if Current row principle amount is greater than zero
                            if (dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value.ToDecimal() > 0)
                            {                                
                                if (dgvInstallments.Rows[iCounter].Cells["EnteredAmount"].Value.ToDecimal() !=
                                dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value.ToDecimal())
                                {
                                    if (((dgvInstallments.Rows[iCounter - 1].Cells["RepaidAmount"].Value.ToDecimal() - 
                                        dgvInstallments.Rows[iCounter - 1].Cells["EnteredAmount"].Value.ToDecimal()) +
                                        dgvInstallments.Rows[iCounter - 1].Cells["PrincipleAmount"].Value.ToDecimal()) !=
                                        dgvInstallments.Rows[iCounter - 1].Cells["TotalPrincipleAmount"].Value.ToDecimal())
                                    {
                                        lblstatus.Text = this.UserMessage.GetMessageByCode(1819);
                                        tmrClear.Enabled = true;
                                        this.UserMessage.ShowMessage(1819);
                                        return false;
                                    }
                                }
                            }
                        }
                        // End
                    }
                }
            }

            return true;
        }

        private void FillParameters()
        {
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.lngRepaymentID = cboEmployee.Tag.ToInt64();
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.lngEmployeeID = cboEmployee.SelectedValue.ToInt64();
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.intLoanID = cboLoanNumber.SelectedValue.ToInt32();
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.dtDate = (dtpDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.decAmount = txtPrincipleAmount.Text.ToDecimal();
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.decCompanyAmount = MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.decAmount;
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.strRemarks = txtRemarks.Text;
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.blnIsClosed = chkLoanClosed.Checked;

            if (mblnAddStatus)
            {
                DataTable datTemp = MobjclsBLLLoanRepayment.FillCombos(new string[] { "CM.CurrencyId", "EmployeeMaster EM " +
                    "INNER JOIN CompanyMaster CM ON EM.CompanyID = CM.CompanyID", "EM.EmployeeID = " + MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.lngEmployeeID + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.intCurrencyID = datTemp.Rows[0]["CurrencyId"].ToInt32();
                }
            }
        }

        private DataTable FillParametersDetails()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("InstalmentNo");
            datTemp.Columns.Add("PrincipleAmount");

            for (int iCounter = 0; iCounter <= dgvInstallments.Rows.Count - 1; iCounter++)
            {
                if (Convert.ToBoolean(dgvInstallments.Rows[iCounter].Cells["IsChecked"].Value) && 
                    dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value.ToDecimal() > 0)
                {
                    DataRow dr = datTemp.NewRow();
                    dr["InstalmentNo"] = dgvInstallments.Rows[iCounter].Cells["InstalmentNo"].Value.ToInt32();
                    dr["PrincipleAmount"] = dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value.ToDecimal();
                    datTemp.Rows.Add(dr);
                }
            }

            return datTemp;
        }

        private void DisplayLoanRepaymentAgainstRecordID(long lngRetValue)
        {
            RecordCount();
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt).ToString();
            BindingNavigatorPositionItem.Text = MobjclsBLLLoanRepayment.getRecordRowNumber(lngRetValue).ToString(); // get record no. against the Repayment ID

            DisplayLoanRepaymentDetails();
            BindingEnableDisable();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.UserMessage.ShowMessage(13) == true)
            {
                MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.lngRepaymentID = cboEmployee.Tag.ToInt64();
                MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.intLoanID = cboLoanNumber.SelectedValue.ToInt32();
                if (MobjclsBLLLoanRepayment.DeleteLoanRepaymentMaster() == true)
                {
                    if (ClsCommonSettings.ThirdPartyIntegration)
                        new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpDate.Value);

                    lblstatus.Text = this.UserMessage.GetMessageByCode(4); // Deleted Successfully
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(4);
                    AddNew();
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboEmployee.Tag.ToInt64() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = cboEmployee.Tag.ToInt64();

                DataTable datTemp = MobjclsBLLLoanRepayment.FillCombos(new string[] { "TOP 1 CompanyID AS CompanyID", "EmployeeMaster", "" +
                    "EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        ObjViewer.PintCompany = datTemp.Rows[0]["CompanyID"].ToInt32();
                }

                ObjViewer.PiFormID = (int)FormID.LoanRepayment;
                ObjViewer.ShowDialog();
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboEmployee.Tag.ToInt64() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "LoanRepayment";
                        ObjEmailPopUp.EmailFormType = EmailFormID.LoanRepayment;
                        ObjEmailPopUp.EmailSource = MobjclsBLLLoanRepayment.GetPrintLoanRepaymentDetails(cboEmployee.Tag.ToInt64());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch { }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "LoanRepayment";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayLoanRepaymentDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();

            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
                BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayLoanRepaymentDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();

            if (TotalRecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                else
                    BindingNavigatorPositionItem.Text = "1";
            }

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayLoanRepaymentDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                BindingNavigatorCountItem.Text = strBindingOf + "1";
            }

            DisplayLoanRepaymentDetails();
            BindingEnableDisable();
        }

        private void DisplayLoanRepaymentDetails()
        {
            MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.strSearchKey = txtSearch.Text.Trim();
            DataTable datTemp = MobjclsBLLLoanRepayment.DisplayLoanRepaymentMaster(BindingNavigatorPositionItem.Text.ToInt64()); // Get Loan Repayment Details

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    mblnAddStatus = false;
                    cboEmployee.Enabled = cboLoanNumber.Enabled = btnClear.Enabled = false;
                    LoadCombos(0);

                    long lngRepaymentID = datTemp.Rows[0]["RepaymentID"].ToInt64();
                    cboEmployee.Tag = lngRepaymentID;
                    cboEmployee.SelectedValue = datTemp.Rows[0]["EmployeeID"].ToInt64();
                    int intLoanID = datTemp.Rows[0]["LoanID"].ToInt32();
                    cboLoanNumber.SelectedValue = intLoanID;
                    txtPrincipleAmount.Text = datTemp.Rows[0]["Amount"].ToString();
                    txtPrincipleAmount.Tag = txtPrincipleAmount.Text; // For Maximum Amount Validation

                    if (ClsCommonSettings.IsArabicView)
                        lblCurrency.Text = datTemp.Rows[0]["CurrencyNameArb"].ToString();
                    else
                        lblCurrency.Text = datTemp.Rows[0]["CurrencyName"].ToString();

                    dtpDate.Value = datTemp.Rows[0]["Date"].ToDateTime();
                    dtpDate.Tag = MobjclsBLLLoanRepayment.getLastLoanRepaymentDate(lngRepaymentID, intLoanID);

                    string strTemp = datTemp.Rows[0]["IsFromSalary"].ToString();

                    if (ClsCommonSettings.IsArabicView)
                    {
                        strTemp = strTemp.Replace("Deducted From Salary", "يثيعؤفثي بقخة سشمشقغ");

                        if (strTemp.Contains("Yes"))
                            strTemp = strTemp.Replace("Yes","غثس");

                        if (strTemp.Contains("No"))
                            strTemp = strTemp.Replace("No", "ىخ");
                    }

                    lblDeductedFromSalary.Text = strTemp;

                    txtRemarks.Text = datTemp.Rows[0]["Remarks"].ToString();
                    MobjclsBLLLoanRepayment.PobjclsDTOLoanRepayment.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();

                    // Display Loan Repayment Details
                    FillGrid();
                    ChangeStatus();

                    bool blnIsBlocked = false;

                    // if the details is coming from the salary
                    if (datTemp.Rows[0]["FromSalary"].ToBoolean())
                        blnIsBlocked = true;

                    // If Loan repayment is existing after the selected loan
                    if (MobjclsBLLLoanRepayment.CheckLoanRepaymentExists(lngRepaymentID, intLoanID) == 1)
                        blnIsBlocked = true;

                    // IF Loan is Closed
                    if (chkLoanClosed.Checked)
                        blnIsBlocked = true;

                    if (blnIsBlocked)
                        GrpMain.Enabled = btnDelete.Enabled = false;
                    else
                        GrpMain.Enabled = true;

                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false; // disable in update mode. Enable these controls when edit started
                }
            }
        }

        private void ChangeStatus()
        {
            if (mblnAddStatus)
            {
                btnAddNew.Enabled = false;
                btnClear.Enabled = true;
                btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            }
            else
            {
                btnAddNew.Enabled = MblnAddPermission;
                btnClear.Enabled = false;

                // if employee is settled don't want to update / delete the Loan Repayment details
                if (MobjclsBLLLoanRepayment.CheckEmployeeExistance(cboEmployee.SelectedValue.ToInt64()) == 1)
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
                else
                {
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                    btnDelete.Enabled = MblnDeletePermission;
                }
                // END

                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
            }
            errValidate.Clear();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = "";
            tmrClear.Enabled = false;
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox obj = (ComboBox)sender;
            obj.DroppedDown = false;
        }

        private void ChangeControlStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.txtSearch.Text.Trim() == string.Empty)
                return;
            txtSearch.Text = txtSearch.Text.Trim().Replace("'", "");
            RecordCount(); // get record count

            if (TotalRecordCnt == 0) // No records
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(40);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(40);
                txtSearch.Text = string.Empty;
                btnAddNew_Click(sender, e);
            }
            else if (TotalRecordCnt > 0)
                BindingNavigatorMoveFirstItem_Click(sender, e); // if record found in search then automatically navigate to first item
        }

        private void FrmLoanRepayment_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {
                if (this.UserMessage.ShowMessage(8) == true) // Your changes will be lost. Please confirm if you wish to cancel?
                {
                    MobjclsBLLLoanRepayment = null;
                    objUserMessage = null;
                    e.Cancel = false;
                }
                else
                    e.Cancel = true;
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;

            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
                e.Handled = true;
        }

        private void FrmLoanRepayment_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "LoanRepayment";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (MblnAddPermission)
                            btnAddNew_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (MblnDeletePermission)
                            btnDelete_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (MblnPrintEmailPermission)
                            btnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (MblnPrintEmailPermission)
                            btnEmail_Click(sender, new EventArgs());//Email
                        break;
                }
            }
            catch { }
        }

        private void dgvInstallments_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void dgvInstallments_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            dgvInstallments.EditingControl.KeyPress += new KeyPressEventHandler(EditingControl_KeyPress);
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                ((DataGridViewComboBoxEditingControl)e.Control).BackColor = Color.White;
            }
        }

        void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (dgvInstallments.CurrentCell.OwningColumn.Index == dgvInstallments.Columns["PrincipleAmount"].Index)
            {
                System.Windows.Forms.TextBox txt = (System.Windows.Forms.TextBox)sender;
                if (e.KeyChar == '.' && ClsCommonSettings.IsAmountRoundByZero == true)
                {
                    e.Handled = true;
                }
                else if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '.' && e.KeyChar != (char)Keys.Delete)
                {
                    e.Handled = true;
                }
                else
                {
                    int dotIndex = -1;
                    if (txt.Text.Contains("."))
                    {
                        dotIndex = txt.Text.IndexOf('.');
                    }
                    if (e.KeyChar == '.' && ClsCommonSettings.IsAmountRoundByZero == false)
                    {
                        if (dotIndex != -1 && !txt.SelectedText.Contains("."))
                        {
                            e.Handled = true;
                        }
                    }
                }

                // If Installment is not selected
                if (!(Convert.ToBoolean(dgvInstallments.Rows[dgvInstallments.CurrentRow.Index].Cells["IsChecked"].Value)))
                    e.Handled = true;
            }
        }

        private void dgvInstallments_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvInstallments.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgvInstallments_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            CalcTotal();
        }

        private void CalcTotal()
        {
            decimal decTotalPrincipleAmount = 0;
            decimal decBalanceAmount = 0;
            for (int iCounter = 0; iCounter <= dgvInstallments.Rows.Count - 1; iCounter++)
            {
                decBalanceAmount += dgvInstallments.Rows[iCounter].Cells["BalanceAmount"].Value.ToDecimal();
                if (Convert.ToBoolean(dgvInstallments.Rows[iCounter].Cells["IsChecked"].Value))
                    decTotalPrincipleAmount += dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value.ToDecimal();
                else
                    dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value = null;
            }
            txtBalanceAmount.Text = decBalanceAmount.ToString();
            txtPrincipleAmount.Text = decTotalPrincipleAmount.ToString();


        }

        private void chkLoanClosed_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int iCounter = 0; iCounter <= dgvInstallments.Rows.Count - 1; iCounter++)
            {
                if (dgvInstallments.Rows[iCounter].Cells["BalanceAmount"].Value.ToDecimal() > 0)
                {
                    dgvInstallments.Rows[iCounter].Cells["IsChecked"].Value = chkSelectAll.Checked;

                    if (chkSelectAll.Checked)
                        dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value =
                            dgvInstallments.Rows[iCounter].Cells["BalanceAmount"].Value.ToDecimal() + dgvInstallments.Rows[iCounter].Cells["EnteredAmount"].Value.ToDecimal();
                    else
                        dgvInstallments.Rows[iCounter].Cells["PrincipleAmount"].Value = 0;
                }                
            }
        }
    }
}