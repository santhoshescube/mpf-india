﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
/*
 * Created By       : Tijo
 * Created Date     : 21 Aug 2013
 * Purpose          : For Employee Expense
*/
namespace MyPayfriend
{
    public partial class FrmEmployeeExpense : Form
    {
        clsBLLEmployeeExpense MobjclsBLLEmployeeExpense = new clsBLLEmployeeExpense();
        long TotalRecordCnt, CurrentRecCnt;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool MblnAddPermissionHRPower, MblnUpdatePermissionHRPower; // For HRPower Enabled 
        bool mblnAddStatus = true;
        private clsMessage objUserMessage = null;
        string strBindingOf = "Of ";

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.Expense);

                return this.objUserMessage;
            }
        }

        public FrmEmployeeExpense()
        {
            InitializeComponent();
            tmrClear.Interval = ClsCommonSettings.TimerInterval;

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.Expense, this);

            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            lblStatusShow.Text = "حالة :";
            strBindingOf = "من ";
        }

        private void FrmEmployeeExpense_Load(object sender, EventArgs e)
        {
            SetPermissions();
            AddNew();
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.Expense,
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            if (ClsCommonSettings.IsHrPowerEnabled)
            {
                MblnAddPermissionHRPower = MblnAddPermission;
                MblnUpdatePermissionHRPower = MblnUpdatePermission;
                MblnAddPermission = MblnUpdatePermission = MblnDeletePermission = false;
            }
        }

        private void LoadCombos(int intType)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0 || intType == 1)
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    if (mblnAddStatus)
                    {
                        datTemp = MobjclsBLLEmployeeExpense.FillCombos(new string[] { "DISTINCT EM.EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullNameArb + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullNameArb + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster EM INNER JOIN PaySalaryStructure SS "+
                            "ON EM.EmployeeID = SS.EmployeeID AND SS.PaymentClassificationID = 1 AND  SS.PayCalculationTypeID = 1 " +
                            "INNER JOIN CompanyMaster C ON EM.CompanyID=C.CompanyID", "" +
                            "EM.WorkStatusID >= 6 AND EM.CompanyID in(Select CompanyID from UserCompanyDetails where UserID="  +ClsCommonSettings.UserID + ") ORDER BY EmployeeName" });
                    }
                    else
                    {
                        datTemp = MobjclsBLLEmployeeExpense.FillCombos(new string[] { "EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullNameArb + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullNameArb + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "1=1 ORDER BY EmployeeName" });
                    }
                }
                else
                {
                    if (mblnAddStatus)
                    {
                        datTemp = MobjclsBLLEmployeeExpense.FillCombos(new string[] { "DISTINCT EM.EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster EM INNER JOIN PaySalaryStructure SS "+
                            "ON EM.EmployeeID = SS.EmployeeID AND SS.PaymentClassificationID = 1 AND  SS.PayCalculationTypeID = 1 " +
                            "INNER JOIN CompanyMaster C ON EM.CompanyID=C.CompanyID", "" +
                            "EM.WorkStatusID >= 6 AND EM.CompanyID in(Select CompanyID from UserCompanyDetails where UserID="  +ClsCommonSettings.UserID +") ORDER BY EmployeeName" });
                    }
                    else
                    {
                        datTemp = MobjclsBLLEmployeeExpense.FillCombos(new string[] { "EmployeeID," +
                            "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "1=1 ORDER BY EmployeeName" });
                    }
                }

                cboEmployee.DataSource = datTemp;
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DisplayMember = "EmployeeName";
            }

            if (intType == 0 || intType == 2)
            {
                if(ClsCommonSettings.IsArabicView)
                    datTemp = MobjclsBLLEmployeeExpense.FillCombos(new string[] { "ExpenseHeadID,DescriptionArb As Description", "ExpenseHeadReference", "" });
                else
                    datTemp = MobjclsBLLEmployeeExpense.FillCombos(new string[] { "ExpenseHeadID,Description", "ExpenseHeadReference", "" });
                cboExpenseType.DataSource = datTemp;
                cboExpenseType.ValueMember = "ExpenseHeadID";
                cboExpenseType.DisplayMember = "Description";
            }
        }

        private void AddNew()
        {
            mblnAddStatus = true;
            cboEmployee.Tag = 0;
            LoadCombos(0);
            cboEmployee.SelectedIndex = -1;
            txtAmount.Text = txtActualAmount.Text = txtRemarks.Text = txtSearch.Text = lblJoiningDate.Text = lblCurrency.Text = "";
            dtpDate.Value = dtpExpenseFrom.Value = dtpExpenseTo.Value = ClsCommonSettings.GetServerDate();
            rdbNone.Checked = true;

            if (ClsCommonSettings.IsArabicView)
            {
                lblReturnAmount.Text = "المبلغ الفعلي";
                lblDocumentAttached.Text = "ليس وثائق تعلق";
            }
            else
            {
                lblReturnAmount.Text = "Actual Amount";
                lblDocumentAttached.Text = "No Document(s) Attached";
            }

            lblstatus.Text = "";
            tmrClear.Enabled = false;
            errValidate.Clear();

            SetAutoCompleteList();
            RecordCount();
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt + 1).ToString();
            BindingNavigatorPositionItem.Text = (TotalRecordCnt + 1).ToString();

            cboEmployee.Enabled = true;
            txtActualAmount.Enabled = false;

            btnClear.Enabled = true;
            btnAddNew.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
            btnDocument.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false;

            cboEmployee.Focus();
        }

        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "", "PayEmployeeExpenseDetails");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }

        private void RecordCount()
        {
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.strSearchKey = txtSearch.Text.Trim(); // Is any search text filteration will be taken that also
            TotalRecordCnt = MobjclsBLLEmployeeExpense.GetRecordCount();
            CurrentRecCnt = TotalRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }

            BindingEnableDisable();
        }

        private void BindingEnableDisable()
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            SaveExpense();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveExpense())
            {
                btnSave.Enabled = false;
                this.Close();
            }
        }

        private bool SaveExpense()
        {
            if (FormValidation())
            {
                bool blnSave = false;

                if (mblnAddStatus)
                {
                    if (this.UserMessage.ShowMessage(1) == true) // Save
                        blnSave = true;
                }
                else
                {
                    if (this.UserMessage.ShowMessage(3) == true) // Update
                        blnSave = true;
                }

                if (blnSave)
                {
                    FillParameters();

                    long lngRetValue = MobjclsBLLEmployeeExpense.SaveExpenseDetails();

                    if (lngRetValue > 0)
                    {
                        if (ClsCommonSettings.ThirdPartyIntegration)
                            new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpDate.Value);

                        if (mblnAddStatus)
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(2);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(2); // Saved Successfully
                        }
                        else
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(21);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(21); // Updated Successfully
                        }

                        DisplayExpenseAgainstRecordID(lngRetValue); // to display the current saved or updated content
                        SetAutoCompleteList();
                        return true;
                    }
                }
            }

            return false;
        }

        private bool FormValidation()
        {
            btnExpenseType.Focus(); // For changing dates
            errValidate.Clear();

            if (cboEmployee.SelectedIndex == -1)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(9124);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(9124, cboEmployee, errValidate);
                return false;
            }

            if (cboExpenseType.SelectedIndex == -1)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(7254);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(7254, cboExpenseType, errValidate);
                return false;
            }            

            //if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate())
            //{
            //    lblstatus.Text = this.UserMessage.GetMessageByCode(7252);
            //    tmrClear.Enabled = true;
            //    this.UserMessage.ShowMessage(7252, dtpDate, errValidate);
            //    return false;
            //}

            if (dtpDate.Value.Date < lblJoiningDate.Text.ToDateTime().Date) // if Expense date is less than Employee Joining Date
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(3081);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(3081, dtpDate, errValidate);
                return false;
            }

            // Check Employee Salary is Processed between this period
            if (MobjclsBLLEmployeeExpense.CheckEmployeeSalaryIsProcessed(cboEmployee.SelectedValue.ToInt64(),
                (dtpDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime()) > 0)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(7256);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(7256);
                return false;
            }

            //if (dtpExpenseFrom.Value.Date > dtpDate.Value.Date)
            //{
            //    lblstatus.Text = this.UserMessage.GetMessageByCode(7253);
            //    tmrClear.Enabled = true;
            //    this.UserMessage.ShowMessage(7253, dtpExpenseFrom, errValidate);
            //    return false;
            //}

            //if (dtpExpenseTo.Value.Date > dtpDate.Value.Date)
            //{
            //    lblstatus.Text = this.UserMessage.GetMessageByCode(7255);
            //    tmrClear.Enabled = true;
            //    this.UserMessage.ShowMessage(7255, dtpExpenseTo, errValidate);
            //    return false;
            //}

            if (dtpExpenseFrom.Value.Date > dtpExpenseTo.Value.Date)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(19);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(19, dtpExpenseFrom, errValidate);
                return false;
            }

            if (txtAmount.Text.ToDecimal() == 0)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(7251);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(7251, txtAmount, errValidate);
                return false;
            }

            return true;
        }

        private void FillParameters()
        {
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.lngEmployeeExpenseID = cboEmployee.Tag.ToInt64();
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.lngExpenseNo = getExpenseNo();
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.lngEmployeeID = cboEmployee.SelectedValue.ToInt64();
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.intExpenseHeadID = cboExpenseType.SelectedValue.ToInt32();
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.dtExpenseFromDate = (dtpExpenseFrom.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.dtExpenseToDate = (dtpExpenseTo.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.dtExpenseDate = (dtpDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.decExpenseAmount = txtAmount.Text.ToDecimal();

            if (rdbNone.Checked)
                MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.intExpenseTransID = (int)EmployeeExpenseTransType.None;
            else if (rdbReimbursement.Checked)
                MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.intExpenseTransID = (int)EmployeeExpenseTransType.Reimbursement;
            else if (rdbDeduction.Checked)
                MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.intExpenseTransID = (int)EmployeeExpenseTransType.Deduction;

            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.decActualAmount = txtActualAmount.Text.ToDecimal();

            if (mblnAddStatus)
            {
                DataTable datTemp = MobjclsBLLEmployeeExpense.FillCombos(new string[] { "CM.CurrencyId", "EmployeeMaster EM " +
                    "INNER JOIN CompanyMaster CM ON EM.CompanyID = CM.CompanyID", "EM.EmployeeID = " + MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.lngEmployeeID + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.intCurrencyID = datTemp.Rows[0]["CurrencyId"].ToInt32();
                }
            }

            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.strRemarks = txtRemarks.Text;
        }

        private long getExpenseNo()
        {
            Random objRan = new Random();
            return objRan.Next(9999).ToInt64();
        }

        private void DisplayExpenseAgainstRecordID(long lngRetValue)
        {
            RecordCount();
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt).ToString();
            BindingNavigatorPositionItem.Text = MobjclsBLLEmployeeExpense.getRecordRowNumber(lngRetValue).ToString(); // get record no. against the Expense ID

            DisplayExpenseDetails();
            BindingEnableDisable();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                // Check Employee Salary is Processed between this period
                if (MobjclsBLLEmployeeExpense.CheckEmployeeSalaryIsProcessed(cboEmployee.SelectedValue.ToInt64(),
                    (dtpDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime()) > 0)
                {
                    lblstatus.Text = this.UserMessage.GetMessageByCode(7256);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(7256);
                    return;
                }

                if (this.UserMessage.ShowMessage(13) == true)
                {
                    MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.lngEmployeeExpenseID = cboEmployee.Tag.ToInt64();
                    if (MobjclsBLLEmployeeExpense.DeleteExpenseDetails() == true)
                    {
                        if (ClsCommonSettings.ThirdPartyIntegration)
                            new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpDate.Value);

                        lblstatus.Text = this.UserMessage.GetMessageByCode(4);
                        tmrClear.Enabled = true;
                        this.UserMessage.ShowMessage(4);
                        AddNew();
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                string MstrMessageCommon = "";
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    MstrMessageCommon = "Details exists in the system.Cannot delete";
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
                else
                {
                    MstrMessageCommon = "Error on DeleteExpenseDetails:Expense" + Ex.Message.ToString();
                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //mObjLogs.WriteLog("Error on DeleteExpenseDetails:Expense." + Ex.Message.ToString() + "", 3);
                }
            }
            catch (Exception ex)
            {

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteExpenseDetails:Expense." + ex.Message.ToString());
                //mObjLogs.WriteLog("Error on DeleteExpenseDetails:Expense." + ex.Message.ToString() + "", 3);
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboEmployee.Tag.ToInt64() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = cboEmployee.Tag.ToInt64();

                DataTable datTemp = MobjclsBLLEmployeeExpense.FillCombos(new string[] { "TOP 1 CompanyID AS CompanyID", "EmployeeMaster", "" +
                    "EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        ObjViewer.PintCompany = datTemp.Rows[0]["CompanyID"].ToInt32();
                }

                ObjViewer.PiFormID = (int)FormID.Expense;
                ObjViewer.ShowDialog();
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboEmployee.Tag.ToInt64() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Expense";
                        ObjEmailPopUp.EmailFormType = EmailFormID.Expense;
                        ObjEmailPopUp.EmailSource = MobjclsBLLEmployeeExpense.GetPrintExpenseDetails(cboEmployee.Tag.ToInt64());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayExpenseDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();

            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
                BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayExpenseDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();

            if (TotalRecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                else
                    BindingNavigatorPositionItem.Text = "1";
            }

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayExpenseDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                BindingNavigatorCountItem.Text = strBindingOf + "1";
            }

            DisplayExpenseDetails();
            BindingEnableDisable();
        }

        private void DisplayExpenseDetails()
        {
            MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.strSearchKey = txtSearch.Text.Trim();
            DataTable datTemp = MobjclsBLLEmployeeExpense.DisplayExpenseDetails(BindingNavigatorPositionItem.Text.ToInt64());

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    mblnAddStatus = false;
                    cboEmployee.Enabled = false;
                    LoadCombos(1);

                    cboEmployee.Tag = datTemp.Rows[0]["EmployeeExpenseID"].ToInt64();
                    cboEmployee.SelectedValue = datTemp.Rows[0]["EmployeeID"].ToInt64();
                    cboExpenseType.SelectedValue = datTemp.Rows[0]["ExpenseHeadID"].ToInt32();
                    dtpExpenseFrom.Value = datTemp.Rows[0]["ExpenseFromDate"].ToDateTime();
                    dtpExpenseTo.Value = datTemp.Rows[0]["ExpenseToDate"].ToDateTime();
                    dtpDate.Value = datTemp.Rows[0]["ExpenseDate"].ToDateTime();
                    txtAmount.Text = datTemp.Rows[0]["ExpenseAmount"].ToString();

                    if (ClsCommonSettings.IsArabicView)
                        lblCurrency.Text = datTemp.Rows[0]["CurrencyNameArb"].ToString();
                    else
                        lblCurrency.Text = datTemp.Rows[0]["CurrencyName"].ToString();                    

                    string strTemp = datTemp.Rows[0]["DocumentAttached"].ToString();
                    if (ClsCommonSettings.IsArabicView)
                    {
                        if (strTemp.Contains("No"))
                            strTemp = strTemp.Replace("No", "ليس");

                        strTemp = strTemp.Replace("Document(s) Attached", "وثائق تعلق");
                    }

                    lblDocumentAttached.Text = strTemp;

                    MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.lngExpenseNo = datTemp.Rows[0]["ExpenseNo"].ToInt64();
                    MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.intCurrencyID = datTemp.Rows[0]["CurrencyId"].ToInt32();

                    if (datTemp.Rows[0]["ExpenseTransID"].ToInt32() == (int)EmployeeExpenseTransType.None)
                        rdbNone.Checked = true;
                    else if (datTemp.Rows[0]["ExpenseTransID"].ToInt32() == (int)EmployeeExpenseTransType.Reimbursement)
                        rdbReimbursement.Checked = true;
                    else if (datTemp.Rows[0]["ExpenseTransID"].ToInt32() == (int)EmployeeExpenseTransType.Deduction)
                        rdbDeduction.Checked = true;

                    txtActualAmount.Text = datTemp.Rows[0]["ActualAmount"].ToString();


                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        txtAmount.Text = txtAmount.Text.ToDecimal().ToString("F" + 0);
                        txtActualAmount.Text = txtActualAmount.Text.ToDecimal().ToString("F" + 0);
                    }

                    txtRemarks.Text = datTemp.Rows[0]["Remarks"].ToString();
                    
                    ChangeStatus();
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false; // disable in update mode. Enable these controls when edit started
                }
            }
        }

        private void rdbNone_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbNone.Checked)
            {
                txtActualAmount.Enabled = false;
                txtActualAmount.Text = "";
                if (ClsCommonSettings.IsArabicView)
                    lblReturnAmount.Text = "المبلغ الفعلي";
                else
                    lblReturnAmount.Text = "Actual Amount";
            }
            else
            {
                txtActualAmount.Enabled = true;
                if (ClsCommonSettings.IsArabicView)
                    lblReturnAmount.Text = "المبلغ الفعلي";
                else
                    lblReturnAmount.Text = "Actual Amount";
            }
        }

        private void rdbReimbursement_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbReimbursement.Checked)
            {
                txtActualAmount.Enabled = true;
                if (ClsCommonSettings.IsArabicView)
                    lblReturnAmount.Text = "المبلغ السداد";
                else
                    lblReturnAmount.Text = "Reim. Amount";
            }
            else
            {
                txtActualAmount.Enabled = false;
                if (ClsCommonSettings.IsArabicView)
                    lblReturnAmount.Text = "المبلغ الفعلي";
                else
                    lblReturnAmount.Text = "Actual Amount";
            }

            ChangeStatus();
        }

        private void rdbDeduction_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbDeduction.Checked)
            {
                txtActualAmount.Enabled = true;
                if (ClsCommonSettings.IsArabicView)
                    lblReturnAmount.Text = "خصم المبلغ";
                else
                    lblReturnAmount.Text = "Ded. Amount";
            }
            else
            {
                txtActualAmount.Enabled = false;
                if (ClsCommonSettings.IsArabicView)
                    lblReturnAmount.Text = "المبلغ الفعلي";
                else
                    lblReturnAmount.Text = "Actual Amount";
            }

            ChangeStatus();
        }

        private void ChangeStatus()
        {
            if (mblnAddStatus)
            {
                btnAddNew.Enabled = btnDocument.Enabled = false;
                btnClear.Enabled = true;
                btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            }
            else
            {
                btnAddNew.Enabled = MblnAddPermission;
                btnClear.Enabled = false;

                // if employee is settled don't want to update / delete the Expense details
                if (MobjclsBLLEmployeeExpense.CheckEmployeeExistance(cboEmployee.SelectedValue.ToInt64()) == 1)
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
                else
                {
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                    btnDelete.Enabled = MblnDeletePermission;
                }
                // END

                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                btnDocument.Enabled = (MblnAddPermission || MblnUpdatePermission) ? true : false;

                if (ClsCommonSettings.IsHrPowerEnabled)
                    btnDocument.Enabled = (MblnAddPermissionHRPower || MblnUpdatePermissionHRPower) ? true : false; // Especially for HRPower Enabled
            }
            errValidate.Clear();
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox obj = (ComboBox)sender;
            obj.DroppedDown = false;
        }

        private void ChangeControlStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = "";
            tmrClear.Enabled = false;
        }

        private void FrmEmployeeExpense_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "Expense";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (MblnAddPermission)
                            btnAddNew_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (MblnDeletePermission)
                            btnDelete_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (MblnPrintEmailPermission)
                            btnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (MblnPrintEmailPermission)
                            btnEmail_Click(sender, new EventArgs());//Email
                        break;
                }
            }
            catch { }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Expense";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void btnExpenseType_Click(object sender, EventArgs e)
        {
            // For new Expense Type entry
            int intTempID = cboExpenseType.SelectedValue.ToInt32(); // For setting selected Expense Type after reference form closing
            FrmCommonRef objFrmCommonRef = new FrmCommonRef("Expense Type", new int[] { 1, 0 }, "ExpenseHeadID,Description,DescriptionArb", "ExpenseHeadReference", "IsPredefined = 0");
            objFrmCommonRef.ShowDialog();
            LoadCombos(2);

            if (objFrmCommonRef.NewID != 0)
                cboExpenseType.SelectedValue = objFrmCommonRef.NewID; // If new Reference is added that will be loaded
            else
                cboExpenseType.SelectedValue = intTempID;

            objFrmCommonRef.Dispose();
        }

        private void FrmEmployeeExpense_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {
                if (this.UserMessage.ShowMessage(8) == true) // Your changes will be lost. Please confirm if you wish to cancel?
                {
                    MobjclsBLLEmployeeExpense = null;
                    objUserMessage = null;
                    e.Cancel = false;
                }
                else
                    e.Cancel = true;
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;
            e.Handled = false;
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            }
            else
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            }

            if (ClsCommonSettings.IsAmountRoundByZero == false && ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable datTemp = MobjclsBLLEmployeeExpense.FillCombos(new string[] { "CONVERT(VARCHAR,EM.DateofJoining,106) AS DateofJoining," +
                        "CR.Code AS CurrencyName,CR.CurrencyNameArb", "EmployeeMaster EM INNER JOIN CompanyMaster CM ON EM.CompanyID = CM.CompanyID " +
                        "INNER JOIN CurrencyReference CR ON CM.CurrencyId = CR.CurrencyID", "" +
                        "EM.EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + "" });

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    lblJoiningDate.Text = datTemp.Rows[0]["DateofJoining"].ToString(); // For Checking Employee Date of Joining

                    if (mblnAddStatus)
                    {
                        if (ClsCommonSettings.IsArabicView)
                            lblCurrency.Text = datTemp.Rows[0]["CurrencyNameArb"].ToStringCustom();
                        else
                            lblCurrency.Text = datTemp.Rows[0]["CurrencyName"].ToStringCustom();
                    }
                }
            }
        }

        private void btnDocument_Click(object sender, EventArgs e)
        {
            using (FrmScanning objScanning = new FrmScanning())
            {
                objScanning.MintDocumentTypeid = (int)DocumentType.Expense;
                objScanning.MlngReferenceID = cboEmployee.SelectedValue.ToInt64();
                objScanning.MintOperationTypeID = (int)OperationType.Employee;
                objScanning.MstrReferenceNo = dtpDate.Value.ToString("dd MMM yyyy") + " [" + MobjclsBLLEmployeeExpense.PobjclsDTOEmployeeExpense.lngExpenseNo.ToString() + "]";
                objScanning.PblnEditable = true;
                objScanning.MintDocumentID = cboEmployee.Tag.ToInt32();
                objScanning.ShowDialog();

                // get attached Document Count
                string strTemp = MobjclsBLLEmployeeExpense.getAttchaedDocumentCount(cboEmployee.Tag.ToInt64());
                if (ClsCommonSettings.IsArabicView)
                {
                    if (strTemp.Contains("No"))
                        strTemp = strTemp.Replace("No", "ليس");

                    strTemp = strTemp.Replace("Document(s) Attached", "وثائق تعلق");
                }

                lblDocumentAttached.Text = strTemp;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.txtSearch.Text.Trim() == string.Empty)
                return;

            RecordCount(); // get record count

            if (TotalRecordCnt == 0) // No records
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(40);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(40);
                txtSearch.Text = string.Empty;
                btnAddNew_Click(sender, e);
            }
            else if (TotalRecordCnt > 0)
                BindingNavigatorMoveFirstItem_Click(sender, e); // if record found in search then automatically navigate to first item
        }
    }
}