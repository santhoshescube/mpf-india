﻿namespace MyPayfriend
{
    partial class FrmUpdateAccruals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTop = new System.Windows.Forms.Panel();
            this.btnShow = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.pnlBtm = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pbUpdate = new System.Windows.Forms.ProgressBar();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.dgvEmployee = new System.Windows.Forms.DataGridView();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateOfJoin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityArrAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeavePassageAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ticket = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isupdated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTop.SuspendLayout();
            this.pnlBtm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.btnShow);
            this.pnlTop.Controls.Add(this.label2);
            this.pnlTop.Controls.Add(this.cboCompany);
            this.pnlTop.Controls.Add(this.label1);
            this.pnlTop.Controls.Add(this.dtpToDate);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(826, 51);
            this.pnlTop.TabIndex = 0;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(535, 12);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 27);
            this.btnShow.TabIndex = 177;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(329, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 176;
            this.label2.Text = "Month";
            // 
            // cboCompany
            // 
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(88, 16);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(212, 21);
            this.cboCompany.TabIndex = 175;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 174;
            this.label1.Text = "Company";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(372, 16);
            this.dtpToDate.MinDate = new System.DateTime(2015, 11, 30, 0, 0, 0, 0);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.ShowUpDown = true;
            this.dtpToDate.Size = new System.Drawing.Size(118, 20);
            this.dtpToDate.TabIndex = 173;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // pnlBtm
            // 
            this.pnlBtm.Controls.Add(this.btnCancel);
            this.pnlBtm.Controls.Add(this.pbUpdate);
            this.pnlBtm.Controls.Add(this.btnUpdate);
            this.pnlBtm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBtm.Location = new System.Drawing.Point(0, 445);
            this.pnlBtm.Name = "pnlBtm";
            this.pnlBtm.Size = new System.Drawing.Size(826, 42);
            this.pnlBtm.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(739, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 33);
            this.btnCancel.TabIndex = 179;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pbUpdate
            // 
            this.pbUpdate.Location = new System.Drawing.Point(4, 22);
            this.pbUpdate.Name = "pbUpdate";
            this.pbUpdate.Size = new System.Drawing.Size(435, 14);
            this.pbUpdate.TabIndex = 178;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Location = new System.Drawing.Point(584, 6);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(139, 33);
            this.btnUpdate.TabIndex = 177;
            this.btnUpdate.Text = "Update Accruals";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 487);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(826, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // dgvEmployee
            // 
            this.dgvEmployee.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmployeeID,
            this.EmployeeName,
            this.DateOfJoin,
            this.GratuityDays,
            this.GratuityAmount,
            this.GratuityArrAmt,
            this.LeavePassageAmount,
            this.Ticket,
            this.isupdated});
            this.dgvEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEmployee.Location = new System.Drawing.Point(0, 51);
            this.dgvEmployee.Name = "dgvEmployee";
            this.dgvEmployee.Size = new System.Drawing.Size(826, 394);
            this.dgvEmployee.TabIndex = 3;
            this.dgvEmployee.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployee_CellValueChanged);
            this.dgvEmployee.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvEmployee_DataError);
            // 
            // EmployeeID
            // 
            this.EmployeeID.DataPropertyName = "EmployeeID";
            this.EmployeeID.HeaderText = "EmployeeID";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.Visible = false;
            // 
            // EmployeeName
            // 
            this.EmployeeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EmployeeName.DataPropertyName = "EmployeeName";
            this.EmployeeName.HeaderText = "Employee";
            this.EmployeeName.Name = "EmployeeName";
            // 
            // DateOfJoin
            // 
            this.DateOfJoin.DataPropertyName = "DateOfJoin";
            this.DateOfJoin.HeaderText = "Date Of Join";
            this.DateOfJoin.Name = "DateOfJoin";
            // 
            // GratuityDays
            // 
            this.GratuityDays.DataPropertyName = "GratuityDays";
            this.GratuityDays.HeaderText = "Days";
            this.GratuityDays.Name = "GratuityDays";
            this.GratuityDays.ReadOnly = true;
            // 
            // GratuityAmount
            // 
            this.GratuityAmount.DataPropertyName = "GratuityAmt";
            this.GratuityAmount.HeaderText = "Gratuity Amount";
            this.GratuityAmount.Name = "GratuityAmount";
            this.GratuityAmount.ReadOnly = true;
            this.GratuityAmount.Width = 120;
            // 
            // GratuityArrAmt
            // 
            this.GratuityArrAmt.HeaderText = "Gratuity Arrear";
            this.GratuityArrAmt.Name = "GratuityArrAmt";
            // 
            // LeavePassageAmount
            // 
            this.LeavePassageAmount.DataPropertyName = "LeavePassageAmt";
            this.LeavePassageAmount.HeaderText = "Leave Salary";
            this.LeavePassageAmount.Name = "LeavePassageAmount";
            this.LeavePassageAmount.ReadOnly = true;
            // 
            // Ticket
            // 
            this.Ticket.DataPropertyName = "Ticket";
            this.Ticket.HeaderText = "Ticket";
            this.Ticket.Name = "Ticket";
            this.Ticket.ReadOnly = true;
            // 
            // isupdated
            // 
            this.isupdated.HeaderText = "isupdated";
            this.isupdated.Name = "isupdated";
            this.isupdated.Visible = false;
            // 
            // FrmUpdateAccruals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 509);
            this.Controls.Add(this.dgvEmployee);
            this.Controls.Add(this.pnlBtm);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pnlTop);
            this.MinimizeBox = false;
            this.Name = "FrmUpdateAccruals";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update Accruals";
            this.Load += new System.EventHandler(this.FrmUpdateAccruals_Load);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlBtm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlBtm;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView dgvEmployee;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboCompany;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.ProgressBar pbUpdate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateOfJoin;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityArrAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn LeavePassageAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ticket;
        private System.Windows.Forms.DataGridViewTextBoxColumn isupdated;
    }
}