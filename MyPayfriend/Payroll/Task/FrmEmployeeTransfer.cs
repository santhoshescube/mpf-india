﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using BLL;
using DTO;
using Microsoft.VisualBasic;

namespace MyPayfriend
{
    public partial class FrmEmployeeTransfer : Form
    {
        #region Declarations

        public int PEmpID = 0;              //ID Pass from Another form
        public int MCompanyID = 0;
        int FromId;                         // From ID  1 Company,2 Branch ,3 Department,4 Designation
        string MstrCommonMessage;
        int MintTransferTypeID = 0;

        private bool MblnAddPermission = false;         //To Set Add Permission
        private bool MblnUpdatePermission = false;      //To Set Update Permission
        private bool MblnDeletePermission = false;      //To Set Delete Permission
        private bool MblnPrintEmailPermission = false;  //To Set PrintEmail Permission

        bool Glb24HourFormat;
        private string MstrMessageCaption;              //Message caption
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid

        private bool MblnIsHrPowerEnabled = false;
        private bool MblnIsFromRequest = false;
        private int MintUserId;
        private clsMessage ObjUserMessage = null;
        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;
        clsBLLEmployeeTransfer MobjclsBLLEmployeeTransfer;
        clsBLLSalaryPayment MobjClsBLLSalaryPayment;

        #endregion
        #region Constructor
        public FrmEmployeeTransfer()
        {
            //Constructor
            InitializeComponent();
            MintCompanyId = ClsCommonSettings.CurrentCompanyID;
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            Glb24HourFormat = ClsCommonSettings.Glb24HourFormat;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MobjclsBLLEmployeeTransfer = new clsBLLEmployeeTransfer();
            MobjClsBLLSalaryPayment = new clsBLLSalaryPayment();

            //if (ClsCommonSettings.IsArabicView)
            //{
            //    this.RightToLeftLayout = true;
            //    this.RightToLeft = RightToLeft.Yes;
            //    SetArabicControls();
            //}
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.EmployeeTransfer, this);
        }
        #endregion Constructor
        #region Events
        private void rbCompanyBranch_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCompanyBranch.Checked == true)
            {
                MintTransferTypeID = 1;
            }
        }

        private void rbDepartment_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDepartment.Checked == true)
            {
                MintTransferTypeID = 2;
            }
        }

        private void rbDesignation_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDesignation.Checked == true)
            {
                MintTransferTypeID = 3;
            }
        }

        private void rbEmployment_CheckedChanged(object sender, EventArgs e)
        {
            if (rbEmployment.Checked == true)
            {
                MintTransferTypeID = 4;
            }

        }

        private void GetControls()
        {

            FrmEmployeeTransfer.ControlCollection obj = new ControlCollection(this);
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            foreach (Control cntrl in obj.Owner.Controls)
            {
                objDAL.GetControls(cntrl, (int)FormID.EmployeeTransfer);
            }
        }
        private void FrmEmployeeTransfer_Load(object sender, EventArgs e)
        {
            if (ClsCommonSettings.IsHrPowerEnabled)
            {
                MblnIsHrPowerEnabled = true;
                pnlRequest.Visible = true;
            }
            else
            {
                pnlRequest.Visible = false;
            }

            GetControls();
            LoadEmployee();

            SetPermissions();

            if(MblnAddPermission)
                WizardWelcomePage.NextButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.True;
            else
                WizardWelcomePage.NextButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.False;

        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillTransferFromToCombo();
        }

        private void WizardEmployeeTransferInfo_NextButtonClick(object sender, CancelEventArgs e)
        {
            try
            {
                epEmployeeTransfer.Clear();
                if (ValidateEmployeeTransferMaster() == false)
                {
                    e.Cancel = true;
                    return;
                }

                if (MintTransferTypeID == 1)
                {
                    MCompanyID = Convert.ToInt32(cboTransferTo.SelectedValue);
                    MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.TransferToID = Convert.ToInt32(cboTransferTo.SelectedValue);
                    MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.TransferFromID = Convert.ToInt32(cboTransferFrom.SelectedValue);
                    setControlsEnableOrDisable(true);
                }
                else
                {
                    setControlsEnableOrDisable(false);
                }
                if (MintTransferTypeID == 1)
                {
                    btnSalaryStructure.Visible = true;
                }
                else
                    btnSalaryStructure.Visible = false;

                SetEmployeeInfo();


                if (ChkTemp.Checked)
                {
                    WizardEmployeeInfo_NextButtonClick(sender, e); 
                    Wizard1.SelectedPage = WizardFinishPage;
                }

            }
            catch
            {
            }



        }
        private void setControlsEnableOrDisable(bool result)
        {
            cboTransactionType.Enabled = cboEmployeeBank.Enabled = cboEmployerBank.Enabled = cboEmployerBankAct.Enabled = txtEmployeeAntNo.Enabled = result;
        }

        private void WizardEmployeeTransferInfo_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            cboEmployee.Focus();
        }

        private void WizardEmployeeInfo_NextButtonClick(object sender, CancelEventArgs e)
        {
            epEmployeeTransfer.Clear();
            if (ValidateEmployeeTransferDetails() == false)
            {
                e.Cancel = true;
                return;
            }
            lblComment.Text = lblEmployeeText.Text + " will be transfered from " + cboTransferFrom.Text + " to " + cboTransferTo.Text;

            lblEmployeeT.Text = cboEmployee.Text;
            lblWorkPolicyT.Text = cboWorkPolicy.Text;
            lblLeavePolicyT.Text = cboLeavePolicy.Text;
            lblTransactionTypeT.Text = cboTransactionType.Text;
            lblLocationT.Text = cboWorkLocation.Text;

            if (cboEmployerBank.Text != "")
                lblEmployerBankT.Text = cboEmployerBank.Text;
            else
                lblEmployerBankT.Text = "-";
            if (cboEmployerBankAct.Text != "")
                lblEmployerBankACT.Text = cboEmployerBankAct.Text;
            else
                lblEmployerBankACT.Text = "-";
            if (cboEmployeeBank.Text != "")
                lblEmployeeBankT.Text = cboEmployeeBank.Text;
            else
                lblEmployeeBankT.Text = "-";
            if (txtEmployeeAntNo.Text != "")
                lblEmployeeBankACT.Text = txtEmployeeAntNo.Text;
            else
                lblEmployeeBankACT.Text = "-";

            DataTable dt = MobjclsBLLEmployeeTransfer.GetEmployeeInfo(Convert.ToInt32(cboEmployee.SelectedValue));
            if (dt.Rows.Count > 0)
            {
                lblDepartmentT.Text = dt.Rows[0]["Department"].ToString();
                lblDesignationT.Text = dt.Rows[0]["Designation"].ToString();
                lblCompanyT.Text = dt.Rows[0]["CompanyName"].ToString();
                if (dt.Rows[0]["EmploymentType"].ToString() != "")
                    lblEmploymentTypeT.Text = dt.Rows[0]["EmploymentType"].ToString();
                else
                    lblEmploymentTypeT.Text = "-";
            }
            if (MintTransferTypeID == 1)
            {
                lblCompanyT.Text = cboTransferTo.Text;
            }
            else if (MintTransferTypeID == 2)
            {
                lblDepartmentT.Text = cboTransferTo.Text;
            }
            else if (MintTransferTypeID == 3)
            {
                lblDesignationT.Text = cboTransferTo.Text;
            }
            else if (MintTransferTypeID == 4)
            {
                lblEmploymentTypeT.Text = cboTransferTo.Text;
            }


        }

        private void WizardEmployeeInfo_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            if (cboTransactionType.Enabled == true)
            {
                cboTransactionType.Focus();
            }
            else
            {
                cboWorkPolicy.Focus();
            }
        }

        private void WizardFinishPage_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            txtOtherInfo.Focus();
        }


        private void WizardFinishPage_FinishButtonClick(object sender, CancelEventArgs e)
        {
            this.Close();
        }

        private void WizardWelcomePage_NextButtonClick(object sender, CancelEventArgs e)
        {
            epEmployeeTransfer.Clear();
            cboEmployee.SelectedIndex = -1;

            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.TransferTypeID = MintTransferTypeID;
            
        }

        private void WizardFinishPage_NextButtonClick(object sender, CancelEventArgs e)
        {
            int iState = 0; string strCompanyCurrency = ""; string strEmpCurrency = "";

            try
            {
                bool CheckEmployeeCurrencyExchangeRate;
                lblWait.Text = "Please wait...";

                CheckEmployeeCurrencyExchangeRate = MobjclsBLLEmployeeTransfer.CheckEmployeeCurrencyExchangeRate(MintTransferTypeID, 0, Convert.ToInt32(cboEmployee.SelectedValue), Convert.ToInt32(cboTransferTo.SelectedValue), ref  iState, ref  strCompanyCurrency, ref  strEmpCurrency);

                if (MintTransferTypeID == 1 && CheckEmployeeCurrencyExchangeRate == false)
                {
                    using (FrmCurrency frmCurrencyDetails = new FrmCurrency())
                    {
                        frmCurrencyDetails.ShowDialog();
                    }

                    CheckEmployeeCurrencyExchangeRate = MobjclsBLLEmployeeTransfer.CheckEmployeeCurrencyExchangeRate(MintTransferTypeID, 0, Convert.ToInt32(cboEmployee.SelectedValue), Convert.ToInt32(cboTransferTo.SelectedValue), ref  iState, ref  strCompanyCurrency, ref strEmpCurrency);

                    if (CheckEmployeeCurrencyExchangeRate == false)
                    {
                        e.Cancel = true;
                        return;
                    }

                }
                if (MintTransferTypeID == 1)
                {
                    if (strCompanyCurrency.Trim() != strEmpCurrency.Trim())
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(9897);
                        MstrCommonMessage = MstrCommonMessage.Replace("@", strEmpCurrency).Replace("*", strCompanyCurrency).Replace("#", "");
                        MessageBox.Show(MstrCommonMessage, MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                tmrProcess.Interval = 2;
                tmrProcess.Enabled = true;
                ProgressBar1.Value = 5;
                ProgressBar1.Visible = true;
                lblWait.Visible = true;
                FillParameters();

                bool payflag = MobjclsBLLEmployeeTransfer.PaymentExist(Convert.ToInt32(cboEmployee.SelectedValue));
                if (MintTransferTypeID == 1)
                {
                    try
                    {
                        if (ChkTemp.Checked == false)
                        {
                            MobjclsBLLEmployeeTransfer.SalaryProcess(Convert.ToInt32(cboEmployee.SelectedValue), dtpTransferDate.Value.ToString("dd-MMM-yyyy"), MintUserId);
                        }
                        if (MobjclsBLLEmployeeTransfer.IsSalaryExists(Convert.ToInt32(cboEmployee.SelectedValue), dtpTransferDate.Value.ToString("dd-MMM-yyyy")) == true)
                        {
                            MobjclsBLLEmployeeTransfer.SalaryRelease(dtpTransferDate.Value.ToString("dd-MMM-yyyy"), Convert.ToInt32(cboEmployee.SelectedValue), MintUserId);
                        }
                    }
                    catch (Exception ex)
                    {
                        mObjLogs.WriteLog("Error on WizardFinishPage_NextButtonClick " + this.Name + " " + ex.Message.ToString(), 1);

                    }
                }

                int iStatus = MobjclsBLLEmployeeTransfer.GetStatus(8, Convert.ToInt32(cboEmployee.SelectedValue), "", "");


                if (iStatus == 1)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(9885);
                    if (MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        iStatus = MobjclsBLLEmployeeTransfer.GetStatus(9, Convert.ToInt32(cboEmployee.SelectedValue), "", "");
                    }
                }
                if (!ChkTemp.Checked)
                {
                    if (IsAttendanceExists())
                    {
                        DeleteAttendance();
                    }
                }

                MobjclsBLLEmployeeTransfer.SaveEmployeeTransfer();

                if (MintTransferTypeID == 1)
                {
                    if (ClsCommonSettings.ThirdPartyIntegration)
                        new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpTransferDate.Value.ToString("dd-MMM-yyyy").ToDateTime());
                }
                tmrProcess.Enabled = false;
                ProgressBar1.Value = 100;

                lblWait.Text = "Employee transfered successfully";
                lblComment.Text = lblEmployeeText.Text + " transfered from " + cboTransferFrom.Text + " to " + cboTransferTo.Text;
                WizardFinishPage.FinishButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.True;
                WizardFinishPage.BackButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.False;
                WizardFinishPage.NextButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.False;
                WizardFinishPage.CancelButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.False;
                lblFinishComment.Visible = false;
                btnSalaryStructure.Enabled = false;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on WizardFinishPage_NextButtonClick " + this.Name + " " + ex.Message.ToString(), 1);
                lblWait.Visible = false;
                ProgressBar1.Visible = false;
                tmrProcess.Enabled = false;
            }
        }

        private void cboEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void cboTransferTo_KeyDown(object sender, KeyEventArgs e)
        {
            cboTransferTo.DroppedDown = false;
        }

        private void cboTransactionType_KeyDown(object sender, KeyEventArgs e)
        {
            cboTransactionType.DroppedDown = false;
        }

        private void cboWorkPolicy_KeyDown(object sender, KeyEventArgs e)
        {
            cboWorkPolicy.DroppedDown = false;
        }

        private void cboEmployerBank_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployeeBank.DroppedDown = false;
        }

        private void cboEmployerBankAct_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployerBankAct.DroppedDown = false;
        }

        private void cboEmployeeBank_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployeeBank.DroppedDown = false;
        }

        private void cboLeavePolicy_KeyDown(object sender, KeyEventArgs e)
        {
            cboLeavePolicy.DroppedDown = false;
        }

        private void btnSalaryStructure_Click(object sender, EventArgs e)
        {
            if (MintTransferTypeID == 1)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(9991);
                if (MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    using (FrmSalaryStructure objSalaryStructure = new FrmSalaryStructure())
                    {
                        objSalaryStructure.PintCompanyID = cboTransferFrom.SelectedValue.ToInt32();
                        objSalaryStructure.PintEmployeeID = cboEmployee.SelectedValue.ToInt32();
                        objSalaryStructure.ShowDialog();
                    }
                }
            }
        }

        private void Wizard1_HelpButtonClick(object sender, CancelEventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "EmployeeTransfer"; Help.ShowDialog(); }
        }

        #endregion Events
        #region Methods
        #region LoadEmployee
        /// <summary>
        /// To load Combo Employee
        /// </summary>
        /// <returns>bool</returns>
        private bool LoadEmployee()
        {
            DataTable datCombos = new DataTable();

            cboEmployee.Text = "";
            //cboTransferFrom.Text = "";
            //cboTransferTo.Text = "";
            if (rdbFromRequest.Checked && MblnIsHrPowerEnabled)
                datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT DISTINCT E.EmployeeID, " +
                    "ISNULL(E.EmployeeFullName, '') + ' - ' + CAST(E.EmployeeNumber AS VARCHAR) AS FirstName " +
                    "FROM EmployeeMaster AS E INNER JOIN PaySalaryStructure EmployeeSalaryHead " +
                    "ON E.EmployeeID = EmployeeSalaryHead.EmployeeID and E.WorkStatusID >= 6 " +
                    "INNER JOIN  HRTransferRequest TR ON  E.EmployeeID=TR.EmployeeID AND TR.StatusId IN (5,7) AND TR.ApprovedBy IS NOT NULL " +
                    "AND TR.RequestID NOT IN (SELECT ISNULL(RequestID,0) FROM  PayEmployeeTransfers) " +
                    " ORDER BY FirstName");
            else
                datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT DISTINCT E.EmployeeID, " +
                    "ISNULL(E.EmployeeFullName, '') + ' - ' + CAST(E.EmployeeNumber AS VARCHAR) AS FirstName " +
                    "FROM EmployeeMaster AS E INNER JOIN PaySalaryStructure EmployeeSalaryHead " +
                    "ON E.EmployeeID = EmployeeSalaryHead.EmployeeID and E.WorkStatusID >= 6 " +
                    "AND E.CompanyID in (Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") ORDER BY FirstName");
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DisplayMember = "FirstName";
            cboEmployee.DataSource = datCombos;
            datCombos = null;

            return true;
        }
        #endregion LoadEmployee
        #region LoadMessage
        /// <summary>
        /// Method to fill the message array according to form
        /// </summary>
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.EmployeeTransfer);
                return this.ObjUserMessage;
            }
        }
        #endregion LoadMessage
        #region LoadCombos
        /// <summary>
        /// Load the combos
        /// </summary>
        private void LoadCombos()
        {
            try
            {
                DataTable datCombos = new DataTable();
                datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT  ABD.CompanyBankAccountID AS CompanyAccID, ABD.BankAccountNo AS AccountNo FROM  BankReference AS BNR INNER JOIN BankBranchReference BBR ON BNR.BankID = BBR.BankID  INNER JOIN CompanyBankAccountDetails AS ABD ON BBR.BankBranchID = ABD.BankBranchID WHERE  ABD.CompanyID = " + MCompanyID + "");
                cboEmployerBankAct.ValueMember = "CompanyAccID";
                cboEmployerBankAct.DisplayMember = "AccountNo";
                cboEmployerBankAct.DataSource = datCombos;
                datCombos = null;

                datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT DISTINCT BB.BankBranchID, BNR.BankName + '-' + BB.BankBranchName AS Description FROM BankReference AS BNR LEFT OUTER JOIN BankBranchReference AS BB ON BNR.BankID = BB.BankID LEFT OUTER JOIN CompanyBankAccountDetails AS ABD ON BB.BankBranchID = ABD.BankBranchID WHERE ABD.CompanyID = " + MCompanyID + "");
                cboEmployerBank.ValueMember = "BankBranchID";
                cboEmployerBank.DisplayMember = "Description";
                cboEmployerBank.DataSource = datCombos;
                datCombos = null;

                datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT TransactionTypeID,TransactionType AS Description FROM TransactionTypeReference ");
                cboTransactionType.ValueMember = "TransactionTypeID";
                cboTransactionType.DisplayMember = "Description";
                cboTransactionType.DataSource = datCombos;
                datCombos = null;

                datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT BankBranchReference.BankBranchID, ISNULL(BankBranchReference.BankBranchName,'')+ ' - ' + ISNULL(BankReference.BankName,'')   as Description   FROM  BankReference INNER JOIN BankBranchReference ON BankReference.BankID = BankBranchReference.BankID ORDER BY BankBranchReference.BankBranchName");
                cboEmployeeBank.ValueMember = "BankBranchID";
                cboEmployeeBank.DisplayMember = "Description";
                cboEmployeeBank.DataSource = datCombos;
                datCombos = null;

                datCombos = MobjclsBLLEmployeeTransfer.FillPolicyCombo();
                cboWorkPolicy.ValueMember = "WorkPolicyID";
                cboWorkPolicy.DisplayMember = "WorkPolicy";
                cboWorkPolicy.DataSource = datCombos;
                datCombos = null;

                datCombos = MobjclsBLLEmployeeTransfer.FillLeavePolicyCombo();
                cboLeavePolicy.ValueMember = "LeavePolicyID";
                cboLeavePolicy.DisplayMember = "LeavePolicyName";
                cboLeavePolicy.DataSource = datCombos;
                datCombos = null;

                datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT WorkLocationID,LocationName FROM WorkLocationReference WHERE  CompanyID = " + MCompanyID + "");
                cboWorkLocation.ValueMember = "WorkLocationID";
                cboWorkLocation.DisplayMember = "LocationName";
                cboWorkLocation.DataSource = datCombos;
                datCombos = null;

            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadCombos " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        #endregion LoadCombos
        #region SetEmployeeInfo
        /// <summary>
        /// Function to set Employee Information
        /// </summary> 
        private void SetEmployeeInfo()
        {
            if (cboEmployee.SelectedIndex != -1)
            {
                DataTable datEmployee = MobjclsBLLEmployeeTransfer.SetEmployeeInfo(Convert.ToInt32(cboEmployee.SelectedValue));
                if (datEmployee.Rows.Count > 0)
                {
                    lblEmployeeText.Text = datEmployee.Rows[0]["EmployeeFullName"].ToString();
                    lblTransactionTypeText.Text = datEmployee.Rows[0]["TransactionType"].ToString();
                    lblWorkPolicyText.Text = datEmployee.Rows[0]["WorkPolicy"].ToString();
                    lblLeavePolicyText.Text = datEmployee.Rows[0]["LeavePolicy"].ToString();
                    lblEmployerBankNameText.Text = datEmployee.Rows[0]["EmployerBank"].ToString();
                    lblEmployerBankAntText.Text = datEmployee.Rows[0]["EmployerAccountNo"].ToString();
                    lblEmployeeBankNameText.Text = datEmployee.Rows[0]["EmployeeBank"].ToString();
                    lblEmployeeAcntNoText.Text = datEmployee.Rows[0]["EmployeeAccountNo"].ToString();
                    lblLocationText.Text = datEmployee.Rows[0]["LocationName"].ToString();

                    LoadCombos();

                    cboTransactionType.SelectedValue = datEmployee.Rows[0]["TransactionTypeID"];
                    cboWorkPolicy.SelectedValue = datEmployee.Rows[0]["WorkPolicyID"];
                    if (cboWorkPolicy.DataSource != null && cboWorkPolicy.Items.Count > 0)
                        cboWorkPolicy.SelectedIndex = 0;

                    if (Convert.ToInt32(datEmployee.Rows[0]["LeavePolicyID"]) != 0)
                    {
                        cboLeavePolicy.SelectedValue = datEmployee.Rows[0]["LeavePolicyID"];
                    }
                    if (Convert.ToInt32(datEmployee.Rows[0]["WorkLocationID"]) != 0)
                    {
                        cboLeavePolicy.SelectedValue = datEmployee.Rows[0]["WorkLocationID"];
                    }
                    cboEmployeeBank.SelectedValue = datEmployee.Rows[0]["BankBranchID"];
                    cboEmployerBank.SelectedValue = datEmployee.Rows[0]["BankNameID"];
                    if (MintTransferTypeID == 1 && cboEmployerBank.DataSource != null && cboEmployerBank.Items.Count > 0)
                        cboEmployerBank.SelectedIndex =-1;
                    cboEmployerBankAct.SelectedValue = datEmployee.Rows[0]["ComBankAccID"];
                    txtEmployeeAntNo.Text = datEmployee.Rows[0]["EmployeeAccountNo"].ToString();
                }

            }

        }
        #endregion SetEmployeeInfo
        #region SetPermissions
        /// <summary>
        /// To set Permissions
        /// </summary>
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Task, (Int32)eMenuID.EmployeeTransfer, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }
        #endregion SetPermissions
        #region FillTransferFromToCombo
        /// <summary>
        /// To Fill Transfer From and Transfer To
        /// </summary>
        /// <returns></returns>
        private bool FillTransferFromToCombo()
        {
            try
            {
                DataTable datCombos = new DataTable();
                cboTransferFrom.DataSource = null;
                cboTransferTo.DataSource = null;
                if (cboEmployee.SelectedValue != null)
                {
                    DataTable datEmployeeDetails = MobjclsBLLEmployeeTransfer.FillDetails("Select CompanyID,DepartmentID,DesignationID,isnull(EmploymentTypeID,0) EmploymentType From EmployeeMaster Where EmployeeID = " + Convert.ToInt32(cboEmployee.SelectedValue) + " ");
                    if (datEmployeeDetails.Rows.Count > 0)
                    {

                        MCompanyID = Convert.ToInt32(datEmployeeDetails.Rows[0]["CompanyID"]);
                        MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.CompanyID = MCompanyID;
                        MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.DepartmentID = Convert.ToInt32(datEmployeeDetails.Rows[0]["DepartmentID"]);
                        MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.DesignationID = Convert.ToInt32(datEmployeeDetails.Rows[0]["DesignationID"]);
                        MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.EmploymentTypeID = Convert.ToInt32(datEmployeeDetails.Rows[0]["EmploymentType"]);
                    }


                    DataTable dtRequsetDetails = MobjclsBLLEmployeeTransfer.FillDetails("SELECT RequestId,TransferTypeId,TransferFrom,TransferTo,TransferDate FROM HRTransferRequest WHERE StatusId IN (5,7) AND ApprovedBy IS NOT NULL AND EmployeeID= " + Convert.ToInt32(cboEmployee.SelectedValue) + " AND RequestId NOT IN(SELECT ISNULL(RequestId,0) FROM  PayEmployeeTransfers)");
                    if (rbCompanyBranch.Checked == true)
                    {
                        MintTransferTypeID = 1;
                    }
                    else if (rbDepartment.Checked == true)
                    {
                        MintTransferTypeID = 2;
                    }

                    else if (rbDesignation.Checked == true)
                    {
                        MintTransferTypeID = 3;
                    }

                    else if (rbEmployment.Checked == true)
                    {
                        MintTransferTypeID = 4;
                    }

                    switch (MintTransferTypeID)
                    {
                        case 1:
                            datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT CompanyID,CompanyName AS Name FROM CompanyMaster Where CompanyID <> " + datEmployeeDetails.Rows[0]["CompanyID"] + " order by Name");
                            cboTransferTo.ValueMember = "CompanyID";
                            cboTransferTo.DisplayMember = "Name";
                            cboTransferTo.DataSource = datCombos;
                            datCombos = null;

                            datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT CompanyID,CompanyName AS Name FROM CompanyMaster  Where CompanyID = " + datEmployeeDetails.Rows[0]["CompanyID"] + "");
                            cboTransferFrom.ValueMember = "CompanyID";
                            cboTransferFrom.DisplayMember = "Name";
                            cboTransferFrom.DataSource = datCombos;
                            datCombos = null;

                            if (rdbFromRequest.Checked && MblnIsHrPowerEnabled)
                            {
                                if (dtRequsetDetails.Rows.Count > 0 && dtRequsetDetails.Rows[0]["TransferTypeId"].ToInt32() == 1)
                                {
                                    cboTransferTo.SelectedValue = dtRequsetDetails.Rows[0]["TransferTo"].ToInt32();
                                }
                                else
                                {
                                    cboTransferTo.DataSource = null;
                                }
                            }
                            break;
                        case 2:
                            datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT DepartmentID,Department AS Description FROM DepartmentReference Where DepartmentID <> " + datEmployeeDetails.Rows[0]["DepartmentID"] + " order by Description");
                            cboTransferTo.ValueMember = "DepartmentID";
                            cboTransferTo.DisplayMember = "Description";
                            cboTransferTo.DataSource = datCombos;
                            datCombos = null;

                            datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT DepartmentID,Department AS Description FROM DepartmentReference Where DepartmentID = " + datEmployeeDetails.Rows[0]["DepartmentID"] + "");
                            cboTransferFrom.ValueMember = "DepartmentID";
                            cboTransferFrom.DisplayMember = "Description";
                            cboTransferFrom.DataSource = datCombos;
                            datCombos = null;

                            if (rdbFromRequest.Checked && MblnIsHrPowerEnabled)
                            {
                                if (dtRequsetDetails.Rows.Count > 0 && dtRequsetDetails.Rows[0]["TransferTypeId"].ToInt32() == 2)
                                {
                                    cboTransferTo.SelectedValue = dtRequsetDetails.Rows[0]["TransferTo"].ToInt32();
                                }
                                else
                                {
                                    cboTransferTo.DataSource = null;
                                }
                            }
                            break;

                        case 3:
                            datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT DesignationID,Designation AS Description FROM DesignationReference Where DesignationID <> " + datEmployeeDetails.Rows[0]["DesignationID"] + " order by Description");
                            cboTransferTo.ValueMember = "DesignationID";
                            cboTransferTo.DisplayMember = "Description";
                            cboTransferTo.DataSource = datCombos;
                            datCombos = null;

                            datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT DesignationID,Designation AS Description FROM DesignationReference Where DesignationID = " + datEmployeeDetails.Rows[0]["DesignationID"] + "");
                            cboTransferFrom.ValueMember = "DesignationID";
                            cboTransferFrom.DisplayMember = "Description";
                            cboTransferFrom.DataSource = datCombos;
                            datCombos = null;

                            if (rdbFromRequest.Checked && MblnIsHrPowerEnabled)
                            {
                                if (dtRequsetDetails.Rows.Count > 0 && dtRequsetDetails.Rows[0]["TransferTypeId"].ToInt32() == 3)
                                {
                                    cboTransferTo.SelectedValue = dtRequsetDetails.Rows[0]["TransferTo"].ToInt32();
                                }
                                else
                                {
                                    cboTransferTo.DataSource = null;
                                }
                            }
                            break;
                        case 4:
                            datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("select EmploymentTypeID,EmploymentType AS Description from EmploymentTypeReference  where EmploymentTypeID Not In (2," + datEmployeeDetails.Rows[0]["EmploymentType"] + ") order by Description");
                            cboTransferTo.ValueMember = "EmploymentTypeID";
                            cboTransferTo.DisplayMember = "Description";
                            cboTransferTo.DataSource = datCombos;
                            datCombos = null;

                            datCombos = MobjclsBLLEmployeeTransfer.LoadCombos("select EmploymentTypeID,EmploymentType AS Description from EmploymentTypeReference  where EmploymentTypeID = " + datEmployeeDetails.Rows[0]["EmploymentType"] + "");
                            cboTransferFrom.ValueMember = "EmploymentTypeID";
                            cboTransferFrom.DisplayMember = "Description";
                            cboTransferFrom.DataSource = datCombos;
                            datCombos = null;

                            if (rdbFromRequest.Checked && MblnIsHrPowerEnabled)
                            {
                                if (dtRequsetDetails.Rows.Count > 0 && dtRequsetDetails.Rows[0]["TransferTypeId"].ToInt32() == 4)
                                {
                                    cboTransferTo.SelectedValue = dtRequsetDetails.Rows[0]["TransferTo"].ToInt32();
                                }
                                else
                                {
                                    cboTransferTo.DataSource = null;
                                }
                            }
                            break;
                    }

                    if (cboTransferFrom.DataSource != null)
                    {
                        if (cboTransferFrom.Items.Count != 0)
                        {
                            cboTransferFrom.SelectedIndex = 0;
                        }
                    }
      
                    cboTransferTo.Enabled = true;
                    if (rdbFromRequest.Checked && MblnIsHrPowerEnabled)
                    {
                        cboTransferTo.Enabled = false;
                        MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.IsFromRequest = 1;
                        MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.RequestId = dtRequsetDetails.Rows[0]["RequestId"].ToInt32();
                    }
                    else
                    {
                        cboTransferTo.Enabled = true;
                        cboTransferTo.SelectedIndex = -1;
                        MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.IsFromRequest = 0;
                        MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.RequestId = 0;
                    }

                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on FillTransferFromToCombo " + this.Name + " " + ex.Message.ToString(), 1);
                cboTransferTo.SelectedIndex = -1;
            }
            return true;

        }
        #endregion FillTransferFromToCombo
        #region ValidateEmployeeTransferMaster
        /// <summary>
        /// Validate Employee Transfer Master
        /// </summary>
        /// <returns>bool</returns>
        private bool ValidateEmployeeTransferMaster()
        {
            bool payflag;
            epEmployeeTransfer.Clear();

            if (cboEmployee.SelectedIndex == -1)
            {
                //''Please select an 'Employee'.
                MstrCommonMessage = UserMessage.GetMessageByCode(9851);
                epEmployeeTransfer.SetError(cboEmployee, MstrCommonMessage);
                UserMessage.ShowMessage(9851, null);
                TmrEmployeeTransfer.Enabled = true;
                cboEmployee.Focus();
                return false;
            }
            else
            {
                epEmployeeTransfer.SetError(cboEmployee, "");
            }

            if ((MobjclsBLLEmployeeTransfer.LoadCombos("SELECT WorkPolicyID FROM EmployeeMaster WHERE EmployeeID = " + cboEmployee.SelectedValue.ToInt32()).Rows.Count == 0) || (MobjclsBLLEmployeeTransfer.LoadCombos("SELECT WorkPolicyID FROM EmployeeMaster WHERE EmployeeID = " + cboEmployee.SelectedValue.ToInt32()).Rows[0]["WorkPolicyID"].ToInt32() <= 0))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(9899);
                UserMessage.ShowMessage(9899, null);
                TmrEmployeeTransfer.Enabled = true;
                cboEmployee.Focus();
                using (FrmEmployee objEmployee = new FrmEmployee())
                {
                    objEmployee.PintEmployeeID = cboEmployee.SelectedValue.ToInt32();
                    objEmployee.ShowDialog();
                }
                return false;
            }

            if (MintTransferTypeID == 4)
            {
                if (cboTransferFrom.SelectedIndex == -1)
                {
                    //'Employement Type' not set to this Employee,So transfer not possible. .
                    MstrCommonMessage = UserMessage.GetMessageByCode(9994);
                    epEmployeeTransfer.SetError(cboTransferFrom, MstrCommonMessage);
                    UserMessage.ShowMessage(9994, null);
                    TmrEmployeeTransfer.Enabled = true;
                    cboTransferFrom.Focus();
                    return false;
                }
                else
                {
                    epEmployeeTransfer.SetError(cboTransferTo, "");
                }
            }

            payflag = MobjclsBLLEmployeeTransfer.PaymentExist(Convert.ToInt32(cboEmployee.SelectedValue));

            if (cboTransferTo.SelectedIndex == -1)
            {
                //''Please select the area to where the employee is transferred - 'Transfer To'.
                MstrCommonMessage = UserMessage.GetMessageByCode(9854);
                epEmployeeTransfer.SetError(cboTransferTo, MstrCommonMessage);
                UserMessage.ShowMessage(9854, null);
                TmrEmployeeTransfer.Enabled = true;
                cboTransferTo.Focus();
                return false;
            }
            else
            {
                epEmployeeTransfer.SetError(cboTransferTo, "");
            }



            if (IdChk() == false)
            {
                //Please select the area to where the employee is transferred - 'Transfer To'.

                MstrCommonMessage = UserMessage.GetMessageByCode(9854);
                UserMessage.ShowMessage(9854, null);
                epEmployeeTransfer.SetError(cboTransferTo, MstrCommonMessage.Replace("#", ""));

                TmrEmployeeTransfer.Enabled = true;
                cboTransferTo.Focus();
                return false;
            }
            else
            {
                epEmployeeTransfer.SetError(cboTransferTo, "");
            }

            if (dtpTransferDate.Value == null)
            {
                //''Please select the Date.
                MstrCommonMessage = UserMessage.GetMessageByCode(9855);
                epEmployeeTransfer.SetError(dtpTransferDate, MstrCommonMessage);
                UserMessage.ShowMessage(9855, null);
                TmrEmployeeTransfer.Enabled = true;
                dtpTransferDate.Focus();
                return false;
            }
            else
            {
                epEmployeeTransfer.SetError(dtpTransferDate, "");
            }

            if (dtpTransferDate.Value.Date > System.DateTime.Now.Date)
            {
                //''The 'Date of Transfer' must be less than or equal to today.
                MstrCommonMessage = UserMessage.GetMessageByCode(9856);
                epEmployeeTransfer.SetError(dtpTransferDate, MstrCommonMessage);
                UserMessage.ShowMessage(9856, null);
                TmrEmployeeTransfer.Enabled = true;
                dtpTransferDate.Focus();
                return false;
            }
            else
            {
                epEmployeeTransfer.SetError(dtpTransferDate, "");
            }
            string strEOJDate=MobjclsBLLEmployeeTransfer.EmpDOJValidation(Convert.ToInt32(cboEmployee.SelectedValue));
            if (Convert.ToDateTime(strEOJDate) > dtpTransferDate.Value.Date)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(9864);
                UserMessage.ShowMessage(9864, null);
                return false;
            }
            try
            {
                if (MintTransferTypeID == 1)
                {
                    if (MobjclsBLLEmployeeTransfer.IsDefaultPolicy() == false)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(9866);
                        UserMessage.ShowMessage(9866, null);
                        return false;
                    }
                }

                //'---------------------------------------------------------------------------------
                DataTable DTable;

                DTable = MobjclsBLLEmployeeTransfer.PaymentReleaseChecking(Convert.ToInt32(cboEmployee.SelectedValue), dtpTransferDate.Value.ToString("dd-MMM-yyyy"));

                if (DTable.Rows.Count > 0 && payflag == true)
                {
                    if (Convert.ToInt32(DTable.Rows[0][0]) == 1)
                    {
                        UserMessage.ShowMessage(9882, null);
                        TmrEmployeeTransfer.Enabled = true;
                        dtpTransferDate.Focus();
                        return false;
                    }
                }
                DTable = null;

                string strTranferDate;
                strTranferDate = MobjclsBLLEmployeeTransfer.GetTransferDate(Convert.ToInt32(cboEmployee.SelectedValue));

                if (Convert.ToDateTime(strTranferDate) > dtpTransferDate.Value.Date)
                {
                    //''The 'Date of Transfer' must be greater than the previous transfer date.
                    MstrCommonMessage = UserMessage.GetMessageByCode(9858);
                    epEmployeeTransfer.SetError(dtpTransferDate, MstrCommonMessage.Replace("#", ""));
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TmrEmployeeTransfer.Enabled = true;
                    dtpTransferDate.Focus();
                    return false;
                }
                if (rdbFromRequest.Checked && MblnIsHrPowerEnabled)
                {
                    string strApprovedDate = MobjclsBLLEmployeeTransfer.GetApprovedDate(Convert.ToInt32(cboEmployee.SelectedValue));

                    if (Convert.ToDateTime(strApprovedDate).Date > dtpTransferDate.Value.Date)
                    {
                        //The 'Date of Transfer' must be greater than the Transfer Request Approved Date.
                        MstrCommonMessage = UserMessage.GetMessageByCode(9996);
                        epEmployeeTransfer.SetError(dtpTransferDate, MstrCommonMessage.Replace("#", ""));
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TmrEmployeeTransfer.Enabled = true;
                        dtpTransferDate.Focus();
                        return false;
                    }
                }
                if (rdbFromRequest.Checked && MblnIsHrPowerEnabled)
                {
                    string strApprovedDate = MobjclsBLLEmployeeTransfer.GetApprovedDate(Convert.ToInt32(cboEmployee.SelectedValue));

                    if (Convert.ToDateTime(strApprovedDate).Date > dtpTransferDate.Value.Date)
                    {
                        //The 'Date of Transfer' must be greater than the Transfer Request Approved Date.
                        MstrCommonMessage = UserMessage.GetMessageByCode(9996);
                        epEmployeeTransfer.SetError(dtpTransferDate, MstrCommonMessage.Replace("#", ""));
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TmrEmployeeTransfer.Enabled = true;
                        dtpTransferDate.Focus();
                        return false;
                    }
                }

                if (!rdbFromRequest.Checked && MblnIsHrPowerEnabled)
                {
                    if (MobjclsBLLEmployeeTransfer.IsTransferRequestExists(Convert.ToInt32(cboEmployee.SelectedValue), MintTransferTypeID))
                    {
                        //''Transfer Request Exists'.
                        MstrCommonMessage = UserMessage.GetMessageByCode(9997);
                        epEmployeeTransfer.SetError(dtpTransferDate, MstrCommonMessage.Replace("#", ""));
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TmrEmployeeTransfer.Enabled = true;
                        dtpTransferDate.Focus();
                        return false;
                    }
                }
                

                if (MobjclsBLLEmployeeTransfer.IsLeaveDateExists(Convert.ToInt32(cboEmployee.SelectedValue), dtpTransferDate.Value.ToString("dd-MMM-yyyy")) == true && payflag == true)
                {
                    //''The 'Transfer Date' must be greater than the 'Leave date'.
                    MstrCommonMessage = UserMessage.GetMessageByCode(9883);
                    epEmployeeTransfer.SetError(dtpTransferDate, MstrCommonMessage.Replace("#", ""));
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TmrEmployeeTransfer.Enabled = true;
                    dtpTransferDate.Focus();
                    return false;
                }
                if (IsAttendanceExisting())
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(9995);
                    epEmployeeTransfer.SetError(dtpTransferDate, MstrCommonMessage.Replace("#", ""));
                    MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TmrEmployeeTransfer.Enabled = true;
                    dtpTransferDate.Focus();
                    return false;
                }
                int MonthID;
                int YearID;

                MonthID = dtpTransferDate.Value.Date.Month;
                YearID = dtpTransferDate.Value.Date.Month;

                if (cboTransferFrom.SelectedIndex != -1)
                {
                    FromId = Convert.ToInt32(cboTransferFrom.SelectedValue);
                }
                if (MintTransferTypeID == 1)
                {
                    if (MobjclsBLLEmployeeTransfer.IsLoanExists(Convert.ToInt32(cboEmployee.SelectedValue)) == true)
                    {
                        ////This employee cannot be transferred as he has not settled his loan. 
                        //MstrCommonMessage = UserMessage.GetMessageByCode(9868);
                        //MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //return false;
                    }

                }

                string StrFromDate;
                string StrToDate;
                DateTime TraDate;
                DateTime TraDate1;
                DateTime PreviousMonthDate; int intSalaryDay; int intTransferDay;

                PreviousMonthDate = DateAndTime.DateAdd(DateInterval.Month, -1, dtpTransferDate.Value.Date);
                intSalaryDay = MobjclsBLLEmployeeTransfer.GetSalaryDay(Convert.ToInt32(cboEmployee.SelectedValue));
                if (intSalaryDay == 0)
                {
                    intSalaryDay = 1;
                }

                intTransferDay = dtpTransferDate.Value.Date.Day;
                if (intSalaryDay <= intTransferDay)
                {
                    StrFromDate = intSalaryDay.ToString() + "-" + GetCurrentMonth(DateAndTime.Month(dtpTransferDate.Value.Date)) + "-" + DateAndTime.Year(dtpTransferDate.Value.Date) + "";
                    TraDate = Convert.ToDateTime(intSalaryDay.ToString() + "-" + GetCurrentMonth(DateAndTime.Month(dtpTransferDate.Value.Date)) + "-" + DateAndTime.Year(dtpTransferDate.Value.Date) + "");
                }
                else
                {
                    StrFromDate = intSalaryDay.ToString() + "-" + GetCurrentMonth(DateAndTime.Month(PreviousMonthDate)) + "-" + DateAndTime.Year(PreviousMonthDate) + "";
                    TraDate = Convert.ToDateTime(intSalaryDay.ToString() + "-" + GetCurrentMonth(DateAndTime.Month(PreviousMonthDate)) + "-" + DateAndTime.Year(PreviousMonthDate) + "");
                }

                TraDate1 = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, TraDate.Date));
                StrToDate = Convert.ToString(TraDate1.Day) + "-" + GetCurrentMonth(DateAndTime.Month(TraDate1.Date)) + "-" + DateAndTime.Year(TraDate1.Date) + "";

                if (MintTransferTypeID == 1 && payflag == true)
                {
                    if (MobjclsBLLEmployeeTransfer.GetStatus(5, Convert.ToInt32(cboEmployee.SelectedValue), "", "") == 2)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(9869);
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (MintTransferTypeID == 1 && payflag == true)
                {
                    if (MobjclsBLLEmployeeTransfer.GetStatus(6, Convert.ToInt32(cboEmployee.SelectedValue), "", "") == 3)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(9870);
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (MintTransferTypeID == 1)
                {
                    if (MobjclsBLLEmployeeTransfer.GetStatus(7, Convert.ToInt32(cboEmployee.SelectedValue), "", "") == 1)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(9881);
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (MintTransferTypeID == 1 && payflag == true)
                {
                    if (MobjclsBLLEmployeeTransfer.GetStatus(0, Convert.ToInt32(cboEmployee.SelectedValue), StrFromDate, StrToDate) == 2)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(9888);
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (rdbDirect.Checked)
                {
                    DataTable dt = MobjclsBLLEmployeeTransfer.LoadCombos("SELECT 1 FROM HRTransferRequest WHERE StatusID NOT in(6,7,3) and EmployeeID =" + cboEmployee.SelectedValue.ToInt32() + " AND TransferTypeId= " + MintTransferTypeID + " ");
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(9997);
                        MessageBox.Show(MstrCommonMessage.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
                return true;

            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on ValidateEmployeeTransferMaster " + this.Name + " " + Ex.Message.ToString(), 1);
                return false;
            }
        }
        #endregion ValidateEmployeeTransferMaster
        #region IdChk
        /// <summary>
        /// Checking where the employee is transferred
        /// </summary>
        /// <returns></returns>
        private bool IdChk()
        {
            try
            {
                int II;
                II = Convert.ToInt32(MintTransferTypeID);
                return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on IdChk " + this.Name + " " + ex.Message.ToString(), 1);
                return false;
            }
        }
        #endregion IdChk
        #region GetCurrentMonth
        /// <summary>
        /// Function to get Current Month
        /// </summary>
        /// <param name="MonthVal"></param>
        /// <returns>string</returns>
        private string GetCurrentMonth(int MonthVal)
        {
            string Months;
            Months = "";
            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }
        #endregion GetCurrentMonth
        #region IsAttendanceExists
        /// <summary>
        /// Checks Attendance Exists for the Employee
        /// </summary>
        /// <returns></returns>
        private bool IsAttendanceExists()
        {
            int intEmployeeID = cboEmployee.SelectedValue.ToInt32();
            int intCompanyID = cboTransferFrom.SelectedValue.ToInt32();

            DataTable datAttendance = new clsBLLSalaryPayment().FillCombos(new string[] { "COUNT(1) AS Count", "PayAttendanceMaster", "EmployeeID = " + intEmployeeID + " AND CONVERT(DATETIME,CONVERT(VARCHAR,Date,106)) >= CONVERT(DATETIME,'" + dtpTransferDate.Value.ToString("dd-MMM-yyyy") + "',106)" });
            if (datAttendance != null && datAttendance.Rows.Count > 0 && datAttendance.Rows[0]["Count"].ToInt32() > 0)
                return true;

            return false;
        }
        private bool IsAttendanceExisting()
        {
            int intEmployeeID = cboEmployee.SelectedValue.ToInt32();
            int intCompanyID = cboTransferFrom.SelectedValue.ToInt32();

            DataTable datAttendance = new clsBLLSalaryPayment().FillCombos(new string[] { "COUNT(1) AS Count", "PayAttendanceMaster", "EmployeeID = " + intEmployeeID + " AND CONVERT(DATETIME,CONVERT(VARCHAR,Date,106)) >= CONVERT(DATETIME,'" + dtpTransferDate.Value.ToString("dd-MMM-yyyy") + "',106) AND AttendenceStatusID=1" });
            if (datAttendance != null && datAttendance.Rows.Count > 0 && datAttendance.Rows[0]["Count"].ToInt32() > 0)
                return true;

            return false;
        }
        #endregion IsAttendanceExists
        #region DeleteAttendance
        private bool DeleteAttendance()
        {
            try
            {
                int intEmployeeID = cboEmployee.SelectedValue.ToInt32();
                DateTime dtTransferDate = dtpTransferDate.Value;
                return MobjclsBLLEmployeeTransfer.DeleteAttendance(intEmployeeID, dtTransferDate);
            }
            catch
            {
                return false;
            }

        }
        #endregion DeleteAttendance
        #region ValidateEmployeeTransferDetails
        /// <summary>
        /// Validate Employee Transfer Details
        /// </summary>
        /// <returns>bool</returns>
        private bool ValidateEmployeeTransferDetails()
        {
            try
            {
                
                if (cboTransactionType.SelectedIndex == -1)
                {
                    //Please select a Transaction type.
                    UserMessage.ShowMessage(9872, null);
                    cboTransactionType.Focus();
                    return false;
                }
                if (cboWorkPolicy.SelectedIndex == -1)
                {
                    //Please select a Work Policy.
                    UserMessage.ShowMessage(9873, null);
                    cboWorkPolicy.Focus();
                    return false;
                }
                if (!ChkTemp.Checked)
                {
                    if (Convert.ToInt32(cboTransactionType.SelectedValue) == 1)
                    {
                        if (cboEmployerBank.SelectedIndex == -1 && cboEmployerBank.Enabled == true)
                        {
                            //Please select a Bank Name.
                            UserMessage.ShowMessage(9874, null);
                            cboEmployerBank.Focus();
                            return false;
                        }


                        if (cboEmployerBankAct.SelectedIndex == -1 && cboEmployerBankAct.Enabled == true)
                        {
                            // Please select a Company Bank Account.

                            UserMessage.ShowMessage(9875, null);
                            cboEmployerBankAct.Focus();
                            return false;
                        }

                        if (txtEmployeeAntNo.Text.Trim() == "" && txtEmployeeAntNo.Enabled == true)
                        {
                            //Please enter an Account Number.

                            UserMessage.ShowMessage(9876, null);
                            txtEmployeeAntNo.Focus();
                            return false;

                        }

                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on ValidateEmployeeTransferDetails " + this.Name + " " + ex.Message.ToString(), 1);
                return false;
            }



        }
        #endregion ValidateEmployeeTransferDetails
        #region FillParameters
        private void FillParameters()
        {
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.TransferTypeID = MintTransferTypeID;
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.EmployeeID = Convert.ToInt32(cboEmployee.SelectedValue);
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.TransferFromID = Convert.ToInt32(cboTransferFrom.SelectedValue);
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.TransferToID = Convert.ToInt32(cboTransferTo.SelectedValue);
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.OtherInfo = txtOtherInfo.Text;
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.TransferDate = dtpTransferDate.Value.Date;
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.TransactionTypeID = Convert.ToInt32(cboTransactionType.SelectedValue);
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.PolicyID = Convert.ToInt32(cboWorkPolicy.SelectedValue);
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.LeavePolicyID = Convert.ToInt32(cboLeavePolicy.SelectedValue);
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.EmployerBankID = Convert.ToInt32(cboEmployerBank.SelectedValue);
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.EmployerAccountID = Convert.ToInt32(cboEmployerBankAct.SelectedValue);
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.EmployeeBankID = Convert.ToInt32(cboEmployeeBank.SelectedValue);
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.EmployeeAccountNo = txtEmployeeAntNo.Text;
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.MachineName = GetHostName().Trim();
            MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.LocationID = Convert.ToInt32(cboWorkLocation.SelectedValue);

            if (ChkTemp.Checked )
            {
                MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.iTemp = 1;
            }
            else
            {
                MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.iTemp = 0;
            }


            if (MintTransferTypeID == 1)
            {
                MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.CompanyID = Convert.ToInt32(cboTransferTo.SelectedValue);
            }
            else if (MintTransferTypeID == 2)
            {
                MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.DepartmentID = Convert.ToInt32(cboTransferTo.SelectedValue);
            }
            else if (MintTransferTypeID == 3)
            {
                MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.DesignationID = Convert.ToInt32(cboTransferTo.SelectedValue);
            }
            else if (MintTransferTypeID == 4)
            {
                MobjclsBLLEmployeeTransfer.clsDTOEmployeeTransfer.EmploymentTypeID = Convert.ToInt32(cboTransferTo.SelectedValue);
            }

        }
        #endregion FillParameters
        #region GetHostName
        /// <summary>
        /// Function to get Host Name
        /// </summary>
        /// <returns>string</returns>
        public string GetHostName()
        {
            return System.Net.Dns.GetHostName().ToString();
        }
        #endregion GetHostName
        public void Changestatus()
        {
      
        }
        private void cboWorkLocation_KeyDown(object sender, KeyEventArgs e)
        {
            cboWorkLocation.DroppedDown = false;
        }

        
        #endregion Methods

        private void rdbFromRequest_CheckedChanged(object sender, EventArgs e)
        {
            LoadEmployee();
        }

        private void rdbDirect_CheckedChanged(object sender, EventArgs e)
        {
            LoadEmployee();
        }

        private void ChkTemp_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //if (ChkTemp.Checked == true)
                //{
                //    WizardEmployeeInfo.Visible = false;
                //}
                //else
                //{
                //    WizardEmployeeInfo.Visible = true;
                //}
            }
            catch
            {
            }
        }

  



       
        
    }
      
}