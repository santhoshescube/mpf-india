﻿namespace MyPayfriend
{
    partial class FrmLoanRepayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label DateLabel;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label9;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLoanRepayment));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.EmployeeExpenseBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAddNew = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatusShow = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.lblDeductedFromSalary = new System.Windows.Forms.Label();
            this.chkLoanClosed = new System.Windows.Forms.CheckBox();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblBalanceAmountDue = new System.Windows.Forms.Label();
            this.lblTotalLoanAmount = new System.Windows.Forms.Label();
            this.lblLoanDate = new System.Windows.Forms.Label();
            this.lblRepaidAmount = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cboLoanNumber = new System.Windows.Forms.ComboBox();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.txtPrincipleAmount = new System.Windows.Forms.TextBox();
            this.dgvInstallments = new System.Windows.Forms.DataGridView();
            this.IsChecked = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.InstalmentNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InstalmentNoAndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BalanceAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrincipleAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RepaidAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPrincipleAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnteredAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.errValidate = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtBalanceAmount = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            DateLabel = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeExpenseBindingNavigator)).BeginInit();
            this.EmployeeExpenseBindingNavigator.SuspendLayout();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.GrpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstallments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(13, 52);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(51, 13);
            label1.TabIndex = 12;
            label1.Text = "Loan No.";
            // 
            // DateLabel
            // 
            DateLabel.AutoSize = true;
            DateLabel.Location = new System.Drawing.Point(541, 48);
            DateLabel.Name = "DateLabel";
            DateLabel.Size = new System.Drawing.Size(67, 13);
            DateLabel.TabIndex = 13;
            DateLabel.Text = "Repaid Date";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(7, 247);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(49, 13);
            label4.TabIndex = 31;
            label4.Text = "Remarks";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(13, 212);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(49, 13);
            label9.TabIndex = 28;
            label9.Text = "Currency";
            // 
            // EmployeeExpenseBindingNavigator
            // 
            this.EmployeeExpenseBindingNavigator.AddNewItem = null;
            this.EmployeeExpenseBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeeExpenseBindingNavigator.DeleteItem = null;
            this.EmployeeExpenseBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.btnAddNew,
            this.btnSaveItem,
            this.btnDelete,
            this.btnClear,
            this.toolStripSeparator1,
            this.btnPrint,
            this.btnEmail,
            this.toolStripSeparator2,
            this.btnHelp});
            this.EmployeeExpenseBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeExpenseBindingNavigator.MoveFirstItem = null;
            this.EmployeeExpenseBindingNavigator.MoveLastItem = null;
            this.EmployeeExpenseBindingNavigator.MoveNextItem = null;
            this.EmployeeExpenseBindingNavigator.MovePreviousItem = null;
            this.EmployeeExpenseBindingNavigator.Name = "EmployeeExpenseBindingNavigator";
            this.EmployeeExpenseBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeeExpenseBindingNavigator.Size = new System.Drawing.Size(767, 25);
            this.EmployeeExpenseBindingNavigator.TabIndex = 4;
            this.EmployeeExpenseBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAddNew
            // 
            this.btnAddNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddNew.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNew.Image")));
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.RightToLeftAutoMirrorImage = true;
            this.btnAddNew.Size = new System.Drawing.Size(23, 22);
            this.btnAddNew.Text = "Add new";
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save Data";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusShow,
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 516);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(767, 22);
            this.ssStatus.TabIndex = 5;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblStatusShow
            // 
            this.lblStatusShow.Name = "lblStatusShow";
            this.lblStatusShow.Size = new System.Drawing.Size(48, 17);
            this.lblStatusShow.Text = "Status : ";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // tmrClear
            // 
            this.tmrClear.Interval = 2000;
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(767, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 90;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.label16);
            this.GrpMain.Controls.Add(this.label15);
            this.GrpMain.Controls.Add(this.txtBalanceAmount);
            this.GrpMain.Controls.Add(this.lblDeductedFromSalary);
            this.GrpMain.Controls.Add(this.chkLoanClosed);
            this.GrpMain.Controls.Add(this.chkSelectAll);
            this.GrpMain.Controls.Add(this.label14);
            this.GrpMain.Controls.Add(this.label10);
            this.GrpMain.Controls.Add(this.label8);
            this.GrpMain.Controls.Add(this.label13);
            this.GrpMain.Controls.Add(this.label5);
            this.GrpMain.Controls.Add(this.label12);
            this.GrpMain.Controls.Add(this.label3);
            this.GrpMain.Controls.Add(this.label2);
            this.GrpMain.Controls.Add(this.label11);
            this.GrpMain.Controls.Add(this.lblBalanceAmountDue);
            this.GrpMain.Controls.Add(this.lblTotalLoanAmount);
            this.GrpMain.Controls.Add(this.lblLoanDate);
            this.GrpMain.Controls.Add(this.lblRepaidAmount);
            this.GrpMain.Controls.Add(label9);
            this.GrpMain.Controls.Add(label4);
            this.GrpMain.Controls.Add(this.txtRemarks);
            this.GrpMain.Controls.Add(DateLabel);
            this.GrpMain.Controls.Add(this.dtpDate);
            this.GrpMain.Controls.Add(this.cboLoanNumber);
            this.GrpMain.Controls.Add(label1);
            this.GrpMain.Controls.Add(this.lblCurrency);
            this.GrpMain.Controls.Add(this.txtPrincipleAmount);
            this.GrpMain.Controls.Add(this.dgvInstallments);
            this.GrpMain.Controls.Add(this.lblEmployee);
            this.GrpMain.Controls.Add(this.Label7);
            this.GrpMain.Controls.Add(this.label6);
            this.GrpMain.Controls.Add(this.cboEmployee);
            this.GrpMain.Controls.Add(this.shapeContainer1);
            this.GrpMain.Location = new System.Drawing.Point(7, 56);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(754, 428);
            this.GrpMain.TabIndex = 0;
            this.GrpMain.TabStop = false;
            // 
            // lblDeductedFromSalary
            // 
            this.lblDeductedFromSalary.AutoSize = true;
            this.lblDeductedFromSalary.Location = new System.Drawing.Point(541, 21);
            this.lblDeductedFromSalary.Name = "lblDeductedFromSalary";
            this.lblDeductedFromSalary.Size = new System.Drawing.Size(135, 13);
            this.lblDeductedFromSalary.TabIndex = 11;
            this.lblDeductedFromSalary.Text = "Deducted From Salary : No";
            // 
            // chkLoanClosed
            // 
            this.chkLoanClosed.AutoSize = true;
            this.chkLoanClosed.Location = new System.Drawing.Point(689, 77);
            this.chkLoanClosed.Name = "chkLoanClosed";
            this.chkLoanClosed.Size = new System.Drawing.Size(58, 17);
            this.chkLoanClosed.TabIndex = 3;
            this.chkLoanClosed.Text = "Closed";
            this.chkLoanClosed.UseVisualStyleBackColor = true;
            this.chkLoanClosed.CheckedChanged += new System.EventHandler(this.chkLoanClosed_CheckedChanged);
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(264, 103);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(15, 14);
            this.chkSelectAll.TabIndex = 27;
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(92, 212);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(10, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Loan Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(92, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = ":";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(92, 160);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(92, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(92, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = ":";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Balance Due";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Repaid";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 131);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Total Loan";
            // 
            // lblBalanceAmountDue
            // 
            this.lblBalanceAmountDue.AutoSize = true;
            this.lblBalanceAmountDue.Location = new System.Drawing.Point(108, 185);
            this.lblBalanceAmountDue.Name = "lblBalanceAmountDue";
            this.lblBalanceAmountDue.Size = new System.Drawing.Size(69, 13);
            this.lblBalanceAmountDue.TabIndex = 26;
            this.lblBalanceAmountDue.Text = "Balance Due";
            // 
            // lblTotalLoanAmount
            // 
            this.lblTotalLoanAmount.AutoSize = true;
            this.lblTotalLoanAmount.Location = new System.Drawing.Point(108, 131);
            this.lblTotalLoanAmount.Name = "lblTotalLoanAmount";
            this.lblTotalLoanAmount.Size = new System.Drawing.Size(58, 13);
            this.lblTotalLoanAmount.TabIndex = 23;
            this.lblTotalLoanAmount.Text = "Total Loan";
            // 
            // lblLoanDate
            // 
            this.lblLoanDate.AutoSize = true;
            this.lblLoanDate.Location = new System.Drawing.Point(108, 104);
            this.lblLoanDate.Name = "lblLoanDate";
            this.lblLoanDate.Size = new System.Drawing.Size(57, 13);
            this.lblLoanDate.TabIndex = 17;
            this.lblLoanDate.Text = "Loan Date";
            // 
            // lblRepaidAmount
            // 
            this.lblRepaidAmount.AutoSize = true;
            this.lblRepaidAmount.Location = new System.Drawing.Point(108, 158);
            this.lblRepaidAmount.Name = "lblRepaidAmount";
            this.lblRepaidAmount.Size = new System.Drawing.Size(41, 13);
            this.lblRepaidAmount.TabIndex = 20;
            this.lblRepaidAmount.Text = "Repaid";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(6, 263);
            this.txtRemarks.MaxLength = 450;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRemarks.Size = new System.Drawing.Size(234, 133);
            this.txtRemarks.TabIndex = 7;
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(614, 45);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(106, 20);
            this.dtpDate.TabIndex = 2;
            this.dtpDate.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // cboLoanNumber
            // 
            this.cboLoanNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLoanNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLoanNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboLoanNumber.DropDownHeight = 134;
            this.cboLoanNumber.FormattingEnabled = true;
            this.cboLoanNumber.IntegralHeight = false;
            this.cboLoanNumber.Location = new System.Drawing.Point(79, 49);
            this.cboLoanNumber.Name = "cboLoanNumber";
            this.cboLoanNumber.Size = new System.Drawing.Size(137, 21);
            this.cboLoanNumber.TabIndex = 1;
            this.cboLoanNumber.SelectedIndexChanged += new System.EventHandler(this.cboLoanNumber_SelectedIndexChanged);
            this.cboLoanNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboLoanNumber.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrency.Location = new System.Drawing.Point(108, 212);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(28, 13);
            this.lblCurrency.TabIndex = 30;
            this.lblCurrency.Text = "CUR";
            // 
            // txtPrincipleAmount
            // 
            this.txtPrincipleAmount.BackColor = System.Drawing.Color.White;
            this.txtPrincipleAmount.Enabled = false;
            this.txtPrincipleAmount.Location = new System.Drawing.Point(591, 402);
            this.txtPrincipleAmount.Name = "txtPrincipleAmount";
            this.txtPrincipleAmount.ReadOnly = true;
            this.txtPrincipleAmount.Size = new System.Drawing.Size(156, 20);
            this.txtPrincipleAmount.TabIndex = 5;
            this.txtPrincipleAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrincipleAmount.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            this.txtPrincipleAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // dgvInstallments
            // 
            this.dgvInstallments.AllowUserToAddRows = false;
            this.dgvInstallments.AllowUserToDeleteRows = false;
            this.dgvInstallments.AllowUserToResizeColumns = false;
            this.dgvInstallments.AllowUserToResizeRows = false;
            this.dgvInstallments.BackgroundColor = System.Drawing.Color.White;
            this.dgvInstallments.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvInstallments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInstallments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IsChecked,
            this.InstalmentNo,
            this.InstalmentNoAndDate,
            this.BalanceAmount,
            this.PrincipleAmount,
            this.RepaidAmount,
            this.TotalPrincipleAmount,
            this.EnteredAmount});
            this.dgvInstallments.Location = new System.Drawing.Point(255, 100);
            this.dgvInstallments.Name = "dgvInstallments";
            this.dgvInstallments.RowHeadersVisible = false;
            this.dgvInstallments.Size = new System.Drawing.Size(492, 296);
            this.dgvInstallments.TabIndex = 4;
            this.dgvInstallments.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstallments_CellValueChanged);
            this.dgvInstallments.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvInstallments_EditingControlShowing);
            this.dgvInstallments.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvInstallments_CurrentCellDirtyStateChanged);
            this.dgvInstallments.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvInstallments_DataError);
            // 
            // IsChecked
            // 
            this.IsChecked.DataPropertyName = "IsChecked";
            this.IsChecked.HeaderText = "";
            this.IsChecked.MinimumWidth = 25;
            this.IsChecked.Name = "IsChecked";
            this.IsChecked.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsChecked.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IsChecked.Width = 25;
            // 
            // InstalmentNo
            // 
            this.InstalmentNo.DataPropertyName = "InstalmentNo";
            this.InstalmentNo.HeaderText = "InstalmentNo";
            this.InstalmentNo.Name = "InstalmentNo";
            this.InstalmentNo.ReadOnly = true;
            this.InstalmentNo.Visible = false;
            // 
            // InstalmentNoAndDate
            // 
            this.InstalmentNoAndDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.InstalmentNoAndDate.DataPropertyName = "InstalmentNoAndDate";
            this.InstalmentNoAndDate.HeaderText = "Instalment No. & Date";
            this.InstalmentNoAndDate.Name = "InstalmentNoAndDate";
            this.InstalmentNoAndDate.ReadOnly = true;
            // 
            // BalanceAmount
            // 
            this.BalanceAmount.DataPropertyName = "BalanceAmount";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BalanceAmount.DefaultCellStyle = dataGridViewCellStyle3;
            this.BalanceAmount.HeaderText = "Balance Amount";
            this.BalanceAmount.MinimumWidth = 150;
            this.BalanceAmount.Name = "BalanceAmount";
            this.BalanceAmount.ReadOnly = true;
            this.BalanceAmount.Width = 150;
            // 
            // PrincipleAmount
            // 
            this.PrincipleAmount.DataPropertyName = "PrincipleAmount";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PrincipleAmount.DefaultCellStyle = dataGridViewCellStyle4;
            this.PrincipleAmount.HeaderText = "Amount";
            this.PrincipleAmount.MaxInputLength = 12;
            this.PrincipleAmount.MinimumWidth = 150;
            this.PrincipleAmount.Name = "PrincipleAmount";
            this.PrincipleAmount.Width = 150;
            // 
            // RepaidAmount
            // 
            this.RepaidAmount.DataPropertyName = "RepaidAmount";
            this.RepaidAmount.HeaderText = "RepaidAmount";
            this.RepaidAmount.Name = "RepaidAmount";
            this.RepaidAmount.ReadOnly = true;
            this.RepaidAmount.Visible = false;
            // 
            // TotalPrincipleAmount
            // 
            this.TotalPrincipleAmount.DataPropertyName = "TotalPrincipleAmount";
            this.TotalPrincipleAmount.HeaderText = "TotalPrincipleAmount";
            this.TotalPrincipleAmount.Name = "TotalPrincipleAmount";
            this.TotalPrincipleAmount.ReadOnly = true;
            this.TotalPrincipleAmount.Visible = false;
            // 
            // EnteredAmount
            // 
            this.EnteredAmount.DataPropertyName = "EnteredAmount";
            this.EnteredAmount.HeaderText = "EnteredAmount";
            this.EnteredAmount.Name = "EnteredAmount";
            this.EnteredAmount.ReadOnly = true;
            this.EnteredAmount.Visible = false;
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(13, 25);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblEmployee.TabIndex = 10;
            this.lblEmployee.Text = "Employee";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(7, 0);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(99, 13);
            this.Label7.TabIndex = 8;
            this.Label7.Text = "Repayment Info";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Loan Status";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(79, 22);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(233, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboEmployee.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(748, 409);
            this.shapeContainer1.TabIndex = 9;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.Color.LightGray;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 28;
            this.lineShape1.X2 = 683;
            this.lineShape1.Y1 = 71;
            this.lineShape1.Y2 = 69;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(4, 490);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(598, 490);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(679, 490);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // errValidate
            // 
            this.errValidate.ContainerControl = this;
            this.errValidate.RightToLeft = true;
            // 
            // txtBalanceAmount
            // 
            this.txtBalanceAmount.BackColor = System.Drawing.Color.White;
            this.txtBalanceAmount.Enabled = false;
            this.txtBalanceAmount.Location = new System.Drawing.Point(325, 402);
            this.txtBalanceAmount.Name = "txtBalanceAmount";
            this.txtBalanceAmount.ReadOnly = true;
            this.txtBalanceAmount.Size = new System.Drawing.Size(156, 20);
            this.txtBalanceAmount.TabIndex = 32;
            this.txtBalanceAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(252, 405);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 33;
            this.label15.Text = "Balance Amt";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(517, 405);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 13);
            this.label16.TabIndex = 34;
            this.label16.Text = "Principle Amt";
            // 
            // FrmLoanRepayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 538);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.EmployeeExpenseBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLoanRepayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loan Repayment";
            this.Load += new System.EventHandler(this.FrmLoanRepayment_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLoanRepayment_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLoanRepayment_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeExpenseBindingNavigator)).EndInit();
            this.EmployeeExpenseBindingNavigator.ResumeLayout(false);
            this.EmployeeExpenseBindingNavigator.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstallments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator EmployeeExpenseBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton btnAddNew;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripButton btnClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatusShow;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Timer tmrClear;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Label lblCurrency;
        internal System.Windows.Forms.ComboBox cboLoanNumber;
        internal System.Windows.Forms.DateTimePicker dtpDate;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Label lblTotalLoanAmount;
        internal System.Windows.Forms.Label lblRepaidAmount;
        internal System.Windows.Forms.Label lblBalanceAmountDue;
        internal System.Windows.Forms.Label lblLoanDate;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label lblDeductedFromSalary;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        internal System.Windows.Forms.Label label12;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblEmployee;
        private System.Windows.Forms.ErrorProvider errValidate;
        private System.Windows.Forms.DataGridView dgvInstallments;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.CheckBox chkSelectAll;
        internal System.Windows.Forms.CheckBox chkLoanClosed;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsChecked;
        private System.Windows.Forms.DataGridViewTextBoxColumn InstalmentNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn InstalmentNoAndDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BalanceAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrincipleAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn RepaidAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPrincipleAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnteredAmount;
        internal System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtBalanceAmount;
        private System.Windows.Forms.TextBox txtPrincipleAmount;
        internal System.Windows.Forms.Label label16;
    }
}