﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Globalization;

     /*******************************************
     *  Created By    : SRUTHY K
     *  Created Date  : 05 July 2013
     *  Purpose       : Add/Update/Delete Loan
     *  Modified By   : Siny
     *  Modified Date : 23 Aug 2013
     *  Modification  :Change in calculation
     *  FormID        : 165
     ********************************************/

namespace MyPayfriend
{
    public partial class FrmEmployeeLoan : Form
    {
        #region Declarations
        public string PstrLoanNumberPrefix;
        ArrayList MaStatusMessage;
        private ArrayList MaMessageArr;
        private string MstrMessageCommon;
        private MessageBoxIcon MmsgMessageIcon;
        string strCondition = string.Empty;
        private int MintCompanyID;
        private int MintScale;
        private int MintScaleZ=0;
        private int MintCurrencyID;
        private bool MblnAddStatus;
        private decimal MdecInstallmentAmt = 0;
        private decimal MdecBasicPay = 0;
        private string MstrSearch = "";
        private int MintRecordCount;
        private int MintCurrentRecordCount;
        private bool MblnGridChangeStatus;
        private decimal MdecPaidAmount = 0;             //To set installment amount already paid
        private decimal MdecTotalPaidAmount = 0;        //To set Total installment amount already paid
        private int MintPaidCount = 0;                  //To set total installments paid  
        private bool MblnPrintEmailPermission = false;  //To set Print Email Permission
        private bool MblnAddPermission = false;         //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        public long EmployeeID { get; set; }

        private Control control = null;

        ClsNotification MobjNotification;
        ClsCommonUtility MobjClsCommonUtility;
        clsBLLEmployeeLoan MobjClsBLLEmployeeLoan;
        string strBindingOf = "Of ";
        #endregion

        #region Constructor
        public FrmEmployeeLoan()
        {
            InitializeComponent();
            MobjNotification = new ClsNotification();
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjClsBLLEmployeeLoan = new clsBLLEmployeeLoan();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void CompanySettings(int CompanyID)
        {
            try
            {
                DataTable DT;
                DT = MobjClsBLLEmployeeLoan.GetCompanySetting(CompanyID);
                if (DT.Rows.Count > 0)
                {
                    PstrLoanNumberPrefix = DT.Rows[0]["Prefix"].ToString();
                }
                else
                {
                    PstrLoanNumberPrefix = "";
                }
            }
            catch
            {
            }
        }

        private void GenerateLoanCode(int ComId)
        {
            try
            {
                CompanySettings(ComId);
                txtLoanNumber.Text = PstrLoanNumberPrefix + (MobjClsBLLEmployeeLoan.GenerateLoanCode(ComId)).ToString();

            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.EmployeeLoan, this);

            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            strBindingOf = "من ";
        }
        #endregion Constructor

        #region SetPermissions
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID>3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Employee, (Int32)eMenuID.Loan, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            }

            if (MblnAddStatus)
            {
                if (ClsCommonSettings.IsHrPowerEnabled)
                    MblnAddPermission = MblnUpdatePermission = MblnDeletePermission = false;
            }
            


        }
        #endregion SetPermissions

        #region SetEnability
        private void SetEnability()
        {
            if (MblnAddStatus)
            {
                BindingNavigatorAddNewItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;
                BtnPrint.Enabled = false;
                BtnEmail.Enabled = false;
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnAddPermission;
            }
            else
            {
                BindingNavigatorAddNewItem.Enabled = true;
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnUpdatePermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
            }
        }
        #endregion SetEnability

        #region DisplayLoan
        /// <summary>
        /// Display loandetails
        /// </summary>
        private void DisplayLoan()
        {
            btnClear.Enabled = false;
            if (MintRecordCount > 0)
            {
                MblnAddStatus = false;

                MobjClsBLLEmployeeLoan = new clsBLLEmployeeLoan();
                MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan = MobjClsBLLEmployeeLoan.GetLoan(BindingNavigatorPositionItem.Text.ToInt32(), EmployeeID, MstrSearch);

                if (MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan != null)
                {

                    txtLoanNumber.Tag = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intLoanID;
                    LoadCombos(3);
                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        MintScaleZ = 0;
                    }
                    cboEmployee.SelectedValue = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intEmployeeID;
                    cboEmployee.Enabled = false;
                    cboCurrency.SelectedValue = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intCurrencyID;
                    txtLoanNumber.Text = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.strLoanNumber;
                    cboLoanType.SelectedValue = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intLoanTypeID;
                    dtpLoanDate.Value = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtLoanDate;
                    cboInterestType.SelectedValue = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intInterestTypeID;
                    txtInterestRate.Text = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.decInterestRate.ToString("F" + MintScale);
                    dtpDeductStartDate.Value = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtDeductStartDate;
                    txtNoOfInstallments.Text = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intNoOfInstallments.ToStringCustom();
                    txtAmount.Text = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.decAmount.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                    txtPaymentAmount.Text = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.decPaymentAmount.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                    txtReason.Text = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.strReason.Trim();
                    lblBalanceAmount.Text ="Balance Amt : " + MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.decBalanceAmount.ToString() ;
   
                    if (MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.blnClosed)
                    {
                        chkClosed.Checked = true;
                        chkClosed.Enabled = false;
                    }
                    else
                    {
                        chkClosed.Checked = false;
                        chkClosed.Enabled = true;
                    }

                    int iCounter = 0;
                    dgvInstallmentDetails.Rows.Clear();
                    MdecPaidAmount = MintPaidCount = 0;
                    MdecTotalPaidAmount = 0;
                    foreach (clsDTOEmployeeLoanInstallmentDetails objDTOInstallmentDetails in MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.objClsDTOEmployeeLoanInstallmentDetails)
                    {
                        dgvInstallmentDetails.Rows.Add();

                        if (ClsCommonSettings.IsArabicView)
                            dgvInstallmentDetails.Rows[iCounter].Cells["InstallmentNo"].Value = "أقساط." + objDTOInstallmentDetails.InstallmentNo;
                        else
                            dgvInstallmentDetails.Rows[iCounter].Cells["InstallmentNo"].Value = "Installments" + objDTOInstallmentDetails.InstallmentNo;

                        dgvInstallmentDetails.Rows[iCounter].Cells["InstallmentNo"].Tag = objDTOInstallmentDetails.InstallmentNo;
                        dgvInstallmentDetails.Rows[iCounter].Cells["InstallmentDate"].Value = objDTOInstallmentDetails.dtInstallmentDate;
                        dgvInstallmentDetails.Rows[iCounter].Cells["Amount"].Value = objDTOInstallmentDetails.decAmount.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                        dgvInstallmentDetails.Rows[iCounter].Cells["InterestAmount"].Value = objDTOInstallmentDetails.decInterestAmount.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                        dgvInstallmentDetails.Rows[iCounter].Cells["IsPaid"].Value = objDTOInstallmentDetails.blnIsPaid;
                        //If loan amount is paid
                        if (objDTOInstallmentDetails.blnIsPaid)
                        {
                            MdecTotalPaidAmount += objDTOInstallmentDetails.decAmount;
                            MdecPaidAmount += (objDTOInstallmentDetails.decAmount - objDTOInstallmentDetails.decInterestAmount);
                            MintPaidCount++;
                            dgvInstallmentDetails.Rows[iCounter].DefaultCellStyle.BackColor = Color.LightGray;
                            dgvInstallmentDetails.Rows[iCounter].ReadOnly = true;
                        }
                        iCounter = iCounter + 1;
                    }
                    lblDeductEndDate.Text = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtDeductEndDate.ToString("dd MMM yyyy");
                }
            }
            SetNavigatorEnability();
            SetEnability();
            btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;
            if (MintPaidCount > 0)
            {
                cboInterestType.Enabled = false;
                txtInterestRate.Enabled = false;
                dtpDeductStartDate.Enabled = false;
            }
            else
            {
                cboInterestType.Enabled = true;
                txtInterestRate.Enabled = true;
                dtpDeductStartDate.Enabled = true;
            }
        }
        #endregion DisplayLoan

        #region CheckIfEmployeeExists
        public bool CheckIfEmployeeExists()
        {
            return MobjClsCommonUtility.FillCombos(new string[] { "COUNT(1) AS COUNT", "Loan", "EmployeeID=" + EmployeeID }).Rows[0]["Count"].ToInt32() > 0 ? true : false;
        }
        #endregion CheckIfEmployeeExists

        #region SetNavigatorEnability
        /// <summary>
        /// Set enability of binding navigator controls
        /// </summary>
        private void SetNavigatorEnability()
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            if (!MblnAddStatus)
            {
                if (MintRecordCount == 0)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                else
                {
                    if (BindingNavigatorPositionItem.Text.ToInt32() == MintRecordCount)
                    {
                        BindingNavigatorMoveNextItem.Enabled = false;
                        BindingNavigatorMoveLastItem.Enabled = false;
                    }
                    if (BindingNavigatorPositionItem.Text.ToInt32() == 1)
                    {
                        BindingNavigatorMoveFirstItem.Enabled = false;
                        BindingNavigatorMovePreviousItem.Enabled = false;
                    }
                }
            }
            else
            {
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
        }
        #endregion SetNavigatorEnability

        #region RecordCount
        /// <summary>
        /// Get record count of loan details
        /// </summary>
        private void RecordCount()
        {
            MintRecordCount = 0;
            MintCurrentRecordCount = 0;
            //  string strSearchKey=txtSearch.Text;
            DataTable datRecCount = null;
            if (MstrSearch != "")
                datRecCount = MobjClsCommonUtility.FillCombos(new string[] { "COUNT(1) AS COUNT", "Loan L " +
                    "INNER JOIN EmployeeMaster EM ON L.EmployeeID = EM.EmployeeID ", "L.CompanyID IN (select CompanyID from UserCompanyDetails where UserID="+ ClsCommonSettings.UserID +")   AND " +
                    "(" + EmployeeID + " = 0 OR L.EmployeeID = " + EmployeeID + " ) " +
                    "AND (EM.EmployeeFullname like '%" + MstrSearch + "%' OR EM.EmployeeNumber like '%" + MstrSearch + "%')" });
            else
                datRecCount = MobjClsCommonUtility.FillCombos(new string[] { "COUNT(1) AS COUNT", "Loan L " +
                    "INNER JOIN EmployeeMaster EM ON L.EmployeeID = EM.EmployeeID ", "L.CompanyID IN (select CompanyID from UserCompanyDetails where UserID="+ ClsCommonSettings.UserID +")   AND " +
                    "(" + EmployeeID + " = 0 OR L.EmployeeID = " + EmployeeID + " )" });
            if (datRecCount != null)
            {
                if (datRecCount.Rows[0]["COUNT"].ToInt32() <= 0 && txtSearch.Text != string.Empty)
                {
                    MstrSearch = "";
                    MessageBox.Show(MobjNotification.GetErrorMessage(MaMessageArr, 40, out MmsgMessageIcon).Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSearch.Text = string.Empty;
                    return;
                }
                MintRecordCount = datRecCount.Rows[0]["COUNT"].ToInt32();
                MintCurrentRecordCount = MintRecordCount;
                BindingNavigatorCountItem.Text = strBindingOf + MintRecordCount.ToStringCustom();
                BindingNavigatorPositionItem.Text = MintRecordCount.ToStringCustom();
            }

        }
        #endregion RecordCount

        #region ClearControls
        /// <summary>
        /// Clear the controls
        /// </summary>
        private void ClearControls()
        {
            LoadCombos(3);
            txtSearch.Text = string.Empty;
            MstrSearch = string.Empty;
            MdecPaidAmount = MintPaidCount = 0;
            cboEmployee.SelectedIndex = -1;
            LblDOJ.Text = string.Empty;
            LblPayClassification.Text = string.Empty;
            cboCurrency.SelectedIndex = -1;
            txtLoanNumber.Text = string.Empty;
            txtLoanNumber.Tag = 0;
            cboLoanType.SelectedIndex = -1;
            dtpLoanDate.Value = ClsCommonSettings.GetServerDate().ToString("dd MMM yyyy", CultureInfo.InvariantCulture).ToDateTime();
            cboInterestType.SelectedIndex = -1;
            txtInterestRate.Text = 0.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
            dtpDeductStartDate.Value = ClsCommonSettings.GetServerDate().ToString("dd MMM yyyy", CultureInfo.InvariantCulture).ToDateTime();
            txtNoOfInstallments.Text = "1";
            lblDeductEndDate.Text = ClsCommonSettings.GetServerDate().ToString("dd MMM yyyy", CultureInfo.InvariantCulture);
            txtAmount.Text = 0.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
            txtPaymentAmount.Text = 0.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
            dgvInstallmentDetails.Rows.Clear();
            txtReason.Text = string.Empty;
            lblStatus.Text = string.Empty;
            LblPayClassification.Tag = 0;
            cboEmployee.Enabled = true;
            chkClosed.Checked = false;
            chkClosed.Enabled = false;

            BindingNavigatorAddNewItem.Enabled = BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;

            MblnAddStatus = true;
            strCondition = string.Empty;
            MintCompanyID = 0;
            MintScale = 0;
            MintCurrencyID = 0;
            MdecInstallmentAmt = 0;
            MdecBasicPay = 0;
            MintRecordCount = 0;
            MintCurrentRecordCount = 0;
            MstrMessageCommon = string.Empty;

            MobjClsBLLEmployeeLoan = new clsBLLEmployeeLoan();
        }
        #endregion ClearControls

        #region LoadErrorMessage
        /// <summary>
        /// Load messages according to formid
        /// </summary>
        private void LoadErrorMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MobjNotification.FillMessageArray((int)FormID.EmployeeLoan, ClsCommonSettings.ProductID);
            MaStatusMessage = MobjNotification.FillStatusMessageArray((int)FormID.EmployeeLoan, ClsCommonSettings.ProductID);
        }
        #endregion LoadErrorMessage

        #region LoadCombos
        /// <summary>
        /// Load the combos
        /// </summary>
        /// <param name="intType"></param>
        private void LoadCombos(int intType)
        {
            DataTable datCombo = new DataTable();
            if (intType == 0)
            {
                datCombo = null;
                if (ClsCommonSettings.IsArabicView)
                    datCombo = MobjClsCommonUtility.FillCombos(new string[] { "InterestTypeID,DescriptionArb AS Description", "InterestTypeReference", "InterestTypeID <> " + (int)eInterestType.Compound + " ORDER BY Description" });
                else
                    datCombo = MobjClsCommonUtility.FillCombos(new string[] { "InterestTypeID,Description", "InterestTypeReference", "InterestTypeID <> " + (int)eInterestType.Compound + " ORDER BY Description" });
                BindCombo(cboInterestType, datCombo, "Description", "InterestTypeID");
            }

            if (intType == 0 || intType == 2)
            {
                datCombo = null;
                if (ClsCommonSettings.IsArabicView)
                    datCombo = MobjClsCommonUtility.FillCombos(new string[] { "LoanTypeID,DescriptionArb AS Description", "LoanTypeReference", "LoanTypeID <> 0  ORDER BY Description" });
                else
                    datCombo = MobjClsCommonUtility.FillCombos(new string[] { "LoanTypeID,Description", "LoanTypeReference", "LoanTypeID <> 0  ORDER BY Description" });
                BindCombo(cboLoanType, datCombo, "Description", "LoanTypeID");
            } 

            if (intType == 0 || intType == 3)
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    if (MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intEmployeeID > 0)
                        datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullNameArb+'['+E.EmployeeNumber+']' + ' ['+C.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN PaySalaryStructure PSS ON E.EmployeeID = PSS.EmployeeID " +
                            "INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "" +
                            "E.WorkStatusID > 5 AND E.CompanyID in(select CompanyID from UserCompanyDetails where UserID ="+ ClsCommonSettings.UserID  +") UNION SELECT E.EmployeeID,E.EmployeeFullNameArb+'['+E.EmployeeNumber+']' + ' ['+C.ShortName+ ']' AS EmployeeName FROM EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID WHERE E.EmployeeID = " + MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intEmployeeID });
                    else
                        datCombo = MobjClsCommonUtility.FillCombos(new string[] { "DISTINCT E.EmployeeID," +
                            "E.EmployeeFullNameArb+'['+E.EmployeeNumber+']' + ' ['+C.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN PaySalaryStructure PSS ON E.EmployeeID = PSS.EmployeeID " +
                            "INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "" +
                            "E.WorkStatusID > 5 AND E.CompanyID in(select CompanyID from UserCompanyDetails where UserID =" + ClsCommonSettings.UserID + ") ORDER BY EmployeeName" });
                }
                else
                {
                    if (MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intEmployeeID > 0)
                        datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullName+'['+E.EmployeeNumber+']' + ' ['+C.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN PaySalaryStructure PSS ON E.EmployeeID = PSS.EmployeeID " +
                            "INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "" +
                            "E.WorkStatusID > 5 AND E.CompanyID in(select CompanyID from UserCompanyDetails where UserID =" + ClsCommonSettings.UserID + ") UNION SELECT E.EmployeeID,E.EmployeeFullName+'['+E.EmployeeNumber+']' + ' ['+C.ShortName+ ']' AS EmployeeName FROM EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID WHERE E.EmployeeID = " + MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intEmployeeID });
                    else
                        datCombo = MobjClsCommonUtility.FillCombos(new string[] { "DISTINCT E.EmployeeID," +
                            "E.EmployeeFullName+'['+E.EmployeeNumber+']' + ' ['+C.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN PaySalaryStructure PSS ON E.EmployeeID = PSS.EmployeeID " +
                            "INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "" +
                            "E.WorkStatusID > 5 AND E.CompanyID in(select CompanyID from UserCompanyDetails where UserID =" + ClsCommonSettings.UserID + ") ORDER BY EmployeeName" });
                }
                BindCombo(cboEmployee, datCombo, "EmployeeName", "EmployeeID");
            }

            if (intType == 0 || intType == 1)
            {
                if (ClsCommonSettings.IsArabicView)
                    datCombo = MobjClsCommonUtility.FillCombos(new string[] { "DISTINCT CR.CurrencyID,CR.CurrencyNameArb AS CurrencyName", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID = CD.CurrencyID", "" });//strCondition(according to company of employee)
                else
                    datCombo = MobjClsCommonUtility.FillCombos(new string[] { "DISTINCT CR.CurrencyID,CR.CurrencyName", "CurrencyReference CR INNER JOIN CurrencyDetails CD ON CR.CurrencyID = CD.CurrencyID", "" });//strCondition(according to company of employee)
                BindCombo(cboCurrency, datCombo, "CurrencyName", "CurrencyID");
            }
        }
        #endregion LoadCombos

        #region BindCombo
        /// <summary>
        /// Bind the given combo with given datatable
        /// </summary>
        /// <param name="cboCombo">combobox to be filled</param>
        /// <param name="datCombo">datatable (source)</param>
        /// <param name="strTextField">Display member</param>
        /// <param name="strValueField">Value member</param>
        private void BindCombo(ComboBox cboCombo, DataTable datCombo, string strTextField, string strValueField)
        {
            if (datCombo != null)
            {
                cboCombo.DisplayMember = strTextField;
                cboCombo.ValueMember = strValueField;
                cboCombo.DataSource = datCombo;
            }
        }
        #endregion BindCombo

        #region GenerateInstallments
        /// <summary>
        /// Filling the installment details
        /// </summary>
        private void GenerateInstallments()
        {
            if (MblnGridChangeStatus == false)
            {
                if (LblPayClassification.Text != string.Empty && cboInterestType.SelectedIndex != -1 && txtInterestRate.Text != string.Empty && txtNoOfInstallments.Text != string.Empty && txtAmount.Text != string.Empty)
                {
                    if (LblPayClassification.Tag.ToInt32() == (int)ePaymentClassification.Monthly)
                    {
                        FillInstallmentDetails(cboInterestType.SelectedValue.ToInt32());
                    }
                }
            }

        }
        #endregion GenerateInstallments

        #region FillInstallmentDetails
        /// <summary>
        /// Filll grid with new details.
        /// </summary>
        /// <param name="intInterestType"></param>
        private void FillInstallmentDetails(int intInterestType)
        {
            try
            {
                if (txtNoOfInstallments.Text.ToInt32() <= MintPaidCount)
                    return;
                int iNoOfRows = 0, iExtraRows = 0, i = 0, iCounter = 0;
                DateTime dtInstallmentDate;
                decimal decInstallmentAmount = 0, decInterestAmt = 0,OriginalAmt =0 ,RoundAmt=0 ,DiffAmt=0 ;
                string strInstallmentNo = string.Empty;
                DataTable datLoanDetails = GetNewTable();

                if (MintPaidCount > 0)//If atleast one installment is paid
                {
                    //Checking if new NoOfInstallments  is greater than than the old value.If yes finding the additional number of rows required
                    if (txtNoOfInstallments.Text.ToInt32() < dgvInstallmentDetails.Rows.Count)
                        iNoOfRows = txtNoOfInstallments.Text.ToInt32();
                    else
                    {
                        iNoOfRows = dgvInstallmentDetails.Rows.Count;
                        iExtraRows = txtNoOfInstallments.Text.ToInt32() - dgvInstallmentDetails.Rows.Count;
                    }

                    if (decInstallmentAmount == 0)
                        decInstallmentAmount = CalculateInstalmentAmount(intInterestType);

                    if (cboInterestType.SelectedValue.ToInt32() != (int)eInterestType.None)
                    {
                        //Calculate interest amount
                        if ((txtNoOfInstallments.Text.ToInt32() - MintPaidCount) > 0)
                            decInterestAmt = ((txtAmount.Text.ToDecimal() - MdecPaidAmount) * txtInterestRate.Text.ToDecimal()) / ((txtNoOfInstallments.Text.ToInt32() - MintPaidCount) * 100);

                        if (decInterestAmt < 0)
                            decInterestAmt = 0;
                    }

                    //Copying the existing values in the grid to the datatable.The amount of paid installment cannot be updated
                    for (i = 0; i < iNoOfRows; i++)
                    {
                        DataRow drRow = datLoanDetails.NewRow();
                        drRow["LoanID"] = txtLoanNumber.Tag.ToInt32();
                        drRow["InstallmentNo"] = dgvInstallmentDetails.Rows[i].Cells["InstallmentNo"].Value;
                        drRow["InstallmentDate"] = dgvInstallmentDetails.Rows[i].Cells["InstallmentDate"].Value;
                        if (dgvInstallmentDetails.Rows[i].Cells["IsPaid"].Value.ToBoolean())
                        {
                            drRow["Amount"] = dgvInstallmentDetails.Rows[i].Cells["Amount"].Value;
                            drRow["InterestAmount"] = dgvInstallmentDetails.Rows[i].Cells["InterestAmount"].Value;
                            drRow["IsPaid"] = true;
                            datLoanDetails.Rows.Add(drRow);
                        }
                        else
                        {
                            drRow["Amount"] = decInstallmentAmount.ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                            drRow["InterestAmount"] = decInterestAmt;
                            drRow["IsPaid"] = false;
                            datLoanDetails.Rows.Add(drRow);
                        }
                    }

                    //If extra rows are needed datarows sre added with new amount values
                    if (iExtraRows > 0)
                    {
                        iCounter = iNoOfRows;
                        for (i = 0; i < iExtraRows; i++)
                        {
                            strInstallmentNo = "Installments" + (iCounter + 1).ToStringCustom();

                            if (ClsCommonSettings.IsArabicView)
                                strInstallmentNo = strInstallmentNo.Replace("Installments", "أقساط.");

                            if (iNoOfRows == 0)
                                dtInstallmentDate = dtpDeductStartDate.Value;
                            else
                                dtInstallmentDate = Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Month, i + 1, (dgvInstallmentDetails.Rows[iNoOfRows - 1].Cells["InstallmentDate"].Value.ToDateTime()));
                            DataRow drRow = datLoanDetails.NewRow();
                            drRow["LoanID"] = txtLoanNumber.Tag.ToInt32();
                            drRow["InstallmentNo"] = strInstallmentNo;
                            drRow["InstallmentDate"] = dtInstallmentDate;
                            drRow["Amount"] = decInstallmentAmount.ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                            drRow["InterestAmount"] = decInterestAmt;
                            drRow["IsPaid"] = false;
                            datLoanDetails.Rows.Add(drRow);
                            iCounter += 1;
                        }
                    }
                }
                else//If none of the installment is paid
                {
                    for (iCounter = 0; iCounter < txtNoOfInstallments.Text.ToInt32(); iCounter++)
                    {
                        strInstallmentNo = "Installments" + (iCounter + 1).ToStringCustom();
                        if (ClsCommonSettings.IsArabicView)
                            strInstallmentNo = strInstallmentNo.Replace("Installments", "أقساط.");

                        if (iCounter == 0)
                            dtInstallmentDate = dtpDeductStartDate.Value;
                        else
                            dtInstallmentDate = Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Month, iCounter, dtpDeductStartDate.Value);

                        if (decInstallmentAmount == 0)
                            decInstallmentAmount = CalculateInstalmentAmount(intInterestType);


                        if (cboInterestType.SelectedValue.ToInt32() != (int)eInterestType.None)
                        {
                            if (txtNoOfInstallments.Text.ToDecimal() > 0)
                                decInterestAmt = (txtPaymentAmount.Text.ToDecimal() - txtAmount.Text.ToDecimal()) / txtNoOfInstallments.Text.ToDecimal();

                            if (decInterestAmt < 0)
                                decInterestAmt = 0;
                        }

                        DataRow drRow = datLoanDetails.NewRow();
                        drRow["LoanID"] = txtLoanNumber.Tag.ToInt32();
                        drRow["InstallmentNo"] = strInstallmentNo;
                        drRow["InstallmentDate"] = dtInstallmentDate;
                        drRow["Amount"] = decInstallmentAmount.ToDecimal().ToString("F" + MintScale);
                        drRow["InterestAmount"] = decInterestAmt;
                        datLoanDetails.Rows.Add(drRow);
                    }
                }
                dgvInstallmentDetails.Rows.Clear();
                //Grid is filled with new datatable values
                if (datLoanDetails != null && datLoanDetails.Rows.Count > 0)
                {
                    MdecPaidAmount = MdecTotalPaidAmount = MintPaidCount = 0;
                    for (iCounter = 0; iCounter < datLoanDetails.Rows.Count; iCounter++)
                    {
                        dgvInstallmentDetails.Rows.Add();
                        dgvInstallmentDetails.Rows[iCounter].Cells["LoanID"].Value = datLoanDetails.Rows[iCounter]["LoanID"].ToStringCustom();
                        dgvInstallmentDetails.Rows[iCounter].Cells["InstallmentNo"].Value = datLoanDetails.Rows[iCounter]["InstallmentNo"].ToStringCustom();
                        dgvInstallmentDetails.Rows[iCounter].Cells["InstallmentNo"].Tag = iCounter + 1;
                        dgvInstallmentDetails.Rows[iCounter].Cells["InstallmentDate"].Value = datLoanDetails.Rows[iCounter]["InstallmentDate"].ToDateTime();

            

                        if (ClsCommonSettings.IsAmountRoundByZero)
                        {
                            dgvInstallmentDetails.Rows[iCounter].Cells["Amount"].Value = Math.Floor(datLoanDetails.Rows[iCounter]["Amount"].ToDecimal()).ToString(); 
                            dgvInstallmentDetails.Rows[iCounter].Cells["AmountDecimal"].Value = datLoanDetails.Rows[iCounter]["Amount"].ToString();

                            OriginalAmt = OriginalAmt + dgvInstallmentDetails.Rows[iCounter].Cells["AmountDecimal"].Value.ToDecimal();
                            RoundAmt = RoundAmt + dgvInstallmentDetails.Rows[iCounter].Cells["Amount"].Value.ToDecimal(); 

                        }
                        else
                        {
                            dgvInstallmentDetails.Rows[iCounter].Cells["Amount"].Value = datLoanDetails.Rows[iCounter]["Amount"].ToString();
                            dgvInstallmentDetails.Rows[iCounter].Cells["AmountDecimal"].Value = datLoanDetails.Rows[iCounter]["Amount"].ToString();

                        }
                        dgvInstallmentDetails.Rows[iCounter].Cells["InterestAmount"].Value = datLoanDetails.Rows[iCounter]["InterestAmount"].ToString();
                        dgvInstallmentDetails.Rows[iCounter].Cells["IsPaid"].Value = datLoanDetails.Rows[iCounter]["IsPaid"].ToBoolean();

                        if ((ClsCommonSettings.IsAmountRoundByZero == true) && (iCounter == datLoanDetails.Rows.Count-1))
                        {
                            //OriginalAmt = dgvInstallmentDetails.Rows[iCounter].Cells["AmountDecimal"].Value.ToDecimal();
                            //RoundAmt = dgvInstallmentDetails.Rows[iCounter].Cells["Amount"].Value.ToDecimal();
                            DiffAmt = OriginalAmt - RoundAmt;
                            if (DiffAmt > 0)
                            {
                                dgvInstallmentDetails.Rows[iCounter].Cells["Amount"].Value =  Math.Round(dgvInstallmentDetails.Rows[iCounter].Cells["Amount"].Value.ToDecimal() + DiffAmt,0).ToString()  ;
                            }
                        }

                        //If loan amount is paid
                        if (datLoanDetails.Rows[iCounter]["IsPaid"].ToBoolean())
                        {
                            MdecTotalPaidAmount += datLoanDetails.Rows[iCounter]["Amount"].ToDecimal();
                            MdecPaidAmount += (datLoanDetails.Rows[iCounter]["Amount"].ToDecimal() - datLoanDetails.Rows[iCounter]["InterestAmount"].ToDecimal());
                            MintPaidCount++;
                            dgvInstallmentDetails.Rows[iCounter].DefaultCellStyle.BackColor = Color.LightGray;
                            dgvInstallmentDetails.Rows[iCounter].ReadOnly = true;
                        }

                    }



                    lblDeductEndDate.Text = datLoanDetails.Rows[datLoanDetails.Rows.Count - 1]["InstallmentDate"].ToDateTime().ToString("dd MMM yyyy");
                }
            }
           
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, LogSeverity.Error);

            }

        }
        #endregion FillInstallmentDetails

        #region CalculateInstalmentAmount
        /// <summary>
        /// Calacualte installment amount
        /// </summary>
        /// <param name="intInterestType">whether simple or none</param>
        /// <returns></returns>
        private decimal CalculateInstalmentAmount(int intInterestType)
        {
            decimal decInstallmentAmount = 0;
            try
            {
                decimal decDenomination = 0;
                decimal decSimpleInterest = 0;
                decimal decLoanAmount = 0;
                decimal decRate = 0;
                decimal decInstallmentNo = 0;
                decRate = txtInterestRate.Text.ToDecimal();
                decLoanAmount = txtAmount.Text.ToDecimal() - MdecPaidAmount;//Upaid amount
                decInstallmentNo = txtNoOfInstallments.Text.ToDecimal() - MintPaidCount;//No of unpaid installments

                decDenomination = 12;

                if (intInterestType == (int)eInterestType.None)
                {
                    decInstallmentAmount = decLoanAmount / decInstallmentNo;
                    txtPaymentAmount.Text = (decLoanAmount + MdecPaidAmount).ToString("F" + MintScale);
                }
                else if (intInterestType == (int)eInterestType.Simple)
                {
                    decSimpleInterest = (decLoanAmount * (decRate / 100));
                    decInstallmentAmount = (decLoanAmount + decSimpleInterest) / decInstallmentNo;
                    // if(MblnAddStatus)
                    txtPaymentAmount.Text = (decLoanAmount + decSimpleInterest + MdecTotalPaidAmount).ToString("F" + MintScale);
                }
                else if (intInterestType == (int)eInterestType.Compound)
                {
                    decSimpleInterest = decLoanAmount * (Math.Pow(((1 + (decRate / 100) * ((1 / decDenomination))).ToDouble()), decInstallmentNo.ToDouble()) - 1).ToDecimal();
                    decInstallmentAmount = (decLoanAmount + decSimpleInterest) / decInstallmentNo;
                    txtPaymentAmount.Text = (decLoanAmount + decSimpleInterest + MdecTotalPaidAmount).ToString("F" + MintScale);
                }


                if (ClsCommonSettings.IsAmountRoundByZero == true)
                {
                    txtPaymentAmount.Text = Math.Round(txtPaymentAmount.Text.ToDecimal() , 0).ToString();  
                }


            }
            catch
            {
                decInstallmentAmount = 0;
            }

            MdecInstallmentAmt = decInstallmentAmount;

            return decInstallmentAmount;
        }
        #endregion CalculateInstalmentAmount

        #region GetNewTable
        /// <summary>
        /// Get a new table with the fields same as grid
        /// </summary>
        /// <returns></returns>
        private DataTable GetNewTable()
        {
            DataTable datLoanDetails = new DataTable();
            datLoanDetails.Columns.Add("LoanID", typeof(int));
            datLoanDetails.Columns.Add("InstallmentNo", typeof(string));
            datLoanDetails.Columns.Add("InstallmentDate", typeof(DateTime));
            datLoanDetails.Columns.Add("Amount", typeof(decimal));
            datLoanDetails.Columns.Add("InterestAmount", typeof(decimal));
            datLoanDetails.Columns.Add("IsPaid", typeof(bool));

            return datLoanDetails;
        }
        #endregion GetNewTable

        #region CalculateDeductToDateMonthly
        /// <summary>
        /// Calculate end date
        /// </summary>
        private void CalculateDeductToDateMonthly()
        {
            if (LblPayClassification.Tag.ToInt32() == (int)ePaymentClassification.Monthly)
            {
                if (MblnAddStatus && txtNoOfInstallments.Text != string.Empty)
                {
                    int intInstallmentNo = txtNoOfInstallments.Text.ToInt32();
                    lblDeductEndDate.Text = Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Month, intInstallmentNo - 1, dtpDeductStartDate.Value).ToString("dd MMM yyyy");
                }
            }
        }
        #endregion CalculateDeductToDateMonthly

        /// <summary>
        /// Autocomplete
        /// </summary>
        /// 
        private void SetAutoCompleteList()
        {
            DataTable dt = null;
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            if (EmployeeID > 0)
            {
                dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), EmployeeID.ToInt32(), "LoanNumber", "Loan");
            }
            else
            {
                dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "LoanNumber", "Loan");
            }
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }

        private void FrmEmployeeLoan_Load(object sender, EventArgs e)
        {
            LoadErrorMessage();
            LoadCombos(0);
            SetPermissions();
            if (EmployeeID <= 0)
            {
                AddNew();
            }
            else
            {
                if (CheckIfEmployeeExists())
                {
                    RecordCount();
                    BindingNavigatorMoveFirstItem_Click(null, null);
                }
                else
                {
                    AddNew();
                    cboEmployee.SelectedValue = EmployeeID;
                }
            }
            SetAutoCompleteList();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            LblPayClassification.Text = string.Empty;
            Lblbasicpay.Text = string.Empty;

            if (cboEmployee.SelectedIndex != -1)
            {
                DataTable datTemp = null;

                datTemp = null;
                datTemp = MobjClsCommonUtility.FillCombos(new string[] { "ISNULL(CurrencyId,0) AS CurrencyID", "PaySalaryStructure", "EmployeeID = " + cboEmployee.SelectedValue.ToInt32() });
                if (datTemp != null && datTemp.Rows.Count > 0)
                {
                    cboCurrency.SelectedValue = datTemp.Rows[0]["CurrencyID"].ToInt32();
                }

                datTemp = null;
                datTemp = MobjClsCommonUtility.FillCombos(new string[] { "CONVERT(VARCHAR(11),DateofJoining,106) AS DateofJoining", "EmployeeMaster", "EmployeeID = " + cboEmployee.SelectedValue.ToInt32() });
                if (datTemp != null && datTemp.Rows.Count > 0)
                {
                    LblDOJ.Text = datTemp.Rows[0]["DateofJoining"].ToString();
                }

                datTemp = null;
                datTemp = MobjClsCommonUtility.FillCombos(new string[] { "CR.CurrencyID,E.CompanyID, ISNULL(CR.Scale, 3) AS Scale", "CurrencyReference AS CR  INNER JOIN PaySalaryStructure SS  INNER JOIN EmployeeMaster AS E ON SS.EmployeeID = E.EmployeeID ON CR.CurrencyID =  SS.CurrencyId", "E.EmployeeID = " + cboEmployee.SelectedValue.ToInt32() });
                if (datTemp != null && datTemp.Rows.Count > 0)
                {
                    MintCompanyID = datTemp.Rows[0]["CompanyID"].ToInt32();
                    MintCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
                    MintScale = datTemp.Rows[0]["Scale"].ToInt32();
                }

                if (MblnAddStatus)
                {
                    GenerateLoanCode(MintCompanyID);
                }



                datTemp = null;
                datTemp = MobjClsCommonUtility.FillCombos(new string[] { "PCR.PaymentClassification,PCR.PaymentClassificationArb," +
                    "PCR.PaymentClassificationID,(SELECT D.Amount FROM PaySalaryStructureDetail D WHERE D.SalaryStructureID = PSS.SalaryStructureID " +
                    "AND D.AdditionDeductionID = 1) AS BasicPay,(SELECT ISNULL(SUM(ISNULL(D.Amount,0)),0) FROM PaySalaryStructureDetail D " +
                    "INNER JOIN PayAdditionDeductionReference ADR ON D.AdditionDeductionID = ADR.AdditionDeductionID AND ADR.IsAddition = 1 " +
                    "WHERE D.SalaryStructureID = PSS.SalaryStructureID ) AS GrossSalary,CR.Code AS CurrencyName,CR.CurrencyNameArb", "" +
                    "PaySalaryStructure PSS INNER JOIN PaymentClassificationReference PCR ON PSS.PaymentClassificationID = PCR.PaymentClassificationID " +
                    "INNER JOIN PaySalaryStructureDetail PSD ON PSS.SalaryStructureID = PSD.SalaryStructureID " +
                    "INNER JOIN PayAdditionDeductionReference ADR ON PSD.AdditionDeductionID = PSD.AdditionDeductionID AND ADR.IsAddition = 1 " +
                    "INNER JOIN CurrencyReference CR ON CR.CurrencyID = PSS.CurrencyId", "PSS.EmployeeID = " + cboEmployee.SelectedValue.ToInt32() + 
                    "GROUP BY PCR.PaymentClassificationID,PCR.PaymentClassification,PCR.PaymentClassificationArb,CR.Code,CR.CurrencyNameArb,PSS.SalaryStructureID" });


                
                if (datTemp != null && datTemp.Rows.Count > 0)
                {
                    if (ClsCommonSettings.IsArabicView)
                    {
                        LblPayClassification.Text = datTemp.Rows[0]["PaymentClassificationArb"].ToStringCustom();
                        Lblbasicpay.Text = datTemp.Rows[0]["GrossSalary"].ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale)) + " (" + datTemp.Rows[0]["CurrencyNameArb"].ToStringCustom() + ")";
                    }
                    else
                    {
                        LblPayClassification.Text = datTemp.Rows[0]["PaymentClassification"].ToStringCustom();
                        Lblbasicpay.Text = datTemp.Rows[0]["GrossSalary"].ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale)) + " (" + datTemp.Rows[0]["CurrencyName"].ToStringCustom() + ")";
                    }

                    LblPayClassification.Tag = datTemp.Rows[0]["PaymentClassificationID"].ToInt32();                    
                    MdecBasicPay = datTemp.Rows[0]["GrossSalary"].ToDecimal();
                }
                else
                {
                    LblPayClassification.Tag = 0;
                    Lblbasicpay.Text = 0.ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                }

                txtInterestRate.Text = txtInterestRate.Text.ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                txtAmount.Text = txtAmount.Text.ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                txtPaymentAmount.Text = txtPaymentAmount.Text.ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
            }
            ChangeStatus();
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnKeyPress(sender, e, false);
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e, bool blnIsAmount)
        {
            try
            {
                if (blnIsAmount)
                {
                    if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                    {
                        e.Handled = true;
                    }
                    if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                    {
                        e.Handled = true;
                    }
                }
                else
                {
                    if (!((Char.IsDigit(e.KeyChar)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }

        private void txtInterestRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnKeyPress(sender, e, true);
        }

        private void txtNoOfInstallments_KeyPress(object sender, KeyPressEventArgs e)
        {
            OnKeyPress(sender, e, false);
        }

        private void txtInterestRate_TextChanged(object sender, EventArgs e)
        {
            if (txtInterestRate.Text != string.Empty)
            {
                if (txtInterestRate.Text.ToDecimal() >= 100)
                    txtInterestRate.Text = "99";
                MblnGridChangeStatus = false;
                GenerateInstallments();
            }
            ChangeStatus();
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpDeductStartDate_ValueChanged(object sender, EventArgs e)
        {
            DateTime dtNewValue = new DateTime(dtpDeductStartDate.Value.Year, dtpDeductStartDate.Value.Month, 1);
            dtpDeductStartDate.Value = dtNewValue;
            CalculateDeductToDateMonthly();
            GenerateInstallments();
            ChangeStatus();
        }

        private void txtNoOfInstallments_TextChanged(object sender, EventArgs e)
        {
            if (txtNoOfInstallments.Text != string.Empty)
            {
                CalculateDeductToDateMonthly();
                MblnGridChangeStatus = false;
                GenerateInstallments();

            }
            ChangeStatus();
        }

        private void cboInterestType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboInterestType.SelectedIndex != -1)
            {
                if (cboInterestType.SelectedValue.ToInt32() == (int)eInterestType.None)
                {
                    txtInterestRate.Enabled = false;
                    txtInterestRate.Text = "0".ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                }
                else
                    txtInterestRate.Enabled = true;
            }
            MblnGridChangeStatus = false;
            GenerateInstallments();
            ChangeStatus();
        }

        private void dtpLoanDate_ValueChanged(object sender, EventArgs e)
        {
            CalculateDeductToDateMonthly();
            GenerateInstallments();
            ChangeStatus();
        }

        private bool ValidateSalaryProcess()
        {
            bool blnStatus = true;

            if (cboEmployee.SelectedValue.ToInt32() > 0)
            {
                int intProcess = 0;
                DataTable datValidation = MobjClsCommonUtility.FillCombos(new string[] { "PaymentID", "PayEmployeePayment", "EmployeeID = " + cboEmployee.SelectedValue.ToInt32() + " AND CONVERT(DATETIME,'" + dtpDeductStartDate.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + "')" + "  BETWEEN CONVERT(DATETIME,CONVERT(VARCHAR,PeriodFrom,106),106) AND CONVERT(DATETIME,CONVERT(VARCHAR,PeriodTo,106),106)" });

                if (datValidation != null && datValidation.Rows.Count > 0)
                {
                    intProcess = 1;
                }
                else
                    intProcess = 0;

                if (intProcess == 1)
                    blnStatus = false;
                else
                    blnStatus = true;

            }
            return blnStatus;
        }

        private void FillRemarks()
        {
            if (txtAmount.Text != string.Empty && cboEmployee.SelectedIndex != -1 && txtPaymentAmount.Text != string.Empty)
            {
                decimal decAmount = 0;
                decimal decInterestAmount = 0.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale)).ToDecimal();
                if (cboInterestType.SelectedValue.ToInt32() != (int)eInterestType.None && txtInterestRate.Text.ToDecimal() > 0)
                {
                    decInterestAmount = txtPaymentAmount.Text.ToDecimal() - txtAmount.Text.ToDecimal();
                    decAmount = txtPaymentAmount.Text.ToDecimal();
                }
                else
                    decAmount = txtAmount.Text.ToDecimal();
                //  txtReason.Text = "Loan being paid to Employee - " + cboEmployee.Text.Trim() + " - " + " of " + cboCurrency.Text.Trim() + " - " + decAmount.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale)) + " Amount, " + decInterestAmount.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale)) + " Interest. ";
            }
        }

        private void txtPaymentAmount_TextChanged(object sender, EventArgs e)
        {
            //  FillRemarks();
            ChangeStatus();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveLoan();
        }

        /// <summary>
        /// Save loan
        /// </summary>
        /// <returns></returns>
        private bool SaveLoan()
        {
            try
            {
                if (ValidateForm())
                {
                    if (MblnAddStatus)
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1, out MmsgMessageIcon).Replace("#", "").Trim();
                        lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("\n") + 1);
                    }
                    else
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 3, out MmsgMessageIcon).Replace("#", "").Trim();
                        lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("\n") + 1);
                    }

                    if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return false;
                    }

                    MobjClsBLLEmployeeLoan = new clsBLLEmployeeLoan();

                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intLoanID = txtLoanNumber.Tag.ToInt32();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intEmployeeID = cboEmployee.SelectedValue.ToInt32();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intCurrencyID = cboCurrency.SelectedValue.ToInt32();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.strLoanNumber = txtLoanNumber.Text.Trim();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intLoanTypeID = cboLoanType.SelectedValue.ToInt32();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtLoanDate = dtpLoanDate.Value;
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intInterestTypeID = cboInterestType.SelectedValue.ToInt32();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.decInterestRate = txtInterestRate.Text.ToDecimal();
                    if (dtpDeductStartDate.Value.ToString("dd-MMM-yyyy").ToDateTime() < dtpLoanDate.Value.ToString("dd-MMM-yyyy").ToDateTime())
                        MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtDeductStartDate = dtpLoanDate.Value;
                    else
                        MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtDeductStartDate = dtpDeductStartDate.Value;
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intNoOfInstallments = txtNoOfInstallments.Text.ToInt32();
                    if (dgvInstallmentDetails.Rows[dgvInstallmentDetails.Rows.Count - 1].Cells["InstallmentDate"].Value != null)
                        MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtDeductEndDate = dgvInstallmentDetails.Rows[dgvInstallmentDetails.Rows.Count - 1].Cells["InstallmentDate"].Value.ToDateTime();
                    else
                        MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtDeductEndDate = lblDeductEndDate.Text.ToDateTime();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.decAmount = txtAmount.Text.ToDecimal();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.decPaymentAmount = txtPaymentAmount.Text.ToDecimal();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.strReason = txtReason.Text.Trim();
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.blnClosed = chkClosed.Checked;
                    if (chkClosed.Checked && chkClosed.Enabled)
                    {
                        MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intClosedBy = ClsCommonSettings.UserID;
                        MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtClosedDate = DateTime.Now.Date;
                    }
                    else
                    {
                        MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intClosedBy = 0;
                        MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.dtClosedDate = DateTime.MinValue;
                    }
                    MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.objClsDTOEmployeeLoanInstallmentDetails = new List<clsDTOEmployeeLoanInstallmentDetails>();
                    foreach (DataGridViewRow dgvRow in dgvInstallmentDetails.Rows)
                    {
                        if (!dgvRow.Cells["IsPaid"].Value.ToBoolean())
                        {
                            clsDTOEmployeeLoanInstallmentDetails objDTOInstallmentDetails = new clsDTOEmployeeLoanInstallmentDetails();
                            objDTOInstallmentDetails.InstallmentNo = dgvRow.Cells["InstallmentNo"].Tag.ToInt32();
                            objDTOInstallmentDetails.dtInstallmentDate = dgvRow.Cells["InstallmentDate"].Value.ToDateTime();
                            objDTOInstallmentDetails.decAmount = dgvRow.Cells["Amount"].Value.ToDecimal();
                            objDTOInstallmentDetails.decInterestAmount = dgvRow.Cells["InterestAmount"].Value.ToDecimal();
                            MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.objClsDTOEmployeeLoanInstallmentDetails.Add(objDTOInstallmentDetails);
                        }
                    }

                    if (MobjClsBLLEmployeeLoan.SaveLoan())
                    {

                        try
                        {

                            if (ClsCommonSettings.ThirdPartyIntegration)
                                new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpLoanDate.Value);
                        }
                        catch (Exception Ex)
                        {
                            ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
                        }
                        SetAutoCompleteList();
                        if (MblnAddStatus)
                        {
                            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 2, out MmsgMessageIcon).Replace("#", "").Trim();
                            RecordCount();
                        }
                        else
                        {
                            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 21, out MmsgMessageIcon).Replace("#", "").Trim();
                        }

                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("\n") + 1);
                        txtLoanNumber.Tag = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intLoanID;
                        DisplayLoan();
                        return true;
                    }
                }
                else
                {
                    if (MstrMessageCommon != string.Empty)
                    {
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("\n") + 1);
                        if (control != null)
                        {
                            errLoan.SetError(control, MstrMessageCommon);
                            control.Focus();
                        }
                    }
                }

                return false;
            }
           
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, LogSeverity.Error);
                return false;
            }
        }

        /// <summary>
        /// Validation before save
        /// </summary>
        /// <returns></returns>
        private bool ValidateForm()
        {
            btnLoanType.Focus();
            MstrMessageCommon = string.Empty;
            DataTable datTemp = null;
            int intDateValidate = 0;
            errLoan.Clear();
            if (!chkClosed.Enabled && chkClosed.Checked)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1619, out MmsgMessageIcon).Replace("#", "").Trim();
                control = chkClosed;
                return false;
            }

            if (cboEmployee.SelectedIndex == -1)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10192, out MmsgMessageIcon).Replace("#", "").Trim();
                control = cboEmployee;
                return false;
            }
            else if ((MobjClsCommonUtility.FillCombos(new string[] { "WorkStatusID", "EmployeeMaster", "EmployeeID = " + cboEmployee.SelectedValue.ToInt32() + " AND WorkStatusID <= 5" })).Rows.Count > 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10193, out MmsgMessageIcon).Replace("*", cboEmployee.Text.Trim() + " ").Trim();
                control = cboEmployee;
                return false;
            }
            else if (LblPayClassification.Tag.ToInt32() == 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10190, out MmsgMessageIcon).Replace("#", "").Trim();
                MstrMessageCommon = MstrMessageCommon + " For " + cboEmployee.Text.Trim();
                control = cboEmployee;
                return false;
            }
            else if (cboCurrency.SelectedIndex == -1)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10205, out MmsgMessageIcon);
                control = cboCurrency;
                return false;
            }
            else if (txtLoanNumber.Text == string.Empty)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10194, out MmsgMessageIcon);
                control = txtLoanNumber;
                return false;
            }
            else if (cboLoanType.SelectedIndex == -1)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10195, out MmsgMessageIcon);
                control = cboLoanType;
                return false;
            }
            else if (dtpLoanDate.Value.Date > ClsCommonSettings.GetServerDate().Date)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10211, out MmsgMessageIcon);
                control = dtpLoanDate;
                return false;
            }
            else if (cboInterestType.SelectedIndex == -1)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10196, out MmsgMessageIcon);
                control = cboInterestType;
                return false;
            }
            else if (cboInterestType.SelectedValue.ToInt32() == (int)eInterestType.Simple && txtInterestRate.Text.ToDecimal() == 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10210, out MmsgMessageIcon);
                control = txtInterestRate;
                return false;
            }
            else if (txtNoOfInstallments.Text == string.Empty || txtNoOfInstallments.Text.ToInt32() == 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10197, out MmsgMessageIcon);
                control = txtNoOfInstallments;
                return false;
            }
            else if (txtAmount.Text == string.Empty || txtAmount.Text.ToDecimal() == 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10198, out MmsgMessageIcon);
                control = txtAmount;
                MblnGridChangeStatus = false;
                return false;
            }
            else if (txtAmount.Text.ToDecimal() < MdecPaidAmount)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1617, out MmsgMessageIcon).Replace("#", "").Trim();
                control = txtAmount;
                MblnGridChangeStatus = false;
                return false;
            }
            else if (txtNoOfInstallments.Text.ToInt32() <= MintPaidCount)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1618, out MmsgMessageIcon).Replace("#", "").Trim();
                control = txtNoOfInstallments;
                MblnGridChangeStatus = false;
                return false;
            }

            //Check prev date validation and salary process validation
            for (int i = 1; i <= dgvInstallmentDetails.Rows.Count - 1; i++)
            {
                if (dgvInstallmentDetails.Rows[i - 1].Cells["InstallmentDate"].Value.ToDateTime().Date > dgvInstallmentDetails.Rows[i].Cells["InstallmentDate"].Value.ToDateTime().Date)
                {
                    intDateValidate = i;
                    break;
                }
            }
            if (intDateValidate > 0)
            {
                control = null;
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10208, out MmsgMessageIcon).Replace("#", "").Trim();
                dgvInstallmentDetails.Focus();
                dgvInstallmentDetails.CurrentCell = dgvInstallmentDetails["InstallmentDate", intDateValidate];
                return false;
            }

            if (cboEmployee.SelectedIndex != -1)
            {
                DateTime dtDateOfJoining = DateTime.MinValue;
                datTemp = null;
                datTemp = MobjClsCommonUtility.FillCombos(new string[] { "convert(datetime,Convert(varchar(10),DateofJoining,101),101) as DateofJoining", "EmployeeMaster", "EmployeeID = " + cboEmployee.SelectedValue.ToInt32() });
                if (datTemp != null && datTemp.Columns["DateOfJoining"] != null && datTemp.Rows.Count > 0)
                {
                    dtDateOfJoining = datTemp.Rows[0]["DateOfJoining"].ToDateTime();
                }
                if (dtpLoanDate.Value.Date < dtDateOfJoining.Date)
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10200, out MmsgMessageIcon);
                    control = dtpLoanDate;
                    return false;
                }
            }


            if (dtpDeductStartDate.Value.Date < dtpLoanDate.Value.Date)
            {
                if (dtpDeductStartDate.Value.Month == dtpLoanDate.Value.Month && dtpDeductStartDate.Value.Year == dtpLoanDate.Value.Year)
                {
                    return true;
                }
                else
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10201, out MmsgMessageIcon);
                    control = dtpLoanDate;
                    return false;
                }

            }



            //if (dtpDeductStartDate.Value.Date < dtpLoanDate.Value.Date)
            //{
            //    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10201, out MmsgMessageIcon);
            //    control = dtpLoanDate;
            //    return false;
            //}
            //if (cboEmployee.SelectedIndex != -1)
            //{
            //    if (MblnAddStatus)
            //    {
            //        if (MobjClsCommonUtility.FillCombos(new string[] { "LoanID", "Loan", "EmployeeID =" + cboEmployee.SelectedValue.ToInt32() + " AND LoanTypeID =" + cboLoanType.SelectedValue.ToInt32() + " AND Closed = 0" }).Rows.Count > 0)
            //        {
            //            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10202, out MmsgMessageIcon);
            //            control = cboEmployee;
            //            return false;
            //        }
            //    }
            //}
            if (txtLoanNumber.Text != string.Empty)
            {
                if (MobjClsBLLEmployeeLoan.IsLoanNumberExists(txtLoanNumber.Tag.ToInt32(), txtLoanNumber.Text.Trim(),cboEmployee.SelectedValue.ToInt32()))
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10203, out MmsgMessageIcon);
                    control = txtLoanNumber;
                    return false;
                }
            }
            
            //Check with prev loan amounts
            DataTable dtPrevInstallments = MobjClsBLLEmployeeLoan.GetPreviousInstallments(cboEmployee.SelectedValue.ToInt32(),MobjClsBLLEmployeeLoan.clsDTOEmployeeLoan.intLoanID);
            int intPercentage = ClsCommonSettings.intLoanAmountPercentage;
            decimal decBasicPay = (MdecBasicPay * intPercentage) / 100;
            //In case prev loan exists
            if (dtPrevInstallments.Rows.Count > 0)
            {
                int iMonth = 0;
                int iYear = 0;

                foreach (DataGridViewRow dgvRow in dgvInstallmentDetails.Rows)
                {
                    if (iMonth != Convert.ToDateTime(dgvRow.Cells["InstallmentDate"].Value).Month || iYear != Convert.ToDateTime(dgvRow.Cells["InstallmentDate"].Value).Year)
                    {
                        iMonth = Convert.ToDateTime(dgvRow.Cells["InstallmentDate"].Value).Month;
                        iYear = Convert.ToDateTime(dgvRow.Cells["InstallmentDate"].Value).Year;
                        decimal decSum = dtPrevInstallments.Compute("SUM(Amount)", "Month=" + iMonth + " AND Year=" + iYear + "").ToDecimal();

                        var sum = dgvInstallmentDetails.Rows.Cast<DataGridViewRow>()
                                            .Where(r => ((Convert.ToDateTime(r.Cells["InstallmentDate"].Value).Month) == iMonth) && (Convert.ToDateTime(r.Cells["InstallmentDate"].Value).Year) == iYear)
                                             .Sum(t => (t.Cells["Amount"].Value.ToDecimal()));
                        if ((sum.ToDecimal() + decSum) > decBasicPay)
                        {
                            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10204, out MmsgMessageIcon);
                            return false;
                        }
                    }

                }
            }
            //If applying for loan for the first time
            else
            {
                int Month = 0;
                int Year = 0;

                foreach (DataGridViewRow dgvRow in dgvInstallmentDetails.Rows)
                {
                    if (Month != Convert.ToDateTime(dgvRow.Cells["InstallmentDate"].Value).Month || Year != Convert.ToDateTime(dgvRow.Cells["InstallmentDate"].Value).Year)
                    {
                        Month = Convert.ToDateTime(dgvRow.Cells["InstallmentDate"].Value).Month;
                        Year = Convert.ToDateTime(dgvRow.Cells["InstallmentDate"].Value).Year;


                        var sum = dgvInstallmentDetails.Rows.Cast<DataGridViewRow>()
                                            .Where(r => ((Convert.ToDateTime(r.Cells["InstallmentDate"].Value).Month) == Month) && (Convert.ToDateTime(r.Cells["InstallmentDate"].Value).Year) == Year)
                                             .Sum(t => (t.Cells["Amount"].Value.ToDecimal()));
                        if (sum.ToDecimal() > decBasicPay)
                        {
                            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10204, out MmsgMessageIcon);
                            return false;
                        }
                    }

                }
            }

            //Modified to check whether repayment exists
            if (dtpDeductStartDate.Enabled || MblnAddStatus)
            {
                if (!ValidateSalaryProcess())
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10191, out MmsgMessageIcon).Replace("#", "").Trim();
                    control = dtpDeductStartDate;
                    return false;
                }
            }

            return ValidateGrid();

        }

        /// <summary>
        /// Validation for installments in grid
        /// </summary>
        /// <returns></returns>
        private bool ValidateGrid()
        {
            decimal decInstallmentAmount = 0;
            string strMessage = string.Empty;

            foreach (DataGridViewRow dgvRow in dgvInstallmentDetails.Rows)
            {
                if (dgvRow.Cells[InstallmentDate.Index].Value == null)
                {
                    strMessage = MobjNotification.GetErrorMessage(MaMessageArr, 10212, out MmsgMessageIcon).Replace("#", "").Trim();
                    MessageBox.Show(strMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    errLoan.SetError(txtPaymentAmount, MstrMessageCommon);
                    lblStatus.Text = strMessage.Remove(0, strMessage.IndexOf("\n") + 1);
                    dgvInstallmentDetails.Focus();
                    dgvInstallmentDetails.CurrentCell = dgvInstallmentDetails[InstallmentDate.Index, dgvRow.Index];
                    return false;
                }
                else

                    decInstallmentAmount = decInstallmentAmount + dgvRow.Cells["Amount"].Value.ToDecimal();
            }

            if (txtInterestRate.Text.ToDecimal() == 0)
            {
                if (decInstallmentAmount != (txtAmount.Text.ToDecimal()))
                {
                    //Total Installment amount and Payment Amount is different
                    strMessage = MobjNotification.GetErrorMessage(MaMessageArr, 10206, out MmsgMessageIcon).Replace("#", "").Trim() + " " + Convert.ToString(decInstallmentAmount - txtAmount.Text.ToDecimal());
                    strMessage = strMessage.Replace("Payment Amount", "Loan Amount");
                    MessageBox.Show(strMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    lblStatus.Text = strMessage.Remove(0, strMessage.IndexOf("\n") + 1);
                    txtPaymentAmount.Focus();
                    return false;
                }
            }
            else if (decInstallmentAmount != (txtPaymentAmount.Text.ToDecimal()))
            {
                //Total Installment amount and Payment Amount is different
                strMessage = MobjNotification.GetErrorMessage(MaMessageArr, 10206, out MmsgMessageIcon).Replace("#", "").Trim() + " " + Convert.ToString(decInstallmentAmount - txtPaymentAmount.Text.ToDecimal());
                MessageBox.Show(strMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblStatus.Text = strMessage.Remove(0, strMessage.IndexOf("\n") + 1);
                txtPaymentAmount.Focus();
                return false;
            }

            return true;
        }


        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        //Add new loan
        private void AddNew()
        {            
            ClearControls();
            btnClear.Enabled = MblnAddPermission;
            if (ClsCommonSettings.IsArabicView)
                lblStatus.Text = "إضافة قرض جديد";
            else            
                lblStatus.Text = "Add New Loan";
            RecordCount();
            BindingNavigatorCountItem.Text = strBindingOf + (MintRecordCount + 1).ToString();
            BindingNavigatorPositionItem.Text = (MintRecordCount + 1).ToString();
            SetEnability();
            SetNavigatorEnability();
            btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;
            cboInterestType.Enabled = txtInterestRate.Enabled = dtpDeductStartDate.Enabled = true;
            if (EmployeeID > 0)
            {
                cboEmployee.Enabled = false;
                cboEmployee.SelectedValue = EmployeeID;
            }
            else
            {
                cboEmployee.Enabled = true;
            }
        }

        private void ChangeStatus()
        {
            //function for changing status
            if (MblnAddStatus)
            {
                btnOK.Enabled = MblnAddPermission;
                btnSave.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                btnOK.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errLoan.Clear();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (BindingNavigatorPositionItem.Text.ToInt32() + 1).ToString();
            if (MintRecordCount < BindingNavigatorPositionItem.Text.ToInt32())
            {
                if (MintRecordCount > 1)
                {
                    BindingNavigatorPositionItem.Text = MintRecordCount.ToString();
                    BindingNavigatorCountItem.Text = strBindingOf + MintRecordCount.ToString();
                }
                else
                {
                    BindingNavigatorPositionItem.Text = "1";
                    BindingNavigatorCountItem.Text = strBindingOf + "1";
                }
            }
            DisplayLoan();
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (BindingNavigatorPositionItem.Text.ToInt32() - 1).ToString();
            if (BindingNavigatorPositionItem.Text.ToInt32() <= 0)
            {
                BindingNavigatorPositionItem.Text = "1";
            }
            if (MintRecordCount > 1)
                BindingNavigatorCountItem.Text = strBindingOf + MintRecordCount.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplayLoan();
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (MintRecordCount > 1)
            {
                BindingNavigatorPositionItem.Text = MintRecordCount.ToString();
                BindingNavigatorCountItem.Text = strBindingOf + MintRecordCount.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                BindingNavigatorCountItem.Text = strBindingOf + "1";
            }
            DisplayLoan();
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = "1";
            if (MintRecordCount > 1)
                BindingNavigatorCountItem.Text = strBindingOf + MintRecordCount.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";
            DisplayLoan();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SaveLoan())
                this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteLoan();
            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 9001, out MmsgMessageIcon).Replace("*", "Loan");
                }
                else
                    MstrMessageCommon = "Error on BindingNavigatorDeleteItem_Click() " + Ex.Message.ToString();

                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
            }
        }

        private void DeleteLoan()
        {
            try
            {
                if (!IsRepaymentExists())
                {
                    if (chkClosed.Checked)
                    {
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1622, out MmsgMessageIcon).Replace("#", "").Trim();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("\n") + 1);
                        return;
                    }
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 13, out MmsgMessageIcon).Replace("#", "").Trim();
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }

                    if (MobjClsBLLEmployeeLoan.DeleteLoan())
                    {
                        if (ClsCommonSettings.ThirdPartyIntegration)
                            new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpLoanDate.Value);

                        MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4, out MmsgMessageIcon).Replace("#", "").Trim();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                        lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("\n") + 1);
                        AddNew();
                        SetAutoCompleteList();
                    }
                }
                else
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10209, out MmsgMessageIcon).Replace("#", "").Trim();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("\n") + 1);
                }
            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                string MstrMessageCommon = "";
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    if (ClsCommonSettings.IsArabicView)
                        MstrMessageCommon = "التفاصيل موجودة في النظام. لا يمكن حذف";
                    else
                        MstrMessageCommon = "Details exists in the system.Cannot delete";
                }
                else
                    MstrMessageCommon = "Error on BindingNavigatorDeleteItem_Click() " + Ex.Message.ToString();

                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, LogSeverity.Error);

            }


        }

        private bool IsRepaymentExists()
        {
            if (MobjClsBLLEmployeeLoan.IsRepaymentExists())
            {
                return true;
            }
            return false;
        }


        private void BtnPrint_Click(object sender, EventArgs e)
        {
            Print();
        }

        private void Print()
        {
            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = "EmployeeLoan";
            ObjViewer.PiRecId = MobjClsBLLEmployeeLoan.PobjClsDTOEmployeeLoan.intLoanID;
            ObjViewer.PiFormID = (int)FormID.EmployeeLoan;
            ObjViewer.ShowDialog();
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            {
                ObjEmailPopUp.MsSubject = "Employee Loan";
                ObjEmailPopUp.EmailFormType = EmailFormID.EmployeeLoan;
                ObjEmailPopUp.EmailSource = MobjClsBLLEmployeeLoan.GetLoanDetails();
                ObjEmailPopUp.ShowDialog();
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            SaveLoan();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void cboCurrency_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCurrency.DroppedDown = false;
        }

        private void cboLoanType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboLoanType.DroppedDown = false;
        }

        private void cboInterestType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboInterestType.DroppedDown = false;
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
            //  FillRemarks();
        }

        private void cboLoanType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtLoanNumber_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpDeductEndDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void btnLoanType_Click(object sender, EventArgs e)
        {
            int intLoanTypeID = cboLoanType.SelectedValue.ToInt32();
            FrmCommonRef objFrmCommonRef = new FrmCommonRef("Loan Type", new int[] { 1, 0 }, "LoanTypeID,Description,DescriptionArb", "LoanTypeReference", "");
            objFrmCommonRef.ShowDialog();
            LoadCombos(2);
            if (objFrmCommonRef.NewID != 0)
                cboLoanType.SelectedValue = objFrmCommonRef.NewID;
            else
                cboLoanType.SelectedValue = intLoanTypeID;
            objFrmCommonRef.Dispose();
        }

        private void dgvInstallmentDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvInstallmentDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dgvInstallmentDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ChangeStatus();
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                MblnGridChangeStatus = true;
                if (dgvInstallmentDetails.Rows[0].Cells["InstallmentDate"].Value != null)
                    dtpDeductStartDate.Value = dgvInstallmentDetails.Rows[0].Cells["InstallmentDate"].Value.ToDateTime();
                if (dgvInstallmentDetails.Rows[dgvInstallmentDetails.Rows.Count - 1].Cells["InstallmentDate"].Value != null)
                    lblDeductEndDate.Text = dgvInstallmentDetails.Rows[dgvInstallmentDetails.Rows.Count - 1].Cells["InstallmentDate"].Value.ToDateTime().ToString("dd MMM yyyy");
                MblnGridChangeStatus = false;
                if (!ClsCommonSettings.IsAmountRoundByZero)
                {
                    if (e.ColumnIndex == Amount.Index)
                    {
                        CalculatePaymentAmount1(e.RowIndex);
                    }
                }
            }

        }

        private void dgvInstallmentDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                //if (e.ColumnIndex == InstallmentDate.Index)
                //{
                //    if (!MblnAddStatus && e.RowIndex > 0)
                //    {
                //        if (dgvInstallmentDetails.Rows[e.RowIndex - 1].Cells["InstallmentDate"].Value.ToDateTime().Date > dgvInstallmentDetails.Rows[e.RowIndex].Cells["InstallmentDate"].Value.ToDateTime().Date)
                //        {
                //            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10208, out MmsgMessageIcon).Replace("#", "").Trim();
                //            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                //            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("\n") + 1);
                //            dgvInstallmentDetails.Focus();
                //            dgvInstallmentDetails.CurrentCell = dgvInstallmentDetails[e.ColumnIndex, e.RowIndex];
                //            dgvInstallmentDetails.Rows[e.RowIndex].Cells["InstallmentDate"].Value = dtpDeductEndDate.Value;
                //        }
                //    }
                //}
                //else
                if (e.ColumnIndex == InterestAmount.Index)
                {
                    CalculatePaymentAmount();
                }
            }
        }

        private void CalculatePaymentAmount1(int iRowIndex)
        {
            try
            {
                decimal decAmount = 0, decRemainingAmt, decNewInstallmentAmt = 0;
                int iCount = (dgvInstallmentDetails.Rows.Count - iRowIndex - 1);
                int iCounter = 0;
                if (iCount <= 0)
                    return;
                for (iCounter = 0; iCounter <= iRowIndex; iCounter++)
                {
                    decAmount += dgvInstallmentDetails.Rows[iCounter].Cells["Amount"].Value.ToDecimal() - dgvInstallmentDetails.Rows[iCounter].Cells["InterestAmount"].Value.ToDecimal();
                }
                decRemainingAmt = txtAmount.Text.ToDecimal() - decAmount;

                decNewInstallmentAmt = (decRemainingAmt / iCount) + ((decRemainingAmt * txtInterestRate.Text.ToDecimal()) / (100 * iCount));
                if (decNewInstallmentAmt > 0)
                {
                    for (int i = iCounter; i < dgvInstallmentDetails.Rows.Count; i++)
                    {
                        dgvInstallmentDetails.Rows[i].Cells["Amount"].Value = decNewInstallmentAmt.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                        dgvInstallmentDetails.Rows[i].Cells["InterestAmount"].Value = (((decRemainingAmt * txtInterestRate.Text.ToDecimal()) / (100 * iCount))).ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
                    }

                }
            }
            catch (Exception Ex1)
            {
                ClsLogWriter.WriteLog(Ex1, LogSeverity.Error);

            }

        }

        private void CalculatePaymentAmount()
        {
            decimal decAmount = 0;

            foreach (DataGridViewRow dgvRow in dgvInstallmentDetails.Rows)
            {
                decAmount = decAmount + dgvRow.Cells["Amount"].Value.ToDecimal();
            }

            //if (txtInterestRate.Text.ToDecimal() == 0)
            //{
            //    txtPaymentAmount.Text = txtAmount.Text.ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
            //}
            //else
            //    txtPaymentAmount.Text = decAmount.ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
        }

        private void dgvInstallmentDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            
            //string strSearchKey = txtSearch.Text;
            DataTable datRecCount = null;

            txtSearch.Text = txtSearch.Text.Trim().Replace("'", "`");
            MstrSearch = txtSearch.Text.Trim();

            datRecCount = MobjClsCommonUtility.FillCombos(new string[] { "COUNT(1) AS COUNT", "Loan L INNER JOIN EmployeeMaster EM ON L.EmployeeID = EM.EmployeeID ", " (" + EmployeeID + " = 0 OR L.EmployeeID = " + EmployeeID + " ) AND (EM.EmployeeFullname like  '%" + MstrSearch + "%' OR EM.EmployeeNumber like '%" + MstrSearch + "%' OR LoanNumber like '%" + MstrSearch + "%')" });

            if (datRecCount != null)
            {
                if (datRecCount.Rows[0]["COUNT"].ToInt32() <= 0 && txtSearch.Text != string.Empty)
                {
                    MstrSearch = "";
                    MessageBox.Show(MobjNotification.GetErrorMessage(MaMessageArr, 40, out MmsgMessageIcon).Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSearch.Text = string.Empty;
                    return;
                }
                MintRecordCount = 0;
                MintCurrentRecordCount = 0;
                MintRecordCount = datRecCount.Rows[0]["COUNT"].ToInt32();
                MintCurrentRecordCount = MintRecordCount;
                BindingNavigatorCountItem.Text = strBindingOf + MintRecordCount.ToStringCustom();
                BindingNavigatorPositionItem.Text = MintRecordCount.ToStringCustom();
            }

            BindingNavigatorMoveFirstItem_Click(null, null);
        }

        private void txtAmount_Leave(object sender, EventArgs e)
        {
            if (txtAmount.Text != string.Empty)
            {
                //Amount cannot be lesser than paid amount
                if (txtAmount.Text.ToDecimal() < MdecPaidAmount)
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1617, out MmsgMessageIcon).Replace("#", "").Trim();
                    MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    return;
                }
                else
                    GenerateInstallments();
            }
            else
                txtPaymentAmount.Text = "0".ToDecimal().ToString("F" +  ((ClsCommonSettings.IsAmountRoundByZero ==true)? MintScaleZ : MintScale));
        }

        private void txtNoOfInstallments_Leave(object sender, EventArgs e)
        {
            if (txtNoOfInstallments.Text.ToInt32() <= 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1620, out MmsgMessageIcon).Replace("#", "").Trim();
                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                txtNoOfInstallments.Text = string.Empty;
                return;
            }
            //No of installments cannot be less than paid installments
            if (txtNoOfInstallments.Text.ToInt32() <= MintPaidCount)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1618, out MmsgMessageIcon).Replace("#", "").Trim();
                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                txtNoOfInstallments.Text = string.Empty;
                return;
            }
        }

        private void chkClosed_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        /// <summary>
        /// Shortcut keys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmEmployeeLoan_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Prev item
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Next item
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//First item
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());//Last item
                        break;
                    case Keys.Control | Keys.P:
                        BtnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        BtnEmail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.F1:
                        BtnHelp_Click(sender, new EventArgs());//help
                        break;
                }
            }
            catch
            {
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {

            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Loan";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void txtReason_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(null, null);
        }

        private void FrmEmployeeLoan_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 8, out MmsgMessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = strInvalidChars + ".";
            }

            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))) && ClsCommonSettings.IsAmountRoundByZero == false)
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ClsCommonSettings.IsAmountRoundByZero == true)
            {
                e.Handled = true;
            }
        }
        private void dgvInstallmentDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvInstallmentDetails.CurrentCell.ColumnIndex == Amount.Index)
            {
                TextBox txt = (TextBox)(e.Control);
                txt.KeyPress += new KeyPressEventHandler(txt_KeyPress);
            }

        }

    }
}
