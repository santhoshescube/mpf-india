﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace MyPayfriend
{
    public partial class FrmEmployeePettyCash : Form
    {
        clsBLLEmployeePettyCash MobjclsBLLEmployeePettyCash = new clsBLLEmployeePettyCash();
        long TotalRecordCnt, CurrentRecCnt;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool mblnAddStatus = true;
        private clsMessage objUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.PettyCash);

                return this.objUserMessage;
            }
        }

        public FrmEmployeePettyCash()
        {
            InitializeComponent();
        }

        private void FrmEmployeePettyCash_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos();
            ClearControls();
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 2)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.PettyCash, 
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void LoadCombos()
        {
            DataTable datTemp = new DataTable();

            if (mblnAddStatus)
            {
                datTemp = MobjclsBLLEmployeePettyCash.FillCombos(new string[] { "EmployeeID," +
                    "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber ELSE EmployeeFullName END AS EmployeeName", "" +
                    "EmployeeMaster", "WorkStatusID >= 6  ORDER BY EmployeeName" });
            }            
            else
            {
                datTemp = MobjclsBLLEmployeePettyCash.FillCombos(new string[] { "EmployeeID," +
                    "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber ELSE EmployeeFullName END AS EmployeeName", "" +
                    "EmployeeMaster", "1=1 ORDER BY EmployeeName" });
            }

            cboEmployee.DataSource = datTemp;
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DisplayMember = "EmployeeName";
        }

        private void ClearControls()
        {
            mblnAddStatus = true;
            cboEmployee.Tag = 0;
            LoadCombos();
            cboEmployee.SelectedIndex = -1;
            txtAmount.Text = txtActualAmount.Text = txtEmployeeRemarks.Text = txtDescription.Text = "";
            dtpDate.Value = ClsCommonSettings.GetServerDate();
            lblDocumentAttached.Text = "No";
            
            RecordCount();
            BindingNavigatorCountItem.Text = "of " + (TotalRecordCnt + 1).ToString();
            BindingNavigatorPositionItem.Text = (TotalRecordCnt + 1).ToString();
            getPettyCashNo();

            cboEmployee.Enabled = true;
            txtActualAmount.Enabled = txtEmployeeRemarks.Enabled = false;

            btnAddNew.Enabled = MblnAddPermission;
            btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = btnDocument.Enabled = false;
        }

        private void RecordCount()
        {
            TotalRecordCnt = MobjclsBLLEmployeePettyCash.GetRecordCount();
            CurrentRecCnt = TotalRecordCnt;
            BindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorCountItem.Text = "of 0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }

            BindingEnableDisable();
        }

        private void BindingEnableDisable()
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

        private void getPettyCashNo()
        {
            txtPettyCashNo.Text = "";
            DataTable datTemp = MobjclsBLLEmployeePettyCash.FillCombos(new string[] { "ISNULL(MAX(PettyCashNo),0) + 1 AS PettyCashNo", "PayPettyCash", "" });
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    txtPettyCashNo.Text = datTemp.Rows[0]["PettyCashNo"].ToString();
            }
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            SavePettyCash();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SavePettyCash();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SavePettyCash();
            this.Close();
        }

        private bool SavePettyCash()
        {
            if (FormValidation())
            {
                bool blnSave = false;

                if (mblnAddStatus)
                {
                    if (this.UserMessage.ShowMessage(1) == true) // Save
                        blnSave = true;
                }
                else
                {
                    if (this.UserMessage.ShowMessage(3) == true) // Update
                        blnSave = true;
                }

                if (blnSave)
                {
                    FillParameters();

                    DataTable datTemp = MobjclsBLLEmployeePettyCash.FillCombos(new string[] { "1", "PayPettyCash", "PettyCashNo = " + txtPettyCashNo.Text.Trim() + "" });
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                            getPettyCashNo();
                    }

                    if (MobjclsBLLEmployeePettyCash.SavePettyCash())
                    {
                        if (mblnAddStatus)
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(2);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(2); // Saved Successfully
                        }
                        else
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(21);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(21); // Updated Successfully
                        }

                        ClearControls();
                    }
                }
            }

            return true;
        }

        private bool FormValidation()
        {
            if (cboEmployee.SelectedIndex == -1)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(9124);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(9124, cboEmployee);
                return false;
            }

            if (txtAmount.Text.ToDecimal() == 0)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(101);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(101, txtAmount);
                return false;
            }

            if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate())
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(9006).Replace("*","");
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(9006, dtpDate);
                return false;
            }

            return true;
        }

        private void FillParameters()
        {
            MobjclsBLLEmployeePettyCash.PobjclsDTOEmployeePettyCash.lngPettyCashID = cboEmployee.Tag.ToInt64();
            MobjclsBLLEmployeePettyCash.PobjclsDTOEmployeePettyCash.lngEmployeeID = cboEmployee.SelectedValue.ToInt64();
            MobjclsBLLEmployeePettyCash.PobjclsDTOEmployeePettyCash.strPettyCashNo = txtPettyCashNo.Text;
            MobjclsBLLEmployeePettyCash.PobjclsDTOEmployeePettyCash.dtDate = (dtpDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            MobjclsBLLEmployeePettyCash.PobjclsDTOEmployeePettyCash.decAmount = txtAmount.Text.ToDecimal();
            MobjclsBLLEmployeePettyCash.PobjclsDTOEmployeePettyCash.decActualAmount = txtActualAmount.Text.ToDecimal();
            MobjclsBLLEmployeePettyCash.PobjclsDTOEmployeePettyCash.strEmployeeRemarks = txtEmployeeRemarks.Text;
            MobjclsBLLEmployeePettyCash.PobjclsDTOEmployeePettyCash.strDescription = txtDescription.Text;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.UserMessage.ShowMessage(13) == true)
            {
                MobjclsBLLEmployeePettyCash.PobjclsDTOEmployeePettyCash.lngPettyCashID = cboEmployee.Tag.ToInt64();
                if (MobjclsBLLEmployeePettyCash.DeletePettyCash() == true)
                {
                    lblstatus.Text = this.UserMessage.GetMessageByCode(4);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(4);
                    ClearControls();
                }
            }            
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = "of 1";

            DisplayPettyCash();
            BindingEnableDisable();
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();

            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
                BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = "of 1";

            DisplayPettyCash();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();

            if (TotalRecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                else
                    BindingNavigatorPositionItem.Text = "1";
            }

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = "of 1";

            DisplayPettyCash();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                BindingNavigatorCountItem.Text = "of " + TotalRecordCnt.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                BindingNavigatorCountItem.Text = "of 1";
            }

            DisplayPettyCash();
            BindingEnableDisable();
        }

        private void DisplayPettyCash()
        {
            DataTable datTemp = MobjclsBLLEmployeePettyCash.DisplayPettyCash(BindingNavigatorPositionItem.Text.ToInt64());

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    mblnAddStatus = false;
                    cboEmployee.Enabled = false;
                    txtActualAmount.Enabled = txtEmployeeRemarks.Enabled = true;
                    LoadCombos();

                    cboEmployee.Tag = datTemp.Rows[0]["PettyCashID"].ToInt64();
                    cboEmployee.SelectedValue = datTemp.Rows[0]["EmployeeID"].ToInt64();
                    txtPettyCashNo.Text = datTemp.Rows[0]["PettyCashNo"].ToString();
                    dtpDate.Value = datTemp.Rows[0]["Date"].ToDateTime();
                    txtAmount.Text = datTemp.Rows[0]["Amount"].ToString();
                    txtActualAmount.Text = datTemp.Rows[0]["ActualAmount"].ToString();
                    txtEmployeeRemarks.Text = datTemp.Rows[0]["Remarks"].ToString();
                    txtDescription.Text = datTemp.Rows[0]["Description"].ToString();

                    datTemp = null;

                    datTemp = MobjclsBLLEmployeePettyCash.FillCombos(new string[] {"COUNT(ISNULL(Node,0)) AS DocumentCount", "TreeMaster", "" +
			            "DocumentTypeID = " + (int)DocumentType.Expense + " AND NavID = " +(int)OperationType.Employee + " AND " +
                        "RecordID = " + cboEmployee.Tag.ToInt64() + " AND CommonId = " + cboEmployee.SelectedValue.ToInt64() + " AND " +
                        "ISNULL(ParentNode,'') <> ''"});
                    lblDocumentAttached.Text = "No Document";

                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            if (datTemp.Rows[0]["DocumentCount"].ToInt32() > 0)
                                lblDocumentAttached.Text = datTemp.Rows[0]["DocumentCount"].ToString() + " Document";
                        }
                    }

                    ChangeStatus();
                }
            }
        }

        private void btnDocument_Click(object sender, EventArgs e)
        {
            using (FrmScanning objScanning = new FrmScanning())
            {
                objScanning.MintDocumentTypeid = (int)DocumentType.Expense;
                objScanning.MlngReferenceID = cboEmployee.SelectedValue.ToInt64();
                objScanning.MintOperationTypeID = (int)OperationType.Employee;
                objScanning.MstrReferenceNo = dtpDate.Value.ToString("dd MMM yyyy") + " [" + txtPettyCashNo.Text + "]";
                objScanning.PblnEditable = true;
                objScanning.MintDocumentID = cboEmployee.Tag.ToInt32();
                objScanning.ShowDialog();
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboEmployee.Tag.ToInt64() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = cboEmployee.Tag.ToInt64();
                DataTable datTemp = MobjclsBLLEmployeePettyCash.FillCombos(new string[] { "CompanyID", "EmployeeMaster", "EmployeeID = " + cboEmployee.SelectedValue.ToInt64() });
                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        ObjViewer.PintCompany = datTemp.Rows[0]["CompanyID"].ToInt32();
                }
                ObjViewer.PiFormID = (int)FormID.PettyCash;
                ObjViewer.ShowDialog();
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboEmployee.Tag.ToInt64() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Petty Cash";
                        ObjEmailPopUp.EmailFormType = EmailFormID.PettyCash;
                        ObjEmailPopUp.EmailSource = MobjclsBLLEmployeePettyCash.DisplayPettyCashDetails(cboEmployee.Tag.ToInt64());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = "";
            tmrClear.Enabled = false;
        }

        private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void ChangeStatus()
        {
            if (mblnAddStatus)
            {
                btnAddNew.Enabled = MblnAddPermission;
                btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = btnDocument.Enabled = false;
            }
            else
            {
                btnAddNew.Enabled = MblnAddPermission;
                btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                btnDelete.Enabled = MblnDeletePermission;
                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                btnDocument.Enabled = true;
            }
        }

        private void cboEmployee_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtActualAmount_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtEmployeeRemarks_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }
    }
}