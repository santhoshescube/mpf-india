﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
namespace MyPayfriend
{
    public partial class FrmUpdateAccruals : Form
    {
        #region Declarations
        clsDTOUpdateAccurals objclsDTOUpdateAccurals;
        string MsMessageCaption;
        ClsNotification MObjNotification;
        clsBLLUpdateAccurals MObjclsBLLUpdateAccrural;
        clsBLLUserInformation MObjclsBLLUserInformation;
        ClsBLLCompanySettings MObjclsBLLCompanySettings;
        clsBLLRptPayments MobjclsBLLRptPayments;

        ArrayList MaMessageArray;
        ArrayList MaStatusMessage;
        private clsMessage objUserMessage = null;
        public int gridValueCheck;
        #endregion
        public  FrmUpdateAccruals()
        {
            InitializeComponent();

            objclsDTOUpdateAccurals = new clsDTOUpdateAccurals();
            MsMessageCaption = ClsCommonSettings.MessageCaption;
            MObjNotification = new ClsNotification();
            MObjclsBLLUpdateAccrural = new clsBLLUpdateAccurals();
            MObjclsBLLUserInformation = new clsBLLUserInformation();
            MObjclsBLLCompanySettings = new ClsBLLCompanySettings();
            MobjclsBLLRptPayments = new clsBLLRptPayments();
        }

        private void FrmUpdateAccruals_Load(object sender, EventArgs e)
        {
       

            try
            {
                LoadCombos();
                int iMonth = dtpToDate.Value.Date.Month;
                int iYear = dtpToDate.Value.Date.Year;
                dtpToDate.Value = Convert.ToDateTime("01-" + Convert.ToString(GetCurrentMonthStr(iMonth)) + "-" + Convert.ToString(iYear) + "");
            }
            catch
            {
            }

        }

        private void LoadCombos()
        {
            DataTable datCombos = new ClsCommonUtility().FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID>0 order by CompanyID" });
            cboCompany.DisplayMember = "CompanyName";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = datCombos;
           

        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            FillEmployeeDetails();
            btnUpdate.Enabled = true;
            btnShow.Enabled = false;
            pbUpdate.Value = 0;
        }
        private void FillEmployeeDetails()
        {
            try
            {
                //dgvEmployee.DataSource = MObjclsBLLUpdateAccrural.GetEmployeeList(cboCompany.SelectedValue.ToInt32(), Convert.ToDateTime(dtpToDate.Text.ToString()).ToString("dd-MMM-yyyy"));
                //dgvEmployee.Columns["GratuityAmount"].DefaultCellStyle.Format = "n" + 2.ToString();
                //dgvEmployee.Columns["GratuityAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //dgvEmployee.Columns["LeavePassageAmount"].DefaultCellStyle.Format = "n" + 2.ToString();
                //dgvEmployee.Columns["LeavePassageAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                this.pbUpdate.Visible = true;
                this.gridValueCheck = 1;
                int num = 1;
                DateTime dateTime1 = Convert.ToDateTime(this.dtpToDate.Text);
                DateTime dateTime2 = new DateTime(dateTime1.Year, dateTime1.Month, 1);
                DateTime dateTime3 = new DateTime(dateTime2.Year, dateTime2.Month, 1).AddMonths(1).AddDays(-1.0);
                this.dgvEmployee.AutoGenerateColumns = false;
                DataTable dataTable1 = new DataTable();
                DataTable employeeList = this.MObjclsBLLUpdateAccrural.GetEmployeeList(this.cboCompany.SelectedValue.ToInt32(), Convert.ToDateTime(dateTime2).ToString("dd-MMM-yyyy"));
                if (employeeList.Rows.Count > 0)
                {
                    this.pbUpdate.Value = 0;
                    this.dgvEmployee.DataSource = (object)employeeList;
                    this.dgvEmployee.Columns["GratuityAmount"].DefaultCellStyle.Format = "n" + 2.ToString();
                    this.dgvEmployee.Columns["GratuityAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    this.dgvEmployee.Columns["LeavePassageAmount"].DefaultCellStyle.Format = "n" + 2.ToString();
                    this.dgvEmployee.Columns["LeavePassageAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    int index = 0;
                    foreach (DataRow row1 in (InternalDataCollectionBase)employeeList.Rows)
                    {
                        DataTable dataTable2 = this.MObjclsBLLUpdateAccrural.isEmployee(row1["EmployeeID"].ToInt32(), Convert.ToDateTime(dateTime2).ToString("dd-MMM-yyyy"), this.cboCompany.SelectedValue.ToInt32());
                        if (dataTable2.Rows.Count > 0)
                        {
                            DataRow row2 = dataTable2.Rows[0];
                            this.dgvEmployee.Rows[index].Cells["GratuityAmount"].Value = (object)row2["gratuityamt"].ToString();
                            this.dgvEmployee.Rows[index].Cells["GratuityArrAmt"].Value = (object)row2["GratuityArrAmt"].ToString();
                            this.dgvEmployee.Rows[index].Cells["LeavePassageAmount"].Value = (object)row2["leavepassageamt"].ToString();
                            this.dgvEmployee.Rows[index].Cells["Ticket"].Value = (object)Math.Round(Convert.ToDecimal(row2["TicketAmt"]), 2).ToString();
                            if (row2["isupdated"].ToString() == "True")
                            {
                                this.dgvEmployee.Rows[index].Cells["isupdated"].Value = (object)1;
                                this.dgvEmployee.Rows[index].ReadOnly = true;
                                Color color = ColorTranslator.FromHtml("#d9d9d9");
                                this.dgvEmployee.Rows[index].DefaultCellStyle.BackColor = color;
                            }
                            else
                                this.dgvEmployee.Rows[index].Cells["isupdated"].Value = (object)0;
                        }
                        else
                        {
                            DataSet gratuityDetailsAccural = clsBLLRptPayments.GetGratuityDetailsAccural(this.cboCompany.SelectedValue.ToInt32(), row1["EmployeeID"].ToInt32(), dateTime2.Date.ToString("dd MMM yyy"), dateTime3.Date.ToString("dd MMM yyy"), row1["GratuityDays"].ToInt32(), -7, 1);
                            Application.DoEvents();
                            this.dgvEmployee.Rows[index].Cells["isupdated"].Value = (object)0;
                            if (gratuityDetailsAccural.Tables.Count > 0 && gratuityDetailsAccural.Tables[0].Rows.Count > 0)
                            {
                                this.dgvEmployee.Rows[index].Cells["GratuityAmount"].Value = gratuityDetailsAccural.Tables[0].Compute("Sum(GratuityDays)", "");
                                if (gratuityDetailsAccural.Tables[0].Rows[0]["GratuityDays"].ToDecimal() > new Decimal(0))
                                    this.dgvEmployee.Rows[index].Cells["GratuityAmount"].Value = (object)Math.Round(Convert.ToDecimal(gratuityDetailsAccural.Tables[0].Compute("Sum(GratuityAmount)", "").ToDecimal() / gratuityDetailsAccural.Tables[0].Compute("Sum(GratuityDays)", "").ToDecimal()), 2, MidpointRounding.AwayFromZero);
                                this.dgvEmployee.Rows[index].Cells["GratuityAmount"].Value = gratuityDetailsAccural.Tables[0].Compute("Sum(GratuityAmount)", "");
                                this.dgvEmployee.Rows[index].Cells["GratuityAmount"].Value = (object)Convert.ToDecimal(this.dgvEmployee.Rows[index].Cells["GratuityAmount"].Value.ToDecimal() * (Decimal)num);
                            }
                            DataSet vacationDetails = clsBLLRptPayments.GetVacationDetails(this.cboCompany.SelectedValue.ToInt32(), row1["EmployeeID"].ToInt32(), Convert.ToDateTime(dateTime3).ToString("dd-MMM-yyyy"), 1);
                            if (vacationDetails.Tables.Count > 1 && vacationDetails.Tables[0].Rows.Count > 0)
                            {
                                DataRow dataRow1 = ((IEnumerable<DataRow>)vacationDetails.Tables[1].Select("AdditionDeductionID=15")).FirstOrDefault<DataRow>();
                                DataRow dataRow2 = ((IEnumerable<DataRow>)vacationDetails.Tables[1].Select("AdditionDeductionID=22")).FirstOrDefault<DataRow>();
                                if (dataRow1 != null && vacationDetails.Tables[1].Rows.Count > 0)
                                    this.dgvEmployee.Rows[index].Cells["LeavePassageAmount"].Value = (object)Math.Round(dataRow1["Amount"].ToDecimal(), 2);
                                if (dataRow2 != null && dataRow2 != null)
                                    this.dgvEmployee.Rows[index].Cells["Ticket"].Value = (object)Math.Round(dataRow2["Amount"].ToDecimal(), 2);
                            }
                        }
                        ++index;
                        this.pbUpdate.Value = Convert.ToInt32(index * 100 / employeeList.Rows.Count);
                    }
                    foreach (DataGridViewColumn column in (BaseCollection)this.dgvEmployee.Columns)
                        column.SortMode = DataGridViewColumnSortMode.NotSortable;
                    this.pbUpdate.Value = 100;
                }
                this.pbUpdate.Value = 100;
                this.gridValueCheck = 0;
                this.pbUpdate.Value = 0;
                this.pbUpdate.Visible = false;
            }
            catch
            {

            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.dgvEmployee.Rows.Count <= 1)
                    return;
                this.SaveData();
                if (ClsCommonSettings.ThirdPartyIntegration)
                    new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(ClsCommonSettings.GetServerDate().ToDateTime());

                int num = (int)MessageBox.Show("Accurals Updated Successfully");
                this.btnUpdate.Enabled = true;
                this.btnShow.Enabled = true;
                this.pbUpdate.Value = 0;
                this.FillEmployeeDetails();
            }
            catch
            {
            }
        }

        public void SaveData()
        {
            try
            {
                this.pbUpdate.Visible = true;
                this.pbUpdate.Enabled = true;
                this.pbUpdate.Value = 0;
                for (int index = 0; index < this.dgvEmployee.Rows.Count; ++index)
                {
                    if (this.dgvEmployee.Rows[index].Cells["EmployeeID"].Value != null)
                    {
                        int intEmpID = this.dgvEmployee.Rows[index].Cells["EmployeeID"].Value.ToInt32();
                        DateTime dateTime = Convert.ToDateTime(this.dtpToDate.Value);
                        float GrtAmt = (float)this.dgvEmployee.Rows[index].Cells["GratuityAmount"].Value.ToDouble();
                        float leavesal = (float)this.dgvEmployee.Rows[index].Cells["LeavePassageAmount"].Value.ToDouble();
                        float etotalsal = (float)this.dgvEmployee.Rows[index].Cells["GratuityAmount"].Value.ToDecimal();
                        float grtArr = (float)this.dgvEmployee.Rows[index].Cells["GratuityArrAmt"].Value.ToDecimal();
                        int cmpId = this.cboCompany.SelectedValue.ToInt32();
                        int grtDays = this.dgvEmployee.Rows[index].Cells["GratuityDays"].Value.ToInt32();
                        float Ticket = float.Parse(this.dgvEmployee.Rows[index].Cells["Ticket"].Value.ToString());
                        int isupd = this.dgvEmployee.Rows[index].Cells["isupdated"].Value.ToInt32();
                        MObjclsBLLUpdateAccrural.SaveAccural(intEmpID, dateTime, leavesal, etotalsal, GrtAmt,grtArr, cmpId, grtDays, Ticket, isupd);
                    }
                    this.pbUpdate.Value = Convert.ToInt32(index * 100 / this.dgvEmployee.Rows.Count);
                }
                this.pbUpdate.Value = 100;
                this.pbUpdate.Value = 0;
                this.pbUpdate.Visible = false;
            }
            catch
            {
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                int iMonth = dtpToDate.Value.Date.Month;
                int iYear = dtpToDate.Value.Date.Year;
                dtpToDate.Value = Convert.ToDateTime("01-" + Convert.ToString(GetCurrentMonthStr(iMonth)) + "-" + Convert.ToString(iYear) + "");
            }
            catch
            {
            }

        }
        private string GetCurrentMonthStr(int MonthVal)
        {
            string Months;
            Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;

            }
            return Months;
        }

        private void dgvEmployee_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvEmployee.Rows.Count <= 1 || this.gridValueCheck != 0)
                return;
            this.dgvEmployee.Rows[e.RowIndex].Cells["isupdated"].Value = (object)1;
        }

        private void dgvEmployee_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }
    }
}
