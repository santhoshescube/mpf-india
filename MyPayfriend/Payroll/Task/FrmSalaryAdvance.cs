﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Microsoft.VisualBasic;
/*
 * Created By       : Tijo
 * Created Date     : 20 Aug 2013
 * Purpose          : For Employee Salary Advance
*/
namespace MyPayfriend
{  
    public partial class FrmSalaryAdvance : Form
    {
        clsBLLSalaryAdvance MobjclsBLLSalaryAdvance = new clsBLLSalaryAdvance();
        long TotalRecordCnt, CurrentRecCnt;
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool mblnAddStatus = true;
        private clsMessage objUserMessage = null;
        public long EmployeeID { get; set; } // for load selected Employee, navigating from Employee Form
        string strBindingOf = "Of ";

        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.SalaryAdvance);

                return this.objUserMessage;
            }
        }

        public FrmSalaryAdvance()
        {
            InitializeComponent();
            tmrClear.Interval = ClsCommonSettings.TimerInterval;

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.SalaryAdvance, this);

            strBindingOf = "من ";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            lblStatusShow.Text = "حالة :";
        }

        private void FrmSalaryAdvance_Load(object sender, EventArgs e)
        {
            SetPermissions();
            AddNew();

            try
            {
                if (EmployeeID > 0) // for load selected Employee Details
                {
                    string strEmployee; 
                    // For Loading All Employees
                    mblnAddStatus = false;
                    LoadCombos();
                    mblnAddStatus = true;
                    // End

                    cboEmployee.SelectedValue = EmployeeID;
                    strEmployee = cboEmployee.Text.Split('-').Last();
                    //txtSearch.Text =  strEmployee.Substring(1,strEmployee.IndexOf("[")-1);
                    RecordCount(EmployeeID.ToInt64());

                    if (TotalRecordCnt > 0)
                        BindingNavigatorMoveFirstItem_Click(sender, e);
                    else
                        btnAddNew_Click(sender, e);                        

                    cboEmployee.Enabled = txtSearch.Enabled = btnSearch.Enabled = false;
                }
            }
            catch { }
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.SalaryAdvance,
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            if (ClsCommonSettings.IsHrPowerEnabled)
                MblnAddPermission = MblnUpdatePermission = MblnDeletePermission = false;
        }

        private void LoadCombos()
        {
            DataTable datTemp = new DataTable();

            if (ClsCommonSettings.IsArabicView)
            {
                if (mblnAddStatus)
                {
                    datTemp = MobjclsBLLSalaryAdvance.FillCombos(new string[] { "DISTINCT EM.EmployeeID," +
                    "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullNameArb + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullNameArb + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                    "EmployeeMaster EM INNER JOIN PaySalaryStructure SS "+
                    "ON EM.EmployeeID = SS.EmployeeID AND SS.PaymentClassificationID = 1 AND  SS.PayCalculationTypeID = 1 " +
                    "INNER JOIN CompanyMaster C ON EM.CompanyID=C.CompanyID", "" +
                    "EM.WorkStatusID >= 6 AND EM.CompanyID in(Select CompanyID from UserCompanyDetails where UserID="  +ClsCommonSettings.UserID+ ") ORDER BY EmployeeName" });
                }
                else
                {
                    datTemp = MobjclsBLLSalaryAdvance.FillCombos(new string[] { "EmployeeID," +
                    "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullNameArb + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullNameArb + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "1=1 ORDER BY EmployeeName" });
                }
            }
            else
            {
                if (mblnAddStatus)
                {
                    datTemp = MobjclsBLLSalaryAdvance.FillCombos(new string[] { "DISTINCT EM.EmployeeID," +
                    "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                    "EmployeeMaster EM INNER JOIN PaySalaryStructure SS "+
                    "ON EM.EmployeeID = SS.EmployeeID AND SS.PaymentClassificationID = 1 AND  SS.PayCalculationTypeID = 1 " +
                    "INNER JOIN CompanyMaster C ON EM.CompanyID=C.CompanyID", "" +
                    "EM.WorkStatusID >= 6 AND EM.CompanyID in(Select CompanyID from UserCompanyDetails where UserID="  +ClsCommonSettings.UserID +") ORDER BY EmployeeName" });
                }
                else
                {
                    datTemp = MobjclsBLLSalaryAdvance.FillCombos(new string[] { "EmployeeID," +
                    "CASE WHEN ISNULL(EmployeeNumber,'')<> '' THEN EmployeeFullName + ' - ' + EmployeeNumber + ' ['+C.ShortName+ ']' ELSE EmployeeFullName + ' ['+C.ShortName+ ']' END AS EmployeeName", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID", "1=1 ORDER BY EmployeeName" });
                }
            }

            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DisplayMember = "EmployeeName";
            cboEmployee.DataSource = datTemp;
        }

        private void AddNew()
        {
            mblnAddStatus = true;            
            cboEmployee.Tag = 0;
            LoadCombos();
            cboEmployee.SelectedIndex = -1;
            txtAmount.Text = txtRemarks.Text = txtSearch.Text = lblCurrency.Text = lblJoiningDate.Text = "";
            lblBasicPayAmount.Text = lblSalAdvPercentAmt.Text = lblTotalSalaryAdvance.Text = "";
            dtpDate.Value = ClsCommonSettings.GetServerDate();
            txtAmount.Tag = 0;
            dtpDate.Tag = 0;            
            lblstatus.Text = "";
            tmrClear.Enabled = false;
            errValidate.Clear();
            
            SetAutoCompleteList();
            RecordCount(EmployeeID.ToInt64());
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt + 1).ToString();
            BindingNavigatorPositionItem.Text = (TotalRecordCnt + 1).ToString();

            GrpMain.Enabled = cboEmployee.Enabled = txtSearch.Enabled = btnSearch.Enabled = true;

            btnClear.Enabled = true;
            btnAddNew.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false;

            cboEmployee.Focus();
        }

        private void RecordCount(long lngEmployeeID)
        {
            MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.strSearchKey = txtSearch.Text.Trim(); // Is any search text filteration will be taken that also
            TotalRecordCnt = MobjclsBLLSalaryAdvance.GetRecordCount(lngEmployeeID);
            CurrentRecCnt = TotalRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                CurrentRecCnt = 0;
                TotalRecordCnt = 0;
            }

            BindingEnableDisable();
        }

        private void BindingEnableDisable() // Navigator visibility settings
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;

            if (TotalRecordCnt == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == Convert.ToDouble(TotalRecordCnt))
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (Convert.ToDouble(BindingNavigatorPositionItem.Text) == 1)
                {
                    BindingNavigatorMoveFirstItem.Enabled = false;
                    BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
        }

        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteList(txtSearch.Text.Trim(), 0, "", "PaySalaryAdvance");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            if (EmployeeID == 0)
                AddNew();
            else
                AddNewEmployeeDetails();
        }

        private void AddNewEmployeeDetails() // add new Salary advance for selected employee
        {
            mblnAddStatus = true;
            cboEmployee.Tag = 0;
            txtAmount.Text = txtRemarks.Text = lblCurrency.Text = "";
            dtpDate.Value = ClsCommonSettings.GetServerDate();
            txtAmount.Tag = 0;
            dtpDate.Tag = 0;
            lblstatus.Text = "";
            tmrClear.Enabled = false;
            errValidate.Clear();

            SetAutoCompleteList();
            RecordCount(EmployeeID.ToInt64());
            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt + 1).ToString();
            BindingNavigatorPositionItem.Text = (TotalRecordCnt + 1).ToString();

            GrpMain.Enabled = true;

            btnClear.Enabled = true;
            btnAddNew.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false;
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            SaveSalaryAdvance();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveSalaryAdvance())
            {
                btnSave.Enabled = false;
                this.Close();
            }
        }

        private bool SaveSalaryAdvance()
        {
            if (FormValidation())
            {
                bool blnSave = false;

                if (mblnAddStatus)
                {
                    if (this.UserMessage.ShowMessage(1) == true) // Save
                        blnSave = true;
                }
                else
                {
                    if (this.UserMessage.ShowMessage(3) == true) // Update
                        blnSave = true;
                }

                if (blnSave)
                {
                    FillParameters(); // fill parameters for saving the content

                    int intRetValue = MobjclsBLLSalaryAdvance.SaveSalaryAdvanceDetails();

                    if (intRetValue > 0)
                    {
                        if (ClsCommonSettings.ThirdPartyIntegration)
                            new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpDate.Value);

                        if (mblnAddStatus)
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(2);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(2); // Saved Successfully
                        }
                        else
                        {
                            lblstatus.Text = this.UserMessage.GetMessageByCode(21);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(21); // Updated Successfully
                        }

                        DisplaySalaryAdvanceAgainstRecordID(intRetValue); // to display the current saved or updated content
                        SetAutoCompleteList();
                        return true;
                    }
                }
            }

            return false;
        }

        private bool FormValidation()
        {
            txtAmount.Focus();
            errValidate.Clear();

            if (cboEmployee.SelectedIndex == -1) // if employee is not selected properly
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(9124);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(9124, cboEmployee, errValidate);
                return false;
            }

            if (txtAmount.Text.ToDecimal() == 0) // if Salary Advance Amount is not enter
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(3068);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(3068, txtAmount, errValidate);
                return false;
            }

            // Salary Advance Amount should not exceeds the allwed salary advance limit
            decimal decMaxAmount = (lblBasicPayAmount.Text.ToDecimal() * (lblSalAdvPercentAmt.Text.ToDecimal() /100)) - 
                (lblTotalSalaryAdvance.Text.ToDecimal() - txtAmount.Tag.ToDecimal());
            
            if (txtAmount.Text.ToDecimal() > decMaxAmount)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(3082);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(3082, txtAmount, errValidate);
                return false;
            }

            if (dtpDate.Value.Date > ClsCommonSettings.GetServerDate()) // if Salary Advance date is greater than current date
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(3072);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(3072, dtpDate, errValidate);
                return false;
            }

            if (dtpDate.Value.Date < lblJoiningDate.Text.ToDateTime().Date) // if Salary Advance date is less than Employee Joining Date
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(3081);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(3081, dtpDate, errValidate);
                return false;
            }

            // Check Employee Salary is Processed between this period
            if (MobjclsBLLSalaryAdvance.CheckEmployeeSalaryIsProcessed(cboEmployee.SelectedValue.ToInt64(), 
                (dtpDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime()) > 0)
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(3076);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(3076);
                return false;
            }

            return true;
        }

        private void FillParameters()
        {
            MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.intSalaryAdvanceID = cboEmployee.Tag.ToInt32();
            MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.lngEmployeeID = cboEmployee.SelectedValue.ToInt64();
            MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.decAmount = txtAmount.Text.ToDecimal();
            MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.dtDate = (dtpDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.strRemarks = txtRemarks.Text;

            if (mblnAddStatus)
            {
                DataTable datTemp = MobjclsBLLSalaryAdvance.FillCombos(new string[] { "CM.CurrencyId", "EmployeeMaster EM " +
                    "INNER JOIN CompanyMaster CM ON EM.CompanyID = CM.CompanyID", "EM.EmployeeID = " + MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.lngEmployeeID + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.intCurrencyID = datTemp.Rows[0]["CurrencyId"].ToInt32();
                }
            }
        }

        private void DisplaySalaryAdvanceAgainstRecordID(int intRetValue)
        {
            RecordCount(EmployeeID.ToInt64());



            BindingNavigatorCountItem.Text = strBindingOf + (TotalRecordCnt).ToString();
            BindingNavigatorPositionItem.Text = MobjclsBLLSalaryAdvance.getRecordRowNumber(intRetValue, EmployeeID.ToInt64()).ToString(); // get record no. against the Deposit ID

            DisplaySalaryAdvanceDetails();
            BindingEnableDisable();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                // Check Employee Salary is Processed between this period
                if (MobjclsBLLSalaryAdvance.CheckEmployeeSalaryIsProcessed(cboEmployee.SelectedValue.ToInt64(),
                    (dtpDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime()) > 0)
                {
                    lblstatus.Text = this.UserMessage.GetMessageByCode(3076);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(3076);
                    return;
                }

                if (this.UserMessage.ShowMessage(13) == true)
                {
                    MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.intSalaryAdvanceID = cboEmployee.Tag.ToInt32();
                    if (MobjclsBLLSalaryAdvance.DeleteSalaryAdvanceDetails() == true)
                    {
                        if (ClsCommonSettings.ThirdPartyIntegration)
                            new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpDate.Value);

                        lblstatus.Text = this.UserMessage.GetMessageByCode(4); // Deleted Successfully
                        tmrClear.Enabled = true;
                        this.UserMessage.ShowMessage(4);
                        AddNew();
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException Ex)
            {
                string MstrMessageCommon = "";
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                {
                    MstrMessageCommon = "Details exists in the system.Cannot delete";
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
                else
                {
                    MstrMessageCommon = "Error on btnDelete_Click:SalaryAdvance" + Ex.Message.ToString();
                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //mObjLogs.WriteLog("Error on DeleteExpenseDetails:Expense." + Ex.Message.ToString() + "", 3);
                }
            }
            catch (Exception ex)
            {

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnDelete_Click:SalaryAdvance" + ex.Message.ToString());
                //mObjLogs.WriteLog("Error on DeleteExpenseDetails:Expense." + ex.Message.ToString() + "", 3);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboEmployee.Tag.ToInt64() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = cboEmployee.Tag.ToInt64();

                DataTable datTemp = MobjclsBLLSalaryAdvance.FillCombos(new string[] { "TOP 1 CompanyID AS CompanyID", "EmployeeMaster", "" +
                    "EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        ObjViewer.PintCompany = datTemp.Rows[0]["CompanyID"].ToInt32();
                }

                ObjViewer.PiFormID = (int)FormID.SalaryAdvance;
                ObjViewer.ShowDialog();
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboEmployee.Tag.ToInt64() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "SalaryAdvance";
                        ObjEmailPopUp.EmailFormType = EmailFormID.SalaryAdvance;
                        ObjEmailPopUp.EmailSource = MobjclsBLLSalaryAdvance.GetPrintSalaryAdvanceDetails(cboEmployee.Tag.ToInt32());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch { }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "SalaryAdvance";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.txtSearch.Text.Trim() == string.Empty)
                return;

            RecordCount(EmployeeID.ToInt64()); // get record count

            if (TotalRecordCnt == 0) // No records
            {
                lblstatus.Text = this.UserMessage.GetMessageByCode(40);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(40);
                txtSearch.Text = string.Empty;
                btnAddNew_Click(sender, e);
            }
            else if (TotalRecordCnt > 0)
                BindingNavigatorMoveFirstItem_Click(sender, e); // if record found in search then automatically navigate to first item
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplaySalaryAdvanceDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();

            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
                BindingNavigatorPositionItem.Text = "1";

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplaySalaryAdvanceDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();

            if (TotalRecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (TotalRecordCnt > 1)
                    BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                else
                    BindingNavigatorPositionItem.Text = "1";
            }

            if (TotalRecordCnt > 1)
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            else
                BindingNavigatorCountItem.Text = strBindingOf + "1";

            DisplaySalaryAdvanceDetails();
            BindingEnableDisable();
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (TotalRecordCnt > 1)
            {
                BindingNavigatorPositionItem.Text = TotalRecordCnt.ToString();
                BindingNavigatorCountItem.Text = strBindingOf + TotalRecordCnt.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                BindingNavigatorCountItem.Text = strBindingOf + "1";
            }

            DisplaySalaryAdvanceDetails();
            BindingEnableDisable();
        }

        private void DisplaySalaryAdvanceDetails()
        {
            MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.strSearchKey = txtSearch.Text.Trim();
            DataTable datTemp = MobjclsBLLSalaryAdvance.DisplaySalaryAdvanceDetails(BindingNavigatorPositionItem.Text.ToInt32(),EmployeeID.ToInt64()); // Get Salary Advance Details

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    mblnAddStatus = false;
                    cboEmployee.Enabled = btnClear.Enabled = false;
                    LoadCombos();

                    cboEmployee.Tag = datTemp.Rows[0]["SalaryAdvanceID"].ToInt64();
                    cboEmployee.SelectedValue = datTemp.Rows[0]["EmployeeID"].ToInt64();
                    dtpDate.Value = datTemp.Rows[0]["Date"].ToDateTime();
                    txtAmount.Text = datTemp.Rows[0]["Amount"].ToString();
                    txtAmount.Tag = datTemp.Rows[0]["Amount"].ToString(); // For Maximum Advance Amount Validation

                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        txtAmount.Text = txtAmount.Text.ToDecimal().ToString("F" + 0);
                    }


                    if (ClsCommonSettings.IsArabicView)
                        lblCurrency.Text = datTemp.Rows[0]["CurrencyNameArb"].ToString();
                    else
                        lblCurrency.Text = datTemp.Rows[0]["CurrencyName"].ToString();

                    txtRemarks.Text = datTemp.Rows[0]["Remarks"].ToString();
                    MobjclsBLLSalaryAdvance.PobjclsDTOSalaryAdvance.intCurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();

                    getTotalSalaryAdvance();
                    ChangeStatus();

                    if (datTemp.Rows[0]["IsSettled"].ToBoolean())
                        GrpMain.Enabled = btnDelete.Enabled = false;
                    else
                        GrpMain.Enabled = true;

                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false; // disable in update mode. Enable these controls when edit started
                }
            }
        }

        private void ChangeStatus()
        {
            DataTable datTemp = MobjclsBLLSalaryAdvance.FillCombos(new string[] { "1", "EmployeeMaster", "WorkStatusID >= 6 AND EmployeeID = " + cboEmployee.SelectedValue.ToInt64() });
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (mblnAddStatus)
                    {
                        btnAddNew.Enabled = false;
                        btnClear.Enabled = true;
                        btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                        btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
                    }
                    else
                    {
                        btnAddNew.Enabled = MblnAddPermission;                        
                        btnClear.Enabled = false;

                        // if employee is settled don't want to update / delete the Salary Advance details
                        if (MobjclsBLLSalaryAdvance.CheckEmployeeExistance(cboEmployee.SelectedValue.ToInt64()) == 1)
                            btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = false;
                        else
                        {
                            btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                            btnDelete.Enabled = MblnDeletePermission;
                        }
                        // END

                        btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                    }
                }
                else
                {
                    if (!mblnAddStatus)
                    {
                        btnAddNew.Enabled = MblnAddPermission;
                        btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                        btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = btnClear.Enabled = false;
                    }
                }
            }
            else
            {
                if (!mblnAddStatus)
                {
                    btnAddNew.Enabled = MblnAddPermission;
                    btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;
                    btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnDelete.Enabled = btnClear.Enabled = false;
                }
            }
            
            errValidate.Clear();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = "";
            tmrClear.Enabled = false;
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox obj = (ComboBox)sender;
            obj.DroppedDown = false;
        }

        private void ChangeControlStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;
            e.Handled = false;
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            }
            else
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            }

            if (ClsCommonSettings.IsAmountRoundByZero == false && ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        private void FrmSalaryAdvance_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {
                if (this.UserMessage.ShowMessage(8) == true) // Your changes will be lost. Please confirm if you wish to cancel?
                {
                    MobjclsBLLSalaryAdvance = null;
                    objUserMessage = null;
                    e.Cancel = false;
                }
                else
                    e.Cancel = true;
            }
        }

        private void FrmSalaryAdvance_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "LoanRepayment";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (MblnAddPermission)
                            btnAddNew_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (MblnDeletePermission)
                            btnDelete_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (MblnPrintEmailPermission)
                            btnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (MblnPrintEmailPermission)
                            btnEmail_Click(sender, new EventArgs());//Email
                        break;
                }
            }
            catch { }
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            getEmployeeDetails();

            DataTable datTemp = MobjclsBLLSalaryAdvance.FillCombos(new string[] { "CONVERT(VARCHAR,EM.DateofJoining,106) AS DateofJoining," +
                        "CR.Code AS CurrencyName,CR.CurrencyNameArb", "EmployeeMaster EM INNER JOIN CompanyMaster CM ON EM.CompanyID = CM.CompanyID " +
                        "INNER JOIN CurrencyReference CR ON CM.CurrencyId = CR.CurrencyID", "" +
                        "EM.EmployeeID = " + cboEmployee.SelectedValue.ToInt64() + "" });

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    lblJoiningDate.Text = datTemp.Rows[0]["DateofJoining"].ToString(); // For Checking Employee Date of Joining

                    if (mblnAddStatus)
                    {
                        if (ClsCommonSettings.IsArabicView)
                            lblCurrency.Text = datTemp.Rows[0]["CurrencyNameArb"].ToStringCustom();
                        else
                            lblCurrency.Text = datTemp.Rows[0]["CurrencyName"].ToStringCustom();
                    }
                }
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            getEmployeeDetails();
            ChangeStatus();
        }

        private void getEmployeeDetails()
        {
            try
            {
                lblBasicPayAmount.Text = lblSalAdvPercentAmt.Text = lblTotalSalaryAdvance.Text = "";
                dtpDate.Tag = 0;
                lblEmployee.Tag = "";

                DataSet dsTemp = new DataSet();
                dsTemp = MobjclsBLLSalaryAdvance.getSalaryStructureDetails(cboEmployee.SelectedValue.ToInt64());

                if (dsTemp != null)
                {
                    if (dsTemp.Tables.Count > 0)
                    {
                        if (dsTemp.Tables[0] != null)
                        {
                            if (dsTemp.Tables[0].Rows.Count > 0)
                            {
                                lblBasicPayAmount.Text = dsTemp.Tables[0].Rows[0]["Amount"].ToString();
                                dtpDate.Tag = dsTemp.Tables[0].Rows[0]["SalaryDay"].ToString(); // For Checking Total Salary Advance Amount
                            }
                        }

                        if (dsTemp.Tables[1] != null)
                        {
                            if (dsTemp.Tables[1].Rows.Count > 0)
                                lblSalAdvPercentAmt.Text = dsTemp.Tables[1].Rows[0]["ConfigurationValue"].ToString();
                        }
                    }
                }

                getTotalSalaryAdvance();
            }
            catch { }
        }

        private void getTotalSalaryAdvance()
        {
            try
            {
                if (dtpDate.Tag.ToInt32() > 0)
                {
                    int intTempMonth = dtpDate.Value.Month;
                    int intTempYear = dtpDate.Value.Year;
                    int intSalaryDay = dtpDate.Tag.ToInt32();

                    if (intSalaryDay > 15)
                    {
                        intTempMonth = dtpDate.Value.Month - 1;

                        if (intTempMonth == 0)
                        {
                            intTempMonth = 12;
                            intTempYear = intTempYear - 1;
                        }
                    }

                    DateTime dtFromDate = (intSalaryDay.ToString() + "/" + GetCurrentMonth(intTempMonth) + "/" + (intTempYear) + "").ToDateTime();
                    DateTime dtToDate = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtFromDate.Date));

                    if (ClsCommonSettings.IsArabicView)
                    {
                        lblTotalAdvancePeriod.Text = "فخفشم سشمشقغ شيرشىؤث ( " + dtFromDate.ToString("dd MMM yyyy") + " - " + dtToDate.ToString("dd MMM yyyy") + ")";
                    }
                    else
                    {
                        lblTotalAdvancePeriod.Text = "Total Salary Advance ( " + dtFromDate.ToString("dd MMM yyyy") + " - " + dtToDate.ToString("dd MMM yyyy") + ")";
                    }

                    DataTable datTemp = MobjclsBLLSalaryAdvance.getTotalSalaryAdvance(cboEmployee.SelectedValue.ToInt64(), dtFromDate, dtToDate);

                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                            lblTotalSalaryAdvance.Text = datTemp.Rows[0]["Amount"].ToString();
                    }
                }
            }
            catch { }
        }

        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }
    }
}