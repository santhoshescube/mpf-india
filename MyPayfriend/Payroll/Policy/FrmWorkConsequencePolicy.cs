﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    /*****************************************************
   * Created By       : Arun
   * Creation Date    : 10 Apr 2012
   * Description      : Handle WorkConsequence Policy
   * 
   * Modified By      : Ranju Mathew
   * Creation Date    : 12 aug 2013
   * Description      : Tuning and performance improving
   * ***************************************************/
    public partial class FrmWorkConsequencePolicy : Form
    {

        #region Declartions
        public int PintWorkConsequencePolicyID = 0;   // From Refernce Form 
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        private string MstrCommonMessage;   
        private bool MblnIsEditMode = false;  //  To Find Whether Is add mode or Edit Mode
        private int MintRecordCnt = 0;     //Total Record Count
        private int MintCurrentRecCnt = 0; // Current Record Count
        private bool MblnChangeStatus = false;
        private bool MblbtnOk = false;

        private clsBLLWorkConsequencePolicy MobjclsBLLWorkConsequencePolicy = null;
        private clsMessage ObjUserMessage = null;
        string strBindingOf = "Of ";
        #endregion Declartions
        /// <summary>
        /// FORM ID=112
        /// Constructor
        /// </summary>
        #region Constructor
        public FrmWorkConsequencePolicy()  
        {
            InitializeComponent();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.WorkPolicyConsequence, this);

            strBindingOf = "من ";
            dgvWorkConsequenceDetails.Columns["colWorkConsequenceParticulars"].HeaderText = "تفاصيل";
        }
        #endregion Constructor

        #region Properties
        private clsMessage UserMessage  // For Notification Message
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.WorkPolicyConsequence);
                return this.ObjUserMessage;
            }
        }
        private clsBLLWorkConsequencePolicy BLLWorkConsequencePolicy
        {
            get
        {
            if (this.MobjclsBLLWorkConsequencePolicy == null)
            this.MobjclsBLLWorkConsequencePolicy = new clsBLLWorkConsequencePolicy();

            return this.MobjclsBLLWorkConsequencePolicy;
        }
        }
        #endregion Properties

        #region Methods

        private void SetPermissions()  // Function for setting permissions Add/Update/Delete/Email-Print
        {
           
        clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
        if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.WorkConsequencePolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
        else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private void ClearAllControls()   // Funtion to clear All Controls
        {
            this.txtPolicyName.Text = "";
            this.txtPolicyName.Tag = 0;

            this.txtPolicyName.Text = "";
            this.rdbCompanyBased.Checked = true;
            this.rdbActualMonth.Checked = false;
            //this.rdbWorkingDays.Checked = false;
            this.cboCalculationBased.SelectedIndex = -1;
            this.cboCalculationBased.Text = "";
            this.dgvWorkConsequenceDetails.Rows.Clear();
            //this.chkRatePerDay.Checked = false;
            this.chkRateOnly.Checked = false;
            //this.txtHoursPerDay.Text = "";
            this.txtRate.Text = "";

            this.txtCalculationPercent.Text = "";

        }

        private void Changestatus(object sender, EventArgs e) //function for changing status
        {
            
        if (!MblnIsEditMode)
            {
                btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
        else
            {
                btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errWorkConsequencePolicy.Clear();
            MblnChangeStatus = true;

        }
        private bool LoadCombos(int intType) // For Loading the Combos 
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
        try
        {

            if (intType == 0 || intType == 1)//Calculation type
            {
                if (ClsCommonSettings.IsArabicView)
                    datCombos = BLLWorkConsequencePolicy.FillCombos(new string[] { "CalculationID,CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2,3)" });
                else
                    datCombos = BLLWorkConsequencePolicy.FillCombos(new string[] { "CalculationID,Calculation", "PayCalculationReference", "CalculationID in(1,2,3)" });
                cboCalculationBased.ValueMember = "CalculationID";
                cboCalculationBased.DisplayMember = "Calculation";
                cboCalculationBased.DataSource = datCombos;
                blnRetvalue = true;
            }


        }
        catch (Exception Ex)
        {
            ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            blnRetvalue = false;
        }
            return blnRetvalue;
        }
        private void FillAdditionDeductionsGrid(DataGridView Dgv, DataTable dtAddDedDetails) // To fill AdditionDeduction Details in Grid
        {
            Dgv.Rows.Clear();
            if (dtAddDedDetails != null)
            {
                if (dtAddDedDetails.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= dtAddDedDetails.Rows.Count - 1; iCounter++)
                    {
                        Dgv.RowCount = Dgv.RowCount + 1;
                        Dgv.Rows[iCounter].Cells[0].Value = dtAddDedDetails.Rows[iCounter]["AddDedID"].ToInt32();//Addition deduction Policy
                        Dgv.Rows[iCounter].Cells[1].Value = dtAddDedDetails.Rows[iCounter]["Checked"].ToBoolean(); ;//ChkBox
                        if (ClsCommonSettings.IsArabicView)
                            Dgv.Rows[iCounter].Cells[2].Value = dtAddDedDetails.Rows[iCounter]["DescriptionArb"].ToStringCustom();//Description
                        else
                            Dgv.Rows[iCounter].Cells[2].Value = dtAddDedDetails.Rows[iCounter]["Description"].ToStringCustom();//Description
                    }
                }
            }
        }

        private void GetRecordCount()   // TO Get Record Count
        {
            MintRecordCnt = 0;
            MintRecordCnt = BLLWorkConsequencePolicy.GetRecordCount();
            if (MintRecordCnt < 0)
                {
                    BindingNavigatorCountItem.Text = strBindingOf + "0";
                    MintRecordCnt = 0;
                }
        }
        private void RefernceDisplay() // To Display Details In Edit Mode
        {

            int intRowNum = 0;
            intRowNum = BLLWorkConsequencePolicy.GetRowNumber(PintWorkConsequencePolicyID);
            if (intRowNum > 0)
            {
                MintCurrentRecCnt = intRowNum;
                DisplayWorkConsequencePolicyInfo();
            }
        }
        private void AddNewWorkConsequencePolicy()  // Add Mode
        {
            MblnChangeStatus = false;
            MblnIsEditMode = false;
            lblstatus.Text = "";
            tmrClear.Enabled = true;
            errWorkConsequencePolicy.Clear();

            ClearAllControls();  //Clear all controls in the form
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();

            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnPrint.Enabled = false;
            btnEmail.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            //txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            //txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;
            btnClear.Enabled = true;

        }
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                {
                    BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
                }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
                }
        }
        private void DisplayWorkConsequencePolicyInfo() //To Display Detail Info
        {

            FillWorkConsequencePolicyInfo();

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            MblnIsEditMode = true;
            SetBindingNavigatorButtons();
            MblnChangeStatus = false;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            btnOk.Enabled = MblnChangeStatus;
            btnSave.Enabled = MblnChangeStatus;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            //txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            //txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;

            btnClear.Enabled = false;
        }
        private void FillWorkConsequencePolicyInfo() // To fill WorkConsequencePolicy
        {
            ClearAllControls();
            DataTable dtWorkConsequencePolicy = BLLWorkConsequencePolicy.DisplayWorkConsequencePolicy(MintCurrentRecCnt);

            if (dtWorkConsequencePolicy.Rows.Count > 0)
            {

                txtPolicyName.Text = dtWorkConsequencePolicy.Rows[0]["WorkConsequencePolicy"].ToStringCustom();
                txtPolicyName.Tag = dtWorkConsequencePolicy.Rows[0]["WorkConsequencePolicyID"].ToInt32();
           

                for (int iCounter = 0; iCounter <= dtWorkConsequencePolicy.Rows.Count - 1; iCounter++)
                {



                        cboCalculationBased.SelectedValue = dtWorkConsequencePolicy.Rows[iCounter]["CalculationID"].ToInt32();

                        if (dtWorkConsequencePolicy.Rows[iCounter]["CompanyBasedOn"].ToInt32() == 1)
                        {
                            rdbCompanyBased.Checked = true;
                        }
                        else 
                        {
                            rdbActualMonth.Checked = true;
                        }
                        txtCalculationPercent.Text = dtWorkConsequencePolicy.Rows[iCounter]["Percentage"].ToDecimal().ToStringCustom();
                        chkRateOnly.Checked = dtWorkConsequencePolicy.Rows[iCounter]["IsRateOnly"].ToBoolean();
                        txtRate.Text = dtWorkConsequencePolicy.Rows[iCounter]["WorkConsequenceRate"].ToDecimal().ToStringCustom();

                        //chkRatePerDay.Checked = dtWorkConsequencePolicy.Rows[iCounter]["IsRateBasedOnHour"].ToBoolean();
                        //txtHoursPerDay.Text = dtWorkConsequencePolicy.Rows[iCounter]["HoursPerDay"].ToStringCustom();
                        if (dtWorkConsequencePolicy.Rows[iCounter]["CalculationID"].ToInt32() == (int)CalculationType.GrossSalary)
                            {
                                DataTable DtAddDedRef = BLLWorkConsequencePolicy.DisplayAddDedDetails(txtPolicyName.Tag.ToInt32(), 9);
                                FillAdditionDeductionsGrid(dgvWorkConsequenceDetails, DtAddDedRef);
                            }

     
                  
                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        txtRate.Text = txtRate.Text.ToDecimal().ToString("F" + 0);
                        //txtHoursPerDay.Text = txtHoursPerDay.Text.ToDecimal().ToString("F" + 0);

                    }




                }

            }


        }
        private bool FormValidation() // For Validation
        {

            errWorkConsequencePolicy.Clear();
            Control control = null;
            bool blnReturnValue = true;
            if (txtCalculationPercent.Text == "")
            {
                txtCalculationPercent.Text = "0";
            }
            if (txtPolicyName.Text.Trim().Length == 0)     // Please enter Policy Name
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(21625);
                control = txtPolicyName;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked == false && cboCalculationBased.SelectedValue.ToInt32() == 0) //Please select Calculation based for WorkConsequence policy
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(21626);
                control = cboCalculationBased;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked && (txtRate.Text.Trim().Length == 0 || txtRate.Text.ToDecimal()==0))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(21627); //Please enter valid Rate for WorkConsequence policy
                control = txtRate;
                blnReturnValue = false;
            }
            else if (txtRate.Text.Trim().Length > 0)
            {
                try
                {
                    decimal rate = Convert.ToDecimal(txtRate.Text);
                }
                catch
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(21627);  //Please enter valid Rate for WorkConsequence policy
                    control = txtRate;
                    blnReturnValue = false;
                }
            }


            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errWorkConsequencePolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                    if (control.Parent.GetType().FullName == "System.Windows.Forms.TabPage")
                        tbPolicy.SelectedTab = (TabPage)control.Parent;

                }
                return blnReturnValue;
            }

            
            if (blnReturnValue)
            {
                if (BLLWorkConsequencePolicy.CheckDuplicate(txtPolicyName.Tag.ToInt32(), txtPolicyName.Text.Trim()))//CheckDuplicate()
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(21635); //Duplicate policy name.Please change policy name
                    control = txtPolicyName;
                    blnReturnValue = false;
                }
            }
            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errWorkConsequencePolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                    if (control.Parent.GetType().FullName == "System.Windows.Forms.TabPage")
                    {
                        tbPolicy.SelectedTab = (TabPage)control.Parent;
                    }
                }
            }

            return blnReturnValue;
        }
        private bool DeleteValidation()
        {
            if (txtPolicyName.Tag.ToInt32() == 0)
            {
                UserMessage.ShowMessage(9015); // Sorry,No Data Found
                return false;

            }
            if (BLLWorkConsequencePolicy.PolicyIDExists(txtPolicyName.Tag.ToInt32()))
            {
                UserMessage.ShowMessage(21628); //Details exists for this WorkConsequence Policy in the system.Please delete the existing details to proceed with WorkConsequence Policy deletion.
                return false;
            }
            if (UserMessage.ShowMessage(21631) == false) // Do you wish to Delete WorkConsequence policy information?
            {
                return false;
            }
            return true;

        }
        private void FillParameterMaster() // Fill Master Details To DTO 
        {
            
            BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.WorkConsequencePolicyID = txtPolicyName.Tag.ToInt32();
            BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.WorkConsequencePolicy = txtPolicyName.Text.Trim();
            BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.CalculationID = cboCalculationBased.SelectedValue.ToInt32();

            if (rdbCompanyBased.Checked == true)
            {
                BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.CompanyBasedOn = 1;  // CompanyBasedOn
            }

            else if (rdbActualMonth.Checked == true)
            {
                BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.CompanyBasedOn = 2;  // ActualMonth
            }
            else
            {
                BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.CompanyBasedOn = 3;
            }

            BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.IsRateOnly = chkRateOnly.Checked;
            BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.WorkConsequenceRate = chkRateOnly.Checked ? txtRate.Text.Trim().ToDecimal() : 0;
            BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.WorkConsequenceType = 9;
            BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.Percentage = Convert.ToDouble(txtCalculationPercent.Text);
             //BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.FixedAmount = 

        }

        private void FillParameterSalPolicyDetails() // fill AddtionDeduction Details
        {
            BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.DTOSalaryPolicyDetailWorkConsequence = new List<clsDTOSalaryPolicyDetailWorkConsequence>();

            if (dgvWorkConsequenceDetails.Rows.Count > 0)
            {
                dgvWorkConsequenceDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                dgvWorkConsequenceDetails.CurrentCell = dgvWorkConsequenceDetails["colWorkConsequenceParticulars", 0];
                foreach (DataGridViewRow row in dgvWorkConsequenceDetails.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["colWorkConsequenceSelectedItem"].Value) == true)
                    {
                        clsDTOSalaryPolicyDetailWorkConsequence objSalaryPolicyDetail = new clsDTOSalaryPolicyDetailWorkConsequence();
                        objSalaryPolicyDetail.PolicyType = (int)PolicyType.WorkConPolicy;
                        objSalaryPolicyDetail.AdditionDeductionID = row.Cells["colWorkConsequenceAddDedID"].Value.ToInt32();
                        BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.DTOSalaryPolicyDetailWorkConsequence.Add(objSalaryPolicyDetail);
                    }
                }
            }


        }
        private bool SaveWorkConsequencePolicy() // Save WorkConsequence Policy Details
        {
            bool blnRetValue = false;

            if (FormValidation())
            {
                int intMessageCode = 0;
                if (txtPolicyName.Tag.ToInt32() > 0)
                    intMessageCode = 21630;
                else
                    intMessageCode = 21629;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParameterMaster();
                    FillParameterSalPolicyDetails();
                    if (BLLWorkConsequencePolicy.WorkConsequencePolicyMasterSave())
                    {
                        if (!MblnIsEditMode)
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(2);
                            UserMessage.ShowMessage(2, null, null, 2);
                        }
                        else
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(21);
                            UserMessage.ShowMessage(21, null, null, 2);
                        }
                        blnRetValue = true;
                        txtPolicyName.Tag = BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.WorkConsequencePolicyID;
                        PintWorkConsequencePolicyID = BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.WorkConsequencePolicyID;
                        if (!MblnIsEditMode)
                        {
                            MintCurrentRecCnt = MintCurrentRecCnt + 1;
                            BindingNavigatorMoveLastItem_Click(null, null);
                        }

                    }

                }
            }

            return blnRetValue;
        }

        private bool DeleteWorkConsequencePolicy()  // Delete WorkConsequence Policy Reference
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                BLLWorkConsequencePolicy.DTOWorkConsequencePolicy.WorkConsequencePolicyID = txtPolicyName.Tag.ToInt32();
                if (BLLWorkConsequencePolicy.DeleteWorkConsequencePolicy())
                {
                    AddNewWorkConsequencePolicy();
                    blnRetValue = true;
                }

            }
            return blnRetValue;

        }
        private void ResetForm(object sender, System.EventArgs e) // To reset Froms
        {
            if (sender is CheckBox)
            {
                CheckBox chk = (CheckBox)sender;
                if (chk.Name == chkRateOnly.Name)
                {
                    txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    txtRate.Enabled = chkRateOnly.Checked ? true : false;

                    cboCalculationBased.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    if (chkRateOnly.Checked)
                    {
                        cboCalculationBased.SelectedIndex = -1;
                        txtCalculationPercent.Text = "";
                    }
                    else
                    {
                        cboCalculationBased.SelectedIndex = 0;
                        txtRate.Text = "";
                    }
                    txtCalculationPercent.Enabled = chkRateOnly.Checked ? false : true;
                    txtCalculationPercent.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    cboCalculationBased.Enabled = chkRateOnly.Checked ? false : true;
                    rdbCompanyBased.Enabled = (chkRateOnly.Checked) ? false : true;
                    rdbActualMonth.Enabled = (chkRateOnly.Checked) ? false : true;

                }
            }
            Changestatus(null, null);
        }
        #endregion Methods

        #region Events

        private void FrmWorkConsequencePolicy_Load(object sender, EventArgs e) // Load 
        {
            SetPermissions();
            LoadCombos(0);
            if (PintWorkConsequencePolicyID > 0)  // Calling WorkConsequence Policy From Another Froms 
            {
                GetRecordCount();
                RefernceDisplay();
            }
            else
            {
                AddNewWorkConsequencePolicy();
            }
        }

        private void FrmWorkConsequencePolicy_FormClosing(object sender, FormClosingEventArgs e) // Form Close Event
        {
            if (btnSave.Enabled)
            {
                if (UserMessage.ShowMessage(8))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e) // Button Action Move First
        {
            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayWorkConsequencePolicyInfo();
                lblstatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e) // Button Action Move Previous
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayWorkConsequencePolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e) // Button Action Move Next
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayWorkConsequencePolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e) // Button Action Move Last
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayWorkConsequencePolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e) // Add Button Click Event
        {
            AddNewWorkConsequencePolicy();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e) // Delete Button Click Event
        {
            if (DeleteWorkConsequencePolicy())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(4);
                tmrClear.Enabled = true;
                UserMessage.ShowMessage(4);
                AddNewWorkConsequencePolicy();
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e) // Save Button Click Event
        {
            if (SaveWorkConsequencePolicy())
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
            }
        }

        private void btnClear_Click(object sender, EventArgs e) // Clear Button Click Event
        {
            AddNewWorkConsequencePolicy();
        }


        private void btnSave_Click(object sender, EventArgs e) // Save Button Click Event
        {
            if (SaveWorkConsequencePolicy())
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
            }
        }

        private void btnOk_Click(object sender, EventArgs e) // Ok Button Click Event
        {
            if (SaveWorkConsequencePolicy())
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)// Cancel Button Click Event
        {
            this.Close();
        }



        private void rdbCompanyBased_CheckedChanged(object sender, EventArgs e) // CompanyBased CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void chkRatePerDay_CheckedChanged(object sender, EventArgs e) //RatePerDay_CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void chkRateOnly_CheckedChanged(object sender, EventArgs e) //RateOnly_CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void rdbShortBasedOnCompany_CheckedChanged(object sender, EventArgs e)//ShortBasedOnCompany_CheckedChanged
        {
            Changestatus(null, null);
        }



        private void tmrClear_Tick(object sender, EventArgs e) 
        {
            tmrClear.Enabled = false;
            lblstatus.Text = "";
        }

        private void txtDecimal_KeyPress(object sender, KeyPressEventArgs e) // Key Press For TextDecimal
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = strInvalidChars + ".";
            }

            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))) && ClsCommonSettings.IsAmountRoundByZero == false)
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ClsCommonSettings.IsAmountRoundByZero == true)
            {
                e.Handled = true;
            }
        }

        private void txtint_KeyPress(object sender, KeyPressEventArgs e) // Key Press For txtint_KeyPress
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            e.Handled = false;
            if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }



        private void chkMergeBoth_CheckedChanged(object sender, EventArgs e)//chkMergeBoth_CheckedChanged
        {
            Changestatus(null, null);
        }



        private void txtPolicyName_TextChanged(object sender, EventArgs e)// txtPolicyName_TextChanged
        {
            ResetForm(sender, e);
            Changestatus(null, null);
        }

        private void dgvWorkConsequenceDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch
            {
            }
        }

        private void dgvWorkConsequenceDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvWorkConsequenceDetails.IsCurrentCellDirty)
                {

                    dgvWorkConsequenceDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }


        private void dgvShortageDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e) //dgvShortageDetails_CellValueChanged
        {
            Changestatus(null, null);
        }

        private void dgvWorkConsequenceDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e) //dgvWorkConsequenceDetails_CellValueChanged
        {
            Changestatus(null, null);
        }

        private void dgvShortageDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void cboCalculationBased_SelectedIndexChanged(object sender, EventArgs e) //cboCalculationBased_SelectedIndexChanged
        {
            //WorkConsequence Tab
            Changestatus(null, null);
            if (cboCalculationBased.SelectedValue.ToInt32() > 0)
            {
                dgvWorkConsequenceDetails.Enabled = true;

                if (cboCalculationBased.SelectedValue.ToInt32() == (int)CalculationType.GrossSalary)
                {
                    DataTable dtADDDedd = BLLWorkConsequencePolicy.GetAdditionDeductions();
                    FillAdditionDeductionsGrid(dgvWorkConsequenceDetails, dtADDDedd);
                }
                else
                {
                    dgvWorkConsequenceDetails.Rows.Clear();
                    dgvWorkConsequenceDetails.Enabled = false;
                }
            }
            else
            {
                dgvWorkConsequenceDetails.Rows.Clear();
                dgvWorkConsequenceDetails.Enabled = false;
            }
        }

      
        private void cboCalculationBased_KeyDown(object sender, KeyEventArgs e) //cboCalculationBased_KeyDown for Like Search
        {
            cboCalculationBased.DroppedDown = false;
            if (e.KeyData == Keys.Enter)
                cboCalculationBased_SelectedIndexChanged(sender, new EventArgs());

        }



        private void txtShortRatePerHour_TextChanged(object sender, EventArgs e) //txtShortRatePerHour_TextChanged 
        {
            Changestatus(null, null);
        }

        private void dgvWorkConsequenceDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e) //dgvWorkConsequenceDetails_CellBeginEdit
        {
            Changestatus(null, null);
        }

        private void txtShortHoursPerDay_TextChanged(object sender, EventArgs e) //txtShortHoursPerDay_TextChanged
        {
            Changestatus(null, null);
        }

        #endregion Events

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "SalaryStructure";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void txtCalculationPercent_TextChanged(object sender, EventArgs e)
        {
            Changestatus(null, null);
        }

       










    }
}
