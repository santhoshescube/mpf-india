﻿namespace MyPayfriend
{
    partial class FrmWorkConsequencePolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label Label4;
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label DescriptionLabel;
            System.Windows.Forms.Label Label5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWorkConsequencePolicy));
            this.WorkConsequencePolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.tbPolicy = new System.Windows.Forms.TabControl();
            this.tabpgWorkConsequence = new System.Windows.Forms.TabPage();
            this.txtCalculationPercent = new System.Windows.Forms.TextBox();
            this.dgvWorkConsequenceDetails = new DemoClsDataGridview.ClsDataGirdView();
            this.colWorkConsequenceAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colWorkConsequenceSelectedItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colWorkConsequenceParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboCalculationBased = new System.Windows.Forms.ComboBox();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.chkRateOnly = new System.Windows.Forms.CheckBox();
            this.rdbActualMonth = new System.Windows.Forms.RadioButton();
            this.rdbCompanyBased = new System.Windows.Forms.RadioButton();
            this.lblRate = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.SelectValue = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SelectValueS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtPolicyName = new System.Windows.Forms.TextBox();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lblHeader = new System.Windows.Forms.Label();
            this.errWorkConsequencePolicy = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.AddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Particulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDedIDS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParticularsS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Label2 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            DescriptionLabel = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.WorkConsequencePolicyBindingNavigator)).BeginInit();
            this.WorkConsequencePolicyBindingNavigator.SuspendLayout();
            this.tbPolicy.SuspendLayout();
            this.tabpgWorkConsequence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkConsequenceDetails)).BeginInit();
            this.ssStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errWorkConsequencePolicy)).BeginInit();
            this.SuspendLayout();
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new System.Drawing.Point(14, 100);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(101, 13);
            Label2.TabIndex = 38;
            Label2.Text = "Exclude from Gross ";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Location = new System.Drawing.Point(11, 11);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(81, 13);
            Label4.TabIndex = 17;
            Label4.Text = "Calculation Day";
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(14, 65);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(109, 13);
            Label1.TabIndex = 36;
            Label1.Text = "Calculation Based On";
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(2, 49);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(66, 13);
            DescriptionLabel.TabIndex = 306;
            DescriptionLabel.Text = "Policy Name";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(10, 217);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(117, 13);
            Label5.TabIndex = 317;
            Label5.Text = "Calculation Percentage";
            // 
            // WorkConsequencePolicyBindingNavigator
            // 
            this.WorkConsequencePolicyBindingNavigator.AddNewItem = null;
            this.WorkConsequencePolicyBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.WorkConsequencePolicyBindingNavigator.DeleteItem = null;
            this.WorkConsequencePolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorSaveItem,
            this.btnClear,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnHelp});
            this.WorkConsequencePolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.WorkConsequencePolicyBindingNavigator.MoveFirstItem = null;
            this.WorkConsequencePolicyBindingNavigator.MoveLastItem = null;
            this.WorkConsequencePolicyBindingNavigator.MoveNextItem = null;
            this.WorkConsequencePolicyBindingNavigator.MovePreviousItem = null;
            this.WorkConsequencePolicyBindingNavigator.Name = "WorkConsequencePolicyBindingNavigator";
            this.WorkConsequencePolicyBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.WorkConsequencePolicyBindingNavigator.Size = new System.Drawing.Size(404, 25);
            this.WorkConsequencePolicyBindingNavigator.TabIndex = 300;
            this.WorkConsequencePolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.Visible = false;
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Visible = false;
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // tbPolicy
            // 
            this.tbPolicy.Controls.Add(this.tabpgWorkConsequence);
            this.tbPolicy.Location = new System.Drawing.Point(5, 72);
            this.tbPolicy.Name = "tbPolicy";
            this.tbPolicy.SelectedIndex = 0;
            this.tbPolicy.Size = new System.Drawing.Size(390, 328);
            this.tbPolicy.TabIndex = 3;
            // 
            // tabpgWorkConsequence
            // 
            this.tabpgWorkConsequence.Controls.Add(this.txtCalculationPercent);
            this.tabpgWorkConsequence.Controls.Add(Label5);
            this.tabpgWorkConsequence.Controls.Add(this.dgvWorkConsequenceDetails);
            this.tabpgWorkConsequence.Controls.Add(Label2);
            this.tabpgWorkConsequence.Controls.Add(Label4);
            this.tabpgWorkConsequence.Controls.Add(this.cboCalculationBased);
            this.tabpgWorkConsequence.Controls.Add(this.txtRate);
            this.tabpgWorkConsequence.Controls.Add(Label1);
            this.tabpgWorkConsequence.Controls.Add(this.chkRateOnly);
            this.tabpgWorkConsequence.Controls.Add(this.rdbActualMonth);
            this.tabpgWorkConsequence.Controls.Add(this.rdbCompanyBased);
            this.tabpgWorkConsequence.Controls.Add(this.lblRate);
            this.tabpgWorkConsequence.Controls.Add(this.shapeContainer2);
            this.tabpgWorkConsequence.Location = new System.Drawing.Point(4, 22);
            this.tabpgWorkConsequence.Name = "tabpgWorkConsequence";
            this.tabpgWorkConsequence.Padding = new System.Windows.Forms.Padding(3);
            this.tabpgWorkConsequence.Size = new System.Drawing.Size(382, 302);
            this.tabpgWorkConsequence.TabIndex = 0;
            this.tabpgWorkConsequence.Text = "Work";
            this.tabpgWorkConsequence.UseVisualStyleBackColor = true;
            // 
            // txtCalculationPercent
            // 
            this.txtCalculationPercent.BackColor = System.Drawing.SystemColors.Info;
            this.txtCalculationPercent.Location = new System.Drawing.Point(138, 214);
            this.txtCalculationPercent.MaxLength = 5;
            this.txtCalculationPercent.Name = "txtCalculationPercent";
            this.txtCalculationPercent.Size = new System.Drawing.Size(231, 20);
            this.txtCalculationPercent.TabIndex = 316;
            this.txtCalculationPercent.TextChanged += new System.EventHandler(this.txtCalculationPercent_TextChanged);
            this.txtCalculationPercent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // dgvWorkConsequenceDetails
            // 
            this.dgvWorkConsequenceDetails.AddNewRow = false;
            this.dgvWorkConsequenceDetails.AllowUserToAddRows = false;
            this.dgvWorkConsequenceDetails.AllowUserToDeleteRows = false;
            this.dgvWorkConsequenceDetails.AllowUserToResizeColumns = false;
            this.dgvWorkConsequenceDetails.AllowUserToResizeRows = false;
            this.dgvWorkConsequenceDetails.AlphaNumericCols = new int[0];
            this.dgvWorkConsequenceDetails.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvWorkConsequenceDetails.CapsLockCols = new int[0];
            this.dgvWorkConsequenceDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWorkConsequenceDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colWorkConsequenceAddDedID,
            this.colWorkConsequenceSelectedItem,
            this.colWorkConsequenceParticulars});
            this.dgvWorkConsequenceDetails.DecimalCols = new int[0];
            this.dgvWorkConsequenceDetails.HasSlNo = false;
            this.dgvWorkConsequenceDetails.LastRowIndex = 0;
            this.dgvWorkConsequenceDetails.Location = new System.Drawing.Point(138, 89);
            this.dgvWorkConsequenceDetails.MultiSelect = false;
            this.dgvWorkConsequenceDetails.Name = "dgvWorkConsequenceDetails";
            this.dgvWorkConsequenceDetails.NegativeValueCols = new int[0];
            this.dgvWorkConsequenceDetails.NumericCols = new int[0];
            this.dgvWorkConsequenceDetails.RowHeadersWidth = 35;
            this.dgvWorkConsequenceDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvWorkConsequenceDetails.Size = new System.Drawing.Size(231, 108);
            this.dgvWorkConsequenceDetails.TabIndex = 3;
            this.dgvWorkConsequenceDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvWorkConsequenceDetails_CellValueChanged);
            this.dgvWorkConsequenceDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvWorkConsequenceDetails_CellBeginEdit);
            this.dgvWorkConsequenceDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvWorkConsequenceDetails_CurrentCellDirtyStateChanged);
            this.dgvWorkConsequenceDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvWorkConsequenceDetails_DataError);
            // 
            // colWorkConsequenceAddDedID
            // 
            this.colWorkConsequenceAddDedID.HeaderText = "AddDedID";
            this.colWorkConsequenceAddDedID.Name = "colWorkConsequenceAddDedID";
            this.colWorkConsequenceAddDedID.Visible = false;
            // 
            // colWorkConsequenceSelectedItem
            // 
            this.colWorkConsequenceSelectedItem.HeaderText = "";
            this.colWorkConsequenceSelectedItem.Name = "colWorkConsequenceSelectedItem";
            this.colWorkConsequenceSelectedItem.Width = 30;
            // 
            // colWorkConsequenceParticulars
            // 
            this.colWorkConsequenceParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colWorkConsequenceParticulars.HeaderText = "Particulars";
            this.colWorkConsequenceParticulars.Name = "colWorkConsequenceParticulars";
            this.colWorkConsequenceParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cboCalculationBased
            // 
            this.cboCalculationBased.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBased.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBased.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBased.DropDownHeight = 134;
            this.cboCalculationBased.FormattingEnabled = true;
            this.cboCalculationBased.IntegralHeight = false;
            this.cboCalculationBased.Location = new System.Drawing.Point(138, 62);
            this.cboCalculationBased.MaxDropDownItems = 10;
            this.cboCalculationBased.Name = "cboCalculationBased";
            this.cboCalculationBased.Size = new System.Drawing.Size(231, 21);
            this.cboCalculationBased.TabIndex = 2;
            this.cboCalculationBased.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBased_SelectedIndexChanged);
            this.cboCalculationBased.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCalculationBased_KeyDown);
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(138, 270);
            this.txtRate.MaxLength = 9;
            this.txtRate.Name = "txtRate";
            this.txtRate.ShortcutsEnabled = false;
            this.txtRate.Size = new System.Drawing.Size(231, 20);
            this.txtRate.TabIndex = 7;
            this.txtRate.TextChanged += new System.EventHandler(this.Changestatus);
            this.txtRate.Validated += new System.EventHandler(this.ResetForm);
            this.txtRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // chkRateOnly
            // 
            this.chkRateOnly.AutoSize = true;
            this.chkRateOnly.Location = new System.Drawing.Point(138, 247);
            this.chkRateOnly.Name = "chkRateOnly";
            this.chkRateOnly.Size = new System.Drawing.Size(71, 17);
            this.chkRateOnly.TabIndex = 6;
            this.chkRateOnly.Text = "Rate only";
            this.chkRateOnly.UseVisualStyleBackColor = true;
            this.chkRateOnly.CheckedChanged += new System.EventHandler(this.chkRateOnly_CheckedChanged);
            // 
            // rdbActualMonth
            // 
            this.rdbActualMonth.AutoSize = true;
            this.rdbActualMonth.Location = new System.Drawing.Point(217, 35);
            this.rdbActualMonth.Name = "rdbActualMonth";
            this.rdbActualMonth.Size = new System.Drawing.Size(88, 17);
            this.rdbActualMonth.TabIndex = 1;
            this.rdbActualMonth.Text = "Actual Month";
            this.rdbActualMonth.UseVisualStyleBackColor = true;
            this.rdbActualMonth.CheckedChanged += new System.EventHandler(this.ResetForm);
            // 
            // rdbCompanyBased
            // 
            this.rdbCompanyBased.AutoSize = true;
            this.rdbCompanyBased.Checked = true;
            this.rdbCompanyBased.Location = new System.Drawing.Point(78, 35);
            this.rdbCompanyBased.Name = "rdbCompanyBased";
            this.rdbCompanyBased.Size = new System.Drawing.Size(117, 17);
            this.rdbCompanyBased.TabIndex = 0;
            this.rdbCompanyBased.TabStop = true;
            this.rdbCompanyBased.Text = "Based on Company";
            this.rdbCompanyBased.UseVisualStyleBackColor = true;
            this.rdbCompanyBased.CheckedChanged += new System.EventHandler(this.rdbCompanyBased_CheckedChanged);
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Location = new System.Drawing.Point(6, 273);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(129, 13);
            this.lblRate.TabIndex = 14;
            this.lblRate.Text = "WorkConsequence Rate";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3});
            this.shapeContainer2.Size = new System.Drawing.Size(376, 296);
            this.shapeContainer2.TabIndex = 41;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 9;
            this.lineShape3.X2 = 367;
            this.lineShape3.Y1 = 24;
            this.lineShape3.Y2 = 24;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 9;
            this.lineShape2.X2 = 367;
            this.lineShape2.Y1 = 24;
            this.lineShape2.Y2 = 24;
            // 
            // SelectValue
            // 
            this.SelectValue.HeaderText = "";
            this.SelectValue.Name = "SelectValue";
            this.SelectValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SelectValue.Width = 30;
            // 
            // SelectValueS
            // 
            this.SelectValueS.HeaderText = "";
            this.SelectValueS.Name = "SelectValueS";
            this.SelectValueS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectValueS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SelectValueS.Width = 30;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(320, 406);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(239, 406);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(5, 406);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPolicyName
            // 
            this.txtPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyName.Location = new System.Drawing.Point(129, 46);
            this.txtPolicyName.MaxLength = 50;
            this.txtPolicyName.Name = "txtPolicyName";
            this.txtPolicyName.Size = new System.Drawing.Size(229, 20);
            this.txtPolicyName.TabIndex = 0;
            this.txtPolicyName.TextChanged += new System.EventHandler(this.txtPolicyName_TextChanged);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 435);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(404, 22);
            this.ssStatus.TabIndex = 310;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // LineShape1
            // 
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 31;
            this.LineShape1.X2 = 388;
            this.LineShape1.Y1 = 37;
            this.LineShape1.Y2 = 37;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(404, 457);
            this.shapeContainer1.TabIndex = 311;
            this.shapeContainer1.TabStop = false;
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(2, 28);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(66, 13);
            this.lblHeader.TabIndex = 312;
            this.lblHeader.Text = "Policy Info";
            // 
            // errWorkConsequencePolicy
            // 
            this.errWorkConsequencePolicy.ContainerControl = this;
            this.errWorkConsequencePolicy.RightToLeft = true;
            // 
            // tmrClear
            // 
            this.tmrClear.Tick += new System.EventHandler(this.tmrClear_Tick);
            // 
            // AddDedID
            // 
            this.AddDedID.HeaderText = "AddDedID";
            this.AddDedID.Name = "AddDedID";
            this.AddDedID.ReadOnly = true;
            this.AddDedID.Visible = false;
            // 
            // Particulars
            // 
            this.Particulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Particulars.HeaderText = "Particulars";
            this.Particulars.Name = "Particulars";
            this.Particulars.ReadOnly = true;
            // 
            // AddDedIDS
            // 
            this.AddDedIDS.HeaderText = "AddDedID";
            this.AddDedIDS.Name = "AddDedIDS";
            this.AddDedIDS.ReadOnly = true;
            this.AddDedIDS.Visible = false;
            // 
            // ParticularsS
            // 
            this.ParticularsS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ParticularsS.HeaderText = "Particulars";
            this.ParticularsS.Name = "ParticularsS";
            this.ParticularsS.ReadOnly = true;
            // 
            // FrmWorkConsequencePolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 457);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.tbPolicy);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtPolicyName);
            this.Controls.Add(DescriptionLabel);
            this.Controls.Add(this.WorkConsequencePolicyBindingNavigator);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmWorkConsequencePolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Work Consequence";
            this.Load += new System.EventHandler(this.FrmWorkConsequencePolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmWorkConsequencePolicy_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.WorkConsequencePolicyBindingNavigator)).EndInit();
            this.WorkConsequencePolicyBindingNavigator.ResumeLayout(false);
            this.WorkConsequencePolicyBindingNavigator.PerformLayout();
            this.tbPolicy.ResumeLayout(false);
            this.tabpgWorkConsequence.ResumeLayout(false);
            this.tabpgWorkConsequence.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkConsequenceDetails)).EndInit();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errWorkConsequencePolicy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator WorkConsequencePolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.TabControl tbPolicy;
        internal System.Windows.Forms.TabPage tabpgWorkConsequence;
        internal System.Windows.Forms.ComboBox cboCalculationBased;
        internal System.Windows.Forms.TextBox txtRate;
        internal System.Windows.Forms.CheckBox chkRateOnly;
        internal System.Windows.Forms.RadioButton rdbActualMonth;
        internal System.Windows.Forms.RadioButton rdbCompanyBased;
        internal System.Windows.Forms.Label lblRate;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AddDedID;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn SelectValue;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Particulars;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AddDedIDS;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn SelectValueS;
        internal System.Windows.Forms.DataGridViewTextBoxColumn ParticularsS;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.TextBox txtPolicyName;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        internal System.Windows.Forms.Label lblHeader;
        private DemoClsDataGridview.ClsDataGirdView dgvWorkConsequenceDetails;
        private System.Windows.Forms.ErrorProvider errWorkConsequencePolicy;
        private System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWorkConsequenceAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colWorkConsequenceSelectedItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWorkConsequenceParticulars;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal System.Windows.Forms.TextBox txtCalculationPercent;
    }
}