﻿namespace MyPayfriend
{
    partial class FrmSettlementPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettlementPolicy));
            this.GrpCSP = new System.Windows.Forms.GroupBox();
            this.tbSettlement = new System.Windows.Forms.TabControl();
            this.TabGratuity = new System.Windows.Forms.TabPage();
            this.lblCaldays = new System.Windows.Forms.Label();
            this.rdbActualMonthN = new System.Windows.Forms.RadioButton();
            this.rdbCompanyBasedN = new System.Windows.Forms.RadioButton();
            this.TabGrauity = new System.Windows.Forms.TabControl();
            this.TabLessThanFive = new System.Windows.Forms.TabPage();
            this.grdGratuity = new System.Windows.Forms.DataGridView();
            this.ParameterID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TabGreaterThanFive = new System.Windows.Forms.TabPage();
            this.grdGratuityFive = new System.Windows.Forms.DataGridView();
            this.ParameterIDFive = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.chkIncludeLeave = new System.Windows.Forms.CheckBox();
            this.lblCalcGratuity = new System.Windows.Forms.Label();
            this.lblGrossGratuity = new System.Windows.Forms.Label();
            this.cboCalculationBasedGratuity = new System.Windows.Forms.ComboBox();
            this.TabTermination = new System.Windows.Forms.TabPage();
            this.lblCalDayTer = new System.Windows.Forms.Label();
            this.rdbActualMonthT = new System.Windows.Forms.RadioButton();
            this.rdbCompanyBasedT = new System.Windows.Forms.RadioButton();
            this.TabTermin = new System.Windows.Forms.TabControl();
            this.TabLesFiveTer = new System.Windows.Forms.TabPage();
            this.grdGratuityTer = new System.Windows.Forms.DataGridView();
            this.ParameterIDTer = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.grdGratuityFiveTer = new System.Windows.Forms.DataGridView();
            this.ParameterIDFiveTer = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.chkIncludeLeaveTer = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboCalculationBasedGratuityTer = new System.Windows.Forms.ComboBox();
            this.TabProvision = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.rdbActualMonthPro = new System.Windows.Forms.RadioButton();
            this.rdbCompanyBasedPro = new System.Windows.Forms.RadioButton();
            this.TabPro = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.grdGratuityPro = new System.Windows.Forms.DataGridView();
            this.ParameterIDPro = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.grdGratuityFivePro = new System.Windows.Forms.DataGridView();
            this.ParameterIDFivePro = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.chkIncludeLeavePro = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboCalculationBasedGratuityPro = new System.Windows.Forms.ComboBox();
            this.txtPolicyName = new System.Windows.Forms.TextBox();
            this.CompanyIDLabel = new System.Windows.Forms.Label();
            this.txtIntText = new System.Windows.Forms.TextBox();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lblAmount = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnBottomCancel = new System.Windows.Forms.Button();
            this.OKSaveButton = new System.Windows.Forms.Button();
            this.PolicyIDTextBox = new System.Windows.Forms.TextBox();
            this.TextboxNumeric = new System.Windows.Forms.TextBox();
            this.CompanySettlementPolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnCancel = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.StatusStripSettlement = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ErrSettlement = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmrSettlement = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfMonths = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfMonthsFive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfDaysFive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityIDFive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailsDataGridViewGratuity = new DemoClsDataGridview.ClsDataGirdView();
            this.txtAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkParticular = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfMonthsTer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfDaysTer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityIDTer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfMonthsFiveTer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfDaysFiveTer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityIDFiveTer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailsDataGridViewGratuityTer = new DemoClsDataGridview.ClsDataGirdView();
            this.txtAddDedIDTer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkParticularTer = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtParticularsTer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfMonthsPro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfDaysPro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityIDPro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfMonthsFivePro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfDaysFivePro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityIDFivePro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailsDataGridViewGratuityPro = new DemoClsDataGridview.ClsDataGirdView();
            this.txtAddDedIDPro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkParticularPro = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtParticularsPro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Label2 = new System.Windows.Forms.Label();
            this.GrpCSP.SuspendLayout();
            this.tbSettlement.SuspendLayout();
            this.TabGratuity.SuspendLayout();
            this.TabGrauity.SuspendLayout();
            this.TabLessThanFive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuity)).BeginInit();
            this.TabGreaterThanFive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityFive)).BeginInit();
            this.TabTermination.SuspendLayout();
            this.TabTermin.SuspendLayout();
            this.TabLesFiveTer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityTer)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityFiveTer)).BeginInit();
            this.TabProvision.SuspendLayout();
            this.TabPro.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityPro)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityFivePro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanySettlementPolicyBindingNavigator)).BeginInit();
            this.CompanySettlementPolicyBindingNavigator.SuspendLayout();
            this.StatusStripSettlement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSettlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewGratuity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewGratuityTer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewGratuityPro)).BeginInit();
            this.SuspendLayout();
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label2.Location = new System.Drawing.Point(4, -2);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(51, 13);
            Label2.TabIndex = 0;
            Label2.Text = "General";
            // 
            // GrpCSP
            // 
            this.GrpCSP.Controls.Add(this.tbSettlement);
            this.GrpCSP.Controls.Add(this.txtPolicyName);
            this.GrpCSP.Controls.Add(this.CompanyIDLabel);
            this.GrpCSP.Controls.Add(Label2);
            this.GrpCSP.Location = new System.Drawing.Point(0, 28);
            this.GrpCSP.Name = "GrpCSP";
            this.GrpCSP.Size = new System.Drawing.Size(435, 405);
            this.GrpCSP.TabIndex = 1;
            this.GrpCSP.TabStop = false;
            // 
            // tbSettlement
            // 
            this.tbSettlement.Controls.Add(this.TabGratuity);
            this.tbSettlement.Controls.Add(this.TabTermination);
            this.tbSettlement.Controls.Add(this.TabProvision);
            this.tbSettlement.Location = new System.Drawing.Point(12, 41);
            this.tbSettlement.Name = "tbSettlement";
            this.tbSettlement.SelectedIndex = 0;
            this.tbSettlement.Size = new System.Drawing.Size(416, 358);
            this.tbSettlement.TabIndex = 11;
            // 
            // TabGratuity
            // 
            this.TabGratuity.Controls.Add(this.lblCaldays);
            this.TabGratuity.Controls.Add(this.rdbActualMonthN);
            this.TabGratuity.Controls.Add(this.rdbCompanyBasedN);
            this.TabGratuity.Controls.Add(this.TabGrauity);
            this.TabGratuity.Controls.Add(this.chkIncludeLeave);
            this.TabGratuity.Controls.Add(this.DetailsDataGridViewGratuity);
            this.TabGratuity.Controls.Add(this.lblCalcGratuity);
            this.TabGratuity.Controls.Add(this.lblGrossGratuity);
            this.TabGratuity.Controls.Add(this.cboCalculationBasedGratuity);
            this.TabGratuity.Location = new System.Drawing.Point(4, 22);
            this.TabGratuity.Name = "TabGratuity";
            this.TabGratuity.Padding = new System.Windows.Forms.Padding(3);
            this.TabGratuity.Size = new System.Drawing.Size(408, 332);
            this.TabGratuity.TabIndex = 0;
            this.TabGratuity.Text = "Normal";
            this.TabGratuity.UseVisualStyleBackColor = true;
            // 
            // lblCaldays
            // 
            this.lblCaldays.AutoSize = true;
            this.lblCaldays.Location = new System.Drawing.Point(6, 133);
            this.lblCaldays.Name = "lblCaldays";
            this.lblCaldays.Size = new System.Drawing.Size(81, 13);
            this.lblCaldays.TabIndex = 69;
            this.lblCaldays.Text = "Calculation Day";
            // 
            // rdbActualMonthN
            // 
            this.rdbActualMonthN.AutoSize = true;
            this.rdbActualMonthN.Location = new System.Drawing.Point(282, 129);
            this.rdbActualMonthN.Name = "rdbActualMonthN";
            this.rdbActualMonthN.Size = new System.Drawing.Size(88, 17);
            this.rdbActualMonthN.TabIndex = 68;
            this.rdbActualMonthN.TabStop = true;
            this.rdbActualMonthN.Text = "Actual Month";
            this.rdbActualMonthN.UseVisualStyleBackColor = true;
            // 
            // rdbCompanyBasedN
            // 
            this.rdbCompanyBasedN.AutoSize = true;
            this.rdbCompanyBasedN.Location = new System.Drawing.Point(143, 129);
            this.rdbCompanyBasedN.Name = "rdbCompanyBasedN";
            this.rdbCompanyBasedN.Size = new System.Drawing.Size(117, 17);
            this.rdbCompanyBasedN.TabIndex = 67;
            this.rdbCompanyBasedN.TabStop = true;
            this.rdbCompanyBasedN.Text = "Based on Company";
            this.rdbCompanyBasedN.UseVisualStyleBackColor = true;
            // 
            // TabGrauity
            // 
            this.TabGrauity.Controls.Add(this.TabLessThanFive);
            this.TabGrauity.Controls.Add(this.TabGreaterThanFive);
            this.TabGrauity.Location = new System.Drawing.Point(3, 159);
            this.TabGrauity.Name = "TabGrauity";
            this.TabGrauity.SelectedIndex = 0;
            this.TabGrauity.Size = new System.Drawing.Size(408, 167);
            this.TabGrauity.TabIndex = 48;
            // 
            // TabLessThanFive
            // 
            this.TabLessThanFive.Controls.Add(this.grdGratuity);
            this.TabLessThanFive.Location = new System.Drawing.Point(4, 22);
            this.TabLessThanFive.Name = "TabLessThanFive";
            this.TabLessThanFive.Padding = new System.Windows.Forms.Padding(3);
            this.TabLessThanFive.Size = new System.Drawing.Size(400, 141);
            this.TabLessThanFive.TabIndex = 0;
            this.TabLessThanFive.Text = "Less Than Five";
            this.TabLessThanFive.UseVisualStyleBackColor = true;
            // 
            // grdGratuity
            // 
            this.grdGratuity.AllowUserToResizeColumns = false;
            this.grdGratuity.AllowUserToResizeRows = false;
            this.grdGratuity.BackgroundColor = System.Drawing.Color.White;
            this.grdGratuity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGratuity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterID,
            this.NoOfMonths,
            this.NoOfDays,
            this.GratuityID});
            this.grdGratuity.Location = new System.Drawing.Point(-7, 3);
            this.grdGratuity.Name = "grdGratuity";
            this.grdGratuity.RowHeadersWidth = 25;
            this.grdGratuity.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGratuity.Size = new System.Drawing.Size(405, 138);
            this.grdGratuity.TabIndex = 2;
            this.grdGratuity.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuity_CellValueChanged);
            this.grdGratuity.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdGratuity_CellBeginEdit);
            this.grdGratuity.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.grdGratuity_UserDeletedRow);
            this.grdGratuity.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuity_CellEndEdit);
            this.grdGratuity.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdGratuity_EditingControlShowing);
            this.grdGratuity.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdGratuity_DataError);
            // 
            // ParameterID
            // 
            this.ParameterID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParameterID.HeaderText = "Parameter";
            this.ParameterID.Name = "ParameterID";
            this.ParameterID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ParameterID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ParameterID.Width = 120;
            // 
            // TabGreaterThanFive
            // 
            this.TabGreaterThanFive.Controls.Add(this.grdGratuityFive);
            this.TabGreaterThanFive.Location = new System.Drawing.Point(4, 22);
            this.TabGreaterThanFive.Name = "TabGreaterThanFive";
            this.TabGreaterThanFive.Padding = new System.Windows.Forms.Padding(3);
            this.TabGreaterThanFive.Size = new System.Drawing.Size(400, 141);
            this.TabGreaterThanFive.TabIndex = 1;
            this.TabGreaterThanFive.Text = "Greater Than Five";
            this.TabGreaterThanFive.UseVisualStyleBackColor = true;
            // 
            // grdGratuityFive
            // 
            this.grdGratuityFive.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.grdGratuityFive.AllowUserToResizeColumns = false;
            this.grdGratuityFive.AllowUserToResizeRows = false;
            this.grdGratuityFive.BackgroundColor = System.Drawing.Color.White;
            this.grdGratuityFive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGratuityFive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterIDFive,
            this.NoOfMonthsFive,
            this.NoOfDaysFive,
            this.GratuityIDFive});
            this.grdGratuityFive.Location = new System.Drawing.Point(-1, 2);
            this.grdGratuityFive.Name = "grdGratuityFive";
            this.grdGratuityFive.RowHeadersWidth = 25;
            this.grdGratuityFive.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGratuityFive.Size = new System.Drawing.Size(399, 139);
            this.grdGratuityFive.TabIndex = 3;
            this.grdGratuityFive.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdGratuityFive_CellBeginEdit);
            this.grdGratuityFive.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuityFive_CellEndEdit);
            this.grdGratuityFive.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdGratuityFive_EditingControlShowing);
            this.grdGratuityFive.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdGratuityFive_DataError);
            // 
            // ParameterIDFive
            // 
            this.ParameterIDFive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParameterIDFive.HeaderText = "Parameter";
            this.ParameterIDFive.Name = "ParameterIDFive";
            this.ParameterIDFive.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ParameterIDFive.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ParameterIDFive.Width = 120;
            // 
            // chkIncludeLeave
            // 
            this.chkIncludeLeave.AutoSize = true;
            this.chkIncludeLeave.Location = new System.Drawing.Point(9, 104);
            this.chkIncludeLeave.Name = "chkIncludeLeave";
            this.chkIncludeLeave.Size = new System.Drawing.Size(130, 17);
            this.chkIncludeLeave.TabIndex = 47;
            this.chkIncludeLeave.Text = "Include Eligible Leave";
            this.chkIncludeLeave.UseVisualStyleBackColor = true;
            // 
            // lblCalcGratuity
            // 
            this.lblCalcGratuity.AutoSize = true;
            this.lblCalcGratuity.Location = new System.Drawing.Point(6, 11);
            this.lblCalcGratuity.Name = "lblCalcGratuity";
            this.lblCalcGratuity.Size = new System.Drawing.Size(107, 13);
            this.lblCalcGratuity.TabIndex = 45;
            this.lblCalcGratuity.Text = "Calculation Based on";
            // 
            // lblGrossGratuity
            // 
            this.lblGrossGratuity.AutoSize = true;
            this.lblGrossGratuity.Location = new System.Drawing.Point(6, 37);
            this.lblGrossGratuity.Name = "lblGrossGratuity";
            this.lblGrossGratuity.Size = new System.Drawing.Size(101, 13);
            this.lblGrossGratuity.TabIndex = 46;
            this.lblGrossGratuity.Text = "Exclude from Gross ";
            // 
            // cboCalculationBasedGratuity
            // 
            this.cboCalculationBasedGratuity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBasedGratuity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBasedGratuity.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBasedGratuity.DropDownHeight = 134;
            this.cboCalculationBasedGratuity.FormattingEnabled = true;
            this.cboCalculationBasedGratuity.IntegralHeight = false;
            this.cboCalculationBasedGratuity.Location = new System.Drawing.Point(143, 8);
            this.cboCalculationBasedGratuity.MaxDropDownItems = 10;
            this.cboCalculationBasedGratuity.Name = "cboCalculationBasedGratuity";
            this.cboCalculationBasedGratuity.Size = new System.Drawing.Size(253, 21);
            this.cboCalculationBasedGratuity.TabIndex = 0;
            this.cboCalculationBasedGratuity.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBasedGratuity_SelectedIndexChanged);
            this.cboCalculationBasedGratuity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCalculationBasedGratuity_KeyPress);
            // 
            // TabTermination
            // 
            this.TabTermination.Controls.Add(this.lblCalDayTer);
            this.TabTermination.Controls.Add(this.rdbActualMonthT);
            this.TabTermination.Controls.Add(this.rdbCompanyBasedT);
            this.TabTermination.Controls.Add(this.TabTermin);
            this.TabTermination.Controls.Add(this.chkIncludeLeaveTer);
            this.TabTermination.Controls.Add(this.DetailsDataGridViewGratuityTer);
            this.TabTermination.Controls.Add(this.label1);
            this.TabTermination.Controls.Add(this.label3);
            this.TabTermination.Controls.Add(this.cboCalculationBasedGratuityTer);
            this.TabTermination.Location = new System.Drawing.Point(4, 22);
            this.TabTermination.Name = "TabTermination";
            this.TabTermination.Size = new System.Drawing.Size(408, 332);
            this.TabTermination.TabIndex = 1;
            this.TabTermination.Text = "Termination";
            this.TabTermination.UseVisualStyleBackColor = true;
            // 
            // lblCalDayTer
            // 
            this.lblCalDayTer.AutoSize = true;
            this.lblCalDayTer.Location = new System.Drawing.Point(12, 129);
            this.lblCalDayTer.Name = "lblCalDayTer";
            this.lblCalDayTer.Size = new System.Drawing.Size(81, 13);
            this.lblCalDayTer.TabIndex = 72;
            this.lblCalDayTer.Text = "Calculation Day";
            // 
            // rdbActualMonthT
            // 
            this.rdbActualMonthT.AutoSize = true;
            this.rdbActualMonthT.Location = new System.Drawing.Point(279, 127);
            this.rdbActualMonthT.Name = "rdbActualMonthT";
            this.rdbActualMonthT.Size = new System.Drawing.Size(88, 17);
            this.rdbActualMonthT.TabIndex = 71;
            this.rdbActualMonthT.TabStop = true;
            this.rdbActualMonthT.Text = "Actual Month";
            this.rdbActualMonthT.UseVisualStyleBackColor = true;
            // 
            // rdbCompanyBasedT
            // 
            this.rdbCompanyBasedT.AutoSize = true;
            this.rdbCompanyBasedT.Location = new System.Drawing.Point(140, 127);
            this.rdbCompanyBasedT.Name = "rdbCompanyBasedT";
            this.rdbCompanyBasedT.Size = new System.Drawing.Size(117, 17);
            this.rdbCompanyBasedT.TabIndex = 70;
            this.rdbCompanyBasedT.TabStop = true;
            this.rdbCompanyBasedT.Text = "Based on Company";
            this.rdbCompanyBasedT.UseVisualStyleBackColor = true;
            // 
            // TabTermin
            // 
            this.TabTermin.Controls.Add(this.TabLesFiveTer);
            this.TabTermin.Controls.Add(this.tabPage2);
            this.TabTermin.Location = new System.Drawing.Point(0, 157);
            this.TabTermin.Name = "TabTermin";
            this.TabTermin.SelectedIndex = 0;
            this.TabTermin.Size = new System.Drawing.Size(408, 167);
            this.TabTermin.TabIndex = 54;
            // 
            // TabLesFiveTer
            // 
            this.TabLesFiveTer.Controls.Add(this.grdGratuityTer);
            this.TabLesFiveTer.Location = new System.Drawing.Point(4, 22);
            this.TabLesFiveTer.Name = "TabLesFiveTer";
            this.TabLesFiveTer.Padding = new System.Windows.Forms.Padding(3);
            this.TabLesFiveTer.Size = new System.Drawing.Size(400, 141);
            this.TabLesFiveTer.TabIndex = 0;
            this.TabLesFiveTer.Text = "Less Than Five";
            this.TabLesFiveTer.UseVisualStyleBackColor = true;
            // 
            // grdGratuityTer
            // 
            this.grdGratuityTer.AllowUserToResizeColumns = false;
            this.grdGratuityTer.AllowUserToResizeRows = false;
            this.grdGratuityTer.BackgroundColor = System.Drawing.Color.White;
            this.grdGratuityTer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGratuityTer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterIDTer,
            this.NoOfMonthsTer,
            this.NoOfDaysTer,
            this.GratuityIDTer});
            this.grdGratuityTer.Location = new System.Drawing.Point(-7, 3);
            this.grdGratuityTer.Name = "grdGratuityTer";
            this.grdGratuityTer.RowHeadersWidth = 25;
            this.grdGratuityTer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGratuityTer.Size = new System.Drawing.Size(407, 135);
            this.grdGratuityTer.TabIndex = 2;
            this.grdGratuityTer.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdGratuityTer_CellBeginEdit);
            this.grdGratuityTer.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuityTer_CellEndEdit);
            this.grdGratuityTer.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdGratuityTer_EditingControlShowing);
            this.grdGratuityTer.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdGratuityTer_DataError);
            // 
            // ParameterIDTer
            // 
            this.ParameterIDTer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParameterIDTer.HeaderText = "Parameter";
            this.ParameterIDTer.Name = "ParameterIDTer";
            this.ParameterIDTer.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ParameterIDTer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ParameterIDTer.Width = 120;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.grdGratuityFiveTer);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(400, 141);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Greater Than Five";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // grdGratuityFiveTer
            // 
            this.grdGratuityFiveTer.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.grdGratuityFiveTer.AllowUserToResizeColumns = false;
            this.grdGratuityFiveTer.AllowUserToResizeRows = false;
            this.grdGratuityFiveTer.BackgroundColor = System.Drawing.Color.White;
            this.grdGratuityFiveTer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGratuityFiveTer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterIDFiveTer,
            this.NoOfMonthsFiveTer,
            this.NoOfDaysFiveTer,
            this.GratuityIDFiveTer});
            this.grdGratuityFiveTer.Location = new System.Drawing.Point(-1, 2);
            this.grdGratuityFiveTer.Name = "grdGratuityFiveTer";
            this.grdGratuityFiveTer.RowHeadersWidth = 25;
            this.grdGratuityFiveTer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGratuityFiveTer.Size = new System.Drawing.Size(401, 139);
            this.grdGratuityFiveTer.TabIndex = 3;
            this.grdGratuityFiveTer.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdGratuityFiveTer_CellBeginEdit);
            this.grdGratuityFiveTer.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuityFiveTer_CellEndEdit);
            this.grdGratuityFiveTer.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdGratuityFiveTer_EditingControlShowing);
            this.grdGratuityFiveTer.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdGratuityFiveTer_DataError);
            // 
            // ParameterIDFiveTer
            // 
            this.ParameterIDFiveTer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParameterIDFiveTer.HeaderText = "Parameter";
            this.ParameterIDFiveTer.Name = "ParameterIDFiveTer";
            this.ParameterIDFiveTer.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ParameterIDFiveTer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ParameterIDFiveTer.Width = 120;
            // 
            // chkIncludeLeaveTer
            // 
            this.chkIncludeLeaveTer.AutoSize = true;
            this.chkIncludeLeaveTer.Location = new System.Drawing.Point(6, 103);
            this.chkIncludeLeaveTer.Name = "chkIncludeLeaveTer";
            this.chkIncludeLeaveTer.Size = new System.Drawing.Size(130, 17);
            this.chkIncludeLeaveTer.TabIndex = 53;
            this.chkIncludeLeaveTer.Text = "Include Eligible Leave";
            this.chkIncludeLeaveTer.UseVisualStyleBackColor = true;
            this.chkIncludeLeaveTer.CheckedChanged += new System.EventHandler(this.chkIncludeLeaveTer_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Calculation Based on";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Exclude from Gross ";
            // 
            // cboCalculationBasedGratuityTer
            // 
            this.cboCalculationBasedGratuityTer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBasedGratuityTer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBasedGratuityTer.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBasedGratuityTer.DropDownHeight = 134;
            this.cboCalculationBasedGratuityTer.FormattingEnabled = true;
            this.cboCalculationBasedGratuityTer.IntegralHeight = false;
            this.cboCalculationBasedGratuityTer.Location = new System.Drawing.Point(140, 6);
            this.cboCalculationBasedGratuityTer.MaxDropDownItems = 10;
            this.cboCalculationBasedGratuityTer.Name = "cboCalculationBasedGratuityTer";
            this.cboCalculationBasedGratuityTer.Size = new System.Drawing.Size(253, 21);
            this.cboCalculationBasedGratuityTer.TabIndex = 49;
            this.cboCalculationBasedGratuityTer.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBasedGratuityTer_SelectedIndexChanged);
            this.cboCalculationBasedGratuityTer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCalculationBasedGratuityTer_KeyPress);
            // 
            // TabProvision
            // 
            this.TabProvision.Controls.Add(this.label4);
            this.TabProvision.Controls.Add(this.rdbActualMonthPro);
            this.TabProvision.Controls.Add(this.rdbCompanyBasedPro);
            this.TabProvision.Controls.Add(this.TabPro);
            this.TabProvision.Controls.Add(this.chkIncludeLeavePro);
            this.TabProvision.Controls.Add(this.DetailsDataGridViewGratuityPro);
            this.TabProvision.Controls.Add(this.label5);
            this.TabProvision.Controls.Add(this.label6);
            this.TabProvision.Controls.Add(this.cboCalculationBasedGratuityPro);
            this.TabProvision.Location = new System.Drawing.Point(4, 22);
            this.TabProvision.Name = "TabProvision";
            this.TabProvision.Size = new System.Drawing.Size(408, 332);
            this.TabProvision.TabIndex = 2;
            this.TabProvision.Text = "Provision";
            this.TabProvision.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 81;
            this.label4.Text = "Calculation Day";
            // 
            // rdbActualMonthPro
            // 
            this.rdbActualMonthPro.AutoSize = true;
            this.rdbActualMonthPro.Location = new System.Drawing.Point(279, 128);
            this.rdbActualMonthPro.Name = "rdbActualMonthPro";
            this.rdbActualMonthPro.Size = new System.Drawing.Size(88, 17);
            this.rdbActualMonthPro.TabIndex = 80;
            this.rdbActualMonthPro.TabStop = true;
            this.rdbActualMonthPro.Text = "Actual Month";
            this.rdbActualMonthPro.UseVisualStyleBackColor = true;
            // 
            // rdbCompanyBasedPro
            // 
            this.rdbCompanyBasedPro.AutoSize = true;
            this.rdbCompanyBasedPro.Location = new System.Drawing.Point(140, 128);
            this.rdbCompanyBasedPro.Name = "rdbCompanyBasedPro";
            this.rdbCompanyBasedPro.Size = new System.Drawing.Size(117, 17);
            this.rdbCompanyBasedPro.TabIndex = 79;
            this.rdbCompanyBasedPro.TabStop = true;
            this.rdbCompanyBasedPro.Text = "Based on Company";
            this.rdbCompanyBasedPro.UseVisualStyleBackColor = true;
            // 
            // TabPro
            // 
            this.TabPro.Controls.Add(this.tabPage1);
            this.TabPro.Controls.Add(this.tabPage3);
            this.TabPro.Location = new System.Drawing.Point(0, 158);
            this.TabPro.Name = "TabPro";
            this.TabPro.SelectedIndex = 0;
            this.TabPro.Size = new System.Drawing.Size(408, 167);
            this.TabPro.TabIndex = 78;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.grdGratuityPro);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(400, 141);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Less Than Five";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // grdGratuityPro
            // 
            this.grdGratuityPro.AllowUserToResizeColumns = false;
            this.grdGratuityPro.AllowUserToResizeRows = false;
            this.grdGratuityPro.BackgroundColor = System.Drawing.Color.White;
            this.grdGratuityPro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGratuityPro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterIDPro,
            this.NoOfMonthsPro,
            this.NoOfDaysPro,
            this.GratuityIDPro});
            this.grdGratuityPro.Location = new System.Drawing.Point(-7, 3);
            this.grdGratuityPro.Name = "grdGratuityPro";
            this.grdGratuityPro.RowHeadersWidth = 25;
            this.grdGratuityPro.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGratuityPro.Size = new System.Drawing.Size(407, 135);
            this.grdGratuityPro.TabIndex = 2;
            this.grdGratuityPro.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdGratuityPro_CellBeginEdit);
            this.grdGratuityPro.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuityPro_CellEndEdit);
            this.grdGratuityPro.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdGratuityPro_EditingControlShowing);
            this.grdGratuityPro.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdGratuityPro_DataError);
            // 
            // ParameterIDPro
            // 
            this.ParameterIDPro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParameterIDPro.HeaderText = "Parameter";
            this.ParameterIDPro.Name = "ParameterIDPro";
            this.ParameterIDPro.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ParameterIDPro.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ParameterIDPro.Width = 120;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.grdGratuityFivePro);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(400, 141);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Greater Than Five";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // grdGratuityFivePro
            // 
            this.grdGratuityFivePro.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.grdGratuityFivePro.AllowUserToResizeColumns = false;
            this.grdGratuityFivePro.AllowUserToResizeRows = false;
            this.grdGratuityFivePro.BackgroundColor = System.Drawing.Color.White;
            this.grdGratuityFivePro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGratuityFivePro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterIDFivePro,
            this.NoOfMonthsFivePro,
            this.NoOfDaysFivePro,
            this.GratuityIDFivePro});
            this.grdGratuityFivePro.Location = new System.Drawing.Point(-1, 2);
            this.grdGratuityFivePro.Name = "grdGratuityFivePro";
            this.grdGratuityFivePro.RowHeadersWidth = 25;
            this.grdGratuityFivePro.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGratuityFivePro.Size = new System.Drawing.Size(401, 139);
            this.grdGratuityFivePro.TabIndex = 3;
            this.grdGratuityFivePro.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdGratuityFivePro_CellBeginEdit);
            this.grdGratuityFivePro.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdGratuityFivePro_CellEndEdit);
            this.grdGratuityFivePro.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdGratuityFivePro_EditingControlShowing);
            this.grdGratuityFivePro.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdGratuityFivePro_DataError);
            // 
            // ParameterIDFivePro
            // 
            this.ParameterIDFivePro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParameterIDFivePro.HeaderText = "Parameter";
            this.ParameterIDFivePro.Name = "ParameterIDFivePro";
            this.ParameterIDFivePro.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ParameterIDFivePro.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ParameterIDFivePro.Width = 120;
            // 
            // chkIncludeLeavePro
            // 
            this.chkIncludeLeavePro.AutoSize = true;
            this.chkIncludeLeavePro.Location = new System.Drawing.Point(6, 104);
            this.chkIncludeLeavePro.Name = "chkIncludeLeavePro";
            this.chkIncludeLeavePro.Size = new System.Drawing.Size(130, 17);
            this.chkIncludeLeavePro.TabIndex = 77;
            this.chkIncludeLeavePro.Text = "Include Eligible Leave";
            this.chkIncludeLeavePro.UseVisualStyleBackColor = true;
            this.chkIncludeLeavePro.CheckedChanged += new System.EventHandler(this.chkIncludeLeavePro_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 75;
            this.label5.Text = "Calculation Based on";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 76;
            this.label6.Text = "Exclude from Gross ";
            // 
            // cboCalculationBasedGratuityPro
            // 
            this.cboCalculationBasedGratuityPro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCalculationBasedGratuityPro.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCalculationBasedGratuityPro.BackColor = System.Drawing.SystemColors.Info;
            this.cboCalculationBasedGratuityPro.DropDownHeight = 134;
            this.cboCalculationBasedGratuityPro.FormattingEnabled = true;
            this.cboCalculationBasedGratuityPro.IntegralHeight = false;
            this.cboCalculationBasedGratuityPro.Location = new System.Drawing.Point(140, 7);
            this.cboCalculationBasedGratuityPro.MaxDropDownItems = 10;
            this.cboCalculationBasedGratuityPro.Name = "cboCalculationBasedGratuityPro";
            this.cboCalculationBasedGratuityPro.Size = new System.Drawing.Size(253, 21);
            this.cboCalculationBasedGratuityPro.TabIndex = 73;
            this.cboCalculationBasedGratuityPro.SelectedIndexChanged += new System.EventHandler(this.cboCalculationBasedGratuityPro_SelectedIndexChanged);
            this.cboCalculationBasedGratuityPro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCalculationBasedGratuityPro_KeyPress);
            this.cboCalculationBasedGratuityPro.SelectedValueChanged += new System.EventHandler(this.cboCalculationBasedGratuityPro_SelectedValueChanged);
            // 
            // txtPolicyName
            // 
            this.txtPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyName.Location = new System.Drawing.Point(121, 15);
            this.txtPolicyName.MaxLength = 30;
            this.txtPolicyName.Name = "txtPolicyName";
            this.txtPolicyName.Size = new System.Drawing.Size(291, 20);
            this.txtPolicyName.TabIndex = 0;
            this.txtPolicyName.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // CompanyIDLabel
            // 
            this.CompanyIDLabel.AutoSize = true;
            this.CompanyIDLabel.Location = new System.Drawing.Point(6, 18);
            this.CompanyIDLabel.Name = "CompanyIDLabel";
            this.CompanyIDLabel.Size = new System.Drawing.Size(60, 13);
            this.CompanyIDLabel.TabIndex = 21;
            this.CompanyIDLabel.Text = "Description";
            // 
            // txtIntText
            // 
            this.txtIntText.Location = new System.Drawing.Point(443, 445);
            this.txtIntText.Name = "txtIntText";
            this.txtIntText.Size = new System.Drawing.Size(100, 20);
            this.txtIntText.TabIndex = 11;
            this.txtIntText.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIntText.Visible = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 53;
            this.LineShape1.X2 = 370;
            this.LineShape1.Y1 = 300;
            this.LineShape1.Y2 = 300;
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(106, 464);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(43, 13);
            this.lblAmount.TabIndex = 47;
            this.lblAmount.Text = "Amount";
            this.lblAmount.Visible = false;
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(6, 439);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(69, 23);
            this.BtnSave.TabIndex = 0;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnBottomCancel
            // 
            this.BtnBottomCancel.Location = new System.Drawing.Point(352, 439);
            this.BtnBottomCancel.Name = "BtnBottomCancel";
            this.BtnBottomCancel.Size = new System.Drawing.Size(71, 23);
            this.BtnBottomCancel.TabIndex = 2;
            this.BtnBottomCancel.Text = "&Cancel";
            this.BtnBottomCancel.UseVisualStyleBackColor = true;
            this.BtnBottomCancel.Click += new System.EventHandler(this.BtnBottomCancel_Click);
            // 
            // OKSaveButton
            // 
            this.OKSaveButton.Location = new System.Drawing.Point(268, 439);
            this.OKSaveButton.Name = "OKSaveButton";
            this.OKSaveButton.Size = new System.Drawing.Size(75, 23);
            this.OKSaveButton.TabIndex = 1;
            this.OKSaveButton.Text = "&Ok";
            this.OKSaveButton.UseVisualStyleBackColor = true;
            this.OKSaveButton.Click += new System.EventHandler(this.OKSaveButton_Click);
            // 
            // PolicyIDTextBox
            // 
            this.PolicyIDTextBox.Location = new System.Drawing.Point(541, 283);
            this.PolicyIDTextBox.Name = "PolicyIDTextBox";
            this.PolicyIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.PolicyIDTextBox.TabIndex = 12;
            this.PolicyIDTextBox.Visible = false;
            // 
            // TextboxNumeric
            // 
            this.TextboxNumeric.Location = new System.Drawing.Point(541, 250);
            this.TextboxNumeric.Name = "TextboxNumeric";
            this.TextboxNumeric.Size = new System.Drawing.Size(100, 20);
            this.TextboxNumeric.TabIndex = 11;
            this.TextboxNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TextboxNumeric.Visible = false;
            // 
            // CompanySettlementPolicyBindingNavigator
            // 
            this.CompanySettlementPolicyBindingNavigator.AddNewItem = null;
            this.CompanySettlementPolicyBindingNavigator.CountItem = null;
            this.CompanySettlementPolicyBindingNavigator.DeleteItem = null;
            this.CompanySettlementPolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripSeparator4,
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorPositionItem,
            this.bnCountItem,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.ToolStripSeparator5,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnCancel,
            this.ToolStripSeparator6,
            this.bnHelp});
            this.CompanySettlementPolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.CompanySettlementPolicyBindingNavigator.MoveFirstItem = null;
            this.CompanySettlementPolicyBindingNavigator.MoveLastItem = null;
            this.CompanySettlementPolicyBindingNavigator.MoveNextItem = null;
            this.CompanySettlementPolicyBindingNavigator.MovePreviousItem = null;
            this.CompanySettlementPolicyBindingNavigator.Name = "CompanySettlementPolicyBindingNavigator";
            this.CompanySettlementPolicyBindingNavigator.PositionItem = null;
            this.CompanySettlementPolicyBindingNavigator.Size = new System.Drawing.Size(437, 25);
            this.CompanySettlementPolicyBindingNavigator.TabIndex = 19;
            this.CompanySettlementPolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.MaxLength = 12;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.CompanySettlementPolicyBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(23, 22);
            this.BtnCancel.ToolTipText = "Clear";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // ToolStripSeparator6
            // 
            this.ToolStripSeparator6.Name = "ToolStripSeparator6";
            this.ToolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // bnHelp
            // 
            this.bnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.Text = "He&lp";
            this.bnHelp.Click += new System.EventHandler(this.bnHelp_Click);
            // 
            // StatusStripSettlement
            // 
            this.StatusStripSettlement.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.StatusStripSettlement.Location = new System.Drawing.Point(0, 468);
            this.StatusStripSettlement.Name = "StatusStripSettlement";
            this.StatusStripSettlement.Size = new System.Drawing.Size(437, 22);
            this.StatusStripSettlement.TabIndex = 20;
            this.StatusStripSettlement.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // ErrSettlement
            // 
            this.ErrSettlement.ContainerControl = this;
            this.ErrSettlement.RightToLeft = true;
            // 
            // TmrSettlement
            // 
            this.TmrSettlement.Tick += new System.EventHandler(this.TmrSettlement_Tick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "NoOfMonths";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "NoOfDays";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn2.MinimumWidth = 80;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "GratuityID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            this.dataGridViewTextBoxColumn3.Width = 5;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "NoOfMonths";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "NoOfDays";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn5.MinimumWidth = 80;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "GratuityID";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "NoOfMonths";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn8.HeaderText = "NoOfDays";
            this.dataGridViewTextBoxColumn8.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn8.MinimumWidth = 80;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "GratuityID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            this.dataGridViewTextBoxColumn9.Width = 5;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "NoOfMonths";
            this.dataGridViewTextBoxColumn10.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn11.HeaderText = "NoOfDays";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn11.MinimumWidth = 80;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "GratuityID";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "NoOfMonths";
            this.dataGridViewTextBoxColumn13.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn14.HeaderText = "NoOfDays";
            this.dataGridViewTextBoxColumn14.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn14.MinimumWidth = 80;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "GratuityID";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Visible = false;
            this.dataGridViewTextBoxColumn15.Width = 5;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "NoOfMonths";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn17.HeaderText = "NoOfDays";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn17.MinimumWidth = 80;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "GratuityID";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Visible = false;
            // 
            // NoOfMonths
            // 
            this.NoOfMonths.HeaderText = "NoOfMonths";
            this.NoOfMonths.MaxInputLength = 5;
            this.NoOfMonths.Name = "NoOfMonths";
            // 
            // NoOfDays
            // 
            this.NoOfDays.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NoOfDays.HeaderText = "NoOfDays";
            this.NoOfDays.MaxInputLength = 5;
            this.NoOfDays.MinimumWidth = 80;
            this.NoOfDays.Name = "NoOfDays";
            // 
            // GratuityID
            // 
            this.GratuityID.HeaderText = "GratuityID";
            this.GratuityID.Name = "GratuityID";
            this.GratuityID.Visible = false;
            this.GratuityID.Width = 5;
            // 
            // NoOfMonthsFive
            // 
            this.NoOfMonthsFive.HeaderText = "NoOfMonths";
            this.NoOfMonthsFive.MaxInputLength = 5;
            this.NoOfMonthsFive.Name = "NoOfMonthsFive";
            // 
            // NoOfDaysFive
            // 
            this.NoOfDaysFive.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NoOfDaysFive.HeaderText = "NoOfDays";
            this.NoOfDaysFive.MaxInputLength = 5;
            this.NoOfDaysFive.MinimumWidth = 80;
            this.NoOfDaysFive.Name = "NoOfDaysFive";
            // 
            // GratuityIDFive
            // 
            this.GratuityIDFive.HeaderText = "GratuityID";
            this.GratuityIDFive.Name = "GratuityIDFive";
            this.GratuityIDFive.Visible = false;
            // 
            // DetailsDataGridViewGratuity
            // 
            this.DetailsDataGridViewGratuity.AddNewRow = false;
            this.DetailsDataGridViewGratuity.AllowUserToAddRows = false;
            this.DetailsDataGridViewGratuity.AllowUserToDeleteRows = false;
            this.DetailsDataGridViewGratuity.AllowUserToResizeColumns = false;
            this.DetailsDataGridViewGratuity.AllowUserToResizeRows = false;
            this.DetailsDataGridViewGratuity.AlphaNumericCols = new int[0];
            this.DetailsDataGridViewGratuity.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DetailsDataGridViewGratuity.CapsLockCols = new int[0];
            this.DetailsDataGridViewGratuity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DetailsDataGridViewGratuity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtAddDedID,
            this.chkParticular,
            this.txtParticulars});
            this.DetailsDataGridViewGratuity.DecimalCols = new int[0];
            this.DetailsDataGridViewGratuity.HasSlNo = false;
            this.DetailsDataGridViewGratuity.LastRowIndex = 0;
            this.DetailsDataGridViewGratuity.Location = new System.Drawing.Point(143, 37);
            this.DetailsDataGridViewGratuity.MultiSelect = false;
            this.DetailsDataGridViewGratuity.Name = "DetailsDataGridViewGratuity";
            this.DetailsDataGridViewGratuity.NegativeValueCols = new int[0];
            this.DetailsDataGridViewGratuity.NumericCols = new int[0];
            this.DetailsDataGridViewGratuity.RowHeadersWidth = 35;
            this.DetailsDataGridViewGratuity.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DetailsDataGridViewGratuity.Size = new System.Drawing.Size(253, 84);
            this.DetailsDataGridViewGratuity.TabIndex = 1;
            this.DetailsDataGridViewGratuity.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DetailsDataGridViewGratuity_CellBeginEdit);
            this.DetailsDataGridViewGratuity.CurrentCellDirtyStateChanged += new System.EventHandler(this.DetailsDataGridViewGratuity_CurrentCellDirtyStateChanged);
            this.DetailsDataGridViewGratuity.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DetailsDataGridViewGratuity_DataError);
            this.DetailsDataGridViewGratuity.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DetailsDataGridViewGratuity_CellContentClick);
            // 
            // txtAddDedID
            // 
            this.txtAddDedID.HeaderText = "txtAddDedID";
            this.txtAddDedID.Name = "txtAddDedID";
            this.txtAddDedID.Visible = false;
            // 
            // chkParticular
            // 
            this.chkParticular.HeaderText = "";
            this.chkParticular.Name = "chkParticular";
            this.chkParticular.Width = 30;
            // 
            // txtParticulars
            // 
            this.txtParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtParticulars.HeaderText = "Particulars";
            this.txtParticulars.Name = "txtParticulars";
            this.txtParticulars.ReadOnly = true;
            this.txtParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NoOfMonthsTer
            // 
            this.NoOfMonthsTer.HeaderText = "NoOfMonths";
            this.NoOfMonthsTer.MaxInputLength = 5;
            this.NoOfMonthsTer.Name = "NoOfMonthsTer";
            // 
            // NoOfDaysTer
            // 
            this.NoOfDaysTer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NoOfDaysTer.HeaderText = "NoOfDays";
            this.NoOfDaysTer.MaxInputLength = 5;
            this.NoOfDaysTer.MinimumWidth = 80;
            this.NoOfDaysTer.Name = "NoOfDaysTer";
            // 
            // GratuityIDTer
            // 
            this.GratuityIDTer.HeaderText = "GratuityID";
            this.GratuityIDTer.Name = "GratuityIDTer";
            this.GratuityIDTer.Visible = false;
            this.GratuityIDTer.Width = 5;
            // 
            // NoOfMonthsFiveTer
            // 
            this.NoOfMonthsFiveTer.HeaderText = "NoOfMonths";
            this.NoOfMonthsFiveTer.MaxInputLength = 5;
            this.NoOfMonthsFiveTer.Name = "NoOfMonthsFiveTer";
            // 
            // NoOfDaysFiveTer
            // 
            this.NoOfDaysFiveTer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NoOfDaysFiveTer.HeaderText = "NoOfDays";
            this.NoOfDaysFiveTer.MaxInputLength = 5;
            this.NoOfDaysFiveTer.MinimumWidth = 80;
            this.NoOfDaysFiveTer.Name = "NoOfDaysFiveTer";
            // 
            // GratuityIDFiveTer
            // 
            this.GratuityIDFiveTer.HeaderText = "GratuityID";
            this.GratuityIDFiveTer.Name = "GratuityIDFiveTer";
            this.GratuityIDFiveTer.Visible = false;
            // 
            // DetailsDataGridViewGratuityTer
            // 
            this.DetailsDataGridViewGratuityTer.AddNewRow = false;
            this.DetailsDataGridViewGratuityTer.AllowUserToAddRows = false;
            this.DetailsDataGridViewGratuityTer.AllowUserToDeleteRows = false;
            this.DetailsDataGridViewGratuityTer.AllowUserToResizeColumns = false;
            this.DetailsDataGridViewGratuityTer.AllowUserToResizeRows = false;
            this.DetailsDataGridViewGratuityTer.AlphaNumericCols = new int[0];
            this.DetailsDataGridViewGratuityTer.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DetailsDataGridViewGratuityTer.CapsLockCols = new int[0];
            this.DetailsDataGridViewGratuityTer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DetailsDataGridViewGratuityTer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtAddDedIDTer,
            this.chkParticularTer,
            this.txtParticularsTer});
            this.DetailsDataGridViewGratuityTer.DecimalCols = new int[0];
            this.DetailsDataGridViewGratuityTer.HasSlNo = false;
            this.DetailsDataGridViewGratuityTer.LastRowIndex = 0;
            this.DetailsDataGridViewGratuityTer.Location = new System.Drawing.Point(140, 35);
            this.DetailsDataGridViewGratuityTer.MultiSelect = false;
            this.DetailsDataGridViewGratuityTer.Name = "DetailsDataGridViewGratuityTer";
            this.DetailsDataGridViewGratuityTer.NegativeValueCols = new int[0];
            this.DetailsDataGridViewGratuityTer.NumericCols = new int[0];
            this.DetailsDataGridViewGratuityTer.RowHeadersWidth = 35;
            this.DetailsDataGridViewGratuityTer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DetailsDataGridViewGratuityTer.Size = new System.Drawing.Size(253, 85);
            this.DetailsDataGridViewGratuityTer.TabIndex = 50;
            this.DetailsDataGridViewGratuityTer.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DetailsDataGridViewGratuityTer_CellValidating);
            this.DetailsDataGridViewGratuityTer.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DetailsDataGridViewGratuityTer_DataError);
            // 
            // txtAddDedIDTer
            // 
            this.txtAddDedIDTer.HeaderText = "txtAddDedID";
            this.txtAddDedIDTer.Name = "txtAddDedIDTer";
            this.txtAddDedIDTer.Visible = false;
            // 
            // chkParticularTer
            // 
            this.chkParticularTer.HeaderText = "";
            this.chkParticularTer.Name = "chkParticularTer";
            this.chkParticularTer.Width = 30;
            // 
            // txtParticularsTer
            // 
            this.txtParticularsTer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtParticularsTer.HeaderText = "Particulars";
            this.txtParticularsTer.Name = "txtParticularsTer";
            this.txtParticularsTer.ReadOnly = true;
            this.txtParticularsTer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NoOfMonthsPro
            // 
            this.NoOfMonthsPro.HeaderText = "NoOfMonths";
            this.NoOfMonthsPro.MaxInputLength = 5;
            this.NoOfMonthsPro.Name = "NoOfMonthsPro";
            // 
            // NoOfDaysPro
            // 
            this.NoOfDaysPro.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NoOfDaysPro.HeaderText = "NoOfDays";
            this.NoOfDaysPro.MaxInputLength = 5;
            this.NoOfDaysPro.MinimumWidth = 80;
            this.NoOfDaysPro.Name = "NoOfDaysPro";
            // 
            // GratuityIDPro
            // 
            this.GratuityIDPro.HeaderText = "GratuityID";
            this.GratuityIDPro.Name = "GratuityIDPro";
            this.GratuityIDPro.Visible = false;
            this.GratuityIDPro.Width = 5;
            // 
            // NoOfMonthsFivePro
            // 
            this.NoOfMonthsFivePro.HeaderText = "NoOfMonths";
            this.NoOfMonthsFivePro.MaxInputLength = 5;
            this.NoOfMonthsFivePro.Name = "NoOfMonthsFivePro";
            // 
            // NoOfDaysFivePro
            // 
            this.NoOfDaysFivePro.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NoOfDaysFivePro.HeaderText = "NoOfDays";
            this.NoOfDaysFivePro.MaxInputLength = 5;
            this.NoOfDaysFivePro.MinimumWidth = 80;
            this.NoOfDaysFivePro.Name = "NoOfDaysFivePro";
            // 
            // GratuityIDFivePro
            // 
            this.GratuityIDFivePro.HeaderText = "GratuityID";
            this.GratuityIDFivePro.Name = "GratuityIDFivePro";
            this.GratuityIDFivePro.Visible = false;
            // 
            // DetailsDataGridViewGratuityPro
            // 
            this.DetailsDataGridViewGratuityPro.AddNewRow = false;
            this.DetailsDataGridViewGratuityPro.AllowUserToAddRows = false;
            this.DetailsDataGridViewGratuityPro.AllowUserToDeleteRows = false;
            this.DetailsDataGridViewGratuityPro.AllowUserToResizeColumns = false;
            this.DetailsDataGridViewGratuityPro.AllowUserToResizeRows = false;
            this.DetailsDataGridViewGratuityPro.AlphaNumericCols = new int[0];
            this.DetailsDataGridViewGratuityPro.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DetailsDataGridViewGratuityPro.CapsLockCols = new int[0];
            this.DetailsDataGridViewGratuityPro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DetailsDataGridViewGratuityPro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtAddDedIDPro,
            this.chkParticularPro,
            this.txtParticularsPro});
            this.DetailsDataGridViewGratuityPro.DecimalCols = new int[0];
            this.DetailsDataGridViewGratuityPro.HasSlNo = false;
            this.DetailsDataGridViewGratuityPro.LastRowIndex = 0;
            this.DetailsDataGridViewGratuityPro.Location = new System.Drawing.Point(140, 36);
            this.DetailsDataGridViewGratuityPro.MultiSelect = false;
            this.DetailsDataGridViewGratuityPro.Name = "DetailsDataGridViewGratuityPro";
            this.DetailsDataGridViewGratuityPro.NegativeValueCols = new int[0];
            this.DetailsDataGridViewGratuityPro.NumericCols = new int[0];
            this.DetailsDataGridViewGratuityPro.RowHeadersWidth = 35;
            this.DetailsDataGridViewGratuityPro.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DetailsDataGridViewGratuityPro.Size = new System.Drawing.Size(253, 85);
            this.DetailsDataGridViewGratuityPro.TabIndex = 74;
            this.DetailsDataGridViewGratuityPro.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DetailsDataGridViewGratuityPro_CellValidating);
            this.DetailsDataGridViewGratuityPro.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DetailsDataGridViewGratuityPro_DataError);
            // 
            // txtAddDedIDPro
            // 
            this.txtAddDedIDPro.HeaderText = "txtAddDedID";
            this.txtAddDedIDPro.Name = "txtAddDedIDPro";
            this.txtAddDedIDPro.Visible = false;
            // 
            // chkParticularPro
            // 
            this.chkParticularPro.HeaderText = "";
            this.chkParticularPro.Name = "chkParticularPro";
            this.chkParticularPro.Width = 30;
            // 
            // txtParticularsPro
            // 
            this.txtParticularsPro.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.txtParticularsPro.HeaderText = "Particulars";
            this.txtParticularsPro.Name = "txtParticularsPro";
            this.txtParticularsPro.ReadOnly = true;
            this.txtParticularsPro.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FrmSettlementPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 490);
            this.Controls.Add(this.StatusStripSettlement);
            this.Controls.Add(this.lblAmount);
            this.Controls.Add(this.CompanySettlementPolicyBindingNavigator);
            this.Controls.Add(this.PolicyIDTextBox);
            this.Controls.Add(this.txtIntText);
            this.Controls.Add(this.TextboxNumeric);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnBottomCancel);
            this.Controls.Add(this.OKSaveButton);
            this.Controls.Add(this.GrpCSP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSettlementPolicy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settlement Policy";
            this.Load += new System.EventHandler(this.FrmSettlementPolicy_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSettlementPolicy_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSettlementPolicy_KeyDown);
            this.GrpCSP.ResumeLayout(false);
            this.GrpCSP.PerformLayout();
            this.tbSettlement.ResumeLayout(false);
            this.TabGratuity.ResumeLayout(false);
            this.TabGratuity.PerformLayout();
            this.TabGrauity.ResumeLayout(false);
            this.TabLessThanFive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuity)).EndInit();
            this.TabGreaterThanFive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityFive)).EndInit();
            this.TabTermination.ResumeLayout(false);
            this.TabTermination.PerformLayout();
            this.TabTermin.ResumeLayout(false);
            this.TabLesFiveTer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityTer)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityFiveTer)).EndInit();
            this.TabProvision.ResumeLayout(false);
            this.TabProvision.PerformLayout();
            this.TabPro.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityPro)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGratuityFivePro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanySettlementPolicyBindingNavigator)).EndInit();
            this.CompanySettlementPolicyBindingNavigator.ResumeLayout(false);
            this.CompanySettlementPolicyBindingNavigator.PerformLayout();
            this.StatusStripSettlement.ResumeLayout(false);
            this.StatusStripSettlement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrSettlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewGratuity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewGratuityTer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailsDataGridViewGratuityPro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GrpCSP;
        internal System.Windows.Forms.TabControl tbSettlement;
        internal System.Windows.Forms.TabPage TabGratuity;
        internal System.Windows.Forms.Label lblAmount;
        internal System.Windows.Forms.Label lblCalcGratuity;
        internal System.Windows.Forms.Label lblGrossGratuity;
        internal System.Windows.Forms.TextBox txtIntText;
        internal System.Windows.Forms.ComboBox cboCalculationBasedGratuity;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.TextBox txtPolicyName;
        internal System.Windows.Forms.Label CompanyIDLabel;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnBottomCancel;
        internal System.Windows.Forms.Button OKSaveButton;
        internal System.Windows.Forms.TextBox PolicyIDTextBox;
        internal System.Windows.Forms.TextBox TextboxNumeric;
        internal System.Windows.Forms.BindingNavigator CompanySettlementPolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripLabel bnCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator6;
        internal System.Windows.Forms.ToolStripButton BtnCancel;
        private DemoClsDataGridview.ClsDataGirdView DetailsDataGridViewGratuity;
        private System.Windows.Forms.DataGridView grdGratuity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        internal System.Windows.Forms.StatusStrip StatusStripSettlement;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.ErrorProvider ErrSettlement;
        internal System.Windows.Forms.Timer TmrSettlement;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtAddDedID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkParticular;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        private System.Windows.Forms.CheckBox chkIncludeLeave;
        private System.Windows.Forms.TabControl TabGrauity;
        private System.Windows.Forms.TabPage TabLessThanFive;
        private System.Windows.Forms.TabPage TabGreaterThanFive;
        private System.Windows.Forms.DataGridView grdGratuityFive;
        private System.Windows.Forms.TabPage TabTermination;
        private System.Windows.Forms.TabControl TabTermin;
        private System.Windows.Forms.TabPage TabLesFiveTer;
        private System.Windows.Forms.DataGridView grdGratuityTer;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView grdGratuityFiveTer;
        private System.Windows.Forms.CheckBox chkIncludeLeaveTer;
        private DemoClsDataGridview.ClsDataGirdView DetailsDataGridViewGratuityTer;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.ComboBox cboCalculationBasedGratuityTer;
        private System.Windows.Forms.DataGridViewComboBoxColumn ParameterID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfMonths;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityID;
        private System.Windows.Forms.DataGridViewComboBoxColumn ParameterIDFive;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfMonthsFive;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfDaysFive;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityIDFive;
        private System.Windows.Forms.DataGridViewComboBoxColumn ParameterIDTer;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfMonthsTer;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfDaysTer;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityIDTer;
        private System.Windows.Forms.DataGridViewComboBoxColumn ParameterIDFiveTer;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfMonthsFiveTer;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfDaysFiveTer;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityIDFiveTer;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtAddDedIDTer;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkParticularTer;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtParticularsTer;
        internal System.Windows.Forms.RadioButton rdbActualMonthN;
        internal System.Windows.Forms.RadioButton rdbCompanyBasedN;
        internal System.Windows.Forms.RadioButton rdbActualMonthT;
        internal System.Windows.Forms.RadioButton rdbCompanyBasedT;
        internal System.Windows.Forms.Label lblCaldays;
        internal System.Windows.Forms.Label lblCalDayTer;
        private System.Windows.Forms.TabPage TabProvision;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.RadioButton rdbActualMonthPro;
        internal System.Windows.Forms.RadioButton rdbCompanyBasedPro;
        private System.Windows.Forms.TabControl TabPro;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView grdGratuityPro;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView grdGratuityFivePro;
        private System.Windows.Forms.CheckBox chkIncludeLeavePro;
        private DemoClsDataGridview.ClsDataGirdView DetailsDataGridViewGratuityPro;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.ComboBox cboCalculationBasedGratuityPro;
        private System.Windows.Forms.DataGridViewComboBoxColumn ParameterIDPro;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfMonthsPro;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfDaysPro;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityIDPro;
        private System.Windows.Forms.DataGridViewComboBoxColumn ParameterIDFivePro;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfMonthsFivePro;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfDaysFivePro;
        private System.Windows.Forms.DataGridViewTextBoxColumn GratuityIDFivePro;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtAddDedIDPro;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkParticularPro;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtParticularsPro;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
    }
}