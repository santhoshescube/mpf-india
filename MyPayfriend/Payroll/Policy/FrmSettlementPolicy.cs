﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DTO;
using BLL;
using DAL;

namespace MyPayfriend
{
    public partial class FrmSettlementPolicy : Form
    {

        bool MbChangeStatus;
        bool MbAddStatus = false;


        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission

        string MsMessageCommon;
        int RecordCnt = 0;
        int iRowIndex;
        //int RecordCnt;
        int CurrentRecCnt;
        //int TempParamID;




        //int RecordCnt = 0;


        //private string MsMessageCommon; //Messagebox display
        private string MstrMessageCaption;              //Message caption

        private ArrayList MsarMessageArr; // Error Message display
        //private ArrayList MaStatusMessage;// Status bar error message display
        public int PiRecID = 0;// Reference From Work policy

        public int PiCompanyID = 0;// Reference From Work policy
        //private int TotalRecordCnt;
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid
        private int MintUserId;

        private MessageBoxIcon MmessageIcon;            // to set the message icon
        //string strPolicyNameCheck = "";
        private bool Glb24HourFormat;

        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;
        clsBLLSettlementPolicy MobjclsBLLSettlementPolicy;
        string strBindingOf = "Of ";

        public FrmSettlementPolicy()
        {
            //Constructor                        
            InitializeComponent();
            MintCompanyId = ClsCommonSettings.CurrentCompanyID;
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            Glb24HourFormat = ClsCommonSettings.Glb24HourFormat;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLSettlementPolicy = new clsBLLSettlementPolicy();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.SettlementPolicy, this);
            lblCaldays.Text = lblCalDayTer.Text = "يوم الحساب";
            TabTermination.Text = "إنهاء الخدمة";
            strBindingOf = "من ";
            DetailsDataGridViewGratuity.Columns["txtParticulars"].HeaderText = "تفاصيل";
            DetailsDataGridViewGratuityTer.Columns["txtParticularsTer"].HeaderText = "تفاصيل";
            
            rdbCompanyBasedT.Text =rdbCompanyBasedN.Text= "على أساس الشركة";
            rdbActualMonthN.Text =rdbActualMonthT.Text =  "شهر الفعلي";
            label1.Text = lblCalcGratuity.Text;
            label3.Text = lblGrossGratuity.Text;
            chkIncludeLeaveTer.Text = chkIncludeLeave.Text;
            TabLesFiveTer.Text = TabLessThanFive.Text;
            tabPage2.Text = TabGreaterThanFive.Text;
            ParameterIDTer.HeaderText = ParameterIDFiveTer.HeaderText = ParameterID.HeaderText;
            NoOfMonthsTer.HeaderText = NoOfMonthsFiveTer.HeaderText = NoOfMonths.HeaderText;
            NoOfDaysTer.HeaderText = NoOfDaysFiveTer.HeaderText = NoOfDays.HeaderText;
        }
        private void FrmSettlementPolicy_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            SetPermissions();
            LoadMessage();
            AddMode();

            BindingNavigatorAddNewItem_Click(sender, e);
            if (PiRecID > 0)
            {
                int RowNum = 0;
                RowNum = MobjclsBLLSettlementPolicy.GetRowNum(PiRecID);

                RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
                MobjclsBLLSettlementPolicy.DisplayDetails(RowNum);
                DisplayDetails(CurrentRecCnt);
                MbAddStatus = false;
                BindingNavigatorSaveItem.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;

                BindingNavigatorPositionItem.Text = "1";
                bnCountItem.Text = strBindingOf + "1";
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled =
                    BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
        }

        private void AddMode()
        {
            try
            {
                txtPolicyName.Text = "";
               rdbCompanyBasedPro.Checked = rdbActualMonthPro.Checked = rdbCompanyBasedN.Checked = rdbActualMonthN.Checked = rdbCompanyBasedT.Checked = rdbActualMonthT.Checked = false; 
                MobjclsBLLSettlementPolicy = new clsBLLSettlementPolicy();
                cboCalculationBasedGratuity.SelectedIndex = -1;
                cboCalculationBasedGratuityPro.SelectedIndex= cboCalculationBasedGratuityTer.SelectedIndex = -1;
                chkIncludeLeave.Checked = false;
                chkIncludeLeaveTer.Checked = false;
                chkIncludeLeavePro.Checked = false;
                if (grdGratuity.RowCount >= 0)
                {
                    grdGratuity.ClearSelection();
                }
                grdGratuityTer.ClearSelection();
                grdGratuityTer.Rows.Clear();
                grdGratuityPro.ClearSelection();
                grdGratuityPro.Rows.Clear();
                grdGratuity.Rows.Clear();
                tbSettlement.SelectedTab = TabGratuity; 
                this.grdGratuity.AlternatingRowsDefaultCellStyle.BackColor = SystemColors.ControlLight;
                LoadCombos(2);
                LoadCombos(4);
                LoadCombos(6);

            }
            catch (Exception ex)
            {
            }
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.SettlementPolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }
        private void LoadMessage()
        {
            // Loading Message
            try
            {
                MsarMessageArr = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.SettlementPolicy, 2);
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage " + this.Name + " " + Ex.Message.ToString(), 1);
            }

        }


        private void ChangeStatus(object sender, EventArgs e)
        {

            if (MbAddStatus == true)
            {
                this.BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                this.OKSaveButton.Enabled = BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BtnSave.Enabled = MblnAddPermission;
            }
            else
            {
                this.BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                this.OKSaveButton.Enabled = BindingNavigatorAddNewItem.Enabled = MblnUpdatePermission;
                this.BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BtnSave.Enabled = MblnUpdatePermission;
            }
            MbChangeStatus = true;
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt - 1;
                if (CurrentRecCnt <= 0)
                {
                    CurrentRecCnt = 1;
                }
                if (CurrentRecCnt <= 1)
                {
                    BindingNavigatorMovePreviousItem.Enabled = false;
                    BindingNavigatorMoveFirstItem.Enabled = false;
                }
                if (RecordCnt > 1 && CurrentRecCnt < RecordCnt)
                {
                    BindingNavigatorMoveNextItem.Enabled = true;
                    BindingNavigatorMoveLastItem.Enabled = true;
                }
                else
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }

            MbAddStatus = false;

            this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            DisplayDetails(CurrentRecCnt);
            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) - 1).ToString();
            if (Convert.ToInt32(BindingNavigatorPositionItem.Text) <= 0)
                BindingNavigatorPositionItem.Text = "1";
            if (RecordCnt > 1)
                bnCountItem.Text = strBindingOf + RecordCnt.ToString();
            else
                bnCountItem.Text = strBindingOf + "1";

            BindingNavigatorSaveItem.Enabled = false;
            BtnSave.Enabled = false;
            OKSaveButton.Enabled = false;
            TmrSettlement.Stop();
            TmrSettlement.Start();

        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = 1;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                if (RecordCnt > 1 && CurrentRecCnt < RecordCnt)
                {
                    BindingNavigatorMoveNextItem.Enabled = true;
                    BindingNavigatorMoveLastItem.Enabled = true;
                }
                else
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }


            MbAddStatus = false;
            this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            DisplayDetails(CurrentRecCnt);

            BindingNavigatorPositionItem.Text = "1";
            if (RecordCnt > 1)
                bnCountItem.Text = strBindingOf + RecordCnt.ToString();
            else
                bnCountItem.Text = strBindingOf + "1";

            BindingNavigatorSaveItem.Enabled = false;
            BtnSave.Enabled = false;
            OKSaveButton.Enabled = false;
            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
            TmrSettlement.Stop();
            TmrSettlement.Start();

        }

        private void CompanySettlementPolicyBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            txtPolicyName.Focus();
            if (FormValidation() == true)
            {
                if (MbAddStatus == true)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 1, out MmessageIcon);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                }
                else
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3, out MmessageIcon);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                }
                if (MessageBox.Show(MsMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
                if (MobjclsBLLSettlementPolicy.SaveCompanySettlementPolicy() == true)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    MbAddStatus = false;
                    BindingNavigatorSaveItem.Enabled = false;
                    BtnSave.Enabled = false;
                    BtnCancel.Enabled = OKSaveButton.Enabled = false;
                    BindingNavigatorMoveLastItem_Click(sender, e);
                }
            }


        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = CurrentRecCnt + 1;
                if (CurrentRecCnt >= RecordCnt)
                {
                    CurrentRecCnt = RecordCnt;
                }
                if (CurrentRecCnt >= RecordCnt)
                {
                    BindingNavigatorMoveNextItem.Enabled = false;
                    BindingNavigatorMoveLastItem.Enabled = false;
                }
                if (RecordCnt > 1 && CurrentRecCnt > 1)
                {
                    BindingNavigatorMovePreviousItem.Enabled = true;
                    BindingNavigatorMoveFirstItem.Enabled = true;
                }
                else
                {
                    BindingNavigatorMovePreviousItem.Enabled = false;
                    BindingNavigatorMoveFirstItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }

            MbAddStatus = false;
            this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            TabGrauity.SelectedIndex = 0;
            MobjclsBLLSettlementPolicy.DisplayDetails(CurrentRecCnt);

            BindingNavigatorPositionItem.Text = (Convert.ToInt32(BindingNavigatorPositionItem.Text) + 1).ToString();
            if (RecordCnt < Convert.ToInt32(BindingNavigatorPositionItem.Text))
            {
                if (RecordCnt > 1)
                    BindingNavigatorPositionItem.Text = RecordCnt.ToString();
                else
                    BindingNavigatorPositionItem.Text = "1";
            }
            if (RecordCnt > 1)
                bnCountItem.Text = strBindingOf + RecordCnt.ToString();
            else
                bnCountItem.Text = strBindingOf + "1";

            BindingNavigatorSaveItem.Enabled = false;
            BtnSave.Enabled = false;
            OKSaveButton.Enabled = false;
            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
            TmrSettlement.Stop();
            TmrSettlement.Start();
            DisplayDetails(CurrentRecCnt);

        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCountNavigate();
            if (RecordCnt > 0)
            {
                CurrentRecCnt = RecordCnt;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
                if (RecordCnt > 1 && CurrentRecCnt > 1)
                {
                    BindingNavigatorMovePreviousItem.Enabled = true;
                    BindingNavigatorMoveFirstItem.Enabled = true;
                }
                else
                {
                    BindingNavigatorMovePreviousItem.Enabled = false;
                    BindingNavigatorMoveFirstItem.Enabled = false;
                }
            }
            else
            {
                CurrentRecCnt = 0;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            MbAddStatus = false;
            this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;

            DisplayDetails(CurrentRecCnt);

            if (RecordCnt > 1)
            {
                BindingNavigatorPositionItem.Text = RecordCnt.ToString();
                bnCountItem.Text = strBindingOf + RecordCnt.ToString();
            }
            else
            {
                BindingNavigatorPositionItem.Text = "1";
                bnCountItem.Text = strBindingOf + "1";
            }

            BindingNavigatorSaveItem.Enabled = false;
            BtnSave.Enabled = false;
            OKSaveButton.Enabled = false;
            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
            TmrSettlement.Stop();
            TmrSettlement.Start();
        }
        private void DisplayDetails(int CurrentRecCnt)
        {
            try
            {
                int iCounter = 0;
                TabGrauity.SelectedIndex = 0;
                MobjclsBLLSettlementPolicy.DisplayDetails(CurrentRecCnt);
                txtPolicyName.Text = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.DescriptionPolicy;
                cboCalculationBasedGratuity.SelectedValue = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID;
                cboCalculationBasedGratuityTer.SelectedValue = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationIDTer;
                cboCalculationBasedGratuityPro.SelectedValue = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationIDPro;
                chkIncludeLeave.Checked = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.blnIsIncludeLeave;
                chkIncludeLeaveTer.Checked = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.blnIsIncludeLeaveTer;
                chkIncludeLeavePro.Checked = MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.blnIsIncludeLeavePro;

                if (MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CompanyBasedN.ToBoolean())
                {
                    rdbCompanyBasedN.Checked = true;
                }
                else
                {
                    rdbActualMonthN.Checked = true;  
                }

                if (MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CompanyBasedT.ToBoolean())
                {
                    rdbCompanyBasedT.Checked = true;
                }
                else
                {
                    rdbActualMonthT.Checked = true;
                }


                if (MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CompanyBasedP.ToBoolean())
                {
                    rdbCompanyBasedPro.Checked = true;
                }
                else
                {
                    rdbActualMonthPro.Checked = true;
                }

                iCounter = 0;
                grdGratuity.Rows.Clear();
                foreach (clsDTOSettlementPolicyDetail objclsDTOSettlementPolicyDetail in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetail)
                {
                    grdGratuity.RowCount = grdGratuity.RowCount + 1;


                    grdGratuity.Rows[iCounter].Cells["ParameterID"].Value = objclsDTOSettlementPolicyDetail.ParameterID;
                    grdGratuity.Rows[iCounter].Cells["ParameterID"].Tag = objclsDTOSettlementPolicyDetail.ParameterID;
                    grdGratuity.Rows[iCounter].Cells["ParameterID"].Value = grdGratuity.Rows[iCounter].Cells["ParameterID"].FormattedValue;

                    grdGratuity.Rows[iCounter].Cells["NoOfMonths"].Value = objclsDTOSettlementPolicyDetail.NoOfMonths;
                    grdGratuity.Rows[iCounter].Cells["NoOfDays"].Value = objclsDTOSettlementPolicyDetail.NoOfDays;
                    grdGratuity.Rows[iCounter].Cells["GratuityID"].Value = 0;//MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID; 
                    iCounter = iCounter + 1;

                }


                iCounter = 0;
                grdGratuityFive.Rows.Clear();
                foreach (clsDTOSettlementPolicyDetailFive objclsDTOSettlementPolicyDetailFive in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFive)
                {
                    grdGratuityFive.RowCount = grdGratuityFive.RowCount + 1;


                    grdGratuityFive.Rows[iCounter].Cells["ParameterIDFive"].Value = objclsDTOSettlementPolicyDetailFive.ParameterID;
                    grdGratuityFive.Rows[iCounter].Cells["ParameterIDFive"].Tag = objclsDTOSettlementPolicyDetailFive.ParameterID;
                    grdGratuityFive.Rows[iCounter].Cells["ParameterIDFive"].Value = grdGratuityFive.Rows[iCounter].Cells["ParameterIDFive"].FormattedValue;

                    grdGratuityFive.Rows[iCounter].Cells["NoOfMonthsFive"].Value = objclsDTOSettlementPolicyDetailFive.NoOfMonths;
                    grdGratuityFive.Rows[iCounter].Cells["NoOfDaysFive"].Value = objclsDTOSettlementPolicyDetailFive.NoOfDays;
                    grdGratuityFive.Rows[iCounter].Cells["GratuityIDFive"].Value = 0;//MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID; 
                    iCounter = iCounter + 1;

                }

                iCounter = 0;
                DetailsDataGridViewGratuity.Rows.Clear();
                foreach (clsDTOInExGra objclsDTOInExGra in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGra)
                {
                    if (Convert.ToInt32(cboCalculationBasedGratuity.SelectedValue) == 2)
                    {
                        DetailsDataGridViewGratuity.RowCount = DetailsDataGridViewGratuity.RowCount + 1;
                        DetailsDataGridViewGratuity.Rows[iCounter].Cells["txtAddDedID"].Value = objclsDTOInExGra.AdditionDeductionID;
                        DetailsDataGridViewGratuity.Rows[iCounter].Cells["chkParticular"].Value = objclsDTOInExGra.Sel;
                        DetailsDataGridViewGratuity.Rows[iCounter].Cells["txtParticulars"].Value = objclsDTOInExGra.DescriptionStr;
                        iCounter = iCounter + 1;
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                iCounter = 0;
                grdGratuityTer.Rows.Clear();
                foreach (clsDTOSettlementPolicyDetailTer objclsDTOSettlementPolicyDetail in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailTer)
                {
                    grdGratuityTer.RowCount = grdGratuityTer.RowCount + 1;


                    grdGratuityTer.Rows[iCounter].Cells["ParameterIDTer"].Value = objclsDTOSettlementPolicyDetail.ParameterID;
                    grdGratuityTer.Rows[iCounter].Cells["ParameterIDTer"].Tag = objclsDTOSettlementPolicyDetail.ParameterID;
                    grdGratuityTer.Rows[iCounter].Cells["ParameterIDTer"].Value = grdGratuityTer.Rows[iCounter].Cells["ParameterIDTer"].FormattedValue;

                    grdGratuityTer.Rows[iCounter].Cells["NoOfMonthsTer"].Value = objclsDTOSettlementPolicyDetail.NoOfMonths;
                    grdGratuityTer.Rows[iCounter].Cells["NoOfDaysTer"].Value = objclsDTOSettlementPolicyDetail.NoOfDays;
                    grdGratuityTer.Rows[iCounter].Cells["GratuityIDTer"].Value = 0;//MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID; 
                    iCounter = iCounter + 1;

                }


                iCounter = 0;
                grdGratuityFiveTer.Rows.Clear();
                foreach (clsDTOSettlementPolicyDetailFiveTer objclsDTOSettlementPolicyDetailFive in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFiveTer)
                {
                    grdGratuityFiveTer.RowCount = grdGratuityFiveTer.RowCount + 1;


                    grdGratuityFiveTer.Rows[iCounter].Cells["ParameterIDFiveTer"].Value = objclsDTOSettlementPolicyDetailFive.ParameterID;
                    grdGratuityFiveTer.Rows[iCounter].Cells["ParameterIDFiveTer"].Tag = objclsDTOSettlementPolicyDetailFive.ParameterID;
                    grdGratuityFiveTer.Rows[iCounter].Cells["ParameterIDFiveTer"].Value = grdGratuityFiveTer.Rows[iCounter].Cells["ParameterIDFiveTer"].FormattedValue;

                    grdGratuityFiveTer.Rows[iCounter].Cells["NoOfMonthsFiveTer"].Value = objclsDTOSettlementPolicyDetailFive.NoOfMonths;
                    grdGratuityFiveTer.Rows[iCounter].Cells["NoOfDaysFiveTer"].Value = objclsDTOSettlementPolicyDetailFive.NoOfDays;
                    grdGratuityFiveTer.Rows[iCounter].Cells["GratuityIDFiveTer"].Value = 0;//MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID; 
                    iCounter = iCounter + 1;

                }

                iCounter = 0;
                DetailsDataGridViewGratuityTer.Rows.Clear();
                foreach (clsDTOInExGraTer objclsDTOInExGra in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGraTer)
                {
                    if (Convert.ToInt32(cboCalculationBasedGratuityTer.SelectedValue) == 2)
                    {
                        DetailsDataGridViewGratuityTer.RowCount = DetailsDataGridViewGratuityTer.RowCount + 1;
                        DetailsDataGridViewGratuityTer.Rows[iCounter].Cells["txtAddDedIDTer"].Value = objclsDTOInExGra.AdditionDeductionID;
                        DetailsDataGridViewGratuityTer.Rows[iCounter].Cells["chkParticularTer"].Value = objclsDTOInExGra.Sel;
                        DetailsDataGridViewGratuityTer.Rows[iCounter].Cells["txtParticularsTer"].Value = objclsDTOInExGra.DescriptionStr;
                        iCounter = iCounter + 1;
                    }
                }


                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                iCounter = 0;
                grdGratuityPro.Rows.Clear();
                foreach (clsDTOSettlementPolicyDetailPro objclsDTOSettlementPolicyDetail in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailPro)
                {
                    grdGratuityPro.RowCount = grdGratuityPro.RowCount + 1;


                    grdGratuityPro.Rows[iCounter].Cells["ParameterIDPro"].Value = objclsDTOSettlementPolicyDetail.ParameterID;
                    grdGratuityPro.Rows[iCounter].Cells["ParameterIDPro"].Tag = objclsDTOSettlementPolicyDetail.ParameterID;
                    grdGratuityPro.Rows[iCounter].Cells["ParameterIDPro"].Value = grdGratuityPro.Rows[iCounter].Cells["ParameterIDPro"].FormattedValue;

                    grdGratuityPro.Rows[iCounter].Cells["NoOfMonthsPro"].Value = objclsDTOSettlementPolicyDetail.NoOfMonths;
                    grdGratuityPro.Rows[iCounter].Cells["NoOfDaysPro"].Value = objclsDTOSettlementPolicyDetail.NoOfDays;
                    grdGratuityPro.Rows[iCounter].Cells["GratuityIDPro"].Value = 0;//MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID; 
                    iCounter = iCounter + 1;

                }


                iCounter = 0;
                grdGratuityFivePro.Rows.Clear();
                foreach (clsDTOSettlementPolicyDetailFivePro objclsDTOSettlementPolicyDetailFive in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFivePro)
                {
                    grdGratuityFivePro.RowCount = grdGratuityFivePro.RowCount + 1;
                    

                    grdGratuityFivePro.Rows[iCounter].Cells["ParameterIDFivePro"].Value = objclsDTOSettlementPolicyDetailFive.ParameterID;
                    grdGratuityFivePro.Rows[iCounter].Cells["ParameterIDFivePro"].Tag = objclsDTOSettlementPolicyDetailFive.ParameterID;
                    grdGratuityFivePro.Rows[iCounter].Cells["ParameterIDFivePro"].Value = grdGratuityFivePro.Rows[iCounter].Cells["ParameterIDFivePro"].FormattedValue;

                    grdGratuityFivePro.Rows[iCounter].Cells["NoOfMonthsFivePro"].Value = objclsDTOSettlementPolicyDetailFive.NoOfMonths;
                    grdGratuityFivePro.Rows[iCounter].Cells["NoOfDaysFivePro"].Value = objclsDTOSettlementPolicyDetailFive.NoOfDays;
                    grdGratuityFivePro.Rows[iCounter].Cells["GratuityIDFivePro"].Value = 0;//MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID; 
                    iCounter = iCounter + 1;

                }

                iCounter = 0;
                DetailsDataGridViewGratuityPro.Rows.Clear();
                foreach (clsDTOInExGraPro objclsDTOInExGra in MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGraPro)
                {
                    if (Convert.ToInt32(cboCalculationBasedGratuityPro.SelectedValue) == 2)
                    {
                        DetailsDataGridViewGratuityPro.RowCount = DetailsDataGridViewGratuityPro.RowCount + 1;
                        DetailsDataGridViewGratuityPro.Rows[iCounter].Cells["txtAddDedIDPro"].Value = objclsDTOInExGra.AdditionDeductionID;
                        DetailsDataGridViewGratuityPro.Rows[iCounter].Cells["chkParticularPro"].Value = objclsDTOInExGra.Sel;
                        DetailsDataGridViewGratuityPro.Rows[iCounter].Cells["txtParticularsPro"].Value = objclsDTOInExGra.DescriptionStr;
                        iCounter = iCounter + 1;
                    }
                }






                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                iCounter = 0;

                BtnCancel.Enabled = false;




            }
            catch (Exception e)
            {

            }
        }

        private void DetailsDataGridViewGratuity_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }

        private void grdGratuity_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }



        }

        private void dgvExcludeEncash_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }


        private void LoadCombos(int intType)
        {


            DataTable datCombos = new DataTable();

            if (intType == 0 || intType == 1)
            {
                datCombos = null;
                if (ClsCommonSettings.IsArabicView)
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "CalculationID, CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                else
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "CalculationID, Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                cboCalculationBasedGratuity.ValueMember = "CalculationID";
                cboCalculationBasedGratuity.DisplayMember = "Calculation";
                cboCalculationBasedGratuity.DataSource = datCombos;
            }
            if (intType == 0 || intType == 2)
            {
                datCombos = null;
                if (ClsCommonSettings.IsArabicView)
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                else
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                ParameterID.ValueMember = "ParameterId";
                ParameterID.DisplayMember = "ParameterName";
                ParameterID.DataSource = datCombos;

                ParameterIDFive.ValueMember = "ParameterId";
                ParameterIDFive.DisplayMember = "ParameterName";
                ParameterIDFive.DataSource = datCombos;
            }
            if (intType == 0 || intType == 3)
            {
                datCombos = null;
                if (ClsCommonSettings.IsArabicView)
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "CalculationID, CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                else
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "CalculationID, Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                cboCalculationBasedGratuityTer.ValueMember = "CalculationID";
                cboCalculationBasedGratuityTer.DisplayMember = "Calculation";
                cboCalculationBasedGratuityTer.DataSource = datCombos;
            }
            if (intType == 0 || intType == 4)
            {
                datCombos = null;
                if (ClsCommonSettings.IsArabicView)
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                else
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                ParameterIDTer.ValueMember = "ParameterId";
                ParameterIDTer.DisplayMember = "ParameterName";
                ParameterIDTer.DataSource = datCombos;

                ParameterIDFiveTer.ValueMember = "ParameterId";
                ParameterIDFiveTer.DisplayMember = "ParameterName";
                ParameterIDFiveTer.DataSource = datCombos;
            }
            if (intType == 0 || intType == 5)
            {
                datCombos = null;
                if (ClsCommonSettings.IsArabicView)
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "CalculationID, CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                else
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "CalculationID, Calculation", "PayCalculationReference", "CalculationID in(1,2,3)", "CalculationID", "Calculation" });
                cboCalculationBasedGratuityPro.ValueMember = "CalculationID";
                cboCalculationBasedGratuityPro.DisplayMember = "Calculation";
                cboCalculationBasedGratuityPro.DataSource = datCombos;


               


            }
            if (intType == 0 || intType == 6)
            {
                datCombos = null;
                if (ClsCommonSettings.IsArabicView)
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                else
                    datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });

                ParameterIDPro.ValueMember = "ParameterId";
                ParameterIDPro.DisplayMember = "ParameterName";
                ParameterIDPro.DataSource = datCombos;


                ParameterIDFivePro.ValueMember = "ParameterId";
                ParameterIDFivePro.DisplayMember = "ParameterName";
                ParameterIDFivePro.DataSource = datCombos;
            }

            datCombos = null;




        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            MbAddStatus = true;
            grdGratuity.RowCount = 1;
            grdGratuityFive.RowCount = 1;
            grdGratuityTer.RowCount = 1;
            grdGratuityFiveTer.RowCount = 1;

            grdGratuityFivePro.RowCount = 1;
            grdGratuityPro.RowCount = 1;

            AddMode();
            BindingNavigatorAddNewItem.Enabled = false;
            RecCount();
            TabGrauity.SelectedIndex = 0;
            bnCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
            BindingNavigatorPositionItem.Text = RecordCnt.ToString();

            MbChangeStatus = false;
            OKSaveButton.Enabled = MbChangeStatus;
            BindingNavigatorDeleteItem.Enabled = false;
            BtnSave.Enabled = MbChangeStatus;
            this.BindingNavigatorSaveItem.Enabled = MbChangeStatus;
            this.BindingNavigatorDeleteItem.Enabled = MbChangeStatus;

            TmrSettlement.Stop();
            TmrSettlement.Start();
            tbSettlement.SelectTab(0);
            TabTermin.SelectTab(0); 
            BtnCancel.Enabled = true;
            txtPolicyName.Focus();
            txtPolicyName.Select();
        }

        private void RecCount()
        {
            RecordCnt = MobjclsBLLSettlementPolicy.RecCount();

            if (RecordCnt > 0)
            {
                bnCountItem.Text = strBindingOf + Convert.ToString(RecordCnt) + "";
                BindingNavigatorPositionItem.Text = Convert.ToString(RecordCnt);
                CurrentRecCnt = RecordCnt;
            }
            else
            {
                CurrentRecCnt = 0;
                bnCountItem.Text = strBindingOf + "0";
            }


            BindingNavigatorMoveNextItem.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = false;
            if (RecordCnt > 1)
            {
                BindingNavigatorMovePreviousItem.Enabled = true;
                BindingNavigatorMoveFirstItem.Enabled = true;
            }
            else
            {
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveFirstItem.Enabled = false;
            }


        }

        private void TmrSettlement_Tick(object sender, EventArgs e)
        {
            try
            {
                lblstatus.Text = "";
                TmrSettlement.Enabled = false;
            }
            catch
            {
            }
        }

        private bool FormValidation()
        {
            try
            {

                ErrSettlement.Clear();
                grdGratuity.CommitEdit(DataGridViewDataErrorContexts.Commit);
                grdGratuityFive.CommitEdit(DataGridViewDataErrorContexts.Commit);
                if (txtPolicyName.Text.Trim() == "")
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2212, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrSettlement.SetError(txtPolicyName, MsMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    tbSettlement.SelectTab(0);
                    txtPolicyName.Focus();
                    return false;
                }
                if (MobjclsBLLSettlementPolicy.CheckploicyNameDup(MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.SetPolicyID, txtPolicyName.Text.Trim()))
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9900, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrSettlement.SetError(txtPolicyName, MsMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    tbSettlement.SelectTab(0);
                    txtPolicyName.Focus();
                    return false;
                }

                if (cboCalculationBasedGratuity.SelectedIndex == -1)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2208, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrSettlement.SetError(cboCalculationBasedGratuity, MsMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    tbSettlement.SelectTab(0);
                    cboCalculationBasedGratuity.Focus();
                    return false;
                }


                grdGratuity.Focus();


                if (cboCalculationBasedGratuityTer.SelectedIndex == -1)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2208, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrSettlement.SetError(cboCalculationBasedGratuityTer, MsMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    tbSettlement.SelectTab(1);
                    cboCalculationBasedGratuityTer.Focus();
                    return false;
                }

                if (cboCalculationBasedGratuityPro.SelectedIndex == -1)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2208, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrSettlement.SetError(cboCalculationBasedGratuityPro, MsMessageCommon.Replace("#", "").Trim());
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    tbSettlement.SelectTab(2);
                    cboCalculationBasedGratuityPro.Focus();
                    return false;
                }


                if (cboCalculationBasedGratuity.SelectedIndex >= 0)
                {
                    if (grdGratuity.Rows.Count == 1)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2201, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TmrSettlement.Enabled = true;
                        tbSettlement.SelectTab(0);
                        grdGratuity.Focus();
                        return false;
                    }
                }



                //if (cboCalculationBasedGratuity.SelectedIndex >= 0 && chkRateOnlyGratuity.Checked == false)
                //{
                //    if (grdGratuityFive.Rows.Count == 1)
                //    {
                //        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2201, out MmessageIcon);
                //        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                //        TmrSettlement.Enabled = true;
                //        TabGrauity.SelectedIndex = 1;
                //        grdGratuityFive.Focus();
                //        return false;
                //    }
                //}


                int i;
                bool blnIsRepeat = false; ;
                bool blnIsRepeatExactly = false;
                for (i = 0; grdGratuity.RowCount - 2 >= i; ++i)
                {
                    //if (grdGratuity.Rows[i].Cells[0].Value==null  ||  grdGratuity.Rows[i].Cells[1].Value==null ||  grdGratuity.Rows[i].Cells[2].Value=null )
                    //  {
                    if (grdGratuity.Rows[i].Cells[0].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2202, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuity.CurrentCell = grdGratuity[0, i];
                        tbSettlement.SelectTab(0);
                        return false;
                    }
                    if (grdGratuity.Rows[i].Cells[1].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2213, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuity.CurrentCell = grdGratuity[1, i];
                        return false;
                    }
                    if (grdGratuity.Rows[i].Cells[2].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2214, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuity.CurrentCell = grdGratuity[2, i];
                        tbSettlement.SelectTab(0);
                        return false;
                    }
                    if (grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32() == 6)
                    {
                        blnIsRepeat = true;
                    }
                    if (grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32() == 7)
                    {
                        blnIsRepeatExactly = true;
                    }
                    for (int k = 0; grdGratuity.Rows.Count.ToInt32() - 2 >= k; ++k)
                    {
                        for (int j = k + 1; grdGratuity.Rows.Count.ToInt32() - 2 >= j; ++j)
                        {
                            if ((grdGratuity.Rows[k].Cells["ParameterID"].Tag.ToInt32() == grdGratuity.Rows[j].Cells["ParameterID"].Tag.ToInt32()) && (grdGratuity.Rows[k].Cells["NoOfMonths"].Value.ToDecimal() == grdGratuity.Rows[j].Cells["NoOfMonths"].Value.ToDecimal()) && (grdGratuity.Rows[k].Cells["NoOfDays"].Value.ToDecimal() == grdGratuity.Rows[j].Cells["NoOfDays"].Value.ToDecimal()))
                            {
                                grdGratuity.CurrentCell = grdGratuity.Rows[j].Cells["NoOfMonths"];
                                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15812, out MmessageIcon);
                                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                TmrSettlement.Enabled = true;
                                return false;

                            }
                        }
                    }
                  
                }

                ////++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
               blnIsRepeat = false; ;
               blnIsRepeatExactly = false;

                for (i = 0; grdGratuityTer.RowCount - 2 >= i; ++i)
                {

                    if (grdGratuityTer.Rows[i].Cells[0].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2202, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuityTer.CurrentCell = grdGratuityTer[0, i];
                        tbSettlement.SelectTab(1);
                        return false;
                    }
                    if (grdGratuityTer.Rows[i].Cells[1].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2213, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuityTer.CurrentCell = grdGratuityTer[1, i];
                        tbSettlement.SelectTab(1);
                        return false;
                    }
                    if (grdGratuityTer.Rows[i].Cells[2].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2214, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuityTer.CurrentCell = grdGratuityTer[2, i];
                        tbSettlement.SelectTab(1);
                        return false;
                    }
                    if (grdGratuityTer.Rows[i].Cells["ParameterIDTer"].Tag.ToInt32() == 6)
                    {
                        blnIsRepeat = true;
                    }
                    if (grdGratuityTer.Rows[i].Cells["ParameterIDTer"].Tag.ToInt32() == 7)
                    {
                        blnIsRepeatExactly = true;
                    }
                    for (int k = 0; grdGratuityTer.Rows.Count.ToInt32() - 2 >= k; ++k)
                    {
                        for (int j = k + 1; grdGratuityTer.Rows.Count.ToInt32() - 2 >= j; ++j)
                        {
                            if ((grdGratuityTer.Rows[k].Cells["ParameterIDTer"].Tag.ToInt32() == grdGratuityTer.Rows[j].Cells["ParameterIDTer"].Tag.ToInt32()) && (grdGratuityTer.Rows[k].Cells["NoOfMonthsTer"].Value.ToDecimal() == grdGratuityTer.Rows[j].Cells["NoOfMonthsTer"].Value.ToDecimal()) && (grdGratuityTer.Rows[k].Cells["NoOfDaysTer"].Value.ToDecimal() == grdGratuityTer.Rows[j].Cells["NoOfDaysTer"].Value.ToDecimal()))
                            {
                                grdGratuityTer.CurrentCell = grdGratuityTer.Rows[j].Cells["NoOfMonthsTer"];
                                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15812, out MmessageIcon);
                                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                tbSettlement.SelectTab(1);
                                TmrSettlement.Enabled = true;
                                return false;

                            }
                        }
                    }

                }

                blnIsRepeat = false; ;
                blnIsRepeatExactly = false;

                for (i = 0; grdGratuityPro.RowCount - 2 >= i; ++i)
                {

                    if (grdGratuityPro.Rows[i].Cells[0].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2202, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuityPro.CurrentCell = grdGratuityPro[0, i];
                        tbSettlement.SelectTab(2);
                        return false;
                    }
                    if (grdGratuityPro.Rows[i].Cells[1].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2213, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuityPro.CurrentCell = grdGratuityPro[1, i];
                        tbSettlement.SelectTab(2);
                        return false;
                    }
                    if (grdGratuityPro.Rows[i].Cells[2].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2214, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        grdGratuityPro.CurrentCell = grdGratuityPro[2, i];
                        tbSettlement.SelectTab(2);
                        return false;
                    }
                    if (grdGratuityPro.Rows[i].Cells["ParameterIDPro"].Tag.ToInt32() == 6)
                    {
                        blnIsRepeat = true;
                    }
                    if (grdGratuityPro.Rows[i].Cells["ParameterIDPro"].Tag.ToInt32() == 7)
                    {
                        blnIsRepeatExactly = true;
                    }
                    for (int k = 0; grdGratuityPro.Rows.Count.ToInt32() - 2 >= k; ++k)
                    {
                        for (int j = k + 1; grdGratuityPro.Rows.Count.ToInt32() - 2 >= j; ++j)
                        {
                            if ((grdGratuityPro.Rows[k].Cells["ParameterIDPro"].Tag.ToInt32() == grdGratuityPro.Rows[j].Cells["ParameterIDPro"].Tag.ToInt32()) && (grdGratuityPro.Rows[k].Cells["NoOfMonthsPro"].Value.ToDecimal() == grdGratuityPro.Rows[j].Cells["NoOfMonthsPro"].Value.ToDecimal()) && (grdGratuityPro.Rows[k].Cells["NoOfDaysPro"].Value.ToDecimal() == grdGratuityPro.Rows[j].Cells["NoOfDaysPro"].Value.ToDecimal()))
                            {
                                grdGratuityPro.CurrentCell = grdGratuityPro.Rows[j].Cells["NoOfMonthsPro"];
                                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15812, out MmessageIcon);
                                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                tbSettlement.SelectTab(2);
                                TmrSettlement.Enabled = true;
                                return false;

                            }
                        }
                    }

                }








                ////++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //----------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------
                for (i = 0; grdGratuityFive.RowCount - 2 >= i; ++i)
                {
                    if (grdGratuityFive.Rows[i].Cells[0].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2202, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TabGrauity.SelectedIndex = 1;
                        grdGratuityFive.CurrentCell = grdGratuityFive[0, i];
                        tbSettlement.SelectTab(0);
                        return false;
                    }
                    if (grdGratuityFive.Rows[i].Cells[1].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2213, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TabGrauity.SelectedIndex = 1;
                        grdGratuityFive.CurrentCell = grdGratuityFive[1, i];
                        return false;
                    }
                    if (grdGratuityFive.Rows[i].Cells[2].Value == null)
                    {
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2214, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TabGrauity.SelectedIndex = 1;
                        grdGratuityFive.CurrentCell = grdGratuityFive[2, i];
                        tbSettlement.SelectTab(0);
                        return false;
                    }
                    if (grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32() == 6)
                    {
                        blnIsRepeat = true;
                    }
                    if (grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32() == 7)
                    {
                        blnIsRepeatExactly = true;
                    }
                    for (int k = 0; grdGratuityFive.Rows.Count.ToInt32() - 2 >= k; ++k)
                    {
                        for (int j = k + 1; grdGratuityFive.Rows.Count.ToInt32() - 2 >= j; ++j)
                        {
                            if ((grdGratuityFive.Rows[k].Cells["ParameterIDFive"].Tag.ToInt32() == grdGratuityFive.Rows[j].Cells["ParameterIDFive"].Tag.ToInt32()) && (grdGratuityFive.Rows[k].Cells["NoOfMonthsFive"].Value.ToDecimal() == grdGratuityFive.Rows[j].Cells["NoOfMonthsFive"].Value.ToDecimal()) && (grdGratuityFive.Rows[k].Cells["NoOfDaysFive"].Value.ToDecimal() == grdGratuityFive.Rows[j].Cells["NoOfDaysFive"].Value.ToDecimal()))
                            {
                                grdGratuityFive.CurrentCell = grdGratuityFive.Rows[j].Cells["NoOfMonthsFive"];
                                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15812, out MmessageIcon);
                                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                TabGrauity.SelectedIndex = 1;
                                TmrSettlement.Enabled = true;
                                return false;

                            }
                        }
                    }

                }
                //----------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------
                if (blnIsRepeat)
                {
                    for (i = 0; grdGratuity.RowCount - 2 >= i; ++i)
                    {
                        if (grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32() != 6)
                        {
                            //MessageBox.Show("Please select all entries as Repeat or None");
                            grdGratuity.CurrentCell = grdGratuity.Rows[i].Cells["ParameterID"];
                            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15813, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TmrSettlement.Enabled = true;
                            return false;
                        }
                    }
                }
                if (blnIsRepeatExactly)
                {
                    for (i = 0; grdGratuity.RowCount - 2 >= i; ++i)
                    {
                        if (grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32() != 7)
                        {
                            //MessageBox.Show("Please select all entries as RepeatExactly or None");
                            grdGratuity.CurrentCell = grdGratuity.Rows[i].Cells["ParameterID"];
                            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15814, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TmrSettlement.Enabled = true;
                            return false;
                        }
                    }
                }

                //--------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------------------------------------------------------------------------
                if (blnIsRepeat)
                {
                    for (i = 0; grdGratuityFive.RowCount - 2 >= i; ++i)
                    {
                        if (grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32() != 6)
                        {
                            //MessageBox.Show("Please select all entries as Repeat or None");
                            grdGratuityFive.CurrentCell = grdGratuityFive.Rows[i].Cells["ParameterIDFive"];
                            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15813, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TabGrauity.SelectedIndex = 1;
                            TmrSettlement.Enabled = true;
                            return false;
                        }
                    }
                }
                if (blnIsRepeatExactly)
                {
                    for (i = 0; grdGratuityFive.RowCount - 2 >= i; ++i)
                    {
                        if (grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32() != 7)
                        {
                            //MessageBox.Show("Please select all entries as RepeatExactly or None");
                            grdGratuityFive.CurrentCell = grdGratuityFive.Rows[i].Cells["ParameterIDFive"];
                            MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 15814, out MmessageIcon);
                            MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TabGrauity.SelectedIndex = 1;
                            TmrSettlement.Enabled = true;
                            return false;
                        }
                    }
                }
                //--------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------------------------------------------------------------------------

                FillParameter();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }



        private void FillParameter()
        {
            try
            {
                //int iCounter = 0;


                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.DescriptionPolicy = txtPolicyName.Text.Trim();
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationID = Convert.ToInt32(cboCalculationBasedGratuity.SelectedValue);
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.blnIsIncludeLeave = chkIncludeLeave.Checked;
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationIDTer = Convert.ToInt32(cboCalculationBasedGratuityTer.SelectedValue);
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.blnIsIncludeLeaveTer = chkIncludeLeaveTer.Checked;
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CalculationIDPro = Convert.ToInt32(cboCalculationBasedGratuityPro.SelectedValue);
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.blnIsIncludeLeavePro = chkIncludeLeavePro.Checked;
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CompanyBasedN=rdbCompanyBasedN.Checked.ToBoolean();  
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CompanyBasedT=rdbCompanyBasedT.Checked.ToBoolean();
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.CompanyBasedP = rdbCompanyBasedPro.Checked.ToBoolean(); 


                if (MbAddStatus == false)
                {
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.ModeAddEdit = 2;
                }
                else
                {
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.ModeAddEdit = 1;
                }


                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetail = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetail>();

                for (int i = 0; Convert.ToInt32(grdGratuity.Rows.Count) - 2 >= i; ++i)
                {
                    clsDTOSettlementPolicyDetail objclsDTOSettlementPolicyDetail = new clsDTOSettlementPolicyDetail();
                    objclsDTOSettlementPolicyDetail.ParameterID = grdGratuity.Rows[i].Cells["ParameterID"].Tag.ToInt32();
                    if (Convert.ToInt32(objclsDTOSettlementPolicyDetail.ParameterID) == 0)
                    {
                        continue;
                    }
                    objclsDTOSettlementPolicyDetail.NoOfMonths = Convert.ToInt32(grdGratuity.Rows[i].Cells["NoOfMonths"].Value);
                    objclsDTOSettlementPolicyDetail.NoOfDays = Convert.ToDecimal(grdGratuity.Rows[i].Cells["NoOfDays"].Value);
                    //objclsDTOSettlementPolicyDetail.decTerminationDays = grdGratuity.Rows[i].Cells["TerminationDays"].Value.ToDecimal();
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetail.Add(objclsDTOSettlementPolicyDetail);
                }

                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFive = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetailFive>();


                for (int i = 0; Convert.ToInt32(grdGratuityFive.Rows.Count) - 2 >= i; ++i)
                {
                    clsDTOSettlementPolicyDetailFive objclsDTOSettlementPolicyDetailFive = new clsDTOSettlementPolicyDetailFive();
                    objclsDTOSettlementPolicyDetailFive.ParameterID = grdGratuityFive.Rows[i].Cells["ParameterIDFive"].Tag.ToInt32();
                    if (Convert.ToInt32(objclsDTOSettlementPolicyDetailFive.ParameterID) == 0)
                    {
                        continue;
                    }
                    objclsDTOSettlementPolicyDetailFive.NoOfMonths = Convert.ToInt32(grdGratuityFive.Rows[i].Cells["NoOfMonthsFive"].Value);
                    objclsDTOSettlementPolicyDetailFive.NoOfDays = Convert.ToDecimal(grdGratuityFive.Rows[i].Cells["NoOfDaysFive"].Value);
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFive.Add(objclsDTOSettlementPolicyDetailFive);
                }

       

                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGra = new System.Collections.Generic.List<DTO.clsDTOInExGra>();


                for (int i = 0; Convert.ToInt32(DetailsDataGridViewGratuity.Rows.Count) - 1 >= i; ++i)
                {
                    clsDTOInExGra objclsDTOInExGra = new clsDTOInExGra();
                    objclsDTOInExGra.AdditionDeductionID = Convert.ToInt32(DetailsDataGridViewGratuity.Rows[i].Cells["txtAddDedID"].Value);
                    objclsDTOInExGra.Sel = Convert.ToBoolean(DetailsDataGridViewGratuity.Rows[i].Cells["chkParticular"].Value);
                    objclsDTOInExGra.DescriptionStr = Convert.ToString(DetailsDataGridViewGratuity.Rows[i].Cells["txtParticulars"].Value);
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGra.Add(objclsDTOInExGra);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailTer = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetailTer>();

                for (int i = 0; Convert.ToInt32(grdGratuityTer.Rows.Count) - 2 >= i; ++i)
                {
                    clsDTOSettlementPolicyDetailTer objclsDTOSettlementPolicyDetail = new clsDTOSettlementPolicyDetailTer();
                    objclsDTOSettlementPolicyDetail.ParameterID = grdGratuityTer.Rows[i].Cells["ParameterIDTer"].Tag.ToInt32();
                    if (Convert.ToInt32(objclsDTOSettlementPolicyDetail.ParameterID) == 0)
                    {
                        continue;
                    }
                    objclsDTOSettlementPolicyDetail.NoOfMonths = Convert.ToInt32(grdGratuityTer.Rows[i].Cells["NoOfMonthsTer"].Value);
                    objclsDTOSettlementPolicyDetail.NoOfDays = Convert.ToDecimal(grdGratuityTer.Rows[i].Cells["NoOfDaysTer"].Value);
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailTer.Add(objclsDTOSettlementPolicyDetail);
                }

                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFiveTer = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetailFiveTer>();


                for (int i = 0; Convert.ToInt32(grdGratuityFiveTer.Rows.Count) - 2 >= i; ++i)
                {
                    clsDTOSettlementPolicyDetailFiveTer objclsDTOSettlementPolicyDetailFive = new clsDTOSettlementPolicyDetailFiveTer();
                    objclsDTOSettlementPolicyDetailFive.ParameterID = grdGratuityFiveTer.Rows[i].Cells["ParameterIDFiveTer"].Tag.ToInt32();
                    if (Convert.ToInt32(objclsDTOSettlementPolicyDetailFive.ParameterID) == 0)
                    {
                        continue;
                    }
                    objclsDTOSettlementPolicyDetailFive.NoOfMonths = Convert.ToInt32(grdGratuityFiveTer.Rows[i].Cells["NoOfMonthsFiveTer"].Value);
                    objclsDTOSettlementPolicyDetailFive.NoOfDays = Convert.ToDecimal(grdGratuityFiveTer.Rows[i].Cells["NoOfDaysFiveTer"].Value);
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFiveTer.Add(objclsDTOSettlementPolicyDetailFive);
                }



                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGraTer = new System.Collections.Generic.List<DTO.clsDTOInExGraTer>();


                for (int i = 0; Convert.ToInt32(DetailsDataGridViewGratuityTer.Rows.Count) - 1 >= i; ++i)
                {
                    clsDTOInExGraTer objclsDTOInExGra = new clsDTOInExGraTer();
                    objclsDTOInExGra.AdditionDeductionID = Convert.ToInt32(DetailsDataGridViewGratuityTer.Rows[i].Cells["txtAddDedIDTer"].Value);
                    objclsDTOInExGra.Sel = Convert.ToBoolean(DetailsDataGridViewGratuityTer.Rows[i].Cells["chkParticularTer"].Value);
                    objclsDTOInExGra.DescriptionStr = Convert.ToString(DetailsDataGridViewGratuityTer.Rows[i].Cells["txtParticularsTer"].Value);
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGraTer.Add(objclsDTOInExGra);
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailPro = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetailPro>();

                for (int i = 0; Convert.ToInt32(grdGratuityPro.Rows.Count) - 2 >= i; ++i)
                {
                    clsDTOSettlementPolicyDetailPro objclsDTOSettlementPolicyDetail = new clsDTOSettlementPolicyDetailPro();
                    objclsDTOSettlementPolicyDetail.ParameterID = grdGratuityPro.Rows[i].Cells["ParameterIDPro"].Tag.ToInt32();
                    if (Convert.ToInt32(objclsDTOSettlementPolicyDetail.ParameterID) == 0)
                    {
                        continue;
                    }
                    objclsDTOSettlementPolicyDetail.NoOfMonths = Convert.ToInt32(grdGratuityPro.Rows[i].Cells["NoOfMonthsPro"].Value);
                    objclsDTOSettlementPolicyDetail.NoOfDays = Convert.ToDecimal(grdGratuityPro.Rows[i].Cells["NoOfDaysPro"].Value);
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailPro.Add(objclsDTOSettlementPolicyDetail);
                }

                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFivePro = new System.Collections.Generic.List<DTO.clsDTOSettlementPolicyDetailFivePro>();


                for (int i = 0; Convert.ToInt32(grdGratuityFivePro.Rows.Count) - 2 >= i; ++i)
                {
                    clsDTOSettlementPolicyDetailFivePro objclsDTOSettlementPolicyDetailFive = new clsDTOSettlementPolicyDetailFivePro();
                    objclsDTOSettlementPolicyDetailFive.ParameterID = grdGratuityFivePro.Rows[i].Cells["ParameterIDFivePro"].Tag.ToInt32();
                    if (Convert.ToInt32(objclsDTOSettlementPolicyDetailFive.ParameterID) == 0)
                    {
                        continue;
                    }
                    objclsDTOSettlementPolicyDetailFive.NoOfMonths = Convert.ToInt32(grdGratuityFivePro.Rows[i].Cells["NoOfMonthsFivePro"].Value);
                    objclsDTOSettlementPolicyDetailFive.NoOfDays = Convert.ToDecimal(grdGratuityFivePro.Rows[i].Cells["NoOfDaysFivePro"].Value);
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOSettlementPolicyDetailFivePro.Add(objclsDTOSettlementPolicyDetailFive);
                }



                MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGraPro = new System.Collections.Generic.List<DTO.clsDTOInExGraPro>();


                for (int i = 0; Convert.ToInt32(DetailsDataGridViewGratuityPro.Rows.Count) - 1 >= i; ++i)
                {
                    clsDTOInExGraPro objclsDTOInExGra = new clsDTOInExGraPro();
                    objclsDTOInExGra.AdditionDeductionID = Convert.ToInt32(DetailsDataGridViewGratuityPro.Rows[i].Cells["txtAddDedIDPro"].Value);
                    objclsDTOInExGra.Sel = Convert.ToBoolean(DetailsDataGridViewGratuityPro.Rows[i].Cells["chkParticularPro"].Value);
                    objclsDTOInExGra.DescriptionStr = Convert.ToString(DetailsDataGridViewGratuityPro.Rows[i].Cells["txtParticularsPro"].Value);
                    MobjclsBLLSettlementPolicy.clsDTOSettlementPolicy.lstclsDTOInExGraPro.Add(objclsDTOInExGra);
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            }
            catch (Exception e)
            {

            }
        }



    

       
        private void cboCalculationBasedGratuity_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCalculationBasedGratuity.DroppedDown = false;
        }

      


        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]'\"";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }
        void txtInt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}.{:?></,`-=\\[]'\"";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0))
            {
                e.Handled = true;
            }
        }

        void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }





        private void DetailsDataGridViewGratuity_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DetailsDataGridViewGratuity.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch
            {

            }
        }

     
        private void grdGratuity_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

            try
            {

                DataTable datCombos = new DataTable();
                ChangeStatus(sender, e);
                if (e.RowIndex >= 0)
                {
                    iRowIndex = grdGratuity.CurrentRow.Index;
                    if (grdGratuity.RowCount >= 1)
                    {
                        if (e.ColumnIndex == 0)
                        {


                            datCombos = null;
                            if (ClsCommonSettings.IsArabicView)
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            else
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            ParameterID.ValueMember = "ParameterId";
                            ParameterID.DisplayMember = "ParameterName";
                            ParameterID.DataSource = datCombos;
                            if (e.RowIndex > 0)
                            {
                                if (grdGratuity.Rows[0].Cells["ParameterID"].Value.ToInt32().ToString() == "6")
                                {
                                    grdGratuity.CurrentCell = grdGratuity[0, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }

                            //TempParamID = Convert.ToInt32(grdGratuity.Rows[0].Cells["ParameterID"].Value);
                        }
                        if (e.ColumnIndex == 1)
                        {
                            if (grdGratuity.CurrentRow.Cells[0].Value == null)
                            {
                                grdGratuity.CurrentCell = grdGratuity[0, iRowIndex];
                                e.Cancel = true;
                            }
                        }

                        if (e.ColumnIndex == 2)
                        {
                            if (grdGratuity.CurrentRow.Cells[1].Value == null)
                            {
                                grdGratuity.CurrentCell = grdGratuity[1, iRowIndex];
                                e.Cancel = true;
                            }
                        }
                    }
                }

            }
            catch
            {
            }

        }

        private void grdGratuity_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (grdGratuity.CurrentCell != null)
                {
                    if (grdGratuity.CurrentCell.ColumnIndex == ParameterID.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (grdGratuity.CurrentCell.ColumnIndex == NoOfMonths.Index || grdGratuity.CurrentCell.ColumnIndex == NoOfDays.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
                this.grdGratuity.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlGratuity_KeyPress);

            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }


        void EditingControlGratuity_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (grdGratuity.CurrentCell.ColumnIndex != null)
                {
                    if (grdGratuity.CurrentCell.ColumnIndex == NoOfMonths.Index)
                    {
                        if (!((Char.IsDigit(e.KeyChar)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,backspace(08)
                        {
                            e.Handled = true;
                        }
                    }
                }
            }
            catch
            {
            }

        }


        private void DetailsDataGridViewGratuity_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void dgvExcludeEncash_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                int ExistFlg = 0;

                ExistFlg = MobjclsBLLSettlementPolicy.ExistsCompanySettlementPolicy();
                if (ExistFlg == 1)
                {
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2204, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                    return;
                }

                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon);
                lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MobjclsBLLSettlementPolicy.DeleteCompanySettlementPolicy();
                    BtnCancel_Click(sender, e);
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblstatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrSettlement.Enabled = true;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {

            BindingNavigatorAddNewItem_Click(sender, e);
        }

        private void cboCalculationBasedGratuity_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                OKSaveButton.Enabled = true;
                BtnSave.Enabled = true;
                if (Convert.ToInt32(cboCalculationBasedGratuity.SelectedValue) == 2)
                {
                    LoadAdditionsList(1);
                    DetailsDataGridViewGratuity.Enabled = true;
                }
                else
                {
                    LoadAdditionsList(1);
                    DetailsDataGridViewGratuity.Enabled = false;
                    DetailsDataGridViewGratuity.Rows.Clear();
                    DetailsDataGridViewGratuity.RowCount = 1;
                }

                ChangeStatus(sender, e);
            }
            catch (Exception Ex)
            {
            }



        }
        private void LoadAdditionsList(int iMode)
        {
            try
            {
                if (iMode == 0)
                {
                    DetailsDataGridViewGratuity.Rows.Clear();
                    DataTable DtablePolicy;
                    DtablePolicy = MobjclsBLLSettlementPolicy.FillAdddetailAddMode();
                    if (DtablePolicy.Rows.Count > 0)
                    {
                        DetailsDataGridViewGratuity.RowCount = 0;
                        for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
                        {
                            DetailsDataGridViewGratuity.RowCount = DetailsDataGridViewGratuity.RowCount + 1;
                            DetailsDataGridViewGratuity.Rows[i].Cells["txtAddDedID"].Value = Convert.ToInt32(DtablePolicy.Rows[i]["AddDedID"]);
                            DetailsDataGridViewGratuity.Rows[i].Cells["txtParticulars"].Value = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                            DetailsDataGridViewGratuity.Rows[i].Cells["chkParticular"].Value = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                        }

                    }
                    else
                    {
                        DetailsDataGridViewGratuity.RowCount = 1;
                    }
                }
                else if (iMode == 1)
                {
                    DetailsDataGridViewGratuity.Rows.Clear();
                    DataTable DtablePolicy;
                    DtablePolicy = MobjclsBLLSettlementPolicy.FillAdddetailAddMode();
                    if (DtablePolicy.Rows.Count > 0)
                    {
                        DetailsDataGridViewGratuity.RowCount = 0;
                        for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
                        {
                            DetailsDataGridViewGratuity.RowCount = DetailsDataGridViewGratuity.RowCount + 1;
                            DetailsDataGridViewGratuity.Rows[i].Cells["txtAddDedID"].Value = Convert.ToInt32(DtablePolicy.Rows[i]["AddDedID"]);
                            DetailsDataGridViewGratuity.Rows[i].Cells["txtParticulars"].Value = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                            DetailsDataGridViewGratuity.Rows[i].Cells["chkParticular"].Value = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                        }
                    }
                    else
                    {
                        DetailsDataGridViewGratuity.RowCount = 1;
                    }
                }
                //----------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------
               
                if (iMode == 2)
                {
                    DetailsDataGridViewGratuityTer.Rows.Clear();
                    DataTable DtablePolicy;
                    DtablePolicy = MobjclsBLLSettlementPolicy.FillAdddetailAddMode();
                    if (DtablePolicy.Rows.Count > 0)
                    {
                        DetailsDataGridViewGratuityTer.RowCount = 0;
                        for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
                        {
                            DetailsDataGridViewGratuityTer.RowCount = DetailsDataGridViewGratuityTer.RowCount + 1;
                            DetailsDataGridViewGratuityTer.Rows[i].Cells["txtAddDedIDTer"].Value = Convert.ToInt32(DtablePolicy.Rows[i]["AddDedID"]);
                            DetailsDataGridViewGratuityTer.Rows[i].Cells["txtParticularsTer"].Value = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                            DetailsDataGridViewGratuityTer.Rows[i].Cells["chkParticularTer"].Value = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                        }
                    }
                    else
                    {
                        DetailsDataGridViewGratuityTer.RowCount = 1;
                    }
                }

                //----------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------------------------------
                if (iMode == 3)
                {
                    DetailsDataGridViewGratuityPro.Rows.Clear();
                    DataTable DtablePolicy;
                    DtablePolicy = MobjclsBLLSettlementPolicy.FillAdddetailAddMode();
                    if (DtablePolicy.Rows.Count > 0)
                    {
                        DetailsDataGridViewGratuityPro.RowCount = 0;
                        for (int i = 0; DtablePolicy.Rows.Count - 1 >= i; ++i)
                        {
                            DetailsDataGridViewGratuityPro.RowCount = DetailsDataGridViewGratuityPro.RowCount + 1;
                            DetailsDataGridViewGratuityPro.Rows[i].Cells["txtAddDedIDPro"].Value = Convert.ToInt32(DtablePolicy.Rows[i]["AddDedID"]);
                            DetailsDataGridViewGratuityPro.Rows[i].Cells["txtParticularsPro"].Value = Convert.ToString(DtablePolicy.Rows[i]["DescriptionStr"]);
                            DetailsDataGridViewGratuityPro.Rows[i].Cells["chkParticularPro"].Value = Convert.ToBoolean(DtablePolicy.Rows[i]["Sel"]);
                        }
                    }
                    else
                    {
                        DetailsDataGridViewGratuityPro.RowCount = 1;
                    }

                }



            }
            catch (Exception ex)
            {
            }
        }



        private void BtnBottomCancel_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void OKSaveButton_Click(object sender, EventArgs e)
        {

            CompanySettlementPolicyBindingNavigatorSaveItem_Click(sender, e);
            if (!BtnSave.Enabled)
            {
                this.Close();
            }


        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            CompanySettlementPolicyBindingNavigatorSaveItem_Click(sender, e);
        }

        private void DetailsDataGridViewGratuity_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (DetailsDataGridViewGratuity.IsCurrentCellDirty)
                {

                    DetailsDataGridViewGratuity.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        private void FrmSettlementPolicy_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnSave.Enabled)
            {

                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 8, out MmessageIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void txtRateGratuity_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
        }

        private void bnHelp_Click(object sender, EventArgs e)
        {
            try
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "SettlementPolicy";
                objHelp.ShowDialog();
                objHelp = null;
            }
            catch
            {


            }
        }

        private void FrmSettlementPolicy_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        bnHelp_Click(null, null);
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled)
                            BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled)
                            BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (BtnCancel.Enabled)
                            BtnCancel_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (BindingNavigatorMovePreviousItem.Enabled)
                            BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Prev item
                        break;
                    case Keys.Control | Keys.Right:
                        if (BindingNavigatorMoveNextItem.Enabled)
                            BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Next item
                        break;
                    case Keys.Control | Keys.Up:
                        if (BindingNavigatorMoveFirstItem.Enabled)
                            BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//First item
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveFirstItem.Enabled)
                            BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//Last item
                        break;

                    //case Keys.Control | Keys.M:
                    //    BtnEmail_Click(sender, new EventArgs());//Cancel
                    //    break;
                }
            }
            catch
            {
            }
        }

        private void grdGratuity_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    grdGratuity.EndEdit();
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (e.ColumnIndex == ParameterID.Index)
                    {

                        grdGratuity.CurrentRow.Cells["ParameterID"].Tag = grdGratuity.CurrentRow.Cells["ParameterID"].Value;
                        grdGratuity.CurrentRow.Cells["ParameterID"].Value = grdGratuity.CurrentRow.Cells["ParameterID"].FormattedValue;

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void grdGratuity_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //LoadParameter();
        }
        public void LoadParameter()
        {

            DataTable datCombos;
            int IntParameterID = 0;

            if (MobjclsBLLSettlementPolicy == null)
                MobjclsBLLSettlementPolicy = new clsBLLSettlementPolicy();

            grdGratuity.CommitEdit(DataGridViewDataErrorContexts.Commit);

            if (grdGratuity.Rows.Count > 1)
            {
                IntParameterID = grdGratuity.Rows[0].Cells[0].Tag.ToInt32();

                // Modified by Laxmi

                int GridCount = grdGratuity.Rows.Count - 2;

                if (IntParameterID == 6 || IntParameterID == 7)
                {

                    for (int i = GridCount; i >= 1; i--)
                    {
                        grdGratuity.Rows.RemoveAt(i);
                    }
                }

                if (IntParameterID < 6)
                {
                    //grdGratuity.Rows[1].ReadOnly = false;
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                    else
                        datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                }
                else if (IntParameterID == 6)
                {

                    //grdGratuity.Rows[1].ReadOnly = true;
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                    else
                        datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                }
                else
                {
                    //grdGratuity.Rows[1].ReadOnly = true;
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId ", "ParameterName" });
                    else
                        datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId ", "ParameterName" });
                }

                ParameterID.ValueMember = "ParameterId";
                ParameterID.DisplayMember = "ParameterName";
                ParameterID.DataSource = datCombos;
            }
        }

        private void grdGratuity_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            if (grdGratuity.Rows.Count == 1)
            {
                grdGratuity.Rows.Clear();
                //  LoadParameter();
            }
        }



        private void grdGratuityFive_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    grdGratuity.EndEdit();
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (e.ColumnIndex == ParameterID.Index)
                    {

                        grdGratuityFive.CurrentRow.Cells["ParameterIDFive"].Tag = grdGratuityFive.CurrentRow.Cells["ParameterIDFive"].Value;
                        grdGratuityFive.CurrentRow.Cells["ParameterIDFive"].Value = grdGratuityFive.CurrentRow.Cells["ParameterIDFive"].FormattedValue;

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void grdGratuityFive_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void grdGratuityFive_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {

                DataTable datCombos = new DataTable();
                ChangeStatus(sender, e);
                if (e.RowIndex >= 0)
                {
                    iRowIndex = grdGratuityFive.CurrentRow.Index;
                    if (grdGratuityFive.RowCount >= 1)
                    {
                        if (e.ColumnIndex == 0)
                        {
                            datCombos = null;
                            if (ClsCommonSettings.IsArabicView)
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            else
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            ParameterIDFive.ValueMember = "ParameterId";
                            ParameterIDFive.DisplayMember = "ParameterName";
                            ParameterIDFive.DataSource = datCombos;
                            if (e.RowIndex > 0)
                            {
                                if (grdGratuityFive.Rows[0].Cells["ParameterIDFive"].Value.ToInt32().ToString() == "6")
                                {
                                    grdGratuityFive.CurrentCell = grdGratuityFive[0, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }

                        }
                        if (e.ColumnIndex == 1)
                        {
                            if (grdGratuityFive.CurrentRow.Cells[0].Value == null)
                            {
                                grdGratuityFive.CurrentCell = grdGratuityFive[0, iRowIndex];
                                e.Cancel = true;
                            }
                        }

                        if (e.ColumnIndex == 2)
                        {
                            if (grdGratuityFive.CurrentRow.Cells[1].Value == null)
                            {
                                grdGratuityFive.CurrentCell = grdGratuityFive[1, iRowIndex];
                                e.Cancel = true;
                            }
                        }
                    }
                }

            }
            catch
            {
            }
        }

        private void grdGratuityFive_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (grdGratuityFive.CurrentCell != null)
                {
                    if (grdGratuityFive.CurrentCell.ColumnIndex == ParameterID.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (grdGratuityFive.CurrentCell.ColumnIndex == NoOfMonths.Index || grdGratuityFive.CurrentCell.ColumnIndex == NoOfDays.Index )
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
                this.grdGratuityFive.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlGratuity_KeyPress);

            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }

        private void cboCalculationBasedGratuityTer_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCalculationBasedGratuityTer.DroppedDown = false;
        }

        private void cboCalculationBasedGratuityTer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OKSaveButton.Enabled = true;
                BtnSave.Enabled = true;
                if (Convert.ToInt32(cboCalculationBasedGratuityTer.SelectedValue) == 2)
                {
                    LoadAdditionsList(2);
                    DetailsDataGridViewGratuityTer.Enabled = true;
                }
                else
                {
                    LoadAdditionsList(2);
                    DetailsDataGridViewGratuityTer.Enabled = false;
                    DetailsDataGridViewGratuityTer.Rows.Clear();
                    DetailsDataGridViewGratuityTer.RowCount = 1;
                }

                ChangeStatus(sender, e);
            }
            catch (Exception Ex)
            {
            }
        }

        private void grdGratuityTer_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void grdGratuityFiveTer_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void DetailsDataGridViewGratuityTer_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void grdGratuityTer_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (grdGratuityTer.CurrentCell != null)
                {
                    if (grdGratuityTer.CurrentCell.ColumnIndex == ParameterID.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (grdGratuityTer.CurrentCell.ColumnIndex == NoOfMonths.Index || grdGratuityTer.CurrentCell.ColumnIndex == NoOfDays.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
                this.grdGratuityTer.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlGratuity_KeyPress);

            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }

        }

        private void grdGratuityFiveTer_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (grdGratuityFiveTer.CurrentCell != null)
                {
                    if (grdGratuityFiveTer.CurrentCell.ColumnIndex == ParameterID.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (grdGratuityFiveTer.CurrentCell.ColumnIndex == NoOfMonths.Index || grdGratuityFiveTer.CurrentCell.ColumnIndex == NoOfDays.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
                this.grdGratuityFiveTer.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlGratuity_KeyPress);

            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }

        private void grdGratuityTer_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {

                DataTable datCombos = new DataTable();
                ChangeStatus(sender, e);
                if (e.RowIndex >= 0)
                {
                    iRowIndex = grdGratuityTer.CurrentRow.Index;
                    if (grdGratuityTer.RowCount >= 1)
                    {
                        if (e.ColumnIndex == 0)
                        {
                            datCombos = null;
                            if (ClsCommonSettings.IsArabicView)
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            else
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            ParameterIDTer.ValueMember = "ParameterId";
                            ParameterIDTer.DisplayMember = "ParameterName";
                            ParameterIDTer.DataSource = datCombos;
                            if (e.RowIndex > 0)
                            {
                                if (grdGratuityTer.Rows[0].Cells["ParameterIDTer"].Value.ToInt32().ToString() == "6")
                                {
                                    grdGratuityTer.CurrentCell = grdGratuityTer[0, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }

                        }
                        if (e.ColumnIndex == 1)
                        {
                            if (grdGratuityTer.CurrentRow.Cells[0].Value == null)
                            {
                                grdGratuityTer.CurrentCell = grdGratuityTer[0, iRowIndex];
                                e.Cancel = true;
                            }
                        }

                        if (e.ColumnIndex == 2)
                        {
                            if (grdGratuityTer.CurrentRow.Cells[1].Value == null)
                            {
                                grdGratuityTer.CurrentCell = grdGratuityTer[1, iRowIndex];
                                e.Cancel = true;
                            }
                        }
                    }
                }

            }
            catch
            {
            }
        }

        private void grdGratuityFiveTer_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

            try
            {

                DataTable datCombos = new DataTable();
                ChangeStatus(sender, e);
                if (e.RowIndex >= 0)
                {
                    iRowIndex = grdGratuityFiveTer.CurrentRow.Index;
                    if (grdGratuityFiveTer.RowCount >= 1)
                    {
                        if (e.ColumnIndex == 0)
                        {
                            datCombos = null;
                            if (ClsCommonSettings.IsArabicView)
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            else
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            ParameterIDFiveTer.ValueMember = "ParameterId";
                            ParameterIDFiveTer.DisplayMember = "ParameterName";
                            ParameterIDFiveTer.DataSource = datCombos;
                            if (e.RowIndex > 0)
                            {
                                if (grdGratuityFiveTer.Rows[0].Cells["ParameterIDFiveTer"].Value.ToInt32().ToString() == "6")
                                {
                                    grdGratuityFiveTer.CurrentCell = grdGratuityFiveTer[0, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }

                        }
                        if (e.ColumnIndex == 1)
                        {
                            if (grdGratuityFiveTer.CurrentRow.Cells[0].Value == null)
                            {
                                grdGratuityFiveTer.CurrentCell = grdGratuityFiveTer[0, iRowIndex];
                                e.Cancel = true;
                            }
                        }

                        if (e.ColumnIndex == 2)
                        {
                            if (grdGratuityFiveTer.CurrentRow.Cells[1].Value == null)
                            {
                                grdGratuityFiveTer.CurrentCell = grdGratuityFiveTer[1, iRowIndex];
                                e.Cancel = true;
                            }
                        }
                    }
                }

            }
            catch
            {
            }


        }

        private void grdGratuityFiveTer_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    grdGratuityFiveTer.EndEdit();
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (e.ColumnIndex == ParameterID.Index)
                    {

                        grdGratuityFiveTer.CurrentRow.Cells["ParameterIDFiveTer"].Tag = grdGratuityFiveTer.CurrentRow.Cells["ParameterIDFiveTer"].Value;
                        grdGratuityFiveTer.CurrentRow.Cells["ParameterIDFiveTer"].Value = grdGratuityFiveTer.CurrentRow.Cells["ParameterIDFiveTer"].FormattedValue;

                    }
                }
                ChangeStatus(sender, e);
            }
            catch (Exception ex)
            {

            }
        }

        private void grdGratuityTer_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    grdGratuityTer.EndEdit();
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (e.ColumnIndex == ParameterID.Index)
                    {

                        grdGratuityTer.CurrentRow.Cells["ParameterIDTer"].Tag = grdGratuityTer.CurrentRow.Cells["ParameterIDTer"].Value;
                        grdGratuityTer.CurrentRow.Cells["ParameterIDTer"].Value = grdGratuityTer.CurrentRow.Cells["ParameterIDTer"].FormattedValue;

                    }
                }
                ChangeStatus(sender, e);
            }
            catch (Exception ex)
            {

            }
        }

        private void chkIncludeLeaveTer_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void DetailsDataGridViewGratuityTer_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void chkIncludeLeavePro_CheckedChanged(object sender, EventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void cboCalculationBasedGratuityPro_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OKSaveButton.Enabled = true;
                BtnSave.Enabled = true;
                if (Convert.ToInt32(cboCalculationBasedGratuityPro.SelectedValue) == 2)
                {
                    LoadAdditionsList(3);
                    DetailsDataGridViewGratuityPro.Enabled = true;
                }
                else
                {
                    LoadAdditionsList(3);
                    DetailsDataGridViewGratuityPro.Enabled = false;
                    DetailsDataGridViewGratuityPro.Rows.Clear();
                    DetailsDataGridViewGratuityPro.RowCount = 1;
                }

                ChangeStatus(sender, e);
            }
            catch (Exception Ex)
            {
            }
        }

        private void DetailsDataGridViewGratuityPro_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void DetailsDataGridViewGratuityPro_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void grdGratuityPro_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (grdGratuityPro.CurrentCell != null)
                {
                    if (grdGratuityPro.CurrentCell.ColumnIndex == ParameterID.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (grdGratuityPro.CurrentCell.ColumnIndex == NoOfMonths.Index || grdGratuityPro.CurrentCell.ColumnIndex == NoOfDays.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
                this.grdGratuityPro.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlGratuity_KeyPress);

            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }

        private void grdGratuityPro_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {

                DataTable datCombos = new DataTable();
                ChangeStatus(sender, e);
                if (e.RowIndex >= 0)
                {
                    iRowIndex = grdGratuityPro.CurrentRow.Index;
                    if (grdGratuityPro.RowCount >= 1)
                    {
                        if (e.ColumnIndex == 0)
                        {
                            datCombos = null;
                            if (ClsCommonSettings.IsArabicView)
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            else
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            ParameterIDPro.ValueMember = "ParameterId";
                            ParameterIDPro.DisplayMember = "ParameterName";
                            ParameterIDPro.DataSource = datCombos;
                            if (e.RowIndex > 0)
                            {
                                if (grdGratuityPro.Rows[0].Cells["ParameterIDPro"].Value.ToInt32().ToString() == "6")
                                {
                                    grdGratuityPro.CurrentCell = grdGratuityTer[0, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }

                        }
                        if (e.ColumnIndex == 1)
                        {
                            if (grdGratuityPro.CurrentRow.Cells[0].Value == null)
                            {
                                grdGratuityPro.CurrentCell = grdGratuityPro[0, iRowIndex];
                                e.Cancel = true;
                            }
                        }

                        if (e.ColumnIndex == 2)
                        {
                            if (grdGratuityPro.CurrentRow.Cells[1].Value == null)
                            {
                                grdGratuityPro.CurrentCell = grdGratuityPro[1, iRowIndex];
                                e.Cancel = true;
                            }
                        }
                    }
                }

            }
            catch
            {
            }
        }

        private void grdGratuityPro_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    grdGratuityPro.EndEdit();
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (e.ColumnIndex == ParameterID.Index)
                    {

                        grdGratuityPro.CurrentRow.Cells["ParameterIDPro"].Tag = grdGratuityPro.CurrentRow.Cells["ParameterIDPro"].Value;
                        grdGratuityPro.CurrentRow.Cells["ParameterIDPro"].Value = grdGratuityPro.CurrentRow.Cells["ParameterIDPro"].FormattedValue;

                    }
                }
                ChangeStatus(sender, e);
            }
            catch (Exception ex)
            {

            }
        }

        private void grdGratuityFivePro_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (grdGratuityFivePro.CurrentCell != null)
                {
                    if (grdGratuityFivePro.CurrentCell.ColumnIndex == ParameterID.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    if (grdGratuityFivePro.CurrentCell.ColumnIndex == NoOfMonths.Index || grdGratuityFivePro.CurrentCell.ColumnIndex == NoOfDays.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
                this.grdGratuityFivePro.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlGratuity_KeyPress);

            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
            }
        }

        private void grdGratuityFivePro_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void grdGratuityFivePro_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {

                DataTable datCombos = new DataTable();
                ChangeStatus(sender, e);
                if (e.RowIndex >= 0)
                {
                    iRowIndex = grdGratuityFivePro.CurrentRow.Index;
                    if (grdGratuityFivePro.RowCount >= 1)
                    {
                        if (e.ColumnIndex == 0)
                        {
                            datCombos = null;
                            if (ClsCommonSettings.IsArabicView)
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterNameArb AS ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            else
                                datCombos = MobjclsBLLSettlementPolicy.FillCombos(new string[] { "ParameterId,ParameterName", "PayParameters", "", "ParameterId", "ParameterName" });
                            ParameterIDFivePro.ValueMember = "ParameterId";
                            ParameterIDFivePro.DisplayMember = "ParameterName";
                            ParameterIDFivePro.DataSource = datCombos;
                            if (e.RowIndex > 0)
                            {
                                if (grdGratuityFivePro.Rows[0].Cells["ParameterIDFivePro"].Value.ToInt32().ToString() == "6")
                                {
                                    grdGratuityFivePro.CurrentCell = grdGratuityFivePro[0, iRowIndex];
                                    e.Cancel = true;
                                    return;
                                }
                            }

                        }
                        if (e.ColumnIndex == 1)
                        {
                            if (grdGratuityFivePro.CurrentRow.Cells[0].Value == null)
                            {
                                grdGratuityFivePro.CurrentCell = grdGratuityFivePro[0, iRowIndex];
                                e.Cancel = true;
                            }
                        }

                        if (e.ColumnIndex == 2)
                        {
                            if (grdGratuityFivePro.CurrentRow.Cells[1].Value == null)
                            {
                                grdGratuityFivePro.CurrentCell = grdGratuityFivePro[1, iRowIndex];
                                e.Cancel = true;
                            }
                        }
                    }
                }

            }
            catch
            {
            }
        }

        private void grdGratuityFivePro_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    grdGratuityFivePro.EndEdit();
                    //------------------------------------------------------------UOM----------------------------------------------------------

                    if (e.ColumnIndex == ParameterID.Index)
                    {

                        grdGratuityFivePro.CurrentRow.Cells["ParameterIDFivePro"].Tag = grdGratuityFivePro.CurrentRow.Cells["ParameterIDFivePro"].Value;
                        grdGratuityFivePro.CurrentRow.Cells["ParameterIDFivePro"].Value = grdGratuityFivePro.CurrentRow.Cells["ParameterIDFivePro"].FormattedValue;

                    }
                }
                ChangeStatus(sender, e);
            }
            catch (Exception ex)
            {

            }
        }

        private void grdGratuityPro_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }

        private void cboCalculationBasedGratuityPro_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCalculationBasedGratuityPro.DroppedDown = false;
        }

        private void cboCalculationBasedGratuityPro_SelectedValueChanged(object sender, EventArgs e)
        {
           
        }

    }
}
