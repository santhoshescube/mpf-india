﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    /*****************************************************
   * Created By       : Arun
   * Creation Date    : 10 Apr 2012
   * Description      : Handle LeaveConsequence Policy
   * 
   * Modified By      : Ranju Mathew
   * Creation Date    : 12 aug 2013
   * Description      : Tuning and performance improving
   * ***************************************************/
    public partial class FrmLeaveConsequencePolicy : Form
    {

        #region Declartions
        public int PintLeaveConsequencePolicyID = 0;   // From Refernce Form 
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        private string MstrCommonMessage;   
        private bool MblnIsEditMode = false;  //  To Find Whether Is add mode or Edit Mode
        private int MintRecordCnt = 0;     //Total Record Count
        private int MintCurrentRecCnt = 0; // Current Record Count
        private bool MblnChangeStatus = false;
        private bool MblbtnOk = false;

        private clsBLLLeaveConsequencePolicy MobjclsBLLLeaveConsequencePolicy = null;
        private clsMessage ObjUserMessage = null;
        string strBindingOf = "Of ";
        #endregion Declartions
        /// <summary>
        /// FORM ID=112
        /// Constructor
        /// </summary>
        #region Constructor
        public FrmLeaveConsequencePolicy()  
        {
            InitializeComponent();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.LeavePolicyConsequence, this);

            strBindingOf = "من ";
            dgvLeaveConsequenceDetails.Columns["colLeaveConsequenceParticulars"].HeaderText = "تفاصيل";
        }
        #endregion Constructor

        #region Properties
        private clsMessage UserMessage  // For Notification Message
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.LeavePolicyConsequence);
                return this.ObjUserMessage;
            }
        }
        private clsBLLLeaveConsequencePolicy BLLLeaveConsequencePolicy
        {
            get
        {
            if (this.MobjclsBLLLeaveConsequencePolicy == null)
            this.MobjclsBLLLeaveConsequencePolicy = new clsBLLLeaveConsequencePolicy();

            return this.MobjclsBLLLeaveConsequencePolicy;
        }
        }
        #endregion Properties

        #region Methods

        private void SetPermissions()  // Function for setting permissions Add/Update/Delete/Email-Print
        {
           
        clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
        if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.LeaveConsequencePolicy, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
        else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private void ClearAllControls()   // Funtion to clear All Controls
        {
            this.txtPolicyName.Text = "";
            this.txtPolicyName.Tag = 0;

            this.txtPolicyName.Text = "";
            this.rdbCompanyBased.Checked = true;
            this.rdbActualMonth.Checked = false;
            //this.rdbWorkingDays.Checked = false;
            this.cboCalculationBased.SelectedIndex = -1;
            this.cboCalculationBased.Text = "";
            this.dgvLeaveConsequenceDetails.Rows.Clear();
            //this.chkRatePerDay.Checked = false;
            this.chkRateOnly.Checked = false;
            //this.txtHoursPerDay.Text = "";
            this.txtRate.Text = "";

            this.txtCalculationPercent.Text = "";

        }

        private void Changestatus(object sender, EventArgs e) //function for changing status
        {
            
        if (!MblnIsEditMode)
            {
                btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
        else
            {
                btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            errLeaveConsequencePolicy.Clear();
            MblnChangeStatus = true;

        }
        private bool LoadCombos(int intType) // For Loading the Combos 
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
        try
        {

            if (intType == 0 || intType == 1)//Calculation type
            {
                if (ClsCommonSettings.IsArabicView)
                    datCombos = BLLLeaveConsequencePolicy.FillCombos(new string[] { "CalculationID,CalculationArb AS Calculation", "PayCalculationReference", "CalculationID in(1,2,3)" });
                else
                    datCombos = BLLLeaveConsequencePolicy.FillCombos(new string[] { "CalculationID,Calculation", "PayCalculationReference", "CalculationID in(1,2,3)" });
                cboCalculationBased.ValueMember = "CalculationID";
                cboCalculationBased.DisplayMember = "Calculation";
                cboCalculationBased.DataSource = datCombos;
                blnRetvalue = true;
            }


        }
        catch (Exception Ex)
        {
            ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            blnRetvalue = false;
        }
            return blnRetvalue;
        }
        private void FillAdditionDeductionsGrid(DataGridView Dgv, DataTable dtAddDedDetails) // To fill AdditionDeduction Details in Grid
        {
            Dgv.Rows.Clear();
            if (dtAddDedDetails != null)
            {
                if (dtAddDedDetails.Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter <= dtAddDedDetails.Rows.Count - 1; iCounter++)
                    {
                        Dgv.RowCount = Dgv.RowCount + 1;
                        Dgv.Rows[iCounter].Cells[0].Value = dtAddDedDetails.Rows[iCounter]["AddDedID"].ToInt32();//Addition deduction Policy
                        Dgv.Rows[iCounter].Cells[1].Value = dtAddDedDetails.Rows[iCounter]["Checked"].ToBoolean(); ;//ChkBox
                        if (ClsCommonSettings.IsArabicView)
                            Dgv.Rows[iCounter].Cells[2].Value = dtAddDedDetails.Rows[iCounter]["DescriptionArb"].ToStringCustom();//Description
                        else
                            Dgv.Rows[iCounter].Cells[2].Value = dtAddDedDetails.Rows[iCounter]["Description"].ToStringCustom();//Description
                    }
                }
            }
        }

        private void GetRecordCount()   // TO Get Record Count
        {
            MintRecordCnt = 0;
            MintRecordCnt = BLLLeaveConsequencePolicy.GetRecordCount();
            if (MintRecordCnt < 0)
                {
                    BindingNavigatorCountItem.Text = strBindingOf + "0";
                    MintRecordCnt = 0;
                }
        }
        private void RefernceDisplay() // To Display Details In Edit Mode
        {

            int intRowNum = 0;
            intRowNum = BLLLeaveConsequencePolicy.GetRowNumber(PintLeaveConsequencePolicyID);
            if (intRowNum > 0)
            {
                MintCurrentRecCnt = intRowNum;
                DisplayLeaveConsequencePolicyInfo();
            }
        }
        private void AddNewLeaveConsequencePolicy()  // Add Mode
        {
            MblnChangeStatus = false;
            MblnIsEditMode = false;
            lblstatus.Text = "";
            tmrClear.Enabled = true;
            errLeaveConsequencePolicy.Clear();

            ClearAllControls();  //Clear all controls in the form
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();

            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            btnPrint.Enabled = false;
            btnEmail.Enabled = false;
            btnOk.Enabled = false;
            btnSave.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            //txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            //txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;
            btnClear.Enabled = true;

        }
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                {
                    BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
                }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
                }
        }
        private void DisplayLeaveConsequencePolicyInfo() //To Display Detail Info
        {

            FillLeaveConsequencePolicyInfo();

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnPrint.Enabled = MblnPrintEmailPermission;
            btnEmail.Enabled = MblnPrintEmailPermission;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            MblnIsEditMode = true;
            SetBindingNavigatorButtons();
            MblnChangeStatus = false;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            btnOk.Enabled = MblnChangeStatus;
            btnSave.Enabled = MblnChangeStatus;

            txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            txtRate.Enabled = chkRateOnly.Checked ? true : false;

            //txtHoursPerDay.BackColor = chkRatePerDay.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
            //txtHoursPerDay.Enabled = chkRatePerDay.Checked ? true : false;

            btnClear.Enabled = false;
        }
        private void FillLeaveConsequencePolicyInfo() // To fill LeaveConsequencePolicy
        {
            ClearAllControls();
            DataTable dtLeaveConsequencePolicy = BLLLeaveConsequencePolicy.DisplayLeaveConsequencePolicy(MintCurrentRecCnt);

            if (dtLeaveConsequencePolicy.Rows.Count > 0)
            {

                txtPolicyName.Text = dtLeaveConsequencePolicy.Rows[0]["LeaveConsequencePolicy"].ToStringCustom();
                txtPolicyName.Tag = dtLeaveConsequencePolicy.Rows[0]["LeaveConsequencePolicyID"].ToInt32();
           

                for (int iCounter = 0; iCounter <= dtLeaveConsequencePolicy.Rows.Count - 1; iCounter++)
                {



                        cboCalculationBased.SelectedValue = dtLeaveConsequencePolicy.Rows[iCounter]["CalculationID"].ToInt32();

                        if (dtLeaveConsequencePolicy.Rows[iCounter]["CompanyBasedOn"].ToInt32() == 1)
                        {
                            rdbCompanyBased.Checked = true;
                        }
                        else 
                        {
                            rdbActualMonth.Checked = true;
                        }
                        txtCalculationPercent.Text = dtLeaveConsequencePolicy.Rows[iCounter]["Percentage"].ToDecimal().ToStringCustom();
                        chkRateOnly.Checked = dtLeaveConsequencePolicy.Rows[iCounter]["IsRateOnly"].ToBoolean();
                        txtRate.Text = dtLeaveConsequencePolicy.Rows[iCounter]["LeaveConsequenceRate"].ToDecimal().ToStringCustom();

                        //chkRatePerDay.Checked = dtLeaveConsequencePolicy.Rows[iCounter]["IsRateBasedOnHour"].ToBoolean();
                        //txtHoursPerDay.Text = dtLeaveConsequencePolicy.Rows[iCounter]["HoursPerDay"].ToStringCustom();
                        if (dtLeaveConsequencePolicy.Rows[iCounter]["CalculationID"].ToInt32() == (int)CalculationType.GrossSalary)
                            {
                                DataTable DtAddDedRef = BLLLeaveConsequencePolicy.DisplayAddDedDetails(txtPolicyName.Tag.ToInt32(), 9);
                                FillAdditionDeductionsGrid(dgvLeaveConsequenceDetails, DtAddDedRef);
                            }

     
                  
                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        txtRate.Text = txtRate.Text.ToDecimal().ToString("F" + 0);
                        //txtHoursPerDay.Text = txtHoursPerDay.Text.ToDecimal().ToString("F" + 0);

                    }




                }

            }


        }
        private bool FormValidation() // For Validation
        {

            errLeaveConsequencePolicy.Clear();
            Control control = null;
            bool blnReturnValue = true;
            if (txtCalculationPercent.Text == "")
            {
                txtCalculationPercent.Text = "0";
            }
            if (txtPolicyName.Text.Trim().Length == 0)     // Please enter Policy Name
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(20100);
                control = txtPolicyName;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked == false && cboCalculationBased.SelectedValue.ToInt32() == 0) //Please select Calculation based for LeaveConsequence policy
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(20101);
                control = cboCalculationBased;
                blnReturnValue = false;
            }
            else if (chkRateOnly.Checked && (txtRate.Text.Trim().Length == 0 || txtRate.Text.ToDecimal()==0))
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(20102); //Please enter valid Rate for LeaveConsequence policy
                control = txtRate;
                blnReturnValue = false;
            }
            else if (txtRate.Text.Trim().Length > 0)
            {
                try
                {
                    decimal rate = Convert.ToDecimal(txtRate.Text);
                }
                catch
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(20102);  //Please enter valid Rate for LeaveConsequence policy
                    control = txtRate;
                    blnReturnValue = false;
                }
            }


            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errLeaveConsequencePolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                    if (control.Parent.GetType().FullName == "System.Windows.Forms.TabPage")
                        tbPolicy.SelectedTab = (TabPage)control.Parent;

                }
                return blnReturnValue;
            }

            
            if (blnReturnValue)
            {
                if (BLLLeaveConsequencePolicy.CheckDuplicate(txtPolicyName.Tag.ToInt32(), txtPolicyName.Text.Trim()))//CheckDuplicate()
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(20110); //Duplicate policy name.Please change policy name
                    control = txtPolicyName;
                    blnReturnValue = false;
                }
            }
            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = MstrCommonMessage.Remove(0, MstrCommonMessage.IndexOf("#") + 1).Trim();
                tmrClear.Enabled = true;
                if (control != null)
                {
                    errLeaveConsequencePolicy.SetError(control, MstrCommonMessage);
                    control.Focus();
                    if (control.Parent.GetType().FullName == "System.Windows.Forms.TabPage")
                    {
                        tbPolicy.SelectedTab = (TabPage)control.Parent;
                    }
                }
            }

            return blnReturnValue;
        }
        private bool DeleteValidation()
        {
            if (txtPolicyName.Tag.ToInt32() == 0)
            {
                UserMessage.ShowMessage(9015); // Sorry,No Data Found
                return false;

            }
            if (BLLLeaveConsequencePolicy.PolicyIDExists(txtPolicyName.Tag.ToInt32()))
            {
                UserMessage.ShowMessage(20103); //Details exists for this LeaveConsequence Policy in the system.Please delete the existing details to proceed with LeaveConsequence Policy deletion.
                return false;
            }
            if (UserMessage.ShowMessage(20106) == false) // Do you wish to Delete LeaveConsequence policy information?
            {
                return false;
            }
            return true;

        }
        private void FillParameterMaster() // Fill Master Details To DTO 
        {
            
            BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID = txtPolicyName.Tag.ToInt32();
            BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.LeaveConsequencePolicy = txtPolicyName.Text.Trim();
            BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.CalculationID = cboCalculationBased.SelectedValue.ToInt32();

            if (rdbCompanyBased.Checked == true)
            {
                BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.CompanyBasedOn = 1;  // CompanyBasedOn
            }

            else if (rdbActualMonth.Checked == true)
            {
                BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.CompanyBasedOn = 2;  // ActualMonth
            }
            else
            {
                BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.CompanyBasedOn = 3;
            }

            BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.IsRateOnly = chkRateOnly.Checked;
            BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.LeaveConsequenceRate = chkRateOnly.Checked ? txtRate.Text.Trim().ToDecimal() : 0;
            BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.LeaveConsequenceType = 9;
            BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.Percentage = Convert.ToDouble(txtCalculationPercent.Text);
             //BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.FixedAmount = 

        }

        private void FillParameterSalPolicyDetails() // fill AddtionDeduction Details
        {
            BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.DTOSalaryPolicyDetailLeaveConsequence = new List<clsDTOSalaryPolicyDetailLeaveConsequence>();

            if (dgvLeaveConsequenceDetails.Rows.Count > 0)
            {
                dgvLeaveConsequenceDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                dgvLeaveConsequenceDetails.CurrentCell = dgvLeaveConsequenceDetails["colLeaveConsequenceParticulars", 0];
                foreach (DataGridViewRow row in dgvLeaveConsequenceDetails.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["colLeaveConsequenceSelectedItem"].Value) == true)
                    {
                        clsDTOSalaryPolicyDetailLeaveConsequence objSalaryPolicyDetail = new clsDTOSalaryPolicyDetailLeaveConsequence();
                        objSalaryPolicyDetail.PolicyType = (int)PolicyType.LeavePolicy;
                        objSalaryPolicyDetail.AdditionDeductionID = row.Cells["colLeaveConsequenceAddDedID"].Value.ToInt32();
                        BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.DTOSalaryPolicyDetailLeaveConsequence.Add(objSalaryPolicyDetail);
                    }
                }
            }


        }
        private bool SaveLeaveConsequencePolicy() // Save LeaveConsequence Policy Details
        {
            bool blnRetValue = false;

            if (FormValidation())
            {
                int intMessageCode = 0;
                if (txtPolicyName.Tag.ToInt32() > 0)
                    intMessageCode = 20105;
                else
                    intMessageCode = 20104;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    FillParameterMaster();
                    FillParameterSalPolicyDetails();
                    if (BLLLeaveConsequencePolicy.LeaveConsequencePolicyMasterSave())
                    {
                        if (!MblnIsEditMode)
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(2);
                            UserMessage.ShowMessage(2, null, null, 2);
                        }
                        else
                        {
                            lblstatus.Text = UserMessage.GetMessageByCode(21);
                            UserMessage.ShowMessage(21, null, null, 2);
                        }
                        blnRetValue = true;
                        txtPolicyName.Tag = BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID;
                        PintLeaveConsequencePolicyID = BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID;
                        if (!MblnIsEditMode)
                        {
                            MintCurrentRecCnt = MintCurrentRecCnt + 1;
                            BindingNavigatorMoveLastItem_Click(null, null);
                        }

                    }

                }
            }

            return blnRetValue;
        }

        private bool DeleteLeaveConsequencePolicy()  // Delete LeaveConsequence Policy Reference
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                BLLLeaveConsequencePolicy.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID = txtPolicyName.Tag.ToInt32();
                if (BLLLeaveConsequencePolicy.DeleteLeaveConsequencePolicy())
                {
                    AddNewLeaveConsequencePolicy();
                    blnRetValue = true;
                }

            }
            return blnRetValue;

        }
        private void ResetForm(object sender, System.EventArgs e) // To reset Froms
        {
            if (sender is CheckBox)
            {
                CheckBox chk = (CheckBox)sender;
                if (chk.Name == chkRateOnly.Name)
                {
                    txtRate.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Info : System.Drawing.SystemColors.Window;
                    txtRate.Enabled = chkRateOnly.Checked ? true : false;

                    cboCalculationBased.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    if (chkRateOnly.Checked)
                    {
                        cboCalculationBased.SelectedIndex = -1;
                        txtCalculationPercent.Text = "";
                    }
                    else
                    {
                        cboCalculationBased.SelectedIndex = 0;
                        txtRate.Text = "";
                    }
                    txtCalculationPercent.Enabled = chkRateOnly.Checked ? false : true;
                    txtCalculationPercent.BackColor = chkRateOnly.Checked ? System.Drawing.SystemColors.Window : System.Drawing.SystemColors.Info;
                    cboCalculationBased.Enabled = chkRateOnly.Checked ? false : true;
                    rdbCompanyBased.Enabled = (chkRateOnly.Checked) ? false : true;
                    rdbActualMonth.Enabled = (chkRateOnly.Checked) ? false : true;

                }
            }
            Changestatus(null, null);
        }
        #endregion Methods

        #region Events

        private void FrmLeaveConsequencePolicy_Load(object sender, EventArgs e) // Load 
        {
            SetPermissions();
            LoadCombos(0);
            if (PintLeaveConsequencePolicyID > 0)  // Calling LeaveConsequence Policy From Another Froms 
            {
                GetRecordCount();
                RefernceDisplay();
            }
            else
            {
                AddNewLeaveConsequencePolicy();
            }
        }

        private void FrmLeaveConsequencePolicy_FormClosing(object sender, FormClosingEventArgs e) // Form Close Event
        {
            if (btnSave.Enabled)
            {
                if (UserMessage.ShowMessage(8))
                {
                    e.Cancel = false;
                }
                else
                { e.Cancel = true; }
            }
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e) // Button Action Move First
        {
            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayLeaveConsequencePolicyInfo();
                lblstatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;
            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e) // Button Action Move Previous
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayLeaveConsequencePolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e) // Button Action Move Next
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayLeaveConsequencePolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(11);
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e) // Button Action Move Last
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayLeaveConsequencePolicyInfo();
                    lblstatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e) // Add Button Click Event
        {
            AddNewLeaveConsequencePolicy();
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e) // Delete Button Click Event
        {
            if (DeleteLeaveConsequencePolicy())
            {
                lblstatus.Text = UserMessage.GetMessageByCode(4);
                tmrClear.Enabled = true;
                UserMessage.ShowMessage(4);
                AddNewLeaveConsequencePolicy();
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e) // Save Button Click Event
        {
            if (SaveLeaveConsequencePolicy())
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
            }
        }

        private void btnClear_Click(object sender, EventArgs e) // Clear Button Click Event
        {
            AddNewLeaveConsequencePolicy();
        }


        private void btnSave_Click(object sender, EventArgs e) // Save Button Click Event
        {
            if (SaveLeaveConsequencePolicy())
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
            }
        }

        private void btnOk_Click(object sender, EventArgs e) // Ok Button Click Event
        {
            if (SaveLeaveConsequencePolicy())
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)// Cancel Button Click Event
        {
            this.Close();
        }



        private void rdbCompanyBased_CheckedChanged(object sender, EventArgs e) // CompanyBased CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void chkRatePerDay_CheckedChanged(object sender, EventArgs e) //RatePerDay_CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void chkRateOnly_CheckedChanged(object sender, EventArgs e) //RateOnly_CheckedChanged
        {
            ResetForm(sender, e);
        }

        private void rdbShortBasedOnCompany_CheckedChanged(object sender, EventArgs e)//ShortBasedOnCompany_CheckedChanged
        {
            Changestatus(null, null);
        }



        private void tmrClear_Tick(object sender, EventArgs e) 
        {
            tmrClear.Enabled = false;
            lblstatus.Text = "";
        }

        private void txtDecimal_KeyPress(object sender, KeyPressEventArgs e) // Key Press For TextDecimal
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = strInvalidChars + ".";
            }

            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))) && ClsCommonSettings.IsAmountRoundByZero == false)
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ClsCommonSettings.IsAmountRoundByZero == true)
            {
                e.Handled = true;
            }
        }

        private void txtint_KeyPress(object sender, KeyPressEventArgs e) // Key Press For txtint_KeyPress
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            e.Handled = false;
            if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }



        private void chkMergeBoth_CheckedChanged(object sender, EventArgs e)//chkMergeBoth_CheckedChanged
        {
            Changestatus(null, null);
        }



        private void txtPolicyName_TextChanged(object sender, EventArgs e)// txtPolicyName_TextChanged
        {
            ResetForm(sender, e);
            Changestatus(null, null);
        }

        private void dgvLeaveConsequenceDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch
            {
            }
        }

        private void dgvLeaveConsequenceDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLeaveConsequenceDetails.IsCurrentCellDirty)
                {

                    dgvLeaveConsequenceDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }


        private void dgvShortageDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e) //dgvShortageDetails_CellValueChanged
        {
            Changestatus(null, null);
        }

        private void dgvLeaveConsequenceDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e) //dgvLeaveConsequenceDetails_CellValueChanged
        {
            Changestatus(null, null);
        }

        private void dgvShortageDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void cboCalculationBased_SelectedIndexChanged(object sender, EventArgs e) //cboCalculationBased_SelectedIndexChanged
        {
            //LeaveConsequence Tab
            Changestatus(null, null);
            if (cboCalculationBased.SelectedValue.ToInt32() > 0)
            {
                dgvLeaveConsequenceDetails.Enabled = true;

                if (cboCalculationBased.SelectedValue.ToInt32() == (int)CalculationType.GrossSalary)
                {
                    DataTable dtADDDedd = BLLLeaveConsequencePolicy.GetAdditionDeductions();
                    FillAdditionDeductionsGrid(dgvLeaveConsequenceDetails, dtADDDedd);
                }
                else
                {
                    dgvLeaveConsequenceDetails.Rows.Clear();
                    dgvLeaveConsequenceDetails.Enabled = false;
                }
            }
            else
            {
                dgvLeaveConsequenceDetails.Rows.Clear();
                dgvLeaveConsequenceDetails.Enabled = false;
            }
        }

      
        private void cboCalculationBased_KeyDown(object sender, KeyEventArgs e) //cboCalculationBased_KeyDown for Like Search
        {
            cboCalculationBased.DroppedDown = false;
            if (e.KeyData == Keys.Enter)
                cboCalculationBased_SelectedIndexChanged(sender, new EventArgs());

        }



        private void txtShortRatePerHour_TextChanged(object sender, EventArgs e) //txtShortRatePerHour_TextChanged 
        {
            Changestatus(null, null);
        }

        private void dgvLeaveConsequenceDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e) //dgvLeaveConsequenceDetails_CellBeginEdit
        {
            Changestatus(null, null);
        }

        private void txtShortHoursPerDay_TextChanged(object sender, EventArgs e) //txtShortHoursPerDay_TextChanged
        {
            Changestatus(null, null);
        }

        #endregion Events

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "SalaryStructure";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void txtCalculationPercent_TextChanged(object sender, EventArgs e)
        {
            Changestatus(null, null);
        }

       










    }
}
