﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace MyPayfriend
{
    /// <summary>
    ///  Modified By        : Laxmi
    ///  Modified Date      : 12/8/2013
    ///  Purpose            : Performance tuning
    ///  FormID             : 173
    ///  ErrorCode          : 10500-10550
    ///  Functionalities    : Receipt/ Issue/ Renew/ Attach documents
    ///                     : Insertion into ALerts
    ///                     : Card Issued is - it is a third party or agent name
    ///                     : No more validations
    /// </summary>
    /// 

    public partial class InsuranceDetails : Form
    {
        #region Variable declarations

        // Public variable declarations
        public int CompanyID { get; set; }        // Public variable From Employee form
        public int AssetID { get; set; }        // Public variable From Employee form
        public int PintInsuranceID = 0;   //Public variable From Navigator


        // Private variable declarations
        bool MblnAddStatus = true;
        string  MstrDocName = string.Empty;
    
        private int MintComboID = 0;                    // To Setting SelectedValue for combo box temporarily
        private int MintTimerInterval;                  // To set timer interval
        private int MintCurrentRecCnt;                  // Current recourd count
        private int MintRecordCnt;                      // Total record count
  
        private bool MblnAddPermission = false;         //To Set Add Permission
        private bool MblnUpdatePermission = false;      //To Set Update Permission
        private bool MblnDeletePermission = false;      //To Set Delete Permission
        private bool MblnPrintEmailPermission = false;  //To Set PrintEmail Permission

        bool mblnAddIssue, mblnAddReceipt,mblnAddRenew, mblnEmail, mblnDelete, mblnUpdate = false;  // Receipt-Issue Permission

        private string MsMessageCommon;
        private string MsMessageCaption;

        private bool mblnSearchStatus = false;

        private ArrayList MsMessageArray;   // Error Message display
        private ArrayList MaStatusMessage;

        MessageBoxIcon MsMessageBoxIcon;
        private string strOf = "of ";

        #endregion VariableDeclarations

        clsAlerts objAlerts;
        clsBLLInsuranceDetails objBLLInsuranceDetails = null;
        clsBLLPermissionSettings objPermission;
        ClsLogWriter objClsLogWriter;       // Object of the LogWriter class
        ClsNotification objClsNotification; // Object of the Notification class

        #region Constructor
        public InsuranceDetails()
        {
            InitializeComponent();

            objPermission = new clsBLLPermissionSettings();
            objBLLInsuranceDetails = new clsBLLInsuranceDetails();
            objAlerts = new clsAlerts();
            objClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
            objClsNotification = new ClsNotification();

            tmInsuranceCard.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer
            MintTimerInterval = ClsCommonSettings.TimerInterval; // current companyid
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }
        #endregion Constructor

        #region Events

        // Form Events

        private void InsuranceDetails_Load(object sender, EventArgs e)
        {
            //if (CompanyID == 0)
            //    CompanyID = ClsCommonSettings.CurrentCompanyID;

            SetAutoCompleteList();
            LoadMessage();
            SetPermissions();
            LoadInitials();
            cboCompany.Select();

            if (PintInsuranceID > 0) //From navigator document view
            {
                MintCurrentRecCnt = 1;
                MintRecordCnt = 1;
                GetInsuranceDetails();
                // Search button disabled
                bSearch.Enabled = false;
                lblStatus.Text = string.Empty;
                tmInsuranceCard.Enabled = true;
            }
            else if (CompanyID > 0 || AssetID >0 ) // From Employee- Load all cards of an employee
            {
                if (MintRecordCnt == 0) // No records
                {
                    MintRecordCnt = 1;
                    bnCountItem.Text = MintRecordCnt.ToString();
                    bnPositionItem.Text = MintCurrentRecCnt.ToString();
                }
                else if (MintRecordCnt > 0)
                    bnMoveFirstItem_Click(sender, e);

                if (CompanyID > 0 && AssetID > 0) txtPolicyNumber.Select();
                else cboAssets.Select();
            }           
        }

        private void InsuranceDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnOk.Enabled)
            {
                // Checking the changes are not saved and shows warning to the user
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 8, out MsMessageBoxIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    objAlerts = null;
                    objBLLInsuranceDetails = null;
                    objPermission = null;
                   
                    objClsLogWriter = null;
                    objClsNotification = null;

                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void InsuranceDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        bnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                 
                    case Keys.Control | Keys.Enter:
                        if (bnAddNewItem.Enabled ) bnAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                       if (bnDeleteItem.Enabled ) bnDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (bnCancel.Enabled ) bnCancel_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (bnMovePreviousItem.Enabled ) bnMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        if (bnMoveNextItem.Enabled ) bnMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        if (bnMoveFirstItem.Enabled ) bnMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        if (bnMoveLastItem.Enabled ) bnMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (bnPrint.Enabled ) bnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (bnEmail.Enabled ) bnEmail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.Alt | Keys.I:
                        if (issueToolStripMenuItem1.Enabled)
                            issueToolStripMenuItem1_Click(sender, new EventArgs());  //issue document
                        break;
                    case Keys.Alt | Keys.E:
                        if (receiptToolStripMenuItem.Enabled)
                            receiptToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                        break;
                    case Keys.Alt | Keys.N:
                        if (RenewToolStripMenuItem.Enabled)
                            issueToolStripMenuItem1_Click(sender, new EventArgs());// renew document
                        break;
                    case Keys.Alt | Keys.D:
                        if (documentsToolStripMenuItem.Enabled)
                            documentsToolStripMenuItem_Click(sender, new EventArgs()); // issue document
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        // Button events
        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            LoadInitials(); 
        }
        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveInsuranceDetails()) DoActionsAfterSave();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveInsuranceDetails()) DoActionsAfterSave();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveInsuranceDetails())
            {
                btnOk.Enabled = false;
                this.Close();
            }
        }
        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            if (txtPolicyNumber.Tag.ToInt32() > 0)
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 13, out MsMessageBoxIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                DeleteInsuranceDetails();
                //objAlerts.UpdateAlerts();
                LoadInitials();
                SetAutoCompleteList();
            }
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            LoadInitials();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPolicyNumber.Tag.ToInt32() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Insurance Details";
                        ObjEmailPopUp.EmailFormType = EmailFormID.InsuranceDetails;
                        ObjEmailPopUp.EmailSource = objBLLInsuranceDetails.DisplayInsuranceReport(txtPolicyNumber.Tag.ToInt32());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 1); }
        }

        private void bnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp objHelp = new FrmHelp())
            {
                objHelp.strFormName = "InsuranceDetails";
                objHelp.ShowDialog();
            }
        }

        //Button navigation Events
        private void bnMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            GetRecordCount();   // get no of records

            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;   // set current record
            }
           
            GetInsuranceDetails();     // display qualification details

            MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 9, out MsMessageBoxIcon);
            lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            tmInsuranceCard.Enabled = true;
       
        }

        private void bnMovePreviousItem_Click(object sender, EventArgs e)
        {

            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty; 

            GetRecordCount();   // get no of records

            if (MintRecordCnt > 0)
            {
                //BtnSearch_Click(sender, new EventArgs());

                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                    MintCurrentRecCnt = 1;
                else
                {
                    GetInsuranceDetails();     // display qualification details

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmInsuranceCard.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 1;
        }

        private void bnMoveNextItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            GetRecordCount();   // get no of records
           

            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                    MintCurrentRecCnt = 1;
                else
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;

                if (MintCurrentRecCnt > MintRecordCnt)
                    MintCurrentRecCnt = MintRecordCnt;
                else
                {
                    GetInsuranceDetails();

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 11, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmInsuranceCard.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 0;
        }

        private void bnMoveLastItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            GetRecordCount();

            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    GetInsuranceDetails();

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 12, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmInsuranceCard.Enabled = true;

                }
            }
        }

        // Document/Receipt/Issue Button events
        private void receiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocID = txtPolicyNumber.Tag.ToInt32();
                new frmDocumentReceiptIssue()
                {
                    eOperationType = OperationType.Company,
                    eDocumentType = DocumentType.InsuranceDetails,
                    DocumentID = iDocID,
                    DocumentNumber = txtPolicyNumber.Text,
                    EmployeeID = cboCompany.SelectedValue.ToInt32(),
                    ExpiryDate = dtpExpiryDate.Value
                }.ShowDialog();
                SetReceiptOrIssueLabel();
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on receiptToolStripMenuItem_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private void issueToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocID = txtPolicyNumber.Tag.ToInt32();
                new frmDocumentReceiptIssue()
                {
                    eOperationType = OperationType.Company,
                    eDocumentType = DocumentType.InsuranceDetails,
                    DocumentID = iDocID,
                    DocumentNumber = txtPolicyNumber.Text,
                    EmployeeID = cboCompany.SelectedValue.ToInt32(),
                    ExpiryDate = dtpExpiryDate.Value
                }.ShowDialog();
                SetReceiptOrIssueLabel();
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on issueToolStripMenuItem1_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private void documentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int iCardID = txtPolicyNumber.Tag.ToInt32();
                if (iCardID > 0)
                {
                    using (FrmScanning objScanning = new FrmScanning())
                    {
                        objScanning.MintDocumentTypeid = (int)DocumentType.InsuranceDetails;
                        objScanning.MlngReferenceID = cboCompany.SelectedValue.ToInt32();
                        objScanning.MintOperationTypeID = (int)OperationType.Employee;
                        objScanning.MstrReferenceNo = txtPolicyNumber.Text;
                        objScanning.PblnEditable = true;
                        objScanning.MintVendorID = cboCompany.SelectedValue.ToInt32();
                        objScanning.MintDocumentID = iCardID;
                        objScanning.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on documentsToolStripMenuItem_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private void RenewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPolicyNumber.Tag.ToInt32() > 0)
                {
                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 20009, out MsMessageBoxIcon); // Do you wish to renew

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    else //YES Do you wish to add
                    {
                        objBLLInsuranceDetails.objDTOInsuranceDetails.InsuranceID = txtPolicyNumber.Tag.ToInt32();
                        objBLLInsuranceDetails.UpdateInsuranceForRenewal();
                        GetRecordCount();
                        GetInsuranceDetails();

                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 20008, out MsMessageBoxIcon); // Do you wish to add
                        if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                        AddNewItem();
                        SetEnableDisable();
                        MblnAddStatus = true;
                        txtPolicyNumber.Text = string.Empty;
                        txtPolicyNumber.Select();
                    }
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on RenewToolStripMenuItem_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }

        // Reference Button events
        private void btnInsuranceCompany_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboInsuranceCompany.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Insurance Company", new int[] { 1, 0, MintTimerInterval }, "InsuranceCompanyID,Description,DescriptionArb", "InsuranceCompanyReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadInsuranceCompany();

                if (objCommon.NewID != 0)
                    cboInsuranceCompany.SelectedValue = objCommon.NewID;
                else
                    cboInsuranceCompany.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on btnInsuranceCompany_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnInsuranceCompany_Click" + ex.Message.ToString());
            }
        }
        private void btnAssets_Click(object sender, EventArgs e)
        {
            try
            {
                
                MintComboID = Convert.ToInt32(cboAssets.SelectedValue);
                using (FrmCompanyAssets objAssets = new FrmCompanyAssets())
                {
                    objAssets.ShowDialog();
                }
                
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on btnAssets_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnAssets_Click" + ex.Message.ToString());
            }
        }

        //Search button Events
        private void BtnSearch_Click(object sender, EventArgs e)
        {

            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;
            GetRecordCount();
          
            if (MintRecordCnt == 0) // No records
            {
                MessageBox.Show( objClsNotification.GetErrorMessage(MsMessageArray, 40, out MsMessageBoxIcon).Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
            }
            else if (MintRecordCnt > 0)
            {
                //AddNewItem();
                bnMoveFirstItem_Click(sender, e);
            }
        }

        // Combo events
        private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }
        private void cboInsuranceCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboInsuranceCompany.DroppedDown = false;
        }
        private void cboCardIssued_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboAssets.DroppedDown = false;
        }

        // Textbox events
        public void OnInputChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                if (e.KeyCode == Keys.Enter)
                    BtnSearch_Click(sender, null);
            }
        }

        private void tmInsuranceCard_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmInsuranceCard.Enabled = false;
            errInsuranceCard.Clear();
            tmInsuranceCard.Stop();
        }

        #endregion Events

        #region Functions
   
        /// <summary>
        /// Load Messages
        /// </summary>
        /// <param name="intType"></param>
        private void LoadMessage()
        {
            MsMessageArray = new ArrayList();
            MaStatusMessage = new ArrayList();
            MsMessageArray = objClsNotification.FillMessageArray((int)FormID.InsuranceDetails, ClsCommonSettings.ProductID);
            MaStatusMessage = objClsNotification.FillStatusMessageArray((int)FormID.InsuranceDetails, ClsCommonSettings.ProductID);
        }
        /// <summary>
        /// Load all combobox
        /// </summary>
        private void LoadCombos()
        {
            try
            {
                DataTable dtCombos = null;

                dtCombos = new ClsCommonUtility().FillCombos(new string[] { "CompanyID,CompanyName", "Companymaster", "CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });

                cboCompany.DisplayMember = "CompanyName";
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DataSource = dtCombos;

                if (CompanyID > 0)
                    cboCompany.SelectedValue = CompanyID;

                LoadInsuranceCompany();

            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadCombos() " + this.Name + " " + ex.Message.ToString(), 1);
                return;
            }
        }
        private void LoadInsuranceCompany()
        {
            DataTable dtCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                dtCombos = new ClsCommonUtility().FillCombos(new string[] { "InsuranceCompanyID,DescriptionArb AS Description ", "InsuranceCompanyReference", "" });
            }
            else
            {
                dtCombos = new ClsCommonUtility().FillCombos(new string[] { "InsuranceCompanyID,Description ", "InsuranceCompanyReference", "" });
            }
            cboInsuranceCompany.DisplayMember = "Description";
            cboInsuranceCompany.ValueMember = "InsuranceCompanyID";
            cboInsuranceCompany.DataSource = dtCombos;
        }
        private void LoadAssets()
        {
            DataTable datCombos = new ClsCommonUtility().FillCombos(new string[] { "CompanyAssetID ,Description", "CompanyAssets", "CompanyID= "+cboCompany.SelectedValue.ToInt32() +" AND  Status <> '" + (int)AssetStausType.Blocked + "' OR CompanyAssetID = " + objBLLInsuranceDetails.objDTOInsuranceDetails.CompanyAssetID });
            cboAssets.DisplayMember = "Description";
            cboAssets.ValueMember = "CompanyAssetID";
            cboAssets.DataSource = datCombos;
           
        }

        /// <summary>
        /// Permission settings
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.Insurance, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.CompanyDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.CompanyDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out mblnEmail, out mblnAddRenew, out mblnUpdate, out mblnDelete);
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
                mblnAddIssue = mblnAddReceipt = mblnAddRenew = true;
            }
        }
        /// <summary>
        /// Get total record count depending on EmployeeID/InsuranceCardID/Searchtext
        /// </summary>
        private void GetRecordCount()
        {
            MintRecordCnt = objBLLInsuranceDetails.GetRecordCount(CompanyID ,AssetID , PintInsuranceID, txtSearch.Text.Trim());
        }
        private void LoadInitials()
        {
            try
            {
                LoadCombos();
               // SetAutoCompleteList();
                txtSearch.Text = "";
                mblnSearchStatus = false;

                AddNewItem();

                cboCompany.SelectedIndex = cboInsuranceCompany.SelectedIndex = cboAssets.SelectedIndex = -1;
                txtPolicyNumber.Clear();
                txtPolicyNumber.Tag = "0";
                txtPolicyNumber.Clear();
                txtPolicyName.Clear();
                txtPolicyAmount.Clear();
                txtRemarks.Clear();

                dtpExpiryDate.Value = System.DateTime.Now.AddDays(1);
                dtpIssueDate.Value = DateTime.Today;

                errInsuranceCard.Clear();
                cboCompany.Enabled = true;
                cboCompany.Focus();

                if (CompanyID > 0) // From Employee
                {
                    cboCompany.Text = string.Empty;
                    cboCompany.SelectedValue = CompanyID;
                    //cboCompany.Enabled = false;
                    cboAssets.Focus();
                }
                FillAssetsInAddMode();
                if (AssetID > 0)
                {
                    //cboAssets.Text = string.Empty;
                    cboAssets.SelectedValue = AssetID ;
                    cboAssets.Enabled = false;
                    txtPolicyNumber.Focus();
                }
             
                SetEnableDisable();
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on LoadInitials() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private bool AddNewItem()
        {
            try
            {
                //txtSearch.Text = string.Empty;

                MblnAddStatus = (PintInsuranceID > 0 ? false : true);
                lblRenewed.Visible = false;
                cboCompany.Enabled = true;
                //cboAssets.Enabled = true;
                cboAssets.Enabled = AssetID  > 0 ? false : true;
                cboInsuranceCompany.Enabled =  btnInsuranceCompany.Enabled = true;
                txtPolicyNumber.Enabled = txtPolicyName.Enabled = txtPolicyAmount.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = true;
                //bnDeleteItem.ToolTipText = "Remove";

                txtSearch.Text = string.Empty;
                GetRecordCount();
                SetAutoCompleteList();

                bnCountItem.Text = strOf + Convert.ToString(MintRecordCnt + 1) + "";
                bnPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
                MintCurrentRecCnt = MintRecordCnt + 1;

                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 655, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmInsuranceCard.Enabled = true;
                return true;
            }
            catch (Exception Ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());
                return false;
            }
        }   
        /// <summary>
        /// Fill Parametersbefor save
        /// </summary>
        private void FillParameters()
        {
            
            objBLLInsuranceDetails.objDTOInsuranceDetails.InsuranceID = txtPolicyNumber.Tag.ToInt32();
            objBLLInsuranceDetails.objDTOInsuranceDetails.CompanyID = cboCompany.SelectedValue.ToInt32();
            objBLLInsuranceDetails.objDTOInsuranceDetails.CompanyAssetID = cboAssets.SelectedValue.ToInt32();
            objBLLInsuranceDetails.objDTOInsuranceDetails.PolicyNumber = txtPolicyNumber.Text.Trim();
            objBLLInsuranceDetails.objDTOInsuranceDetails.PolicyName = txtPolicyName.Text.Trim();
            objBLLInsuranceDetails.objDTOInsuranceDetails.InsuranceCompanyID = cboInsuranceCompany.SelectedValue.ToInt32();
            objBLLInsuranceDetails.objDTOInsuranceDetails.PolicyAmount =Convert.ToDecimal(txtPolicyAmount.Text.Trim());
            objBLLInsuranceDetails.objDTOInsuranceDetails.IssueDate = dtpIssueDate.Value.Date;
            objBLLInsuranceDetails.objDTOInsuranceDetails.ExpiryDate = dtpExpiryDate.Value.Date;
            objBLLInsuranceDetails.objDTOInsuranceDetails.Remarks = txtRemarks.Text.Trim();
            objBLLInsuranceDetails.objDTOInsuranceDetails.IsRenewed = lblRenewed.Visible ? true : false;
        }

        public void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;
            e.Handled = false;
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            }
            else
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            }

            if (ClsCommonSettings.IsAmountRoundByZero == false && ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Load report
        /// </summary>
        private void LoadReport()
        {
            try
            {
                if (txtPolicyNumber.Tag.ToInt32() > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();

                    ObjViewer.intCompanyID = cboCompany.SelectedValue.ToInt32();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = txtPolicyNumber.Tag.ToInt32();
                    ObjViewer.PiFormID = (int)FormID.InsuranceDetails;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        /// <summary>
        /// Delete Insurance card
        /// </summary>
        private void DeleteInsuranceDetails()
        {
            try
            {
                objBLLInsuranceDetails.objDTOInsuranceDetails.InsuranceID = txtPolicyNumber.Tag.ToInt32();
                objBLLInsuranceDetails.DeleteInsuranceDetails();
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 4, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmInsuranceCard.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
               
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on DeleteInsuranceDetails() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        /// <summary>
        /// Get Insurance card details
        /// </summary>
        private void GetInsuranceDetails()
        {
            try
            {
                DataTable DtInsuranceCard = objBLLInsuranceDetails.GetInsuranceDetails(MintCurrentRecCnt, CompanyID,AssetID , PintInsuranceID, txtSearch.Text.Trim());//get data from BLL
                if (DtInsuranceCard.Rows.Count > 0)
                {
                    objBLLInsuranceDetails.objDTOInsuranceDetails.CompanyID = Convert.ToInt32(DtInsuranceCard.Rows[0]["CompanyID"]);
                    cboCompany.SelectedValue = DtInsuranceCard.Rows[0]["CompanyID"];
                    objBLLInsuranceDetails.objDTOInsuranceDetails.CompanyAssetID = Convert.ToInt32(DtInsuranceCard.Rows[0]["CompanyAssetID"]);

                    LoadAssets();

                    
                    cboAssets.SelectedValue = DtInsuranceCard.Rows[0]["CompanyAssetID"];
                    txtPolicyNumber.Tag = DtInsuranceCard.Rows[0]["InsuranceId"].ToInt32();
                    txtPolicyNumber.Text = DtInsuranceCard.Rows[0]["PolicyNumber"].ToString();
                    txtPolicyName.Text = DtInsuranceCard.Rows[0]["PolicyName"].ToString();
                    cboInsuranceCompany.SelectedValue = DtInsuranceCard.Rows[0]["InsuranceCompanyID"];
                    txtPolicyAmount.Text = DtInsuranceCard.Rows[0]["PolicyAmount"].ToString();
                    dtpIssueDate.Value = DtInsuranceCard.Rows[0]["IssueDate"].ToDateTime();
                    dtpExpiryDate.Value = DtInsuranceCard.Rows[0]["ExpiryDate"].ToDateTime();
                    txtRemarks.Text = DtInsuranceCard.Rows[0]["Remarks"].ToString();
                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        txtPolicyAmount.Text = txtPolicyAmount.Text.ToDecimal().ToString("F" + 0);
                    }
                    if (Convert.ToBoolean(DtInsuranceCard.Rows[0]["IsRenewed"]))
                    {
                        lblRenewed.Visible = true;
                        cboCompany.Enabled = cboAssets.Enabled = cboInsuranceCompany.Enabled =  btnInsuranceCompany.Enabled = false;
                        txtPolicyNumber.Enabled = txtPolicyName.Enabled = txtPolicyAmount.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = false;
                    }
                    else
                    {
                        lblRenewed.Visible = false;
                        cboCompany.Enabled = true; // !(CompanyID > 0);
                        cboAssets.Enabled = !(AssetID > 0);
                         cboInsuranceCompany.Enabled = btnInsuranceCompany.Enabled = true;
                        txtPolicyNumber.Enabled = txtPolicyName.Enabled = txtPolicyAmount.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = true;
                    }


                    MblnAddStatus = false;

                    MstrDocName = txtPolicyNumber.Text.Replace("'", "").Trim();

                    bnPositionItem.Text = MintCurrentRecCnt.ToString();
                    bnCountItem.Text = strOf + Convert.ToString(MintRecordCnt) + "";
                    bnAddNewItem.Enabled = MblnAddPermission;

                    SetEnableDisable();
                    SetReceiptOrIssueLabel();
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on GetInsuranceDetails() " + this.Name + " " + ex.Message.ToString(), 1); }
           
        }
        /// <summary>
        /// button enable/disable depends on New mode or update mode
        /// </summary>
        private void SetEnableDisable()
        {
            btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;
            bnPrint.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission );
            bnEmail.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            bnMoreActions.Enabled = (MblnAddStatus == true ? false : true);

            bnAddNewItem.Enabled = (MblnAddStatus == true ? false : (PintInsuranceID > 0 ? false : MblnAddPermission ));

            if (MblnAddStatus) bnDeleteItem.Enabled = false;
            else
            {
                bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (PintInsuranceID > 0 ? false : MblnDeletePermission));
                //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Insurance card could not be removed." : "Remove");
            }
            bnCancel.Enabled = (MblnAddStatus == true ? true : false);

            if (PintInsuranceID <= 0)
            {
                bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = true;
                int iCurrentRec = Convert.ToInt32(bnPositionItem.Text); // Current position of the record

                if (iCurrentRec >= MintRecordCnt)  // If the current is last record, disables move next and last button
                {
                    bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
                }
                if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    bnMoveFirstItem.Enabled = bnMovePreviousItem.Enabled = false;
                }
            }
            else      
                bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = false;
        }
        /// <summary>
        /// Save Insurance card
        /// </summary>
        private bool SaveInsuranceDetails()
        {
            cboCompany.Focus();

            bool blnReturnValue = false;

            try
            {
                if (FormValidation())
                {
                    if (MblnAddStatus)
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 1, out MsMessageBoxIcon);
                    else
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 3, out MsMessageBoxIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)

                        return false;

                    FillParameters();

                    txtPolicyNumber.Tag = objBLLInsuranceDetails.SaveInsuranceDetails(MblnAddStatus).ToInt32();

                    if (txtPolicyNumber.Tag.ToInt32() > 0)
                    {
                        if (PintInsuranceID > 0)
                        {
                            PintInsuranceID = txtPolicyNumber.Tag.ToInt32();
                            MintCurrentRecCnt = 1;
                        }
                        clsAlerts objclsAlerts = new clsAlerts();
                        objclsAlerts.AlertMessage((int)DocumentType.InsuranceDetails, "Insurance", "InsurancePolicyNumber", txtPolicyNumber.Tag.ToInt32(), txtPolicyNumber.Text, dtpExpiryDate.Value, "Comp", cboCompany.SelectedValue.ToInt32(), cboCompany.Text.Trim(), true,0);
                        
                        string strMsg = "";
                        if (MblnAddStatus)//insert
                        {
                            strMsg = objClsNotification.GetErrorMessage(MsMessageArray, 2, out MsMessageBoxIcon);
                        }
                        else
                        {
                            strMsg = objClsNotification.GetErrorMessage(MsMessageArray, 21, out MsMessageBoxIcon);
                        } 
                        lblStatus.Text = strMsg.Remove(0, strMsg.IndexOf("#") + 1);
                        MessageBox.Show(strMsg.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                       
                        tmInsuranceCard.Enabled = true;
                        blnReturnValue = true;
                    }
                }

                return blnReturnValue;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on SaveInsuranceDetails() " + this.Name + " " + ex.Message.ToString(), 1);
                return blnReturnValue;
            }
        }
        /// <summary>
        /// Do actions after save
        /// Enable and disable form controls
        /// </summary>
        private void DoActionsAfterSave()
        {
            SetAutoCompleteList();
            MblnAddStatus = false;
            btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = bnCancel.Enabled=false; 
            documentsToolStripMenuItem.Enabled =  MblnAddPermission;
            bnAddNewItem.Enabled = (PintInsuranceID > 0 ? false : MblnAddPermission) ;
            bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            if (MblnAddStatus) bnDeleteItem.Enabled = false;
            else
            {
                bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (PintInsuranceID > 0 ? false : MblnDeletePermission));
                //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Insurance policy could not be removed." : "Remove");
            }
            SetReceiptOrIssueLabel();
        }
        /// <summary>
        /// Validation of controls
        /// </summary>        
        private bool FormValidation()
        {
            errInsuranceCard.Clear();
            bool bReturnValue = true;
            Control cControl = new Control();

            if (Convert.ToInt32(cboCompany.SelectedValue) == 0) // company Name
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 20000, out MsMessageBoxIcon);
                cControl = cboCompany;
                bReturnValue = false;
            }
            else if (Convert.ToInt32(cboAssets.SelectedValue) == 0)// Assets
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 20001, out MsMessageBoxIcon);
                cControl = cboAssets;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtPolicyNumber.Text.Trim())) // Policy Number
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 20003, out MsMessageBoxIcon);
                cControl = txtPolicyNumber;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtPolicyName.Text.Trim())) // Policy Name
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 20004, out MsMessageBoxIcon);
                cControl = txtPolicyName;
                bReturnValue = false;
            }
            else if (Convert.ToInt32(cboInsuranceCompany.SelectedValue) == 0) // Insurance Company
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 20002, out MsMessageBoxIcon);
                cControl = cboInsuranceCompany;
                bReturnValue = false;
            }
            
            else if (String.IsNullOrEmpty(txtPolicyAmount.Text.Trim())) // Policy Amount
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 20010, out MsMessageBoxIcon);
                cControl = txtPolicyAmount;
                bReturnValue = false;
            }
            else if (dtpExpiryDate.Value.Date <= dtpIssueDate.Value.Date)
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray,20005, out MsMessageBoxIcon);
                cControl = dtpExpiryDate;
                bReturnValue = false;
            }
            else if (dtpIssueDate.Value > ClsCommonSettings.GetServerDate())
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 20006, out MsMessageBoxIcon);
                cControl = dtpIssueDate;
                bReturnValue = false;
            }
            // Card Number Duplication 
            //else 
            //{
            //    if (objBLLInsuranceCard.CheckDuplication(( MblnAddStatus && txtPolicyNumber.Tag.ToInt32() >0 ? 0 : txtPolicyNumber.Tag.ToInt32()), txtPolicyNumber.Text.ToString()))
            //    {
            //        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10507, out MsMessageBoxIcon);
            //        cControl = txtPolicyNumber;
            //        bReturnValue = false;
            //    }
            //}
            if (bReturnValue == false)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (cControl != null)
                {
                    errInsuranceCard.SetError(cControl, MsMessageCommon);
                    cControl.Focus();
                }
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmInsuranceCard.Enabled = true;
            }
            return bReturnValue;
        }      
        /// <summary>
        /// Fill employee when navigate buttons
        /// </summary>
        private void FillCompany()
        {
            DataTable datCombos = new ClsCommonUtility().FillCombos(new string[] { "CompanyName ,CompanyID", "CompanyMaster", " CompanyID = " + objBLLInsuranceDetails.objDTOInsuranceDetails.CompanyID });
            cboCompany.DisplayMember = "CompanyName";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = datCombos;
        }           
        private void ChangeStatus()
        {
            errInsuranceCard.Clear();
            if (MblnAddStatus) // add mode
            {
                btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = MblnAddPermission;
            }
            else  //edit mode
            {
                btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = MblnUpdatePermission;
            }
        }
        /// <summary>
        /// Set Receipt /Issue button style(enable/disable)
        /// </summary>
        private void SetReceiptOrIssueLabel()
        {
            bnMoreActions.Enabled = true;
         
            RenewToolStripMenuItem.Enabled = lblRenewed.Visible ? false : mblnAddRenew;
            issueToolStripMenuItem1.Enabled = receiptToolStripMenuItem.Enabled = false;
            bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Company, (int)DocumentType.InsuranceDetails, txtPolicyNumber.Tag.ToInt32());

            if (IsReceipt)
                issueToolStripMenuItem1.Enabled = mblnAddIssue;
            else
                receiptToolStripMenuItem.Enabled = mblnAddReceipt;
        }
        /// <summary>
        /// Autocomplete with Employeename/Employeecode and CardNumber
        /// </summary>
        /// 
        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsBLLInsuranceDetails.GetAutoCompleteList("", CompanyID, "PolicyNumber", "InsuranceDetails",AssetID);
            this.txtSearch.AutoCompleteCustomSource = clsBLLTradeLicense.ConvertToAutoCompleteCollection(dt);
           
        }
        #endregion Functions

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.InsuranceDetails, this);

            bnMoreActions.Text = "الإجراءات";
            bnMoreActions.ToolTipText = "الإجراءات";
            documentsToolStripMenuItem.Text = "وثائق";
            receiptToolStripMenuItem.Text = "استلام";
            issueToolStripMenuItem1.Text = "قضية";
            RenewToolStripMenuItem.Text = "جدد";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            strOf = "من ";
        }
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAssetsInAddMode();
            cboAssets.Text = string.Empty;

        }
        private void FillAssetsInAddMode()
        {
            try
            {
                DataTable dtCombos = new ClsCommonUtility().FillCombos(new string[] { "CompanyAssetID,Description", "CompanyAssets", "CompanyID=" + cboCompany.SelectedValue.ToInt32() + "  AND Status <> '" + (int)AssetStausType.Blocked + "'" });
                cboAssets.DisplayMember = "Description";
                cboAssets.ValueMember = "CompanyAssetID";
                cboAssets.DataSource = dtCombos;

                if (AssetID > 0)
                    cboAssets.SelectedValue = AssetID;
                else
                    cboAssets.SelectedIndex = -1;
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on cboCompany_SelectedIndexChanged() " + this.Name + " " + ex.Message.ToString(), 1); }

        }

    }
}
