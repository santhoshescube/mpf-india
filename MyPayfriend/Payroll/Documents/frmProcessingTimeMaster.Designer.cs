﻿namespace MyPayfriend
{
    partial class frmProcessingTimeMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProcessingTimeMaster));
            this.BindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.SaveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.btncancel = new System.Windows.Forms.ToolStripButton();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.TmFocus = new System.Windows.Forms.Timer(this.components);
            this.Panel1 = new System.Windows.Forms.Panel();
            this.lbluser = new System.Windows.Forms.Label();
            this.cbouser = new System.Windows.Forms.ComboBox();
            this.grdProcessingtime = new System.Windows.Forms.DataGridView();
            this.contextRightMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.applyAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.Button();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnBottomCancel = new System.Windows.Forms.Button();
            this.Timerprtime = new System.Windows.Forms.Timer(this.components);
            this.ErrorProviderproc = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnOk = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator1)).BeginInit();
            this.BindingNavigator1.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProcessingtime)).BeginInit();
            this.contextRightMenu.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderproc)).BeginInit();
            this.SuspendLayout();
            // 
            // BindingNavigator1
            // 
            this.BindingNavigator1.AddNewItem = null;
            this.BindingNavigator1.CountItem = null;
            this.BindingNavigator1.DeleteItem = null;
            this.BindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveToolStripButton,
            this.btncancel,
            this.BtnHelp});
            this.BindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.BindingNavigator1.MoveFirstItem = null;
            this.BindingNavigator1.MoveLastItem = null;
            this.BindingNavigator1.MoveNextItem = null;
            this.BindingNavigator1.MovePreviousItem = null;
            this.BindingNavigator1.Name = "BindingNavigator1";
            this.BindingNavigator1.PositionItem = null;
            this.BindingNavigator1.Size = new System.Drawing.Size(425, 25);
            this.BindingNavigator1.TabIndex = 64;
            this.BindingNavigator1.Text = "BindingNavigator1";
            // 
            // SaveToolStripButton
            // 
            this.SaveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("SaveToolStripButton.Image")));
            this.SaveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveToolStripButton.Name = "SaveToolStripButton";
            this.SaveToolStripButton.Size = new System.Drawing.Size(51, 22);
            this.SaveToolStripButton.Text = "&Save";
            this.SaveToolStripButton.Click += new System.EventHandler(this.SaveToolStripButton_Click);
            // 
            // btncancel
            // 
            this.btncancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(38, 22);
            this.btncancel.Text = "Clear";
            this.btncancel.Visible = false;
            // 
            // BtnHelp
            // 
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(52, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.Color.White;
            this.GroupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.GroupBox1.Controls.Add(this.PictureBox1);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Location = new System.Drawing.Point(12, 7);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(403, 62);
            this.GroupBox1.TabIndex = 61;
            this.GroupBox1.TabStop = false;
            // 
            // PictureBox1
            // 
            this.PictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PictureBox1.BackgroundImage")));
            this.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PictureBox1.Location = new System.Drawing.Point(-5, 0);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(32, 33);
            this.PictureBox1.TabIndex = 61;
            this.PictureBox1.TabStop = false;
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(27, 11);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(389, 44);
            this.Label1.TabIndex = 60;
            this.Label1.Text = resources.GetString("Label1.Text");
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.White;
            this.Panel1.Controls.Add(this.GroupBox1);
            this.Panel1.Controls.Add(this.lbluser);
            this.Panel1.Controls.Add(this.cbouser);
            this.Panel1.Controls.Add(this.grdProcessingtime);
            this.Panel1.Location = new System.Drawing.Point(0, 29);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(424, 362);
            this.Panel1.TabIndex = 65;
            this.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel1_Paint);
            // 
            // lbluser
            // 
            this.lbluser.AutoSize = true;
            this.lbluser.Location = new System.Drawing.Point(12, 83);
            this.lbluser.Name = "lbluser";
            this.lbluser.Size = new System.Drawing.Size(60, 13);
            this.lbluser.TabIndex = 59;
            this.lbluser.Text = "User Name";
            // 
            // cbouser
            // 
            this.cbouser.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbouser.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbouser.BackColor = System.Drawing.SystemColors.Info;
            this.cbouser.FormattingEnabled = true;
            this.cbouser.Location = new System.Drawing.Point(89, 80);
            this.cbouser.Name = "cbouser";
            this.cbouser.Size = new System.Drawing.Size(230, 21);
            this.cbouser.TabIndex = 1;
            this.cbouser.SelectedIndexChanged += new System.EventHandler(this.cbouser_SelectedIndexChanged);
            this.cbouser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbouser_KeyPress);
            // 
            // grdProcessingtime
            // 
            this.grdProcessingtime.AllowUserToAddRows = false;
            this.grdProcessingtime.AllowUserToDeleteRows = false;
            this.grdProcessingtime.AllowUserToResizeRows = false;
            this.grdProcessingtime.BackgroundColor = System.Drawing.Color.White;
            this.grdProcessingtime.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProcessingtime.ContextMenuStrip = this.contextRightMenu;
            this.grdProcessingtime.Location = new System.Drawing.Point(7, 107);
            this.grdProcessingtime.Name = "grdProcessingtime";
            this.grdProcessingtime.RowHeadersWidth = 25;
            this.grdProcessingtime.Size = new System.Drawing.Size(409, 245);
            this.grdProcessingtime.TabIndex = 2;
            this.grdProcessingtime.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProcessingtime_CellValueChanged);
            this.grdProcessingtime.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdProcessingtime_CellMouseUp);
            this.grdProcessingtime.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProcessingtime_CellEndEdit);
            this.grdProcessingtime.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdProcessingtime_EditingControlShowing);
            this.grdProcessingtime.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdProcessingtime_CurrentCellDirtyStateChanged);
            this.grdProcessingtime.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdProcessingtime_DataError);
            // 
            // contextRightMenu
            // 
            this.contextRightMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applyAllToolStripMenuItem});
            this.contextRightMenu.Name = "contextRightMenu";
            this.contextRightMenu.Size = new System.Drawing.Size(123, 26);
            this.contextRightMenu.Text = "Apply All";
            // 
            // applyAllToolStripMenuItem
            // 
            this.applyAllToolStripMenuItem.Name = "applyAllToolStripMenuItem";
            this.applyAllToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.applyAllToolStripMenuItem.Text = "Apply All";
            this.applyAllToolStripMenuItem.Click += new System.EventHandler(this.applyAllToolStripMenuItem_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(7, 396);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 62;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 437);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(425, 22);
            this.StatusStrip1.TabIndex = 63;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.ForeColor = System.Drawing.Color.Black;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // btnBottomCancel
            // 
            this.btnBottomCancel.Location = new System.Drawing.Point(341, 396);
            this.btnBottomCancel.Name = "btnBottomCancel";
            this.btnBottomCancel.Size = new System.Drawing.Size(75, 23);
            this.btnBottomCancel.TabIndex = 61;
            this.btnBottomCancel.Text = "&Cancel";
            this.btnBottomCancel.UseVisualStyleBackColor = true;
            this.btnBottomCancel.Click += new System.EventHandler(this.btnBottomCancel_Click);
            // 
            // Timerprtime
            // 
            this.Timerprtime.Interval = 2000;
            // 
            // ErrorProviderproc
            // 
            this.ErrorProviderproc.ContainerControl = this;
            this.ErrorProviderproc.RightToLeft = true;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(259, 396);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 60;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // frmProcessingTimeMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 459);
            this.Controls.Add(this.BindingNavigator1);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.btnBottomCancel);
            this.Controls.Add(this.btnOk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProcessingTimeMaster";
            this.Text = "Processing Time For Document Types";
            this.Load += new System.EventHandler(this.frmProcessingTimeMaster_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProcessingTimeMaster_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmProcessingTimeMaster_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator1)).EndInit();
            this.BindingNavigator1.ResumeLayout(false);
            this.BindingNavigator1.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProcessingtime)).EndInit();
            this.contextRightMenu.ResumeLayout(false);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderproc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator BindingNavigator1;
        internal System.Windows.Forms.ToolStripButton SaveToolStripButton;
        internal System.Windows.Forms.ToolStripButton btncancel;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Timer TmFocus;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Label lbluser;
        internal System.Windows.Forms.ComboBox cbouser;
        internal System.Windows.Forms.DataGridView grdProcessingtime;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.Button btnBottomCancel;
        internal System.Windows.Forms.Timer Timerprtime;
        internal System.Windows.Forms.ErrorProvider ErrorProviderproc;
        internal System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ContextMenuStrip contextRightMenu;
        private System.Windows.Forms.ToolStripMenuItem applyAllToolStripMenuItem;
    }
}