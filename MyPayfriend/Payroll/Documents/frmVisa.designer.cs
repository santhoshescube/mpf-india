﻿
    partial class frmVisa 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label VisaIssueDateLabel;
            System.Windows.Forms.Label PlaceofIssueLabel;
            System.Windows.Forms.Label VisaExpiryDateLabel;
            System.Windows.Forms.Label VisaNumberLabel;
            System.Windows.Forms.Label IssueCountryIDLabel;
            System.Windows.Forms.Label EmployeeIDLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVisa));
            System.Windows.Forms.Label lblUID;
            System.Windows.Forms.Label lblVisaComapny;
            System.Windows.Forms.Label label5;
            this.BindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripDropDownBtnMore = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuItemAttachDocuments = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.receiptIssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.renewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.cboCountry = new System.Windows.Forms.ComboBox();
            this.BtnAddCountry = new System.Windows.Forms.Button();
            this.dtpVisaExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.dtpVisaIssueDate = new System.Windows.Forms.DateTimePicker();
            this.txtPlaceOfIssue = new System.Windows.Forms.TextBox();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.txtVisaNumber = new System.Windows.Forms.TextBox();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnVisaType = new System.Windows.Forms.Button();
            this.CboVisaType = new System.Windows.Forms.ComboBox();
            this.lblRenewed = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.txtUID = new System.Windows.Forms.TextBox();
            this.cboVisaComapny = new System.Windows.Forms.ComboBox();
            this.cboVisaDesignation = new System.Windows.Forms.ComboBox();
            this.cboDesignation = new System.Windows.Forms.Button();
            VisaIssueDateLabel = new System.Windows.Forms.Label();
            PlaceofIssueLabel = new System.Windows.Forms.Label();
            VisaExpiryDateLabel = new System.Windows.Forms.Label();
            VisaNumberLabel = new System.Windows.Forms.Label();
            IssueCountryIDLabel = new System.Windows.Forms.Label();
            EmployeeIDLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            lblUID = new System.Windows.Forms.Label();
            lblVisaComapny = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator)).BeginInit();
            this.BindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.StatusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // VisaIssueDateLabel
            // 
            VisaIssueDateLabel.AutoSize = true;
            VisaIssueDateLabel.Location = new System.Drawing.Point(10, 243);
            VisaIssueDateLabel.Name = "VisaIssueDateLabel";
            VisaIssueDateLabel.Size = new System.Drawing.Size(58, 13);
            VisaIssueDateLabel.TabIndex = 84;
            VisaIssueDateLabel.Text = "Issue Date";
            // 
            // PlaceofIssueLabel
            // 
            PlaceofIssueLabel.AutoSize = true;
            PlaceofIssueLabel.Location = new System.Drawing.Point(10, 78);
            PlaceofIssueLabel.Name = "PlaceofIssueLabel";
            PlaceofIssueLabel.Size = new System.Drawing.Size(74, 13);
            PlaceofIssueLabel.TabIndex = 100;
            PlaceofIssueLabel.Text = "Place of Issue";
            // 
            // VisaExpiryDateLabel
            // 
            VisaExpiryDateLabel.AutoSize = true;
            VisaExpiryDateLabel.Location = new System.Drawing.Point(263, 243);
            VisaExpiryDateLabel.Name = "VisaExpiryDateLabel";
            VisaExpiryDateLabel.Size = new System.Drawing.Size(61, 13);
            VisaExpiryDateLabel.TabIndex = 85;
            VisaExpiryDateLabel.Text = "Expiry Date";
            // 
            // VisaNumberLabel
            // 
            VisaNumberLabel.AutoSize = true;
            VisaNumberLabel.Location = new System.Drawing.Point(10, 52);
            VisaNumberLabel.Name = "VisaNumberLabel";
            VisaNumberLabel.Size = new System.Drawing.Size(67, 13);
            VisaNumberLabel.TabIndex = 60;
            VisaNumberLabel.Text = "Visa Number";
            // 
            // IssueCountryIDLabel
            // 
            IssueCountryIDLabel.AutoSize = true;
            IssueCountryIDLabel.Location = new System.Drawing.Point(10, 107);
            IssueCountryIDLabel.Name = "IssueCountryIDLabel";
            IssueCountryIDLabel.Size = new System.Drawing.Size(43, 13);
            IssueCountryIDLabel.TabIndex = 87;
            IssueCountryIDLabel.Text = "Country";
            // 
            // EmployeeIDLabel
            // 
            EmployeeIDLabel.AutoSize = true;
            EmployeeIDLabel.Location = new System.Drawing.Point(10, 28);
            EmployeeIDLabel.Name = "EmployeeIDLabel";
            EmployeeIDLabel.Size = new System.Drawing.Size(84, 13);
            EmployeeIDLabel.TabIndex = 70;
            EmployeeIDLabel.Text = "Employee Name";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(10, 275);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(58, 13);
            label1.TabIndex = 141;
            label1.Text = "Remarks";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(10, 136);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(54, 13);
            label3.TabIndex = 146;
            label3.Text = "Visa Type";
            // 
            // BindingNavigator
            // 
            this.BindingNavigator.AddNewItem = null;
            this.BindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.BindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.BindingNavigator.DeleteItem = null;
            this.BindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.ToolStripSeparator1,
            this.BindingNavigatorAddNewItem,
            this.bnSaveItem,
            this.BindingNavigatorDeleteItem,
            this.btnClear,
            this.toolStripSeparator4,
            this.ToolStripDropDownBtnMore,
            this.ToolStripSeparator3,
            this.BtnPrint,
            this.BtnEmail,
            this.BindingNavigatorSeparator2,
            this.BtnHelp});
            this.BindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.BindingNavigator.MoveFirstItem = null;
            this.BindingNavigator.MoveLastItem = null;
            this.BindingNavigator.MoveNextItem = null;
            this.BindingNavigator.MovePreviousItem = null;
            this.BindingNavigator.Name = "BindingNavigator";
            this.BindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.BindingNavigator.Size = new System.Drawing.Size(467, 25);
            this.BindingNavigator.TabIndex = 0;
            this.BindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Enabled = false;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripDropDownBtnMore
            // 
            this.ToolStripDropDownBtnMore.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAttachDocuments,
            this.toolStripSeparator2,
            this.receiptIssueToolStripMenuItem,
            this.issueToolStripMenuItem,
            this.toolStripSeparator5,
            this.renewToolStripMenuItem});
            this.ToolStripDropDownBtnMore.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.ToolStripDropDownBtnMore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripDropDownBtnMore.Name = "ToolStripDropDownBtnMore";
            this.ToolStripDropDownBtnMore.Size = new System.Drawing.Size(76, 22);
            this.ToolStripDropDownBtnMore.Text = "Actions";
            this.ToolStripDropDownBtnMore.ToolTipText = "Actions";
            // 
            // mnuItemAttachDocuments
            // 
            this.mnuItemAttachDocuments.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.mnuItemAttachDocuments.Name = "mnuItemAttachDocuments";
            this.mnuItemAttachDocuments.Size = new System.Drawing.Size(135, 22);
            this.mnuItemAttachDocuments.Text = "&Documents";
            this.mnuItemAttachDocuments.Click += new System.EventHandler(this.mnuItemAttachDocuments_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(132, 6);
            // 
            // receiptIssueToolStripMenuItem
            // 
            this.receiptIssueToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.receiptIssueToolStripMenuItem.Name = "receiptIssueToolStripMenuItem";
            this.receiptIssueToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.receiptIssueToolStripMenuItem.Text = "R&eceipt";
            this.receiptIssueToolStripMenuItem.Click += new System.EventHandler(this.receiptIssueToolStripMenuItem_Click);
            // 
            // issueToolStripMenuItem
            // 
            this.issueToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.issueToolStripMenuItem.Name = "issueToolStripMenuItem";
            this.issueToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.issueToolStripMenuItem.Text = "&Issue";
            this.issueToolStripMenuItem.Click += new System.EventHandler(this.receiptIssueToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(132, 6);
            // 
            // renewToolStripMenuItem
            // 
            this.renewToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.renew;
            this.renewToolStripMenuItem.Name = "renewToolStripMenuItem";
            this.renewToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.renewToolStripMenuItem.Text = "Re&new";
            this.renewToolStripMenuItem.Click += new System.EventHandler(this.renewToolStripMenuItem_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Fax";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(10, 292);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(419, 58);
            this.txtRemarks.TabIndex = 13;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 61;
            this.lineShape2.X2 = 425;
            this.lineShape2.Y1 = 268;
            this.lineShape2.Y2 = 268;
            // 
            // cboCountry
            // 
            this.cboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCountry.BackColor = System.Drawing.SystemColors.Info;
            this.cboCountry.DisplayMember = "CountryID";
            this.cboCountry.DropDownHeight = 134;
            this.cboCountry.FormattingEnabled = true;
            this.cboCountry.IntegralHeight = false;
            this.cboCountry.Location = new System.Drawing.Point(99, 104);
            this.cboCountry.Name = "cboCountry";
            this.cboCountry.Size = new System.Drawing.Size(294, 21);
            this.cboCountry.TabIndex = 3;
            this.cboCountry.ValueMember = "CountryID";
            this.cboCountry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCountry_KeyPress);
            // 
            // BtnAddCountry
            // 
            this.BtnAddCountry.Location = new System.Drawing.Point(399, 104);
            this.BtnAddCountry.Name = "BtnAddCountry";
            this.BtnAddCountry.Size = new System.Drawing.Size(33, 21);
            this.BtnAddCountry.TabIndex = 4;
            this.BtnAddCountry.Text = "....";
            this.BtnAddCountry.UseVisualStyleBackColor = true;
            this.BtnAddCountry.Click += new System.EventHandler(this.BtnAddCountry_Click);
            // 
            // dtpVisaExpiryDate
            // 
            this.dtpVisaExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpVisaExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpVisaExpiryDate.Location = new System.Drawing.Point(330, 243);
            this.dtpVisaExpiryDate.Name = "dtpVisaExpiryDate";
            this.dtpVisaExpiryDate.Size = new System.Drawing.Size(102, 20);
            this.dtpVisaExpiryDate.TabIndex = 12;
            // 
            // dtpVisaIssueDate
            // 
            this.dtpVisaIssueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpVisaIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpVisaIssueDate.Location = new System.Drawing.Point(99, 243);
            this.dtpVisaIssueDate.Name = "dtpVisaIssueDate";
            this.dtpVisaIssueDate.Size = new System.Drawing.Size(102, 20);
            this.dtpVisaIssueDate.TabIndex = 11;
            // 
            // txtPlaceOfIssue
            // 
            this.txtPlaceOfIssue.Location = new System.Drawing.Point(99, 78);
            this.txtPlaceOfIssue.MaxLength = 50;
            this.txtPlaceOfIssue.Name = "txtPlaceOfIssue";
            this.txtPlaceOfIssue.Size = new System.Drawing.Size(274, 20);
            this.txtPlaceOfIssue.TabIndex = 2;
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DisplayMember = "EmployeeID";
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(99, 25);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(333, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.ValueMember = "EmployeeID";
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboEmployee_KeyPress);
            // 
            // txtVisaNumber
            // 
            this.txtVisaNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtVisaNumber.Location = new System.Drawing.Point(99, 52);
            this.txtVisaNumber.MaxLength = 50;
            this.txtVisaNumber.Name = "txtVisaNumber";
            this.txtVisaNumber.Size = new System.Drawing.Size(198, 20);
            this.txtVisaNumber.TabIndex = 1;
            this.txtVisaNumber.TextChanged += new System.EventHandler(this.txtVisaNumber_TextChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(10, 421);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(315, 421);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(70, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(391, 422);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(70, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.RightToLeft = true;
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 453);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(467, 22);
            this.StatusStrip1.TabIndex = 146;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboDesignation);
            this.groupBox1.Controls.Add(this.lblRenewed);
            this.groupBox1.Controls.Add(this.cboVisaDesignation);
            this.groupBox1.Controls.Add(label5);
            this.groupBox1.Controls.Add(this.cboVisaComapny);
            this.groupBox1.Controls.Add(lblVisaComapny);
            this.groupBox1.Controls.Add(lblUID);
            this.groupBox1.Controls.Add(this.txtUID);
            this.groupBox1.Controls.Add(this.btnVisaType);
            this.groupBox1.Controls.Add(this.CboVisaType);
            this.groupBox1.Controls.Add(label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(EmployeeIDLabel);
            this.groupBox1.Controls.Add(this.cboEmployee);
            this.groupBox1.Controls.Add(this.dtpVisaExpiryDate);
            this.groupBox1.Controls.Add(this.BtnAddCountry);
            this.groupBox1.Controls.Add(VisaExpiryDateLabel);
            this.groupBox1.Controls.Add(this.cboCountry);
            this.groupBox1.Controls.Add(this.dtpVisaIssueDate);
            this.groupBox1.Controls.Add(VisaIssueDateLabel);
            this.groupBox1.Controls.Add(VisaNumberLabel);
            this.groupBox1.Controls.Add(IssueCountryIDLabel);
            this.groupBox1.Controls.Add(this.txtVisaNumber);
            this.groupBox1.Controls.Add(PlaceofIssueLabel);
            this.groupBox1.Controls.Add(this.txtPlaceOfIssue);
            this.groupBox1.Controls.Add(this.shapeContainer1);
            this.groupBox1.Location = new System.Drawing.Point(12, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 357);
            this.groupBox1.TabIndex = 147;
            this.groupBox1.TabStop = false;
            // 
            // btnVisaType
            // 
            this.btnVisaType.Location = new System.Drawing.Point(252, 132);
            this.btnVisaType.Name = "btnVisaType";
            this.btnVisaType.Size = new System.Drawing.Size(33, 21);
            this.btnVisaType.TabIndex = 6;
            this.btnVisaType.Text = "....";
            this.btnVisaType.UseVisualStyleBackColor = true;
            this.btnVisaType.Click += new System.EventHandler(this.btnVisaType_Click);
            // 
            // CboVisaType
            // 
            this.CboVisaType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboVisaType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboVisaType.BackColor = System.Drawing.SystemColors.Info;
            this.CboVisaType.DisplayMember = "CountryID";
            this.CboVisaType.DropDownHeight = 134;
            this.CboVisaType.FormattingEnabled = true;
            this.CboVisaType.IntegralHeight = false;
            this.CboVisaType.Location = new System.Drawing.Point(99, 133);
            this.CboVisaType.Name = "CboVisaType";
            this.CboVisaType.Size = new System.Drawing.Size(147, 21);
            this.CboVisaType.TabIndex = 5;
            this.CboVisaType.ValueMember = "CountryID";
            // 
            // lblRenewed
            // 
            this.lblRenewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenewed.ForeColor = System.Drawing.Color.Maroon;
            this.lblRenewed.Location = new System.Drawing.Point(361, 266);
            this.lblRenewed.Name = "lblRenewed";
            this.lblRenewed.Size = new System.Drawing.Size(71, 18);
            this.lblRenewed.TabIndex = 9;
            this.lblRenewed.Text = "Renewed";
            this.lblRenewed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRenewed.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 143;
            this.label2.Text = "Visa Details";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(438, 338);
            this.shapeContainer1.TabIndex = 142;
            this.shapeContainer1.TabStop = false;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(467, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 149;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code | Visa Number";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblUID
            // 
            lblUID.AutoSize = true;
            lblUID.Location = new System.Drawing.Point(10, 213);
            lblUID.Name = "lblUID";
            lblUID.Size = new System.Drawing.Size(26, 13);
            lblUID.TabIndex = 148;
            lblUID.Text = "UID";
            // 
            // txtUID
            // 
            this.txtUID.Location = new System.Drawing.Point(99, 213);
            this.txtUID.MaxLength = 50;
            this.txtUID.Name = "txtUID";
            this.txtUID.Size = new System.Drawing.Size(147, 20);
            this.txtUID.TabIndex = 9;
            this.txtUID.TextChanged += new System.EventHandler(this.txtUID_TextChanged);
            // 
            // cboVisaComapny
            // 
            this.cboVisaComapny.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVisaComapny.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVisaComapny.BackColor = System.Drawing.Color.White;
            this.cboVisaComapny.DisplayMember = "CountryID";
            this.cboVisaComapny.DropDownHeight = 134;
            this.cboVisaComapny.FormattingEnabled = true;
            this.cboVisaComapny.IntegralHeight = false;
            this.cboVisaComapny.Location = new System.Drawing.Point(99, 159);
            this.cboVisaComapny.Name = "cboVisaComapny";
            this.cboVisaComapny.Size = new System.Drawing.Size(294, 21);
            this.cboVisaComapny.TabIndex = 7;
            this.cboVisaComapny.TextChanged += new System.EventHandler(this.cboVisaComapny_TextChanged);
            // 
            // lblVisaComapny
            // 
            lblVisaComapny.AutoSize = true;
            lblVisaComapny.Location = new System.Drawing.Point(10, 162);
            lblVisaComapny.Name = "lblVisaComapny";
            lblVisaComapny.Size = new System.Drawing.Size(74, 13);
            lblVisaComapny.TabIndex = 150;
            lblVisaComapny.Text = "Visa Company";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(10, 189);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(86, 13);
            label5.TabIndex = 150;
            label5.Text = "Visa Designation";
            // 
            // cboVisaDesignation
            // 
            this.cboVisaDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVisaDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVisaDesignation.BackColor = System.Drawing.Color.White;
            this.cboVisaDesignation.DisplayMember = "CountryID";
            this.cboVisaDesignation.DropDownHeight = 134;
            this.cboVisaDesignation.FormattingEnabled = true;
            this.cboVisaDesignation.IntegralHeight = false;
            this.cboVisaDesignation.Location = new System.Drawing.Point(99, 186);
            this.cboVisaDesignation.Name = "cboVisaDesignation";
            this.cboVisaDesignation.Size = new System.Drawing.Size(294, 21);
            this.cboVisaDesignation.TabIndex = 8;
            this.cboVisaDesignation.TextChanged += new System.EventHandler(this.cboVisaDesignation_TextChanged);
            // 
            // cboDesignation
            // 
            this.cboDesignation.Location = new System.Drawing.Point(396, 186);
            this.cboDesignation.Name = "cboDesignation";
            this.cboDesignation.Size = new System.Drawing.Size(33, 21);
            this.cboDesignation.TabIndex = 4;
            this.cboDesignation.Text = "....";
            this.cboDesignation.UseVisualStyleBackColor = true;
            this.cboDesignation.Click += new System.EventHandler(this.cboDesignation_Click);
            // 
            // frmVisa
            // 
            this.AccessibleDescription = "";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 475);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.BindingNavigator);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVisa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Visa";
            this.Load += new System.EventHandler(this.frmVisa_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVisa_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVisa_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator)).EndInit();
            this.BindingNavigator.ResumeLayout(false);
            this.BindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer Timer;
        internal System.Windows.Forms.BindingNavigator BindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.ComboBox cboCountry;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.Button BtnAddCountry;
        internal System.Windows.Forms.DateTimePicker dtpVisaExpiryDate;
        internal System.Windows.Forms.TextBox txtVisaNumber;
        internal System.Windows.Forms.TextBox txtPlaceOfIssue;
        internal System.Windows.Forms.DateTimePicker dtpVisaIssueDate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtRemarks;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.ToolStripDropDownButton ToolStripDropDownBtnMore;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAttachDocuments;
        private System.Windows.Forms.ToolStripMenuItem receiptIssueToolStripMenuItem;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        private System.Windows.Forms.ToolStripMenuItem issueToolStripMenuItem;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        private System.Windows.Forms.ToolStripButton btnClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.GroupBox groupBox1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem renewToolStripMenuItem;
        private System.Windows.Forms.Label lblRenewed;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        internal System.Windows.Forms.ErrorProvider errorProvider1;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.Button btnVisaType;
        internal System.Windows.Forms.ComboBox CboVisaType;
        internal System.Windows.Forms.TextBox txtUID;
        internal System.Windows.Forms.ComboBox cboVisaComapny;
        internal System.Windows.Forms.ComboBox cboVisaDesignation;
        internal System.Windows.Forms.Button cboDesignation;

    }
