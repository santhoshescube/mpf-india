﻿namespace MyPayfriend
{
    partial class frmCompanyLeaseAgreement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblEmployee;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label VisaRenewDateLabel;
            System.Windows.Forms.Label VisaExpiryDateLabel;
            System.Windows.Forms.Label lblLeasePeriod;
            System.Windows.Forms.Label VisaIssueDateLabel;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label Label6;
            System.Windows.Forms.Label Label5;
            System.Windows.Forms.Label Label7;
            System.Windows.Forms.Label Label8;
            System.Windows.Forms.Label Label15;
            System.Windows.Forms.Label Label14;
            System.Windows.Forms.Label Label12;
            System.Windows.Forms.Label Label11;
            System.Windows.Forms.Label Label9;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCompanyLeaseAgreement));
            this.EmployeeInsuranceCardBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnCancel = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.documentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.receiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.renewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.stsStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ContractInfo = new System.Windows.Forms.TabPage();
            this.lblRenewed = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtContactNumbers = new System.Windows.Forms.TextBox();
            this.txtTenant = new System.Windows.Forms.TextBox();
            this.chkDepositHeld = new System.Windows.Forms.CheckBox();
            this.txtRentalValue = new System.Windows.Forms.TextBox();
            this.txtLeasePeriod = new System.Windows.Forms.TextBox();
            this.dtpDocumentDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.txtLessor = new System.Windows.Forms.TextBox();
            this.txtAgreementNumber = new System.Windows.Forms.TextBox();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.LeaseTab = new System.Windows.Forms.TabControl();
            this.BuildingInfo = new System.Windows.Forms.TabPage();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblLeaseUnitType = new System.Windows.Forms.Label();
            this.txtLandlord = new System.Windows.Forms.TextBox();
            this.btnLeaseUnitType = new System.Windows.Forms.Button();
            this.txtBuildingNumber = new System.Windows.Forms.TextBox();
            this.cboLeaseUnitType = new System.Windows.Forms.ComboBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.txtPlotNumber = new System.Windows.Forms.TextBox();
            this.txtUnitNumber = new System.Windows.Forms.TextBox();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.errLease = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmLease = new System.Windows.Forms.Timer(this.components);
            lblEmployee = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            VisaRenewDateLabel = new System.Windows.Forms.Label();
            VisaExpiryDateLabel = new System.Windows.Forms.Label();
            lblLeasePeriod = new System.Windows.Forms.Label();
            VisaIssueDateLabel = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            Label6 = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            Label7 = new System.Windows.Forms.Label();
            Label8 = new System.Windows.Forms.Label();
            Label15 = new System.Windows.Forms.Label();
            Label14 = new System.Windows.Forms.Label();
            Label12 = new System.Windows.Forms.Label();
            Label11 = new System.Windows.Forms.Label();
            Label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeInsuranceCardBindingNavigator)).BeginInit();
            this.EmployeeInsuranceCardBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.stsStatus.SuspendLayout();
            this.ContractInfo.SuspendLayout();
            this.LeaseTab.SuspendLayout();
            this.BuildingInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errLease)).BeginInit();
            this.SuspendLayout();
            // 
            // lblEmployee
            // 
            lblEmployee.AutoSize = true;
            lblEmployee.Location = new System.Drawing.Point(11, 10);
            lblEmployee.Name = "lblEmployee";
            lblEmployee.Size = new System.Drawing.Size(51, 13);
            lblEmployee.TabIndex = 72;
            lblEmployee.Text = "Company";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(11, 38);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(98, 13);
            label1.TabIndex = 73;
            label1.Text = "Agreement Number";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(11, 64);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(38, 13);
            label2.TabIndex = 76;
            label2.Text = "Lessor";
            // 
            // VisaRenewDateLabel
            // 
            VisaRenewDateLabel.AutoSize = true;
            VisaRenewDateLabel.Location = new System.Drawing.Point(11, 172);
            VisaRenewDateLabel.Name = "VisaRenewDateLabel";
            VisaRenewDateLabel.Size = new System.Drawing.Size(55, 13);
            VisaRenewDateLabel.TabIndex = 80;
            VisaRenewDateLabel.Text = "Start Date";
            // 
            // VisaExpiryDateLabel
            // 
            VisaExpiryDateLabel.AutoSize = true;
            VisaExpiryDateLabel.Location = new System.Drawing.Point(270, 172);
            VisaExpiryDateLabel.Name = "VisaExpiryDateLabel";
            VisaExpiryDateLabel.Size = new System.Drawing.Size(52, 13);
            VisaExpiryDateLabel.TabIndex = 79;
            VisaExpiryDateLabel.Text = "End Date";
            // 
            // lblLeasePeriod
            // 
            lblLeasePeriod.AutoSize = true;
            lblLeasePeriod.Location = new System.Drawing.Point(11, 197);
            lblLeasePeriod.Name = "lblLeasePeriod";
            lblLeasePeriod.Size = new System.Drawing.Size(69, 13);
            lblLeasePeriod.TabIndex = 84;
            lblLeasePeriod.Text = "Lease Period";
            // 
            // VisaIssueDateLabel
            // 
            VisaIssueDateLabel.AutoSize = true;
            VisaIssueDateLabel.Location = new System.Drawing.Point(11, 225);
            VisaIssueDateLabel.Name = "VisaIssueDateLabel";
            VisaIssueDateLabel.Size = new System.Drawing.Size(82, 13);
            VisaIssueDateLabel.TabIndex = 83;
            VisaIssueDateLabel.Text = "Document Date";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(11, 90);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(68, 13);
            label3.TabIndex = 87;
            label3.Text = "Rental Value";
            // 
            // Label6
            // 
            Label6.AutoSize = true;
            Label6.Location = new System.Drawing.Point(11, 144);
            Label6.Name = "Label6";
            Label6.Size = new System.Drawing.Size(89, 13);
            Label6.TabIndex = 92;
            Label6.Text = "Contact Numbers";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(11, 118);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(41, 13);
            Label5.TabIndex = 93;
            Label5.Text = "Tenant";
            // 
            // Label7
            // 
            Label7.AutoSize = true;
            Label7.Location = new System.Drawing.Point(11, 251);
            Label7.Name = "Label7";
            Label7.Size = new System.Drawing.Size(45, 13);
            Label7.TabIndex = 90;
            Label7.Text = "Address";
            // 
            // Label8
            // 
            Label8.AutoSize = true;
            Label8.Location = new System.Drawing.Point(8, 15);
            Label8.Name = "Label8";
            Label8.Size = new System.Drawing.Size(55, 13);
            Label8.TabIndex = 31;
            Label8.Text = "Land Lord";
            // 
            // Label15
            // 
            Label15.AutoSize = true;
            Label15.Location = new System.Drawing.Point(9, 172);
            Label15.Name = "Label15";
            Label15.Size = new System.Drawing.Size(31, 13);
            Label15.TabIndex = 25;
            Label15.Text = "Total";
            // 
            // Label14
            // 
            Label14.AutoSize = true;
            Label14.Location = new System.Drawing.Point(9, 146);
            Label14.Name = "Label14";
            Label14.Size = new System.Drawing.Size(65, 13);
            Label14.TabIndex = 26;
            Label14.Text = "Plot Number";
            // 
            // Label12
            // 
            Label12.AutoSize = true;
            Label12.Location = new System.Drawing.Point(9, 94);
            Label12.Name = "Label12";
            Label12.Size = new System.Drawing.Size(84, 13);
            Label12.TabIndex = 28;
            Label12.Text = "Building Number";
            // 
            // Label11
            // 
            Label11.AutoSize = true;
            Label11.Location = new System.Drawing.Point(9, 120);
            Label11.Name = "Label11";
            Label11.Size = new System.Drawing.Size(48, 13);
            Label11.TabIndex = 27;
            Label11.Text = "Location";
            // 
            // Label9
            // 
            Label9.AutoSize = true;
            Label9.Location = new System.Drawing.Point(8, 41);
            Label9.Name = "Label9";
            Label9.Size = new System.Drawing.Size(66, 13);
            Label9.TabIndex = 30;
            Label9.Text = "Unit Number";
            // 
            // EmployeeInsuranceCardBindingNavigator
            // 
            this.EmployeeInsuranceCardBindingNavigator.AddNewItem = null;
            this.EmployeeInsuranceCardBindingNavigator.CountItem = this.bnCountItem;
            this.EmployeeInsuranceCardBindingNavigator.DeleteItem = null;
            this.EmployeeInsuranceCardBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.BindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.ToolStripSeparator1,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnCancel,
            this.BindingNavigatorSeparator2,
            this.bnMoreActions,
            this.ToolStripSeparator3,
            this.bnPrint,
            this.bnEmail,
            this.ToolStripSeparator2,
            this.bnHelp});
            this.EmployeeInsuranceCardBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeInsuranceCardBindingNavigator.MoveFirstItem = null;
            this.EmployeeInsuranceCardBindingNavigator.MoveLastItem = null;
            this.EmployeeInsuranceCardBindingNavigator.MoveNextItem = null;
            this.EmployeeInsuranceCardBindingNavigator.MovePreviousItem = null;
            this.EmployeeInsuranceCardBindingNavigator.Name = "EmployeeInsuranceCardBindingNavigator";
            this.EmployeeInsuranceCardBindingNavigator.PositionItem = this.bnPositionItem;
            this.EmployeeInsuranceCardBindingNavigator.Size = new System.Drawing.Size(469, 25);
            this.EmployeeInsuranceCardBindingNavigator.TabIndex = 71;
            this.EmployeeInsuranceCardBindingNavigator.Text = "BindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.bnMoveFirstItem_Click);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.bnMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.bnMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.bnMoveLastItem_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add New";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Remove";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // bnCancel
            // 
            this.bnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnCancel.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.bnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(23, 22);
            this.bnCancel.Text = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.bnCancel_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentsToolStripMenuItem,
            this.toolStripSeparator6,
            this.receiptToolStripMenuItem,
            this.issueToolStripMenuItem1,
            this.toolStripSeparator4,
            this.renewToolStripMenuItem});
            this.bnMoreActions.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "&Actions";
            // 
            // documentsToolStripMenuItem
            // 
            this.documentsToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.documentsToolStripMenuItem.Name = "documentsToolStripMenuItem";
            this.documentsToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.documentsToolStripMenuItem.Text = "&Documents";
            this.documentsToolStripMenuItem.Click += new System.EventHandler(this.documentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(132, 6);
            // 
            // receiptToolStripMenuItem
            // 
            this.receiptToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.receiptToolStripMenuItem.Name = "receiptToolStripMenuItem";
            this.receiptToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.receiptToolStripMenuItem.Text = "R&eceipt";
            this.receiptToolStripMenuItem.Click += new System.EventHandler(this.receiptToolStripMenuItem_Click);
            // 
            // issueToolStripMenuItem1
            // 
            this.issueToolStripMenuItem1.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.issueToolStripMenuItem1.Name = "issueToolStripMenuItem1";
            this.issueToolStripMenuItem1.Size = new System.Drawing.Size(135, 22);
            this.issueToolStripMenuItem1.Text = "&Issue";
            this.issueToolStripMenuItem1.Click += new System.EventHandler(this.issueToolStripMenuItem1_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(132, 6);
            // 
            // renewToolStripMenuItem
            // 
            this.renewToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.renew;
            this.renewToolStripMenuItem.Name = "renewToolStripMenuItem";
            this.renewToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.renewToolStripMenuItem.Text = "Re&new";
            this.renewToolStripMenuItem.Click += new System.EventHandler(this.renewToolStripMenuItem_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPrint
            // 
            this.bnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnHelp
            // 
            this.bnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.Text = "He&lp";
            this.bnHelp.Click += new System.EventHandler(this.bnHelp_Click);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(469, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 89;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code | Card Number";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Tooltip = "Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // stsStatus
            // 
            this.stsStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.stsStatus.Location = new System.Drawing.Point(0, 440);
            this.stsStatus.Name = "stsStatus";
            this.stsStatus.Size = new System.Drawing.Size(469, 22);
            this.stsStatus.TabIndex = 90;
            this.stsStatus.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // ContractInfo
            // 
            this.ContractInfo.AutoScroll = true;
            this.ContractInfo.Controls.Add(this.lblRenewed);
            this.ContractInfo.Controls.Add(this.txtAddress);
            this.ContractInfo.Controls.Add(this.txtContactNumbers);
            this.ContractInfo.Controls.Add(Label6);
            this.ContractInfo.Controls.Add(this.txtTenant);
            this.ContractInfo.Controls.Add(Label5);
            this.ContractInfo.Controls.Add(Label7);
            this.ContractInfo.Controls.Add(this.chkDepositHeld);
            this.ContractInfo.Controls.Add(this.txtRentalValue);
            this.ContractInfo.Controls.Add(label3);
            this.ContractInfo.Controls.Add(this.txtLeasePeriod);
            this.ContractInfo.Controls.Add(lblLeasePeriod);
            this.ContractInfo.Controls.Add(this.dtpDocumentDate);
            this.ContractInfo.Controls.Add(VisaIssueDateLabel);
            this.ContractInfo.Controls.Add(VisaRenewDateLabel);
            this.ContractInfo.Controls.Add(this.dtpStartDate);
            this.ContractInfo.Controls.Add(this.dtpEndDate);
            this.ContractInfo.Controls.Add(VisaExpiryDateLabel);
            this.ContractInfo.Controls.Add(label2);
            this.ContractInfo.Controls.Add(this.txtLessor);
            this.ContractInfo.Controls.Add(this.txtAgreementNumber);
            this.ContractInfo.Controls.Add(label1);
            this.ContractInfo.Controls.Add(this.cboCompany);
            this.ContractInfo.Controls.Add(lblEmployee);
            this.ContractInfo.Location = new System.Drawing.Point(4, 22);
            this.ContractInfo.Name = "ContractInfo";
            this.ContractInfo.Padding = new System.Windows.Forms.Padding(3);
            this.ContractInfo.Size = new System.Drawing.Size(445, 326);
            this.ContractInfo.TabIndex = 0;
            this.ContractInfo.Tag = "0";
            this.ContractInfo.Text = "Contract Info";
            this.ContractInfo.UseVisualStyleBackColor = true;
            // 
            // lblRenewed
            // 
            this.lblRenewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenewed.ForeColor = System.Drawing.Color.Maroon;
            this.lblRenewed.Location = new System.Drawing.Point(340, 201);
            this.lblRenewed.Name = "lblRenewed";
            this.lblRenewed.Size = new System.Drawing.Size(87, 22);
            this.lblRenewed.TabIndex = 115;
            this.lblRenewed.Text = "Renewed";
            this.lblRenewed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRenewed.Visible = false;
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress.Location = new System.Drawing.Point(133, 251);
            this.txtAddress.MaxLength = 500;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAddress.Size = new System.Drawing.Size(294, 69);
            this.txtAddress.TabIndex = 12;
            this.txtAddress.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtContactNumbers
            // 
            this.txtContactNumbers.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactNumbers.Location = new System.Drawing.Point(133, 144);
            this.txtContactNumbers.MaxLength = 50;
            this.txtContactNumbers.Name = "txtContactNumbers";
            this.txtContactNumbers.Size = new System.Drawing.Size(294, 20);
            this.txtContactNumbers.TabIndex = 7;
            this.txtContactNumbers.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtTenant
            // 
            this.txtTenant.BackColor = System.Drawing.SystemColors.Window;
            this.txtTenant.Location = new System.Drawing.Point(133, 118);
            this.txtTenant.MaxLength = 50;
            this.txtTenant.Name = "txtTenant";
            this.txtTenant.Size = new System.Drawing.Size(294, 20);
            this.txtTenant.TabIndex = 6;
            this.txtTenant.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // chkDepositHeld
            // 
            this.chkDepositHeld.AutoSize = true;
            this.chkDepositHeld.Location = new System.Drawing.Point(340, 93);
            this.chkDepositHeld.Name = "chkDepositHeld";
            this.chkDepositHeld.Size = new System.Drawing.Size(87, 17);
            this.chkDepositHeld.TabIndex = 5;
            this.chkDepositHeld.Text = "Deposit Held";
            this.chkDepositHeld.UseVisualStyleBackColor = true;
            this.chkDepositHeld.CheckedChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtRentalValue
            // 
            this.txtRentalValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtRentalValue.Location = new System.Drawing.Point(133, 90);
            this.txtRentalValue.MaxLength = 13;
            this.txtRentalValue.Name = "txtRentalValue";
            this.txtRentalValue.Size = new System.Drawing.Size(157, 20);
            this.txtRentalValue.TabIndex = 4;
            this.txtRentalValue.TextChanged += new System.EventHandler(this.OnInputChanged);
            this.txtRentalValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // txtLeasePeriod
            // 
            this.txtLeasePeriod.BackColor = System.Drawing.SystemColors.Window;
            this.txtLeasePeriod.Location = new System.Drawing.Point(133, 197);
            this.txtLeasePeriod.MaxLength = 13;
            this.txtLeasePeriod.Name = "txtLeasePeriod";
            this.txtLeasePeriod.Size = new System.Drawing.Size(99, 20);
            this.txtLeasePeriod.TabIndex = 10;
            this.txtLeasePeriod.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpDocumentDate
            // 
            this.dtpDocumentDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDocumentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDocumentDate.Location = new System.Drawing.Point(133, 225);
            this.dtpDocumentDate.Name = "dtpDocumentDate";
            this.dtpDocumentDate.Size = new System.Drawing.Size(99, 20);
            this.dtpDocumentDate.TabIndex = 11;
            this.dtpDocumentDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(133, 172);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(99, 20);
            this.dtpStartDate.TabIndex = 8;
            this.dtpStartDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Location = new System.Drawing.Point(328, 172);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(99, 20);
            this.dtpEndDate.TabIndex = 9;
            this.dtpEndDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtLessor
            // 
            this.txtLessor.BackColor = System.Drawing.SystemColors.Info;
            this.txtLessor.Location = new System.Drawing.Point(133, 64);
            this.txtLessor.MaxLength = 50;
            this.txtLessor.Name = "txtLessor";
            this.txtLessor.Size = new System.Drawing.Size(294, 20);
            this.txtLessor.TabIndex = 3;
            this.txtLessor.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtAgreementNumber
            // 
            this.txtAgreementNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtAgreementNumber.Location = new System.Drawing.Point(133, 38);
            this.txtAgreementNumber.MaxLength = 50;
            this.txtAgreementNumber.Name = "txtAgreementNumber";
            this.txtAgreementNumber.Size = new System.Drawing.Size(294, 20);
            this.txtAgreementNumber.TabIndex = 2;
            this.txtAgreementNumber.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DisplayMember = "EmployeeID";
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.Location = new System.Drawing.Point(133, 10);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(294, 21);
            this.cboCompany.TabIndex = 1;
            this.cboCompany.ValueMember = "EmployeeID";
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // LeaseTab
            // 
            this.LeaseTab.Controls.Add(this.ContractInfo);
            this.LeaseTab.Controls.Add(this.BuildingInfo);
            this.LeaseTab.Location = new System.Drawing.Point(6, 56);
            this.LeaseTab.Name = "LeaseTab";
            this.LeaseTab.SelectedIndex = 0;
            this.LeaseTab.Size = new System.Drawing.Size(453, 352);
            this.LeaseTab.TabIndex = 91;
            // 
            // BuildingInfo
            // 
            this.BuildingInfo.Controls.Add(this.txtRemarks);
            this.BuildingInfo.Controls.Add(this.label4);
            this.BuildingInfo.Controls.Add(this.lblLeaseUnitType);
            this.BuildingInfo.Controls.Add(this.txtLandlord);
            this.BuildingInfo.Controls.Add(Label8);
            this.BuildingInfo.Controls.Add(this.btnLeaseUnitType);
            this.BuildingInfo.Controls.Add(this.txtBuildingNumber);
            this.BuildingInfo.Controls.Add(this.cboLeaseUnitType);
            this.BuildingInfo.Controls.Add(Label15);
            this.BuildingInfo.Controls.Add(this.txtTotal);
            this.BuildingInfo.Controls.Add(Label14);
            this.BuildingInfo.Controls.Add(this.txtPlotNumber);
            this.BuildingInfo.Controls.Add(Label12);
            this.BuildingInfo.Controls.Add(Label11);
            this.BuildingInfo.Controls.Add(this.txtUnitNumber);
            this.BuildingInfo.Controls.Add(this.txtLocation);
            this.BuildingInfo.Controls.Add(Label9);
            this.BuildingInfo.Controls.Add(this.shapeContainer1);
            this.BuildingInfo.Location = new System.Drawing.Point(4, 22);
            this.BuildingInfo.Name = "BuildingInfo";
            this.BuildingInfo.Padding = new System.Windows.Forms.Padding(3);
            this.BuildingInfo.Size = new System.Drawing.Size(445, 326);
            this.BuildingInfo.TabIndex = 1;
            this.BuildingInfo.Text = "Building Info";
            this.BuildingInfo.UseVisualStyleBackColor = true;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(11, 226);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(417, 94);
            this.txtRemarks.TabIndex = 9;
            this.txtRemarks.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 59;
            this.label4.Text = "Remarks";
            // 
            // lblLeaseUnitType
            // 
            this.lblLeaseUnitType.AutoSize = true;
            this.lblLeaseUnitType.Location = new System.Drawing.Point(8, 67);
            this.lblLeaseUnitType.Name = "lblLeaseUnitType";
            this.lblLeaseUnitType.Size = new System.Drawing.Size(85, 13);
            this.lblLeaseUnitType.TabIndex = 29;
            this.lblLeaseUnitType.Text = "Lease Unit Type";
            // 
            // txtLandlord
            // 
            this.txtLandlord.BackColor = System.Drawing.SystemColors.Window;
            this.txtLandlord.Location = new System.Drawing.Point(108, 15);
            this.txtLandlord.MaxLength = 50;
            this.txtLandlord.Name = "txtLandlord";
            this.txtLandlord.Size = new System.Drawing.Size(320, 20);
            this.txtLandlord.TabIndex = 1;
            this.txtLandlord.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // btnLeaseUnitType
            // 
            this.btnLeaseUnitType.Location = new System.Drawing.Point(396, 65);
            this.btnLeaseUnitType.Name = "btnLeaseUnitType";
            this.btnLeaseUnitType.Size = new System.Drawing.Size(32, 23);
            this.btnLeaseUnitType.TabIndex = 4;
            this.btnLeaseUnitType.Text = "...";
            this.btnLeaseUnitType.UseVisualStyleBackColor = true;
            this.btnLeaseUnitType.Click += new System.EventHandler(this.btnLeaseUnitType_Click);
            // 
            // txtBuildingNumber
            // 
            this.txtBuildingNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtBuildingNumber.Location = new System.Drawing.Point(108, 94);
            this.txtBuildingNumber.MaxLength = 30;
            this.txtBuildingNumber.Name = "txtBuildingNumber";
            this.txtBuildingNumber.Size = new System.Drawing.Size(282, 20);
            this.txtBuildingNumber.TabIndex = 5;
            this.txtBuildingNumber.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // cboLeaseUnitType
            // 
            this.cboLeaseUnitType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLeaseUnitType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLeaseUnitType.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboLeaseUnitType.DisplayMember = "EmployeeID";
            this.cboLeaseUnitType.DropDownHeight = 134;
            this.cboLeaseUnitType.FormattingEnabled = true;
            this.cboLeaseUnitType.IntegralHeight = false;
            this.cboLeaseUnitType.Location = new System.Drawing.Point(108, 67);
            this.cboLeaseUnitType.Name = "cboLeaseUnitType";
            this.cboLeaseUnitType.Size = new System.Drawing.Size(282, 21);
            this.cboLeaseUnitType.TabIndex = 3;
            this.cboLeaseUnitType.ValueMember = "EmployeeID";
            this.cboLeaseUnitType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboLeaseUnitType_KeyPress);
            this.cboLeaseUnitType.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotal.Location = new System.Drawing.Point(108, 172);
            this.txtTotal.MaxLength = 13;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(138, 20);
            this.txtTotal.TabIndex = 8;
            this.txtTotal.TextChanged += new System.EventHandler(this.OnInputChanged);
            this.txtTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // txtPlotNumber
            // 
            this.txtPlotNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtPlotNumber.Location = new System.Drawing.Point(108, 146);
            this.txtPlotNumber.MaxLength = 30;
            this.txtPlotNumber.Name = "txtPlotNumber";
            this.txtPlotNumber.Size = new System.Drawing.Size(282, 20);
            this.txtPlotNumber.TabIndex = 7;
            this.txtPlotNumber.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtUnitNumber
            // 
            this.txtUnitNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtUnitNumber.Location = new System.Drawing.Point(108, 41);
            this.txtUnitNumber.MaxLength = 30;
            this.txtUnitNumber.Name = "txtUnitNumber";
            this.txtUnitNumber.Size = new System.Drawing.Size(282, 20);
            this.txtUnitNumber.TabIndex = 2;
            this.txtUnitNumber.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtLocation
            // 
            this.txtLocation.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocation.Location = new System.Drawing.Point(108, 120);
            this.txtLocation.MaxLength = 50;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(320, 20);
            this.txtLocation.TabIndex = 6;
            this.txtLocation.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(439, 320);
            this.shapeContainer1.TabIndex = 60;
            this.shapeContainer1.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.Color.Silver;
            this.LineShape1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 19;
            this.LineShape1.X2 = 428;
            this.LineShape1.Y1 = 204;
            this.LineShape1.Y2 = 204;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.Visible = false;
            this.lineShape2.X1 = 70;
            this.lineShape2.X2 = 458;
            this.lineShape2.Y1 = 18;
            this.lineShape2.Y2 = 18;
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(3, 414);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 92;
            this.BtnSave.Text = "&Save";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(384, 414);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 94;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(304, 414);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 93;
            this.BtnOk.Text = "&OK";
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // errLease
            // 
            this.errLease.ContainerControl = this;
            this.errLease.RightToLeft = true;
            // 
            // tmLease
            // 
            this.tmLease.Interval = 2000;
            this.tmLease.Tick += new System.EventHandler(this.tmLease_Tick);
            // 
            // frmCompanyLeaseAgreement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 462);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.LeaseTab);
            this.Controls.Add(this.stsStatus);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.EmployeeInsuranceCardBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCompanyLeaseAgreement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lease Agreement";
            this.Load += new System.EventHandler(this.frmCompanyLeaseAgreement_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCompanyLeaseAgreement_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCompanyLeaseAgreement_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeInsuranceCardBindingNavigator)).EndInit();
            this.EmployeeInsuranceCardBindingNavigator.ResumeLayout(false);
            this.EmployeeInsuranceCardBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.stsStatus.ResumeLayout(false);
            this.stsStatus.PerformLayout();
            this.ContractInfo.ResumeLayout(false);
            this.ContractInfo.PerformLayout();
            this.LeaseTab.ResumeLayout(false);
            this.BuildingInfo.ResumeLayout(false);
            this.BuildingInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errLease)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator EmployeeInsuranceCardBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel bnCountItem;
        internal System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox bnPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton bnMoveNextItem;
        internal System.Windows.Forms.ToolStripButton bnMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        internal System.Windows.Forms.ToolStripButton bnCancel;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        private System.Windows.Forms.ToolStripMenuItem documentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem receiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem issueToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton bnPrint;
        internal System.Windows.Forms.ToolStripButton bnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.StatusStrip stsStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.TabPage ContractInfo;
        internal System.Windows.Forms.TabControl LeaseTab;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.TabPage BuildingInfo;
        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.TextBox txtLessor;
        internal System.Windows.Forms.TextBox txtAgreementNumber;
        internal System.Windows.Forms.TextBox txtLeasePeriod;
        internal System.Windows.Forms.DateTimePicker dtpDocumentDate;
        internal System.Windows.Forms.DateTimePicker dtpStartDate;
        internal System.Windows.Forms.DateTimePicker dtpEndDate;
        internal System.Windows.Forms.TextBox txtAddress;
        internal System.Windows.Forms.TextBox txtContactNumbers;
        internal System.Windows.Forms.TextBox txtTenant;
        internal System.Windows.Forms.CheckBox chkDepositHeld;
        internal System.Windows.Forms.TextBox txtRentalValue;
        internal System.Windows.Forms.Label lblLeaseUnitType;
        internal System.Windows.Forms.TextBox txtLandlord;
        internal System.Windows.Forms.Button btnLeaseUnitType;
        internal System.Windows.Forms.TextBox txtBuildingNumber;
        internal System.Windows.Forms.ComboBox cboLeaseUnitType;
        internal System.Windows.Forms.TextBox txtTotal;
        internal System.Windows.Forms.TextBox txtPlotNumber;
        internal System.Windows.Forms.TextBox txtUnitNumber;
        internal System.Windows.Forms.TextBox txtLocation;
        internal System.Windows.Forms.Label label4;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.ErrorProvider errLease;
        internal System.Windows.Forms.Timer tmLease;
        private System.Windows.Forms.ToolStripMenuItem renewToolStripMenuItem;
        private System.Windows.Forms.Label lblRenewed;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    }
}