﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{
    public partial class frmAlertreport : Form
    {
        clsBLLAlertreport objBLLalertReport;
        private string strMReportPath = "";
        public int intUserID = ClsCommonSettings.UserID;
        public frmAlertreport()
        {
            InitializeComponent();
            objBLLalertReport = new clsBLLAlertreport();
        }
        private void LoadCombos(int intType)
        {
            DataTable datCombos = new DataTable();
            //if (intType == 0 || intType == 1)
            //{
            //    datCombos = null;
            //    datCombos = objBLLalertReport.FillCombos("SELECT -1 AS EmployeeID ,'ALL' AS EmployeeFullName UNION SELECT EmployeeID,EmployeeFullName FROM  EmployeeMaster");
            //    cboAlertReport.ValueMember = "EmployeeID";
            //    cboAlertReport.DisplayMember = "EmployeeFullName";
            //    cboAlertReport.DataSource = datCombos;
            //    cboAlertReport.SelectedValue = -1;
            //}

            if (intType == 0 || intType == 2)
            {
                datCombos = null;
                datCombos = objBLLalertReport.FillCombos(" SELECT DocumentTypeID,Description FROM  DocumentTypeReference  WHERE DocumentTypeID NOT IN(7,10,11,12,13) ORDER BY Predefined DESC ");
                cboDocType.ValueMember = "DocumentTypeID";
                cboDocType.DisplayMember = "Description";
                cboDocType.DataSource = datCombos;

            }
            if (intType == 3)
            {
                datCombos = null;
                datCombos = objBLLalertReport.FillCombos("SELECT -1 AS DocumentTypeID ,'ALL' AS Description UNION SELECT DocumentTypeID,Description FROM  DocumentTypeReference WHERE DocumentTypeID NOT IN(7,10,11,12,13)");
                cboDocType.ValueMember = "DocumentTypeID";
                cboDocType.DisplayMember = "Description";
                cboDocType.DataSource = datCombos;
                cboDocType.SelectedValue = -1;
            }
        }
        private void ShowReport()
        {
            this.RptView.Reset();
          
            int intDocTypeID = 0;
          
            int intAlertType = 0;
            if (cboAlertReport.SelectedIndex == 0)
            {
                intAlertType = 1;
            }
            else
            {
                intAlertType = 0;
            }
            string strFromDate = "";
            string strToDate = "";
            strFromDate = dtpFromDate.Value.ToString("dd-MMM-yyyy");
            strToDate = dtpToDate.Value.ToString("dd-MMM-yyyy");
            intDocTypeID = cboDocType.SelectedValue.ToInt32();
           
            


            DataTable dtCompany = new clsBLLReportViewer().DisplayCompanyDefaultHeader();

            if (ClsCommonSettings.IsArabicView)
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\rptAlertInfoReportArb.rdlc";

            }
            else
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\rptAlertInfoReport.rdlc";

            }

            this.RptView.ProcessingMode = ProcessingMode.Local;
            this.RptView.LocalReport.ReportPath = strMReportPath;

           
                ReportParameter[] RepParam = new ReportParameter[1];
                RepParam[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);


                this.RptView.LocalReport.SetParameters(RepParam);
                this.RptView.LocalReport.DataSources.Clear();
                DataTable dtAerts = objBLLalertReport.GetAlerts(intDocTypeID,intUserID,strFromDate,strToDate,intAlertType);

                RptView.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                if (dtAerts.Rows.Count > 0)
                {


                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtAlertReport", dtAerts));


                }
                else
                {
                    MessageBox.Show("No Information found");
                }

                

            
            
            this.RptView.SetDisplayMode(DisplayMode.PrintLayout);
            this.RptView.ZoomMode = ZoomMode.Percent;
            this.RptView.ZoomPercent = 100;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ShowReport();
        }

        private void frmAlertreport_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
        }
    }

}
