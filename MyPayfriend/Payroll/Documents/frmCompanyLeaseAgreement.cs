﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace MyPayfriend
{
    /// <summary>
    ///  Created By         : Laxmi
    ///  Created Date       : 28 Aug 2013
    ///  Purpose            : Lease Agreement transactions
    ///  FormID             : 187
    ///  ErrorCode          : 18650-
    ///  Functionalities    : Receipt/ Issue/ Attach documents
    ///                    
    /// </summary>
    /// 

    public partial class frmCompanyLeaseAgreement : Form
    {
        #region Variable declarations

        // Public variable declarations
        public int CompanyID  {get; set;}        // Public variable From Company form
        public int LeaseID = 0;                 //Public variable From Navigator


        // Private variable declarations
        bool MblnAddStatus = true;
        string MstrDocName = string.Empty;


        private int MintTimerInterval;                  // To set timer interval
        private int MintCurrentRecCnt;                  // Current recourd count
        private int MintRecordCnt;                      // Total record count

        private bool MblnAddPermission = false;         //To Set Add Permission
        private bool MblnUpdatePermission = false;      //To Set Update Permission
        private bool MblnDeletePermission = false;      //To Set Delete Permission
        private bool MblnPrintEmailPermission = false;  //To Set PrintEmail Permission
        private bool mblnSearchStatus = false;
        bool mblnAddIssue, mblnAddReceipt, mblnAddRenew, mblnEmail, mblnDelete, mblnUpdate = false;  // Receipt-Issue Permission

        private string MsMessageCommon;
        private string MsMessageCaption;

        private ArrayList MsMessageArray;   // Error Message display
        private ArrayList MaStatusMessage;

        MessageBoxIcon MsMessageBoxIcon;

        #endregion VariableDeclarations

        clsBLLLeaseAgreement objBLLLeaseAgreement = null;
        clsBLLPermissionSettings objPermission;
        
        ClsLogWriter objClsLogWriter;       // Object of the LogWriter class
        ClsNotification objClsNotification; // Object of the Notification class
        private string strOf = "of ";
        #region Constructor
        public frmCompanyLeaseAgreement()
        {
            InitializeComponent();

            objPermission = new clsBLLPermissionSettings();
            objBLLLeaseAgreement  = new clsBLLLeaseAgreement ();
         
            objClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
            objClsNotification = new ClsNotification();

            tmLease.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer
            MintTimerInterval = ClsCommonSettings.TimerInterval; // current companyid
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }
        #endregion Constructor

        #region Events

        // Form Events
        private void frmCompanyLeaseAgreement_Load(object sender, EventArgs e)
        {
            //if (CompanyID == 0)
            //    CompanyID = ClsCommonSettings.CurrentCompanyID;

            SetAutoCompleteList();

            LoadMessage();
            LoadCombos();
            SetPermissions();

            LoadInitials();
            cboCompany.Select(); 

            if (LeaseID  > 0) //From navigator document view
            {
                MintCurrentRecCnt = 1;
                GetRecordCount();
                GetLeaseDetails();
                // Search button disabled
                bSearch.Enabled = false;
                lblStatus.Text = string.Empty;
                tmLease.Enabled = true;
            }
            else if (CompanyID > 0) // From Company- Load all agreement of company
            {
                GetRecordCount();
                if (MintRecordCnt == 0) // No records
                {
                    MintRecordCnt = 1;
                    bnCountItem.Text = MintRecordCnt.ToString();
                    bnPositionItem.Text = MintCurrentRecCnt.ToString();

                }
                else if (MintRecordCnt > 0)
                    bnMoveFirstItem_Click(sender, e);

                txtAgreementNumber.Select();
            }      
        }
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.LeaseAgreement, this);
            bnMoreActions.Text = "الإجراءات";
            bnMoreActions.ToolTipText = "الإجراءات";
            documentsToolStripMenuItem.Text = "وثائق";
            receiptToolStripMenuItem.Text = "استلام";
            issueToolStripMenuItem1.Text = "قضية";
            renewToolStripMenuItem.Text = "جدد";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            strOf = "من ";
        }
        private void frmCompanyLeaseAgreement_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnOk.Enabled)
            {
                // Checking the changes are not saved and shows warning to the user
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 8, out MsMessageBoxIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                 
                    objBLLLeaseAgreement = null;
                    objPermission = null;
               
                    objClsLogWriter = null;
                    objClsNotification = null;

                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void frmCompanyLeaseAgreement_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        bnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (bnAddNewItem.Enabled) bnAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (bnDeleteItem.Enabled) bnDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (bnCancel.Enabled) bnCancel_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (bnMovePreviousItem.Enabled) bnMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        if (bnMoveNextItem.Enabled) bnMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        if (bnMoveFirstItem.Enabled) bnMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        if (bnMoveLastItem.Enabled) bnMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (bnPrint.Enabled) bnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (bnEmail.Enabled) bnEmail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.Alt | Keys.I:
                        if (issueToolStripMenuItem1.Enabled)
                            issueToolStripMenuItem1_Click(sender, new EventArgs());  //issue document
                        break;
                    case Keys.Alt | Keys.E:
                        if (receiptToolStripMenuItem.Enabled)
                            receiptToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                        break;
                    case Keys.Alt | Keys.D:
                        if (documentsToolStripMenuItem.Enabled)
                            documentsToolStripMenuItem_Click(sender, new EventArgs()); // issue document
                        break;
                    case Keys.Alt | Keys.N:
                        if (renewToolStripMenuItem.Enabled)
                            renewToolStripMenuItem_Click(sender, new EventArgs());// renew document
                        break;
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on frmCompanyLeaseAgreement_KeyDown() " + this.Name + " " + ex.Message.ToString(), 1); }
        }

        //Button Events
        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            LoadInitials(); 
        }

        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveLeaseAgreement()) DoActionsAfterSave();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (SaveLeaseAgreement()) DoActionsAfterSave();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (SaveLeaseAgreement())
            {
                BtnOk.Enabled = false;
                this.Close();
            }
        }
        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            if (txtAgreementNumber.Tag.ToInt32() > 0)
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 13, out MsMessageBoxIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                DeleteLeaseAgreement();
                //objAlerts.UpdateAlerts();
                LoadInitials();
                SetAutoCompleteList();
            }
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            LoadInitials();
        }

        private void bnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        private void bnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAgreementNumber.Tag.ToInt32() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Company LeaseAgreement";
                        ObjEmailPopUp.EmailFormType = EmailFormID.LeaseAgreement;
                        ObjEmailPopUp.EmailSource = objBLLLeaseAgreement.DisplayLeaseAgreementReport(txtAgreementNumber.Tag.ToInt32());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 1); }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void bnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "Lease"; Help.ShowDialog(); }
        }
        //Button navigation Events
        private void bnMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            GetRecordCount();   // get no of records
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;          // set current record
                GetLeaseDetails();     // display qualification details

                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 9, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmLease.Enabled = true;
            }
        }

        private void bnMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty; 
            GetRecordCount();   // get no of records
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                    MintCurrentRecCnt = 1;
                else
                {
                    GetLeaseDetails();     // display Lease agreement details

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmLease.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 1;
        }

        private void bnMoveNextItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            GetRecordCount();   // get no of records
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                    MintCurrentRecCnt = 1;
                else
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;

                if (MintCurrentRecCnt > MintRecordCnt)
                    MintCurrentRecCnt = MintRecordCnt;
                else
                {
                    GetLeaseDetails();

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 11, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmLease.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 0;
        }

        private void bnMoveLastItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    GetLeaseDetails();

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 12, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmLease.Enabled = true;

                }
            }
        }

        // Document/Receipt/Issue Button events
        private void documentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int iCardID = txtAgreementNumber.Tag.ToInt32();
                if (iCardID > 0)
                {
                    using (FrmScanning objScanning = new FrmScanning())
                    {
                        objScanning.MintDocumentTypeid = (int)DocumentType.Lease_Agreement;
                        objScanning.MlngReferenceID = cboCompany.SelectedValue.ToInt32();
                        objScanning.MintOperationTypeID = (int)OperationType.Company;
                        objScanning.MstrReferenceNo = txtAgreementNumber.Text;
                        objScanning.PblnEditable = true;
                        objScanning.MintVendorID = cboCompany.SelectedValue.ToInt32();
                        objScanning.MintDocumentID = iCardID;
                        objScanning.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on documentsToolStripMenuItem_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }

        private void receiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocID = txtAgreementNumber.Tag.ToInt32();
                new frmDocumentReceiptIssue()
                {
                    eOperationType = OperationType.Company,
                    eDocumentType = DocumentType.Lease_Agreement,
                    DocumentID = iDocID,
                    DocumentNumber = txtAgreementNumber.Text,
                    EmployeeID = cboCompany.SelectedValue.ToInt32(),
                    ExpiryDate = dtpEndDate.Value

                }.ShowDialog();
                SetReceiptOrIssueLabel();
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on receiptToolStripMenuItem_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }

        private void issueToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocID = txtAgreementNumber.Tag.ToInt32();
                new frmDocumentReceiptIssue()
                {
                    eOperationType = OperationType.Company,
                    eDocumentType = DocumentType.Lease_Agreement,
                    DocumentID = iDocID,
                    DocumentNumber = txtAgreementNumber.Text,
                    EmployeeID = cboCompany.SelectedValue.ToInt32()

                }.ShowDialog();
                SetReceiptOrIssueLabel();
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on issueToolStripMenuItem1_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }

     
        // Reference Button events
        private void btnLeaseUnitType_Click(object sender, EventArgs e)
        {
            try
            {
                int LeaseUnitTypeID = Convert.ToInt32(cboLeaseUnitType.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Lease Unittype", new int[] { 1, 0, MintTimerInterval }, "LeaseUnitTypeID,Description,DescriptionArb", "LeaseUnitTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadLeaseUnitType();

              
                if (objCommon.NewID != 0)
                    cboLeaseUnitType.SelectedValue = objCommon.NewID;
                else
                    cboLeaseUnitType.SelectedValue = LeaseUnitTypeID;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on btnLeaseUnitType_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnLeaseUnitType_Click" + ex.Message.ToString());
            }
        }
        //Serach button Events
        private void btnSearch_Click(object sender, EventArgs e)
        {

            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;
            GetRecordCount();

            if (MintRecordCnt == 0) // No records
            {
                MessageBox.Show(objClsNotification.GetErrorMessage(MsMessageArray, 40, out MsMessageBoxIcon).Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
            }
            else if (MintRecordCnt > 0)
            {
                AddNewItem();
                bnMoveFirstItem_Click(sender, e);
            }
        }

        private void cboLeaseUnitType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboLeaseUnitType.DroppedDown = false;
        }
        // Textbox events
        public void OnInputChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }
      

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                if (e.KeyCode == Keys.Enter)
                    btnSearch_Click(sender, null);
            }
        }
        //timer events
        public void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;
            e.Handled = false;
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            }
            else
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            }

            if (ClsCommonSettings.IsAmountRoundByZero == false && ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        private void renewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAgreementNumber.Tag.ToInt32() > 0)
                {
                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10653, out MsMessageBoxIcon); // Do you wish to renew

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    else //YES Do you wish to add
                    {
                        objBLLLeaseAgreement.objDTOLeaseAgreement.LeaseID = txtAgreementNumber.Tag.ToInt32();
                        objBLLLeaseAgreement.UpdateLeaseForRenewal();

                        GetRecordCount();
                        GetLeaseDetails();

                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10654, out MsMessageBoxIcon); // Do you wish to add
                        if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                        AddNewItem();
                        SetEnableDisable();
                        MblnAddStatus = true;
                        txtAgreementNumber.Text = string.Empty;
                        txtAgreementNumber.Select();
                    }
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on renewToolStripMenuItem_Click " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        private void tmLease_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmLease.Enabled = false;
            errLease.Clear();
            tmLease.Stop();
        }
        #endregion Events

        #region Functions
        /// <summary>
        /// Load Messages
        /// </summary>
        /// <param name="intType"></param>
        private void LoadMessage()
        {
            MsMessageArray = new ArrayList();
            MaStatusMessage = new ArrayList();
            MsMessageArray = objClsNotification.FillMessageArray((int)FormID.LeaseAgreement, ClsCommonSettings.ProductID);
            MaStatusMessage = objClsNotification.FillStatusMessageArray((int)FormID.LeaseAgreement, ClsCommonSettings.ProductID);
        }
        /// <summary>
        /// Load all combobox
        /// </summary>
        private void LoadCombos()
        {
            try
            {
                DataTable dtCombo = new ClsCommonUtility().FillCombos(new string[] { "CompanyID,CompanyName ", "CompanyMaster", "CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DataSource = dtCombo;

                if (CompanyID > 0)
                    cboCompany.SelectedValue = CompanyID;
                LoadLeaseUnitType();

            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadCombos " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }
        private void LoadLeaseUnitType()
        {
            DataTable dtCombo;
            if (ClsCommonSettings.IsArabicView)
            {
                dtCombo = new ClsCommonUtility().FillCombos(new string[] { "LeaseUnitTypeID,DescriptionArb AS Description ", "LeaseUnitTypeReference", "" });

            }
            else
            {
                dtCombo = new ClsCommonUtility().FillCombos(new string[] { "LeaseUnitTypeID,Description ", "LeaseUnitTypeReference", "" });

            }
            cboLeaseUnitType.DisplayMember = "Description";
            cboLeaseUnitType.ValueMember = "LeaseUnitTypeID";
            cboLeaseUnitType.DataSource = dtCombo;
        }

        /// <summary>
        /// Permission settings
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.LeaseAgreement, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.CompanyDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.CompanyDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out mblnEmail, out mblnAddRenew, out mblnUpdate, out mblnDelete);
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
                mblnAddIssue = mblnAddReceipt = mblnAddRenew=true;
            }
        }
        /// <summary>
        /// Get total record count depending on EmployeeID/InsuranceCardID/Searchtext
        /// </summary>
        private void GetRecordCount()
        {
            try
            {
                MintRecordCnt = objBLLLeaseAgreement.GetRecordCount(CompanyID, LeaseID, txtSearch.Text.Trim());
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on GetRecordCount " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }
        private void LoadInitials()
        {
            txtSearch.Text = "";
            mblnSearchStatus = false;

            AddNewItem();
            LeaseTab.SelectedTab = ContractInfo;
            cboCompany.SelectedIndex = cboLeaseUnitType.SelectedIndex = -1;
            txtAgreementNumber.Tag = "0";
            chkDepositHeld.Checked = false;
            txtAddress.Text = txtAgreementNumber.Text =  txtBuildingNumber.Text = string.Empty;
            txtContactNumbers.Text = txtLandlord.Text = txtLessor.Text = txtRentalValue.Text =txtTenant.Text = txtUnitNumber.Text = string.Empty ;
            txtLocation.Text =  txtPlotNumber.Text = txtTotal.Text = txtLeasePeriod.Text = string.Empty ;
            txtRemarks.Text = string.Empty;
            dtpDocumentDate.Value = dtpStartDate.Value= DateTime.Now.Date;
            dtpEndDate.Value = DateTime.Now.Date.AddDays(1);
        
            errLease.Clear();

            if (CompanyID > 0) // From Employee
            {
                cboCompany.Text = string.Empty;
                cboCompany.SelectedValue = CompanyID;
                //cboCompany.Enabled = false;
                txtAgreementNumber.Select();
            }
            else
            {
                cboCompany.Enabled = true;
                cboCompany.Select();
            }
            SetEnableDisable();
        }
        private bool AddNewItem()
        {
            try
            {
                //txtSearch.Text = string.Empty;

                MblnAddStatus = (LeaseID  > 0 ? false : true);
                lblRenewed.Visible = false;
                cboCompany.Enabled =  true;
                cboLeaseUnitType.Enabled = chkDepositHeld.Enabled =btnLeaseUnitType.Enabled= true;
                txtAddress.ReadOnly = false;
                 txtAgreementNumber.Enabled = txtBuildingNumber.Enabled = true;
                txtContactNumbers.Enabled = txtLandlord.Enabled = txtLessor.Enabled = txtRentalValue.Enabled = txtTenant.Enabled = txtUnitNumber.Enabled = true;
                txtLocation.Enabled = txtPlotNumber.Enabled = txtTotal.Enabled = txtLeasePeriod.Enabled = true;
                dtpDocumentDate.Enabled = dtpStartDate.Enabled = dtpEndDate.Enabled = true;

                //bnDeleteItem.ToolTipText = "Remove";

                GetRecordCount();

                bnCountItem.Text = strOf + Convert.ToString(MintRecordCnt + 1) + "";
                bnPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
                MintCurrentRecCnt = MintRecordCnt + 1;

                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 655, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmLease.Enabled = true;
                return true;
            }
            catch (Exception Ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());
                return false;
            }
        }
        /// <summary>
        /// button enable/disable depends on New mode or update mode
        /// </summary>
        private void SetEnableDisable()
        {
            BtnSave.Enabled = BtnOk.Enabled = bnSaveItem.Enabled = false;
            bnPrint.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            bnEmail.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            bnMoreActions.Enabled = (MblnAddStatus == true ? false : true);

            bnAddNewItem.Enabled = (MblnAddStatus == true ? false : (LeaseID  > 0 ? false : MblnAddPermission));

            if (MblnAddStatus) bnDeleteItem.Enabled = false;
            else
            {
                bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (LeaseID > 0 ? false : MblnDeletePermission));
                //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Lease Agreement could not be removed." : "Remove");
            }

            bnCancel.Enabled = (MblnAddStatus == true ? true : false);

            if (LeaseID  <= 0)
            {
                bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = true;
                int iCurrentRec = Convert.ToInt32(bnPositionItem.Text); // Current position of the record

                if (iCurrentRec >= MintRecordCnt)  // If the current is last record, disables move next and last button
                {
                    bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
                }
                if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    bnMoveFirstItem.Enabled = bnMovePreviousItem.Enabled = false;
                }
            }
            else
                bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = false;
        }
        /// <summary>
        /// Do actions after save
        /// Enable and disable form controls
        /// </summary>
        private void DoActionsAfterSave()
        {
            LeaseTab.SelectedTab = ContractInfo;

            SetAutoCompleteList();
            MblnAddStatus = false;
            BtnOk.Enabled = BtnSave.Enabled = bnSaveItem.Enabled = bnCancel.Enabled = false;
            documentsToolStripMenuItem.Enabled = MblnAddPermission;
            bnAddNewItem.Enabled = (LeaseID  > 0 ? false : MblnAddPermission);
            bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            if (MblnAddStatus) bnDeleteItem.Enabled = false;
            else
            {
                bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (LeaseID  > 0 ? false : MblnDeletePermission));
                //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Lease Agreement could not be removed." : "Remove");
            }
            SetReceiptOrIssueLabel();
        }
        /// <summary>
        /// Set Receipt /Issue button style(enable/disable)
        /// </summary>
        private void SetReceiptOrIssueLabel()
        {
            bnMoreActions.Enabled = true;

            renewToolStripMenuItem.Enabled = lblRenewed.Visible ? false : mblnAddRenew;

            issueToolStripMenuItem1.Enabled = receiptToolStripMenuItem.Enabled = false;
            bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Company, (int)DocumentType.Lease_Agreement, txtAgreementNumber.Tag.ToInt32());

            if (IsReceipt)
                issueToolStripMenuItem1.Enabled = mblnAddIssue;
            else
                receiptToolStripMenuItem.Enabled = mblnAddReceipt;
        }

        private void ChangeStatus()
        {
            errLease.Clear();
            if (MblnAddStatus) // add mode
            {
                BtnOk.Enabled = BtnSave.Enabled = bnSaveItem.Enabled = MblnAddPermission;
            }
            else  //edit mode
            {
                BtnOk.Enabled = BtnSave.Enabled = bnSaveItem.Enabled = MblnUpdatePermission;
            }
        }
        /// <summary>
        /// Validation of controls
        /// </summary>        
        private bool FormValidation()
        {
            errLease.Clear();
            bool bReturnValue = true;
            Control cControl = new Control();

            if (Convert.ToInt32(cboCompany.SelectedValue) == 0) // Company Name
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10650, out MsMessageBoxIcon);
                cControl = cboCompany;
                bReturnValue = false;
            }
          
            else if (String.IsNullOrEmpty(txtAgreementNumber.Text.Trim())) // Agreement Number
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10651, out MsMessageBoxIcon);
                cControl = txtAgreementNumber;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtLessor.Text.Trim())) // Lessor
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10652, out MsMessageBoxIcon);
                cControl = txtLessor;
                bReturnValue = false;
            }
            else if (dtpEndDate.Value.Date <= dtpStartDate.Value.Date)
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10655, out MsMessageBoxIcon);
                cControl = dtpEndDate;
                bReturnValue = false;
            }
            else if (dtpStartDate.Value > ClsCommonSettings.GetServerDate())
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10656, out MsMessageBoxIcon);
                cControl = dtpStartDate;
                bReturnValue = false;
            }
            if (bReturnValue == false)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (cControl != null)
                {
                    errLease.SetError(cControl, MsMessageCommon);
                    cControl.Focus();
                }
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmLease.Enabled = true;
            }
            return bReturnValue;
        }
        /// <summary>
        /// Save Insurance card
        /// </summary>
        private bool SaveLeaseAgreement()
        {
            cboCompany.Focus();

            bool blnReturnValue = false;

            try
            {
                if (FormValidation())
                {
                    if (MblnAddStatus)
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 1, out MsMessageBoxIcon);
                    else
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 3, out MsMessageBoxIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)

                        return false;

                    FillParameters();
                  

                    txtAgreementNumber.Tag = objBLLLeaseAgreement.SaveLeaseAgreement(MblnAddStatus).ToInt32();

                    if (txtAgreementNumber.Tag.ToInt32() > 0)
                    {
                        if (LeaseID > 0)
                        {
                            LeaseID = txtAgreementNumber.Tag.ToInt32();
                            MintCurrentRecCnt = 1;
                        }

                        clsAlerts objclsAlerts = new clsAlerts();
                        objclsAlerts.AlertMessage((int)DocumentType.Lease_Agreement, "Lease Agreement", "Agreement Number", txtAgreementNumber.Tag.ToInt32(), txtAgreementNumber.Text, dtpEndDate.Value, "Comp", cboCompany.SelectedValue.ToInt32(), cboCompany.Text.Trim(), true,0);

                        string strMsg = "";
                        if (MblnAddStatus)//insert
                        {
                            strMsg = objClsNotification.GetErrorMessage(MsMessageArray, 2, out MsMessageBoxIcon);
                        }
                        else
                        {
                            strMsg = objClsNotification.GetErrorMessage(MsMessageArray, 21, out MsMessageBoxIcon);
                        }
                        MessageBox.Show(strMsg.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = strMsg.Remove(0, strMsg.IndexOf("#") + 1);
                        tmLease.Enabled = true;
                        blnReturnValue = true;
                    }
                }

                return blnReturnValue;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on SaveInsuranceCardDetails() " + this.Name + " " + ex.Message.ToString(), 1);
                return blnReturnValue;
            }
        }
        /// <summary>
        /// Fill Parameters before save
        /// </summary>
        private void FillParameters()
        {


            objBLLLeaseAgreement.objDTOLeaseAgreement.LeaseID = txtAgreementNumber.Tag.ToInt32();

            objBLLLeaseAgreement.objDTOLeaseAgreement.CompanyID = cboCompany.SelectedValue.ToInt32();
            objBLLLeaseAgreement.objDTOLeaseAgreement.AgreementNo = txtAgreementNumber.Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.Lessor = txtLessor.Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.StartDate = dtpStartDate.Value.Date;
            objBLLLeaseAgreement.objDTOLeaseAgreement.EndDate = dtpEndDate.Value.Date;
            objBLLLeaseAgreement.objDTOLeaseAgreement.DocumentDate = dtpDocumentDate.Value.Date;
            objBLLLeaseAgreement.objDTOLeaseAgreement.LeasePeriod = txtLeasePeriod.Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.RentalValue = txtRentalValue.Text.ToDecimal();
            objBLLLeaseAgreement.objDTOLeaseAgreement.DepositHeld = chkDepositHeld.Checked;
            objBLLLeaseAgreement.objDTOLeaseAgreement.Tenant = txtTenant.Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.ContactNumbers = txtContactNumbers.Text.Trim ();
            objBLLLeaseAgreement.objDTOLeaseAgreement.Address = txtAddress.Text.Trim();

            objBLLLeaseAgreement.objDTOLeaseAgreement.LandLord = txtLandlord.Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.UnitNumber = txtUnitNumber.Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.LeaseUnitTypeID = cboLeaseUnitType.SelectedValue.ToInt32();
            objBLLLeaseAgreement.objDTOLeaseAgreement.BuildingNumber = txtBuildingNumber.Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.Location = txtLocation .Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.PlotNumber = txtPlotNumber.Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.Total = txtTotal.Text.ToDecimal ();
            objBLLLeaseAgreement.objDTOLeaseAgreement.Remarks = txtRemarks.Text.Trim();
            objBLLLeaseAgreement.objDTOLeaseAgreement.IsRenewed = lblRenewed.Visible ? true : false;
           
        }
        /// <summary>
        /// Get Insurance card details
        /// </summary>
        private void GetLeaseDetails()
        {

            DataTable dtLease= objBLLLeaseAgreement.GetLeaseAgreementDetails(MintCurrentRecCnt, CompanyID, LeaseID , txtSearch.Text.Trim());//get data from BLL
            if (dtLease.Rows.Count > 0)
            {
                objBLLLeaseAgreement.objDTOLeaseAgreement.CompanyID = Convert.ToInt32(dtLease.Rows[0]["CompanyID"]);
                txtAgreementNumber.Tag = dtLease.Rows[0]["LeaseID"].ToInt32();
                cboCompany.SelectedValue = dtLease.Rows[0]["CompanyID"];
                txtAgreementNumber.Text =Convert.ToString (dtLease.Rows[0]["AgreementNo"]);
                txtLessor.Text = Convert.ToString(dtLease.Rows[0]["Lessor"]);
                dtpStartDate.Value = dtLease.Rows[0]["StartDate"].ToDateTime();
                dtpEndDate.Value = dtLease.Rows[0]["EndDate"].ToDateTime();
                dtpDocumentDate.Value = dtLease.Rows[0]["DocumentDate"].ToDateTime();
                txtLeasePeriod.Text = dtLease.Rows[0]["LeasePeriod"].ToString();
                txtRentalValue.Text = dtLease.Rows[0]["RentalValue"].ToString();
                if (ClsCommonSettings.IsAmountRoundByZero)
                {
                    txtRentalValue.Text = txtRentalValue.Text.ToDecimal().ToString("F" + 0);
                }
                chkDepositHeld.Checked = Convert.ToBoolean(dtLease.Rows[0]["DepositHeld"]);
                txtTenant.Text = Convert.ToString(dtLease.Rows[0]["Tenant"]);
                txtContactNumbers.Text = Convert.ToString(dtLease.Rows[0]["ContactNumbers"]);
                txtAddress.Text = Convert.ToString(dtLease.Rows[0]["Address"]);
                txtLandlord.Text = Convert.ToString(dtLease.Rows[0]["Landlord"]);
                txtUnitNumber.Text = Convert.ToString(dtLease.Rows[0]["UnitNumber"]);
                cboLeaseUnitType.SelectedValue = dtLease.Rows[0]["LeaseUnitTypeID"].ToInt32();
                txtBuildingNumber.Text = Convert.ToString(dtLease.Rows[0]["BuildingNumber"]);
                txtLocation.Text = Convert.ToString(dtLease.Rows[0]["Location"]);
                txtPlotNumber.Text = Convert.ToString(dtLease.Rows[0]["PlotNumber"]);
                txtTotal.Text = dtLease.Rows[0]["Total"].ToString();
                txtRemarks.Text = Convert.ToString(dtLease.Rows[0]["Remarks"]);

                if (Convert.ToBoolean(dtLease.Rows[0]["IsRenewed"]))
                {
                    lblRenewed.Visible = true;
                    cboCompany.Enabled = cboLeaseUnitType.Enabled =chkDepositHeld.Enabled = btnLeaseUnitType.Enabled = false;
                    txtAddress.ReadOnly = true;
                    txtAgreementNumber.Enabled = txtBuildingNumber.Enabled = false;
                    txtContactNumbers.Enabled = txtLandlord.Enabled = txtLessor.Enabled = txtRentalValue.Enabled = txtTenant.Enabled = txtUnitNumber.Enabled =false ;
                    txtLocation.Enabled = txtPlotNumber.Enabled = txtTotal.Enabled = txtLeasePeriod.Enabled =false ;
                    dtpDocumentDate.Enabled = dtpStartDate.Enabled = dtpEndDate.Enabled = false;
                }
                else
                {
                    lblRenewed.Visible = false;
                    cboCompany.Enabled =  true;
                    cboLeaseUnitType.Enabled = chkDepositHeld.Enabled =btnLeaseUnitType.Enabled= true ;
                    txtAddress.ReadOnly = false;
                    txtAgreementNumber.Enabled = txtBuildingNumber.Enabled = true;
                    txtContactNumbers.Enabled = txtLandlord.Enabled = txtLessor.Enabled = txtRentalValue.Enabled = txtTenant.Enabled = txtUnitNumber.Enabled = true;
                    txtLocation.Enabled = txtPlotNumber.Enabled = txtTotal.Enabled = txtLeasePeriod.Enabled = true;
                    dtpDocumentDate.Enabled = dtpStartDate.Enabled = dtpEndDate.Enabled = true;
                }
                MblnAddStatus = false;

                MstrDocName = txtAgreementNumber.Text.Replace("'", "").Trim();

                bnPositionItem.Text = MintCurrentRecCnt.ToString();
                bnCountItem.Text = strOf + Convert.ToString(MintRecordCnt) + "";
                bnAddNewItem.Enabled = MblnAddPermission;

                SetEnableDisable();
                SetReceiptOrIssueLabel();
            }
        }
        /// <summary>
        /// Load report
        /// </summary>
        private void LoadReport()
        {
            try
            {
                if (txtAgreementNumber.Tag.ToInt32() > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();

                    ObjViewer.intCompanyID = cboCompany.SelectedValue.ToInt32();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = txtAgreementNumber.Tag.ToInt64();
                    ObjViewer.PiFormID = (int)FormID.LeaseAgreement;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        /// <summary>
        /// Delete Insurance card
        /// </summary>
        private void DeleteLeaseAgreement()
        {
            try
            {
                objBLLLeaseAgreement.objDTOLeaseAgreement.LeaseID  = txtAgreementNumber.Tag.ToInt32();
                objBLLLeaseAgreement.DeleteLeaseAgreement();
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 4, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmLease.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
               
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on DeleteLeaseAgreement() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        /// <summary>
        /// Autocomplete with CompanyName/Code and Agreementnumber
        /// </summary>
        /// 
        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsBLLTradeLicense.GetAutoCompleteList(txtSearch.Text.Trim(), CompanyID, "AgreementNo", "CompanyLeaseAgreement");
            this.txtSearch.AutoCompleteCustomSource = clsBLLTradeLicense.ConvertToAutoCompleteCollection(dt);

        }
        #endregion Functions

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }

    }
}
