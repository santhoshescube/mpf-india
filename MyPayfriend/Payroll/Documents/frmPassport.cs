﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;
using MyPayfriend;



public partial class frmPassport : Form
{
    /// <summary>
    /// Form for passport functionalities like ,add edit, delete , print and email
    /// </summary>
    /// <Modified By>Jisha Bejoy</Modified>
    /// <Modified on>22 Aug 2013</Modified>
    /// <Purpose>Code review , search option added, renew functionality modified and fine tuning</Purpose>
    #region Private variables

    clsPassportBLL objPassportBLL = null; 
    ClsNotificationNew MobjClsNotification = null;
    ClsLogWriter MObjClsLogWriter;   
  
    int? PassportID = null;
    int intRowIndex = 1;
    int intTotalRecords = 0;     
    bool blnNewMode = false;  
    public int PiPassportID = 0;
    public long EmployeeID { get; set; } // from employee form
    private bool formLoading = false;
    private bool IsFromEmployeeForm { get; set; }
    private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
    private bool MblnAddPermission = false;           //To set Add Permission
    private bool MblnUpdatePermission = false;      //To set Update Permission
    private bool MblnDeletePermission = false;      //To set Delete Permission
    private bool MblnAddUpdatePermission = false;   //To set Add Update Permission
    private bool mblnAddIssue = false;
    private bool mblnAddReceipt = false;
    private bool mblnAddRenew = false;
    private bool mblnSearchStatus = false;


    bool mblnEmail, mblnDelete, mblnUpdate = false;
    private bool IsReceipt { get; set; }
    bool IsVerified = false;   
    DataTable datMessages;
    string MstrCommonMessage = "";  
    MessageBoxIcon objMsgIcon;
    private string strOf = "of ";

    #endregion

    #region Constructor


    private void LoadMessage()
    {
        MobjClsNotification = new ClsNotificationNew();
        datMessages = MobjClsNotification.FillMessageArray((int)FormID.Passport, 4);
    }

    /// <summary>
    /// Initial settings
    /// </summary>
    public frmPassport()
    {
        InitializeComponent();
        this.objPassportBLL = new clsPassportBLL();
        this.MobjClsNotification = new ClsNotificationNew();
        this.objPassportBLL.Passport = new clsDTOPassport();       
       
        this.MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
        this.dtpIssueDate.Text = DateTime.Today.ToString("dd/MMM/yyyy");
        this.dtpExpiryDate.Text = DateTime.Today.AddDays(1).ToString("dd/MMM/yyyy");
        this.cboEmployee.SelectedIndexChanged += new EventHandler(OnInputChanged);
        this.txtPlaceOfIssue.TextChanged += new EventHandler(OnInputChanged);
        this.txtPassportNumber.TextChanged += new EventHandler(OnInputChanged);
        this.cboCountry.SelectedIndexChanged += new EventHandler(OnInputChanged);
        this.dtpExpiryDate.TextChanged += new EventHandler(OnInputChanged);
        this.dtpIssueDate.TextChanged += new EventHandler(OnInputChanged);
        this.txtPlaceOfBirth.TextChanged += new EventHandler(OnInputChanged);
        this.txtOldPassportNumbers.TextChanged += new EventHandler(OnInputChanged);
        this.txtResidencePermits.TextChanged += new EventHandler(OnInputChanged);
        this.txtEntryRestrictions.TextChanged += new EventHandler(OnInputChanged);
        this.txtNameinPassport.TextChanged += new EventHandler(OnInputChanged);
        this.chkPagesAvailable.CheckedChanged += new EventHandler(OnInputChanged);

        if (ClsCommonSettings.IsArabicView)
        {
            this.RightToLeftLayout = true;
            this.RightToLeft = RightToLeft.Yes;
            SetArabicControls();
        }
    }
    #endregion
    private void SetArabicControls()
    {
        ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        objDAL.SetArabicVersion((int)FormID.Passport, this);

        ToolStripDropDownBtnMore.Text = "الإجراءات";
        ToolStripDropDownBtnMore.ToolTipText = "الإجراءات";
        mnuItemAttachDocuments.Text = "وثائق";
        receiptToolStripMenuItem.Text = "استلام";
        issueToolStripMenuItem.Text = "قضية";
        renewToolStripMenuItem.Text = "جدد";
        txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
        strOf = "من ";
    }
    #region Load
    private void frmPassport_Load(object sender, EventArgs e)
    {
        try
        {

            SetAutoCompleteList();
            LoadMessage();
            SetPermissions(ClsCommonSettings.CurrentCompanyID);
            LoadInitials();

            if (PiPassportID > 0)
            {
                objPassportBLL.Passport.PassportID = PiPassportID;
                intTotalRecords = 1;
                intRowIndex = 1;
                this.BindNavigator();
                this.txtSearch.Enabled = false;
                lblStatus.Text = string.Empty;
                Timer.Enabled = true;
            }
            if (this.EmployeeID != 0)
            {
                this.IsFromEmployeeForm = true;
                if (intTotalRecords == 0) // No records
                {
                    intTotalRecords = 1;
                    bnCountItem.Text = intTotalRecords.ToString();
                    bnPositionItem.Text = intRowIndex.ToString();

                }
                else
                {
                    BindingNavigatorMoveFirstItem_Click(sender, e);
                }
            
            }

        }
        catch (Exception)
        {

        }

    }
    #endregion

    #region Functions

    private void LoadInitials()
    {
        LoadCombos();

        AddNewHandler();

        ClearInputs();

        SetEnableDisable();

    }

    /// <summary>
    /// button enable/disable depends on New mode or update mode
    /// </summary>
    private void SetEnableDisable()
    {
        btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;
        BtnPrint.Enabled = (blnNewMode == true ? false : MblnPrintEmailPermission);
        BtnEmail.Enabled = (blnNewMode == true ? false : MblnPrintEmailPermission);
        ToolStripDropDownBtnMore.Enabled = (blnNewMode == true ? false : true);

        BindingNavigatorAddNewItem.Enabled = (blnNewMode == true ? false : (PiPassportID > 0 ? false : MblnAddPermission));

        if (blnNewMode) BindingNavigatorDeleteItem.Enabled = false;
        else
        {
            BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (PiPassportID > 0 ? false : MblnDeletePermission));
            //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewedpassport could not be removed." : "Remove");
        }
        btnClear.Enabled = (blnNewMode == true ? true : false);

        if (PiPassportID <= 0)
        {
            bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = true;
            int iCurrentRec = Convert.ToInt32(bnPositionItem.Text); // Current position of the record

            if (intRowIndex >= intTotalRecords)  // If the current is last record, disables move next and last button
            {
                bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
            }
            if (intRowIndex == 1)  // If the current is first record, disables previous and first buttons
            {
                bnMoveFirstItem.Enabled = bnMovePreviousItem.Enabled = false;
            }
        }
        else
            bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = false;
    }

    private void AddNewHandler()
    {
        try
        {
            blnNewMode = (PiPassportID > 0 ? false : true);
            lblRenewed.Visible = false;
            cboEmployee.Enabled = (EmployeeID  > 0 ? false : true);
            cboCountry.Enabled = true;
            txtPassportNumber.Enabled = txtNameinPassport.Enabled = txtPlaceOfBirth.Enabled = txtPlaceOfIssue.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled =chkPagesAvailable.Enabled = btnCountry .Enabled = true;
            //BindingNavigatorDeleteItem.ToolTipText = "Remove";
            txtSearch.Text = string.Empty;
            GetRecordCount();
            //SetAutoCompleteList();

            bnCountItem.Text = strOf + Convert.ToString(intTotalRecords + 1) + "";
            bnPositionItem.Text = Convert.ToString(intTotalRecords + 1);
            intRowIndex = intTotalRecords + 1;
   
            string MsMessageCommon = MobjClsNotification.GetErrorMessage(datMessages, 655, out objMsgIcon);
            lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            Timer.Enabled = true;
            
        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on Addnewhandler()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Addnewhandler()" + Ex.Message.ToString());
         
        }
    }



    private void DeleteHandler()
    {
        try
        {
            // Set ID of the vehicle to be deleted
            this.objPassportBLL.Passport.PassportID = this.PassportID;

            // Invoke delete method and get delete status.
            bool success = this.objPassportBLL.DeletePassport();

            // Send feedback to user
            if (success)
            {
                this.AssingStatuText((int)CommonMessages.Deleted);
                ShowMessage(4);
                LoadInitials();
                SetAutoCompleteList();
            }
            else
                ShowMessage("Deletion failed");

        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on DeleteHandler()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on DeleteHandler()" + Ex.Message.ToString());
        }
    }

    private bool SaveHandler()
    {
        bool success = false;
        IsVerified = false;
        try
        {
            // Validate user inputs
            this.VerifyInputs();

            if (IsVerified)
            {
                if (objPassportBLL.Passport.PassportID == 0 || objPassportBLL.Passport.PassportID == null)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1);
                }
                else
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 3);
                }

                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string strMessage = string.Empty;
                    clsAlerts objclsAlerts = new clsAlerts();
                    if (blnNewMode)
                    {
                        Int32 intPassportId = objPassportBLL.InsertPassport();

                        if (intPassportId > 0)
                        {
                            success = true;
                            this.AssingStatuText((int)CommonMessages.Saved );
                            intTotalRecords = intTotalRecords == 0 ? 1 : intTotalRecords;
                            if (PiPassportID > 0)
                            {
                                PiPassportID = intPassportId;
                                intRowIndex = 1;
                            }
                            this.blnNewMode = false;
                            this.BindNavigator();
                            ShowMessage(2);
                            objclsAlerts.AlertMessage((int)DocumentType.Passport, "Passport", "PassportNumber", intPassportId, txtPassportNumber.Text, dtpExpiryDate.Value, "Emp", cboEmployee.SelectedValue.ToInt32(), cboEmployee.Text, false,0);

                        }
                        else
                            ShowMessage("Saving passport failed");
                    }
                    else
                    {
                        objPassportBLL.Passport.DocumentID = 0;
                        success = objPassportBLL.UpdatePasspost();

                        if (success)
                        {  
                            this.AssingStatuText((int)CommonMessages.Updated);
                            ShowMessage(21);
                            objclsAlerts.AlertMessage((int)DocumentType.Passport, "Passport", "PassportNumber", PassportID.ToInt32(), txtPassportNumber.Text, dtpExpiryDate.Value, "Emp", cboEmployee.SelectedValue.ToInt32(), cboEmployee.Text, false,0);
                       

                        }
                        else
                            ShowMessage("Failed to update the passport");

                    }
                    GetRecordCount();
                    RefreshFormAfterSave();
                }
            }
            return success;

        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on Savehandler() "+this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on SaveHandler()" + Ex.Message.ToString());
            return success;
        }
    }

    private void LoadCombos()
    {
        try
        { 
            DataTable datCombos = null;

            if (ClsCommonSettings.IsArabicView)
            {
                if (EmployeeID > 0)
                {
                    cboEmployee.Text = string.Empty;
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeFullNameArb +'-'+ E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName  ," +
                        "E.EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" + 
                        "E.WorkStatusID > 5 And and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") and E.EmployeeID = " + EmployeeID });

                }
                else
                {
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeFullNameArb +'-'+ E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                        "E.EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "E.WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") order by E.Employeefullname " });

                }
            }
            else
            {
                if (EmployeeID > 0)
                {
                    cboEmployee.Text = string.Empty;
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeFullName +'-'+ E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                        "E.EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "E.WorkStatusID > 5 And  E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") and E.EmployeeID = " + EmployeeID });

                }
                else
                {
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeFullName +'-'+ E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                        "E.EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "E.WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") order by E.Employeefullname " });

                }
            }


            cboEmployee.DisplayMember = "EmployeeName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = datCombos;
          
            LoadCountry();

        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on LoadCombos()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on LoadCombos()" + Ex.Message.ToString());
        }
    }

    private void FillEmployee()
    {
        try
        {
            DataTable datCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                    "EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") AND WorkStatusID > 5 OR EmployeeID = " + objPassportBLL.Passport.EmployeeID });

            }
            else
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                    "EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") AND WorkStatusID > 5 OR EmployeeID = " + objPassportBLL.Passport.EmployeeID });

            }
            cboEmployee.DisplayMember = "EmployeeName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = datCombos;
        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on FillEmployee()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on FillEmployee()" + Ex.Message.ToString());
        }
    }
    
    private void LoadCountry()
    {
        try
        {
            DataTable datCombos = null;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "CountryId, CountryNameArb as Description", "CountryReference", "" });
            }
            else
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "CountryId, CountryName as Description", "CountryReference", "" });
            }
            cboCountry.DisplayMember = "Description";
            cboCountry.ValueMember = "CountryID";
            cboCountry.DataSource = datCombos;

        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on FillEmployee()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on FillEmployee()" + Ex.Message.ToString());
        }
    }
    private void VerifyInputs()
    {
        try
        {
            errorProvider1.Clear();
            txtSearch.Text = "";
             // create new instanace of passport to clear all existing values (if any)           
            this.objPassportBLL.Passport = new clsDTOPassport();
          
            if (!blnNewMode)               
                this.objPassportBLL.Passport.PassportID = (int)this.PassportID; // Set ID of the passport to be updated.

            if (cboEmployee.SelectedIndex == -1)
            {
                ShowMessage(9719, cboEmployee);
                return;
            }
            else
            {
                this.objPassportBLL.Passport.EmployeeID = Convert.ToInt32(this.cboEmployee.SelectedValue);
            }

            if (this.txtPassportNumber.Text.Trim() == string.Empty)
            {
                ShowMessage(9720, txtPassportNumber);
                return;
            }
            else
                this.objPassportBLL.Passport.PassportNumber = txtPassportNumber.Text.Trim();

            if (this.txtNameinPassport.Text.Trim() == string.Empty)
            {
                ShowMessage(9721, txtNameinPassport);
                return;
            }
            else
                this.objPassportBLL.Passport.NameinPassport = this.txtNameinPassport.Text.Trim();

            if (this.txtPlaceOfIssue.Text.Trim() == string.Empty)
            {
                ShowMessage(9722, txtPlaceOfIssue);
                return;
            }
            else
                this.objPassportBLL.Passport.PlaceofIssue = this.txtPlaceOfIssue.Text.Trim();

            if (this.cboCountry.SelectedIndex == -1)
            {
                ShowMessage(9723, cboCountry);
                return;
            }
            else
                this.objPassportBLL.Passport.CountryID = Convert.ToInt32(this.cboCountry.SelectedValue);
            this.objPassportBLL.Passport.PlaceOfBirth = txtPlaceOfBirth.Text.Trim();
            this.objPassportBLL.Passport.PassportPagesAvailable = chkPagesAvailable.Checked;
            this.objPassportBLL.Passport.IsRenew = lblRenewed.Visible;
            this.objPassportBLL.Passport.IssueDate = Convert.ToDateTime(dtpIssueDate.Text);

            if (objPassportBLL.Passport.IssueDate > DateTime.Today)
            {
                ShowMessage(9725, dtpIssueDate);
                return;
            }
            this.objPassportBLL.Passport.ExpiryDate = dtpExpiryDate.Value.Date;

            if (this.objPassportBLL.Passport.IssueDate >= objPassportBLL.Passport.ExpiryDate)
            {

                ShowMessage(9724, dtpExpiryDate);
                return;
            }
            //if (objPassportBLL.IsPassportNoExists())
            //{
            //    ShowMessage(9726, txtPassportNumber);
            //    return;
            //}

            this.objPassportBLL.Passport.ResidencePermits = txtResidencePermits.Text.Trim();
            this.objPassportBLL.Passport.OldPassportNumbers = txtOldPassportNumbers.Text.Trim();
            this.objPassportBLL.Passport.EntryRestrictions = txtEntryRestrictions.Text.Trim();

            IsVerified = true;
         
        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on Validation()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Validation()" + Ex.Message.ToString());
        }
    }  

    public void BindComboBox(ComboBox cbo, string strDisplayField, string strValueField, object dataSource)
    {
        cbo.DisplayMember = strDisplayField;
        cbo.ValueMember = strValueField;
        cbo.DataSource = dataSource;
        cbo.SelectedIndex = -1;
    }

    /// <summary>
    /// Enable or disable issue and receipt button
    /// </summary>
    private void SetIssueReceiptButton()
    {
       
        if (! clsPassportBLL.IsEmployeeInService(cboEmployee.SelectedValue.ToInt32()))
        {
            renewToolStripMenuItem.Enabled = false;
        }
        else
        {
            renewToolStripMenuItem.Enabled = lblRenewed.Visible ? false : mblnAddRenew;
        }
       
        issueToolStripMenuItem.Enabled = receiptToolStripMenuItem.Enabled = false;
        if (objPassportBLL.Passport.PassportID != null)
        {
            IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Employee, (int)DocumentType.Passport, objPassportBLL.Passport.PassportID.ToInt32());
        }
        if (IsReceipt)
            issueToolStripMenuItem.Enabled = mblnAddIssue;
        else
            receiptToolStripMenuItem.Enabled = mblnAddReceipt;
    }
    /// <summary>
    /// Enable and disable form controls after save
    /// </summary>
    private void RefreshFormAfterSave()
    {
        try
        {
            SetAutoCompleteList();
            blnNewMode = false;
            btnOK.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = btnClear.Enabled = false;
            mnuItemAttachDocuments.Enabled = MblnAddPermission;
            BindingNavigatorAddNewItem.Enabled = (PiPassportID > 0 ? false : MblnAddPermission);
            BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
            if (blnNewMode) BindingNavigatorDeleteItem.Enabled = false;
            else
            {
                BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (PiPassportID > 0 ? false : MblnDeletePermission));
                //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed passport could not be removed." : "Remove");
            }
            SetIssueReceiptButton();

        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on RefreashFormAfterSave()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on RefreshAfterSave()" + Ex.Message.ToString());
        }
    }

    private void BindContorls()
    {
        try
        {
            this.txtPassportNumber.Text = this.objPassportBLL.Passport.PassportNumber;
            this.txtNameinPassport.Text = this.objPassportBLL.Passport.NameinPassport;
            this.txtPlaceOfIssue.Text = this.objPassportBLL.Passport.PlaceofIssue;
            this.txtPlaceOfBirth.Text = this.objPassportBLL.Passport.PlaceOfBirth;
            if (intTotalRecords > 0)
                FillEmployee();
            cboEmployee.SelectedValue = this.objPassportBLL.Passport.EmployeeID;            
            this.SetIndex(this.cboCountry, this.objPassportBLL.Passport.CountryID);
            chkPagesAvailable.Checked = this.objPassportBLL.Passport.PassportPagesAvailable;
            this.dtpIssueDate.Text = this.objPassportBLL.Passport.IssueDate == ClsCommonSettings.NullDate ? string.Empty : objPassportBLL.Passport.IssueDate.ToString();
            this.dtpExpiryDate.Text = this.objPassportBLL.Passport.ExpiryDate == ClsCommonSettings.NullDate ? string.Empty : this.objPassportBLL.Passport.ExpiryDate.ToString();
            this.txtResidencePermits.Text = this.objPassportBLL.Passport.ResidencePermits;
            this.txtOldPassportNumbers.Text = this.objPassportBLL.Passport.OldPassportNumbers;
            this.txtEntryRestrictions.Text = this.objPassportBLL.Passport.EntryRestrictions;

            if (objPassportBLL.Passport.IsRenew)
            {
                lblRenewed.Visible = true;
                renewToolStripMenuItem.Enabled = false;
                txtPlaceOfIssue.Enabled = txtPassportNumber.Enabled = txtNameinPassport.Enabled = txtPlaceOfBirth.Enabled = cboCountry.Enabled = cboEmployee.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = btnCountry.Enabled=chkPagesAvailable.Enabled  = false;
                BindingNavigatorDeleteItem.Enabled = false;

            }
            else
            {
                lblRenewed.Visible = false;
                renewToolStripMenuItem.Enabled = true;
                cboEmployee.Enabled = EmployeeID == 0 ? true : false;
                txtPlaceOfIssue.Enabled = txtPassportNumber.Enabled = txtNameinPassport.Enabled = txtPlaceOfBirth.Enabled = chkPagesAvailable.Enabled = cboCountry.Enabled =  dtpExpiryDate.Enabled = dtpIssueDate.Enabled = btnCountry.Enabled=chkPagesAvailable.Enabled  = true;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            }

        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on Bind()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Bind()" + Ex.Message.ToString());
        }
    }

    private void SetIndex(ComboBox cbo, long? value)
    {
        if (value != null)
            cbo.SelectedValue = value.ToString();
    }

    private void ClearInputs()
    {
        try
        {
            errorProvider1.Clear();
            txtSearch.Text = "";
            this.txtPassportNumber.Text = string.Empty;
            this.txtNameinPassport.Text = string.Empty;
            this.txtPlaceOfIssue.Text = string.Empty;
            this.txtPlaceOfBirth.Text = string.Empty;

            chkPagesAvailable.Checked = false;
            this.dtpIssueDate.Text = DateTime.Today.ToString("dd/MMM/yyyy");
            this.dtpExpiryDate.Text = DateTime.Today.AddDays(1).ToString("dd/MMM/yyyy");
            ChangeVisiblity(3);
            if (EmployeeID == 0)
            {
                cboEmployee.SelectedIndex = -1;
                cboEmployee.Enabled = true;
                cboEmployee.Select();
                cboEmployee.Text = string.Empty;
            }
            else
            {
                cboEmployee.SelectedValue = this.EmployeeID;
                cboEmployee.Enabled = false;
                txtPassportNumber.Select();
            }

            this.txtResidencePermits.Text = string.Empty;
            this.txtOldPassportNumbers.Text = string.Empty;
            this.txtEntryRestrictions.Text = string.Empty;
            this.cboCountry.SelectedIndex = -1;
            this.cboCountry.Text = string.Empty;
            btnOK.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = false;

        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on Clear: ClearInputs()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on ClearInputs()" + Ex.Message.ToString());
        }
    }



    private void BindNavigator()
    {
        try
        {
            errorProvider1.Clear();
            if (objPassportBLL.Passport == null)
                objPassportBLL.Passport = new clsDTOPassport();

            if (!blnNewMode)
            {
                if (this.IsFromEmployeeForm)
                    this.objPassportBLL.Passport.EmployeeID = this.EmployeeID;
                else
                    this.objPassportBLL.Passport.EmployeeID = 0;
                bool success = objPassportBLL.GetPassportDetails(txtSearch.Text.Trim(), PiPassportID, intRowIndex);

                if (success)
                {
                    btnClear.Enabled = false;
                    this.intTotalRecords = (Int32)this.objPassportBLL.Passport.Pager.TotalRecords;
                    this.BindContorls();
                    this.bnPositionItem.Text = this.intRowIndex.ToString();
                    this.bnCountItem.Text = string.Format(strOf + " {0}", this.intTotalRecords);
                 
                    this.PassportID = this.objPassportBLL.Passport.PassportID;
                    if (ClsCommonSettings.CurrentCompanyID != objPassportBLL.Passport.EmpCompanyID)
                        this.SetPermissions(objPassportBLL.Passport.EmpCompanyID);
                }
                this.SetEnableDisable();
                SetIssueReceiptButton();
            }
            this.blnNewMode = false;

        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on BindNavigator()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on BindNavigator()" + Ex.Message.ToString());
        }
    }

   
   
    private void DisableEnableButtons(ControlState state)
    {
        errorProvider1.Clear();
        if (blnNewMode) // add mode
        {
            btnOK.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnAddPermission;
        }
        else  //edit mode
        {
            btnOK.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
        }
    }

    private void AssingStatuText(int intCode)
    {
        this.lblStatus.Text = MobjClsNotification.GetStatusMessage(datMessages, intCode);
        this.Timer.Start();
    }

    public void OnInputChanged(object sender, EventArgs e)
    {     
        this.DisableEnableButtons(ControlState.Enable);
    }

    private void SetPermissions(int companyId)
    {
        // Function for setting permissions

        // Function for setting permissions

        clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

        if (ClsCommonSettings.RoleID > 3)
        {
            objClsBLLPermissionSettings.GetPermissions(companyId, (Int32)eMenuID.Passport, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            objClsBLLPermissionSettings.GetPermissions(companyId, (Int32)eMenuID.EmployeeDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
            objClsBLLPermissionSettings.GetPermissions(companyId, (Int32)eMenuID.EmployeeDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
            objClsBLLPermissionSettings.GetPermissions(companyId, (Int32)eMenuID.DocumentRenew, out mblnEmail, out mblnAddRenew, out mblnUpdate, out mblnDelete);
            
            if (MblnAddPermission == true || MblnUpdatePermission == true)
                MblnAddUpdatePermission = true;
        }
        else
        {
            MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            mblnAddIssue = mblnAddReceipt= mblnAddRenew  = true;
        }
    }
  
    private void ChangeVisiblity(int Option)
    {
        switch (Option)
        {
            case 1:
                btnRemarks.Text = ToolStripMenuOldPassports.Text;
                txtOldPassportNumbers.Visible = true;
                txtEntryRestrictions.Visible =txtResidencePermits.Visible = false;              
                lblOtherInfo.Text = "Old Passports";
                break;
            case 2:
                btnRemarks.Text = ToolStripMenuEntryRestrict.Text;
                txtOldPassportNumbers.Visible = txtResidencePermits.Visible = false;
                txtEntryRestrictions.Visible = true;               
                lblOtherInfo.Text = "Entry Restrictions";
                break;
            case 3:
                btnRemarks.Text = ToolStripMenuResidencePermit.Text;
                txtOldPassportNumbers.Visible = txtEntryRestrictions.Visible =false;             
                txtResidencePermits.Visible = true;
                lblOtherInfo.Text = "Residence Permits";
                break;
        }
    }

    private void ShowMessage(int MessageCode)
    {
        MessageBox.Show(MobjClsNotification.GetErrorMessage(datMessages, MessageCode), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    private void ShowMessage(int MessageCode, Control dt)
    {
        string MstrMessage = MobjClsNotification.GetErrorMessage(datMessages, MessageCode);
        MessageBox.Show(MstrMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        errorProvider1.SetError(dt, MstrMessage);
        dt.Focus();
    }

    private void ShowMessage(string Message)
    {
        MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }
    private void SetAutoCompleteList()
    {
        this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
        DataTable dt = clsBLLInsuranceCard.GetAutoCompleteList(txtSearch.Text.Trim(), EmployeeID, "PassportNumber", "EmployeePassportDetails");
        this.txtSearch.AutoCompleteCustomSource = clsBLLInsuranceCard.ConvertToAutoCompleteCollection(dt);
    }

    /// <summary>
    /// Get total record count while searching
    /// </summary>
    private void GetRecordCount()
    {
        intTotalRecords = objPassportBLL.GetRecordCount(txtSearch.Text.Trim(), EmployeeID, PiPassportID);
    }
    #endregion

    #region Events

    private void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.SaveHandler();
            SetIssueReceiptButton();

        }
        catch (Exception)
        {
          
        }
    }   

    private void btnOK_Click(object sender, EventArgs e)
    {
        bool Saved = this.SaveHandler();

        if (Saved)
        {          
            this.btnOK.Enabled = false;
            this.Close();
        }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {     
            this.Close(); 
    }

    private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
    {
        this.LoadInitials();
        this.AssingStatuText((int)CommonMessages.NewRecord);
       
    }

    private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
    {
        
        int dtDocRequest = clsBLLDocumentMasterNew.GetDocRequestReferenceExists(this.PassportID.ToInt32(), (int)DocumentType.Passport);
        if (dtDocRequest > 0)
        {
            string Message = "Cannot Delete as Document Request Exists.";
            MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        else
        {
            string Message = MobjClsNotification.GetErrorMessage(datMessages, 13).Replace("@", "\n");
            DialogResult confirmation = MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (confirmation == DialogResult.Yes)
                this.DeleteHandler();
        }
    }

    private void btnClear_Click(object sender, EventArgs e)
    {
        this.ClearInputs();
    }

    private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
    {
        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;
        GetRecordCount();

        blnNewMode = false;
        this.intRowIndex = 1;
        this.BindNavigator();
        this.AssingStatuText((int)CommonMessages.FirstRecord);
    }

    private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
    {


        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;

        GetRecordCount();

        if (intTotalRecords > 0)
        {
            this.intRowIndex -= 1;
        }
        blnNewMode = false;
        this.BindNavigator();
        this.AssingStatuText((int)CommonMessages.PreviousRecord);
    }

    private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
    {
        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;
        GetRecordCount();

        blnNewMode = false;
        this.intRowIndex = this.intTotalRecords;
        this.BindNavigator();
        this.AssingStatuText((int)CommonMessages.LastRecord);
    }

    private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
    {
        
        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;
        GetRecordCount();


        if (!blnNewMode)
        {
            this.intRowIndex += 1;
            this.BindNavigator();
        }      
        this.AssingStatuText((int)CommonMessages.NextRecord);
        this.errorProvider1.Clear();
    }

    #region Timer
    private void Timer_Tick(object sender, EventArgs e)
    {
        // Clear text in the status bar after the time interval of timer.      
        this.Timer.Stop();
    }
    #endregion

    private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {      
        if (!this.formLoading && this.cboEmployee.SelectedIndex != -1)
        {
            this.OnInputChanged(sender, e);          
        }
    }
 
    private void btnContext_Click(object sender, EventArgs e)
    {
        this.ContextMenuStrip1.Show(this.btnContext, this.btnContext.PointToClient(System.Windows.Forms.Cursor.Position));

    }
 
    private void ToolStripMenuResidencePermit_Click(object sender, EventArgs e)
    {
        ChangeVisiblity(3);
    }

    private void ToolStripMenuOldPassports_Click(object sender, EventArgs e)
    {
        ChangeVisiblity(1);
    }

    private void ToolStripMenuEntryRestrict_Click(object sender, EventArgs e)
    {
        ChangeVisiblity(2);
    }

    private void frmPassport_FormClosing(object sender, FormClosingEventArgs e)
    {
        if (btnOK.Enabled)
        {
            DialogResult confirmation = MessageBox.Show(MobjClsNotification.GetErrorMessage(datMessages, (int)CommonMessages.FormClose), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            e.Cancel = (confirmation == DialogResult.No);
        }
    }

    private void BtnPrint_Click(object sender, EventArgs e)
    {
        try
        {
            using (frmEmployeePassportReport frmReport = new frmEmployeePassportReport())
            {
                frmReport.PassportID = Convert.ToInt32(objPassportBLL.Passport.PassportID);
                DataTable datEmps = new ClsCommonUtility().FillCombos(new string[] { "CompanyID, FirstName", "EmployeeMaster", "  EmployeeID = " + cboEmployee.SelectedValue });
                if (datEmps.Rows.Count > 0)
                {
                    frmReport.CompanyID = datEmps.Rows[0]["CompanyID"].ToInt32();
                }
                frmReport.StartPosition = FormStartPosition.CenterScreen;
                frmReport.FormBorderStyle = FormBorderStyle.FixedSingle;
                frmReport.ShowDialog();
            }
        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on Print()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Print()" + Ex.Message.ToString());
        }
    }

    private void CboCountry_KeyPress(object sender, KeyPressEventArgs e)
    {
        cboCountry.DroppedDown = false;
    }

    private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
    {
        cboEmployee.DroppedDown = false;

    }

    private void btnContext_Click_1(object sender, EventArgs e)
    {
        this.ContextMenuStrip1.Show(this.btnContext, this.btnContext.PointToClient(System.Windows.Forms.Cursor.Position));


    }

    private void mnuItemAttachDocuments_Click(object sender, EventArgs e)
    {
        if (objPassportBLL.Passport.PassportID > 0)
        {
            using (FrmScanning objScanning = new FrmScanning())
            {
                objScanning.MintDocumentTypeid = (int)DocumentType.Passport;
                objScanning.MlngReferenceID = Convert.ToInt32(cboEmployee.SelectedValue);
                objScanning.MintOperationTypeID = (int)OperationType.Employee;
                objScanning.MstrReferenceNo = txtPassportNumber.Text;
                objScanning.PblnEditable = true;
                objScanning.MintVendorID = Convert.ToInt32(cboEmployee.SelectedValue);
                objScanning.MintDocumentID = Convert.ToInt32(objPassportBLL.Passport.PassportID);
                objScanning.ShowDialog();
            }
        }
    }

    private void receiptIssueToolStripMenuItem_Click(object sender, EventArgs e)
    {  
        new frmDocumentReceiptIssue()
        {
            eOperationType = OperationType.Employee,
            eDocumentType = DocumentType.Passport,
            DocumentID = Convert.ToInt32(objPassportBLL.Passport.PassportID),
            DocumentNumber = txtPassportNumber.Text,
            EmployeeID = cboEmployee.SelectedValue.ToInt32(),
            ExpiryDate = dtpExpiryDate.Value
        }.ShowDialog();
        SetIssueReceiptButton();
    }        
  
    private void btnCountry_Click_1(object sender, EventArgs e)
    {
        try
        {
            // Calling Reference form for ItemCategory
            int intCountryID = Convert.ToInt32(cboCountry.SelectedValue);
            FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryId, CountryName,CountryNameArb", "CountryReference", "");
            objCommon.ShowDialog();
            objCommon.Dispose();
            LoadCountry();
            if (objCommon.NewID != 0)
                cboCountry.SelectedValue = objCommon.NewID;
            else
                cboCountry.SelectedValue = intCountryID;
        }
        catch (Exception Ex)
        {       
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on FrmCommonRef() " + Ex.Message.ToString());

        }
    }
   
    private void BtnEmail_Click(object sender, EventArgs e)
    {
        using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
        {
            ObjEmailPopUp.MsSubject = "Passport Information";
            ObjEmailPopUp.EmailFormType = EmailFormID.Passport;
            ObjEmailPopUp.EmailSource = clsReportBLL.GetEmployeePassportReportD(Convert.ToInt32(objPassportBLL.Passport.PassportID));
            ObjEmailPopUp.ShowDialog();
        }
    }

    private void cboEmployee_KeyDown(object sender, KeyEventArgs e)
    {
        cboEmployee.DroppedDown = false;
    }

    private void cboCountry_KeyDown(object sender, KeyEventArgs e)
    {
        cboCountry.DroppedDown = false;
    }   

    private void btnSearch_Click(object sender, EventArgs e)
    {
        blnNewMode = false;
        if (txtSearch.Text == string.Empty)
            mblnSearchStatus = false;
        else
            mblnSearchStatus = true;

        GetRecordCount();
        if (intTotalRecords == 0) // No records
        {
            ShowMessage(40);
            txtSearch.Text = string.Empty;
            BindingNavigatorAddNewItem_Click(sender, e); 
        }
        else if (intTotalRecords > 0)
        {
            BindingNavigatorMoveFirstItem_Click(sender, e);
        }
    }

    private void frmPassport_KeyDown(object sender, KeyEventArgs e)
    {
        try
        {
            switch (e.KeyData)
            {
                case Keys.F1:
                    BtnHelp_Click(sender, new EventArgs());
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.Control | Keys.Enter:
                    if (BindingNavigatorAddNewItem.Enabled)
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item                
                    break;
                case Keys.Alt | Keys.R:
                    if (BindingNavigatorDeleteItem.Enabled)
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                    break;
                case Keys.Control | Keys.E:
                    if (btnClear.Enabled)
                        btnClear_Click(sender, new EventArgs());//Clear
                    break;
                case Keys.Control | Keys.Left:
                    if (bnMovePreviousItem.Enabled)
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                    break;
                case Keys.Control | Keys.Right:
                    if (bnMoveNextItem.Enabled)
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                    break;
                case Keys.Control | Keys.Up:
                    if (bnMoveFirstItem.Enabled)
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                    break;
                case Keys.Control | Keys.Down:
                    if (bnMoveLastItem.Enabled)
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                    break;
                case Keys.Control | Keys.P:
                    if (BtnPrint.Enabled)
                        BtnPrint_Click(sender, new EventArgs());//Print
                    break;
                case Keys.Control | Keys.M:
                    if (BtnEmail.Enabled)
                        BtnEmail_Click(sender, new EventArgs());//Email
                    break;
                case Keys.Alt | Keys.I:
                    if (issueToolStripMenuItem.Enabled)
                        receiptIssueToolStripMenuItem_Click(sender, new EventArgs());  //issue document
                    break;
                case Keys.Alt | Keys.E:
                    if (receiptToolStripMenuItem.Enabled)
                        receiptIssueToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                    break;
                case Keys.Alt | Keys.N:
                    if (renewToolStripMenuItem.Enabled)
                        renewToolStripMenuItem_Click(sender, new EventArgs());// renew document
                    break;
                case Keys.Alt | Keys.D:
                    if (mnuItemAttachDocuments.Enabled)
                        mnuItemAttachDocuments_Click(sender, new EventArgs()); // issue document
                    break;
            }
        }
        catch (Exception)
        {
        }
    }

    private void renewToolStripMenuItem_Click(object sender, EventArgs e)
    {
        try
        {
            if (objPassportBLL.Passport.PassportID.ToInt32() > 0)
            {
                string sMessage = MobjClsNotification.GetErrorMessage(datMessages, 17622, out objMsgIcon); // Do you wish to renew

                if (MessageBox.Show(sMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                else //YES Do you wish to add
                {
                    objPassportBLL.Passport.PassportID = objPassportBLL.Passport.PassportID.ToInt32();
                    objPassportBLL.RenewPassport(); // renewed status updationg, at the same time alert is removed
                    BindNavigator();

                    sMessage = MobjClsNotification.GetErrorMessage(datMessages, 17623, out objMsgIcon); // Do you wish to add
                    if (MessageBox.Show(sMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        AddNewHandler();
                        SetEnableDisable();
                        txtOldPassportNumbers.Text = txtOldPassportNumbers.Text == "" ? txtOldPassportNumbers.Text + txtPassportNumber.Text : txtOldPassportNumbers.Text + "," + txtPassportNumber.Text;
                        txtPassportNumber.Text = string.Empty;
                        blnNewMode = true;
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            MObjClsLogWriter.WriteLog("Error on Renew()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Renew()" + Ex.Message.ToString());
        }
    }
 
    private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
    {
        this.SaveHandler();
    }

    private void BtnHelp_Click(object sender, EventArgs e)
    {               
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "Passport"; Help.ShowDialog(); }      
    }
       
    private void txtSearch_KeyDown(object sender, KeyEventArgs e)
    {
        if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
        {
            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(sender, null);
        }
    }

    #endregion

    private void txtSearch_TextChanged(object sender, EventArgs e)
    {
        mblnSearchStatus = false;
    }
}
