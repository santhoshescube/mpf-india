﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace MyPayfriend
{

    /// <summary>
    ///  Modified By        : Laxmi
    ///  Modified Date      : 12/8/2013
    ///  Purpose            : Performance tuning
    ///  FormID             : 166
    ///  ErrorCode          : 5001 -
    ///  Functionalities    : Receipt/ Issue/ Attach documents
    ///                       checks duplication for  same degree exists for same employee
    /// </summary>
    /// 

    public partial class EmployeeQualification : Form
    {
        #region Variable declarations

        // Public variable declarations

        public long EmployeeID {get;set;}                //variable for employeeID FROM Employee form
        public int  PQualificationID {get;set;}          //variable for QualificationID FROM Navigator form
   
        private int MintRecordCnt = 0;                  //Total record count
        private int MintCurrentRecCnt = 0;              //current record
        
        //Permission variable declarations

        private bool MblnPrintEmailPermission   = false;    //To set Print Email Permission
        private bool MblnAddPermission          = false;          //To set Add Permission
        private bool MblnUpdatePermission       = false;      //To set Update Permission
        private bool MblnDeletePermission       = false;      //To set Delete Permission

        private bool MblnViewIssue , MblnViewReceipt , mblnEmail, mblnDelete, mblnUpdate = false;
   
        bool MblnAddStatus = true;                      // Insertion mode-true else false
        private bool mblnSearchStatus = false;
        private string MstrMessageCommon;               //to set the message
       
        private MessageBoxIcon MmessageIcon;            // to set the message icon
        private ArrayList MsarMessageArr;                  // Error Message display
        private string strOf = "of ";
        #endregion Variable Declarations

        clsBLLEmployeeQualification objclsBLLEmployeeQualification = null; // Object foe QualificationBLL class
        ClsNotification objclsNotification; // Object of NotificationClass for getting Messages
        ClsLogWriter objClsLogWriter;       // Object of the LogWriter class
       
        #region Constructor
        public EmployeeQualification()
        {
            InitializeComponent();
            objclsBLLEmployeeQualification = new clsBLLEmployeeQualification(); //new instance of BLL 
            objClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
           
            tmrEmployeeQualification.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }
        #endregion Constructor
                private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.Qualification, this);
            BtnActions.Text = "الإجراءات";
            BtnActions.ToolTipText = "الإجراءات";
            BtnScan.Text = "وثائق";
            ReceiptToolStripMenuItem.Text = "استلام";
            IssueToolStripMenuItem.Text = "قضية";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            strOf = "من ";
        }
        #region Events

        //Form Events
        private void EmployeeQualification_Load(object sender, EventArgs e)
        {
            this.SetAutoCompleteList(); // Seaching with employeename/number

            this.LoadMessage();          //get all message
            this.SetPermissions();       //Control permissions

            this.LoadInitials();      //go to add new mode

            if (EmployeeID > 0) // from Employee 
            {
                if (MintRecordCnt == 0)
                {
                    MintRecordCnt = 1;
                    BindingNavigatorCountItem.Text = MintRecordCnt.ToString();
                    BindingNavigatorPositionItem.Text = "1";
                }
                else if (MintRecordCnt  > 0)
                    BindingNavigatorMoveFirstItem_Click(sender, e);
            }
            if (PQualificationID > 0) // From Navigator
            {
                MintCurrentRecCnt  = 1;
                MintRecordCnt = 1;
                DisplayQualificationInfo();

               // Searching disabled
                bSearch.Enabled = false;
                lblStatus.Text = string.Empty;
                tmrEmployeeQualification.Enabled = true;
            }
        }
        private void EmployeeQualification_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        BtnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
               
                    case Keys.Control | Keys.Enter:
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        BtnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        BindingNavigatorMovePreviousItem_Click (sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        BindingNavigatorMoveNextItem_Click (sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        BindingNavigatorMoveFirstItem_Click (sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        BtnPrint_Click (sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        BtnEmail_Click (sender, new EventArgs());//Email
                        break;
                    case Keys.Alt | Keys.I:
                        if (IssueToolStripMenuItem.Enabled)
                            ReceiptToolStripMenuItem_Click(sender, new EventArgs());  //issue document
                        break;
                    case Keys.Alt | Keys.E:
                        if (ReceiptToolStripMenuItem.Enabled)
                            ReceiptToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                        break;
                    case Keys.Alt | Keys.D:
                        if (BtnScan.Enabled)
                            BtnScan_Click(sender, new EventArgs()); // issue document
                        break;
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on EmployeeQualification_KeyDown() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        private void EmployeeQualification_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnOk.Enabled)
            {
                // Checking the changes are not saved and shows warning to the user
                MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 8, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    objclsBLLEmployeeQualification =  null;
                    objclsNotification = null;
                     e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        //Button Events
        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            mblnSearchStatus = false;
            LoadInitials();
        }
        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (Save()) DoActionsAfterSave();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (Save()) DoActionsAfterSave(); 
        }
        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                BtnOk.Enabled = false;
                this.Close();
            }
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
                this.Close();
        }
        private void BtnClear_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            mblnSearchStatus = false;
            LoadInitials();
        }
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                int dtDocRequest = clsBLLDocumentMasterNew.GetDocRequestReferenceExists(objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID.ToInt32(), (int)DocumentType.Qualification);
                if (dtDocRequest > 0)
                {
                    string Message = "Cannot Delete as Document Request Exists.";
                    MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID > 0)
                    {
                        MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon).Replace("#", "").Trim();
                        if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            return;

                        if (objclsBLLEmployeeQualification.DeleteEmployeeQualification())
                        {
                            //message deleted successfully.
                            MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon).Replace("#", "").Trim();
                            MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                            LoadInitials();
                            SetAutoCompleteList();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on BindingNavigatorDeleteItem_Click() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.intCompanyID = clsBLLCommonUtility.GetCompanyID(CboEmployee.SelectedValue.ToInt32());
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID;
                    ObjViewer.PiFormID = (int)FormID.Qualification;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on BtnPrint_Click() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Employee Qualification";
                        ObjEmailPopUp.EmailFormType = EmailFormID.Qualification;
                        ObjEmailPopUp.EmailSource = objclsBLLEmployeeQualification.QualificationDetailsPrintEmail();
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on BtnEmail_Click() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        private void BtnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "Qualification"; Help.ShowDialog(); }
        }
        //Button navigation Events
        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            GetRecordCount(EmployeeID, PQualificationID);   // get no of records

            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;          // set current record
                DisplayQualificationInfo();     // display qualification details
                MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
                lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrEmployeeQualification.Enabled = true;
            }
        }
        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty; 

            GetRecordCount(EmployeeID, PQualificationID);   // get no of records
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                    MintCurrentRecCnt = 1;
                else
                {
                    DisplayQualificationInfo();     // display qualification details
                    MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 10, out MmessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrEmployeeQualification.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 1;
        }
        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            GetRecordCount(EmployeeID, PQualificationID);
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                    MintCurrentRecCnt = 1;
                else
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;

                if (MintCurrentRecCnt > MintRecordCnt)
                    MintCurrentRecCnt = MintRecordCnt;
                else
                {
                    DisplayQualificationInfo();
                    MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 11, out MmessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrEmployeeQualification.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 0;
        }
        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            GetRecordCount(EmployeeID, PQualificationID);
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayQualificationInfo();
                    MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 12, out MmessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrEmployeeQualification.Enabled = true;
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;
            GetRecordCount(EmployeeID, PQualificationID);
            if (MintRecordCnt == 0) // No records
            {
                MessageBox.Show(objclsNotification.GetErrorMessage(MsarMessageArr, 40, out MmessageIcon).Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
            }
            else if (MintRecordCnt > 0)
            {
                LoadInitials();
                BindingNavigatorMoveFirstItem_Click(sender, e);
            }
        }

        //Document/Receipt/Isse Button Events
        private void BtnScan_Click(object sender, EventArgs e)
        {
            try
            {
                if (objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID > 0)
                {
                    using (FrmScanning objScanning = new FrmScanning())
                    {
                        objScanning.MintDocumentTypeid = (int)DocumentType.Qualification;
                        objScanning.MlngReferenceID = CboEmployee.SelectedValue.ToInt32();
                        objScanning.MintOperationTypeID = (int)OperationType.Employee;
                        objScanning.MstrReferenceNo = txtRefNumber.Text.Trim();
                        objScanning.PblnEditable = true;
                        objScanning.MintVendorID = CboEmployee.SelectedValue.ToInt32();
                        objScanning.MintDocumentID = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID;
                        objScanning.ShowDialog();
                    }
                }
            }
            catch( Exception ex)
            {
                  objClsLogWriter.WriteLog("Error on BtnScan_Click() " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        private void ReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmDocumentReceiptIssue()
            {
                eOperationType = OperationType.Employee,
                eDocumentType = DocumentType.Qualification,
                DocumentID = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID,
                DocumentNumber = txtRefNumber.Text.Trim(),
                EmployeeID = CboEmployee.SelectedValue.ToInt32()
            }.ShowDialog();

            SetIssueReceiptButton();
        }

        //Reference Button Events
        private void BtnUniversity_Click(object sender, EventArgs e)
        {
            try
            {
                int MintComboID = CboUniversity.SelectedValue.ToInt32();
                FrmCommonRef objCommon = new FrmCommonRef("University", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "UniversityID,Description As University,DescriptionArb As UniversityArb", "UniversityReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                FillUniversity();
                if (objCommon.NewID != 0)
                    CboUniversity.SelectedValue = objCommon.NewID;
                else
                    CboUniversity.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on BtnUniversity_Click() " + this.Name + " " + ex.Message.ToString(), 2);       
            }
        }

        private void BtnDegree_Click(object sender, EventArgs e)
        {
            try
            {
                int MintComboID = CboDegree.SelectedValue.ToInt32();
                using (FrmDegreeReference objDegree =new FrmDegreeReference())
                {
                    objDegree.PstrFormName = "Qualification";
                    objDegree.PstrRefTableName = "QualificationTypeReference";
                    objDegree.PstrTableName = "PayDegreeReference";
                    objDegree.PstrRefValueField = "QualificationTypeID";
                    objDegree.PstrRefTextField = "QualificationType";

                    objDegree.PstrValueField = "DegreeID";
                    objDegree.PstrTextField = "Degree";
                    objDegree.PstrValue1Field = "QualificationTypeID";
                    objDegree.PintRefValue =CboDegree.SelectedValue.ToInt32() >0 ? (((System.Data.DataRowView)(CboDegree.SelectedItem)).Row.ItemArray[2]).ToInt32() : 0; 
                    objDegree.PintValue = CboDegree.SelectedValue.ToInt32();
                    objDegree.ShowDialog();  
                }
                FillDegree();
                CboDegree.SelectedValue = MintComboID;

            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on BtnDegree_Click() " + this.Name + " " + ex.Message.ToString(), 2);       
            }
        }

        //Combobox Events
      
        private void CboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboEmployee.DroppedDown = false;
        }
        private void CboUniversity_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboUniversity.DroppedDown = false;
        }
        private void CboDegree_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboDegree.DroppedDown = false;
        }   
        //Textbox  Events
        public void OnInputChanged(object sender, EventArgs e)
        {
            Changestatus ();
        }
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                if (e.KeyCode == Keys.Enter)
                    btnSearch_Click(sender, null);
            }
        }

        private void tmrEmployeeQualification_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmrEmployeeQualification.Enabled = false;
            errEmployeeQualification.Clear();
            tmrEmployeeQualification.Stop();
        }
        #endregion Events

        #region Functions

        /// <summary>
        /// Autocomplete
        /// </summary>
        /// 
        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsBLLInsuranceCard.GetAutoCompleteList(txtSearch.Text.Trim(), EmployeeID, "", "EmployeeQualificationDetails");
            this.txtSearch.AutoCompleteCustomSource = clsBLLInsuranceCard.ConvertToAutoCompleteCollection(dt);
        }

        /// <summary>
        /// Load combos
        /// </summary>
        private void LoadCombos()
        {
            DataTable dtCombos = null;
            if (EmployeeID > 0)
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    dtCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeID," +
                        "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstNAme", "" +
                        "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID > 5 and EmployeeID=" + EmployeeID + " and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });

                }
                else
                {
                    dtCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeID," +
                        "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstNAme", "" +
                        "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID > 5 and EmployeeID=" + EmployeeID + "and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });

                }
                CboEmployee.DisplayMember = "FirstName";
                CboEmployee.ValueMember = "EmployeeID";
                CboEmployee.DataSource = dtCombos;

                CboEmployee.SelectedValue = EmployeeID;
            }
            else
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    dtCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeID," +
                        "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstNAme", "" +
                        "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });

                }
                else
                {
                    dtCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeID," +
                        "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstNAme", "" +
                        "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });

                }
                CboEmployee.DisplayMember = "FirstName";
                CboEmployee.ValueMember = "EmployeeID";
                CboEmployee.DataSource = dtCombos;
            }
            FillUniversity();
            FillDegree();
        }

        private void FillUniversity()
        {
            DataTable dtCombo;
            if (ClsCommonSettings.IsArabicView)
            {
                dtCombo = new ClsCommonUtility().FillCombos(new string[] { "UniversityID,DescriptionArb AS Description", "UniversityReference", "" });
            }
            else
            {
                dtCombo = new ClsCommonUtility().FillCombos(new string[] { "UniversityID,Description", "UniversityReference", "" });
            }
            CboUniversity.DisplayMember = "Description";
            CboUniversity.ValueMember = "UniversityID";
            CboUniversity.DataSource = dtCombo;
        }

        /// <summary>
        /// Fill degree using datatable
        /// Reason for function -Selected QualificationtypeID in itemarray(2) used for calling degree form 
        /// </summary>
        private void FillDegree()
        {
            CboDegree.DataSource = objclsBLLEmployeeQualification.FillDegree();
            CboDegree.ValueMember = "DegreeID";
            CboDegree.DisplayMember = "Degree";
        }

        /// <summary>
        /// Fill employee 
        /// Filling ot in service Employees in update mode if qualification exists
        /// </summary>
        private void FillEmployee()
        {
            DataTable datCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = objclsBLLEmployeeQualification.FillCombos(new string[] { "EmployeeID, " +
                    "EmployeeFullNameArb + '-' + EmployeeNumber + ' ['+CM.ShortName+ ']' as FirstNAme", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") OR EmployeeID = " + objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.EmployeeID });

            }
            else
            {
                datCombos = objclsBLLEmployeeQualification.FillCombos(new string[] { "EmployeeID, " +
                    "EmployeeFullName + '-' + EmployeeNumber + ' ['+CM.ShortName+ ']' as FirstNAme", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") OR EmployeeID = " + objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.EmployeeID });

            }
            CboEmployee.DisplayMember = "FirstNAme";
            CboEmployee.ValueMember = "EmployeeID";
            CboEmployee.DataSource = datCombos;
        }

        /// <summary>
        /// Loads all message
        /// </summary>
        private void LoadMessage()
        {
            try
            {
                // Loading Message
                objclsNotification = new ClsNotification();
                MsarMessageArr = new ArrayList();
                MsarMessageArr = objclsNotification.FillMessageArray((int)FormID.Qualification, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                 objClsLogWriter.WriteLog("Error on LoadMessage() " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        /// <summary>
        /// Clear all form controls
        /// </summary>
        private void ClearAllControls()
        {
           // txtSearch.Text = string.Empty;

            CboEmployee.SelectedIndex = -1;
            CboEmployee.Text = "";
            CboUniversity.SelectedValue = 0;
            CboUniversity.SelectedIndex = -1;
            CboDegree.SelectedValue = 0;

            txtSchoolOrCollege.Text = txtCertificate.Text =txtGradeorPercentage.Text = txtRemarks.Text =txtRefNumber.Text = "";

            ChkCertificatesVerified.Checked = false;
            ChkCertificatesAttested.Checked = false;
            ChkUniversityClearenceRequired.Checked = false;
            ChkClearenceCompleted.Checked = false;

            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID = 0;
            dtpToDate.Value = dtpFromDate.Value = ClsCommonSettings.GetServerDate();
        }

        /// <summary>
        /// Add new mode
        /// </summary>
        private void LoadInitials()
        {
            try
            {
                LoadCombos();

                ClearAllControls();

                MblnAddStatus = true;
            
                GetRecordCount(EmployeeID ,PQualificationID);   // gets no of records

                MintRecordCnt = MintRecordCnt + 1;
                MintCurrentRecCnt = MintRecordCnt;
                BindingNavigatorCountItem.Text = strOf + Convert.ToString(MintRecordCnt) + "";
                BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();

                MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 655, out MmessageIcon);
                lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrEmployeeQualification.Enabled = true;

                if (EmployeeID > 0)
                {
                    CboEmployee.Enabled = false;
                    CboEmployee.SelectedValue = EmployeeID;
                }
                else
                {
                    CboEmployee.Enabled = true;
                    CboEmployee.Focus();
                }

                SetEnableDisable();
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadInitials " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }

        /// <summary>
        /// Gets no of records
        /// </summary>
        private void GetRecordCount(long intEmpID, int intQid)
        {
            try
            {
                MintRecordCnt = 0;

                MintRecordCnt = objclsBLLEmployeeQualification.GetRecordCount(EmployeeID, PQualificationID, txtSearch.Text.Trim());
                if (MintRecordCnt < 0)
                {
                    BindingNavigatorCountItem.Text = "of 0";
                    MintRecordCnt = 0;
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on GetRecordCount() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }

        /// <summary>
        /// save or update employee qualification
        /// </summary>
        /// <returns>bool</returns>
        private bool Save()
        {
            try
            {
                if (FormValidation())   // validate form controls
                {

                    if (MblnAddStatus )
                        MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 1, out MmessageIcon);
                    else
                        MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 3, out MmessageIcon);

                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;

                    FillParameters();

                    objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID = objclsBLLEmployeeQualification.Save();

                    if (MblnAddStatus)//insert
                    {
                        MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                    }
                    else
                    {
                        MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon);
                    }

                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrEmployeeQualification.Enabled = true;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on Save " + this.Name + " " + ex.Message.ToString(), 1);
                return false;
            }
        }

        /// <summary>
        /// Validates form controls
        /// </summary>
        /// <returns>bool</returns>
        private bool FormValidation()
        {
            errEmployeeQualification.Clear();
            bool bReturnValue = true;
            Control cControl = new Control();

            if (CboEmployee.SelectedIndex == -1)        //employee validation
            {
                MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 5001, out MmessageIcon);
                cControl = CboEmployee ;
                bReturnValue = false;

            }
       

            else if (CboDegree.SelectedIndex == -1)     //degree validation
            {
                MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 5002, out MmessageIcon);
                cControl = CboDegree;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtRefNumber.Text.Trim()))// Reference Number
            {
                MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 5011, out MmessageIcon);
                cControl = txtRefNumber;
                bReturnValue = false;
            }
            else if (dtpFromDate.Value > ClsCommonSettings.GetServerDate())     // date validation
            {
                MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 5007, out MmessageIcon);
                cControl = dtpFromDate ;
                bReturnValue = false;
            }
            else if (dtpToDate.Value.Date < dtpFromDate.Value.Date)     // date validation
            {
                MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 5003, out MmessageIcon);
                cControl = dtpFromDate;
                bReturnValue = false;
            }
            else
            {
                if (IsExistDegree()) // checks if same degree exists for employee
                {
                    MstrMessageCommon = objclsNotification.GetErrorMessage(MsarMessageArr, 5010, out MmessageIcon);
                    cControl = CboDegree ;
                    bReturnValue = false;
                }
            }
            if (bReturnValue == false)
            {
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption , MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (cControl != null)
                {
                    errEmployeeQualification.SetError(cControl, MstrMessageCommon);
                    cControl.Focus();
                }
                lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrEmployeeQualification.Enabled = true;
            }

            return bReturnValue;
        }

        private void FillParameters()
        {
            //fill business objects
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.EmployeeID = CboEmployee.SelectedValue.ToInt32();
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.UniversityID = CboUniversity.SelectedValue.ToInt32();
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.ReferenceNumber = txtRefNumber.Text.Trim();
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.SchoolOrCollege = txtSchoolOrCollege.Text;
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.DegreeID = CboDegree.SelectedValue.ToInt32();
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.CertificateTitle = txtCertificate.Text;
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.GradeorPercentage = txtGradeorPercentage.Text;

            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.FromDate = dtpFromDate.Value;
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.ToDate = dtpToDate.Value;

            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.CertificatesVerified = ChkCertificatesVerified.Checked ? 1 : 0;
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.CertificatesAttested = ChkCertificatesAttested.Checked ? 1 : 0;
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.UniversityClearenceRequired = ChkUniversityClearenceRequired.Checked ? 1 : 0;
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.ClearenceCompleted = ChkClearenceCompleted.Checked ? 1 : 0;
            objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.Remarks = txtRemarks.Text.Trim();
        }
        /// <summary>
        /// Checks if same degree exists for employee
        /// </summary>
        /// <returns>bool</returns>
        private bool IsExistDegree()
        {
            if (objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID > 0)
            {
                if (objclsBLLEmployeeQualification.FillCombos(new string[] { "QualificationID", "EmployeeQualificationDetails", "DegreeID = " + CboDegree.SelectedValue.ToInt32() + " AND EmployeeID = "+CboEmployee.SelectedValue.ToInt32()+" AND QualificationID <>" + objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID }).Rows.Count > 0)
                {
                    return true;
                }
            }
            else
            {
                if (objclsBLLEmployeeQualification.FillCombos(new string[] { "QualificationID", "EmployeeQualificationDetails", "DegreeID = " + CboDegree.SelectedValue.ToInt32() + " AND EmployeeID = " + CboEmployee.SelectedValue.ToInt32() }).Rows.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Set user permissions
        /// </summary>
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.Qualification, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentIssue, out mblnEmail, out MblnViewIssue, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentReceipt, out mblnEmail, out MblnViewReceipt, out mblnUpdate, out mblnDelete);

               
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = MblnViewIssue = MblnViewReceipt = true;

        }

        /// <summary>
        /// Display Qualification details
        /// </summary>
        private void DisplayQualificationInfo()
        {
            try
            {
                if (objclsBLLEmployeeQualification.DisplayQualificationInfo(MintCurrentRecCnt, EmployeeID, PQualificationID, txtSearch.Text.Trim()))//get qualification details
                {
                    MblnAddStatus = false;
                    //set vaalues to control
                    FillEmployee();
                    CboEmployee.SelectedValue = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.EmployeeID;
                    CboUniversity.SelectedValue = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.UniversityID;
                    txtSchoolOrCollege.Text = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.SchoolOrCollege;
                    CboDegree.SelectedValue = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.DegreeID;
                    txtCertificate.Text = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.CertificateTitle;
                    txtGradeorPercentage.Text = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.GradeorPercentage;
                    dtpFromDate.Value = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.FromDate;
                    dtpToDate.Value = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.ToDate;
                    ChkCertificatesAttested.Checked = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.CertificatesAttested == 1 ? true : false;
                    ChkCertificatesVerified.Checked = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.CertificatesVerified == 1 ? true : false;
                    ChkClearenceCompleted.Checked = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.ClearenceCompleted == 1 ? true : false;
                    ChkUniversityClearenceRequired.Checked = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.UniversityClearenceRequired == 1 ? true : false;
                    txtRemarks.Text = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.Remarks;
                    txtRefNumber.Text = objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.ReferenceNumber;

                    MblnAddStatus = false;

                    BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
                    BindingNavigatorCountItem.Text = strOf + Convert.ToString(MintRecordCnt) + "";

                     BindingNavigatorAddNewItem.Enabled = MblnAddPermission;

                    SetEnableDisable();
                    SetIssueReceiptButton();
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on DisplayQualificationInfo() " + this.Name + " " + ex.Message.ToString(), 1);
            }
          
        }

        /// <summary>
        /// Enable or disable issue and receipt button
        /// </summary>
        private void SetIssueReceiptButton()
        {
            BtnActions.Enabled = true;
            IssueToolStripMenuItem.Enabled = ReceiptToolStripMenuItem.Enabled = false;

            bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Employee, (int)DocumentType.Qualification, objclsBLLEmployeeQualification.objclsDTOEmployeeQualification.QualificationID);

            if (IsReceipt)
                IssueToolStripMenuItem.Enabled = MblnViewIssue;
            else
                ReceiptToolStripMenuItem.Enabled = MblnViewReceipt;
        }
 
        /// <summary>
        /// Enable and disable binding navigator buttons
        /// </summary>
        private void SetEnableDisable()
        {
            BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = false;
            BtnPrint.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            BtnEmail.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            BtnActions.Enabled = (MblnAddStatus == true ? false : true);

            BindingNavigatorAddNewItem.Enabled = (MblnAddStatus == true ? false : (PQualificationID > 0 ? false : MblnAddPermission ));
            BindingNavigatorDeleteItem.Enabled = (MblnAddStatus == true ? false : (PQualificationID > 0 ? false : MblnDeletePermission));

            BtnClear.Enabled = ((MblnAddStatus) ? true : false);

            if (PQualificationID  <= 0)
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = true;

                int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
                int iRecordCnt = MintRecordCnt;  // Total count of the records

                if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                {
                    BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
                }
                if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
            else
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false ;
        }

        /// <summary>
        /// Enable and disable controls on selection change
        /// </summary>
        private void Changestatus()
        {
            errEmployeeQualification.Clear();
            //if (ClsCommonSettings.IsHrPowerEnabled)
            //{
            //    BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = false;
            //    return;
            //}
            //function for changing status
            if (MblnAddStatus) // add mode
            {
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled  = MblnAddPermission;
            }
            else  //edit mode
            {
                BtnOk.Enabled =  BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
        }

        /// <summary>
        /// Do actions after save
        /// Enable and disable form controls
        /// </summary>
        private void DoActionsAfterSave()
        {
            SetAutoCompleteList();

            MblnAddStatus = false;

            BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled =BtnClear.Enabled =  false; 
            BtnScan.Enabled = MblnAddPermission;
            BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
            BindingNavigatorAddNewItem.Enabled = (PQualificationID > 0 ? false : MblnAddPermission);
            BindingNavigatorDeleteItem.Enabled = (PQualificationID > 0 ? false : MblnDeletePermission);

            SetIssueReceiptButton();
        }
        #endregion Functions

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }
    }
}
