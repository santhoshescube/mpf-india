﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using MyPayfriend;

/* *******************************************

Author      :		 <Laxmi>
Create date :        <13 Apr 2012>
Description :	     <Stock Register> 
Modified By :        Laxmi
Modified date:       20 Aug 2013
 
*******************************************
*/

public partial class frmStockRegister : DevComponents.DotNetBar.Office2007Form
{

    #region VariableDeclarations

    string MstrCommonMessage; // for message display


    private int TotalRecords { get; set; } // Total count for pager
    private int CurrentIndex { get; set; } // Current page index
    private int PageSize { get; set; } // pagecount (25/50/100)

    private int OperationTypeID { get; set; } // used in grid mouseclick
    private int DocumentTypeID { get; set; } // used in grid mouseclick
    private int DocumentID { get; set; } // used in grid mouseclick
    private int OrderNo { get; set; }   // used in grid mouseclick

    private bool IsFirstTime = true;  //setting IsFirstTime = true to avoid filling the datagrid when the selected index changes 

    private bool MblnAddPermissionReceipt; // Permissions
    private bool MblnUpdatePermissionReceipt;
    private bool MblnDeletePermissionReceipt;
    private bool MblnPrintEmailPermissionReceipt;
    private bool MblnViewPermissionReceipt;


    private bool MblnAddPermissionIssue;
    private bool MblnUpdatePermissionIssue;
    private bool MblnDeletePermissionIssue;
    private bool MblnPrintEmailPermissionIssue;
    private bool MblnViewPermissionIssue;

    // Class objects

    private clsBLLDocumentMasterNew MobjClsBLLDocumentMaster = null;
    private clsMessage ObjUserMessage = null;
    ClsLogWriter objClsLogWriter;       // Object of the LogWriter class

    #endregion

    #region Constructor
    public frmStockRegister()
    {
        InitializeComponent();
        objClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
        if (ClsCommonSettings.IsArabicView)
        {
            this.RightToLeftLayout = true;
            this.RightToLeft = RightToLeft.Yes;
            SetArabicControls();
        }



    }
    #endregion
    private void SetArabicControls()
    {
        ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        objDAL.SetArabicVersion((int)FormID.StockRegister, this);
        wbDocArb.Visible = true;
        wbDocs.Visible = false;
        gpView.Text = "رأي";
        rbtnAll.Text = "وثيقة تسجيل";
        rbtReceipt.Text = "مستندات متاح";
        rbtIssue.Text = "أصدر مستندات";
        gpSearch.Text = "بحث";
        lblType.Text = "نوع";
        lblDocumentType.Text = "نوع الوثيقة";
        lblDocumentNumber.Text = "عدد الوثيقة";
        lblTotalRecords.Text = "مجموع السجلات";
        GrpPanelDocMap.Text = "خريطة المستند";


        rbtnAll.Location = new Point(12,3);
        rbtReceipt.Location = new Point(34, 26);  
        rbtIssue.Location = new Point(45,52);
       

    }
    #region Properties
    private clsMessage UserMessage
    {
        get
        {
            if (this.ObjUserMessage == null)
                this.ObjUserMessage = new clsMessage(FormID.Documents);
            return this.ObjUserMessage;
        }
    }

    private clsBLLDocumentMasterNew BLLDocumentMaster
    {
        get
        {
            if (this.MobjClsBLLDocumentMaster == null)
                this.MobjClsBLLDocumentMaster = new clsBLLDocumentMasterNew();
            return this.MobjClsBLLDocumentMaster;
        }
    }
    #endregion

    #region Events

    // Form Events
    private void frmStockRegister_Load(object sender, EventArgs e)
    {
        CurrentIndex = 0;

        SetPermissionForReceiptIssue();
        //setting IsFirstTime = true to avoid filling the datagrid when the selected index changes 
        IsFirstTime = true;
        LoadCombo();
        IsFirstTime = false;
    }
    private void frmStockRegister_Shown(object sender, EventArgs e)
    {
        FilldgvDocGrid();
    }

    private void dgvDocGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
    {
        try
        {
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on dgvDocGrid_DataError() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }
    private void dgvDocGrid_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
        try
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    OperationTypeID = dgvDocGrid.Rows[e.RowIndex].Cells[3].Value.ToInt16();
                    DocumentTypeID = dgvDocGrid.Rows[e.RowIndex].Cells[4].Value.ToInt16();
                    DocumentID = dgvDocGrid.Rows[e.RowIndex].Cells[5].Value.ToInt16();

                    ShowDocumentDetails(dgvDocGrid.Rows[e.RowIndex].Cells[(int)ColumnType.clmDocumentID].Value.ToInt32(), e.RowIndex);
                }
            }
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on dgvDocGrid_CellMouseClick() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }
    private void dgvDocGrid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
        try
        {
            if (e.RowIndex >= 0)
            {
                OperationTypeID = dgvDocGrid.Rows[e.RowIndex].Cells["clmOperationTypeID"].Value.ToInt32();
                DocumentTypeID = dgvDocGrid.Rows[e.RowIndex].Cells["clmDocumentTypeID"].Value.ToInt32();
                DocumentID = dgvDocGrid.Rows[e.RowIndex].Cells["clmDocumentID"].Value.ToInt32();
                OrderNo = dgvDocGrid.Rows[e.RowIndex].Cells["clmOrderNo"].Value.ToInt32();


                if (e.ColumnIndex == 1 && dgvDocGrid.Rows[e.RowIndex].Cells["clmPrint"].Tag.ToBoolean() == true)
                {
                    frmDocumentReceiptIssue obj = new frmDocumentReceiptIssue();

                    obj.eOperationType = (OperationType)OperationTypeID;
                    obj.eDocumentType = (DocumentType)DocumentTypeID;
                    obj.DocumentID = DocumentID;
                    obj.OrderNo = OrderNo;
                    obj.IsFromStockRegister = true;
                    obj.SetInitials();
                    obj.LoadReport();
                }
                else if (e.ColumnIndex != 1)
                {

                    bool IsReceipted = clsBLLDocumentMasterNew.IsReceipted((int)(OperationType)OperationTypeID, (int)(DocumentType)DocumentTypeID, DocumentID);
                    if (OperationTypeID == 1 || (OperationTypeID == 2 && !IsReceipted))
                    {
                        new frmDocumentReceiptIssue()
                        {
                            eOperationType = (OperationType)OperationTypeID,
                            eDocumentType = (DocumentType)DocumentTypeID,
                            DocumentID = DocumentID,
                            DocumentNumber = dgvDocGrid.Rows[e.RowIndex].Cells["clmDocumentNumber"].Value.ToString(),
                            EmployeeID = dgvDocGrid.Rows[e.RowIndex].Cells["clmReceivedFrom"].Value.ToInt32(),
                            CanUpdateFromStockRegister = true

                        }.ShowDialog();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on dgvDocGrid_CellMouseDoubleClick() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }

    private void dgvDocSummary_DataError(object sender, DataGridViewDataErrorEventArgs e)
    {
        try { }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on dgvDocSummary_DataError() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }
    private void dgvDocSummary_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
        try
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == 1 && e.RowIndex == 0)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(13);
                        if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                       DialogResult.Yes)
                        {

                            clsBLLDocumentMasterNew.DeleteDocumentReceiptIssue(OperationTypeID, DocumentTypeID, DocumentID, dgvDocSummary.Rows[e.RowIndex].Cells["OrderNo"].Value.ToInt32());

                            FilldgvDocGrid();
                            ShowDocumentDetails(dgvDocSummary.Rows[e.RowIndex].Cells["DocumentID"].Value.ToInt32(), e.RowIndex);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on dgvDocSummary_CellMouseClick() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }
    private void dgvDocSummary_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
        int Status = 0;
        try
        {
            if (e.RowIndex >= 0)
            {
                OperationTypeID = dgvDocSummary.Rows[e.RowIndex].Cells["OperationTypeID"].Value.ToInt32();
                DocumentTypeID = dgvDocSummary.Rows[e.RowIndex].Cells["DocumentTypeID"].Value.ToInt32();
                DocumentID = dgvDocSummary.Rows[e.RowIndex].Cells["DocumentID"].Value.ToInt32();
                OrderNo = dgvDocSummary.Rows[e.RowIndex].Cells["OrderNo"].Value.ToInt32();
                Status = Convert.ToInt32(dgvDocSummary.Rows[e.RowIndex].Cells["Status"].Value);

                bool IsReceipted = clsBLLDocumentMasterNew.IsReceipted((int)(OperationType)OperationTypeID, (int)(DocumentType)DocumentTypeID, DocumentID);
                if (OperationTypeID == 1 || (OperationTypeID == 2 && Status == 1))
                {

                    new frmDocumentReceiptIssue()
                    {
                        eOperationType = (OperationType)OperationTypeID,
                        eDocumentType = (DocumentType)DocumentTypeID,
                        DocumentID = DocumentID,
                        OrderNo = OrderNo,
                        IsFromStockRegister = true,
                        CanUpdateFromStockRegister = (e.RowIndex == 0 ? true : false)
                    }.ShowDialog();

                }
            }
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on dgvDocSummary_CellMouseDoubleClick() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }

    private void rbtnAll_CheckedChanged(object sender, EventArgs e)
    {
        if (!IsFirstTime)
        {
            FilldgvDocGrid();

            //bck_Wrkr.RunWorkerAsync();
            pnlBottom.Visible = false;
        }

    }
    private void CancelToolStripButton_Click(object sender, EventArgs e)
    {
        cboDocumentType.SelectedIndex = 0;
        cboOperationType.SelectedIndex = 0;
    }
    private void HelpToolStripButton_Click(object sender, EventArgs e)
    {
        using (FrmHelp objHelp = new FrmHelp())
        {
            objHelp.strFormName = "StockRegister";
            objHelp.ShowDialog();
        }
    }
    // Pager events       
    private void cboPageCount_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!IsFirstTime)
        {
            PageSize = Convert.ToInt32(cboPageCount.Text);
            CurrentIndex = 0;
            FilldgvDocGrid();
        }
    }
    private void BtnFirst_Click(object sender, EventArgs e)
    {
        CurrentIndex = 0;
        FilldgvDocGrid();
    }
    private void BtnPrevious_Click(object sender, EventArgs e)
    {
        CurrentIndex = CurrentIndex - 1;
        FilldgvDocGrid();
    }
    private void BtnNext_Click(object sender, EventArgs e)
    {
        CurrentIndex = CurrentIndex + 1;
        FilldgvDocGrid();
    }
    private void BtnLast_Click(object sender, EventArgs e)
    {
        CurrentIndex = (TotalRecords / PageSize);
        FilldgvDocGrid();
    }
    // Combobox events
    private void cboOperationType_SelectedValueChanged(object sender, EventArgs e)
    {
        FillDocumentNumber();
    }
    private void cboDocumentType_SelectedValueChanged(object sender, EventArgs e)
    {
        FillDocumentNumber();
    }
    private void cboOperationType_KeyDown(object sender, KeyEventArgs e)
    {
        try { cboOperationType.DroppedDown = false; } 
        catch { }
    }
    private void cboDocumentType_KeyDown(object sender, KeyEventArgs e)
    {
        try { cboDocumentType.DroppedDown = false; }
        catch { }
    }
    private void cboDocumentNumber_KeyDown(object sender, KeyEventArgs e)
    {
        try { cboDocumentNumber.DroppedDown = false; }
        catch { }
    }

    #endregion

    #region Functions
    /// <summary>
    /// Permission settings
    /// </summary>
    private void SetPermissionForReceiptIssue()
    {
        clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
        if (ClsCommonSettings.RoleID > 3)
        {
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentReceipt, out MblnPrintEmailPermissionReceipt, out MblnAddPermissionReceipt, out MblnUpdatePermissionReceipt, out MblnDeletePermissionReceipt, out MblnViewPermissionReceipt);
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentIssue, out MblnPrintEmailPermissionIssue, out MblnAddPermissionIssue, out MblnUpdatePermissionIssue, out MblnDeletePermissionIssue, out MblnViewPermissionIssue);

        }
        else
        {
            MblnAddPermissionReceipt = MblnPrintEmailPermissionReceipt = MblnUpdatePermissionReceipt = MblnDeletePermissionReceipt = MblnViewPermissionReceipt = true;
            MblnAddPermissionIssue = MblnPrintEmailPermissionIssue = MblnUpdatePermissionIssue = MblnDeletePermissionIssue = MblnViewPermissionIssue = true;
        }

        if (MblnAddPermissionReceipt == false && MblnUpdatePermissionReceipt == false)
            MblnViewPermissionReceipt = false;
        else
            MblnViewPermissionReceipt = true;

        if (MblnAddPermissionIssue == false && MblnUpdatePermissionIssue == false)
            MblnViewPermissionIssue = false;
        else
            MblnViewPermissionIssue = true;

    }
    private void FillCombos()
    {
        GetAllDocumentTypes();
        GetAllOperationType();
    }
    /// <summary>
    /// Fill document number based on OPeration type and documenttype
    /// </summary>
    private void FillDocumentNumber()
    {
        DataTable dtDocumentNumber = clsBLLDocumentMasterNew.GetAllDocumentNumber(cboOperationType.SelectedValue.ToInt32(), cboDocumentType.SelectedValue.ToInt32());
        DataRow dr = dtDocumentNumber.NewRow();
        if (ClsCommonSettings.IsArabicView)
        {
            dr["DocumentNumber"] = "أي";
        }
        else
        {
            dr["DocumentNumber"] = "Any";
        }
        dr["DocumentID"] = 0;

        dtDocumentNumber.Rows.InsertAt(dr, 0);

        cboDocumentNumber.DataSource = dtDocumentNumber;
        cboDocumentNumber.DisplayMember = "DocumentNumber";
        cboDocumentNumber.ValueMember = "DocumentID";
    }
    /// <summary>
    /// Set enable disable paging button
    /// </summary>

    private void EnableDisableAllPagingControl(bool status)
    {
        BtnLast.Enabled = BtnFirst.Enabled = BtnNext.Enabled = BtnPrevious.Enabled = status;
    }
    /// <summary>
    /// Fill all Document types
    /// </summary>
    private void GetAllDocumentTypes()
    {
        DataTable dtDocumentType = clsBLLDocumentMasterNew.GetAllDocumentTypes();
        DataRow dr = dtDocumentType.NewRow();

        dr["DocumentTypeID"] = 0;
        if (ClsCommonSettings.IsArabicView)
        {
            dr["DocumentType"] = "أي";
        }
        else
        {
            dr["DocumentType"] = "Any";
        }
        dtDocumentType.Rows.InsertAt(dr, 0);

        cboDocumentType.DataSource = dtDocumentType;
        cboDocumentType.DisplayMember = "DocumentType";
        cboDocumentType.ValueMember = "DocumentTypeID";
        cboDocumentType.SelectedIndex = 0;
    }
    /// <summary>
    /// Fill all Opeartion types Company /Employee
    /// </summary>
    private void GetAllOperationType()
    {
        DataTable dtOperationType = clsBLLDocumentMasterNew.GetAllOperationTypes();

        DataRow dr = dtOperationType.NewRow();

        dr["OperationTypeID"] = 0;
        if (ClsCommonSettings.IsArabicView)
        {
            dr["OperationType"] = "أي";
        }
        else
        {
            dr["OperationType"] = "Any";
        }
        dtOperationType.Rows.InsertAt(dr, 0);


        cboOperationType.DataSource = dtOperationType;
        cboOperationType.DisplayMember = "OperationType";
        cboOperationType.ValueMember = "OperationTypeID";
        cboOperationType.SelectedIndex = 0;
    }
    /// <summary>
    /// Fill documents 
    /// </summary>
    public void FilldgvDocGrid()
    {
        int StatusID = -1;
        DataTable dtMain = null;

        //Binding with null values
        dgvDocGrid.DataSource = clsBLLDocumentMasterNew.GetStockRegisterDocuments(-2, CurrentIndex, Convert.ToInt32(cboPageCount.Text), Convert.ToInt32(cboOperationType.SelectedValue), Convert.ToInt32(cboDocumentType.SelectedValue), cboDocumentNumber.Text);
        //dgvDocSummary.DataSource = null;

        EnableDisableAllPagingControl(false);

        // dgvDocSummary.Columns["Img"].Visible = false;

        StatusID = (rbtnAll.Checked ? -1 : (rbtReceipt.Checked ? 1 : 0));

        TotalRecords = clsBLLDocumentMasterNew.GetStockRegisterTotalRecords(StatusID, Convert.ToInt32(cboOperationType.SelectedValue), Convert.ToInt32(cboDocumentType.SelectedValue), cboDocumentNumber.SelectedIndex == 0 ? "" : cboDocumentNumber.Text);
        dtMain = clsBLLDocumentMasterNew.GetStockRegisterDocuments(StatusID, CurrentIndex, Convert.ToInt32(cboPageCount.Text), Convert.ToInt32(cboOperationType.SelectedValue), Convert.ToInt32(cboDocumentType.SelectedValue), cboDocumentNumber.SelectedIndex == 0 ? "" : cboDocumentNumber.Text);
        lblTotalDocs.Text = TotalRecords.ToString();
        SetPageDetailsCaption();
        dgvDocGrid.DataSource = dtMain;
        dgvDocGrid.Columns["clmPrint"].DisplayIndex = 19;

        SetGridImage();
        EnableDisableAllPagingControl(true);
        EnableOrDisablePagingControls();
        EnableDisablePrintDelete();
        if (ClsCommonSettings.IsArabicView)
        {
            dgvDocGrid.Columns["clmType"].HeaderText = "نوع الوثيقة";
            dgvDocGrid.Columns["clmReference"].HeaderText = "عامل";
            dgvDocGrid.Columns["clmDocumentNumber"].HeaderText = "عدد الوثيقة";
            dgvDocGrid.Columns["clmDate"].HeaderText = "تاريخ";
            dgvDocGrid.Columns["ExpiryDate"].HeaderText = "تاريخ انتهاء الصلاحية";
            dgvDocGrid.Columns["clmCustodian"].HeaderText = "ولي";
            dgvDocGrid.Columns["clmDocumentStatus"].HeaderText = "الحالة ثيقة";
            dgvDocGrid.Columns["clmBinDetails"].HeaderText = "بن عدد";
            dgvDocGrid.Columns["clmIssueReason"].HeaderText = "سبب المشكلة";
            dgvDocGrid.Columns["clmReturnDate"].HeaderText = "العودة التسجيل";
            dgvDocGrid.Columns["clmRemarks"].HeaderText = "تصريحات";
            //dgvDocGrid.Columns["clmOrderNo"].HeaderText = "";
            //dgvDocGrid.Columns["clmReceivedFrom"].HeaderText = "";
        }

    }

    private void EnableDisablePrintDelete()
    {
        foreach (DataGridViewRow dr in dgvDocGrid.Rows)
        {
            if (dr.Cells["clmStatusID"].Value.ToInt32() == 1)
            {
                dr.Cells["clmPrint"].Tag = MblnPrintEmailPermissionReceipt;

                if (MblnPrintEmailPermissionReceipt)
                    dr.Cells["clmPrint"].Value = global::MyPayfriend.Properties.Resources.Print;
                else
                    dr.Cells["clmPrint"].Value = global::MyPayfriend.Properties.Resources.Print_inactive;

            }

            else if (dr.Cells["clmStatusID"].Value.ToInt32() == 0)
            {
                dr.Cells["clmPrint"].Tag = MblnPrintEmailPermissionIssue;

                if (MblnPrintEmailPermissionIssue)
                    dr.Cells["clmPrint"].Value = global::MyPayfriend.Properties.Resources.Print;
                else
                    dr.Cells["clmPrint"].Value = global::MyPayfriend.Properties.Resources.Print_inactive;
            }
        }
    }
    /// <summary>
    /// Setting pager properties
    /// </summary>
    private void SetPageDetailsCaption()
    {
        int TotalIndex = (TotalRecords / PageSize) + ((TotalRecords % PageSize) > 0 ? 1 : 0);

        lblPageCount.Text = string.Format("{0} of {1}", CurrentIndex < TotalIndex ? CurrentIndex + 1 : CurrentIndex, TotalIndex);
        // BarNavigation.Visible = ((TotalRecords / PageSize) > 0);
    }

    private void EnableOrDisablePagingControls()
    {
        BtnFirst.Enabled = BtnPrevious.Enabled = (CurrentIndex > 0);

        BtnNext.Enabled = BtnLast.Enabled = (CurrentIndex != (TotalRecords / PageSize));
    }
    /// <summary>
    /// Set image for document receipt/issue
    /// </summary>
    private void SetGridImage()
    {
        if (dgvDocGrid.Rows.Count > 0)
        {
            for (int i = 0; i < dgvDocGrid.RowCount; i++)
            {
                if (Convert.ToInt32(dgvDocGrid.Rows[i].Cells["clmStatusID"].Value) == 1)//Receipt
                {
                    dgvDocGrid.Rows[i].Cells["clmImage"].Value = global::MyPayfriend.Properties.Resources.DocumentIn;//ImageList1.Images[0];// In
                    dgvDocGrid.Rows[i].Cells["clmImage"].ToolTipText = "In";
                }
                else
                {
                    dgvDocGrid.Rows[i].Cells["clmImage"].Value = global::MyPayfriend.Properties.Resources.DocumentOut;
                    dgvDocGrid.Rows[i].Cells["clmImage"].ToolTipText = "Out";
                }
            }
            dgvDocGrid.ClearSelection();
        }
    }

    public void ShowDocumentDetails(int iDocumentID, int iRowindex)
    {
        pnlBottom.Visible = true;
        dgvDocSummary.DataSource = null;
        DataTable dtDocDetails = null;
        dtDocDetails = clsBLLDocumentMasterNew.GetAllTransactionsByDocumentID(OperationTypeID, DocumentTypeID, DocumentID);

        if (dtDocDetails.Rows.Count > 0)
        {
            if (ClsCommonSettings.IsArabicView)
            {
                GrpPanelDocMap.Text = "(" + dgvDocGrid.Rows[iRowindex].Cells[9].Value + ")" + dgvDocGrid.Rows[iRowindex].Cells[6].Value + " - خريطة المستند";
            }
            else
            {
                GrpPanelDocMap.Text = "Document Map - " + dgvDocGrid.Rows[iRowindex].Cells[6].Value + "(" + dgvDocGrid.Rows[iRowindex].Cells[9].Value + ")";

            }
            dgvDocSummary.DataSource = dtDocDetails;

            DisableSorting();
            dgvDocSummary.Columns["OperationTypeID"].Visible = false;
            dgvDocSummary.Columns["DocumentTypeID"].Visible = false;
            dgvDocSummary.Columns["DocumentID"].Visible = false;
            dgvDocSummary.Columns["Status"].Visible = false;
            dgvDocSummary.Columns["OrderNo"].Visible = false;
            dgvDocSummary.Columns["Remarks"].AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dgvDocSummary.Columns["Img"].Visible = true;

            if (ClsCommonSettings.IsArabicView)
            {
                dgvDocSummary.Columns["Employee"].HeaderText = "عامل";
                dgvDocSummary.Columns["DocumentNumber"].HeaderText = "عدد الوثيقة";
                dgvDocSummary.Columns["Date"].HeaderText = "تاريخ";
                dgvDocSummary.Columns["DocumentStatus"].HeaderText = "الحالة ثيقة";
                dgvDocSummary.Columns["Custodian"].HeaderText = "ولي";
                dgvDocSummary.Columns["BinName"].HeaderText = "اسم بن ل";
                dgvDocSummary.Columns["IssueReason"].HeaderText = "سبب المشكلة";
                dgvDocSummary.Columns["ExpectedDate"].HeaderText = "التاريخ المتوقع";
                dgvDocSummary.Columns["Remarks"].HeaderText = "تصريحات";

            }
            // Grid image settings
            if (dgvDocSummary.RowCount > 0)
            {
                for (int i = 0; i < dgvDocSummary.RowCount; i++)
                {
                    if (Convert.ToInt32(dgvDocSummary.Rows[i].Cells["Status"].Value) == 0)
                    {
                        dgvDocSummary.Rows[i].Cells["Img"].Value = global::MyPayfriend.Properties.Resources.DocumentOut;//ImageList1.Images[0];// In
                        dgvDocSummary.Rows[i].Cells["Img"].ToolTipText = "Out";


                        if (i == 0)
                        {
                            if (MblnDeletePermissionIssue)
                                dgvDocSummary.Rows[i].Cells["clmdelete"].Value = global::MyPayfriend.Properties.Resources.Delete;
                            else
                                dgvDocSummary.Rows[i].Cells["clmdelete"].Value = global::MyPayfriend.Properties.Resources.Delete_Inactive;
                        }
                    }
                    else
                    {
                        dgvDocSummary.Rows[i].Cells["Img"].Value = global::MyPayfriend.Properties.Resources.DocumentIn;
                        dgvDocSummary.Rows[i].Cells["Img"].ToolTipText = "In";


                        if (i == 0)
                        {
                            if (MblnDeletePermissionReceipt)
                                dgvDocSummary.Rows[i].Cells["clmdelete"].Value = global::MyPayfriend.Properties.Resources.Delete;
                            else
                                dgvDocSummary.Rows[i].Cells["clmdelete"].Value = global::MyPayfriend.Properties.Resources.Delete_Inactive;
                        }
                    }

                    if (i > 0)
                        dgvDocSummary.Rows[i].Cells["clmdelete"].Value = global::MyPayfriend.Properties.Resources.Delete_Inactive;

                }
            }
            clmdelete.DisplayIndex = 14;
            //  EnableDisablePrint();
        }

    }
    /// <summary>
    /// disable sorting for detail grid
    /// </summary>
    private void DisableSorting()
    {
        dgvDocSummary.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[10].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[11].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[12].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[13].SortMode = DataGridViewColumnSortMode.NotSortable;
        dgvDocSummary.Columns[14].SortMode = DataGridViewColumnSortMode.NotSortable;
    }

    /// <summary>
    /// Fill combos
    /// Fill operationtype and documenttype
    /// </summary>
    private void LoadCombo()
    {
        try
        {
            DataTable datCombos = null;
            cboOperationType.DisplayMember = "Description";
            cboOperationType.ValueMember = "TypeID";
            cboOperationType.DataSource = datCombos;

            cboDocumentType.DisplayMember = "Description";
            cboDocumentType.ValueMember = "DocumentTypeID";
            cboDocumentType.DataSource = datCombos;

            cboPageCount.Items.Add("25");
            cboPageCount.Items.Add("50");
            cboPageCount.Items.Add("100");
            cboPageCount.ControlText = "25";

            PageSize = 25; ;

            GetAllOperationType();
            GetAllDocumentTypes();
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on LoadCombos() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }
    #endregion Functions

    #region Enum
    enum NavButton
    {
        First = 1,
        Next = 2,
        Previous = 3,
        Last = 4
    };
    enum ColumnType
    {
        clmType = 2,
        clmReference,
        clmDocumentType,
        clmDocumentNumber = 5,
        clmDate,
        clmCustodian,
        clmDocumentStatus,
        clmBinDetails,
        clmIssueReason,
        clmReturnDate,
        clmRemarks,
        clmDocumentID = 13,
        clmStatusId = 14,
        clmRecordID = 15


    }
    #endregion Enum

    private void dgvDocGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }





}

