﻿namespace MyPayfriend
{
    partial class frmAlertreport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.cboDocType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDocType = new DevComponents.DotNetBar.LabelX();
            this.btnShow = new System.Windows.Forms.Button();
            this.cboAlertReport = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.lblEmployee = new DevComponents.DotNetBar.LabelX();
            this.RptView = new Microsoft.Reporting.WinForms.ReportViewer();
            this.expandablePanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.dtpFromDate);
            this.expandablePanel1.Controls.Add(this.lblFromDate);
            this.expandablePanel1.Controls.Add(this.dtpToDate);
            this.expandablePanel1.Controls.Add(this.lblToDate);
            this.expandablePanel1.Controls.Add(this.cboDocType);
            this.expandablePanel1.Controls.Add(this.lblDocType);
            this.expandablePanel1.Controls.Add(this.btnShow);
            this.expandablePanel1.Controls.Add(this.cboAlertReport);
            this.expandablePanel1.Controls.Add(this.lblEmployee);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(1020, 88);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 7;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(565, 30);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(104, 20);
            this.dtpFromDate.TabIndex = 41;
            // 
            // lblFromDate
            // 
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(497, 28);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(62, 23);
            this.lblFromDate.TabIndex = 39;
            this.lblFromDate.Text = "From Date";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(565, 56);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(104, 20);
            this.dtpToDate.TabIndex = 42;
            // 
            // lblToDate
            // 
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(497, 55);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(45, 23);
            this.lblToDate.TabIndex = 40;
            this.lblToDate.Text = "To Date";
            // 
            // cboDocType
            // 
            this.cboDocType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDocType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDocType.DisplayMember = "Text";
            this.cboDocType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDocType.DropDownHeight = 134;
            this.cboDocType.FormattingEnabled = true;
            this.cboDocType.IntegralHeight = false;
            this.cboDocType.ItemHeight = 14;
            this.cboDocType.Location = new System.Drawing.Point(320, 36);
            this.cboDocType.Name = "cboDocType";
            this.cboDocType.Size = new System.Drawing.Size(149, 20);
            this.cboDocType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDocType.TabIndex = 23;
            // 
            // lblDocType
            // 
            this.lblDocType.AutoSize = true;
            // 
            // 
            // 
            this.lblDocType.BackgroundStyle.Class = "";
            this.lblDocType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDocType.Location = new System.Drawing.Point(235, 39);
            this.lblDocType.Name = "lblDocType";
            this.lblDocType.Size = new System.Drawing.Size(80, 15);
            this.lblDocType.TabIndex = 22;
            this.lblDocType.Text = "Document Type";
            // 
            // btnShow
            // 
            this.btnShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnShow.Image = global::MyPayfriend.Properties.Resources.View;
            this.btnShow.Location = new System.Drawing.Point(733, 39);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(21, 20);
            this.btnShow.TabIndex = 17;
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cboAlertReport
            // 
            this.cboAlertReport.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAlertReport.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAlertReport.DisplayMember = "Text";
            this.cboAlertReport.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboAlertReport.DropDownHeight = 134;
            this.cboAlertReport.FormattingEnabled = true;
            this.cboAlertReport.IntegralHeight = false;
            this.cboAlertReport.ItemHeight = 14;
            this.cboAlertReport.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.cboAlertReport.Location = new System.Drawing.Point(77, 36);
            this.cboAlertReport.Name = "cboAlertReport";
            this.cboAlertReport.Size = new System.Drawing.Size(149, 20);
            this.cboAlertReport.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboAlertReport.TabIndex = 4;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "Expiry Alert";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Return Alert";
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployee.BackgroundStyle.Class = "";
            this.lblEmployee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployee.Location = new System.Drawing.Point(12, 39);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(58, 15);
            this.lblEmployee.TabIndex = 2;
            this.lblEmployee.Text = "AlertReport";
            // 
            // RptView
            // 
            this.RptView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RptView.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource2.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource2.Value = null;
            this.RptView.LocalReport.DataSources.Add(reportDataSource2);
            this.RptView.LocalReport.ReportEmbeddedResource = "MindSoftERP.bin.Debug.MainReports.RptCompanyHeader.rdlc";
            this.RptView.Location = new System.Drawing.Point(0, 88);
            this.RptView.Name = "RptView";
            this.RptView.Size = new System.Drawing.Size(1020, 413);
            this.RptView.TabIndex = 8;
            // 
            // frmAlertreport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 501);
            this.Controls.Add(this.RptView);
            this.Controls.Add(this.expandablePanel1);
            this.Name = "frmAlertreport";
            this.Text = "frmAlertreport";
            this.Load += new System.EventHandler(this.frmAlertreport_Load);
            this.expandablePanel1.ResumeLayout(false);
            this.expandablePanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDocType;
        private DevComponents.DotNetBar.LabelX lblDocType;
        internal System.Windows.Forms.Button btnShow;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboAlertReport;
        private DevComponents.DotNetBar.LabelX lblEmployee;
        private Microsoft.Reporting.WinForms.ReportViewer RptView;
        internal System.Windows.Forms.DateTimePicker dtpFromDate;
        internal DevComponents.DotNetBar.LabelX lblFromDate;
        internal System.Windows.Forms.DateTimePicker dtpToDate;
        internal DevComponents.DotNetBar.LabelX lblToDate;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
    }
}