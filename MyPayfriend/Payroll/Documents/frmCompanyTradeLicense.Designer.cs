﻿namespace MyPayfriend
{
    partial class frmCompanyTradeLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label Label5;
            System.Windows.Forms.Label Label4;
            System.Windows.Forms.Label Label13;
            System.Windows.Forms.Label Label3;
            System.Windows.Forms.Label lblPolicyNumber;
            System.Windows.Forms.Label lblIssueDate;
            System.Windows.Forms.Label lblInsuranceCompany;
            System.Windows.Forms.Label lblExpiryDate;
            System.Windows.Forms.Label lblEmployee;
            System.Windows.Forms.Label label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCompanyTradeLicense));
            this.EmployeeInsuranceCardBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnCancel = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.documentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.receiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.RenewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.grpInsuranceCard = new System.Windows.Forms.GroupBox();
            this.txtPartner = new System.Windows.Forms.TextBox();
            this.txtFee = new System.Windows.Forms.TextBox();
            this.lblRenewed = new System.Windows.Forms.Label();
            this.btnProvince = new System.Windows.Forms.Button();
            this.cboProvince = new System.Windows.Forms.ComboBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtLicenseNumber = new System.Windows.Forms.TextBox();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.txtSponsor = new System.Windows.Forms.TextBox();
            this.dtpIssueDate = new System.Windows.Forms.DateTimePicker();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.stsStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.errTradelicense = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmTradeLicense = new System.Windows.Forms.Timer(this.components);
            Label1 = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            Label13 = new System.Windows.Forms.Label();
            Label3 = new System.Windows.Forms.Label();
            lblPolicyNumber = new System.Windows.Forms.Label();
            lblIssueDate = new System.Windows.Forms.Label();
            lblInsuranceCompany = new System.Windows.Forms.Label();
            lblExpiryDate = new System.Windows.Forms.Label();
            lblEmployee = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeInsuranceCardBindingNavigator)).BeginInit();
            this.EmployeeInsuranceCardBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.grpInsuranceCard.SuspendLayout();
            this.stsStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errTradelicense)).BeginInit();
            this.SuspendLayout();
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label1.Location = new System.Drawing.Point(13, 164);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(49, 13);
            Label1.TabIndex = 113;
            Label1.Text = "Province";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label5.Location = new System.Drawing.Point(13, 138);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(24, 13);
            Label5.TabIndex = 112;
            Label5.Text = "City";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Location = new System.Drawing.Point(13, 59);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(46, 13);
            Label4.TabIndex = 109;
            Label4.Text = "Sponsor";
            // 
            // Label13
            // 
            Label13.AutoSize = true;
            Label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label13.Location = new System.Drawing.Point(8, 262);
            Label13.Name = "Label13";
            Label13.Size = new System.Drawing.Size(58, 13);
            Label13.TabIndex = 18;
            Label13.Text = "Remarks";
            // 
            // Label3
            // 
            Label3.AutoSize = true;
            Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label3.Location = new System.Drawing.Point(8, 3);
            Label3.Name = "Label3";
            Label3.Size = new System.Drawing.Size(91, 13);
            Label3.TabIndex = 65;
            Label3.Text = "License Details";
            // 
            // lblPolicyNumber
            // 
            lblPolicyNumber.AutoSize = true;
            lblPolicyNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblPolicyNumber.Location = new System.Drawing.Point(13, 112);
            lblPolicyNumber.Name = "lblPolicyNumber";
            lblPolicyNumber.Size = new System.Drawing.Size(84, 13);
            lblPolicyNumber.TabIndex = 106;
            lblPolicyNumber.Text = "License Number";
            // 
            // lblIssueDate
            // 
            lblIssueDate.AutoSize = true;
            lblIssueDate.Location = new System.Drawing.Point(13, 218);
            lblIssueDate.Name = "lblIssueDate";
            lblIssueDate.Size = new System.Drawing.Size(58, 13);
            lblIssueDate.TabIndex = 84;
            lblIssueDate.Text = "Issue Date";
            // 
            // lblInsuranceCompany
            // 
            lblInsuranceCompany.AutoSize = true;
            lblInsuranceCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblInsuranceCompany.Location = new System.Drawing.Point(13, 85);
            lblInsuranceCompany.Name = "lblInsuranceCompany";
            lblInsuranceCompany.Size = new System.Drawing.Size(67, 13);
            lblInsuranceCompany.TabIndex = 107;
            lblInsuranceCompany.Text = "Sponsor Fee";
            // 
            // lblExpiryDate
            // 
            lblExpiryDate.AutoSize = true;
            lblExpiryDate.Location = new System.Drawing.Point(270, 218);
            lblExpiryDate.Name = "lblExpiryDate";
            lblExpiryDate.Size = new System.Drawing.Size(61, 13);
            lblExpiryDate.TabIndex = 12;
            lblExpiryDate.Text = "Expiry Date";
            // 
            // lblEmployee
            // 
            lblEmployee.AutoSize = true;
            lblEmployee.Location = new System.Drawing.Point(13, 32);
            lblEmployee.Name = "lblEmployee";
            lblEmployee.Size = new System.Drawing.Size(51, 13);
            lblEmployee.TabIndex = 70;
            lblEmployee.Text = "Company";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(13, 191);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(41, 13);
            label2.TabIndex = 117;
            label2.Text = "Partner";
            // 
            // EmployeeInsuranceCardBindingNavigator
            // 
            this.EmployeeInsuranceCardBindingNavigator.AddNewItem = null;
            this.EmployeeInsuranceCardBindingNavigator.CountItem = this.bnCountItem;
            this.EmployeeInsuranceCardBindingNavigator.DeleteItem = null;
            this.EmployeeInsuranceCardBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.BindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.ToolStripSeparator1,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnCancel,
            this.BindingNavigatorSeparator2,
            this.bnMoreActions,
            this.ToolStripSeparator3,
            this.bnPrint,
            this.bnEmail,
            this.ToolStripSeparator2,
            this.bnHelp});
            this.EmployeeInsuranceCardBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeInsuranceCardBindingNavigator.MoveFirstItem = null;
            this.EmployeeInsuranceCardBindingNavigator.MoveLastItem = null;
            this.EmployeeInsuranceCardBindingNavigator.MoveNextItem = null;
            this.EmployeeInsuranceCardBindingNavigator.MovePreviousItem = null;
            this.EmployeeInsuranceCardBindingNavigator.Name = "EmployeeInsuranceCardBindingNavigator";
            this.EmployeeInsuranceCardBindingNavigator.PositionItem = this.bnPositionItem;
            this.EmployeeInsuranceCardBindingNavigator.Size = new System.Drawing.Size(469, 25);
            this.EmployeeInsuranceCardBindingNavigator.TabIndex = 71;
            this.EmployeeInsuranceCardBindingNavigator.Text = "BindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.bnMoveFirstItem_Click);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.bnMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.bnMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.bnMoveLastItem_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add New";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Remove";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // bnCancel
            // 
            this.bnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnCancel.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.bnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(23, 22);
            this.bnCancel.Text = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.bnCancel_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentsToolStripMenuItem,
            this.toolStripSeparator6,
            this.receiptToolStripMenuItem,
            this.issueToolStripMenuItem1,
            this.toolStripSeparator7,
            this.RenewToolStripMenuItem});
            this.bnMoreActions.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "&Actions";
            // 
            // documentsToolStripMenuItem
            // 
            this.documentsToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.documentsToolStripMenuItem.Name = "documentsToolStripMenuItem";
            this.documentsToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.documentsToolStripMenuItem.Text = "&Documents";
            this.documentsToolStripMenuItem.Click += new System.EventHandler(this.documentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(132, 6);
            // 
            // receiptToolStripMenuItem
            // 
            this.receiptToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.receiptToolStripMenuItem.Name = "receiptToolStripMenuItem";
            this.receiptToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.receiptToolStripMenuItem.Text = "R&eceipt";
            this.receiptToolStripMenuItem.Click += new System.EventHandler(this.receiptToolStripMenuItem_Click);
            // 
            // issueToolStripMenuItem1
            // 
            this.issueToolStripMenuItem1.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.issueToolStripMenuItem1.Name = "issueToolStripMenuItem1";
            this.issueToolStripMenuItem1.Size = new System.Drawing.Size(135, 22);
            this.issueToolStripMenuItem1.Text = "&Issue";
            this.issueToolStripMenuItem1.Click += new System.EventHandler(this.issueToolStripMenuItem1_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(132, 6);
            // 
            // RenewToolStripMenuItem
            // 
            this.RenewToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.renew;
            this.RenewToolStripMenuItem.Name = "RenewToolStripMenuItem";
            this.RenewToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.RenewToolStripMenuItem.Text = "Re&new";
            this.RenewToolStripMenuItem.Click += new System.EventHandler(this.RenewToolStripMenuItem_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPrint
            // 
            this.bnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnHelp
            // 
            this.bnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.Text = "He&lp";
            this.bnHelp.Click += new System.EventHandler(this.bnHelp_Click);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(469, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 89;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by CompanyName | Shortname | License Number\r\n\r\n";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Tooltip = "Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // grpInsuranceCard
            // 
            this.grpInsuranceCard.Controls.Add(this.txtPartner);
            this.grpInsuranceCard.Controls.Add(label2);
            this.grpInsuranceCard.Controls.Add(this.txtFee);
            this.grpInsuranceCard.Controls.Add(this.lblRenewed);
            this.grpInsuranceCard.Controls.Add(this.btnProvince);
            this.grpInsuranceCard.Controls.Add(this.cboProvince);
            this.grpInsuranceCard.Controls.Add(Label1);
            this.grpInsuranceCard.Controls.Add(this.txtCity);
            this.grpInsuranceCard.Controls.Add(Label5);
            this.grpInsuranceCard.Controls.Add(Label4);
            this.grpInsuranceCard.Controls.Add(this.txtLicenseNumber);
            this.grpInsuranceCard.Controls.Add(Label13);
            this.grpInsuranceCard.Controls.Add(this.dtpExpiryDate);
            this.grpInsuranceCard.Controls.Add(Label3);
            this.grpInsuranceCard.Controls.Add(lblPolicyNumber);
            this.grpInsuranceCard.Controls.Add(lblIssueDate);
            this.grpInsuranceCard.Controls.Add(this.txtSponsor);
            this.grpInsuranceCard.Controls.Add(this.dtpIssueDate);
            this.grpInsuranceCard.Controls.Add(lblInsuranceCompany);
            this.grpInsuranceCard.Controls.Add(lblExpiryDate);
            this.grpInsuranceCard.Controls.Add(this.cboCompany);
            this.grpInsuranceCard.Controls.Add(this.txtRemarks);
            this.grpInsuranceCard.Controls.Add(lblEmployee);
            this.grpInsuranceCard.Controls.Add(this.ShapeContainer2);
            this.grpInsuranceCard.Location = new System.Drawing.Point(7, 58);
            this.grpInsuranceCard.Name = "grpInsuranceCard";
            this.grpInsuranceCard.Size = new System.Drawing.Size(454, 359);
            this.grpInsuranceCard.TabIndex = 90;
            this.grpInsuranceCard.TabStop = false;
            // 
            // txtPartner
            // 
            this.txtPartner.BackColor = System.Drawing.SystemColors.Info;
            this.txtPartner.Location = new System.Drawing.Point(133, 187);
            this.txtPartner.MaxLength = 50;
            this.txtPartner.Name = "txtPartner";
            this.txtPartner.Size = new System.Drawing.Size(270, 20);
            this.txtPartner.TabIndex = 7;
            this.txtPartner.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtFee
            // 
            this.txtFee.BackColor = System.Drawing.SystemColors.Info;
            this.txtFee.Location = new System.Drawing.Point(133, 82);
            this.txtFee.MaxLength = 10;
            this.txtFee.Name = "txtFee";
            this.txtFee.Size = new System.Drawing.Size(114, 20);
            this.txtFee.TabIndex = 2;
            this.txtFee.TextChanged += new System.EventHandler(this.OnInputChanged);
            this.txtFee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFee_KeyPress);
            // 
            // lblRenewed
            // 
            this.lblRenewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenewed.ForeColor = System.Drawing.Color.Maroon;
            this.lblRenewed.Location = new System.Drawing.Point(365, 244);
            this.lblRenewed.Name = "lblRenewed";
            this.lblRenewed.Size = new System.Drawing.Size(71, 22);
            this.lblRenewed.TabIndex = 114;
            this.lblRenewed.Text = "Renewed";
            this.lblRenewed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRenewed.Visible = false;
            // 
            // btnProvince
            // 
            this.btnProvince.Location = new System.Drawing.Point(409, 160);
            this.btnProvince.Name = "btnProvince";
            this.btnProvince.Size = new System.Drawing.Size(32, 23);
            this.btnProvince.TabIndex = 6;
            this.btnProvince.Text = "....";
            this.btnProvince.UseVisualStyleBackColor = true;
            this.btnProvince.TextChanged += new System.EventHandler(this.OnInputChanged);
            this.btnProvince.Click += new System.EventHandler(this.btnCardIssued_Click);
            // 
            // cboProvince
            // 
            this.cboProvince.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboProvince.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboProvince.BackColor = System.Drawing.SystemColors.Window;
            this.cboProvince.DropDownHeight = 134;
            this.cboProvince.FormattingEnabled = true;
            this.cboProvince.IntegralHeight = false;
            this.cboProvince.Location = new System.Drawing.Point(133, 160);
            this.cboProvince.Name = "cboProvince";
            this.cboProvince.Size = new System.Drawing.Size(270, 21);
            this.cboProvince.TabIndex = 5;
            this.cboProvince.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboProvince_KeyPress);
            this.cboProvince.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Info;
            this.txtCity.Location = new System.Drawing.Point(133, 134);
            this.txtCity.MaxLength = 50;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(270, 20);
            this.txtCity.TabIndex = 4;
            this.txtCity.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtLicenseNumber
            // 
            this.txtLicenseNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtLicenseNumber.Location = new System.Drawing.Point(133, 108);
            this.txtLicenseNumber.MaxLength = 20;
            this.txtLicenseNumber.Name = "txtLicenseNumber";
            this.txtLicenseNumber.Size = new System.Drawing.Size(270, 20);
            this.txtLicenseNumber.TabIndex = 3;
            this.txtLicenseNumber.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(334, 214);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.Size = new System.Drawing.Size(102, 20);
            this.dtpExpiryDate.TabIndex = 9;
            this.dtpExpiryDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtSponsor
            // 
            this.txtSponsor.BackColor = System.Drawing.SystemColors.Info;
            this.txtSponsor.Location = new System.Drawing.Point(133, 55);
            this.txtSponsor.MaxLength = 50;
            this.txtSponsor.Name = "txtSponsor";
            this.txtSponsor.Size = new System.Drawing.Size(270, 20);
            this.txtSponsor.TabIndex = 1;
            this.txtSponsor.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpIssueDate
            // 
            this.dtpIssueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssueDate.Location = new System.Drawing.Point(133, 214);
            this.dtpIssueDate.Name = "dtpIssueDate";
            this.dtpIssueDate.Size = new System.Drawing.Size(102, 20);
            this.dtpIssueDate.TabIndex = 8;
            this.dtpIssueDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DisplayMember = "EmployeeID";
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.Location = new System.Drawing.Point(133, 28);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(306, 21);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.ValueMember = "EmployeeID";
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(18, 284);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(421, 67);
            this.txtRemarks.TabIndex = 11;
            this.txtRemarks.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // ShapeContainer2
            // 
            this.ShapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer2.Name = "ShapeContainer2";
            this.ShapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape2});
            this.ShapeContainer2.Size = new System.Drawing.Size(448, 340);
            this.ShapeContainer2.TabIndex = 110;
            this.ShapeContainer2.TabStop = false;
            // 
            // LineShape2
            // 
            this.LineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape2.Name = "LineShape3";
            this.LineShape2.X1 = 55;
            this.LineShape2.X2 = 434;
            this.LineShape2.Y1 = 253;
            this.LineShape2.Y2 = 253;
            // 
            // stsStatus
            // 
            this.stsStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.stsStatus.Location = new System.Drawing.Point(0, 455);
            this.stsStatus.Name = "stsStatus";
            this.stsStatus.Size = new System.Drawing.Size(469, 22);
            this.stsStatus.TabIndex = 91;
            this.stsStatus.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(8, 423);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 92;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(386, 423);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 94;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOk.Location = new System.Drawing.Point(301, 423);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 93;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // errTradelicense
            // 
            this.errTradelicense.ContainerControl = this;
            this.errTradelicense.RightToLeft = true;
            // 
            // tmTradeLicense
            // 
            this.tmTradeLicense.Interval = 2000;
            this.tmTradeLicense.Tick += new System.EventHandler(this.tmTradeLicense_Tick);
            // 
            // frmCompanyTradeLicense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 477);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.stsStatus);
            this.Controls.Add(this.grpInsuranceCard);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.EmployeeInsuranceCardBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCompanyTradeLicense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TradeLicense";
            this.Load += new System.EventHandler(this.frmCompanyTradeLicense_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCompanyTradeLicense_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCompanyTradeLicense_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeInsuranceCardBindingNavigator)).EndInit();
            this.EmployeeInsuranceCardBindingNavigator.ResumeLayout(false);
            this.EmployeeInsuranceCardBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.grpInsuranceCard.ResumeLayout(false);
            this.grpInsuranceCard.PerformLayout();
            this.stsStatus.ResumeLayout(false);
            this.stsStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errTradelicense)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator EmployeeInsuranceCardBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel bnCountItem;
        internal System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox bnPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton bnMoveNextItem;
        internal System.Windows.Forms.ToolStripButton bnMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        internal System.Windows.Forms.ToolStripButton bnCancel;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        private System.Windows.Forms.ToolStripMenuItem documentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem receiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem issueToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem RenewToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton bnPrint;
        internal System.Windows.Forms.ToolStripButton bnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.GroupBox grpInsuranceCard;
        private System.Windows.Forms.Label lblRenewed;
        internal System.Windows.Forms.Button btnProvince;
        internal System.Windows.Forms.ComboBox cboProvince;
        internal System.Windows.Forms.TextBox txtCity;
        internal System.Windows.Forms.TextBox txtLicenseNumber;
        internal System.Windows.Forms.DateTimePicker dtpExpiryDate;
        internal System.Windows.Forms.TextBox txtSponsor;
        internal System.Windows.Forms.DateTimePicker dtpIssueDate;
        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape2;
        internal System.Windows.Forms.StatusStrip stsStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.TextBox txtPartner;
        internal System.Windows.Forms.TextBox txtFee;
        internal System.Windows.Forms.ErrorProvider errTradelicense;
        internal System.Windows.Forms.Timer tmTradeLicense;
    }
}