﻿
partial class frmPassport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label15;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label label18;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPassport));
            this.btnCancel = new System.Windows.Forms.Button();
            this.ContextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuResidencePermit = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuOldPassports = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuEntryRestrict = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.BindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripDropDownBtnMore = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuItemAttachDocuments = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.receiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.renewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBoxPassport = new System.Windows.Forms.GroupBox();
            this.lblOtherInfo = new System.Windows.Forms.Label();
            this.txtResidencePermits = new System.Windows.Forms.TextBox();
            this.txtOldPassportNumbers = new System.Windows.Forms.TextBox();
            this.txtEntryRestrictions = new System.Windows.Forms.TextBox();
            this.btnContext = new System.Windows.Forms.Button();
            this.btnRemarks = new System.Windows.Forms.Button();
            this.lblRenewed = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.dtpIssueDate = new System.Windows.Forms.DateTimePicker();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.txtPlaceOfBirth = new System.Windows.Forms.TextBox();
            this.btnCountry = new System.Windows.Forms.Button();
            this.cboCountry = new System.Windows.Forms.ComboBox();
            this.txtPlaceOfIssue = new System.Windows.Forms.TextBox();
            this.chkPagesAvailable = new System.Windows.Forms.CheckBox();
            this.txtPassportNumber = new System.Windows.Forms.TextBox();
            this.txtNameinPassport = new System.Windows.Forms.TextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            label5 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label15 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label18 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.ContextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator)).BeginInit();
            this.BindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.StatusStrip1.SuspendLayout();
            this.groupBoxPassport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(15, 189);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(83, 13);
            label5.TabIndex = 123;
            label5.Text = "Pages Available";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(248, 219);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(61, 13);
            label11.TabIndex = 119;
            label11.Text = "Expiry Date";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(16, 219);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(58, 13);
            label13.TabIndex = 124;
            label13.Text = "Issue Date";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Location = new System.Drawing.Point(15, 164);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(72, 13);
            label15.TabIndex = 122;
            label15.Text = "Place Of Birth";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(15, 84);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(90, 13);
            label14.TabIndex = 118;
            label14.Text = "Name in Passport";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(15, 57);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(88, 13);
            label16.TabIndex = 117;
            label16.Text = "Passport Number";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(16, 136);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(46, 13);
            label17.TabIndex = 121;
            label17.Text = "Country ";
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Location = new System.Drawing.Point(15, 110);
            label18.Name = "label18";
            label18.Size = new System.Drawing.Size(74, 13);
            label18.TabIndex = 120;
            label18.Text = "Place of Issue";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(15, 29);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(84, 13);
            label12.TabIndex = 101;
            label12.Text = "Employee Name";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(7, 246);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(63, 13);
            label2.TabIndex = 142;
            label2.Text = "Other info";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(406, 411);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(62, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ContextMenuStrip1
            // 
            this.ContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuResidencePermit,
            this.ToolStripMenuOldPassports,
            this.ToolStripMenuEntryRestrict});
            this.ContextMenuStrip1.Name = "ContextMenuStrip1";
            this.ContextMenuStrip1.Size = new System.Drawing.Size(166, 70);
            // 
            // ToolStripMenuResidencePermit
            // 
            this.ToolStripMenuResidencePermit.Name = "ToolStripMenuResidencePermit";
            this.ToolStripMenuResidencePermit.Size = new System.Drawing.Size(165, 22);
            this.ToolStripMenuResidencePermit.Text = "Residence Permit";
            this.ToolStripMenuResidencePermit.Click += new System.EventHandler(this.ToolStripMenuResidencePermit_Click);
            // 
            // ToolStripMenuOldPassports
            // 
            this.ToolStripMenuOldPassports.Name = "ToolStripMenuOldPassports";
            this.ToolStripMenuOldPassports.Size = new System.Drawing.Size(165, 22);
            this.ToolStripMenuOldPassports.Text = "Old Passports";
            this.ToolStripMenuOldPassports.Click += new System.EventHandler(this.ToolStripMenuOldPassports_Click);
            // 
            // ToolStripMenuEntryRestrict
            // 
            this.ToolStripMenuEntryRestrict.Name = "ToolStripMenuEntryRestrict";
            this.ToolStripMenuEntryRestrict.Size = new System.Drawing.Size(165, 22);
            this.ToolStripMenuEntryRestrict.Text = "Entry Restrictions";
            this.ToolStripMenuEntryRestrict.Click += new System.EventHandler(this.ToolStripMenuEntryRestrict_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(329, 411);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(70, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(10, 411);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // BindingNavigator
            // 
            this.BindingNavigator.AddNewItem = null;
            this.BindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.BindingNavigator.CountItem = this.bnCountItem;
            this.BindingNavigator.DeleteItem = null;
            this.BindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.BindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.ToolStripSeparator1,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.btnClear,
            this.BindingNavigatorSeparator2,
            this.ToolStripDropDownBtnMore,
            this.ToolStripSeparator3,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.BindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.BindingNavigator.MoveFirstItem = null;
            this.BindingNavigator.MoveLastItem = null;
            this.BindingNavigator.MoveNextItem = null;
            this.BindingNavigator.MovePreviousItem = null;
            this.BindingNavigator.Name = "BindingNavigator";
            this.BindingNavigator.PositionItem = this.bnPositionItem;
            this.BindingNavigator.Size = new System.Drawing.Size(480, 25);
            this.BindingNavigator.TabIndex = 68;
            this.BindingNavigator.Text = "BindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripDropDownBtnMore
            // 
            this.ToolStripDropDownBtnMore.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAttachDocuments,
            this.toolStripSeparator4,
            this.receiptToolStripMenuItem,
            this.issueToolStripMenuItem,
            this.toolStripSeparator5,
            this.renewToolStripMenuItem});
            this.ToolStripDropDownBtnMore.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.ToolStripDropDownBtnMore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripDropDownBtnMore.Name = "ToolStripDropDownBtnMore";
            this.ToolStripDropDownBtnMore.Size = new System.Drawing.Size(76, 22);
            this.ToolStripDropDownBtnMore.Text = "Actions";
            this.ToolStripDropDownBtnMore.ToolTipText = "Actions";
            // 
            // mnuItemAttachDocuments
            // 
            this.mnuItemAttachDocuments.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.mnuItemAttachDocuments.Name = "mnuItemAttachDocuments";
            this.mnuItemAttachDocuments.Size = new System.Drawing.Size(135, 22);
            this.mnuItemAttachDocuments.Text = "&Documents";
            this.mnuItemAttachDocuments.Click += new System.EventHandler(this.mnuItemAttachDocuments_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(132, 6);
            // 
            // receiptToolStripMenuItem
            // 
            this.receiptToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.receiptToolStripMenuItem.Name = "receiptToolStripMenuItem";
            this.receiptToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.receiptToolStripMenuItem.Text = "R&eceipt";
            this.receiptToolStripMenuItem.Click += new System.EventHandler(this.receiptIssueToolStripMenuItem_Click);
            // 
            // issueToolStripMenuItem
            // 
            this.issueToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.issueToolStripMenuItem.Name = "issueToolStripMenuItem";
            this.issueToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.issueToolStripMenuItem.Text = "&Issue";
            this.issueToolStripMenuItem.Click += new System.EventHandler(this.receiptIssueToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(132, 6);
            // 
            // renewToolStripMenuItem
            // 
            this.renewToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.renew;
            this.renewToolStripMenuItem.Name = "renewToolStripMenuItem";
            this.renewToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.renewToolStripMenuItem.Text = "Re&new";
            this.renewToolStripMenuItem.Click += new System.EventHandler(this.renewToolStripMenuItem_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Fax";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            this.ToolStripSeparator2.Visible = false;
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // Timer
            // 
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 60;
            this.label8.Text = "Passport Info";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.RightToLeft = true;
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 441);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(480, 22);
            this.StatusStrip1.TabIndex = 147;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBoxPassport
            // 
            this.groupBoxPassport.Controls.Add(this.lblOtherInfo);
            this.groupBoxPassport.Controls.Add(this.txtResidencePermits);
            this.groupBoxPassport.Controls.Add(this.txtOldPassportNumbers);
            this.groupBoxPassport.Controls.Add(this.txtEntryRestrictions);
            this.groupBoxPassport.Controls.Add(this.btnContext);
            this.groupBoxPassport.Controls.Add(this.btnRemarks);
            this.groupBoxPassport.Controls.Add(this.lblRenewed);
            this.groupBoxPassport.Controls.Add(label2);
            this.groupBoxPassport.Controls.Add(this.label8);
            this.groupBoxPassport.Controls.Add(label12);
            this.groupBoxPassport.Controls.Add(this.cboEmployee);
            this.groupBoxPassport.Controls.Add(label13);
            this.groupBoxPassport.Controls.Add(this.dtpIssueDate);
            this.groupBoxPassport.Controls.Add(label11);
            this.groupBoxPassport.Controls.Add(this.dtpExpiryDate);
            this.groupBoxPassport.Controls.Add(this.txtPlaceOfBirth);
            this.groupBoxPassport.Controls.Add(label15);
            this.groupBoxPassport.Controls.Add(this.btnCountry);
            this.groupBoxPassport.Controls.Add(label18);
            this.groupBoxPassport.Controls.Add(this.cboCountry);
            this.groupBoxPassport.Controls.Add(this.txtPlaceOfIssue);
            this.groupBoxPassport.Controls.Add(label17);
            this.groupBoxPassport.Controls.Add(label16);
            this.groupBoxPassport.Controls.Add(this.chkPagesAvailable);
            this.groupBoxPassport.Controls.Add(this.txtPassportNumber);
            this.groupBoxPassport.Controls.Add(label5);
            this.groupBoxPassport.Controls.Add(label14);
            this.groupBoxPassport.Controls.Add(this.txtNameinPassport);
            this.groupBoxPassport.Controls.Add(this.shapeContainer1);
            this.groupBoxPassport.Location = new System.Drawing.Point(10, 52);
            this.groupBoxPassport.Name = "groupBoxPassport";
            this.groupBoxPassport.Size = new System.Drawing.Size(461, 347);
            this.groupBoxPassport.TabIndex = 145;
            this.groupBoxPassport.TabStop = false;
            // 
            // lblOtherInfo
            // 
            this.lblOtherInfo.AutoSize = true;
            this.lblOtherInfo.Location = new System.Drawing.Point(16, 307);
            this.lblOtherInfo.Name = "lblOtherInfo";
            this.lblOtherInfo.Size = new System.Drawing.Size(95, 13);
            this.lblOtherInfo.TabIndex = 126;
            this.lblOtherInfo.Text = "Residence Permits";
            // 
            // txtResidencePermits
            // 
            this.txtResidencePermits.Location = new System.Drawing.Point(131, 269);
            this.txtResidencePermits.MaxLength = 200;
            this.txtResidencePermits.Multiline = true;
            this.txtResidencePermits.Name = "txtResidencePermits";
            this.txtResidencePermits.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResidencePermits.Size = new System.Drawing.Size(310, 64);
            this.txtResidencePermits.TabIndex = 113;
            // 
            // txtOldPassportNumbers
            // 
            this.txtOldPassportNumbers.AcceptsReturn = true;
            this.txtOldPassportNumbers.Location = new System.Drawing.Point(131, 269);
            this.txtOldPassportNumbers.MaxLength = 200;
            this.txtOldPassportNumbers.Multiline = true;
            this.txtOldPassportNumbers.Name = "txtOldPassportNumbers";
            this.txtOldPassportNumbers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOldPassportNumbers.Size = new System.Drawing.Size(310, 66);
            this.txtOldPassportNumbers.TabIndex = 115;
            // 
            // txtEntryRestrictions
            // 
            this.txtEntryRestrictions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEntryRestrictions.Location = new System.Drawing.Point(131, 270);
            this.txtEntryRestrictions.MaxLength = 200;
            this.txtEntryRestrictions.Multiline = true;
            this.txtEntryRestrictions.Name = "txtEntryRestrictions";
            this.txtEntryRestrictions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtEntryRestrictions.Size = new System.Drawing.Size(310, 63);
            this.txtEntryRestrictions.TabIndex = 114;
            // 
            // btnContext
            // 
            this.btnContext.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnContext.ContextMenuStrip = this.ContextMenuStrip1;
            this.btnContext.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnContext.Location = new System.Drawing.Point(105, 268);
            this.btnContext.Name = "btnContext";
            this.btnContext.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.btnContext.Size = new System.Drawing.Size(20, 23);
            this.btnContext.TabIndex = 116;
            this.btnContext.Text = "6";
            this.btnContext.UseVisualStyleBackColor = true;
            this.btnContext.Click += new System.EventHandler(this.btnContext_Click_1);
            // 
            // btnRemarks
            // 
            this.btnRemarks.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnRemarks.Location = new System.Drawing.Point(19, 267);
            this.btnRemarks.Name = "btnRemarks";
            this.btnRemarks.Size = new System.Drawing.Size(80, 23);
            this.btnRemarks.TabIndex = 127;
            this.btnRemarks.TabStop = false;
            this.btnRemarks.Text = "Residence Permit";
            this.btnRemarks.UseVisualStyleBackColor = true;
            // 
            // lblRenewed
            // 
            this.lblRenewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenewed.ForeColor = System.Drawing.Color.Maroon;
            this.lblRenewed.Location = new System.Drawing.Point(335, 189);
            this.lblRenewed.Name = "lblRenewed";
            this.lblRenewed.Size = new System.Drawing.Size(71, 18);
            this.lblRenewed.TabIndex = 144;
            this.lblRenewed.Text = "Renewed";
            this.lblRenewed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRenewed.Visible = false;
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DisplayMember = "EmployeeID";
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(118, 26);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(323, 21);
            this.cboEmployee.TabIndex = 102;
            this.cboEmployee.ValueMember = "EmployeeID";
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployee_KeyDown);
            // 
            // dtpIssueDate
            // 
            this.dtpIssueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssueDate.Location = new System.Drawing.Point(118, 216);
            this.dtpIssueDate.Name = "dtpIssueDate";
            this.dtpIssueDate.Size = new System.Drawing.Size(103, 20);
            this.dtpIssueDate.TabIndex = 111;
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(315, 216);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.Size = new System.Drawing.Size(103, 20);
            this.dtpExpiryDate.TabIndex = 112;
            // 
            // txtPlaceOfBirth
            // 
            this.txtPlaceOfBirth.Location = new System.Drawing.Point(118, 163);
            this.txtPlaceOfBirth.MaxLength = 50;
            this.txtPlaceOfBirth.Name = "txtPlaceOfBirth";
            this.txtPlaceOfBirth.Size = new System.Drawing.Size(295, 20);
            this.txtPlaceOfBirth.TabIndex = 109;
            // 
            // btnCountry
            // 
            this.btnCountry.Location = new System.Drawing.Point(411, 133);
            this.btnCountry.Name = "btnCountry";
            this.btnCountry.Size = new System.Drawing.Size(30, 20);
            this.btnCountry.TabIndex = 128;
            this.btnCountry.Text = "...";
            this.btnCountry.UseVisualStyleBackColor = true;
            this.btnCountry.Click += new System.EventHandler(this.btnCountry_Click_1);
            // 
            // cboCountry
            // 
            this.cboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCountry.BackColor = System.Drawing.SystemColors.Info;
            this.cboCountry.DropDownHeight = 134;
            this.cboCountry.FormattingEnabled = true;
            this.cboCountry.IntegralHeight = false;
            this.cboCountry.Location = new System.Drawing.Point(118, 133);
            this.cboCountry.MaxDropDownItems = 10;
            this.cboCountry.Name = "cboCountry";
            this.cboCountry.Size = new System.Drawing.Size(287, 21);
            this.cboCountry.TabIndex = 107;
            this.cboCountry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCountry_KeyDown);
            // 
            // txtPlaceOfIssue
            // 
            this.txtPlaceOfIssue.BackColor = System.Drawing.SystemColors.Info;
            this.txtPlaceOfIssue.Location = new System.Drawing.Point(118, 103);
            this.txtPlaceOfIssue.MaxLength = 50;
            this.txtPlaceOfIssue.Name = "txtPlaceOfIssue";
            this.txtPlaceOfIssue.Size = new System.Drawing.Size(295, 20);
            this.txtPlaceOfIssue.TabIndex = 106;
            // 
            // chkPagesAvailable
            // 
            this.chkPagesAvailable.Location = new System.Drawing.Point(118, 189);
            this.chkPagesAvailable.Name = "chkPagesAvailable";
            this.chkPagesAvailable.Size = new System.Drawing.Size(53, 22);
            this.chkPagesAvailable.TabIndex = 110;
            this.chkPagesAvailable.TabStop = false;
            this.chkPagesAvailable.UseVisualStyleBackColor = true;
            // 
            // txtPassportNumber
            // 
            this.txtPassportNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtPassportNumber.Location = new System.Drawing.Point(118, 54);
            this.txtPassportNumber.MaxLength = 40;
            this.txtPassportNumber.Name = "txtPassportNumber";
            this.txtPassportNumber.Size = new System.Drawing.Size(246, 20);
            this.txtPassportNumber.TabIndex = 103;
            // 
            // txtNameinPassport
            // 
            this.txtNameinPassport.BackColor = System.Drawing.SystemColors.Info;
            this.txtNameinPassport.Location = new System.Drawing.Point(118, 77);
            this.txtNameinPassport.MaxLength = 100;
            this.txtNameinPassport.Multiline = true;
            this.txtNameinPassport.Name = "txtNameinPassport";
            this.txtNameinPassport.Size = new System.Drawing.Size(295, 20);
            this.txtNameinPassport.TabIndex = 105;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(455, 328);
            this.shapeContainer1.TabIndex = 145;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 70;
            this.lineShape1.X2 = 440;
            this.lineShape1.Y1 = 238;
            this.lineShape1.Y2 = 238;
            // 
            // panel1
            // 
            this.panel1.AllowDrop = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.ContextMenuStrip = this.ContextMenuStrip1;
            this.panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel1.Location = new System.Drawing.Point(12, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(415, 243);
            this.panel1.TabIndex = 129;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(480, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 148;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code | Card Number";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // frmPassport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 463);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.groupBoxPassport);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.BindingNavigator);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnSave);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPassport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Passport";
            this.Load += new System.EventHandler(this.frmPassport_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPassport_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPassport_KeyDown);
            this.ContextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigator)).EndInit();
            this.BindingNavigator.ResumeLayout(false);
            this.BindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.groupBoxPassport.ResumeLayout(false);
            this.groupBoxPassport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator BindingNavigator;
        internal System.Windows.Forms.ToolStripLabel bnCountItem;
        internal System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox bnPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton bnMoveNextItem;
        internal System.Windows.Forms.ToolStripButton bnMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuResidencePermit;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuOldPassports;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuEntryRestrict;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripDropDownButton ToolStripDropDownBtnMore;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAttachDocuments;
        private System.Windows.Forms.ToolStripMenuItem receiptToolStripMenuItem;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        private System.Windows.Forms.ToolStripMenuItem issueToolStripMenuItem;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.GroupBox groupBoxPassport;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.ToolStripMenuItem renewToolStripMenuItem;
        internal System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblRenewed;
        internal System.Windows.Forms.TextBox txtEntryRestrictions;
        private System.Windows.Forms.Button btnCountry;
        internal System.Windows.Forms.ComboBox cboCountry;
        internal System.Windows.Forms.TextBox txtPlaceOfIssue;
        internal System.Windows.Forms.TextBox txtPassportNumber;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.TextBox txtNameinPassport;
        internal System.Windows.Forms.TextBox txtPlaceOfBirth;
        internal System.Windows.Forms.Label lblOtherInfo;
        internal System.Windows.Forms.DateTimePicker dtpIssueDate;
        internal System.Windows.Forms.Button btnContext;
        internal System.Windows.Forms.Button btnRemarks;
        internal System.Windows.Forms.DateTimePicker dtpExpiryDate;
        internal System.Windows.Forms.TextBox txtOldPassportNumbers;
        internal System.Windows.Forms.CheckBox chkPagesAvailable;
        internal System.Windows.Forms.TextBox txtResidencePermits;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
       
    }
