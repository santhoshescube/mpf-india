﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MyPayfriend;
using System.Collections;

/* 
=================================================
Description  :	     <Document Transactions>
Created By   :		RAJESH.R
Created Date :      <6 JUNE 2013>
 
================================================

 Modified By : Megha
 Date        : 26/08/2013
 Description : Performed Code Optimization and added Functionalities for: Renew, Search, and Code Optimization

*/

public partial class frmDocumentMasterNew : Form
{
    #region Declarations

    /* Variable Declarations */

    public long EmployeeID { get; set; }//Reference ID From Employee View
    public int PintCompanyID { get; set; }//Company ID
    public DocumentType ePDocumentType { get; set; }//Document TypeID
    public DocumentType eDocumentType { get; set; }//Document TypeID
    public OperationType eOperationType { get; set; }//Operation TypeID
    public int DocumentID { get; set; }//Document ID
    public int DocumentIDNav { get; set; }//Document ID
    public long PintEmployeeID { get; set; }//EmployeeID ID

    private int StatusID { get; set; }
    private int TotalRecordCount { get; set; }
    private int CurrentIndex { get; set; }
    private int OrderNo { get; set; }

    // Error Message display
    private string sCommonMessage = String.Empty;
    private string MsMessageCaption = ClsCommonSettings.MessageCaption;
    private ArrayList MaMessageArr;
    private ArrayList MaStatusMessage;
    private MessageBoxIcon MmessageIcon;

    //To set Permissions
    private bool MblnAddPermission = false;
    private bool MblnAddUpdatePermission = false;
    private bool MblnPrintEmailPermission = false;
    private bool MblnUpdatePermission = false;
    private bool MblnDeletePermission = false;
    bool blnIsAddMode;

    //Receipt Permissions
    private bool MblnAddPermissionReceipt = false;
    private bool MblnUpdatePermissionReceipt = false;
    private bool MblnDeletePermissionReceipt = false;
    private bool MblnPrintEmailPermissionReceipt = false;
    private bool MblnViewPermissionReceipt = false;
    private bool MblnRenewPermission = false;

    //Issue Permissions
    private bool MblnAddPermissionIssue = false;
    private bool MblnUpdatePermissionIssue = false;
    private bool MblnDeletePermissionIssue = false;
    private bool MblnPrintEmailPermissionIssue = false;
    private bool MblnViewPermissionIssue = false;
    private bool mblnSearchStatus = false;
    private Boolean MiOKClose = false;
    private Boolean DocFlag = false;
    public Boolean MainFlag = false;
    private Boolean ReferenceFlag = false;

    //Object Instantiation
    private ClsNotification mObjNotification = null;
    private clsBLLDocumentMasterNew objDocumentMasterBLL { get; set; }
    clsBLLPermissionSettings objClsBLLPermissionSettings { get; set; }
    private string strOf = "of ";
    #endregion Declarations

    #region Constructor

    public frmDocumentMasterNew()
    {
        InitializeComponent();
        DocumentIDNav = DocumentID;
        //PintEmployeeID = EmployeeID; 
        this.Load += new EventHandler(frmDocumentMasterNew_Load);
        mObjNotification = new ClsNotification();
        if (ClsCommonSettings.IsArabicView)
        {
            this.RightToLeftLayout = true;
            this.RightToLeft = RightToLeft.Yes;
            SetArabicControls();
        }

    }

    #endregion Constructor


    private void SetArabicControls()
    {
        ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        objDAL.SetArabicVersion((int)FormID.Documents, this);
        bnMoreActions.Text = "الإجراءات";
        bnMoreActions.ToolTipText = "الإجراءات";
        mnuItemAttachDocuments.Text = "وثائق";
        receiptToolStripMenuItem.Text = "استلام";
        IssueToolStripMenuItem.Text = "قضية";
        RenewToolStripMenuItem.Text = "جدد";
        txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
        strOf = "من ";
    }
    #region Events

    #region Form Events

    /// <summary>
    /// Form Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void frmDocumentMasterNew_Load(object sender, EventArgs e)
    {
       
        SetAutoCompleteList();
        LoadMessages();
        SetPermissionForReceiptIssue();

        if (DocumentID > 0)
            DocFlag = true;

        ClearAll();
        FillCombos();
        NewDocument();//Adds a New Document
        EmployeeID = PintEmployeeID;
        if (DocumentIDNav > 0)//Navigator View
        {
            CurrentIndex = 1;
            DocumentID = DocumentIDNav;
            DocFlag = true;
            bnCountItem.Text = "1";
            bnPositionItem.Text = "1";
            RefernceDisplay();
            ReferenceFlag = false;
            bnDeleteItem.Enabled = (DocumentIDNav > 0 ? false : MblnDeletePermission);
        }
        else if (EmployeeID > 0 || PintCompanyID > 0)//Employee View and Company View
        {
            GetTotalRecordCount();

            if (TotalRecordCount > 0)
                bnMoveFirstItem_Click(sender, e);
            else
                DocumentID = 0;

            if (PintCompanyID > 0)
            {
                rdbCompany.Checked = true;
                rdbEmployee.Enabled = false;
                GetAllReferenceNumberByOperationTypeID();
                cboReferenceNumber.SelectedValue = PintCompanyID;
            }
            else
            {
                rdbCompany.Enabled = false;
                FillEmployee();
                cboReferenceNumber.SelectedValue = EmployeeID;
            }
            EnableDisableNavigatorActionButtons();//false
            if (PintCompanyID <= 0 && EmployeeID > 0 && TotalRecordCount == 0)
                BtnClear.Enabled = true;
            cboReferenceNumber.Enabled = btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;//BtnClear.Enabled = 
        }
        else
        {
            blnIsAddMode = true;
            rdbCompany.Checked = true;
            rdbEmployee.Checked = true; 
        }
        btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = mblnSearchStatus = false;
    }

    /// <summary>
    /// Event Called when a Form is closed while manipulating a record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void frmDocumentMasterNew_FormClosing(object sender, FormClosingEventArgs e)
    {
        if (btnOk.Enabled)
        {
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);

            if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim().Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                objClsBLLDocumentMaster = null;
                objDocumentMasterBLL = null;
                objClsBLLPermissionSettings = null;
                mObjNotification = null;

                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;//Closes Form
            }
        }
    }

    /// <summary>
    /// Event for defining short-cut keys
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void DocumentMasterNew_KeyDown(object sender, KeyEventArgs e)
    {
        try
        {
            switch (e.KeyData)
            {
                case Keys.F1:
                    BtnHelp_Click(sender, new EventArgs());//Help
                    break;
                case Keys.Escape://Close form
                    this.Close();
                    break;

                case Keys.Control | Keys.Enter:
                    if (bnAddNewItem.Enabled) bnAddNewItem_Click(sender, new EventArgs());//addnew item
                    break;
                case Keys.Alt | Keys.R:
                    if (bnDeleteItem.Enabled) bnDeleteItem_Click(sender, new EventArgs());//delete
                    break;
                case Keys.Control | Keys.E:
                    if (BtnClear.Enabled) BtnClear_Click(sender, new EventArgs());//Clear
                    break;
                case Keys.Control | Keys.Left:
                    if (bnMovePreviousItem.Enabled) bnMovePreviousItem_Click(sender, new EventArgs());//Move to Previous item
                    break;
                case Keys.Control | Keys.Right:
                    if (bnMoveNextItem.Enabled) bnMoveNextItem_Click(sender, new EventArgs());//Move to Next item
                    break;
                case Keys.Control | Keys.Up:
                    if (bnMoveFirstItem.Enabled) bnMoveFirstItem_Click(sender, new EventArgs());//Move to First item
                    break;
                case Keys.Control | Keys.Down:
                    if (bnMoveLastItem.Enabled) bnMoveLastItem_Click(sender, new EventArgs());//Move to Last item
                    break;
                case Keys.Control | Keys.P:
                    if (bnPrint.Enabled) bnPrint_Click(sender, new EventArgs());//Print
                    break;
                case Keys.Control | Keys.M:
                    if (BtnMail.Enabled) BtnMail_Click(sender, new EventArgs());//Email
                    break;
                case Keys.Alt | Keys.I:
                    if (IssueToolStripMenuItem.Enabled)
                        IssueToolStripMenuItem_Click(sender, new EventArgs());  //issue document
                    break;
                case Keys.Alt | Keys.E:
                    if (receiptToolStripMenuItem.Enabled)
                        receiptToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                    break;
                case Keys.Alt | Keys.N:
                    if (RenewToolStripMenuItem.Enabled)
                        RenewToolStripMenuItem_Click(sender, new EventArgs());// renew document
                    break;
                case Keys.Alt | Keys.D:
                    if (mnuItemAttachDocuments.Enabled)
                        mnuItemAttachDocuments_Click(sender, new EventArgs()); // document scan
                    break;
            }
        }
        catch (Exception)
        {
        }
    }

    #endregion Form Events

    #region Save/Cancel/Addnew/Delete

    /// <summary>
    /// Events for Saving/Updating Records
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void bnSaveItem_Click(object sender, EventArgs e)
    {
        MiOKClose = false;

        if (FormValidate())//Validate controls
        {
            if (blnIsAddMode)
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);//Save msg
            else
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);//Update msg
            if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                SaveDocument();//save/Update
            }
            else
            {
                bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = true;
            }

            if (EmployeeID > 0 || PintCompanyID > 0)//From Company and Employee View
            {
                BtnClear.Enabled = cboReferenceNumber.Enabled = false;
            }
            //if (DocFlag)
            //{
            //    bnDeleteItem.Enabled = bnAddNewItem.Enabled = false;
            //}
        }
    }

    /// <summary>
    /// Save Documents
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnSave_Click(object sender, EventArgs e)
    {
        MiOKClose = false;
        bnSaveItem_Click(new object(), new EventArgs());
    }

    /// <summary>
    /// Event to Save/Update and Close
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnOk_Click(object sender, EventArgs e)
    {
        bnSaveItem_Click(new object(), new EventArgs());

        if (MiOKClose)
            this.Close();
    }

    /// <summary>
    /// Adds a new empty Document
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void bnAddNewItem_Click(object sender, EventArgs e)
    {
        ClearAll();
        blnIsAddMode = true;
        lblRenewed.Visible = false;
        NewDocument();//Adds a new document
        blnIsAddMode = true;
        DocFlag = false;
        lblRenewed.Visible = false;
    }

    /// <summary>
    /// Clear Controls
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void BtnClear_Click(object sender, EventArgs e)
    {
        ClearAll();
        NewDocument();
    }

    /// <summary>
    /// Deletes a Document
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void bnDeleteItem_Click(object sender, EventArgs e)
    {

        if (eOperationType == OperationType.Employee)
        {
            int dtDocRequest = clsBLLDocumentMasterNew.GetDocRequestReferenceExists(txtDocumentNumber.Tag.ToInt32(), Convert.ToInt32(cboDocumentType.SelectedValue));
            if (dtDocRequest > 0)
            {
                string Message = "Cannot Delete as Document Request Exists.";
                MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                else
                {
                    DeleteOtherDocument();//Delete Method
                }
            }
        }
        else
        {
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
            if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            else
            {
                DeleteOtherDocument();//Delete Method
            }
        }
        SetAutoCompleteList();
    }

    /// <summary>
    /// Closes Form
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnCancel_Click(object sender, EventArgs e)
    {
        this.Close();
    }

    #endregion Save/Cancel/Addnew/Delete

    #region NavigatorEvents

    /// <summary>
    /// Positions and displays First Record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void bnMoveFirstItem_Click(object sender, EventArgs e)
    {
        sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 9, out MmessageIcon);
        lblstatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
        DocumentID = 0;

        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;

        CurrentIndex = 1;
        GetDocumentDetails();
        if (EmployeeID > 0 || PintCompanyID > 0)//To Disable ReferenceNumber Combo from Navigator and Employee View
        {
            cboReferenceNumber.Enabled = false;
        }
        EnableDisableNavigatorControls();
        EnableDisableNavigatorActionButtons();
    }

    /// <summary>
    /// Positions and displays Previous Record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void bnMovePreviousItem_Click(object sender, EventArgs e)
    {
        sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 10, out MmessageIcon);
        lblstatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);

        DocumentID = 0;

        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;

        GetTotalRecordCount();
        CurrentIndex = CurrentIndex - 1;
        if (CurrentIndex <= 0)
            CurrentIndex = 1;
        else
        {
            GetDocumentDetails();
        }
        if (EmployeeID > 0 || PintCompanyID > 0)//To Disable ReferenceNumber Combo from Navigator and Employee View
        {
            cboReferenceNumber.Enabled = false;
        }
        EnableDisableNavigatorControls();
        EnableDisableNavigatorActionButtons();
    }

    /// <summary>
    /// Positions and displays Next Record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void bnMoveNextItem_Click(object sender, EventArgs e)
    {
        sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 11, out MmessageIcon);
        lblstatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);

        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;

        DocumentID = 0;
        CurrentIndex = CurrentIndex + 1;
        GetDocumentDetails();
        if (EmployeeID > 0 || PintCompanyID > 0)//To Disable ReferenceNumber Combo from Navigator and Employee View
        {
            cboReferenceNumber.Enabled = false;
        }
        EnableDisableNavigatorControls();
        EnableDisableNavigatorActionButtons();
    }

    /// <summary>
    /// Positions and displays Last Record
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void bnMoveLastItem_Click(object sender, EventArgs e)
    {
        sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 12, out MmessageIcon);
        lblstatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);

        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;

        DocumentID = 0;
        GetTotalRecordCount();
        CurrentIndex = TotalRecordCount;
        GetDocumentDetails();
        if (EmployeeID > 0 || PintCompanyID > 0)//To Disable ReferenceNumber Combo from Navigator and Employee View
        {
            cboReferenceNumber.Enabled = false;
        }
      
        EnableDisableNavigatorControls();
        EnableDisableNavigatorActionButtons();
    }

    #endregion NavigatorEvents

    #region Print/Email

    /// <summary>
    /// To print Currently selected Document
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void bnPrint_Click(object sender, EventArgs e)
    {
        LoadReport();
    }

    /// <summary>
    /// Loads Email Content with current Document Details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void BtnMail_Click(object sender, EventArgs e)
    {
        int DocumentID = txtDocumentNumber.Tag.ToInt32();
        clsBLLDocumentMasterNew objClsBLLDocumentMaster = new clsBLLDocumentMasterNew();
        if (DocumentID > 0)
        {
            using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
            {
                ObjEmailPopUp.MsSubject = "Other Document Details";
                ObjEmailPopUp.EmailFormType = EmailFormID.Documents;
                ObjEmailPopUp.EmailSource = objClsBLLDocumentMaster.DocumentMasterEmail(DocumentID);
                ObjEmailPopUp.ShowDialog();
            }
        }
    }

    #endregion Print/Email

    #region Document/Receipt/Issue/Renew

    /// <summary>
    /// Document View/Attach
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void mnuItemAttachDocuments_Click(object sender, EventArgs e)
    {
        using (FrmScanning objScanning = new FrmScanning())
        {
            objScanning.MintDocumentTypeid = cboDocumentType.SelectedValue.ToInt32();
            objScanning.MlngReferenceID = Convert.ToInt32(cboReferenceNumber.SelectedValue);
            objScanning.MintOperationTypeID = rdbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee;
            objScanning.MstrReferenceNo = txtDocumentNumber.Text;

            objScanning.PblnEditable = true;
            objScanning.MintDocumentID = DocumentID;
            objScanning.ShowDialog();
        }
    }

    /// <summary>
    /// Issue
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void IssueToolStripMenuItem_Click(object sender, EventArgs e)
    {
        try
        {
            new frmDocumentReceiptIssue()
            {
                eOperationType = rdbCompany.Checked ? OperationType.Company : OperationType.Employee,
                DocumentID = txtDocumentNumber.Tag.ToInt32(),
                eDocumentType = (DocumentType)(cboDocumentType.SelectedValue),
                DocumentNumber = txtDocumentNumber.Text.Trim(),
                EmployeeID = Convert.ToInt32(cboReferenceNumber.SelectedValue)

            }.ShowDialog();
            if (!DocFlag)
                DocumentID = 0;
            if (PintCompanyID > 0)
                cboReferenceNumber.Enabled = false;
            if (DocFlag)
                bnDeleteItem.Enabled = bnAddNewItem.Enabled = false;
            SetReceiptOrIssueLabel();
        }
        catch (Exception ex)
        {
        }

    }

    /// <summary>
    /// Receipt
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void receiptToolStripMenuItem_Click(object sender, EventArgs e)
    {
        try
        {
            new frmDocumentReceiptIssue()
            {
                eOperationType = rdbCompany.Checked ? OperationType.Company : OperationType.Employee,
                DocumentID = txtDocumentNumber.Tag.ToInt32(),
                eDocumentType = (DocumentType)(cboDocumentType.SelectedValue),
                DocumentNumber = txtDocumentNumber.Text.Trim(),
                EmployeeID = Convert.ToInt32(cboReferenceNumber.SelectedValue),
                ExpiryDate = dtpExpiryDate.Value

            }.ShowDialog();
            if (!DocFlag)
                DocumentID = 0;
            if (PintCompanyID > 0)
                cboReferenceNumber.Enabled = false;
            if (DocFlag)
                bnDeleteItem.Enabled = bnAddNewItem.Enabled = false;
            SetReceiptOrIssueLabel();
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// To Renew a Document
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RenewToolStripMenuItem_Click(object sender, EventArgs e)
    {
        objClsBLLDocumentMaster = new clsBLLDocumentMasterNew();
        objClsBLLDocumentMaster.objClsDTODocumentMaster = new clsDTODocumentMaster();

        if (txtDocumentNumber.Tag.ToInt32() > 0)
        {
            if (MessageBox.Show("Do You want to Renew Document?".Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return;//returns when -No- is clicked
            else //-YES- If the Document is to be Renewed
            {
                DocumentID = txtDocumentNumber.Tag.ToInt32();
                objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID = cboReferenceNumber.SelectedValue.ToInt64();
                ReferenceFlag = true;
                if (clsBLLDocumentMasterNew.UpdateDocumentMaster(DocumentID, cboDocumentType.SelectedValue.ToInt32()) > 0)//Update Renew Status
                {
                    lblRenewed.Visible = true;
                    CurrentIndex = CurrentIndex - 1;

                    cboReferenceNumber.Enabled = rdbCompany.Enabled = rdbEmployee.Enabled = cboReferenceNumber.Enabled = cboDocumentType.Enabled = txtDocumentNumber.Enabled = false;
                    dtpExpiryDate.Enabled = dtpIssuedDate.Enabled = RenewToolStripMenuItem.Enabled = bnDeleteItem.Enabled = BtnClear.Enabled = false;
                    btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;
                }
                if (MessageBox.Show("Do You want to Add New Document?".Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    btnDocumentType.Enabled = ReferenceFlag = false;
                    return;
                }
                NewDocument();//Adds a new Document with currently renewed Datas except for Document Number 
                BtnClear.Enabled = false;
                ShowRenewStatus();//Renew Status
                txtDocumentNumber.Text = string.Empty;
            }
        }
    }

    #endregion Document/Receipt/Issue/Renew

    #region Text Changed Events

    //Enable Button controls on Specified Text Change and clear Error Provider
    private void rdbCompany_CheckedChanged(object sender, EventArgs e)
    {
        if (ClsCommonSettings.IsArabicView)
        {
            lblTypeName.Text = rdbCompany.Checked ? "اسم الشركة" : "اسم الموظف";

        }
        else
        {
            lblTypeName.Text = rdbCompany.Checked ? "Company Name" : "Employee Name";

        }

        //GetTotalRecordCount();//Gets Record Count
        //TotalRecordCount = TotalRecordCount + 1;
        //CurrentIndex = TotalRecordCount;
        EnableDisableNavigatorControls();
        //blnIsAddMode = true;
        EnableDisableNavigatorActionButtons();//false

        GetAllReferenceNumberByOperationTypeID();
    }

    private void cboReferenceNumber_SelectedIndexChanged(object sender, EventArgs e)
    {
        ChangeStatus(sender, e);
    }

    private void cboDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ChangeStatus(sender, e);
    }

    private void txtDocumentNumber_TextChanged(object sender, EventArgs e)
    {
        ChangeStatus(sender, e);
    }

    private void txtRemarks_TextChanged(object sender, EventArgs e)
    {
        ChangeStatus(sender, e);
    }

    private void dtpIssueDate_ValueChanged(object sender, EventArgs e)
    {
        ChangeStatus(sender, e);
    }

    private void dtpExpiryDate_ValueChanged(object sender, EventArgs e)
    {
        ChangeStatus(sender, e);
    }

    private void bnPositionItem_TextChanged(object sender, EventArgs e)
    {
        CurrentIndex = bnPositionItem.Text.ToInt32();
    }

    /// <summary>
    /// This method is called while Updating a record(ie; on key press) 
    /// </summary>
    private void ChangeStatus(object sender, EventArgs e)
    {
        if (blnIsAddMode) // add mode
        {
            btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = MblnAddPermission;
        }
        else  //edit mode
        {
            btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = MblnUpdatePermission;
        }
        errDocuments.Clear();
    }

    #endregion Text Changed Events

    #region Search

    private void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text == string.Empty)
            mblnSearchStatus = false;
        else
            mblnSearchStatus = true;

        if (this.txtSearch.Text.Trim() == string.Empty)
            return;
        GetTotalRecordCount();//Gets Total record count

        if (TotalRecordCount == 0) // No records
        {
            MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 40, out MmessageIcon).Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtSearch.Text = string.Empty;
        }
        else if (TotalRecordCount > 0)
        {
            //Displays records matching searchkey from Starting
            if (txtSearch.Text.Trim() != "")
            {
                bnMoveFirstItem_Click(null, null);
            }
            else
            {   //returns back to previous position             
                NewDocument();
                bnMoveFirstItem_Click(sender, e);
            }
        }
        ReferenceFlag = false;
    }

    private void txtSearch_KeyDown(object sender, KeyEventArgs e)
    {
        if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
        {
            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(sender, null);
        }
    }

    private void txtSearch_TextChanged(object sender, EventArgs e)
    {
        mblnSearchStatus = false;
    }

    #endregion Search

    /// <summary>
    /// Loads all Document Type onto a pop up window , new one can be added deleted and edited
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnDocumentType_Click(object sender, EventArgs e)
    {
        try
        {
            FrmCommonRef objCommon = new FrmCommonRef("Document Type", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DocumentTypeID,Predefined,Description,DescriptionArb", "DocumentTypeReference", "DocumentTypeID not in(17,18,19,20) AND Predefined = 0");
            objCommon.ShowDialog();
            objCommon.Dispose();
            int intDocumentTypeID = Convert.ToInt32(cboDocumentType.SelectedValue);

            GetAllDocumentTypes();

            if (objCommon.NewID != 0)
                cboDocumentType.SelectedValue = objCommon.NewID;
            else
                cboDocumentType.SelectedValue = intDocumentTypeID;
        }
        catch (Exception Ex)
        {
        }
    }

    private void cboReferenceNumber_KeyPress(object sender, KeyPressEventArgs e)
    {
        cboReferenceNumber.DroppedDown = false;
    }

    private void BtnHelp_Click(object sender, EventArgs e)
    {
        using (FrmHelp Help = new FrmHelp())
        { Help.strFormName = "OtherDocuments"; Help.ShowDialog(); }
    }

    #endregion Events

    #region Properties

    private clsBLLDocumentMasterNew objClsBLLDocumentMaster;

    private clsBLLDocumentMasterNew MobjClsBLLDocumentMaster
    {
        get
        {
            if (this.objClsBLLDocumentMaster == null)
                this.objClsBLLDocumentMaster = new clsBLLDocumentMasterNew();

            return this.objClsBLLDocumentMaster;
        }
    }

    #endregion Properties

    #region Methods

    /// <summary>
    /// Messages from Notification Master are loaded initially
    /// </summary>
    public void LoadMessages()
    {
        MaMessageArr = new ArrayList();
        MaStatusMessage = new ArrayList();
        MaMessageArr = mObjNotification.FillMessageArray((int)FormID.Documents, ClsCommonSettings.ProductID);
        MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.Documents, ClsCommonSettings.ProductID);
    }

    /// <summary>
    /// Gets Total Document 
    /// </summary>
    private void GetTotalRecordCount()
    {
        if (!DocFlag)
            DocumentID = 0;

        if (MainFlag)
            eOperationType = 0;

        if (PintCompanyID > 0)
        {
            rdbCompany.Checked = true;
        }

        if (PintEmployeeID > 0)
        {
            rdbCompany.Checked = false;
        }

        TotalRecordCount = clsBLLDocumentMasterNew.GetTotalRecordCount(EmployeeID, DocumentID, PintCompanyID, txtSearch.Text.Trim(),
            rdbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee, PintEmployeeID, (Int32)ePDocumentType);

    }

    /// <summary>
    /// Method called for creating a new Document
    /// </summary>
    private void NewDocument()
    {
        rdbEmployee.Enabled = rdbCompany.Enabled = receiptToolStripMenuItem.Enabled = true;//MblnAddPermission = 
        bnMoreActions.Enabled = IssueToolStripMenuItem.Enabled = bnDeleteItem.Enabled = false;

        if (PintEmployeeID > 0)
        {
            rdbCompany.Enabled = rdbCompany.Checked = false;
        }
        else if (PintCompanyID > 0)
        {
            rdbCompany.Enabled = rdbCompany.Checked = true;
        }

        if (!DocFlag)
        {
            DocumentID = 0;
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 655, out MmessageIcon);
            lblstatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
        }
        else if (!blnIsAddMode)
        {
            CurrentIndex = 1;
            rdbCompany.Checked = true;
            if (blnIsAddMode == false)
            {
                GetDocumentDetails();
            }
            receiptToolStripMenuItem.Enabled = blnIsAddMode;// = true;
            IssueToolStripMenuItem.Enabled = false;
            GetAllReferenceNumberByOperationTypeID();//Fills combo with Company Names 

            if (objClsBLLDocumentMaster != null)
            {
                if (objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID.ToInt32() > 0)
                {
                    cboReferenceNumber.SelectedValue = objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID.ToInt32();
                    rdbEmployee.Enabled = RenewToolStripMenuItem.Checked = false;
                    rdbCompany.Enabled = true;
                }
            }
            cboReferenceNumber.Enabled = cboDocumentType.Enabled = txtDocumentNumber.Enabled = dtpExpiryDate.Enabled = true;
            dtpIssuedDate.Enabled = BtnClear.Enabled = true;
            bnSaveItem.Enabled = btnOk.Enabled = btnSave.Enabled = bnAddNewItem.Enabled = rdbEmployee.Enabled = false;
            return;
        }
        GetTotalRecordCount();//Gets Record Count
        TotalRecordCount = TotalRecordCount + 1;
        CurrentIndex = TotalRecordCount;

        EnableDisableNavigatorControls();
        //blnIsAddMode = true;
        EnableDisableNavigatorActionButtons();//false

        if (rdbCompany.Checked)
        {
            GetAllReferenceNumberByOperationTypeID();//Fills combo with Company Names            
        }
        else
        {
            FillEmployee();//Fills combo with Employees            
        }
        cboReferenceNumber.Enabled = cboDocumentType.Enabled = txtDocumentNumber.Enabled = dtpExpiryDate.Enabled = true;
        dtpIssuedDate.Enabled = BtnClear.Enabled = true;

        if (EmployeeID > 0)//From Employee View
        {
            FillEmployee();
            cboReferenceNumber.SelectedValue = EmployeeID;
            cboReferenceNumber.Enabled = rdbCompany.Enabled = false;
        }
        else if (PintCompanyID > 0)//From Company View
        {
            rdbCompany.Checked = true;
            GetAllReferenceNumberByOperationTypeID();
            cboReferenceNumber.SelectedValue = PintCompanyID;
            cboReferenceNumber.Enabled = rdbEmployee.Enabled = false;
        }
        else if (DocumentID > 0)//From Navigator
        {
            bnMoveFirstItem_Click(null, null);
        }

        bnSaveItem.Enabled = btnOk.Enabled = btnSave.Enabled = mblnSearchStatus = false;
    }

    /// <summary>
    /// method is called for retrieving and displaying Card details from Navigator view
    /// </summary>
    private void RefernceDisplay()
    {
        int RowNum = 0;

        RowNum = EmployeeID > 0 ? 1 : clsBLLDocumentMasterNew.GetDocumentCurrentIndex(DocumentID);
        if (RowNum > 0)
        {
            CurrentIndex = 1;
            ReferenceFlag = true;
            GetDocumentDetails();//Displays 

            if (objClsBLLDocumentMaster != null)
            {
                objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID = cboReferenceNumber.SelectedValue.ToInt64();
                cboReferenceNumber.SelectedValue = objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID.ToInt32();
            }
            EnableDisableReferenceDisplay();//Enable/Disable Buttons and BindingNavigator Controls
        }
    }

    private void EnableDisableReferenceDisplay()
    {
        bnAddNewItem.Enabled = bnMoveFirstItem.Enabled = BtnClear.Enabled = txtSearch.Enabled = btnSearch.Enabled = false;
        bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = bnMovePreviousItem.Enabled = false;
        bnSaveItem.Enabled = false;
        bnDeleteItem.Enabled = MblnDeletePermission; 
    }

    /// <summary>
    /// Retrieves Document Details to be Displayed
    /// </summary>
    private void GetDocumentDetails()
    {
        try
        {
            receiptToolStripMenuItem.Enabled = IssueToolStripMenuItem.Enabled = blnIsAddMode = false;

            GetTotalRecordCount();//Gets Total Document Count

            if (TotalRecordCount > 0)
            {
                if (MainFlag)
                    eDocumentType = 0;

                using (DataTable dt = clsBLLDocumentMasterNew.GetDocumentDetails(CurrentIndex, (int)ePDocumentType, DocumentID, //(int)eOperationType, EmployeeID, PintCompanyID, txtSearch.Text.Trim()))
                    rdbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee, EmployeeID, PintCompanyID, txtSearch.Text.Trim(),PintEmployeeID))
                {
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        DocumentID = dr["DocumentID"].ToInt32();
                        txtDocumentNumber.Tag = dr["DocumentID"].ToInt32();
                        StatusID = dr["StatusID"].ToInt16();

                        //statusid = -1-->new,0-->issued ,1-->receipt
                        if (StatusID == -1 || StatusID == 0)//Status = New
                            receiptToolStripMenuItem.Enabled = MblnViewPermissionReceipt;
                        else
                            IssueToolStripMenuItem.Enabled = MblnViewPermissionIssue;

                        if (IssueToolStripMenuItem.Enabled == false && receiptToolStripMenuItem.Enabled == false)
                            bnMoreActions.Enabled = false;

                        eOperationType = (OperationType)dr["OperationTypeID"].ToInt16();//Operation Type(Employee/Company)

                        objClsBLLDocumentMaster = new clsBLLDocumentMasterNew();
                        objClsBLLDocumentMaster.objClsDTODocumentMaster = new clsDTODocumentMaster();

                        objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID = dr["ReferenceID"].ToInt32();
                        if (eOperationType == OperationType.Employee)
                        {
                            rdbEmployee.Checked = rdbEmployee.Enabled = true;
                            FillEmployee();//fill Employee datas
                            rdbCompany.Enabled = false;
                        }
                        else
                        {
                            rdbCompany.Checked = rdbCompany.Enabled = true;
                            GetAllReferenceNumberByOperationTypeID();//fill company datas
                            rdbEmployee.Enabled = false;
                        }

                        cboDocumentType.SelectedValue = dr["DocumentTypeID"].ToInt16();
                        eDocumentType = (DocumentType)dr["DocumentTypeID"].ToInt16();

                        cboReferenceNumber.SelectedValue = dr["ReferenceID"].ToInt32();
                        txtDocumentNumber.Text = dr["DocumentNumber"].ToString();
                        txtRemarks.Text = dr["Remarks"].ToString();
                        dtpIssuedDate.Value = dr["IssuedDate"].ToDateTime();
                        dtpExpiryDate.Value = dr["ExpiryDate"].ToDateTime();

                        if (DocFlag && MblnAddPermission && !ReferenceFlag)
                            bnAddNewItem.Enabled = RenewToolStripMenuItem.Checked = false;

                        if (Convert.ToBoolean(dr["IsRenewed"].ToBoolean()))
                        {
                            //If Document is Renewed
                            lblRenewed.Visible = true;
                            RenewToolStripMenuItem.Enabled = cboReferenceNumber.Enabled = cboReferenceNumber.Enabled = cboDocumentType.Enabled = txtDocumentNumber.Enabled = false;
                            dtpExpiryDate.Enabled = dtpIssuedDate.Enabled = bnDeleteItem.Enabled = BtnClear.Enabled = false;
                            btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = btnDocumentType.Enabled = false;
                        }
                        else
                        {
                            //Document Not renewed
                            lblRenewed.Visible = btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = BtnClear.Enabled = false;
                            RenewToolStripMenuItem.Enabled = btnDocumentType.Enabled = true;
                            cboReferenceNumber.Enabled = cboDocumentType.Enabled = txtDocumentNumber.Enabled = dtpExpiryDate.Enabled = dtpIssuedDate.Enabled = true;
                        }
                        EnableDisableNavigatorActionButtons();
                        SetReceiptOrIssueLabel();
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    /// <summary>
    /// Clears all Form Controls
    /// </summary>
    private void ClearAll()
    {
        txtDocumentNumber.Tag = 0;
        cboDocumentType.SelectedIndex = cboReferenceNumber.SelectedIndex = -1;
        cboDocumentType.Text = cboReferenceNumber.Text = "";
        txtDocumentNumber.Text = txtRemarks.Text = "";
        rdbEmployee.Checked = true;
        lblRenewed.Visible = mblnSearchStatus = false;

        dtpIssuedDate.Value = DateTime.Now.Date;//Sets Current Date By Default
        dtpExpiryDate.Value = DateTime.Now.AddDays(1);//Current Date + 1

        txtSearch.Text = "";//Clear Search Field
    }

    /// <summary>
    /// Form Control Validation
    /// </summary>
    /// <returns></returns>
    private bool FormValidate()
    {
        errDocuments.Clear();//clear error provider        
        bool bReturnValue = true;
        Control elcControl = new Control();

        if (cboReferenceNumber.SelectedIndex < 0)//Employee/Company Name
        {
            if (rdbCompany.Checked)
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 1069, out MmessageIcon);
            else
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 1070, out MmessageIcon);

            elcControl = cboReferenceNumber;
            bReturnValue = false;
        }

        if (cboDocumentType.SelectedIndex < 0)//If Document Type is left Un selected
        {
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 1051, out MmessageIcon);
            elcControl = cboDocumentType;
            bReturnValue = false;
        }

        if (txtDocumentNumber.Text.Trim() == string.Empty)//Document Number Field Empty
        {
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 1054, out MmessageIcon);
            elcControl = txtDocumentNumber;
            bReturnValue = false;
        }

        if (ReferenceFlag)
            DocumentID = 0;
        else
            DocumentID = txtDocumentNumber.Tag.ToInt32();
        //int operationTypeID = rdbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee;
        //if (clsBLLDocumentMasterNew.IsExistsDocumentNumber(DocumentID, txtDocumentNumber.Text.Trim(), operationTypeID))//Document Number Duplication
        //{
        //    sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 1055, out MmessageIcon);
        //    elcControl = txtDocumentNumber;
        //    bReturnValue = false;
        //}

        if (dtpExpiryDate.Value.Date <= dtpIssuedDate.Value.Date)//Issue Date > Expiry Date
        {
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 43, out MmessageIcon);
            elcControl = dtpExpiryDate;
            bReturnValue = false;
        }

        if (dtpIssuedDate.Value > ClsCommonSettings.GetServerDate())//Issue Date > Current Date
        {
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 42, out MmessageIcon);
            elcControl = dtpIssuedDate;
            bReturnValue = false;
        }

        if (bReturnValue == false)
        {
            MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

            lblstatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
            if (elcControl != null)
            {
                errDocuments.SetError(elcControl, sCommonMessage);
                elcControl.Focus();
            }
            tmDocuments.Enabled = true;
        }
        return bReturnValue;
    }

    /// <summary>
    /// Save/Update Function
    /// </summary>
    private void SaveDocument()
    {
        try
        {
            DateTime ExpectedReturnDate = DateTime.Now;
            objDocumentMasterBLL = new clsBLLDocumentMasterNew();

            if (DocumentID <= 0)
            {
                //blnIsAddMode = true;
                RenewToolStripMenuItem.Checked = false;
            }
            objDocumentMasterBLL.objClsDTODocumentMaster = new clsDTODocumentMaster()//Insertion
            {
                DocumentID = DocumentID,
                OperationTypeID = rdbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee,
                DocumentTypeID = cboDocumentType.SelectedValue.ToInt32(),
                ReferenceNumber = cboReferenceNumber.Text,
                ReferenceID = cboReferenceNumber.SelectedValue.ToInt32(),
                DocumentNumber = txtDocumentNumber.Text.Trim(),
                IssuedDate = dtpIssuedDate.Value,
                ExpiryDate = dtpExpiryDate.Value,
                IsRenew = lblRenewed.Visible,
                Remarks = txtRemarks.Text.Trim()
            };

            DocumentID = objDocumentMasterBLL.SaveDocuments();

            

            if (DocumentID > 0)//If Saved
            {
                clsAlerts objclsAlerts = new clsAlerts();
                MiOKClose = true;
                ReferenceFlag = false;
                txtDocumentNumber.Tag = DocumentID;
                string strMode = "Emp";
                if (rdbCompany.Checked)
                {
                    eOperationType = OperationType.Company;
                    strMode = "Comp";
                    rdbEmployee.Enabled = false;
                    rdbCompany.Enabled = true;
                }
                else
                {
                    eOperationType = OperationType.Employee;
                    strMode = "Emp";
                    rdbCompany.Enabled = false;
                    rdbEmployee.Enabled = true;
                }
                eDocumentType = (DocumentType)cboDocumentType.SelectedValue.ToInt32();
                if (blnIsAddMode)
                {
                    objclsAlerts.AlertMessage(cboDocumentType.SelectedValue.ToInt32(), cboDocumentType.Text, cboDocumentType.Text + "Number", DocumentID, txtDocumentNumber.Text, dtpExpiryDate.Value, strMode, cboReferenceNumber.SelectedValue.ToInt32(), cboReferenceNumber.Text, rdbCompany.Checked,0);
                }
                else
                {
                    if (!lblRenewed.Visible)
                    {
                        objclsAlerts.AlertMessage(cboDocumentType.SelectedValue.ToInt32(), cboDocumentType.Text, cboDocumentType.Text + "Number", DocumentID, txtDocumentNumber.Text, dtpExpiryDate.Value, strMode, cboReferenceNumber.SelectedValue.ToInt32(), cboReferenceNumber.Text, rdbCompany.Checked,0);
                    }
                }
                if (blnIsAddMode)
                {
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);//Save msg 
                    blnIsAddMode = false;
                }
                else
                {
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);//Update msg                     
                }
                lblstatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = BtnClear.Enabled = false;
                bnMoreActions.Enabled = MblnAddUpdatePermission;
                bnAddNewItem.Enabled = (DocumentIDNav > 0 ? false : MblnAddPermission);
            
                bnPrint.Enabled = BtnMail.Enabled = MblnPrintEmailPermission;
                if (blnIsAddMode) bnDeleteItem.Enabled = false;
                else
                {
                    bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (DocumentID > 0 ? MblnDeletePermission : false));
                    //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Document could not be removed." : "Remove");
                }
                bnDeleteItem.Enabled = (DocumentIDNav > 0 ? false : MblnDeletePermission);
                if (lblRenewed.Visible)
                    RenewToolStripMenuItem.Enabled = false;
                else
                    RenewToolStripMenuItem.Enabled = true;
                SetReceiptOrIssueLabel();
                SetAutoCompleteList();
            }
            else
                MessageBox.Show("Failed to save documents", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        catch (Exception ex)
        {
            MessageBox.Show("Failed to save documents", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }

    /// <summary>
    /// Deletion of a Document
    /// </summary>
    private void DeleteOtherDocument()
    {
        try
        {
            if (DocumentID == 0)
                DocumentID = txtDocumentNumber.Tag.ToInt32();

            if (clsBLLDocumentMasterNew.DeleteOtherDocument((int)eOperationType, (int)eDocumentType, DocumentID))
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);

                CurrentIndex = CurrentIndex - 1;
                ClearAll();//Clear Form Controls
                NewDocument();//Add a New Document
            }
            else
                MessageBox.Show("Failed to delete document", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        catch (Exception ex)
        { }
    }

    /// <summary>
    /// Event to set Enability for Binding Navigator Buttons
    /// </summary>
    /// <param name="Status"></param>
    private void EnableDisableNavigatorActionButtons()
    {
        btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;
        bnPrint.Enabled = (blnIsAddMode == true ? false : MblnPrintEmailPermission);
        BtnMail.Enabled = (blnIsAddMode == true ? false : MblnPrintEmailPermission);
        bnMoreActions.Enabled = (blnIsAddMode == true ? false : true);
        if (eOperationType == OperationType.Employee)
        {
            RenewToolStripMenuItem.Enabled = (clsPassportBLL.IsEmployeeInService(cboReferenceNumber.SelectedValue.ToInt64()) ? (lblRenewed.Visible ? false : MblnRenewPermission) : false);
        }
        else
        {
            RenewToolStripMenuItem.Enabled = (lblRenewed.Visible ? false : MblnRenewPermission);
        }
        btnDocumentType.Enabled = (lblRenewed.Visible ? false : true);
        bnAddNewItem.Enabled = (blnIsAddMode == true ? false : (DocumentID > 0 ? MblnAddPermission : false));

        if (blnIsAddMode) bnDeleteItem.Enabled = false;
        else
        {
            bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (DocumentID > 0 ? MblnDeletePermission : false));
            //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Document could not be removed." : "Remove");
        }
        BtnClear.Enabled = (blnIsAddMode == true ? true : false);
    }

    /// <summary>
    /// Method to display Printable Document details
    /// </summary>
    private void LoadReport()
    {
        try
        {
            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = this.Text;
            ObjViewer.PiFormID = (int)FormID.Documents;
            ObjViewer.OperationTypeID = (int)eOperationType;
            ObjViewer.DocumentTypeID = (int)eDocumentType;
            ObjViewer.DocumentID = DocumentID;
            ObjViewer.CurrentIndex = CurrentIndex;
            ObjViewer.ShowDialog();
        }
        catch (Exception ex)
        {
        }
    }

    private void ShowRenewStatus()
    {
        btnSave.Enabled = btnOk.Enabled = false;
        bnPrint.Enabled = (blnIsAddMode == true ? false : true);
        BtnMail.Enabled = (blnIsAddMode == true ? false : true);
        bnMoreActions.Enabled = (blnIsAddMode == true ? false : true);
        bnAddNewItem.Enabled = (blnIsAddMode == true ? false : true);
        if (blnIsAddMode)
        {
            lblRenewed.Visible = false;
            bnDeleteItem.Enabled = false;
            RenewToolStripMenuItem.Enabled = true;
        }
        else
        {
            bnDeleteItem.Enabled = (lblRenewed.Visible ? false : true);
            bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Sorry Unable to Delete as the Document has been Renewed" : "Delete");
        }
        BtnClear.Enabled = (blnIsAddMode == true ? true : false);
    }

    /// <summary>
    /// Method to Enable/Disable Receipt/Issue based on their status(ie; if Receipted then disable receipt and vice-versa)
    /// </summary>
    private void SetReceiptOrIssueLabel()//Receipt/Issue/Document  
    {
        if (eOperationType == OperationType.Employee)
        {
            RenewToolStripMenuItem.Enabled = (clsPassportBLL.IsEmployeeInService(cboReferenceNumber.SelectedValue.ToInt64()) ? (lblRenewed.Visible ? false : MblnRenewPermission) : false);
        }
        else
        {
            RenewToolStripMenuItem.Enabled = (lblRenewed.Visible ? false : MblnRenewPermission);
        }
        receiptToolStripMenuItem.Enabled = IssueToolStripMenuItem.Enabled = false;
        bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)eOperationType, cboDocumentType.SelectedValue.ToInt32(), txtDocumentNumber.Tag.ToInt32());

        if (IsReceipt)
            IssueToolStripMenuItem.Enabled = MblnAddPermissionIssue;
        else
            receiptToolStripMenuItem.Enabled = MblnAddPermissionReceipt;
    }

    /// <summary>
    /// Sets Access Permission based on Role of user
    /// </summary>
    private void SetPermissionForReceiptIssue()
    {
        clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
        if (ClsCommonSettings.RoleID > 3)
        {
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (eOperationType == OperationType.Employee ? (Int32)eMenuID.EmployeeDocumentReceipt : (Int32)eMenuID.CompanyDocumentReceipt), out MblnPrintEmailPermissionReceipt, out MblnAddPermissionReceipt, out MblnUpdatePermissionReceipt, out MblnDeletePermissionReceipt, out MblnViewPermissionReceipt);
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (eOperationType == OperationType.Employee ? (Int32)eMenuID.EmployeeDocumentIssue : (Int32)eMenuID.CompanyDocumentIssue), out MblnPrintEmailPermissionIssue, out MblnAddPermissionIssue, out MblnUpdatePermissionIssue, out MblnDeletePermissionIssue, out MblnViewPermissionIssue);
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.OtherDocuments, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out MblnPrintEmailPermissionReceipt, out MblnRenewPermission, out MblnUpdatePermissionReceipt, out MblnDeletePermissionReceipt);
        }
        else
        {
            MblnAddPermissionReceipt = MblnPrintEmailPermissionReceipt = MblnUpdatePermissionReceipt = MblnDeletePermissionReceipt = MblnViewPermissionReceipt = true;
            MblnAddPermissionIssue = MblnPrintEmailPermissionIssue = MblnUpdatePermissionIssue = MblnDeletePermissionIssue = MblnViewPermissionIssue = true;
            MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = MblnRenewPermission = true;

            bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = (MblnAddPermission || MblnAddUpdatePermission);
        }

        if (MblnAddPermissionReceipt == false && MblnUpdatePermissionReceipt == false)
            MblnViewPermissionReceipt = false;
        else
            MblnViewPermissionReceipt = true;

        if (MblnAddPermissionIssue == false && MblnUpdatePermissionIssue == false)
            MblnViewPermissionIssue = false;
        else
            MblnViewPermissionIssue = true;

        if (MblnViewPermissionReceipt == false && MblnViewPermissionIssue == false)
            bnMoreActions.Enabled = false;

        if (MblnAddPermission || MblnUpdatePermission)
            MblnAddUpdatePermission = true;
        else
            MblnAddUpdatePermission = false;
        BtnMail.Enabled = bnPrint.Enabled = MblnPrintEmailPermission;
        bnDeleteItem.Enabled = MblnDeletePermission;
        bnAddNewItem.Enabled = MblnAddPermission;

        bnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddUpdatePermission;

    }

    /// <summary>
    /// sets Binding Navigator Enability
    /// </summary>
    private void EnableDisableNavigatorControls()
    {
        /* Condition to Enable/Disable the Move First/Next/Previous/Last Navigator Keys based on the record count. ie; if 0 then Disable else Enable*/
        if (DocumentID > 0 && DocFlag)//Navigator view
        {
            bnAddNewItem.Enabled = bnDeleteItem.Enabled = bnMoveFirstItem.Enabled = BtnClear.Enabled = false;
            bnMovePreviousItem.Enabled = bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
        }
        if (TotalRecordCount == 1 || (CurrentIndex > TotalRecordCount))
        {
            bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = false;
            bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = false;
        }
        else if (CurrentIndex == TotalRecordCount)
        {
            bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = false;
            bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = true;
        }
        else if (CurrentIndex == 1)
        {
            bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = true;
            bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = false;
        }
        else if (CurrentIndex < TotalRecordCount)
        {
            bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = true;
            bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = true;
        }

        bnPositionItem.Text = CurrentIndex.ToString();
          bnCountItem.Text = strOf + TotalRecordCount.ToString();
    }

    #region FillCombos

    /// <summary>
    /// Fills Reference Combo with Company Name
    /// </summary>
    private void GetAllReferenceNumberByOperationTypeID()
    {
        cboReferenceNumber.DataSource = clsBLLDocumentMasterNew.GetAllReferenceNumberByOperationTypeID(rdbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee, false);
        cboReferenceNumber.DisplayMember = "ReferenceNumber";
        cboReferenceNumber.ValueMember = "ReferenceID";
        cboReferenceNumber.SelectedIndex = -1;

        if (txtDocumentNumber.Tag.ToInt32() > 0)
        {
            //cboReferenceNumber
            cboReferenceNumber.SelectedValue = objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID.ToInt32();
            rdbEmployee.Enabled = false;
            rdbCompany.Enabled = true;
        }
    }

    private void FillCombos()
    {
        GetAllDocumentTypes();//Method for loading Document type combo
    }

    /// <summary>
    /// Fills Reference Combo with Employee Name
    /// </summary>
    private void FillEmployee()
    {
        DataTable datCombos = null;

        if (ClsCommonSettings.IsArabicView)
        {
            if (blnIsAddMode)
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                    "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" +
                    "ORDER BY ReferenceNumber" });

                if (EmployeeID > 0)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "(WorkStatusID >= 6 AND EmployeeID =" + EmployeeID + ") " +
                        " ORDER BY ReferenceNumber" });

                if (EmployeeID > 0 && txtDocumentNumber.Tag.ToInt32() > 0)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "(WorkStatusID >= 6 OR EmployeeID =" + EmployeeID + ") " +
                        " ORDER BY ReferenceNumber" });

                if (ReferenceFlag)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") OR " +
                        "EmployeeID = " + objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID });
            }
            else
            {
                if (DocumentID > 0)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") OR " +
                        "EmployeeID = " + objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID });

                if (EmployeeID > 0)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "(WorkStatusID > 5 OR " +
                        "EmployeeID =" + EmployeeID + ") ORDER BY ReferenceNumber" });

                if (ReferenceFlag)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") OR " +
                        "EmployeeID = " + objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID });
            }
        }
        else
        {
            if (blnIsAddMode)
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                    "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") " +
                    "ORDER BY ReferenceNumber" });

                if (EmployeeID > 0)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "(WorkStatusID >= 6 AND EmployeeID =" + EmployeeID + ") " +
                        " ORDER BY ReferenceNumber" });

                if (EmployeeID > 0 && txtDocumentNumber.Tag.ToInt32() > 0)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "(WorkStatusID >= 6 OR EmployeeID =" + EmployeeID + ") " +
                        " ORDER BY ReferenceNumber" });
                if (ReferenceFlag)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") OR " +
                        "EmployeeID = " + objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID });
            }
            else
            {
                if (DocumentID > 0)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") OR " +
                        "EmployeeID = " + objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID });

                if (EmployeeID > 0)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "(WorkStatusID > 5 OR " +
                        "EmployeeID =" + EmployeeID + ") ORDER BY ReferenceNumber" });

                if (ReferenceFlag)
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS ReferenceNumber ," +
                        "EmployeeID as ReferenceID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "WorkStatusID >= 6 AND E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") OR " +
                        "EmployeeID = " + objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID });
            }
        }

        cboReferenceNumber.DisplayMember = "ReferenceNumber";
        cboReferenceNumber.ValueMember = "ReferenceID";
        cboReferenceNumber.DataSource = datCombos;
        cboReferenceNumber.SelectedIndex = -1;

        if (txtDocumentNumber.Tag.ToInt32() > 0)
        {
            cboReferenceNumber.SelectedValue = objClsBLLDocumentMaster.objClsDTODocumentMaster.ReferenceID.ToInt32();
            rdbEmployee.Enabled = true;
            rdbCompany.Enabled = false;
        }
    }

    /// <summary>
    /// Fills Document Type combo
    /// </summary>
    private void GetAllDocumentTypes()
    {
        cboDocumentType.DataSource = clsBLLDocumentMasterNew.GetAllOtherDocumentTypes();
        cboDocumentType.DisplayMember = "DocumentType";
        cboDocumentType.ValueMember = "DocumentTypeID";
        cboDocumentType.SelectedIndex = -1;
    }

    #endregion FillCombos

    /// <summary>
    /// AutoComplete and Search
    /// </summary>
    private void SetAutoCompleteList()
    {
        clsBLLDocumentMasterNew objDocumentMasterBLL = new clsBLLDocumentMasterNew();
        clsDTODocumentMaster objDTODocumentMaster = new clsDTODocumentMaster();

        objDTODocumentMaster.ReferenceID = EmployeeID > 0 ? EmployeeID : PintCompanyID;

        this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
        DataTable dt = clsBLLDocumentMasterNew.GetAutoCompleteList(txtSearch.Text.Trim(), objDTODocumentMaster.ReferenceID.ToInt32());
        this.txtSearch.AutoCompleteCustomSource = objDocumentMasterBLL.ConvertToAutoCompleteCollection(dt);
    }

    #endregion Methods
}

