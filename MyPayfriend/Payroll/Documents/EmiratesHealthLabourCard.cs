﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

///
/// Modified By: Megha
/// Date: 12/08/2013
/// Performed Code Optimization and added Functionalities for: Renew/Issue, Search
///

namespace MyPayfriend
{
    public partial class EmiratesHealthLabourCard : Form
    {
        #region Variable Declarations

        public long EmployeeID { get; set; }//From Employee View
        public int PiEmCardID { get; set; }//From Navigator View

        /* Variable Declarations */
        int RecordCnt, CurrentRecCnt;

        bool MAddStatus;
        bool blnAdd, blnUpdate, blnDelete, blnAddUpdate, blnPrintEmail, blnRenew = false; //Sets Add/Update/Delete/Print/Email Permissions
        bool mblnAddIssue, mblnAddReceipt, mblnEmail, mblnDelete, mblnUpdate = false;//Issue-Receipt
        private Boolean MiOKClose = false;
        private bool mblnSearchStatus = false;

        //   Error Message display
        string sCommonMessage, StrMessageCaption;
        public ArrayList MaMessage;
        private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;

        //Object Instantiation
        clsBLLEmiratesCard objBLLEmiratescard = new clsBLLEmiratesCard();
        clsConnection objConnection = new clsConnection();
        clsBLLPermissionSettings objPermission;
        private ClsNotification mObjNotification = null;
        private string strOf = "of ";

        #endregion Variable Declarations

        #region Constructors

        public EmiratesHealthLabourCard(int intID)//from Document
        {
            InitializeComponent();
            StrMessageCaption = ClsCommonSettings.MessageCaption;
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        public EmiratesHealthLabourCard()//From Navigator/Employee View
        {
            InitializeComponent();
            StrMessageCaption = ClsCommonSettings.MessageCaption;
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        #endregion Constructors
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.EmirateHealthCard, this);
            ToolStripDropDownBtnMore.Text = "الإجراءات";
            ToolStripDropDownBtnMore.ToolTipText = "الإجراءات";
            BtnScan.Text = "وثائق";
            ReceiptToolStripMenuItem.Text = "استلام";
            IssueToolStripMenuItem.Text = "قضية";
            RenewToolStripMenuItem.Text = "جدد";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            //lblStatusShow.Text = "حالة :";
            strOf = "من ";
        }
        #region Events

        #region Form-Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EmiratesHealthLabourCard_Load(object sender, EventArgs e)
        {
            SetAutoCompleteList();

            LoadMesage();
            SetPermissions();
            Loadcombos();
            ClearAllControls();
            LoadInitial();

            if (PiEmCardID > 0)//From Navigator View
            {
                CurrentRecCnt = 1;
                BindingNavigatorCountItem.Text = "1";
                BindingNavigatorPositionItem.Text = "1";

                RefernceDisplay();
            }
            else if (EmployeeID > 0)//From Employee View
            {
                RecCount();

                if (RecordCnt > 0)
                    BindingNavigatorMoveFirstItem_Click(sender, e);
                else
                    BtnCancel.Enabled = true;

                CboEmployee.Enabled = BindingNavigatorSaveItem.Enabled = false;//BtnCancel.Enabled = 
                CboEmployee.SelectedValue = EmployeeID;
            }
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 655, out MmessageIcon);
            LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);

            BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
        }

        /// <summary>
        /// Event Called when a Form is closed while manipulating a record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EmiratesCard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnOk.Enabled)
            {
                sCommonMessage = new ClsNotification().GetErrorMessage(MaMessage, 17204, out MmessageIcon);

                if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim().Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    objBLLEmiratescard = null;
                    objPermission = null;
                    mObjNotification = null;
                    objConnection = null;

                    e.Cancel = false;//Retains the form
                }
                else
                {
                    e.Cancel = true;//Closes the form 
                }
            }
        }

        /// <summary>
        /// Event for defining short-cut keys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EmiratesHealthLabourCard_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        if (BtnHelp.Enabled) BtnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape://Close form
                        this.Close();
                        break;

                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled) BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled) BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (BtnCancel.Enabled) BtnCancel_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (BindingNavigatorMovePreviousItem.Enabled) BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Move to Previous item
                        break;
                    case Keys.Control | Keys.Right:
                        if (BindingNavigatorMoveNextItem.Enabled) BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Move to Next item
                        break;
                    case Keys.Control | Keys.Up:
                        if (BindingNavigatorMoveFirstItem.Enabled) BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//Move to First item
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveLastItem.Enabled) BindingNavigatorMoveLastItem_Click(sender, new EventArgs());//Move to Last item
                        break;
                    case Keys.Control | Keys.P:
                        if (BtnPrint.Enabled) BtnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (BtnEmail.Enabled) BtnEmail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.Alt | Keys.I:
                        if (IssueToolStripMenuItem.Enabled)
                            ReceiptToolStripMenuItem_Click(sender, new EventArgs());  //issue document
                        break;
                    case Keys.Alt | Keys.E:
                        if (ReceiptToolStripMenuItem.Enabled)
                            ReceiptToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                        break;
                    case Keys.Alt | Keys.N:
                        if (RenewToolStripMenuItem.Enabled)
                            RenewToolStripMenuItem_Click(sender, new EventArgs());// renew document
                        break;
                    case Keys.Alt | Keys.D:
                        if (BtnScan.Enabled)
                            BtnScan_Click(sender, new EventArgs()); // issue document
                        break;
                }
            }
            catch (Exception)
            {
            }
        }
        #endregion Form-Events

        #region Save/Cancel/Addnew/Delete
        /// <summary>
        /// Event for Adding a new Record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            Loadcombos();
            ClearAllControls();
            AddNewEmiratesCardEntry();

            BtnCancel.Enabled = true;
            BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
            CboEmployee.Focus();
        }

        /// <summary>
        /// Clears the Form Controls only during Updation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            ClearAllControls();
            AddNewEmiratesCardEntry();
        }

        /// <summary>
        /// For Saving a Record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            MiOKClose = false;
            if (SaveEmiratesCardEntry())
            {
                MiOKClose = true;
                if (Grpmain.Tag.ToInt32() > 0 && MAddStatus)
                {
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 2, out MmessageIcon);//Save msg                    
                }
                else
                {
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 21, out MmessageIcon);//Update msg                    
                }
                SetAutoCompleteList();

                LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), StrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                BtnCancel.Enabled = MAddStatus = false;
                BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = BtnCancel.Enabled = false;
                ToolStripDropDownBtnMore.Enabled = blnAddUpdate;
                BindingNavigatorAddNewItem.Enabled = (PiEmCardID > 0 ? false : blnAdd);
                BtnPrint.Enabled = BtnEmail.Enabled = blnPrintEmail;
                if (MAddStatus) BindingNavigatorDeleteItem.Enabled = false;
                else
                {
                    BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (PiEmCardID > 0 ? false : blnDelete));
                    //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Emirates card could not be removed." : "Remove");
                }
                SetReceiptOrIssueLabel();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            MiOKClose = false;
            BindingNavigatorSaveItem_Click(sender, e);
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            BindingNavigatorSaveItem_Click(sender, e);
            if (MiOKClose)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Event for Closing form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Deletes a Record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (Grpmain.Tag.ToInt32() > 0)
            {
                int dtDocRequest = clsBLLDocumentMasterNew.GetDocRequestReferenceExists(Grpmain.Tag.ToInt32(), (int)DocumentType.National_ID_Card);
                if (dtDocRequest > 0)
                {
                    string Message = "Cannot Delete as Document Request Exists.";
                    MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 13, out MmessageIcon);
                    if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), StrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                    else
                        if (DeleteEmiratesCard() == true)  //'' Delete Journal Info
                        {
                            sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 4, out MmessageIcon);
                            MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                            if (PiEmCardID > 0)//Navigator View
                            {
                                BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = BtnCancel.Enabled = false;
                                CardNumberTextBox.Enabled = CardPersonalNoTextBox.Enabled = DTPExpiryDate.Enabled = DTPIssueDate.Enabled = RemarksText.Enabled = false;
                            }
                            BtnCancel.Enabled = true;
                        }
                        else
                        {
                            MessageBox.Show("Cannot delete information", StrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                }
            }
            else
                MessageBox.Show("No record to delete ", StrMessageCaption, MessageBoxButtons.OK);

            BindingNavigatorSaveItem.Enabled = false;
        }

        #endregion Save/Cancel/Addnew/Delete

        #region BindingNavigatorMoveFirst/Last/Previous/NextItem
        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            RecCount();

            if (CurrentRecCnt > 1)
                CurrentRecCnt = 1;

            if (EmployeeID > 0)
            {
                CboEmployee.Enabled = BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
            }
            DisplayEmiratesCardInfo();

            BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;

            sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 9, out MmessageIcon);
            LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
            TmrCard.Enabled = true;

        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            RecCount();

            if (RecordCnt > 0)
            {

                CurrentRecCnt = BindingNavigatorPositionItem.Text.ToInt32() - 1;
                if (CurrentRecCnt <= 0)
                    CurrentRecCnt = 1;
                else
                {
                    DisplayEmiratesCardInfo();
                    if (EmployeeID > 0)
                    {
                        CboEmployee.Enabled = BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
                    }
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 10, out MmessageIcon);
                    LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                    TmrCard.Enabled = true;
                }
            }
            else
                CurrentRecCnt = 1;

            BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            RecCount();

            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            if (RecordCnt > 0)
            {
                if (RecordCnt == 1)
                    CurrentRecCnt = 1;
                else
                    CurrentRecCnt = CurrentRecCnt + 1;


                if (CurrentRecCnt > RecordCnt)
                    CurrentRecCnt = RecordCnt;
                else
                    DisplayEmiratesCardInfo();
                if (EmployeeID > 0)
                {
                    CboEmployee.Enabled = BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
                }
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 11, out MmessageIcon);
                LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                TmrCard.Enabled = true;
            }
            else
                CurrentRecCnt = 0;

            BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            RecCount();
            if (RecordCnt > 0)
            {
                if (RecordCnt != CurrentRecCnt)
                {
                    CurrentRecCnt = RecordCnt;
                    DisplayEmiratesCardInfo();
                    if (EmployeeID > 0)
                    {
                        CboEmployee.Enabled = BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
                    }
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 12, out MmessageIcon);
                    LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                    TmrCard.Enabled = true;
                }
            }
            BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
        }

        #endregion BindingNavigatorMoveFirst/Last/Previous/NextItem

        #region Print/Email
        /// <summary>
        /// Binds the Employee details to be mailed on to the mail's body
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEmail_Click(object sender, EventArgs e)
        {
            if (Grpmain.Tag.ToInt32() > 0)
            {
                objBLLEmiratescard.objDTOEmiratesCard.EmiratesID = Grpmain.Tag.ToInt32();
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = ClsCommonSettings.NationalityCard;
                    ObjEmailPopUp.EmailFormType = EmailFormID.EmirateHealthCard;
                    ObjEmailPopUp.EmailSource = objBLLEmiratescard.GetEmployeeEmirateHealthCardReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        #endregion Print/Email

        #region Document/Receipt/Issue/Renew

        private void BtnScan_Click(object sender, EventArgs e)
        {
            if (objBLLEmiratescard.objDTOEmiratesCard.EmiratesID.ToInt32() > 0)
            {
                using (FrmScanning objScanning = new FrmScanning())
                {
                    objScanning.MintDocumentTypeid = (int)DocumentType.National_ID_Card;
                    objScanning.MlngReferenceID = CboEmployee.SelectedValue.ToInt32();
                    objScanning.MintOperationTypeID = (int)OperationType.Employee;
                    objScanning.MstrReferenceNo = CardNumberTextBox.Text;
                    objScanning.PblnEditable = true;
                    objScanning.MintVendorID = CboEmployee.SelectedValue.ToInt32();
                    objScanning.MintDocumentID = objBLLEmiratescard.objDTOEmiratesCard.EmiratesID.ToInt32();
                    objScanning.ShowDialog();

                }
            }
        }

        /// <summary>
        /// Event is fired when a Receipt is Issued
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmDocumentReceiptIssue()
            {
                eOperationType = OperationType.Employee,
                eDocumentType = DocumentType.National_ID_Card,
                DocumentID = objBLLEmiratescard.objDTOEmiratesCard.EmiratesID.ToInt32(),
                DocumentNumber = CardNumberTextBox.Text,
                EmployeeID = CboEmployee.SelectedValue.ToInt32(),
                ExpiryDate = DTPExpiryDate.Value
            }.ShowDialog();
            SetReceiptOrIssueLabel();

        }

        private void IssueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmDocumentReceiptIssue()
            {
                eOperationType = OperationType.Employee,
                eDocumentType = DocumentType.National_ID_Card,
                DocumentID = objBLLEmiratescard.objDTOEmiratesCard.EmiratesID.ToInt32(),
                DocumentNumber = CardNumberTextBox.Text,
                EmployeeID = CboEmployee.SelectedValue.ToInt32(),
                ExpiryDate = DTPExpiryDate.Value
            }.ShowDialog();
            SetReceiptOrIssueLabel();
        }

        /// <summary>
        /// For Renewal of an Existing Card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RenewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Grpmain.Tag.ToInt32() > 0)
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 17205, out MmessageIcon);
                if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                else //YES Do you wish to add
                {
                    objBLLEmiratescard.objDTOEmiratesCard.EmiratesID = Grpmain.Tag.ToInt32();
                    CardNumberTextBox.Tag = objBLLEmiratescard.objDTOEmiratesCard.EmployeeID;

                    if (objBLLEmiratescard.UpdateEmiratesCardRenewed() > 0)//Updates Renewed field
                    {
                        lblRenewed.Visible = true;
                        CurrentRecCnt = CurrentRecCnt - 1;

                        //Disable Input Controls when Renewed
                        CboEmployee.Enabled = CardNumberTextBox.Enabled = CardPersonalNoTextBox.Enabled = DTPExpiryDate.Enabled = DTPIssueDate.Enabled = false;
                        BindingNavigatorDeleteItem.Enabled = RenewToolStripMenuItem.Enabled = mblnUpdate = mblnDelete = false;
                    }

                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 17206, out MmessageIcon);
                    if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }
                    AddNewEmiratesCardEntry();//Adds new Record with Employee details loaded except the Card Number
                    ShowRenewStatus();//Enable/Disable Renew based on status
                    SetEnableDisable();
                    BtnOk.Enabled = true;
                    BtnCancel.Enabled = false;
                    CardNumberTextBox.Text = string.Empty;
                }
            }
        }

        #endregion Document/Receipt/Issue/Renew

        #region Text-Change-Events

        private void CboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void CboEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            CboEmployee.DroppedDown = false;
        }

        private void RemarksText_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void CardPersonalNoTextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void CardNumberTextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void DTPExpiryDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void xtBox_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        #endregion Text-Change-Events

        #region Search
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;
            if (txtSearch.Text.Trim() == string.Empty)
                return;

            RecCount();

            if (RecordCnt == 0) // No records
            {
                MessageBox.Show(mObjNotification.GetErrorMessage(MaMessage, 40, out MmessageIcon).Replace("#", "").Trim(), StrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
            }
            else if (RecordCnt > 0)
            {
                //Displays records matching searchkey
                if (txtSearch.Text.Trim() != "")
                {
                    BindingNavigatorMoveFirstItem_Click(null, null);
                }
                else
                {
                    AddNewEmiratesCardEntry();
                    BindingNavigatorMoveFirstItem_Click(sender, e);
                }
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                if (e.KeyCode == Keys.Enter)
                    btnSearch_Click(sender, null);
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }

        #endregion Search

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "EmiratesCard"; Help.ShowDialog(); }
        }

        #endregion Events

        #region Functions

        /// <summary>
        /// Load Messages
        /// </summary>
        private void LoadMesage()
        {
            mObjNotification = new ClsNotification();
            MaMessage = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessage = mObjNotification.FillMessageArray((int)FormID.EmirateHealthCard, ClsCommonSettings.ProductID);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.EmirateHealthCard, ClsCommonSettings.ProductID);
        }

        /// <summary>
        /// Permission settings
        /// </summary>
        private void SetPermissions()
        {
            objPermission = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objPermission.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.NationalIDCard, out blnPrintEmail, out blnAdd, out blnUpdate, out blnDelete);
                objPermission.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
                objPermission.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
                objPermission.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out mblnEmail, out blnRenew, out mblnUpdate, out mblnDelete);
            }
            else
            {
                blnAdd = blnUpdate = blnDelete = blnPrintEmail = mblnAddIssue = mblnAddReceipt = blnRenew = true;
            }

            if (blnAdd || blnUpdate)
                blnAddUpdate = true;
            else
                blnAddUpdate = false;

            this.BindingNavigatorAddNewItem.Enabled = blnAdd;
            this.BindingNavigatorDeleteItem.Enabled = blnDelete;
            this.BtnOk.Enabled = this.BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = blnAddUpdate;
            this.BtnEmail.Enabled = this.BtnPrint.Enabled = blnPrintEmail;
        }

        /// <summary>
        /// Method to load the Employee Combo with with Employee names
        /// </summary>
        private void Loadcombos()
        {
            /* Loads Employee Combo for Emirates Card */
            DataTable dtCombos = null;


            if (ClsCommonSettings.IsArabicView)
            {
                if (EmployeeID > 0)
                {
                    if (CardNumberTextBox.Tag.ToInt32() > 0)
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "(E.WorkStatusID >= 6 or E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                    else
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "(E.WorkStatusID >= 6 and E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                }
                else
                {
                    if (CardNumberTextBox.Tag.ToInt32() > 0 && MAddStatus)
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and (E.WorkStatusID >= 6 or E.EmployeeID =" + CardNumberTextBox.Tag.ToInt32() + ")  ORDER BY E.EmployeeFullName" });
                    else
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and E.WorkStatusID >= 6  ORDER BY E.EmployeeFullName" });
                }
            }
            else
            {
                if (EmployeeID > 0)
                {
                    if (CardNumberTextBox.Tag.ToInt32() > 0)
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "(E.WorkStatusID >= 6 or E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                    else
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "(E.WorkStatusID >= 6 and E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                }
                else
                {
                    if (CardNumberTextBox.Tag.ToInt32() > 0 && MAddStatus)
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and (E.WorkStatusID >= 6 or E.EmployeeID =" + CardNumberTextBox.Tag.ToInt32() + ")  ORDER BY E.EmployeeFullName" });
                    else
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                            "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and E.WorkStatusID >= 6  ORDER BY E.EmployeeFullName" });
                }
            }
            CboEmployee.DisplayMember = "EmployeeName";
            CboEmployee.ValueMember = "EmployeeID";
            CboEmployee.DataSource = dtCombos;

            if (Grpmain.Tag.ToInt32() == 0 && MAddStatus == true)
            {
                CboEmployee.Text = null;
            }
            if (CardNumberTextBox.Tag.ToInt32() > 0)
            {
                CboEmployee.SelectedValue = CardNumberTextBox.Tag.ToInt32();
            }
        }

        /// <summary>
        /// Binds Employee names onto the combo having the employee name selected based on EmployeeID
        /// </summary>
        private void FillEmployee()
        {
            DataTable datCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                    "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and E.WorkStatusID >= 6 OR E.EmployeeID = " + objBLLEmiratescard.objDTOEmiratesCard.EmployeeID });

            }
            else
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                    "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and E.WorkStatusID >= 6 OR E.EmployeeID = " + objBLLEmiratescard.objDTOEmiratesCard.EmployeeID });

            }
            CboEmployee.DisplayMember = "EmployeeName";
            CboEmployee.ValueMember = "EmployeeID";
            CboEmployee.DataSource = datCombos;

            if (objBLLEmiratescard.objDTOEmiratesCard.EmiratesID == 0)
            {
                CboEmployee.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// method is called for retrieving and displaying Card details from Navigator view
        /// </summary>
        private void RefernceDisplay()
        {
            objBLLEmiratescard.objDTOEmiratesCard.EmiratesID = PiEmCardID;
            int RowNum = 0;

            RowNum = objBLLEmiratescard.GetRowNumber();
            if (RowNum > 0)
            {
                CurrentRecCnt = 1;
                DisplayEmiratesCardInfo();//Displays cards
                EnableDisableReferenceDisplay();//Enable/Disable Buttons and BindingNavigator Controls

            }
        }

        private void EnableDisableReferenceDisplay()
        {
            BindingNavigatorAddNewItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BtnCancel.Enabled = txtSearch.Enabled = btnSearch.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            BindingNavigatorSaveItem.Enabled = BindingNavigatorDeleteItem.Enabled = false;
            LblStatus.Text = "Emirates Card for " + CboEmployee.Text;
        }

        /// <summary>
        /// Gets Card Count
        /// </summary>
        private void RecCount()
        {
            RecordCnt = 0;

            if (EmployeeID > 0)//Employee View
                objBLLEmiratescard.objDTOEmiratesCard.EmployeeID = EmployeeID;
            else
                objBLLEmiratescard.objDTOEmiratesCard.EmployeeID = 0;

            if (PiEmCardID > 0)//Navigator View
            {
                objBLLEmiratescard.objDTOEmiratesCard.EmiratesID = PiEmCardID;
            }
            else
            {
                objBLLEmiratescard.objDTOEmiratesCard.EmiratesID = 0;
            }
            if (objBLLEmiratescard.DisplayRecordCount(txtSearch.Text.Trim()) > 0)
                RecordCnt = objBLLEmiratescard.objDTOEmiratesCard.iRecordCount;
        }

        /// <summary>
        /// Method for clearing all the Form Controls
        /// </summary>
        private void ClearAllControls()
        {
            CboEmployee.SelectedIndex = -1;
            CboEmployee.Text = "";
            CardNumberTextBox.Text = "";
            CardPersonalNoTextBox.Text = "";
            txtSearch.Text = string.Empty;//Clear Search Field
            DTPIssueDate.Value = System.DateTime.Now.Date;
            DTPExpiryDate.Value = System.DateTime.Now.AddDays(1);
            RemarksText.Text = "";
            lblRenewed.Visible = mblnSearchStatus = false;

            CardNumberTextBox.Tag = 0;
        }

        /// <summary>
        /// Called on page load
        /// </summary>
        private void LoadInitial()
        {
            TmrCard.Enabled = true;
            ErrorProviderCard.Clear();
            Grpmain.Tag = 0;
            ClearAllControls(); /* Clear all controls in the form */

            RecCount();/* Obtains the total number of records*/

            CboEmployee.Enabled = true;

            if (EmployeeID > 0 && RecordCnt > 0)
            {
                CurrentRecCnt = RecordCnt;
                DisplayEmiratesCardInfo();
                CboEmployee.SelectedValue = EmployeeID;
                CboEmployee.Enabled = false;
            }
            else
            {
                CboEmployee.Enabled = MAddStatus = true;
                ToolStripDropDownBtnMore.Enabled = BtnPrint.Enabled = BtnEmail.Enabled = false;
                BindingNavigatorAddNewItem.Enabled = BindingNavigatorDeleteItem.Enabled = lblRenewed.Visible = false;
            }

            if (EmployeeID > 0)
            {
                CboEmployee.SelectedValue = EmployeeID;
                CboEmployee.Enabled = false;
            }

            CurrentRecCnt = RecordCnt;
            BindingNavigatorCountItem.Text = strOf + Convert.ToString(RecordCnt + 1) + "";
            BindingNavigatorPositionItem.Text = (RecordCnt + 1).ToString();
            CurrentRecCnt = BindingNavigatorPositionItem.Text.ToInt32();

            SetEnableDisable();
            SetNavigatorEnability();
            ShowRenewStatus();
        }

        private void SetNavigatorEnability()
        {
            /* Condition to Enable/Disable the Move First/Next/Previous/Last Navigator Keys based on the record count. ie; if 0 then Disable else Enable*/
            BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;

            if (PiEmCardID > 0)//Navigator view
            {
                BindingNavigatorAddNewItem.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BtnCancel.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (RecordCnt == 0)//When No Record
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (CurrentRecCnt >= RecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (CurrentRecCnt == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }

        /// <summary>
        /// Adds a new Card
        /// </summary>
        /// <returns></returns>
        private bool AddNewEmiratesCardEntry()
        {
            TmrCard.Enabled = MAddStatus = true;
            ErrorProviderCard.Clear();
            Grpmain.Tag = 0;

            RecCount();//Total Card Count 
            Loadcombos();//Fills Employee Combo

            BindingNavigatorCountItem.Text = strOf + Convert.ToString(RecordCnt + 1) + "";
            BindingNavigatorPositionItem.Text = (RecordCnt + 1).ToString();
            CurrentRecCnt = BindingNavigatorPositionItem.Text.ToInt32();

            CboEmployee.Enabled = CardNumberTextBox.Enabled = CardPersonalNoTextBox.Enabled = DTPExpiryDate.Enabled = DTPIssueDate.Enabled = true;

            if (EmployeeID > 0)//Employee View
            {
                objBLLEmiratescard.objDTOEmiratesCard.EmployeeID = EmployeeID;
                Loadcombos();
                CboEmployee.SelectedValue = EmployeeID;
                CboEmployee.Enabled = BtnCancel.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
            else
                CboEmployee.Enabled = true;

            SetNavigatorEnability();
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 655, out MmessageIcon);
            LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);

            lblRenewed.Visible = BtnPrint.Enabled = BtnEmail.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
            BindingNavigatorAddNewItem.Enabled = BindingNavigatorDeleteItem.Enabled = ToolStripDropDownBtnMore.Enabled = BindingNavigatorSaveItem.Enabled = false;
            BtnCancel.Enabled = true;

            return true;
        }

        /// <summary>
        /// Method for Inserting and Updating Emirates Card
        /// </summary>
        /// <returns></returns>
        private bool SaveEmiratesCardEntry()
        {
            if (FormValidation())
            {
                if (MAddStatus)
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 1, out MmessageIcon);//Save msg confirmation
                else
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 3, out MmessageIcon);//Update msg confirmation

                if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), StrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = true;
                    return false;
                }

                if (SaveEmiratesCardInfo() == true)
                {
                    BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
                }

                Grpmain.Tag = objBLLEmiratescard.objDTOEmiratesCard.EmiratesID;

                TmrCard.Enabled = ToolStripDropDownBtnMore.Enabled = BindingNavigatorAddNewItem.Enabled = BtnEmail.Enabled = BtnPrint.Enabled = true;
                BindingNavigatorDeleteItem.Enabled = blnDelete;

                return true;
            }
            return false;
        }

        /// <summary>
        /// Method for Validating Form Controls
        /// </summary>
        /// <returns></returns>
        private bool FormValidation()
        {
            ErrorProviderCard.Clear();
            bool bReturnValue = true;
            Control elcControl = new Control();

            if (CboEmployee.SelectedIndex == -1)//employee
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 9124, out MmessageIcon);
                elcControl = CboEmployee;
                bReturnValue = false;

            }
            else if (CardNumberTextBox.Text == "")//empty field
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 17201, out MmessageIcon);
                elcControl = CardNumberTextBox;
                bReturnValue = false;

            }
            else if (DTPIssueDate.Value.Date >= DTPExpiryDate.Value.Date)//Issue date>Expiry Date
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 17202, out MmessageIcon);
                elcControl = DTPIssueDate;
                bReturnValue = false;

            }
            else if (DTPIssueDate.Value > ClsCommonSettings.GetServerDate())//Issue date>Current Date
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 17203, out MmessageIcon);
                elcControl = DTPExpiryDate;
                bReturnValue = false;

            }

            if (bReturnValue == false)
            {
                MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), StrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                if (elcControl != null)
                {
                    ErrorProviderCard.SetError(elcControl, sCommonMessage);
                    elcControl.Focus();
                }

                TmrCard.Enabled = true;
            }
            return bReturnValue;
        }

        /// <summary>
        /// save card
        /// </summary>
        /// <returns></returns>
        private bool SaveEmiratesCardInfo()
        {
            objBLLEmiratescard.objDTOEmiratesCard.EmployeeID = Convert.ToInt32(CboEmployee.SelectedValue);
            objBLLEmiratescard.objDTOEmiratesCard.CardNumber = CardNumberTextBox.Text.ToString().Trim();
            objBLLEmiratescard.objDTOEmiratesCard.CardPersonalIDNumber = CardPersonalNoTextBox.Text.ToString().Trim();
            objBLLEmiratescard.objDTOEmiratesCard.IssueDate = DTPIssueDate.Value.Date;
            objBLLEmiratescard.objDTOEmiratesCard.ExpiryDate = DTPExpiryDate.Value.Date;
            objBLLEmiratescard.objDTOEmiratesCard.Remarks = RemarksText.Text;
            objBLLEmiratescard.objDTOEmiratesCard.IsRenewed = lblRenewed.Visible;

            clsAlerts objclsAlerts = new clsAlerts();

            if (MAddStatus == false)
                objBLLEmiratescard.objDTOEmiratesCard.EmiratesID = Grpmain.Tag.ToInt32();

            if (objBLLEmiratescard.SaveEmiratesCardInfo(MAddStatus))//save
            {

                Grpmain.Tag = objBLLEmiratescard.objDTOEmiratesCard.EmiratesID;
                if (objBLLEmiratescard.objDTOEmiratesCard.EmiratesID > 0)
                {
                    if (MAddStatus)
                    {
                        objclsAlerts.AlertMessage((int)DocumentType.National_ID_Card, ClsCommonSettings.NationalityCard, ClsCommonSettings.NationalityCard + "Number", objBLLEmiratescard.objDTOEmiratesCard.EmiratesID, CardNumberTextBox.Text, DTPExpiryDate.Value, "Emp", CboEmployee.SelectedValue.ToInt32(), CboEmployee.Text.Trim(), false,0);
                    }
                    else
                    {
                        if (objBLLEmiratescard.objDTOEmiratesCard.IsRenewed == false)
                        {
                            objclsAlerts.AlertMessage((int)DocumentType.National_ID_Card, ClsCommonSettings.NationalityCard, ClsCommonSettings.NationalityCard + "Number", objBLLEmiratescard.objDTOEmiratesCard.EmiratesID, CardNumberTextBox.Text, DTPExpiryDate.Value, "Emp", CboEmployee.SelectedValue.ToInt32(), CboEmployee.Text.Trim(), false,0);

                        }
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        private void DisplayEmiratesCardInfo()
        {
            try
            {
                RecCount();

                if (EmployeeID > 0)//Employee view
                {
                    DisplayEmiratesCardInfo(CurrentRecCnt, EmployeeID, 0, txtSearch.Text.Trim());
                    CboEmployee.Enabled = false;
                }
                else
                {
                    CboEmployee.Enabled = true;
                    DisplayEmiratesCardInfo(CurrentRecCnt, 0, PiEmCardID, txtSearch.Text.Trim());
                }

                ErrorProviderCard.Clear();

                BindingNavigatorAddNewItem.Enabled = blnAdd;
                BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = blnAddUpdate;
                BindingNavigatorDeleteItem.Enabled = blnDelete;
                BtnPrint.Enabled = BtnEmail.Enabled = blnPrintEmail;

                MAddStatus = false;
                TmrCard.Enabled = ToolStripDropDownBtnMore.Enabled = true;

                BindingNavigatorPositionItem.Text = CurrentRecCnt.ToString();
                BindingNavigatorCountItem.Text = strOf + Convert.ToString(RecordCnt) + "";

                SetEnableDisable();
                SetNavigatorEnability();
                SetReceiptOrIssueLabel();
            }
            catch (Exception e)
            {
            }
        }

        /// <summary>
        /// Retrieves Card details based on Search factors
        /// </summary>
        /// <param name="CurntCount"></param>
        /// <param name="empid"></param>
        /// <param name="PiEmCardID"></param>
        /// <param name="strSearchKey"></param>
        private void DisplayEmiratesCardInfo(int CurntCount, long empid, int PiEmCardID, string strSearchKey)
        {
            if (objBLLEmiratescard.DisplayEmiratesCardInfo(CurntCount, empid, PiEmCardID, strSearchKey) == true)
            {
                Grpmain.Tag = objBLLEmiratescard.objDTOEmiratesCard.EmiratesID;
            }
            else
            {
                Grpmain.Tag = 0;
            }

            FillEmployee();
            if (objBLLEmiratescard.objDTOEmiratesCard.EmployeeID.ToInt32() == 0)
            {
                Grpmain.Tag = 0;
            }
            //Fill Controls
            CboEmployee.SelectedValue = objBLLEmiratescard.objDTOEmiratesCard.EmployeeID;
            CardNumberTextBox.Text = objBLLEmiratescard.objDTOEmiratesCard.CardNumber.Trim();
            CardPersonalNoTextBox.Text = objBLLEmiratescard.objDTOEmiratesCard.CardPersonalIDNumber.Trim();
            DTPIssueDate.Value = objBLLEmiratescard.objDTOEmiratesCard.IssueDate;
            DTPExpiryDate.Value = objBLLEmiratescard.objDTOEmiratesCard.ExpiryDate;
            RemarksText.Text = objBLLEmiratescard.objDTOEmiratesCard.Remarks;

            if (Convert.ToBoolean(objBLLEmiratescard.objDTOEmiratesCard.IsRenewed))//If Renewed
            {
                lblRenewed.Visible = true;
                CboEmployee.Enabled = CardNumberTextBox.Enabled = CardPersonalNoTextBox.Enabled = DTPExpiryDate.Enabled = DTPIssueDate.Enabled = false;
                RenewToolStripMenuItem.Enabled = false;
            }
            else//Not renewed
            {
                lblRenewed.Visible = false;
                CboEmployee.Enabled = CardNumberTextBox.Enabled = CardPersonalNoTextBox.Enabled = DTPExpiryDate.Enabled = DTPIssueDate.Enabled = true;
                RenewToolStripMenuItem.Enabled = true;
            }
            this.BtnCancel.Enabled = false;
        }

        private bool DeleteEmiratesCard()
        {
            try
            {
                objBLLEmiratescard.objDTOEmiratesCard.EmiratesID = Grpmain.Tag.ToInt32();
                if (objBLLEmiratescard.DeleteEmiratesCardInfo())
                {   //Adds new Card if deleted
                    ClearAllControls();
                    AddNewEmiratesCardEntry();
                    SetAutoCompleteList();
                    TmrCard.Enabled = true;
                    Application.DoEvents();
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private void ChangeStatus()
        {
            if (MAddStatus) // add mode
            {
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = blnAdd;
            }
            else  //edit mode
            {
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = blnUpdate;
            }
            ErrorProviderCard.Clear();
        }
        private void SetEnableDisable()
        {
            BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
            BtnPrint.Enabled = (MAddStatus == true ? false : blnPrintEmail);
            BtnEmail.Enabled = (MAddStatus == true ? false : blnPrintEmail);
            ToolStripDropDownBtnMore.Enabled = (MAddStatus == true ? false : true);
            RenewToolStripMenuItem.Enabled = (lblRenewed.Visible ? false : blnRenew);

            BindingNavigatorAddNewItem.Enabled = (MAddStatus == true ? false : (PiEmCardID > 0 ? false : blnAdd));

            if (MAddStatus) BindingNavigatorDeleteItem.Enabled = false;
            else
            {
                BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (PiEmCardID > 0 ? false : blnDelete));
                //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Emirates card could not be removed." : "Remove");
            }
            BtnCancel.Enabled = (MAddStatus == true ? true : false);
        }
        /// <summary>
        /// To show to be Printed Report format for a particular employee Card
        /// </summary>
        private void LoadReport() //Print
        {
            try
            {
                if (Grpmain.Tag.ToInt32() > 0)
                {
                    objBLLEmiratescard.objDTOEmiratesCard.EmiratesID = Grpmain.Tag.ToInt32();
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    DataTable datEmps = objBLLEmiratescard.FillCombos(new string[] { "CompanyID, FirstName", "EmployeeMaster", "  EmployeeID = " + CboEmployee.SelectedValue.ToInt32() });
                    if (datEmps.Rows.Count > 0)
                    {
                        ObjViewer.intCompanyID = datEmps.Rows[0]["CompanyID"].ToInt32();
                    }
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = objBLLEmiratescard.objDTOEmiratesCard.EmiratesID.ToInt32();
                    ObjViewer.PiFormID = (int)FormID.EmirateHealthCard;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {

            }
        }

        /// <summary>
        /// Method to Enable/Disable Receipt/Issue based on their status(ie; if Receipted then disable receipt and vice-versa)
        /// </summary>
        private void SetReceiptOrIssueLabel()//Receipt/Issue/Document  
        {
            if (!clsPassportBLL.IsEmployeeInService(CboEmployee.SelectedValue.ToInt32()))
            {
                RenewToolStripMenuItem.Enabled = false;
            }
            else
            {
                RenewToolStripMenuItem.Enabled = lblRenewed.Visible ? false : blnRenew;
            }

            ReceiptToolStripMenuItem.Enabled = IssueToolStripMenuItem.Enabled = false;

            bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Employee, (int)DocumentType.National_ID_Card, objBLLEmiratescard.objDTOEmiratesCard.EmiratesID.ToInt32());

            if (IsReceipt)
                IssueToolStripMenuItem.Enabled = mblnAddIssue;
            else
                ReceiptToolStripMenuItem.Enabled = mblnAddReceipt;
        }

        private void ShowRenewStatus()
        {
            BtnSave.Enabled = BtnOk.Enabled = false;
            BtnPrint.Enabled = (MAddStatus == true ? false : true);
            BtnEmail.Enabled = (MAddStatus == true ? false : true);
            ToolStripDropDownBtnMore.Enabled = (MAddStatus == true ? false : true);
            BindingNavigatorAddNewItem.Enabled = (MAddStatus == true ? false : true);
            if (MAddStatus) BindingNavigatorDeleteItem.Enabled = false;
            else
            {
                BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : true);
                BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Sorry Unable to Delete as the Card has been Renewed" : "Delete");
            }
            BtnCancel.Enabled = (MAddStatus == true ? true : false);
        }

        private void SetAutoCompleteList()//AutoComplete and Search
        {
            clsBLLEmiratesCard objBLLEmiratescard = new clsBLLEmiratesCard();

            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;

            //Loads all Employee Name, Card Number and Employee Number for Emirates Card
            DataTable dt = clsBLLInsuranceCard.GetAutoCompleteList(txtSearch.Text.Trim(), EmployeeID, "CardNumber", "EmployeeEmiratesCard");
            this.txtSearch.AutoCompleteCustomSource = objBLLEmiratescard.ConvertToAutoCompleteCollection(dt);
        }

        #endregion Functions
    }
}
