﻿namespace MyPayfriend
{
    partial class EmployeeQualification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label FromMonthLabel;
            System.Windows.Forms.Label ToMonthLabel;
            System.Windows.Forms.Label EmployeeIDLabel;
            System.Windows.Forms.Label UniversityIDLabel;
            System.Windows.Forms.Label SchoolOrCollegeLabel;
            System.Windows.Forms.Label ClearenceCompletedLabel;
            System.Windows.Forms.Label DegreeIDLabel;
            System.Windows.Forms.Label GradeorPercentageLabel;
            System.Windows.Forms.Label CertificateTitleLabel;
            System.Windows.Forms.Label Label13;
            System.Windows.Forms.Label Label4;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmployeeQualification));
            this.EmployeeQualificationDetailsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.BtnScan = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.txtRefNumber = new System.Windows.Forms.TextBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.Label1 = new System.Windows.Forms.Label();
            this.BtnDegree = new System.Windows.Forms.Button();
            this.txtSchoolOrCollege = new System.Windows.Forms.TextBox();
            this.CboUniversity = new System.Windows.Forms.ComboBox();
            this.BtnUniversity = new System.Windows.Forms.Button();
            this.CboDegree = new System.Windows.Forms.ComboBox();
            this.CboEmployee = new System.Windows.Forms.ComboBox();
            this.ChkClearenceCompleted = new System.Windows.Forms.CheckBox();
            this.ChkUniversityClearenceRequired = new System.Windows.Forms.CheckBox();
            this.ChkCertificatesVerified = new System.Windows.Forms.CheckBox();
            this.ChkCertificatesAttested = new System.Windows.Forms.CheckBox();
            this.txtGradeorPercentage = new System.Windows.Forms.TextBox();
            this.txtCertificate = new System.Windows.Forms.TextBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.errEmployeeQualification = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrEmployeeQualification = new System.Windows.Forms.Timer(this.components);
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.ToolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusQual = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            FromMonthLabel = new System.Windows.Forms.Label();
            ToMonthLabel = new System.Windows.Forms.Label();
            EmployeeIDLabel = new System.Windows.Forms.Label();
            UniversityIDLabel = new System.Windows.Forms.Label();
            SchoolOrCollegeLabel = new System.Windows.Forms.Label();
            ClearenceCompletedLabel = new System.Windows.Forms.Label();
            DegreeIDLabel = new System.Windows.Forms.Label();
            GradeorPercentageLabel = new System.Windows.Forms.Label();
            CertificateTitleLabel = new System.Windows.Forms.Label();
            Label13 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeQualificationDetailsBindingNavigator)).BeginInit();
            this.EmployeeQualificationDetailsBindingNavigator.SuspendLayout();
            this.GrpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errEmployeeQualification)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FromMonthLabel
            // 
            FromMonthLabel.AutoSize = true;
            FromMonthLabel.Location = new System.Drawing.Point(12, 162);
            FromMonthLabel.Name = "FromMonthLabel";
            FromMonthLabel.Size = new System.Drawing.Size(63, 13);
            FromMonthLabel.TabIndex = 15;
            FromMonthLabel.Text = "From Period";
            // 
            // ToMonthLabel
            // 
            ToMonthLabel.AutoSize = true;
            ToMonthLabel.Location = new System.Drawing.Point(280, 162);
            ToMonthLabel.Name = "ToMonthLabel";
            ToMonthLabel.Size = new System.Drawing.Size(53, 13);
            ToMonthLabel.TabIndex = 19;
            ToMonthLabel.Text = "To Period";
            // 
            // EmployeeIDLabel
            // 
            EmployeeIDLabel.AutoSize = true;
            EmployeeIDLabel.Location = new System.Drawing.Point(12, 33);
            EmployeeIDLabel.Name = "EmployeeIDLabel";
            EmployeeIDLabel.Size = new System.Drawing.Size(84, 13);
            EmployeeIDLabel.TabIndex = 5;
            EmployeeIDLabel.Text = "Employee Name";
            // 
            // UniversityIDLabel
            // 
            UniversityIDLabel.AutoSize = true;
            UniversityIDLabel.Location = new System.Drawing.Point(12, 58);
            UniversityIDLabel.Name = "UniversityIDLabel";
            UniversityIDLabel.Size = new System.Drawing.Size(53, 13);
            UniversityIDLabel.TabIndex = 11;
            UniversityIDLabel.Text = "University";
            // 
            // SchoolOrCollegeLabel
            // 
            SchoolOrCollegeLabel.AutoSize = true;
            SchoolOrCollegeLabel.Location = new System.Drawing.Point(12, 86);
            SchoolOrCollegeLabel.Name = "SchoolOrCollegeLabel";
            SchoolOrCollegeLabel.Size = new System.Drawing.Size(92, 13);
            SchoolOrCollegeLabel.TabIndex = 9;
            SchoolOrCollegeLabel.Text = "School Or College";
            // 
            // ClearenceCompletedLabel
            // 
            ClearenceCompletedLabel.AutoSize = true;
            ClearenceCompletedLabel.Location = new System.Drawing.Point(250, 261);
            ClearenceCompletedLabel.Name = "ClearenceCompletedLabel";
            ClearenceCompletedLabel.Size = new System.Drawing.Size(0, 13);
            ClearenceCompletedLabel.TabIndex = 37;
            // 
            // DegreeIDLabel
            // 
            DegreeIDLabel.AutoSize = true;
            DegreeIDLabel.Location = new System.Drawing.Point(12, 112);
            DegreeIDLabel.Name = "DegreeIDLabel";
            DegreeIDLabel.Size = new System.Drawing.Size(65, 13);
            DegreeIDLabel.TabIndex = 23;
            DegreeIDLabel.Text = "Qualification";
            // 
            // GradeorPercentageLabel
            // 
            GradeorPercentageLabel.AutoSize = true;
            GradeorPercentageLabel.Location = new System.Drawing.Point(12, 214);
            GradeorPercentageLabel.Name = "GradeorPercentageLabel";
            GradeorPercentageLabel.Size = new System.Drawing.Size(106, 13);
            GradeorPercentageLabel.TabIndex = 29;
            GradeorPercentageLabel.Text = "Grade or Percentage";
            // 
            // CertificateTitleLabel
            // 
            CertificateTitleLabel.AutoSize = true;
            CertificateTitleLabel.Location = new System.Drawing.Point(12, 190);
            CertificateTitleLabel.Name = "CertificateTitleLabel";
            CertificateTitleLabel.Size = new System.Drawing.Size(77, 13);
            CertificateTitleLabel.TabIndex = 27;
            CertificateTitleLabel.Text = "Certificate Title";
            // 
            // Label13
            // 
            Label13.AutoSize = true;
            Label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label13.Location = new System.Drawing.Point(6, 285);
            Label13.Name = "Label13";
            Label13.Size = new System.Drawing.Size(58, 13);
            Label13.TabIndex = 113;
            Label13.Text = "Remarks";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Location = new System.Drawing.Point(12, 136);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(97, 13);
            Label4.TabIndex = 116;
            Label4.Text = "Reference Number";
            // 
            // EmployeeQualificationDetailsBindingNavigator
            // 
            this.EmployeeQualificationDetailsBindingNavigator.AddNewItem = null;
            this.EmployeeQualificationDetailsBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeeQualificationDetailsBindingNavigator.DeleteItem = null;
            this.EmployeeQualificationDetailsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnClear,
            this.ToolStripSeparator1,
            this.BtnActions,
            this.ToolStripSeparator2,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator3,
            this.BtnHelp});
            this.EmployeeQualificationDetailsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeQualificationDetailsBindingNavigator.MoveFirstItem = null;
            this.EmployeeQualificationDetailsBindingNavigator.MoveLastItem = null;
            this.EmployeeQualificationDetailsBindingNavigator.MoveNextItem = null;
            this.EmployeeQualificationDetailsBindingNavigator.MovePreviousItem = null;
            this.EmployeeQualificationDetailsBindingNavigator.Name = "EmployeeQualificationDetailsBindingNavigator";
            this.EmployeeQualificationDetailsBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeeQualificationDetailsBindingNavigator.Size = new System.Drawing.Size(474, 25);
            this.EmployeeQualificationDetailsBindingNavigator.TabIndex = 1;
            this.EmployeeQualificationDetailsBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(23, 22);
            this.BtnClear.Text = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnActions
            // 
            this.BtnActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnScan,
            this.ToolStripSeparator5,
            this.ReceiptToolStripMenuItem,
            this.IssueToolStripMenuItem});
            this.BtnActions.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.BtnActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnActions.Name = "BtnActions";
            this.BtnActions.Size = new System.Drawing.Size(76, 22);
            this.BtnActions.Text = "&Actions";
            this.BtnActions.ToolTipText = "More Actions";
            // 
            // BtnScan
            // 
            this.BtnScan.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.BtnScan.Name = "BtnScan";
            this.BtnScan.Size = new System.Drawing.Size(135, 22);
            this.BtnScan.Text = "&Documents";
            this.BtnScan.Click += new System.EventHandler(this.BtnScan_Click);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(132, 6);
            // 
            // ReceiptToolStripMenuItem
            // 
            this.ReceiptToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.ReceiptToolStripMenuItem.Name = "ReceiptToolStripMenuItem";
            this.ReceiptToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.ReceiptToolStripMenuItem.Text = "R&eceipt";
            this.ReceiptToolStripMenuItem.Click += new System.EventHandler(this.ReceiptToolStripMenuItem_Click);
            // 
            // IssueToolStripMenuItem
            // 
            this.IssueToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.IssueToolStripMenuItem.Name = "IssueToolStripMenuItem";
            this.IssueToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.IssueToolStripMenuItem.Text = "&Issue";
            this.IssueToolStripMenuItem.Click += new System.EventHandler(this.ReceiptToolStripMenuItem_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Fax";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "E-mail";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(Label4);
            this.GrpMain.Controls.Add(this.txtRefNumber);
            this.GrpMain.Controls.Add(Label13);
            this.GrpMain.Controls.Add(this.txtRemarks);
            this.GrpMain.Controls.Add(this.dtpToDate);
            this.GrpMain.Controls.Add(this.dtpFromDate);
            this.GrpMain.Controls.Add(FromMonthLabel);
            this.GrpMain.Controls.Add(ToMonthLabel);
            this.GrpMain.Controls.Add(this.Label1);
            this.GrpMain.Controls.Add(EmployeeIDLabel);
            this.GrpMain.Controls.Add(this.BtnDegree);
            this.GrpMain.Controls.Add(UniversityIDLabel);
            this.GrpMain.Controls.Add(this.txtSchoolOrCollege);
            this.GrpMain.Controls.Add(SchoolOrCollegeLabel);
            this.GrpMain.Controls.Add(this.CboUniversity);
            this.GrpMain.Controls.Add(this.BtnUniversity);
            this.GrpMain.Controls.Add(this.CboDegree);
            this.GrpMain.Controls.Add(this.CboEmployee);
            this.GrpMain.Controls.Add(this.ChkClearenceCompleted);
            this.GrpMain.Controls.Add(ClearenceCompletedLabel);
            this.GrpMain.Controls.Add(this.ChkUniversityClearenceRequired);
            this.GrpMain.Controls.Add(this.ChkCertificatesVerified);
            this.GrpMain.Controls.Add(this.ChkCertificatesAttested);
            this.GrpMain.Controls.Add(DegreeIDLabel);
            this.GrpMain.Controls.Add(this.txtGradeorPercentage);
            this.GrpMain.Controls.Add(GradeorPercentageLabel);
            this.GrpMain.Controls.Add(CertificateTitleLabel);
            this.GrpMain.Controls.Add(this.txtCertificate);
            this.GrpMain.Controls.Add(this.shapeContainer2);
            this.GrpMain.Location = new System.Drawing.Point(8, 57);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(453, 376);
            this.GrpMain.TabIndex = 58;
            this.GrpMain.TabStop = false;
            // 
            // txtRefNumber
            // 
            this.txtRefNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtRefNumber.Location = new System.Drawing.Point(139, 133);
            this.txtRefNumber.MaxLength = 50;
            this.txtRefNumber.Name = "txtRefNumber";
            this.txtRefNumber.Size = new System.Drawing.Size(264, 20);
            this.txtRefNumber.TabIndex = 7;
            this.txtRefNumber.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(18, 303);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(422, 65);
            this.txtRemarks.TabIndex = 16;
            this.txtRemarks.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(339, 159);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(101, 20);
            this.dtpToDate.TabIndex = 9;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(139, 158);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(101, 20);
            this.dtpFromDate.TabIndex = 8;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(6, 1);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(163, 13);
            this.Label1.TabIndex = 57;
            this.Label1.Text = "Certifications && Institutions";
            // 
            // BtnDegree
            // 
            this.BtnDegree.Location = new System.Drawing.Point(408, 106);
            this.BtnDegree.Name = "BtnDegree";
            this.BtnDegree.Size = new System.Drawing.Size(32, 23);
            this.BtnDegree.TabIndex = 6;
            this.BtnDegree.Text = "...";
            this.BtnDegree.UseVisualStyleBackColor = true;
            this.BtnDegree.Click += new System.EventHandler(this.BtnDegree_Click);
            // 
            // txtSchoolOrCollege
            // 
            this.txtSchoolOrCollege.Location = new System.Drawing.Point(139, 83);
            this.txtSchoolOrCollege.MaxLength = 100;
            this.txtSchoolOrCollege.Name = "txtSchoolOrCollege";
            this.txtSchoolOrCollege.Size = new System.Drawing.Size(301, 20);
            this.txtSchoolOrCollege.TabIndex = 3;
            this.txtSchoolOrCollege.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // CboUniversity
            // 
            this.CboUniversity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboUniversity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboUniversity.DropDownHeight = 134;
            this.CboUniversity.FormattingEnabled = true;
            this.CboUniversity.IntegralHeight = false;
            this.CboUniversity.Location = new System.Drawing.Point(139, 56);
            this.CboUniversity.Name = "CboUniversity";
            this.CboUniversity.Size = new System.Drawing.Size(264, 21);
            this.CboUniversity.TabIndex = 1;
            this.CboUniversity.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            this.CboUniversity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboUniversity_KeyPress);
            // 
            // BtnUniversity
            // 
            this.BtnUniversity.Location = new System.Drawing.Point(409, 56);
            this.BtnUniversity.Name = "BtnUniversity";
            this.BtnUniversity.Size = new System.Drawing.Size(32, 23);
            this.BtnUniversity.TabIndex = 2;
            this.BtnUniversity.Text = "...";
            this.BtnUniversity.UseVisualStyleBackColor = true;
            this.BtnUniversity.Click += new System.EventHandler(this.BtnUniversity_Click);
            // 
            // CboDegree
            // 
            this.CboDegree.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboDegree.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDegree.BackColor = System.Drawing.SystemColors.Info;
            this.CboDegree.DropDownHeight = 134;
            this.CboDegree.FormattingEnabled = true;
            this.CboDegree.IntegralHeight = false;
            this.CboDegree.Location = new System.Drawing.Point(139, 108);
            this.CboDegree.Name = "CboDegree";
            this.CboDegree.Size = new System.Drawing.Size(264, 21);
            this.CboDegree.TabIndex = 5;
            this.CboDegree.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            this.CboDegree.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboDegree_KeyPress);
            // 
            // CboEmployee
            // 
            this.CboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.CboEmployee.DropDownHeight = 134;
            this.CboEmployee.FormattingEnabled = true;
            this.CboEmployee.IntegralHeight = false;
            this.CboEmployee.Location = new System.Drawing.Point(139, 30);
            this.CboEmployee.Name = "CboEmployee";
            this.CboEmployee.Size = new System.Drawing.Size(301, 21);
            this.CboEmployee.TabIndex = 0;
            this.CboEmployee.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            this.CboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboEmployee_KeyPress);
            // 
            // ChkClearenceCompleted
            // 
            this.ChkClearenceCompleted.Location = new System.Drawing.Point(253, 261);
            this.ChkClearenceCompleted.Name = "ChkClearenceCompleted";
            this.ChkClearenceCompleted.Size = new System.Drawing.Size(175, 21);
            this.ChkClearenceCompleted.TabIndex = 15;
            this.ChkClearenceCompleted.Text = "Clearance Completed?";
            this.ChkClearenceCompleted.UseVisualStyleBackColor = true;
            this.ChkClearenceCompleted.CheckedChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // ChkUniversityClearenceRequired
            // 
            this.ChkUniversityClearenceRequired.Location = new System.Drawing.Point(253, 237);
            this.ChkUniversityClearenceRequired.Name = "ChkUniversityClearenceRequired";
            this.ChkUniversityClearenceRequired.Size = new System.Drawing.Size(187, 21);
            this.ChkUniversityClearenceRequired.TabIndex = 13;
            this.ChkUniversityClearenceRequired.Text = "University Clearance Required?";
            this.ChkUniversityClearenceRequired.UseVisualStyleBackColor = true;
            this.ChkUniversityClearenceRequired.CheckedChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // ChkCertificatesVerified
            // 
            this.ChkCertificatesVerified.Location = new System.Drawing.Point(78, 261);
            this.ChkCertificatesVerified.Name = "ChkCertificatesVerified";
            this.ChkCertificatesVerified.Size = new System.Drawing.Size(142, 24);
            this.ChkCertificatesVerified.TabIndex = 14;
            this.ChkCertificatesVerified.Text = "Certificates Verified?";
            this.ChkCertificatesVerified.UseVisualStyleBackColor = true;
            this.ChkCertificatesVerified.CheckedChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // ChkCertificatesAttested
            // 
            this.ChkCertificatesAttested.Location = new System.Drawing.Point(78, 237);
            this.ChkCertificatesAttested.Name = "ChkCertificatesAttested";
            this.ChkCertificatesAttested.Size = new System.Drawing.Size(128, 24);
            this.ChkCertificatesAttested.TabIndex = 12;
            this.ChkCertificatesAttested.Text = "Certificates Attested?";
            this.ChkCertificatesAttested.UseVisualStyleBackColor = true;
            this.ChkCertificatesAttested.CheckedChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtGradeorPercentage
            // 
            this.txtGradeorPercentage.Location = new System.Drawing.Point(139, 211);
            this.txtGradeorPercentage.MaxLength = 10;
            this.txtGradeorPercentage.Name = "txtGradeorPercentage";
            this.txtGradeorPercentage.Size = new System.Drawing.Size(75, 20);
            this.txtGradeorPercentage.TabIndex = 11;
            this.txtGradeorPercentage.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtCertificate
            // 
            this.txtCertificate.Location = new System.Drawing.Point(139, 186);
            this.txtCertificate.MaxLength = 50;
            this.txtCertificate.Name = "txtCertificate";
            this.txtCertificate.Size = new System.Drawing.Size(301, 20);
            this.txtCertificate.TabIndex = 10;
            this.txtCertificate.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(447, 357);
            this.shapeContainer2.TabIndex = 114;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 53;
            this.lineShape1.X2 = 438;
            this.lineShape1.Y1 = 276;
            this.lineShape1.Y2 = 276;
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(8, 439);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 62;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Location = new System.Drawing.Point(387, 439);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 60;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOk.Location = new System.Drawing.Point(305, 439);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 59;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // errEmployeeQualification
            // 
            this.errEmployeeQualification.ContainerControl = this;
            this.errEmployeeQualification.RightToLeft = true;
            // 
            // tmrEmployeeQualification
            // 
            this.tmrEmployeeQualification.Interval = 2000;
            this.tmrEmployeeQualification.Tick += new System.EventHandler(this.tmrEmployeeQualification_Tick);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(474, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 89;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Tooltip = "Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ToolStripStatus
            // 
            this.ToolStripStatus.Name = "ToolStripStatus";
            this.ToolStripStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 17);
            this.lblStatus.Text = "Status";
            this.lblStatus.ToolTipText = "Status";
            // 
            // StatusQual
            // 
            this.StatusQual.Name = "StatusQual";
            this.StatusQual.Size = new System.Drawing.Size(0, 17);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatus,
            this.ToolStripStatusLabel1,
            this.lblStatus,
            this.StatusQual});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 470);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(474, 22);
            this.StatusStrip1.TabIndex = 61;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // EmployeeQualification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 492);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.GrpMain);
            this.Controls.Add(this.EmployeeQualificationDetailsBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmployeeQualification";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Qualification";
            this.Load += new System.EventHandler(this.EmployeeQualification_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EmployeeQualification_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EmployeeQualification_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeQualificationDetailsBindingNavigator)).EndInit();
            this.EmployeeQualificationDetailsBindingNavigator.ResumeLayout(false);
            this.EmployeeQualificationDetailsBindingNavigator.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errEmployeeQualification)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator EmployeeQualificationDetailsBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripDropDownButton BtnActions;
        internal System.Windows.Forms.ToolStripMenuItem BtnScan;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripMenuItem ReceiptToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem IssueToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button BtnDegree;
        internal System.Windows.Forms.TextBox txtSchoolOrCollege;
        internal System.Windows.Forms.ComboBox CboUniversity;
        internal System.Windows.Forms.Button BtnUniversity;
        internal System.Windows.Forms.ComboBox CboDegree;
        internal System.Windows.Forms.ComboBox CboEmployee;
        internal System.Windows.Forms.CheckBox ChkClearenceCompleted;
        internal System.Windows.Forms.CheckBox ChkUniversityClearenceRequired;
        internal System.Windows.Forms.CheckBox ChkCertificatesVerified;
        internal System.Windows.Forms.CheckBox ChkCertificatesAttested;
        internal System.Windows.Forms.TextBox txtGradeorPercentage;
        internal System.Windows.Forms.TextBox txtCertificate;
      
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.ErrorProvider errEmployeeQualification;
        private System.Windows.Forms.Timer tmrEmployeeQualification;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatus;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.ToolStripStatusLabel StatusQual;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        internal System.Windows.Forms.TextBox txtRefNumber;

    }
}