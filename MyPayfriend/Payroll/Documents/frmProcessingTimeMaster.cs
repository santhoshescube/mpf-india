﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MyPayfriend
{
    /// <summary>
    /// Form used to insert and update alert  procesing time details 
    /// </summary>
    /// <createdby></createdby>
    /// <createdon></createdon>
    ///<Modified by>Jisha Bijoy</Modified>
    ///<Modified on>14 Aug 2013</Modified>
    ///<Purpose>Fine tuning ,code review and added apply all functionality in cell</Purpose>
    public partial class frmProcessingTimeMaster : Form
    {
        #region Declartions

        public int PintTimeMasterID = 0, MDOCID = 0, iCurRow = 0, iCurColumn = 0, MintTimerInterval;

        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;              //To set Add Permission
        private bool MblnUpdatePermission = false;          //To set Update Permission
        private bool MblnDeletePermission = false;         //To set Delete Permission
        private bool MblnViewPermission = false;
        private bool MiOKClose = false;
        private bool MFlagChange;

        private string MstrMessageCaption;              //Message caption
        private string MstrMessageCommon;              //to set the message           

        private ArrayList MsarMessageArr;               // Error Message display
        private ArrayList MsarStatusMessage;            // To set status message 

        private MessageBoxIcon MmessageIcon;          // to set the message icon

        ClsLogWriter mObjLogs;
        ClsNotification mObjNotification;
        clsAlerts objAlerts;
        clsBLLProcessingTimeMaster objclsBLLProcessingTimeMaster = null;

        #endregion Declartions

        #region Functions
        /// <summary>
        ///  Inializing  all objects
        /// </summary>
        public frmProcessingTimeMaster()
        {
            InitializeComponent();
            objclsBLLProcessingTimeMaster = new clsBLLProcessingTimeMaster();
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            mObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            objAlerts = new clsAlerts();
            grdProcessingtime.CellValueChanged += new DataGridViewCellEventHandler(OnInputChanged);
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }

        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.ProcessingTimeMaster, this);
        }
        /// <summary>
        /// populate messages 
        /// </summary>
        private void LoadMessage()
        {
            try
            {
                // Loading Message
                MsarMessageArr = new ArrayList();
                MsarStatusMessage = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.ProcessingTimeMaster, 2);
                MsarStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.ProcessingTimeMaster, 2);
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadMessage()" + ex.Message.ToString());
            }
        }

        /// <summary>
        /// Apply permissions
        /// </summary>
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.ProcessingTime, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission, out MblnViewPermission);

            }
            else
                MblnUpdatePermission = MblnViewPermission = true;
        }

        /// <summary>
        /// Populating user combo box
        /// </summary>     
        private void LoadCombos()
        {
            try
            {
                cbouser.ValueMember = "userid";
                cbouser.DisplayMember = "username";
                cbouser.DataSource = objclsBLLProcessingTimeMaster.FillCombos(new string[] { "distinct UM.userid,UM.username", "UserMaster UM inner join UserCompanyDetails UC on UM.UserId=UC.UserID ", "UC.CompanyID= "+ClsCommonSettings.CurrentCompanyID+" and UM.userid>1" });

            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }

        }
        /// <summary>
        /// Populating grid 
        /// </summary>      
        private void ProcessTimeGrid()
        {
            objclsBLLProcessingTimeMaster.DTOProcessingTimeMaster.intUserID = Convert.ToInt32(cbouser.SelectedValue);
            objclsBLLProcessingTimeMaster.DTOProcessingTimeMaster.intDocumentTypeID = MDOCID;
            grdProcessingtime.DataSource = objclsBLLProcessingTimeMaster.FillGrid();

            grdProcessingtime.Columns["ProcessingId"].Visible = false;
            grdProcessingtime.Columns["Status"].Visible = false;
            grdProcessingtime.Columns["DocumentTypeID"].Visible = false;
            grdProcessingtime.Columns["PrTime"].Visible = false;
            if (ClsCommonSettings.IsArabicView)
            {
                grdProcessingtime.Columns["DocumentType"].HeaderText = "نوع الوثيقة";
                grdProcessingtime.Columns["ProcessingTime"].HeaderText = "زمن التحميل";
                grdProcessingtime.Columns["AlertInterval"].HeaderText = "تنبيه الفاصل";
                grdProcessingtime.Columns["ReturnTime"].HeaderText = "عودة التوقيت";
            }
            if (grdProcessingtime.Rows.Count > 0)
            {
                grdProcessingtime.Columns[1].Width = 150;
                grdProcessingtime.Columns[1].Width = 90;
                grdProcessingtime.Columns[1].ReadOnly = true;
                grdProcessingtime.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }



        }
        /// <summary>
        /// Fill parameters to DTO
        /// </summary>
        private void FillParameters()
        {
            objclsBLLProcessingTimeMaster.DTOProcessingTimeMaster.DTOProcessingTimeMasterDetail = new List<clsDTOProcessingTimeMasterDetail>();

            for (int i = 0; i < grdProcessingtime.Rows.Count; i++)
            {
                if (grdProcessingtime.Rows[i].Cells["Status"].Value.ToInt32() == 1)
                {
                    clsDTOProcessingTimeMasterDetail objDetail = new clsDTOProcessingTimeMasterDetail();
                    objDetail.intProcessingID = grdProcessingtime.Rows[i].Cells[0].Value.ToInt32();
                    objDetail.intProcessingTime = grdProcessingtime.Rows[i].Cells[2].Value.ToInt32();
                    objDetail.intAlertInterval = grdProcessingtime.Rows[i].Cells[3].Value.ToInt32();
                    objDetail.intReturnTime = grdProcessingtime.Rows[i].Cells[5].Value.ToInt32();
                    objclsBLLProcessingTimeMaster.DTOProcessingTimeMaster.DTOProcessingTimeMasterDetail.Add(objDetail);
                }
            }

        }
        /// <summary>
        /// Save processing time and  update alert due date
        /// </summary>
        /// <returns>bool indicates  insertion or updation status  </returns>
        private bool SaveProcessingtime()
        {
            try
            {
                if (cbouser.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 10216, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    cbouser.Focus();
                    return false;
                }


                if (MiOKClose == true)
                    MstrMessageCommon = "Do you want to save and close?";
                else
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3, out MmessageIcon);

                MstrMessageCommon = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                if (grdProcessingtime.IsCurrentCellDirty)
                    grdProcessingtime.CommitEdit(DataGridViewDataErrorContexts.Commit);

                if (MessageBox.Show(MstrMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    FillParameters();
                    objclsBLLProcessingTimeMaster.docProcessingTime(); // insert processing time
                }
                else
                {
                    return false;
                }

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 21, out MmessageIcon);
                MstrMessageCommon = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                MstrMessageCommon = "Do you wish to update the existing documents processingtime for the user -";
                if (MessageBox.Show(MstrMessageCommon + cbouser.Text + "?", MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    objAlerts.UpdateAlerts(Convert.ToInt32(cbouser.SelectedValue.ToInt32()));
                    // updating document alert due date for the selected user
                    if (UpdateAlerts())
                    {
                        MstrMessageCommon = "Updated successfully.";
                        MessageBox.Show(MstrMessageCommon);
                        this.lblStatus.Text = MstrMessageCommon;
                    }
                    else
                    {
                        MstrMessageCommon = "Updated failed.";
                        MessageBox.Show(MstrMessageCommon);
                        this.lblStatus.Text = MstrMessageCommon;
                    }
                }


                MFlagChange = false;
                btnOk.Enabled = MFlagChange;
                btnSave.Enabled = MFlagChange;
                ProcessTimeGrid();


                return true;
            }
            catch (Exception Ex)
            {

                mObjLogs.WriteLog("Error on saveprocessingTime() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on SaveProcessingTime()" + Ex.Message.ToString());

                return false;
            }
        }

        /// <summary>
        /// update alert due date in alerts table for the selected user
        /// </summary>
        /// <returns>updation status</returns>
        private bool UpdateAlerts()
        {
            try
            {
                bool blnStatus = false;

                for (int i = 0; i < grdProcessingtime.Rows.Count; i++)
                {
                    if (grdProcessingtime.Rows[i].Cells["Status"].Value.ToInt32() == 1)
                    {
                        objclsBLLProcessingTimeMaster.DTOProcessingTimeMaster.intUserID = Convert.ToInt32(cbouser.SelectedValue);
                        objclsBLLProcessingTimeMaster.DTOProcessingTimeMaster.intDocumentTypeID = Convert.ToInt32(grdProcessingtime.Rows[i].Cells["DocumenttypeID"].Value);
                        objclsBLLProcessingTimeMaster.DTOProcessingTimeMaster.intProcessingtime = Convert.ToInt32(grdProcessingtime.Rows[i].Cells["ProcessingTime"].Value);
                        objclsBLLProcessingTimeMaster.DTOProcessingTimeMaster.intReturnTime = grdProcessingtime.Rows[i].Cells["ReturnTime"].Value.ToInt32();
                        blnStatus = objclsBLLProcessingTimeMaster.UpdateAlert();

                    }
                }
                return blnStatus;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on UpdateAlerts() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on UpdateAlerts()" + Ex.Message.ToString());
                return false;
            }
        }

        private void SetEnableDisable(ControlState status)
        {
            if (status == ControlState.Enable)
            {
                btnOk.Enabled = btnSave.Enabled = SaveToolStripButton.Enabled = MblnUpdatePermission;
            }
            else
            {
                btnOk.Enabled = btnSave.Enabled = SaveToolStripButton.Enabled = false;
            }
        }

        public void OnInputChanged(object sender, EventArgs e)
        {
            this.SetEnableDisable(ControlState.Enable);
        }
        #endregion

        #region Events

        private void frmProcessingTimeMaster_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadCombos();
            LoadMessage();
            SetEnableDisable(ControlState.Disable);
        }

        void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[];.'\"";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0))
            {
                e.Handled = true;
            }
        }

        private void cbouser_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProcessTimeGrid();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (MFlagChange == true)
                MiOKClose = true;


            if (SaveProcessingtime() == true)
            {
                this.Close();
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            MiOKClose = false;
            SaveProcessingtime();

        }

        private void SaveToolStripButton_Click(object sender, EventArgs e)
        {
            MiOKClose = false;
            SaveProcessingtime();
        }
        private void BtnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            {
                Help.strFormName = "ProcessingTime"; Help.ShowDialog();
            }
        }
        private void grdProcessingtime_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            btnOk.Enabled = MblnUpdatePermission;
            btnSave.Enabled = MblnUpdatePermission;
        }

        private void grdProcessingtime_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if ((e.RowIndex >= 0))
                    grdProcessingtime.CurrentRow.Cells["Status"].Value = 1;


                btnOk.Enabled = MblnUpdatePermission;
                btnSave.Enabled = MblnUpdatePermission;
            }
            catch (Exception)
            { }
        }

        private void grdProcessingtime_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            MFlagChange = true;
        }

        private void cbouser_KeyPress(object sender, KeyPressEventArgs e)
        {
            cbouser.DroppedDown = false;
        }

        private void grdProcessingtime_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if ((grdProcessingtime.CurrentCell.ColumnIndex == grdProcessingtime.Columns["ProcessingTime"].Index) || (grdProcessingtime.CurrentCell.ColumnIndex == grdProcessingtime.Columns["AlertInterval"].Index) || (grdProcessingtime.CurrentCell.ColumnIndex == grdProcessingtime.Columns["ReturnTime"].Index))
            {
                TextBox txt = (TextBox)(e.Control);
                txt.KeyPress += new KeyPressEventHandler(txt_KeyPress);

            }
            if (grdProcessingtime.CurrentCell.ColumnIndex == grdProcessingtime.Columns["ProcessingTime"].Index)
            {
                TextBox txt = (TextBox)(e.Control);
                txt.MaxLength = 3;
            }
            if ((grdProcessingtime.CurrentCell.ColumnIndex == grdProcessingtime.Columns["AlertInterval"].Index) || (grdProcessingtime.CurrentCell.ColumnIndex == grdProcessingtime.Columns["ReturnTime"].Index))
            {
                TextBox txt = (TextBox)(e.Control);
                txt.MaxLength = 2;
            }
        }

        private void btnBottomCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdProcessingtime_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    if (e.RowIndex > -1 && e.ColumnIndex > 1)
                    {
                        iCurColumn = e.ColumnIndex;
                        iCurRow = e.RowIndex;
                        contextRightMenu.Show(this.grdProcessingtime, this.grdProcessingtime.PointToClient(Cursor.Position));
                    }
                    else
                    {
                        contextRightMenu.Hide();
                    }
                }
                else
                {
                    contextRightMenu.Hide();
                }
            }
            catch (Exception)
            { }

        }

        /// <summary>
        /// Apply all context menu functionality ... filling current cell value to proceeding cells ,
        /// and also update its staus as 1 indicates that cell value is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void applyAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = (DataGridViewRow)grdProcessingtime.Rows[iCurRow];
                int iValue = row.Cells[iCurColumn].Value.ToInt32();
                for (int i = row.Index; i < grdProcessingtime.Rows.Count; i++)
                {
                    grdProcessingtime.Rows[i].Cells[iCurColumn].Value = iValue;
                    grdProcessingtime.Rows[i].Cells["Status"].Value = 1;
                }
            }
            catch (Exception)
            { }

        }

        #endregion

        private void frmProcessingTimeMaster_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void frmProcessingTimeMaster_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnOk.Enabled)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    objclsBLLProcessingTimeMaster = null;
                    objAlerts = null;
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void grdProcessingtime_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch (Exception)
            { }
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }



    }

}
