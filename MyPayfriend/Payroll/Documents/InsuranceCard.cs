﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace MyPayfriend
{
    /// <summary>
    ///  Modified By        : Laxmi
    ///  Modified Date      : 12/8/2013
    ///  Purpose            : Performance tuning
    ///  FormID             : 173
    ///  ErrorCode          : 10500-10550
    ///  Functionalities    : Receipt/ Issue/ Renew/ Attach documents
    ///                     : Insertion into ALerts
    ///                     : Card Issued is - it is a third party or agent name
    ///                     : No more validations
    /// </summary>
    /// 

    public partial class InsuranceCard : Form
    {
        #region Variable declarations

        // Public variable declarations
        public long EmployeeID { get; set; }        // Public variable From Employee form
        public int PintInsuranceCardID = 0;   //Public variable From Navigator


        // Private variable declarations
        bool MblnAddStatus = true;
        string  MstrDocName = string.Empty;
    
        private int MintComboID = 0;                    // To Setting SelectedValue for combo box temporarily
        private int MintTimerInterval;                  // To set timer interval
        private int MintCurrentRecCnt;                  // Current recourd count
        private int MintRecordCnt;                      // Total record count
  
        private bool MblnAddPermission = false;         //To Set Add Permission
        private bool MblnUpdatePermission = false;      //To Set Update Permission
        private bool MblnDeletePermission = false;      //To Set Delete Permission
        private bool MblnPrintEmailPermission = false;  //To Set PrintEmail Permission

        bool mblnAddIssue, mblnAddReceipt,mblnAddRenew, mblnEmail, mblnDelete, mblnUpdate = false;  // Receipt-Issue Permission

        

        private string MsMessageCommon;
        private string MsMessageCaption;

        private bool mblnSearchStatus = false;

        private ArrayList MsMessageArray;   // Error Message display
        private ArrayList MaStatusMessage;

        MessageBoxIcon MsMessageBoxIcon;

        #endregion VariableDeclarations

        clsAlerts objAlerts;
        clsBLLInsuranceCard objBLLInsuranceCard = null;
        clsBLLPermissionSettings objPermission;
      
        ClsLogWriter objClsLogWriter;       // Object of the LogWriter class
        ClsNotification objClsNotification; // Object of the Notification class
        private string strOf = "of ";
        #region Constructor
        public InsuranceCard()
        {
            InitializeComponent();

            objPermission = new clsBLLPermissionSettings();
           
            objBLLInsuranceCard = new clsBLLInsuranceCard();
            objAlerts = new clsAlerts();
            objClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
            objClsNotification = new ClsNotification();

            tmInsuranceCard.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer
            MintTimerInterval = ClsCommonSettings.TimerInterval; // current companyid
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption
                  if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }


        }
        #endregion Constructor
        
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.InsuranceCard, this);

            bnMoreActions.Text = "الإجراءات";
            bnMoreActions.ToolTipText = "الإجراءات";
            documentsToolStripMenuItem.Text = "وثائق";
            receiptToolStripMenuItem.Text = "استلام";
            issueToolStripMenuItem1.Text = "قضية";
            RenewToolStripMenuItem.Text = "جدد";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            strOf = "من ";
        }

        #region Events

        // Form Events

        private void InsuranceCard_Load(object sender, EventArgs e)
        {

            LoadMessage();
            SetPermissions();

            LoadInitials();
           
            cboEmployee.Select();

            if (PintInsuranceCardID > 0) //From navigator document view
            {
                MintCurrentRecCnt = 1;
                MintRecordCnt = 1;
                GetInsuranceCardDetails();
                // Search button disabled
                bSearch.Enabled = false;
                lblStatus.Text = string.Empty;
                tmInsuranceCard.Enabled = true;
            }

            else if (EmployeeID > 0) // From Employee- Load all cards of an employee
            {
                if (MintRecordCnt == 0) // No records
                {
                    MintRecordCnt = 1;
                    bnCountItem.Text = MintRecordCnt.ToString();
                    bnPositionItem.Text = MintCurrentRecCnt.ToString();

                }
                else if (MintRecordCnt > 0)
                    bnMoveFirstItem_Click(sender, e);

                txtCardNumber.Select();
            }
        }
        private void InsuranceCard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnOk.Enabled)
            {
                // Checking the changes are not saved and shows warning to the user
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 8, out MsMessageBoxIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    objAlerts = null;
                    objBLLInsuranceCard = null;
                    objPermission = null;
                    
                    objClsLogWriter = null;
                    objClsNotification = null;

                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void InsuranceCard_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        bnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                 
                    case Keys.Control | Keys.Enter:
                        if (bnAddNewItem.Enabled ) bnAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                       if (bnDeleteItem.Enabled ) bnDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (bnCancel.Enabled ) bnCancel_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (bnMovePreviousItem.Enabled ) bnMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        if (bnMoveNextItem.Enabled ) bnMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        if (bnMoveFirstItem.Enabled ) bnMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        if (bnMoveLastItem.Enabled ) bnMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (bnPrint.Enabled ) bnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (bnEmail.Enabled ) bnEmail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.Alt | Keys.I:
                        if (issueToolStripMenuItem1.Enabled)
                            issueToolStripMenuItem1_Click(sender, new EventArgs());  //issue document
                        break;
                    case Keys.Alt | Keys.E:
                        if (receiptToolStripMenuItem.Enabled)
                            receiptToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                        break;
                    case Keys.Alt | Keys.N:
                        if (RenewToolStripMenuItem.Enabled)
                            issueToolStripMenuItem1_Click(sender, new EventArgs());// renew document
                        break;
                    case Keys.Alt | Keys.D:
                        if (documentsToolStripMenuItem.Enabled)
                            documentsToolStripMenuItem_Click(sender, new EventArgs()); // issue document
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        // Button events
        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            LoadInitials(); 
        }
        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveInsuranceCardDetails()) DoActionsAfterSave();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveInsuranceCardDetails()) DoActionsAfterSave();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveInsuranceCardDetails())
            {
                btnOk.Enabled = false;
                this.Close();
            }
        }
        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            if (txtCardNumber.Tag.ToInt32() > 0)
            {
                int dtDocRequest = clsBLLDocumentMasterNew.GetDocRequestReferenceExists(txtCardNumber.Tag.ToInt32(), (int)DocumentType.Insurance_Card);
                if (dtDocRequest > 0)
                {
                    string Message = "Cannot Delete as Document Request Exists.";
                    MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 13, out MsMessageBoxIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    DeleteInsuranceCard();
                    //objAlerts.UpdateAlerts();
                    LoadInitials();
                    SetAutoCompleteList();
                }
            }
        }
        private void bnCancel_Click(object sender, EventArgs e)
        {
            LoadInitials();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void bnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        private void bnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCardNumber.Tag.ToInt32() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Employee Insurance Card";
                        ObjEmailPopUp.EmailFormType = EmailFormID.InsuranceCardID;
                        ObjEmailPopUp.EmailSource = objBLLInsuranceCard.DisplayInsuranceCardReport(txtCardNumber.Tag.ToInt32());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private void bnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp objHelp = new FrmHelp())
            {
                objHelp.strFormName = "InsuranceCard";
                objHelp.ShowDialog();
            }
        }
        //Button navigation Events
        private void bnMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            GetRecordCount();   // get no of records

            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;          // set current record
                GetInsuranceCardDetails();     // display qualification details

                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 9, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmInsuranceCard.Enabled = true;
            }
        }
        private void bnMovePreviousItem_Click(object sender, EventArgs e)
        {

            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty; 

            GetRecordCount();   // get no of records

            if (MintRecordCnt > 0)
            {
                //BtnSearch_Click(sender, new EventArgs());

                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                    MintCurrentRecCnt = 1;
                else
                {
                    GetInsuranceCardDetails();     // display qualification details

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmInsuranceCard.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 1;
        }
        private void bnMoveNextItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            GetRecordCount();   // get no of records
           

            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                    MintCurrentRecCnt = 1;
                else
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;

                if (MintCurrentRecCnt > MintRecordCnt)
                    MintCurrentRecCnt = MintRecordCnt;
                else
                {
                    GetInsuranceCardDetails();

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 11, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmInsuranceCard.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 0;
        }
        private void bnMoveLastItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            GetRecordCount();

            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    GetInsuranceCardDetails();

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 12, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmInsuranceCard.Enabled = true;

                }
            }
        }

        // Document/Receipt/Issue Button events
        private void receiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocID = txtCardNumber.Tag.ToInt32();
                new frmDocumentReceiptIssue()
                {
                    eOperationType = OperationType.Employee,
                    eDocumentType = DocumentType.Insurance_Card,
                    DocumentID = iDocID,
                    DocumentNumber = txtCardNumber.Text,
                    EmployeeID = cboEmployee.SelectedValue.ToInt32(),
                    ExpiryDate = dtpExpiryDate.Value
                }.ShowDialog();
                SetReceiptOrIssueLabel();
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on receiptToolStripMenuItem_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private void issueToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocID = txtCardNumber.Tag.ToInt32();
                new frmDocumentReceiptIssue()
                {
                    eOperationType = OperationType.Employee,
                    eDocumentType = DocumentType.Insurance_Card,
                    DocumentID = iDocID,
                    DocumentNumber = txtCardNumber.Text,
                    EmployeeID = cboEmployee.SelectedValue.ToInt32(),
                    ExpiryDate = dtpExpiryDate.Value
                }.ShowDialog();
                SetReceiptOrIssueLabel();
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on issueToolStripMenuItem1_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private void documentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int iCardID = txtCardNumber.Tag.ToInt32();
                if (iCardID > 0)
                {
                    using (FrmScanning objScanning = new FrmScanning())
                    {
                        objScanning.MintDocumentTypeid = (int)DocumentType.Insurance_Card;
                        objScanning.MlngReferenceID = cboEmployee.SelectedValue.ToInt32();
                        objScanning.MintOperationTypeID = (int)OperationType.Employee;
                        objScanning.MstrReferenceNo = txtCardNumber.Text;
                        objScanning.PblnEditable = true;
                        objScanning.MintVendorID = cboEmployee.SelectedValue.ToInt32();
                        objScanning.MintDocumentID = iCardID;
                        objScanning.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on documentsToolStripMenuItem_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private void RenewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCardNumber.Tag.ToInt32() > 0)
                {
                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10509, out MsMessageBoxIcon); // Do you wish to renew

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    else //YES Do you wish to add
                    {
                        objBLLInsuranceCard.objDTOInsuranceCard.InsuranceCardID = txtCardNumber.Tag.ToInt32();
                        objBLLInsuranceCard.UpdateInsuranceForRenewal();
                        GetRecordCount();
                        GetInsuranceCardDetails();

                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10508, out MsMessageBoxIcon); // Do you wish to add
                        if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                        AddNewItem();
                        SetEnableDisable();
                        MblnAddStatus = true;
                        txtCardNumber.Text = string.Empty;
                        txtCardNumber.Select();
                    }
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on RenewToolStripMenuItem_Click() " + this.Name + " " + ex.Message.ToString(), 1); }
        }

        // Reference Button events
        private void btnInsuranceCompany_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboInsuranceCompany.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Insurance Company", new int[] { 1, 0, MintTimerInterval }, "InsuranceCompanyID,Description,DescriptionArb", "InsuranceCompanyReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadInsuranceCompany();

                if (objCommon.NewID != 0)
                    cboInsuranceCompany.SelectedValue = objCommon.NewID;
                else
                    cboInsuranceCompany.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on BtnCountry_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCountry_Click" + ex.Message.ToString());
            }
        }
        private void btnCardIssued_Click(object sender, EventArgs e)
        {
            try
            {
                MintComboID = Convert.ToInt32(cboCardIssued.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Insurance Card Issue", new int[] { 1, 0, MintTimerInterval }, "IssuedID,IssuedBy,IssuedByArb", "InsuranceCardIssueReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCardIssued();

                if (objCommon.NewID != 0)
                    cboCardIssued.SelectedValue = objCommon.NewID;
                else
                    cboCardIssued.SelectedValue = MintComboID;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on BtnCountry_Click " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCountry_Click" + ex.Message.ToString());
            }
        }

        //Search button Events
        private void BtnSearch_Click(object sender, EventArgs e)
        {

            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;
            GetRecordCount();
          
            if (MintRecordCnt == 0) // No records
            {
                MessageBox.Show( objClsNotification.GetErrorMessage(MsMessageArray, 40, out MsMessageBoxIcon).Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
            }
            else if (MintRecordCnt > 0)
            {
                AddNewItem();
                bnMoveFirstItem_Click(sender, e);
            }
        }

        // Combo events
        private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }
        private void cboInsuranceCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboInsuranceCompany.DroppedDown = false;
        }
        private void cboCardIssued_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCardIssued.DroppedDown = false;
        }

        // Textbox events
        public void OnInputChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                if (e.KeyCode == Keys.Enter)
                    BtnSearch_Click(sender, null);
            }
        }

        private void tmInsuranceCard_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmInsuranceCard.Enabled = false;
            errInsuranceCard.Clear();
            tmInsuranceCard.Stop();
        }

        #endregion Events

        #region Functions
   
        /// <summary>
        /// Load Messages
        /// </summary>
        /// <param name="intType"></param>
        private void LoadMessage()
        {
            MsMessageArray = new ArrayList();
            MaStatusMessage = new ArrayList();
            MsMessageArray = objClsNotification.FillMessageArray((int)FormID.InsuranceCard, ClsCommonSettings.ProductID);
            MaStatusMessage = objClsNotification.FillStatusMessageArray((int)FormID.InsuranceCard, ClsCommonSettings.ProductID);
        }
        /// <summary>
        /// Load all combobox
        /// </summary>
        private void LoadCombos()
        {
            try
            {
                DataTable dtCombos = null;

                if (EmployeeID > 0)
                {
                    if (ClsCommonSettings.IsArabicView)
                    {
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeID," +
                            "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstNAme", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "WorkStatusID > 5 and EmployeeID=" + EmployeeID + "" });

                    }
                    else
                    {
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeID," +
                            "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstNAme", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "WorkStatusID > 5 and EmployeeID=" + EmployeeID + "" });

                    }

                    cboEmployee.DisplayMember = "FirstNAme";
                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DataSource = dtCombos;

                    cboEmployee.SelectedValue = EmployeeID;
                }
                else
                {
                    if (ClsCommonSettings.IsArabicView)
                    {
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeID," +
                            "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstNAme", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });

                    }
                    else
                    {
                        dtCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeID," +
                            "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstNAme", "" +
                            "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });

                    }

                    cboEmployee.DisplayMember = "FirstNAme";
                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DataSource = dtCombos;

                }

                LoadInsuranceCompany();
                LoadCardIssued();


            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadCombos() " + this.Name + " " + ex.Message.ToString(), 1);
                return;
            }
        }

        private void LoadInsuranceCompany()
        {
            DataTable dtCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                dtCombos = new ClsCommonUtility().FillCombos(new string[] { "InsuranceCompanyID,DescriptionArb as Insurancecompany", "InsuranceCompanyReference", "" });
            }
            else
            {
                dtCombos = new ClsCommonUtility().FillCombos(new string[] { "InsuranceCompanyID,Description as Insurancecompany", "InsuranceCompanyReference", "" });
            }
            cboInsuranceCompany.DisplayMember = "Insurancecompany";
            cboInsuranceCompany.ValueMember = "InsuranceCompanyID";
            cboInsuranceCompany.DataSource = dtCombos;
        }
        private void LoadCardIssued()
        {
            DataTable dtCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                dtCombos = new ClsCommonUtility().FillCombos(new string[] { "IssuedID,IssuedByArb AS IssuedBy", "InsuranceCardIssueReference", "" });
            }
            else
            {
                dtCombos = new ClsCommonUtility().FillCombos(new string[] { "IssuedID,IssuedBy ", "InsuranceCardIssueReference", "" });
            }
            cboCardIssued.DisplayMember = "IssuedBy";
            cboCardIssued.ValueMember = "IssuedID";
            cboCardIssued.DataSource = dtCombos;
        }

        /// <summary>
        /// Permission settings
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.InsuranceCard, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out mblnEmail, out mblnAddRenew, out mblnUpdate, out mblnDelete);
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
                mblnAddIssue = mblnAddReceipt =mblnAddRenew = true;
            }
        }
        /// <summary>
        /// Get total record count depending on EmployeeID/InsuranceCardID/Searchtext
        /// </summary>
        private void GetRecordCount()
        {
            MintRecordCnt = objBLLInsuranceCard.GetRecordCount(EmployeeID , PintInsuranceCardID, txtSearch.Text.Trim());
        }
        private void LoadInitials()
        {
            try
            {
                
                LoadCombos();

                txtSearch.Text = "";
                mblnSearchStatus = false;

                SetAutoCompleteList();
                AddNewItem();


                cboEmployee.SelectedIndex = cboInsuranceCompany.SelectedIndex = cboCardIssued.SelectedIndex = -1;
                txtCardNumber.Clear();
                txtCardNumber.Tag = "0";
                txtPolicyNumber.Clear();
                txtPolicyName.Clear();
                txtRemarks.Clear();

                dtpExpiryDate.Value = System.DateTime.Now.AddDays(1);
                dtpIssueDate.Value = DateTime.Today;

                errInsuranceCard.Clear();

                if (EmployeeID > 0) // From Employee
                {
                    cboEmployee.Text = string.Empty;
                    cboEmployee.SelectedValue = EmployeeID;
                    cboEmployee.Enabled = false;
                    txtCardNumber.Focus();
                }
                else
                {
                    cboEmployee.Enabled = true;
                    cboEmployee.Focus();
                }
                SetEnableDisable();
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on LoadInitials() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private bool AddNewItem()
        {
            try
            {
                //txtSearch.Text = string.Empty;

                MblnAddStatus = (PintInsuranceCardID > 0 ? false : true);
                lblRenewed.Visible = false;
                cboEmployee.Enabled = EmployeeID > 0 ? false : true;
                cboCardIssued.Enabled = cboInsuranceCompany.Enabled = btnCardIssued.Enabled = btnInsuranceCompany.Enabled = true;
                txtCardNumber.Enabled = txtPolicyName.Enabled = txtPolicyNumber.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = true;
                //bnDeleteItem.ToolTipText = "Remove";

              
                GetRecordCount();

                bnCountItem.Text = strOf + Convert.ToString(MintRecordCnt + 1) + "";
                bnPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
                MintCurrentRecCnt = MintRecordCnt + 1;

                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 655, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmInsuranceCard.Enabled = true;
                return true;
            }
            catch (Exception Ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());
                return false;
            }
        }   
        /// <summary>
        /// Fill Parametersbefor save
        /// </summary>
        private void FillParameters()
        {
            
            objBLLInsuranceCard.objDTOInsuranceCard.InsuranceCardID = txtCardNumber.Tag.ToInt32();
            objBLLInsuranceCard.objDTOInsuranceCard.EmployeeID = cboEmployee.SelectedValue.ToInt32();
            objBLLInsuranceCard.objDTOInsuranceCard.CardNumber = txtCardNumber.Text.Trim();
            objBLLInsuranceCard.objDTOInsuranceCard.PolicyNumber = txtPolicyNumber.Text.Trim();
            objBLLInsuranceCard.objDTOInsuranceCard.PolicyName = txtPolicyName.Text.Trim();
            objBLLInsuranceCard.objDTOInsuranceCard.InsuranceCompanyID = cboInsuranceCompany.SelectedValue.ToInt32();
            objBLLInsuranceCard.objDTOInsuranceCard.IssueDate = dtpIssueDate.Value.Date;
            objBLLInsuranceCard.objDTOInsuranceCard.ExpiryDate = dtpExpiryDate.Value.Date;
            objBLLInsuranceCard.objDTOInsuranceCard.Remarks = txtRemarks.Text.Trim();
            objBLLInsuranceCard.objDTOInsuranceCard.IssueID = cboCardIssued.SelectedValue.ToInt32();
            objBLLInsuranceCard.objDTOInsuranceCard.IsRenewed = lblRenewed.Visible ? true :false ;
        }
        /// <summary>
        /// Load report
        /// </summary>
        private void LoadReport()
        {
            try
            {
                if (txtCardNumber.Tag.ToInt32() > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();

                    ObjViewer.intCompanyID = clsBLLCommonUtility.GetCompanyID(cboEmployee.SelectedValue.ToInt32());
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = txtCardNumber.Tag.ToInt32();
                    ObjViewer.PiFormID = (int)FormID.InsuranceCard;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        /// <summary>
        /// Delete Insurance card
        /// </summary>
        private void DeleteInsuranceCard()
        {
            try
            {
                objBLLInsuranceCard.objDTOInsuranceCard.InsuranceCardID = txtCardNumber.Tag.ToInt32();
                objBLLInsuranceCard.DeleteInsuranceCard();
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 4, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmInsuranceCard.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
               
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on DeleteInsuranceCard() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        /// <summary>
        /// Get Insurance card details
        /// </summary>
        private void GetInsuranceCardDetails()
        {
            try
            {
                DataTable DtInsuranceCard = objBLLInsuranceCard.GetInsuranceCardDetails(MintCurrentRecCnt, EmployeeID, PintInsuranceCardID, txtSearch.Text.Trim());//get data from BLL
                if (DtInsuranceCard.Rows.Count > 0)
                {
                    objBLLInsuranceCard.objDTOInsuranceCard.EmployeeID = Convert.ToInt32(DtInsuranceCard.Rows[0]["EmployeeID"]);
                    FillEmployee();
                    cboEmployee.SelectedValue = DtInsuranceCard.Rows[0]["EmployeeID"];
                    txtCardNumber.Text = DtInsuranceCard.Rows[0]["CardNumber"].ToString();
                    txtCardNumber.Tag = DtInsuranceCard.Rows[0]["InsuranceCardId"].ToInt32();
                    txtPolicyNumber.Text = DtInsuranceCard.Rows[0]["PolicyNumber"].ToString();
                    txtPolicyName.Text = DtInsuranceCard.Rows[0]["PolicyName"].ToString();
                    cboInsuranceCompany.SelectedValue = DtInsuranceCard.Rows[0]["InsuranceCompanyID"];
                    dtpIssueDate.Value = DtInsuranceCard.Rows[0]["IssueDate"].ToDateTime();
                    dtpExpiryDate.Value = DtInsuranceCard.Rows[0]["ExpiryDate"].ToDateTime();
                    txtRemarks.Text = DtInsuranceCard.Rows[0]["Remarks"].ToString();
                    cboCardIssued.SelectedValue = DtInsuranceCard.Rows[0]["IssuedID"];

                    if (Convert.ToBoolean(DtInsuranceCard.Rows[0]["IsRenewed"]))
                    {
                        lblRenewed.Visible = true;
                        cboEmployee.Enabled = cboCardIssued.Enabled = cboInsuranceCompany.Enabled = btnCardIssued.Enabled = btnInsuranceCompany.Enabled = false;
                        txtCardNumber.Enabled = txtPolicyName.Enabled = txtPolicyNumber.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = false;
                    }
                    else
                    {
                        lblRenewed.Visible = false;
                        cboEmployee.Enabled = !(EmployeeID > 0);
                        cboCardIssued.Enabled = cboInsuranceCompany.Enabled = btnCardIssued.Enabled = btnInsuranceCompany.Enabled = true;
                        txtCardNumber.Enabled = txtPolicyName.Enabled = txtPolicyNumber.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = true;
                    }


                    MblnAddStatus = false;

                    MstrDocName = txtCardNumber.Text.Replace("'", "").Trim();

                    bnPositionItem.Text = MintCurrentRecCnt.ToString();
                    bnCountItem.Text = strOf + Convert.ToString(MintRecordCnt) + "";
                    bnAddNewItem.Enabled = MblnAddPermission;

                    SetEnableDisable();
                    SetReceiptOrIssueLabel();
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on GetInsuranceCardDetails() " + this.Name + " " + ex.Message.ToString(), 1); }
           
        }
        /// <summary>
        /// button enable/disable depends on New mode or update mode
        /// </summary>
        private void SetEnableDisable()
        {
            btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;
            bnPrint.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission );
            bnEmail.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            bnMoreActions.Enabled = (MblnAddStatus == true ? false : true);

            bnAddNewItem.Enabled = (MblnAddStatus == true ? false : (PintInsuranceCardID > 0 ? false : MblnAddPermission ));

            if (MblnAddStatus) bnDeleteItem.Enabled = false;
            else
            {
                bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (PintInsuranceCardID > 0 ? false : MblnDeletePermission));
                //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Insurance card could not be removed." : "Remove");
            }
            bnCancel.Enabled = (MblnAddStatus == true ? true : false);

            if (PintInsuranceCardID <= 0)
            {
                bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = true;
                int iCurrentRec = Convert.ToInt32(bnPositionItem.Text); // Current position of the record

                if (iCurrentRec >= MintRecordCnt)  // If the current is last record, disables move next and last button
                {
                    bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
                }
                if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    bnMoveFirstItem.Enabled = bnMovePreviousItem.Enabled = false;
                }
            }
            else      
                bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = false;
        }
        /// <summary>
        /// Save Insurance card
        /// </summary>
        private bool SaveInsuranceCardDetails()
        {
            cboEmployee.Focus();

            bool blnReturnValue = false;

            try
            {
                if (FormValidation())
                {
                    if (MblnAddStatus)
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 1, out MsMessageBoxIcon);
                    else
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 3, out MsMessageBoxIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)

                        return false;

                    FillParameters();

                    txtCardNumber.Tag = objBLLInsuranceCard.SaveInsuranceDetails(MblnAddStatus).ToInt32();

                    if (txtCardNumber.Tag.ToInt32() > 0)
                    {
                        if (PintInsuranceCardID > 0)
                        {
                            PintInsuranceCardID = txtCardNumber.Tag.ToInt32();
                            MintCurrentRecCnt = 1;
                        }
                        clsAlerts objclsAlerts = new clsAlerts();
                        objclsAlerts.AlertMessage((int)DocumentType.Insurance_Card, "InsuranceCard", "InsuranceCardNumber", txtCardNumber.Tag.ToInt32(), txtCardNumber.Text, dtpExpiryDate.Value, "Emp", cboEmployee.SelectedValue.ToInt32(),cboEmployee.Text.Trim(),false,0 );
                        
                        string strMsg = "";
                        if (MblnAddStatus)//insert
                        {
                            strMsg = objClsNotification.GetErrorMessage(MsMessageArray, 2, out MsMessageBoxIcon);
                        }
                        else
                        {
                            strMsg = objClsNotification.GetErrorMessage(MsMessageArray, 21, out MsMessageBoxIcon);
                        }
                        MessageBox.Show(strMsg.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = strMsg.Remove(0, strMsg.IndexOf("#") + 1);
                        tmInsuranceCard.Enabled = true;
                        blnReturnValue = true;
                    }
                }

                return blnReturnValue;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on SaveInsuranceCardDetails() " + this.Name + " " + ex.Message.ToString(), 1);
                return blnReturnValue;
            }
        }
        /// <summary>
        /// Do actions after save
        /// Enable and disable form controls
        /// </summary>
        private void DoActionsAfterSave()
        {
            SetAutoCompleteList();
            MblnAddStatus = false;
            btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = bnCancel.Enabled=false; 
            documentsToolStripMenuItem.Enabled =  MblnAddPermission;
            bnAddNewItem.Enabled = (PintInsuranceCardID > 0 ? false : MblnAddPermission) ;
            bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            if (MblnAddStatus) bnDeleteItem.Enabled = false;
            else
            {
                bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (PintInsuranceCardID > 0 ? false : MblnDeletePermission));
                //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Insurance card could not be removed." : "Remove");
            }
            SetReceiptOrIssueLabel();
        }
        /// <summary>
        /// Validation of controls
        /// </summary>        
        private bool FormValidation()
        {
            errInsuranceCard.Clear();
            bool bReturnValue = true;
            Control cControl = new Control();

            if (Convert.ToInt32(cboEmployee.SelectedValue) == 0) // Employee Name
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10500, out MsMessageBoxIcon);
                cControl = cboEmployee;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtCardNumber.Text.Trim()))// Insurance Card Number
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10501, out MsMessageBoxIcon);
                cControl = txtCardNumber;
                bReturnValue = false;
            }
            else if (Convert.ToInt32(cboInsuranceCompany.SelectedValue) == 0) // Insurance Company
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10502, out MsMessageBoxIcon);
                cControl = cboInsuranceCompany;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtPolicyNumber.Text.Trim())) // Policy Number
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10503, out MsMessageBoxIcon);
                cControl = txtPolicyNumber;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtPolicyName.Text.Trim())) // Policy Name
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10504, out MsMessageBoxIcon);
                cControl = txtPolicyName;
                bReturnValue = false;
            }
            else if (dtpExpiryDate.Value.Date <= dtpIssueDate.Value.Date)
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10505, out MsMessageBoxIcon);
                cControl = dtpExpiryDate;
                bReturnValue = false;
            }
            else if (dtpIssueDate.Value > ClsCommonSettings.GetServerDate())
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10506, out MsMessageBoxIcon);
                cControl = dtpIssueDate;
                bReturnValue = false;
            }
            // Card Number Duplication 
            //else 
            //{
            //    if (objBLLInsuranceCard.CheckDuplication(( MblnAddStatus && txtCardNumber.Tag.ToInt32() >0 ? 0 : txtCardNumber.Tag.ToInt32()), txtCardNumber.Text.ToString()))
            //    {
            //        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10507, out MsMessageBoxIcon);
            //        cControl = txtCardNumber;
            //        bReturnValue = false;
            //    }
            //}
            if (bReturnValue == false)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (cControl != null)
                {
                    errInsuranceCard.SetError(cControl, MsMessageCommon);
                    cControl.Focus();
                }
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmInsuranceCard.Enabled = true;
            }
            return bReturnValue;
        }      
        /// <summary>
        /// Fill employee when navigate buttons
        /// </summary>
        private void FillEmployee()
        {
            DataTable datCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstName ," +
                    "EmployeeID as EmployeeId", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "(WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")) OR EmployeeID = " + objBLLInsuranceCard.objDTOInsuranceCard.EmployeeID });

            }
            else
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS FirstName ," +
                    "EmployeeID as EmployeeId", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "(WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")) OR EmployeeID = " + objBLLInsuranceCard.objDTOInsuranceCard.EmployeeID });

            }

            cboEmployee.DisplayMember = "FirstName";
            cboEmployee.ValueMember = "EmployeeId";
            cboEmployee.DataSource = datCombos;
        }          
        private void ChangeStatus()
        {
            errInsuranceCard.Clear();
            if (MblnAddStatus) // add mode
            {
                btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = MblnAddPermission;
            }
            else  //edit mode
            {
                btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = MblnUpdatePermission;
            }
        }
        /// <summary>
        /// Set Receipt /Issue button style(enable/disable)
        /// </summary>
        private void SetReceiptOrIssueLabel()
        {
            bnMoreActions.Enabled = true;

            RenewToolStripMenuItem.Enabled = (clsPassportBLL.IsEmployeeInService(cboEmployee.SelectedValue.ToInt64()) ? (lblRenewed.Visible ? false : mblnAddRenew) : false);
            
            issueToolStripMenuItem1.Enabled = receiptToolStripMenuItem.Enabled = false;
            bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Employee, (int)DocumentType.Insurance_Card, txtCardNumber.Tag.ToInt32());

            if (IsReceipt)
                issueToolStripMenuItem1.Enabled = mblnAddIssue;
            else
                receiptToolStripMenuItem.Enabled = mblnAddReceipt;
        }
        /// <summary>
        /// Autocomplete with Employeename/Employeecode and CardNumber
        /// </summary>
        /// 
        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsBLLInsuranceCard.GetAutoCompleteList(txtSearch.Text.Trim(),EmployeeID , "CardNumber", "EmployeeInsuranceCard");
            this.txtSearch.AutoCompleteCustomSource = clsBLLInsuranceCard.ConvertToAutoCompleteCollection(dt);
           
        }
        #endregion Functions

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }

       
    }
}
