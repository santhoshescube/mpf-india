﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{

    /// <summary>
    ///  Author        : Thasni
    ///  Creation Date : 12 Apr 2011
    ///  
    ///  Modified By        : Laxmi
    ///  Modified Date      : 26/8/2013
    ///  Purpose            : Performance tuning
    ///  FormID             : 168
    ///  ErrorCode          : 501 -
    ///  Functionalities    : Receipt/ Issue/ Renew/ Attach documents
    ///                     : Insertion into ALerts
    /// </summary>

    public partial class frmDrivingLicense : Form
    {
        #region variable declarations
        DataTable datMessages;

        //public variables
        public long EmployeeID { get; set; } // public variable From Employee
        public int PiDrivingLicense = 0;   // Public variable from navigator

        int? LicenseID = null;
        int intRowIndex = 0;                // Current recourd count
        int intTotalRecords = 0;            // Total record count

        Mode mode;  //------------------------------------------check this also , we can handle the operations using blnNewMode variable, this both are for same purpose
        private bool formLoading = false;

        ClsNotificationNew MobjClsNotification = null;
        clsDrivingLicenseBLL objDrivingLicenseBLL = null;
        clsReferneceTableBLL objReferenceTable = null;

        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool mblnSearchStatus = false;
        private string strOf = "of ";
        bool mblnAddIssue, mblnAddReceipt, mblnAddRenew, mblnEmail, mblnDelete, mblnUpdate = false;

        #endregion

        #region Constructor

        public frmDrivingLicense()
        {
            InitializeComponent();
            this.objDrivingLicenseBLL = new clsDrivingLicenseBLL();
            this.objDrivingLicenseBLL.DrivingLicense = new clsDrivingLicense();
            this.MobjClsNotification = new ClsNotificationNew();
            this.objReferenceTable = new clsReferneceTableBLL();

            Timer.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }
        #endregion
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.DrivingLicense, this);

            ToolStripDropDownBtnMore.Text = "الإجراءات";
            ToolStripDropDownBtnMore.ToolTipText = "الإجراءات";
            EmployeeLicenseDetailsBindingNavigatorSaveItem.ToolTipText = "حفظ البيانات";
            mnuItemAttachDocuments.Text = "وثائق";
            receiptToolStripMenuItem.Text = "استلام";
            issueToolStripMenuItem.Text = "قضية";
            RenewToolStripMenuItem.Text = "جدد";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            strOf = "من ";
            GrpMain.Text = " تفاصيل الترخيص";
        }
        #region Events
        // Form Events
        private void frmDrivingLicense_Load(object sender, EventArgs e)
        {
            try
            {

                this.formLoading = true;
                LoadMessage();
                LoadCombos();
                SetPermissions();
                LoadInitials();

                if (PiDrivingLicense > 0) // From Navigator
                {
                    objDrivingLicenseBLL.DrivingLicense.LicenseID = PiDrivingLicense;
                    intTotalRecords = 1;
                    intRowIndex = 1;
                    this.BindNavigator();
                    SetIssueReceiptButton();
                    this.txtSearch.Enabled = false;
                    lblStatus.Text = string.Empty;
                    Timer.Enabled = true;
                }
                if (this.EmployeeID != 0)
                {
                    if (intTotalRecords == 0) // No records
                    {
                        intTotalRecords = 1;
                        BindingNavigatorCountItem.Text = intTotalRecords.ToString();
                        BindingNavigatorPositionItem.Text = intRowIndex.ToString();
                    }
                    else
                        BindingNavigatorMoveFirstItem_Click(sender, e);
                }
                this.formLoading = false;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }

        }
        private void frmDrivingLicense_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnOK.Enabled)
            {
                if (MessageBox.Show(MobjClsNotification.GetErrorMessage(datMessages, 8).Replace("#", "").Trim().Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                      MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    objDrivingLicenseBLL = null;
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void frmDrivingLicense_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        BtnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;

                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled) BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled) BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (btnClear.Enabled) btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (BindingNavigatorMovePreviousItem.Enabled) BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        if (BindingNavigatorMoveNextItem.Enabled) BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        if (BindingNavigatorMoveFirstItem.Enabled) BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveLastItem.Enabled) BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (BtnPrint.Enabled) BtnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (BtnFax.Enabled) BtnFax_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.Alt | Keys.I:
                        if (issueToolStripMenuItem.Enabled)
                            receiptToolStripMenuItem_Click(sender, new EventArgs());  //issue document
                        break;
                    case Keys.Alt | Keys.E:
                        if (receiptToolStripMenuItem.Enabled)
                            receiptToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                        break;
                    case Keys.Alt | Keys.N:
                        if (RenewToolStripMenuItem.Enabled)
                            RenewToolStripMenuItem_Click(sender, new EventArgs());// renew document
                        break;
                    case Keys.Alt | Keys.D:
                        if (mnuItemAttachDocuments.Enabled)
                            mnuItemAttachDocuments_Click(sender, new EventArgs()); // Document
                        break;
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }
        /// <summary>
        /// Do actions after save
        /// Enable and disable form controls
        /// </summary>
        private void DoActionsAfterSave()
        {
            SetAutoCompleteList();

            this.mode = Mode.Update;

            btnOK.Enabled = btnSave.Enabled = EmployeeLicenseDetailsBindingNavigatorSaveItem.Enabled = btnClear.Enabled = false;
            mnuItemAttachDocuments.Enabled = MblnAddPermission;
            BindingNavigatorAddNewItem.Enabled = (PiDrivingLicense > 0 ? false : MblnAddPermission);
            BtnPrint.Enabled = BtnFax.Enabled = MblnPrintEmailPermission;

            BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (PiDrivingLicense > 0 ? false : MblnDeletePermission));
            //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Driving license could not be removed." : "Remove");

            SetIssueReceiptButton();
        }

        private void EmployeeLicenseDetailsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (this.SaveHandler()) DoActionsAfterSave();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.SaveHandler()) DoActionsAfterSave();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SaveHandler())
            {
                btnOK.Enabled = false;
                this.Close();
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            LoadInitials();
        }
        private void LoadInitials()
        {
            txtSearch.Text = "";
            mblnSearchStatus = false;

            SetAutoCompleteList();

            BindNavigator();
            AddNewHandler();
            LoadCombos();
            this.txtLicenseName.Text = this.txtLicenseNumber.Text = this.txtRemarks.Text = string.Empty;
            this.cboEmployee.SelectedIndex = this.cboLicenseType.SelectedIndex = this.cboCountry.SelectedIndex = -1;
            this.dtpIssueDate.Value = DateTime.Today;
            dtpExpiryDate.Value = System.DateTime.Now.AddDays(1);

            errorDrivingLicense.Clear();

            if (EmployeeID > 0) // From Employee
            {
                cboEmployee.Text = string.Empty;
                cboEmployee.SelectedValue = EmployeeID;
                cboEmployee.Enabled = false;
                txtLicenseNumber.Focus();
            }
            else
            {
                cboEmployee.Enabled = true;
                cboEmployee.Focus();
            }
            SetEnableDisable();
        }
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            int dtDocRequest = clsBLLDocumentMasterNew.GetDocRequestReferenceExists(this.LicenseID.ToInt32(), (int)DocumentType.Driving_License);
            if (dtDocRequest > 0)
            {
                string Message = "Cannot Delete as Document Request Exists.";
                MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                string Message = MobjClsNotification.GetErrorMessage(datMessages, 13).Replace("@", "\n");
                DialogResult confirmation = MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmation == DialogResult.Yes)
                {
                    this.DeleteHandler();
                    LoadInitials();
                    SetAutoCompleteList();
                }
            }
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            LoadInitials();
        }
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (objDrivingLicenseBLL.DrivingLicense.LicenseID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();

                    ObjViewer.intCompanyID = clsBLLCommonUtility.GetCompanyID(cboEmployee.SelectedValue.ToInt32());
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = this.objDrivingLicenseBLL.DrivingLicense.LicenseID.ToInt32();
                    ObjViewer.PiFormID = (int)FormID.DrivingLicense;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }
        private void BtnFax_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Driving License Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.DrivingLicense;
                    ObjEmailPopUp.EmailSource = objDrivingLicenseBLL.GetEmployeeLicenseDetails();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }
        private void BtnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "DrivingLicense"; Help.ShowDialog(); }
        }
        // Navigation button events
        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            this.intRowIndex = 1;
            this.BindNavigator();
            SetIssueReceiptButton();
            this.AssingStatuText((int)CommonMessages.FirstRecord);
        }
        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            this.intRowIndex -= 1;
            this.BindNavigator();
            SetIssueReceiptButton();
            this.AssingStatuText((int)CommonMessages.PreviousRecord);
        }
        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;


            if (this.mode != Mode.Insert)
            {
                this.intRowIndex += 1;
                this.BindNavigator();
                SetIssueReceiptButton();
            }
            this.AssingStatuText((int)CommonMessages.NextRecord);
        }
        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            this.intRowIndex = this.intTotalRecords;
            this.BindNavigator();
            SetIssueReceiptButton();
            this.AssingStatuText((int)CommonMessages.LastRecord);
        }

        //Reference button Events
        private void BtnLicencetype_Click(object sender, EventArgs e)
        {
            int intLicenseID = cboLicenseType.SelectedValue.ToInt32();

            using (FrmCommonRef objfrmCommon = new FrmCommonRef("License Type", new int[] { 1, 0 }, "LicenseTypeID,LicenseType,LicenseTypeArb", "LicenseTypeReference", ""))
            {
                objfrmCommon.ShowDialog();
                //this.BindReference(ReferenceTables.LicenseType, this.cboLicenseType, "LicenseType", "LicenseTypeID");

                DataTable datCombos2;
                if (ClsCommonSettings.IsArabicView)
                {
                    datCombos2 = objDrivingLicenseBLL.FillCombos(new string[] { "LicenseTypeID, LicenseTypeArb AS LicenseType ", "LicenseTypeReference", "" });
                }
                else
                {
                    datCombos2 = objDrivingLicenseBLL.FillCombos(new string[] { "LicenseTypeID,LicenseType", "LicenseTypeReference", "" });
                }

                cboLicenseType.DisplayMember = "LicenseType";
                cboLicenseType.ValueMember = "LicenseTypeID";
                cboLicenseType.DataSource = datCombos2;

                if (intLicenseID == 0)
                    cboLicenseType.SelectedValue = objfrmCommon.NewID;
                else if (objfrmCommon.NewID != 0)
                    cboLicenseType.SelectedValue = objfrmCommon.NewID;
                else
                    cboLicenseType.SelectedValue = intLicenseID;
            }

        }
        private void BtnCountry_Click(object sender, EventArgs e)
        {
            int intCountryID = cboCountry.SelectedValue.ToInt32();

            using (FrmCommonRef objfrmCommon = new FrmCommonRef("Country", new int[] { 1, 0 }, "CountryID,CountryName,CountryNameArb", "CountryReference", ""))
            {
                objfrmCommon.ShowDialog();
                //this.BindReference(ReferenceTables.Country, this.cboCountry, "CountryName", "CountryID");
                DataTable datCombos3;

                if (ClsCommonSettings.IsArabicView)
                {
                    datCombos3 = objDrivingLicenseBLL.FillCombos(new string[] { "CountryID,CountryNameArb as Description", "CountryReference", "" });

                }
                else
                {
                    datCombos3 = objDrivingLicenseBLL.FillCombos(new string[] { "CountryID,CountryName as Description", "CountryReference", "" });
                }
                cboCountry.DisplayMember = "Description";
                cboCountry.ValueMember = "CountryID";
                cboCountry.DataSource = datCombos3;

                if (intCountryID == 0)
                    cboCountry.SelectedValue = objfrmCommon.NewID;
                else if (objfrmCommon.NewID != 0)
                    cboCountry.SelectedValue = objfrmCommon.NewID;
                else
                    cboCountry.SelectedValue = intCountryID;
            }
        }

        //Combobox events

        private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }
        private void cboLicenseType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboLicenseType.DroppedDown = false;
        }
        private void cboCountry_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCountry.DroppedDown = false;
        }

        private void mnuItemAttachDocuments_Click(object sender, EventArgs e)
        {
            try
            {
                if (objDrivingLicenseBLL.DrivingLicense.LicenseID > 0)
                {
                    using (FrmScanning objScanning = new FrmScanning())
                    {
                        objScanning.MintDocumentTypeid = (int)DocumentType.Driving_License;
                        objScanning.MlngReferenceID = Convert.ToInt32(cboEmployee.SelectedValue);
                        objScanning.MintOperationTypeID = (int)OperationType.Employee;
                        objScanning.MstrReferenceNo = txtLicenseNumber.Text;
                        //= MobjClsBLLDocumentMaster.objClsDTODocumentMaster.intVendorID;
                        objScanning.PblnEditable = true;
                        objScanning.MintVendorID = Convert.ToInt32(cboEmployee.SelectedValue);
                        objScanning.MintDocumentID = Convert.ToInt32(objDrivingLicenseBLL.DrivingLicense.LicenseID);
                        objScanning.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }
        private void receiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new frmDocumentReceiptIssue()
                {
                    eOperationType = OperationType.Employee,
                    eDocumentType = DocumentType.Driving_License,
                    DocumentID = Convert.ToInt32(objDrivingLicenseBLL.DrivingLicense.LicenseID),
                    DocumentNumber = txtLicenseNumber.Text,
                    EmployeeID = cboEmployee.SelectedValue.ToInt32(),
                    ExpiryDate = dtpExpiryDate.Value
                }.ShowDialog();

                SetIssueReceiptButton();
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }
        private void RenewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = MobjClsNotification.GetErrorMessage(datMessages, (mode == Mode.Insert) ? 1 : 3);

                if (objDrivingLicenseBLL.DrivingLicense.LicenseID.ToInt32() > 0)
                {
                    strMessage = MobjClsNotification.GetErrorMessage(datMessages, 508); // Do you wish to renew

                    if (MessageBox.Show(strMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    else //YES Do you wish to add
                    {
                        objDrivingLicenseBLL.UpdateLicenseForRenewal();
                        this.LicenseID = objDrivingLicenseBLL.DrivingLicense.LicenseID.ToInt32();

                        this.BindNavigator();

                        strMessage = MobjClsNotification.GetErrorMessage(datMessages, 509); // Do you wish to add
                        if (MessageBox.Show(strMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                        AddNewHandler();
                        SetEnableDisable();
                        this.mode = Mode.Insert;
                        txtLicenseNumber.Text = string.Empty;
                        txtLicenseNumber.Select();
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                if (e.KeyCode == Keys.Enter)
                    btnSearch_Click(sender, null);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;

            GetRecordCount();

            if (intTotalRecords == 0) // No records
            {
                MessageBox.Show(MobjClsNotification.GetErrorMessage(datMessages, 40).Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
                // BindNavigator();
            }
            else if (intTotalRecords > 0)
            {
                AddNewHandler();
                BindingNavigatorMoveFirstItem_Click(sender, e);
            }
        }
        #endregion

        #region Functions

        /// <summary>
        /// Load drivinglicense messages from notificationmaster
        /// </summary>
        private void LoadMessage()
        {
            MobjClsNotification = new ClsNotificationNew();
            datMessages = MobjClsNotification.FillMessageArray((int)FormID.DrivingLicense, 4);
        }
        /// <summary>
        /// Showmessage based on Messagecode
        /// </summary>
        /// <param name="MessageCode"></param>
        private void ShowMessage(int MessageCode)
        {
            MessageBox.Show(MobjClsNotification.GetErrorMessage(datMessages, MessageCode), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        /// <summary>
        /// Dispaly message in Messagebox
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="msgIcon"></param>
        private void ShowMessage(string Message, MessageBoxIcon msgIcon)
        {
            MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, msgIcon);
        }
        /// <summary>
        /// show message in errorprovider
        /// </summary>
        /// <param name="MessageCode"></param>
        /// <param name="dt"></param>
        private void ShowMessage(int MessageCode, Control dt)
        {
            string strMessage = MobjClsNotification.GetErrorMessage(datMessages, MessageCode);
            MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            errorDrivingLicense.SetError(dt, strMessage);
            dt.Focus();
        }
        /// <summary>
        /// enable textbox and button in new mode
        /// </summary>
        private void AddNewHandler()
        {

            lblRenewed.Visible = false;
            cboEmployee.Enabled = !(EmployeeID > 0);
            cboLicenseType.Enabled = cboCountry.Enabled = BtnLicencetype.Enabled = BtnCountry.Enabled = true;
            txtLicenseNumber.Enabled = txtLicenseName.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = true;

            this.IncrementTotalCount();

            this.mode = PiDrivingLicense > 0 ? Mode.Update : Mode.Insert;

            BindingNavigatorAddNewItem.Enabled = BindingNavigatorDeleteItem.Enabled = BtnPrint.Enabled = false;

            this.AssingStatuText((int)CommonMessages.NewRecord);

        }
        /// <summary>
        /// DElete driving license and alert
        /// </summary>
        private void DeleteHandler()
        {
            try
            {
                // Set ID of the vehicle to be deleted
                this.objDrivingLicenseBLL.DrivingLicense.LicenseID = this.LicenseID;

                // Invoke delete method and get delete status.

                if (this.objDrivingLicenseBLL.DeleteDrivingLicense())
                {
                    string strMessage = MobjClsNotification.GetErrorMessage(datMessages, 4);
                    this.AssingStatuText((int)CommonMessages.Deleted);
                    MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }

        }
        private void DisableEnableControls_IssueReceipt(bool status)
        {
            cboEmployee.Enabled = status;
        }

        private bool SaveHandler()
        {
            bool success = false;
            string strMessage = string.Empty;
            try
            {
                // Insert vehicle if validation succeeded
                if (this.VerifyInputs())
                {
                    strMessage = MobjClsNotification.GetErrorMessage(datMessages, (mode == Mode.Insert) ? 1 : 3);
                    DialogResult confirmation = MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (confirmation == DialogResult.Yes)
                    {

                        // string strMessage = string.Empty;
                        Int32 intLicenseID = objDrivingLicenseBLL.InsertDrivingLicense(mode == Mode.Insert ? true : false);
                        if (intLicenseID > 0)
                        {
                            objDrivingLicenseBLL.DrivingLicense.LicenseID = this.LicenseID = intLicenseID;
                            if (PiDrivingLicense > 0)
                            {
                                PiDrivingLicense = intLicenseID;
                                intRowIndex = 1;
                            }
                            clsAlerts objclsAlerts = new clsAlerts();
                            success = true;
                            objclsAlerts.AlertMessage((int)DocumentType.Driving_License, "Driving License", "LicenseNumber", intLicenseID, txtLicenseNumber.Text, dtpExpiryDate.Value, "Emp", cboEmployee.SelectedValue.ToInt32(), cboEmployee.Text, false,0);
                        }

                        if (mode == Mode.Insert)
                            strMessage = "Driving license has been inserted successfully.";
                        else
                            strMessage = "Driving license has been updated successfully.";

                        if (success)
                        {
                            ShowMessage(strMessage, MessageBoxIcon.Information);
                            this.AssingStatuText((int)(mode == Mode.Insert ? CommonMessages.Saved : CommonMessages.Updated));
                        }
                        else
                            MessageBox.Show(strMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, (success) ? MessageBoxIcon.Information : MessageBoxIcon.Exclamation);
                    }

                }
                return success;

            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
                return success; ;
            }
        }
        /// <summary>
        /// Fill combos employee filling based on worksttaus
        /// </summary>
        private void LoadCombos()
        {
            try
            {
                if (EmployeeID > 0)
                {
                    DataTable datCombos;
                    if (ClsCommonSettings.IsArabicView)
                    {
                        datCombos = objDrivingLicenseBLL.FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                            "EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "WorkStatusID > 5 AND EmployeeID = " + EmployeeID });

                    }
                    else
                    {
                        datCombos = objDrivingLicenseBLL.FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                            "EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                            "WorkStatusID > 5 AND EmployeeID = " + EmployeeID });

                    }
                    cboEmployee.DisplayMember = "EmployeeName";
                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DataSource = datCombos;

                    //using (DataSet ds = objDrivingLicenseBLL.GetReferenceTableData())
                    //{
                    //    if (ds.Tables.Count > 0)
                    //    {
                    //        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                    //            this.BindComboBox(cboLicenseType, "LicenseType", "LicenseTypeID", ds.Tables[1]);

                    //        if (ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                    //            this.BindComboBox(cboCountry, "Description", "CountryID", ds.Tables[2]);

                    //    }
                    //}
                    DataTable datCombos2;
                    DataTable datCombos3;

                    if (ClsCommonSettings.IsArabicView)
                    {
                        datCombos2 = objDrivingLicenseBLL.FillCombos(new string[] { "LicenseTypeID, LicenseTypeArb AS LicenseType ", "LicenseTypeReference", "" });

                        datCombos3 = objDrivingLicenseBLL.FillCombos(new string[] { "CountryID,CountryNameArb as Description", "CountryReference", "" });

                    }
                    else
                    {
                        datCombos2 = objDrivingLicenseBLL.FillCombos(new string[] { "LicenseTypeID,LicenseType", "LicenseTypeReference", "" });

                        datCombos3 = objDrivingLicenseBLL.FillCombos(new string[] { "CountryID,CountryName as Description", "CountryReference", "" });
                    }

                    cboLicenseType.DisplayMember = "LicenseType";
                    cboLicenseType.ValueMember = "LicenseTypeID";
                    cboLicenseType.DataSource = datCombos2;

                    cboCountry.DisplayMember = "Description";
                    cboCountry.ValueMember = "CountryID";
                    cboCountry.DataSource = datCombos3;
                }
                else
                {
                    using (DataSet ds = objDrivingLicenseBLL.GetReferenceTableData())
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                this.BindComboBox(cboEmployee, "EmployeeName", "EmployeeID", ds.Tables[0]);

                            //if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                            //    this.BindComboBox(cboLicenseType, "LicenseType", "LicenseTypeID", ds.Tables[1]);

                            //if (ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                            //    this.BindComboBox(cboCountry, "Description", "CountryID", ds.Tables[2]);

                            DataTable datCombos2;
                            DataTable datCombos3;

                            if (ClsCommonSettings.IsArabicView)
                            {
                                datCombos2 = objDrivingLicenseBLL.FillCombos(new string[] { "LicenseTypeID, LicenseTypeArb AS LicenseType ", "LicenseTypeReference", "" });

                                datCombos3 = objDrivingLicenseBLL.FillCombos(new string[] { "CountryID,CountryNameArb as Description", "CountryReference", "" });

                            }
                            else
                            {
                                datCombos2 = objDrivingLicenseBLL.FillCombos(new string[] { "LicenseTypeID,LicenseType", "LicenseTypeReference", "" });

                                datCombos3 = objDrivingLicenseBLL.FillCombos(new string[] { "CountryID,CountryName as Description", "CountryReference", "" });
                            }

                            cboLicenseType.DisplayMember = "LicenseType";
                            cboLicenseType.ValueMember = "LicenseTypeID";
                            cboLicenseType.DataSource = datCombos2;

                            cboCountry.DisplayMember = "Description";
                            cboCountry.ValueMember = "CountryID";
                            cboCountry.DataSource = datCombos3;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
                //this.Logger.WriteLogAndDisplayMessage("Error on LoadCombos()", "Error on LoadCombos", this.Name, ex, LogSeverity.Major);
            }
        }
        /// <summary>
        /// Fill employee in navigation events
        /// </summary>
        private void FillEmployee()
        {
            DataTable datCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = objDrivingLicenseBLL.FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                    "EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "WorkStatusID > 5 OR EmployeeID = " + objDrivingLicenseBLL.DrivingLicense.EmployeeID.ToInt32() });

            }
            else
            {
                datCombos = objDrivingLicenseBLL.FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                    "EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "WorkStatusID > 5 OR EmployeeID = " + objDrivingLicenseBLL.DrivingLicense.EmployeeID.ToInt32() });

            }
            cboEmployee.DisplayMember = "EmployeeName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = datCombos;
        }
        private bool VerifyInputs()
        {
            try
            {
                errorDrivingLicense.Clear();
                //this.objValidationStructure.Status = ValidationStatus.Invalid;

                // create new instanace of Driving License to clear all existing values (if any)
                this.objDrivingLicenseBLL.DrivingLicense = new clsDrivingLicense();

                if (this.mode == Mode.Update)
                    // Set ID of the vehicle to be updated.
                    this.objDrivingLicenseBLL.DrivingLicense.LicenseID = (int)this.LicenseID;

                if (cboEmployee.SelectedIndex == -1)
                {
                    ShowMessage(501, cboEmployee);//objValidationStructure.ShowMessage(501,cboEmployee,errorDrivingLicense);
                    return false;
                }
                else
                {
                    this.objDrivingLicenseBLL.DrivingLicense.EmployeeID = Convert.ToInt32(this.cboEmployee.SelectedValue);
                }

                if (this.txtLicenseNumber.Text.Trim() == string.Empty)
                {
                    ShowMessage(502, txtLicenseNumber);//objValidationStructure.ShowMessage(502,txtLicenseNumber,errorDrivingLicense);
                    return false;
                }
                else
                    this.objDrivingLicenseBLL.DrivingLicense.LicenceNumber = txtLicenseNumber.Text.Trim();

                if (this.txtLicenseName.Text.Trim() == string.Empty)
                {
                    ShowMessage(503, txtLicenseName);// objValidationStructure.ShowMessage(503,txtName,errorDrivingLicense);
                    return false;
                }
                else
                    this.objDrivingLicenseBLL.DrivingLicense.LicenceHoldersName = this.txtLicenseName.Text.Trim();

                if (this.cboLicenseType.SelectedIndex == -1)
                {
                    ShowMessage(504, cboLicenseType);//objValidationStructure.ShowMessage(504,cboLicenseType,errorDrivingLicense);
                    return false;
                }
                else
                    this.objDrivingLicenseBLL.DrivingLicense.LicenceTypeID = Convert.ToInt32(this.cboLicenseType.SelectedValue);


                if (this.cboCountry.SelectedIndex == -1)
                {
                    ShowMessage(505, cboCountry);//objValidationStructure.ShowMessage(505,cboCountry,errorDrivingLicense);
                    return false;
                }
                else
                    // Vehicle Type is only a caption used to display in UI. LicenseType is used to represent it in database
                    this.objDrivingLicenseBLL.DrivingLicense.LicencedCountryID = Convert.ToInt32(this.cboCountry.SelectedValue);

                this.objDrivingLicenseBLL.DrivingLicense.IssueDate = Convert.ToDateTime(dtpIssueDate.Text);
                this.objDrivingLicenseBLL.DrivingLicense.ExpiryDate = Convert.ToDateTime(dtpExpiryDate.Text);
                this.objDrivingLicenseBLL.DrivingLicense.Remarks = txtRemarks.Text.Trim();
                if (objDrivingLicenseBLL.DrivingLicense.IssueDate > DateTime.Today)
                {
                    ShowMessage(507, dtpIssueDate);// objValidationStructure.ShowMessage(507, dtpIssueDate, errorDrivingLicense);
                    return false;
                }

                if (this.objDrivingLicenseBLL.DrivingLicense.IssueDate >= objDrivingLicenseBLL.DrivingLicense.ExpiryDate)
                {
                    ShowMessage(506, dtpExpiryDate);//objValidationStructure.ShowMessage(506,dtpIssueDate,errorDrivingLicense);
                    return false;
                }

                return true;
                //this.objValidationStructure.Status = ValidationStatus.Valid;              
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
                return false;
                //this.Logger.WriteLogAndDisplayMessage("Error on VerifyInputs()", "Error on VerifyInputs()", this.Name, ex, LogSeverity.Major);
            }
        }
        public void BindComboBox(ComboBox cbo, string strDisplayField, string strValueField, object dataSource)
        {
            cbo.DisplayMember = strDisplayField;
            cbo.ValueMember = strValueField;
            cbo.DataSource = dataSource;
            cbo.SelectedIndex = -1;
        }
        private void BindContorls()
        {
            try
            {
                this.txtLicenseNumber.Text = this.objDrivingLicenseBLL.DrivingLicense.LicenceNumber;
                this.txtLicenseName.Text = this.objDrivingLicenseBLL.DrivingLicense.LicenceHoldersName;
                FillEmployee();
                cboEmployee.SelectedValue = this.objDrivingLicenseBLL.DrivingLicense.EmployeeID.ToInt64();
                cboLicenseType.SelectedValue = this.objDrivingLicenseBLL.DrivingLicense.LicenceTypeID.ToInt32();
                cboCountry.SelectedValue = this.objDrivingLicenseBLL.DrivingLicense.LicencedCountryID.ToInt32();
                this.dtpIssueDate.Text = this.objDrivingLicenseBLL.DrivingLicense.IssueDate == ClsCommonSettings.NullDate ? string.Empty : this.objDrivingLicenseBLL.DrivingLicense.IssueDate.ToString();
                this.dtpExpiryDate.Text = this.objDrivingLicenseBLL.DrivingLicense.ExpiryDate == ClsCommonSettings.NullDate ? string.Empty : this.objDrivingLicenseBLL.DrivingLicense.ExpiryDate.ToString();
                this.txtRemarks.Text = this.objDrivingLicenseBLL.DrivingLicense.Remarks;
                if (Convert.ToBoolean(objDrivingLicenseBLL.DrivingLicense.IsRenew))
                {
                    lblRenewed.Visible = true;
                    cboEmployee.Enabled = cboLicenseType.Enabled = cboCountry.Enabled = BtnLicencetype.Enabled = BtnCountry.Enabled = false;
                    txtLicenseNumber.Enabled = txtLicenseName.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = false;
                }
                else
                {
                    lblRenewed.Visible = false;
                    cboEmployee.Enabled = !(EmployeeID > 0);
                    cboLicenseType.Enabled = cboCountry.Enabled = BtnLicencetype.Enabled = BtnCountry.Enabled = true;
                    txtLicenseNumber.Enabled = txtLicenseName.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }

        private void BindReference(ReferenceTables refTable, ComboBox cbo, string strDisplayMember, string strValueMember)
        {
            try
            {
                DataTable dt = this.objDrivingLicenseBLL.GetRefernceTable(refTable);
                // first clear existing items
                cbo.DataSource = null;
                cbo.Items.Clear();

                if (dt.Rows.Count > 0)
                    this.BindComboBox(cbo, strDisplayMember, strValueMember, dt);
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }
        /// <summary>
        /// Get total record count while searching
        /// </summary>
        private void GetRecordCount()
        {

            this.intTotalRecords = objDrivingLicenseBLL.GetRecordCount(EmployeeID, PiDrivingLicense, txtSearch.Text.Trim());
        }

        private void BindNavigator()
        {
            try
            {
                if (objDrivingLicenseBLL.DrivingLicense == null)
                    objDrivingLicenseBLL.DrivingLicense = new clsDrivingLicense();

                this.objDrivingLicenseBLL.DrivingLicense.EmployeeID = EmployeeID;
                this.objDrivingLicenseBLL.DrivingLicense.LicenseID = PiDrivingLicense;
                bool success = objDrivingLicenseBLL.GetLicenseByRowIndex(intRowIndex, txtSearch.Text.Trim());
                if (success)
                {
                    this.LicenseID = this.objDrivingLicenseBLL.DrivingLicense.LicenseID;
                    this.BindingNavigatorPositionItem.Text = this.intRowIndex.ToString();
                    this.intTotalRecords = (Int32)this.objDrivingLicenseBLL.DrivingLicense.Pager.TotalRecords;
                    if (EmployeeID > 0 || PiDrivingLicense > 0 || !formLoading)
                    {
                        this.BindContorls();
                    }
                }
                else
                {
                    intTotalRecords = 0;
                    this.BindingNavigatorPositionItem.Text = "1";
                }

                this.BindingNavigatorCountItem.Text = string.Format(strOf + " {0}", this.intTotalRecords);
                this.mode = Mode.Update;

                this.SetEnableDisable();

            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
                //this.Logger.WriteLogAndDisplayMessage("Error on BindNavigator()", "Error on BindNavigator()", this.Name, ex, LogSeverity.Major);
            }
        }
        private void SetIssueReceiptButton()
        {
            try
            {
                ToolStripDropDownBtnMore.Enabled = true;

                RenewToolStripMenuItem.Enabled = (clsPassportBLL.IsEmployeeInService(cboEmployee.SelectedValue.ToInt64()) ? (lblRenewed.Visible ? false : mblnAddRenew) : false);

                issueToolStripMenuItem.Enabled = receiptToolStripMenuItem.Enabled = false;

                bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Employee, (int)DocumentType.Driving_License, objDrivingLicenseBLL.DrivingLicense.LicenseID.ToInt32());

                if (IsReceipt)
                    issueToolStripMenuItem.Enabled = mblnAddIssue;
                else
                    receiptToolStripMenuItem.Enabled = mblnAddReceipt;
            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }

        private void IncrementTotalCount()
        {
            int i = this.intTotalRecords + 1;

            this.BindingNavigatorPositionItem.Text = i.ToString();
            this.BindingNavigatorCountItem.Text = string.Format(strOf + " {0}", i);
            this.BindingNavigatorMoveNextItem.Enabled = this.BindingNavigatorMoveLastItem.Enabled = false;
            this.BindingNavigatorMoveFirstItem.Enabled = this.BindingNavigatorMovePreviousItem.Enabled = this.intTotalRecords > 0;
            this.BindingNavigatorDeleteItem.Enabled = false;
            intRowIndex = intTotalRecords + 1;

        }
        /// <summary>
        /// Enables and disable navigator buttons when navigate  buttons
        /// </summary>
        private void SetEnableDisable()
        {
            EmployeeLicenseDetailsBindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = false;

            if (this.mode == Mode.Insert) BindingNavigatorDeleteItem.Enabled = false;
            else
            {
                BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (PiDrivingLicense > 0 ? false : MblnDeletePermission));
                //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Insurance card could not be removed." : "Remove");
            }
            BindingNavigatorAddNewItem.Enabled = (this.mode == Mode.Insert ? false : (PiDrivingLicense > 0 ? false : MblnAddPermission));
            BtnFax.Enabled = BtnPrint.Enabled = (this.mode == Mode.Insert ? false : MblnPrintEmailPermission); ;
            this.ToolStripDropDownBtnMore.Enabled = (this.mode == Mode.Insert ? false : true);
            btnClear.Enabled = (this.mode == Mode.Insert ? true : false);

            if (PiDrivingLicense <= 0)
            {
                BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = true;
                int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record

                if (intRowIndex >= intTotalRecords)  // If the current is last record, disables move next and last button
                {
                    BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
                }
                if (intRowIndex == 1)  // If the current is first record, disables previous and first buttons
                {
                    BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
                }
            }
            else
                this.BindingNavigatorMoveFirstItem.Enabled = this.BindingNavigatorMovePreviousItem.Enabled = this.BindingNavigatorMoveNextItem.Enabled = this.BindingNavigatorMoveLastItem.Enabled = false;

        }

        /// <summary>
        /// Assigning status message
        /// </summary>
        /// <param name="intCode"></param>
        private void AssingStatuText(int intCode)
        {
            this.lblStatus.Text = MobjClsNotification.GetStatusMessage(datMessages, intCode);
            this.Timer.Start();
        }
        public void OnInputChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void ChangeStatus()
        {
            errorDrivingLicense.Clear();
            //if (ClsCommonSettings.IsHrPowerEnabled)
            //{
            //    btnOK.Enabled = btnSave.Enabled = EmployeeLicenseDetailsBindingNavigatorSaveItem.Enabled = false;
            //    return;
            //}
            if (this.mode == Mode.Insert) // add mode
            {
                btnOK.Enabled = btnSave.Enabled = EmployeeLicenseDetailsBindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else  //edit mode
            {
                btnOK.Enabled = btnSave.Enabled = EmployeeLicenseDetailsBindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
        }
        private void SetPermissions()
        {
            // Function for setting permissions

            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DrivingLicense, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out mblnEmail, out mblnAddRenew, out mblnUpdate, out mblnDelete);
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
                mblnAddIssue = mblnAddReceipt = mblnAddRenew = true;
            }
        }
        /// <summary>
        /// Autocomplete with Employeename/Employeecode and CardNumber
        /// </summary>
        /// 
        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsBLLInsuranceCard.GetAutoCompleteList(txtSearch.Text.Trim(), EmployeeID, "LicenceNumber", "EmployeeLicenseDetails");
            this.txtSearch.AutoCompleteCustomSource = clsBLLInsuranceCard.ConvertToAutoCompleteCollection(dt);
        }

        #endregion
        #region Timer

        private void Timer_Tick(object sender, EventArgs e)
        {
            // Clear text in the status bar after the time interval of timer.
            this.lblToolStripStatus.Text = string.Empty;
            this.Timer.Stop();
        }
        #endregion

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }

        private void ToolStripDropDownBtnMore_Click(object sender, EventArgs e)
        {

        }
    }
}
