﻿
    partial class frmStockRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStockRegister));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DockSite7 = new DevComponents.DotNetBar.DockSite();
            this.Bar1 = new DevComponents.DotNetBar.Bar();
            this.PrintStripButton = new DevComponents.DotNetBar.ButtonItem();
            this.CancelToolStripButton = new DevComponents.DotNetBar.ButtonItem();
            this.LabelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.cmbCompanyFilter = new DevComponents.DotNetBar.ComboBoxItem();
            this.HelpToolStripButton = new DevComponents.DotNetBar.ButtonItem();
            this.CustomizeItem1 = new DevComponents.DotNetBar.CustomizeItem();
            this.pnlLeft = new DevComponents.DotNetBar.PanelEx();
            this.wbDocArb = new DevComponents.DotNetBar.Controls.WarningBox();
            this.wbDocs = new DevComponents.DotNetBar.Controls.WarningBox();
            this.gpSearch = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lblDocumentNumber = new DevComponents.DotNetBar.LabelX();
            this.lblDocumentType = new DevComponents.DotNetBar.LabelX();
            this.cboDocumentNumber = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboDocumentType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.cboOperationType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.gpView = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.rbtnAll = new System.Windows.Forms.RadioButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.rbtIssue = new System.Windows.Forms.RadioButton();
            this.rbtReceipt = new System.Windows.Forms.RadioButton();
            this.LabelX4 = new DevComponents.DotNetBar.LabelX();
            this.expSplitterLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.GrpPanelDocMap = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dgvDocSummary = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.Img = new System.Windows.Forms.DataGridViewImageColumn();
            this.clmdelete = new System.Windows.Forms.DataGridViewImageColumn();
            this.expandableSplitter1 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlRight = new DevComponents.DotNetBar.PanelEx();
            this.dgvDocGrid = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.clmImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.clmRecordID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOperationTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDocumentTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDocumentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmReference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmStatusID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDocumentNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCustodian = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDocumentStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmBinDetails = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmIssueReason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmReturnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmReceivedFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmPrint = new System.Windows.Forms.DataGridViewImageColumn();
            this.BarNavigation = new DevComponents.DotNetBar.Bar();
            this.lblTotalDocs = new System.Windows.Forms.Label();
            this.lblTotalRecords = new System.Windows.Forms.Label();
            this.ToolStripStatusLabel = new DevComponents.DotNetBar.LabelItem();
            this.GridNavigation = new DevComponents.DotNetBar.ItemContainer();
            this.cboPageCount = new DevComponents.DotNetBar.ComboBoxItem();
            this.BtnFirst = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrevious = new DevComponents.DotNetBar.ButtonItem();
            this.lblPageCount = new DevComponents.DotNetBar.LabelItem();
            this.BtnNext = new DevComponents.DotNetBar.ButtonItem();
            this.BtnLast = new DevComponents.DotNetBar.ButtonItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.In = new System.Windows.Forms.ToolStripMenuItem();
            this.Out = new System.Windows.Forms.ToolStripMenuItem();
            this.BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bck_Wrkr = new System.ComponentModel.BackgroundWorker();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DockSite7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).BeginInit();
            this.pnlLeft.SuspendLayout();
            this.gpSearch.SuspendLayout();
            this.gpView.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.GrpPanelDocMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocSummary)).BeginInit();
            this.pnlRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarNavigation)).BeginInit();
            this.BarNavigation.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // DockSite7
            // 
            this.DockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.DockSite7.Controls.Add(this.Bar1);
            this.DockSite7.Dock = System.Windows.Forms.DockStyle.Top;
            this.DockSite7.Location = new System.Drawing.Point(0, 0);
            this.DockSite7.Name = "DockSite7";
            this.DockSite7.Size = new System.Drawing.Size(1225, 28);
            this.DockSite7.TabIndex = 77;
            this.DockSite7.TabStop = false;
            // 
            // Bar1
            // 
            this.Bar1.AccessibleDescription = "DotNetBar Bar (Bar1)";
            this.Bar1.AccessibleName = "DotNetBar Bar";
            this.Bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.Bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.Bar1.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003;
            this.Bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.PrintStripButton,
            this.CancelToolStripButton,
            this.LabelItem1,
            this.cmbCompanyFilter,
            this.HelpToolStripButton,
            this.CustomizeItem1});
            this.Bar1.Location = new System.Drawing.Point(0, 0);
            this.Bar1.Name = "Bar1";
            this.Bar1.Size = new System.Drawing.Size(370, 28);
            this.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.Bar1.TabIndex = 0;
            this.Bar1.TabStop = false;
            this.Bar1.Text = "Bar1";
            this.Bar1.Visible = false;
            // 
            // PrintStripButton
            // 
            this.PrintStripButton.Image = global::MyPayfriend.Properties.Resources.Print;
            this.PrintStripButton.Name = "PrintStripButton";
            this.PrintStripButton.Text = "Print";
            this.PrintStripButton.Tooltip = "Print";
            this.PrintStripButton.Visible = false;
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Text = "Clear Filter";
            this.CancelToolStripButton.Tooltip = "Remove Filter";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // LabelItem1
            // 
            this.LabelItem1.BeginGroup = true;
            this.LabelItem1.Name = "LabelItem1";
            this.LabelItem1.PaddingBottom = 2;
            this.LabelItem1.PaddingLeft = 10;
            this.LabelItem1.PaddingRight = 2;
            this.LabelItem1.Text = "<b> Show by</b>";
            this.LabelItem1.Visible = false;
            // 
            // cmbCompanyFilter
            // 
            this.cmbCompanyFilter.ComboWidth = 200;
            this.cmbCompanyFilter.DropDownHeight = 106;
            this.cmbCompanyFilter.ItemHeight = 17;
            this.cmbCompanyFilter.Name = "cmbCompanyFilter";
            this.cmbCompanyFilter.Stretch = true;
            this.cmbCompanyFilter.Visible = false;
            this.cmbCompanyFilter.WatermarkColor = System.Drawing.SystemColors.ControlDarkDark;
            this.cmbCompanyFilter.WatermarkText = "All <b>Companies</b> && <b>Branches</b>";
            // 
            // HelpToolStripButton
            // 
            this.HelpToolStripButton.BeginGroup = true;
            this.HelpToolStripButton.Image = global::MyPayfriend.Properties.Resources.help;
            this.HelpToolStripButton.Name = "HelpToolStripButton";
            this.HelpToolStripButton.Text = "Help";
            this.HelpToolStripButton.Tooltip = "Help";
            this.HelpToolStripButton.Click += new System.EventHandler(this.HelpToolStripButton_Click);
            // 
            // CustomizeItem1
            // 
            this.CustomizeItem1.CustomizeItemVisible = false;
            this.CustomizeItem1.Name = "CustomizeItem1";
            this.CustomizeItem1.Text = "&Add or Remove Buttons";
            this.CustomizeItem1.Tooltip = "Bar Options";
            // 
            // pnlLeft
            // 
            this.pnlLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLeft.Controls.Add(this.wbDocArb);
            this.pnlLeft.Controls.Add(this.wbDocs);
            this.pnlLeft.Controls.Add(this.gpSearch);
            this.pnlLeft.Controls.Add(this.gpView);
            this.pnlLeft.Controls.Add(this.LabelX4);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 28);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(195, 503);
            this.pnlLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeft.Style.GradientAngle = 90;
            this.pnlLeft.TabIndex = 78;
            // 
            // wbDocArb
            // 
            this.wbDocArb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wbDocArb.AutoScroll = true;
            this.wbDocArb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.wbDocArb.CloseButtonVisible = false;
            this.wbDocArb.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wbDocArb.Location = new System.Drawing.Point(8, 327);
            this.wbDocArb.Name = "wbDocArb";
            this.wbDocArb.OptionsButtonVisible = false;
            this.wbDocArb.OptionsText = "More Tips!";
            this.wbDocArb.Size = new System.Drawing.Size(179, 167);
            this.wbDocArb.TabIndex = 35;
            this.wbDocArb.Text = resources.GetString("wbDocArb.Text");
            this.wbDocArb.Visible = false;
            // 
            // wbDocs
            // 
            this.wbDocs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wbDocs.AutoScroll = true;
            this.wbDocs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(219)))), ((int)(((byte)(249)))));
            this.wbDocs.CloseButtonVisible = false;
            this.wbDocs.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wbDocs.Location = new System.Drawing.Point(8, 327);
            this.wbDocs.Name = "wbDocs";
            this.wbDocs.OptionsButtonVisible = false;
            this.wbDocs.OptionsText = "More Tips!";
            this.wbDocs.Size = new System.Drawing.Size(179, 167);
            this.wbDocs.TabIndex = 34;
            this.wbDocs.Text = resources.GetString("wbDocs.Text");
            // 
            // gpSearch
            // 
            this.gpSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gpSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpSearch.Controls.Add(this.lblDocumentNumber);
            this.gpSearch.Controls.Add(this.lblDocumentType);
            this.gpSearch.Controls.Add(this.cboDocumentNumber);
            this.gpSearch.Controls.Add(this.cboDocumentType);
            this.gpSearch.Controls.Add(this.lblType);
            this.gpSearch.Controls.Add(this.cboOperationType);
            this.gpSearch.Location = new System.Drawing.Point(8, 139);
            this.gpSearch.Name = "gpSearch";
            this.gpSearch.Size = new System.Drawing.Size(179, 179);
            // 
            // 
            // 
            this.gpSearch.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpSearch.Style.BackColorGradientAngle = 90;
            this.gpSearch.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpSearch.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpSearch.Style.BorderBottomWidth = 1;
            this.gpSearch.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpSearch.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpSearch.Style.BorderLeftWidth = 1;
            this.gpSearch.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpSearch.Style.BorderRightWidth = 1;
            this.gpSearch.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpSearch.Style.BorderTopWidth = 1;
            this.gpSearch.Style.Class = "";
            this.gpSearch.Style.CornerDiameter = 4;
            this.gpSearch.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpSearch.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpSearch.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpSearch.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpSearch.StyleMouseDown.Class = "";
            this.gpSearch.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpSearch.StyleMouseOver.Class = "";
            this.gpSearch.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpSearch.TabIndex = 33;
            this.gpSearch.Text = "Search";
            // 
            // lblDocumentNumber
            // 
            // 
            // 
            // 
            this.lblDocumentNumber.BackgroundStyle.Class = "";
            this.lblDocumentNumber.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDocumentNumber.Location = new System.Drawing.Point(3, 104);
            this.lblDocumentNumber.Name = "lblDocumentNumber";
            this.lblDocumentNumber.Size = new System.Drawing.Size(106, 15);
            this.lblDocumentNumber.TabIndex = 28;
            this.lblDocumentNumber.Text = "Document Number";
            // 
            // lblDocumentType
            // 
            // 
            // 
            // 
            this.lblDocumentType.BackgroundStyle.Class = "";
            this.lblDocumentType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDocumentType.Location = new System.Drawing.Point(3, 55);
            this.lblDocumentType.Name = "lblDocumentType";
            this.lblDocumentType.Size = new System.Drawing.Size(90, 15);
            this.lblDocumentType.TabIndex = 28;
            this.lblDocumentType.Text = "Document Type";
            // 
            // cboDocumentNumber
            // 
            this.cboDocumentNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDocumentNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDocumentNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDocumentNumber.DisplayMember = "Text";
            this.cboDocumentNumber.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDocumentNumber.DropDownHeight = 134;
            this.cboDocumentNumber.FormattingEnabled = true;
            this.cboDocumentNumber.IntegralHeight = false;
            this.cboDocumentNumber.ItemHeight = 14;
            this.cboDocumentNumber.Location = new System.Drawing.Point(3, 125);
            this.cboDocumentNumber.Name = "cboDocumentNumber";
            this.cboDocumentNumber.Size = new System.Drawing.Size(167, 20);
            this.cboDocumentNumber.TabIndex = 27;
            this.cboDocumentNumber.SelectedIndexChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            this.cboDocumentNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboDocumentNumber_KeyDown);
            // 
            // cboDocumentType
            // 
            this.cboDocumentType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDocumentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDocumentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDocumentType.DisplayMember = "Text";
            this.cboDocumentType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDocumentType.DropDownHeight = 134;
            this.cboDocumentType.FormattingEnabled = true;
            this.cboDocumentType.IntegralHeight = false;
            this.cboDocumentType.ItemHeight = 14;
            this.cboDocumentType.Location = new System.Drawing.Point(3, 78);
            this.cboDocumentType.Name = "cboDocumentType";
            this.cboDocumentType.Size = new System.Drawing.Size(167, 20);
            this.cboDocumentType.TabIndex = 27;
            this.cboDocumentType.SelectedIndexChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            this.cboDocumentType.SelectedValueChanged += new System.EventHandler(this.cboDocumentType_SelectedValueChanged);
            this.cboDocumentType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboDocumentType_KeyDown);
            // 
            // lblType
            // 
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(3, 3);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(36, 15);
            this.lblType.TabIndex = 26;
            this.lblType.Text = "Type";
            // 
            // cboOperationType
            // 
            this.cboOperationType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboOperationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOperationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOperationType.DisplayMember = "Text";
            this.cboOperationType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboOperationType.DropDownHeight = 134;
            this.cboOperationType.FormattingEnabled = true;
            this.cboOperationType.IntegralHeight = false;
            this.cboOperationType.ItemHeight = 14;
            this.cboOperationType.Location = new System.Drawing.Point(3, 25);
            this.cboOperationType.Name = "cboOperationType";
            this.cboOperationType.Size = new System.Drawing.Size(167, 20);
            this.cboOperationType.TabIndex = 25;
            this.cboOperationType.SelectedIndexChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            this.cboOperationType.SelectedValueChanged += new System.EventHandler(this.cboOperationType_SelectedValueChanged);
            this.cboOperationType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboOperationType_KeyDown);
            // 
            // gpView
            // 
            this.gpView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gpView.CanvasColor = System.Drawing.SystemColors.Control;
            this.gpView.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.gpView.Controls.Add(this.rbtnAll);
            this.gpView.Controls.Add(this.rbtIssue);
            this.gpView.Controls.Add(this.rbtReceipt);
            this.gpView.Location = new System.Drawing.Point(8, 29);
            this.gpView.Name = "gpView";
            this.gpView.Size = new System.Drawing.Size(179, 104);
            // 
            // 
            // 
            this.gpView.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.gpView.Style.BackColorGradientAngle = 90;
            this.gpView.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.gpView.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpView.Style.BorderBottomWidth = 1;
            this.gpView.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.gpView.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpView.Style.BorderLeftWidth = 1;
            this.gpView.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpView.Style.BorderRightWidth = 1;
            this.gpView.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.gpView.Style.BorderTopWidth = 1;
            this.gpView.Style.Class = "";
            this.gpView.Style.CornerDiameter = 4;
            this.gpView.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.gpView.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.gpView.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.gpView.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.gpView.StyleMouseDown.Class = "";
            this.gpView.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.gpView.StyleMouseOver.Class = "";
            this.gpView.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.gpView.TabIndex = 32;
            this.gpView.Text = "View";
            // 
            // rbtnAll
            // 
            this.rbtnAll.Checked = true;
            this.rbtnAll.ImageIndex = 3;
            this.rbtnAll.ImageList = this.ImageList1;
            this.rbtnAll.Location = new System.Drawing.Point(12, 3);
            this.rbtnAll.Name = "rbtnAll";
            this.rbtnAll.Size = new System.Drawing.Size(132, 23);
            this.rbtnAll.TabIndex = 22;
            this.rbtnAll.TabStop = true;
            this.rbtnAll.Text = "Document Register";
            this.rbtnAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbtnAll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rbtnAll.UseVisualStyleBackColor = true;
            this.rbtnAll.CheckedChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "DocumentIn.PNG");
            this.ImageList1.Images.SetKeyName(1, "DocumentOut.PNG");
            this.ImageList1.Images.SetKeyName(2, "Add.png");
            this.ImageList1.Images.SetKeyName(3, "Stock Book.png");
            // 
            // rbtIssue
            // 
            this.rbtIssue.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtIssue.ImageIndex = 1;
            this.rbtIssue.ImageList = this.ImageList1;
            this.rbtIssue.Location = new System.Drawing.Point(12, 52);
            this.rbtIssue.Name = "rbtIssue";
            this.rbtIssue.Size = new System.Drawing.Size(100, 28);
            this.rbtIssue.TabIndex = 20;
            this.rbtIssue.Text = "Docs Issued";
            this.rbtIssue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbtIssue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rbtIssue.UseVisualStyleBackColor = true;
            this.rbtIssue.CheckedChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            // 
            // rbtReceipt
            // 
            this.rbtReceipt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtReceipt.ImageIndex = 0;
            this.rbtReceipt.ImageList = this.ImageList1;
            this.rbtReceipt.Location = new System.Drawing.Point(12, 26);
            this.rbtReceipt.Name = "rbtReceipt";
            this.rbtReceipt.Size = new System.Drawing.Size(112, 28);
            this.rbtReceipt.TabIndex = 21;
            this.rbtReceipt.Text = "Docs Available";
            this.rbtReceipt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbtReceipt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rbtReceipt.UseVisualStyleBackColor = true;
            this.rbtReceipt.CheckedChanged += new System.EventHandler(this.rbtnAll_CheckedChanged);
            // 
            // LabelX4
            // 
            // 
            // 
            // 
            this.LabelX4.BackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarCaptionBackground2;
            this.LabelX4.BackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarCaptionBackground;
            this.LabelX4.BackgroundStyle.Class = "";
            this.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX4.Dock = System.Windows.Forms.DockStyle.Top;
            this.LabelX4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelX4.ForeColor = System.Drawing.Color.White;
            this.LabelX4.Location = new System.Drawing.Point(0, 0);
            this.LabelX4.Name = "LabelX4";
            this.LabelX4.Size = new System.Drawing.Size(195, 20);
            this.LabelX4.TabIndex = 31;
            this.LabelX4.Text = " Documents";
            // 
            // expSplitterLeft
            // 
            this.expSplitterLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitterLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitterLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expSplitterLeft.ExpandableControl = this.pnlLeft;
            this.expSplitterLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitterLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitterLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSplitterLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSplitterLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSplitterLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSplitterLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expSplitterLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expSplitterLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expSplitterLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expSplitterLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expSplitterLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expSplitterLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitterLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitterLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSplitterLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSplitterLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitterLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitterLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expSplitterLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expSplitterLeft.Location = new System.Drawing.Point(195, 28);
            this.expSplitterLeft.Name = "expSplitterLeft";
            this.expSplitterLeft.Size = new System.Drawing.Size(2, 503);
            this.expSplitterLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expSplitterLeft.TabIndex = 81;
            this.expSplitterLeft.TabStop = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.GrpPanelDocMap);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(197, 358);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1028, 173);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 86;
            // 
            // GrpPanelDocMap
            // 
            this.GrpPanelDocMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GrpPanelDocMap.CanvasColor = System.Drawing.SystemColors.Control;
            this.GrpPanelDocMap.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.GrpPanelDocMap.Controls.Add(this.dgvDocSummary);
            this.GrpPanelDocMap.Location = new System.Drawing.Point(7, 9);
            this.GrpPanelDocMap.Name = "GrpPanelDocMap";
            this.GrpPanelDocMap.Size = new System.Drawing.Size(1015, 155);
            // 
            // 
            // 
            this.GrpPanelDocMap.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.GrpPanelDocMap.Style.BackColorGradientAngle = 90;
            this.GrpPanelDocMap.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.GrpPanelDocMap.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpPanelDocMap.Style.BorderBottomWidth = 1;
            this.GrpPanelDocMap.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GrpPanelDocMap.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpPanelDocMap.Style.BorderLeftWidth = 1;
            this.GrpPanelDocMap.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpPanelDocMap.Style.BorderRightWidth = 1;
            this.GrpPanelDocMap.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GrpPanelDocMap.Style.BorderTopWidth = 1;
            this.GrpPanelDocMap.Style.Class = "";
            this.GrpPanelDocMap.Style.CornerDiameter = 4;
            this.GrpPanelDocMap.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GrpPanelDocMap.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GrpPanelDocMap.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GrpPanelDocMap.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.GrpPanelDocMap.StyleMouseDown.Class = "";
            this.GrpPanelDocMap.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.GrpPanelDocMap.StyleMouseOver.Class = "";
            this.GrpPanelDocMap.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GrpPanelDocMap.TabIndex = 2;
            this.GrpPanelDocMap.Text = "Document Map";
            // 
            // dgvDocSummary
            // 
            this.dgvDocSummary.AllowUserToAddRows = false;
            this.dgvDocSummary.AllowUserToDeleteRows = false;
            this.dgvDocSummary.AllowUserToResizeColumns = false;
            this.dgvDocSummary.AllowUserToResizeRows = false;
            this.dgvDocSummary.BackgroundColor = System.Drawing.Color.White;
            this.dgvDocSummary.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDocSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Img,
            this.clmdelete});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDocSummary.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDocSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDocSummary.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvDocSummary.Location = new System.Drawing.Point(0, 0);
            this.dgvDocSummary.Name = "dgvDocSummary";
            this.dgvDocSummary.ReadOnly = true;
            this.dgvDocSummary.RowHeadersVisible = false;
            this.dgvDocSummary.Size = new System.Drawing.Size(1009, 134);
            this.dgvDocSummary.TabIndex = 0;
            this.dgvDocSummary.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDocSummary_CellMouseClick);
            this.dgvDocSummary.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDocSummary_CellMouseDoubleClick);
            this.dgvDocSummary.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDocSummary_DataError);
            // 
            // Img
            // 
            this.Img.DataPropertyName = "Img";
            this.Img.HeaderText = "";
            this.Img.Name = "Img";
            this.Img.ReadOnly = true;
            this.Img.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Img.ToolTipText = "Status";
            this.Img.Visible = false;
            this.Img.Width = 20;
            // 
            // clmdelete
            // 
            this.clmdelete.DataPropertyName = "clmdelete";
            this.clmdelete.Description = "Delete";
            this.clmdelete.HeaderText = "";
            this.clmdelete.Name = "clmdelete";
            this.clmdelete.ReadOnly = true;
            this.clmdelete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clmdelete.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.clmdelete.ToolTipText = "Delete";
            this.clmdelete.Width = 30;
            // 
            // expandableSplitter1
            // 
            this.expandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter1.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.expandableSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.expandableSplitter1.ExpandableControl = this.pnlBottom;
            this.expandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.Location = new System.Drawing.Point(197, 356);
            this.expandableSplitter1.Name = "expandableSplitter1";
            this.expandableSplitter1.Size = new System.Drawing.Size(1028, 2);
            this.expandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitter1.TabIndex = 87;
            this.expandableSplitter1.TabStop = false;
            // 
            // pnlRight
            // 
            this.pnlRight.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlRight.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlRight.Controls.Add(this.dgvDocGrid);
            this.pnlRight.Controls.Add(this.BarNavigation);
            this.pnlRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRight.Location = new System.Drawing.Point(197, 28);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(1028, 328);
            this.pnlRight.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlRight.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlRight.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlRight.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlRight.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlRight.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlRight.Style.GradientAngle = 90;
            this.pnlRight.TabIndex = 88;
            // 
            // dgvDocGrid
            // 
            this.dgvDocGrid.AllowUserToAddRows = false;
            this.dgvDocGrid.AllowUserToDeleteRows = false;
            this.dgvDocGrid.AllowUserToResizeColumns = false;
            this.dgvDocGrid.AllowUserToResizeRows = false;
            this.dgvDocGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDocGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDocGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDocGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmImage,
            this.clmRecordID,
            this.clmOperationTypeID,
            this.clmDocumentTypeID,
            this.clmDocumentID,
            this.clmType,
            this.clmReference,
            this.clmStatusID,
            this.clmDocumentNumber,
            this.clmDate,
            this.ExpiryDate,
            this.clmCustodian,
            this.clmDocumentStatus,
            this.clmBinDetails,
            this.clmIssueReason,
            this.clmReturnDate,
            this.clmRemarks,
            this.clmOrderNo,
            this.clmReceivedFrom,
            this.clmPrint});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDocGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDocGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDocGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvDocGrid.Location = new System.Drawing.Point(0, 0);
            this.dgvDocGrid.MultiSelect = false;
            this.dgvDocGrid.Name = "dgvDocGrid";
            this.dgvDocGrid.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDocGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDocGrid.RowHeadersVisible = false;
            this.dgvDocGrid.Size = new System.Drawing.Size(1028, 294);
            this.dgvDocGrid.TabIndex = 33;
            this.dgvDocGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDocGrid_CellMouseClick);
            this.dgvDocGrid.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDocGrid_CellMouseDoubleClick);
            this.dgvDocGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDocGrid_DataError);
            this.dgvDocGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDocGrid_CellContentClick);
            // 
            // clmImage
            // 
            this.clmImage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.clmImage.DataPropertyName = "clmImage";
            this.clmImage.Description = "In";
            this.clmImage.HeaderText = "";
            this.clmImage.Name = "clmImage";
            this.clmImage.ReadOnly = true;
            this.clmImage.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmImage.ToolTipText = "Status";
            this.clmImage.Width = 25;
            // 
            // clmRecordID
            // 
            this.clmRecordID.DataPropertyName = "RowIndex";
            this.clmRecordID.HeaderText = "RecordID";
            this.clmRecordID.Name = "clmRecordID";
            this.clmRecordID.ReadOnly = true;
            this.clmRecordID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmRecordID.Visible = false;
            // 
            // clmOperationTypeID
            // 
            this.clmOperationTypeID.DataPropertyName = "OperationTypeID";
            this.clmOperationTypeID.HeaderText = "OperationTypeID";
            this.clmOperationTypeID.Name = "clmOperationTypeID";
            this.clmOperationTypeID.ReadOnly = true;
            this.clmOperationTypeID.Visible = false;
            // 
            // clmDocumentTypeID
            // 
            this.clmDocumentTypeID.DataPropertyName = "DocumentTypeID";
            this.clmDocumentTypeID.HeaderText = "DocumentTypeID";
            this.clmDocumentTypeID.Name = "clmDocumentTypeID";
            this.clmDocumentTypeID.ReadOnly = true;
            this.clmDocumentTypeID.Visible = false;
            // 
            // clmDocumentID
            // 
            this.clmDocumentID.DataPropertyName = "DocumentID";
            this.clmDocumentID.HeaderText = "DocumentID";
            this.clmDocumentID.Name = "clmDocumentID";
            this.clmDocumentID.ReadOnly = true;
            this.clmDocumentID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmDocumentID.Visible = false;
            // 
            // clmType
            // 
            this.clmType.DataPropertyName = "DocumentType";
            this.clmType.FillWeight = 12F;
            this.clmType.HeaderText = "DocumentType";
            this.clmType.Name = "clmType";
            this.clmType.ReadOnly = true;
            this.clmType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmReference
            // 
            this.clmReference.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmReference.DataPropertyName = "Employee";
            this.clmReference.FillWeight = 11.52F;
            this.clmReference.HeaderText = "Employee";
            this.clmReference.Name = "clmReference";
            this.clmReference.ReadOnly = true;
            this.clmReference.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmStatusID
            // 
            this.clmStatusID.DataPropertyName = "Status";
            this.clmStatusID.HeaderText = "StatusID";
            this.clmStatusID.Name = "clmStatusID";
            this.clmStatusID.ReadOnly = true;
            this.clmStatusID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmStatusID.Visible = false;
            // 
            // clmDocumentNumber
            // 
            this.clmDocumentNumber.DataPropertyName = "DocumentNumber";
            this.clmDocumentNumber.FillWeight = 11.52F;
            this.clmDocumentNumber.HeaderText = "Document Number";
            this.clmDocumentNumber.Name = "clmDocumentNumber";
            this.clmDocumentNumber.ReadOnly = true;
            this.clmDocumentNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmDate
            // 
            this.clmDate.DataPropertyName = "Date";
            this.clmDate.FillWeight = 11.52F;
            this.clmDate.HeaderText = "Date";
            this.clmDate.Name = "clmDate";
            this.clmDate.ReadOnly = true;
            this.clmDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ExpiryDate
            // 
            this.ExpiryDate.DataPropertyName = "ExpiryDate";
            this.ExpiryDate.FillWeight = 11.52F;
            this.ExpiryDate.HeaderText = "ExpiryDate";
            this.ExpiryDate.Name = "ExpiryDate";
            this.ExpiryDate.ReadOnly = true;
            this.ExpiryDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmCustodian
            // 
            this.clmCustodian.DataPropertyName = "Custodian";
            this.clmCustodian.FillWeight = 11.52F;
            this.clmCustodian.HeaderText = "Custodian";
            this.clmCustodian.Name = "clmCustodian";
            this.clmCustodian.ReadOnly = true;
            this.clmCustodian.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmDocumentStatus
            // 
            this.clmDocumentStatus.DataPropertyName = "DocumentStatus";
            this.clmDocumentStatus.FillWeight = 14.16636F;
            this.clmDocumentStatus.HeaderText = "Document Status";
            this.clmDocumentStatus.Name = "clmDocumentStatus";
            this.clmDocumentStatus.ReadOnly = true;
            this.clmDocumentStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmBinDetails
            // 
            this.clmBinDetails.DataPropertyName = "BinName";
            this.clmBinDetails.FillWeight = 11.52F;
            this.clmBinDetails.HeaderText = "Bin No";
            this.clmBinDetails.Name = "clmBinDetails";
            this.clmBinDetails.ReadOnly = true;
            this.clmBinDetails.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmIssueReason
            // 
            this.clmIssueReason.DataPropertyName = "IssueReason";
            this.clmIssueReason.FillWeight = 11.52F;
            this.clmIssueReason.HeaderText = "Issue Reason";
            this.clmIssueReason.Name = "clmIssueReason";
            this.clmIssueReason.ReadOnly = true;
            this.clmIssueReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmReturnDate
            // 
            this.clmReturnDate.DataPropertyName = "ReturnDate";
            this.clmReturnDate.FillWeight = 11.52F;
            this.clmReturnDate.HeaderText = "Return Date";
            this.clmReturnDate.Name = "clmReturnDate";
            this.clmReturnDate.ReadOnly = true;
            this.clmReturnDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmRemarks
            // 
            this.clmRemarks.DataPropertyName = "Remarks";
            this.clmRemarks.FillWeight = 11.52F;
            this.clmRemarks.HeaderText = "Remarks";
            this.clmRemarks.Name = "clmRemarks";
            this.clmRemarks.ReadOnly = true;
            this.clmRemarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmOrderNo
            // 
            this.clmOrderNo.DataPropertyName = "OrderNo";
            this.clmOrderNo.HeaderText = "OrderNo";
            this.clmOrderNo.Name = "clmOrderNo";
            this.clmOrderNo.ReadOnly = true;
            this.clmOrderNo.Visible = false;
            // 
            // clmReceivedFrom
            // 
            this.clmReceivedFrom.DataPropertyName = "ReceivedFrom";
            this.clmReceivedFrom.HeaderText = "ReceivedFrom";
            this.clmReceivedFrom.Name = "clmReceivedFrom";
            this.clmReceivedFrom.ReadOnly = true;
            this.clmReceivedFrom.Visible = false;
            // 
            // clmPrint
            // 
            this.clmPrint.FillWeight = 15.5061F;
            this.clmPrint.HeaderText = "";
            this.clmPrint.Name = "clmPrint";
            this.clmPrint.ReadOnly = true;
            this.clmPrint.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // BarNavigation
            // 
            this.BarNavigation.AccessibleDescription = "DotNetBar Bar (BarNavigation)";
            this.BarNavigation.AccessibleName = "DotNetBar Bar";
            this.BarNavigation.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.BarNavigation.BackColor = System.Drawing.Color.White;
            this.BarNavigation.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.BarNavigation.CanCustomize = false;
            this.BarNavigation.Controls.Add(this.lblTotalDocs);
            this.BarNavigation.Controls.Add(this.lblTotalRecords);
            this.BarNavigation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BarNavigation.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.BarNavigation.DockSide = DevComponents.DotNetBar.eDockSide.Document;
            this.BarNavigation.DockTabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Right;
            this.BarNavigation.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.BarNavigation.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ToolStripStatusLabel,
            this.GridNavigation});
            this.BarNavigation.Location = new System.Drawing.Point(0, 294);
            this.BarNavigation.Name = "BarNavigation";
            this.BarNavigation.Size = new System.Drawing.Size(1028, 34);
            this.BarNavigation.Stretch = true;
            this.BarNavigation.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.BarNavigation.TabIndex = 32;
            this.BarNavigation.TabStop = false;
            this.BarNavigation.Text = "Navigation";
            // 
            // lblTotalDocs
            // 
            this.lblTotalDocs.AutoSize = true;
            this.lblTotalDocs.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.lblTotalDocs.Location = new System.Drawing.Point(486, 10);
            this.lblTotalDocs.Name = "lblTotalDocs";
            this.lblTotalDocs.Size = new System.Drawing.Size(40, 15);
            this.lblTotalDocs.TabIndex = 10001;
            this.lblTotalDocs.Text = "label2";
            // 
            // lblTotalRecords
            // 
            this.lblTotalRecords.AutoSize = true;
            this.lblTotalRecords.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.lblTotalRecords.Location = new System.Drawing.Point(397, 10);
            this.lblTotalRecords.Name = "lblTotalRecords";
            this.lblTotalRecords.Size = new System.Drawing.Size(89, 15);
            this.lblTotalRecords.TabIndex = 10000;
            this.lblTotalRecords.Text = "Total Records :";
            // 
            // ToolStripStatusLabel
            // 
            this.ToolStripStatusLabel.CanCustomize = false;
            this.ToolStripStatusLabel.Name = "ToolStripStatusLabel";
            this.ToolStripStatusLabel.PaddingLeft = 5;
            this.ToolStripStatusLabel.SingleLineColor = System.Drawing.SystemColors.MenuHighlight;
            this.ToolStripStatusLabel.Stretch = true;
            // 
            // GridNavigation
            // 
            // 
            // 
            // 
            this.GridNavigation.BackgroundStyle.Class = "";
            this.GridNavigation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GridNavigation.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.GridNavigation.Name = "GridNavigation";
            this.GridNavigation.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cboPageCount,
            this.BtnFirst,
            this.BtnPrevious,
            this.lblPageCount,
            this.BtnNext,
            this.BtnLast});
            this.GridNavigation.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // cboPageCount
            // 
            this.cboPageCount.DropDownHeight = 106;
            this.cboPageCount.ItemHeight = 17;
            this.cboPageCount.Name = "cboPageCount";
            this.cboPageCount.SelectedIndexChanged += new System.EventHandler(this.cboPageCount_SelectedIndexChanged);
            // 
            // BtnFirst
            // 
            this.BtnFirst.Image = global::MyPayfriend.Properties.Resources.ArrowLeftStart1;
            this.BtnFirst.Name = "BtnFirst";
            this.BtnFirst.Text = "Move First";
            this.BtnFirst.Tooltip = "Move First";
            this.BtnFirst.Click += new System.EventHandler(this.BtnFirst_Click);
            // 
            // BtnPrevious
            // 
            this.BtnPrevious.Image = global::MyPayfriend.Properties.Resources.ArrowLeft1;
            this.BtnPrevious.Name = "BtnPrevious";
            this.BtnPrevious.Text = "Move Previous";
            this.BtnPrevious.Tooltip = "Move Previous";
            this.BtnPrevious.Click += new System.EventHandler(this.BtnPrevious_Click);
            // 
            // lblPageCount
            // 
            this.lblPageCount.BeginGroup = true;
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.PaddingLeft = 3;
            this.lblPageCount.PaddingRight = 3;
            this.lblPageCount.Text = "{0} of {0}";
            this.lblPageCount.Width = 80;
            // 
            // BtnNext
            // 
            this.BtnNext.BeginGroup = true;
            this.BtnNext.Image = global::MyPayfriend.Properties.Resources.ArrowRight1;
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Text = "Next";
            this.BtnNext.Tooltip = "Move Next";
            this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // BtnLast
            // 
            this.BtnLast.Image = global::MyPayfriend.Properties.Resources.ArrowRightStart1;
            this.BtnLast.Name = "BtnLast";
            this.BtnLast.Text = "Last";
            this.BtnLast.Tooltip = "Move Last";
            this.BtnLast.Click += new System.EventHandler(this.BtnLast_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.In,
            this.Out});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(95, 48);
            // 
            // In
            // 
            this.In.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.In.Name = "In";
            this.In.Size = new System.Drawing.Size(94, 22);
            this.In.Text = "In";
            // 
            // Out
            // 
            this.Out.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.Out.Name = "Out";
            this.Out.Size = new System.Drawing.Size(94, 22);
            this.Out.Text = "Out";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Type";
            this.dataGridViewTextBoxColumn1.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 90;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Reference";
            this.dataGridViewTextBoxColumn2.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Reference";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Visible = false;
            this.dataGridViewTextBoxColumn2.Width = 90;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "DocumentType";
            this.dataGridViewTextBoxColumn3.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Document Type";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Visible = false;
            this.dataGridViewTextBoxColumn3.Width = 90;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "DocumentNumber";
            this.dataGridViewTextBoxColumn4.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Document Number";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Visible = false;
            this.dataGridViewTextBoxColumn4.Width = 90;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Date";
            this.dataGridViewTextBoxColumn5.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Date";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 90;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Custodian";
            this.dataGridViewTextBoxColumn6.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn6.HeaderText = "Custodian";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "DocumentStatus";
            this.dataGridViewTextBoxColumn7.FillWeight = 139.5349F;
            this.dataGridViewTextBoxColumn7.HeaderText = "DocumentStatus";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Visible = false;
            this.dataGridViewTextBoxColumn7.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "BinDetails";
            this.dataGridViewTextBoxColumn8.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn8.HeaderText = "Bin Details";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 90;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "IssueReason";
            this.dataGridViewTextBoxColumn9.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn9.HeaderText = "IssueReason";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 90;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "ReturnDate";
            this.dataGridViewTextBoxColumn10.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn10.HeaderText = "ReturnDate";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 90;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Remarks";
            this.dataGridViewTextBoxColumn11.FillWeight = 96.40591F;
            this.dataGridViewTextBoxColumn11.HeaderText = "Remarks";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 79;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "DocumentID";
            this.dataGridViewTextBoxColumn12.FillWeight = 14.16636F;
            this.dataGridViewTextBoxColumn12.HeaderText = "DocumentID";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            this.dataGridViewTextBoxColumn12.Width = 13;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "StatusID";
            this.dataGridViewTextBoxColumn13.FillWeight = 11.52F;
            this.dataGridViewTextBoxColumn13.HeaderText = "StatusID";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 13;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "IssueReason";
            this.dataGridViewTextBoxColumn14.FillWeight = 11.52F;
            this.dataGridViewTextBoxColumn14.HeaderText = "RecordID";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 75;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "ReturnDate";
            this.dataGridViewTextBoxColumn15.FillWeight = 11.52F;
            this.dataGridViewTextBoxColumn15.HeaderText = "Return Date";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 79;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Remarks";
            this.dataGridViewTextBoxColumn16.FillWeight = 11.52F;
            this.dataGridViewTextBoxColumn16.HeaderText = "Remarks";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Width = 79;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "OrderNo";
            this.dataGridViewTextBoxColumn17.HeaderText = "OrderNo";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Visible = false;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "ReceivedFrom";
            this.dataGridViewTextBoxColumn18.HeaderText = "ReceivedFrom";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Visible = false;
            // 
            // frmStockRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 531);
            this.Controls.Add(this.pnlRight);
            this.Controls.Add(this.expandableSplitter1);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.expSplitterLeft);
            this.Controls.Add(this.pnlLeft);
            this.Controls.Add(this.DockSite7);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmStockRegister";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Document Register";
            this.Load += new System.EventHandler(this.frmStockRegister_Load);
            this.Shown += new System.EventHandler(this.frmStockRegister_Shown);
            this.DockSite7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Bar1)).EndInit();
            this.pnlLeft.ResumeLayout(false);
            this.gpSearch.ResumeLayout(false);
            this.gpView.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.GrpPanelDocMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocSummary)).EndInit();
            this.pnlRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarNavigation)).EndInit();
            this.BarNavigation.ResumeLayout(false);
            this.BarNavigation.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.DockSite DockSite7;
        internal DevComponents.DotNetBar.Bar Bar1;
        internal DevComponents.DotNetBar.ButtonItem PrintStripButton;
        internal DevComponents.DotNetBar.ButtonItem CancelToolStripButton;
        internal DevComponents.DotNetBar.LabelItem LabelItem1;
        internal DevComponents.DotNetBar.ComboBoxItem cmbCompanyFilter;
        internal DevComponents.DotNetBar.ButtonItem HelpToolStripButton;
        internal DevComponents.DotNetBar.CustomizeItem CustomizeItem1;
        private DevComponents.DotNetBar.PanelEx pnlLeft;
        internal DevComponents.DotNetBar.ExpandableSplitter expSplitterLeft;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        internal DevComponents.DotNetBar.ExpandableSplitter expandableSplitter1;
        private DevComponents.DotNetBar.PanelEx pnlRight;
        internal DevComponents.DotNetBar.Bar BarNavigation;
        internal DevComponents.DotNetBar.LabelItem ToolStripStatusLabel;
        internal DevComponents.DotNetBar.ItemContainer GridNavigation;
        internal DevComponents.DotNetBar.ComboBoxItem cboPageCount;
        internal DevComponents.DotNetBar.ButtonItem BtnFirst;
        internal DevComponents.DotNetBar.ButtonItem BtnPrevious;
        internal DevComponents.DotNetBar.LabelItem lblPageCount;
        internal DevComponents.DotNetBar.ButtonItem BtnNext;
        internal DevComponents.DotNetBar.ButtonItem BtnLast;
        internal DevComponents.DotNetBar.Controls.GroupPanel GrpPanelDocMap;
        internal DevComponents.DotNetBar.Controls.DataGridViewX dgvDocSummary;
        internal DevComponents.DotNetBar.Controls.DataGridViewX dgvDocGrid;
        internal DevComponents.DotNetBar.Controls.WarningBox wbDocs;
        internal DevComponents.DotNetBar.Controls.GroupPanel gpSearch;
        internal DevComponents.DotNetBar.Controls.GroupPanel gpView;
        internal System.Windows.Forms.RadioButton rbtnAll;
        internal System.Windows.Forms.RadioButton rbtIssue;
        internal System.Windows.Forms.RadioButton rbtReceipt;
        internal DevComponents.DotNetBar.LabelX LabelX4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        internal System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem In;
        private System.Windows.Forms.ToolStripMenuItem Out;
        internal DevComponents.DotNetBar.LabelX lblType;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboOperationType;
        internal DevComponents.DotNetBar.LabelX lblDocumentType;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboDocumentType;
        internal System.Windows.Forms.BindingSource BindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.ComponentModel.BackgroundWorker bck_Wrkr;
        private System.Windows.Forms.DataGridViewImageColumn Img;
        private System.Windows.Forms.DataGridViewImageColumn clmdelete;
        internal DevComponents.DotNetBar.LabelX lblDocumentNumber;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboDocumentNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.Label lblTotalDocs;
        private System.Windows.Forms.Label lblTotalRecords;
        private System.Windows.Forms.DataGridViewImageColumn clmImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmRecordID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmOperationTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDocumentTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDocumentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmType;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmReference;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmStatusID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDocumentNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCustodian;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDocumentStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmBinDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmIssueReason;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmReturnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmRemarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmReceivedFrom;
        private System.Windows.Forms.DataGridViewImageColumn clmPrint;
        internal DevComponents.DotNetBar.Controls.WarningBox wbDocArb;
    }
