﻿namespace MyPayfriend
{
    partial class FrmEmployeeLabourCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label OrganisationLabel;
            System.Windows.Forms.Label FromDateLabel;
            System.Windows.Forms.Label CardPersonNoLabel;
            System.Windows.Forms.Label ToDateLabel1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeeLabourCard));
            this.EmployeeLabourCardBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripDropDownBtnMore = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnReceipt = new System.Windows.Forms.ToolStripMenuItem();
            this.btnIssue = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.RenewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.Grpmain = new System.Windows.Forms.GroupBox();
            this.lblRenewed = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtCardPersonalNo = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.dtpIssueDate = new System.Windows.Forms.DateTimePicker();
            this.txtCardNumber = new System.Windows.Forms.TextBox();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ErrorProviderCard = new System.Windows.Forms.ErrorProvider(this.components);
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.lblStatus = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            OrganisationLabel = new System.Windows.Forms.Label();
            FromDateLabel = new System.Windows.Forms.Label();
            CardPersonNoLabel = new System.Windows.Forms.Label();
            ToDateLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLabourCardBindingNavigator)).BeginInit();
            this.EmployeeLabourCardBindingNavigator.SuspendLayout();
            this.Grpmain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(17, 25);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(84, 13);
            Label1.TabIndex = 113;
            Label1.Text = "Employee Name";
            // 
            // OrganisationLabel
            // 
            OrganisationLabel.AutoSize = true;
            OrganisationLabel.Location = new System.Drawing.Point(17, 52);
            OrganisationLabel.Name = "OrganisationLabel";
            OrganisationLabel.Size = new System.Drawing.Size(72, 13);
            OrganisationLabel.TabIndex = 8;
            OrganisationLabel.Text = "Card Number ";
            // 
            // FromDateLabel
            // 
            FromDateLabel.AutoSize = true;
            FromDateLabel.Location = new System.Drawing.Point(17, 107);
            FromDateLabel.Name = "FromDateLabel";
            FromDateLabel.Size = new System.Drawing.Size(58, 13);
            FromDateLabel.TabIndex = 14;
            FromDateLabel.Text = "Issue Date";
            // 
            // CardPersonNoLabel
            // 
            CardPersonNoLabel.AutoSize = true;
            CardPersonNoLabel.Location = new System.Drawing.Point(17, 78);
            CardPersonNoLabel.Name = "CardPersonNoLabel";
            CardPersonNoLabel.Size = new System.Drawing.Size(88, 13);
            CardPersonNoLabel.TabIndex = 12;
            CardPersonNoLabel.Text = "Personal Number";
            // 
            // ToDateLabel1
            // 
            ToDateLabel1.AutoSize = true;
            ToDateLabel1.Location = new System.Drawing.Point(255, 107);
            ToDateLabel1.Name = "ToDateLabel1";
            ToDateLabel1.Size = new System.Drawing.Size(61, 13);
            ToDateLabel1.TabIndex = 44;
            ToDateLabel1.Text = "Expiry Date";
            // 
            // EmployeeLabourCardBindingNavigator
            // 
            this.EmployeeLabourCardBindingNavigator.AddNewItem = null;
            this.EmployeeLabourCardBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeeLabourCardBindingNavigator.DeleteItem = null;
            this.EmployeeLabourCardBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.btnClear,
            this.ToolStripSeparator1,
            this.ToolStripDropDownBtnMore,
            this.ToolStripSeparator3,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.EmployeeLabourCardBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeLabourCardBindingNavigator.MoveFirstItem = null;
            this.EmployeeLabourCardBindingNavigator.MoveLastItem = null;
            this.EmployeeLabourCardBindingNavigator.MoveNextItem = null;
            this.EmployeeLabourCardBindingNavigator.MovePreviousItem = null;
            this.EmployeeLabourCardBindingNavigator.Name = "EmployeeLabourCardBindingNavigator";
            this.EmployeeLabourCardBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeeLabourCardBindingNavigator.Size = new System.Drawing.Size(465, 25);
            this.EmployeeLabourCardBindingNavigator.TabIndex = 1;
            this.EmployeeLabourCardBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save ";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripDropDownBtnMore
            // 
            this.ToolStripDropDownBtnMore.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDocument,
            this.ToolStripSeparator5,
            this.btnReceipt,
            this.btnIssue,
            this.toolStripSeparator6,
            this.RenewToolStripMenuItem});
            this.ToolStripDropDownBtnMore.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.ToolStripDropDownBtnMore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripDropDownBtnMore.Name = "ToolStripDropDownBtnMore";
            this.ToolStripDropDownBtnMore.Size = new System.Drawing.Size(76, 22);
            this.ToolStripDropDownBtnMore.Text = "&Actions";
            this.ToolStripDropDownBtnMore.ToolTipText = "More Actions";
            // 
            // btnDocument
            // 
            this.btnDocument.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.btnDocument.Name = "btnDocument";
            this.btnDocument.Size = new System.Drawing.Size(135, 22);
            this.btnDocument.Text = "&Documents";
            this.btnDocument.Click += new System.EventHandler(this.btnDocument_Click);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(132, 6);
            // 
            // btnReceipt
            // 
            this.btnReceipt.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.btnReceipt.Name = "btnReceipt";
            this.btnReceipt.Size = new System.Drawing.Size(135, 22);
            this.btnReceipt.Text = "R&eceipt";
            this.btnReceipt.Click += new System.EventHandler(this.btnReceipt_Click);
            // 
            // btnIssue
            // 
            this.btnIssue.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.btnIssue.Name = "btnIssue";
            this.btnIssue.Size = new System.Drawing.Size(135, 22);
            this.btnIssue.Text = "&Issue";
            this.btnIssue.Click += new System.EventHandler(this.btnIssue_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(132, 6);
            // 
            // RenewToolStripMenuItem
            // 
            this.RenewToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.renew;
            this.RenewToolStripMenuItem.Name = "RenewToolStripMenuItem";
            this.RenewToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.RenewToolStripMenuItem.Text = "Re&new";
            this.RenewToolStripMenuItem.Click += new System.EventHandler(this.RenewToolStripMenuItem_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Scan";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // Grpmain
            // 
            this.Grpmain.Controls.Add(this.lblRenewed);
            this.Grpmain.Controls.Add(Label1);
            this.Grpmain.Controls.Add(this.txtRemarks);
            this.Grpmain.Controls.Add(this.txtCardPersonalNo);
            this.Grpmain.Controls.Add(this.Label3);
            this.Grpmain.Controls.Add(this.Label6);
            this.Grpmain.Controls.Add(OrganisationLabel);
            this.Grpmain.Controls.Add(this.dtpExpiryDate);
            this.Grpmain.Controls.Add(this.dtpIssueDate);
            this.Grpmain.Controls.Add(FromDateLabel);
            this.Grpmain.Controls.Add(CardPersonNoLabel);
            this.Grpmain.Controls.Add(this.txtCardNumber);
            this.Grpmain.Controls.Add(this.cboEmployee);
            this.Grpmain.Controls.Add(ToDateLabel1);
            this.Grpmain.Controls.Add(this.ShapeContainer1);
            this.Grpmain.Location = new System.Drawing.Point(2, 54);
            this.Grpmain.Name = "Grpmain";
            this.Grpmain.Size = new System.Drawing.Size(451, 279);
            this.Grpmain.TabIndex = 56;
            this.Grpmain.TabStop = false;
            // 
            // lblRenewed
            // 
            this.lblRenewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenewed.ForeColor = System.Drawing.Color.Maroon;
            this.lblRenewed.Location = new System.Drawing.Point(367, 136);
            this.lblRenewed.Name = "lblRenewed";
            this.lblRenewed.Size = new System.Drawing.Size(71, 22);
            this.lblRenewed.TabIndex = 117;
            this.lblRenewed.Text = "Renewed";
            this.lblRenewed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRenewed.Visible = false;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(20, 178);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(421, 92);
            this.txtRemarks.TabIndex = 6;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // txtCardPersonalNo
            // 
            this.txtCardPersonalNo.BackColor = System.Drawing.SystemColors.Window;
            this.txtCardPersonalNo.Location = new System.Drawing.Point(124, 74);
            this.txtCardPersonalNo.MaxLength = 50;
            this.txtCardPersonalNo.Name = "txtCardPersonalNo";
            this.txtCardPersonalNo.Size = new System.Drawing.Size(234, 20);
            this.txtCardPersonalNo.TabIndex = 2;
            this.txtCardPersonalNo.TextChanged += new System.EventHandler(this.txtCardPersonalNo_TextChanged);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(9, 154);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(58, 13);
            this.Label3.TabIndex = 36;
            this.Label3.Text = "Remarks";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(9, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(75, 13);
            this.Label6.TabIndex = 36;
            this.Label6.Text = "Card Details";
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(330, 103);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.Size = new System.Drawing.Size(105, 20);
            this.dtpExpiryDate.TabIndex = 4;
            this.dtpExpiryDate.ValueChanged += new System.EventHandler(this.dtpExpiryDate_ValueCahenged);
            // 
            // dtpIssueDate
            // 
            this.dtpIssueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssueDate.Location = new System.Drawing.Point(124, 103);
            this.dtpIssueDate.Name = "dtpIssueDate";
            this.dtpIssueDate.Size = new System.Drawing.Size(105, 20);
            this.dtpIssueDate.TabIndex = 3;
            this.dtpIssueDate.ValueChanged += new System.EventHandler(this.dtpIssueDate_ValueChanged);
            // 
            // txtCardNumber
            // 
            this.txtCardNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtCardNumber.Location = new System.Drawing.Point(124, 48);
            this.txtCardNumber.MaxLength = 50;
            this.txtCardNumber.Name = "txtCardNumber";
            this.txtCardNumber.Size = new System.Drawing.Size(234, 20);
            this.txtCardNumber.TabIndex = 1;
            this.txtCardNumber.TextChanged += new System.EventHandler(this.txtCardNumber_TextChanged);
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(124, 21);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(308, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboEmployee_KeyPress);
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployee_KeyDown);
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape2});
            this.ShapeContainer1.Size = new System.Drawing.Size(445, 260);
            this.ShapeContainer1.TabIndex = 105;
            this.ShapeContainer1.TabStop = false;
            // 
            // LineShape2
            // 
            this.LineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape2.Name = "LineShape2";
            this.LineShape2.X1 = 35;
            this.LineShape2.X2 = 435;
            this.LineShape2.Y1 = 146;
            this.LineShape2.Y2 = 146;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(5, 343);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 57;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(378, 343);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 59;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(297, 343);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 58;
            this.btnOK.Text = "&Ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Location = new System.Drawing.Point(0, 371);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(465, 22);
            this.StatusStrip1.TabIndex = 61;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ErrorProviderCard
            // 
            this.ErrorProviderCard.ContainerControl = this;
            this.ErrorProviderCard.RightToLeft = true;
            // 
            // Timer1
            // 
            this.Timer1.Interval = 5000;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(465, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 90;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name/Code && Card Number";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(-1, 376);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 91;
            // 
            // FrmEmployeeLabourCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(465, 393);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.Grpmain);
            this.Controls.Add(this.EmployeeLabourCardBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEmployeeLabourCard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Labour Card";
            this.Load += new System.EventHandler(this.FrmEmployeeLabourCard_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LabourCard_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmEmployeeLabourCard_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLabourCardBindingNavigator)).EndInit();
            this.EmployeeLabourCardBindingNavigator.ResumeLayout(false);
            this.EmployeeLabourCardBindingNavigator.PerformLayout();
            this.Grpmain.ResumeLayout(false);
            this.Grpmain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator EmployeeLabourCardBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripDropDownButton ToolStripDropDownBtnMore;
        internal System.Windows.Forms.ToolStripMenuItem btnDocument;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripMenuItem btnReceipt;
        internal System.Windows.Forms.ToolStripMenuItem btnIssue;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.GroupBox Grpmain;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.TextBox txtCardPersonalNo;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.DateTimePicker dtpExpiryDate;
        internal System.Windows.Forms.DateTimePicker dtpIssueDate;
        internal System.Windows.Forms.TextBox txtCardNumber;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape2;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ErrorProvider ErrorProviderCard;
        internal System.Windows.Forms.Timer Timer1;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem RenewToolStripMenuItem;
        private System.Windows.Forms.Label lblRenewed;
        private System.Windows.Forms.Label lblStatus;
    }
}