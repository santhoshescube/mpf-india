﻿
partial class frmDocumentReceiptIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDocumentReceiptIssue));
            this.tmDocuments = new System.Windows.Forms.Timer(this.components);
            this.DocumentsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.errDocuments = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblReceiptIssueHeading = new System.Windows.Forms.Label();
            this.txtDocumentNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboDocumentType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnActions = new DevComponents.DotNetBar.ButtonItem();
            this.btnInstallments = new DevComponents.DotNetBar.ButtonItem();
            this.btnExpense = new DevComponents.DotNetBar.ButtonItem();
            this.btnApprove = new DevComponents.DotNetBar.ButtonItem();
            this.btnReject = new DevComponents.DotNetBar.ButtonItem();
            this.btnDocuments = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem8 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem10 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem11 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblReceivedBy = new System.Windows.Forms.Label();
            this.cboBinNumber = new System.Windows.Forms.ComboBox();
            this.lblBinNumber = new System.Windows.Forms.Label();
            this.cboDocumentStatus = new System.Windows.Forms.ComboBox();
            this.dtpExpectedReturnDate = new System.Windows.Forms.DateTimePicker();
            this.lblDocumentStatus = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.cboCustodian = new System.Windows.Forms.ComboBox();
            this.dtpTransactionDate = new System.Windows.Forms.DateTimePicker();
            this.lblTransactionDate = new System.Windows.Forms.Label();
            this.lblExpRetDate = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.rdbEmployee = new System.Windows.Forms.RadioButton();
            this.lblTypeName = new System.Windows.Forms.Label();
            this.rdbCompany = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.cboReferenceNumber = new System.Windows.Forms.ComboBox();
            this.btnReceiptIssueReason = new System.Windows.Forms.Button();
            this.btnBinNumber = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentsBindingNavigator)).BeginInit();
            this.DocumentsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errDocuments)).BeginInit();
            this.ssStatus.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(6, 257);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(58, 13);
            label5.TabIndex = 141;
            label5.Text = "Remarks";
            // 
            // DocumentsBindingNavigator
            // 
            this.DocumentsBindingNavigator.AddNewItem = null;
            this.DocumentsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.DocumentsBindingNavigator.CountItem = null;
            this.DocumentsBindingNavigator.DeleteItem = null;
            this.DocumentsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnSaveItem,
            this.bnPrint,
            this.toolStripSeparator1});
            this.DocumentsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.DocumentsBindingNavigator.MoveFirstItem = null;
            this.DocumentsBindingNavigator.MoveLastItem = null;
            this.DocumentsBindingNavigator.MoveNextItem = null;
            this.DocumentsBindingNavigator.MovePreviousItem = null;
            this.DocumentsBindingNavigator.Name = "DocumentsBindingNavigator";
            this.DocumentsBindingNavigator.PositionItem = null;
            this.DocumentsBindingNavigator.Size = new System.Drawing.Size(483, 25);
            this.DocumentsBindingNavigator.TabIndex = 4;
            this.DocumentsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(51, 22);
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.ToolTipText = "Save Item";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnPrint
            // 
            this.bnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(52, 22);
            this.bnPrint.Text = "Print";
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // errDocuments
            // 
            this.errDocuments.ContainerControl = this;
            this.errDocuments.RightToLeft = true;
            // 
            // lblReceiptIssueHeading
            // 
            this.lblReceiptIssueHeading.AutoSize = true;
            this.lblReceiptIssueHeading.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceiptIssueHeading.Location = new System.Drawing.Point(6, 0);
            this.lblReceiptIssueHeading.Name = "lblReceiptIssueHeading";
            this.lblReceiptIssueHeading.Size = new System.Drawing.Size(76, 13);
            this.lblReceiptIssueHeading.TabIndex = 60;
            this.lblReceiptIssueHeading.Text = "Receipt Info";
            // 
            // txtDocumentNumber
            // 
            this.txtDocumentNumber.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtDocumentNumber.Location = new System.Drawing.Point(123, 85);
            this.txtDocumentNumber.MaxLength = 30;
            this.txtDocumentNumber.Name = "txtDocumentNumber";
            this.txtDocumentNumber.Size = new System.Drawing.Size(328, 20);
            this.txtDocumentNumber.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Document Number";
            // 
            // cboDocumentType
            // 
            this.cboDocumentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDocumentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDocumentType.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cboDocumentType.DropDownHeight = 134;
            this.cboDocumentType.FormattingEnabled = true;
            this.cboDocumentType.IntegralHeight = false;
            this.cboDocumentType.Items.AddRange(new object[] {
            "Passport"});
            this.cboDocumentType.Location = new System.Drawing.Point(123, 57);
            this.cboDocumentType.Name = "cboDocumentType";
            this.cboDocumentType.Size = new System.Drawing.Size(328, 21);
            this.cboDocumentType.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Document Type";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(319, 423);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 16;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(400, 423);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(8, 422);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnActions
            // 
            this.btnActions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.Name = "btnActions";
            this.btnActions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnInstallments,
            this.btnExpense,
            this.btnApprove,
            this.btnReject,
            this.btnDocuments});
            this.btnActions.Text = "Actions";
            this.btnActions.Tooltip = "Actions";
            // 
            // btnInstallments
            // 
            this.btnInstallments.Name = "btnInstallments";
            this.btnInstallments.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.btnInstallments.Text = "Installments";
            this.btnInstallments.Tooltip = "Installments";
            // 
            // btnExpense
            // 
            this.btnExpense.Image = ((System.Drawing.Image)(resources.GetObject("btnExpense.Image")));
            this.btnExpense.Name = "btnExpense";
            this.btnExpense.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.btnExpense.Text = "Expense";
            this.btnExpense.Tooltip = "Expense";
            // 
            // btnApprove
            // 
            this.btnApprove.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnApprove.Text = "Approve";
            this.btnApprove.Tooltip = "Approve";
            // 
            // btnReject
            // 
            this.btnReject.Name = "btnReject";
            this.btnReject.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.btnReject.Text = "Reject";
            // 
            // btnDocuments
            // 
            this.btnDocuments.Name = "btnDocuments";
            this.btnDocuments.Text = "Documents";
            this.btnDocuments.Tooltip = "Documents";
            // 
            // buttonItem1
            // 
            this.buttonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem1.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem1.Image")));
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2,
            this.buttonItem3,
            this.buttonItem4,
            this.buttonItem5,
            this.buttonItem6});
            this.buttonItem1.Text = "Actions";
            this.buttonItem1.Tooltip = "Actions";
            // 
            // buttonItem2
            // 
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.buttonItem2.Text = "Installments";
            this.buttonItem2.Tooltip = "Installments";
            // 
            // buttonItem3
            // 
            this.buttonItem3.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem3.Image")));
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.buttonItem3.Text = "Expense";
            this.buttonItem3.Tooltip = "Expense";
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonItem4.Text = "Approve";
            this.buttonItem4.Tooltip = "Approve";
            // 
            // buttonItem5
            // 
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.buttonItem5.Text = "Reject";
            // 
            // buttonItem6
            // 
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.Text = "Documents";
            this.buttonItem6.Tooltip = "Documents";
            // 
            // buttonItem7
            // 
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem7.Image")));
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem8,
            this.buttonItem9,
            this.buttonItem10,
            this.buttonItem11,
            this.buttonItem12});
            this.buttonItem7.Text = "Actions";
            this.buttonItem7.Tooltip = "Actions";
            // 
            // buttonItem8
            // 
            this.buttonItem8.Name = "buttonItem8";
            this.buttonItem8.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.buttonItem8.Text = "Installments";
            this.buttonItem8.Tooltip = "Installments";
            // 
            // buttonItem9
            // 
            this.buttonItem9.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem9.Image")));
            this.buttonItem9.Name = "buttonItem9";
            this.buttonItem9.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.buttonItem9.Text = "Expense";
            this.buttonItem9.Tooltip = "Expense";
            // 
            // buttonItem10
            // 
            this.buttonItem10.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem10.Name = "buttonItem10";
            this.buttonItem10.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonItem10.Text = "Approve";
            this.buttonItem10.Tooltip = "Approve";
            // 
            // buttonItem11
            // 
            this.buttonItem11.Name = "buttonItem11";
            this.buttonItem11.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.buttonItem11.Text = "Reject";
            // 
            // buttonItem12
            // 
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.Text = "Documents";
            this.buttonItem12.Tooltip = "Documents";
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1,
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 454);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(483, 22);
            this.ssStatus.TabIndex = 311;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lblReceivedBy
            // 
            this.lblReceivedBy.AutoSize = true;
            this.lblReceivedBy.Location = new System.Drawing.Point(17, 145);
            this.lblReceivedBy.Name = "lblReceivedBy";
            this.lblReceivedBy.Size = new System.Drawing.Size(68, 13);
            this.lblReceivedBy.TabIndex = 130;
            this.lblReceivedBy.Text = "Received By";
            // 
            // cboBinNumber
            // 
            this.cboBinNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBinNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBinNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboBinNumber.DropDownHeight = 134;
            this.cboBinNumber.FormattingEnabled = true;
            this.cboBinNumber.IntegralHeight = false;
            this.cboBinNumber.Location = new System.Drawing.Point(123, 199);
            this.cboBinNumber.Name = "cboBinNumber";
            this.cboBinNumber.Size = new System.Drawing.Size(290, 21);
            this.cboBinNumber.TabIndex = 8;
            this.cboBinNumber.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // lblBinNumber
            // 
            this.lblBinNumber.AutoSize = true;
            this.lblBinNumber.Location = new System.Drawing.Point(17, 202);
            this.lblBinNumber.Name = "lblBinNumber";
            this.lblBinNumber.Size = new System.Drawing.Size(62, 13);
            this.lblBinNumber.TabIndex = 136;
            this.lblBinNumber.Text = "Bin Number";
            // 
            // cboDocumentStatus
            // 
            this.cboDocumentStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDocumentStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDocumentStatus.BackColor = System.Drawing.SystemColors.Info;
            this.cboDocumentStatus.DropDownHeight = 134;
            this.cboDocumentStatus.FormattingEnabled = true;
            this.cboDocumentStatus.IntegralHeight = false;
            this.cboDocumentStatus.Location = new System.Drawing.Point(123, 170);
            this.cboDocumentStatus.Name = "cboDocumentStatus";
            this.cboDocumentStatus.Size = new System.Drawing.Size(290, 21);
            this.cboDocumentStatus.TabIndex = 6;
            this.cboDocumentStatus.SelectionChangeCommitted += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpExpectedReturnDate
            // 
            this.dtpExpectedReturnDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpectedReturnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpectedReturnDate.Location = new System.Drawing.Point(352, 232);
            this.dtpExpectedReturnDate.Name = "dtpExpectedReturnDate";
            this.dtpExpectedReturnDate.ShowCheckBox = true;
            this.dtpExpectedReturnDate.Size = new System.Drawing.Size(99, 20);
            this.dtpExpectedReturnDate.TabIndex = 11;
            this.dtpExpectedReturnDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // lblDocumentStatus
            // 
            this.lblDocumentStatus.AutoSize = true;
            this.lblDocumentStatus.Location = new System.Drawing.Point(17, 173);
            this.lblDocumentStatus.Name = "lblDocumentStatus";
            this.lblDocumentStatus.Size = new System.Drawing.Size(89, 13);
            this.lblDocumentStatus.TabIndex = 140;
            this.lblDocumentStatus.Text = "Document Status";
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 32;
            this.lineShape2.X2 = 459;
            this.lineShape2.Y1 = 248;
            this.lineShape2.Y2 = 248;
            // 
            // cboCustodian
            // 
            this.cboCustodian.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCustodian.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCustodian.BackColor = System.Drawing.SystemColors.Info;
            this.cboCustodian.DropDownHeight = 134;
            this.cboCustodian.FormattingEnabled = true;
            this.cboCustodian.IntegralHeight = false;
            this.cboCustodian.Location = new System.Drawing.Point(123, 141);
            this.cboCustodian.Name = "cboCustodian";
            this.cboCustodian.Size = new System.Drawing.Size(328, 21);
            this.cboCustodian.TabIndex = 5;
            this.cboCustodian.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpTransactionDate
            // 
            this.dtpTransactionDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTransactionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransactionDate.Location = new System.Drawing.Point(123, 230);
            this.dtpTransactionDate.Name = "dtpTransactionDate";
            this.dtpTransactionDate.Size = new System.Drawing.Size(107, 20);
            this.dtpTransactionDate.TabIndex = 10;
            this.dtpTransactionDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // lblTransactionDate
            // 
            this.lblTransactionDate.AutoSize = true;
            this.lblTransactionDate.Location = new System.Drawing.Point(17, 232);
            this.lblTransactionDate.Name = "lblTransactionDate";
            this.lblTransactionDate.Size = new System.Drawing.Size(70, 13);
            this.lblTransactionDate.TabIndex = 125;
            this.lblTransactionDate.Text = "Receipt Date";
            // 
            // lblExpRetDate
            // 
            this.lblExpRetDate.AutoSize = true;
            this.lblExpRetDate.Location = new System.Drawing.Point(236, 232);
            this.lblExpRetDate.Name = "lblExpRetDate";
            this.lblExpRetDate.Size = new System.Drawing.Size(113, 13);
            this.lblExpRetDate.TabIndex = 144;
            this.lblExpRetDate.Text = "Expected Return Date";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(20, 282);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(431, 81);
            this.txtRemarks.TabIndex = 12;
            this.txtRemarks.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // rdbEmployee
            // 
            this.rdbEmployee.AutoSize = true;
            this.rdbEmployee.Location = new System.Drawing.Point(123, 30);
            this.rdbEmployee.Name = "rdbEmployee";
            this.rdbEmployee.Size = new System.Drawing.Size(71, 17);
            this.rdbEmployee.TabIndex = 318;
            this.rdbEmployee.Text = "Employee";
            this.rdbEmployee.UseVisualStyleBackColor = true;
            // 
            // lblTypeName
            // 
            this.lblTypeName.AutoSize = true;
            this.lblTypeName.Location = new System.Drawing.Point(17, 116);
            this.lblTypeName.Name = "lblTypeName";
            this.lblTypeName.Size = new System.Drawing.Size(79, 13);
            this.lblTypeName.TabIndex = 317;
            this.lblTypeName.Text = "Received From";
            // 
            // rdbCompany
            // 
            this.rdbCompany.AutoSize = true;
            this.rdbCompany.Location = new System.Drawing.Point(200, 30);
            this.rdbCompany.Name = "rdbCompany";
            this.rdbCompany.Size = new System.Drawing.Size(69, 17);
            this.rdbCompany.TabIndex = 319;
            this.rdbCompany.Text = "Company";
            this.rdbCompany.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 17);
            this.label3.TabIndex = 316;
            this.label3.Text = "Type";
            this.label3.UseCompatibleTextRendering = true;
            // 
            // cboReferenceNumber
            // 
            this.cboReferenceNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReferenceNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReferenceNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboReferenceNumber.DropDownHeight = 134;
            this.cboReferenceNumber.FormattingEnabled = true;
            this.cboReferenceNumber.IntegralHeight = false;
            this.cboReferenceNumber.Location = new System.Drawing.Point(123, 113);
            this.cboReferenceNumber.Name = "cboReferenceNumber";
            this.cboReferenceNumber.Size = new System.Drawing.Size(328, 21);
            this.cboReferenceNumber.TabIndex = 4;
            this.cboReferenceNumber.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // btnReceiptIssueReason
            // 
            this.btnReceiptIssueReason.Location = new System.Drawing.Point(419, 170);
            this.btnReceiptIssueReason.Name = "btnReceiptIssueReason";
            this.btnReceiptIssueReason.Size = new System.Drawing.Size(32, 23);
            this.btnReceiptIssueReason.TabIndex = 7;
            this.btnReceiptIssueReason.Text = "...";
            this.btnReceiptIssueReason.UseVisualStyleBackColor = true;
            this.btnReceiptIssueReason.Click += new System.EventHandler(this.btnReceiptIssueReason_Click);
            // 
            // btnBinNumber
            // 
            this.btnBinNumber.Location = new System.Drawing.Point(419, 199);
            this.btnBinNumber.Name = "btnBinNumber";
            this.btnBinNumber.Size = new System.Drawing.Size(32, 23);
            this.btnBinNumber.TabIndex = 9;
            this.btnBinNumber.Text = "...";
            this.btnBinNumber.UseVisualStyleBackColor = true;
            this.btnBinNumber.Click += new System.EventHandler(this.btnBinNumber_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(label5);
            this.groupBox1.Controls.Add(this.dtpTransactionDate);
            this.groupBox1.Controls.Add(this.dtpExpectedReturnDate);
            this.groupBox1.Controls.Add(this.lblTransactionDate);
            this.groupBox1.Controls.Add(this.lblExpRetDate);
            this.groupBox1.Controls.Add(this.lblReceiptIssueHeading);
            this.groupBox1.Controls.Add(this.btnBinNumber);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnReceiptIssueReason);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cboDocumentType);
            this.groupBox1.Controls.Add(this.rdbEmployee);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblTypeName);
            this.groupBox1.Controls.Add(this.txtDocumentNumber);
            this.groupBox1.Controls.Add(this.rdbCompany);
            this.groupBox1.Controls.Add(this.cboDocumentStatus);
            this.groupBox1.Controls.Add(this.lblBinNumber);
            this.groupBox1.Controls.Add(this.cboReferenceNumber);
            this.groupBox1.Controls.Add(this.cboBinNumber);
            this.groupBox1.Controls.Add(this.lblReceivedBy);
            this.groupBox1.Controls.Add(this.lblDocumentStatus);
            this.groupBox1.Controls.Add(this.cboCustodian);
            this.groupBox1.Controls.Add(this.shapeContainer1);
            this.groupBox1.Location = new System.Drawing.Point(8, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 381);
            this.groupBox1.TabIndex = 321;
            this.groupBox1.TabStop = false;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(461, 362);
            this.shapeContainer1.TabIndex = 321;
            this.shapeContainer1.TabStop = false;
            // 
            // frmDocumentReceiptIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(483, 476);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.DocumentsBindingNavigator);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDocumentReceiptIssue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Documents";
            this.Load += new System.EventHandler(this.frmDocumentReceiptIssue_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDocumentReceiptIssue_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.DocumentsBindingNavigator)).EndInit();
            this.DocumentsBindingNavigator.ResumeLayout(false);
            this.DocumentsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errDocuments)).EndInit();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmDocuments;
        private System.Windows.Forms.BindingNavigator DocumentsBindingNavigator;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        private System.Windows.Forms.ToolStripButton bnPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ErrorProvider errDocuments;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboDocumentType;
        private System.Windows.Forms.TextBox txtDocumentNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblReceiptIssueHeading;
        private DevComponents.DotNetBar.ButtonItem btnActions;
        private DevComponents.DotNetBar.ButtonItem btnInstallments;
        private DevComponents.DotNetBar.ButtonItem btnExpense;
        private DevComponents.DotNetBar.ButtonItem btnApprove;
        private DevComponents.DotNetBar.ButtonItem btnReject;
        private DevComponents.DotNetBar.ButtonItem btnDocuments;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.ButtonItem buttonItem8;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private DevComponents.DotNetBar.ButtonItem buttonItem10;
        private DevComponents.DotNetBar.ButtonItem buttonItem11;
        private DevComponents.DotNetBar.ButtonItem buttonItem12;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Label lblDocumentStatus;
        internal System.Windows.Forms.DateTimePicker dtpExpectedReturnDate;
        internal System.Windows.Forms.ComboBox cboDocumentStatus;
        internal System.Windows.Forms.Label lblBinNumber;
        internal System.Windows.Forms.ComboBox cboBinNumber;
        internal System.Windows.Forms.Label lblReceivedBy;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.Label lblExpRetDate;
        internal System.Windows.Forms.Label lblTransactionDate;
        internal System.Windows.Forms.DateTimePicker dtpTransactionDate;
        internal System.Windows.Forms.ComboBox cboCustodian;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.RadioButton rdbEmployee;
        private System.Windows.Forms.Label lblTypeName;
        private System.Windows.Forms.RadioButton rdbCompany;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboReferenceNumber;
        internal System.Windows.Forms.Button btnBinNumber;
        internal System.Windows.Forms.Button btnReceiptIssueReason;
        private System.Windows.Forms.GroupBox groupBox1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
    }
