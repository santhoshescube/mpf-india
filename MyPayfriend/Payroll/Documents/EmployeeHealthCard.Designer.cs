﻿namespace MyPayfriend
{
    partial class EmployeeHealthCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label OrganisationLabel;
            System.Windows.Forms.Label FromDateLabel;
            System.Windows.Forms.Label CardPersonNoLabel;
            System.Windows.Forms.Label ToDateLabel1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmployeeHealthCard));
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.IssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnMail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnSave = new System.Windows.Forms.Button();
            this.LblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.ReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnCancel1 = new System.Windows.Forms.Button();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnScan = new System.Windows.Forms.ToolStripMenuItem();
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.EmployeeHealthCardBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnCancel = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripDropDownBtnMore = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.RenewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.PnlVisa = new System.Windows.Forms.GroupBox();
            this.TxtRemarks = new System.Windows.Forms.TextBox();
            this.TxtCardPersonalNumber = new System.Windows.Forms.TextBox();
            this.TxtCardNumber = new System.Windows.Forms.TextBox();
            this.CboEmployee = new System.Windows.Forms.ComboBox();
            this.lblRenewed = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.DtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.DtpIssueDate = new System.Windows.Forms.DateTimePicker();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            Label1 = new System.Windows.Forms.Label();
            OrganisationLabel = new System.Windows.Forms.Label();
            FromDateLabel = new System.Windows.Forms.Label();
            CardPersonNoLabel = new System.Windows.Forms.Label();
            ToDateLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeHealthCardBindingNavigator)).BeginInit();
            this.EmployeeHealthCardBindingNavigator.SuspendLayout();
            this.PnlVisa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(17, 25);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(84, 13);
            Label1.TabIndex = 113;
            Label1.Text = "Employee Name";
            // 
            // OrganisationLabel
            // 
            OrganisationLabel.AutoSize = true;
            OrganisationLabel.Location = new System.Drawing.Point(17, 52);
            OrganisationLabel.Name = "OrganisationLabel";
            OrganisationLabel.Size = new System.Drawing.Size(72, 13);
            OrganisationLabel.TabIndex = 8;
            OrganisationLabel.Text = "Card Number ";
            // 
            // FromDateLabel
            // 
            FromDateLabel.AutoSize = true;
            FromDateLabel.Location = new System.Drawing.Point(17, 108);
            FromDateLabel.Name = "FromDateLabel";
            FromDateLabel.Size = new System.Drawing.Size(58, 13);
            FromDateLabel.TabIndex = 14;
            FromDateLabel.Text = "Issue Date";
            // 
            // CardPersonNoLabel
            // 
            CardPersonNoLabel.AutoSize = true;
            CardPersonNoLabel.Location = new System.Drawing.Point(17, 78);
            CardPersonNoLabel.Name = "CardPersonNoLabel";
            CardPersonNoLabel.Size = new System.Drawing.Size(88, 13);
            CardPersonNoLabel.TabIndex = 12;
            CardPersonNoLabel.Text = "Personal Number";
            // 
            // ToDateLabel1
            // 
            ToDateLabel1.AutoSize = true;
            ToDateLabel1.Location = new System.Drawing.Point(255, 108);
            ToDateLabel1.Name = "ToDateLabel1";
            ToDateLabel1.Size = new System.Drawing.Size(61, 13);
            ToDateLabel1.TabIndex = 44;
            ToDateLabel1.Text = "Expiry Date";
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // IssueToolStripMenuItem
            // 
            this.IssueToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.IssueToolStripMenuItem.Name = "IssueToolStripMenuItem";
            this.IssueToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.IssueToolStripMenuItem.Text = "&Issue";
            this.IssueToolStripMenuItem.Click += new System.EventHandler(this.IssueToolStripMenuItem_Click);
            // 
            // BtnMail
            // 
            this.BtnMail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnMail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.BtnMail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnMail.Name = "BtnMail";
            this.BtnMail.Size = new System.Drawing.Size(23, 22);
            this.BtnMail.Text = "Email";
            this.BtnMail.Click += new System.EventHandler(this.BtnMail_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Fax";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(4, 347);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 6;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // LblStatus
            // 
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // ErrorProvider
            // 
            this.ErrorProvider.ContainerControl = this;
            this.ErrorProvider.RightToLeft = true;
            // 
            // ReceiptToolStripMenuItem
            // 
            this.ReceiptToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.ReceiptToolStripMenuItem.Name = "ReceiptToolStripMenuItem";
            this.ReceiptToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.ReceiptToolStripMenuItem.Text = "R&eceipt";
            this.ReceiptToolStripMenuItem.Click += new System.EventHandler(this.ReceiptToolStripMenuItem_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 376);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(464, 22);
            this.StatusStrip1.TabIndex = 71;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnOk.Location = new System.Drawing.Point(301, 347);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 7;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnCancel1
            // 
            this.BtnCancel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnCancel1.Location = new System.Drawing.Point(382, 347);
            this.BtnCancel1.Name = "BtnCancel1";
            this.BtnCancel1.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel1.TabIndex = 8;
            this.BtnCancel1.Text = "&Cancel";
            this.BtnCancel1.UseVisualStyleBackColor = true;
            this.BtnCancel1.Click += new System.EventHandler(this.BtnCancel1_Click);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(132, 6);
            // 
            // BtnScan
            // 
            this.BtnScan.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.BtnScan.Name = "BtnScan";
            this.BtnScan.Size = new System.Drawing.Size(135, 22);
            this.BtnScan.Text = "&Documents";
            this.BtnScan.Click += new System.EventHandler(this.BtnScan_Click);
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // EmployeeHealthCardBindingNavigator
            // 
            this.EmployeeHealthCardBindingNavigator.AddNewItem = null;
            this.EmployeeHealthCardBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeeHealthCardBindingNavigator.DeleteItem = null;
            this.EmployeeHealthCardBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.ToolStripSeparator1,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnCancel,
            this.BindingNavigatorSeparator2,
            this.ToolStripDropDownBtnMore,
            this.ToolStripSeparator3,
            this.BtnPrint,
            this.BtnMail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.EmployeeHealthCardBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeHealthCardBindingNavigator.MoveFirstItem = null;
            this.EmployeeHealthCardBindingNavigator.MoveLastItem = null;
            this.EmployeeHealthCardBindingNavigator.MoveNextItem = null;
            this.EmployeeHealthCardBindingNavigator.MovePreviousItem = null;
            this.EmployeeHealthCardBindingNavigator.Name = "EmployeeHealthCardBindingNavigator";
            this.EmployeeHealthCardBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeeHealthCardBindingNavigator.Size = new System.Drawing.Size(464, 25);
            this.EmployeeHealthCardBindingNavigator.TabIndex = 67;
            this.EmployeeHealthCardBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnCancel.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.BtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(23, 22);
            this.BtnCancel.Text = "Clear";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripDropDownBtnMore
            // 
            this.ToolStripDropDownBtnMore.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnScan,
            this.ToolStripSeparator5,
            this.ReceiptToolStripMenuItem,
            this.IssueToolStripMenuItem,
            this.toolStripSeparator6,
            this.RenewToolStripMenuItem});
            this.ToolStripDropDownBtnMore.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.ToolStripDropDownBtnMore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripDropDownBtnMore.Name = "ToolStripDropDownBtnMore";
            this.ToolStripDropDownBtnMore.Size = new System.Drawing.Size(76, 22);
            this.ToolStripDropDownBtnMore.Text = "&Actions";
            this.ToolStripDropDownBtnMore.ToolTipText = "More Actions";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(132, 6);
            // 
            // RenewToolStripMenuItem
            // 
            this.RenewToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.renew;
            this.RenewToolStripMenuItem.Name = "RenewToolStripMenuItem";
            this.RenewToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.RenewToolStripMenuItem.Text = "Re&new";
            this.RenewToolStripMenuItem.Click += new System.EventHandler(this.RenewToolStripMenuItem_Click);
            // 
            // Timer
            // 
            this.Timer.Interval = 2000;
            // 
            // PnlVisa
            // 
            this.PnlVisa.Controls.Add(this.TxtRemarks);
            this.PnlVisa.Controls.Add(this.TxtCardPersonalNumber);
            this.PnlVisa.Controls.Add(this.TxtCardNumber);
            this.PnlVisa.Controls.Add(this.CboEmployee);
            this.PnlVisa.Controls.Add(this.lblRenewed);
            this.PnlVisa.Controls.Add(Label1);
            this.PnlVisa.Controls.Add(this.Label3);
            this.PnlVisa.Controls.Add(this.Label6);
            this.PnlVisa.Controls.Add(OrganisationLabel);
            this.PnlVisa.Controls.Add(this.DtpExpiryDate);
            this.PnlVisa.Controls.Add(this.DtpIssueDate);
            this.PnlVisa.Controls.Add(FromDateLabel);
            this.PnlVisa.Controls.Add(CardPersonNoLabel);
            this.PnlVisa.Controls.Add(ToDateLabel1);
            this.PnlVisa.Controls.Add(this.ShapeContainer1);
            this.PnlVisa.Location = new System.Drawing.Point(4, 55);
            this.PnlVisa.Name = "PnlVisa";
            this.PnlVisa.Size = new System.Drawing.Size(451, 284);
            this.PnlVisa.TabIndex = 72;
            this.PnlVisa.TabStop = false;
            // 
            // TxtRemarks
            // 
            this.TxtRemarks.Location = new System.Drawing.Point(20, 183);
            this.TxtRemarks.MaxLength = 500;
            this.TxtRemarks.Multiline = true;
            this.TxtRemarks.Name = "TxtRemarks";
            this.TxtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TxtRemarks.Size = new System.Drawing.Size(421, 92);
            this.TxtRemarks.TabIndex = 5;
            this.TxtRemarks.TextChanged += new System.EventHandler(this.TxtRemarks_TextChanged);
            // 
            // TxtCardPersonalNumber
            // 
            this.TxtCardPersonalNumber.BackColor = System.Drawing.SystemColors.Window;
            this.TxtCardPersonalNumber.Location = new System.Drawing.Point(124, 78);
            this.TxtCardPersonalNumber.MaxLength = 50;
            this.TxtCardPersonalNumber.Name = "TxtCardPersonalNumber";
            this.TxtCardPersonalNumber.Size = new System.Drawing.Size(229, 20);
            this.TxtCardPersonalNumber.TabIndex = 2;
            this.TxtCardPersonalNumber.TextChanged += new System.EventHandler(this.TxtCardPersonalNumber_TextChanged);
            // 
            // TxtCardNumber
            // 
            this.TxtCardNumber.BackColor = System.Drawing.SystemColors.Info;
            this.TxtCardNumber.Location = new System.Drawing.Point(124, 49);
            this.TxtCardNumber.MaxLength = 50;
            this.TxtCardNumber.Name = "TxtCardNumber";
            this.TxtCardNumber.Size = new System.Drawing.Size(229, 20);
            this.TxtCardNumber.TabIndex = 1;
            this.TxtCardNumber.TextChanged += new System.EventHandler(this.TxtCardNumber_TextChanged);
            // 
            // CboEmployee
            // 
            this.CboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.CboEmployee.DisplayMember = "EmployeeID";
            this.CboEmployee.DropDownHeight = 134;
            this.CboEmployee.FormattingEnabled = true;
            this.CboEmployee.IntegralHeight = false;
            this.CboEmployee.Location = new System.Drawing.Point(124, 19);
            this.CboEmployee.Name = "CboEmployee";
            this.CboEmployee.Size = new System.Drawing.Size(306, 21);
            this.CboEmployee.TabIndex = 0;
            this.CboEmployee.ValueMember = "EmployeeID";
            this.CboEmployee.SelectedIndexChanged += new System.EventHandler(this.CboEmployee_SelectedIndexChanged);
            this.CboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboEmployee_KeyPress);
            // 
            // lblRenewed
            // 
            this.lblRenewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenewed.ForeColor = System.Drawing.Color.Maroon;
            this.lblRenewed.Location = new System.Drawing.Point(367, 136);
            this.lblRenewed.Name = "lblRenewed";
            this.lblRenewed.Size = new System.Drawing.Size(71, 22);
            this.lblRenewed.TabIndex = 117;
            this.lblRenewed.Text = "Renewed";
            this.lblRenewed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRenewed.Visible = false;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(9, 154);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(58, 13);
            this.Label3.TabIndex = 36;
            this.Label3.Text = "Remarks";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(9, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(75, 13);
            this.Label6.TabIndex = 36;
            this.Label6.Text = "Card Details";
            // 
            // DtpExpiryDate
            // 
            this.DtpExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpExpiryDate.Location = new System.Drawing.Point(330, 106);
            this.DtpExpiryDate.Name = "DtpExpiryDate";
            this.DtpExpiryDate.Size = new System.Drawing.Size(105, 20);
            this.DtpExpiryDate.TabIndex = 4;
            this.DtpExpiryDate.ValueChanged += new System.EventHandler(this.DtpExpiryDate_ValueChanged);
            // 
            // DtpIssueDate
            // 
            this.DtpIssueDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpIssueDate.Location = new System.Drawing.Point(124, 106);
            this.DtpIssueDate.Name = "DtpIssueDate";
            this.DtpIssueDate.Size = new System.Drawing.Size(105, 20);
            this.DtpIssueDate.TabIndex = 3;
            this.DtpIssueDate.ValueChanged += new System.EventHandler(this.DtpIssueDate_ValueChanged);
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape2});
            this.ShapeContainer1.Size = new System.Drawing.Size(445, 265);
            this.ShapeContainer1.TabIndex = 105;
            this.ShapeContainer1.TabStop = false;
            // 
            // LineShape2
            // 
            this.LineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape2.Name = "LineShape2";
            this.LineShape2.X1 = 35;
            this.LineShape2.X2 = 435;
            this.LineShape2.Y1 = 146;
            this.LineShape2.Y2 = 146;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(464, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 91;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name/Code && Card Number";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // EmployeeHealthCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(464, 398);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.PnlVisa);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnCancel1);
            this.Controls.Add(this.EmployeeHealthCardBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmployeeHealthCard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmployeeHealthCard";
            this.Load += new System.EventHandler(this.EmployeeHealthCard_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EmployeeHealthCard_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EmployeeHealthCard_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeHealthCardBindingNavigator)).EndInit();
            this.EmployeeHealthCardBindingNavigator.ResumeLayout(false);
            this.EmployeeHealthCardBindingNavigator.PerformLayout();
            this.PnlVisa.ResumeLayout(false);
            this.PnlVisa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripMenuItem IssueToolStripMenuItem;
        internal System.Windows.Forms.ToolStripButton BtnMail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.ToolStripStatusLabel LblStatus;
        internal System.Windows.Forms.ErrorProvider ErrorProvider;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.Button BtnCancel1;
        internal System.Windows.Forms.BindingNavigator EmployeeHealthCardBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BtnCancel;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripDropDownButton ToolStripDropDownBtnMore;
        internal System.Windows.Forms.ToolStripMenuItem BtnScan;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripMenuItem ReceiptToolStripMenuItem;
        internal System.Windows.Forms.Timer Timer;
        internal System.Windows.Forms.GroupBox PnlVisa;
        private System.Windows.Forms.Label lblRenewed;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.DateTimePicker DtpExpiryDate;
        internal System.Windows.Forms.DateTimePicker DtpIssueDate;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape2;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.TextBox TxtCardPersonalNumber;
        internal System.Windows.Forms.TextBox TxtCardNumber;
        internal System.Windows.Forms.ComboBox CboEmployee;
        internal System.Windows.Forms.TextBox TxtRemarks;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem RenewToolStripMenuItem;
    }
}