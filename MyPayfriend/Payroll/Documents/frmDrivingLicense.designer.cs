﻿namespace MyPayfriend
{
    partial class frmDrivingLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label EmployeeIDLabel;
            System.Windows.Forms.Label ExpiryDateLabel;
            System.Windows.Forms.Label IssueDateLabel;
            System.Windows.Forms.Label LicencedCountryIDLabel;
            System.Windows.Forms.Label LicenceTypeIDLabel;
            System.Windows.Forms.Label LicenceHoldersNameLabel;
            System.Windows.Forms.Label LicenceNumberLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDrivingLicense));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.EmployeeLicenseDetailsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.EmployeeLicenseDetailsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripDropDownBtnMore = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuItemAttachDocuments = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.receiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.RenewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnFax = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.GrpMain = new System.Windows.Forms.GroupBox();
            this.lblRenewed = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.dtpIssueDate = new System.Windows.Forms.DateTimePicker();
            this.txtLicenseName = new System.Windows.Forms.TextBox();
            this.txtLicenseNumber = new System.Windows.Forms.TextBox();
            this.BtnCountry = new System.Windows.Forms.Button();
            this.BtnLicencetype = new System.Windows.Forms.Button();
            this.cboLicenseType = new System.Windows.Forms.ComboBox();
            this.cboCountry = new System.Windows.Forms.ComboBox();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.errorDrivingLicense = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblToolStripStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            EmployeeIDLabel = new System.Windows.Forms.Label();
            ExpiryDateLabel = new System.Windows.Forms.Label();
            IssueDateLabel = new System.Windows.Forms.Label();
            LicencedCountryIDLabel = new System.Windows.Forms.Label();
            LicenceTypeIDLabel = new System.Windows.Forms.Label();
            LicenceHoldersNameLabel = new System.Windows.Forms.Label();
            LicenceNumberLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLicenseDetailsBindingNavigator)).BeginInit();
            this.EmployeeLicenseDetailsBindingNavigator.SuspendLayout();
            this.GrpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorDrivingLicense)).BeginInit();
            this.lblToolStripStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // EmployeeIDLabel
            // 
            EmployeeIDLabel.AutoSize = true;
            EmployeeIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            EmployeeIDLabel.Location = new System.Drawing.Point(17, 26);
            EmployeeIDLabel.Name = "EmployeeIDLabel";
            EmployeeIDLabel.Size = new System.Drawing.Size(84, 13);
            EmployeeIDLabel.TabIndex = 4;
            EmployeeIDLabel.Text = "Employee Name";
            // 
            // ExpiryDateLabel
            // 
            ExpiryDateLabel.AutoSize = true;
            ExpiryDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ExpiryDateLabel.Location = new System.Drawing.Point(244, 176);
            ExpiryDateLabel.Name = "ExpiryDateLabel";
            ExpiryDateLabel.Size = new System.Drawing.Size(61, 13);
            ExpiryDateLabel.TabIndex = 18;
            ExpiryDateLabel.Text = "Expiry Date";
            // 
            // IssueDateLabel
            // 
            IssueDateLabel.AutoSize = true;
            IssueDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            IssueDateLabel.Location = new System.Drawing.Point(18, 177);
            IssueDateLabel.Name = "IssueDateLabel";
            IssueDateLabel.Size = new System.Drawing.Size(58, 13);
            IssueDateLabel.TabIndex = 16;
            IssueDateLabel.Text = "Issue Date";
            // 
            // LicencedCountryIDLabel
            // 
            LicencedCountryIDLabel.AutoSize = true;
            LicencedCountryIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            LicencedCountryIDLabel.Location = new System.Drawing.Point(17, 145);
            LicencedCountryIDLabel.Name = "LicencedCountryIDLabel";
            LicencedCountryIDLabel.Size = new System.Drawing.Size(92, 13);
            LicencedCountryIDLabel.TabIndex = 14;
            LicencedCountryIDLabel.Text = "Licensed Country ";
            // 
            // LicenceTypeIDLabel
            // 
            LicenceTypeIDLabel.AutoSize = true;
            LicenceTypeIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            LicenceTypeIDLabel.Location = new System.Drawing.Point(17, 117);
            LicenceTypeIDLabel.Name = "LicenceTypeIDLabel";
            LicenceTypeIDLabel.Size = new System.Drawing.Size(74, 13);
            LicenceTypeIDLabel.TabIndex = 12;
            LicenceTypeIDLabel.Text = "License Type ";
            // 
            // LicenceHoldersNameLabel
            // 
            LicenceHoldersNameLabel.AutoSize = true;
            LicenceHoldersNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            LicenceHoldersNameLabel.Location = new System.Drawing.Point(17, 86);
            LicenceHoldersNameLabel.Name = "LicenceHoldersNameLabel";
            LicenceHoldersNameLabel.Size = new System.Drawing.Size(100, 13);
            LicenceHoldersNameLabel.TabIndex = 10;
            LicenceHoldersNameLabel.Text = "Name as in License";
            // 
            // LicenceNumberLabel
            // 
            LicenceNumberLabel.AutoSize = true;
            LicenceNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            LicenceNumberLabel.Location = new System.Drawing.Point(17, 57);
            LicenceNumberLabel.Name = "LicenceNumberLabel";
            LicenceNumberLabel.Size = new System.Drawing.Size(84, 13);
            LicenceNumberLabel.TabIndex = 8;
            LicenceNumberLabel.Text = "License Number";
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.Location = new System.Drawing.Point(392, 404);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(70, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOK.Location = new System.Drawing.Point(316, 404);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(70, 23);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "&OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.Location = new System.Drawing.Point(9, 404);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // EmployeeLicenseDetailsBindingNavigator
            // 
            this.EmployeeLicenseDetailsBindingNavigator.AddNewItem = null;
            this.EmployeeLicenseDetailsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.EmployeeLicenseDetailsBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.EmployeeLicenseDetailsBindingNavigator.DeleteItem = null;
            this.EmployeeLicenseDetailsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.EmployeeLicenseDetailsBindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.btnClear,
            this.ToolStripSeparator2,
            this.ToolStripDropDownBtnMore,
            this.ToolStripSeparator3,
            this.BtnPrint,
            this.BtnFax,
            this.ToolStripSeparator1,
            this.BtnHelp});
            this.EmployeeLicenseDetailsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeLicenseDetailsBindingNavigator.MoveFirstItem = null;
            this.EmployeeLicenseDetailsBindingNavigator.MoveLastItem = null;
            this.EmployeeLicenseDetailsBindingNavigator.MoveNextItem = null;
            this.EmployeeLicenseDetailsBindingNavigator.MovePreviousItem = null;
            this.EmployeeLicenseDetailsBindingNavigator.Name = "EmployeeLicenseDetailsBindingNavigator";
            this.EmployeeLicenseDetailsBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.EmployeeLicenseDetailsBindingNavigator.Size = new System.Drawing.Size(469, 25);
            this.EmployeeLicenseDetailsBindingNavigator.TabIndex = 58;
            this.EmployeeLicenseDetailsBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // EmployeeLicenseDetailsBindingNavigatorSaveItem
            // 
            this.EmployeeLicenseDetailsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EmployeeLicenseDetailsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("EmployeeLicenseDetailsBindingNavigatorSaveItem.Image")));
            this.EmployeeLicenseDetailsBindingNavigatorSaveItem.Name = "EmployeeLicenseDetailsBindingNavigatorSaveItem";
            this.EmployeeLicenseDetailsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.EmployeeLicenseDetailsBindingNavigatorSaveItem.Text = "Save";
            this.EmployeeLicenseDetailsBindingNavigatorSaveItem.Click += new System.EventHandler(this.EmployeeLicenseDetailsBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripDropDownBtnMore
            // 
            this.ToolStripDropDownBtnMore.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAttachDocuments,
            this.toolStripSeparator4,
            this.receiptToolStripMenuItem,
            this.issueToolStripMenuItem,
            this.toolStripSeparator5,
            this.RenewToolStripMenuItem});
            this.ToolStripDropDownBtnMore.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.ToolStripDropDownBtnMore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripDropDownBtnMore.Name = "ToolStripDropDownBtnMore";
            this.ToolStripDropDownBtnMore.Size = new System.Drawing.Size(76, 22);
            this.ToolStripDropDownBtnMore.Text = "Actions";
            this.ToolStripDropDownBtnMore.ToolTipText = "Actions";
            this.ToolStripDropDownBtnMore.Click += new System.EventHandler(this.ToolStripDropDownBtnMore_Click);
            // 
            // mnuItemAttachDocuments
            // 
            this.mnuItemAttachDocuments.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.mnuItemAttachDocuments.Name = "mnuItemAttachDocuments";
            this.mnuItemAttachDocuments.Size = new System.Drawing.Size(152, 22);
            this.mnuItemAttachDocuments.Text = "&Documents";
            this.mnuItemAttachDocuments.Click += new System.EventHandler(this.mnuItemAttachDocuments_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(149, 6);
            // 
            // receiptToolStripMenuItem
            // 
            this.receiptToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.receiptToolStripMenuItem.Name = "receiptToolStripMenuItem";
            this.receiptToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.receiptToolStripMenuItem.Text = "R&eceipt";
            this.receiptToolStripMenuItem.Click += new System.EventHandler(this.receiptToolStripMenuItem_Click);
            // 
            // issueToolStripMenuItem
            // 
            this.issueToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.issueToolStripMenuItem.Name = "issueToolStripMenuItem";
            this.issueToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.issueToolStripMenuItem.Text = "&Issue";
            this.issueToolStripMenuItem.Click += new System.EventHandler(this.receiptToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(149, 6);
            // 
            // RenewToolStripMenuItem
            // 
            this.RenewToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.renew;
            this.RenewToolStripMenuItem.Name = "RenewToolStripMenuItem";
            this.RenewToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.RenewToolStripMenuItem.Text = "Re&new";
            this.RenewToolStripMenuItem.Click += new System.EventHandler(this.RenewToolStripMenuItem_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Fax";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnFax
            // 
            this.BtnFax.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnFax.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.BtnFax.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnFax.Name = "BtnFax";
            this.BtnFax.Size = new System.Drawing.Size(23, 22);
            this.BtnFax.Text = "Email";
            this.BtnFax.Click += new System.EventHandler(this.BtnFax_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 20);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // GrpMain
            // 
            this.GrpMain.Controls.Add(this.lblRenewed);
            this.GrpMain.Controls.Add(this.txtRemarks);
            this.GrpMain.Controls.Add(this.Label1);
            this.GrpMain.Controls.Add(EmployeeIDLabel);
            this.GrpMain.Controls.Add(this.dtpExpiryDate);
            this.GrpMain.Controls.Add(this.cboEmployee);
            this.GrpMain.Controls.Add(ExpiryDateLabel);
            this.GrpMain.Controls.Add(this.dtpIssueDate);
            this.GrpMain.Controls.Add(IssueDateLabel);
            this.GrpMain.Controls.Add(LicencedCountryIDLabel);
            this.GrpMain.Controls.Add(LicenceTypeIDLabel);
            this.GrpMain.Controls.Add(this.txtLicenseName);
            this.GrpMain.Controls.Add(LicenceHoldersNameLabel);
            this.GrpMain.Controls.Add(this.txtLicenseNumber);
            this.GrpMain.Controls.Add(this.BtnCountry);
            this.GrpMain.Controls.Add(LicenceNumberLabel);
            this.GrpMain.Controls.Add(this.BtnLicencetype);
            this.GrpMain.Controls.Add(this.cboLicenseType);
            this.GrpMain.Controls.Add(this.cboCountry);
            this.GrpMain.Controls.Add(this.ShapeContainer1);
            this.GrpMain.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrpMain.Location = new System.Drawing.Point(8, 57);
            this.GrpMain.Name = "GrpMain";
            this.GrpMain.Size = new System.Drawing.Size(454, 341);
            this.GrpMain.TabIndex = 0;
            this.GrpMain.TabStop = false;
            this.GrpMain.Text = "License Details";
            // 
            // lblRenewed
            // 
            this.lblRenewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenewed.ForeColor = System.Drawing.Color.Maroon;
            this.lblRenewed.Location = new System.Drawing.Point(370, 199);
            this.lblRenewed.Name = "lblRenewed";
            this.lblRenewed.Size = new System.Drawing.Size(71, 22);
            this.lblRenewed.TabIndex = 115;
            this.lblRenewed.Text = "Renewed";
            this.lblRenewed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRenewed.Visible = false;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtRemarks.Location = new System.Drawing.Point(20, 237);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(421, 95);
            this.txtRemarks.TabIndex = 60;
            this.txtRemarks.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(6, 220);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(58, 13);
            this.Label1.TabIndex = 58;
            this.Label1.Text = "Remarks";
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpiryDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(342, 171);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.Size = new System.Drawing.Size(99, 20);
            this.dtpExpiryDate.TabIndex = 8;
            this.dtpExpiryDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(147, 23);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(294, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboEmployee_KeyPress);
            // 
            // dtpIssueDate
            // 
            this.dtpIssueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIssueDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssueDate.Location = new System.Drawing.Point(124, 171);
            this.dtpIssueDate.Name = "dtpIssueDate";
            this.dtpIssueDate.Size = new System.Drawing.Size(99, 20);
            this.dtpIssueDate.TabIndex = 7;
            this.dtpIssueDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtLicenseName
            // 
            this.txtLicenseName.BackColor = System.Drawing.SystemColors.Info;
            this.txtLicenseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLicenseName.Location = new System.Drawing.Point(147, 84);
            this.txtLicenseName.MaxLength = 50;
            this.txtLicenseName.Name = "txtLicenseName";
            this.txtLicenseName.Size = new System.Drawing.Size(294, 20);
            this.txtLicenseName.TabIndex = 2;
            this.txtLicenseName.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // txtLicenseNumber
            // 
            this.txtLicenseNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtLicenseNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLicenseNumber.Location = new System.Drawing.Point(147, 54);
            this.txtLicenseNumber.MaxLength = 40;
            this.txtLicenseNumber.Name = "txtLicenseNumber";
            this.txtLicenseNumber.Size = new System.Drawing.Size(226, 20);
            this.txtLicenseNumber.TabIndex = 1;
            this.txtLicenseNumber.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // BtnCountry
            // 
            this.BtnCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCountry.Location = new System.Drawing.Point(411, 141);
            this.BtnCountry.Name = "BtnCountry";
            this.BtnCountry.Size = new System.Drawing.Size(30, 23);
            this.BtnCountry.TabIndex = 6;
            this.BtnCountry.Text = "....";
            this.BtnCountry.UseVisualStyleBackColor = true;
            this.BtnCountry.Click += new System.EventHandler(this.BtnCountry_Click);
            // 
            // BtnLicencetype
            // 
            this.BtnLicencetype.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLicencetype.Location = new System.Drawing.Point(411, 111);
            this.BtnLicencetype.Name = "BtnLicencetype";
            this.BtnLicencetype.Size = new System.Drawing.Size(30, 23);
            this.BtnLicencetype.TabIndex = 4;
            this.BtnLicencetype.Text = "....";
            this.BtnLicencetype.UseVisualStyleBackColor = true;
            this.BtnLicencetype.Click += new System.EventHandler(this.BtnLicencetype_Click);
            // 
            // cboLicenseType
            // 
            this.cboLicenseType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLicenseType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLicenseType.BackColor = System.Drawing.SystemColors.Info;
            this.cboLicenseType.DropDownHeight = 134;
            this.cboLicenseType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLicenseType.FormattingEnabled = true;
            this.cboLicenseType.IntegralHeight = false;
            this.cboLicenseType.Location = new System.Drawing.Point(147, 114);
            this.cboLicenseType.Name = "cboLicenseType";
            this.cboLicenseType.Size = new System.Drawing.Size(256, 21);
            this.cboLicenseType.TabIndex = 3;
            this.cboLicenseType.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            this.cboLicenseType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboLicenseType_KeyPress);
            // 
            // cboCountry
            // 
            this.cboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCountry.BackColor = System.Drawing.SystemColors.Info;
            this.cboCountry.DropDownHeight = 134;
            this.cboCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCountry.FormattingEnabled = true;
            this.cboCountry.IntegralHeight = false;
            this.cboCountry.Location = new System.Drawing.Point(147, 143);
            this.cboCountry.Name = "cboCountry";
            this.cboCountry.Size = new System.Drawing.Size(256, 21);
            this.cboCountry.TabIndex = 5;
            this.cboCountry.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            this.cboCountry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCountry_KeyPress);
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 17);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.ShapeContainer1.Size = new System.Drawing.Size(448, 321);
            this.ShapeContainer1.TabIndex = 59;
            this.ShapeContainer1.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.Color.Silver;
            this.LineShape1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 37;
            this.LineShape1.X2 = 446;
            this.LineShape1.Y1 = 210;
            this.LineShape1.Y2 = 210;
            // 
            // Timer
            // 
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // errorDrivingLicense
            // 
            this.errorDrivingLicense.ContainerControl = this;
            this.errorDrivingLicense.RightToLeft = true;
            // 
            // lblToolStripStatus
            // 
            this.lblToolStripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.lblToolStripStatus.Location = new System.Drawing.Point(0, 433);
            this.lblToolStripStatus.Name = "lblToolStripStatus";
            this.lblToolStripStatus.Size = new System.Drawing.Size(469, 22);
            this.lblToolStripStatus.TabIndex = 92;
            this.lblToolStripStatus.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(469, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 93;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code | Card Number";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Tooltip = "Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // frmDrivingLicense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 455);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.lblToolStripStatus);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.EmployeeLicenseDetailsBindingNavigator);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.GrpMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDrivingLicense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Driving License";
            this.Load += new System.EventHandler(this.frmDrivingLicense_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDrivingLicense_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmDrivingLicense_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeLicenseDetailsBindingNavigator)).EndInit();
            this.EmployeeLicenseDetailsBindingNavigator.ResumeLayout(false);
            this.EmployeeLicenseDetailsBindingNavigator.PerformLayout();
            this.GrpMain.ResumeLayout(false);
            this.GrpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorDrivingLicense)).EndInit();
            this.lblToolStripStatus.ResumeLayout(false);
            this.lblToolStripStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GrpMain;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.DateTimePicker dtpExpiryDate;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.DateTimePicker dtpIssueDate;
        internal System.Windows.Forms.TextBox txtLicenseName;
        internal System.Windows.Forms.TextBox txtLicenseNumber;
        internal System.Windows.Forms.Button BtnCountry;
        internal System.Windows.Forms.Button BtnLicencetype;
        internal System.Windows.Forms.ComboBox cboLicenseType;
        internal System.Windows.Forms.ComboBox cboCountry;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.BindingNavigator EmployeeLicenseDetailsBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton EmployeeLicenseDetailsBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnFax;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.Button  btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Timer Timer;
        internal System.Windows.Forms.ErrorProvider errorDrivingLicense;
        private System.Windows.Forms.ToolStripDropDownButton ToolStripDropDownBtnMore;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAttachDocuments;
        private System.Windows.Forms.ToolStripMenuItem receiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem issueToolStripMenuItem;
        internal System.Windows.Forms.StatusStrip lblToolStripStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label lblRenewed;
        private System.Windows.Forms.ToolStripMenuItem RenewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    }
}