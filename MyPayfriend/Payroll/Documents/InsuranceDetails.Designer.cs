﻿namespace MyPayfriend
{
    partial class InsuranceDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label Label5;
            System.Windows.Forms.Label Label13;
            System.Windows.Forms.Label Label3;
            System.Windows.Forms.Label lblPolicyNumber;
            System.Windows.Forms.Label lblIssueDate;
            System.Windows.Forms.Label lblInsuranceCompany;
            System.Windows.Forms.Label lblExpiryDate;
            System.Windows.Forms.Label lblEmployee;
            System.Windows.Forms.Label label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InsuranceDetails));
            this.grpInsuranceCard = new System.Windows.Forms.GroupBox();
            this.txtPolicyAmount = new System.Windows.Forms.TextBox();
            this.lblRenewed = new System.Windows.Forms.Label();
            this.cboAssets = new System.Windows.Forms.ComboBox();
            this.txtPolicyName = new System.Windows.Forms.TextBox();
            this.btnInsuranceCompany = new System.Windows.Forms.Button();
            this.txtPolicyNumber = new System.Windows.Forms.TextBox();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.cboInsuranceCompany = new System.Windows.Forms.ComboBox();
            this.dtpIssueDate = new System.Windows.Forms.DateTimePicker();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.stsStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.EmployeeInsuranceCardBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnCancel = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.documentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.receiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.RenewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnHelp = new System.Windows.Forms.ToolStripButton();
            this.bnScan = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemReceipt = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.IssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.errInsuranceCard = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmInsuranceCard = new System.Windows.Forms.Timer(this.components);
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            Label1 = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            Label13 = new System.Windows.Forms.Label();
            Label3 = new System.Windows.Forms.Label();
            lblPolicyNumber = new System.Windows.Forms.Label();
            lblIssueDate = new System.Windows.Forms.Label();
            lblInsuranceCompany = new System.Windows.Forms.Label();
            lblExpiryDate = new System.Windows.Forms.Label();
            lblEmployee = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.grpInsuranceCard.SuspendLayout();
            this.stsStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeInsuranceCardBindingNavigator)).BeginInit();
            this.EmployeeInsuranceCardBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errInsuranceCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label1.Location = new System.Drawing.Point(15, 58);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(33, 13);
            Label1.TabIndex = 113;
            Label1.Text = "Asset";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label5.Location = new System.Drawing.Point(15, 112);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(66, 13);
            Label5.TabIndex = 112;
            Label5.Text = "Policy Name";
            // 
            // Label13
            // 
            Label13.AutoSize = true;
            Label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label13.Location = new System.Drawing.Point(8, 238);
            Label13.Name = "Label13";
            Label13.Size = new System.Drawing.Size(58, 13);
            Label13.TabIndex = 18;
            Label13.Text = "Remarks";
            // 
            // Label3
            // 
            Label3.AutoSize = true;
            Label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label3.Location = new System.Drawing.Point(8, 3);
            Label3.Name = "Label3";
            Label3.Size = new System.Drawing.Size(106, 13);
            Label3.TabIndex = 65;
            Label3.Text = "Insurance Details";
            // 
            // lblPolicyNumber
            // 
            lblPolicyNumber.AutoSize = true;
            lblPolicyNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblPolicyNumber.Location = new System.Drawing.Point(15, 85);
            lblPolicyNumber.Name = "lblPolicyNumber";
            lblPolicyNumber.Size = new System.Drawing.Size(75, 13);
            lblPolicyNumber.TabIndex = 106;
            lblPolicyNumber.Text = "Policy Number";
            // 
            // lblIssueDate
            // 
            lblIssueDate.AutoSize = true;
            lblIssueDate.Location = new System.Drawing.Point(15, 193);
            lblIssueDate.Name = "lblIssueDate";
            lblIssueDate.Size = new System.Drawing.Size(58, 13);
            lblIssueDate.TabIndex = 84;
            lblIssueDate.Text = "Issue Date";
            // 
            // lblInsuranceCompany
            // 
            lblInsuranceCompany.AutoSize = true;
            lblInsuranceCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblInsuranceCompany.Location = new System.Drawing.Point(15, 139);
            lblInsuranceCompany.Name = "lblInsuranceCompany";
            lblInsuranceCompany.Size = new System.Drawing.Size(101, 13);
            lblInsuranceCompany.TabIndex = 107;
            lblInsuranceCompany.Text = "Insurance Company";
            // 
            // lblExpiryDate
            // 
            lblExpiryDate.AutoSize = true;
            lblExpiryDate.Location = new System.Drawing.Point(270, 198);
            lblExpiryDate.Name = "lblExpiryDate";
            lblExpiryDate.Size = new System.Drawing.Size(61, 13);
            lblExpiryDate.TabIndex = 12;
            lblExpiryDate.Text = "Expiry Date";
            // 
            // lblEmployee
            // 
            lblEmployee.AutoSize = true;
            lblEmployee.Location = new System.Drawing.Point(15, 31);
            lblEmployee.Name = "lblEmployee";
            lblEmployee.Size = new System.Drawing.Size(51, 13);
            lblEmployee.TabIndex = 70;
            lblEmployee.Text = "Company";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(15, 166);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(74, 13);
            label2.TabIndex = 116;
            label2.Text = "Policy Amount";
            // 
            // grpInsuranceCard
            // 
            this.grpInsuranceCard.Controls.Add(this.txtPolicyAmount);
            this.grpInsuranceCard.Controls.Add(label2);
            this.grpInsuranceCard.Controls.Add(this.lblRenewed);
            this.grpInsuranceCard.Controls.Add(this.cboAssets);
            this.grpInsuranceCard.Controls.Add(Label1);
            this.grpInsuranceCard.Controls.Add(this.txtPolicyName);
            this.grpInsuranceCard.Controls.Add(Label5);
            this.grpInsuranceCard.Controls.Add(this.btnInsuranceCompany);
            this.grpInsuranceCard.Controls.Add(this.txtPolicyNumber);
            this.grpInsuranceCard.Controls.Add(Label13);
            this.grpInsuranceCard.Controls.Add(this.dtpExpiryDate);
            this.grpInsuranceCard.Controls.Add(Label3);
            this.grpInsuranceCard.Controls.Add(lblPolicyNumber);
            this.grpInsuranceCard.Controls.Add(lblIssueDate);
            this.grpInsuranceCard.Controls.Add(this.cboInsuranceCompany);
            this.grpInsuranceCard.Controls.Add(this.dtpIssueDate);
            this.grpInsuranceCard.Controls.Add(lblInsuranceCompany);
            this.grpInsuranceCard.Controls.Add(lblExpiryDate);
            this.grpInsuranceCard.Controls.Add(this.cboCompany);
            this.grpInsuranceCard.Controls.Add(this.txtRemarks);
            this.grpInsuranceCard.Controls.Add(lblEmployee);
            this.grpInsuranceCard.Controls.Add(this.ShapeContainer2);
            this.grpInsuranceCard.Location = new System.Drawing.Point(8, 57);
            this.grpInsuranceCard.Name = "grpInsuranceCard";
            this.grpInsuranceCard.Size = new System.Drawing.Size(454, 346);
            this.grpInsuranceCard.TabIndex = 68;
            this.grpInsuranceCard.TabStop = false;
            // 
            // txtPolicyAmount
            // 
            this.txtPolicyAmount.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyAmount.Location = new System.Drawing.Point(133, 166);
            this.txtPolicyAmount.MaxLength = 10;
            this.txtPolicyAmount.Name = "txtPolicyAmount";
            this.txtPolicyAmount.Size = new System.Drawing.Size(270, 20);
            this.txtPolicyAmount.TabIndex = 7;
            this.txtPolicyAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            // 
            // lblRenewed
            // 
            this.lblRenewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenewed.ForeColor = System.Drawing.Color.Maroon;
            this.lblRenewed.Location = new System.Drawing.Point(365, 223);
            this.lblRenewed.Name = "lblRenewed";
            this.lblRenewed.Size = new System.Drawing.Size(71, 22);
            this.lblRenewed.TabIndex = 114;
            this.lblRenewed.Text = "Renewed";
            this.lblRenewed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRenewed.Visible = false;
            // 
            // cboAssets
            // 
            this.cboAssets.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAssets.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAssets.BackColor = System.Drawing.SystemColors.Info;
            this.cboAssets.DropDownHeight = 134;
            this.cboAssets.FormattingEnabled = true;
            this.cboAssets.IntegralHeight = false;
            this.cboAssets.Location = new System.Drawing.Point(133, 56);
            this.cboAssets.Name = "cboAssets";
            this.cboAssets.Size = new System.Drawing.Size(270, 21);
            this.cboAssets.TabIndex = 1;
            this.cboAssets.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            this.cboAssets.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCardIssued_KeyPress);
            // 
            // txtPolicyName
            // 
            this.txtPolicyName.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyName.Location = new System.Drawing.Point(133, 111);
            this.txtPolicyName.MaxLength = 50;
            this.txtPolicyName.Name = "txtPolicyName";
            this.txtPolicyName.Size = new System.Drawing.Size(270, 20);
            this.txtPolicyName.TabIndex = 4;
            this.txtPolicyName.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // btnInsuranceCompany
            // 
            this.btnInsuranceCompany.Location = new System.Drawing.Point(408, 135);
            this.btnInsuranceCompany.Name = "btnInsuranceCompany";
            this.btnInsuranceCompany.Size = new System.Drawing.Size(32, 23);
            this.btnInsuranceCompany.TabIndex = 6;
            this.btnInsuranceCompany.Text = "....";
            this.btnInsuranceCompany.UseVisualStyleBackColor = true;
            this.btnInsuranceCompany.Click += new System.EventHandler(this.btnInsuranceCompany_Click);
            // 
            // txtPolicyNumber
            // 
            this.txtPolicyNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtPolicyNumber.Location = new System.Drawing.Point(133, 84);
            this.txtPolicyNumber.MaxLength = 50;
            this.txtPolicyNumber.Name = "txtPolicyNumber";
            this.txtPolicyNumber.Size = new System.Drawing.Size(270, 20);
            this.txtPolicyNumber.TabIndex = 3;
            this.txtPolicyNumber.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(334, 196);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.Size = new System.Drawing.Size(102, 20);
            this.dtpExpiryDate.TabIndex = 9;
            this.dtpExpiryDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // cboInsuranceCompany
            // 
            this.cboInsuranceCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInsuranceCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInsuranceCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboInsuranceCompany.DropDownHeight = 134;
            this.cboInsuranceCompany.FormattingEnabled = true;
            this.cboInsuranceCompany.IntegralHeight = false;
            this.cboInsuranceCompany.Location = new System.Drawing.Point(133, 138);
            this.cboInsuranceCompany.Name = "cboInsuranceCompany";
            this.cboInsuranceCompany.Size = new System.Drawing.Size(270, 21);
            this.cboInsuranceCompany.TabIndex = 5;
            this.cboInsuranceCompany.SelectedIndexChanged += new System.EventHandler(this.OnInputChanged);
            this.cboInsuranceCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboInsuranceCompany_KeyPress);
            // 
            // dtpIssueDate
            // 
            this.dtpIssueDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssueDate.Location = new System.Drawing.Point(133, 193);
            this.dtpIssueDate.Name = "dtpIssueDate";
            this.dtpIssueDate.Size = new System.Drawing.Size(102, 20);
            this.dtpIssueDate.TabIndex = 8;
            this.dtpIssueDate.ValueChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DisplayMember = "CompanyID";
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.Location = new System.Drawing.Point(133, 28);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(306, 21);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.ValueMember = "CompanyID";
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboEmployee_KeyPress);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(18, 258);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(421, 82);
            this.txtRemarks.TabIndex = 10;
            this.txtRemarks.TextChanged += new System.EventHandler(this.OnInputChanged);
            // 
            // ShapeContainer2
            // 
            this.ShapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer2.Name = "ShapeContainer2";
            this.ShapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape2});
            this.ShapeContainer2.Size = new System.Drawing.Size(448, 327);
            this.ShapeContainer2.TabIndex = 110;
            this.ShapeContainer2.TabStop = false;
            // 
            // LineShape2
            // 
            this.LineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape2.Name = "LineShape3";
            this.LineShape2.X1 = 55;
            this.LineShape2.X2 = 434;
            this.LineShape2.Y1 = 231;
            this.LineShape2.Y2 = 231;
            // 
            // stsStatus
            // 
            this.stsStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.stsStatus.Location = new System.Drawing.Point(0, 440);
            this.stsStatus.Name = "stsStatus";
            this.stsStatus.Size = new System.Drawing.Size(469, 22);
            this.stsStatus.TabIndex = 69;
            this.stsStatus.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // EmployeeInsuranceCardBindingNavigator
            // 
            this.EmployeeInsuranceCardBindingNavigator.AddNewItem = null;
            this.EmployeeInsuranceCardBindingNavigator.CountItem = this.bnCountItem;
            this.EmployeeInsuranceCardBindingNavigator.DeleteItem = null;
            this.EmployeeInsuranceCardBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.BindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.ToolStripSeparator1,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnCancel,
            this.BindingNavigatorSeparator2,
            this.bnMoreActions,
            this.ToolStripSeparator3,
            this.bnPrint,
            this.bnEmail,
            this.ToolStripSeparator2,
            this.bnHelp});
            this.EmployeeInsuranceCardBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.EmployeeInsuranceCardBindingNavigator.MoveFirstItem = null;
            this.EmployeeInsuranceCardBindingNavigator.MoveLastItem = null;
            this.EmployeeInsuranceCardBindingNavigator.MoveNextItem = null;
            this.EmployeeInsuranceCardBindingNavigator.MovePreviousItem = null;
            this.EmployeeInsuranceCardBindingNavigator.Name = "EmployeeInsuranceCardBindingNavigator";
            this.EmployeeInsuranceCardBindingNavigator.PositionItem = this.bnPositionItem;
            this.EmployeeInsuranceCardBindingNavigator.Size = new System.Drawing.Size(469, 25);
            this.EmployeeInsuranceCardBindingNavigator.TabIndex = 70;
            this.EmployeeInsuranceCardBindingNavigator.Text = "BindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.bnMoveFirstItem_Click);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.bnMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.bnMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.bnMoveLastItem_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add New";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.Text = "Save";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Remove";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // bnCancel
            // 
            this.bnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnCancel.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.bnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancel.Name = "bnCancel";
            this.bnCancel.Size = new System.Drawing.Size(23, 22);
            this.bnCancel.Text = "Clear";
            this.bnCancel.Click += new System.EventHandler(this.bnCancel_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentsToolStripMenuItem,
            this.toolStripSeparator6,
            this.receiptToolStripMenuItem,
            this.issueToolStripMenuItem1,
            this.toolStripSeparator7,
            this.RenewToolStripMenuItem});
            this.bnMoreActions.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "&Actions";
            // 
            // documentsToolStripMenuItem
            // 
            this.documentsToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.documentsToolStripMenuItem.Name = "documentsToolStripMenuItem";
            this.documentsToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.documentsToolStripMenuItem.Text = "&Documents";
            this.documentsToolStripMenuItem.Click += new System.EventHandler(this.documentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(132, 6);
            // 
            // receiptToolStripMenuItem
            // 
            this.receiptToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.receiptToolStripMenuItem.Name = "receiptToolStripMenuItem";
            this.receiptToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.receiptToolStripMenuItem.Text = "R&eceipt";
            this.receiptToolStripMenuItem.Click += new System.EventHandler(this.receiptToolStripMenuItem_Click);
            // 
            // issueToolStripMenuItem1
            // 
            this.issueToolStripMenuItem1.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.issueToolStripMenuItem1.Name = "issueToolStripMenuItem1";
            this.issueToolStripMenuItem1.Size = new System.Drawing.Size(135, 22);
            this.issueToolStripMenuItem1.Text = "&Issue";
            this.issueToolStripMenuItem1.Click += new System.EventHandler(this.issueToolStripMenuItem1_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(132, 6);
            // 
            // RenewToolStripMenuItem
            // 
            this.RenewToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.renew;
            this.RenewToolStripMenuItem.Name = "RenewToolStripMenuItem";
            this.RenewToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.RenewToolStripMenuItem.Text = "Re&new";
            this.RenewToolStripMenuItem.Click += new System.EventHandler(this.RenewToolStripMenuItem_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPrint
            // 
            this.bnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnHelp
            // 
            this.bnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnHelp.Image = ((System.Drawing.Image)(resources.GetObject("bnHelp.Image")));
            this.bnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnHelp.Name = "bnHelp";
            this.bnHelp.Size = new System.Drawing.Size(23, 22);
            this.bnHelp.Text = "He&lp";
            this.bnHelp.Click += new System.EventHandler(this.bnHelp_Click);
            // 
            // bnScan
            // 
            this.bnScan.Name = "bnScan";
            this.bnScan.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D)));
            this.bnScan.Size = new System.Drawing.Size(173, 22);
            this.bnScan.Text = "Documents";
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(170, 6);
            // 
            // mnuItemReceipt
            // 
            this.mnuItemReceipt.Name = "mnuItemReceipt";
            this.mnuItemReceipt.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.R)));
            this.mnuItemReceipt.Size = new System.Drawing.Size(173, 22);
            this.mnuItemReceipt.Text = "Receipt";
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(170, 6);
            // 
            // IssueToolStripMenuItem
            // 
            this.IssueToolStripMenuItem.Name = "IssueToolStripMenuItem";
            this.IssueToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.I)));
            this.IssueToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.IssueToolStripMenuItem.Text = "Issue";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(8, 409);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(386, 409);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOk.Location = new System.Drawing.Point(301, 409);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // errInsuranceCard
            // 
            this.errInsuranceCard.ContainerControl = this;
            this.errInsuranceCard.RightToLeft = true;
            // 
            // tmInsuranceCard
            // 
            this.tmInsuranceCard.Interval = 2000;
            this.tmInsuranceCard.Tick += new System.EventHandler(this.tmInsuranceCard_Tick);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(469, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 88;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Company | Asset | Policy Number";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Tooltip = "Search";
            this.btnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // InsuranceDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(469, 462);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.EmployeeInsuranceCardBindingNavigator);
            this.Controls.Add(this.stsStatus);
            this.Controls.Add(this.grpInsuranceCard);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsuranceDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insurance Details";
            this.Load += new System.EventHandler(this.InsuranceDetails_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InsuranceDetails_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InsuranceDetails_KeyDown);
            this.grpInsuranceCard.ResumeLayout(false);
            this.grpInsuranceCard.PerformLayout();
            this.stsStatus.ResumeLayout(false);
            this.stsStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeInsuranceCardBindingNavigator)).EndInit();
            this.EmployeeInsuranceCardBindingNavigator.ResumeLayout(false);
            this.EmployeeInsuranceCardBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errInsuranceCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox grpInsuranceCard;
        internal System.Windows.Forms.ComboBox cboAssets;
        internal System.Windows.Forms.TextBox txtPolicyName;
        internal System.Windows.Forms.Button btnInsuranceCompany;
        internal System.Windows.Forms.TextBox txtPolicyNumber;
        internal System.Windows.Forms.DateTimePicker dtpExpiryDate;
        internal System.Windows.Forms.ComboBox cboInsuranceCompany;
        internal System.Windows.Forms.DateTimePicker dtpIssueDate;
        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape2;
        internal System.Windows.Forms.StatusStrip stsStatus;
        internal System.Windows.Forms.BindingNavigator EmployeeInsuranceCardBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel bnCountItem;
        internal System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox bnPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton bnMoveNextItem;
        internal System.Windows.Forms.ToolStripButton bnMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        internal System.Windows.Forms.ToolStripButton bnCancel;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        internal System.Windows.Forms.ToolStripMenuItem bnScan;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripMenuItem mnuItemReceipt;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripMenuItem IssueToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton bnPrint;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton bnHelp;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.ErrorProvider errInsuranceCard;
        internal System.Windows.Forms.Timer tmInsuranceCard;
        private System.Windows.Forms.ToolStripMenuItem documentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem issueToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnEmail;
        private System.Windows.Forms.ToolStripMenuItem RenewToolStripMenuItem;
        private System.Windows.Forms.Label lblRenewed;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.TextBox txtPolicyAmount;

    }
}