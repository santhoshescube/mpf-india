﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyPayfriend;


public partial class frmVisa : Form
{
    /// <summary>
    /// Form for Visa functionalities like ,add edit, delete , print and email
    /// </summary>
    /// <Modified By>Jisha Bejoy</Modified>
    /// <Modified on>20 Aug 2013</Modified>
    /// <Purpose>Code review , search option added, renew functionality modified and fine tuning</Purpose>
    #region  variables

    clsVisaBLL objVisaBLL = null;
    ClsNotificationNew MobjClsNotification = null; 
    ClsLogWriter Logger;

    int? VisaID = null;
    int intRowIndex = 1;
    int intTotalRecords = 0;   
    bool blnNewMode = false;
   
    int tempDocumentID = 0; 
    private bool IsReceipt { get; set; }
    private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
    private bool MblnAddPermission = false;           //To set Add Permission
    private bool MblnUpdatePermission = false;      //To set Update Permission
    private bool MblnDeletePermission = false;      //To set Delete Permission
    private bool MblnAddUpdatePermission = false;
    private bool mblnAddRenew = false;
    private bool mblnAddIssue = false;
    private bool mblnAddReceipt = false;
    private bool mblnSearchStatus = false;
    bool mblnEmail, mblnDelete, mblnUpdate = false;
    DataTable datMessages;
    string MstrCommonMessage = "";
    private bool IsVerified = false;    
    MessageBoxIcon objMsgIcon;     
    public long EmployeeID { get; set; } //  ID of the employee whose visa is going to add/modify.This has to be set from employee form
    public int piVisaID = 0;   
    private bool IsFromEmployeeForm { get; set; } //True if visa form is loaded from employee form otherwise false.
    private bool formLoading = false;
    private string strOf = "of ";
    #endregion

    private clsBLLDocumentMasterNew objClsBLLDocumentMaster = null;
    private clsBLLDocumentMasterNew MobjClsBLLDocumentMaster
    {
        get
        {
            if (this.objClsBLLDocumentMaster == null)
                this.objClsBLLDocumentMaster = new clsBLLDocumentMasterNew();

            return this.objClsBLLDocumentMaster;
        }
    }
    public frmVisa()
    {
        try
        {
            InitializeComponent();
           
            this.Text = "Visa";             // set title of the form
            this.objVisaBLL = new clsVisaBLL();
            this.objVisaBLL.Visa = new clsVisa();
            MobjClsNotification = new ClsNotificationNew();
           
            this.Logger = new ClsLogWriter(Application.StartupPath);

            this.dtpVisaIssueDate.Text = DateTime.Today.ToString("dd/MMM/yyyy");
            this.dtpVisaExpiryDate.Text = DateTime.Today.AddDays(1).ToString("dd/MMM/yyyy");

            // Set a common event handler for controls to enable buttons (save) when user enter/change input
            this.cboEmployee.SelectedIndexChanged += new EventHandler(OnInputChanged);
            this.txtPlaceOfIssue.TextChanged += new EventHandler(OnInputChanged);
            this.txtVisaNumber.TextChanged += new EventHandler(OnInputChanged);
            this.cboCountry.SelectedIndexChanged += new EventHandler(OnInputChanged);
            this.cboCountry.TextChanged += new EventHandler(OnInputChanged);
            this.dtpVisaExpiryDate.TextChanged += new EventHandler(OnInputChanged);
            this.dtpVisaIssueDate.TextChanged += new EventHandler(OnInputChanged);
            this.txtRemarks.TextChanged += new EventHandler(OnInputChanged);
            this.CboVisaType.SelectedIndexChanged += new EventHandler(OnInputChanged);
            this.CboVisaType.TextChanged  += new EventHandler(OnInputChanged);  
            // Initialise default values.           
            this.IsFromEmployeeForm = false;

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }
        catch (Exception)
        {
           
        }
    }
    private void SetArabicControls()
    {
        ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        objDAL.SetArabicVersion((int)FormID.Visa, this);

        ToolStripDropDownBtnMore.Text = "الإجراءات";
        ToolStripDropDownBtnMore.ToolTipText = "الإجراءات";
        mnuItemAttachDocuments.Text = "وثائق";
        receiptIssueToolStripMenuItem.Text = "استلام";
        issueToolStripMenuItem.Text = "قضية";
        renewToolStripMenuItem.Text = "جدد";
        txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
        strOf = "من ";
    }
    private void LoadMessage()
    {
        datMessages = MobjClsNotification.FillMessageArray((int)FormID.Visa, ClsCommonSettings.ProductID);
    }

    #region Events

    private void frmVisa_Load(object sender, EventArgs e)
    {
        try
        {

            SetAutoCompleteList();
            LoadMessage();
            SetPermissions();
            LoadInitials();

            if (piVisaID > 0)
            {
                objVisaBLL.Visa.VisaID = piVisaID;
                intTotalRecords = 1;
                intRowIndex = 1;
                this.BindNavigator();
                this.txtSearch.Enabled = false;
                lblStatus.Text = string.Empty;
                Timer.Enabled = true;
            }
            if (this.EmployeeID != 0)
            {
                this.IsFromEmployeeForm = true;
                if (intTotalRecords == 0) // No records
                {
                    intTotalRecords = 1;
                    BindingNavigatorCountItem.Text = intTotalRecords.ToString();
                    BindingNavigatorPositionItem.Text = intRowIndex.ToString();

                }
                else
                {
                    BindingNavigatorMoveFirstItem_Click(sender, e);
                }

            }



        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on Visa:  FormLoad()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Load()" + Ex.Message.ToString());
        }
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.SaveHandler();
        }
        catch (Exception )
        {
          
        }
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
        bool Saved = this.SaveHandler();
        if (Saved)
        {
           
            this.btnOK.Enabled = false;
            this.Close();
        }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
     
                this.Close();
         
    }

    public void OnInputChanged(object sender, EventArgs e)
    {
        this.DisableEnableButtons(ControlState.Enable);

        // Some inputs have been entered by the user. So we need to show a confirmation 
        // to user indicating that if you are not saved, entered info will be lost.     
     
    }

    private void BtnAddCountry_Click(object sender, EventArgs e)
    {
        try
        {
            // Calling Reference form for ItemCategory
            int intCountryID = Convert.ToInt32(cboCountry.SelectedValue);
            FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryID, CountryName,CountryNameArb  ", "CountryReference", "");
            objCommon.ShowDialog();
            objCommon.Dispose();
            LoadCountry();
            if (objCommon.NewID != 0)
                cboCountry.SelectedValue = objCommon.NewID;
            else
                cboCountry.SelectedValue = intCountryID;
        }
        catch (Exception Ex)
        {      
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on FrmCommonRef() " + Ex.Message.ToString());
        }

    } 
    /// <summary>
    /// form closing 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void frmVisa_FormClosing(object sender, FormClosingEventArgs e)
    {
        if (this.btnOK.Enabled)
        {
            string MsMessageCommon = MobjClsNotification.GetErrorMessage(datMessages , 8, out objMsgIcon);

            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
              
                objVisaBLL = null;
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
    }

    private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {       
        // first make sure that this event has not been raised on form load event.
        if (!this.formLoading && this.cboEmployee.SelectedIndex != -1)
        {
            this.OnInputChanged(sender, e);     
        }
    }

    #endregion

    #region Functions
    /// <summary>
    /// initial load settings
    /// </summary>
    private void LoadInitials()
    {
        LoadCombos();

        AddNewHandler();

        ClearInputs();

        SetEnableDisable();

    }
    /// <summary>
    /// Set add new mode
    /// </summary>
    /// <returns></returns>
    private bool AddNewHandler()
    {
        try
        {
            blnNewMode = (piVisaID  > 0 ? false : true);
            lblRenewed.Visible = false;
            cboEmployee.Enabled = (EmployeeID > 0 ? false : true);
            cboCountry.Enabled = true;
            txtVisaNumber.Enabled = txtPlaceOfIssue.Enabled = txtRemarks.Enabled = dtpVisaExpiryDate.Enabled = dtpVisaIssueDate.Enabled = BtnAddCountry.Enabled = true;
            //BindingNavigatorDeleteItem.ToolTipText = "Remove";

            txtSearch.Text = "";
            mblnSearchStatus = false;

            GetRecordCount();
           // SetAutoCompleteList();

            BindingNavigatorCountItem.Text = strOf + Convert.ToString(intTotalRecords + 1) + "";
            BindingNavigatorPositionItem.Text = Convert.ToString(intTotalRecords + 1);
            intRowIndex = intTotalRecords + 1;

            string MsMessageCommon = MobjClsNotification.GetErrorMessage(datMessages, 655, out objMsgIcon);
            lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            Timer.Enabled = true;
            return true;
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on Addnewhandler()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Addnewhandler()" + Ex.Message.ToString());
            return false;
        }
    }
   
    /// <summary>
    /// delete visa
    /// </summary>
    private void DeleteHandler()
    {
        try
        {
            // Set ID of the vehicle to be deleted
            this.objVisaBLL.Visa.VisaID = this.VisaID;

            // Invoke delete method and get delete status.
            bool success = this.objVisaBLL.DeleteVisa();

            // Send feedback to user
            if (success)
            {
                this.AssingStatuText((int)CommonMessages.Deleted);
                ShowMessage(4);
                LoadInitials();
                SetAutoCompleteList();
            }
            else
                ShowMessage("Deletion failed");
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on DeleteHandler()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on DeleteHandler()" + Ex.Message.ToString());
        }
    }

    /// <summary>
    /// save visa
    /// </summary>
    /// <returns>status - indicates success or not</returns>
    private bool SaveHandler()
    {
        bool success = IsVerified = false;      
        try
        {
            // Validate user inputs
            this.VerifyInputs();

            // Save visa if validation succeeded
            if (IsVerified)
            {
                if (objVisaBLL.Visa.VisaID == 0 || objVisaBLL.Visa.VisaID == null)
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 1);
                }
                else
                {
                    MstrCommonMessage = MobjClsNotification.GetErrorMessage(datMessages, 3);
                }
                if (MessageBox.Show(MstrCommonMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                  
                    clsAlerts objclsAlerts = new clsAlerts();
                    // Insert visa if mode is insert
                    if (blnNewMode)
                    {
                        Int32 intVisaID = objVisaBLL.InsertVisa();                     

                        if (intVisaID > 0)
                        {
                            
                            success = true; 
                            this.AssingStatuText((int)CommonMessages.Saved);
                            ShowMessage(2); 
                            if (piVisaID > 0)
                            {
                                piVisaID = intVisaID;
                                intRowIndex = 1;
                            }
                            this.blnNewMode = false;                        
                            this.BindNavigator();                          
                           
                            // switch to insert mode.
                            objclsAlerts.AlertMessage((int)DocumentType.Visa, "Visa", "VisaNumber", intVisaID, txtVisaNumber.Text, dtpVisaExpiryDate.Value, "Emp", cboEmployee.SelectedValue.ToInt32(), cboEmployee.Text, false,0);
                            
                        }
                        else
                            ShowMessage("Saving visa failed");
                    }
                    else
                    {
                  
                        success = objVisaBLL.UpdateVisa();
                        if (success)
                        { 
                            this.AssingStatuText((int)CommonMessages.Updated);
                            ShowMessage(21);                           
                            objclsAlerts.AlertMessage((int)DocumentType.Visa, "Visa", "VisaNumber", VisaID.ToInt32(), txtVisaNumber.Text, dtpVisaExpiryDate.Value, "Emp", cboEmployee.SelectedValue.ToInt32(), cboEmployee.Text, false,0);
                        }
                        else
                        {
                            ShowMessage("Failed to update the visa");
                        }
                       
                    }
                  
                    GetRecordCount();
                    RefreshFormAfterSave();
               
                }
            }
            return success;
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error onSavehandler()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Savehandler()" + Ex.Message.ToString());
            return success;
        }

    }
    /// <summary>
    /// Enable or disable issue and receipt button
    /// </summary>
    private void SetIssueReceiptButton()
    {
      
        if (!clsPassportBLL.IsEmployeeInService(cboEmployee.SelectedValue.ToInt32()))
        {
            renewToolStripMenuItem.Enabled = false;
        }
        else
        {
            renewToolStripMenuItem.Enabled = lblRenewed.Visible ? false : mblnAddRenew;
        }
       
        issueToolStripMenuItem.Enabled = receiptIssueToolStripMenuItem.Enabled = false;
        if (objVisaBLL.Visa.VisaID != null)
        {
            IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Employee, (int)DocumentType.Visa, objVisaBLL.Visa.VisaID.ToInt32());
        }
        if (IsReceipt)
            issueToolStripMenuItem.Enabled = mblnAddIssue;
        else
            receiptIssueToolStripMenuItem.Enabled = mblnAddReceipt;
    }
    /// <summary>
    /// Enable and disable form controls after save
    /// </summary>

    private void RefreshFormAfterSave()
    {
        try
        {
            SetAutoCompleteList();
            blnNewMode = false;
            btnOK.Enabled = btnSave.Enabled = bnSaveItem.Enabled = btnClear.Enabled = false;
            mnuItemAttachDocuments.Enabled = MblnAddPermission;
            BindingNavigatorAddNewItem.Enabled = (piVisaID > 0 ? false : MblnAddPermission);
            BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
            if (blnNewMode) BindingNavigatorDeleteItem.Enabled = false;
            else
            {
                BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (piVisaID > 0 ? false : MblnDeletePermission));
                //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed visa could not be removed." : "Remove");
            }
            SetIssueReceiptButton();
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on RefreshFormAfterSave()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on RefreshFormAfterSave()" + Ex.Message.ToString());
        }
    }
    /// <summary>
    /// bind country and employee combo
    /// </summary>
    private void LoadCombos()
    {
        try
        {
            DataTable datCombos = null;

            if (ClsCommonSettings.IsArabicView)
            {
                if (EmployeeID > 0)
                {
                    cboEmployee.Text = string.Empty;
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeFullNameArb +'-'+ E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                    "E.EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "E.WorkStatusID > 5 And E.EmployeeID = " + EmployeeID });

                }
                else
                {
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeFullNameArb +'-'+ E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                        "E.EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "E.WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") order by E.Employeefullname " });

                }
            }
            else
            {
                if (EmployeeID > 0)
                {
                    cboEmployee.Text = string.Empty;
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeFullName +'-'+ E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                        "E.EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "E.WorkStatusID > 5 And E.EmployeeID = " + EmployeeID });

                }
                else
                {
                    datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeFullName +'-'+ E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                        "E.EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                        "E.WorkStatusID > 5 and E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") order by E.Employeefullname " });

                }
            }


            cboEmployee.DisplayMember = "EmployeeName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = datCombos;

            LoadCountry();
            LoadVisaType();
            LoadDesignation();
            LoadVisaCompany();

        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on LoadCombos()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on LoadCombos()" + Ex.Message.ToString());
        }
    }

    /// <summary>
    /// form validation before saving
    /// </summary>
    private void VerifyInputs()
    {
        try
        {
            this.errorProvider1.Clear();
            txtSearch.Text = "";
            // create new instanace of vehicle to clear all existing values (if any)
            this.objVisaBLL.Visa = new clsVisa();

            tempDocumentID = Convert.ToInt32(objVisaBLL.Visa.DocumentID);         
            if (!this.blnNewMode)               
                this.objVisaBLL.Visa.VisaID = (int)this.VisaID; // Set ID of the visa to be updated.

            if (this.cboEmployee.SelectedIndex == -1)
            {
                ShowMessage(10210, cboEmployee);  
                return;
            }
            else
                this.objVisaBLL.Visa.EmployeeID = Convert.ToInt32(this.cboEmployee.SelectedValue);


            if (this.txtVisaNumber.Text.Trim() == string.Empty)
            {
                ShowMessage(10211, txtVisaNumber);
                return;
            }
            else
            {
                this.objVisaBLL.Visa.VisaNumber = this.txtVisaNumber.Text.Trim();
            }

            this.objVisaBLL.Visa.PlaceofIssue = this.txtPlaceOfIssue.Text.Trim();

            if (this.cboCountry.SelectedIndex == -1)
            {
                ShowMessage(10213, cboCountry);
                return;
            }
            else
                this.objVisaBLL.Visa.IssueCountryID = Convert.ToInt32(this.cboCountry.SelectedValue);

            if (this.dtpVisaIssueDate.Text != string.Empty)
            {
                this.objVisaBLL.Visa.VisaIssueDate = Convert.ToDateTime(dtpVisaIssueDate.Text);

                if (!this.objVisaBLL.IsValidIssueDate((DateTime)objVisaBLL.Visa.VisaIssueDate, (int)objVisaBLL.Visa.EmployeeID))
                {
                    MessageBox.Show("Visa issue date should be greater than date of birth of employee.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            if (objVisaBLL.Visa.VisaIssueDate > ClsCommonSettings.GetServerDate())
            {
                MessageBox.Show("Visa issue date should not be  greater than current Date.", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (this.dtpVisaExpiryDate.Text != string.Empty)
                this.objVisaBLL.Visa.VisaExpiryDate = Convert.ToDateTime(this.dtpVisaExpiryDate.Text);


            if (objVisaBLL.Visa.VisaIssueDate != ClsCommonSettings.NullDate && objVisaBLL.Visa.VisaExpiryDate != ClsCommonSettings.NullDate)
            {
                if (objVisaBLL.Visa.VisaIssueDate > objVisaBLL.Visa.VisaExpiryDate)
                {
                    ShowMessage(10214, dtpVisaExpiryDate);
                    return;
                }
            }
            if (CboVisaType.SelectedIndex != -1)
            {
                objVisaBLL.Visa.VisaTypeID = Convert.ToInt32(CboVisaType.SelectedValue);
            }
            else
            {
                MessageBox.Show("Please select visa type", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            objVisaBLL.Visa.Remarks = txtRemarks.Text.Trim();
            objVisaBLL.Visa.IsRenew = lblRenewed.Visible;
            objVisaBLL.Visa.UID = txtUID.Text.ToStringCustom();
            objVisaBLL.Visa.visaDesignationID =  cboVisaDesignation.SelectedValue.ToInt32();
            objVisaBLL.Visa.visaCompanyID =  cboVisaComapny.SelectedValue.ToInt32();
            IsVerified = true;
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on VerifyInputs()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on VerifyInputs()" + Ex.Message.ToString());
        }
    }

    public void BindComboBox(ComboBox cbo, string strDisplayField, string strValueField, object dataSource)
    {
        cbo.DisplayMember = strDisplayField;
        cbo.ValueMember = strValueField;
        cbo.DataSource = dataSource;
        cbo.SelectedIndex = -1;
    }
    /// <summary>
    /// Bind controls with visa details
    /// </summary>
    private void BindContorls()
    {
        try
        {
            this.txtVisaNumber.Text = this.objVisaBLL.Visa.VisaNumber;        
            this.SetIndex(this.cboCountry, this.objVisaBLL.Visa.IssueCountryID);          
            if (intTotalRecords > 0)
                FillEmployee();  
            this.cboEmployee.SelectedValue = this.objVisaBLL.Visa.EmployeeID;
            this.txtPlaceOfIssue.Text = this.objVisaBLL.Visa.PlaceofIssue;
            this.dtpVisaExpiryDate.Text = this.objVisaBLL.Visa.VisaExpiryDate == ClsCommonSettings.NullDate ? string.Empty : this.objVisaBLL.Visa.VisaExpiryDate.ToString();
            this.dtpVisaIssueDate.Text = this.objVisaBLL.Visa.VisaIssueDate == ClsCommonSettings.NullDate ? string.Empty : this.objVisaBLL.Visa.VisaIssueDate.ToString();
            this.CboVisaType.SelectedValue = this.objVisaBLL.Visa.VisaTypeID == 0 ? -1 : this.objVisaBLL.Visa.VisaTypeID.ToInt32();  
            txtRemarks.Text = objVisaBLL.Visa.Remarks;
            txtUID.Text = objVisaBLL.Visa.UID;
            if(objVisaBLL.Visa.visaCompanyID.ToInt32() == 0)
            {
                cboVisaComapny.SelectedIndex = -1;
                cboVisaComapny.Text = "";
            }else
            {
                cboVisaComapny.SelectedValue = objVisaBLL.Visa.visaCompanyID; 
            }
            if(objVisaBLL.Visa.visaDesignationID.ToInt32() == 0)
            {
                cboVisaDesignation.SelectedIndex = -1;
                cboVisaDesignation.Text = "";
            }else
            {
                cboVisaDesignation.SelectedValue = objVisaBLL.Visa.visaDesignationID;
            }          

            if (objVisaBLL.Visa.IsRenew)
            {
                lblRenewed.Visible = true;
                renewToolStripMenuItem.Enabled = false;
                txtPlaceOfIssue.Enabled = txtVisaNumber.Enabled = cboCountry.Enabled = cboEmployee.Enabled = dtpVisaExpiryDate.Enabled = dtpVisaIssueDate.Enabled = BtnAddCountry.Enabled = false;
                BindingNavigatorDeleteItem.Enabled = false;

            }
            else
            {
                lblRenewed.Visible = false;
                renewToolStripMenuItem.Enabled = true;
                cboEmployee.Enabled = EmployeeID == 0 ? true : false;
                txtPlaceOfIssue.Enabled = txtVisaNumber.Enabled = cboCountry.Enabled = dtpVisaExpiryDate.Enabled = dtpVisaIssueDate.Enabled = BtnAddCountry.Enabled = true;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            }      
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on BindControls()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on BindContrls()" + Ex.Message.ToString());
        }
    }

    /// <summary>
    /// set combobox selected indes
    /// </summary>
    /// <param name="cbo">combobox</param>
    /// <param name="value">value</param>
    private void SetIndex(ComboBox cbo,  int? value)
    {
        if (value != ClsCommonSettings.NullInt)
            cbo.SelectedValue = value.ToString();
    }


    /// <summary>
    /// clear all fields
    /// </summary>
    private void ClearInputs()
    {
        try
        {
            this.txtPlaceOfIssue.Text = string.Empty;
            this.txtVisaNumber.Text = string.Empty;
            this.dtpVisaExpiryDate.Value = System.DateTime.Now.AddDays(1);
            this.dtpVisaIssueDate.Value = System.DateTime.Now;
            this.cboCountry.SelectedValue = -1;
            this.cboVisaComapny.SelectedIndex = -1;
            this.cboVisaDesignation.SelectedIndex = -1;
            txtUID.Text = string.Empty;
            this.cboCountry.Text = "";
            this.txtRemarks.Text = "";
            btnSave.Enabled = btnOK.Enabled = bnSaveItem.Enabled = false;

            if (EmployeeID == 0)
            {
                cboEmployee.SelectedIndex = -1;
                cboEmployee.Enabled = true;
                cboEmployee.Text = string.Empty;
                cboEmployee.Select();
            }
            else
            {
                cboEmployee.SelectedValue = this.EmployeeID;                
                cboEmployee.Enabled = false;
                txtVisaNumber.Select();
            }
            txtSearch.Text = "";
            mblnSearchStatus = false;
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on ClearInputs()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on ClearInputs()" + Ex.Message.ToString());
        }
    }

    /// <summary>
    /// button enable/disable depends on New mode or update mode
    /// </summary>
    private void SetEnableDisable()
    {
        btnSave.Enabled = btnOK.Enabled = bnSaveItem.Enabled = false;
        BtnPrint.Enabled = (blnNewMode == true ? false : MblnPrintEmailPermission);
        BtnEmail.Enabled = (blnNewMode == true ? false : MblnPrintEmailPermission);
        ToolStripDropDownBtnMore.Enabled = (blnNewMode == true ? false : true);

        BindingNavigatorAddNewItem.Enabled = (blnNewMode == true ? false : (piVisaID > 0 ? false : MblnAddPermission));

        if (blnNewMode) BindingNavigatorDeleteItem.Enabled = false;
        else
        {
            BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (piVisaID > 0 ? false : MblnDeletePermission));
            //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewedpassport could not be removed." : "Remove");
        }
        btnClear.Enabled = (blnNewMode == true ? true : false);

        if (piVisaID <= 0)
        {
            BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = true;
            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record

            if (intRowIndex >= intTotalRecords)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (intRowIndex == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }
        else
            BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = false;
    }
    

    /// <summary>
    /// Bind visa details to controls as per search criteria
    /// </summary> 
    private void BindNavigator()
    {
        try
        {
        errorProvider1.Clear();
        // Instantiate visa object if required.
        if (this.objVisaBLL.Visa == null)
            this.objVisaBLL.Visa = new clsVisa();
        if (!blnNewMode)
        {
                      
                if (this.IsFromEmployeeForm)
                    this.objVisaBLL.Visa.EmployeeID = this.EmployeeID;
                else
                    this.objVisaBLL.Visa.EmployeeID = 0;   
                // success status indicates the search criteria returns any record or not, at the same time all values filled to visa object .
                bool success = objVisaBLL.GetVisaDetails(piVisaID, txtSearch.Text.Trim(),intRowIndex);

                if (success)
                {                      
                  
                    this.intTotalRecords = (Int32)this.objVisaBLL.Visa.Pager.TotalRecords;
                    this.BindContorls();
                    this.BindingNavigatorPositionItem.Text = this.intRowIndex.ToString();
                    this.BindingNavigatorCountItem.Text = string.Format(strOf + " {0}", this.intTotalRecords);
               
                    this.VisaID = this.objVisaBLL.Visa.VisaID;
                }
                this.SetEnableDisable();
                SetIssueReceiptButton();
            }

            this.blnNewMode = false;
            
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on BindNavigator()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on BindNavigator()" + Ex.Message.ToString());
         
           
        }
    }

    /// <summary>
    /// renew visa and saving renewed visa details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void renewToolStripMenuItem_Click(object sender, EventArgs e)
    {
        try
        {
            if (objVisaBLL.Visa.VisaID.ToInt32() > 0)
            {
                string sMessage = MobjClsNotification.GetErrorMessage(datMessages, 17611, out objMsgIcon); // Do you wish to renew

                if (MessageBox.Show(sMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                else //YES Do you wish to add
                {
                    objVisaBLL.Visa.VisaID = objVisaBLL.Visa.VisaID.ToInt32();
                    objVisaBLL.Renew(); // renew status updating, and also deleting that alerts
                    BindNavigator();

                    sMessage = MobjClsNotification.GetErrorMessage(datMessages, 17612, out objMsgIcon); // Do you wish to add
                    if (MessageBox.Show(sMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        //if wish to add new visa loading the existing visa details 
                        AddNewHandler();
                        SetEnableDisable();
                        txtVisaNumber.Text = string.Empty;
                        blnNewMode = true;
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on Renew()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Renew()" + Ex.Message.ToString());
        }
    }   
   
    /// <summary>
    /// enable or disable save button as per functionality
    /// </summary>
    /// <param name="state">controls state</param>
    private void DisableEnableButtons(ControlState state)
    {
        errorProvider1.Clear();
        if (blnNewMode)
        {
            bnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnAddPermission;
        }
        else
        {
            bnSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = MblnUpdatePermission;
        }
    }

    /// <summary>
    /// for setting the status message on status bar
    /// </summary>
    /// <param name="intCode">meddage code</param>
    private void AssingStatuText(int intCode)
    {
        this.lblStatus.Text = this.MobjClsNotification.GetStatusMessage(datMessages, intCode);
        this.Timer.Start();
    }

    #endregion

    #region Binding Navigator Methods

    private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
    {
        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;
        GetRecordCount();

        this.blnNewMode = false;
        this.intRowIndex = 1;
        this.BindNavigator();
        this.AssingStatuText((int)CommonMessages.FirstRecord);  
    }

    private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
    {
      

        if (!mblnSearchStatus )
            txtSearch.Text = string.Empty; 
        
        GetRecordCount();

        if (intTotalRecords > 0 )        
            this.intRowIndex -= 1;


       
        this.blnNewMode = false;
        this.BindNavigator();
        this.AssingStatuText((int)CommonMessages.PreviousRecord);      
    }
    
    private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
    {
        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;

        GetRecordCount();

      
        if (!blnNewMode)
        {
            this.intRowIndex += 1;
            this.BindNavigator();
        }
    
        this.AssingStatuText((int)CommonMessages.NextRecord);
        this.errorProvider1.Clear();
    }

    private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
    {
       
        if (!mblnSearchStatus)
            txtSearch.Text = string.Empty;
        GetRecordCount();

        this.blnNewMode = false;    
        this.intRowIndex = this.intTotalRecords;
        this.BindNavigator();      
        this.AssingStatuText((int)CommonMessages.LastRecord);
        this.errorProvider1.Clear();
    }

    private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
    {
        this.LoadInitials();
        this.AssingStatuText((int)CommonMessages.NewRecord);   
    } 

    private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
    {
        int dtDocRequest = clsBLLDocumentMasterNew.GetDocRequestReferenceExists(this.VisaID.ToInt32(), (int)DocumentType.Visa);
        if (dtDocRequest > 0)
        {
            string Message = "Cannot Delete as Document Request Exists.";
            MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        else
        {
            string Message = MobjClsNotification.GetErrorMessage(datMessages, 13).Replace("@", "\n");
            DialogResult confirmation = MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (confirmation == DialogResult.Yes)
                this.DeleteHandler();
        }
    }

    private void CancelToolStripButton_Click(object sender, EventArgs e)
    {
        this.ClearInputs();
    }

    #endregion

    #region Timer

    private void Timer_Tick(object sender, EventArgs e)
    {
        // Clear text in the status bar after the time interval of timer.
        this.lblStatus.Text = string.Empty;
        this.Timer.Stop();
    }

    #endregion
    

    private void BtnPrint_Click(object sender, EventArgs e)
    {
        LoadReport(); //print visa details
    }

    private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
    {
        cboEmployee.DroppedDown = false;
    }

    private void cboCountry_KeyPress(object sender, KeyPressEventArgs e)
    {
        cboCountry.DroppedDown = false;
    }

    private void btnSave_Click_1(object sender, EventArgs e)
    {
        try
        {
            this.SaveHandler();
        }
        catch (Exception )
        {
          
        }
    }
    /// <summary>
    /// set permission as per user role
    /// </summary>
    private void SetPermissions()
    {
        // Function for setting permissions
        clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
        if (ClsCommonSettings.RoleID > 3)
        {
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.Visa, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out mblnEmail, out mblnAddRenew, out mblnUpdate, out mblnDelete);
            
        }
        else
        {
            MblnAddPermission = MblnAddUpdatePermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            mblnAddIssue = mblnAddReceipt = mblnAddRenew = true;
        }
        if (MblnAddPermission == true || MblnUpdatePermission == true)
        {
            MblnAddUpdatePermission = true;
        }
     
    }

    /// <summary>
    /// receipt or issue document
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void receiptIssueToolStripMenuItem_Click(object sender, EventArgs e)
    {
        new frmDocumentReceiptIssue()
        {
            eOperationType = OperationType.Employee,
            eDocumentType = DocumentType.Visa,
            DocumentID = objVisaBLL.Visa.VisaID.ToInt32(),
            DocumentNumber = txtVisaNumber.Text,
            EmployeeID = cboEmployee.SelectedValue.ToInt32(),
            ExpiryDate = dtpVisaExpiryDate.Value
        }.ShowDialog();
        SetReceiptOrIssueLabel();
    }
   

    /// <summary>
    /// set receipt or issue  button as per functionality
    /// </summary>
    private void SetReceiptOrIssueLabel()
    {
         issueToolStripMenuItem.Enabled = receiptIssueToolStripMenuItem.Enabled = false;
      
        if (objVisaBLL.Visa != null)
        {
            IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Employee, (int)DocumentType.Visa, objVisaBLL.Visa.VisaID.ToInt32());

            if (IsReceipt)
                issueToolStripMenuItem.Enabled = mblnAddIssue;
            else
                receiptIssueToolStripMenuItem.Enabled = mblnAddReceipt;            
        }
    }

    private void LoadVisaCompany()
    {
        try
        {
            DataTable datCombos = null;
            datCombos = objVisaBLL.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
            cboVisaComapny.DisplayMember = "CompanyName";
            cboVisaComapny.ValueMember = "CompanyID";
            cboVisaComapny.DataSource = datCombos;
        }
        catch (Exception)
        {
        }

    }
    private void LoadDesignation()
    {
        try
        {
            DataTable datCombos = null;
            datCombos = objVisaBLL.FillCombos(new string[] { "DesignationID,Designation", "DesignationReference", "" });
            cboVisaDesignation.DisplayMember = "Designation";
            cboVisaDesignation.ValueMember = "DesignationID";
            cboVisaDesignation.DataSource = datCombos;
        }
        catch (Exception)
        {
        }

    }

    private void LoadVisaType()
    {
        try
        {
            DataTable datCombos = null;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = objVisaBLL.FillCombos(new string[] { "VisaTypeID,VisaTypeArb As VisaType", "VisaTypeReference", "" });
            }
            else
            {
                datCombos = objVisaBLL.FillCombos(new string[] { "VisaTypeID,VisaType", "VisaTypeReference", "" });
            }
            CboVisaType.DisplayMember = "VisaType";
            CboVisaType.ValueMember = "VisaTypeID";
            CboVisaType.DataSource = datCombos;
        }
        catch (Exception)
        {
        }

    }
    /// <summary>
    /// fill country combo
    /// </summary>
    private void LoadCountry()
    {
        try
        {
            DataTable datCombos = null;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = objVisaBLL.FillCombos(new string[] { "CountryId, CountryNameArb as Description ", "CountryReference", "" });
            }
            else
            {
                datCombos = objVisaBLL.FillCombos(new string[] { "CountryId, CountryName as Description ", "CountryReference", "" });
            }
            cboCountry.DisplayMember = "Description";
            cboCountry.ValueMember = "CountryID";
            cboCountry.DataSource = datCombos;
        }
        catch (Exception)
        {

        }
    }
    // fill employees
    private void FillEmployee()
    {
        DataTable datCombos;
        if (ClsCommonSettings.IsArabicView)
        {
            datCombos = objVisaBLL.FillCombos(new string[] { "EmployeeFullNameArb +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                "EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                "WorkStatusID > 5 OR EmployeeID = " + objVisaBLL.Visa.EmployeeID });

        }
        else
        {
            datCombos = objVisaBLL.FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName ," +
                "EmployeeID", "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                "WorkStatusID > 5 OR EmployeeID = " + objVisaBLL.Visa.EmployeeID });

        }

        cboEmployee.DisplayMember = "EmployeeName";
        cboEmployee.ValueMember = "EmployeeID";
        cboEmployee.DataSource = datCombos;
    }
    /// <summary>
    /// show message as per message code
    /// </summary>
    /// <param name="MessageCode"></param>
    private void ShowMessage(int MessageCode)
    {
        MessageBox.Show(MobjClsNotification.GetErrorMessage(datMessages, MessageCode), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    /// <summary>
    /// message showing as per code and control
    /// </summary>
    /// <param name="MessageCode">message code</param>
    /// <param name="dt">control</param>
    private void ShowMessage(int MessageCode, Control dt)
    {
        dt.Focus();
        string mMessage = MobjClsNotification.GetErrorMessage(datMessages, MessageCode);
        MessageBox.Show(mMessage, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        errorProvider1.SetError(dt, mMessage);
    }
    /// <summary>
    /// show message as per message 
    /// </summary>
    /// <param name="Message">string message</param>
    private void ShowMessage(string Message)
    {
        MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    /// <summary>
    /// attach documents to selected visa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void mnuItemAttachDocuments_Click(object sender, EventArgs e)
    {
        if (objVisaBLL.Visa.VisaID.ToInt32() > 0)
        {
            using (FrmScanning objScanning = new FrmScanning())
            {
                objScanning.MintDocumentTypeid = (int)DocumentType.Visa;
                objScanning.MlngReferenceID = cboEmployee.SelectedValue.ToInt32();
                objScanning.MintOperationTypeID = (int)OperationType.Employee;
                objScanning.MstrReferenceNo = txtVisaNumber.Text;
                objScanning.PblnEditable = true;
                objScanning.MintVendorID = cboEmployee.SelectedValue.ToInt32();
                objScanning.MintDocumentID = objVisaBLL.Visa.VisaID.ToInt32();
                objScanning.ShowDialog();

            }
        }
    }   
    /// <summary>
    /// email visa details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void BtnEmail_Click(object sender, EventArgs e)
    {
        try
        {
            if (objVisaBLL.Visa.VisaID.ToInt32() > 0)
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Employee Visa";
                    ObjEmailPopUp.EmailFormType = EmailFormID.Visa;
                    ObjEmailPopUp.EmailSource = objVisaBLL.GetEmployeeVisaReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on Email()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on Email()" + Ex.Message.ToString());
        }

    }

    /// <summary>
    ///  load printing details
    /// </summary>
    private void LoadReport()
    {
        try
        {
            if (objVisaBLL.Visa.VisaID.ToInt32() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                DataTable datEmps = new ClsCommonUtility().FillCombos(new string[] { "CompanyID, FirstName", "EmployeeMaster", "  EmployeeID = " + cboEmployee.SelectedValue });
                if (datEmps.Rows.Count > 0)
                {
                    ObjViewer.intCompanyID = datEmps.Rows[0]["CompanyID"].ToInt32();
                }
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = objVisaBLL.Visa.VisaID.ToInt32();
                ObjViewer.PiFormID = (int)FormID.Visa;
                ObjViewer.ShowDialog();
            }
        }
        catch (Exception Ex)
        {
            Logger.WriteLog("Error on Print:LoadReport()" + this.Name + " " + Ex.Message.ToString(), 2);
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on LoadReport()" + Ex.Message.ToString());
        }
    }

    private void btnClear_Click(object sender, EventArgs e)
    {
        this.ClearInputs();
    }

    /// <summary>
    /// autocomplete  search textbox
    /// </summary>
    private void SetAutoCompleteList()
    {
        try
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsBLLInsuranceCard.GetAutoCompleteList(txtSearch.Text.Trim(), this.EmployeeID, "VisaNumber", "EmployeeVisaDetails");
            this.txtSearch.AutoCompleteCustomSource = clsBLLInsuranceCard.ConvertToAutoCompleteCollection(dt);
        }
        catch (Exception)
        {
        }
    }

    /// <summary>
    /// Get total record count while searching
    /// </summary>
    private void GetRecordCount()
    {
        intTotalRecords = objVisaBLL.GetRecordCount(txtSearch.Text.Trim(), this.EmployeeID  ,piVisaID);
    }   

    private void txtSearch_KeyDown(object sender, KeyEventArgs e)
    {
        if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
        {
            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(sender, null);
        }
    }

    /// <summary>
    /// sear visa as per criteria
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnSearch_Click(object sender, EventArgs e)
    {
     
        blnNewMode = false;
        if (txtSearch.Text == string.Empty)
            mblnSearchStatus = false;
        else
            mblnSearchStatus = true;
        GetRecordCount();
        if (intTotalRecords  == 0) // No records
        {
            ShowMessage(40);
            txtSearch.Text = string.Empty;
            BindingNavigatorAddNewItem_Click(sender, e); 
        }
        else if (intTotalRecords  > 0)
        {      
            BindingNavigatorMoveFirstItem_Click(sender, e);
        }
    }

    private void txtVisaNumber_TextChanged(object sender, EventArgs e)
    {

    }

    private void frmVisa_KeyDown(object sender, KeyEventArgs e)
    {
        try
        {
            switch (e.KeyData)
            {
                case Keys.F1:
                    BtnHelp_Click(sender, new EventArgs());
                    break;
                case Keys.Escape:
                    this.Close();
                    break;               
                case Keys.Control | Keys.Enter:
                    if(BindingNavigatorAddNewItem.Enabled)
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item                
                    break;
                case Keys.Alt | Keys.R:
                    if(BindingNavigatorDeleteItem.Enabled)
                        BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                    break;
                case Keys.Control | Keys.E:
                    if( btnClear.Enabled)
                        btnClear_Click(sender, new EventArgs());//Clear
                    break;
                case Keys.Control | Keys.Left:
                    if(BindingNavigatorMovePreviousItem.Enabled)
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                    break;
                case Keys.Control | Keys.Right:
                    if(BindingNavigatorMoveNextItem.Enabled)
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                    break;
                case Keys.Control | Keys.Up:
                    if(BindingNavigatorMoveFirstItem.Enabled)
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                    break;
                case Keys.Control | Keys.Down:
                    if(BindingNavigatorMoveLastItem.Enabled)
                      BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                    break;
                case Keys.Control | Keys.P:
                    if(BtnPrint.Enabled)
                        BtnPrint_Click(sender, new EventArgs());//Print
                    break;
                case Keys.Control | Keys.M:
                    if(BtnEmail.Enabled )
                     BtnEmail_Click(sender, new EventArgs());//Email
                    break;
                case Keys.Alt | Keys.I:
                    if(issueToolStripMenuItem.Enabled)
                        receiptIssueToolStripMenuItem_Click(sender, new EventArgs());  //issue document
                    break ;
                case Keys.Alt | Keys.E:
                    if (receiptIssueToolStripMenuItem.Enabled)
                        receiptIssueToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                    break ;
                 case Keys.Alt | Keys.N:
                    if (renewToolStripMenuItem.Enabled)
                        renewToolStripMenuItem_Click(sender, new EventArgs()) ;// renew document
                    break ;
                 case Keys.Alt | Keys.D:
                    if(mnuItemAttachDocuments.Enabled)
                        mnuItemAttachDocuments_Click(sender, new EventArgs()); // issue document
                    break ;
            }
        }
        catch (Exception)
        {
        }
    }

    private void BtnHelp_Click(object sender, EventArgs e)
    {
        using (FrmHelp objHelp = new FrmHelp())
        {
            objHelp.strFormName = "Visa";
            objHelp.ShowDialog();
        }
    }

    private void bnSaveItem_Click(object sender, EventArgs e)
    {
        this.SaveHandler();
    }

    private void txtSearch_TextChanged(object sender, EventArgs e)
    {
        mblnSearchStatus = false;
    }

    private void btnVisaType_Click(object sender, EventArgs e)
    {
        try
        {
            // Calling Reference form for ItemCategory

            if (ClsCommonSettings.IsArabicView)
            {
                MstrCommonMessage = "نوع التأشيرة";
            }
            else
            {
                MstrCommonMessage = "Visa Type";
            }

            int intVisaTypeID = Convert.ToInt32(CboVisaType.SelectedValue);
            FrmCommonRef objCommon = new FrmCommonRef(MstrCommonMessage , new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "VisaTypeID,VisaType,VisaTypeArb", "VisaTypeReference", "");
            objCommon.ShowDialog();
            objCommon.Dispose();
            LoadVisaType(); 
            if (objCommon.NewID != 0)
                cboCountry.SelectedValue = objCommon.NewID;
            else
                cboCountry.SelectedValue = intVisaTypeID;
        }
        catch (Exception Ex)
        {
            if (ClsCommonSettings.ShowErrorMess)
                MessageBox.Show("Error on FrmCommonRef() " + Ex.Message.ToString());
        }
    }

    private void cboVisaComapny_TextChanged(object sender, EventArgs e)
    {
        if (!this.formLoading && this.cboVisaComapny.SelectedIndex != -1)
        {
            this.OnInputChanged(sender, e);
        }
    }

    private void cboVisaDesignation_TextChanged(object sender, EventArgs e)
    {
        if (!this.formLoading && this.cboVisaDesignation.SelectedIndex != -1)
        {
            this.OnInputChanged(sender, e);
        }
    }

    private void txtUID_TextChanged(object sender, EventArgs e)
    {
        this.OnInputChanged(sender, e);
    }

    private void cboDesignation_Click(object sender, EventArgs e)
    {
        try
        {
            int DesignationID = Convert.ToInt32(cboVisaDesignation.SelectedValue);
            FrmCommonRef objCommon = new FrmCommonRef("Designation",
                new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DesignationID,IsPredefined, Designation,DesignationArb", "DesignationReference", "");
            objCommon.ShowDialog();
            objCommon.Dispose();
            int intWorkingAt = Convert.ToInt32(cboVisaDesignation.SelectedValue);
            LoadDesignation();
            if (objCommon.NewID != 0)
                cboVisaDesignation.SelectedValue = objCommon.NewID;
            else
                cboVisaDesignation.SelectedValue = intWorkingAt;
        }
        catch (Exception Ex)
        {
          
        }
    }

    

}
