﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MyPayfriend;

public partial class frmDocumentReceiptIssue : Form
{

    #region Declarations

    public DocumentType eDocumentType { get; set; }
    public OperationType eOperationType { get; set; }

    public int DocumentID { get; set; }
    public int EmployeeID { get; set; }
    public int OrderNo { get; set; }

    public string DocumentNumber { get; set; }

    public bool IsFromStockRegister { get; set; }
    public bool CanUpdateFromStockRegister = true;

    public DateTime? ExpiryDate = null;

    private clsMessage ObjUserMessage = null;

    private bool IsReceipted { get; set; }

    private bool CanClose = false;

    private bool MblnAddPermission;
    private bool MblnPrintEmailPermission;
    private bool MblnUpdatePermission;
    private bool MblnDeletePermission;

    bool MblnAddStatus = true;

    private clsMessage UserMessage
    {
        get
        {
            if (this.ObjUserMessage == null)
                this.ObjUserMessage = new clsMessage(FormID.Documents);
            return this.ObjUserMessage;
        }
    }
    private clsBLLDocumentMasterNew objDocumentMasterBLL { get; set; }
    ClsLogWriter objClsLogWriter;       // Object of the LogWriter class

    #endregion

    #region Constructor
    public frmDocumentReceiptIssue()
    {
        InitializeComponent();
        objClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
        this.Load += new EventHandler(frmDocumentMasterNew_Load);

        if (ClsCommonSettings.IsArabicView)
        {
            this.RightToLeftLayout = true;
            this.RightToLeft = RightToLeft.Yes;
            SetArabicControls();
        }
    }
    #endregion

    private void SetArabicControls()
    {
        ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
        objDAL.SetArabicVersion((int)FormID.DocumentIssueReceipt, this);
    }

    #region Properties


    private clsBLLDocumentMasterNew objClsBLLDocumentMaster;
    private clsBLLDocumentMasterNew MobjClsBLLDocumentMaster
    {
        get
        {
            if (this.objClsBLLDocumentMaster == null)
                this.objClsBLLDocumentMaster = new clsBLLDocumentMasterNew();

            return this.objClsBLLDocumentMaster;
        }
    }

    #endregion


    #region Events

    void frmDocumentMasterNew_Load(object sender, EventArgs e)
    {
        SetPermissions();
        SetInitials();
    }
    private void frmDocumentReceiptIssue_FormClosing(object sender, FormClosingEventArgs e)
    {
        if (btnOk.Enabled)
        {
            if (MessageBox.Show("Are you sure you want to close the form?", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                foreach (Form form in Program.objMain.MdiChildren)
                {
                    if (form.GetType() == new frmStockRegister().GetType())
                    {
                        ((frmStockRegister)form).FilldgvDocGrid();
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }
    }

    private void bnSaveItem_Click(object sender, EventArgs e)
    {
        if (IsValidate())
        {
            if (MessageBox.Show("Are you sure you want to save the document?", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                SaveDocumentReceiptIssue();
            // btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = false;

        }
    }
    private void btnSave_Click(object sender, EventArgs e)
    {
        bnSaveItem_Click(new object(), new EventArgs());
    }
    private void bnPrint_Click(object sender, EventArgs e)
    {
        LoadReport();
    }
    private void btnOk_Click(object sender, EventArgs e)
    {
        bnSaveItem_Click(new object(), new EventArgs());
        if (CanClose)
            this.Close();
    }
    private void btnCancel_Click(object sender, EventArgs e)
    {
        this.Close();
    }
    private void btnReceiptIssueReason_Click(object sender, EventArgs e)
    {
        try
        {
            FrmCommonRef objCommon;
            if (IsReceipted)
                objCommon = new FrmCommonRef("Document Status", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DocumentReceiptID,IsPredefined,DocumentReceipt,DocumentReceiptArb", "DocumentReceiptReference", "");
            else
                objCommon = new FrmCommonRef("Issue Reason", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "IssueReasonID,IsPredefined,IssueReason,IssueReasonArb", "DocumentIssueReference", "");

            objCommon.ShowDialog();
            objCommon.Dispose();
            int intDocumentStatusID = Convert.ToInt32(cboDocumentStatus.SelectedValue);

            GetAllDocumentReceiptIssueStatus();

            if (objCommon.NewID != 0)
                cboDocumentStatus.SelectedValue = objCommon.NewID;
            else
                cboDocumentStatus.SelectedValue = intDocumentStatusID;
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on btnReceiptIssueReason_Click() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }
    private void btnBinNumber_Click(object sender, EventArgs e)
    {
        try
        {
            FrmCommonRef objCommon = new FrmCommonRef("Bin Details", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "BinID,IsPredefined,BinName,BinNameArb", "DocumentBinReference", "");
            objCommon.ShowDialog();
            objCommon.Dispose();
            int intBinID = Convert.ToInt32(cboBinNumber.SelectedValue);

            GetAllBin();

            if (objCommon.NewID != 0)
                cboBinNumber.SelectedValue = objCommon.NewID;
            else
                cboBinNumber.SelectedValue = intBinID;
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on btnBinNumber_Click() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }
    // Combobox Textbox events
    public void OnInputChanged(object sender, EventArgs e)
    {
        ChangeStatus();
    }

    #endregion Events

    #region Methods

    private void SetPermissions()
    {
        try
        {
            IsReceipted = !clsBLLDocumentMasterNew.IsReceipted((int)eOperationType, (int)eDocumentType, DocumentID);

            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                if (IsReceipted)
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (eOperationType == OperationType.Company ? (Int32)eMenuID.CompanyDocumentReceipt : (Int32)eMenuID.EmployeeDocumentReceipt), out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                else
                    objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (eOperationType == OperationType.Company ? (Int32)eMenuID.CompanyDocumentIssue : (Int32)eMenuID.EmployeeDocumentIssue), out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            //if (IsReceipted && (int)eOperationType==2 && ClsCommonSettings.IsHrPowerEnabled)
            //    MblnAddPermission = MblnUpdatePermission = MblnDeletePermission = false;
        }
        catch (Exception ex)
        {
            MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            objClsLogWriter.WriteLog("Error on SetInitials() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }
    public void SetInitials()
    {
        try
        {
            if (eOperationType == OperationType.Company)
                rdbCompany.Checked = true;
            else
                rdbEmployee.Checked = true;

            GetAllDocumentTypes();
            GetAllReferenceNumberByOperationTypeID();
            GetAllBin();
            GetAllCustodian(IsFromStockRegister ? false : true);
            IsReceipted = !clsBLLDocumentMasterNew.IsReceipted((int)eOperationType, (int)eDocumentType, DocumentID);
            DisableControls();



            if (!IsFromStockRegister)
            {
                GetAllDocumentReceiptIssueStatus();
                cboDocumentType.SelectedValue = (int)eDocumentType;
                txtDocumentNumber.Text = DocumentNumber;
                cboBinNumber.SelectedValue = clsBLLDocumentMasterNew.GetBinID((int)eOperationType, (int)eDocumentType, DocumentID);
                ChangeFormForReceipt_Issue();
                bnPrint.Enabled = false;
            }
            else
            {
                GetDocumentDetailsByOrderNo(); // From stock register bottom grid 
            }
            btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = false;

            cboCustodian.Select();
            MblnAddStatus = true;
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on SetInitials() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }
    private void ChangeStatus()
    {
        errDocuments.Clear();
        if (ClsCommonSettings.IsHrPowerEnabled)
        {
            if (eOperationType == OperationType.Employee &&  IsReceipted==false)
            {
                btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = false;
                return;
            }
        }
        if (MblnAddStatus) // add mode
        {
            btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = (!(CanUpdateFromStockRegister) ? CanUpdateFromStockRegister : MblnAddPermission); ;
        }
        else  //edit mode
        {

            btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = (!(CanUpdateFromStockRegister) ? CanUpdateFromStockRegister : MblnUpdatePermission);
        }

    }
    private void GetAllReferenceNumberByOperationTypeID()
    {
        cboReferenceNumber.DataSource = clsBLLDocumentMasterNew.GetAllReferenceNumberByOperationTypeID((int)OperationType.Employee, false);
        cboReferenceNumber.DisplayMember = "ReferenceNumber";
        cboReferenceNumber.ValueMember = "ReferenceID";
        cboReferenceNumber.SelectedIndex = -1;
     }
    private void GetAllDocumentTypes()
    {
        cboDocumentType.DataSource = clsBLLDocumentMasterNew.GetAllDocumentTypes();
        cboDocumentType.DisplayMember = "DocumentType";
        cboDocumentType.ValueMember = "DocumentTypeID";
    }
    private void GetAllBin()
    {
        cboBinNumber.DataSource = clsBLLDocumentMasterNew.GetAllBin();
        cboBinNumber.DisplayMember = "BinName";
        cboBinNumber.ValueMember = "BinID";
        cboBinNumber.SelectedIndex = -1;
    }
    private void GetAllCustodian(bool Status)
    {
        cboCustodian.DataSource = clsBLLDocumentMasterNew.GetAllCustodian(Status);
        cboCustodian.DisplayMember = "EmployeeFullName";
        cboCustodian.ValueMember = "EmployeeID";
        cboCustodian.SelectedIndex = -1;
    }
    private void GetAllDocumentReceiptIssueStatus()
    {
        if (IsReceipted)
        {
            cboDocumentStatus.DataSource = clsBLLDocumentMasterNew.GetAllDocumentReceipts();
            cboDocumentStatus.DisplayMember = "DocumentReceipt";
            cboDocumentStatus.ValueMember = "DocumentReceiptID";

        }
        else
        {

            cboDocumentStatus.DataSource = clsBLLDocumentMasterNew.GetAllDocumentIssues();
            cboDocumentStatus.DisplayMember = "IssueReason";
            cboDocumentStatus.ValueMember = "IssueReasonID";

        }
        cboDocumentStatus.SelectedIndex = -1;
    }
    private void DisableControls()
    {
        rdbCompany.Enabled = rdbEmployee.Enabled = cboDocumentType.Enabled = txtDocumentNumber.Enabled = false;

        if (eOperationType == OperationType.Company)
        {
            if (IsReceipted)
                cboReferenceNumber.Enabled = true;
            else
            {
                cboReferenceNumber.Enabled = false;
                cboReferenceNumber.SelectedValue = clsBLLDocumentMasterNew.GetEmployeeID((int)eOperationType, (int)eDocumentType, DocumentID);
            }
        }
        else
        {
            cboReferenceNumber.Enabled = false;
            cboReferenceNumber.SelectedValue = EmployeeID;// clsBLLDocumentMasterNew.GetEmployeeID((int)eOperationType, (int)eDocumentType, DocumentID);
        }
    }
    private void GetDocumentDetailsByOrderNo()
    {
        using (DataTable dt = clsBLLDocumentMasterNew.GetDocumentDetailsByOrderNo((int)eOperationType, (int)eDocumentType, DocumentID, OrderNo))
        {
            if (dt.Rows.Count > 0)
            {

                bnPrint.Enabled = MblnPrintEmailPermission;

                //EnableDisableNavigatorActionButtons(true); 
                DataRow dr = dt.Rows[0];
                DocumentID = dr["DocumentID"].ToInt32();
                OrderNo = dr["OrderNo"].ToInt32();

                IsReceipted = Convert.ToBoolean(dr["IsReceipted"]);
                //receiptIssueToolStripMenuItem.Text = IsReceipted ? "Issue" : "Receipt";
                ChangeFormForReceipt_Issue();

                GetAllDocumentReceiptIssueStatus();

                // cboOperationType.SelectedValue = dr["OperationTypeID"].ToInt16();
                cboDocumentType.SelectedValue = dr["DocumentTypeID"].ToInt16();
                cboReferenceNumber.SelectedValue = dr["ReceivedFrom"].ToInt32();
                cboCustodian.SelectedValue = dr["Custodian"].ToInt32();
                cboBinNumber.SelectedValue = dr["BinID"].ToInt16();

                if (IsReceipted)
                    cboDocumentStatus.SelectedValue = dr["DocumentStatusID"].ToInt16();
                else
                    cboDocumentStatus.SelectedValue = dr["IssueReasonID"].ToInt16();

                txtDocumentNumber.Text = dr["DocumentNumber"].ToString();
                txtRemarks.Text = dr["Remarks"].ToString();

                if (dr["ReceiptIssueDate"] != null && dr["ReceiptIssueDate"] != DBNull.Value)
                    dtpTransactionDate.Value = dr["ReceiptIssueDate"].ToDateTime();

                if (dr["ExpectedReturnDate"] != null && dr["ExpectedReturnDate"] != DBNull.Value)
                {
                    dtpExpectedReturnDate.Checked = true;
                    dtpExpectedReturnDate.Value = Convert.ToDateTime(dr["ExpectedReturnDate"]);
                }
                else
                    dtpExpectedReturnDate.Checked = false;
            }
        }
    }
    private void ClearAll()
    {

        cboDocumentType.SelectedIndex = -1;
        cboReferenceNumber.SelectedIndex = -1;
        cboBinNumber.SelectedIndex = -1;
        cboCustodian.SelectedIndex = -1;
        cboDocumentStatus.SelectedIndex = -1;

        txtDocumentNumber.Text = "";
        txtRemarks.Text = "";

        // dtpExpectedReturnDate.MinDate =  dtpTransactionDate.MaxDate = dtpTransactionDate.Value = dtpExpiryDate.Value = clsBLLDocumentMasterNew.GetCurrentDate();    
    }
    private void ChangeFormForReceipt_Issue()
    {
        txtRemarks.Text = "";
        if (IsReceipted)  // Receipt
        {
            if (ClsCommonSettings.IsArabicView)
            {
                this.Text = "الوثائق - الإيصال";
                lblReceiptIssueHeading.Text = "معلومات استلام"; // Heading
                lblTransactionDate.Text = "تاريخ استلام";
                lblReceivedBy.Text = "تلقى بواسطة";
                lblDocumentStatus.Text = "الحالة ثيقة";
                lblTypeName.Text = "تلقى من";
            }
            else
            {

                this.Text = "Documents - Receipt";
                lblReceiptIssueHeading.Text = "Receipt Info"; // Heading
                lblTransactionDate.Text = "Receipt Date";
                lblReceivedBy.Text = "Received By";
                lblDocumentStatus.Text = "Document Status";
                lblTypeName.Text = "Received From";
            }
            lblExpRetDate.Visible = false;
            dtpExpectedReturnDate.Visible = false;
            cboBinNumber.Enabled = btnBinNumber.Enabled = true;
        }
        else
        {
            if (ClsCommonSettings.IsArabicView)
            {
                this.Text = "الوثائق - العدد";
                lblReceiptIssueHeading.Text = "معلومات القضية"; // Heading
                lblTransactionDate.Text = "تاريخ الاصدار";
                lblReceivedBy.Text = "الصادرة عن";
                lblDocumentStatus.Text = "سبب المشكلة";
                lblTypeName.Text = "أصدرت ل";
            }
            else
            {
                this.Text = "Documents - Issue";
                lblReceiptIssueHeading.Text = "Issue Info"; // Heading
                lblTransactionDate.Text = "Issue Date";
                lblReceivedBy.Text = "Issued By";
                lblDocumentStatus.Text = "Reason for Issue";
                lblTypeName.Text = "Issued To";
            }
            dtpExpectedReturnDate.Visible = true;
            lblExpRetDate.Visible = true;
            cboBinNumber.Enabled = btnBinNumber.Enabled = false;

        }
    }
    private bool IsValidate()
    {
        CanClose = false;
        errDocuments.Clear();


        if (cboDocumentType.SelectedIndex < 0)
        {
            UserMessage.ShowMessage(1051);
            SetErrorControl(cboDocumentType);
            return false;
        }

        else if (cboReferenceNumber.SelectedIndex < 0)
        {
            MessageBox.Show("Please select received from ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            SetErrorControl(cboReferenceNumber);
            return false;
        }
        else if (txtDocumentNumber.Text.Trim() == string.Empty)
        {
            UserMessage.ShowMessage(1054);
            SetErrorControl(txtDocumentNumber);
            return false;
        }

        if (IsReceipted)
        {
            if (cboCustodian.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(1063);
                SetErrorControl(cboCustodian);
                return false;
            }
            else if (cboDocumentStatus.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(1065);
                SetErrorControl(cboDocumentStatus);
                return false;
            }

            else if (dtpTransactionDate.Value.Date > clsBLLDocumentMasterNew.GetCurrentDate())
            {
                MessageBox.Show("Receipt date must not be a future date", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }
        else
        {

            if (cboCustodian.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(1064);
                SetErrorControl(cboCustodian);
                return false;
            }
            else if (cboDocumentStatus.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(1067);
                SetErrorControl(cboDocumentStatus);
                return false;
            }

            else if (dtpTransactionDate.Value.Date > clsBLLDocumentMasterNew.GetCurrentDate())
            {
                MessageBox.Show("Issue date must not be a future date", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else if (dtpExpectedReturnDate.Checked && (dtpExpectedReturnDate.Value.Date < dtpTransactionDate.Value.Date))
            {
                MessageBox.Show("Expected return date must be greater than or equal to issue date", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }


        }
        if (cboBinNumber.SelectedIndex < 0)
        {
            UserMessage.ShowMessage(1066);
            SetErrorControl(cboBinNumber);
            return false;
        }

        //if (clsBLLDocumentMasterNew.IsExistsDocumentNumber(DocumentID, txtDocumentNumber.Text.Trim()))
        //{
        //    UserMessage.ShowMessage(1055);
        //    SetErrorControl(txtDocumentNumber);
        //    return false;
        //}
        CanClose = true;
        return true;

    }
    private void SetErrorControl(Control c)
    {
        c.Focus();
        errDocuments.SetError(c, "  ");
    }
    private bool SaveDocumentReceiptIssue()
    {
        try
        {
            DateTime? ExpectedReturnDate = null;

            if (dtpExpectedReturnDate.Checked)
                ExpectedReturnDate = Convert.ToDateTime(dtpExpectedReturnDate.Value);
            objDocumentMasterBLL = new clsBLLDocumentMasterNew();
            objDocumentMasterBLL.objClsDTODocumentMaster = new clsDTODocumentMaster()
            {
                DocumentID = DocumentID,
                OperationTypeID = (int)eOperationType,
                DocumentTypeID = cboDocumentType.SelectedValue.ToInt32(),
                DocumentNumber = txtDocumentNumber.Text.Trim(),
                ReceiptIssueDate = dtpTransactionDate.Value,
                Custodian = cboCustodian.SelectedValue.ToInt32(),
                DocumentStatusID = cboDocumentStatus.SelectedValue.ToInt16(),
                BinID = cboBinNumber.SelectedValue.ToInt16(),
                ReceiptIssueReasonID = cboDocumentStatus.SelectedValue.ToInt16(),
                Remarks = txtRemarks.Text.Trim(),
                IsReceipted = IsReceipted,
                OrderNo = OrderNo,
                ExpectedReturnDate = ExpectedReturnDate,
                ReceivedFrom = Convert.ToInt32(cboReferenceNumber.SelectedValue),
                ExpiryDate = ExpiryDate

            };

            OrderNo = objDocumentMasterBLL.SaveDocumentReceiptIssue();
            if (OrderNo > 0)
            {
                clsAlerts objclsAlerts = new clsAlerts();
                if (!IsReceipted)
                {
                    if (ExpectedReturnDate != null)
                    {
                        objclsAlerts.AlertMessage(cboDocumentType.SelectedValue.ToInt32(), cboDocumentType.Text, cboDocumentType.Text + " Document Number", DocumentID, txtDocumentNumber.Text.Trim(), dtpExpectedReturnDate.Value, "Handover", cboCustodian.SelectedValue.ToInt32(), cboReferenceNumber.Text, rdbCompany.Checked,0);
                    }
                }

                MessageBox.Show("Document saved successfully", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                bnPrint.Enabled = MblnPrintEmailPermission;
                btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = false;

                if (bnPrint.Enabled)
                {
                    if (MessageBox.Show("Do you want to generate a receipt for this transaction?", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = false;
                        LoadReport();
                    }
                    else
                    {
                        btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = false;
                    }
                }
                return true;
            }
            else
            {
                MessageBox.Show("Failed to save documents", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on SaveDocumentReceiptIssue() " + this.Name + " " + ex.Message.ToString(), 1);
            return false;
        }
    }
    public void LoadReport()
    {
        try
        {

            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = this.Text;
            ObjViewer.PiFormID = (int)FormID.DocumentIssueReceipt;
            ObjViewer.OperationTypeID = (int)eOperationType;
            ObjViewer.DocumentTypeID = (int)eDocumentType;
            ObjViewer.DocumentID = DocumentID;
            ObjViewer.IsReceipted = IsReceipted;
            ObjViewer.OrderID = OrderNo;
            ObjViewer.ShowDialog();
        }
        catch (Exception ex)
        {
            objClsLogWriter.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 1);
        }
    }

    #endregion

    private void frmDocumentReceiptIssue_Load(object sender, EventArgs e)
    {

    }
}

