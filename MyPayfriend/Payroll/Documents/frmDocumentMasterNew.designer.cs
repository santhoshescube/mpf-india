﻿
partial class frmDocumentMasterNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDocumentMasterNew));
            this.tmDocuments = new System.Windows.Forms.Timer(this.components);
            this.DocumentsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bnMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bnMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bnPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuItemAttachDocuments = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.receiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.RenewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnMail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.errDocuments = new System.Windows.Forms.ErrorProvider(this.components);
            this.rdbEmployee = new System.Windows.Forms.RadioButton();
            this.rdbCompany = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.cboReferenceNumber = new System.Windows.Forms.ComboBox();
            this.lblTypeName = new System.Windows.Forms.Label();
            this.btnDocumentType = new System.Windows.Forms.Button();
            this.txtDocumentNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboDocumentType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnActions = new DevComponents.DotNetBar.ButtonItem();
            this.btnInstallments = new DevComponents.DotNetBar.ButtonItem();
            this.btnExpense = new DevComponents.DotNetBar.ButtonItem();
            this.btnApprove = new DevComponents.DotNetBar.ButtonItem();
            this.btnReject = new DevComponents.DotNetBar.ButtonItem();
            this.btnDocuments = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem8 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem10 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem11 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.lblIssuedDate = new System.Windows.Forms.Label();
            this.dtpIssuedDate = new System.Windows.Forms.DateTimePicker();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.lblRenewed = new System.Windows.Forms.Label();
            this.grpmain = new System.Windows.Forms.GroupBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentsBindingNavigator)).BeginInit();
            this.DocumentsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errDocuments)).BeginInit();
            this.ssStatus.SuspendLayout();
            this.grpmain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(17, 235);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(58, 13);
            label5.TabIndex = 141;
            label5.Text = "Remarks";
            // 
            // DocumentsBindingNavigator
            // 
            this.DocumentsBindingNavigator.AddNewItem = null;
            this.DocumentsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.DocumentsBindingNavigator.CountItem = this.bnCountItem;
            this.DocumentsBindingNavigator.DeleteItem = null;
            this.DocumentsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.bindingNavigatorSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.BtnClear,
            this.toolStripSeparator1,
            this.bnMoreActions,
            this.toolStripSeparator3,
            this.bnPrint,
            this.BtnMail,
            this.toolStripSeparator5,
            this.BtnHelp});
            this.DocumentsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.DocumentsBindingNavigator.MoveFirstItem = null;
            this.DocumentsBindingNavigator.MoveLastItem = null;
            this.DocumentsBindingNavigator.MoveNextItem = null;
            this.DocumentsBindingNavigator.MovePreviousItem = null;
            this.DocumentsBindingNavigator.Name = "DocumentsBindingNavigator";
            this.DocumentsBindingNavigator.PositionItem = this.bnPositionItem;
            this.DocumentsBindingNavigator.Size = new System.Drawing.Size(468, 25);
            this.DocumentsBindingNavigator.TabIndex = 4;
            this.DocumentsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bnCountItem
            // 
            this.bnCountItem.Name = "bnCountItem";
            this.bnCountItem.RightToLeftAutoMirrorImage = true;
            this.bnCountItem.Size = new System.Drawing.Size(35, 22);
            this.bnCountItem.Text = "of {0}";
            this.bnCountItem.ToolTipText = "Total number of items";
            // 
            // bnMoveFirstItem
            // 
            this.bnMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            this.bnMoveFirstItem.Name = "bnMoveFirstItem";
            this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveFirstItem.Text = "Move first";
            this.bnMoveFirstItem.Click += new System.EventHandler(this.bnMoveFirstItem_Click);
            // 
            // bnMovePreviousItem
            // 
            this.bnMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
            this.bnMovePreviousItem.Name = "bnMovePreviousItem";
            this.bnMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bnMovePreviousItem.Text = "Move previous";
            this.bnMovePreviousItem.Click += new System.EventHandler(this.bnMovePreviousItem_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPositionItem
            // 
            this.bnPositionItem.AccessibleName = "Position";
            this.bnPositionItem.AutoSize = false;
            this.bnPositionItem.Name = "bnPositionItem";
            this.bnPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bnPositionItem.Text = "0";
            this.bnPositionItem.ToolTipText = "Current position";
            this.bnPositionItem.TextChanged += new System.EventHandler(this.bnPositionItem_TextChanged);
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            this.bnMoveNextItem.Click += new System.EventHandler(this.bnMoveNextItem_Click);
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            this.bnMoveLastItem.Click += new System.EventHandler(this.bnMoveLastItem_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add new ";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.ToolTipText = "Save Item";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.ToolTipText = "Delete Item";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(23, 22);
            this.BtnClear.Text = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAttachDocuments,
            this.toolStripSeparator2,
            this.receiptToolStripMenuItem,
            this.IssueToolStripMenuItem,
            this.toolStripSeparator4,
            this.RenewToolStripMenuItem});
            this.bnMoreActions.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.bnMoreActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "Actions";
            this.bnMoreActions.ToolTipText = "Actions";
            // 
            // mnuItemAttachDocuments
            // 
            this.mnuItemAttachDocuments.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.mnuItemAttachDocuments.Name = "mnuItemAttachDocuments";
            this.mnuItemAttachDocuments.Size = new System.Drawing.Size(135, 22);
            this.mnuItemAttachDocuments.Text = "&Documents";
            this.mnuItemAttachDocuments.Click += new System.EventHandler(this.mnuItemAttachDocuments_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(132, 6);
            // 
            // receiptToolStripMenuItem
            // 
            this.receiptToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentIn;
            this.receiptToolStripMenuItem.Name = "receiptToolStripMenuItem";
            this.receiptToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.receiptToolStripMenuItem.Text = "R&eceipt";
            this.receiptToolStripMenuItem.Click += new System.EventHandler(this.receiptToolStripMenuItem_Click);
            // 
            // IssueToolStripMenuItem
            // 
            this.IssueToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DocumentOut;
            this.IssueToolStripMenuItem.Name = "IssueToolStripMenuItem";
            this.IssueToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.IssueToolStripMenuItem.Text = "&Issue";
            this.IssueToolStripMenuItem.Click += new System.EventHandler(this.IssueToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(132, 6);
            // 
            // RenewToolStripMenuItem
            // 
            this.RenewToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.renew;
            this.RenewToolStripMenuItem.Name = "RenewToolStripMenuItem";
            this.RenewToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.RenewToolStripMenuItem.Text = "Re&new";
            this.RenewToolStripMenuItem.Click += new System.EventHandler(this.RenewToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator3.Visible = false;
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(23, 22);
            this.bnPrint.ToolTipText = "Print";
            this.bnPrint.Click += new System.EventHandler(this.bnPrint_Click);
            // 
            // BtnMail
            // 
            this.BtnMail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnMail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.BtnMail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnMail.Name = "BtnMail";
            this.BtnMail.Size = new System.Drawing.Size(23, 22);
            this.BtnMail.Text = "Email";
            this.BtnMail.Click += new System.EventHandler(this.BtnMail_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 20);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // errDocuments
            // 
            this.errDocuments.ContainerControl = this;
            this.errDocuments.RightToLeft = true;
            // 
            // rdbEmployee
            // 
            this.rdbEmployee.AutoSize = true;
            this.rdbEmployee.Location = new System.Drawing.Point(136, 74);
            this.rdbEmployee.Name = "rdbEmployee";
            this.rdbEmployee.Size = new System.Drawing.Size(71, 17);
            this.rdbEmployee.TabIndex = 128;
            this.rdbEmployee.Text = "Employee";
            this.rdbEmployee.UseVisualStyleBackColor = true;
            this.rdbEmployee.CheckedChanged += new System.EventHandler(this.rdbCompany_CheckedChanged);
            // 
            // rdbCompany
            // 
            this.rdbCompany.AutoSize = true;
            this.rdbCompany.Location = new System.Drawing.Point(213, 74);
            this.rdbCompany.Name = "rdbCompany";
            this.rdbCompany.Size = new System.Drawing.Size(69, 17);
            this.rdbCompany.TabIndex = 128;
            this.rdbCompany.Text = "Company";
            this.rdbCompany.UseVisualStyleBackColor = true;
            this.rdbCompany.CheckedChanged += new System.EventHandler(this.rdbCompany_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Type";
            this.label3.UseCompatibleTextRendering = true;
            // 
            // cboReferenceNumber
            // 
            this.cboReferenceNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReferenceNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReferenceNumber.BackColor = System.Drawing.SystemColors.Info;
            this.cboReferenceNumber.DropDownHeight = 134;
            this.cboReferenceNumber.FormattingEnabled = true;
            this.cboReferenceNumber.IntegralHeight = false;
            this.cboReferenceNumber.Location = new System.Drawing.Point(136, 102);
            this.cboReferenceNumber.Name = "cboReferenceNumber";
            this.cboReferenceNumber.Size = new System.Drawing.Size(301, 21);
            this.cboReferenceNumber.TabIndex = 1;
            this.cboReferenceNumber.SelectedIndexChanged += new System.EventHandler(this.cboReferenceNumber_SelectedIndexChanged);
            this.cboReferenceNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboReferenceNumber_KeyPress);
            // 
            // lblTypeName
            // 
            this.lblTypeName.AutoSize = true;
            this.lblTypeName.Location = new System.Drawing.Point(28, 106);
            this.lblTypeName.Name = "lblTypeName";
            this.lblTypeName.Size = new System.Drawing.Size(97, 13);
            this.lblTypeName.TabIndex = 18;
            this.lblTypeName.Text = "Reference Number";
            // 
            // btnDocumentType
            // 
            this.btnDocumentType.Location = new System.Drawing.Point(411, 130);
            this.btnDocumentType.Name = "btnDocumentType";
            this.btnDocumentType.Size = new System.Drawing.Size(26, 23);
            this.btnDocumentType.TabIndex = 3;
            this.btnDocumentType.Text = "...";
            this.btnDocumentType.UseVisualStyleBackColor = true;
            this.btnDocumentType.Click += new System.EventHandler(this.btnDocumentType_Click);
            this.btnDocumentType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DocumentMasterNew_KeyDown);
            // 
            // txtDocumentNumber
            // 
            this.txtDocumentNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtDocumentNumber.Location = new System.Drawing.Point(136, 161);
            this.txtDocumentNumber.MaxLength = 30;
            this.txtDocumentNumber.Name = "txtDocumentNumber";
            this.txtDocumentNumber.Size = new System.Drawing.Size(269, 20);
            this.txtDocumentNumber.TabIndex = 4;
            this.txtDocumentNumber.TextChanged += new System.EventHandler(this.txtDocumentNumber_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Document Number";
            // 
            // cboDocumentType
            // 
            this.cboDocumentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDocumentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDocumentType.BackColor = System.Drawing.SystemColors.Info;
            this.cboDocumentType.DropDownHeight = 134;
            this.cboDocumentType.FormattingEnabled = true;
            this.cboDocumentType.IntegralHeight = false;
            this.cboDocumentType.Items.AddRange(new object[] {
            "Passport"});
            this.cboDocumentType.Location = new System.Drawing.Point(136, 131);
            this.cboDocumentType.Name = "cboDocumentType";
            this.cboDocumentType.Size = new System.Drawing.Size(269, 21);
            this.cboDocumentType.TabIndex = 2;
            this.cboDocumentType.SelectedIndexChanged += new System.EventHandler(this.cboDocumentType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Document Type";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(302, 372);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 16;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(389, 372);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(15, 372);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnActions
            // 
            this.btnActions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.Name = "btnActions";
            this.btnActions.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnInstallments,
            this.btnExpense,
            this.btnApprove,
            this.btnReject,
            this.btnDocuments});
            this.btnActions.Text = "Actions";
            this.btnActions.Tooltip = "Actions";
            // 
            // btnInstallments
            // 
            this.btnInstallments.Name = "btnInstallments";
            this.btnInstallments.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.btnInstallments.Text = "Installments";
            this.btnInstallments.Tooltip = "Installments";
            // 
            // btnExpense
            // 
            this.btnExpense.Image = ((System.Drawing.Image)(resources.GetObject("btnExpense.Image")));
            this.btnExpense.Name = "btnExpense";
            this.btnExpense.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.btnExpense.Text = "Expense";
            this.btnExpense.Tooltip = "Expense";
            // 
            // btnApprove
            // 
            this.btnApprove.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.btnApprove.Text = "Approve";
            this.btnApprove.Tooltip = "Approve";
            // 
            // btnReject
            // 
            this.btnReject.Name = "btnReject";
            this.btnReject.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.btnReject.Text = "Reject";
            // 
            // btnDocuments
            // 
            this.btnDocuments.Name = "btnDocuments";
            this.btnDocuments.Text = "Documents";
            this.btnDocuments.Tooltip = "Documents";
            // 
            // buttonItem1
            // 
            this.buttonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem1.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem1.Image")));
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2,
            this.buttonItem3,
            this.buttonItem4,
            this.buttonItem5,
            this.buttonItem6});
            this.buttonItem1.Text = "Actions";
            this.buttonItem1.Tooltip = "Actions";
            // 
            // buttonItem2
            // 
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.buttonItem2.Text = "Installments";
            this.buttonItem2.Tooltip = "Installments";
            // 
            // buttonItem3
            // 
            this.buttonItem3.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem3.Image")));
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.buttonItem3.Text = "Expense";
            this.buttonItem3.Tooltip = "Expense";
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonItem4.Text = "Approve";
            this.buttonItem4.Tooltip = "Approve";
            // 
            // buttonItem5
            // 
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.buttonItem5.Text = "Reject";
            // 
            // buttonItem6
            // 
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.Text = "Documents";
            this.buttonItem6.Tooltip = "Documents";
            // 
            // buttonItem7
            // 
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem7.Image")));
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem8,
            this.buttonItem9,
            this.buttonItem10,
            this.buttonItem11,
            this.buttonItem12});
            this.buttonItem7.Text = "Actions";
            this.buttonItem7.Tooltip = "Actions";
            // 
            // buttonItem8
            // 
            this.buttonItem8.Name = "buttonItem8";
            this.buttonItem8.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlH);
            this.buttonItem8.Text = "Installments";
            this.buttonItem8.Tooltip = "Installments";
            // 
            // buttonItem9
            // 
            this.buttonItem9.Image = ((System.Drawing.Image)(resources.GetObject("buttonItem9.Image")));
            this.buttonItem9.Name = "buttonItem9";
            this.buttonItem9.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlE);
            this.buttonItem9.Text = "Expense";
            this.buttonItem9.Tooltip = "Expense";
            // 
            // buttonItem10
            // 
            this.buttonItem10.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem10.Name = "buttonItem10";
            this.buttonItem10.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.buttonItem10.Text = "Approve";
            this.buttonItem10.Tooltip = "Approve";
            // 
            // buttonItem11
            // 
            this.buttonItem11.Name = "buttonItem11";
            this.buttonItem11.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlR);
            this.buttonItem11.Text = "Reject";
            // 
            // buttonItem12
            // 
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.Text = "Documents";
            this.buttonItem12.Tooltip = "Documents";
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 399);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(468, 22);
            this.ssStatus.TabIndex = 311;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(34, 262);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(410, 92);
            this.txtRemarks.TabIndex = 15;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // lblIssuedDate
            // 
            this.lblIssuedDate.AutoSize = true;
            this.lblIssuedDate.Location = new System.Drawing.Point(29, 192);
            this.lblIssuedDate.Name = "lblIssuedDate";
            this.lblIssuedDate.Size = new System.Drawing.Size(64, 13);
            this.lblIssuedDate.TabIndex = 145;
            this.lblIssuedDate.Text = "Issued Date";
            // 
            // dtpIssuedDate
            // 
            this.dtpIssuedDate.CalendarForeColor = System.Drawing.Color.Red;
            this.dtpIssuedDate.CalendarMonthBackground = System.Drawing.Color.Yellow;
            this.dtpIssuedDate.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.dtpIssuedDate.CalendarTitleForeColor = System.Drawing.Color.Navy;
            this.dtpIssuedDate.CalendarTrailingForeColor = System.Drawing.Color.Fuchsia;
            this.dtpIssuedDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpIssuedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssuedDate.Location = new System.Drawing.Point(137, 188);
            this.dtpIssuedDate.Name = "dtpIssuedDate";
            this.dtpIssuedDate.Size = new System.Drawing.Size(107, 20);
            this.dtpIssuedDate.TabIndex = 144;
            this.dtpIssuedDate.ValueChanged += new System.EventHandler(this.dtpIssueDate_ValueChanged);
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.CalendarForeColor = System.Drawing.Color.Red;
            this.dtpExpiryDate.CalendarMonthBackground = System.Drawing.Color.Yellow;
            this.dtpExpiryDate.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.dtpExpiryDate.CalendarTitleForeColor = System.Drawing.Color.Navy;
            this.dtpExpiryDate.CalendarTrailingForeColor = System.Drawing.Color.Fuchsia;
            this.dtpExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(331, 188);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.Size = new System.Drawing.Size(107, 20);
            this.dtpExpiryDate.TabIndex = 144;
            this.dtpExpiryDate.ValueChanged += new System.EventHandler(this.dtpExpiryDate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(264, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 145;
            this.label2.Text = "Expiry Date";
            // 
            // lblRenewed
            // 
            this.lblRenewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenewed.ForeColor = System.Drawing.Color.Maroon;
            this.lblRenewed.Location = new System.Drawing.Point(348, 217);
            this.lblRenewed.Name = "lblRenewed";
            this.lblRenewed.Size = new System.Drawing.Size(71, 22);
            this.lblRenewed.TabIndex = 318;
            this.lblRenewed.Text = "Renewed";
            this.lblRenewed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblRenewed.Visible = false;
            // 
            // grpmain
            // 
            this.grpmain.Controls.Add(this.Label6);
            this.grpmain.Controls.Add(this.shapeContainer1);
            this.grpmain.Location = new System.Drawing.Point(12, 61);
            this.grpmain.Name = "grpmain";
            this.grpmain.Size = new System.Drawing.Size(450, 304);
            this.grpmain.TabIndex = 319;
            this.grpmain.TabStop = false;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(6, -1);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(107, 13);
            this.Label6.TabIndex = 37;
            this.Label6.Text = "Document Details";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(444, 285);
            this.shapeContainer1.TabIndex = 38;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.lineShape2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 21;
            this.lineShape2.X2 = 436;
            this.lineShape2.Y1 = 166;
            this.lineShape2.Y2 = 166;
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(468, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 322;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(12, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(307, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name/Code && Document Number";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "             ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // frmDocumentMasterNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(468, 421);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.lblRenewed);
            this.Controls.Add(this.rdbEmployee);
            this.Controls.Add(this.lblTypeName);
            this.Controls.Add(this.rdbCompany);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboDocumentType);
            this.Controls.Add(this.cboReferenceNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDocumentNumber);
            this.Controls.Add(this.btnDocumentType);
            this.Controls.Add(this.lblIssuedDate);
            this.Controls.Add(this.dtpIssuedDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpExpiryDate);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.DocumentsBindingNavigator);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(label5);
            this.Controls.Add(this.txtRemarks);
            this.Controls.Add(this.grpmain);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDocumentMasterNew";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Documents";
            this.Load += new System.EventHandler(this.frmDocumentMasterNew_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDocumentMasterNew_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DocumentMasterNew_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.DocumentsBindingNavigator)).EndInit();
            this.DocumentsBindingNavigator.ResumeLayout(false);
            this.DocumentsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errDocuments)).EndInit();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.grpmain.ResumeLayout(false);
            this.grpmain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmDocuments;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        private System.Windows.Forms.ToolStripButton bnPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ErrorProvider errDocuments;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboReferenceNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboDocumentType;
        private System.Windows.Forms.Label lblTypeName;
        private System.Windows.Forms.TextBox txtDocumentNumber;
        private System.Windows.Forms.Label label4;
        private DevComponents.DotNetBar.ButtonItem btnActions;
        private DevComponents.DotNetBar.ButtonItem btnInstallments;
        private DevComponents.DotNetBar.ButtonItem btnExpense;
        private DevComponents.DotNetBar.ButtonItem btnApprove;
        private DevComponents.DotNetBar.ButtonItem btnReject;
        private DevComponents.DotNetBar.ButtonItem btnDocuments;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.ButtonItem buttonItem8;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private DevComponents.DotNetBar.ButtonItem buttonItem10;
        private DevComponents.DotNetBar.ButtonItem buttonItem11;
        private DevComponents.DotNetBar.ButtonItem buttonItem12;
        private System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAttachDocuments;
        internal System.Windows.Forms.Button btnDocumentType;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        private System.Windows.Forms.ToolStripMenuItem receiptToolStripMenuItem;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.RadioButton rdbEmployee;
        private System.Windows.Forms.RadioButton rdbCompany;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.DateTimePicker dtpExpiryDate;
        internal System.Windows.Forms.Label lblIssuedDate;
        internal System.Windows.Forms.DateTimePicker dtpIssuedDate;
        private System.Windows.Forms.ToolStripMenuItem IssueToolStripMenuItem;
        public System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem RenewToolStripMenuItem;
        private System.Windows.Forms.Label lblRenewed;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.ToolStripButton BtnMail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.GroupBox grpmain;
        internal System.Windows.Forms.Label Label6;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.ToolStripLabel bnCountItem;
        internal System.Windows.Forms.ToolStripButton bnMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton bnMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox bnPositionItem;
        internal System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton bnMoveNextItem;
        internal System.Windows.Forms.ToolStripButton bnMoveLastItem;
        internal System.Windows.Forms.BindingNavigator DocumentsBindingNavigator;
    }
