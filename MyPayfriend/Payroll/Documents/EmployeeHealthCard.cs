﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

///
/// Modified By: Megha
/// Date: 20/08/2013
/// Performed Code Optimization and added Functionalities for: Renew/Issue, Search, Save, Delete
///

namespace MyPayfriend
{
    public partial class EmployeeHealthCard : Form
    {
        #region Declarations

        /* Variable Declarations */

        public long EmployeeID { get; set; }//From Employee View
        public int MiHealthCardID { get; set; }//From Navigator View

        public int MiRowNumber = 0;
        public int MiReordCount = 0;

        //------------permission-------------------//
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission        
        private Boolean blAddStatus;
        private Boolean MiOKClose = false;
        private bool mblnSearchStatus = false;

        //Receipt Or Issue Permissions
        public bool mblnAddIssue = false;
        public bool mblnAddReceipt = false;
        public bool mblnEmail = false;
        public bool mblnDelete = false;
        public bool mblnUpdate = false;
        public bool mblnRenew = false;

        //   Error Message display
        private string sCommonMessage = String.Empty;
        private ArrayList MaMessageArr;
        private string MsMessageCaption = ClsCommonSettings.MessageCaption;
        private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;

        // Object Declaration and Instantiation
        ClsCommonUtility objCommon;
        clsBLLHealthcard objHealthcard;
        private ClsNotification mObjNotification = null;
        private string strOf = "of ";
        #endregion Declarations

        #region Constructors

        /// <summary>
        /// Object Instantiation
        /// </summary>
        public EmployeeHealthCard()
        {
            InitializeComponent();
            objCommon = new ClsCommonUtility();
            objHealthcard = new clsBLLHealthcard();
            mObjNotification = new ClsNotification();
            
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        #endregion Constructors

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.HealthCard, this);

            ToolStripDropDownBtnMore.Text = "الإجراءات";
            ToolStripDropDownBtnMore.ToolTipText = "الإجراءات";
            BtnScan.Text = "وثائق";
            ReceiptToolStripMenuItem.Text = "استلام";
            IssueToolStripMenuItem.Text = "قضية";
            RenewToolStripMenuItem.Text = "جدد";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            strOf = "من ";
        }
        #region Events

        #region Form-Events
        /// <summary>
        /// Health Card Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EmployeeHealthCard_Load(object sender, EventArgs e)
        {
            RenewToolStripMenuItem.Checked = false;
            SetAutoCompleteList();//Retrieves all datas likely to be searched
            LoadMessages();
            SetPermissions();
            SetReceiptOrIssueLabel();

            ClearControls();//Clears Form Controls
            LoadInitials();//Adds a new Form

            if (MiHealthCardID > 0)//Navigator View
            {
                MiRowNumber = 1;
                BindingNavigatorCountItem.Text = "1";
                BindingNavigatorPositionItem.Text = "1";

                GetHealthCardDetails();
                RefernceDisplay();
            }
            else if (EmployeeID > 0)//Employee View
            {
                GetRecordCount();
                if (MiReordCount > 0)
                    BindingNavigatorMoveFirstItem_Click(sender, e);
                else
                    BtnCancel.Enabled = true;
                CboEmployee.Enabled = BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
            BindingNavigatorSaveItem.Enabled = false;
            CboEmployee.Select();
        }

        /// <summary>
        /// Event Called when a Form is closed while manipulating a record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EmployeeHealthCard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnOk.Enabled)
            {
                sCommonMessage = new ClsNotification().GetErrorMessage(MaMessageArr, 17108, out MmessageIcon);

                if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim().Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    objHealthcard = null;
                    mObjNotification = null;

                    e.Cancel = false;//Retains the form
                }

                else
                {
                    e.Cancel = true;//Closes the form
                }

            }
        }

        /// <summary>
        /// Event for defining short-cut keys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EmployeeHealthCard_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        if (BtnHelp.Enabled) BtnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape://Close form
                        this.Close();
                        break;

                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled) BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled) BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (BtnCancel.Enabled) BtnCancel_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (BindingNavigatorMovePreviousItem.Enabled) BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Move to Previous item
                        break;
                    case Keys.Control | Keys.Right:
                        if (BindingNavigatorMoveNextItem.Enabled) BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Move to Next item
                        break;
                    case Keys.Control | Keys.Up:
                        if (BindingNavigatorMoveFirstItem.Enabled) BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//Move to First item
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveLastItem.Enabled) BindingNavigatorMoveLastItem_Click(sender, new EventArgs());//Move to Last item
                        break;
                    case Keys.Control | Keys.P:
                        if (BtnPrint.Enabled) BtnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (BtnMail.Enabled) BtnMail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.Alt | Keys.I:
                        if (IssueToolStripMenuItem.Enabled)
                            IssueToolStripMenuItem_Click(sender, new EventArgs());  //issue document
                        break;
                    case Keys.Alt | Keys.E:
                        if (ReceiptToolStripMenuItem.Enabled)
                            ReceiptToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                        break;
                    case Keys.Alt | Keys.N:
                        if (RenewToolStripMenuItem.Enabled)
                            RenewToolStripMenuItem_Click(sender, new EventArgs());// renew document
                        break;
                    case Keys.Alt | Keys.D:
                        if (BtnScan.Enabled)
                            BtnScan_Click(sender, new EventArgs()); // issue document
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion Form-Events

        #region Save/Cancel/Addnew/Delete

        /// <summary>
        /// Adds a new Card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            ClearControls();
            LoadInitials();

            lblRenewed.Visible = false;
            RenewToolStripMenuItem.Checked = BindingNavigatorSaveItem.Enabled = false;
            CboEmployee.Focus();
        }

        /// <summary>
        /// Saves Card Details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_Click(object sender, EventArgs e)
        {
            MiOKClose = false;
            if (SaveHealthCardDetails())//Save
            {
                BtnCancel.Enabled = false;//BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = 
                ToolStripDropDownBtnMore.Enabled = MblnAddUpdatePermission;
                BindingNavigatorAddNewItem.Enabled = (MiHealthCardID > 0 ? false : MblnAddPermission);
                BtnPrint.Enabled = BtnMail.Enabled = MblnPrintEmailPermission;
                if (blAddStatus) BindingNavigatorDeleteItem.Enabled = false;
                else
                {
                    BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (MiHealthCardID > 0 ? false : MblnDeletePermission));
                    //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Health card could not be removed." : "Remove");
                }                
                SetAutoCompleteList();
                SetReceiptOrIssueLabel();
            }
        }

        /// <summary>
        /// Saves card information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            BtnSave_Click(sender, e);
        }

        /// <summary>
        /// To save data and close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOk_Click(object sender, EventArgs e)
        {
            BtnSave_Click(sender, e);
            if (MiOKClose)
                this.Close();
        }

        /// <summary>
        /// Deletes a Record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (TxtCardNumber.Tag.ToInt32() > 0)//Deletes if card Exists
            {
                int dtDocRequest = clsBLLDocumentMasterNew.GetDocRequestReferenceExists(TxtCardNumber.Tag.ToInt32(), (int)DocumentType.Health_Card);
                if (dtDocRequest > 0)
                {
                    string Message = "Cannot Delete as Document Request Exists.";
                    MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (MessageBox.Show("Do you want to delete the Health Card ?", MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DeleteHealthCard();
                        if (MiHealthCardID > 0)//Navigator View
                        {
                            BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = BtnCancel.Enabled = false;
                            TxtCardNumber.Enabled = TxtCardPersonalNumber.Enabled = DtpExpiryDate.Enabled = DtpIssueDate.Enabled = TxtRemarks.Enabled = false;
                        }
                    }
                }
            }
            else
                MessageBox.Show("No details to delete", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            RenewToolStripMenuItem.Checked = false;
        }

        /// <summary>
        /// Method for clearing all the Form Controls
        /// </summary>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            ClearControls();
            LoadInitials();
            BindingNavigatorSaveItem.Enabled = false;
        }

        private void BtnCancel1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion Save/Cancel/Addnew/Delete

        #region Bindingnavigator Record view Events

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            MiRowNumber = 1;
            GetHealthCardDetails();
            if (EmployeeID > 0)//Employee View
            {
                CboEmployee.Enabled = BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
                CboEmployee.SelectedValue = EmployeeID;
            }
            SetBindingNavigator();
            BtnCancel.Enabled = false;
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 9, out MmessageIcon);
            LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
            Timer.Enabled = true;
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            GetRecordCount();

            if (MiReordCount > 0)
            {
                MiRowNumber = BindingNavigatorPositionItem.Text.ToInt32() - 1;
                if (MiRowNumber <= 0)
                    MiRowNumber = 1;
                else
                {
                    GetHealthCardDetails();
                    if (EmployeeID > 0)//Employee View
                    {
                        CboEmployee.Enabled = BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
                    }
                    BtnCancel.Enabled = false;
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 10, out MmessageIcon);
                    LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                    Timer.Enabled = true;
                }
            }
            else
                MiRowNumber = 1;
            SetBindingNavigator();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();

            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            if (MiReordCount > 0)
            {
                if (MiReordCount == 1)
                    MiRowNumber = 1;
                else
                    MiRowNumber = MiRowNumber + 1;

                if (MiRowNumber > MiReordCount)
                    MiRowNumber = MiReordCount;
                else
                    GetHealthCardDetails();
                if (EmployeeID > 0)//Employee View
                {
                    CboEmployee.Enabled = BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
                }
                SetBindingNavigator();
                BtnCancel.Enabled = false;
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 11, out MmessageIcon);
                LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                Timer.Enabled = true;
            }
            else
                MiRowNumber = 0;
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            GetRecordCount();
            MiRowNumber = MiReordCount;
            GetHealthCardDetails();
            if (EmployeeID > 0)//Employee View
            {
                CboEmployee.Enabled = BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
            SetBindingNavigator();
            BtnCancel.Enabled = false;
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 12, out MmessageIcon);
            LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
            Timer.Enabled = true;
        }

        #endregion Bindingnavigator Record view Events

        #region Document/Receipt/Issue/Renew

        private void BtnScan_Click(object sender, EventArgs e)
        {
            int iCardID = TxtCardNumber.Tag.ToInt32();
            if (iCardID > 0)
            {
                using (FrmScanning objScanning = new FrmScanning())
                {
                    objScanning.MintDocumentTypeid = (int)DocumentType.Health_Card;
                    objScanning.MlngReferenceID = CboEmployee.SelectedValue.ToInt32();
                    objScanning.MintOperationTypeID = (int)OperationType.Employee;
                    objScanning.MstrReferenceNo = TxtCardNumber.Text;
                    objScanning.PblnEditable = true;
                    objScanning.MintVendorID = CboEmployee.SelectedValue.ToInt32();
                    objScanning.MintDocumentID = iCardID;
                    objScanning.ShowDialog();

                }
            }
        }

        private void ReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iDocID = TxtCardNumber.Tag.ToInt32();
            new frmDocumentReceiptIssue()
            {
                eOperationType = OperationType.Employee,
                eDocumentType = DocumentType.Health_Card,
                DocumentID = iDocID,
                DocumentNumber = TxtCardNumber.Text,
                EmployeeID = CboEmployee.SelectedValue.ToInt32(),
                ExpiryDate = DtpExpiryDate.Value
            }.ShowDialog();
            SetReceiptOrIssueLabel();
        }

        private void IssueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iDocID = TxtCardNumber.Tag.ToInt32();
            new frmDocumentReceiptIssue()
            {
                eOperationType = OperationType.Employee,
                eDocumentType = DocumentType.Health_Card,
                DocumentID = iDocID,
                DocumentNumber = TxtCardNumber.Text,
                EmployeeID = CboEmployee.SelectedValue.ToInt32(),
                ExpiryDate = DtpExpiryDate.Value
            }.ShowDialog();
            SetReceiptOrIssueLabel();
        }

        /// <summary>
        /// For Renewal of an Existing Card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RenewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TxtCardNumber.Tag.ToInt32() > 0)
            {
                if (MessageBox.Show("Do You want to Renew Health Card?".Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                else //YES Do you wish to add
                {
                    objHealthcard.PobjDTOHealthCard.CardID = TxtCardNumber.Tag.ToInt32();
                    objHealthcard.PobjDTOHealthCard.EmployeeID = CboEmployee.SelectedValue.ToInt32();

                    if (objHealthcard.UpdateHealthCard(TxtCardNumber.Tag.ToInt32()) > 0)
                    {
                        lblRenewed.Visible = true;
                        MiRowNumber = MiRowNumber - 1;
                        CboEmployee.Enabled = TxtCardNumber.Enabled = TxtCardPersonalNumber.Enabled = DtpIssueDate.Enabled = DtpExpiryDate.Enabled = false;
                        RenewToolStripMenuItem.Enabled = BindingNavigatorDeleteItem.Enabled = mblnUpdate = mblnDelete = false;
                    }
                    if (MessageBox.Show("Do You want to Add New Health Card?".Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }

                    LoadInitials();
                    BtnCancel.Enabled = false;
                    ShowRenewStatus();
                    TxtCardNumber.Text = string.Empty;
                }
            }
        }

        #endregion Document/Receipt/Issue/Renew

        #region Print/Email

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        /// <summary>
        /// To Retrieve and fill Email body with appropriate datas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void BtnMail_Click(object sender, EventArgs e)
        {
            int iCardID = TxtCardNumber.Tag.ToInt32();
            if (iCardID > 0)
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Employee Health Card";
                    ObjEmailPopUp.EmailFormType = EmailFormID.HealthCard;
                    ObjEmailPopUp.EmailSource = objHealthcard.DisplaHealthCardReport(iCardID);
                    ObjEmailPopUp.ShowDialog();
                }
            }
        }

        #endregion Print/Email

        #region Search

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;
            if (txtSearch.Text.Trim() == string.Empty)
                return;

            GetRecordCount();

            if (MiReordCount == 0) // No records
            {
                MessageBox.Show(mObjNotification.GetErrorMessage(MaMessageArr, 40, out MmessageIcon).Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
            }
            else if (MiReordCount > 0)
            {
                //Displays records matching searchkey
                if (txtSearch.Text.Trim() != "")
                {
                    BindingNavigatorMoveFirstItem_Click(null, null);
                }
                else
                {
                    LoadInitials();
                    BindingNavigatorMoveFirstItem_Click(sender, e);
                }
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                if (e.KeyCode == Keys.Enter)
                    btnSearch_Click(sender, null);
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }

        #endregion Search

        #region TextChanged Events

        private void TxtCardNumber_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void TxtCardPersonalNumber_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void DtpIssueDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void TxtRemarks_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus(sender, e);
        }

        private void DtpExpiryDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus(sender, e);
        }

        /// <summary>
        /// This method is called while Updating a record(ie; on key press) 
        /// </summary>
        private void ChangeStatus(object sender, EventArgs e)
        {
            if (blAddStatus)
                BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = MblnAddPermission;
            else
                BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = MblnUpdatePermission;
            ErrorProvider.Clear();
        }

        private void CboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboEmployee.DroppedDown = false;
        }

        private void CboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus(sender, e);
        }

        #endregion TextChanged Events

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "HealthCard"; Help.ShowDialog(); }
        }

        #endregion Events

        #region Methods

        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsBLLInsuranceCard.GetAutoCompleteList(txtSearch.Text.Trim(), EmployeeID, "CardNumber", "EmplyeeHealthCard");
            this.txtSearch.AutoCompleteCustomSource = objHealthcard.ConvertToAutoCompleteCollection(dt);
        }

        /// <summary>
        /// Messages from Notification Master are loaded initially
        /// </summary>
        public void LoadMessages()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = mObjNotification.FillMessageArray((int)FormID.HealthCard, ClsCommonSettings.ProductID);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.HealthCard, ClsCommonSettings.ProductID);
        }

        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.HealthCard, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            if (MblnAddPermission || MblnUpdatePermission)
                MblnAddUpdatePermission = true;
            else
                MblnAddUpdatePermission = false;
            BtnMail.Enabled = BtnPrint.Enabled = MblnPrintEmailPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;

            BtnOk.Enabled = BtnSave.Enabled = BtnCancel.Enabled = MblnAddUpdatePermission;

        }

        private void SetReceiptOrIssueLabel()
        {
            bool mblnAddIssue = false;
            bool mblnAddReceipt = false;
            bool mblnEmail = false;
            bool mblnDelete = false;
            bool mblnUpdate = false;

            clsBLLPermissionSettings objPermission1 = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objPermission1.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
                objPermission1.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
                objPermission1.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out mblnEmail, out mblnRenew, out mblnUpdate, out mblnDelete);
            }
            else
            {
                mblnAddIssue = mblnAddReceipt = mblnRenew = true;
            }

            RenewToolStripMenuItem.Enabled = (clsPassportBLL.IsEmployeeInService(CboEmployee.SelectedValue.ToInt64()) ? (lblRenewed.Visible ? false : mblnRenew) : false);
            
            IssueToolStripMenuItem.Enabled = ReceiptToolStripMenuItem.Enabled = false;
            bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Employee, (int)DocumentType.Health_Card, TxtCardNumber.Tag.ToInt32());

            if (IsReceipt)
                IssueToolStripMenuItem.Enabled = mblnAddIssue;
            else
                ReceiptToolStripMenuItem.Enabled = mblnAddReceipt;
        }

        /// <summary>
        /// Clears All Form Controls and loads on with default values
        /// </summary>
        private void ClearControls()
        {
            CboEmployee.SelectedIndex = -1;
            objHealthcard.PobjDTOHealthCard.EmployeeID = 0;
            objHealthcard.PobjDTOHealthCard.CardID = 0;
            TxtCardNumber.Clear();
            TxtCardNumber.Tag = "0";
            TxtCardPersonalNumber.Clear();
            TxtRemarks.Clear();
            txtSearch.Text = string.Empty;//Clear Search Field
            DtpExpiryDate.Value = DateTime.Now.AddDays(1);
            DtpIssueDate.Value = DateTime.Now.Date;
            lblRenewed.Visible = mblnSearchStatus = false;
            
        }

        /// <summary>
        /// Function to Add a new Card
        /// </summary>
        private void LoadInitials()
        {
            ToolStripDropDownBtnMore.Enabled = mblnSearchStatus = false;

            blAddStatus = true;
           
            TxtCardNumber.Tag = 0;

            GetRecordCount();
            LoadEmployees(0);

            MiRowNumber = MiReordCount;

            BindingNavigatorCountItem.Text = strOf + Convert.ToString(MiReordCount + 1) + "";
            BindingNavigatorPositionItem.Text = (MiReordCount + 1).ToString();
            MiRowNumber = BindingNavigatorPositionItem.Text.ToInt32();

            CboEmployee.Enabled = TxtCardNumber.Enabled = TxtCardPersonalNumber.Enabled = DtpExpiryDate.Enabled = DtpIssueDate.Enabled = true;

            SetBindingNavigator();
            SetEnableDisable();
            ShowRenewStatus();

            LblStatus.Text = "Please add new Information";
            Timer.Enabled = true;

            BtnCancel.Enabled = true;
            if (EmployeeID > 0)//Employee View
            {
                objHealthcard.PobjDTOHealthCard.EmployeeID = EmployeeID;
                LoadEmployees(1);
                CboEmployee.SelectedValue = EmployeeID;
                CboEmployee.Enabled = BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;//BtnCancel.Enabled = 

            }
            else
                CboEmployee.Enabled = true;
            if (MiReordCount == 0)
                BtnCancel.Enabled = true;

        }

        /// <summary>
        /// Fills Employee Combo
        /// </summary>
        /// <param name="intType"></param>
        private void LoadEmployees(int intType)
        {
            DataTable dtCombos = null;
            if (intType == 0 || intType == 1)
            {

                if (ClsCommonSettings.IsArabicView)
                {
                    if (EmployeeID > 0)
                    {
                        if (objHealthcard.PobjDTOHealthCard.CardID > 0)
                            dtCombos = objCommon.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "(E.WorkStatusID >= 6 or E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        else
                            dtCombos = objCommon.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "(E.WorkStatusID >= 6 and E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        CboEmployee.SelectedValue = EmployeeID;
                    }
                    else
                    {
                        if (objHealthcard.PobjDTOHealthCard.CardID > 0)
                            dtCombos = objCommon.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and (E.WorkStatusID >= 6 or E.EmployeeID =" + objHealthcard.PobjDTOHealthCard.EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        else
                            dtCombos = objCommon.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and E.WorkStatusID >= 6  ORDER BY E.EmployeeFullName" });
                    }
                }
                else
                {
                    if (EmployeeID > 0)
                    {
                        if (objHealthcard.PobjDTOHealthCard.CardID > 0)
                            dtCombos = objCommon.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "(E.WorkStatusID >= 6 or E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        else
                            dtCombos = objCommon.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "(E.WorkStatusID >= 6 and E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        CboEmployee.SelectedValue = EmployeeID;
                    }
                    else
                    {
                        if (objHealthcard.PobjDTOHealthCard.CardID > 0)
                            dtCombos = objCommon.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and (E.WorkStatusID >= 6 or E.EmployeeID =" + objHealthcard.PobjDTOHealthCard.EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        else
                            dtCombos = objCommon.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and E.WorkStatusID >= 6  ORDER BY E.EmployeeFullName" });
                    }
                }

                CboEmployee.DisplayMember = "EmployeeName";
                CboEmployee.ValueMember = "EmployeeID";
                CboEmployee.DataSource = dtCombos;
                if (TxtCardNumber.Tag.ToInt32() == 0 && blAddStatus == true)
                {
                    CboEmployee.Text = null;
                }
                if (objHealthcard.PobjDTOHealthCard.EmployeeID > 0)
                    CboEmployee.SelectedValue = objHealthcard.PobjDTOHealthCard.EmployeeID;

            }

        }

        /// <summary>
        /// Fill employee when navigate buttons
        /// </summary>
        private void FillEmployee()
        {
            DataTable datCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                    "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and  E.WorkStatusID >= 6 OR E.EmployeeID = " + objHealthcard.PobjDTOHealthCard.EmployeeID });

            }
            else
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                    "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and  E.WorkStatusID >= 6 OR E.EmployeeID = " + objHealthcard.PobjDTOHealthCard.EmployeeID });

            }
            CboEmployee.DisplayMember = "EmployeeName";
            CboEmployee.ValueMember = "EmployeeID";
            CboEmployee.DataSource = datCombos;
            if (objHealthcard.PobjDTOHealthCard.CardID == 0)
            {
                CboEmployee.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// method is called for retrieving and displaying Card details from Navigator view
        /// </summary>
        private void RefernceDisplay()
        {
            objHealthcard.PobjDTOHealthCard.CardID = MiHealthCardID;
            int RowNum = 0;

            RowNum = objHealthcard.GetRowNumber(MiHealthCardID);
            if (RowNum > 0)
            {
                MiRowNumber = 1;
                GetHealthCardDetails();//Retrieves Card Details
                EnableDisableReferenceDisplay();//Enable/Disable Buttons and BindingNavigator Controls
            }
        }

        private void EnableDisableReferenceDisplay()
        {
            BindingNavigatorAddNewItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BtnCancel.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            BindingNavigatorSaveItem.Enabled = BindingNavigatorDeleteItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = txtSearch.Enabled = btnSearch.Enabled = false;
            LblStatus.Text = "Health Card for " + CboEmployee.Text;
        }

        /// <summary>
        /// Gets the Total Row Count or based on Search factor
        /// </summary>
        private void GetRecordCount()
        {
            MiReordCount = objHealthcard.GetRowCount(EmployeeID, MiHealthCardID, txtSearch.Text.Trim());
        }

        /// <summary>
        /// Retrieves Card Info and loads onto Form Controls
        /// </summary>
        private void GetHealthCardDetails()
        {
            GetRecordCount();//Gets Record Count
            blAddStatus = false;
            ToolStripDropDownBtnMore.Enabled = true;

            if (MiReordCount > 0)
            {
                DataTable dtHealthCard = objHealthcard.GetHealthCardDetails(MiRowNumber, EmployeeID, MiHealthCardID, txtSearch.Text.Trim());
                if (dtHealthCard.Rows.Count > 0)
                {
                    //Loads Card Details
                    objHealthcard.PobjDTOHealthCard.EmployeeID = Convert.ToInt32(dtHealthCard.Rows[0]["EmployeeID"]);
                    FillEmployee();

                    CboEmployee.SelectedValue = Convert.ToString(dtHealthCard.Rows[0]["EmployeeID"]);
                    TxtCardNumber.Text = Convert.ToString(dtHealthCard.Rows[0]["CardNumber"]);
                    TxtCardNumber.Tag = Convert.ToString(dtHealthCard.Rows[0]["HealthCardId"]);
                    TxtCardPersonalNumber.Text = Convert.ToString(dtHealthCard.Rows[0]["CardPersonalNumber"]);
                    DtpIssueDate.Value = Convert.ToDateTime(dtHealthCard.Rows[0]["IssueDate"]);
                    DtpExpiryDate.Value = Convert.ToDateTime(dtHealthCard.Rows[0]["ExpiryDate"]);
                    TxtRemarks.Text = Convert.ToString(dtHealthCard.Rows[0]["Remarks"]);


                    SetReceiptOrIssueLabel();
                    if (Convert.ToBoolean(dtHealthCard.Rows[0]["IsRenewed"]))//Enable/Disable Renew Status
                    {
                        lblRenewed.Visible = true;
                        CboEmployee.Enabled = TxtCardNumber.Enabled = TxtCardPersonalNumber.Enabled = DtpExpiryDate.Enabled = DtpIssueDate.Enabled = false;
                        RenewToolStripMenuItem.Enabled = false;
                    }
                    else
                    {
                        lblRenewed.Visible = false;
                        CboEmployee.Enabled = TxtCardNumber.Enabled = TxtCardPersonalNumber.Enabled = DtpExpiryDate.Enabled = DtpIssueDate.Enabled = true;
                        RenewToolStripMenuItem.Enabled = true;
                    }
                    blAddStatus = false;
                    SetEnableDisable();
                }
                BindingNavigatorPositionItem.Text = MiRowNumber.ToString();
                BindingNavigatorCountItem.Text = strOf + Convert.ToString(MiReordCount) + "";

                SetBindingNavigator();
            }
            else
            {
                TxtCardNumber.Tag = 0;
            }
        }

        private void SetBindingNavigator()
        {
            /* Condition to Enable/Disable the Move First/Next/Previous/Last Navigator Keys based on the record count. ie; if 0 then Disable else Enable*/
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;

            if (MiHealthCardID > 0)//Navigator view
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
                BtnCancel.Enabled = false;
            }
            if (MiReordCount == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (MiRowNumber >= MiReordCount)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (MiRowNumber == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }

        }

        private bool SaveHealthCardDetails()
        {
            if (FormValidation())//Checks for Validation of Controls
            {
                if (blAddStatus)
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);//Save msg confirmation
                else
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);//Update msg confirmation

                if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = true;
                    return false;
                }
                else
                {
                    BtnSave.Enabled = BtnOk.Enabled = BindingNavigatorSaveItem.Enabled = false;
                }

                if (TxtCardNumber.Tag.ToInt32() > 0)
                {
                    blAddStatus = false;//False for Updation
                }

                if (blAddStatus)
                {
                    objHealthcard.PobjDTOHealthCard.CardID = 0;
                    RenewToolStripMenuItem.Checked = false;
                }
                else
                {
                    objHealthcard.PobjDTOHealthCard.CardID = TxtCardNumber.Tag.ToInt32();
                }
                objHealthcard.PobjDTOHealthCard.CardID = (TxtCardNumber.Tag).ToInt32();
                objHealthcard.PobjDTOHealthCard.CardNumber = TxtCardNumber.Text.Trim();
                objHealthcard.PobjDTOHealthCard.CardPersonalIDNumber = TxtCardPersonalNumber.Text.Trim();
                objHealthcard.PobjDTOHealthCard.EmployeeID = CboEmployee.SelectedValue.ToInt32();
                objHealthcard.PobjDTOHealthCard.IssueDate = DtpIssueDate.Value;
                objHealthcard.PobjDTOHealthCard.ExpiryDate = DtpExpiryDate.Value;
                objHealthcard.PobjDTOHealthCard.Remarks = TxtRemarks.Text.Trim();
                objHealthcard.PobjDTOHealthCard.IsRenewed = lblRenewed.Visible;

                Int32 iHealthCardID = objHealthcard.SaveHealthCardDetails(blAddStatus);//Save(true)/Update(false)
                if (iHealthCardID > 0)
                {
                    MiOKClose = true;

                    if (TxtCardNumber.Tag.ToInt32() > 0)
                    {
                        sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);//Update msg      
                    }
                    else
                    {
                        sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);//Save msg        
                    }
                    LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                    MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    TxtCardNumber.Tag = iHealthCardID;
                    clsAlerts objclsAlerts = new clsAlerts();

                    if (blAddStatus)
                    {
                        objclsAlerts.AlertMessage((int)DocumentType.Health_Card, "HealthCard", "HealthCardNumber", iHealthCardID, TxtCardNumber.Text, DtpExpiryDate.Value, "Emp", CboEmployee.SelectedValue.ToInt32(), CboEmployee.Text.Trim(), false,0);
                        blAddStatus = false;
                    }
                    else
                    {
                        if (!RenewToolStripMenuItem.Checked)
                        {
                            objclsAlerts.AlertMessage((int)DocumentType.Health_Card, "HealthCard", "HealthCardNumber", iHealthCardID, TxtCardNumber.Text, DtpExpiryDate.Value, "Emp", CboEmployee.SelectedValue.ToInt32(), CboEmployee.Text.Trim(), false,0);
                        }
                    }
                }
                else
                {
                    if (TxtCardNumber.Tag.ToInt32() > 0)
                        MessageBox.Show("Cannot update", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Cannot save", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Timer.Enabled = true;
                return true;

            }
            return false;
        }

        private bool FormValidation()
        {
            ErrorProvider.Clear();
            bool bReturnValue = true;
            Control cControl = new Control();

            if (CboEmployee.SelectedIndex == -1)//Employee Selection
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 9124, out MmessageIcon);
                cControl = CboEmployee;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(TxtCardNumber.Text.Trim()))   // Health Card Number
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 17101, out MmessageIcon);
                cControl = TxtCardNumber;
                bReturnValue = false;
            }
            else if (DtpIssueDate.Value > ClsCommonSettings.GetServerDate())//Issue Date>Current Date
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 17103, out MmessageIcon);
                cControl = DtpIssueDate;
                bReturnValue = false;
            }
            else if (DtpExpiryDate.Value.Date <= DtpIssueDate.Value.Date)//Issue Date>expiry date
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessageArr, 17102, out MmessageIcon);
                cControl = DtpExpiryDate;
                bReturnValue = false;
            }

            if (bReturnValue == false)//Display Message
            {
                MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                LblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                if (cControl != null)//Focus on Control
                {
                    ErrorProvider.SetError(cControl, sCommonMessage);
                    cControl.Focus();
                }
                Timer.Enabled = true;
            }

            return bReturnValue;
        }

        private void SetEnableDisable()
        {
            BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
            BtnPrint.Enabled = (blAddStatus == true ? false : MblnPrintEmailPermission);
            BtnMail.Enabled = (blAddStatus == true ? false : MblnPrintEmailPermission);
            ToolStripDropDownBtnMore.Enabled = (blAddStatus == true ? false : true);
            RenewToolStripMenuItem.Enabled = (clsPassportBLL.IsEmployeeInService(CboEmployee.SelectedValue.ToInt64()) ? (lblRenewed.Visible ? false : mblnRenew) : false);

            BindingNavigatorAddNewItem.Enabled = (blAddStatus == true ? false : (MiHealthCardID > 0 ? false : MblnAddPermission));

            if (blAddStatus) BindingNavigatorDeleteItem.Enabled = false;
            else
            {
                BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (MiHealthCardID > 0 ? false : MblnDeletePermission));
                //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Health card could not be removed." : "Remove");
            }
            BtnCancel.Enabled = (blAddStatus == true ? true : false);
        }

        private void DeleteHealthCard()
        {
            if (objHealthcard.DeleteHealthCard(TxtCardNumber.Tag.ToInt32()))
            {
                MessageBox.Show("Deleted successfully", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearControls();
                LoadInitials();//Add new Card after deleting
                SetAutoCompleteList();
            }
            else
                MessageBox.Show("Cannot delete information", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        /// <summary>
        /// Gets the data to be printed
        /// </summary>
        private void LoadReport()
        {
            try
            {
                int iCardID = TxtCardNumber.Tag.ToInt32();
                if (iCardID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    DataTable datEmps = new ClsCommonUtility().FillCombos(new string[] { "CompanyID, FirstName", "EmployeeMaster", "  EmployeeID = " + CboEmployee.SelectedValue.ToInt32() });
                    if (datEmps.Rows.Count > 0)
                    {
                        ObjViewer.intCompanyID = datEmps.Rows[0]["CompanyID"].ToInt32();
                    }
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = iCardID;
                    ObjViewer.PiFormID = (int)FormID.HealthCard;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {

            }
        }

        private void ShowRenewStatus()
        {
            BtnSave.Enabled = BtnOk.Enabled = false;
            BtnPrint.Enabled = (blAddStatus == true ? false : true);
            BtnMail.Enabled = (blAddStatus == true ? false : true);
            ToolStripDropDownBtnMore.Enabled = (blAddStatus == true ? false : true);
            BindingNavigatorAddNewItem.Enabled = (blAddStatus == true ? false : true);
            if (blAddStatus)
            {
                lblRenewed.Visible = false;
                BindingNavigatorDeleteItem.Enabled = false;
                RenewToolStripMenuItem.Enabled = true;

            }
            else
            {
                BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : true);
                BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Sorry Unable to Delete as the Card has been Renewed" : "Delete");
            }
            BtnCancel.Enabled = (blAddStatus == true ? true : false);
        }

        #endregion Methods       
    }
}
