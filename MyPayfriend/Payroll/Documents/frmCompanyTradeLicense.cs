﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace MyPayfriend
{ 
    /// <summary>
    ///  Modified By        : Laxmi
    ///  Modified Date      : 22/8/2013
    ///  Purpose            : INsertion/Updation/Deletion of Trade licnese
    ///  FormID             : 184
    ///  ErrorCode          : 10600-10650
    ///  Functionalities    : Receipt/ Issue/ Renew/ Attach documents
    ///                     : Insertion into ALerts   WHEN Insert/Update tradelicense
    ///                     : Deletion from Alerts WHEN Renew and delete tradelicense
    /// </summary>
    /// 

    public partial class frmCompanyTradeLicense : Form
    {   
        #region Variable declarations

        // Public variable declarations
        public int CompanyID {get;set;}       // Public variable From Company form
        public int PintTradeLicenseID = 0;   //Public variable From Navigator


        // Private variable declarations
        bool MblnAddStatus = true;
        string  MstrDocName = string.Empty;
    
     
        private int MintTimerInterval;                  // To set timer interval
        private int MintCurrentRecCnt;                  // Current recourd count
        private int MintRecordCnt;                      // Total record count
  
        private bool MblnAddPermission = false;         //To Set Add Permission
        private bool MblnUpdatePermission = false;      //To Set Update Permission
        private bool MblnDeletePermission = false;      //To Set Delete Permission
        private bool MblnPrintEmailPermission = false;  //To Set PrintEmail Permission
        private bool mblnSearchStatus = false;
        bool mblnAddIssue, mblnAddReceipt,mblnAddRenew, mblnEmail, mblnDelete, mblnUpdate = false;  // Receipt-Issue Permission

        private string MsMessageCommon;
        private string MsMessageCaption;

        private ArrayList MsMessageArray;   // Error Message display
        private ArrayList MaStatusMessage;
        private string strOf = "of ";

        MessageBoxIcon MsMessageBoxIcon;

        #endregion VariableDeclarations

        clsAlerts objAlerts;
        clsBLLTradeLicense objBLLTradeLicense = null;
        clsBLLPermissionSettings objPermission;
        clsConnection objConnection;
        ClsLogWriter objClsLogWriter;       // Object of the LogWriter class
        ClsNotification objClsNotification; // Object of the Notification class

        #region Constructor
        public frmCompanyTradeLicense()
        {
            InitializeComponent();

            objPermission = new clsBLLPermissionSettings();
            objConnection = new clsConnection();
            objBLLTradeLicense  = new clsBLLTradeLicense ();
            objAlerts = new clsAlerts();
            objClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
            objClsNotification = new ClsNotification();

            tmTradeLicense.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer
            MintTimerInterval = ClsCommonSettings.TimerInterval; // current companyid
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }
        #endregion Constructor
        #region Events

        // Form Events
        private void frmCompanyTradeLicense_Load(object sender, EventArgs e)
        {
            //if (CompanyID == 0)
            //    CompanyID = ClsCommonSettings.CurrentCompanyID;

            SetAutoCompleteList();
            LoadMessage();
            LoadCombos();
            SetPermissions();
            LoadInitials();
            cboCompany.Select();  

            if (PintTradeLicenseID  > 0) //From navigator document view
            {
                MintCurrentRecCnt = 1;
                GetRecordCount();
                GetTradeLicenseDetails ();
                // Search button disabled
                bSearch.Enabled = false;
                lblStatus.Text = string.Empty;
                tmTradeLicense.Enabled = true;
            }
            else if (CompanyID  > 0) // From Employee- Load all cards of an employee
            {
                GetRecordCount();
                if (MintRecordCnt == 0) // No records
                {
                    MintRecordCnt = 1;
                    bnCountItem.Text = MintRecordCnt.ToString();
                    bnPositionItem.Text = MintCurrentRecCnt.ToString();

                }
                else if (MintRecordCnt > 0)
                    bnMoveFirstItem_Click(sender, e);
                txtSponsor.Select();
            }                    
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.TradeLicense, this);

            bnMoreActions.Text = "الإجراءات";
            bnMoreActions.ToolTipText = "الإجراءات";
            documentsToolStripMenuItem.Text = "وثائق";
            receiptToolStripMenuItem.Text = "استلام";
            issueToolStripMenuItem1.Text = "قضية";
            RenewToolStripMenuItem.Text = "جدد";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            //lblStatusShow.Text = "حالة :";
            strOf = "من ";
        }
        private void frmCompanyTradeLicense_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnOk.Enabled)
            {
                // Checking the changes are not saved and shows warning to the user
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 8, out MsMessageBoxIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    objAlerts = null;
                    objBLLTradeLicense = null;
                    objPermission = null;
                    objConnection = null;
                    objClsLogWriter = null;
                    objClsNotification = null;

                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void frmCompanyTradeLicense_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        bnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                   
                    case Keys.Control | Keys.Enter:
                        if (bnAddNewItem.Enabled) bnAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                       if (bnDeleteItem.Enabled ) bnDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (bnCancel.Enabled) bnCancel_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                       if (bnMovePreviousItem.Enabled ) bnMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                       if (bnMoveNextItem.Enabled ) bnMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        if ( bnMoveFirstItem.Enabled ) bnMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                       if (bnMoveLastItem.Enabled ) bnMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                       if (bnPrint.Enabled) bnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                       if (bnEmail.Enabled ) bnEmail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.Alt | Keys.I:
                        if (issueToolStripMenuItem1.Enabled)
                            issueToolStripMenuItem1_Click(sender, new EventArgs());  //issue document
                        break;
                    case Keys.Alt | Keys.E:
                        if (receiptToolStripMenuItem.Enabled)
                            receiptToolStripMenuItem_Click(sender, new EventArgs()); //receipt document
                        break;
                    case Keys.Alt | Keys.D:
                        if (documentsToolStripMenuItem.Enabled)
                            documentsToolStripMenuItem_Click(sender, new EventArgs()); // issue document
                        break;
                    case Keys.Alt | Keys.N:
                        if (RenewToolStripMenuItem.Enabled)
                            RenewToolStripMenuItem_Click(sender, new EventArgs());// renew document
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        //Button Events
        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            LoadInitials(); 
        }
        private void bnSaveItem_Click(object sender, EventArgs e)
        {
            if (SaveTradeLicense ()) DoActionsAfterSave();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveTradeLicense()) DoActionsAfterSave();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveTradeLicense())
            {
                btnOk.Enabled = false;
                this.Close();
            }
        }
        private void bnDeleteItem_Click(object sender, EventArgs e)
        {
            if (txtLicenseNumber.Tag.ToInt32() > 0)
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 13, out MsMessageBoxIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                DeleteInsuranceCard();
                //objAlerts.UpdateAlerts();
                LoadInitials();
                SetAutoCompleteList();
            }
        }
        private void bnCancel_Click(object sender, EventArgs e)
        {
            LoadInitials();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void bnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        private void bnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtLicenseNumber.Tag.ToInt32() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Company Trade License";
                        ObjEmailPopUp.EmailFormType = EmailFormID.TradeLicense;
                        ObjEmailPopUp.EmailSource = objBLLTradeLicense.DisplayTradeLicenseReport(txtLicenseNumber.Tag.ToInt32());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        private void bnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "TradeLicense"; Help.ShowDialog(); }

        }
        //Button navigation Events
        private void bnMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            GetRecordCount();   // get no of records
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;          // set current record
                GetTradeLicenseDetails();     // display qualification details

                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 9, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmTradeLicense.Enabled = true;
            }
        }
        private void bnMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty; 
            GetRecordCount();   // get no of records
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                    MintCurrentRecCnt = 1;
                else
                {
                    GetTradeLicenseDetails();     // display Trade license details

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmTradeLicense.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 1;
        }
        private void bnMoveNextItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            GetRecordCount();   // get no of records
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                    MintCurrentRecCnt = 1;
                else
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;

                if (MintCurrentRecCnt > MintRecordCnt)
                    MintCurrentRecCnt = MintRecordCnt;
                else
                {
                    GetTradeLicenseDetails();

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 11, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmTradeLicense.Enabled = true;
                }
            }
            else
                MintCurrentRecCnt = 0;
        }
        private void bnMoveLastItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    GetTradeLicenseDetails();

                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 12, out MsMessageBoxIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmTradeLicense.Enabled = true;

                }
            }
        }
     
        // Document/Receipt/Issue Button events
        private void receiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocID = txtLicenseNumber.Tag.ToInt32();
                new frmDocumentReceiptIssue()
                {
                    eOperationType = OperationType.Company,
                    eDocumentType = DocumentType.Trading_License,
                    DocumentID = iDocID,
                    DocumentNumber = txtLicenseNumber.Text,
                    EmployeeID = cboCompany.SelectedValue.ToInt32(),
                    ExpiryDate = dtpExpiryDate.Value
                }.ShowDialog();
                SetReceiptOrIssueLabel();
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on receiptToolStripMenuItem_Click " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }
        private void issueToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                int iDocID = txtLicenseNumber.Tag.ToInt32();
                new frmDocumentReceiptIssue()
                {
                    eOperationType = OperationType.Company,
                    eDocumentType = DocumentType.Trading_License,
                    DocumentID = iDocID,
                    DocumentNumber = txtLicenseNumber.Text,
                    EmployeeID = cboCompany.SelectedValue.ToInt32(),
                    ExpiryDate = dtpExpiryDate.Value
                }.ShowDialog();
                SetReceiptOrIssueLabel();
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on issueToolStripMenuItem1_Click " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }
        private void RenewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtLicenseNumber.Tag.ToInt32() > 0)
                {
                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10610, out MsMessageBoxIcon); // Do you wish to renew

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    else //YES Do you wish to add
                    {
                        objBLLTradeLicense.objDTOTradeLicense.TradeLicenseID = txtLicenseNumber.Tag.ToInt32();
                        objBLLTradeLicense.UpdateTradeLicenseForRenewal();

                        GetRecordCount();
                        GetTradeLicenseDetails();

                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10609, out MsMessageBoxIcon); // Do you wish to add
                        if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                        AddNewItem();
                        SetEnableDisable();
                        MblnAddStatus = true;
                        txtLicenseNumber.Text = string.Empty;
                        txtLicenseNumber.Select();
                    }
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on RenewToolStripMenuItem_Click " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }
        private void documentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int iCardID = txtLicenseNumber.Tag.ToInt32();
                if (iCardID > 0)
                {
                    using (FrmScanning objScanning = new FrmScanning())
                    {
                        objScanning.MintDocumentTypeid = (int)DocumentType.Trading_License;
                        objScanning.MlngReferenceID = cboCompany.SelectedValue.ToInt32();
                        objScanning.MintOperationTypeID = (int)OperationType.Company;
                        objScanning.MstrReferenceNo = txtLicenseNumber.Text;
                        objScanning.PblnEditable = true;
                        objScanning.MintVendorID = cboCompany.SelectedValue.ToInt32();
                        objScanning.MintDocumentID = iCardID;
                        objScanning.ShowDialog();

                    }
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on documentsToolStripMenuItem_Click " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        // Reference Button events
        private void btnCardIssued_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Province", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "ProvinceID,IsPredefined,ProvinceName,ProvinceNameArb", "ProvinceReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intLevelID = Convert.ToInt32(cboProvince.SelectedValue);

                if (ClsCommonSettings.IsArabicView)
                {
                    objConnection.Fill_Combo(cboProvince, "select ProvinceID,ProvinceNameArb as ProvinceName from ProvinceReference", "ProvinceID", "ProvinceName");
                }
                else
                {
                    objConnection.Fill_Combo(cboProvince, "select ProvinceID,ProvinceName  from ProvinceReference", "ProvinceID", "ProvinceName");

                }

                if (objCommon.NewID != 0)
                    cboProvince.SelectedValue = objCommon.NewID;
                else
                    cboProvince.SelectedValue = intLevelID;
            }

            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on BtnQualLevel_Click() " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        //Search button Events
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;
            GetRecordCount();

            if (MintRecordCnt == 0) // No records
            {
                MessageBox.Show(objClsNotification.GetErrorMessage(MsMessageArray, 40, out MsMessageBoxIcon).Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
            }
            else if (MintRecordCnt > 0)
            {
                AddNewItem();
                bnMoveFirstItem_Click(sender, e);
            }
        }

        // Combo events
        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }
        private void cboProvince_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboProvince.DroppedDown = false;
        }

        // Textbox events
        public void OnInputChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                if (e.KeyCode == Keys.Enter)
                    btnSearch_Click (sender, null);
            }
        }
        private void txtFee_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars;
            e.Handled = false;
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            }
            else
            {
                strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            }

            if (ClsCommonSettings.IsAmountRoundByZero == false && ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))))
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }
        //Timer Event
        private void tmTradeLicense_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            tmTradeLicense.Enabled = false;
            errTradelicense.Clear();
            tmTradeLicense.Stop();
        }
        #endregion Events
        #region Functions
        /// <summary>
        /// Load Messages
        /// </summary>
        /// <param name="intType"></param>
        private void LoadMessage()
        {
            MsMessageArray = new ArrayList();
            MaStatusMessage = new ArrayList();
            MsMessageArray = objClsNotification.FillMessageArray((int)FormID.TradeLicense, ClsCommonSettings.ProductID);
            MaStatusMessage = objClsNotification.FillStatusMessageArray((int)FormID.TradeLicense, ClsCommonSettings.ProductID);
        }
        /// <summary>
        /// Load all combobox
        /// </summary>
        private void LoadCombos()
        {
            try
            {
                objConnection.Fill_Combo(cboCompany, "SELECT  CompanyID,CompanyName  from CompanyMaster WHERE CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ")", "CompanyID", "CompanyName");
                if (CompanyID  > 0)
                    cboCompany.SelectedValue = CompanyID ;

                if (ClsCommonSettings.IsArabicView)
                {
                    objConnection.Fill_Combo(cboProvince, "select ProvinceID,ProvinceNameArb ProvinceName  from ProvinceReference", "ProvinceID", "ProvinceName");
                }
                else
                {
                    objConnection.Fill_Combo(cboProvince, "select ProvinceID,ProvinceName  from ProvinceReference", "ProvinceID", "ProvinceName");

                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadCombos() " + this.Name + " " + ex.Message.ToString(), 1);
                return;
            }
        }
        /// <summary>
        /// Permission settings
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.TradeLicense, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.CompanyDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.CompanyDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out mblnEmail, out mblnAddRenew, out mblnUpdate, out mblnDelete);
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
                mblnAddIssue = mblnAddReceipt = mblnAddRenew = true;
            }
        }
        /// <summary>
        /// Get total record count depending on EmployeeID/InsuranceCardID/Searchtext
        /// </summary>
        private void GetRecordCount()
        {
            try
            {
                MintRecordCnt = objBLLTradeLicense.GetRecordCount(CompanyID, PintTradeLicenseID, txtSearch.Text.Trim());
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on GetRecordCount " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }
        private void LoadInitials()
        {
            try
            {
                txtSearch.Text = "";
                mblnSearchStatus = false;


                AddNewItem();

                cboCompany.SelectedIndex = cboProvince.SelectedIndex = -1;
                txtLicenseNumber.Tag = "0";
                txtFee.Clear();
                txtCity.Text = txtPartner.Text = txtSponsor.Text = txtLicenseNumber.Text = txtRemarks.Text = string.Empty;
                dtpExpiryDate.Value = System.DateTime.Now.AddDays(1);
                dtpIssueDate.Value = DateTime.Today;

                errTradelicense.Clear();

                if (CompanyID > 0) // From Employee
                {
                    cboCompany.Text = string.Empty;
                    cboCompany.SelectedValue = CompanyID;
                    //cboCompany.Enabled = false;
                    txtSponsor.Focus();
                }
                else
                {
                    cboCompany.Enabled = true;
                    cboCompany.Focus();
                }
                SetEnableDisable();
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadInitials " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }
        private bool AddNewItem()
        {
            try
            {
                //txtSearch.Text = string.Empty;

                MblnAddStatus = (PintTradeLicenseID  > 0 ? false : true);
                lblRenewed.Visible = false;
                cboCompany.Enabled = true;
                txtCity.Enabled = txtPartner.Enabled = txtSponsor.Enabled = txtLicenseNumber.Enabled = txtFee.Enabled = true;
                cboProvince.Enabled =btnProvince.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = true;

                //bnDeleteItem.ToolTipText = "Remove";

                GetRecordCount();

                bnCountItem.Text = strOf + Convert.ToString(MintRecordCnt + 1) + "";
                bnPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
                MintCurrentRecCnt = MintRecordCnt + 1;

                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 655, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmTradeLicense.Enabled = true;
                return true;
            }
            catch (Exception Ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());
                return false;
            }
        }
        /// <summary>
        /// button enable/disable depends on New mode or update mode
        /// </summary>
        private void SetEnableDisable()
        {
            btnSave.Enabled = btnOk.Enabled = bnSaveItem.Enabled = false;
            bnPrint.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            bnEmail.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            bnMoreActions.Enabled = (MblnAddStatus == true ? false : true);

            bnAddNewItem.Enabled = (MblnAddStatus == true ? false : (PintTradeLicenseID > 0 ? false : MblnAddPermission));

            if (MblnAddStatus) bnDeleteItem.Enabled = false;
            else
            {
                bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (PintTradeLicenseID > 0 ? false : MblnDeletePermission));
                //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Trade license could not be removed." : "Remove");
            }
            bnCancel.Enabled = (MblnAddStatus == true ? true : false);

            if (PintTradeLicenseID <= 0)
            {
                bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = true;
                int iCurrentRec = Convert.ToInt32(bnPositionItem.Text); // Current position of the record

                if (iCurrentRec >= MintRecordCnt)  // If the current is last record, disables move next and last button
                {
                    bnMoveNextItem.Enabled = bnMoveLastItem.Enabled = false;
                }
                if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                {
                    bnMoveFirstItem.Enabled = bnMovePreviousItem.Enabled = false;
                }
            }
            else
                bnMovePreviousItem.Enabled = bnMoveFirstItem.Enabled = bnMoveLastItem.Enabled = bnMoveNextItem.Enabled = false;
        }
        /// <summary>
        /// Do actions after save
        /// Enable and disable form controls
        /// </summary>
        private void DoActionsAfterSave()
        {
            SetAutoCompleteList();
            MblnAddStatus = false;
            btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = bnCancel.Enabled = false;
            documentsToolStripMenuItem.Enabled = MblnAddPermission;
            bnAddNewItem.Enabled = (PintTradeLicenseID  > 0 ? false : MblnAddPermission);
            bnPrint.Enabled = bnEmail.Enabled = MblnPrintEmailPermission;
            if (MblnAddStatus) bnDeleteItem.Enabled = false;
            else
            {
                bnDeleteItem.Enabled = (lblRenewed.Visible ? false : (PintTradeLicenseID > 0 ? false : MblnDeletePermission));
                //bnDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Trade license could not be removed." : "Remove");
            }
            SetReceiptOrIssueLabel();
        }
        /// <summary>
        /// Set Receipt /Issue button style(enable/disable)
        /// </summary>
        private void SetReceiptOrIssueLabel()
        {
            bnMoreActions.Enabled = true;

            RenewToolStripMenuItem.Enabled = lblRenewed.Visible ? false : mblnAddRenew;
            issueToolStripMenuItem1.Enabled = receiptToolStripMenuItem.Enabled = false;
            bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Company, (int)DocumentType.Trading_License, txtLicenseNumber.Tag.ToInt32());

            if (IsReceipt)
                issueToolStripMenuItem1.Enabled = mblnAddIssue;
            else
                receiptToolStripMenuItem.Enabled = mblnAddReceipt;
        }

        private void ChangeStatus()
        {
            errTradelicense.Clear();
            if (MblnAddStatus) // add mode
            {
                btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = MblnAddPermission;
            }
            else  //edit mode
            {
                btnOk.Enabled = btnSave.Enabled = bnSaveItem.Enabled = MblnUpdatePermission;
            }
        }
        /// <summary>
        /// Validation of controls
        /// </summary>        
        private bool FormValidation()
        {
            errTradelicense.Clear();
            bool bReturnValue = true;
            Control cControl = new Control();

            if (Convert.ToInt32(cboCompany .SelectedValue) == 0) // Company Name
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10600, out MsMessageBoxIcon);
                cControl = cboCompany;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtSponsor.Text.Trim()))// Sponsor
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10601, out MsMessageBoxIcon);
                cControl = txtSponsor ;
                bReturnValue = false;
            }
            else if (txtFee.Text.ToDecimal() == 0) // Sponsor fee
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10602, out MsMessageBoxIcon);
                cControl = txtFee;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtLicenseNumber.Text.Trim())) // License Number
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10603, out MsMessageBoxIcon);
                cControl = txtLicenseNumber ;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtCity.Text.Trim())) // City
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10604, out MsMessageBoxIcon);
                cControl = txtCity;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtPartner.Text.Trim())) // Partner
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10605, out MsMessageBoxIcon);
                cControl = txtPartner;
                bReturnValue = false;
            }
            else if (dtpExpiryDate.Value.Date <= dtpIssueDate.Value.Date)
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10606, out MsMessageBoxIcon);
                cControl = dtpExpiryDate;
                bReturnValue = false;
            }
            else if (dtpIssueDate.Value > ClsCommonSettings.GetServerDate())
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10607, out MsMessageBoxIcon);
                cControl = dtpIssueDate;
                bReturnValue = false;
            }
            // Card Number Duplication 
            //else 
            //{
            //    if (objBLLInsuranceCard.CheckDuplication(( MblnAddStatus && txtCardNumber.Tag.ToInt32() >0 ? 0 : txtCardNumber.Tag.ToInt32()), txtCardNumber.Text.ToString()))
            //    {
            //        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 10507, out MsMessageBoxIcon);
            //        cControl = txtCardNumber;
            //        bReturnValue = false;
            //    }
            //}
            if (bReturnValue == false)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (cControl != null)
                {
                    errTradelicense.SetError(cControl, MsMessageCommon);
                    cControl.Focus();
                }
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmTradeLicense.Enabled = true;
            }
            return bReturnValue;
        }
        /// <summary>
        /// Save Insurance card
        /// </summary>
        private bool SaveTradeLicense()
        {
            cboCompany.Focus();

            bool blnReturnValue = false;

            try
            {
                if (FormValidation())
                {
                    if (MblnAddStatus)
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 1, out MsMessageBoxIcon);
                    else
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 3, out MsMessageBoxIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)

                        return false;

                    FillParameters();

                    txtLicenseNumber.Tag = objBLLTradeLicense.SaveTradeLicenseDetails(MblnAddStatus).ToInt32();

                    if (txtLicenseNumber.Tag.ToInt32() > 0)
                    {
                        if (PintTradeLicenseID  > 0)
                        {
                            PintTradeLicenseID = txtLicenseNumber.Tag.ToInt32();
                            MintCurrentRecCnt  = 1;
                        }
                        clsAlerts objclsAlerts = new clsAlerts();
                        objclsAlerts.AlertMessage((int)DocumentType.Trading_License, "TradeLicense", "TradeLicense Number", txtLicenseNumber.Tag.ToInt32(), txtLicenseNumber.Text, dtpExpiryDate.Value, "Comp", cboCompany.SelectedValue.ToInt32(), cboCompany.Text.Trim(), true,0 );

                        string strMsg = "";
                        if (MblnAddStatus)//insert
                        {
                            strMsg = objClsNotification.GetErrorMessage(MsMessageArray, 2, out MsMessageBoxIcon);
                        }
                        else
                        {
                            strMsg = objClsNotification.GetErrorMessage(MsMessageArray, 21, out MsMessageBoxIcon);
                        }
                        MessageBox.Show(strMsg.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = strMsg.Remove(0, strMsg.IndexOf("#") + 1);
                        tmTradeLicense.Enabled = true;
                        blnReturnValue = true;
                    }
                }

                return blnReturnValue;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on SaveTradeLicense() " + this.Name + " " + ex.Message.ToString(), 1);
                return blnReturnValue;
            }
        }
        /// <summary>
        /// Fill Parameters before save
        /// </summary>
        private void FillParameters()
        {

            objBLLTradeLicense.objDTOTradeLicense.TradeLicenseID = txtLicenseNumber.Tag.ToInt32();
            objBLLTradeLicense.objDTOTradeLicense.CompanyID = cboCompany.SelectedValue.ToInt32();
            objBLLTradeLicense.objDTOTradeLicense.Sponsor = txtSponsor.Text.Trim();
            objBLLTradeLicense.objDTOTradeLicense.SponserFee  = txtFee.Text.ToDecimal();
            objBLLTradeLicense.objDTOTradeLicense.LicenceNumber = txtLicenseNumber.Text.Trim();
            objBLLTradeLicense.objDTOTradeLicense.City = txtCity.Text.Trim();
            objBLLTradeLicense.objDTOTradeLicense.ProvinceID = cboProvince.SelectedValue.ToInt32();
            objBLLTradeLicense.objDTOTradeLicense.Partner = txtPartner.Text.Trim();
            objBLLTradeLicense.objDTOTradeLicense.IssueDate = dtpIssueDate.Value.Date;
            objBLLTradeLicense.objDTOTradeLicense.ExpiryDate = dtpExpiryDate.Value.Date;
            objBLLTradeLicense.objDTOTradeLicense.Remarks = txtRemarks.Text.Trim();
            objBLLTradeLicense.objDTOTradeLicense.IsRenewed = lblRenewed.Visible ? true : false;
        }
        /// <summary>
        /// Get Insurance card details
        /// </summary>
        private void GetTradeLicenseDetails()
        {
            try
            {
                DataTable dtTradeLicense = objBLLTradeLicense.GetTradeLicenseDetails(MintCurrentRecCnt, CompanyID, PintTradeLicenseID, txtSearch.Text.Trim());//get data from BLL
                if (dtTradeLicense.Rows.Count > 0)
                {
                    objBLLTradeLicense.objDTOTradeLicense.CompanyID = Convert.ToInt32(dtTradeLicense.Rows[0]["CompanyID"]);
                    cboCompany.SelectedValue = dtTradeLicense.Rows[0]["CompanyID"];
                    txtSponsor.Text = dtTradeLicense.Rows[0]["Sponsor"].ToString();
                    txtFee.Text = dtTradeLicense.Rows[0]["SponserFee"].ToString();
                    txtLicenseNumber.Tag = dtTradeLicense.Rows[0]["TradeLicenseID"].ToInt32();
                    txtLicenseNumber.Text = dtTradeLicense.Rows[0]["LicenceNumber"].ToString();
                    txtCity.Text = dtTradeLicense.Rows[0]["City"].ToString();
                    txtPartner.Text = dtTradeLicense.Rows[0]["Partner"].ToString();
                    cboProvince.SelectedValue = dtTradeLicense.Rows[0]["ProvinceID"];
                    dtpIssueDate.Value = dtTradeLicense.Rows[0]["IssueDate"].ToDateTime();
                    dtpExpiryDate.Value = dtTradeLicense.Rows[0]["ExpiryDate"].ToDateTime();
                    txtRemarks.Text = dtTradeLicense.Rows[0]["Remarks"].ToString();

                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        txtFee.Text = txtFee.Text.ToDecimal().ToString("F" + 0);
                    }
                    if (Convert.ToBoolean(dtTradeLicense.Rows[0]["IsRenewed"]))
                    {
                        lblRenewed.Visible = true;
                        cboCompany.Enabled = txtCity.Enabled = txtPartner.Enabled = txtSponsor.Enabled = txtLicenseNumber.Enabled = txtFee.Enabled = false;
                        cboProvince.Enabled = btnProvince.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = false;
                    }
                    else
                    {
                        lblRenewed.Visible = false;
                        cboCompany.Enabled = true;
                        txtCity.Enabled = txtPartner.Enabled = txtSponsor.Enabled = txtLicenseNumber.Enabled = txtFee.Enabled = true;
                        cboProvince.Enabled = btnProvince.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = true;
                    }


                    MblnAddStatus = false;

                    MstrDocName = txtLicenseNumber.Text.Replace("'", "").Trim();

                    bnPositionItem.Text = MintCurrentRecCnt.ToString();
                    bnCountItem.Text = strOf + Convert.ToString(MintRecordCnt) + "";
                    bnAddNewItem.Enabled = MblnAddPermission;

                    SetEnableDisable();
                    SetReceiptOrIssueLabel();

                }
            }
            catch (Exception ex)
            { objClsLogWriter.WriteLog("Error on GetTradeLicenseDetails() " + this.Name + " " + ex.Message.ToString(), 1); }
        }
        /// <summary>
        /// Load report
        /// </summary>
        private void LoadReport()
        {
            try
            {
                if (txtLicenseNumber.Tag.ToInt32() > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();

                    ObjViewer.intCompanyID = cboCompany.SelectedValue.ToInt32();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = txtLicenseNumber.Tag.ToInt32();
                    ObjViewer.PiFormID = (int)FormID.TradeLicense;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        /// <summary>
        /// Delete Insurance card
        /// </summary>
        private void DeleteInsuranceCard()
        {
            try
            {
                objBLLTradeLicense.objDTOTradeLicense.TradeLicenseID = txtLicenseNumber.Tag.ToInt32();
                objBLLTradeLicense.DeleteTradeLicense();
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 4, out MsMessageBoxIcon);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmTradeLicense.Enabled = true;
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
               
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on DeleteInsuranceCard() " + this.Name + " " + ex.Message.ToString(), 1);
            }
        }
        /// <summary>
        /// Autocomplete with Employeename/Employeecode and CardNumber
        /// </summary>
        /// 
        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsBLLTradeLicense.GetAutoCompleteList(txtSearch.Text.Trim(), CompanyID, "LicenceNumber", "CompanyTradeLicencesDetails");
            this.txtSearch.AutoCompleteCustomSource = clsBLLTradeLicense.ConvertToAutoCompleteCollection(dt);

        }
        #endregion Functions

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }

    }
}
