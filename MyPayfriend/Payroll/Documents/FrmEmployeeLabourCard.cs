﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

///
/// Modified By: Megha
/// Date: 16/08/2013
/// Performed Code Optimization and added Functionalities for: Renew/Issue, Search
///

namespace MyPayfriend
{
    public partial class FrmEmployeeLabourCard : Form
    {
        #region Variable Declaration

        public int PLabourCardId { get; set; }//From Employee View
        public long EmployeeID { get; set; }//From Navigator View

        private int MintRecordCount = 0;
        private int MintCurrentRecordCount = 0;

        private bool MblnAddStatus;
        private bool MblnPrintEmailPermission = false;     //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;       //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission 
        private bool MblnAddUpdatePermission = false;  //To set Add Update Permission
        bool MiOKClose = false;
        private bool mblnSearchStatus = false;       //Search

        //Receipt Or Issue Permissions
        public bool mblnAddIssue = false;
        public bool mblnAddReceipt = false;
        public bool mblnEmail = false;
        public bool mblnDelete = false;
        public bool mblnUpdate = false;
        public bool mblnRenew = false;

        //Error Message display
        string sCommonMessage;
        public ArrayList MaMessage;
        private ArrayList MaStatusMessage;
        private MessageBoxIcon MmessageIcon;
        private string MsMessageCaption = ClsCommonSettings.MessageCaption;

        //Object Instantiation
        private ClsCommonUtility MobjClsCommonUtility;
        private clsBLLEmployeeLabourCard MobjClsBLLEmployeeLabourCard;
        private ClsNotification mObjNotification = null;
        private string strOf = "of ";

        #endregion Variable Declaration

        #region Constructor
        public FrmEmployeeLabourCard()
        {
            InitializeComponent();

            MobjClsCommonUtility = new ClsCommonUtility();
            MobjClsBLLEmployeeLabourCard = new clsBLLEmployeeLabourCard();
              if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }


        }
        #endregion Constructor

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.LabourCard, this);

            ToolStripDropDownBtnMore.Text = "الإجراءات";
            ToolStripDropDownBtnMore.ToolTipText = "الإجراءات";
            btnDocument.Text = "وثائق";
            btnReceipt.Text = "استلام";
            btnIssue.Text = "قضية";
            RenewToolStripMenuItem.Text = "جدد";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            strOf = "من ";
        }
        #region Events

        #region Form-Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmEmployeeLabourCard_Load(object sender, EventArgs e)
        {
            RenewToolStripMenuItem.Checked = false;
            SetAutoCompleteList();
            LoadMesage();
            SetReceiptOrIssuePermissions();
            LoadCombo(0);
            ClearControls();
            AddNew();

            if (PLabourCardId > 0)//Navigator View
            {
                MintCurrentRecordCount = 1;
                BindingNavigatorCountItem.Text = "1";
                BindingNavigatorPositionItem.Text = "1";

                RefernceDisplay();
            }
            else if (EmployeeID > 0)//Employee View
            {
                RecordCount();

                if (MintRecordCount > 0)
                    BindingNavigatorMoveFirstItem_Click(sender, e);
                else
                    btnClear.Enabled = true;
                cboEmployee.Enabled = btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;
                cboEmployee.SelectedValue = EmployeeID;
            }
            btnCancel.Enabled = true;
        }

        /// <summary>
        /// Event Called when a Form is closed while manipulating a record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LabourCard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnOK.Enabled)
            {
                sCommonMessage = new ClsNotification().GetErrorMessage(MaMessage, 17605, out MmessageIcon);

                if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MobjClsBLLEmployeeLabourCard = null;
                    mObjNotification = null;
                    MobjClsCommonUtility = null;

                    e.Cancel = false;//Retains the form
                }
                else
                {
                    e.Cancel = true;//Closes the form
                }
            }
        }

        /// <summary>
        /// Event for defining short-cut keys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmEmployeeLabourCard_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        if (BtnHelp.Enabled) BtnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape://Close form
                        this.Close();
                        break;

                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled) BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled) BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (btnClear.Enabled) btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (BindingNavigatorMovePreviousItem.Enabled) BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());//Move to Previous item
                        break;
                    case Keys.Control | Keys.Right:
                        if (BindingNavigatorMoveNextItem.Enabled) BindingNavigatorMoveNextItem_Click(sender, new EventArgs());//Move to Next item
                        break;
                    case Keys.Control | Keys.Up:
                        if (BindingNavigatorMoveFirstItem.Enabled) BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());//Move to First item
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveLastItem.Enabled) BindingNavigatorMoveLastItem_Click(sender, new EventArgs());//Move to Last item
                        break;
                    case Keys.Control | Keys.P:
                        if (BtnPrint.Enabled) BtnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (BtnEmail.Enabled) BtnEmail_Click(sender, new EventArgs());//Email
                        break;
                    case Keys.Alt | Keys.I:
                        if (btnIssue.Enabled)
                            btnIssue_Click(sender, new EventArgs());  //issue document
                        break;
                    case Keys.Alt | Keys.E:
                        if (btnReceipt.Enabled)
                            btnReceipt_Click(sender, new EventArgs()); //receipt document
                        break;
                    case Keys.Alt | Keys.N:
                        if (RenewToolStripMenuItem.Enabled)
                            RenewToolStripMenuItem_Click(sender, new EventArgs());// renew document
                        break;
                    case Keys.Alt | Keys.D:
                        if (btnDocument.Enabled)
                            btnDocument_Click(sender, new EventArgs()); // issue document
                        break;
                }
            }
            catch (Exception)
            {
            }
        }
        #endregion Form-Events

        #region Save/Cancel/AddNew/Delete
        /// <summary>
        /// Adds a new Card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            btnCancel.Enabled = btnClear.Enabled = true;
            txtSearch.Text = string.Empty;
            ClearControls();
            AddNew();//Adds a new Card

            lblRenewed.Visible = RenewToolStripMenuItem.Checked = false;            
            cboEmployee.Focus();
        }

        /// <summary>
        /// Saves card information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);
        }

        /// <summary>
        /// Saves Card Details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            MiOKClose = false;
            if (SaveLabourCard())
            {
                SetAutoCompleteList();

                btnClear.Enabled = false;
                ToolStripDropDownBtnMore.Enabled = MblnAddUpdatePermission;
                BindingNavigatorAddNewItem.Enabled = (PLabourCardId > 0 ? false : MblnAddPermission);
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                if (MblnAddStatus) BindingNavigatorDeleteItem.Enabled = false;
                else
                {
                    BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (PLabourCardId > 0 ? false : MblnDeletePermission));
                    //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Labour card could not be removed." : "Remove");
                }
                SetReceiptOrIssueLabel();                
            }
        }

        /// <summary>
        /// To save data and close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);
            if (MiOKClose)
                this.Close();
        }

        /// <summary>
        /// Deletes a Record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.blnIsRenewed == true)
            {
                BindingNavigatorDeleteItem.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
            else
            {
                
                    if (txtCardNumber.Tag.ToInt32() > 0)//Deletes if card Exists
                    {
                        int dtDocRequest = clsBLLDocumentMasterNew.GetDocRequestReferenceExists(txtCardNumber.Tag.ToInt32(), (int)DocumentType.Labour_Card);
                        if (dtDocRequest > 0)
                        {
                            string Message = "Cannot Delete as Document Request Exists.";
                            MessageBox.Show(Message, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            if (MessageBox.Show("Do you want to delete the Labour Card ?", MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                DeleteLabourCard(txtCardNumber.Tag.ToInt32());//Delete Function
                                if (PLabourCardId > 0)//Navigator View
                                {
                                    btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = btnClear.Enabled = false;
                                    txtCardNumber.Enabled = txtCardPersonalNo.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = txtRemarks.Enabled = false;
                                }
                            }
                        }
                    }
                    else
                        MessageBox.Show("No details to delete", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            RenewToolStripMenuItem.Checked = false;
        }

        /// <summary>
        /// Method for clearing all the Form Controls
        /// </summary>
        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
            AddNew();
        }

        /// <summary>
        /// Form close after confirmation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close(); //Closes form    
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "LabourCard"; Help.ShowDialog(); }
        }
        #endregion Save/Cancel/AddNew/Delete

        #region Bindingnavigator Record view Events

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            RecordCount();

            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            if (MintCurrentRecordCount >= MintRecordCount)
                return;

            MintCurrentRecordCount = MintCurrentRecordCount + 1;
            DisplayLabourCardInfo();
            if (EmployeeID > 0)//Employee View
            {
                cboEmployee.Enabled = btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
            SetNavigatorEnability();
            btnClear.Enabled = false;
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 11, out MmessageIcon);
            lblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
            Timer1.Enabled = true;
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            RecordCount();
            if (MintCurrentRecordCount >= MintRecordCount && MintRecordCount == 0)
                return;
            MintCurrentRecordCount = MintRecordCount;
            DisplayLabourCardInfo();
            if (EmployeeID > 0)//Employee View
            {
                cboEmployee.Enabled = btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
            SetNavigatorEnability();
            btnClear.Enabled = false;
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 12, out MmessageIcon);
            lblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
            Timer1.Enabled = true;
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            if (BindingNavigatorPositionItem.Text.ToInt32() <= 1)
                return;
            MintCurrentRecordCount = BindingNavigatorPositionItem.Text.ToInt32() - 1;
            DisplayLabourCardInfo();

            if (EmployeeID > 0)//Employee View
            {
                cboEmployee.Enabled = btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
            SetNavigatorEnability();
            btnClear.Enabled = false;
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 10, out MmessageIcon);
            lblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
            Timer1.Enabled = true;
        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            if (!mblnSearchStatus)
                txtSearch.Text = string.Empty;

            btnClear.Enabled = false;
            MintCurrentRecordCount = 1;
            DisplayLabourCardInfo();
            if (EmployeeID > 0)//Employee View
            {
                cboEmployee.Enabled = btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
            SetNavigatorEnability();
            sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 9, out MmessageIcon);
            lblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
            Timer1.Enabled = true;
        }

        #endregion Bindingnavigator Record view Events

        #region Document/Receipt/Issue/Renew

        // Document/Receipt/Issue events
        private void btnDocument_Click(object sender, EventArgs e)
        {
            int iCardID = txtCardNumber.Tag.ToInt32();
            if (iCardID > 0)
            {
                using (FrmScanning objScanning = new FrmScanning())
                {
                    objScanning.MintDocumentTypeid = (int)DocumentType.Labour_Card;
                    objScanning.MlngReferenceID = cboEmployee.SelectedValue.ToInt32();
                    objScanning.MintOperationTypeID = (int)OperationType.Employee;
                    objScanning.MstrReferenceNo = txtCardNumber.Text;
                    objScanning.PblnEditable = true;
                    objScanning.MintVendorID = cboEmployee.SelectedValue.ToInt32();
                    objScanning.MintDocumentID = iCardID;
                    objScanning.ShowDialog();
                }
            }
        }

        private void btnReceipt_Click(object sender, EventArgs e)
        {
            int iDocID = txtCardNumber.Tag.ToInt32();
            new frmDocumentReceiptIssue()
            {
                eOperationType = OperationType.Employee,
                eDocumentType = DocumentType.Labour_Card,
                DocumentID = iDocID,
                DocumentNumber = txtCardNumber.Text,
                EmployeeID = cboEmployee.SelectedValue.ToInt32(),
                ExpiryDate = dtpExpiryDate.Value
            }.ShowDialog();
            SetReceiptOrIssueLabel();
        }

        private void btnIssue_Click(object sender, EventArgs e)
        {
            int iDocID = txtCardNumber.Tag.ToInt32();
            new frmDocumentReceiptIssue()
            {
                eOperationType = OperationType.Employee,
                eDocumentType = DocumentType.Labour_Card,
                DocumentID = iDocID,
                DocumentNumber = txtCardNumber.Text,
                EmployeeID = cboEmployee.SelectedValue.ToInt32(),
                ExpiryDate = dtpExpiryDate.Value
            }.ShowDialog();
            SetReceiptOrIssueLabel();
        }

        /// <summary>
        /// For Renewal of an Existing Card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RenewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (txtCardNumber.Tag.ToInt32() > 0)
            {
                if (MessageBox.Show("Do You want to Renew Labour Card?".Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                else //YES Do you wish to add
                {
                    MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intLabourCardID = txtCardNumber.Tag.ToInt32();
                    MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intEmployeeID = cboEmployee.SelectedValue.ToInt32();

                    if (MobjClsBLLEmployeeLabourCard.UpdateLabourCard() > 0)//Updates Renewed field
                    {
                        lblRenewed.Visible = true;
                        MintCurrentRecordCount = MintCurrentRecordCount - 1;

                        //Disable Input Controls when Renewed
                        cboEmployee.Enabled = txtCardNumber.Enabled = txtCardPersonalNo.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = false;
                        RenewToolStripMenuItem.Enabled = mblnUpdate = mblnDelete = false;
                    }

                    if (MessageBox.Show("Do You want to Add New Labour Card?".Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }
                    AddNew();//Adds new Record with Employee details loaded except the Card Number
                    btnClear.Enabled = false;

                    ShowRenewStatus();//Enable/Disable Renew based on status
                    txtCardNumber.Text = string.Empty;
                    btnSave.Enabled = btnOK.Enabled = true;
                }
            }
        }

        #endregion Document/Receipt/Issue/Renew

        #region Print/Email
        /// <summary>
        /// To Retrieve and fill Email body with appropriate datas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEmail_Click(object sender, EventArgs e)
        {
            int iCardID = txtCardNumber.Tag.ToInt32();
            if (iCardID > 0)
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Employee Labour Card";
                    ObjEmailPopUp.EmailFormType = EmailFormID.LabourCard;
                    ObjEmailPopUp.EmailSource = MobjClsBLLEmployeeLabourCard.GetReportDetails(iCardID);
                    ObjEmailPopUp.ShowDialog();
                }
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        #endregion Print/Email

        #region Search
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == string.Empty)
                mblnSearchStatus = false;
            else
                mblnSearchStatus = true;
            if (txtSearch.Text.Trim() == string.Empty)
                return;

            RecordCount();

            if (MintRecordCount == 0) // No records
            {
                MessageBox.Show(mObjNotification.GetErrorMessage(MaMessage, 40, out MmessageIcon).Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
            }
            else if (MintRecordCount > 0)
            {
                //Displays records matching searchkey
                if (txtSearch.Text.Trim() != "")
                {
                    BindingNavigatorMoveFirstItem_Click(null, null);
                }
                else
                {
                    AddNew();
                    BindingNavigatorMoveFirstItem_Click(sender, e);
                }
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                if (e.KeyCode == Keys.Enter)
                    btnSearch_Click(sender, null);
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            mblnSearchStatus = false;
        }

        #endregion Search

        #region TextChanged Events

        private void txtCardNumber_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtCardPersonalNo_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpIssueDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpExpiryDate_ValueCahenged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void cboEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        #endregion TextChanged Events

        #endregion Events

        #region Functions

        #region Employee Combo manipulations

        /// <summary>
        /// Method to load the Employee Combo with with Employee names
        /// </summary>
        private void LoadCombo(int intType)
        {
            DataTable datCombo = null;

            if (intType == 0 || intType == 1)
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    if (EmployeeID > 0)
                    {
                        if (MblnAddStatus && MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intLabourCardID > 0)
                            datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "(E.WorkStatusID >= 6 or E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        else
                            datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "(E.WorkStatusID >= 6 and E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        cboEmployee.SelectedValue = EmployeeID;
                    }
                    else
                    {
                        if (MblnAddStatus && MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intLabourCardID > 0)
                            datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and (E.WorkStatusID >= 6 or E.EmployeeID =" + MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intEmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        else
                            datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and E.WorkStatusID >= 6 ORDER BY E.EmployeeFullName" });
                    }
                }
                else
                {
                    if (EmployeeID > 0)
                    {
                        if (MblnAddStatus && MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intLabourCardID > 0)
                            datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "(E.WorkStatusID >= 6 or E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        else
                            datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "(E.WorkStatusID >= 6 and E.EmployeeID =" + EmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        cboEmployee.SelectedValue = EmployeeID;
                    }
                    else
                    {
                        if (MblnAddStatus && MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intLabourCardID > 0)
                            datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and (E.WorkStatusID >= 6 or E.EmployeeID =" + MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intEmployeeID + ")  ORDER BY E.EmployeeFullName" });
                        else
                            datCombo = MobjClsCommonUtility.FillCombos(new string[] { "E.EmployeeID," +
                                "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                                "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                                "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") and E.WorkStatusID >= 6 ORDER BY E.EmployeeFullName" });
                    }
                }

                cboEmployee.DisplayMember = "EmployeeName";
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DataSource = datCombo;

                if (txtCardNumber.Tag.ToInt32() == 0 && MblnAddStatus == true)//after renew and adding a new card
                {
                    cboEmployee.Text = null;
                }
                if ((MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID > 0 && EmployeeID > 0) || (MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID > 0 && PLabourCardId > 0))
                    cboEmployee.SelectedValue = MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID;
                else if (MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID > 0 && MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intLabourCardID > 0)
                    cboEmployee.SelectedValue = MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID;
            }
        }

        /// <summary>
        /// Fill employee when navigate buttons
        /// </summary>
        private void FillEmployee()
        {
            DataTable datCombos;
            if (ClsCommonSettings.IsArabicView)
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                    "E.EmployeeFullNameArb+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") AND  E.WorkStatusID >= 6 OR E.EmployeeID = " + MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID });

            }
            else
            {
                datCombos = new ClsCommonUtility().FillCombos(new string[] { "E.EmployeeID," +
                    "E.EmployeeFullName+'-'+E.EmployeeNumber + ' ['+CM.ShortName+ ']' AS EmployeeName", "" +
                    "EmployeeMaster E INNER JOIN CompanyMaster CM ON E.CompanyID = CM.CompanyID", "" +
                    "E.CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ") AND E.WorkStatusID >= 6 OR E.EmployeeID = " + MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID });

            }
            cboEmployee.DisplayMember = "EmployeeName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = datCombos;
            if (MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intLabourCardID == 0)
            {
                cboEmployee.SelectedIndex = -1;
            }
        }

        #endregion Employee Combo manipulations

        /// <summary>
        /// Gets all Messages to be displayed on form load
        /// </summary>
        private void LoadMesage()
        {
            mObjNotification = new ClsNotification();
            MaMessage = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessage = mObjNotification.FillMessageArray((int)FormID.LabourCard, ClsCommonSettings.ProductID);
            MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.LabourCard, ClsCommonSettings.ProductID);
        }

        /// <summary>
        /// Access Permissions
        /// </summary>
        public void SetReceiptOrIssuePermissions()
        {
            clsBLLPermissionSettings objPermission1 = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objPermission1.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentIssue, out mblnEmail, out mblnAddIssue, out mblnUpdate, out mblnDelete);
                objPermission1.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.EmployeeDocumentReceipt, out mblnEmail, out mblnAddReceipt, out mblnUpdate, out mblnDelete);
                objPermission1.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.LabourCard, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                objPermission1.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentRenew, out mblnEmail, out mblnRenew, out mblnUpdate, out mblnDelete);
            }
            else
            {
                mblnAddIssue = mblnAddReceipt = mblnRenew = true;
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            }
            if (MblnAddPermission || MblnUpdatePermission)
                MblnAddUpdatePermission = true;
            else
                MblnAddUpdatePermission = false;

            BtnEmail.Enabled = BtnPrint.Enabled = MblnPrintEmailPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;

            btnOK.Enabled = btnSave.Enabled = btnCancel.Enabled = MblnAddUpdatePermission;
        }

        /// <summary>
        /// Method invoked for clearing controls
        /// </summary>
        private void ClearControls()
        {
            cboEmployee.SelectedIndex = -1;
            cboEmployee.Text = "";
            txtCardNumber.Text = string.Empty;
            txtCardPersonalNo.Text = string.Empty;
            txtSearch.Text = string.Empty;//Clear Search Field
            MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intLabourCardID = 0;
            dtpIssueDate.Value = System.DateTime.Now.Date; ;
            dtpExpiryDate.Value = System.DateTime.Now.AddDays(1); ;
            lblRenewed.Visible = mblnSearchStatus = false;
            txtRemarks.Text = string.Empty;
            
        }

        /// <summary>
        /// Gets total record count
        /// </summary>
        private void RecordCount()
        {
            MintRecordCount = MobjClsBLLEmployeeLabourCard.GetRecordCount(EmployeeID, PLabourCardId, txtSearch.Text.Trim());
        }

        /// <summary>
        /// To add A new Labour Card
        /// </summary>
        private void AddNew()
        {
            ToolStripDropDownBtnMore.Enabled = MiOKClose = false;
            MblnAddStatus = true;
            txtCardNumber.Tag = 0;

            sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 655, out MmessageIcon);
            lblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);

            RecordCount();
            LoadCombo(0);
            BindingNavigatorCountItem.Text = strOf + (MintRecordCount + 1).ToString();
            BindingNavigatorPositionItem.Text = (MintRecordCount + 1).ToString();
            MintCurrentRecordCount = BindingNavigatorPositionItem.Text.ToInt32();

            cboEmployee.Enabled = txtCardNumber.Enabled = txtCardPersonalNo.Enabled = dtpIssueDate.Enabled = dtpExpiryDate.Enabled = true;

            SetNavigatorEnability();
            SetEnability();
            ShowRenewStatus();

            if (EmployeeID > 0)//Employee View
            {
                MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intEmployeeID = EmployeeID.ToInt32();
                LoadCombo(1);
                cboEmployee.SelectedValue = EmployeeID;
                cboEmployee.Enabled = btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;//btnClear.Enabled = 
            }
            if (MintRecordCount == 0)
                btnClear.Enabled = true;
            BindingNavigatorSaveItem.Enabled = mblnSearchStatus = false;
            btnCancel.Enabled = true;
        }

        /// <summary>
        /// method is called for retrieving and displaying Card details from Navigator view
        /// </summary>
        private void RefernceDisplay()
        {
            MobjClsBLLEmployeeLabourCard.clsDTOEmployeeLabourCard.intLabourCardID = PLabourCardId;
            int RowNum = 0;

            RowNum = MobjClsBLLEmployeeLabourCard.GetRowNumber();//Card Rownumber
            if (RowNum > 0)
            {
                MintCurrentRecordCount = 1;
                DisplayLabourCardInfo();//Displays cards
                EnableDisableReferenceDisplay();//Enable/Disable Buttons and BindingNavigator Controls
            }
        }

        private void EnableDisableReferenceDisplay()
        {
            BindingNavigatorAddNewItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = btnClear.Enabled = txtSearch.Enabled = btnSearch.Enabled = false;
            BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            BindingNavigatorSaveItem.Enabled = BindingNavigatorDeleteItem.Enabled = btnSave.Enabled = btnOK.Enabled = false;
            lblStatus.Text = "Labour Card for " + cboEmployee.Text;
        }

        /// <summary>
        /// Display Card Information
        /// </summary>
        private void DisplayLabourCardInfo()
        {
            RecordCount();//Total Record Count
            DisplayInfo();//retrieve Card details
            MblnAddStatus = false;
            ErrorProviderCard.Clear();

            BindingNavigatorPositionItem.Text = MintCurrentRecordCount.ToStringCustom();
            BindingNavigatorCountItem.Text = strOf + MintRecordCount.ToStringCustom();

            SetEnability();//Sets Button Enability
            if (Convert.ToBoolean(MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.blnIsRenewed))
            {
                BindingNavigatorDeleteItem.Enabled = MblnAddStatus = false;
            }
            BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = false;
            SetReceiptOrIssueLabel();
        }

        /// <summary>
        /// Retrieves Card Info and loads onto Form Controls
        /// </summary>
        private void DisplayInfo()
        {
            MblnAddStatus = RenewToolStripMenuItem.Checked = false;
            ToolStripDropDownBtnMore.Enabled = true;

            if (MintRecordCount > 0)
            {
                MobjClsBLLEmployeeLabourCard = new clsBLLEmployeeLabourCard();
                MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard = MobjClsBLLEmployeeLabourCard.DisplayEmployeeLabourCard(MintCurrentRecordCount, EmployeeID, PLabourCardId, txtSearch.Text.Trim());

                if (MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard != null)
                {
                    FillEmployee();//Load Employee Combo

                    if (MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID.ToInt32() == 0)
                    {
                        txtCardNumber.Tag = 0;
                    }
                    //Bind values onto controls
                    txtCardNumber.Tag = MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intLabourCardID;
                    cboEmployee.SelectedValue = MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID;
                    txtCardNumber.Text = MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.strCardNumber.ToStringCustom().Trim();
                    txtCardPersonalNo.Text = MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.strCardPersonalNumber.ToStringCustom().Trim();
                    dtpIssueDate.Value = MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.dtIssueDate.ToString("dd MMM yyyy").ToDateTime();
                    dtpExpiryDate.Value = MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.dtExpiryDate.ToString("dd MMM yyyy").ToDateTime();
                    txtRemarks.Text = MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.strRemarks.Trim();

                    if (Convert.ToBoolean(MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.blnIsRenewed))//Enable/Disable Renew Status
                    {
                        lblRenewed.Visible = true;
                        cboEmployee.Enabled = txtCardNumber.Enabled = txtCardPersonalNo.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = false;
                        RenewToolStripMenuItem.Enabled = false;
                    }
                    else
                    {
                        lblRenewed.Visible = false;
                        cboEmployee.Enabled = txtCardNumber.Enabled = txtCardPersonalNo.Enabled = dtpExpiryDate.Enabled = dtpIssueDate.Enabled = true;
                        RenewToolStripMenuItem.Enabled = true;
                    }
                }
            }
            else
            {
                txtCardNumber.Tag = 0;
            }
        }

        /// <summary>
        /// Sets Control enability after a new record is added
        /// </summary>
        private void SetEnability()
        {
            btnSave.Enabled = btnOK.Enabled = BindingNavigatorSaveItem.Enabled = false;
            BtnPrint.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            BtnEmail.Enabled = (MblnAddStatus == true ? false : MblnPrintEmailPermission);
            ToolStripDropDownBtnMore.Enabled = (MblnAddStatus == true ? false : true);
            RenewToolStripMenuItem.Enabled = (clsPassportBLL.IsEmployeeInService(cboEmployee.SelectedValue.ToInt64()) ? (lblRenewed.Visible ? false : mblnRenew) : false);

            BindingNavigatorAddNewItem.Enabled = (MblnAddStatus == true ? false : (PLabourCardId > 0 ? false : MblnAddPermission));

            if (MblnAddStatus) BindingNavigatorDeleteItem.Enabled = false;
            else
            {
                BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : (PLabourCardId > 0 ? false : MblnDeletePermission));
                //BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Renewed Labour card could not be removed." : "Remove");
            }
            btnClear.Enabled = (MblnAddStatus == true ? true : false);
        }

        /// <summary>
        /// Sets Enability of Binding Navigators
        /// </summary>
        private void SetNavigatorEnability()
        {
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = true;
            if (PLabourCardId > 0)//Navigator view
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
                btnClear.Enabled = false;
            }
            if (MintRecordCount == 0)
            {
                BindingNavigatorMoveFirstItem.Enabled = false;
                BindingNavigatorMovePreviousItem.Enabled = false;
                BindingNavigatorMoveNextItem.Enabled = false;
                BindingNavigatorMoveLastItem.Enabled = false;
            }

            if (MintCurrentRecordCount >= MintRecordCount)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (MintCurrentRecordCount == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }

        /// <summary>
        /// Save/Update
        /// </summary>
        /// <returns></returns>
        private bool SaveLabourCard()
        {
            if (FormValidation())//Checks for Validation of Controls
            {
                if (MblnAddStatus)
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 1, out MmessageIcon);//Save msg confirmation
                else
                    sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 3, out MmessageIcon);//Update msg confirmation

                if (MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = true;
                    return false;
                }
                else
                    BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOK.Enabled = false;

                //Retrieve values from controls
                FillParameters();

                Int32 intLabourCardID = MobjClsBLLEmployeeLabourCard.SaveLabourCard();//Save/Update
                if (intLabourCardID > 0)
                {
                    MiOKClose = true;

                    if (txtCardNumber.Tag.ToInt32() > 0)
                    {
                        sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 21, out MmessageIcon);//Update msg      
                    }
                    else
                    {
                        sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 2, out MmessageIcon);//Save msg                             
                    }
                    lblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                    MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    txtCardNumber.Tag = intLabourCardID;
                    clsAlerts objclsAlerts = new clsAlerts();
                    if (MblnAddStatus)
                    {
                        objclsAlerts.AlertMessage((int)DocumentType.Labour_Card, "LabourCard", "LabourCardNumber", intLabourCardID, txtCardNumber.Text, dtpExpiryDate.Value, "Emp", cboEmployee.SelectedValue.ToInt32(), cboEmployee.Text.Trim(), false,0);
                        MblnAddStatus = false;
                    }
                    else
                    {
                        if (!RenewToolStripMenuItem.Checked)
                        {
                            objclsAlerts.AlertMessage((int)DocumentType.Labour_Card, "LabourCard", "LabourCardNumber", intLabourCardID, txtCardNumber.Text, dtpExpiryDate.Value, "Emp", cboEmployee.SelectedValue.ToInt32(), cboEmployee.Text.Trim(), false,0);
                        }
                    }
                }
                else
                {
                    if (txtCardNumber.Tag.ToInt32() > 0)
                        MessageBox.Show("Cannot update", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Cannot save", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Timer1.Enabled = true;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Form Control Validations
        /// </summary>
        /// <returns></returns>
        public bool FormValidation()
        {
            ErrorProviderCard.Clear();
            bool bReturnValue = true;
            Control elcControl = new Control();

            if (cboEmployee.SelectedIndex == -1)//Employee Selection
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 9124, out MmessageIcon);
                elcControl = cboEmployee;
                bReturnValue = false;
            }
            else if (txtCardNumber.Text.Trim() == string.Empty)//Card Number
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 17601, out MmessageIcon);
                elcControl = txtCardNumber;
                bReturnValue = false;
            }
            else if (dtpIssueDate.Value.Date > ClsCommonSettings.GetServerDate())//Issue Date>Current Date
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 17602, out MmessageIcon);
                elcControl = dtpIssueDate;
                bReturnValue = false;
            }
            else if (dtpIssueDate.Value.Date >= dtpExpiryDate.Value.Date)//Issue Date>expiry date
            {
                sCommonMessage = mObjNotification.GetErrorMessage(MaMessage, 17603, out MmessageIcon);
                elcControl = dtpIssueDate;
                bReturnValue = false;
            }

            if (bReturnValue == false)//Display Message
            {
                MessageBox.Show(sCommonMessage.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                lblStatus.Text = sCommonMessage.Remove(0, sCommonMessage.IndexOf("#") + 1);
                if (elcControl != null)//Focus on Control
                {
                    ErrorProviderCard.SetError(elcControl, sCommonMessage);
                    elcControl.Focus();
                }
                Timer1.Enabled = true;
            }
            return bReturnValue;
        }

        /// <summary>
        /// Gets Input data onto class object
        /// </summary>
        private void FillParameters()
        {
            MobjClsBLLEmployeeLabourCard = new clsBLLEmployeeLabourCard();
            if (txtCardNumber.Tag.ToInt32() > 0)
            {
                MblnAddStatus = false;
            }
            if (MblnAddStatus)//CardID = 0 if new card
            {
                MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intLabourCardID = 0;
                RenewToolStripMenuItem.Checked = false;
            }
            else
            {
                MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intLabourCardID = txtCardNumber.Tag.ToInt32();
            }
            MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.intEmployeeID = cboEmployee.SelectedValue.ToInt32();
            MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.strCardNumber = txtCardNumber.Text.Trim();
            MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.strCardPersonalNumber = txtCardPersonalNo.Text.Trim();
            MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.dtIssueDate = dtpIssueDate.Value;
            MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.dtExpiryDate = dtpExpiryDate.Value;
            MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.strRemarks = txtRemarks.Text.Trim();
            MobjClsBLLEmployeeLabourCard.PobjClsDTOEmployeeLabourCard.blnIsRenewed = lblRenewed.Visible;
        }

        /// <summary>
        /// Deletes a Card Entry
        /// </summary>
        /// <param name="LabourCardID"></param>
        private void DeleteLabourCard(int LabourCardID)
        {
            if (MobjClsBLLEmployeeLabourCard.DeleteLabourCard(LabourCardID))
            {
                MessageBox.Show("Deleted successfully", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearControls();
                AddNew();//Add new Card after deleting
                SetAutoCompleteList();
            }
            else
                MessageBox.Show("Cannot delete information", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Gets the data to be printed
        /// </summary>
        private void LoadReport()
        {
            try
            {
                int iCardID = txtCardNumber.Tag.ToInt32();
                if (iCardID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    DataTable datEmps = new ClsCommonUtility().FillCombos(new string[] { "CompanyID, FirstName", "EmployeeMaster", "  EmployeeID = " + cboEmployee.SelectedValue.ToInt32() });
                    if (datEmps.Rows.Count > 0)
                    {
                        ObjViewer.intCompanyID = datEmps.Rows[0]["CompanyID"].ToInt32();
                    }
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = iCardID;
                    ObjViewer.PiFormID = (int)FormID.LabourCard;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {

            }
        }

        /// <summary>
        /// Sets enability of Issue and Receipt button controls
        /// </summary>
        private void SetReceiptOrIssueLabel()
        {
            RenewToolStripMenuItem.Enabled = (clsPassportBLL.IsEmployeeInService(cboEmployee.SelectedValue.ToInt64()) ? (lblRenewed.Visible ? false : mblnRenew) : false);
            
            btnIssue.Enabled = btnReceipt.Enabled = false;
            bool IsReceipt = clsBLLDocumentMasterNew.IsReceipted((int)OperationType.Employee, (int)DocumentType.Labour_Card, txtCardNumber.Tag.ToInt32());

            if (IsReceipt)
                btnIssue.Enabled = mblnAddIssue;
            else
                btnReceipt.Enabled = mblnAddReceipt;
        }

        /// <summary>
        /// Sets enability of controls after renew
        /// </summary>
        private void ShowRenewStatus()
        {
            btnSave.Enabled = btnOK.Enabled = false;
            BtnPrint.Enabled = (MblnAddStatus == true ? false : true);
            BtnEmail.Enabled = (MblnAddStatus == true ? false : true);
            ToolStripDropDownBtnMore.Enabled = (MblnAddStatus == true ? false : true);
            BindingNavigatorAddNewItem.Enabled = (MblnAddStatus == true ? false : true);
            if (MblnAddStatus)
            {
                lblRenewed.Visible = false;
                BindingNavigatorDeleteItem.Enabled = false;
                RenewToolStripMenuItem.Enabled = true;
            }
            else
            {
                BindingNavigatorDeleteItem.Enabled = (lblRenewed.Visible ? false : true);
                BindingNavigatorDeleteItem.ToolTipText = (lblRenewed.Visible ? "Sorry Unable to Delete as the Card has been Renewed" : "Delete");
            }
            btnCancel.Enabled = (MblnAddStatus == true ? true : false);
        }

        private void ChangeStatus()
        {
            if (MblnAddStatus) // add mode
            {
                btnOK.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else  //edit mode
            {
                btnOK.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            ErrorProviderCard.Clear();
        }

        private void SetAutoCompleteList()
        {
            clsBLLEmployeeLabourCard MobjClsBLLEmployeeLabourCard = new clsBLLEmployeeLabourCard();

            //Loads all Employee Name, Card Number and Employee Number for Labour Card
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsBLLInsuranceCard.GetAutoCompleteList(txtSearch.Text.Trim(), EmployeeID, "CardNumber", "EmployeeLabourCard");
            this.txtSearch.AutoCompleteCustomSource = MobjClsBLLEmployeeLabourCard.ConvertToAutoCompleteCollection(dt);
        }

        #endregion Functions       

    }
}
