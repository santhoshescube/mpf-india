﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace MyPayfriend
{
    public partial class FrmSalaryTemplate : Form
    {
        #region Declartions
        public int PintSalaryTempAddDedID = 0;   // From Refernce Form 
        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission
        public int PintSalaryTempId;
        private string MstrCommonMessage;
        private bool MblnIsEditMode = false;  //  To Find Whether Is add mode or Edit Mode
        private int MintRecordCnt = 0;     //Total Record Count
        private int MintCurrentRecCnt = 0; // Current Record Count
        private bool MblnChangeStatus = false;
        private bool MblbtnOk = false;
        private int SaveRange = 0;
        private clsBLLSalaryTemplate MobjclsBLLSalaryTemplate;
        private clsMessage ObjUserMessage = null;
        string strBindingOf = "Of ";
        #endregion Declartions
        #region Constructors
        public FrmSalaryTemplate()
        {
            InitializeComponent();
            MobjclsBLLSalaryTemplate = new clsBLLSalaryTemplate();
        }
        #endregion Constructors

        private clsMessage UserMessage  // For Notification Message
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalaryTemplate);
                return this.ObjUserMessage;
            }
        }
        private void SetPermissions()  // Function for setting permissions Add/Update/Delete/Email-Print
        {

            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SalaryTemplate, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
            {

                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            }
        }

        private void FrmSalaryTemplate_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            SetPermissions();
            if (PintSalaryTempId > 0)  // Calling SalTemplate From Another Froms 
            {
                GetRecordCount();
                RefernceDisplay();
            }
            else
            {
                AddNewSalaryTemplate();
            }
        }
        #region LoadCombos
        private bool LoadCombos(int intType) // For Loading the Combos 
        {
            bool blnRetvalue = false;
            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0 || intType == 1)//Calculation type
                {
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "AdditionDeductionID,AdditionDeduction", "PayAdditionDeductionReference", " IsAddition=1 And (AdditionDeductionID=1 or IsPredefined=0)  " });
                    else
                        datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "AdditionDeductionID,AdditionDeduction", "PayAdditionDeductionReference", " IsAddition=1 And (AdditionDeductionID=1 or IsPredefined=0) " });
                    colParticulars.ValueMember = "AdditionDeductionID";
                    colParticulars.DisplayMember = "AdditionDeduction";
                    colParticulars.DataSource = datCombos;
                    DgvSalaryTemp.Rows[0].Cells["colParticulars"].Value = 1;
                    //DgvSalaryTemp.Rows[0].Cells["colParticulars"].ReadOnly = true;
                    blnRetvalue = true;
                    blnRetvalue = true;
                    datCombos = null;
                }
                //if (intType == 0 || intType == 2)//
                //{

                //    datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "AdditionDeductionPolicyId,PolicyName", "PayAdditionDeductionPolicyMaster", "" });
                //    colDedPolicy.ValueMember = "AdditionDeductionPolicyId";
                //    colDedPolicy.DisplayMember = "PolicyName";
                //    if (datCombos != null)
                //    {
                //        DataRow dr = datCombos.NewRow();
                //        dr["AdditionDeductionPolicyId"] = -1;
                //        dr["PolicyName"] = "None";
                //        datCombos.Rows.InsertAt(dr, 0);
                //    }
                //    colDedPolicy.DataSource = datCombos;
                //    datCombos = null;
                //    blnRetvalue = true;
                //}

                if (intType == 0 || intType == 3)//Calculation type
                {
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "AdditionDeductionID,AdditionDeduction", "PayAdditionDeductionReference where  IsAddition=0 And  IsPredefined=0 ", " " });
                    else
                        datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "AdditionDeductionID,AdditionDeduction", "PayAdditionDeductionReference where  IsAddition=0 And  IsPredefined=0 ", " " });

                    colDedParticulars.ValueMember = "AdditionDeductionID";
                    colDedParticulars.DisplayMember = "AdditionDeduction";
                    colDedParticulars.DataSource = datCombos;
                    //DgvSalaryTemp.Rows[0].Cells["colParticulars"].Value = 1;
                    //DgvSalaryTemp.Rows[0].Cells["colParticulars"].ReadOnly = true;
                    blnRetvalue = true;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 4)//
                {
                    cboAbsentPolicy.Text = "";
                    datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "AbsentPolicyID,AbsentPolicy", "PayAbsentPolicyMaster", "" });
                    cboAbsentPolicy.ValueMember = "AbsentPolicyID";
                    cboAbsentPolicy.DisplayMember = "AbsentPolicy";
                    cboAbsentPolicy.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 5)
                {
                    datCombos = null;
                    datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "SetPolicyID,DescriptionPolicy", "PaySettlementPolicy", "" });
                    cboSettlement.DataSource = null;
                    cboSettlement.ValueMember = "SetPolicyID";
                    cboSettlement.DisplayMember = "DescriptionPolicy";
                    cboSettlement.DataSource = datCombos;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 6)
                {
                    cboHolidayPolicy.Text = "";
                    datCombos = null;
                    datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "HolidayPolicyID,HolidayPolicy", "PayHolidayPolicyMaster", "" });
                    cboHolidayPolicy.DataSource = null;
                    cboHolidayPolicy.ValueMember = "HolidayPolicyID";
                    cboHolidayPolicy.DisplayMember = "HolidayPolicy";
                    cboHolidayPolicy.DataSource = datCombos;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 7)
                {
                    cboEncashPolicy.Text = "";
                    datCombos = null;
                    datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "EncashPolicyID,EncashPolicy", "PayEncashPolicyMaster", "" });
                    cboEncashPolicy.DataSource = null;
                    cboEncashPolicy.ValueMember = "EncashPolicyID";
                    cboEncashPolicy.DisplayMember = "EncashPolicy";
                    cboEncashPolicy.DataSource = datCombos;
                    blnRetvalue = true;
                }
                if (intType == 0 || intType == 8)//
                {
                    cboOvertimePolicy.Text = "";
                    datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "OTPolicyID,OTPolicy", "PayOTPolicyMaster", "" });
                    cboOvertimePolicy.ValueMember = "OTPolicyID";
                    cboOvertimePolicy.DisplayMember = "OTPolicy";
                    cboOvertimePolicy.DataSource = datCombos;
                    datCombos = null;
                    blnRetvalue = true;
                }




            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }
        #endregion LoadCombos

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            //ClearAllControls();
            bool blnRetValue = false;
            SaveSalaryTemplate();
            DisplaySalaryTemplateInfo();
            DgvSalaryTemp.Rows[0].Cells["colParticulars"].Value = 1;


        }
        private void GetRecordCount()   // TO Get Record Count
        {
            MintRecordCnt = 0;
            MintRecordCnt = MobjclsBLLSalaryTemplate.GetRecordCount();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                MintRecordCnt = 0;
            }
            else if (MintRecordCnt > 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + MintRecordCnt;

            }
        }
        public bool SaveSalaryTemplate()
        {
            bool blnSave = false;
            if (FormValidation())
            {
                int intMessageCode = 0;
                if (TxtName.Tag.ToInt32() > 0)
                    intMessageCode = 3;
                else
                    intMessageCode = 1;

                if (UserMessage.ShowMessage(intMessageCode))
                {
                    GetSalaryTemp();
                    GetSalaryTempDetails();
                    GetDeductionDetails();
                    if (MobjclsBLLSalaryTemplate.SavePolicy())
                    {
                        UserMessage.ShowMessage(2, null, null, 2);
                    }
                    PintSalaryTempAddDedID = MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.SalaryTempId;
                    blnSave = true;
                }
            }
            return blnSave;
        }
        public void GetSalaryTemp()
        {
            MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.SalaryTempId = TxtName.Tag.ToInt32();
            MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.SalaryTempName = TxtName.Text;
            MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.AbsentPolicyID=cboAbsentPolicy.SelectedValue.ToInt32();
            MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.EncashPolicyID=cboEncashPolicy.SelectedValue.ToInt32();
            MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.HolidayPolicyID=cboHolidayPolicy.SelectedValue.ToInt32();
            MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.OTPolicyID=cboOvertimePolicy.SelectedValue.ToInt32();
            MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.SetPolicyID = cboSettlement.SelectedValue.ToInt32();


        }
        public void GetDeductionDetails()
        {
            MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.lstclsDTOSalaryTemplateDeduction = new List<clsDTOSalaryTemplateDeduction>();
            if (TxtName.Text != "")
                if (DgvDedSalTemplate.Rows.Count > 0)
                    for (int iIndex = 0; iIndex < DgvDedSalTemplate.Rows.Count - 1; iIndex++)
                    {
                        clsDTOSalaryTemplateDeduction objclsDTOSalaryTemplateDeduction = new clsDTOSalaryTemplateDeduction();
                        objclsDTOSalaryTemplateDeduction.Amount = DgvDedSalTemplate.Rows[iIndex].Cells["colAmount"].Value.ToDecimal();
                        objclsDTOSalaryTemplateDeduction.PolicyID = DgvDedSalTemplate.Rows[iIndex].Cells["colDedPolicy"].Tag.ToInt32();
                        objclsDTOSalaryTemplateDeduction.SalaryTempParticularsDedID = DgvDedSalTemplate.Rows[iIndex].Cells["colDedParticulars"].Value.ToInt32();
                        MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.lstclsDTOSalaryTemplateDeduction.Add(objclsDTOSalaryTemplateDeduction);
                    }
        }


        public void GetSalaryTempDetails()
        {
            MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.lstclsDTOSalaryTemplateDetails = new List<clsDTOSalaryTemplateDetails>();
            if (TxtName.Text != "")
                if (DgvSalaryTemp.Rows.Count > 0)
                    for (int iIndex = 0; iIndex < DgvSalaryTemp.Rows.Count - 1; iIndex++)
                    {
                        clsDTOSalaryTemplateDetails objclsDTOSalaryTemplateDetails = new clsDTOSalaryTemplateDetails();
                        objclsDTOSalaryTemplateDetails.SalaryTempParticularsID = DgvSalaryTemp.Rows[iIndex].Cells["colParticulars"].Value.ToInt32();
                        objclsDTOSalaryTemplateDetails.SalaryTempPercentage = DgvSalaryTemp.Rows[iIndex].Cells["colPercentage"].Value.ToDecimal();
                        MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.lstclsDTOSalaryTemplateDetails.Add(objclsDTOSalaryTemplateDetails);
                    }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

            AddNewSalaryTemplate();
        }
        private void AddNewSalaryTemplate()  // Add Mode
        {

            MblnChangeStatus = false;
            MblnIsEditMode = false;
            ClearAllControls();  //Clear all controls in the form
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
            BindingNavigatorAddNewItem.Enabled = false;
            BindingNavigatorDeleteItem.Enabled = false;
            BtnPrint.Enabled = false;
            //btnEmail.Enabled = false;
            BtnOk.Enabled = true;
            BtnSave.Enabled = true;
            BindingNavigatorSaveItem.Enabled = true;
            BindingNavigatorMoveFirstItem.Enabled = true;
            BindingNavigatorMoveLastItem.Enabled = false;
            BindingNavigatorMovePreviousItem.Enabled = true;
            BindingNavigatorMoveNextItem.Enabled = false;
        }
        private void ClearAllControls()   // Funtion to clear All Controls
        {
            //DataTable dTNull = new DataTable();
            TxtName.Text = "";
            TxtName.Tag = 0;
            DgvDedSalTemplate.Rows.Clear();
            DgvSalaryTemp.Rows.Clear();
            tabAdditionDeduction.SelectedIndex = 0;
            LblTotalPercentage.Text = "0";
            DgvSalaryTemp.Rows[0].Cells["colParticulars"].Value = 1;
            cboOvertimePolicy.SelectedValue=-1;
            cboAbsentPolicy.SelectedValue=-1;
            cboHolidayPolicy.SelectedValue=-1; 
            cboEncashPolicy.SelectedValue=-1;
            cboSettlement.SelectedValue = -1;
        }
        private void RefernceDisplay() // To Display Details In Edit Mode
        {

            int intRowNum = 0;
            intRowNum = MobjclsBLLSalaryTemplate.GetRowNumber(PintSalaryTempId);
            if (intRowNum > 0)
            {
                MintCurrentRecCnt = intRowNum;
                DisplaySalaryTemplateInfo();
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            BindingNavigatorSaveItem_Click(sender, e);
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (SaveSalaryTemplate())
            {
                BindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
                this.Close();
            }

        }
        private bool FormValidation()
        {
            decimal percent = 0;
            Control control = null;
            bool blnReturnValue = true;
            DgvSalaryTemp.EndEdit(); 
            if (TxtName.Text.Trim().Length == 0)     // Please enter Policy Name
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(21644);
                control = TxtName;
                blnReturnValue = false;
            }
           
            //else if (DgvSalaryTemp.Rows.Count > 0)
            //{
            //    int Check = 0;
            //    for (int Count = 0; Count < DgvSalaryTemp.Rows.Count; Count++)
            //    {
            //        if (DgvSalaryTemp.Rows[Count].Cells["colParticulars"].Value.ToInt32() == 1)
            //        {
            //            Check = Check + 1;
            //        }

            //    }
                //if (Check <= 0)
                //{
                //    MstrCommonMessage = UserMessage.GetMessageByCode(21649);
                //    control = txtGrossAmount;
                //    blnReturnValue = false;
                //}
                //if (Check > 1)
                //{
                //    MstrCommonMessage = UserMessage.GetMessageByCode(21650);
                //    control = txtGrossAmount;
                //    blnReturnValue = false;
                //}


            //}
            if (DgvSalaryTemp.Rows.Count > 0)
            {
                for (int Count = 0; Count < DgvSalaryTemp.Rows.Count; Count++)
                {
                    //decimal percent=0;
                    percent += DgvSalaryTemp.Rows[Count].Cells["colPercentage"].Value.ToDecimal();
                }
                if (percent > 100)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(21647);
                    control = DgvSalaryTemp;
                    blnReturnValue = false;
                }
                else if (percent < 100)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(21646);
                    control = DgvSalaryTemp;
                    blnReturnValue = false;
                }
            }
            if (blnReturnValue)
            {
                if (MobjclsBLLSalaryTemplate.CheckDuplicate(TxtName.Tag.ToInt32(), TxtName.Text.Trim()))//CheckDuplicate()
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(21648); //Duplicate policy name.Please change policy name
                    control = TxtName;
                    blnReturnValue = false;
                }
            }
            if (blnReturnValue == false)
            {
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (control != null)
                {
                    control.Focus();
                }

            }
            return blnReturnValue;
        }
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
        }

        public void FillSalaryTemplateInfoDetails()
        {

            try
            {
                DgvSalaryTemp.Rows.Clear();

                DataSet dETAILS = new DataSet();
                dETAILS = MobjclsBLLSalaryTemplate.FillDetails(MintCurrentRecCnt);

                //dETAILS = MobjclsBLLAdditionDeductionPercentage.FillDetails(MobjclsBLLAdditionDeductionPercentage.DTOAdditionDeductionPercentage.PercentagePolicyID);
                if (dETAILS.Tables[0].Rows.Count > 0)
                {
                    TxtName.Text = dETAILS.Tables[0].Rows[0]["SalaryTempName"].ToString();
                    TxtName.Tag = dETAILS.Tables[0].Rows[0]["SalaryTempId"].ToInt32();
                    cboHolidayPolicy.SelectedValue = dETAILS.Tables[0].Rows[0]["HolidayPolicyID"].ToString();
                    cboOvertimePolicy.SelectedValue = dETAILS.Tables[0].Rows[0]["OTPolicyID"].ToString();
                    cboSettlement.SelectedValue = dETAILS.Tables[0].Rows[0]["SetPolicyID"].ToString();
                    cboEncashPolicy.SelectedValue = dETAILS.Tables[0].Rows[0]["EncashPolicyID"].ToString();
                    cboAbsentPolicy.SelectedValue = dETAILS.Tables[0].Rows[0]["AbsentPolicyID"].ToString();
                         
                }
                if (dETAILS.Tables[1].Rows.Count > 0)
                {
                    for (int iCounter = 0; iCounter < dETAILS.Tables[1].Rows.Count; iCounter++)
                    {
                        DgvSalaryTemp.RowCount = DgvSalaryTemp.RowCount + 1;
                        DgvSalaryTemp.Rows[iCounter].Cells[0].Value = dETAILS.Tables[1].Rows[iCounter]["SalaryTempParticularsID"].ToInt32();
                        DgvSalaryTemp.Rows[iCounter].Cells[1].Value = dETAILS.Tables[1].Rows[iCounter]["SalaryTempPercentage"].ToString();
                    }
                }
                DgvDedSalTemplate.RowCount = 1;
                if (dETAILS.Tables[2].Rows.Count > 0)
                {

                    for (int iCounter = 0; iCounter < dETAILS.Tables[2].Rows.Count; iCounter++)
                    {
                        DgvDedSalTemplate.RowCount = DgvDedSalTemplate.RowCount + 1;
                        DgvDedSalTemplate.Rows[iCounter].Cells[0].Value = dETAILS.Tables[2].Rows[iCounter]["SalaryTempParticularsID"].ToInt32();
                        DgvDedSalTemplate.Rows[iCounter].Cells[1].Value = dETAILS.Tables[2].Rows[iCounter]["SalaryTempPercentage"].ToString();
                        DgvDedSalTemplate.Rows[iCounter].Cells[2].Tag = dETAILS.Tables[2].Rows[iCounter]["AdditionDeductionPolicyID"].ToInt32();
                        FillDeductionPolicy(DgvDedSalTemplate.Rows[iCounter].Cells[0].Value.ToInt32() , iCounter);
                        if (DgvDedSalTemplate.Rows[iCounter].Cells[2].Tag.ToInt32() > 0)
                        {
                            DgvDedSalTemplate.Rows[iCounter].Cells[2].Value = dETAILS.Tables[2].Rows[iCounter]["AdditionDeductionPolicyID"].ToInt32();
                            DgvDedSalTemplate.Rows[iCounter].Cells[2].Value = DgvDedSalTemplate.Rows[iCounter].Cells[2].FormattedValue;
                        }
                        else
                        {
                            DgvDedSalTemplate.Rows[iCounter].Cells[2].Value = -1;
                            DgvDedSalTemplate.Rows[iCounter].Cells[2].Tag = -1;
                            DgvDedSalTemplate.Rows[iCounter].Cells[2].Value = DgvDedSalTemplate.Rows[iCounter].Cells[2].FormattedValue;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }


        private void DisplaySalaryTemplateInfo() //To Display Detail Info
        {
            FillSalaryTemplateInfoDetails();
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BtnPrint.Enabled = MblnPrintEmailPermission;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            MblnIsEditMode = true;
            SetBindingNavigatorButtons();
            MblnChangeStatus = false;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            BtnOk.Enabled = MblnChangeStatus;
            BtnSave.Enabled = MblnChangeStatus;

        }
        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = 1;
                DisplaySalaryTemplateInfo();

            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                    DisplaySalaryTemplateInfo();
                }
                else
                {

                    MblnAddPermission = true;
                    DisplaySalaryTemplateInfo();

                }

            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            MintCurrentRecCnt = MintRecordCnt;
            DisplaySalaryTemplateInfo();
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }
                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplaySalaryTemplateInfo();

                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void DgvSalaryTemp_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            decimal perc = 0;
            if (DgvSalaryTemp.Rows.Count > 0)
                for (int iCOUNT = 0; iCOUNT < DgvSalaryTemp.Rows.Count; iCOUNT++)
                {
                    if (DgvSalaryTemp.Rows[iCOUNT].Cells[1].Value != null)
                    {
                        perc += DgvSalaryTemp.Rows[iCOUNT].Cells["colPercentage"].Value.ToDecimal();
                        LblTotalPercentage.Text = perc.ToString();
                    }
                }
        }

        private void DgvSalaryTemp_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {

            }
        }

        private void BtnBottomCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DgvSalaryTemp_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            Changestatus(null, null);
        }
        private void Changestatus(object sender, EventArgs e) //function for changing status
        {
            if (!MblnIsEditMode)
            {
                BtnSave.Enabled = BtnOk.Enabled = MblnAddPermission;
                BindingNavigatorSaveItem.Enabled = MblnAddPermission;
            }
            else
            {
                BtnSave.Enabled = BtnOk.Enabled = MblnUpdatePermission;
                BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            }
            MblnChangeStatus = true;

        }

        private void DgvDedSalTemplate_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {

            }

        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (DeleteSalaryTemplate())
            {
                UserMessage.GetMessageByCode(4);
                UserMessage.ShowMessage(4);
            }
        }
        public bool DeleteSalaryTemplate()
        {
            bool blnRetValue = false;
            if (DeleteValidation())
            {
                MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.SalaryTempId = TxtName.Tag.ToInt32();
                if (MobjclsBLLSalaryTemplate.DeleteSalaryTemplate())
                {
                    AddNewSalaryTemplate();
                    blnRetValue = true;
                }

            }
            return blnRetValue;
        }
        private bool DeleteValidation()
        {
            if (TxtName.Tag.ToInt32() == 0)
            {
                UserMessage.ShowMessage(9015); // Sorry,No Data Found
                return false;

            }
            if (MobjclsBLLSalaryTemplate.GetSalaryTemplateExists(Convert.ToInt32(TxtName.Tag)))
            {
                UserMessage.ShowMessage(9017); // Sorry,No Data Found
                return false;
            }

            if (UserMessage.ShowMessage(13)) // Do you wish to Delete Absent policy information?
            {
                return true;
            }
            else
            {
                return false;
            }
            return true;

        }

        private void tabAdditionDeduction_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabAdditionDeduction.SelectedIndex == 1)
            {
                label2.Visible = false;
                LblTotalPercentage.Visible = false;
            }
            else
            {
                label2.Visible = true;
                LblTotalPercentage.Visible = true;
            }
        }

        private void DgvDedSalTemplate_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                 
                    if (e.ColumnIndex != -1 && e.RowIndex != -1)
                    {
                        if (e.ColumnIndex == colDedParticulars.Index)
                        {
                            if (DgvDedSalTemplate.CurrentCell.Value.ToInt32() != 0)
                            {
                                int intAdditionDeductionID = DgvDedSalTemplate.CurrentRow.Cells["colDedParticulars"].Value.ToInt32();
                                if (intAdditionDeductionID > 0)
                                {
                                    DataTable dtTemp = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "AdditionDeductionPolicyID", "SalaryTempDetail", "SalaryTempId = " + MobjclsBLLSalaryTemplate.objclsDTOSalaryTemplate.SalaryTempId  + " AND SalaryTempParticularsID = " + DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedParticulars"].Value.ToInt32() + " AND AdditionDeductionPolicyId IS NOT NULL" });
                                    FillDeductionPolicy(intAdditionDeductionID, e.RowIndex);
                                    if (dtTemp != null && dtTemp.Rows.Count > 0)
                                    {
                                        DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Value = dtTemp.Rows[0]["AdditionDeductionPolicyId"];
                                        DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Tag = dtTemp.Rows[0]["AdditionDeductionPolicyId"];
                                        DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Value = DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].FormattedValue;
                                    }
                                    else
                                    {
                                        DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Value = -1;
                                        DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Tag = -1;
                                        DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Value = DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].FormattedValue;
                                    }
                                }
                            }
                        }
                        else if (e.ColumnIndex == 1)
                        {
                            if (DgvDedSalTemplate.Rows[e.RowIndex].Cells["colAmount"].Value.ToInt32() > 0)
                            {
                                DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Tag = -5;
                                DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Value = -1;
                                DgvDedSalTemplate.CurrentCell = DgvDedSalTemplate[e.ColumnIndex, e.RowIndex]; 
                                DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Value = DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].FormattedValue;
                                DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Tag = -1;
                            }
                        }
                        else if (e.ColumnIndex == colDedPolicy.Index)
                        {
                            if (DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Tag.ToInt32()  != -5 && DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedParticulars"].Value.ToInt32() != 0 && DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Tag.ToInt32() > 0)
                            {
                                DgvDedSalTemplate.Rows[e.RowIndex].Cells["colAmount"].Value = string.Empty;
                            }
                        }
                    }
             }
            catch
            {
            }
        }
        private void FillDeductionPolicy(int intAdditionDeductionID, int intRowIndex)
        {
            try
            {
                DataTable datCombos = MobjclsBLLSalaryTemplate.FillCombos(new string[] { "AdditionDeductionPolicyId,PolicyName", "PayAdditionDeductionPolicyMaster", "AdditionDeductionID = " + intAdditionDeductionID });
                colDedPolicy.ValueMember = "AdditionDeductionPolicyId";
                colDedPolicy.DisplayMember = "PolicyName";
                if (datCombos != null)
                {
                    DataRow dr = datCombos.NewRow();
                    dr["AdditionDeductionPolicyId"] = "-1";
                    dr["PolicyName"] = "None";
                    datCombos.Rows.InsertAt(dr, 0);
                }
                colDedPolicy.DataSource = datCombos;
            }
            catch
            {
            }
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            ClearAllControls();
        }

        private void DgvDedSalTemplate_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

            if (e.ColumnIndex == colDedPolicy.Index)
            {
                if (DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedParticulars"].Value != null)
                {
                    int intAdditionDeductionID = DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedParticulars"].Value.ToInt32();
                    int tag = 0;
                    if (DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedParticulars"].Tag != null)
                    {
                        tag = DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedParticulars"].Tag.ToInt32();
                    }
                    FillDeductionPolicy(intAdditionDeductionID, e.RowIndex);
                    if (tag != 0)
                        DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedParticulars"].Tag = tag;
                }
            }


            Changestatus(null, null);
        }
        private void DgvSalaryTemp_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (DgvSalaryTemp!=null)
                {
                    //if (DgvSalaryTemp.CurrentCell.ColumnIndex != null)
                    //{
                        if (DgvSalaryTemp.CurrentCell.ColumnIndex == colPercentage.Index)
                        {
                            TextBox txt = (TextBox)(e.Control);
                            txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                        }
                    //}
                    if (DgvSalaryTemp.CurrentCell.ColumnIndex == colParticulars.Index)
                    {
                        // Avoid Black Color For dgvColParticulars
                        ComboBox cboParticulars = e.Control as ComboBox;
                        colParticulars_SelectedIndexChanged(cboParticulars, e);
                    }
                    else  if (DgvSalaryTemp.CurrentCell.ColumnIndex == colDedParticulars.Index)
                    {
                        // Avoid Black Color For dgvColParticulars
                        ComboBox colDedParticulars1 = e.Control as ComboBox;
                        colDedParticulars_SelectedIndexChanged(colDedParticulars1, e);
                    }
                }
            }
            catch
            {
            }
        }
        private void colDedParticulars_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Avoid Black Color For dgvColParticulars
            DataGridViewComboBoxEditingControl cboParticulars1 = (DataGridViewComboBoxEditingControl)sender;
            if (DgvDedSalTemplate.Rows[DgvDedSalTemplate.CurrentCell.RowIndex].Cells[2].Value.ToInt32() != -1)
            {
                DgvDedSalTemplate.Rows[DgvDedSalTemplate.CurrentCell.RowIndex].Cells[DgvDedSalTemplate.CurrentCell.ColumnIndex].ReadOnly = false;
                DgvDedSalTemplate.Rows[DgvDedSalTemplate.CurrentCell.RowIndex].Cells[1].Value = String.Empty;
            }
        }
        private void colParticulars_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Avoid Black Color For dgvColParticulars
            DataGridViewComboBoxEditingControl cboParticulars = (DataGridViewComboBoxEditingControl)sender;
            cboParticulars.SelectedIndexChanged -= new EventHandler(colParticulars_SelectedIndexChanged);
            cboParticulars.DropDown += new EventHandler(colParticulars_DropDown);
            cboParticulars.GotFocus += new EventHandler(colParticulars_DropDown);
        }
        private void colParticulars_DropDown(object sender, EventArgs e)
        {
            DataGridViewComboBoxEditingControl cboParticulars = (DataGridViewComboBoxEditingControl)sender;
            cboParticulars.BackColor = Color.White;
        }
        private void cololParticulars_DropDown(object sender, EventArgs e)
        {
            DataGridViewComboBoxEditingControl cboParticulars = (DataGridViewComboBoxEditingControl)sender;
            cboParticulars.BackColor = Color.White;
        }

        private void DgvDedSalTemplate_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

            if (DgvDedSalTemplate.CurrentCell.ColumnIndex == colAmount.Index)
            {
                TextBox txt = (TextBox)(e.Control);
                txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
            }

            if (DgvDedSalTemplate.CurrentCell.ColumnIndex == colDedParticulars.Index)
            {
                // Avoid Black Color For dgvColParticulars
                ComboBox cboParticulars1 = e.Control as ComboBox;
                colParticulars1_SelectedIndexChanged(cboParticulars1, e);

            }
            if (DgvDedSalTemplate.CurrentCell.ColumnIndex == colDedPolicy.Index)
            {
                // Avoid Black Color For dgvColParticulars
                ComboBox cboParticulars2 = e.Control as ComboBox;
                colParticulars2_SelectedIndexChanged(cboParticulars2, e);

            }
        }
        private void colParticulars1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Avoid Black Color For dgvColParticulars
            DataGridViewComboBoxEditingControl cboParticulars1 = (DataGridViewComboBoxEditingControl)sender;
            cboParticulars1.SelectedIndexChanged -= new EventHandler(colParticulars_SelectedIndexChanged);
            cboParticulars1.DropDown += new EventHandler(colParticulars1_DropDown);
            cboParticulars1.GotFocus += new EventHandler(colParticulars1_DropDown);
        }
        private void colParticulars2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Avoid Black Color For dgvColParticulars
            DataGridViewComboBoxEditingControl cboParticulars2 = (DataGridViewComboBoxEditingControl)sender;
            cboParticulars2.SelectedIndexChanged -= new EventHandler(colParticulars_SelectedIndexChanged);
            cboParticulars2.DropDown += new EventHandler(colParticulars2_DropDown);
            cboParticulars2.GotFocus += new EventHandler(colParticulars2_DropDown);
        }
        private void colParticulars1_DropDown(object sender, EventArgs e)
        {
            DataGridViewComboBoxEditingControl cboParticulars1 = (DataGridViewComboBoxEditingControl)sender;
            cboParticulars1.BackColor = Color.White;
        }
        private void colParticulars2_DropDown(object sender, EventArgs e)
        {
            DataGridViewComboBoxEditingControl cboParticulars2 = (DataGridViewComboBoxEditingControl)sender;
            cboParticulars2.BackColor = Color.White;
        }

        private void txtGrossAmount_TextChanged(object sender, EventArgs e)
        {
            Changestatus(null, null);
        }
        private void TxtName_TextChanged(object sender, EventArgs e)
        {
            Changestatus(null, null);
        }
        private void DgvSalaryTemp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                Changestatus(null, null);
            }
        }

        private void DgvDedSalTemplate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                Changestatus(null, null);
            }
        }

    
        private void btnSettlement_Click(object sender, EventArgs e)
        {
            int intID;
            if (cboSettlement.SelectedIndex != -1)
            {
                intID = Convert.ToInt32(cboSettlement.SelectedValue);
            }
            else
            {
                intID = -1;
            }
            using (FrmSettlementPolicy Comfrm = new FrmSettlementPolicy())
            {
                Comfrm.PiRecID = intID;
                Comfrm.ShowDialog();
                LoadCombos(5);
                cboSettlement.SelectedValue = intID;
            }
        }

        private void btnEncashPolicy_Click(object sender, EventArgs e)
        {
            int iEncashPolicy = cboEncashPolicy.SelectedValue.ToInt32();
            using (frmEncashPolicy objFrmEncashPolicy = new frmEncashPolicy())
            {
                objFrmEncashPolicy.PintEncashPolicyID = iEncashPolicy;
                objFrmEncashPolicy.ShowDialog();
                LoadCombos(7);
                if (objFrmEncashPolicy.PintEncashPolicyID != 0)
                    cboEncashPolicy.SelectedValue = objFrmEncashPolicy.PintEncashPolicyID;
                else
                    cboEncashPolicy.SelectedValue = iEncashPolicy;
            }
        }

        private void btnOvertimePolicy_Click(object sender, EventArgs e)
        {
            try
            {
                int iOTPolicyID = Convert.ToInt32(cboOvertimePolicy.SelectedValue);
                using (FrmOvertimePolicy objFrmOTPolicy = new FrmOvertimePolicy())
                {
                    objFrmOTPolicy.PintOTPolicyID = iOTPolicyID;
                    objFrmOTPolicy.ShowDialog();
                    LoadCombos(8);
                    if (objFrmOTPolicy.PintOTPolicyIDForFocus != 0)

                        cboOvertimePolicy.SelectedValue = objFrmOTPolicy.PintOTPolicyIDForFocus;
                    else
                        cboOvertimePolicy.SelectedValue = objFrmOTPolicy.PintOTPolicyID;
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
        }

        private void txtDecimal_KeyPress(object sender, KeyPressEventArgs e) // Key Press For TextDecimal
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = strInvalidChars + ".";
            }

            e.Handled = false;
            if (((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains("."))) && ClsCommonSettings.IsAmountRoundByZero == false)
            {
                e.Handled = true;
            }
            else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0 && ClsCommonSettings.IsAmountRoundByZero == true)
            {
                e.Handled = true;
            }
        }

        private void btnAbsentPolicy_Click(object sender, EventArgs e)
        {
            int iAbsentPolicyID = cboAbsentPolicy.SelectedValue.ToInt32();
            using (FrmAbsentPolicy objFrmAbsentPolicy = new FrmAbsentPolicy())
            {
                objFrmAbsentPolicy.PintAbsentPolicyID = iAbsentPolicyID;
                objFrmAbsentPolicy.ShowDialog();
                LoadCombos(4);
                if (objFrmAbsentPolicy.PintAbsentPolicyID != 0)
                    cboAbsentPolicy.SelectedValue = objFrmAbsentPolicy.PintAbsentPolicyID;
                else
                    cboAbsentPolicy.SelectedValue = iAbsentPolicyID;
            }
        }

        private void btnHolidayPolicy_Click(object sender, EventArgs e)
        {
            int iHolidayPolicy = cboHolidayPolicy.SelectedValue.ToInt32();
            using (FrmHolidayPolicy objFrmHolidayPolicy = new FrmHolidayPolicy())
            {
                objFrmHolidayPolicy.PintHolidayPolicyID = iHolidayPolicy;
                objFrmHolidayPolicy.ShowDialog();
                LoadCombos(6);
                if (objFrmHolidayPolicy.PintHolidayPolicyID != 0)
                    cboHolidayPolicy.SelectedValue = objFrmHolidayPolicy.PintHolidayPolicyID;
                else
                    cboHolidayPolicy.SelectedValue = iHolidayPolicy;
            }
        }

        private void DgvDedSalTemplate_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                DgvDedSalTemplate.EndEdit();
                if (e.ColumnIndex == colDedPolicy.Index)
                {
                    DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Tag = DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Value;
                    DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].Value = DgvDedSalTemplate.Rows[e.RowIndex].Cells["colDedPolicy"].FormattedValue;
                }
            }
        }

        private void DgvDedSalTemplate_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (DgvDedSalTemplate.IsCurrentCellDirty)
                {
                    if (DgvDedSalTemplate.CurrentCell != null)
                        DgvDedSalTemplate.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }

        }


    }

}





