﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public partial class FrmSIF : Form
    {
        clsBLLSalaryProcessAndRelease MobjclsBLLSalaryProcessAndRelease;
        private clsMessage ObjUserMessage = null;
        public int PCompanyID;
        string MstrSIFfileName = "";
        //public string PCompanyName;
        string MstrCommonMessage;
        string MstrMessageCaption = ClsCommonSettings.MessageCaption;
        public FrmSIF()
        {
            InitializeComponent();
            MobjclsBLLSalaryProcessAndRelease = new clsBLLSalaryProcessAndRelease();
            GrdEDRDetails.RowsDefaultCellStyle.BackColor = Color.Wheat;
            GrdEDRDetails.AlternatingRowsDefaultCellStyle.BackColor = Color.BurlyWood;
           
        }
       
        private void DtpCurrentMonth_ValueChanged(object sender, EventArgs e)
        {
            int iMonth = DtpCurrentMonth.Value.Date.Month;
            int iYear = DtpCurrentMonth.Value.Date.Year;
        }
        private string GetCurrentMonthStr(int MonthVal)
        {
            string Months;
            Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;

            }


            return Months;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {


            try
            {
                string strPath = "";
                int IsVisa = 0;
                int Is50Percentage = 0;
                if (cboBank.SelectedIndex == -1)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(15);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (cboCompany.SelectedIndex == -1)
                {
                    return;
                }
                else
                {
                    PCompanyID = cboCompany.SelectedValue.ToInt32();
                }
                if (rtbVisaCompany.Checked)
                {
                    IsVisa = 1;
                }
                else
                {
                    rtbVisaCompany.Checked = false;
                    IsVisa = 0;
                }

                GrdEDRDetails.DataSource = null;
                DataSet DT;
            


                DT = MobjclsBLLSalaryProcessAndRelease.InsertIntoFileTables(DtpCurrentMonth.Value.ToString("dd-MMM-yyyy"), strPath, PCompanyID, cboBank.SelectedValue.ToInt32(), IsVisa);
                if (DT.Tables[0].Rows.Count > 0)
                {


                    btnShow.Tag =DT.Tables[0].Rows[0]["SIFID"].ToDecimal();
                    MstrSIFfileName = DT.Tables[0].Rows[0]["SIFName"].ToString(); 
                    if (DT.Tables[1] != null)
                    {
                        if (DT.Tables[1].Rows.Count > 0)
                        {
                            GrdEDRDetails.DataSource = DT.Tables[1];
                            
                            if (GrdEDRDetails.Columns.Contains("EDRID"))
                                GrdEDRDetails.Columns["EDRID"].Visible = false;

                            if (GrdEDRDetails.Columns.Contains("Employee UniqueID"))
                                GrdEDRDetails.Columns["Employee UniqueID"].Width = 175;

                            if (GrdEDRDetails.Columns.Contains("Income Fixed Component"))
                                GrdEDRDetails.Columns["Income Fixed Component"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                            if (GrdEDRDetails.Columns.Contains("Income Variable Component"))
                                GrdEDRDetails.Columns["Income Variable Component"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                            if (GrdEDRDetails.Columns.Contains("Days on leave for period"))
                                GrdEDRDetails.Columns["Days on leave for period"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        }
                    }
                    if (DT.Tables[2] != null)
                    {
                        if (DT.Tables[2].Rows.Count > 0)
                        {
                            GrdSCRDetails.DataSource = DT.Tables[2];

                            if (GrdSCRDetails.Columns.Contains("Record Type"))
                                GrdSCRDetails.Columns["Record Type"].Width = 60;

                            if (GrdSCRDetails.Columns.Contains("Employer Unique ID"))
                                GrdSCRDetails.Columns["Employer Unique ID"].Width = 175;


                            if (GrdSCRDetails.Columns.Contains("Bank Code Of The Employer"))
                                GrdSCRDetails.Columns["Bank Code Of The Employer"].Width = 200;


                            if (GrdSCRDetails.Columns.Contains("File Creation Date"))
                                GrdSCRDetails.Columns["File Creation Date"].Width = 100;

                            if (GrdSCRDetails.Columns.Contains("File Creation Time"))
                                GrdSCRDetails.Columns["File Creation Time"].Width = 100;


                            if (GrdSCRDetails.Columns.Contains("Salary Month"))
                                GrdSCRDetails.Columns["Salary Month"].Width = 100;


                            if (GrdSCRDetails.Columns.Contains("EDR Count"))
                                GrdSCRDetails.Columns["EDR Count"].Width = 100;

                            if (GrdSCRDetails.Columns.Contains("Total Salary"))
                                GrdSCRDetails.Columns["Total Salary"].Width = 200;

                            if (GrdSCRDetails.Columns.Contains("Payment Currency"))
                                GrdSCRDetails.Columns["Payment Currency"].Width = 200;

                            if (GrdSCRDetails.Columns.Contains("Employer Reference"))
                                GrdSCRDetails.Columns["Employer Reference"].Width = 200;


                        }
                    }
                }



                for (int i = 0; i <= GrdEDRDetails.Columns.Count - 1; i++)
                {
                    if (GrdEDRDetails.Columns[i].HeaderText != "Income Fixed Component" && GrdEDRDetails.Columns[i].HeaderText != "Income Variable Component")
                    {
                        GrdEDRDetails.Columns[i].ReadOnly = true;
                    }
                }


                for (int i = 0; i <= GrdSCRDetails.Columns.Count - 1; i++)
                {
                    //if (GrdSCRDetails.Columns[i].HeaderText != "Total Salary")
                    //{
                        GrdSCRDetails.Columns[i].ReadOnly = true;
                    //}
                }

            }
            catch
            {

            }

        }
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalaryRelease);
                return this.ObjUserMessage;
            }
        }
        private void FrmSIF_Load(object sender, EventArgs e)
        {
            try
            {
                rtbVisaCompany.Checked = true;
                rtbWorkingCompany.Checked = true;
                LoadCombo(0);

              

            }
            catch
            {
            }
        }
        private void LoadCombo(int TypeID)
        {
            try
            {
                if (TypeID == 0 || TypeID == 1)
                {
                    DataTable datTempBank = new DataTable();
                    datTempBank.Columns.Add("BankID");
                    datTempBank.Columns.Add("Bank");

                    if (PCompanyID >= 0)
                    {
                        datTempBank = MobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(26, PCompanyID, rtbVisaCompany.Checked.ToBoolean()); // Get Bank
                    }
                    cboBank.ValueMember = "BankID";
                    cboBank.DisplayMember = "Bank";
                    cboBank.DataSource = datTempBank;
                    cboBank.Text = "";
                    cboBank.SelectedIndex = -1;
                }
                if (TypeID == 0 || TypeID == 2)
                {
                    DataTable datTemp = MobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] {"CompanyID,CompanyName", "CompanyMaster" ,""}); 
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datTemp;
                }

            }
            catch
            {
            }
        }

        private void cboBank_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                cboBank.DroppedDown = false; 
            }
            catch
            {
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboCompany.SelectedIndex != -1)
                {
                    PCompanyID = Convert.ToInt32(cboCompany.SelectedValue.ToInt32());    
                }

                LoadCombo(1); LoadCombo(1);
            }
            catch
            {
            }
        }

        private void rtbVisaCompany_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCombo(1);
            }
            catch
            {
            }
        }



        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnEvaluate_Click(object sender, EventArgs e)
        {
            try
            {
                GrdTot();
               
            }
            catch
            {
            }
        }
        private decimal GrdTot()
        {
            try
            {
                decimal GrandTot = 0;
                for (int i = 0; i <= GrdEDRDetails.Rows.Count - 1; i++)
                {
                    GrandTot = GrandTot + (GrdEDRDetails.Rows[i].Cells["Income Fixed Component"].Value.ToDecimal() + GrdEDRDetails.Rows[i].Cells["Income Variable Component"].Value.ToDecimal());
                }

                GrdSCRDetails.Rows[0].Cells["Total Salary"].Value = GrandTot;
                return GrandTot;
            }
            catch
            {
                return 0;
            }
        }
        private void GrdEDRDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            this.GrdEDRDetails.EditingControl.KeyPress += new KeyPressEventHandler(EditingControlConsequence_KeyPress);
        }

        void EditingControlConsequence_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (GrdEDRDetails.Columns.Contains("Income Fixed Component"))
            {
                if (GrdEDRDetails.CurrentCell.OwningColumn.Name == "Income Fixed Component" || GrdEDRDetails.CurrentCell.OwningColumn.Name == "Income Variable Component")
                {
                    if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                    {
                        e.Handled = true;
                    }
                    if (!string.IsNullOrEmpty(GrdEDRDetails.EditingControl.Text) && (e.KeyChar == 46))//checking more than one "."
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string strpath = "";
                decimal DecTotal = 0;
                DecTotal = GrdTot();
                DataTable DT = (DataTable)GrdEDRDetails.DataSource;
                MobjclsBLLSalaryProcessAndRelease.UpdateSIF(DT, btnShow.Tag.ToInt32());
                saveFileDialog1.ShowDialog();
                strpath = saveFileDialog1.SelectedPath.ToString();
                MobjclsBLLSalaryProcessAndRelease.ExportToCSV(MstrSIFfileName, Convert.ToUInt32(btnShow.Tag), strpath);
            }
            catch
            {
            }
        }

        private void pnlBottom_Click(object sender, EventArgs e)
        {

        }
    }
}
