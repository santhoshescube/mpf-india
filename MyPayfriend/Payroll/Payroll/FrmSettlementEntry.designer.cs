﻿namespace MyPayfriend
{
    partial class FrmSettlementEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label12;
            System.Windows.Forms.Label Label4;
            System.Windows.Forms.Label LblDeduction;
            System.Windows.Forms.Label LblEarning;
            System.Windows.Forms.Label Label5;
            System.Windows.Forms.Label DateLabel;
            System.Windows.Forms.Label NetAmountLabel;
            System.Windows.Forms.Label lblCheqNo;
            System.Windows.Forms.Label lblCheqDate;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label lblParticulars;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettlementEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtAbsentDays = new System.Windows.Forms.TextBox();
            this.chkAbsent = new System.Windows.Forms.CheckBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.txtNofDaysExperience = new System.Windows.Forms.TextBox();
            this.lblTakenLeavePayDays = new System.Windows.Forms.Label();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.EmployeeSettlementBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.lblEligibleLeavePayDays = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.EmployeeSettlementBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnCancel = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripSplitButton();
            this.btnTemplate1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnTemplate2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnTemplate3 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnTemplate4 = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnPrintRpt = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.Label10 = new System.Windows.Forms.Label();
            this.LblEmployeeSettlement = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusStripEmployeeSettlement = new System.Windows.Forms.StatusStrip();
            this.OKSaveButton = new System.Windows.Forms.Button();
            this.TmrEmployeeSettlement = new System.Windows.Forms.Timer(this.components);
            this.ErrEmployeeSettlement = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtLeavePayDays = new System.Windows.Forms.TextBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.lblSettlementPolicy = new System.Windows.Forms.Label();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.DtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblNoofMonths = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.EmployeeSettlementBindingSourcerpt = new System.Windows.Forms.BindingSource(this.components);
            this.RemarksTextBox1 = new System.Windows.Forms.TextBox();
            this.BtnBottomCancel = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.CboEmployee = new System.Windows.Forms.ComboBox();
            this.SettlementTab = new System.Windows.Forms.TabControl();
            this.SettlementTabEmpInfo = new System.Windows.Forms.TabPage();
            this.cboTransactionType = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpChequeDate = new System.Windows.Forms.DateTimePicker();
            this.DocumentReturnCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtEligibleDays = new System.Windows.Forms.TextBox();
            this.txtExcludeHolidays = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkExcludeHolidays = new System.Windows.Forms.CheckBox();
            this.GrdLeaveType = new System.Windows.Forms.DataGridView();
            this.Select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.LeaveTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeaveType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Days = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNoticePeriod = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.txtExcludeLeaveDays = new System.Windows.Forms.TextBox();
            this.txtChequeNo = new System.Windows.Forms.TextBox();
            this.BtnProcess = new System.Windows.Forms.Button();
            this.LblType = new System.Windows.Forms.Label();
            this.CboType = new System.Windows.Forms.ComboBox();
            this.SettlementTabParticulars = new System.Windows.Forms.TabPage();
            this.btnAdditionDeductions = new System.Windows.Forms.Button();
            this.cboParticulars = new System.Windows.Forms.ComboBox();
            this.EmployeeSettlementDetailDataGridView = new System.Windows.Forms.DataGridView();
            this.EmployeeSettlementDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Particulars = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Credit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Debit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RepaymentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoanID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsAddition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalaryPaymentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LblCurrency = new System.Windows.Forms.Label();
            this.NetAmountTextBox = new System.Windows.Forms.TextBox();
            this.TxtTotalDeduction = new System.Windows.Forms.TextBox();
            this.TxtTotalEarning = new System.Windows.Forms.TextBox();
            this.SettlementTabHistory = new System.Windows.Forms.TabPage();
            this.DtGrdEmpHistory = new System.Windows.Forms.DataGridView();
            this.ColDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFromDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColToDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GratuityTab = new System.Windows.Forms.TabPage();
            this.DtGrdGratuity = new System.Windows.Forms.DataGridView();
            this.label17 = new System.Windows.Forms.Label();
            this.DtpLastWorkingDate = new System.Windows.Forms.DateTimePicker();
            this.dtpResignationDate = new System.Windows.Forms.DateTimePicker();
            this.lblToDate = new System.Windows.Forms.Label();
            this.LblEmployee = new System.Windows.Forms.Label();
            this.DateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.TextBoxNumeric = new System.Windows.Forms.TextBox();
            this.EmployeeSettlementDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BtnSave = new System.Windows.Forms.Button();
            this.lblPayBack = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expLeft = new DevComponents.DotNetBar.ExpandablePanel();
            this.dgvSearch = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.SettlementID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Employee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeArb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTopSearch = new DevComponents.DotNetBar.PanelEx();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cboSearchEmployee = new System.Windows.Forms.ComboBox();
            this.lblSearchEmployee = new System.Windows.Forms.Label();
            this.lblEmployeeCode = new System.Windows.Forms.Label();
            this.cboSearchCode = new System.Windows.Forms.ComboBox();
            this.lblSearchTo = new System.Windows.Forms.Label();
            this.lblSearchFrom = new System.Windows.Forms.Label();
            this.dtpSearchToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpSearchFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblSearchCompany = new System.Windows.Forms.Label();
            this.cboSearchCompany = new System.Windows.Forms.ComboBox();
            this.btnShow = new System.Windows.Forms.Button();
            this.gmajilgmai = new System.Windows.Forms.ProgressBar();
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.ColYears = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCalDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColGRTYDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PerDayGratuity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColGratuityAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Label12 = new System.Windows.Forms.Label();
            Label4 = new System.Windows.Forms.Label();
            LblDeduction = new System.Windows.Forms.Label();
            LblEarning = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            DateLabel = new System.Windows.Forms.Label();
            NetAmountLabel = new System.Windows.Forms.Label();
            lblCheqNo = new System.Windows.Forms.Label();
            lblCheqDate = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            lblParticulars = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementBindingNavigator)).BeginInit();
            this.EmployeeSettlementBindingNavigator.SuspendLayout();
            this.StatusStripEmployeeSettlement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployeeSettlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementBindingSourcerpt)).BeginInit();
            this.SettlementTab.SuspendLayout();
            this.SettlementTabEmpInfo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdLeaveType)).BeginInit();
            this.SettlementTabParticulars.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementDetailDataGridView)).BeginInit();
            this.SettlementTabHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtGrdEmpHistory)).BeginInit();
            this.GratuityTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtGrdGratuity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementDetailBindingSource)).BeginInit();
            this.expLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).BeginInit();
            this.pnlTopSearch.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label12
            // 
            Label12.AutoSize = true;
            Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label12.Location = new System.Drawing.Point(7, 166);
            Label12.Name = "Label12";
            Label12.Size = new System.Drawing.Size(67, 13);
            Label12.TabIndex = 7;
            Label12.Text = "Leave Pay";
            // 
            // Label4
            // 
            Label4.AutoSize = true;
            Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label4.Location = new System.Drawing.Point(246, 13);
            Label4.Name = "Label4";
            Label4.Size = new System.Drawing.Size(51, 13);
            Label4.TabIndex = 0;
            Label4.Text = "Gratuity";
            // 
            // LblDeduction
            // 
            LblDeduction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            LblDeduction.AutoSize = true;
            LblDeduction.BackColor = System.Drawing.Color.Transparent;
            LblDeduction.FlatStyle = System.Windows.Forms.FlatStyle.System;
            LblDeduction.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Italic);
            LblDeduction.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            LblDeduction.Location = new System.Drawing.Point(569, 304);
            LblDeduction.Name = "LblDeduction";
            LblDeduction.Size = new System.Drawing.Size(42, 9);
            LblDeduction.TabIndex = 134;
            LblDeduction.Text = "Debit Total";
            // 
            // LblEarning
            // 
            LblEarning.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            LblEarning.AutoSize = true;
            LblEarning.BackColor = System.Drawing.Color.Transparent;
            LblEarning.FlatStyle = System.Windows.Forms.FlatStyle.System;
            LblEarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Italic);
            LblEarning.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            LblEarning.Location = new System.Drawing.Point(416, 304);
            LblEarning.Name = "LblEarning";
            LblEarning.Size = new System.Drawing.Size(45, 9);
            LblEarning.TabIndex = 132;
            LblEarning.Text = "Credit Total";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(362, 10);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(43, 13);
            Label5.TabIndex = 187;
            Label5.Text = "Amount";
            // 
            // DateLabel
            // 
            DateLabel.AutoSize = true;
            DateLabel.Location = new System.Drawing.Point(599, 152);
            DateLabel.Name = "DateLabel";
            DateLabel.Size = new System.Drawing.Size(71, 13);
            DateLabel.TabIndex = 5;
            DateLabel.Text = "Process Date";
            DateLabel.Visible = false;
            // 
            // NetAmountLabel
            // 
            NetAmountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            NetAmountLabel.AutoSize = true;
            NetAmountLabel.Location = new System.Drawing.Point(497, 332);
            NetAmountLabel.Name = "NetAmountLabel";
            NetAmountLabel.Size = new System.Drawing.Size(63, 13);
            NetAmountLabel.TabIndex = 8;
            NetAmountLabel.Text = "Net Amount";
            // 
            // lblCheqNo
            // 
            lblCheqNo.AutoSize = true;
            lblCheqNo.Location = new System.Drawing.Point(9, 198);
            lblCheqNo.Name = "lblCheqNo";
            lblCheqNo.Size = new System.Drawing.Size(64, 13);
            lblCheqNo.TabIndex = 2;
            lblCheqNo.Text = "Cheque No.";
            // 
            // lblCheqDate
            // 
            lblCheqDate.AutoSize = true;
            lblCheqDate.Location = new System.Drawing.Point(9, 242);
            lblCheqDate.Name = "lblCheqDate";
            lblCheqDate.Size = new System.Drawing.Size(70, 13);
            lblCheqDate.TabIndex = 4;
            lblCheqDate.Text = "Cheque Date";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label16.Location = new System.Drawing.Point(4, 13);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(70, 13);
            label16.TabIndex = 26;
            label16.Text = "Experience";
            // 
            // lblParticulars
            // 
            lblParticulars.AutoSize = true;
            lblParticulars.Location = new System.Drawing.Point(6, 8);
            lblParticulars.Name = "lblParticulars";
            lblParticulars.Size = new System.Drawing.Size(51, 13);
            lblParticulars.TabIndex = 211;
            lblParticulars.Text = "Particular";
            // 
            // txtAbsentDays
            // 
            this.txtAbsentDays.Enabled = false;
            this.txtAbsentDays.Location = new System.Drawing.Point(118, 270);
            this.txtAbsentDays.MaxLength = 9;
            this.txtAbsentDays.Name = "txtAbsentDays";
            this.txtAbsentDays.Size = new System.Drawing.Size(106, 20);
            this.txtAbsentDays.TabIndex = 7;
            this.txtAbsentDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAbsentDays.TextChanged += new System.EventHandler(this.txtAbsentDays_TextChanged);
            // 
            // chkAbsent
            // 
            this.chkAbsent.AutoSize = true;
            this.chkAbsent.Location = new System.Drawing.Point(11, 272);
            this.chkAbsent.Name = "chkAbsent";
            this.chkAbsent.Size = new System.Drawing.Size(103, 17);
            this.chkAbsent.TabIndex = 10;
            this.chkAbsent.Text = "Consider Absent";
            this.chkAbsent.UseVisualStyleBackColor = true;
            this.chkAbsent.CheckedChanged += new System.EventHandler(this.chkAbsent_CheckedChanged);
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(261, 273);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(73, 13);
            this.Label9.TabIndex = 12;
            this.Label9.Text = "Balance Days";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(11, 70);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(58, 13);
            this.Label13.TabIndex = 3;
            this.Label13.Text = "Total Days";
            // 
            // txtNofDaysExperience
            // 
            this.txtNofDaysExperience.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNofDaysExperience.Location = new System.Drawing.Point(118, 70);
            this.txtNofDaysExperience.MaxLength = 9;
            this.txtNofDaysExperience.Name = "txtNofDaysExperience";
            this.txtNofDaysExperience.Size = new System.Drawing.Size(76, 13);
            this.txtNofDaysExperience.TabIndex = 2;
            this.txtNofDaysExperience.Text = "NIL";
            this.txtNofDaysExperience.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // lblTakenLeavePayDays
            // 
            this.lblTakenLeavePayDays.AutoSize = true;
            this.lblTakenLeavePayDays.Location = new System.Drawing.Point(115, 235);
            this.lblTakenLeavePayDays.Name = "lblTakenLeavePayDays";
            this.lblTakenLeavePayDays.Size = new System.Drawing.Size(24, 13);
            this.lblTakenLeavePayDays.TabIndex = 6;
            this.lblTakenLeavePayDays.Text = "NIL";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // EmployeeSettlementBindingNavigatorSaveItem
            // 
            this.EmployeeSettlementBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EmployeeSettlementBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("EmployeeSettlementBindingNavigatorSaveItem.Image")));
            this.EmployeeSettlementBindingNavigatorSaveItem.Name = "EmployeeSettlementBindingNavigatorSaveItem";
            this.EmployeeSettlementBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.EmployeeSettlementBindingNavigatorSaveItem.Text = "Save Data";
            this.EmployeeSettlementBindingNavigatorSaveItem.Click += new System.EventHandler(this.EmployeeSettlementBindingNavigatorSaveItem_Click);
            // 
            // lblEligibleLeavePayDays
            // 
            this.lblEligibleLeavePayDays.AutoSize = true;
            this.lblEligibleLeavePayDays.Location = new System.Drawing.Point(115, 198);
            this.lblEligibleLeavePayDays.Name = "lblEligibleLeavePayDays";
            this.lblEligibleLeavePayDays.Size = new System.Drawing.Size(24, 13);
            this.lblEligibleLeavePayDays.TabIndex = 5;
            this.lblEligibleLeavePayDays.Text = "NIL";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(11, 234);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(65, 13);
            this.Label11.TabIndex = 5;
            this.Label11.Text = "Taken Days";
            // 
            // EmployeeSettlementBindingNavigator
            // 
            this.EmployeeSettlementBindingNavigator.AddNewItem = null;
            this.EmployeeSettlementBindingNavigator.CountItem = null;
            this.EmployeeSettlementBindingNavigator.DeleteItem = null;
            this.EmployeeSettlementBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewItem,
            this.EmployeeSettlementBindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnCancel,
            this.ToolStripSeparator1,
            this.BtnPrint,
            this.BtnPrintRpt,
            this.btnEmail,
            this.ToolStripSeparator3,
            this.BtnHelp});
            this.EmployeeSettlementBindingNavigator.Location = new System.Drawing.Point(258, 0);
            this.EmployeeSettlementBindingNavigator.MoveFirstItem = null;
            this.EmployeeSettlementBindingNavigator.MoveLastItem = null;
            this.EmployeeSettlementBindingNavigator.MoveNextItem = null;
            this.EmployeeSettlementBindingNavigator.MovePreviousItem = null;
            this.EmployeeSettlementBindingNavigator.Name = "EmployeeSettlementBindingNavigator";
            this.EmployeeSettlementBindingNavigator.PositionItem = null;
            this.EmployeeSettlementBindingNavigator.Size = new System.Drawing.Size(736, 25);
            this.EmployeeSettlementBindingNavigator.TabIndex = 0;
            this.EmployeeSettlementBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(23, 22);
            this.BtnCancel.ToolTipText = "Clear";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnTemplate1,
            this.btnTemplate2,
            this.btnTemplate3,
            this.btnTemplate4});
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(32, 22);
            this.BtnPrint.ToolTipText = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            this.BtnPrint.ButtonClick += new System.EventHandler(this.BtnPrint_ButtonClick);
            // 
            // btnTemplate1
            // 
            this.btnTemplate1.Name = "btnTemplate1";
            this.btnTemplate1.Size = new System.Drawing.Size(130, 22);
            this.btnTemplate1.Text = "Template1";
            this.btnTemplate1.Click += new System.EventHandler(this.btnTemplate1_Click);
            // 
            // btnTemplate2
            // 
            this.btnTemplate2.Name = "btnTemplate2";
            this.btnTemplate2.Size = new System.Drawing.Size(130, 22);
            this.btnTemplate2.Text = "Template2";
            this.btnTemplate2.Click += new System.EventHandler(this.btnTemplate2_Click);
            // 
            // btnTemplate3
            // 
            this.btnTemplate3.Name = "btnTemplate3";
            this.btnTemplate3.Size = new System.Drawing.Size(130, 22);
            this.btnTemplate3.Text = "Template3";
            this.btnTemplate3.Click += new System.EventHandler(this.btnTemplate3_Click);
            // 
            // btnTemplate4
            // 
            this.btnTemplate4.Name = "btnTemplate4";
            this.btnTemplate4.Size = new System.Drawing.Size(130, 22);
            this.btnTemplate4.Text = "Template4";
            this.btnTemplate4.Click += new System.EventHandler(this.btnTemplate4_Click);
            // 
            // BtnPrintRpt
            // 
            this.BtnPrintRpt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrintRpt.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrintRpt.Image")));
            this.BtnPrintRpt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrintRpt.Name = "BtnPrintRpt";
            this.BtnPrintRpt.Size = new System.Drawing.Size(23, 22);
            this.BtnPrintRpt.ToolTipText = "Print";
            this.BtnPrintRpt.Visible = false;
            this.BtnPrintRpt.Click += new System.EventHandler(this.BtnPrintRpt_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnEmail.Image")));
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.ToolTipText = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(11, 195);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(67, 13);
            this.Label10.TabIndex = 8;
            this.Label10.Text = "Eligible Days";
            // 
            // LblEmployeeSettlement
            // 
            this.LblEmployeeSettlement.Name = "LblEmployeeSettlement";
            this.LblEmployeeSettlement.Size = new System.Drawing.Size(0, 17);
            // 
            // StatusStripEmployeeSettlement
            // 
            this.StatusStripEmployeeSettlement.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblEmployeeSettlement});
            this.StatusStripEmployeeSettlement.Location = new System.Drawing.Point(0, 502);
            this.StatusStripEmployeeSettlement.Name = "StatusStripEmployeeSettlement";
            this.StatusStripEmployeeSettlement.Size = new System.Drawing.Size(994, 22);
            this.StatusStripEmployeeSettlement.TabIndex = 172;
            this.StatusStripEmployeeSettlement.Text = "StatusStrip1";
            // 
            // OKSaveButton
            // 
            this.OKSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKSaveButton.Location = new System.Drawing.Point(831, 476);
            this.OKSaveButton.Name = "OKSaveButton";
            this.OKSaveButton.Size = new System.Drawing.Size(75, 23);
            this.OKSaveButton.TabIndex = 7;
            this.OKSaveButton.Text = "&Confirm";
            this.OKSaveButton.UseVisualStyleBackColor = true;
            this.OKSaveButton.Click += new System.EventHandler(this.OKSaveButton_Click);
            // 
            // TmrEmployeeSettlement
            // 
            this.TmrEmployeeSettlement.Interval = 1000;
            // 
            // ErrEmployeeSettlement
            // 
            this.ErrEmployeeSettlement.ContainerControl = this;
            this.ErrEmployeeSettlement.RightToLeft = true;
            // 
            // txtLeavePayDays
            // 
            this.txtLeavePayDays.Location = new System.Drawing.Point(368, 269);
            this.txtLeavePayDays.MaxLength = 9;
            this.txtLeavePayDays.Name = "txtLeavePayDays";
            this.txtLeavePayDays.Size = new System.Drawing.Size(106, 20);
            this.txtLeavePayDays.TabIndex = 9;
            this.txtLeavePayDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLeavePayDays.TextChanged += new System.EventHandler(this.txtLeavePayDays_TextChanged);
            this.txtLeavePayDays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(417, 6);
            this.txtAmount.MaxLength = 13;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(124, 20);
            this.txtAmount.TabIndex = 2;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // lblSettlementPolicy
            // 
            this.lblSettlementPolicy.AutoSize = true;
            this.lblSettlementPolicy.Location = new System.Drawing.Point(119, 22);
            this.lblSettlementPolicy.Name = "lblSettlementPolicy";
            this.lblSettlementPolicy.Size = new System.Drawing.Size(24, 13);
            this.lblSettlementPolicy.TabIndex = 0;
            this.lblSettlementPolicy.Text = "NIL";
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Location = new System.Drawing.Point(9, 110);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(66, 13);
            this.lblFromDate.TabIndex = 7;
            this.lblFromDate.Text = "Joining Date";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(11, 39);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(69, 13);
            this.Label8.TabIndex = 1;
            this.Label8.Text = "Total Months";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(9, 22);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(105, 13);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "Settlement Policy";
            // 
            // DtpFromDate
            // 
            this.DtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpFromDate.Enabled = false;
            this.DtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFromDate.Location = new System.Drawing.Point(107, 106);
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.Size = new System.Drawing.Size(106, 20);
            this.DtpFromDate.TabIndex = 2;
            this.DtpFromDate.ValueChanged += new System.EventHandler(this.DtpFromDate_ValueChanged);
            // 
            // lblNoofMonths
            // 
            this.lblNoofMonths.AutoSize = true;
            this.lblNoofMonths.Location = new System.Drawing.Point(115, 39);
            this.lblNoofMonths.Name = "lblNoofMonths";
            this.lblNoofMonths.Size = new System.Drawing.Size(24, 13);
            this.lblNoofMonths.TabIndex = 1;
            this.lblNoofMonths.Text = "NIL";
            // 
            // Label7
            // 
            this.Label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(16, 300);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(49, 13);
            this.Label7.TabIndex = 191;
            this.Label7.Text = "Remarks";
            // 
            // EmployeeSettlementBindingSourcerpt
            // 
            this.EmployeeSettlementBindingSourcerpt.DataMember = "EmployeeSettlement";
            // 
            // RemarksTextBox1
            // 
            this.RemarksTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RemarksTextBox1.Location = new System.Drawing.Point(78, 297);
            this.RemarksTextBox1.MaxLength = 500;
            this.RemarksTextBox1.Multiline = true;
            this.RemarksTextBox1.Name = "RemarksTextBox1";
            this.RemarksTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RemarksTextBox1.Size = new System.Drawing.Size(306, 52);
            this.RemarksTextBox1.TabIndex = 10;
            this.RemarksTextBox1.TextChanged += new System.EventHandler(this.RemarksTextBox1_TextChanged);
            // 
            // BtnBottomCancel
            // 
            this.BtnBottomCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnBottomCancel.Location = new System.Drawing.Point(912, 476);
            this.BtnBottomCancel.Name = "BtnBottomCancel";
            this.BtnBottomCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnBottomCancel.TabIndex = 8;
            this.BtnBottomCancel.Text = "&Cancel";
            this.BtnBottomCancel.UseVisualStyleBackColor = true;
            this.BtnBottomCancel.Click += new System.EventHandler(this.BtnBottomCancel_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.Location = new System.Drawing.Point(566, 4);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(43, 24);
            this.AddBtn.TabIndex = 4;
            this.AddBtn.Text = "Add";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // CboEmployee
            // 
            this.CboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.CboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.CboEmployee.DropDownHeight = 134;
            this.CboEmployee.FormattingEnabled = true;
            this.CboEmployee.IntegralHeight = false;
            this.CboEmployee.Location = new System.Drawing.Point(395, 33);
            this.CboEmployee.Name = "CboEmployee";
            this.CboEmployee.Size = new System.Drawing.Size(337, 21);
            this.CboEmployee.TabIndex = 1;
            this.CboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // SettlementTab
            // 
            this.SettlementTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SettlementTab.Controls.Add(this.SettlementTabEmpInfo);
            this.SettlementTab.Controls.Add(this.SettlementTabParticulars);
            this.SettlementTab.Controls.Add(this.SettlementTabHistory);
            this.SettlementTab.Controls.Add(this.GratuityTab);
            this.SettlementTab.Location = new System.Drawing.Point(261, 89);
            this.SettlementTab.Name = "SettlementTab";
            this.SettlementTab.SelectedIndex = 0;
            this.SettlementTab.Size = new System.Drawing.Size(730, 381);
            this.SettlementTab.TabIndex = 5;
            // 
            // SettlementTabEmpInfo
            // 
            this.SettlementTabEmpInfo.Controls.Add(this.cboTransactionType);
            this.SettlementTabEmpInfo.Controls.Add(this.label14);
            this.SettlementTabEmpInfo.Controls.Add(this.dtpChequeDate);
            this.SettlementTabEmpInfo.Controls.Add(this.DocumentReturnCheckBox);
            this.SettlementTabEmpInfo.Controls.Add(lblCheqNo);
            this.SettlementTabEmpInfo.Controls.Add(this.groupBox2);
            this.SettlementTabEmpInfo.Controls.Add(lblCheqDate);
            this.SettlementTabEmpInfo.Controls.Add(this.txtChequeNo);
            this.SettlementTabEmpInfo.Controls.Add(this.DtpFromDate);
            this.SettlementTabEmpInfo.Controls.Add(this.Label3);
            this.SettlementTabEmpInfo.Controls.Add(this.lblSettlementPolicy);
            this.SettlementTabEmpInfo.Controls.Add(this.lblFromDate);
            this.SettlementTabEmpInfo.Controls.Add(this.BtnProcess);
            this.SettlementTabEmpInfo.Controls.Add(this.LblType);
            this.SettlementTabEmpInfo.Controls.Add(this.CboType);
            this.SettlementTabEmpInfo.Location = new System.Drawing.Point(4, 22);
            this.SettlementTabEmpInfo.Name = "SettlementTabEmpInfo";
            this.SettlementTabEmpInfo.Padding = new System.Windows.Forms.Padding(3);
            this.SettlementTabEmpInfo.Size = new System.Drawing.Size(722, 355);
            this.SettlementTabEmpInfo.TabIndex = 0;
            this.SettlementTabEmpInfo.Text = "Employee Info";
            this.SettlementTabEmpInfo.UseVisualStyleBackColor = true;
            // 
            // cboTransactionType
            // 
            this.cboTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTransactionType.BackColor = System.Drawing.SystemColors.Info;
            this.cboTransactionType.DropDownHeight = 134;
            this.cboTransactionType.FormattingEnabled = true;
            this.cboTransactionType.IntegralHeight = false;
            this.cboTransactionType.Location = new System.Drawing.Point(107, 150);
            this.cboTransactionType.Name = "cboTransactionType";
            this.cboTransactionType.Size = new System.Drawing.Size(106, 21);
            this.cboTransactionType.TabIndex = 3;
            this.cboTransactionType.SelectedIndexChanged += new System.EventHandler(this.cboTransactionType_SelectedIndexChanged);
            this.cboTransactionType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 154);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Trans: Type";
            // 
            // dtpChequeDate
            // 
            this.dtpChequeDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpChequeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpChequeDate.Location = new System.Drawing.Point(107, 238);
            this.dtpChequeDate.Name = "dtpChequeDate";
            this.dtpChequeDate.Size = new System.Drawing.Size(106, 20);
            this.dtpChequeDate.TabIndex = 5;
            this.dtpChequeDate.ValueChanged += new System.EventHandler(this.dtpChequeDate_ValueChanged);
            // 
            // DocumentReturnCheckBox
            // 
            this.DocumentReturnCheckBox.Location = new System.Drawing.Point(12, 297);
            this.DocumentReturnCheckBox.Name = "DocumentReturnCheckBox";
            this.DocumentReturnCheckBox.Size = new System.Drawing.Size(112, 18);
            this.DocumentReturnCheckBox.TabIndex = 6;
            this.DocumentReturnCheckBox.Text = "Document Return";
            this.DocumentReturnCheckBox.UseVisualStyleBackColor = true;
            this.DocumentReturnCheckBox.CheckedChanged += new System.EventHandler(this.DocumentReturnCheckBox_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Label9);
            this.groupBox2.Controls.Add(this.chkAbsent);
            this.groupBox2.Controls.Add(this.txtAbsentDays);
            this.groupBox2.Controls.Add(this.txtLeavePayDays);
            this.groupBox2.Controls.Add(this.Label10);
            this.groupBox2.Controls.Add(this.lblEligibleLeavePayDays);
            this.groupBox2.Controls.Add(this.lblTakenLeavePayDays);
            this.groupBox2.Controls.Add(this.Label11);
            this.groupBox2.Controls.Add(Label12);
            this.groupBox2.Controls.Add(this.txtEligibleDays);
            this.groupBox2.Controls.Add(this.txtExcludeHolidays);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.chkExcludeHolidays);
            this.groupBox2.Controls.Add(Label4);
            this.groupBox2.Controls.Add(this.GrdLeaveType);
            this.groupBox2.Controls.Add(this.txtNoticePeriod);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtNofDaysExperience);
            this.groupBox2.Controls.Add(this.Label13);
            this.groupBox2.Controls.Add(this.lblNoofMonths);
            this.groupBox2.Controls.Add(this.Label8);
            this.groupBox2.Controls.Add(label16);
            this.groupBox2.Controls.Add(this.shapeContainer2);
            this.groupBox2.Controls.Add(this.txtExcludeLeaveDays);
            this.groupBox2.Location = new System.Drawing.Point(232, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(482, 310);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            // 
            // txtEligibleDays
            // 
            this.txtEligibleDays.Location = new System.Drawing.Point(118, 97);
            this.txtEligibleDays.MaxLength = 9;
            this.txtEligibleDays.Name = "txtEligibleDays";
            this.txtEligibleDays.Size = new System.Drawing.Size(106, 20);
            this.txtEligibleDays.TabIndex = 3;
            this.txtEligibleDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtExcludeHolidays
            // 
            this.txtExcludeHolidays.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtExcludeHolidays.Location = new System.Drawing.Point(368, 237);
            this.txtExcludeHolidays.MaxLength = 9;
            this.txtExcludeHolidays.Name = "txtExcludeHolidays";
            this.txtExcludeHolidays.Size = new System.Drawing.Size(84, 13);
            this.txtExcludeHolidays.TabIndex = 8;
            this.txtExcludeHolidays.Text = "NIL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Eligible Days";
            // 
            // chkExcludeHolidays
            // 
            this.chkExcludeHolidays.AutoSize = true;
            this.chkExcludeHolidays.Location = new System.Drawing.Point(255, 235);
            this.chkExcludeHolidays.Name = "chkExcludeHolidays";
            this.chkExcludeHolidays.Size = new System.Drawing.Size(107, 17);
            this.chkExcludeHolidays.TabIndex = 19;
            this.chkExcludeHolidays.Text = "Exclude Holidays";
            this.chkExcludeHolidays.UseVisualStyleBackColor = true;
            this.chkExcludeHolidays.CheckedChanged += new System.EventHandler(this.chkExcludeHolidays_CheckedChanged);
            // 
            // GrdLeaveType
            // 
            this.GrdLeaveType.AllowUserToAddRows = false;
            this.GrdLeaveType.AllowUserToDeleteRows = false;
            this.GrdLeaveType.AllowUserToResizeColumns = false;
            this.GrdLeaveType.AllowUserToResizeRows = false;
            this.GrdLeaveType.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.GrdLeaveType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdLeaveType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Select,
            this.LeaveTypeID,
            this.LeaveType,
            this.Days});
            this.GrdLeaveType.GridColor = System.Drawing.SystemColors.GrayText;
            this.GrdLeaveType.Location = new System.Drawing.Point(255, 36);
            this.GrdLeaveType.Name = "GrdLeaveType";
            this.GrdLeaveType.RowHeadersVisible = false;
            this.GrdLeaveType.Size = new System.Drawing.Size(220, 177);
            this.GrdLeaveType.TabIndex = 9;
            this.GrdLeaveType.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrdLeaveType_CellValueChanged);
            this.GrdLeaveType.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.GrdLeaveType_EditingControlShowing);
            this.GrdLeaveType.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.GrdLeaveType_DataError);
            // 
            // Select
            // 
            this.Select.HeaderText = "Select";
            this.Select.Name = "Select";
            this.Select.Width = 40;
            // 
            // LeaveTypeID
            // 
            this.LeaveTypeID.HeaderText = "LeaveTypeID";
            this.LeaveTypeID.Name = "LeaveTypeID";
            this.LeaveTypeID.Visible = false;
            // 
            // LeaveType
            // 
            this.LeaveType.HeaderText = "Exclude";
            this.LeaveType.Name = "LeaveType";
            // 
            // Days
            // 
            this.Days.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Days.HeaderText = "Days";
            this.Days.Name = "Days";
            // 
            // txtNoticePeriod
            // 
            this.txtNoticePeriod.Location = new System.Drawing.Point(118, 128);
            this.txtNoticePeriod.MaxLength = 9;
            this.txtNoticePeriod.Name = "txtNoticePeriod";
            this.txtNoticePeriod.Size = new System.Drawing.Size(106, 20);
            this.txtNoticePeriod.TabIndex = 4;
            this.txtNoticePeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoticePeriod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimal_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 132);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Notice(Days)";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3,
            this.lineShape2,
            this.lineShape4});
            this.shapeContainer2.Size = new System.Drawing.Size(476, 291);
            this.shapeContainer2.TabIndex = 26;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.Color.LightGray;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 31;
            this.lineShape3.X2 = 229;
            this.lineShape3.Y1 = 157;
            this.lineShape3.Y2 = 157;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.Color.LightGray;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 284;
            this.lineShape2.X2 = 475;
            this.lineShape2.Y1 = 4;
            this.lineShape2.Y2 = 4;
            // 
            // lineShape4
            // 
            this.lineShape4.BorderColor = System.Drawing.Color.LightGray;
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 53;
            this.lineShape4.X2 = 230;
            this.lineShape4.Y1 = 5;
            this.lineShape4.Y2 = 5;
            // 
            // txtExcludeLeaveDays
            // 
            this.txtExcludeLeaveDays.Location = new System.Drawing.Point(263, -47);
            this.txtExcludeLeaveDays.MaxLength = 9;
            this.txtExcludeLeaveDays.Name = "txtExcludeLeaveDays";
            this.txtExcludeLeaveDays.Size = new System.Drawing.Size(68, 20);
            this.txtExcludeLeaveDays.TabIndex = 0;
            this.txtExcludeLeaveDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtExcludeLeaveDays.Visible = false;
            // 
            // txtChequeNo
            // 
            this.txtChequeNo.Location = new System.Drawing.Point(107, 194);
            this.txtChequeNo.MaxLength = 99;
            this.txtChequeNo.Name = "txtChequeNo";
            this.txtChequeNo.Size = new System.Drawing.Size(106, 20);
            this.txtChequeNo.TabIndex = 4;
            this.txtChequeNo.TextChanged += new System.EventHandler(this.txtChequeNo_TextChanged);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Location = new System.Drawing.Point(638, 324);
            this.BtnProcess.Name = "BtnProcess";
            this.BtnProcess.Size = new System.Drawing.Size(80, 25);
            this.BtnProcess.TabIndex = 6;
            this.BtnProcess.Text = "Process";
            this.BtnProcess.UseVisualStyleBackColor = true;
            this.BtnProcess.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // LblType
            // 
            this.LblType.AutoSize = true;
            this.LblType.Location = new System.Drawing.Point(9, 66);
            this.LblType.Name = "LblType";
            this.LblType.Size = new System.Drawing.Size(84, 13);
            this.LblType.TabIndex = 1;
            this.LblType.Text = "Settlement Type";
            // 
            // CboType
            // 
            this.CboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboType.BackColor = System.Drawing.SystemColors.Info;
            this.CboType.DropDownHeight = 134;
            this.CboType.FormattingEnabled = true;
            this.CboType.IntegralHeight = false;
            this.CboType.Location = new System.Drawing.Point(107, 62);
            this.CboType.Name = "CboType";
            this.CboType.Size = new System.Drawing.Size(106, 21);
            this.CboType.TabIndex = 1;
            this.CboType.SelectedIndexChanged += new System.EventHandler(this.CboType_SelectedIndexChanged);
            this.CboType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // SettlementTabParticulars
            // 
            this.SettlementTabParticulars.Controls.Add(lblParticulars);
            this.SettlementTabParticulars.Controls.Add(this.btnAdditionDeductions);
            this.SettlementTabParticulars.Controls.Add(this.cboParticulars);
            this.SettlementTabParticulars.Controls.Add(this.EmployeeSettlementDetailDataGridView);
            this.SettlementTabParticulars.Controls.Add(this.RemarksTextBox1);
            this.SettlementTabParticulars.Controls.Add(this.Label7);
            this.SettlementTabParticulars.Controls.Add(LblDeduction);
            this.SettlementTabParticulars.Controls.Add(LblEarning);
            this.SettlementTabParticulars.Controls.Add(this.LblCurrency);
            this.SettlementTabParticulars.Controls.Add(Label5);
            this.SettlementTabParticulars.Controls.Add(this.AddBtn);
            this.SettlementTabParticulars.Controls.Add(NetAmountLabel);
            this.SettlementTabParticulars.Controls.Add(this.txtAmount);
            this.SettlementTabParticulars.Controls.Add(this.NetAmountTextBox);
            this.SettlementTabParticulars.Controls.Add(this.TxtTotalDeduction);
            this.SettlementTabParticulars.Controls.Add(this.TxtTotalEarning);
            this.SettlementTabParticulars.Location = new System.Drawing.Point(4, 22);
            this.SettlementTabParticulars.Name = "SettlementTabParticulars";
            this.SettlementTabParticulars.Padding = new System.Windows.Forms.Padding(3);
            this.SettlementTabParticulars.Size = new System.Drawing.Size(722, 355);
            this.SettlementTabParticulars.TabIndex = 1;
            this.SettlementTabParticulars.Text = "Particulars";
            this.SettlementTabParticulars.UseVisualStyleBackColor = true;
            // 
            // btnAdditionDeductions
            // 
            this.btnAdditionDeductions.Location = new System.Drawing.Point(307, 3);
            this.btnAdditionDeductions.Name = "btnAdditionDeductions";
            this.btnAdditionDeductions.Size = new System.Drawing.Size(30, 24);
            this.btnAdditionDeductions.TabIndex = 210;
            this.btnAdditionDeductions.Text = "...";
            this.btnAdditionDeductions.UseVisualStyleBackColor = true;
            this.btnAdditionDeductions.Click += new System.EventHandler(this.btnAdditionDeductions_Click);
            // 
            // cboParticulars
            // 
            this.cboParticulars.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboParticulars.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboParticulars.BackColor = System.Drawing.Color.White;
            this.cboParticulars.DropDownHeight = 134;
            this.cboParticulars.FormattingEnabled = true;
            this.cboParticulars.IntegralHeight = false;
            this.cboParticulars.Location = new System.Drawing.Point(63, 5);
            this.cboParticulars.Name = "cboParticulars";
            this.cboParticulars.Size = new System.Drawing.Size(238, 21);
            this.cboParticulars.TabIndex = 209;
            this.cboParticulars.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboParticulars_KeyDown);
            // 
            // EmployeeSettlementDetailDataGridView
            // 
            this.EmployeeSettlementDetailDataGridView.AllowUserToAddRows = false;
            this.EmployeeSettlementDetailDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.EmployeeSettlementDetailDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.EmployeeSettlementDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EmployeeSettlementDetailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmployeeSettlementDetailID,
            this.Particulars,
            this.Credit,
            this.Debit,
            this.RepaymentID,
            this.LoanID,
            this.IsAddition,
            this.Flag,
            this.SalaryPaymentID});
            this.EmployeeSettlementDetailDataGridView.Location = new System.Drawing.Point(0, 32);
            this.EmployeeSettlementDetailDataGridView.MultiSelect = false;
            this.EmployeeSettlementDetailDataGridView.Name = "EmployeeSettlementDetailDataGridView";
            this.EmployeeSettlementDetailDataGridView.RowHeadersWidth = 25;
            this.EmployeeSettlementDetailDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.EmployeeSettlementDetailDataGridView.Size = new System.Drawing.Size(719, 259);
            this.EmployeeSettlementDetailDataGridView.TabIndex = 5;
            this.EmployeeSettlementDetailDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.EmployeeSettlementDetailDataGridView_CellValueChanged);
            this.EmployeeSettlementDetailDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.EmployeeSettlementDetailDataGridView_CellBeginEdit);
            this.EmployeeSettlementDetailDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.EmployeeSettlementDetailDataGridView_EditingControlShowing);
            this.EmployeeSettlementDetailDataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.EmployeeSettlementDetailDataGridView_CurrentCellDirtyStateChanged);
            this.EmployeeSettlementDetailDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.EmployeeSettlementDetailDataGridView_DataError);
            this.EmployeeSettlementDetailDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EmployeeSettlementDetailDataGridView_KeyDown);
            this.EmployeeSettlementDetailDataGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.EmployeeSettlementDetailDataGridView_RowsRemoved);
            // 
            // EmployeeSettlementDetailID
            // 
            this.EmployeeSettlementDetailID.HeaderText = "EmployeeSettlementDetailID";
            this.EmployeeSettlementDetailID.Name = "EmployeeSettlementDetailID";
            this.EmployeeSettlementDetailID.Visible = false;
            // 
            // Particulars
            // 
            this.Particulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Particulars.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.Particulars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Particulars.HeaderText = "Particulars";
            this.Particulars.Name = "Particulars";
            this.Particulars.ReadOnly = true;
            this.Particulars.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Particulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Credit
            // 
            this.Credit.HeaderText = "Credit";
            this.Credit.MaxInputLength = 13;
            this.Credit.Name = "Credit";
            // 
            // Debit
            // 
            this.Debit.HeaderText = "Debit";
            this.Debit.MaxInputLength = 13;
            this.Debit.Name = "Debit";
            // 
            // RepaymentID
            // 
            this.RepaymentID.HeaderText = "RepaymentID";
            this.RepaymentID.Name = "RepaymentID";
            this.RepaymentID.Visible = false;
            // 
            // LoanID
            // 
            this.LoanID.HeaderText = "LoanID";
            this.LoanID.Name = "LoanID";
            this.LoanID.Visible = false;
            // 
            // IsAddition
            // 
            this.IsAddition.HeaderText = "IsAddition";
            this.IsAddition.Name = "IsAddition";
            this.IsAddition.Visible = false;
            // 
            // Flag
            // 
            this.Flag.HeaderText = "Flag";
            this.Flag.Name = "Flag";
            this.Flag.Visible = false;
            // 
            // SalaryPaymentID
            // 
            this.SalaryPaymentID.HeaderText = "SalaryPaymentID";
            this.SalaryPaymentID.Name = "SalaryPaymentID";
            this.SalaryPaymentID.Visible = false;
            // 
            // LblCurrency
            // 
            this.LblCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblCurrency.AutoSize = true;
            this.LblCurrency.Location = new System.Drawing.Point(566, 331);
            this.LblCurrency.Name = "LblCurrency";
            this.LblCurrency.Size = new System.Drawing.Size(0, 13);
            this.LblCurrency.TabIndex = 9;
            // 
            // NetAmountTextBox
            // 
            this.NetAmountTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.NetAmountTextBox.Enabled = false;
            this.NetAmountTextBox.Location = new System.Drawing.Point(567, 329);
            this.NetAmountTextBox.Name = "NetAmountTextBox";
            this.NetAmountTextBox.Size = new System.Drawing.Size(147, 20);
            this.NetAmountTextBox.TabIndex = 22;
            this.NetAmountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotalDeduction
            // 
            this.TxtTotalDeduction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotalDeduction.Enabled = false;
            this.TxtTotalDeduction.Location = new System.Drawing.Point(567, 297);
            this.TxtTotalDeduction.Name = "TxtTotalDeduction";
            this.TxtTotalDeduction.Size = new System.Drawing.Size(147, 20);
            this.TxtTotalDeduction.TabIndex = 7;
            this.TxtTotalDeduction.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtTotalEarning
            // 
            this.TxtTotalEarning.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTotalEarning.Enabled = false;
            this.TxtTotalEarning.Location = new System.Drawing.Point(414, 297);
            this.TxtTotalEarning.Name = "TxtTotalEarning";
            this.TxtTotalEarning.Size = new System.Drawing.Size(147, 20);
            this.TxtTotalEarning.TabIndex = 6;
            this.TxtTotalEarning.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SettlementTabHistory
            // 
            this.SettlementTabHistory.Controls.Add(this.DtGrdEmpHistory);
            this.SettlementTabHistory.Location = new System.Drawing.Point(4, 22);
            this.SettlementTabHistory.Name = "SettlementTabHistory";
            this.SettlementTabHistory.Padding = new System.Windows.Forms.Padding(3);
            this.SettlementTabHistory.Size = new System.Drawing.Size(722, 355);
            this.SettlementTabHistory.TabIndex = 2;
            this.SettlementTabHistory.Text = "History";
            this.SettlementTabHistory.UseVisualStyleBackColor = true;
            // 
            // DtGrdEmpHistory
            // 
            this.DtGrdEmpHistory.AllowUserToAddRows = false;
            this.DtGrdEmpHistory.BackgroundColor = System.Drawing.Color.White;
            this.DtGrdEmpHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtGrdEmpHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColDescription,
            this.ColFromDate,
            this.ColToDate,
            this.ColAmount});
            this.DtGrdEmpHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DtGrdEmpHistory.Location = new System.Drawing.Point(3, 3);
            this.DtGrdEmpHistory.Name = "DtGrdEmpHistory";
            this.DtGrdEmpHistory.ReadOnly = true;
            this.DtGrdEmpHistory.RowHeadersVisible = false;
            this.DtGrdEmpHistory.Size = new System.Drawing.Size(716, 349);
            this.DtGrdEmpHistory.TabIndex = 24;
            // 
            // ColDescription
            // 
            this.ColDescription.HeaderText = "Description";
            this.ColDescription.Name = "ColDescription";
            this.ColDescription.ReadOnly = true;
            this.ColDescription.Width = 175;
            // 
            // ColFromDate
            // 
            this.ColFromDate.HeaderText = "From Date";
            this.ColFromDate.Name = "ColFromDate";
            this.ColFromDate.ReadOnly = true;
            this.ColFromDate.Width = 90;
            // 
            // ColToDate
            // 
            this.ColToDate.HeaderText = "To Date";
            this.ColToDate.Name = "ColToDate";
            this.ColToDate.ReadOnly = true;
            this.ColToDate.Width = 90;
            // 
            // ColAmount
            // 
            this.ColAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColAmount.HeaderText = "Amount";
            this.ColAmount.Name = "ColAmount";
            this.ColAmount.ReadOnly = true;
            // 
            // GratuityTab
            // 
            this.GratuityTab.Controls.Add(this.DtGrdGratuity);
            this.GratuityTab.Location = new System.Drawing.Point(4, 22);
            this.GratuityTab.Name = "GratuityTab";
            this.GratuityTab.Padding = new System.Windows.Forms.Padding(3);
            this.GratuityTab.Size = new System.Drawing.Size(722, 355);
            this.GratuityTab.TabIndex = 3;
            this.GratuityTab.Text = "Gratuity";
            this.GratuityTab.UseVisualStyleBackColor = true;
            // 
            // DtGrdGratuity
            // 
            this.DtGrdGratuity.AllowUserToAddRows = false;
            this.DtGrdGratuity.BackgroundColor = System.Drawing.Color.White;
            this.DtGrdGratuity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtGrdGratuity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColYears,
            this.ColCalDays,
            this.ColGRTYDays,
            this.PerDayGratuity,
            this.ColGratuityAmt});
            this.DtGrdGratuity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DtGrdGratuity.Location = new System.Drawing.Point(3, 3);
            this.DtGrdGratuity.Name = "DtGrdGratuity";
            this.DtGrdGratuity.ReadOnly = true;
            this.DtGrdGratuity.RowHeadersVisible = false;
            this.DtGrdGratuity.Size = new System.Drawing.Size(716, 349);
            this.DtGrdGratuity.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(532, 64);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 213;
            this.label17.Text = "Last Working";
            // 
            // DtpLastWorkingDate
            // 
            this.DtpLastWorkingDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpLastWorkingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpLastWorkingDate.Location = new System.Drawing.Point(625, 60);
            this.DtpLastWorkingDate.Name = "DtpLastWorkingDate";
            this.DtpLastWorkingDate.Size = new System.Drawing.Size(106, 20);
            this.DtpLastWorkingDate.TabIndex = 3;
            this.DtpLastWorkingDate.ValueChanged += new System.EventHandler(this.DtpToDate_ValueChanged);
            // 
            // dtpResignationDate
            // 
            this.dtpResignationDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpResignationDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpResignationDate.Location = new System.Drawing.Point(395, 60);
            this.dtpResignationDate.Name = "dtpResignationDate";
            this.dtpResignationDate.Size = new System.Drawing.Size(106, 20);
            this.dtpResignationDate.TabIndex = 2;
            this.dtpResignationDate.ValueChanged += new System.EventHandler(this.dtpResignationDate_ValueChanged);
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.Location = new System.Drawing.Point(281, 64);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(89, 13);
            this.lblToDate.TabIndex = 3;
            this.lblToDate.Text = "Resignation Date";
            // 
            // LblEmployee
            // 
            this.LblEmployee.AutoSize = true;
            this.LblEmployee.Location = new System.Drawing.Point(281, 37);
            this.LblEmployee.Name = "LblEmployee";
            this.LblEmployee.Size = new System.Drawing.Size(53, 13);
            this.LblEmployee.TabIndex = 0;
            this.LblEmployee.Text = "Employee";
            // 
            // DateDateTimePicker
            // 
            this.DateDateTimePicker.CustomFormat = "dd-MMM-yyyy";
            this.DateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateDateTimePicker.Location = new System.Drawing.Point(692, 149);
            this.DateDateTimePicker.Name = "DateDateTimePicker";
            this.DateDateTimePicker.Size = new System.Drawing.Size(106, 20);
            this.DateDateTimePicker.TabIndex = 6;
            this.DateDateTimePicker.Visible = false;
            this.DateDateTimePicker.ValueChanged += new System.EventHandler(this.DateDateTimePicker_ValueChanged);
            // 
            // TextBoxNumeric
            // 
            this.TextBoxNumeric.Location = new System.Drawing.Point(1000, 272);
            this.TextBoxNumeric.Name = "TextBoxNumeric";
            this.TextBoxNumeric.Size = new System.Drawing.Size(110, 20);
            this.TextBoxNumeric.TabIndex = 190;
            this.TextBoxNumeric.Visible = false;
            // 
            // EmployeeSettlementDetailBindingSource
            // 
            this.EmployeeSettlementDetailBindingSource.DataMember = "EmployeeSettlementDetail";
            // 
            // BtnSave
            // 
            this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnSave.Location = new System.Drawing.Point(265, 476);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(60, 23);
            this.BtnSave.TabIndex = 6;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // lblPayBack
            // 
            this.lblPayBack.AutoSize = true;
            this.lblPayBack.Location = new System.Drawing.Point(118, 531);
            this.lblPayBack.Name = "lblPayBack";
            this.lblPayBack.Size = new System.Drawing.Size(0, 13);
            this.lblPayBack.TabIndex = 173;
            this.lblPayBack.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "EmployeeSettlementDetailID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Particulars";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Credit";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Debit";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "RepaymentID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "LoanID";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "IsAddition";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Flag";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "SalaryPaymentID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // expLeft
            // 
            this.expLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.expLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expLeft.Controls.Add(this.dgvSearch);
            this.expLeft.Controls.Add(this.pnlTopSearch);
            this.expLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.expLeft.ExpandButtonVisible = false;
            this.expLeft.Location = new System.Drawing.Point(0, 0);
            this.expLeft.Name = "expLeft";
            this.expLeft.Size = new System.Drawing.Size(258, 502);
            this.expLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expLeft.Style.GradientAngle = 90;
            this.expLeft.TabIndex = 192;
            this.expLeft.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expLeft.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expLeft.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expLeft.TitleStyle.GradientAngle = 90;
            this.expLeft.TitleText = "Search";
            // 
            // dgvSearch
            // 
            this.dgvSearch.AllowUserToAddRows = false;
            this.dgvSearch.AllowUserToDeleteRows = false;
            this.dgvSearch.AllowUserToResizeColumns = false;
            this.dgvSearch.AllowUserToResizeRows = false;
            this.dgvSearch.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSearch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SettlementID,
            this.EmployeeID,
            this.Employee,
            this.EmployeeArb,
            this.Date});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSearch.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSearch.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSearch.Location = new System.Drawing.Point(0, 165);
            this.dgvSearch.Name = "dgvSearch";
            this.dgvSearch.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSearch.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvSearch.RowHeadersVisible = false;
            this.dgvSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSearch.Size = new System.Drawing.Size(258, 337);
            this.dgvSearch.TabIndex = 1;
            this.dgvSearch.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSearch_CellDoubleClick);
            // 
            // SettlementID
            // 
            this.SettlementID.DataPropertyName = "SettlementID";
            this.SettlementID.HeaderText = "SettlementID";
            this.SettlementID.Name = "SettlementID";
            this.SettlementID.ReadOnly = true;
            this.SettlementID.Visible = false;
            // 
            // EmployeeID
            // 
            this.EmployeeID.DataPropertyName = "EmployeeID";
            this.EmployeeID.HeaderText = "EmployeeID";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.ReadOnly = true;
            this.EmployeeID.Visible = false;
            // 
            // Employee
            // 
            this.Employee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Employee.DataPropertyName = "Employee";
            this.Employee.HeaderText = "Employee";
            this.Employee.Name = "Employee";
            this.Employee.ReadOnly = true;
            // 
            // EmployeeArb
            // 
            this.EmployeeArb.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EmployeeArb.DataPropertyName = "EmployeeArb";
            this.EmployeeArb.HeaderText = "EmployeeArb";
            this.EmployeeArb.Name = "EmployeeArb";
            this.EmployeeArb.ReadOnly = true;
            this.EmployeeArb.Visible = false;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "Date";
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // pnlTopSearch
            // 
            this.pnlTopSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlTopSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlTopSearch.Controls.Add(this.btnSearch);
            this.pnlTopSearch.Controls.Add(this.cboSearchEmployee);
            this.pnlTopSearch.Controls.Add(this.lblSearchEmployee);
            this.pnlTopSearch.Controls.Add(this.lblEmployeeCode);
            this.pnlTopSearch.Controls.Add(this.cboSearchCode);
            this.pnlTopSearch.Controls.Add(this.lblSearchTo);
            this.pnlTopSearch.Controls.Add(this.lblSearchFrom);
            this.pnlTopSearch.Controls.Add(this.dtpSearchToDate);
            this.pnlTopSearch.Controls.Add(this.dtpSearchFromDate);
            this.pnlTopSearch.Controls.Add(this.lblSearchCompany);
            this.pnlTopSearch.Controls.Add(this.cboSearchCompany);
            this.pnlTopSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopSearch.Location = new System.Drawing.Point(0, 26);
            this.pnlTopSearch.Name = "pnlTopSearch";
            this.pnlTopSearch.Size = new System.Drawing.Size(258, 139);
            this.pnlTopSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlTopSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlTopSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlTopSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlTopSearch.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlTopSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlTopSearch.Style.GradientAngle = 90;
            this.pnlTopSearch.TabIndex = 2;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(190, 111);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(60, 23);
            this.btnSearch.TabIndex = 223;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cboSearchEmployee
            // 
            this.cboSearchEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchEmployee.DropDownHeight = 134;
            this.cboSearchEmployee.FormattingEnabled = true;
            this.cboSearchEmployee.IntegralHeight = false;
            this.cboSearchEmployee.Location = new System.Drawing.Point(67, 59);
            this.cboSearchEmployee.Name = "cboSearchEmployee";
            this.cboSearchEmployee.Size = new System.Drawing.Size(183, 21);
            this.cboSearchEmployee.TabIndex = 222;
            this.cboSearchEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // lblSearchEmployee
            // 
            this.lblSearchEmployee.AutoSize = true;
            this.lblSearchEmployee.Location = new System.Drawing.Point(8, 63);
            this.lblSearchEmployee.Name = "lblSearchEmployee";
            this.lblSearchEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblSearchEmployee.TabIndex = 221;
            this.lblSearchEmployee.Text = "Employee";
            // 
            // lblEmployeeCode
            // 
            this.lblEmployeeCode.AutoSize = true;
            this.lblEmployeeCode.Location = new System.Drawing.Point(8, 36);
            this.lblEmployeeCode.Name = "lblEmployeeCode";
            this.lblEmployeeCode.Size = new System.Drawing.Size(32, 13);
            this.lblEmployeeCode.TabIndex = 220;
            this.lblEmployeeCode.Text = "Code";
            // 
            // cboSearchCode
            // 
            this.cboSearchCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchCode.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchCode.DropDownHeight = 134;
            this.cboSearchCode.FormattingEnabled = true;
            this.cboSearchCode.IntegralHeight = false;
            this.cboSearchCode.Location = new System.Drawing.Point(67, 32);
            this.cboSearchCode.Name = "cboSearchCode";
            this.cboSearchCode.Size = new System.Drawing.Size(183, 21);
            this.cboSearchCode.TabIndex = 219;
            this.cboSearchCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // lblSearchTo
            // 
            this.lblSearchTo.AutoSize = true;
            this.lblSearchTo.Location = new System.Drawing.Point(8, 116);
            this.lblSearchTo.Name = "lblSearchTo";
            this.lblSearchTo.Size = new System.Drawing.Size(20, 13);
            this.lblSearchTo.TabIndex = 218;
            this.lblSearchTo.Text = "To";
            // 
            // lblSearchFrom
            // 
            this.lblSearchFrom.AutoSize = true;
            this.lblSearchFrom.Location = new System.Drawing.Point(8, 90);
            this.lblSearchFrom.Name = "lblSearchFrom";
            this.lblSearchFrom.Size = new System.Drawing.Size(30, 13);
            this.lblSearchFrom.TabIndex = 217;
            this.lblSearchFrom.Text = "From";
            // 
            // dtpSearchToDate
            // 
            this.dtpSearchToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpSearchToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSearchToDate.Location = new System.Drawing.Point(67, 112);
            this.dtpSearchToDate.Name = "dtpSearchToDate";
            this.dtpSearchToDate.Size = new System.Drawing.Size(106, 20);
            this.dtpSearchToDate.TabIndex = 216;
            // 
            // dtpSearchFromDate
            // 
            this.dtpSearchFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpSearchFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSearchFromDate.Location = new System.Drawing.Point(67, 86);
            this.dtpSearchFromDate.Name = "dtpSearchFromDate";
            this.dtpSearchFromDate.Size = new System.Drawing.Size(106, 20);
            this.dtpSearchFromDate.TabIndex = 215;
            // 
            // lblSearchCompany
            // 
            this.lblSearchCompany.AutoSize = true;
            this.lblSearchCompany.Location = new System.Drawing.Point(8, 9);
            this.lblSearchCompany.Name = "lblSearchCompany";
            this.lblSearchCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSearchCompany.TabIndex = 3;
            this.lblSearchCompany.Text = "Company";
            // 
            // cboSearchCompany
            // 
            this.cboSearchCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchCompany.DropDownHeight = 134;
            this.cboSearchCompany.FormattingEnabled = true;
            this.cboSearchCompany.IntegralHeight = false;
            this.cboSearchCompany.Location = new System.Drawing.Point(67, 5);
            this.cboSearchCompany.Name = "cboSearchCompany";
            this.cboSearchCompany.Size = new System.Drawing.Size(183, 21);
            this.cboSearchCompany.TabIndex = 2;
            this.cboSearchCompany.SelectedIndexChanged += new System.EventHandler(this.cboSearchCompany_SelectedIndexChanged);
            this.cboSearchCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(746, 59);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(60, 23);
            this.btnShow.TabIndex = 4;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // gmajilgmai
            // 
            this.gmajilgmai.Location = new System.Drawing.Point(815, 63);
            this.gmajilgmai.Name = "gmajilgmai";
            this.gmajilgmai.Size = new System.Drawing.Size(164, 14);
            this.gmajilgmai.TabIndex = 214;
            this.gmajilgmai.Visible = false;
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMain.Controls.Add(this.EmployeeSettlementBindingNavigator);
            this.pnlMain.Controls.Add(this.expLeft);
            this.pnlMain.Controls.Add(this.BtnBottomCancel);
            this.pnlMain.Controls.Add(this.gmajilgmai);
            this.pnlMain.Controls.Add(this.OKSaveButton);
            this.pnlMain.Controls.Add(this.BtnSave);
            this.pnlMain.Controls.Add(this.SettlementTab);
            this.pnlMain.Controls.Add(this.btnShow);
            this.pnlMain.Controls.Add(this.StatusStripEmployeeSettlement);
            this.pnlMain.Controls.Add(this.label17);
            this.pnlMain.Controls.Add(this.dtpResignationDate);
            this.pnlMain.Controls.Add(this.DtpLastWorkingDate);
            this.pnlMain.Controls.Add(this.LblEmployee);
            this.pnlMain.Controls.Add(this.CboEmployee);
            this.pnlMain.Controls.Add(this.lblToDate);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(994, 524);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 216;
            this.pnlMain.Text = "panelEx1";
            // 
            // ColYears
            // 
            this.ColYears.HeaderText = "Years";
            this.ColYears.Name = "ColYears";
            this.ColYears.ReadOnly = true;
            this.ColYears.Width = 250;
            // 
            // ColCalDays
            // 
            this.ColCalDays.HeaderText = "Cal Days";
            this.ColCalDays.Name = "ColCalDays";
            this.ColCalDays.ReadOnly = true;
            this.ColCalDays.Width = 80;
            // 
            // ColGRTYDays
            // 
            this.ColGRTYDays.HeaderText = "Gratuity Days";
            this.ColGRTYDays.Name = "ColGRTYDays";
            this.ColGRTYDays.ReadOnly = true;
            this.ColGRTYDays.Width = 80;
            // 
            // PerDayGratuity
            // 
            this.PerDayGratuity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PerDayGratuity.HeaderText = "Per Day Gratuity";
            this.PerDayGratuity.Name = "PerDayGratuity";
            this.PerDayGratuity.ReadOnly = true;
            // 
            // ColGratuityAmt
            // 
            this.ColGratuityAmt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColGratuityAmt.HeaderText = "Gratuity Amt";
            this.ColGratuityAmt.Name = "ColGratuityAmt";
            this.ColGratuityAmt.ReadOnly = true;
            // 
            // FrmSettlementEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 524);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(DateLabel);
            this.Controls.Add(this.lblPayBack);
            this.Controls.Add(this.TextBoxNumeric);
            this.Controls.Add(this.DateDateTimePicker);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSettlementEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settlement Process";
            this.Load += new System.EventHandler(this.FrmSettlementEntry_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSettlementEntry_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSettlementEntry_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementBindingNavigator)).EndInit();
            this.EmployeeSettlementBindingNavigator.ResumeLayout(false);
            this.EmployeeSettlementBindingNavigator.PerformLayout();
            this.StatusStripEmployeeSettlement.ResumeLayout(false);
            this.StatusStripEmployeeSettlement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployeeSettlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementBindingSourcerpt)).EndInit();
            this.SettlementTab.ResumeLayout(false);
            this.SettlementTabEmpInfo.ResumeLayout(false);
            this.SettlementTabEmpInfo.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdLeaveType)).EndInit();
            this.SettlementTabParticulars.ResumeLayout(false);
            this.SettlementTabParticulars.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementDetailDataGridView)).EndInit();
            this.SettlementTabHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtGrdEmpHistory)).EndInit();
            this.GratuityTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DtGrdGratuity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSettlementDetailBindingSource)).EndInit();
            this.expLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            this.pnlTopSearch.ResumeLayout(false);
            this.pnlTopSearch.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox txtAbsentDays;
        internal System.Windows.Forms.CheckBox chkAbsent;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.TextBox txtNofDaysExperience;
        internal System.Windows.Forms.Label lblTakenLeavePayDays;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton EmployeeSettlementBindingNavigatorSaveItem;
        internal System.Windows.Forms.Label lblEligibleLeavePayDays;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.BindingNavigator EmployeeSettlementBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BtnCancel;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.ToolStripStatusLabel LblEmployeeSettlement;
        internal System.Windows.Forms.StatusStrip StatusStripEmployeeSettlement;
        internal System.Windows.Forms.Button OKSaveButton;
        internal System.Windows.Forms.Timer TmrEmployeeSettlement;
        internal System.Windows.Forms.ErrorProvider ErrEmployeeSettlement;
        internal System.Windows.Forms.Button BtnBottomCancel;
        internal System.Windows.Forms.TextBox txtLeavePayDays;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.Label lblSettlementPolicy;
        internal System.Windows.Forms.Label lblFromDate;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.DateTimePicker DtpFromDate;
        internal System.Windows.Forms.Label lblNoofMonths;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.TextBox RemarksTextBox1;
        internal System.Windows.Forms.Button AddBtn;
        internal System.Windows.Forms.ComboBox CboEmployee;
        internal System.Windows.Forms.ComboBox CboType;
        internal System.Windows.Forms.DateTimePicker DateDateTimePicker;
        internal System.Windows.Forms.DateTimePicker DtpLastWorkingDate;
        internal System.Windows.Forms.Label LblType;
        internal System.Windows.Forms.Label LblEmployee;
        internal System.Windows.Forms.Label lblToDate;
        internal System.Windows.Forms.Label LblCurrency;
        internal System.Windows.Forms.CheckBox DocumentReturnCheckBox;
        internal System.Windows.Forms.Button BtnProcess;
        internal System.Windows.Forms.TextBox NetAmountTextBox;
        internal System.Windows.Forms.TextBox TxtTotalDeduction;
        internal System.Windows.Forms.TextBox TxtTotalEarning;
        internal System.Windows.Forms.TextBox TextBoxNumeric;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Label lblPayBack;
        internal System.Windows.Forms.BindingSource EmployeeSettlementBindingSourcerpt;
        internal System.Windows.Forms.BindingSource EmployeeSettlementDetailBindingSource;
        private System.Windows.Forms.DataGridView EmployeeSettlementDetailDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.TabControl SettlementTab;
        private System.Windows.Forms.TabPage SettlementTabEmpInfo;
        private System.Windows.Forms.TabPage SettlementTabParticulars;
        private System.Windows.Forms.TabPage SettlementTabHistory;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal System.Windows.Forms.ComboBox cboTransactionType;
        private System.Windows.Forms.Label label14;
        internal System.Windows.Forms.DateTimePicker dtpChequeDate;
        private System.Windows.Forms.TextBox txtChequeNo;
        private System.Windows.Forms.DataGridView DtGrdEmpHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFromDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColToDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAmount;
        private System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.CheckBox chkExcludeHolidays;
        internal System.Windows.Forms.TextBox txtEligibleDays;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox txtExcludeHolidays;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private System.Windows.Forms.TabPage GratuityTab;
        private System.Windows.Forms.DataGridView DtGrdGratuity;
        internal System.Windows.Forms.ToolStripButton BtnPrintRpt;
        private System.Windows.Forms.DataGridView GrdLeaveType;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Select;
        private System.Windows.Forms.DataGridViewTextBoxColumn LeaveTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LeaveType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Days;
        internal System.Windows.Forms.TextBox txtExcludeLeaveDays;
        internal System.Windows.Forms.TextBox txtNoticePeriod;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.DateTimePicker dtpResignationDate;
        internal System.Windows.Forms.Label label17;
        private DevComponents.DotNetBar.ExpandablePanel expLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSearch;
        private DevComponents.DotNetBar.PanelEx pnlTopSearch;
        internal System.Windows.Forms.Label lblEmployeeCode;
        internal System.Windows.Forms.ComboBox cboSearchCode;
        internal System.Windows.Forms.Label lblSearchTo;
        internal System.Windows.Forms.Label lblSearchFrom;
        internal System.Windows.Forms.DateTimePicker dtpSearchToDate;
        internal System.Windows.Forms.DateTimePicker dtpSearchFromDate;
        internal System.Windows.Forms.Label lblSearchCompany;
        internal System.Windows.Forms.ComboBox cboSearchCompany;
        internal System.Windows.Forms.ComboBox cboSearchEmployee;
        internal System.Windows.Forms.Label lblSearchEmployee;
        internal System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn SettlementID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Employee;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeArb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        internal System.Windows.Forms.Button btnShow;
        internal System.Windows.Forms.ToolStripSplitButton BtnPrint;
        private System.Windows.Forms.ToolStripMenuItem btnTemplate1;
        private System.Windows.Forms.ToolStripMenuItem btnTemplate2;
        private System.Windows.Forms.ToolStripMenuItem btnTemplate3;
        private System.Windows.Forms.ToolStripMenuItem btnTemplate4;
        private System.Windows.Forms.ProgressBar gmajilgmai;
        private DevComponents.DotNetBar.PanelEx pnlMain;
        internal System.Windows.Forms.Button btnAdditionDeductions;
        internal System.Windows.Forms.ComboBox cboParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeSettlementDetailID;
        private System.Windows.Forms.DataGridViewComboBoxColumn Particulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn Credit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Debit;
        private System.Windows.Forms.DataGridViewTextBoxColumn RepaymentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoanID;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsAddition;
        private System.Windows.Forms.DataGridViewTextBoxColumn Flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalaryPaymentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColYears;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCalDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColGRTYDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn PerDayGratuity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColGratuityAmt;
    }
}