﻿namespace MyPayfriend
{
    partial class FrmSalaryTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label11;
            System.Windows.Forms.Label Label5;
            System.Windows.Forms.Label Label6;
            System.Windows.Forms.Label Label7;
            System.Windows.Forms.Label label4;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalaryTemplate));
            this.label1 = new System.Windows.Forms.Label();
            this.PolicyBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BtnCancel = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.ToolStripbtnHelp = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnEncashPolicy = new System.Windows.Forms.Button();
            this.btnHolidayPolicy = new System.Windows.Forms.Button();
            this.btnOvertimePolicy = new System.Windows.Forms.Button();
            this.cboHolidayPolicy = new System.Windows.Forms.ComboBox();
            this.cboEncashPolicy = new System.Windows.Forms.ComboBox();
            this.btnAbsentPolicy = new System.Windows.Forms.Button();
            this.cboOvertimePolicy = new System.Windows.Forms.ComboBox();
            this.cboAbsentPolicy = new System.Windows.Forms.ComboBox();
            this.btnSettlement = new System.Windows.Forms.Button();
            this.cboSettlement = new System.Windows.Forms.ComboBox();
            this.tabAdditionDeduction = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.DgvSalaryTemp = new System.Windows.Forms.DataGridView();
            this.colParticulars = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.DgvDedSalTemplate = new System.Windows.Forms.DataGridView();
            this.colDedParticulars = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDedPolicy = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LblTotalPercentage = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnBottomCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.TxtName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Label11 = new System.Windows.Forms.Label();
            Label5 = new System.Windows.Forms.Label();
            Label6 = new System.Windows.Forms.Label();
            Label7 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyBindingNavigator)).BeginInit();
            this.PolicyBindingNavigator.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabAdditionDeduction.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSalaryTemp)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvDedSalTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // Label11
            // 
            Label11.AutoSize = true;
            Label11.Location = new System.Drawing.Point(5, 37);
            Label11.Name = "Label11";
            Label11.Size = new System.Drawing.Size(60, 13);
            Label11.TabIndex = 1047;
            Label11.Text = "Settlement ";
            // 
            // Label5
            // 
            Label5.AutoSize = true;
            Label5.Location = new System.Drawing.Point(275, 38);
            Label5.Name = "Label5";
            Label5.Size = new System.Drawing.Size(74, 13);
            Label5.TabIndex = 1057;
            Label5.Text = "Encash Policy";
            // 
            // Label6
            // 
            Label6.AutoSize = true;
            Label6.Location = new System.Drawing.Point(7, 86);
            Label6.Name = "Label6";
            Label6.Size = new System.Drawing.Size(73, 13);
            Label6.TabIndex = 1058;
            Label6.Text = "Holiday Policy";
            // 
            // Label7
            // 
            Label7.AutoSize = true;
            Label7.Location = new System.Drawing.Point(275, 12);
            Label7.Name = "Label7";
            Label7.Size = new System.Drawing.Size(80, 13);
            Label7.TabIndex = 1059;
            Label7.Text = "Overtime Policy";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(7, 63);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(71, 13);
            label4.TabIndex = 1056;
            label4.Text = "Absent Policy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(193, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // PolicyBindingNavigator
            // 
            this.PolicyBindingNavigator.AddNewItem = null;
            this.PolicyBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.PolicyBindingNavigator.DeleteItem = null;
            this.PolicyBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorSaveItem,
            this.BtnCancel,
            this.ToolStripSeparator1,
            this.BtnPrint,
            this.ToolStripbtnHelp});
            this.PolicyBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.PolicyBindingNavigator.MoveFirstItem = null;
            this.PolicyBindingNavigator.MoveLastItem = null;
            this.PolicyBindingNavigator.MoveNextItem = null;
            this.PolicyBindingNavigator.MovePreviousItem = null;
            this.PolicyBindingNavigator.Name = "PolicyBindingNavigator";
            this.PolicyBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.PolicyBindingNavigator.Size = new System.Drawing.Size(567, 25);
            this.PolicyBindingNavigator.TabIndex = 46;
            this.PolicyBindingNavigator.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add new";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(23, 22);
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.ToolTipText = "Clear";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "ToolStripButton1";
            this.BtnPrint.ToolTipText = "Print";
            this.BtnPrint.Visible = false;
            // 
            // ToolStripbtnHelp
            // 
            this.ToolStripbtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripbtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripbtnHelp.Image")));
            this.ToolStripbtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripbtnHelp.Name = "ToolStripbtnHelp";
            this.ToolStripbtnHelp.Size = new System.Drawing.Size(23, 22);
            this.ToolStripbtnHelp.Text = "He&lp";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnEncashPolicy);
            this.panel1.Controls.Add(this.btnHolidayPolicy);
            this.panel1.Controls.Add(this.btnOvertimePolicy);
            this.panel1.Controls.Add(this.cboHolidayPolicy);
            this.panel1.Controls.Add(this.cboEncashPolicy);
            this.panel1.Controls.Add(this.btnAbsentPolicy);
            this.panel1.Controls.Add(this.cboOvertimePolicy);
            this.panel1.Controls.Add(Label5);
            this.panel1.Controls.Add(Label6);
            this.panel1.Controls.Add(Label7);
            this.panel1.Controls.Add(this.cboAbsentPolicy);
            this.panel1.Controls.Add(label4);
            this.panel1.Controls.Add(this.btnSettlement);
            this.panel1.Controls.Add(Label11);
            this.panel1.Controls.Add(this.cboSettlement);
            this.panel1.Controls.Add(this.tabAdditionDeduction);
            this.panel1.Controls.Add(this.LblTotalPercentage);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.BtnSave);
            this.panel1.Controls.Add(this.BtnBottomCancel);
            this.panel1.Controls.Add(this.BtnOk);
            this.panel1.Controls.Add(this.TxtName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(0, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(567, 434);
            this.panel1.TabIndex = 47;
            // 
            // btnEncashPolicy
            // 
            this.btnEncashPolicy.Location = new System.Drawing.Point(523, 33);
            this.btnEncashPolicy.Name = "btnEncashPolicy";
            this.btnEncashPolicy.Size = new System.Drawing.Size(32, 23);
            this.btnEncashPolicy.TabIndex = 1055;
            this.btnEncashPolicy.Text = "...";
            this.btnEncashPolicy.UseVisualStyleBackColor = true;
            this.btnEncashPolicy.Click += new System.EventHandler(this.btnEncashPolicy_Click);
            // 
            // btnHolidayPolicy
            // 
            this.btnHolidayPolicy.Location = new System.Drawing.Point(233, 84);
            this.btnHolidayPolicy.Name = "btnHolidayPolicy";
            this.btnHolidayPolicy.Size = new System.Drawing.Size(32, 23);
            this.btnHolidayPolicy.TabIndex = 1053;
            this.btnHolidayPolicy.Text = "...";
            this.btnHolidayPolicy.UseVisualStyleBackColor = true;
            this.btnHolidayPolicy.Click += new System.EventHandler(this.btnHolidayPolicy_Click);
            // 
            // btnOvertimePolicy
            // 
            this.btnOvertimePolicy.Location = new System.Drawing.Point(523, 7);
            this.btnOvertimePolicy.Name = "btnOvertimePolicy";
            this.btnOvertimePolicy.Size = new System.Drawing.Size(32, 23);
            this.btnOvertimePolicy.TabIndex = 1051;
            this.btnOvertimePolicy.Text = "...";
            this.btnOvertimePolicy.UseVisualStyleBackColor = true;
            this.btnOvertimePolicy.Click += new System.EventHandler(this.btnOvertimePolicy_Click);
            // 
            // cboHolidayPolicy
            // 
            this.cboHolidayPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboHolidayPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboHolidayPolicy.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboHolidayPolicy.DropDownHeight = 134;
            this.cboHolidayPolicy.FormattingEnabled = true;
            this.cboHolidayPolicy.IntegralHeight = false;
            this.cboHolidayPolicy.Location = new System.Drawing.Point(84, 85);
            this.cboHolidayPolicy.Name = "cboHolidayPolicy";
            this.cboHolidayPolicy.Size = new System.Drawing.Size(147, 21);
            this.cboHolidayPolicy.TabIndex = 1052;
            // 
            // cboEncashPolicy
            // 
            this.cboEncashPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEncashPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEncashPolicy.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboEncashPolicy.DropDownHeight = 134;
            this.cboEncashPolicy.FormattingEnabled = true;
            this.cboEncashPolicy.IntegralHeight = false;
            this.cboEncashPolicy.Location = new System.Drawing.Point(368, 34);
            this.cboEncashPolicy.Name = "cboEncashPolicy";
            this.cboEncashPolicy.Size = new System.Drawing.Size(147, 21);
            this.cboEncashPolicy.TabIndex = 1054;
            // 
            // btnAbsentPolicy
            // 
            this.btnAbsentPolicy.Location = new System.Drawing.Point(233, 58);
            this.btnAbsentPolicy.Name = "btnAbsentPolicy";
            this.btnAbsentPolicy.Size = new System.Drawing.Size(32, 23);
            this.btnAbsentPolicy.TabIndex = 1049;
            this.btnAbsentPolicy.Text = "...";
            this.btnAbsentPolicy.UseVisualStyleBackColor = true;
            this.btnAbsentPolicy.Click += new System.EventHandler(this.btnAbsentPolicy_Click);
            // 
            // cboOvertimePolicy
            // 
            this.cboOvertimePolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOvertimePolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOvertimePolicy.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboOvertimePolicy.DropDownHeight = 134;
            this.cboOvertimePolicy.FormattingEnabled = true;
            this.cboOvertimePolicy.IntegralHeight = false;
            this.cboOvertimePolicy.Location = new System.Drawing.Point(368, 8);
            this.cboOvertimePolicy.Name = "cboOvertimePolicy";
            this.cboOvertimePolicy.Size = new System.Drawing.Size(147, 21);
            this.cboOvertimePolicy.TabIndex = 1050;
            // 
            // cboAbsentPolicy
            // 
            this.cboAbsentPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAbsentPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAbsentPolicy.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboAbsentPolicy.DropDownHeight = 134;
            this.cboAbsentPolicy.FormattingEnabled = true;
            this.cboAbsentPolicy.IntegralHeight = false;
            this.cboAbsentPolicy.Location = new System.Drawing.Point(84, 59);
            this.cboAbsentPolicy.Name = "cboAbsentPolicy";
            this.cboAbsentPolicy.Size = new System.Drawing.Size(147, 21);
            this.cboAbsentPolicy.TabIndex = 1048;
            // 
            // btnSettlement
            // 
            this.btnSettlement.Location = new System.Drawing.Point(233, 32);
            this.btnSettlement.Name = "btnSettlement";
            this.btnSettlement.Size = new System.Drawing.Size(32, 23);
            this.btnSettlement.TabIndex = 1046;
            this.btnSettlement.Text = "...";
            this.btnSettlement.UseVisualStyleBackColor = true;
            this.btnSettlement.Click += new System.EventHandler(this.btnSettlement_Click);
            // 
            // cboSettlement
            // 
            this.cboSettlement.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSettlement.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSettlement.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cboSettlement.DropDownHeight = 134;
            this.cboSettlement.FormattingEnabled = true;
            this.cboSettlement.IntegralHeight = false;
            this.cboSettlement.Location = new System.Drawing.Point(85, 33);
            this.cboSettlement.Name = "cboSettlement";
            this.cboSettlement.Size = new System.Drawing.Size(147, 21);
            this.cboSettlement.TabIndex = 1045;
            // 
            // tabAdditionDeduction
            // 
            this.tabAdditionDeduction.Controls.Add(this.tabPage1);
            this.tabAdditionDeduction.Controls.Add(this.tabPage2);
            this.tabAdditionDeduction.Location = new System.Drawing.Point(5, 116);
            this.tabAdditionDeduction.Name = "tabAdditionDeduction";
            this.tabAdditionDeduction.SelectedIndex = 0;
            this.tabAdditionDeduction.Size = new System.Drawing.Size(559, 267);
            this.tabAdditionDeduction.TabIndex = 63;
            this.tabAdditionDeduction.SelectedIndexChanged += new System.EventHandler(this.tabAdditionDeduction_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.DgvSalaryTemp);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(551, 241);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Addition";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // DgvSalaryTemp
            // 
            this.DgvSalaryTemp.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.DgvSalaryTemp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvSalaryTemp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colParticulars,
            this.colPercentage});
            this.DgvSalaryTemp.Location = new System.Drawing.Point(-1, 0);
            this.DgvSalaryTemp.Name = "DgvSalaryTemp";
            this.DgvSalaryTemp.RowHeadersWidth = 25;
            this.DgvSalaryTemp.Size = new System.Drawing.Size(553, 242);
            this.DgvSalaryTemp.TabIndex = 57;
            this.DgvSalaryTemp.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvSalaryTemp_CellValueChanged);
            this.DgvSalaryTemp.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DgvSalaryTemp_CellBeginEdit);
            this.DgvSalaryTemp.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DgvSalaryTemp_EditingControlShowing);
            this.DgvSalaryTemp.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvSalaryTemp_DataError);
            this.DgvSalaryTemp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvSalaryTemp_KeyDown);
            // 
            // colParticulars
            // 
            this.colParticulars.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colParticulars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.colParticulars.HeaderText = "Particulars";
            this.colParticulars.Name = "colParticulars";
            this.colParticulars.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colParticulars.Width = 300;
            // 
            // colPercentage
            // 
            this.colPercentage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colPercentage.HeaderText = "Percentage";
            this.colPercentage.Name = "colPercentage";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.DgvDedSalTemplate);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(551, 241);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Deduction";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // DgvDedSalTemplate
            // 
            this.DgvDedSalTemplate.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.DgvDedSalTemplate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvDedSalTemplate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDedParticulars,
            this.colAmount,
            this.colDedPolicy});
            this.DgvDedSalTemplate.Location = new System.Drawing.Point(-1, 0);
            this.DgvDedSalTemplate.Name = "DgvDedSalTemplate";
            this.DgvDedSalTemplate.RowHeadersVisible = false;
            this.DgvDedSalTemplate.Size = new System.Drawing.Size(553, 242);
            this.DgvDedSalTemplate.TabIndex = 7;
            this.DgvDedSalTemplate.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvDedSalTemplate_CellValueChanged);
            this.DgvDedSalTemplate.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DgvDedSalTemplate_CellBeginEdit);
            this.DgvDedSalTemplate.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvDedSalTemplate_CellEndEdit);
            this.DgvDedSalTemplate.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DgvDedSalTemplate_EditingControlShowing);
            this.DgvDedSalTemplate.CurrentCellDirtyStateChanged += new System.EventHandler(this.DgvDedSalTemplate_CurrentCellDirtyStateChanged);
            this.DgvDedSalTemplate.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DgvDedSalTemplate_DataError);
            this.DgvDedSalTemplate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DgvDedSalTemplate_KeyDown);
            // 
            // colDedParticulars
            // 
            this.colDedParticulars.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colDedParticulars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.colDedParticulars.HeaderText = "Particulars";
            this.colDedParticulars.Name = "colDedParticulars";
            this.colDedParticulars.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDedParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colDedParticulars.Width = 220;
            // 
            // colAmount
            // 
            this.colAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colAmount.HeaderText = "Amount";
            this.colAmount.Name = "colAmount";
            // 
            // colDedPolicy
            // 
            this.colDedPolicy.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colDedPolicy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.colDedPolicy.HeaderText = "Deduction Policy";
            this.colDedPolicy.Name = "colDedPolicy";
            this.colDedPolicy.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDedPolicy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // LblTotalPercentage
            // 
            this.LblTotalPercentage.AutoSize = true;
            this.LblTotalPercentage.Location = new System.Drawing.Point(402, 386);
            this.LblTotalPercentage.Name = "LblTotalPercentage";
            this.LblTotalPercentage.Size = new System.Drawing.Size(0, 13);
            this.LblTotalPercentage.TabIndex = 62;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(305, 384);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Total Percentage :";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(12, 404);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 51;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnBottomCancel
            // 
            this.BtnBottomCancel.Location = new System.Drawing.Point(474, 404);
            this.BtnBottomCancel.Name = "BtnBottomCancel";
            this.BtnBottomCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnBottomCancel.TabIndex = 53;
            this.BtnBottomCancel.Text = "&Cancel";
            this.BtnBottomCancel.UseVisualStyleBackColor = true;
            this.BtnBottomCancel.Click += new System.EventHandler(this.BtnBottomCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(393, 404);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 52;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // TxtName
            // 
            this.TxtName.BackColor = System.Drawing.SystemColors.Info;
            this.TxtName.Location = new System.Drawing.Point(86, 8);
            this.TxtName.MaxLength = 50;
            this.TxtName.Name = "TxtName";
            this.TxtName.Size = new System.Drawing.Size(147, 20);
            this.TxtName.TabIndex = 48;
            this.TxtName.TextChanged += new System.EventHandler(this.TxtName_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 49;
            this.label3.Text = " Name";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Percentage";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // FrmSalaryTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 465);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PolicyBindingNavigator);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSalaryTemplate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SalaryTemplate";
            this.Load += new System.EventHandler(this.FrmSalaryTemplate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PolicyBindingNavigator)).EndInit();
            this.PolicyBindingNavigator.ResumeLayout(false);
            this.PolicyBindingNavigator.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabAdditionDeduction.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvSalaryTemp)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvDedSalTemplate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.BindingNavigator PolicyBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtnCancel;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton ToolStripbtnHelp;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnBottomCancel;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.TextBox TxtName;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.Label LblTotalPercentage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabAdditionDeduction;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView DgvSalaryTemp;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView DgvDedSalTemplate;
        private System.Windows.Forms.DataGridViewComboBoxColumn colDedParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewComboBoxColumn colDedPolicy;
        private System.Windows.Forms.DataGridViewComboBoxColumn colParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPercentage;
        internal System.Windows.Forms.Button btnSettlement;
        internal System.Windows.Forms.ComboBox cboSettlement;
        internal System.Windows.Forms.Button btnEncashPolicy;
        internal System.Windows.Forms.Button btnHolidayPolicy;
        internal System.Windows.Forms.Button btnOvertimePolicy;
        internal System.Windows.Forms.ComboBox cboHolidayPolicy;
        internal System.Windows.Forms.ComboBox cboEncashPolicy;
        internal System.Windows.Forms.Button btnAbsentPolicy;
        internal System.Windows.Forms.ComboBox cboOvertimePolicy;
        internal System.Windows.Forms.ComboBox cboAbsentPolicy;
    }
}