﻿namespace MyPayfriend
{
    partial class FrmSIF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSIF));
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rtbVisaCompany = new System.Windows.Forms.RadioButton();
            this.rtbWorkingCompany = new System.Windows.Forms.RadioButton();
            this.cboBank = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DtpCurrentMonth = new System.Windows.Forms.DateTimePicker();
            this.saveFileDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.GrdEDRDetails = new System.Windows.Forms.DataGridView();
            this.btnEvaluate = new System.Windows.Forms.Button();
            this.pnlMiddle = new DevComponents.DotNetBar.PanelEx();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.btnPrint = new System.Windows.Forms.Button();
            this.GrdSCRDetails = new System.Windows.Forms.DataGridView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.expandableSplitter1 = new DevComponents.DotNetBar.ExpandableSplitter();
            ((System.ComponentModel.ISupportInitialize)(this.GrdEDRDetails)).BeginInit();
            this.pnlMiddle.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdSCRDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // cboCompany
            // 
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(334, 76);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(257, 21);
            this.cboCompany.TabIndex = 14;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "WPS Based On";
            // 
            // rtbVisaCompany
            // 
            this.rtbVisaCompany.AutoSize = true;
            this.rtbVisaCompany.Location = new System.Drawing.Point(117, 102);
            this.rtbVisaCompany.Name = "rtbVisaCompany";
            this.rtbVisaCompany.Size = new System.Drawing.Size(92, 17);
            this.rtbVisaCompany.TabIndex = 12;
            this.rtbVisaCompany.TabStop = true;
            this.rtbVisaCompany.Text = "Visa Company";
            this.rtbVisaCompany.UseVisualStyleBackColor = true;
            this.rtbVisaCompany.CheckedChanged += new System.EventHandler(this.rtbVisaCompany_CheckedChanged);
            // 
            // rtbWorkingCompany
            // 
            this.rtbWorkingCompany.AutoSize = true;
            this.rtbWorkingCompany.Location = new System.Drawing.Point(117, 79);
            this.rtbWorkingCompany.Name = "rtbWorkingCompany";
            this.rtbWorkingCompany.Size = new System.Drawing.Size(112, 17);
            this.rtbWorkingCompany.TabIndex = 11;
            this.rtbWorkingCompany.TabStop = true;
            this.rtbWorkingCompany.Text = "Working Company";
            this.rtbWorkingCompany.UseVisualStyleBackColor = true;
            // 
            // cboBank
            // 
            this.cboBank.FormattingEnabled = true;
            this.cboBank.Location = new System.Drawing.Point(334, 103);
            this.cboBank.Name = "cboBank";
            this.cboBank.Size = new System.Drawing.Size(257, 21);
            this.cboBank.TabIndex = 10;
            this.cboBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboBank_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(268, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Bank";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(268, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Company";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(610, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Salary Day";
            // 
            // DtpCurrentMonth
            // 
            this.DtpCurrentMonth.AllowDrop = true;
            this.DtpCurrentMonth.CustomFormat = "dd MMM yyyy";
            this.DtpCurrentMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpCurrentMonth.Location = new System.Drawing.Point(674, 77);
            this.DtpCurrentMonth.Name = "DtpCurrentMonth";
            this.DtpCurrentMonth.Size = new System.Drawing.Size(103, 20);
            this.DtpCurrentMonth.TabIndex = 4;
            this.DtpCurrentMonth.ValueChanged += new System.EventHandler(this.DtpCurrentMonth_ValueChanged);
            // 
            // GrdEDRDetails
            // 
            this.GrdEDRDetails.AllowUserToAddRows = false;
            this.GrdEDRDetails.AllowUserToDeleteRows = false;
            this.GrdEDRDetails.AllowUserToResizeRows = false;
            this.GrdEDRDetails.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.GrdEDRDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdEDRDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrdEDRDetails.Location = new System.Drawing.Point(0, 0);
            this.GrdEDRDetails.Name = "GrdEDRDetails";
            this.GrdEDRDetails.RowHeadersWidth = 20;
            this.GrdEDRDetails.Size = new System.Drawing.Size(1342, 498);
            this.GrdEDRDetails.TabIndex = 92;
            this.GrdEDRDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.GrdEDRDetails_EditingControlShowing);
            // 
            // btnEvaluate
            // 
            this.btnEvaluate.Location = new System.Drawing.Point(1055, 78);
            this.btnEvaluate.Name = "btnEvaluate";
            this.btnEvaluate.Size = new System.Drawing.Size(77, 25);
            this.btnEvaluate.TabIndex = 15;
            this.btnEvaluate.Text = "Evaluate";
            this.btnEvaluate.UseVisualStyleBackColor = true;
            this.btnEvaluate.Click += new System.EventHandler(this.btnEvaluate_Click);
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMiddle.Controls.Add(this.GrdEDRDetails);
            this.pnlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMiddle.Location = new System.Drawing.Point(0, 0);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Size = new System.Drawing.Size(1342, 498);
            this.pnlMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMiddle.Style.GradientAngle = 90;
            this.pnlMiddle.TabIndex = 128;
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.btnShow);
            this.pnlBottom.Controls.Add(this.btnPrint);
            this.pnlBottom.Controls.Add(this.GrdSCRDetails);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Controls.Add(this.label4);
            this.pnlBottom.Controls.Add(this.rtbVisaCompany);
            this.pnlBottom.Controls.Add(this.rtbWorkingCompany);
            this.pnlBottom.Controls.Add(this.btnEvaluate);
            this.pnlBottom.Controls.Add(this.cboCompany);
            this.pnlBottom.Controls.Add(this.cboBank);
            this.pnlBottom.Controls.Add(this.label2);
            this.pnlBottom.Controls.Add(this.label1);
            this.pnlBottom.Controls.Add(this.label3);
            this.pnlBottom.Controls.Add(this.DtpCurrentMonth);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 369);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1342, 129);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 129;
            this.pnlBottom.Click += new System.EventHandler(this.pnlBottom_Click);
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(783, 77);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(64, 42);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 145;
            this.btnShow.Text = "New SIF Details";
            this.btnShow.Tooltip = "New SIF Details";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(1138, 77);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(77, 25);
            this.btnPrint.TabIndex = 18;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // GrdSCRDetails
            // 
            this.GrdSCRDetails.AllowUserToAddRows = false;
            this.GrdSCRDetails.AllowUserToDeleteRows = false;
            this.GrdSCRDetails.AllowUserToResizeRows = false;
            this.GrdSCRDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GrdSCRDetails.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.GrdSCRDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdSCRDetails.Location = new System.Drawing.Point(0, 3);
            this.GrdSCRDetails.Name = "GrdSCRDetails";
            this.GrdSCRDetails.Size = new System.Drawing.Size(1342, 70);
            this.GrdSCRDetails.TabIndex = 17;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(1221, 76);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 25);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // expandableSplitter1
            // 
            this.expandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.expandableSplitter1.ExpandableControl = this.pnlBottom;
            this.expandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.Location = new System.Drawing.Point(0, 364);
            this.expandableSplitter1.Name = "expandableSplitter1";
            this.expandableSplitter1.Size = new System.Drawing.Size(1342, 5);
            this.expandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitter1.TabIndex = 130;
            this.expandableSplitter1.TabStop = false;
            // 
            // FrmSIF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1342, 498);
            this.Controls.Add(this.expandableSplitter1);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlMiddle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSIF";
            this.Text = "SIF";
            this.Load += new System.EventHandler(this.FrmSIF_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GrdEDRDetails)).EndInit();
            this.pnlMiddle.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdSCRDetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.DateTimePicker DtpCurrentMonth;
        private System.Windows.Forms.FolderBrowserDialog saveFileDialog1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboBank;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rtbVisaCompany;
        private System.Windows.Forms.RadioButton rtbWorkingCompany;
        private System.Windows.Forms.ComboBox cboCompany;
        private System.Windows.Forms.DataGridView GrdEDRDetails;
        private DevComponents.DotNetBar.PanelEx pnlMiddle;
        private System.Windows.Forms.Button btnEvaluate;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView GrdSCRDetails;
        private System.Windows.Forms.Button btnPrint;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter1;
    }
}