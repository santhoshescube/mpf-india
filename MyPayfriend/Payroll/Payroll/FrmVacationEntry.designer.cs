﻿namespace MyPayfriend
{
    partial class FrmVacationEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblGivenAmount;
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label lblNetAmount;
            System.Windows.Forms.Label lblDeductions;
            System.Windows.Forms.Label lblAdditions;
            System.Windows.Forms.Label lblProcessDate;
            System.Windows.Forms.Label lblAmount;
            System.Windows.Forms.Label lblParticulars;
            System.Windows.Forms.Label lblCheqNo;
            System.Windows.Forms.Label lblAccount;
            System.Windows.Forms.Label lblChequeDate;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label Label12;
            System.Windows.Forms.Label label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVacationEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblRejoinDaate = new System.Windows.Forms.Label();
            this.dtpRejoinDate = new System.Windows.Forms.DateTimePicker();
            this.chkEncashOnly = new System.Windows.Forms.CheckBox();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.lblTotalEligibleLeavePayDays = new System.Windows.Forms.Label();
            this.btnShow = new System.Windows.Forms.Button();
            this.tabVacationProcess = new System.Windows.Forms.TabControl();
            this.tabEmployeeInfo = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.cboVacationPolicy = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkAgent = new System.Windows.Forms.CheckBox();
            this.txtEncashComboOffDay = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblAbsentDays = new System.Windows.Forms.Label();
            this.lblAbsentDaysText = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ChkFullMonthSalary = new System.Windows.Forms.CheckBox();
            this.txtChequeNo = new System.Windows.Forms.TextBox();
            this.cboTransactionType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpChequeDate = new System.Windows.Forms.DateTimePicker();
            this.lblVacationDays = new System.Windows.Forms.Label();
            this.lblExperienceDays = new System.Windows.Forms.Label();
            this.lblTotalEligibleLeavePayDaysText = new System.Windows.Forms.Label();
            this.txtEligibleLeavePayDays = new System.Windows.Forms.TextBox();
            this.dtpProcessDate = new System.Windows.Forms.DateTimePicker();
            this.lblEligibleLeavePayDays = new System.Windows.Forms.Label();
            this.lblExperienceDaysText = new System.Windows.Forms.Label();
            this.dtpJoiningDate = new System.Windows.Forms.DateTimePicker();
            this.lblNoOfDays = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.lblEligibleLeavesText = new System.Windows.Forms.Label();
            this.lblIsPaid = new System.Windows.Forms.Label();
            this.lblEligibleLeaves = new System.Windows.Forms.Label();
            this.lblAdditionLeaves = new System.Windows.Forms.Label();
            this.lblAdditionalLeavesText = new System.Windows.Forms.Label();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkIsPaid = new System.Windows.Forms.CheckBox();
            this.chkProcessSalaryForCurrentMonth = new System.Windows.Forms.CheckBox();
            this.chkConsiderAbsentDays = new System.Windows.Forms.CheckBox();
            this.lblActualRejoinDate = new System.Windows.Forms.Label();
            this.lblActualRejoinDateDec = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.lblToDate = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblTicketAmount = new System.Windows.Forms.Label();
            this.btnProcess = new System.Windows.Forms.Button();
            this.lblIssuedTickets = new System.Windows.Forms.Label();
            this.txtNoOfTicketsIssued = new System.Windows.Forms.TextBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.txtTicketAmount = new System.Windows.Forms.TextBox();
            this.tabParticulars = new System.Windows.Forms.TabPage();
            this.ChkParticulars = new System.Windows.Forms.CheckBox();
            this.TextboxNumeric1 = new System.Windows.Forms.TextBox();
            this.TextboxNumeric = new System.Windows.Forms.TextBox();
            this.txtGivenAmount = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtNetAmount = new System.Windows.Forms.TextBox();
            this.txtTotalAddition = new System.Windows.Forms.TextBox();
            this.txtTotalDeduction = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.dgvVacationDetails = new System.Windows.Forms.DataGridView();
            this.dgvColVacationDetailID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColParticulars = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColIsAddition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColNoOfDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColReleaseLater = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvColTempAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAdditionDeductions = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cboParticulars = new System.Windows.Forms.ComboBox();
            this.shapeContainer4 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.tabHistory = new System.Windows.Forms.TabPage();
            this.dgvEmpHistory = new System.Windows.Forms.DataGridView();
            this.lblSearchEmployee = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.cboAccount = new System.Windows.Forms.ComboBox();
            this.errorProviderVacation = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrVacation = new System.Windows.Forms.Timer(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ssVacation = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnEmail = new System.Windows.Forms.ToolStripButton();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorVacationProcess = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.bnDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bnClearItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnVacationExtension = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrint = new System.Windows.Forms.ToolStripSplitButton();
            this.template1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.template2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expLeft = new DevComponents.DotNetBar.ExpandablePanel();
            this.dgvSearch = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.VacationID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Employee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeArb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTopSearch = new DevComponents.DotNetBar.PanelEx();
            this.cboSearchEmployee = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblEmployeeCode = new System.Windows.Forms.Label();
            this.cboSearchCode = new System.Windows.Forms.ComboBox();
            this.lblSearchTo = new System.Windows.Forms.Label();
            this.lblSearchFrom = new System.Windows.Forms.Label();
            this.dtpSearchToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpSearchFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblSearchCompany = new System.Windows.Forms.Label();
            this.cboSearchCompany = new System.Windows.Forms.ComboBox();
            this.dtpDateValidate = new System.Windows.Forms.DateTimePicker();
            lblGivenAmount = new System.Windows.Forms.Label();
            Label2 = new System.Windows.Forms.Label();
            lblNetAmount = new System.Windows.Forms.Label();
            lblDeductions = new System.Windows.Forms.Label();
            lblAdditions = new System.Windows.Forms.Label();
            lblProcessDate = new System.Windows.Forms.Label();
            lblAmount = new System.Windows.Forms.Label();
            lblParticulars = new System.Windows.Forms.Label();
            lblCheqNo = new System.Windows.Forms.Label();
            lblAccount = new System.Windows.Forms.Label();
            lblChequeDate = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            Label12 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            this.tabVacationProcess.SuspendLayout();
            this.tabEmployeeInfo.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabParticulars.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVacationDetails)).BeginInit();
            this.tabHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderVacation)).BeginInit();
            this.ssVacation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorVacationProcess)).BeginInit();
            this.BindingNavigatorVacationProcess.SuspendLayout();
            this.expLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).BeginInit();
            this.pnlTopSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblGivenAmount
            // 
            lblGivenAmount.AutoSize = true;
            lblGivenAmount.Location = new System.Drawing.Point(353, 265);
            lblGivenAmount.Name = "lblGivenAmount";
            lblGivenAmount.Size = new System.Drawing.Size(74, 13);
            lblGivenAmount.TabIndex = 15;
            lblGivenAmount.Text = "Given Amount";
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label2.Location = new System.Drawing.Point(-2, 16);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(147, 13);
            Label2.TabIndex = 0;
            Label2.Text = "Vacation Settlement Info";
            // 
            // lblNetAmount
            // 
            lblNetAmount.AutoSize = true;
            lblNetAmount.Location = new System.Drawing.Point(191, 265);
            lblNetAmount.Name = "lblNetAmount";
            lblNetAmount.Size = new System.Drawing.Size(63, 13);
            lblNetAmount.TabIndex = 13;
            lblNetAmount.Text = "Net Amount";
            // 
            // lblDeductions
            // 
            lblDeductions.AutoSize = true;
            lblDeductions.Location = new System.Drawing.Point(353, 238);
            lblDeductions.Name = "lblDeductions";
            lblDeductions.Size = new System.Drawing.Size(61, 13);
            lblDeductions.TabIndex = 11;
            lblDeductions.Text = "Deductions";
            // 
            // lblAdditions
            // 
            lblAdditions.AutoSize = true;
            lblAdditions.Location = new System.Drawing.Point(191, 238);
            lblAdditions.Name = "lblAdditions";
            lblAdditions.Size = new System.Drawing.Size(50, 13);
            lblAdditions.TabIndex = 9;
            lblAdditions.Text = "Additions";
            // 
            // lblProcessDate
            // 
            lblProcessDate.AutoSize = true;
            lblProcessDate.Location = new System.Drawing.Point(132, 52);
            lblProcessDate.Name = "lblProcessDate";
            lblProcessDate.Size = new System.Drawing.Size(71, 13);
            lblProcessDate.TabIndex = 6;
            lblProcessDate.Text = "Process Date";
            lblProcessDate.Visible = false;
            // 
            // lblAmount
            // 
            lblAmount.AutoSize = true;
            lblAmount.Location = new System.Drawing.Point(272, 32);
            lblAmount.Name = "lblAmount";
            lblAmount.Size = new System.Drawing.Size(43, 13);
            lblAmount.TabIndex = 3;
            lblAmount.Text = "Amount";
            // 
            // lblParticulars
            // 
            lblParticulars.AutoSize = true;
            lblParticulars.Location = new System.Drawing.Point(5, 32);
            lblParticulars.Name = "lblParticulars";
            lblParticulars.Size = new System.Drawing.Size(51, 13);
            lblParticulars.TabIndex = 208;
            lblParticulars.Text = "Particular";
            // 
            // lblCheqNo
            // 
            lblCheqNo.AutoSize = true;
            lblCheqNo.Location = new System.Drawing.Point(4, 47);
            lblCheqNo.Name = "lblCheqNo";
            lblCheqNo.Size = new System.Drawing.Size(64, 13);
            lblCheqNo.TabIndex = 2;
            lblCheqNo.Text = "Cheque No ";
            // 
            // lblAccount
            // 
            lblAccount.AutoSize = true;
            lblAccount.Location = new System.Drawing.Point(393, 537);
            lblAccount.Name = "lblAccount";
            lblAccount.Size = new System.Drawing.Size(47, 13);
            lblAccount.TabIndex = 1043;
            lblAccount.Text = "Account";
            lblAccount.Visible = false;
            // 
            // lblChequeDate
            // 
            lblChequeDate.AutoSize = true;
            lblChequeDate.Location = new System.Drawing.Point(4, 72);
            lblChequeDate.Name = "lblChequeDate";
            lblChequeDate.Size = new System.Drawing.Size(70, 13);
            lblChequeDate.TabIndex = 4;
            lblChequeDate.Text = "Cheque Date";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.Location = new System.Drawing.Point(6, 13);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(83, 13);
            label6.TabIndex = 1;
            label6.Text = "Vacation Info";
            // 
            // Label12
            // 
            Label12.AutoSize = true;
            Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label12.Location = new System.Drawing.Point(2, 79);
            Label12.Name = "Label12";
            Label12.Size = new System.Drawing.Size(67, 13);
            Label12.TabIndex = 12;
            Label12.Text = "Leave Pay";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(6, 199);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(47, 13);
            label1.TabIndex = 19;
            label1.Text = "Ticket ";
            // 
            // lblRejoinDaate
            // 
            this.lblRejoinDaate.AutoSize = true;
            this.lblRejoinDaate.Location = new System.Drawing.Point(12, 47);
            this.lblRejoinDaate.Name = "lblRejoinDaate";
            this.lblRejoinDaate.Size = new System.Drawing.Size(63, 13);
            this.lblRejoinDaate.TabIndex = 4;
            this.lblRejoinDaate.Text = "Rejoin Date";
            // 
            // dtpRejoinDate
            // 
            this.dtpRejoinDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpRejoinDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRejoinDate.Location = new System.Drawing.Point(81, 41);
            this.dtpRejoinDate.Name = "dtpRejoinDate";
            this.dtpRejoinDate.Size = new System.Drawing.Size(109, 20);
            this.dtpRejoinDate.TabIndex = 5;
            this.dtpRejoinDate.ValueChanged += new System.EventHandler(this.dtpRejoinDate_ValueChanged);
            // 
            // chkEncashOnly
            // 
            this.chkEncashOnly.AutoSize = true;
            this.chkEncashOnly.Location = new System.Drawing.Point(8, 77);
            this.chkEncashOnly.Name = "chkEncashOnly";
            this.chkEncashOnly.Size = new System.Drawing.Size(86, 17);
            this.chkEncashOnly.TabIndex = 0;
            this.chkEncashOnly.Text = "Encash Only";
            this.chkEncashOnly.UseVisualStyleBackColor = true;
            this.chkEncashOnly.CheckedChanged += new System.EventHandler(this.chkEncashOnly_CheckedChanged);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // lblTotalEligibleLeavePayDays
            // 
            this.lblTotalEligibleLeavePayDays.AutoSize = true;
            this.lblTotalEligibleLeavePayDays.Location = new System.Drawing.Point(12, 99);
            this.lblTotalEligibleLeavePayDays.Name = "lblTotalEligibleLeavePayDays";
            this.lblTotalEligibleLeavePayDays.Size = new System.Drawing.Size(148, 13);
            this.lblTotalEligibleLeavePayDays.TabIndex = 13;
            this.lblTotalEligibleLeavePayDays.Text = "Total Eligible Leave Pay Days";
            // 
            // btnShow
            // 
            this.btnShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShow.Location = new System.Drawing.Point(691, 35);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(60, 23);
            this.btnShow.TabIndex = 225;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // tabVacationProcess
            // 
            this.tabVacationProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabVacationProcess.Controls.Add(this.tabEmployeeInfo);
            this.tabVacationProcess.Controls.Add(this.tabParticulars);
            this.tabVacationProcess.Controls.Add(this.tabHistory);
            this.tabVacationProcess.Location = new System.Drawing.Point(262, 65);
            this.tabVacationProcess.Name = "tabVacationProcess";
            this.tabVacationProcess.SelectedIndex = 0;
            this.tabVacationProcess.Size = new System.Drawing.Size(525, 461);
            this.tabVacationProcess.TabIndex = 1;
            // 
            // tabEmployeeInfo
            // 
            this.tabEmployeeInfo.Controls.Add(this.label7);
            this.tabEmployeeInfo.Controls.Add(this.cboVacationPolicy);
            this.tabEmployeeInfo.Controls.Add(this.groupBox3);
            this.tabEmployeeInfo.Controls.Add(this.groupBox2);
            this.tabEmployeeInfo.Controls.Add(this.lblTicketAmount);
            this.tabEmployeeInfo.Controls.Add(this.btnProcess);
            this.tabEmployeeInfo.Controls.Add(this.lblIssuedTickets);
            this.tabEmployeeInfo.Controls.Add(label6);
            this.tabEmployeeInfo.Controls.Add(this.txtNoOfTicketsIssued);
            this.tabEmployeeInfo.Controls.Add(this.shapeContainer2);
            this.tabEmployeeInfo.Controls.Add(this.txtTicketAmount);
            this.tabEmployeeInfo.Location = new System.Drawing.Point(4, 22);
            this.tabEmployeeInfo.Name = "tabEmployeeInfo";
            this.tabEmployeeInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabEmployeeInfo.Size = new System.Drawing.Size(517, 435);
            this.tabEmployeeInfo.TabIndex = 0;
            this.tabEmployeeInfo.Text = "Vacation Info";
            this.tabEmployeeInfo.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(519, 201);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 1057;
            this.label7.Text = "Vacation Policy";
            this.label7.Visible = false;
            // 
            // cboVacationPolicy
            // 
            this.cboVacationPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVacationPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVacationPolicy.BackColor = System.Drawing.SystemColors.Info;
            this.cboVacationPolicy.DropDownHeight = 134;
            this.cboVacationPolicy.FormattingEnabled = true;
            this.cboVacationPolicy.IntegralHeight = false;
            this.cboVacationPolicy.Location = new System.Drawing.Point(519, 217);
            this.cboVacationPolicy.Name = "cboVacationPolicy";
            this.cboVacationPolicy.Size = new System.Drawing.Size(181, 21);
            this.cboVacationPolicy.TabIndex = 1056;
            this.cboVacationPolicy.Visible = false;
            this.cboVacationPolicy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            this.cboVacationPolicy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboVacationPolicy_KeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkAgent);
            this.groupBox3.Controls.Add(this.txtEncashComboOffDay);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(label1);
            this.groupBox3.Controls.Add(this.lblAbsentDays);
            this.groupBox3.Controls.Add(this.lblAbsentDaysText);
            this.groupBox3.Controls.Add(Label12);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(lblProcessDate);
            this.groupBox3.Controls.Add(this.lblVacationDays);
            this.groupBox3.Controls.Add(this.lblTotalEligibleLeavePayDays);
            this.groupBox3.Controls.Add(this.lblExperienceDays);
            this.groupBox3.Controls.Add(this.lblTotalEligibleLeavePayDaysText);
            this.groupBox3.Controls.Add(this.txtEligibleLeavePayDays);
            this.groupBox3.Controls.Add(this.dtpProcessDate);
            this.groupBox3.Controls.Add(this.lblEligibleLeavePayDays);
            this.groupBox3.Controls.Add(this.lblExperienceDaysText);
            this.groupBox3.Controls.Add(this.dtpJoiningDate);
            this.groupBox3.Controls.Add(this.lblNoOfDays);
            this.groupBox3.Controls.Add(this.Label3);
            this.groupBox3.Controls.Add(this.lblEligibleLeavesText);
            this.groupBox3.Controls.Add(this.lblIsPaid);
            this.groupBox3.Controls.Add(this.lblEligibleLeaves);
            this.groupBox3.Controls.Add(this.lblAdditionLeaves);
            this.groupBox3.Controls.Add(this.lblAdditionalLeavesText);
            this.groupBox3.Controls.Add(this.shapeContainer3);
            this.groupBox3.Location = new System.Drawing.Point(18, 134);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(495, 258);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            // 
            // chkAgent
            // 
            this.chkAgent.AutoSize = true;
            this.chkAgent.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkAgent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chkAgent.Location = new System.Drawing.Point(15, 224);
            this.chkAgent.Name = "chkAgent";
            this.chkAgent.Size = new System.Drawing.Size(65, 17);
            this.chkAgent.TabIndex = 1058;
            this.chkAgent.Text = "Is Agent";
            this.chkAgent.UseVisualStyleBackColor = true;
            this.chkAgent.CheckedChanged += new System.EventHandler(this.chkCompany_CheckedChanged);
            // 
            // txtEncashComboOffDay
            // 
            this.txtEncashComboOffDay.Enabled = false;
            this.txtEncashComboOffDay.Location = new System.Drawing.Point(122, 167);
            this.txtEncashComboOffDay.MaxLength = 6;
            this.txtEncashComboOffDay.Name = "txtEncashComboOffDay";
            this.txtEncashComboOffDay.Size = new System.Drawing.Size(44, 20);
            this.txtEncashComboOffDay.TabIndex = 1055;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 171);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 13);
            this.label8.TabIndex = 1054;
            this.label8.Text = "Combo Encash Days";
            // 
            // lblAbsentDays
            // 
            this.lblAbsentDays.AutoSize = true;
            this.lblAbsentDays.Location = new System.Drawing.Point(12, 147);
            this.lblAbsentDays.Name = "lblAbsentDays";
            this.lblAbsentDays.Size = new System.Drawing.Size(67, 13);
            this.lblAbsentDays.TabIndex = 16;
            this.lblAbsentDays.Text = "Absent Days";
            // 
            // lblAbsentDaysText
            // 
            this.lblAbsentDaysText.AutoSize = true;
            this.lblAbsentDaysText.Location = new System.Drawing.Point(119, 147);
            this.lblAbsentDaysText.Name = "lblAbsentDaysText";
            this.lblAbsentDaysText.Size = new System.Drawing.Size(13, 13);
            this.lblAbsentDaysText.TabIndex = 17;
            this.lblAbsentDaysText.Text = "0";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ChkFullMonthSalary);
            this.groupBox4.Controls.Add(this.txtChequeNo);
            this.groupBox4.Controls.Add(this.cboTransactionType);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(lblChequeDate);
            this.groupBox4.Controls.Add(lblCheqNo);
            this.groupBox4.Controls.Add(this.dtpChequeDate);
            this.groupBox4.Location = new System.Drawing.Point(241, 74);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(245, 125);
            this.groupBox4.TabIndex = 1050;
            this.groupBox4.TabStop = false;
            // 
            // ChkFullMonthSalary
            // 
            this.ChkFullMonthSalary.AutoSize = true;
            this.ChkFullMonthSalary.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ChkFullMonthSalary.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ChkFullMonthSalary.Location = new System.Drawing.Point(4, 97);
            this.ChkFullMonthSalary.Name = "ChkFullMonthSalary";
            this.ChkFullMonthSalary.Size = new System.Drawing.Size(128, 17);
            this.ChkFullMonthSalary.TabIndex = 1059;
            this.ChkFullMonthSalary.Text = "Pay Full Month Salary";
            this.ChkFullMonthSalary.UseVisualStyleBackColor = true;
            this.ChkFullMonthSalary.CheckedChanged += new System.EventHandler(this.ChkFullMonthSalary_CheckedChanged);
            // 
            // txtChequeNo
            // 
            this.txtChequeNo.Location = new System.Drawing.Point(90, 44);
            this.txtChequeNo.MaxLength = 99;
            this.txtChequeNo.Name = "txtChequeNo";
            this.txtChequeNo.Size = new System.Drawing.Size(140, 20);
            this.txtChequeNo.TabIndex = 3;
            this.txtChequeNo.TextChanged += new System.EventHandler(this.txtChequeNo_TextChanged);
            // 
            // cboTransactionType
            // 
            this.cboTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTransactionType.BackColor = System.Drawing.SystemColors.Info;
            this.cboTransactionType.DropDownHeight = 134;
            this.cboTransactionType.FormattingEnabled = true;
            this.cboTransactionType.IntegralHeight = false;
            this.cboTransactionType.Location = new System.Drawing.Point(90, 16);
            this.cboTransactionType.Name = "cboTransactionType";
            this.cboTransactionType.Size = new System.Drawing.Size(141, 21);
            this.cboTransactionType.TabIndex = 1;
            this.cboTransactionType.SelectedIndexChanged += new System.EventHandler(this.cboTransactionType_SelectedIndexChanged);
            this.cboTransactionType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Trans: Type";
            // 
            // dtpChequeDate
            // 
            this.dtpChequeDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpChequeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpChequeDate.Location = new System.Drawing.Point(90, 70);
            this.dtpChequeDate.Name = "dtpChequeDate";
            this.dtpChequeDate.Size = new System.Drawing.Size(85, 20);
            this.dtpChequeDate.TabIndex = 5;
            this.dtpChequeDate.ValueChanged += new System.EventHandler(this.dtpChequeDate_ValueChanged);
            // 
            // lblVacationDays
            // 
            this.lblVacationDays.AutoSize = true;
            this.lblVacationDays.Location = new System.Drawing.Point(12, 54);
            this.lblVacationDays.Name = "lblVacationDays";
            this.lblVacationDays.Size = new System.Drawing.Size(76, 13);
            this.lblVacationDays.TabIndex = 2;
            this.lblVacationDays.Text = "Vacation Days";
            // 
            // lblExperienceDays
            // 
            this.lblExperienceDays.AutoSize = true;
            this.lblExperienceDays.Location = new System.Drawing.Point(328, 47);
            this.lblExperienceDays.Name = "lblExperienceDays";
            this.lblExperienceDays.Size = new System.Drawing.Size(87, 13);
            this.lblExperienceDays.TabIndex = 10;
            this.lblExperienceDays.Text = "Experience Days";
            // 
            // lblTotalEligibleLeavePayDaysText
            // 
            this.lblTotalEligibleLeavePayDaysText.AutoSize = true;
            this.lblTotalEligibleLeavePayDaysText.Location = new System.Drawing.Point(185, 99);
            this.lblTotalEligibleLeavePayDaysText.Name = "lblTotalEligibleLeavePayDaysText";
            this.lblTotalEligibleLeavePayDaysText.Size = new System.Drawing.Size(13, 13);
            this.lblTotalEligibleLeavePayDaysText.TabIndex = 18;
            this.lblTotalEligibleLeavePayDaysText.Text = "0";
            // 
            // txtEligibleLeavePayDays
            // 
            this.txtEligibleLeavePayDays.Location = new System.Drawing.Point(122, 120);
            this.txtEligibleLeavePayDays.MaxLength = 6;
            this.txtEligibleLeavePayDays.Name = "txtEligibleLeavePayDays";
            this.txtEligibleLeavePayDays.Size = new System.Drawing.Size(44, 20);
            this.txtEligibleLeavePayDays.TabIndex = 15;
            this.txtEligibleLeavePayDays.TextChanged += new System.EventHandler(this.txtEligibleLeavePayDays_TextChanged);
            this.txtEligibleLeavePayDays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEligibleLeavePayDays_KeyPress);
            // 
            // dtpProcessDate
            // 
            this.dtpProcessDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpProcessDate.Enabled = false;
            this.dtpProcessDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpProcessDate.Location = new System.Drawing.Point(206, 48);
            this.dtpProcessDate.Name = "dtpProcessDate";
            this.dtpProcessDate.Size = new System.Drawing.Size(100, 20);
            this.dtpProcessDate.TabIndex = 7;
            this.dtpProcessDate.TabStop = false;
            this.dtpProcessDate.Visible = false;
            this.dtpProcessDate.ValueChanged += new System.EventHandler(this.dtpProcessDate_ValueChanged);
            // 
            // lblEligibleLeavePayDays
            // 
            this.lblEligibleLeavePayDays.AutoSize = true;
            this.lblEligibleLeavePayDays.Location = new System.Drawing.Point(12, 124);
            this.lblEligibleLeavePayDays.Name = "lblEligibleLeavePayDays";
            this.lblEligibleLeavePayDays.Size = new System.Drawing.Size(85, 13);
            this.lblEligibleLeavePayDays.TabIndex = 14;
            this.lblEligibleLeavePayDays.Text = "Leave Pay Days";
            // 
            // lblExperienceDaysText
            // 
            this.lblExperienceDaysText.AutoSize = true;
            this.lblExperienceDaysText.Location = new System.Drawing.Point(419, 47);
            this.lblExperienceDaysText.Name = "lblExperienceDaysText";
            this.lblExperienceDaysText.Size = new System.Drawing.Size(13, 13);
            this.lblExperienceDaysText.TabIndex = 11;
            this.lblExperienceDaysText.Text = "0";
            // 
            // dtpJoiningDate
            // 
            this.dtpJoiningDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpJoiningDate.Enabled = false;
            this.dtpJoiningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpJoiningDate.Location = new System.Drawing.Point(207, 19);
            this.dtpJoiningDate.Name = "dtpJoiningDate";
            this.dtpJoiningDate.Size = new System.Drawing.Size(100, 20);
            this.dtpJoiningDate.TabIndex = 5;
            this.dtpJoiningDate.TabStop = false;
            // 
            // lblNoOfDays
            // 
            this.lblNoOfDays.AutoSize = true;
            this.lblNoOfDays.Location = new System.Drawing.Point(88, 54);
            this.lblNoOfDays.Name = "lblNoOfDays";
            this.lblNoOfDays.Size = new System.Drawing.Size(13, 13);
            this.lblNoOfDays.TabIndex = 3;
            this.lblNoOfDays.Text = "0";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(132, 23);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(66, 13);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "Joining Date";
            // 
            // lblEligibleLeavesText
            // 
            this.lblEligibleLeavesText.AutoSize = true;
            this.lblEligibleLeavesText.Location = new System.Drawing.Point(88, 25);
            this.lblEligibleLeavesText.Name = "lblEligibleLeavesText";
            this.lblEligibleLeavesText.Size = new System.Drawing.Size(13, 13);
            this.lblEligibleLeavesText.TabIndex = 1;
            this.lblEligibleLeavesText.Text = "0";
            // 
            // lblIsPaid
            // 
            this.lblIsPaid.AutoSize = true;
            this.lblIsPaid.Location = new System.Drawing.Point(162, 109);
            this.lblIsPaid.Name = "lblIsPaid";
            this.lblIsPaid.Size = new System.Drawing.Size(0, 13);
            this.lblIsPaid.TabIndex = 1022;
            this.lblIsPaid.Visible = false;
            // 
            // lblEligibleLeaves
            // 
            this.lblEligibleLeaves.AutoSize = true;
            this.lblEligibleLeaves.Location = new System.Drawing.Point(12, 25);
            this.lblEligibleLeaves.Name = "lblEligibleLeaves";
            this.lblEligibleLeaves.Size = new System.Drawing.Size(78, 13);
            this.lblEligibleLeaves.TabIndex = 0;
            this.lblEligibleLeaves.Text = "Eligible Leaves";
            // 
            // lblAdditionLeaves
            // 
            this.lblAdditionLeaves.AutoSize = true;
            this.lblAdditionLeaves.Location = new System.Drawing.Point(328, 19);
            this.lblAdditionLeaves.Name = "lblAdditionLeaves";
            this.lblAdditionLeaves.Size = new System.Drawing.Size(79, 13);
            this.lblAdditionLeaves.TabIndex = 8;
            this.lblAdditionLeaves.Text = "Extended Days";
            // 
            // lblAdditionalLeavesText
            // 
            this.lblAdditionalLeavesText.AutoSize = true;
            this.lblAdditionalLeavesText.Location = new System.Drawing.Point(419, 19);
            this.lblAdditionalLeavesText.Name = "lblAdditionalLeavesText";
            this.lblAdditionalLeavesText.Size = new System.Drawing.Size(13, 13);
            this.lblAdditionalLeavesText.TabIndex = 9;
            this.lblAdditionalLeavesText.Text = "0";
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape4});
            this.shapeContainer3.Size = new System.Drawing.Size(489, 239);
            this.shapeContainer3.TabIndex = 1052;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 61;
            this.lineShape2.X2 = 484;
            this.lineShape2.Y1 = 191;
            this.lineShape2.Y2 = 191;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 33;
            this.lineShape4.X2 = 199;
            this.lineShape4.Y1 = 71;
            this.lineShape4.Y2 = 71;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkIsPaid);
            this.groupBox2.Controls.Add(this.chkProcessSalaryForCurrentMonth);
            this.groupBox2.Controls.Add(this.chkConsiderAbsentDays);
            this.groupBox2.Controls.Add(this.lblActualRejoinDate);
            this.groupBox2.Controls.Add(this.lblActualRejoinDateDec);
            this.groupBox2.Controls.Add(this.chkEncashOnly);
            this.groupBox2.Controls.Add(this.dtpToDate);
            this.groupBox2.Controls.Add(this.dtpRejoinDate);
            this.groupBox2.Controls.Add(this.lblRejoinDaate);
            this.groupBox2.Controls.Add(this.lblFromDate);
            this.groupBox2.Controls.Add(this.lblToDate);
            this.groupBox2.Controls.Add(this.dtpFromDate);
            this.groupBox2.Location = new System.Drawing.Point(19, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(494, 100);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // chkIsPaid
            // 
            this.chkIsPaid.AutoSize = true;
            this.chkIsPaid.Location = new System.Drawing.Point(226, 77);
            this.chkIsPaid.Name = "chkIsPaid";
            this.chkIsPaid.Size = new System.Drawing.Size(60, 17);
            this.chkIsPaid.TabIndex = 8;
            this.chkIsPaid.Text = "Unpaid";
            this.chkIsPaid.UseVisualStyleBackColor = true;
            // 
            // chkProcessSalaryForCurrentMonth
            // 
            this.chkProcessSalaryForCurrentMonth.AutoSize = true;
            this.chkProcessSalaryForCurrentMonth.Location = new System.Drawing.Point(295, 77);
            this.chkProcessSalaryForCurrentMonth.Name = "chkProcessSalaryForCurrentMonth";
            this.chkProcessSalaryForCurrentMonth.Size = new System.Drawing.Size(193, 17);
            this.chkProcessSalaryForCurrentMonth.TabIndex = 2;
            this.chkProcessSalaryForCurrentMonth.Text = "Worked Days Current Month Salary";
            this.chkProcessSalaryForCurrentMonth.UseVisualStyleBackColor = true;
            this.chkProcessSalaryForCurrentMonth.CheckedChanged += new System.EventHandler(this.chkProcessSalaryForCurrentMonth_CheckedChanged);
            // 
            // chkConsiderAbsentDays
            // 
            this.chkConsiderAbsentDays.AutoSize = true;
            this.chkConsiderAbsentDays.Location = new System.Drawing.Point(90, 77);
            this.chkConsiderAbsentDays.Name = "chkConsiderAbsentDays";
            this.chkConsiderAbsentDays.Size = new System.Drawing.Size(130, 17);
            this.chkConsiderAbsentDays.TabIndex = 1;
            this.chkConsiderAbsentDays.Text = "Consider Absent Days";
            this.chkConsiderAbsentDays.UseVisualStyleBackColor = true;
            this.chkConsiderAbsentDays.CheckedChanged += new System.EventHandler(this.chkConsiderAbsentDays_CheckedChanged);
            // 
            // lblActualRejoinDate
            // 
            this.lblActualRejoinDate.AutoSize = true;
            this.lblActualRejoinDate.Location = new System.Drawing.Point(356, 48);
            this.lblActualRejoinDate.Name = "lblActualRejoinDate";
            this.lblActualRejoinDate.Size = new System.Drawing.Size(0, 13);
            this.lblActualRejoinDate.TabIndex = 7;
            // 
            // lblActualRejoinDateDec
            // 
            this.lblActualRejoinDateDec.AutoSize = true;
            this.lblActualRejoinDateDec.Location = new System.Drawing.Point(264, 47);
            this.lblActualRejoinDateDec.Name = "lblActualRejoinDateDec";
            this.lblActualRejoinDateDec.Size = new System.Drawing.Size(96, 13);
            this.lblActualRejoinDateDec.TabIndex = 6;
            this.lblActualRejoinDateDec.Text = "Actual Rejoin Date";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(359, 14);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(109, 20);
            this.dtpToDate.TabIndex = 3;
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Location = new System.Drawing.Point(12, 17);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(56, 13);
            this.lblFromDate.TabIndex = 0;
            this.lblFromDate.Text = "From Date";
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.Location = new System.Drawing.Point(264, 17);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(46, 13);
            this.lblToDate.TabIndex = 2;
            this.lblToDate.Text = "To Date";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(81, 14);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(109, 20);
            this.dtpFromDate.TabIndex = 1;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            // 
            // lblTicketAmount
            // 
            this.lblTicketAmount.AutoSize = true;
            this.lblTicketAmount.Location = new System.Drawing.Point(779, 329);
            this.lblTicketAmount.Name = "lblTicketAmount";
            this.lblTicketAmount.Size = new System.Drawing.Size(81, 13);
            this.lblTicketAmount.TabIndex = 22;
            this.lblTicketAmount.Text = "Ticket Expense";
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Location = new System.Drawing.Point(431, 398);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(85, 25);
            this.btnProcess.TabIndex = 5;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // lblIssuedTickets
            // 
            this.lblIssuedTickets.AutoSize = true;
            this.lblIssuedTickets.Location = new System.Drawing.Point(559, 329);
            this.lblIssuedTickets.Name = "lblIssuedTickets";
            this.lblIssuedTickets.Size = new System.Drawing.Size(107, 13);
            this.lblIssuedTickets.TabIndex = 20;
            this.lblIssuedTickets.Text = "No Of Tickets Issued";
            // 
            // txtNoOfTicketsIssued
            // 
            this.txtNoOfTicketsIssued.Location = new System.Drawing.Point(669, 326);
            this.txtNoOfTicketsIssued.MaxLength = 9;
            this.txtNoOfTicketsIssued.Name = "txtNoOfTicketsIssued";
            this.txtNoOfTicketsIssued.Size = new System.Drawing.Size(104, 20);
            this.txtNoOfTicketsIssued.TabIndex = 21;
            this.txtNoOfTicketsIssued.TextChanged += new System.EventHandler(this.txtNoOfTicketsIssued_TextChanged);
            this.txtNoOfTicketsIssued.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoOfTicketsIssued_KeyPress);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3});
            this.shapeContainer2.Size = new System.Drawing.Size(511, 429);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 84;
            this.lineShape3.X2 = 503;
            this.lineShape3.Y1 = 18;
            this.lineShape3.Y2 = 18;
            // 
            // txtTicketAmount
            // 
            this.txtTicketAmount.Location = new System.Drawing.Point(861, 326);
            this.txtTicketAmount.MaxLength = 9;
            this.txtTicketAmount.Name = "txtTicketAmount";
            this.txtTicketAmount.Size = new System.Drawing.Size(102, 20);
            this.txtTicketAmount.TabIndex = 23;
            this.txtTicketAmount.TextChanged += new System.EventHandler(this.txtTicketAmount_TextChanged);
            this.txtTicketAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTicketAmount_KeyPress);
            // 
            // tabParticulars
            // 
            this.tabParticulars.Controls.Add(this.ChkParticulars);
            this.tabParticulars.Controls.Add(lblDeductions);
            this.tabParticulars.Controls.Add(this.TextboxNumeric1);
            this.tabParticulars.Controls.Add(lblGivenAmount);
            this.tabParticulars.Controls.Add(this.TextboxNumeric);
            this.tabParticulars.Controls.Add(this.txtGivenAmount);
            this.tabParticulars.Controls.Add(this.lblRemarks);
            this.tabParticulars.Controls.Add(this.txtRemarks);
            this.tabParticulars.Controls.Add(lblNetAmount);
            this.tabParticulars.Controls.Add(lblAdditions);
            this.tabParticulars.Controls.Add(this.txtNetAmount);
            this.tabParticulars.Controls.Add(this.txtTotalAddition);
            this.tabParticulars.Controls.Add(this.txtTotalDeduction);
            this.tabParticulars.Controls.Add(this.Label4);
            this.tabParticulars.Controls.Add(this.lblCurrency);
            this.tabParticulars.Controls.Add(this.dgvVacationDetails);
            this.tabParticulars.Controls.Add(lblParticulars);
            this.tabParticulars.Controls.Add(this.btnAdditionDeductions);
            this.tabParticulars.Controls.Add(this.txtAmount);
            this.tabParticulars.Controls.Add(lblAmount);
            this.tabParticulars.Controls.Add(this.btnAdd);
            this.tabParticulars.Controls.Add(this.cboParticulars);
            this.tabParticulars.Controls.Add(Label2);
            this.tabParticulars.Controls.Add(this.shapeContainer4);
            this.tabParticulars.Location = new System.Drawing.Point(4, 22);
            this.tabParticulars.Name = "tabParticulars";
            this.tabParticulars.Padding = new System.Windows.Forms.Padding(3);
            this.tabParticulars.Size = new System.Drawing.Size(517, 435);
            this.tabParticulars.TabIndex = 1;
            this.tabParticulars.Text = "Particulars";
            this.tabParticulars.UseVisualStyleBackColor = true;
            // 
            // ChkParticulars
            // 
            this.ChkParticulars.AutoSize = true;
            this.ChkParticulars.Location = new System.Drawing.Point(8, 55);
            this.ChkParticulars.Name = "ChkParticulars";
            this.ChkParticulars.Size = new System.Drawing.Size(166, 17);
            this.ChkParticulars.TabIndex = 1010;
            this.ChkParticulars.Text = "Add particulars to \"salary slip\"";
            this.ChkParticulars.UseVisualStyleBackColor = true;
            // 
            // TextboxNumeric1
            // 
            this.TextboxNumeric1.Location = new System.Drawing.Point(304, 339);
            this.TextboxNumeric1.Name = "TextboxNumeric1";
            this.TextboxNumeric1.Size = new System.Drawing.Size(29, 20);
            this.TextboxNumeric1.TabIndex = 1009;
            this.TextboxNumeric1.Visible = false;
            // 
            // TextboxNumeric
            // 
            this.TextboxNumeric.Location = new System.Drawing.Point(385, 339);
            this.TextboxNumeric.Name = "TextboxNumeric";
            this.TextboxNumeric.Size = new System.Drawing.Size(29, 20);
            this.TextboxNumeric.TabIndex = 1008;
            this.TextboxNumeric.Visible = false;
            // 
            // txtGivenAmount
            // 
            this.txtGivenAmount.Enabled = false;
            this.txtGivenAmount.Location = new System.Drawing.Point(430, 262);
            this.txtGivenAmount.Name = "txtGivenAmount";
            this.txtGivenAmount.Size = new System.Drawing.Size(85, 20);
            this.txtGivenAmount.TabIndex = 16;
            this.txtGivenAmount.TabStop = false;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(4, 299);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 17;
            this.lblRemarks.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(68, 302);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRemarks.Size = new System.Drawing.Size(448, 78);
            this.txtRemarks.TabIndex = 18;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // txtNetAmount
            // 
            this.txtNetAmount.Enabled = false;
            this.txtNetAmount.Location = new System.Drawing.Point(256, 262);
            this.txtNetAmount.Name = "txtNetAmount";
            this.txtNetAmount.Size = new System.Drawing.Size(89, 20);
            this.txtNetAmount.TabIndex = 14;
            this.txtNetAmount.TabStop = false;
            // 
            // txtTotalAddition
            // 
            this.txtTotalAddition.Enabled = false;
            this.txtTotalAddition.Location = new System.Drawing.Point(256, 235);
            this.txtTotalAddition.Name = "txtTotalAddition";
            this.txtTotalAddition.Size = new System.Drawing.Size(89, 20);
            this.txtTotalAddition.TabIndex = 10;
            this.txtTotalAddition.TabStop = false;
            // 
            // txtTotalDeduction
            // 
            this.txtTotalDeduction.Enabled = false;
            this.txtTotalDeduction.Location = new System.Drawing.Point(429, 235);
            this.txtTotalDeduction.Name = "txtTotalDeduction";
            this.txtTotalDeduction.Size = new System.Drawing.Size(85, 20);
            this.txtTotalDeduction.TabIndex = 12;
            this.txtTotalDeduction.TabStop = false;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(4, 238);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(49, 13);
            this.Label4.TabIndex = 7;
            this.Label4.Text = "Currency";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(69, 238);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(26, 13);
            this.lblCurrency.TabIndex = 8;
            this.lblCurrency.Text = "INR";
            // 
            // dgvVacationDetails
            // 
            this.dgvVacationDetails.AllowUserToAddRows = false;
            this.dgvVacationDetails.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvVacationDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVacationDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColVacationDetailID,
            this.dgvColParticulars,
            this.dgvColAmount,
            this.dgvColIsAddition,
            this.dgvColNoOfDays,
            this.dgvColReleaseLater,
            this.dgvColTempAmount,
            this.ActualAmt});
            this.dgvVacationDetails.Location = new System.Drawing.Point(6, 77);
            this.dgvVacationDetails.MultiSelect = false;
            this.dgvVacationDetails.Name = "dgvVacationDetails";
            this.dgvVacationDetails.RowHeadersWidth = 35;
            this.dgvVacationDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvVacationDetails.Size = new System.Drawing.Size(510, 152);
            this.dgvVacationDetails.TabIndex = 6;
            this.dgvVacationDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVacationDetails_CellValueChanged);
            this.dgvVacationDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvVacationDetails_CellBeginEdit);
            this.dgvVacationDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVacationDetails_CellEndEdit);
            this.dgvVacationDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvVacationDetails_EditingControlShowing);
            this.dgvVacationDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvVacationDetails_CurrentCellDirtyStateChanged);
            this.dgvVacationDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvVacationDetails_DataError);
            this.dgvVacationDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvVacationDetails_KeyDown);
            this.dgvVacationDetails.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvVacationDetails_RowsRemoved);
            // 
            // dgvColVacationDetailID
            // 
            this.dgvColVacationDetailID.HeaderText = "VacationDetailID";
            this.dgvColVacationDetailID.Name = "dgvColVacationDetailID";
            this.dgvColVacationDetailID.Visible = false;
            // 
            // dgvColParticulars
            // 
            this.dgvColParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColParticulars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgvColParticulars.HeaderText = "Particular";
            this.dgvColParticulars.Name = "dgvColParticulars";
            this.dgvColParticulars.ReadOnly = true;
            this.dgvColParticulars.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dgvColAmount
            // 
            this.dgvColAmount.HeaderText = "Amount";
            this.dgvColAmount.MaxInputLength = 8;
            this.dgvColAmount.Name = "dgvColAmount";
            // 
            // dgvColIsAddition
            // 
            this.dgvColIsAddition.HeaderText = "IsAddition";
            this.dgvColIsAddition.Name = "dgvColIsAddition";
            this.dgvColIsAddition.Visible = false;
            // 
            // dgvColNoOfDays
            // 
            this.dgvColNoOfDays.HeaderText = "NoOfDays";
            this.dgvColNoOfDays.Name = "dgvColNoOfDays";
            // 
            // dgvColReleaseLater
            // 
            this.dgvColReleaseLater.HeaderText = "Release Later";
            this.dgvColReleaseLater.Name = "dgvColReleaseLater";
            this.dgvColReleaseLater.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvColReleaseLater.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvColReleaseLater.Width = 80;
            // 
            // dgvColTempAmount
            // 
            this.dgvColTempAmount.HeaderText = "Amount";
            this.dgvColTempAmount.MaxInputLength = 20;
            this.dgvColTempAmount.Name = "dgvColTempAmount";
            this.dgvColTempAmount.Visible = false;
            // 
            // ActualAmt
            // 
            this.ActualAmt.HeaderText = "ActualAmt";
            this.ActualAmt.Name = "ActualAmt";
            this.ActualAmt.Visible = false;
            // 
            // btnAdditionDeductions
            // 
            this.btnAdditionDeductions.Location = new System.Drawing.Point(232, 26);
            this.btnAdditionDeductions.Name = "btnAdditionDeductions";
            this.btnAdditionDeductions.Size = new System.Drawing.Size(30, 24);
            this.btnAdditionDeductions.TabIndex = 2;
            this.btnAdditionDeductions.Text = "...";
            this.btnAdditionDeductions.UseVisualStyleBackColor = true;
            this.btnAdditionDeductions.Click += new System.EventHandler(this.btnAdditionDeductions_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(342, 29);
            this.txtAmount.MaxLength = 8;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(108, 20);
            this.txtAmount.TabIndex = 4;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(465, 26);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(50, 24);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cboParticulars
            // 
            this.cboParticulars.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboParticulars.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboParticulars.BackColor = System.Drawing.Color.White;
            this.cboParticulars.DropDownHeight = 134;
            this.cboParticulars.FormattingEnabled = true;
            this.cboParticulars.IntegralHeight = false;
            this.cboParticulars.Location = new System.Drawing.Point(74, 29);
            this.cboParticulars.Name = "cboParticulars";
            this.cboParticulars.Size = new System.Drawing.Size(156, 21);
            this.cboParticulars.TabIndex = 1;
            // 
            // shapeContainer4
            // 
            this.shapeContainer4.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer4.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer4.Name = "shapeContainer4";
            this.shapeContainer4.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.shapeContainer4.Size = new System.Drawing.Size(511, 429);
            this.shapeContainer4.TabIndex = 0;
            this.shapeContainer4.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 10;
            this.LineShape1.X2 = 509;
            this.LineShape1.Y1 = 22;
            this.LineShape1.Y2 = 22;
            // 
            // tabHistory
            // 
            this.tabHistory.Controls.Add(this.dgvEmpHistory);
            this.tabHistory.Location = new System.Drawing.Point(4, 22);
            this.tabHistory.Name = "tabHistory";
            this.tabHistory.Size = new System.Drawing.Size(517, 435);
            this.tabHistory.TabIndex = 2;
            this.tabHistory.Text = "Vacation History";
            this.tabHistory.UseVisualStyleBackColor = true;
            // 
            // dgvEmpHistory
            // 
            this.dgvEmpHistory.AllowUserToAddRows = false;
            this.dgvEmpHistory.AllowUserToDeleteRows = false;
            this.dgvEmpHistory.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvEmpHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmpHistory.Location = new System.Drawing.Point(3, 3);
            this.dgvEmpHistory.Name = "dgvEmpHistory";
            this.dgvEmpHistory.ReadOnly = true;
            this.dgvEmpHistory.RowHeadersVisible = false;
            this.dgvEmpHistory.Size = new System.Drawing.Size(516, 396);
            this.dgvEmpHistory.TabIndex = 1052;
            // 
            // lblSearchEmployee
            // 
            this.lblSearchEmployee.AutoSize = true;
            this.lblSearchEmployee.Location = new System.Drawing.Point(283, 38);
            this.lblSearchEmployee.Name = "lblSearchEmployee";
            this.lblSearchEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblSearchEmployee.TabIndex = 221;
            this.lblSearchEmployee.Text = "Employee";
            // 
            // cboEmployee
            // 
            this.cboEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(352, 35);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(333, 21);
            this.cboEmployee.TabIndex = 0;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployee_KeyDown);
            // 
            // cboAccount
            // 
            this.cboAccount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAccount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAccount.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cboAccount.DropDownHeight = 134;
            this.cboAccount.FormattingEnabled = true;
            this.cboAccount.IntegralHeight = false;
            this.cboAccount.Location = new System.Drawing.Point(445, 534);
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Size = new System.Drawing.Size(10, 21);
            this.cboAccount.TabIndex = 1042;
            this.cboAccount.Visible = false;
            this.cboAccount.SelectedIndexChanged += new System.EventHandler(this.cboAccount_SelectedIndexChanged);
            this.cboAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboAccount_KeyDown);
            // 
            // errorProviderVacation
            // 
            this.errorProviderVacation.ContainerControl = this;
            this.errorProviderVacation.RightToLeft = true;
            // 
            // tmrVacation
            // 
            this.tmrVacation.Interval = 2000;
            this.tmrVacation.Tick += new System.EventHandler(this.tmrVacation_Tick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(266, 532);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // ssVacation
            // 
            this.ssVacation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.ssVacation.Location = new System.Drawing.Point(0, 562);
            this.ssVacation.Name = "ssVacation";
            this.ssVacation.Size = new System.Drawing.Size(787, 22);
            this.ssVacation.TabIndex = 227;
            this.ssVacation.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(708, 532);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(627, 532);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "C&onfirm ";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bnEmail
            // 
            this.bnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnEmail.Image = ((System.Drawing.Image)(resources.GetObject("bnEmail.Image")));
            this.bnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnEmail.Name = "bnEmail";
            this.bnEmail.Size = new System.Drawing.Size(23, 22);
            this.bnEmail.ToolTipText = "Email";
            this.bnEmail.Click += new System.EventHandler(this.bnEmail_Click);
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.Text = "Add new";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // BindingNavigatorVacationProcess
            // 
            this.BindingNavigatorVacationProcess.AddNewItem = null;
            this.BindingNavigatorVacationProcess.CountItem = null;
            this.BindingNavigatorVacationProcess.DeleteItem = null;
            this.BindingNavigatorVacationProcess.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnAddNewItem,
            this.bnSaveItem,
            this.bnDeleteItem,
            this.bnClearItem,
            this.ToolStripSeparator3,
            this.bnVacationExtension,
            this.ToolStripSeparator1,
            this.bnPrint,
            this.bnEmail,
            this.ToolStripSeparator2,
            this.BtnHelp});
            this.BindingNavigatorVacationProcess.Location = new System.Drawing.Point(258, 0);
            this.BindingNavigatorVacationProcess.MoveFirstItem = null;
            this.BindingNavigatorVacationProcess.MoveLastItem = null;
            this.BindingNavigatorVacationProcess.MoveNextItem = null;
            this.BindingNavigatorVacationProcess.MovePreviousItem = null;
            this.BindingNavigatorVacationProcess.Name = "BindingNavigatorVacationProcess";
            this.BindingNavigatorVacationProcess.PositionItem = null;
            this.BindingNavigatorVacationProcess.Size = new System.Drawing.Size(529, 25);
            this.BindingNavigatorVacationProcess.TabIndex = 223;
            this.BindingNavigatorVacationProcess.Text = "BindingNavigator1";
            // 
            // bnSaveItem
            // 
            this.bnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bnSaveItem.Image")));
            this.bnSaveItem.Name = "bnSaveItem";
            this.bnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bnSaveItem.Text = "Save Data";
            this.bnSaveItem.Click += new System.EventHandler(this.bnSaveItem_Click);
            // 
            // bnDeleteItem
            // 
            this.bnDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
            this.bnDeleteItem.Name = "bnDeleteItem";
            this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bnDeleteItem.Text = "Delete";
            this.bnDeleteItem.Click += new System.EventHandler(this.bnDeleteItem_Click);
            // 
            // bnClearItem
            // 
            this.bnClearItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnClearItem.Image = ((System.Drawing.Image)(resources.GetObject("bnClearItem.Image")));
            this.bnClearItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnClearItem.Name = "bnClearItem";
            this.bnClearItem.Size = new System.Drawing.Size(23, 22);
            this.bnClearItem.ToolTipText = "Clear";
            this.bnClearItem.Click += new System.EventHandler(this.bnClearItem_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bnVacationExtension
            // 
            this.bnVacationExtension.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnVacationExtension.Image = global::MyPayfriend.Properties.Resources.VacationEs;
            this.bnVacationExtension.Name = "bnVacationExtension";
            this.bnVacationExtension.RightToLeftAutoMirrorImage = true;
            this.bnVacationExtension.Size = new System.Drawing.Size(23, 22);
            this.bnVacationExtension.Text = "0";
            this.bnVacationExtension.ToolTipText = "Vacation Extension / Confirm Rejoin Date";
            this.bnVacationExtension.Click += new System.EventHandler(this.bnVacationExtension_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnPrint
            // 
            this.bnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnPrint.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.template1ToolStripMenuItem,
            this.template2ToolStripMenuItem});
            this.bnPrint.Image = ((System.Drawing.Image)(resources.GetObject("bnPrint.Image")));
            this.bnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnPrint.Name = "bnPrint";
            this.bnPrint.Size = new System.Drawing.Size(32, 22);
            this.bnPrint.ToolTipText = "Print";
            // 
            // template1ToolStripMenuItem
            // 
            this.template1ToolStripMenuItem.Name = "template1ToolStripMenuItem";
            this.template1ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.template1ToolStripMenuItem.Text = "Template1";
            this.template1ToolStripMenuItem.Click += new System.EventHandler(this.template1ToolStripMenuItem_Click);
            // 
            // template2ToolStripMenuItem
            // 
            this.template2ToolStripMenuItem.Name = "template2ToolStripMenuItem";
            this.template2ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.template2ToolStripMenuItem.Text = "Template2";
            this.template2ToolStripMenuItem.Click += new System.EventHandler(this.template2ToolStripMenuItem_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "VacationDetailID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "IsAddition";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "NoOfDays";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ReleaseLater";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            this.dataGridViewTextBoxColumn6.Width = 175;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "From Date";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "To Date";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 90;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // expLeft
            // 
            this.expLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.expLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expLeft.Controls.Add(this.dgvSearch);
            this.expLeft.Controls.Add(this.pnlTopSearch);
            this.expLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.expLeft.ExpandButtonVisible = false;
            this.expLeft.Location = new System.Drawing.Point(0, 0);
            this.expLeft.Name = "expLeft";
            this.expLeft.Size = new System.Drawing.Size(258, 562);
            this.expLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expLeft.Style.GradientAngle = 90;
            this.expLeft.TabIndex = 1044;
            this.expLeft.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expLeft.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expLeft.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expLeft.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expLeft.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expLeft.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expLeft.TitleStyle.GradientAngle = 90;
            this.expLeft.TitleText = "Search";
            // 
            // dgvSearch
            // 
            this.dgvSearch.AllowUserToAddRows = false;
            this.dgvSearch.AllowUserToDeleteRows = false;
            this.dgvSearch.AllowUserToResizeColumns = false;
            this.dgvSearch.AllowUserToResizeRows = false;
            this.dgvSearch.BackgroundColor = System.Drawing.Color.White;
            this.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VacationID,
            this.EmployeeID,
            this.Employee,
            this.EmployeeArb,
            this.Date});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSearch.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSearch.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSearch.Location = new System.Drawing.Point(0, 165);
            this.dgvSearch.Name = "dgvSearch";
            this.dgvSearch.ReadOnly = true;
            this.dgvSearch.RowHeadersVisible = false;
            this.dgvSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSearch.Size = new System.Drawing.Size(258, 397);
            this.dgvSearch.TabIndex = 1;
            this.dgvSearch.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSearch_CellDoubleClick);
            // 
            // VacationID
            // 
            this.VacationID.DataPropertyName = "VacationID";
            this.VacationID.HeaderText = "VacationID";
            this.VacationID.Name = "VacationID";
            this.VacationID.ReadOnly = true;
            this.VacationID.Visible = false;
            // 
            // EmployeeID
            // 
            this.EmployeeID.DataPropertyName = "EmployeeID";
            this.EmployeeID.HeaderText = "EmployeeID";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.ReadOnly = true;
            this.EmployeeID.Visible = false;
            // 
            // Employee
            // 
            this.Employee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Employee.DataPropertyName = "Employee";
            this.Employee.HeaderText = "Employee";
            this.Employee.Name = "Employee";
            this.Employee.ReadOnly = true;
            // 
            // EmployeeArb
            // 
            this.EmployeeArb.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EmployeeArb.DataPropertyName = "EmployeeArb";
            this.EmployeeArb.HeaderText = "EmployeeArb";
            this.EmployeeArb.Name = "EmployeeArb";
            this.EmployeeArb.ReadOnly = true;
            this.EmployeeArb.Visible = false;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "Date";
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // pnlTopSearch
            // 
            this.pnlTopSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlTopSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlTopSearch.Controls.Add(this.cboSearchEmployee);
            this.pnlTopSearch.Controls.Add(this.label9);
            this.pnlTopSearch.Controls.Add(this.btnSearch);
            this.pnlTopSearch.Controls.Add(this.lblEmployeeCode);
            this.pnlTopSearch.Controls.Add(this.cboSearchCode);
            this.pnlTopSearch.Controls.Add(this.lblSearchTo);
            this.pnlTopSearch.Controls.Add(this.lblSearchFrom);
            this.pnlTopSearch.Controls.Add(this.dtpSearchToDate);
            this.pnlTopSearch.Controls.Add(this.dtpSearchFromDate);
            this.pnlTopSearch.Controls.Add(this.lblSearchCompany);
            this.pnlTopSearch.Controls.Add(this.cboSearchCompany);
            this.pnlTopSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopSearch.Location = new System.Drawing.Point(0, 26);
            this.pnlTopSearch.Name = "pnlTopSearch";
            this.pnlTopSearch.Size = new System.Drawing.Size(258, 139);
            this.pnlTopSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlTopSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlTopSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlTopSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlTopSearch.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlTopSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlTopSearch.Style.GradientAngle = 90;
            this.pnlTopSearch.TabIndex = 2;
            // 
            // cboSearchEmployee
            // 
            this.cboSearchEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchEmployee.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchEmployee.DropDownHeight = 134;
            this.cboSearchEmployee.FormattingEnabled = true;
            this.cboSearchEmployee.IntegralHeight = false;
            this.cboSearchEmployee.Location = new System.Drawing.Point(67, 59);
            this.cboSearchEmployee.Name = "cboSearchEmployee";
            this.cboSearchEmployee.Size = new System.Drawing.Size(183, 21);
            this.cboSearchEmployee.TabIndex = 225;
            this.cboSearchEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 224;
            this.label9.Text = "Employee";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(190, 111);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(60, 23);
            this.btnSearch.TabIndex = 223;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblEmployeeCode
            // 
            this.lblEmployeeCode.AutoSize = true;
            this.lblEmployeeCode.Location = new System.Drawing.Point(8, 36);
            this.lblEmployeeCode.Name = "lblEmployeeCode";
            this.lblEmployeeCode.Size = new System.Drawing.Size(32, 13);
            this.lblEmployeeCode.TabIndex = 220;
            this.lblEmployeeCode.Text = "Code";
            // 
            // cboSearchCode
            // 
            this.cboSearchCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchCode.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchCode.DropDownHeight = 134;
            this.cboSearchCode.FormattingEnabled = true;
            this.cboSearchCode.IntegralHeight = false;
            this.cboSearchCode.Location = new System.Drawing.Point(67, 32);
            this.cboSearchCode.Name = "cboSearchCode";
            this.cboSearchCode.Size = new System.Drawing.Size(183, 21);
            this.cboSearchCode.TabIndex = 219;
            this.cboSearchCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // lblSearchTo
            // 
            this.lblSearchTo.AutoSize = true;
            this.lblSearchTo.Location = new System.Drawing.Point(8, 116);
            this.lblSearchTo.Name = "lblSearchTo";
            this.lblSearchTo.Size = new System.Drawing.Size(20, 13);
            this.lblSearchTo.TabIndex = 218;
            this.lblSearchTo.Text = "To";
            // 
            // lblSearchFrom
            // 
            this.lblSearchFrom.AutoSize = true;
            this.lblSearchFrom.Location = new System.Drawing.Point(8, 90);
            this.lblSearchFrom.Name = "lblSearchFrom";
            this.lblSearchFrom.Size = new System.Drawing.Size(30, 13);
            this.lblSearchFrom.TabIndex = 217;
            this.lblSearchFrom.Text = "From";
            // 
            // dtpSearchToDate
            // 
            this.dtpSearchToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpSearchToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSearchToDate.Location = new System.Drawing.Point(67, 112);
            this.dtpSearchToDate.Name = "dtpSearchToDate";
            this.dtpSearchToDate.Size = new System.Drawing.Size(106, 20);
            this.dtpSearchToDate.TabIndex = 216;
            // 
            // dtpSearchFromDate
            // 
            this.dtpSearchFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpSearchFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSearchFromDate.Location = new System.Drawing.Point(67, 86);
            this.dtpSearchFromDate.Name = "dtpSearchFromDate";
            this.dtpSearchFromDate.Size = new System.Drawing.Size(106, 20);
            this.dtpSearchFromDate.TabIndex = 215;
            // 
            // lblSearchCompany
            // 
            this.lblSearchCompany.AutoSize = true;
            this.lblSearchCompany.Location = new System.Drawing.Point(8, 9);
            this.lblSearchCompany.Name = "lblSearchCompany";
            this.lblSearchCompany.Size = new System.Drawing.Size(51, 13);
            this.lblSearchCompany.TabIndex = 3;
            this.lblSearchCompany.Text = "Company";
            // 
            // cboSearchCompany
            // 
            this.cboSearchCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboSearchCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSearchCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboSearchCompany.DropDownHeight = 134;
            this.cboSearchCompany.FormattingEnabled = true;
            this.cboSearchCompany.IntegralHeight = false;
            this.cboSearchCompany.Location = new System.Drawing.Point(67, 5);
            this.cboSearchCompany.Name = "cboSearchCompany";
            this.cboSearchCompany.Size = new System.Drawing.Size(183, 21);
            this.cboSearchCompany.TabIndex = 2;
            this.cboSearchCompany.SelectedIndexChanged += new System.EventHandler(this.cboSearchCompany_SelectedIndexChanged);
            this.cboSearchCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cb_KeyPress);
            // 
            // dtpDateValidate
            // 
            this.dtpDateValidate.Location = new System.Drawing.Point(863, 149);
            this.dtpDateValidate.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpDateValidate.Name = "dtpDateValidate";
            this.dtpDateValidate.Size = new System.Drawing.Size(52, 20);
            this.dtpDateValidate.TabIndex = 1045;
            // 
            // FrmVacationEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 584);
            this.Controls.Add(this.dtpDateValidate);
            this.Controls.Add(this.tabVacationProcess);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.BindingNavigatorVacationProcess);
            this.Controls.Add(this.lblSearchEmployee);
            this.Controls.Add(this.expLeft);
            this.Controls.Add(this.cboEmployee);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ssVacation);
            this.Controls.Add(lblAccount);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.cboAccount);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmVacationEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vacation Process";
            this.Load += new System.EventHandler(this.FrmVacationEntry_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmVacationEntry_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmVacationEntry_KeyDown);
            this.tabVacationProcess.ResumeLayout(false);
            this.tabEmployeeInfo.ResumeLayout(false);
            this.tabEmployeeInfo.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabParticulars.ResumeLayout(false);
            this.tabParticulars.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVacationDetails)).EndInit();
            this.tabHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderVacation)).EndInit();
            this.ssVacation.ResumeLayout(false);
            this.ssVacation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorVacationProcess)).EndInit();
            this.BindingNavigatorVacationProcess.ResumeLayout(false);
            this.BindingNavigatorVacationProcess.PerformLayout();
            this.expLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            this.pnlTopSearch.ResumeLayout(false);
            this.pnlTopSearch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColAmount;
        internal System.Windows.Forms.Label lblRejoinDaate;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColIsAddition;
        internal System.Windows.Forms.DateTimePicker dtpRejoinDate;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColNoOfDays;
        //internal System.Windows.Forms.DataGridViewComboBoxColumn dgvColParticulars;
        internal System.Windows.Forms.CheckBox chkEncashOnly;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColVacationDetailID;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.Label lblTotalEligibleLeavePayDays;
        internal System.Windows.Forms.Label lblIsPaid;
        internal System.Windows.Forms.Label lblTotalEligibleLeavePayDaysText;
        internal System.Windows.Forms.TextBox txtEligibleLeavePayDays;
        internal System.Windows.Forms.Label lblEligibleLeavePayDays;
        internal System.Windows.Forms.CheckBox chkConsiderAbsentDays;
        internal System.Windows.Forms.Label lblAbsentDays;
        internal System.Windows.Forms.Label lblAbsentDaysText;
        internal System.Windows.Forms.Label lblExperienceDays;
        internal System.Windows.Forms.Label lblExperienceDaysText;
        internal System.Windows.Forms.TextBox txtGivenAmount;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox txtTicketAmount;
        internal System.Windows.Forms.Label lblTicketAmount;
        internal System.Windows.Forms.Label lblCurrency;
        internal System.Windows.Forms.DateTimePicker dtpJoiningDate;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.CheckBox chkProcessSalaryForCurrentMonth;
        internal System.Windows.Forms.ComboBox cboParticulars;
        internal System.Windows.Forms.Label lblAdditionLeaves;
        internal System.Windows.Forms.Label lblAdditionalLeavesText;
        internal System.Windows.Forms.TextBox txtNoOfTicketsIssued;
        internal System.Windows.Forms.Label lblIssuedTickets;
        internal System.Windows.Forms.Label lblEligibleLeaves;
        internal System.Windows.Forms.Label lblEligibleLeavesText;
        internal System.Windows.Forms.Label lblVacationDays;
        internal System.Windows.Forms.TextBox txtNetAmount;
        internal System.Windows.Forms.TextBox txtTotalDeduction;
        internal System.Windows.Forms.TextBox txtTotalAddition;
        internal System.Windows.Forms.ComboBox cboEmployee;
        internal System.Windows.Forms.Label lblFromDate;
        internal System.Windows.Forms.Label lblToDate;
        internal System.Windows.Forms.DateTimePicker dtpFromDate;
        internal System.Windows.Forms.DateTimePicker dtpToDate;
        internal System.Windows.Forms.Label lblNoOfDays;
        internal System.Windows.Forms.DateTimePicker dtpProcessDate;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.Label lblRemarks;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.Button btnProcess;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.TextBox TextboxNumeric;
        internal System.Windows.Forms.TextBox TextboxNumeric1;
        //internal System.Windows.Forms.DataGridViewCheckBoxColumn dgvColReleaseLater;
        //internal System.Windows.Forms.DataGridViewTextBoxColumn dgvColTempAmount;
        internal System.Windows.Forms.ErrorProvider errorProviderVacation;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.StatusStrip ssVacation;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.BindingNavigator BindingNavigatorVacationProcess;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.ToolStripButton bnSaveItem;
        internal System.Windows.Forms.ToolStripButton bnDeleteItem;
        internal System.Windows.Forms.ToolStripButton bnClearItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton bnVacationExtension;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton bnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.Timer tmrVacation;
                                                    
        //private DemoClsDataGridview.ClsDataGirdView DetailsDataGridViewVacation;
        //private System.Windows.Forms.DataGridViewTextBoxColumn colAbsentAddDedID;
        //private System.Windows.Forms.DataGridViewCheckBoxColumn colAbsentSelectedItem;
        //private System.Windows.Forms.DataGridViewTextBoxColumn colAbsentParticulars;
        private System.Windows.Forms.DataGridView dgvVacationDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.TextBox txtChequeNo;
        internal System.Windows.Forms.DateTimePicker dtpChequeDate;
        internal System.Windows.Forms.ComboBox cboAccount;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.ComboBox cboTransactionType;
        internal System.Windows.Forms.Button btnAdditionDeductions;
        private System.Windows.Forms.TabControl tabVacationProcess;
        private System.Windows.Forms.TabPage tabEmployeeInfo;
        private System.Windows.Forms.TabPage tabParticulars;
        private System.Windows.Forms.TabPage tabHistory;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer4;
        private System.Windows.Forms.DataGridView dgvEmpHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.TextBox txtEncashComboOffDay;
        internal System.Windows.Forms.Label lblActualRejoinDateDec;
        internal System.Windows.Forms.Label lblActualRejoinDate;
        internal System.Windows.Forms.ComboBox cboVacationPolicy;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.CheckBox chkAgent;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColVacationDetailID;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvColParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColIsAddition;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColNoOfDays;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgvColReleaseLater;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColTempAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualAmt;
        private DevComponents.DotNetBar.ExpandablePanel expLeft;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSearch;
        private DevComponents.DotNetBar.PanelEx pnlTopSearch;
        internal System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.Label lblSearchEmployee;
        internal System.Windows.Forms.Label lblEmployeeCode;
        internal System.Windows.Forms.ComboBox cboSearchCode;
        internal System.Windows.Forms.Label lblSearchTo;
        internal System.Windows.Forms.Label lblSearchFrom;
        internal System.Windows.Forms.DateTimePicker dtpSearchToDate;
        internal System.Windows.Forms.DateTimePicker dtpSearchFromDate;
        internal System.Windows.Forms.Label lblSearchCompany;
        internal System.Windows.Forms.ComboBox cboSearchCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn VacationID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Employee;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeArb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        internal System.Windows.Forms.Button btnShow;
        internal System.Windows.Forms.ComboBox cboSearchEmployee;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.ToolStripSplitButton bnPrint;
        private System.Windows.Forms.ToolStripMenuItem template1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem template2ToolStripMenuItem;
        private System.Windows.Forms.DateTimePicker dtpDateValidate;
        internal System.Windows.Forms.CheckBox ChkFullMonthSalary;
        internal System.Windows.Forms.CheckBox ChkParticulars;
        internal System.Windows.Forms.CheckBox chkIsPaid;
        
    }
}