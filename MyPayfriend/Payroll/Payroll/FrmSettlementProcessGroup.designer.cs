﻿namespace MyPayfriend
{
    partial class FrmSettlementProcessGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettlementProcessGroup));
            this.ToolStripStatus = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.pnlMiddle = new DevComponents.DotNetBar.PanelEx();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.dgvSettlementDetails = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.SelectAll = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.expandableSplitter1 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlLeft = new DevComponents.DotNetBar.PanelEx();
            this.expPnlSettlementProcess = new DevComponents.DotNetBar.ExpandablePanel();
            this.doj = new System.Windows.Forms.DateTimePicker();
            this.chkIncludeCompany = new System.Windows.Forms.CheckBox();
            this.cboBranch = new System.Windows.Forms.ComboBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.showProgressBar = new System.Windows.Forms.ProgressBar();
            this.DtpLastWorkingDate = new System.Windows.Forms.DateTimePicker();
            this.chkAdvanceSearch = new System.Windows.Forms.CheckBox();
            this.dtpResignationDate = new System.Windows.Forms.DateTimePicker();
            this.cboDepartment = new System.Windows.Forms.ComboBox();
            this.lblToDate = new System.Windows.Forms.Label();
            this.lblDept = new System.Windows.Forms.Label();
            this.cboDesignation = new System.Windows.Forms.ComboBox();
            this.lblDesg = new System.Windows.Forms.Label();
            this.btnShow = new System.Windows.Forms.Button();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.ItemPanelMiddileBottom = new DevComponents.DotNetBar.Bar();
            this.cboCountItem = new DevComponents.DotNetBar.ComboBoxItem();
            this.btnFirstGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.btnPreviousGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.lblPageCount = new DevComponents.DotNetBar.LabelItem();
            this.btnNextGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.btnLastGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.LabelItem24 = new DevComponents.DotNetBar.LabelItem();
            this.LabelItem25 = new DevComponents.DotNetBar.LabelItem();
            this.LabelItem26 = new DevComponents.DotNetBar.LabelItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.labelItem5 = new DevComponents.DotNetBar.LabelItem();
            this.labelItem4 = new DevComponents.DotNetBar.LabelItem();
            this.labelItem3 = new DevComponents.DotNetBar.LabelItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.comboBoxItem1 = new DevComponents.DotNetBar.ComboBoxItem();
            this.ItemContainer5 = new DevComponents.DotNetBar.ItemContainer();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DeleteContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrSalryProcessRelease = new System.Windows.Forms.Timer(this.components);
            this.errSettlementProcessGroup = new System.Windows.Forms.ErrorProvider(this.components);
            this.TimerFormSize = new System.Windows.Forms.Timer(this.components);
            this.DeleteSingleRowContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.BtnSingleRow = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripDes = new System.Windows.Forms.ToolTip(this.components);
            this.btnConfirm = new System.Windows.Forms.Button();
            this.LblType = new System.Windows.Forms.Label();
            this.CboType = new System.Windows.Forms.ComboBox();
            this.ToolStripStatus.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlementDetails)).BeginInit();
            this.pnlLeft.SuspendLayout();
            this.expPnlSettlementProcess.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemPanelMiddileBottom)).BeginInit();
            this.DeleteContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errSettlementProcessGroup)).BeginInit();
            this.DeleteSingleRowContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStripStatus
            // 
            this.ToolStripStatus.BackColor = System.Drawing.Color.Transparent;
            this.ToolStripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel2,
            this.tsProgressBar,
            this.lblStatus});
            this.ToolStripStatus.Location = new System.Drawing.Point(0, 505);
            this.ToolStripStatus.Name = "ToolStripStatus";
            this.ToolStripStatus.Size = new System.Drawing.Size(1208, 22);
            this.ToolStripStatus.TabIndex = 122;
            this.ToolStripStatus.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel2
            // 
            this.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2";
            this.ToolStripStatusLabel2.Size = new System.Drawing.Size(48, 17);
            this.ToolStripStatusLabel2.Text = "Status : ";
            // 
            // tsProgressBar
            // 
            this.tsProgressBar.Name = "tsProgressBar";
            this.tsProgressBar.Size = new System.Drawing.Size(100, 16);
            this.tsProgressBar.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMain.Controls.Add(this.pnlMiddle);
            this.pnlMain.Controls.Add(this.pnlLeft);
            this.pnlMain.Controls.Add(this.pnlBottom);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1208, 505);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 123;
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMiddle.Controls.Add(this.chkSelectAll);
            this.pnlMiddle.Controls.Add(this.dgvSettlementDetails);
            this.pnlMiddle.Controls.Add(this.expandableSplitter1);
            this.pnlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMiddle.Location = new System.Drawing.Point(246, 0);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Size = new System.Drawing.Size(962, 460);
            this.pnlMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMiddle.Style.GradientAngle = 90;
            this.pnlMiddle.TabIndex = 127;
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(15, 6);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(15, 14);
            this.chkSelectAll.TabIndex = 7;
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // dgvSettlementDetails
            // 
            this.dgvSettlementDetails.AllowUserToAddRows = false;
            this.dgvSettlementDetails.AllowUserToDeleteRows = false;
            this.dgvSettlementDetails.AllowUserToResizeColumns = false;
            this.dgvSettlementDetails.AllowUserToResizeRows = false;
            this.dgvSettlementDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSettlementDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvSettlementDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSettlementDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SelectAll});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSettlementDetails.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSettlementDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSettlementDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSettlementDetails.Location = new System.Drawing.Point(5, 0);
            this.dgvSettlementDetails.Name = "dgvSettlementDetails";
            this.dgvSettlementDetails.RowHeadersVisible = false;
            this.dgvSettlementDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSettlementDetails.Size = new System.Drawing.Size(957, 460);
            this.dgvSettlementDetails.TabIndex = 2;
            this.dgvSettlementDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvSalaryDetails_CurrentCellDirtyStateChanged);
            this.dgvSettlementDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvSalaryDetails_DataError);
            // 
            // SelectAll
            // 
            this.SelectAll.DataPropertyName = "SelectAll";
            this.SelectAll.HeaderText = "";
            this.SelectAll.Name = "SelectAll";
            this.SelectAll.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectAll.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // expandableSplitter1
            // 
            this.expandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter1.ExpandableControl = this.pnlLeft;
            this.expandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitter1.Name = "expandableSplitter1";
            this.expandableSplitter1.Size = new System.Drawing.Size(5, 460);
            this.expandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitter1.TabIndex = 0;
            this.expandableSplitter1.TabStop = false;
            this.expandableSplitter1.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitter1_ExpandedChanged);
            // 
            // pnlLeft
            // 
            this.pnlLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLeft.Controls.Add(this.expPnlSettlementProcess);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(246, 460);
            this.pnlLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeft.Style.GradientAngle = 90;
            this.pnlLeft.TabIndex = 126;
            // 
            // expPnlSettlementProcess
            // 
            this.expPnlSettlementProcess.CanvasColor = System.Drawing.SystemColors.Control;
            this.expPnlSettlementProcess.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expPnlSettlementProcess.Controls.Add(this.LblType);
            this.expPnlSettlementProcess.Controls.Add(this.CboType);
            this.expPnlSettlementProcess.Controls.Add(this.doj);
            this.expPnlSettlementProcess.Controls.Add(this.chkIncludeCompany);
            this.expPnlSettlementProcess.Controls.Add(this.cboBranch);
            this.expPnlSettlementProcess.Controls.Add(this.Label7);
            this.expPnlSettlementProcess.Controls.Add(this.label3);
            this.expPnlSettlementProcess.Controls.Add(this.showProgressBar);
            this.expPnlSettlementProcess.Controls.Add(this.DtpLastWorkingDate);
            this.expPnlSettlementProcess.Controls.Add(this.chkAdvanceSearch);
            this.expPnlSettlementProcess.Controls.Add(this.dtpResignationDate);
            this.expPnlSettlementProcess.Controls.Add(this.cboDepartment);
            this.expPnlSettlementProcess.Controls.Add(this.lblToDate);
            this.expPnlSettlementProcess.Controls.Add(this.lblDept);
            this.expPnlSettlementProcess.Controls.Add(this.cboDesignation);
            this.expPnlSettlementProcess.Controls.Add(this.lblDesg);
            this.expPnlSettlementProcess.Controls.Add(this.btnShow);
            this.expPnlSettlementProcess.Controls.Add(this.cboCompany);
            this.expPnlSettlementProcess.Controls.Add(this.Label1);
            this.expPnlSettlementProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.expPnlSettlementProcess.ExpandButtonVisible = false;
            this.expPnlSettlementProcess.Location = new System.Drawing.Point(0, 0);
            this.expPnlSettlementProcess.Name = "expPnlSettlementProcess";
            this.expPnlSettlementProcess.Size = new System.Drawing.Size(246, 460);
            this.expPnlSettlementProcess.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expPnlSettlementProcess.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expPnlSettlementProcess.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expPnlSettlementProcess.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expPnlSettlementProcess.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expPnlSettlementProcess.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expPnlSettlementProcess.Style.GradientAngle = 90;
            this.expPnlSettlementProcess.TabIndex = 3;
            this.expPnlSettlementProcess.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expPnlSettlementProcess.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expPnlSettlementProcess.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expPnlSettlementProcess.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expPnlSettlementProcess.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expPnlSettlementProcess.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expPnlSettlementProcess.TitleStyle.GradientAngle = 90;
            this.expPnlSettlementProcess.TitleText = "Settlement Process";
            // 
            // doj
            // 
            this.doj.CustomFormat = "dd-MMM-yyyy";
            this.doj.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.doj.Location = new System.Drawing.Point(85, 392);
            this.doj.Name = "doj";
            this.doj.Size = new System.Drawing.Size(106, 20);
            this.doj.TabIndex = 217;
            this.doj.Visible = false;
            // 
            // chkIncludeCompany
            // 
            this.chkIncludeCompany.AutoSize = true;
            this.chkIncludeCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIncludeCompany.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkIncludeCompany.Location = new System.Drawing.Point(119, 81);
            this.chkIncludeCompany.Name = "chkIncludeCompany";
            this.chkIncludeCompany.Size = new System.Drawing.Size(123, 17);
            this.chkIncludeCompany.TabIndex = 123;
            this.chkIncludeCompany.Text = "Include Company";
            this.chkIncludeCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkIncludeCompany.UseVisualStyleBackColor = true;
            // 
            // cboBranch
            // 
            this.cboBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBranch.DropDownHeight = 105;
            this.cboBranch.FormattingEnabled = true;
            this.cboBranch.IntegralHeight = false;
            this.cboBranch.Location = new System.Drawing.Point(9, 102);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(228, 21);
            this.cboBranch.TabIndex = 124;
            this.cboBranch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // Label7
            // 
            this.Label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label7.Location = new System.Drawing.Point(6, 78);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(60, 21);
            this.Label7.TabIndex = 125;
            this.Label7.Text = "Branch";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(6, 286);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 215;
            this.label3.Text = "Last Working";
            // 
            // showProgressBar
            // 
            this.showProgressBar.Location = new System.Drawing.Point(33, 351);
            this.showProgressBar.Name = "showProgressBar";
            this.showProgressBar.Size = new System.Drawing.Size(166, 18);
            this.showProgressBar.TabIndex = 216;
            this.showProgressBar.Visible = false;
            // 
            // DtpLastWorkingDate
            // 
            this.DtpLastWorkingDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpLastWorkingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpLastWorkingDate.Location = new System.Drawing.Point(119, 282);
            this.DtpLastWorkingDate.Name = "DtpLastWorkingDate";
            this.DtpLastWorkingDate.Size = new System.Drawing.Size(106, 20);
            this.DtpLastWorkingDate.TabIndex = 214;
            // 
            // chkAdvanceSearch
            // 
            this.chkAdvanceSearch.AutoSize = true;
            this.chkAdvanceSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAdvanceSearch.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkAdvanceSearch.Location = new System.Drawing.Point(119, 130);
            this.chkAdvanceSearch.Name = "chkAdvanceSearch";
            this.chkAdvanceSearch.Size = new System.Drawing.Size(120, 17);
            this.chkAdvanceSearch.TabIndex = 110;
            this.chkAdvanceSearch.Text = "Advance Search";
            this.chkAdvanceSearch.UseVisualStyleBackColor = true;
            this.chkAdvanceSearch.Visible = false;
            this.chkAdvanceSearch.CheckedChanged += new System.EventHandler(this.chkAdvanceSearch_CheckedChanged);
            // 
            // dtpResignationDate
            // 
            this.dtpResignationDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpResignationDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpResignationDate.Location = new System.Drawing.Point(120, 252);
            this.dtpResignationDate.Name = "dtpResignationDate";
            this.dtpResignationDate.Size = new System.Drawing.Size(106, 20);
            this.dtpResignationDate.TabIndex = 124;
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.DropDownHeight = 105;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.IntegralHeight = false;
            this.cboDepartment.Location = new System.Drawing.Point(9, 152);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(228, 21);
            this.cboDepartment.TabIndex = 112;
            this.cboDepartment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.BackColor = System.Drawing.Color.Transparent;
            this.lblToDate.Location = new System.Drawing.Point(6, 256);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(89, 13);
            this.lblToDate.TabIndex = 125;
            this.lblToDate.Text = "Resignation Date";
            // 
            // lblDept
            // 
            this.lblDept.AutoSize = true;
            this.lblDept.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDept.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDept.Location = new System.Drawing.Point(6, 135);
            this.lblDept.Name = "lblDept";
            this.lblDept.Size = new System.Drawing.Size(62, 13);
            this.lblDept.TabIndex = 120;
            this.lblDept.Text = "Department";
            this.lblDept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboDesignation
            // 
            this.cboDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDesignation.DropDownHeight = 105;
            this.cboDesignation.FormattingEnabled = true;
            this.cboDesignation.IntegralHeight = false;
            this.cboDesignation.Location = new System.Drawing.Point(9, 193);
            this.cboDesignation.Name = "cboDesignation";
            this.cboDesignation.Size = new System.Drawing.Size(228, 21);
            this.cboDesignation.TabIndex = 113;
            this.cboDesignation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblDesg
            // 
            this.lblDesg.AutoSize = true;
            this.lblDesg.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDesg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDesg.Location = new System.Drawing.Point(6, 178);
            this.lblDesg.Name = "lblDesg";
            this.lblDesg.Size = new System.Drawing.Size(63, 13);
            this.lblDesg.TabIndex = 122;
            this.lblDesg.Text = "Designation";
            this.lblDesg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnShow
            // 
            this.btnShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShow.Location = new System.Drawing.Point(85, 318);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(69, 27);
            this.btnShow.TabIndex = 115;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cboCompany
            // 
            this.cboCompany.AllowDrop = true;
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DropDownHeight = 105;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.Location = new System.Drawing.Point(9, 51);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(228, 21);
            this.cboCompany.TabIndex = 98;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            // 
            // Label1
            // 
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label1.Location = new System.Drawing.Point(6, 27);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(70, 21);
            this.Label1.TabIndex = 118;
            this.Label1.Text = "Company";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.ItemPanelMiddileBottom);
            this.pnlBottom.Controls.Add(this.btnProcess);
            this.pnlBottom.Controls.Add(this.btnBack);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 460);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1208, 45);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 124;
            // 
            // ItemPanelMiddileBottom
            // 
            this.ItemPanelMiddileBottom.AntiAlias = true;
            this.ItemPanelMiddileBottom.BackColor = System.Drawing.Color.Transparent;
            this.ItemPanelMiddileBottom.DockSide = DevComponents.DotNetBar.eDockSide.Bottom;
            this.ItemPanelMiddileBottom.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cboCountItem,
            this.btnFirstGridCollection,
            this.btnPreviousGridCollection,
            this.lblPageCount,
            this.btnNextGridCollection,
            this.btnLastGridCollection});
            this.ItemPanelMiddileBottom.Location = new System.Drawing.Point(3, 8);
            this.ItemPanelMiddileBottom.Name = "ItemPanelMiddileBottom";
            this.ItemPanelMiddileBottom.Size = new System.Drawing.Size(334, 28);
            this.ItemPanelMiddileBottom.Stretch = true;
            this.ItemPanelMiddileBottom.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ItemPanelMiddileBottom.TabIndex = 115;
            this.ItemPanelMiddileBottom.TabStop = false;
            this.ItemPanelMiddileBottom.Text = "bar1";
            // 
            // cboCountItem
            // 
            this.cboCountItem.DropDownHeight = 106;
            this.cboCountItem.ItemHeight = 17;
            this.cboCountItem.Name = "cboCountItem";
            this.cboCountItem.SelectedIndexChanged += new System.EventHandler(this.cboCountItem_SelectedIndexChanged);
            // 
            // btnFirstGridCollection
            // 
            this.btnFirstGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowLeftStart;
            this.btnFirstGridCollection.Name = "btnFirstGridCollection";
            this.btnFirstGridCollection.Text = "First";
            this.btnFirstGridCollection.Tooltip = "First";
            this.btnFirstGridCollection.Click += new System.EventHandler(this.btnFirstGridCollection_Click);
            // 
            // btnPreviousGridCollection
            // 
            this.btnPreviousGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowLeft;
            this.btnPreviousGridCollection.Name = "btnPreviousGridCollection";
            this.btnPreviousGridCollection.Text = "Previous";
            this.btnPreviousGridCollection.Tooltip = "Previous";
            this.btnPreviousGridCollection.Click += new System.EventHandler(this.btnPreviousGridCollection_Click);
            // 
            // lblPageCount
            // 
            this.lblPageCount.BeginGroup = true;
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.Text = "{0} of {0}";
            // 
            // btnNextGridCollection
            // 
            this.btnNextGridCollection.BeginGroup = true;
            this.btnNextGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowRight;
            this.btnNextGridCollection.Name = "btnNextGridCollection";
            this.btnNextGridCollection.Text = "Next";
            this.btnNextGridCollection.Tooltip = "Next";
            this.btnNextGridCollection.Click += new System.EventHandler(this.btnNextGridCollection_Click);
            // 
            // btnLastGridCollection
            // 
            this.btnLastGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowRightStart;
            this.btnLastGridCollection.Name = "btnLastGridCollection";
            this.btnLastGridCollection.Text = "Last";
            this.btnLastGridCollection.Tooltip = "Last";
            this.btnLastGridCollection.Click += new System.EventHandler(this.btnLastGridCollection_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcess.Image = global::MyPayfriend.Properties.Resources.Salary_processing;
            this.btnProcess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProcess.Location = new System.Drawing.Point(889, 2);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(90, 40);
            this.btnProcess.TabIndex = 0;
            this.btnProcess.Text = "&Process";
            this.btnProcess.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.btnProcess, "Process");
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.Image = global::MyPayfriend.Properties.Resources.go_back;
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(793, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(90, 40);
            this.btnBack.TabIndex = 122;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.btnBack, "Back");
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = global::MyPayfriend.Properties.Resources.Logout1;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(1085, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 40);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.btnCancel, "Cancel");
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // LabelItem24
            // 
            this.LabelItem24.Name = "LabelItem24";
            this.LabelItem24.Text = "|";
            // 
            // LabelItem25
            // 
            this.LabelItem25.BeginGroup = true;
            this.LabelItem25.Name = "LabelItem25";
            this.LabelItem25.Text = "{0} of {0}";
            // 
            // LabelItem26
            // 
            this.LabelItem26.Name = "LabelItem26";
            this.LabelItem26.Text = "|";
            // 
            // buttonItem4
            // 
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Text = "Last";
            // 
            // buttonItem3
            // 
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Text = "Next";
            // 
            // labelItem5
            // 
            this.labelItem5.Name = "labelItem5";
            this.labelItem5.Text = "|";
            // 
            // labelItem4
            // 
            this.labelItem4.BeginGroup = true;
            this.labelItem4.Name = "labelItem4";
            this.labelItem4.Text = "{0} of {0}";
            // 
            // labelItem3
            // 
            this.labelItem3.Name = "labelItem3";
            this.labelItem3.Text = "|";
            // 
            // buttonItem2
            // 
            this.buttonItem2.Image = global::MyPayfriend.Properties.Resources.ArrowLeft;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Text = "Previous";
            // 
            // buttonItem1
            // 
            this.buttonItem1.Image = global::MyPayfriend.Properties.Resources.ArrowLeftStart;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "First";
            // 
            // comboBoxItem1
            // 
            this.comboBoxItem1.Caption = "ComboBoxItem1";
            this.comboBoxItem1.DropDownHeight = 106;
            this.comboBoxItem1.Name = "comboBoxItem1";
            // 
            // ItemContainer5
            // 
            // 
            // 
            // 
            this.ItemContainer5.BackgroundStyle.Class = "";
            this.ItemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ItemContainer5.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.ItemContainer5.Name = "ItemContainer5";
            this.ItemContainer5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.comboBoxItem1,
            this.buttonItem1,
            this.buttonItem2,
            this.labelItem3,
            this.labelItem4,
            this.labelItem5,
            this.buttonItem3,
            this.buttonItem4});
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // DeleteContextMenuStrip
            // 
            this.DeleteContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemDelete});
            this.DeleteContextMenuStrip.Name = "CopyDetailsContextMenuStrip";
            this.DeleteContextMenuStrip.Size = new System.Drawing.Size(108, 26);
            // 
            // ToolStripMenuItemDelete
            // 
            this.ToolStripMenuItemDelete.Name = "ToolStripMenuItemDelete";
            this.ToolStripMenuItemDelete.Size = new System.Drawing.Size(107, 22);
            this.ToolStripMenuItemDelete.Text = "Delete";
            // 
            // tmrSalryProcessRelease
            // 
            this.tmrSalryProcessRelease.Interval = 2000;
            // 
            // errSettlementProcessGroup
            // 
            this.errSettlementProcessGroup.ContainerControl = this;
            // 
            // TimerFormSize
            // 
            this.TimerFormSize.Interval = 2000;
            // 
            // DeleteSingleRowContextMenuStrip
            // 
            this.DeleteSingleRowContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnSingleRow});
            this.DeleteSingleRowContextMenuStrip.Name = "CopyDetailsContextMenuStrip";
            this.DeleteSingleRowContextMenuStrip.Size = new System.Drawing.Size(153, 26);
            // 
            // BtnSingleRow
            // 
            this.BtnSingleRow.Name = "BtnSingleRow";
            this.BtnSingleRow.Size = new System.Drawing.Size(152, 22);
            this.BtnSingleRow.Text = "Delete this row";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfirm.Image = global::MyPayfriend.Properties.Resources.Salary_processing;
            this.btnConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirm.Location = new System.Drawing.Point(988, 462);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(90, 40);
            this.btnConfirm.TabIndex = 217;
            this.btnConfirm.Text = "C&onfirm";
            this.btnConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.btnConfirm, "Process");
            this.btnConfirm.UseVisualStyleBackColor = true;
            // 
            // LblType
            // 
            this.LblType.AutoSize = true;
            this.LblType.Location = new System.Drawing.Point(6, 227);
            this.LblType.Name = "LblType";
            this.LblType.Size = new System.Drawing.Size(84, 13);
            this.LblType.TabIndex = 219;
            this.LblType.Text = "Settlement Type";
            // 
            // CboType
            // 
            this.CboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboType.BackColor = System.Drawing.SystemColors.Info;
            this.CboType.DropDownHeight = 134;
            this.CboType.FormattingEnabled = true;
            this.CboType.IntegralHeight = false;
            this.CboType.Location = new System.Drawing.Point(120, 223);
            this.CboType.Name = "CboType";
            this.CboType.Size = new System.Drawing.Size(106, 21);
            this.CboType.TabIndex = 218;
            this.CboType.SelectedIndexChanged += new System.EventHandler(this.CboType_SelectedIndexChanged);
            // 
            // FrmSettlementProcessGroup
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1208, 527);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.ToolStripStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmSettlementProcessGroup";
            this.Text = "Settlement Process - Batchwise";
            this.Deactivate += new System.EventHandler(this.FrmSettlementProcessGroup_Deactivate);
            this.Load += new System.EventHandler(this.FrmSettlementProcessGroup_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmSettlementProcessGroup_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSettlementProcessGroup_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSettlementProcessGroup_KeyDown);
            this.ToolStripStatus.ResumeLayout(false);
            this.ToolStripStatus.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlMiddle.ResumeLayout(false);
            this.pnlMiddle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlementDetails)).EndInit();
            this.pnlLeft.ResumeLayout(false);
            this.expPnlSettlementProcess.ResumeLayout(false);
            this.expPnlSettlementProcess.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemPanelMiddileBottom)).EndInit();
            this.DeleteContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errSettlementProcessGroup)).EndInit();
            this.DeleteSingleRowContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.StatusStrip ToolStripStatus;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel2;
        internal System.Windows.Forms.ToolStripProgressBar tsProgressBar;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private DevComponents.DotNetBar.PanelEx pnlMain;
        internal System.Windows.Forms.Button btnBack;
        internal System.Windows.Forms.Button btnProcess;
        internal System.Windows.Forms.Button btnCancel;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private DevComponents.DotNetBar.PanelEx pnlLeft;
        private DevComponents.DotNetBar.ExpandablePanel expPnlSettlementProcess;
        internal System.Windows.Forms.CheckBox chkAdvanceSearch;
        internal System.Windows.Forms.ComboBox cboDepartment;
        internal System.Windows.Forms.Label lblDept;
        internal System.Windows.Forms.ComboBox cboDesignation;
        internal System.Windows.Forms.Label lblDesg;
        internal System.Windows.Forms.Button btnShow;
        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.Label Label1;
        private DevComponents.DotNetBar.PanelEx pnlMiddle;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter1;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        internal System.Windows.Forms.CheckBox chkSelectAll;
        internal System.Windows.Forms.ContextMenuStrip DeleteContextMenuStrip;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemDelete;
        internal System.Windows.Forms.Timer tmrSalryProcessRelease;
        internal System.Windows.Forms.ErrorProvider errSettlementProcessGroup;
        internal System.Windows.Forms.Timer TimerFormSize;
        internal System.Windows.Forms.ContextMenuStrip DeleteSingleRowContextMenuStrip;
        internal System.Windows.Forms.ToolStripMenuItem BtnSingleRow;
        private DevComponents.DotNetBar.Bar ItemPanelMiddileBottom;
        private DevComponents.DotNetBar.ComboBoxItem cboCountItem;
        private DevComponents.DotNetBar.ButtonItem btnFirstGridCollection;
        private DevComponents.DotNetBar.ButtonItem btnPreviousGridCollection;
        private DevComponents.DotNetBar.LabelItem lblPageCount;
        private DevComponents.DotNetBar.ButtonItem btnNextGridCollection;
        private DevComponents.DotNetBar.ButtonItem btnLastGridCollection;
        private System.Windows.Forms.ToolTip ToolStripDes;
        internal System.Windows.Forms.CheckBox chkIncludeCompany;
        internal System.Windows.Forms.ComboBox cboBranch;
        internal System.Windows.Forms.Label Label7;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSettlementDetails;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectAll;
        internal System.Windows.Forms.DateTimePicker dtpResignationDate;
        internal System.Windows.Forms.Label lblToDate;
        private DevComponents.DotNetBar.LabelItem LabelItem24;
        private DevComponents.DotNetBar.LabelItem LabelItem25;
        private DevComponents.DotNetBar.LabelItem LabelItem26;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.LabelItem labelItem5;
        private DevComponents.DotNetBar.LabelItem labelItem4;
        private DevComponents.DotNetBar.LabelItem labelItem3;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ComboBoxItem comboBoxItem1;
        private DevComponents.DotNetBar.ItemContainer ItemContainer5;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.DateTimePicker DtpLastWorkingDate;
        private System.Windows.Forms.ProgressBar showProgressBar;
        internal System.Windows.Forms.Button btnConfirm;
        internal System.Windows.Forms.DateTimePicker doj;
        internal System.Windows.Forms.Label LblType;
        internal System.Windows.Forms.ComboBox CboType;

    }
}