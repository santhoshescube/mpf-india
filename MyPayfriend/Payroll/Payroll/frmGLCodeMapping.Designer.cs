﻿namespace MyPayfriend
{
    partial class frmGLCodeMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGLCodeMapping));
            this.AddDedBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAdditionDeduction = new System.Windows.Forms.ToolStripButton();
            this.tsbExpenseType = new System.Windows.Forms.ToolStripButton();
            this.LblAdded = new System.Windows.Forms.StatusStrip();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblHeader = new System.Windows.Forms.Label();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.tmrGLCodeMapping = new System.Windows.Forms.Timer(this.components);
            this.dgvAdditionDeduction = new System.Windows.Forms.DataGridView();
            this.AdditionDeductionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AdditionDeduction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AdditionDeductionArb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rbSalary = new System.Windows.Forms.RadioButton();
            this.rbExpense = new System.Windows.Forms.RadioButton();
            this.pnlTop = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.AddDedBindingNavigator)).BeginInit();
            this.AddDedBindingNavigator.SuspendLayout();
            this.LblAdded.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdditionDeduction)).BeginInit();
            this.pnlTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // AddDedBindingNavigator
            // 
            this.AddDedBindingNavigator.AddNewItem = null;
            this.AddDedBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.AddDedBindingNavigator.CountItem = null;
            this.AddDedBindingNavigator.DeleteItem = null;
            this.AddDedBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorSaveItem,
            this.toolStripSeparator1,
            this.tsbAdditionDeduction,
            this.tsbExpenseType});
            this.AddDedBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.AddDedBindingNavigator.MoveFirstItem = null;
            this.AddDedBindingNavigator.MoveLastItem = null;
            this.AddDedBindingNavigator.MoveNextItem = null;
            this.AddDedBindingNavigator.MovePreviousItem = null;
            this.AddDedBindingNavigator.Name = "AddDedBindingNavigator";
            this.AddDedBindingNavigator.PositionItem = null;
            this.AddDedBindingNavigator.Size = new System.Drawing.Size(658, 25);
            this.AddDedBindingNavigator.TabIndex = 2;
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            this.BindingNavigatorSeparator2.Visible = false;
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbAdditionDeduction
            // 
            this.tsbAdditionDeduction.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAdditionDeduction.Image = global::MyPayfriend.Properties.Resources.Addition_Deduction2;
            this.tsbAdditionDeduction.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdditionDeduction.Name = "tsbAdditionDeduction";
            this.tsbAdditionDeduction.Size = new System.Drawing.Size(23, 22);
            this.tsbAdditionDeduction.Text = "Addition / Deduction";
            this.tsbAdditionDeduction.Click += new System.EventHandler(this.tsbAdditionDeduction_Click);
            // 
            // tsbExpenseType
            // 
            this.tsbExpenseType.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbExpenseType.Image = ((System.Drawing.Image)(resources.GetObject("tsbExpenseType.Image")));
            this.tsbExpenseType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExpenseType.Name = "tsbExpenseType";
            this.tsbExpenseType.Size = new System.Drawing.Size(23, 22);
            this.tsbExpenseType.Text = "Expense Type";
            this.tsbExpenseType.Click += new System.EventHandler(this.tsbExpenseType_Click);
            // 
            // LblAdded
            // 
            this.LblAdded.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblstatus});
            this.LblAdded.Location = new System.Drawing.Point(0, 381);
            this.LblAdded.Name = "LblAdded";
            this.LblAdded.Size = new System.Drawing.Size(658, 22);
            this.LblAdded.TabIndex = 6;
            this.LblAdded.Text = "StatusStrip1";
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(0, 25);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(168, 13);
            this.lblHeader.TabIndex = 87;
            this.lblHeader.Text = "Addition && Deduction Details";
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 150;
            this.LineShape1.X2 = 650;
            this.LineShape1.Y1 = 32;
            this.LineShape1.Y2 = 32;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(658, 403);
            this.shapeContainer1.TabIndex = 88;
            this.shapeContainer1.TabStop = false;
            // 
            // tmrGLCodeMapping
            // 
            this.tmrGLCodeMapping.Interval = 3000;
            this.tmrGLCodeMapping.Tick += new System.EventHandler(this.tmrGLCodeMapping_Tick);
            // 
            // dgvAdditionDeduction
            // 
            this.dgvAdditionDeduction.AllowUserToAddRows = false;
            this.dgvAdditionDeduction.AllowUserToDeleteRows = false;
            this.dgvAdditionDeduction.AllowUserToResizeColumns = false;
            this.dgvAdditionDeduction.AllowUserToResizeRows = false;
            this.dgvAdditionDeduction.BackgroundColor = System.Drawing.Color.White;
            this.dgvAdditionDeduction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAdditionDeduction.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AdditionDeductionID,
            this.AdditionDeduction,
            this.AdditionDeductionArb,
            this.Code,
            this.PRCode});
            this.dgvAdditionDeduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAdditionDeduction.Location = new System.Drawing.Point(0, 69);
            this.dgvAdditionDeduction.Name = "dgvAdditionDeduction";
            this.dgvAdditionDeduction.Size = new System.Drawing.Size(658, 312);
            this.dgvAdditionDeduction.TabIndex = 89;
            this.dgvAdditionDeduction.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvAdditionDeduction_CurrentCellDirtyStateChanged);
            // 
            // AdditionDeductionID
            // 
            this.AdditionDeductionID.DataPropertyName = "AdditionDeductionID";
            this.AdditionDeductionID.HeaderText = "AdditionDeductionID";
            this.AdditionDeductionID.Name = "AdditionDeductionID";
            this.AdditionDeductionID.ReadOnly = true;
            this.AdditionDeductionID.Visible = false;
            // 
            // AdditionDeduction
            // 
            this.AdditionDeduction.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AdditionDeduction.DataPropertyName = "AdditionDeduction";
            this.AdditionDeduction.HeaderText = "Addition / Deduction";
            this.AdditionDeduction.Name = "AdditionDeduction";
            this.AdditionDeduction.ReadOnly = true;
            // 
            // AdditionDeductionArb
            // 
            this.AdditionDeductionArb.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AdditionDeductionArb.DataPropertyName = "AdditionDeductionArb";
            this.AdditionDeductionArb.HeaderText = "خصم علاوة على ذلك";
            this.AdditionDeductionArb.Name = "AdditionDeductionArb";
            this.AdditionDeductionArb.ReadOnly = true;
            this.AdditionDeductionArb.Visible = false;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "Code";
            this.Code.HeaderText = "Code";
            this.Code.MaxInputLength = 50;
            this.Code.Name = "Code";
            this.Code.Width = 150;
            // 
            // PRCode
            // 
            this.PRCode.DataPropertyName = "PRCode";
            this.PRCode.HeaderText = "Provision Account";
            this.PRCode.MaxInputLength = 50;
            this.PRCode.Name = "PRCode";
            this.PRCode.Width = 150;
            // 
            // rbSalary
            // 
            this.rbSalary.AutoSize = true;
            this.rbSalary.Checked = true;
            this.rbSalary.Location = new System.Drawing.Point(58, 7);
            this.rbSalary.Name = "rbSalary";
            this.rbSalary.Size = new System.Drawing.Size(94, 17);
            this.rbSalary.TabIndex = 90;
            this.rbSalary.TabStop = true;
            this.rbSalary.Text = "Salary Related";
            this.rbSalary.UseVisualStyleBackColor = true;
            this.rbSalary.CheckedChanged += new System.EventHandler(this.rbSalary_CheckedChanged);
            // 
            // rbExpense
            // 
            this.rbExpense.AutoSize = true;
            this.rbExpense.Location = new System.Drawing.Point(186, 7);
            this.rbExpense.Name = "rbExpense";
            this.rbExpense.Size = new System.Drawing.Size(66, 17);
            this.rbExpense.TabIndex = 91;
            this.rbExpense.Text = "Expense";
            this.rbExpense.UseVisualStyleBackColor = true;
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.rbSalary);
            this.pnlTop.Controls.Add(this.rbExpense);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 38);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(658, 31);
            this.pnlTop.TabIndex = 92;
            // 
            // frmGLCodeMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 403);
            this.Controls.Add(this.dgvAdditionDeduction);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.LblAdded);
            this.Controls.Add(this.AddDedBindingNavigator);
            this.Controls.Add(this.shapeContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGLCodeMapping";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "GL Code Mapping";
            this.Load += new System.EventHandler(this.frmGLCodeMapping_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AddDedBindingNavigator)).EndInit();
            this.AddDedBindingNavigator.ResumeLayout(false);
            this.AddDedBindingNavigator.PerformLayout();
            this.LblAdded.ResumeLayout(false);
            this.LblAdded.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdditionDeduction)).EndInit();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator AddDedBindingNavigator;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbAdditionDeduction;
        internal System.Windows.Forms.StatusStrip LblAdded;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        internal System.Windows.Forms.Label lblHeader;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.Timer tmrGLCodeMapping;
        private System.Windows.Forms.DataGridView dgvAdditionDeduction;
        private System.Windows.Forms.RadioButton rbSalary;
        private System.Windows.Forms.RadioButton rbExpense;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.ToolStripButton tsbExpenseType;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdditionDeductionID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdditionDeduction;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdditionDeductionArb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRCode;
    }
}