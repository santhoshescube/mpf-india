﻿namespace MyPayfriend
{
    partial class FrmSalaryRelease
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalaryRelease));
            this.ToolStripStatus = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.pnlMiddle = new DevComponents.DotNetBar.PanelEx();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.dgvSalaryDetails = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.SelectAll = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.expandableSplitter1 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlLeft = new DevComponents.DotNetBar.PanelEx();
            this.trvSalaryRelease = new System.Windows.Forms.TreeView();
            this.expPnlSalaryRelease = new DevComponents.DotNetBar.ExpandablePanel();
            this.bRelease = new DevComponents.DotNetBar.Bar();
            this.btnEmail = new DevComponents.DotNetBar.ButtonItem();
            this.btnPrint = new DevComponents.DotNetBar.ButtonItem();
            this.btnSalarySlip = new DevComponents.DotNetBar.ButtonItem();
            this.BtnAddAdditionDeduction = new DevComponents.DotNetBar.ButtonItem();
            this.btnPayment = new DevComponents.DotNetBar.ButtonItem();
            this.btnSIF = new DevComponents.DotNetBar.ButtonItem();
            this.btnAddPariculars = new DevComponents.DotNetBar.ButtonItem();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.txtSearchFor = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem2 = new DevComponents.DotNetBar.LabelItem();
            this.cboRepColForSearch = new DevComponents.DotNetBar.ComboBoxItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.btnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.pnlSalaryReleaseBottom = new DevComponents.DotNetBar.PanelEx();
            this.btnShow = new System.Windows.Forms.Button();
            this.cboDesignationFil = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboDepartmentFil = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnFilterClear = new System.Windows.Forms.Button();
            this.cboFilterBank = new System.Windows.Forms.ComboBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.cboFilterTransactionType = new System.Windows.Forms.ComboBox();
            this.Label15 = new System.Windows.Forms.Label();
            this.cboFilterCompany = new System.Windows.Forms.ComboBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.cboFilterProcessYear = new System.Windows.Forms.ComboBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.ItemPanelMiddileBottom = new DevComponents.DotNetBar.Bar();
            this.cboCountItem = new DevComponents.DotNetBar.ComboBoxItem();
            this.btnFirstGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.btnPreviousGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.lblPageCount = new DevComponents.DotNetBar.LabelItem();
            this.btnNextGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.btnLastGridCollection = new DevComponents.DotNetBar.ButtonItem();
            this.pnlMentioned = new System.Windows.Forms.Panel();
            this.Label10 = new System.Windows.Forms.Label();
            this.PictureBox3 = new System.Windows.Forms.PictureBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnRelease = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.LabelItem24 = new DevComponents.DotNetBar.LabelItem();
            this.LabelItem25 = new DevComponents.DotNetBar.LabelItem();
            this.LabelItem26 = new DevComponents.DotNetBar.LabelItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.labelItem5 = new DevComponents.DotNetBar.LabelItem();
            this.labelItem4 = new DevComponents.DotNetBar.LabelItem();
            this.labelItem3 = new DevComponents.DotNetBar.LabelItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.comboBoxItem1 = new DevComponents.DotNetBar.ComboBoxItem();
            this.ItemContainer5 = new DevComponents.DotNetBar.ItemContainer();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DeleteContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrSalryProcessRelease = new System.Windows.Forms.Timer(this.components);
            this.errSalaryProcessRelease = new System.Windows.Forms.ErrorProvider(this.components);
            this.TimerFormSize = new System.Windows.Forms.Timer(this.components);
            this.DeleteSingleRowContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.BtnSingleRow = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripDes = new System.Windows.Forms.ToolTip(this.components);
            this.ToolStripStatus.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalaryDetails)).BeginInit();
            this.pnlLeft.SuspendLayout();
            this.expPnlSalaryRelease.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bRelease)).BeginInit();
            this.pnlSalaryReleaseBottom.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemPanelMiddileBottom)).BeginInit();
            this.pnlMentioned.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            this.DeleteContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errSalaryProcessRelease)).BeginInit();
            this.DeleteSingleRowContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStripStatus
            // 
            this.ToolStripStatus.BackColor = System.Drawing.Color.Transparent;
            this.ToolStripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel2,
            this.tsProgressBar,
            this.lblStatus});
            this.ToolStripStatus.Location = new System.Drawing.Point(0, 505);
            this.ToolStripStatus.Name = "ToolStripStatus";
            this.ToolStripStatus.Size = new System.Drawing.Size(1208, 22);
            this.ToolStripStatus.TabIndex = 122;
            this.ToolStripStatus.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel2
            // 
            this.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2";
            this.ToolStripStatusLabel2.Size = new System.Drawing.Size(48, 17);
            this.ToolStripStatusLabel2.Text = "Status : ";
            // 
            // tsProgressBar
            // 
            this.tsProgressBar.Name = "tsProgressBar";
            this.tsProgressBar.Size = new System.Drawing.Size(100, 16);
            this.tsProgressBar.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMain.Controls.Add(this.pnlMiddle);
            this.pnlMain.Controls.Add(this.pnlLeft);
            this.pnlMain.Controls.Add(this.expPnlSalaryRelease);
            this.pnlMain.Controls.Add(this.pnlBottom);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1208, 505);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMain.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 123;
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMiddle.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlMiddle.Controls.Add(this.chkSelectAll);
            this.pnlMiddle.Controls.Add(this.dgvSalaryDetails);
            this.pnlMiddle.Controls.Add(this.expandableSplitter1);
            this.pnlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMiddle.Location = new System.Drawing.Point(246, 83);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Size = new System.Drawing.Size(962, 377);
            this.pnlMiddle.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMiddle.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlMiddle.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlMiddle.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMiddle.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMiddle.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMiddle.Style.GradientAngle = 90;
            this.pnlMiddle.TabIndex = 127;
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(15, 6);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(15, 14);
            this.chkSelectAll.TabIndex = 7;
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // dgvSalaryDetails
            // 
            this.dgvSalaryDetails.AllowUserToAddRows = false;
            this.dgvSalaryDetails.AllowUserToDeleteRows = false;
            this.dgvSalaryDetails.AllowUserToResizeColumns = false;
            this.dgvSalaryDetails.AllowUserToResizeRows = false;
            this.dgvSalaryDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSalaryDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvSalaryDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSalaryDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SelectAll});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSalaryDetails.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSalaryDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSalaryDetails.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSalaryDetails.Location = new System.Drawing.Point(5, 0);
            this.dgvSalaryDetails.Name = "dgvSalaryDetails";
            this.dgvSalaryDetails.RowHeadersVisible = false;
            this.dgvSalaryDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSalaryDetails.Size = new System.Drawing.Size(957, 377);
            this.dgvSalaryDetails.TabIndex = 2;
            this.dgvSalaryDetails.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSalaryDetails_CellMouseClick);
            this.dgvSalaryDetails.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalaryDetails_CellDoubleClick);
            this.dgvSalaryDetails.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSalaryDetails_ColumnHeaderMouseClick);
            this.dgvSalaryDetails.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalaryDetails_CellClick);
            this.dgvSalaryDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvSalaryDetails_CurrentCellDirtyStateChanged);
            this.dgvSalaryDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvSalaryDetails_DataError);
            // 
            // SelectAll
            // 
            this.SelectAll.DataPropertyName = "SelectAll";
            this.SelectAll.HeaderText = "";
            this.SelectAll.Name = "SelectAll";
            this.SelectAll.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SelectAll.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // expandableSplitter1
            // 
            this.expandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter1.ExpandableControl = this.pnlLeft;
            this.expandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.Location = new System.Drawing.Point(0, 0);
            this.expandableSplitter1.Name = "expandableSplitter1";
            this.expandableSplitter1.Size = new System.Drawing.Size(5, 377);
            this.expandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expandableSplitter1.TabIndex = 0;
            this.expandableSplitter1.TabStop = false;
            this.expandableSplitter1.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expandableSplitter1_ExpandedChanged);
            // 
            // pnlLeft
            // 
            this.pnlLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLeft.Controls.Add(this.trvSalaryRelease);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 83);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(246, 377);
            this.pnlLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeft.Style.GradientAngle = 90;
            this.pnlLeft.TabIndex = 126;
            // 
            // trvSalaryRelease
            // 
            this.trvSalaryRelease.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trvSalaryRelease.Location = new System.Drawing.Point(0, 0);
            this.trvSalaryRelease.Name = "trvSalaryRelease";
            this.trvSalaryRelease.Size = new System.Drawing.Size(246, 377);
            this.trvSalaryRelease.TabIndex = 2;
            this.trvSalaryRelease.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.trvSalaryProcessRelease_NodeMouseClick);
            // 
            // expPnlSalaryRelease
            // 
            this.expPnlSalaryRelease.CanvasColor = System.Drawing.SystemColors.Control;
            this.expPnlSalaryRelease.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expPnlSalaryRelease.Controls.Add(this.bRelease);
            this.expPnlSalaryRelease.Controls.Add(this.pnlSalaryReleaseBottom);
            this.expPnlSalaryRelease.Dock = System.Windows.Forms.DockStyle.Top;
            this.expPnlSalaryRelease.Location = new System.Drawing.Point(0, 0);
            this.expPnlSalaryRelease.Name = "expPnlSalaryRelease";
            this.expPnlSalaryRelease.Size = new System.Drawing.Size(1208, 83);
            this.expPnlSalaryRelease.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expPnlSalaryRelease.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expPnlSalaryRelease.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expPnlSalaryRelease.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expPnlSalaryRelease.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expPnlSalaryRelease.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expPnlSalaryRelease.Style.GradientAngle = 90;
            this.expPnlSalaryRelease.TabIndex = 125;
            this.expPnlSalaryRelease.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expPnlSalaryRelease.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expPnlSalaryRelease.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expPnlSalaryRelease.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expPnlSalaryRelease.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expPnlSalaryRelease.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expPnlSalaryRelease.TitleStyle.GradientAngle = 90;
            this.expPnlSalaryRelease.TitleText = "Salary Release";
            this.expPnlSalaryRelease.ExpandedChanged += new DevComponents.DotNetBar.ExpandChangeEventHandler(this.expPnlSalaryRelease_ExpandedChanged);
            // 
            // bRelease
            // 
            this.bRelease.AntiAlias = true;
            this.bRelease.Dock = System.Windows.Forms.DockStyle.Top;
            this.bRelease.DockSide = DevComponents.DotNetBar.eDockSide.Top;
            this.bRelease.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnEmail,
            this.btnPrint,
            this.btnSalarySlip,
            this.BtnAddAdditionDeduction,
            this.btnPayment,
            this.btnSIF,
            this.btnAddPariculars,
            this.labelItem1,
            this.txtSearchFor,
            this.labelItem2,
            this.cboRepColForSearch,
            this.btnSearch,
            this.btnHelp});
            this.bRelease.Location = new System.Drawing.Point(0, 26);
            this.bRelease.Name = "bRelease";
            this.bRelease.Size = new System.Drawing.Size(1208, 28);
            this.bRelease.Stretch = true;
            this.bRelease.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bRelease.TabIndex = 2;
            this.bRelease.TabStop = false;
            this.bRelease.Text = "bar1";
            // 
            // btnEmail
            // 
            this.btnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Text = "Email";
            this.btnEmail.Tooltip = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BeginGroup = true;
            this.btnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Text = "Print";
            this.btnPrint.Tooltip = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSalarySlip
            // 
            this.btnSalarySlip.BeginGroup = true;
            this.btnSalarySlip.Image = global::MyPayfriend.Properties.Resources.SalaryStructure1;
            this.btnSalarySlip.Name = "btnSalarySlip";
            this.btnSalarySlip.Text = "Pay Slip";
            this.btnSalarySlip.Tooltip = "Pay Slip";
            this.btnSalarySlip.Click += new System.EventHandler(this.btnSalarySlip_Click);
            // 
            // BtnAddAdditionDeduction
            // 
            this.BtnAddAdditionDeduction.BeginGroup = true;
            this.BtnAddAdditionDeduction.Image = global::MyPayfriend.Properties.Resources.Addition_Deduction2;
            this.BtnAddAdditionDeduction.Name = "BtnAddAdditionDeduction";
            this.BtnAddAdditionDeduction.Text = "Add Additions / Deductions";
            this.BtnAddAdditionDeduction.Tooltip = "Add Additions / Deductions";
            this.BtnAddAdditionDeduction.Visible = false;
            this.BtnAddAdditionDeduction.Click += new System.EventHandler(this.BtnAddAdditionDeduction_Click);
            // 
            // btnPayment
            // 
            this.btnPayment.BeginGroup = true;
            this.btnPayment.Image = global::MyPayfriend.Properties.Resources.Payment1;
            this.btnPayment.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.btnPayment.Name = "btnPayment";
            this.btnPayment.Text = "Payment";
            this.btnPayment.Tooltip = "Payment";
            this.btnPayment.Click += new System.EventHandler(this.btnPayment_Click);
            // 
            // btnSIF
            // 
            this.btnSIF.BeginGroup = true;
            this.btnSIF.Image = global::MyPayfriend.Properties.Resources.Role_Settings;
            this.btnSIF.Name = "btnSIF";
            this.btnSIF.Text = "SIF";
            this.btnSIF.Tooltip = "SIF";
            this.btnSIF.Click += new System.EventHandler(this.btnSIF_Click);
            // 
            // btnAddPariculars
            // 
            this.btnAddPariculars.Image = ((System.Drawing.Image)(resources.GetObject("btnAddPariculars.Image")));
            this.btnAddPariculars.Name = "btnAddPariculars";
            this.btnAddPariculars.Text = "Add Particulars";
            this.btnAddPariculars.Tooltip = "Add Particulars";
            this.btnAddPariculars.Click += new System.EventHandler(this.btnAddPariculars_Click);
            // 
            // labelItem1
            // 
            this.labelItem1.BeginGroup = true;
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "Search";
            // 
            // txtSearchFor
            // 
            this.txtSearchFor.Name = "txtSearchFor";
            this.txtSearchFor.TextBoxWidth = 200;
            this.txtSearchFor.WatermarkColor = System.Drawing.SystemColors.GrayText;
            // 
            // labelItem2
            // 
            this.labelItem2.Name = "labelItem2";
            this.labelItem2.Text = "In";
            // 
            // cboRepColForSearch
            // 
            this.cboRepColForSearch.ComboWidth = 200;
            this.cboRepColForSearch.DropDownHeight = 105;
            this.cboRepColForSearch.ItemHeight = 17;
            this.cboRepColForSearch.Name = "cboRepColForSearch";
            this.cboRepColForSearch.Tooltip = "Select Column Name";
            this.cboRepColForSearch.WatermarkText = "Select Column Name";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem9";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.Image = global::MyPayfriend.Properties.Resources.help;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Text = "Help";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // pnlSalaryReleaseBottom
            // 
            this.pnlSalaryReleaseBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlSalaryReleaseBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlSalaryReleaseBottom.Controls.Add(this.btnShow);
            this.pnlSalaryReleaseBottom.Controls.Add(this.cboDesignationFil);
            this.pnlSalaryReleaseBottom.Controls.Add(this.label13);
            this.pnlSalaryReleaseBottom.Controls.Add(this.cboDepartmentFil);
            this.pnlSalaryReleaseBottom.Controls.Add(this.label5);
            this.pnlSalaryReleaseBottom.Controls.Add(this.btnFilterClear);
            this.pnlSalaryReleaseBottom.Controls.Add(this.cboFilterBank);
            this.pnlSalaryReleaseBottom.Controls.Add(this.Label16);
            this.pnlSalaryReleaseBottom.Controls.Add(this.cboFilterTransactionType);
            this.pnlSalaryReleaseBottom.Controls.Add(this.Label15);
            this.pnlSalaryReleaseBottom.Controls.Add(this.cboFilterCompany);
            this.pnlSalaryReleaseBottom.Controls.Add(this.Label12);
            this.pnlSalaryReleaseBottom.Controls.Add(this.cboFilterProcessYear);
            this.pnlSalaryReleaseBottom.Controls.Add(this.Label11);
            this.pnlSalaryReleaseBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlSalaryReleaseBottom.Location = new System.Drawing.Point(0, 50);
            this.pnlSalaryReleaseBottom.Name = "pnlSalaryReleaseBottom";
            this.pnlSalaryReleaseBottom.Size = new System.Drawing.Size(1208, 33);
            this.pnlSalaryReleaseBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlSalaryReleaseBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlSalaryReleaseBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlSalaryReleaseBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlSalaryReleaseBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlSalaryReleaseBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlSalaryReleaseBottom.Style.GradientAngle = 90;
            this.pnlSalaryReleaseBottom.TabIndex = 1;
            this.pnlSalaryReleaseBottom.Click += new System.EventHandler(this.pnlSalaryReleaseBottom_Click);
            // 
            // btnShow
            // 
            this.btnShow.FlatAppearance.BorderSize = 0;
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShow.Image = global::MyPayfriend.Properties.Resources.Go;
            this.btnShow.Location = new System.Drawing.Point(1151, 6);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(19, 21);
            this.btnShow.TabIndex = 119;
            this.ToolStripDes.SetToolTip(this.btnShow, "Show");
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cboDesignationFil
            // 
            this.cboDesignationFil.AllowDrop = true;
            this.cboDesignationFil.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDesignationFil.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDesignationFil.DropDownHeight = 105;
            this.cboDesignationFil.FormattingEnabled = true;
            this.cboDesignationFil.IntegralHeight = false;
            this.cboDesignationFil.Location = new System.Drawing.Point(959, 6);
            this.cboDesignationFil.Name = "cboDesignationFil";
            this.cboDesignationFil.Size = new System.Drawing.Size(180, 21);
            this.cboDesignationFil.TabIndex = 118;
            this.ToolStripDes.SetToolTip(this.cboDesignationFil, "Designation");
            this.cboDesignationFil.SelectedIndexChanged += new System.EventHandler(this.cboDesignationFil_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.Image = ((System.Drawing.Image)(resources.GetObject("label13.Image")));
            this.label13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label13.Location = new System.Drawing.Point(934, 4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 21);
            this.label13.TabIndex = 117;
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.label13, "Department");
            // 
            // cboDepartmentFil
            // 
            this.cboDepartmentFil.AllowDrop = true;
            this.cboDepartmentFil.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartmentFil.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartmentFil.DropDownHeight = 105;
            this.cboDepartmentFil.FormattingEnabled = true;
            this.cboDepartmentFil.IntegralHeight = false;
            this.cboDepartmentFil.Location = new System.Drawing.Point(729, 5);
            this.cboDepartmentFil.Name = "cboDepartmentFil";
            this.cboDepartmentFil.Size = new System.Drawing.Size(199, 21);
            this.cboDepartmentFil.TabIndex = 116;
            this.ToolStripDes.SetToolTip(this.cboDepartmentFil, "Department");
            this.cboDepartmentFil.SelectedIndexChanged += new System.EventHandler(this.cboDepartmentFil_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(699, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 21);
            this.label5.TabIndex = 115;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.label5, "Department");
            // 
            // btnFilterClear
            // 
            this.btnFilterClear.FlatAppearance.BorderSize = 0;
            this.btnFilterClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilterClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnFilterClear.Location = new System.Drawing.Point(1176, 6);
            this.btnFilterClear.Name = "btnFilterClear";
            this.btnFilterClear.Size = new System.Drawing.Size(19, 21);
            this.btnFilterClear.TabIndex = 114;
            this.ToolStripDes.SetToolTip(this.btnFilterClear, "Clear");
            this.btnFilterClear.UseVisualStyleBackColor = true;
            this.btnFilterClear.Click += new System.EventHandler(this.btnFilterClear_Click);
            // 
            // cboFilterBank
            // 
            this.cboFilterBank.AllowDrop = true;
            this.cboFilterBank.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterBank.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterBank.DropDownHeight = 105;
            this.cboFilterBank.FormattingEnabled = true;
            this.cboFilterBank.IntegralHeight = false;
            this.cboFilterBank.Location = new System.Drawing.Point(493, 6);
            this.cboFilterBank.Name = "cboFilterBank";
            this.cboFilterBank.Size = new System.Drawing.Size(200, 21);
            this.cboFilterBank.TabIndex = 105;
            this.cboFilterBank.SelectedIndexChanged += new System.EventHandler(this.cboFilterBank_SelectedIndexChanged);
            this.cboFilterBank.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            this.cboFilterBank.TextChanged += new System.EventHandler(this.cboFilterBank_SelectedIndexChanged);
            // 
            // Label16
            // 
            this.Label16.Image = ((System.Drawing.Image)(resources.GetObject("Label16.Image")));
            this.Label16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label16.Location = new System.Drawing.Point(463, 6);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(24, 21);
            this.Label16.TabIndex = 113;
            this.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.Label16, "Bank");
            // 
            // cboFilterTransactionType
            // 
            this.cboFilterTransactionType.AllowDrop = true;
            this.cboFilterTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterTransactionType.DropDownHeight = 105;
            this.cboFilterTransactionType.FormattingEnabled = true;
            this.cboFilterTransactionType.IntegralHeight = false;
            this.cboFilterTransactionType.Location = new System.Drawing.Point(370, 6);
            this.cboFilterTransactionType.Name = "cboFilterTransactionType";
            this.cboFilterTransactionType.Size = new System.Drawing.Size(87, 21);
            this.cboFilterTransactionType.TabIndex = 104;
            this.cboFilterTransactionType.SelectedIndexChanged += new System.EventHandler(this.cboFilterTransactionType_SelectedIndexChanged);
            this.cboFilterTransactionType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            this.cboFilterTransactionType.TextChanged += new System.EventHandler(this.cboFilterTransactionType_SelectedIndexChanged);
            // 
            // Label15
            // 
            this.Label15.Image = ((System.Drawing.Image)(resources.GetObject("Label15.Image")));
            this.Label15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label15.Location = new System.Drawing.Point(343, 6);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(23, 21);
            this.Label15.TabIndex = 112;
            this.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.Label15, "Transaction Type");
            // 
            // cboFilterCompany
            // 
            this.cboFilterCompany.AllowDrop = true;
            this.cboFilterCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterCompany.DropDownHeight = 105;
            this.cboFilterCompany.FormattingEnabled = true;
            this.cboFilterCompany.IntegralHeight = false;
            this.cboFilterCompany.Location = new System.Drawing.Point(137, 7);
            this.cboFilterCompany.Name = "cboFilterCompany";
            this.cboFilterCompany.Size = new System.Drawing.Size(200, 21);
            this.cboFilterCompany.TabIndex = 102;
            this.cboFilterCompany.SelectedIndexChanged += new System.EventHandler(this.cboFilterCompany_SelectedIndexChanged);
            this.cboFilterCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            this.cboFilterCompany.TextChanged += new System.EventHandler(this.cboFilterCompany_SelectedIndexChanged);
            // 
            // Label12
            // 
            this.Label12.Image = ((System.Drawing.Image)(resources.GetObject("Label12.Image")));
            this.Label12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label12.Location = new System.Drawing.Point(116, 8);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(23, 21);
            this.Label12.TabIndex = 109;
            this.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.Label12, "Company");
            // 
            // cboFilterProcessYear
            // 
            this.cboFilterProcessYear.AllowDrop = true;
            this.cboFilterProcessYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterProcessYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterProcessYear.DropDownHeight = 105;
            this.cboFilterProcessYear.FormattingEnabled = true;
            this.cboFilterProcessYear.IntegralHeight = false;
            this.cboFilterProcessYear.Location = new System.Drawing.Point(25, 8);
            this.cboFilterProcessYear.Name = "cboFilterProcessYear";
            this.cboFilterProcessYear.Size = new System.Drawing.Size(87, 21);
            this.cboFilterProcessYear.TabIndex = 101;
            this.cboFilterProcessYear.SelectedIndexChanged += new System.EventHandler(this.cboFilterProcessYear_SelectedIndexChanged);
            this.cboFilterProcessYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            this.cboFilterProcessYear.TextChanged += new System.EventHandler(this.cboFilterProcessYear_SelectedIndexChanged);
            // 
            // Label11
            // 
            this.Label11.Image = global::MyPayfriend.Properties.Resources.Financial__year;
            this.Label11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label11.Location = new System.Drawing.Point(6, 8);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(17, 21);
            this.Label11.TabIndex = 108;
            this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.Label11, "Process Year");
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.ItemPanelMiddileBottom);
            this.pnlBottom.Controls.Add(this.pnlMentioned);
            this.pnlBottom.Controls.Add(this.btnRelease);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 460);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1208, 45);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 124;
            // 
            // ItemPanelMiddileBottom
            // 
            this.ItemPanelMiddileBottom.AntiAlias = true;
            this.ItemPanelMiddileBottom.BackColor = System.Drawing.Color.Transparent;
            this.ItemPanelMiddileBottom.DockSide = DevComponents.DotNetBar.eDockSide.Bottom;
            this.ItemPanelMiddileBottom.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cboCountItem,
            this.btnFirstGridCollection,
            this.btnPreviousGridCollection,
            this.lblPageCount,
            this.btnNextGridCollection,
            this.btnLastGridCollection});
            this.ItemPanelMiddileBottom.Location = new System.Drawing.Point(3, 8);
            this.ItemPanelMiddileBottom.Name = "ItemPanelMiddileBottom";
            this.ItemPanelMiddileBottom.Size = new System.Drawing.Size(334, 28);
            this.ItemPanelMiddileBottom.Stretch = true;
            this.ItemPanelMiddileBottom.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ItemPanelMiddileBottom.TabIndex = 115;
            this.ItemPanelMiddileBottom.TabStop = false;
            this.ItemPanelMiddileBottom.Text = "bar1";
            // 
            // cboCountItem
            // 
            this.cboCountItem.DropDownHeight = 106;
            this.cboCountItem.ItemHeight = 17;
            this.cboCountItem.Name = "cboCountItem";
            this.cboCountItem.SelectedIndexChanged += new System.EventHandler(this.cboCountItem_SelectedIndexChanged);
            // 
            // btnFirstGridCollection
            // 
            this.btnFirstGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowLeftStart;
            this.btnFirstGridCollection.Name = "btnFirstGridCollection";
            this.btnFirstGridCollection.Text = "First";
            this.btnFirstGridCollection.Tooltip = "First";
            this.btnFirstGridCollection.Click += new System.EventHandler(this.btnFirstGridCollection_Click);
            // 
            // btnPreviousGridCollection
            // 
            this.btnPreviousGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowLeft;
            this.btnPreviousGridCollection.Name = "btnPreviousGridCollection";
            this.btnPreviousGridCollection.Text = "Previous";
            this.btnPreviousGridCollection.Tooltip = "Previous";
            this.btnPreviousGridCollection.Click += new System.EventHandler(this.btnPreviousGridCollection_Click);
            // 
            // lblPageCount
            // 
            this.lblPageCount.BeginGroup = true;
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.Text = "{0} of {0}";
            // 
            // btnNextGridCollection
            // 
            this.btnNextGridCollection.BeginGroup = true;
            this.btnNextGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowRight;
            this.btnNextGridCollection.Name = "btnNextGridCollection";
            this.btnNextGridCollection.Text = "Next";
            this.btnNextGridCollection.Tooltip = "Next";
            this.btnNextGridCollection.Click += new System.EventHandler(this.btnNextGridCollection_Click);
            // 
            // btnLastGridCollection
            // 
            this.btnLastGridCollection.Image = global::MyPayfriend.Properties.Resources.ArrowRightStart;
            this.btnLastGridCollection.Name = "btnLastGridCollection";
            this.btnLastGridCollection.Text = "Last";
            this.btnLastGridCollection.Tooltip = "Last";
            this.btnLastGridCollection.Click += new System.EventHandler(this.btnLastGridCollection_Click);
            // 
            // pnlMentioned
            // 
            this.pnlMentioned.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlMentioned.Controls.Add(this.Label10);
            this.pnlMentioned.Controls.Add(this.PictureBox3);
            this.pnlMentioned.Controls.Add(this.Label8);
            this.pnlMentioned.Controls.Add(this.Label9);
            this.pnlMentioned.Controls.Add(this.PictureBox1);
            this.pnlMentioned.Controls.Add(this.PictureBox2);
            this.pnlMentioned.Location = new System.Drawing.Point(382, 10);
            this.pnlMentioned.Name = "pnlMentioned";
            this.pnlMentioned.Size = new System.Drawing.Size(465, 25);
            this.pnlMentioned.TabIndex = 125;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(339, 6);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(119, 13);
            this.Label10.TabIndex = 5;
            this.Label10.Text = "Salary Process Possible";
            // 
            // PictureBox3
            // 
            this.PictureBox3.BackColor = System.Drawing.Color.White;
            this.PictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox3.Location = new System.Drawing.Point(311, 5);
            this.PictureBox3.Name = "PictureBox3";
            this.PictureBox3.Size = new System.Drawing.Size(22, 15);
            this.PictureBox3.TabIndex = 4;
            this.PictureBox3.TabStop = false;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(35, 6);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(122, 13);
            this.Label8.TabIndex = 1;
            this.Label8.Text = "Salary Already Released";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(191, 6);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(114, 13);
            this.Label9.TabIndex = 3;
            this.Label9.Text = "Cannot Process Salary";
            // 
            // PictureBox1
            // 
            this.PictureBox1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox1.Location = new System.Drawing.Point(7, 5);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(22, 15);
            this.PictureBox1.TabIndex = 0;
            this.PictureBox1.TabStop = false;
            // 
            // PictureBox2
            // 
            this.PictureBox2.BackColor = System.Drawing.Color.Firebrick;
            this.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox2.Location = new System.Drawing.Point(163, 5);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(22, 15);
            this.PictureBox2.TabIndex = 2;
            this.PictureBox2.TabStop = false;
            // 
            // btnRelease
            // 
            this.btnRelease.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRelease.Image = global::MyPayfriend.Properties.Resources.Salary_processing;
            this.btnRelease.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRelease.Location = new System.Drawing.Point(989, 2);
            this.btnRelease.Name = "btnRelease";
            this.btnRelease.Size = new System.Drawing.Size(90, 40);
            this.btnRelease.TabIndex = 0;
            this.btnRelease.Text = "&Release";
            this.btnRelease.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.btnRelease, "Release");
            this.btnRelease.UseVisualStyleBackColor = true;
            this.btnRelease.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = global::MyPayfriend.Properties.Resources.Logout1;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(1085, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 40);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolStripDes.SetToolTip(this.btnCancel, "Cancel");
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // LabelItem24
            // 
            this.LabelItem24.Name = "LabelItem24";
            this.LabelItem24.Text = "|";
            // 
            // LabelItem25
            // 
            this.LabelItem25.BeginGroup = true;
            this.LabelItem25.Name = "LabelItem25";
            this.LabelItem25.Text = "{0} of {0}";
            // 
            // LabelItem26
            // 
            this.LabelItem26.Name = "LabelItem26";
            this.LabelItem26.Text = "|";
            // 
            // buttonItem4
            // 
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Text = "Last";
            // 
            // buttonItem3
            // 
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Text = "Next";
            // 
            // labelItem5
            // 
            this.labelItem5.Name = "labelItem5";
            this.labelItem5.Text = "|";
            // 
            // labelItem4
            // 
            this.labelItem4.BeginGroup = true;
            this.labelItem4.Name = "labelItem4";
            this.labelItem4.Text = "{0} of {0}";
            // 
            // labelItem3
            // 
            this.labelItem3.Name = "labelItem3";
            this.labelItem3.Text = "|";
            // 
            // buttonItem2
            // 
            this.buttonItem2.Image = global::MyPayfriend.Properties.Resources.ArrowLeft;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Text = "Previous";
            // 
            // buttonItem1
            // 
            this.buttonItem1.Image = global::MyPayfriend.Properties.Resources.ArrowLeftStart;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "First";
            // 
            // comboBoxItem1
            // 
            this.comboBoxItem1.Caption = "ComboBoxItem1";
            this.comboBoxItem1.DropDownHeight = 106;
            this.comboBoxItem1.Name = "comboBoxItem1";
            // 
            // ItemContainer5
            // 
            // 
            // 
            // 
            this.ItemContainer5.BackgroundStyle.Class = "";
            this.ItemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ItemContainer5.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.ItemContainer5.Name = "ItemContainer5";
            this.ItemContainer5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.comboBoxItem1,
            this.buttonItem1,
            this.buttonItem2,
            this.labelItem3,
            this.labelItem4,
            this.labelItem5,
            this.buttonItem3,
            this.buttonItem4});
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // DeleteContextMenuStrip
            // 
            this.DeleteContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemDelete});
            this.DeleteContextMenuStrip.Name = "CopyDetailsContextMenuStrip";
            this.DeleteContextMenuStrip.Size = new System.Drawing.Size(108, 26);
            // 
            // ToolStripMenuItemDelete
            // 
            this.ToolStripMenuItemDelete.Name = "ToolStripMenuItemDelete";
            this.ToolStripMenuItemDelete.Size = new System.Drawing.Size(107, 22);
            this.ToolStripMenuItemDelete.Text = "Delete";
            this.ToolStripMenuItemDelete.Click += new System.EventHandler(this.ToolStripMenuItemDelete_Click);
            // 
            // tmrSalryProcessRelease
            // 
            this.tmrSalryProcessRelease.Interval = 2000;
            this.tmrSalryProcessRelease.Tick += new System.EventHandler(this.tmrSalryProcessRelease_Tick);
            // 
            // errSalaryProcessRelease
            // 
            this.errSalaryProcessRelease.ContainerControl = this;
            // 
            // TimerFormSize
            // 
            this.TimerFormSize.Interval = 2000;
            // 
            // DeleteSingleRowContextMenuStrip
            // 
            this.DeleteSingleRowContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnSingleRow});
            this.DeleteSingleRowContextMenuStrip.Name = "CopyDetailsContextMenuStrip";
            this.DeleteSingleRowContextMenuStrip.Size = new System.Drawing.Size(153, 26);
            // 
            // BtnSingleRow
            // 
            this.BtnSingleRow.Name = "BtnSingleRow";
            this.BtnSingleRow.Size = new System.Drawing.Size(152, 22);
            this.BtnSingleRow.Text = "Delete this row";
            this.BtnSingleRow.Click += new System.EventHandler(this.BtnSingleRow_Click);
            // 
            // FrmSalaryRelease
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1208, 527);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.ToolStripStatus);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmSalaryRelease";
            this.Text = "Salary Release";
            this.Deactivate += new System.EventHandler(this.FrmSalaryProcessRelease_Deactivate);
            this.Load += new System.EventHandler(this.FrmSalaryProcessRelease_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmSalaryProcessRelease_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSalaryProcessRelease_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSalaryProcessRelease_KeyDown);
            this.ToolStripStatus.ResumeLayout(false);
            this.ToolStripStatus.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlMiddle.ResumeLayout(false);
            this.pnlMiddle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalaryDetails)).EndInit();
            this.pnlLeft.ResumeLayout(false);
            this.expPnlSalaryRelease.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bRelease)).EndInit();
            this.pnlSalaryReleaseBottom.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemPanelMiddileBottom)).EndInit();
            this.pnlMentioned.ResumeLayout(false);
            this.pnlMentioned.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            this.DeleteContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errSalaryProcessRelease)).EndInit();
            this.DeleteSingleRowContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.StatusStrip ToolStripStatus;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel2;
        internal System.Windows.Forms.ToolStripProgressBar tsProgressBar;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private DevComponents.DotNetBar.PanelEx pnlMain;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.PictureBox PictureBox3;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.PictureBox PictureBox2;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.Button btnRelease;
        internal System.Windows.Forms.Button btnCancel;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private System.Windows.Forms.Panel pnlMentioned;
        private DevComponents.DotNetBar.ExpandablePanel expPnlSalaryRelease;
        private DevComponents.DotNetBar.PanelEx pnlSalaryReleaseBottom;
        internal System.Windows.Forms.Button btnFilterClear;
        internal System.Windows.Forms.ComboBox cboFilterBank;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.ComboBox cboFilterTransactionType;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.ComboBox cboFilterCompany;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.ComboBox cboFilterProcessYear;
        internal System.Windows.Forms.Label Label11;
        private DevComponents.DotNetBar.Bar bRelease;
        private DevComponents.DotNetBar.ButtonItem btnEmail;
        private DevComponents.DotNetBar.ButtonItem btnPrint;
        private DevComponents.DotNetBar.ButtonItem btnSalarySlip;
        private DevComponents.DotNetBar.ButtonItem BtnAddAdditionDeduction;
        private DevComponents.DotNetBar.ButtonItem btnPayment;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.TextBoxItem txtSearchFor;
        private DevComponents.DotNetBar.PanelEx pnlLeft;
        private DevComponents.DotNetBar.LabelItem labelItem2;
        private DevComponents.DotNetBar.ComboBoxItem cboRepColForSearch;
        internal System.Windows.Forms.TreeView trvSalaryRelease;
        private DevComponents.DotNetBar.PanelEx pnlMiddle;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter1;
        internal DevComponents.DotNetBar.LabelItem LabelItem24;
        internal DevComponents.DotNetBar.LabelItem LabelItem25;
        internal DevComponents.DotNetBar.LabelItem LabelItem26;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSalaryDetails;
        internal DevComponents.DotNetBar.ButtonItem buttonItem4;
        internal DevComponents.DotNetBar.ButtonItem buttonItem3;
        internal DevComponents.DotNetBar.LabelItem labelItem5;
        internal DevComponents.DotNetBar.LabelItem labelItem4;
        internal DevComponents.DotNetBar.LabelItem labelItem3;
        internal DevComponents.DotNetBar.ButtonItem buttonItem2;
        internal DevComponents.DotNetBar.ButtonItem buttonItem1;
        internal DevComponents.DotNetBar.ComboBoxItem comboBoxItem1;
        internal DevComponents.DotNetBar.ItemContainer ItemContainer5;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        internal System.Windows.Forms.CheckBox chkSelectAll;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        internal System.Windows.Forms.ContextMenuStrip DeleteContextMenuStrip;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemDelete;
        internal System.Windows.Forms.Timer tmrSalryProcessRelease;
        internal System.Windows.Forms.ErrorProvider errSalaryProcessRelease;
        internal System.Windows.Forms.Timer TimerFormSize;
        internal System.Windows.Forms.ContextMenuStrip DeleteSingleRowContextMenuStrip;
        internal System.Windows.Forms.ToolStripMenuItem BtnSingleRow;
        private DevComponents.DotNetBar.ButtonItem btnSIF;
        private DevComponents.DotNetBar.Bar ItemPanelMiddileBottom;
        private DevComponents.DotNetBar.ComboBoxItem cboCountItem;
        private DevComponents.DotNetBar.ButtonItem btnFirstGridCollection;
        private DevComponents.DotNetBar.ButtonItem btnPreviousGridCollection;
        private DevComponents.DotNetBar.LabelItem lblPageCount;
        private DevComponents.DotNetBar.ButtonItem btnNextGridCollection;
        private DevComponents.DotNetBar.ButtonItem btnLastGridCollection;
        private DevComponents.DotNetBar.ButtonItem btnAddPariculars;
        private System.Windows.Forms.ToolTip ToolStripDes;
        internal System.Windows.Forms.ComboBox cboDepartmentFil;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.ComboBox cboDesignationFil;
        internal System.Windows.Forms.Label label13;
        private DevComponents.DotNetBar.ButtonItem btnHelp;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectAll;
        internal System.Windows.Forms.Button btnShow;

    }
}