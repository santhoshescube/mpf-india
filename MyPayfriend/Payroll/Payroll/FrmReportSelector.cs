﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using BLL;
using DTO;

namespace MyPayfriend
{
    public partial class FrmReportSelector : DevComponents.DotNetBar.Office2007Form
    {

       public DataTable datTempSalary;
       public int PiSettlementID;

        public FrmReportSelector()
        {
            InitializeComponent();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = "Employee Settlement";
                ObjViewer.PiRecId = PiSettlementID;
                ObjViewer.PiFormID = (int)FormID.SettlementProcess;
                if (CboTemplate.SelectedIndex  == 0)
                {
                    ObjViewer.OperationTypeID = 1000;
                }
                else if (CboTemplate.SelectedIndex == 1)
                {
                    ObjViewer.OperationTypeID = 2000;
                }
                else if (CboTemplate.SelectedIndex == 2)
                {
                    ObjViewer.OperationTypeID = 3000;
                }
                else if (CboTemplate.SelectedIndex == 3)
                {
                    ObjViewer.OperationTypeID = -1;
                }
                else if (CboTemplate.SelectedIndex == 4)
                {
                    ObjViewer.OperationTypeID = 4000;
                }


                ObjViewer.No = 0;

                if (datTempSalary != null)
                {
                    if (datTempSalary.Rows.Count > 0)
                    {
                        ObjViewer.dteFromDate = Convert.ToDateTime(datTempSalary.Rows[0]["PeriodFrom"]);
                        ObjViewer.dteToDate = Convert.ToDateTime(datTempSalary.Rows[0]["PeriodTo"]);
                        ObjViewer.No = 1;
                    }
                }

                ObjViewer.ShowDialog();
            }
            catch { }
        }
    }
}
