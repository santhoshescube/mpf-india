﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
/* Design By        : Tijo 
 * Designed Date    : 26 Jun 2013
 * Developed By     : Tijo
 * Develop St.Date  : 26 Jun 2013
 * Develop End Date : 28 Jun 2013
 * Purpose          : Salary Procee and Release
*/
namespace MyPayfriend
{
    public partial class FrmSalaryProcessRelease : DevComponents.DotNetBar.Office2007Form
    {
        private bool MbFirst = false;
        private string NavClicked = "";
        private string RecordID1 = "0";
        private string RecordID2 = "0";
        private int CurrentPage = 1;
        private int TotalPage = 1;
        private DataSet dsTemp = new DataSet();
        private string MstrCommonMessage;
        
        public string strProcessDate;
        public string FillDate;
        public bool FlagSelect;
        public int PBankNameIDSalRel;
        public int PCompanyID;
        public int PProcessYear;

        public int PDepartmentID;
        public int PDesignationID;

     
        clsConnection mObjCon = new clsConnection();
        clsBLLSalaryProcessAndRelease mobjclsBLLSalaryProcessAndRelease = new clsBLLSalaryProcessAndRelease();
        clsBLLSalaryProcess mobjProcess = new clsBLLSalaryProcess();
        private clsMessage ObjUserMessage = null;

        bool norecord;
        int pageRows = 25;
        bool FlgBlk = true;
        bool MChangeStatus; // Check state of the page
        bool MViewPermission;
        bool MAddPermission;
        bool MUpdatePermission;
        bool MDeletePermission;
        bool blnSalaryDayIsEditable = false;
        bool MblnPrintEmailPermission; bool MblnAddPermission; bool MblnUpdatePermission; bool MblnDeletePermission;
        int CurrentMonth;
        int CurrentYear;
        int MCompanyID;
        int MEmployeeID;        
        int MPaymentClassificationID;
        int BranchIndicator;
        int TemplateID;
        int TransactionTypeIDRel;
        int MCurrencyID;
        int intCompanyID = ClsCommonSettings.CurrentCompanyID;
        
        string MFromDate;
        string MToDate;
        string mdtFrom = "";
        string mDtTo = "";
        string mLOP = "";
        string mHLOP = "";
        string mLLOP = "";
        string mSType = "";
        string MstrMessageCaption = ClsCommonSettings.MessageCaption;
        string GetHostName = "";

        DateTime LoadDate;

        DataTable DtProcessEmployee = new DataTable();
        DataTable datEmployeeSalaryDetails = new DataTable();
        DataTable datTempSalary = new DataTable();
              
        TreeNode TvRoot;
        TreeNode TvChild;
        TreeNode TvChildChild;
        bool CobFlg = false;
        string FprocessDate = "";
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalaryRelease);
                return this.ObjUserMessage;
            }
        }

        public FrmSalaryProcessRelease()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
                expPnlSalaryProcess.TitleText  = "عملية الراتب ";
                Label1.Text = "شركة";
                Label7.Text = "الفرع";
                Label2.Text = "من تاريخ";
                Label4.Text = "إلى تاريخ";
                lblDept.Text = "قسم";
                lblDesg.Text = "تعيين";
                btnShow.Text = "عرض";
                chkIncludeCompany.Text = "تشمل شركة ";
                Label6.Text = "دفع جدول: شهري ";
                Label3.Text = "شهر عملية ";
                chkAdvanceSearch.Text = "بحث متقدم ";
                Label8.Text = "تم إصداره بالفعل الراتب ";
                Label9.Text = "لا يمكن معالجة راتب ";
                Label10.Text = "الراتب العملية المحتملة";
            }
        }

        enum NavButton
        {
            First = 1,
            Next = 2,
            Previous = 3,
            Last = 4
        }

        private void FrmSalaryProcessRelease_Load(object sender, EventArgs e)
        {

            expPnlSalaryProcess.Visible = true;
      
            
            pnlMentioned.Visible = true;
            if (ClsCommonSettings.IsArabicView)
            {
                btnProcess.Text = "عملية";
            }
            else
            {
                btnProcess.Text = "&Process";
            }
            btnProcess.Image = Properties.Resources.Salary_processing;
            pnlLeft.Width = 250;
            //this.ToolStripDes.SetToolTip(this.Label16, "Bank");



            cboCountItem.Items.Add("All");
            cboCountItem.Items.Add("25");
            cboCountItem.Items.Add("50");
            cboCountItem.Items.Add("100");
            cboCountItem.Text = "25";
            SetPermissions();
            SetPermissionsRelease();
            ClearControls();
            LoadCombos();

            dgvSalaryDetails.DataSource = getNewTableForProcess();
            datTempSalary = (DataTable)dgvSalaryDetails.DataSource;

            GridColumnDisabled();

            GetHostName = System.Net.Dns.GetHostName().Trim();
        }
        private void SetPermissionsRelease()
        {
            //Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SalaryPayment, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

    
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            
                objDAL.SetArabicVersion((int)FormID.SalaryProcess, this);
           
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetPermissions()
        {
            if (ClsCommonSettings.RoleID <= 3)
                MViewPermission = MAddPermission = MUpdatePermission = MDeletePermission = true;
            else
            {
                clsBLLPermissionSettings objPermission = new clsBLLPermissionSettings();
                int intMenuID = 0;
              
                    intMenuID = (int)eMenuID.SalaryProcess;
        

                objPermission.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (int)eModuleID.Payroll, intMenuID,
                        out MViewPermission, out MAddPermission, out MUpdatePermission, out MDeletePermission);
            }
        }

        private void ClearControls()
        {
            if (!datEmployeeSalaryDetails.Columns.Contains("EmployeeID"))
                datEmployeeSalaryDetails.Columns.Add("EmployeeID");

            if (!datEmployeeSalaryDetails.Columns.Contains("CompanyID"))
                datEmployeeSalaryDetails.Columns.Add("CompanyID");

            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(12, 0); // Get Current Month & Year
            CurrentMonth = datTemp.Rows[0]["MonthNo"].ToInt32();
            CurrentYear = datTemp.Rows[0]["YearNo"].ToInt32();
            LoadDate = ("01/" + GetCurrentMonth(CurrentMonth) + "/" + CurrentYear + "").ToDateTime();

            dtpCurrentMonth.Value = LoadDate;
            dtpFromDate.Value = LoadDate;
            dtpToDate.Value = LoadDate;

            dtpFromDate.Enabled = false;
            dtpToDate.Enabled = false;

            MCompanyID = -1;
            MEmployeeID = -1;
            MFromDate = dtpFromDate.Text.Trim();
            MToDate = dtpToDate.Text.Trim();

          
                btnProcess.Enabled = false;
           
            chkAdvanceSearch.Checked = false;
            getAdvanceSearch();
            getSalaryDayIsEditable();
           
        }

        private bool getSalaryDayIsEditable()
        {
            blnSalaryDayIsEditable = false;

            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] { "UPPER(ConfigurationValue) AS ConfigurationValue", "" +
                "ConfigurationMaster", "ConfigurationItem = 'SalaryDayIsEditable'" });
            
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (datTemp.Rows[0][0].ToString() == "YES")
                        blnSalaryDayIsEditable = true;
                }
            }

            dtpCurrentMonth.Enabled = !blnSalaryDayIsEditable;
            dtpFromDate.Enabled = blnSalaryDayIsEditable;
            dtpToDate.Enabled = false;
            dtpToDate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));

            return blnSalaryDayIsEditable;
        }

        private void getAdvanceSearch()
        {
            lblDept.Visible = chkAdvanceSearch.Checked;
            cboDepartment.Visible = chkAdvanceSearch.Checked;
            lblDesg.Visible = chkAdvanceSearch.Checked;
            cboDesignation.Visible = chkAdvanceSearch.Checked;

            if (chkAdvanceSearch.Checked == false)
                btnShow.Location = new Point(165, cboDepartment.Location.Y);
            else
                btnShow.Location = new Point(165, (cboDesignation.Location.Y + 30));

            cboDepartment.SelectedIndex = -1;
            cboDesignation.SelectedIndex = -1;
        }

        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }

        

        private void LoadCombos()
        {
            try
            {
                CobFlg = true;
                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(1, 0); // Get Company
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datTemp;
                cboCompany.SelectedIndex = -1;

                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(2, 0); // Get Department
                cboDepartment.ValueMember = "DepartmentID";
                cboDepartment.DisplayMember = "Department";
                cboDepartment.DataSource = datTemp;
                cboDepartment.SelectedIndex = -1;

            


                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(3, 0); // Get Designation
                cboDesignation.ValueMember = "DesignationID";
                cboDesignation.DisplayMember = "Designation";
                cboDesignation.DataSource = datTemp;
                cboDesignation.SelectedIndex = -1;



             

                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(11, 0); // Get Payment Classification
                cboPaymentClassification.ValueMember = "PaymentClassificationID";
                cboPaymentClassification.DisplayMember = "PaymentClassification";
                cboPaymentClassification.DataSource = datTemp;
                cboPaymentClassification.SelectedValue = 1;

                cboCompany.SelectedValue = intCompanyID;


                CobFlg = false; 
            }
            catch{
                CobFlg = false; 
            }
        }

        private DataTable getNewTableForProcess()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("SelectAll");
            datTemp.Columns.Add("EmployeeID");
            datTemp.Columns.Add("EmployeeName");
            datTemp.Columns.Add("CompanyID");
            datTemp.Columns.Add("CompanyName");
            datTemp.Columns.Add("DepartmentID");
            datTemp.Columns.Add("Department");
            datTemp.Columns.Add("DesignationID");
            datTemp.Columns.Add("Designation");
            datTemp.Columns.Add("EmploymentTypeID");
            datTemp.Columns.Add("EmploymentType");
            datTemp.Columns.Add("TransactionTypeID");
            datTemp.Columns.Add("TransactionType");
            datTemp.Columns.Add("PaymentClassificationID");
            datTemp.Columns.Add("PaymentClassification");
            datTemp.Columns.Add("BasicPay");
            datTemp.Columns.Add("Currency");
            datTemp.Columns.Add("Reason");
            datTemp.Columns.Add("Released");
            return datTemp;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex != -1)
            {
                DataTable datTempBranch = new DataTable();
                datTempBranch.Columns.Add("BranchID");
                datTempBranch.Columns.Add("BranchName");

                if (cboCompany.SelectedIndex >= 0)
                    datTempBranch = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(7, cboCompany.SelectedValue.ToInt32()); // Get Branch

                cboBranch.ValueMember = "BranchID";
                cboBranch.DisplayMember = "BranchName";
                cboBranch.DataSource = datTempBranch;
                cboBranch.SelectedIndex = 0;

                cboDepartment.SelectedIndex = -1;
                cboDesignation.SelectedIndex = -1;
                cboDepartment.Text = "";
                cboDesignation.Text = "";
            }
        }

      
        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany_SelectedIndexChanged(sender, e);
            ComboBox_KeyPress(sender, e);
        }
        private void PointSelectAll(int Status)
        {
            try
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    int X1 = dgvSalaryDetails.Location.X.ToInt32();
                    int Y1 = dgvSalaryDetails.Location.Y.ToInt32();
                   
                        X1 = X1 + dgvSalaryDetails.Width - 23;
                        chkSelectAll.Location = new Point(X1, Y1);
                   
                }

            }
            catch
            {

            }
        }
        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {

                PointSelectAll(0);
                if (MAddPermission == false)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(9131);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (FormValidationProcess())
                {
                    //if (SaveDetails())
                        Application.DoEvents();
                }

                btnProcess.Enabled = true;
            }
            catch
            {
                btnProcess.Enabled = false;
            }
        }

        private bool FormValidationProcess()
        {
            try
            {
                DateTime MAXDate = new DateTime();
                int CompanyID = 0;

                errSalaryProcessRelease.Clear();
                if (blnSalaryDayIsEditable)
                    MAXDate = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));
                else
                    MAXDate = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, LoadDate));
                
                if (MAXDate < dtpToDate.Value.Date)
                    dtpToDate.Value = MAXDate;                

                if (cboPaymentClassification.SelectedIndex == -1)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(8153);
                    errSalaryProcessRelease.SetError(cboPaymentClassification, MstrCommonMessage.Replace("#", "").Trim());
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);                    
                    lblStatus.Text = MstrCommonMessage.Replace("#", "").Trim();
                    tmrSalryProcessRelease.Enabled = true;
                    cboPaymentClassification.Focus();
                    return false;
                }
                else
                    errSalaryProcessRelease.SetError(cboPaymentClassification, "");

                if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(1807);
                    errSalaryProcessRelease.SetError(dtpFromDate, MstrCommonMessage.Replace("#", "").Trim());
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblStatus.Text = MstrCommonMessage.Replace("#", "").Trim();
                    tmrSalryProcessRelease.Enabled = true;
                    dtpFromDate.Focus();
                    return false;
                }
                else
                    errSalaryProcessRelease.SetError(dtpFromDate, "");                

                if (chkPartial.Checked)
                {
                    if (DateCheckingForPartial() == false)
                    {
                        MstrCommonMessage = UserMessage.GetMessageByCode(1807);
                        errSalaryProcessRelease.SetError(dtpToDate, MstrCommonMessage.Replace("#", "").Trim());
                        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MstrCommonMessage.Replace("#", "").Trim();
                        tmrSalryProcessRelease.Enabled = true;
                        dtpToDate.Focus();
                        return false;
                    }
                }

                if (cboCompany.SelectedIndex != -1)
                {
                    intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                    if (cboBranch.SelectedValue.ToInt32() > 0)
                    {
                        CompanyID = Convert.ToInt32(cboBranch.SelectedValue);
                        BranchIndicator = chkIncludeCompany.Checked ? -1 : -2;

                        if (BranchIndicator == -2)
                            intCompanyID = Convert.ToInt32(cboBranch.SelectedValue);
                    }
                    else if (cboBranch.SelectedValue.ToInt32() == -2)
                    {
                        CompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                        BranchIndicator = -2;
                    }
                    else if (cboBranch.SelectedValue.ToInt32() == -1)
                    {
                        CompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                        BranchIndicator = chkIncludeCompany.Checked ? 0 : -3;
                    }
                }
                else
                {
                    CompanyID = 0;
                    MstrCommonMessage = UserMessage.GetMessageByCode(14);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                MCompanyID = CompanyID;
                MEmployeeID = 0;
                MFromDate = dtpFromDate.Text.Trim();
                MToDate = dtpToDate.Text.Trim();
                MPaymentClassificationID = cboPaymentClassification.SelectedValue.ToInt32();
                GridList(); // Showing Processed and Non Processed Data in the grid


                //if (intSalaryProcessForm == 1)
                //{
                //    GridColumnDisabled();
                //}

                if (dgvSalaryDetails.RowCount > 0)
                {
                    Application.DoEvents();
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool DateCheckingForPartial()
        {
            long DateDiffCnt = 0;
            DateDiffCnt = 0;

            if (cboPaymentClassification.SelectedIndex != -1)
            {
                switch (cboPaymentClassification.SelectedValue.ToInt32())
                {
                    case 1:
                        DateDiffCnt = DateAndTime.DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        if (DateDiffCnt >= 1)
                            return false;
                        else
                            return true;
                    case 2:
                        DateDiffCnt = DateAndTime.DateDiff(DateInterval.Weekday, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        if (DateDiffCnt >= 1)
                            return false;
                        else
                            return true;
                    case 3:
                        DateDiffCnt = DateAndTime.DateDiff(DateInterval.Weekday, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        if (DateDiffCnt * 2 >= 1)
                            return false;
                        else
                            return true;
                    case 4:
                        long DateDiffCnt2 = 0;
                        long DateDiffCnt1 = 0;

                        DateDiffCnt2 = DateAndTime.DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        DateDiffCnt1 = DateAndTime.DateDiff(DateInterval.Day, dtpFromDate.Value.Date, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);

                        if (DateDiffCnt1 < DateDiffCnt2)
                            DateDiffCnt = 1;

                        if (DateDiffCnt >= 1)
                            return false;
                        else
                            return true;
                    default:
                        return false;
                }
            }
            else
                return false;
        }

        private void GridList()
        {
            try
            {
                int intTempDept = 0;
                int intTempDesg = 0;
                int BranchID = 0;
                if (cboDepartment.SelectedIndex >= 0)
                    intTempDept = cboDepartment.SelectedValue.ToInt32();

                if (cboDesignation.SelectedIndex >= 0)
                    intTempDesg = cboDesignation.SelectedValue.ToInt32();
                BranchIndicator = -2;

                if (cboCompany.SelectedIndex != -1)
                {
                    MCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                }
                else
                {
                    MCompanyID = ClsCommonSettings.CurrentCompanyID;
                }

                if (cboBranch.SelectedIndex != -1)
                {
                    BranchID = Convert.ToInt32(cboBranch.SelectedValue);
                }
                else
                {
                    BranchID = ClsCommonSettings.CurrentCompanyID;
                }

                if (cboBranch.SelectedValue.ToInt32() > 0)
                {
                    BranchIndicator = chkIncludeCompany.Checked ? -1 : -2;
                }
                else if (cboBranch.SelectedValue.ToInt32() == -2)
                {
                    BranchIndicator = -2;
                }
                else if (cboBranch.SelectedValue.ToInt32() == -1)
                {
                    BranchIndicator = chkIncludeCompany.Checked ? 0 : -3;
                }

                DataSet ds = new DataSet();
                ds = mobjclsBLLSalaryProcessAndRelease.GetProcessEmployee(0, MCompanyID, MFromDate, MToDate, GetHostName, ClsCommonSettings.UserID, BranchIndicator,
                    intTempDept, intTempDesg, BranchID);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    dgvSalaryDetails.DataSource = ds.Tables[0];
                    datTempSalary = (DataTable)dgvSalaryDetails.DataSource; // For Paging
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        fillDataGrid_dtgBrowse();
                        GridColumnColor();
                    }
                }
                else
                {
                    dgvSalaryDetails.DataSource = getNewTableForProcess();
                    datTempSalary = (DataTable)dgvSalaryDetails.DataSource;

                    GridColumnDisabled();
                }


                dgvSalaryDetails.ClearSelection();
            }
            catch
            {
            }
        }

        private void GridColumnColor()
        {
            GridColumnDisabled();

            for (int i = 0; i <= dgvSalaryDetails.Rows.Count - 1; i++)
            {
                
                    if (dgvSalaryDetails.Columns.Contains("Reason"))
                    {
                        if (dgvSalaryDetails.Rows[i].Cells["Reason"].Value.ToString() != "" &&
                            dgvSalaryDetails.Rows[i].Cells["Released"].Value.ToString() != "1")
                        {
                            dgvSalaryDetails.Rows[i].DefaultCellStyle.ForeColor = System.Drawing.Color.Firebrick;
                            dgvSalaryDetails.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke;
                            dgvSalaryDetails.Rows[i].ReadOnly = true;
                        }
                        else if (dgvSalaryDetails.Rows[i].Cells["Reason"].Value.ToString() == "" &&
                            dgvSalaryDetails.Rows[i].Cells["Released"].Value.ToString() == "1")
                        {
                            dgvSalaryDetails.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.LightSteelBlue;
                            dgvSalaryDetails.Rows[i].ReadOnly = true;
                        }
                    }
              
            }
        }

        private void GridColumnDisabled()
        {
            try
            {
                if (dgvSalaryDetails.Columns.Contains("SelectAll"))
                {
                    dgvSalaryDetails.Columns["SelectAll"].Visible = true;
                    dgvSalaryDetails.Columns["SelectAll"].Width = 30;
                    dgvSalaryDetails.Columns["SelectAll"].HeaderText = "";
                    dgvSalaryDetails.Columns["SelectAll"].ValueType = typeof(bool);
                }

                if (dgvSalaryDetails.Columns.Contains("EmployeeID"))
                    dgvSalaryDetails.Columns["EmployeeID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("CompanyID"))
                    dgvSalaryDetails.Columns["CompanyID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("DepartmentID"))
                    dgvSalaryDetails.Columns["DepartmentID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("DesignationID"))
                    dgvSalaryDetails.Columns["DesignationID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("EmploymentTypeID"))
                    dgvSalaryDetails.Columns["EmploymentTypeID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("TransactionTypeID"))
                    dgvSalaryDetails.Columns["TransactionTypeID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("PaymentClassificationID"))
                    dgvSalaryDetails.Columns["PaymentClassificationID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("Status"))
                    dgvSalaryDetails.Columns["Status"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("VendorID"))
                    dgvSalaryDetails.Columns["VendorID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("EmpVenFlag"))
                    dgvSalaryDetails.Columns["EmpVenFlag"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("PaymentID"))
                    dgvSalaryDetails.Columns["PaymentID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("IsPartial"))
                    dgvSalaryDetails.Columns["IsPartial"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("RecordID"))
                    dgvSalaryDetails.Columns["RecordID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("ProcessYear"))
                    dgvSalaryDetails.Columns["ProcessYear"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("ProcessMonth"))
                    dgvSalaryDetails.Columns["ProcessMonth"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("ProcessDate"))
                    dgvSalaryDetails.Columns["ProcessDate"].Visible = false;
                
                //if (dgvSalaryDetails.Columns.Contains("EmployeeNo"))
                //    dgvSalaryDetails.Columns["EmployeeNo"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("VendorName"))
                    dgvSalaryDetails.Columns["VendorName"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("Released"))
                    dgvSalaryDetails.Columns["Released"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("EmpCurrencyID"))
                    dgvSalaryDetails.Columns["EmpCurrencyID"].Visible = false;

                if (dgvSalaryDetails.Columns.Contains("IsVacationSettle"))
                    dgvSalaryDetails.Columns["IsVacationSettle"].Visible = false;

                //if (intSalaryProcessForm == 2)
                //{
                //    if (dgvSalaryDetails.Columns.Contains("NetAmount"))
                //    {
                //        dgvSalaryDetails.Columns["NetAmount"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //    }
                //}
                //    if (dgvSalaryDetails.Columns.Contains("EmployeeName"))
                //    {
                //        dgvSalaryDetails.Columns["EmployeeName"].Frozen = false;
                //        dgvSalaryDetails.Columns["EmployeeName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //    }
                //}


            }
            catch { }
        }

        private bool SaveDetails()
        {
            try
            {
                int ParaPaymentClassificationID = 0;
                int TypeIndex = 0;
                string ParaProcessDateFrom = "";
                string ParaProcessDateTo = "";

                if (cboPaymentClassification.SelectedIndex != -1)
                    ParaPaymentClassificationID = cboPaymentClassification.SelectedValue.ToInt32();
                else
                    ParaPaymentClassificationID = 0;

                //if (cboTransactionType.SelectedIndex >= 0)
                //    TypeIndex = cboTransactionType.SelectedValue.ToInt32();
                //else
                //    TypeIndex = 0;

                ParaProcessDateFrom = dtpFromDate.Text;
                ParaProcessDateTo = dtpToDate.Text;

                //if (cboTransactionType.SelectedIndex != -1)
                //    TypeIndex = Convert.ToInt32(cboTransactionType.SelectedValue);

                //mobjclsBLLSalaryProcessAndRelease.DeletePayment(ParaProcessDateFrom, GetHostName, ParaProcessDateTo, cboCompany.SelectedValue.ToInt32());

                TypeIndex = 1;
                if (datEmployeeSalaryDetails.Rows.Count > 0)
                {
                    lblStatus.Text = "Salary Processing Initiated";
                    tmrSalryProcessRelease.Enabled = true;

                    tsProgressBar.Visible = true;
                    tsProgressBar.Minimum = 0;
                    tsProgressBar.Maximum = 100;

                    for (int i = 0; i <= datEmployeeSalaryDetails.Rows.Count - 1; i++)
                    {
                        mobjclsBLLSalaryProcessAndRelease.SalaryProcess(Convert.ToInt32(datEmployeeSalaryDetails.Rows[i]["EmployeeID"].ToString()), 
                            Convert.ToInt32(datEmployeeSalaryDetails.Rows[i]["CompanyID"].ToString()), ParaProcessDateFrom, ParaProcessDateTo, 
                            chkPartial.Checked, TypeIndex);
                        tsProgressBar.Value = Convert.ToInt32((i * 100) / datEmployeeSalaryDetails.Rows.Count);
                    }

                    tsProgressBar.Value = 100;
                    tsProgressBar.Visible = false;
                    tsProgressBar.Value = 0;

                    lblStatus.Text = "Salary Processing Successfully Completed";

                    MessageBox.Show(lblStatus.Text, MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information); 

                    tmrSalryProcessRelease.Enabled = true;
            
                }
               
                    if (MblnAddPermission == true || MblnPrintEmailPermission == true || MblnUpdatePermission == true || MblnDeletePermission == true)
                    {
                        btnProcess.Enabled = true;
                    }
                    else
                    {
                        btnProcess.Enabled = false;
                    }
                
             

                return true;
            }
            catch
            {
                return false;
            }
        }

        private void chkPartial_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPartial.Checked)
                dtpToDate.Enabled = false;
            else
            {
                dtpToDate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));
                dtpToDate.Enabled = false;
            }

            if (blnSalaryDayIsEditable)
                dtpToDate.Enabled = false ;
            else
            {
                if (chkPartial.Checked)
                    dtpToDate.Enabled = false;
                else
                    dtpToDate.Enabled = false;
            }
        }

        private void dtpCurrentMonth_ValueChanged(object sender, EventArgs e)
        {
            MChangeStatus = true;
            btnProcess.Enabled = MChangeStatus;
            dtpCurrentMonth.Value = ("01/" + GetCurrentMonth(dtpCurrentMonth.Value.Month) + "/" + (dtpCurrentMonth.Value.Year) + "").ToDateTime();
            dtpFromDate.Value = ("01/" + GetCurrentMonth(dtpCurrentMonth.Value.Month) + "/" + (dtpCurrentMonth.Value.Year) + "").ToDateTime();
            dtpToDate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));
            dtpToDate.Enabled = false;
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            dtpToDate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date));
        }

        private void dgvSalaryDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                dgvSalaryDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch
            {
            }
        }

        private void dgvSalaryDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (e.ColumnIndex >= 0)
                    {
                       
                            if (dgvSalaryDetails.Rows[e.RowIndex].Cells["Reason"].Value.ToString() != "" &&
                                dgvSalaryDetails.Rows[e.RowIndex].Cells["Released"].Value.ToString() != "1")
                                lblStatus.Text = "Not able to process this Employee. Reason : " +
                                    dgvSalaryDetails.Rows[e.RowIndex].Cells["Reason"].Value.ToString();
                            else if (dgvSalaryDetails.Rows[e.RowIndex].Cells["Reason"].Value.ToString() == "" &&
                                dgvSalaryDetails.Rows[e.RowIndex].Cells["Released"].Value.ToString() == "1")
                                lblStatus.Text = "This Month Salary of this Employee is Processed and Released";
                            else
                            {
                                lblStatus.Text = "";

                                for (int iCounter = 0; iCounter <= datTempSalary.Rows.Count - 1; iCounter++)
                                {
                                    if (datTempSalary.Rows[iCounter]["EmployeeID"].ToInt32() == dgvSalaryDetails.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToInt32())
                                    {
                                        datTempSalary.Rows[iCounter]["SelectAll"] = true;
                                        break;
                                    }
                                }
                            }
                                         
                    }
                }
                //-------


                //TreeNode tr = new TreeNode();
                //tr = trvSalaryProcessRelease.SelectedNode;      
                //if (tr != null)
                //{
                //    trvSalaryProcessRelease.LabelEdit = true;
                //    tr.BeginEdit();
                //    trvSalaryProcessRelease.SelectedNode = tr;
                //}
            }
            catch
            {
            }

        }

        private void dgvSalaryDetails_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                GridColumnColor();
            }
            catch
            {
            }
        }

      
        private bool CheckReleased(int PaymentID)
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] {"PaymentID", "PayEmployeePayment", "" +
                "PaymentID = isnull(" + PaymentID + ",0) and isnull(Released,0) = 1"});

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            fillSalaryProcessAndReleaseTable();

          
                if (SaveDetails())
                {
                    btnShow_Click(sender, e); 
                }

          
        }

        private void fillSalaryProcessAndReleaseTable()
        {
            datEmployeeSalaryDetails.Rows.Clear();
            
            for (int i = 0; i <= datTempSalary.Rows.Count - 1; i++)
            {
                if (Convert.ToBoolean(datTempSalary.Rows[i]["SelectAll"].ToBoolean()))
                {
                    DataRow dr = datEmployeeSalaryDetails.NewRow();

                    if (datTempSalary.Columns.Contains("EmployeeID"))
                        dr["EmployeeID"] = datTempSalary.Rows[i]["EmployeeID"].ToString();

                    if (datTempSalary.Columns.Contains("CompanyID"))
                        dr["CompanyID"] = datTempSalary.Rows[i]["CompanyID"].ToString();

                    datEmployeeSalaryDetails.Rows.Add(dr);
                }
            }
        }
        private void GridSetUp()
        {
            try
            {
                FlgBlk = false;
                int I = 0;
                int IntScale = 0;

                if (dgvSalaryDetails.Columns.Count > 1)
                {
                    if (dgvSalaryDetails.Columns.Contains("SelectAll"))
                    {
                        dgvSalaryDetails.Columns["SelectAll"].Width = 30;
                        dgvSalaryDetails.Columns["SelectAll"].HeaderText = "";
                    }

                    if (dgvSalaryDetails.Columns.Contains("CompanyName"))
                        dgvSalaryDetails.Columns["CompanyName"].Width = 175;

                    if (dgvSalaryDetails.Columns.Contains("EmployeeName"))
                    {
                        dgvSalaryDetails.Columns["EmployeeName"].Width = 150;
                        dgvSalaryDetails.Columns["EmployeeName"].Frozen = true;
                    }

                    if (dgvSalaryDetails.Columns.Contains("VendorName"))
                        dgvSalaryDetails.Columns["VendorName"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("PaymentMode"))
                        dgvSalaryDetails.Columns["PaymentMode"].Visible = false;                    

                    if (dgvSalaryDetails.Columns.Contains("PeriodFrom"))
                        dgvSalaryDetails.Columns["PeriodFrom"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("PeriodTo"))
                        dgvSalaryDetails.Columns["PeriodTo"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("BankName"))
                        dgvSalaryDetails.Columns["BankName"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("TransactionType"))
                        dgvSalaryDetails.Columns["TransactionType"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("PaymentDate"))
                        dgvSalaryDetails.Columns["PaymentDate"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("BasicPay"))
                        dgvSalaryDetails.Columns["BasicPay"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("Additions"))
                        dgvSalaryDetails.Columns["Additions"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("Deductions"))
                        dgvSalaryDetails.Columns["Deductions"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("NetAmount"))
                        dgvSalaryDetails.Columns["NetAmount"].Width = 125;

                    if (dgvSalaryDetails.Columns.Contains("status"))
                        dgvSalaryDetails.Columns["status"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("VendorID"))
                        dgvSalaryDetails.Columns["VendorID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("EmpVenFlag"))
                        dgvSalaryDetails.Columns["EmpVenFlag"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("PaymentID"))
                        dgvSalaryDetails.Columns["PaymentID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("EmployeeID"))
                        dgvSalaryDetails.Columns["EmployeeID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("PaymentClassificationID"))
                        dgvSalaryDetails.Columns["PaymentClassificationID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("ProcessYear"))
                        dgvSalaryDetails.Columns["ProcessYear"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("ProcessMonth"))
                        dgvSalaryDetails.Columns["ProcessMonth"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("ProcessDate"))
                        dgvSalaryDetails.Columns["ProcessDate"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("TransactionTypeID"))
                        dgvSalaryDetails.Columns["TransactionTypeID"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("IsPartial"))
                        dgvSalaryDetails.Columns["IsPartial"].Visible = false;

                    if (dgvSalaryDetails.Columns.Contains("RecordId"))
                        dgvSalaryDetails.Columns["RecordId"].Visible = false;
                    
                    //dgvSalaryDetails.Columns[23].ReadOnly = true;

                    for (I = 0; I <= dgvSalaryDetails.ColumnCount - 1; I++)
                    {
                        dgvSalaryDetails.Columns[I].SortMode = DataGridViewColumnSortMode.NotSortable;

                        if (I != 0)
                        {
                            dgvSalaryDetails.Columns[I].ReadOnly = true;
                            dgvSalaryDetails.Columns[I].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        }
                        else
                            dgvSalaryDetails.Columns[I].ReadOnly = false;
                    }
                }


                IntScale = 2;

                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos( new string[] {"isnull(Y.Scale,2) as Scale", "" +
                    "CurrencyReference AS Y INNER JOIN CompanyMaster AS C ON C.CurrencyId = Y.CurrencyID", "C.CompanyID = " + PCompanyID + ""});

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        IntScale = datTemp.Rows[0]["Scale"].ToInt32();
                }

                if (dgvSalaryDetails.Columns.Contains("BasicPay"))
                {
                    dgvSalaryDetails.Columns["BasicPay"].DefaultCellStyle.Format = "n" + IntScale.ToString();
                    dgvSalaryDetails.Columns["BasicPay"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                if (dgvSalaryDetails.Columns.Contains("Additions"))
                {
                    dgvSalaryDetails.Columns["Additions"].DefaultCellStyle.Format = "n" + IntScale.ToString();
                    dgvSalaryDetails.Columns["Additions"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                if (dgvSalaryDetails.Columns.Contains("Deductions"))
                {
                    dgvSalaryDetails.Columns["Deductions"].DefaultCellStyle.Format = "n" + IntScale.ToString();
                    dgvSalaryDetails.Columns["Deductions"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                if (dgvSalaryDetails.Columns.Contains("NetAmount"))
                {
                    dgvSalaryDetails.Columns["NetAmount"].DefaultCellStyle.Format = "n" + IntScale.ToString();
                    dgvSalaryDetails.Columns["NetAmount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }

                if (dgvSalaryDetails.Columns.Contains("NetAmount"))
                    dgvSalaryDetails.Columns["NetAmount"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
  

                PointSelectAll(0); 
            }
            catch { }
        }

        private bool SetTempTableValue()
        {
            string ComputerName = GetHostName;
            string ProcessDate = "";
            int EmployeeID = 0;
            long PaymentID = 0;
            FlagSelect = false;

            mobjclsBLLSalaryProcessAndRelease.DeleteTempEmployeeIDForPaymentRelease(ComputerName);

            for (int II = 0; II <= dgvSalaryDetails.RowCount - 1; II++)
            {
                if (dgvSalaryDetails.Rows[II].Cells["PaymentID"].Value != null)
                {
                    if (dgvSalaryDetails.Rows[II].Cells["SelectAll"].Value.ToBoolean() == true &&
                        dgvSalaryDetails.Rows[II].Cells["Status"].Value.ToBoolean() == false)
                    {
                        PaymentID = dgvSalaryDetails.Rows[II].Cells["PaymentID"].Value.ToInt64();
                        ProcessDate = dgvSalaryDetails.Rows[II].Cells["ProcessDate"].Value.ToStringCustom();
                        EmployeeID = dgvSalaryDetails.Rows[II].Cells["EmployeeID"].Value.ToInt32();
                        FlagSelect = true;
                        mobjclsBLLSalaryProcessAndRelease.InsertTempEmployeeIDForPaymentRelease(EmployeeID, ProcessDate, ComputerName, PaymentID);
                    }
                }
            }

            if (FlagSelect)
                return true;
            else
                return false;
        }

        private bool CheckPaymentRelease()
        {
            string sPaymentIds = "";

            for (int i = 0; i<= dgvSalaryDetails.RowCount - 1; i++)
            {
                if (dgvSalaryDetails.Rows[i].Cells["PaymentID"].Value != null)
                {
                    if (dgvSalaryDetails.Rows[i].Cells["SelectAll"].Value.ToBoolean() == true &&
                        dgvSalaryDetails.Rows[i].Cells["Status"].Value.ToBoolean() == false)
                        sPaymentIds += dgvSalaryDetails.Rows[i].Cells["PaymentID"].Value.ToStringCustom() + ",";
                }
            }

            if (sPaymentIds != "")
            {
                sPaymentIds = sPaymentIds.Substring(0, sPaymentIds.Length - 1);
                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] { "PaymentID", "PayEmployeePayment", "" +
                    "Released = 1 AND PaymentID IN (" + sPaymentIds + ")"});

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return false;
        }

        private bool checkPaymentIDUse()
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.checkPaymentIDUse(GetHostName);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }

        private bool IsPartialChk(long PaymentID)
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.CheckPaymentIsPartial(PaymentID);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private void tmrSalryProcessRelease_Tick(object sender, EventArgs e)
        {
            tmrSalryProcessRelease.Enabled = false;
            lblStatus.Text = "";
        }

      
        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (MViewPermission == false)
                    return;

                if ((dgvSalaryDetails.RowCount - 1) < 0)
                    return;

                if (dgvSalaryDetails.CurrentRow.Index < 0)
                    return;

                ClsWebform objClsWebform = new ClsWebform();
                using (FrmEmailPopup objemail = new FrmEmailPopup())
                {
                    objemail.MsSubject = "Payment";
                    objemail.EmailFormType = EmailFormID.SalaryPayment;
                    objemail.MiRecordID = Convert.ToInt32(dgvSalaryDetails.Rows[dgvSalaryDetails.CurrentRow.Index].Cells["PaymentID"].Value);
                    objemail.ShowDialog();
                }
            }
            catch
            {
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (MViewPermission == false)
                return;

            FillDate = FillDate.Replace("@", "").Trim();

            if (FillDate == "01 Jan 1900")
                return;

            if (FillDate.Trim().Length < 7)
                return;

            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = "Salary Release";
            ObjViewer.sDate = FillDate.ToString().Trim();
            ObjViewer.PiFormID = (int)FormID.SalaryRelease;
            ObjViewer.PintCompany = Convert.ToInt32(cboCompany.SelectedValue);
            ObjViewer.ShowDialog();
        }

        private void btnSalarySlip_Click(object sender, EventArgs e)
        {
            //Salary Slip Employee Currency
            string sPaymentID;
            int iStype = 0; 

            if (dgvSalaryDetails.RowCount == 0)
                return;

            int iRowIndex = dgvSalaryDetails.CurrentRow.Index;
            
            if (iRowIndex > -1)
            {
                FrmPayRoll obj = new FrmPayRoll();
                obj.PstrType = "salslip";
                obj.EmpVenFlag = "1";
                sPaymentID = "";

                obj.CompanyID = GetCompanyID(Convert.ToInt32(dgvSalaryDetails.CurrentRow.Cells["PaymentID"].Value));

                dgvSalaryDetails.Columns["PeriodFrom"].Width = 125;
                dgvSalaryDetails.Columns["PeriodTo"].Width = 125;

                mdtFrom = dgvSalaryDetails.Rows[iRowIndex].Cells["PeriodFrom"].Value.ToString();
                mDtTo = dgvSalaryDetails.Rows[iRowIndex].Cells["PeriodTo"].Value.ToString();
                SalaryType(iStype);

                String sYear = "";
                sYear = sYear + DateAndTime.Year(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value));
                String sMonth = "";

                sMonth = Strings.Format(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value), "MMMM");

                obj.PiSelRowCount = dgvSalaryDetails.SelectedRows.Count;
                obj.piYear = Convert.ToInt32(sYear);
                obj.psMonth = sMonth;
                mLLOP = "";
                mHLOP = "";

                sPaymentID = "";
                foreach (DataGridViewRow drPAyId in dgvSalaryDetails.SelectedRows)
                    sPaymentID = sPaymentID + drPAyId.Cells["PaymentID"].Value + ",";

                if (sPaymentID.Trim().Length > 0)
                {
                    if (Strings.Len(sPaymentID) > 5000)
                        sPaymentID = sPaymentID.Substring(0, 5000);

                    sPaymentID = sPaymentID.Substring(0, sPaymentID.LastIndexOf(","));
                    obj.CompanyPayFlag = 0;
                    obj.pPayID = sPaymentID;
                    obj.pLOP = mHLOP;
                    obj.pLblLOP = mLLOP;
                    obj.pMonthlyMonth = iStype;
                    obj.pSType = "";
                    obj.pFormName = "Pay Slip";
                    obj.ShowDialog();
                }
                obj.Dispose();
            }
        }

        private int GetCompanyID(int intPaymentID)
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] { "CompanyID", "PayEmployeePayment", "PaymentID=" + intPaymentID + ""});

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return Convert.ToInt32(datTemp.Rows[0]["CompanyID"]);
                else
                    return 0;
            }
            else
                return 0;
        }

        private void SalaryType(int sType)
        {
            string mLOP = "";
            switch (sType)
            {
                case 1:
                    mSType = "SALARY SLIP DAILY-";
                    break;
                case 2:
                    mSType = "WEEKLY-" + mdtFrom + "-" + mDtTo;
                    mLLOP = "LOP";
                    mHLOP = Convert.ToString(mLOP);
                    break;
                case 3:
                    mSType = "FORTNIGHT-" + mdtFrom + "-" + mDtTo;
                    mLLOP = "LOP";
                    mHLOP = Convert.ToString(mLOP);
                    break;
                case 4:
                    mSType = "PAYSLIP FOR THE MONTH OF " + Strings.UCase(Strings.Format(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value), "MMMM")) + " " + DateAndTime.Year(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value));
                    mLLOP = "LOP";
                    mHLOP = Convert.ToString(mLOP);
                    break;
            }
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (dgvSalaryDetails.Rows.Count < 1)
            //        return;

            //    if (trvSalaryProcessRelease.SelectedNode.Tag == null)
            //        return;

            //    if (cboFilterCompany.SelectedIndex == -1)
            //    {
            //        MstrCommonMessage = UserMessage.GetMessageByCode(9100);
            //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }



            //    if (cboFilterTransactionType.SelectedIndex == -1)
            //    {
            //        MstrCommonMessage = UserMessage.GetMessageByCode(1831);
            //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }

            //    using (FrmBankTransfer mobj = new FrmBankTransfer())
            //    {
            //        mobj.PiCompanyID = Convert.ToInt32(cboFilterCompany.SelectedValue);
            //        mobj.PstrCompanyName = cboFilterCompany.Text.ToString();
            //        //mobj.PiCurrencyID = Convert.ToInt32(cboFilterCurrency.SelectedValue);
            //        mobj.PstrType = "bankpayment";
            //        mobj.MachineName = GetHostName;
            //        mobj.pFormName = this.Text;
            //        //mobj.PiCurrencyIDFilter = Convert.ToInt32(cboFilterCurrency.SelectedValue);
            //        mobj.PdtProcessedDate = Convert.ToString(trvSalaryProcessRelease.SelectedNode.Text.Trim());
            //        mobj.ShowDialog();
            //    }
            //}
            //catch
            //{
            //}
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (dgvSalaryDetails.Rows.Count > 0)
            {
                for (int i = 0; i <= dgvSalaryDetails.Rows.Count - 1; i++)
                {
                    if (dgvSalaryDetails.Rows[i].ReadOnly == false)
                    {
                        dgvSalaryDetails.Rows[i].Cells["SelectAll"].Value = chkSelectAll.Checked;
                        datTempSalary.Rows[i]["SelectAll"] = chkSelectAll.Checked;
                    }
                }
            }
        }
   
        private void chkAdvanceSearch_CheckedChanged(object sender, EventArgs e)
        {
            getAdvanceSearch();
        }

        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            
            //dgvSalaryDetails.DataSource = null;
            pnlMentioned.Visible = true;
          
            expPnlSalaryProcess.Visible = true;
            this.Text = "Salary Process " + this.Text.Replace("Salary Release ", "");
            if (ClsCommonSettings.IsArabicView)
            {
                btnProcess.Text = "عملية";
            }
            else
            {
                btnProcess.Text = "&Process";
            }
            btnProcess.Image = Properties.Resources.Salary_processing;
       
            pnlLeft.Width = 250;

            dgvSalaryDetails.DataSource = getNewTableForProcess();
            datTempSalary = (DataTable)dgvSalaryDetails.DataSource;
            GridColumnDisabled();
            btnBack.Visible = false;
            btnShow_Click(sender, e);
            chkSelectAll.Checked = true;
            chkSelectAll.Checked = false;
        }

        private void btnFirstGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.First.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void btnPreviousGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.Previous.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void btnNextGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.Next.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void btnLastGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.Last.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void fillDataGrid_dtgBrowse()
        {
            try
            {
                DataTable dt = datTempSalary;

                if (dt == null)
                    return;

                DeterminePageBoundaries();
                int iGrdrowcount = (dt.Rows.Count);

                if (iGrdrowcount >= 24)
                    ItemPanelMiddileBottom.Visible = true;

                if (norecord == false)
                {
                    dgvSalaryDetails.Enabled = true;
                    dt.DefaultView.RowFilter = "RecordID >= " + RecordID1 + " and RecordID <= " + RecordID2;
                    dgvSalaryDetails.DataSource = dt.DefaultView.ToTable();
                    GridColumnDisabled();
                    lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                }
              
                GridColumnColor();
            }
            catch { }
        }

        private void DeterminePageBoundaries()
        {
            try
            {
                DataTable datPages = datTempSalary;
                int TotalRowCount = datPages.Rows.Count;
                if (TotalRowCount == 0)
                {
                    //MsgBox("No records to Display.")
                    norecord = true;
                    //Exit Sub
                }
                else
                    norecord = false;
                //This is the maximum rows to display on a page. So we want paging to be implemented at 100 rows a page 

                int pages = 0;

                //if the rows per page are less than the total row count do the following: 
                if (pageRows < TotalRowCount)
                {
                    //if the Modulus returns > 0  there should be another page. 
                    if ((TotalRowCount % pageRows) > 0)
                        pages = ((TotalRowCount / pageRows) - ((TotalRowCount % pageRows) / pageRows) + 1);
                    else
                    {
                        //There is nothing left after the Modulus, so the pageRows divide exactly... 
                        //...into TotalRowCount leaving no rest, thus no extra page needs to be addd. 
                        pages = TotalRowCount / pageRows;
                    }
                }
                else
                {
                    //if the rows per page are more than the total row count, we will obviously only ever have 1 page 
                    pages = 1;
                }
                TotalPage = pages;

                //We now need to determine the LowerBoundary and UpperBoundary in order to correctly... 
                //...determine the Correct RecordID//s to use in the bindingSource1.Filter property. 
                int LowerBoundary = 0;
                int UpperBoundary = 0;

                //We now need to know what button was clicked, if any (First, Last, Next, Previous) 
                switch (NavClicked)
                {
                    case "First":
                        {
                            //First clicked, the Current Page will always be 1 
                            CurrentPage = 1;
                            //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            //if the rows per page are less than the total row count do the following: 
                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                            {
                                //if the rows per page are more than the total row count do the following: 
                                //There is only one page, so get the total row count as an UpperBoundary 
                                UpperBoundary = TotalRowCount;
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "Last":
                        {
                            //Last clicked, the CurrentPage will always be = to the variable pages 
                            CurrentPage = pages;
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));
                            //The UpperBoundary will always be the sum total of all the rows 
                            UpperBoundary = TotalRowCount;
                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "Next":
                        {
                            //Next clicked 
                            if (CurrentPage != pages)
                            {
                                //if we arent on the last page already, add another page 
                                CurrentPage += 1;
                            }

                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            if (CurrentPage == pages)
                            {
                                //if we are on the last page, the UpperBoundary will always be the sum total of all the rows 
                                UpperBoundary = TotalRowCount;
                            }
                            else
                            {
                                //else if we have a pageRow of 50 and we are on page 3, the UpperBoundary = 150 
                                UpperBoundary = (pageRows * CurrentPage);
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }

                            break;
                        }
                    case "Previous":
                        {
                            //Previous clicked 
                            if (CurrentPage != 1)
                            {
                                //if we aren//t on the first page already, subtract 1 from the CurrentPage 
                                CurrentPage -= 1;
                            }
                            //Get the LowerBoundary 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                                UpperBoundary = TotalRowCount;

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "1": //25
                    case "2": //50
                    case "3": //100
                        {
                            //First clicked, the Current Page will always be 1 
                            CurrentPage = 1;
                            //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            //if the rows per page are less than the total row count do the following: 
                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                            {
                                //if the rows per page are more than the total row count do the following: 
                                //There is only one page, so get the total row count as an UpperBoundary 
                                UpperBoundary = TotalRowCount;
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "0": // All
                        {
                            //First clicked, the Current Page will always be 1 
                            CurrentPage = 1;
                            //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                            LowerBoundary = 1;
                            UpperBoundary = TotalRowCount;

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }

                            break;
                        }
                    default:
                        {
                            //No button was clicked. 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            //if the rows per page are less than the total row count do the following: 
                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                            {
                                //if the rows per page are more than the total row count do the following: 
                                //Therefore there is only one page, so get the total row count as an UpperBoundary 
                                UpperBoundary = TotalRowCount;
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                }
            }
            catch { }
        }

        private void cboCountItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            FlgBlk = false;

            if (cboCountItem.SelectedIndex > 0)
                pageRows = Convert.ToInt32(cboCountItem.Text);
            else
                pageRows = datTempSalary.Rows.Count;

            NavClicked = cboCountItem.SelectedIndex.ToString();
            fillDataGrid_dtgBrowse();
            lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
            FlgBlk = true;
        }

      

      

        private void btnSalarySlipCompany_Click(object sender, EventArgs e)
        {

            string sPaymentID;
            int iStype = 0;

            if (dgvSalaryDetails.RowCount == 0)
                return;

            int iRowIndex = dgvSalaryDetails.CurrentRow.Index;

            if (iRowIndex > -1)
            {
                FrmPayRoll obj = new FrmPayRoll();
                obj.PstrType = "salslip";
                obj.EmpVenFlag = "1";
                sPaymentID = "";

                obj.CompanyID = GetCompanyID(Convert.ToInt32(dgvSalaryDetails.CurrentRow.Cells["PaymentID"].Value));

                dgvSalaryDetails.Columns["PeriodFrom"].Width = 125;
                dgvSalaryDetails.Columns["PeriodTo"].Width = 125;

                mdtFrom = dgvSalaryDetails.Rows[iRowIndex].Cells["PeriodFrom"].Value.ToString();
                mDtTo = dgvSalaryDetails.Rows[iRowIndex].Cells["PeriodTo"].Value.ToString();
                SalaryType(iStype);

                String sYear = "";
                sYear = sYear + DateAndTime.Year(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value));
                String sMonth = "";

                sMonth = Strings.Format(Convert.ToDateTime(dgvSalaryDetails.CurrentRow.Cells["PeriodFrom"].Value), "MMMM");

                obj.PiSelRowCount = dgvSalaryDetails.SelectedRows.Count;
                obj.piYear = Convert.ToInt32(sYear);
                obj.psMonth = sMonth;
                mLLOP = "";
                mHLOP = "";

                sPaymentID = "";
                foreach (DataGridViewRow drPAyId in dgvSalaryDetails.SelectedRows)
                    sPaymentID = sPaymentID + drPAyId.Cells["PaymentID"].Value + ",";

                if (sPaymentID.Trim().Length > 0)
                {
                    if (Strings.Len(sPaymentID) > 5000)
                        sPaymentID = sPaymentID.Substring(0, 5000);

                    sPaymentID = sPaymentID.Substring(0, sPaymentID.LastIndexOf(","));
                    obj.pPayID = sPaymentID;
                    obj.CompanyPayFlag = 1;
                    obj.pLOP = mHLOP;
                    obj.pLblLOP = mLLOP;
                    obj.pMonthlyMonth = iStype;
                    obj.pSType = "";
                    obj.pFormName = "Pay Slip";
                    obj.ShowDialog();
                }
                obj.Dispose();
            }



        }

   

       

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();


            objHelp.strFormName = "Salary Process";

            objHelp.ShowDialog();
            objHelp = null;
        }

        private void FrmSalaryProcessRelease_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                      

                            objHelp.strFormName = "Salary Process";
                      
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.P:
                        btnPrint_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.M:
                        btnEmail_Click(sender, new EventArgs());
                        break;
                }
            }
            catch (Exception)
            {
            }
        }


        

        private void dgvSalaryDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void expandableSplitter1_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            PointSelectAll(1);
        }

        private void FrmSalaryProcessRelease_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void FrmSalaryProcessRelease_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void FrmSalaryProcessRelease_Deactivate(object sender, EventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void pnlSalaryReleaseBottom_Click(object sender, EventArgs e)
        {

        }

  
        

       

    }
}