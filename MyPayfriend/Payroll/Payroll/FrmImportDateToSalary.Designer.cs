﻿namespace MyPayfriend
{
    partial class FrmImportDateToSalary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmImportDateToSalary));
            this.ProjectCreationStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.DeductionPolicyStatusStrip = new System.Windows.Forms.StatusStrip();
            this.btnSave = new System.Windows.Forms.Button();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.btnImport = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnAddDed = new System.Windows.Forms.Button();
            this.cboAdditionsDeductions = new System.Windows.Forms.ComboBox();
            this.cmsApplyToAll = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.dgvEmployeeDetails = new System.Windows.Forms.DataGridView();
            this.ColEmpNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnEmployeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnEmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnPaymentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAddDedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errAddParicularsToAll = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.txtFile = new System.Windows.Forms.TextBox();
            this.DeductionPolicyStatusStrip.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.cmsApplyToAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errAddParicularsToAll)).BeginInit();
            this.SuspendLayout();
            // 
            // ProjectCreationStatusLabel
            // 
            this.ProjectCreationStatusLabel.Name = "ProjectCreationStatusLabel";
            this.ProjectCreationStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 17);
            this.lblStatus.Text = "Status";
            // 
            // DeductionPolicyStatusStrip
            // 
            this.DeductionPolicyStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.ProjectCreationStatusLabel});
            this.DeductionPolicyStatusStrip.Location = new System.Drawing.Point(0, 474);
            this.DeductionPolicyStatusStrip.Name = "DeductionPolicyStatusStrip";
            this.DeductionPolicyStatusStrip.Size = new System.Drawing.Size(652, 22);
            this.DeductionPolicyStatusStrip.TabIndex = 1016;
            this.DeductionPolicyStatusStrip.Text = "StatusStrip1";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(10, 442);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1015;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // Panel1
            // 
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.dtpDate);
            this.Panel1.Controls.Add(this.lblFromDate);
            this.Panel1.Controls.Add(this.btnImport);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.Controls.Add(this.btnAddDed);
            this.Panel1.Controls.Add(this.cboAdditionsDeductions);
            this.Panel1.Location = new System.Drawing.Point(10, 4);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(640, 53);
            this.Panel1.TabIndex = 1012;
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(385, 18);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.ShowUpDown = true;
            this.dtpDate.Size = new System.Drawing.Size(85, 20);
            this.dtpDate.TabIndex = 161;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(329, 18);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(46, 15);
            this.lblFromDate.TabIndex = 160;
            this.lblFromDate.Text = "Payment";
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(531, 17);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(72, 23);
            this.btnImport.TabIndex = 159;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(3, 17);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(56, 13);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "Particulars";
            // 
            // btnAddDed
            // 
            this.btnAddDed.Location = new System.Drawing.Point(272, 17);
            this.btnAddDed.Name = "btnAddDed";
            this.btnAddDed.Size = new System.Drawing.Size(32, 23);
            this.btnAddDed.TabIndex = 4;
            this.btnAddDed.Text = "...";
            this.btnAddDed.UseVisualStyleBackColor = true;
            this.btnAddDed.Click += new System.EventHandler(this.btnAddDed_Click);
            // 
            // cboAdditionsDeductions
            // 
            this.cboAdditionsDeductions.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAdditionsDeductions.FormattingEnabled = true;
            this.cboAdditionsDeductions.Location = new System.Drawing.Point(67, 17);
            this.cboAdditionsDeductions.Name = "cboAdditionsDeductions";
            this.cboAdditionsDeductions.Size = new System.Drawing.Size(199, 21);
            this.cboAdditionsDeductions.TabIndex = 1;
            this.cboAdditionsDeductions.SelectedIndexChanged += new System.EventHandler(this.cboAdditionsDeductions_SelectedIndexChanged);
            // 
            // cmsApplyToAll
            // 
            this.cmsApplyToAll.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemCopy});
            this.cmsApplyToAll.Name = "CopyDetailsContextMenuStrip";
            this.cmsApplyToAll.Size = new System.Drawing.Size(68, 26);
            // 
            // ToolStripMenuItemCopy
            // 
            this.ToolStripMenuItemCopy.Name = "ToolStripMenuItemCopy";
            this.ToolStripMenuItemCopy.Size = new System.Drawing.Size(67, 22);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(575, 442);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1014;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(494, 442);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1013;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dgvEmployeeDetails
            // 
            this.dgvEmployeeDetails.AllowUserToAddRows = false;
            this.dgvEmployeeDetails.AllowUserToResizeRows = false;
            this.dgvEmployeeDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmployeeDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployeeDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColEmpNo,
            this.clmnEmployeeName,
            this.clmnAmount,
            this.clmnEmployeeID,
            this.clmnPaymentID,
            this.colAddDedID,
            this.CurrencyID});
            this.dgvEmployeeDetails.Location = new System.Drawing.Point(10, 63);
            this.dgvEmployeeDetails.Name = "dgvEmployeeDetails";
            this.dgvEmployeeDetails.Size = new System.Drawing.Size(640, 373);
            this.dgvEmployeeDetails.TabIndex = 1017;
            this.dgvEmployeeDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployeeDetails_CellValueChanged);
            this.dgvEmployeeDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvEmployeeDetails_CellBeginEdit);
            this.dgvEmployeeDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvEmployeeDetails_EditingControlShowing);
            this.dgvEmployeeDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvEmployeeDetails_CurrentCellDirtyStateChanged);
            // 
            // ColEmpNo
            // 
            this.ColEmpNo.HeaderText = "Employee No";
            this.ColEmpNo.Name = "ColEmpNo";
            // 
            // clmnEmployeeName
            // 
            this.clmnEmployeeName.HeaderText = "Employee Name";
            this.clmnEmployeeName.Name = "clmnEmployeeName";
            this.clmnEmployeeName.Width = 245;
            // 
            // clmnAmount
            // 
            this.clmnAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmnAmount.ContextMenuStrip = this.cmsApplyToAll;
            this.clmnAmount.HeaderText = "Amount";
            this.clmnAmount.MaxInputLength = 12;
            this.clmnAmount.Name = "clmnAmount";
            // 
            // clmnEmployeeID
            // 
            this.clmnEmployeeID.HeaderText = "clmnEmployeeID";
            this.clmnEmployeeID.Name = "clmnEmployeeID";
            this.clmnEmployeeID.Visible = false;
            // 
            // clmnPaymentID
            // 
            this.clmnPaymentID.HeaderText = "clmnPaymentID";
            this.clmnPaymentID.Name = "clmnPaymentID";
            this.clmnPaymentID.Visible = false;
            // 
            // colAddDedID
            // 
            this.colAddDedID.HeaderText = "AddDedID";
            this.colAddDedID.Name = "colAddDedID";
            this.colAddDedID.Visible = false;
            // 
            // CurrencyID
            // 
            this.CurrencyID.HeaderText = "CurrencyID";
            this.CurrencyID.Name = "CurrencyID";
            this.CurrencyID.Visible = false;
            // 
            // errAddParicularsToAll
            // 
            this.errAddParicularsToAll.ContainerControl = this;
            this.errAddParicularsToAll.RightToLeft = true;
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(814, 256);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(46, 20);
            this.txtFile.TabIndex = 1058;
            this.txtFile.Visible = false;
            // 
            // FrmImportDateToSalary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 496);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.dgvEmployeeDetails);
            this.Controls.Add(this.DeductionPolicyStatusStrip);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmImportDateToSalary";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import";
            this.Load += new System.EventHandler(this.FrmImportDateToSalary_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmImportDateToSalary_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmImportDateToSalary_KeyDown);
            this.DeductionPolicyStatusStrip.ResumeLayout(false);
            this.DeductionPolicyStatusStrip.PerformLayout();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.cmsApplyToAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errAddParicularsToAll)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

   
        internal System.Windows.Forms.ToolStripStatusLabel ProjectCreationStatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.StatusStrip DeductionPolicyStatusStrip;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.ContextMenuStrip cmsApplyToAll;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemCopy;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnAddDed;
        internal System.Windows.Forms.ComboBox cboAdditionsDeductions;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.DataGridView dgvEmployeeDetails;
        private System.Windows.Forms.ErrorProvider errAddParicularsToAll;
        private System.Windows.Forms.Timer tmrClear;
        internal System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.TextBox txtFile;
        internal System.Windows.Forms.DateTimePicker dtpDate;
        internal DevComponents.DotNetBar.LabelX lblFromDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEmpNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnEmployeeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnEmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnPaymentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAddDedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyID;
    }
}