﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public partial class frmGLCodeMapping : Form
    {
        clsBLLGLCodeMapping objBLLGLCodeMapping;
        string MstrMessageCaption;
        string MstrCommonMessage;
        clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.Configuration);
                return this.ObjUserMessage;
            }
        }

        public frmGLCodeMapping()
        {
            InitializeComponent();
            objBLLGLCodeMapping = new clsBLLGLCodeMapping();
            MstrMessageCaption = ClsCommonSettings.MessageCaption;

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                this.Text = "GL رسم الخرائط مدونة";
                dgvAdditionDeduction.Columns["AdditionDeductionArb"].Visible = true;
                dgvAdditionDeduction.Columns["Code"].HeaderText = "كود";
                dgvAdditionDeduction.Columns["PRCode"].HeaderText = "كود"; // want to change with Provision Account
                lblHeader.Text = tsbAdditionDeduction.ToolTipText = "خصم علاوة على ذلك";
                BindingNavigatorSaveItem.Text = "حفظ";
            }
        }

        private void frmGLCodeMapping_Load(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void FillGrid()
        {
            dgvAdditionDeduction.DataSource = objBLLGLCodeMapping.getGLCodeMapping(rbSalary.Checked);

            if (rbSalary.Checked)
            {
                dgvAdditionDeduction.Columns["AdditionDeduction"].HeaderText = "Addition / Deduction";
                dgvAdditionDeduction.Columns["AdditionDeductionArb"].HeaderText = "بالإضافة إلى ذلك / خصم";
            }
            else
            {
                dgvAdditionDeduction.Columns["AdditionDeduction"].HeaderText = "Expense Type";
                dgvAdditionDeduction.Columns["AdditionDeductionArb"].HeaderText = "نوع حساب";
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (UserMessage.ShowMessage(1)) // if Save
                SaveGLCodeMapping();
        }

        private void SaveGLCodeMapping()
        {
            bool blnSave = false;
            objBLLGLCodeMapping.PobjclsDTOGLCodeMapping.AdditionDeductionDetails = new List<clsDTOAdditionDeductionDetails>();

            for (int i = 0; i <= dgvAdditionDeduction.Rows.Count - 1; i++)
            {
                clsDTOAdditionDeductionDetails objAdditionDeductionDetails = new clsDTOAdditionDeductionDetails();
                objAdditionDeductionDetails.AdditionDeductionID = dgvAdditionDeduction.Rows[i].Cells["AdditionDeductionID"].Value.ToInt32();
                objAdditionDeductionDetails.Code = dgvAdditionDeduction.Rows[i].Cells["Code"].Value.ToStringCustom();
                objAdditionDeductionDetails.PRCode = dgvAdditionDeduction.Rows[i].Cells["PRCode"].Value.ToStringCustom();
                objBLLGLCodeMapping.PobjclsDTOGLCodeMapping.AdditionDeductionDetails.Add(objAdditionDeductionDetails);
            }

            blnSave = objBLLGLCodeMapping.SaveGLCodeMapping(rbSalary.Checked);

            if (blnSave)
            {
                MstrCommonMessage = UserMessage.GetMessageByCode(2);
                lblstatus.Text = MstrCommonMessage.Split('#').Last();
                MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                tmrGLCodeMapping.Enabled = true;
                FillGrid();
            }
        }

        private void tsbAdditionDeduction_Click(object sender, EventArgs e)
        {
            using (FrmAdditionDeduction objAdditionDeduction = new FrmAdditionDeduction())
                objAdditionDeduction.ShowDialog();

            FillGrid();
        }

        private void tsbExpenseType_Click(object sender, EventArgs e)
        {
            using (FrmCommonRef objFrmCommonRef = new FrmCommonRef("Expense Type", new int[] { 1, 0 },
                "ExpenseHeadID,Description,DescriptionArb", "ExpenseHeadReference", "IsPredefined = 0"))
                objFrmCommonRef.ShowDialog();

            FillGrid();
        }

        private void tmrGLCodeMapping_Tick(object sender, EventArgs e)
        {
            lblstatus.Text = "";
            tmrGLCodeMapping.Enabled = false;
        }

        private void dgvAdditionDeduction_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgvAdditionDeduction.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void rbSalary_CheckedChanged(object sender, EventArgs e)
        {
            FillGrid();
        }
    }
}
