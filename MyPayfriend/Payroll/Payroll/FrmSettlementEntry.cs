﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using BLL;
using DTO;
namespace MyPayfriend
{
    public partial class FrmSettlementEntry : Form //DevComponents.DotNetBar.Office2007Form
    {
        bool MbChangeStatus;
        bool MbAddStatus ;
        int MiCompanyID ;
        int miCurrencyId ;
        bool MbPrintEmailPermission =true;
        bool MbAddPermission =true;
        bool MbUpdatePermission  =true;
        bool MbDeletePermission =true;
        string MsMessageCommon = "";
        bool  bProcess = false;
        bool  bClickProcess  =false;
        bool  bLoad  =false;
        public  int Pscale;
        bool  blnConfirm  =false;
        DataTable datTempSalary;
        Double dblTempSalary ;
        public bool IsFormLoad;
        private string MstrMessageCommon; //Messagebox display
        private string MstrMessageCaption;              //Message caption
        private ArrayList MsarMessageArr; // Error Message display

        public int PiCompanyID = 0;// Reference From Work policy
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid
        private int MintUserId;

        private MessageBoxIcon MmessageIcon;            // to set the message icon
        private bool Glb24HourFormat;
        private DateTime SysDate; 
        ClsNotification mObjNotification;
        clsBLLSettlementProcess MobjclsBLLSettlementProcess;
        /* 
================================================
   Author:		<Author, Lince P Thomas>
   Create date: <Create Date,,8 Jan 2010>
   Description:	<Description,,Company Form>
 *
 *  Modified :	   <Saju>
    Modified date: <Modified Date,19 August 2013>
    Description:	<Description,Settlement Process Form>
================================================
*/
        public FrmSettlementEntry ()
        {
            MobjclsBLLSettlementProcess = new clsBLLSettlementProcess();
            InitializeComponent();
            MintCompanyId = ClsCommonSettings.CurrentCompanyID;
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            Glb24HourFormat = ClsCommonSettings.Glb24HourFormat;
            SysDate= ClsCommonSettings.GetServerDate();
            mObjNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls(); 
            }
        }
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.SettlementProcess, this);

            EmployeeSettlementBindingNavigatorSaveItem.ToolTipText = "حفظ البيانات";
            GratuityTab.Text = "بقشيش";
            DtGrdGratuity.Columns["ColYears"].HeaderText = "عام";
            DtGrdGratuity.Columns["ColCalDays"].HeaderText = "مجموع أيام";
            DtGrdGratuity.Columns["ColGRTYDays"].HeaderText = "بقشيش أيام";
            DtGrdGratuity.Columns["ColGratuityAmt"].HeaderText = "بقشيش مبلغ";
            Employee.HeaderText = "عامل";
            Date.HeaderText = "تاريخ";
        }
        #endregion SetArabicControls
        private void FrmSettlementEntry_Load(object sender, EventArgs e)
        {
            IsFormLoad = false;
            LoadMessage();
            LoadCombos(0);
            LoadInitial();
            SetPermissions();
            BindingNavigatorAddNewItem_Click(sender, e); 
            TmrEmployeeSettlement.Interval = MintTimerInterval;
            DocumentReturnCheckBox.Checked = false;
            MbChangeStatus = false;
            BtnProcess.Enabled = MbChangeStatus;
            OKSaveButton.Enabled = MbChangeStatus;
            BtnSave.Enabled = MbChangeStatus;
            EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;
            BindingNavigatorDeleteItem.Enabled = MbChangeStatus;
            IsFormLoad = true;
          //  new ClsDALArabicConverter().GetControls(pnlMain, (int)FormID.SettlementProcess);
        }

        private void LoadCombos(int intType)
        {
            DataTable datCombos = new DataTable();

            // Search 
            if (intType == 0)
            {
                cboSearchCompany.DataSource = null;

                datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "Distinct C.CompanyID,C.CompanyName", 
                    "CompanyMaster AS C INNER JOIN UserCompanyDetails AS U ON C.CompanyID = U.CompanyID", "U.UserID =" + ClsCommonSettings.UserID });
                cboSearchCompany.DisplayMember = "CompanyName";
                cboSearchCompany.ValueMember = "CompanyID";

                DataRow dr = datCombos.NewRow();
                dr["CompanyID"] = 0;
                if (ClsCommonSettings.IsArabicView)
                    dr["CompanyName"] = "أي";
                else
                    dr["CompanyName"] = "All";
                datCombos.Rows.InsertAt(dr, 0);

                cboSearchCompany.DataSource = datCombos;
            }

            if (intType == 0 || intType == 2)
            {
                cboSearchEmployee.DataSource = cboSearchCode.DataSource = null;
                cboSearchEmployee.Text = cboSearchCode.Text = string.Empty;

                datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { 
                    "DISTINCT EM.EmployeeID," + 
                    (ClsCommonSettings.IsArabicView ? "EM.EmployeeFullNameArb" : "EM.EmployeeFullName") + " AS Name,EM.EmployeeNumber", 
                    "EmployeeMaster AS EM INNER JOIN PayEmployeeSettlement AS ESH ON EM.EmployeeID = ESH.EmployeeID " +
                    "INNER JOIN UserCompanyDetails AS U ON EM.CompanyID = U.CompanyID", 
                    "EM.EmployeeID > 0 and U.UserID =" + ClsCommonSettings.UserID + " AND " +
                    "(EM.CompanyID = " + cboSearchCompany.SelectedValue.ToInt32() + 
                    " OR " + cboSearchCompany.SelectedValue.ToInt32() + " = 0) order by Name" });

                cboSearchEmployee.ValueMember = "EmployeeID";
                cboSearchEmployee.DisplayMember = "Name";
                cboSearchEmployee.DataSource = datCombos;

                cboSearchCode.ValueMember = "EmployeeID";
                cboSearchCode.DisplayMember = "EmployeeNumber";
                cboSearchCode.DataSource = datCombos;
            }
            // End

            if (intType == 0 || intType == 1)////CAlculation type
            {
                datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { 
                    "WorkstatusID," + (ClsCommonSettings.IsArabicView ? "WorkstatusArb" : "Workstatus") + " AS Workstatus", 
                    "WorkStatusReference", " workstatusID <6" });
                CboType.ValueMember = "WorkstatusID";
                CboType.DisplayMember = "Workstatus";
                CboType.DataSource = datCombos;
            }

            if (intType == 0 || intType == 1)////CAlculation type
            {
                CboEmployee.DataSource = null;

                datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { 
                    "DISTINCT EM.EmployeeID," + 
                    (ClsCommonSettings.IsArabicView ? "EM.EmployeeFullNameArb + ' - ' + EM.EmployeeNumber" : 
                    "EM.EmployeeFullName + ' - ' + EM.EmployeeNumber") + " AS Name", 
                    "EmployeeMaster AS EM INNER JOIN PaySalaryStructure AS ESH ON EM.EmployeeID = ESH.EmployeeID " +
                    "INNER JOIN UserCompanyDetails AS U ON EM.CompanyID = U.CompanyID", 
                    "EM.EmployeeID > 0 and U.UserID =" + ClsCommonSettings.UserID + " AND " +
                    "(EM.CompanyID = " + cboSearchCompany.SelectedValue.ToInt32() + 
                    " OR " + cboSearchCompany.SelectedValue.ToInt32() + " = 0) order by Name" });

                CboEmployee.ValueMember = "EmployeeID";
                CboEmployee.DisplayMember = "Name";
                CboEmployee.DataSource = datCombos;
            }

            if ( intType == 5)////CAlculation type
            {
                CboEmployee.DataSource = null;
                datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { "DISTINCT EM.EmployeeID," +
                    (ClsCommonSettings.IsArabicView ? "EM.EmployeeFullNameArb + ' - ' + EM.EmployeeNumber" : 
                    "EM.EmployeeFullName + ' - ' + EM.EmployeeNumber") + " AS Name",
                    "EmployeeMaster EM inner join PaySalaryStructure ESH on EM.EmployeeID=ESH.EmployeeID " +
                    "inner join UserCompanyDetails U ON EM.CompanyID=U.CompanyID AND U.UserID =" + ClsCommonSettings.UserID + "", 
                    "WorkStatusID>=6 AND " +
                    "(EM.CompanyID = " + cboSearchCompany.SelectedValue.ToInt32() + 
                    " OR " + cboSearchCompany.SelectedValue.ToInt32() + " = 0) order by Name" });

                CboEmployee.ValueMember = "EmployeeID";
                CboEmployee.DisplayMember = "Name";
                CboEmployee.DataSource = datCombos;
            }

            if (intType == 0 || intType == 3)//Transaction type
            {
                datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { 
                    "TransactionTypeID," + (ClsCommonSettings.IsArabicView ? "TransactionTypeArb" : "TransactionType") + " AS TransactionType",
                    "TransactionTypeReference", "" });

                cboTransactionType.ValueMember = "TransactionTypeID";
                cboTransactionType.DisplayMember = "TransactionType";
                cboTransactionType.DataSource = datCombos;
                datCombos = null;
            }

 
            if (intType == 0 || intType == 4)
            {
                    datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { 
                    "AdditionDeductionID," + (ClsCommonSettings.IsArabicView ? "AdditionDeductionArb" : "AdditionDeduction") + " AS AdditionDeduction",
                    "PayAdditionDeductionReference", "(IsPredefined=0 or AdditionDeductionID in(27,28))" });

                cboParticulars.ValueMember = "AdditionDeductionID";
                cboParticulars.DisplayMember = "AdditionDeduction";
                cboParticulars.DataSource = datCombos;
                datCombos = null;
            }

            if (intType == 0 || intType == 6)
            {
                datCombos = MobjclsBLLSettlementProcess.FillCombos(new string[] { 
                    "AdditionDeductionID," + (ClsCommonSettings.IsArabicView ? "AdditionDeductionArb" : "AdditionDeduction") + " AS AdditionDeduction",
                    "PayAdditionDeductionReference", "" });

                Particulars.ValueMember = "AdditionDeductionID";
                Particulars.DisplayMember = "AdditionDeduction";
                Particulars.DataSource = datCombos;
                datCombos = null;
            }
        }

        private void LoadInitial()
        {
            try
            {
                CboEmployee.SelectedIndex = -1;
                txtNofDaysExperience.Text = "0";
                lblNoofMonths.Text = "0";
                txtLeavePayDays.Text = "0";
                lblSettlementPolicy.Text = ClsCommonSettings.IsArabicView ? "لا شيء" : "NIL";
                lblEligibleLeavePayDays.Text = "0";
                lblTakenLeavePayDays.Text = "0";
                txtLeavePayDays.Text = "0";
                TxtTotalEarning.Text = "";
                TxtTotalDeduction.Text = "";
                NetAmountTextBox.Text = "";
                txtExcludeHolidays.Text = ClsCommonSettings.IsArabicView ? "لا شيء" : "NIL";
                txtEligibleDays.Text = "";
                chkExcludeHolidays.Checked = false;
            }
            catch { }
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (
                    Int32)eModuleID.Payroll, (Int32)eMenuID.SettlementProcess, 
                    out MbPrintEmailPermission, out MbAddPermission, out MbUpdatePermission, out MbDeletePermission);
            }
            else            
                MbAddPermission = MbPrintEmailPermission = MbUpdatePermission = MbDeletePermission = true;
        }

        private void LoadMessage()
        {
            try
            {
                MsarMessageArr = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.SettlementProcess , 2);
            }
            catch { }
        }

        private int IsValidSet(int SetTypeID, int EmpID)
        {
            try
            {
                int IntRet;

                if (ClsCommonSettings.IsHrPowerEnabled)
                {
                    if (MbAddStatus == true)
                    {
                        IntRet=MobjclsBLLSettlementProcess.IsValidSet(SetTypeID, EmpID);
                        return IntRet;
                    }
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            try 
            {
                ((Control)this.SettlementTabEmpInfo).Enabled = true;
                ((Control)this.SettlementTabParticulars).Enabled = true;
                ((Control)this.SettlementTabHistory).Enabled = true;
                ((Control)this.GratuityTab).Enabled = true;
                MbAddStatus = true;
                BindingNavigatorAddNewItem.Enabled = false;     
                LblType.Tag = "0";
                LblEmployee.Tag = "0";
                CboEmployee.Focus();
                CboEmployee.Enabled = btnShow.Enabled = true;
                DtpLastWorkingDate.Value = ClsCommonSettings.GetServerDate();
                DateDateTimePicker.Value = ClsCommonSettings.GetServerDate();
                dtpResignationDate.Value = ClsCommonSettings.GetServerDate();
                bProcess = false;
                CboType.Enabled = true;
                DtpLastWorkingDate.Enabled = true;
                dtpResignationDate.Enabled = true;
                DocumentReturnCheckBox.Checked = false;
                txtLeavePayDays.Enabled = true;
                Clear();
                CboEmployee.SelectedIndex = -1;
                txtNofDaysExperience.Text = "0";
                lblNoofMonths.Text = "0";
                txtLeavePayDays.Text = "0";
                lblSettlementPolicy.Text = ClsCommonSettings.IsArabicView ? "لا شيء" : "NIL";
                lblEligibleLeavePayDays.Text = "0";
                lblTakenLeavePayDays.Text = "0";
                txtLeavePayDays.Text = "0";
                BtnCancel.Enabled = true;
                MbChangeStatus = false;
                BtnProcess.Enabled = MbChangeStatus;
                OKSaveButton.Enabled = MbChangeStatus;
                BindingNavigatorDeleteItem.Enabled = MbChangeStatus;
                BtnSave.Enabled = MbChangeStatus;
                EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;
                BtnPrint.Enabled = MbChangeStatus;
                btnEmail.Enabled = MbChangeStatus;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 1;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed = false;
                EmployeeSettlementDetailDataGridView.RowCount = 1;
                SettlementTab.SelectedTab = SettlementTabEmpInfo;
                chkAbsent.Checked = false;
                IsFormLoad = false;
                LoadCombos(5);
                IsFormLoad = true;
                Clear();           
            } 
            catch { }
        }

        private void Clear()
        {
            try
            {
                bClickProcess = false;
                bProcess = false;
                LblEmployeeSettlement.Text = "";
                LblEmployee.Tag = "0";
                LblType.Tag = "0";
                cboParticulars.SelectedIndex = -1;
                txtAmount.Text = "";
                txtNoticePeriod.Text = "0";
                CboEmployee.SelectedIndex = -1;
                CboType.SelectedIndex = -1;
                lblPayBack.Text = "";
                LblCurrency.Text = "";
                TxtTotalEarning.Text = "";
                TxtTotalDeduction.Text = "";
                NetAmountTextBox.Text = "";
                ErrEmployeeSettlement.Clear();
                EmployeeSettlementDetailDataGridView.Rows.Clear();
                DtGrdGratuity.Rows.Clear();    
                RemarksTextBox1.Text = "";
                cboTransactionType.SelectedIndex =-1;
                txtChequeNo.Text = "";
                LblType.Tag =0;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed = false;
                CboEmployee.Enabled = btnShow.Enabled = true;
                lblSettlementPolicy.Text = "";
                lblNoofMonths.Text = "";
                txtNofDaysExperience.Text = "";
                txtLeavePayDays.Text = "";
                lblEligibleLeavePayDays.Text = "";
                lblTakenLeavePayDays.Text = "";
                lblEligibleLeavePayDays.Text = "";
                txtAbsentDays.Text = "";
                txtExcludeHolidays.Text = "";
                txtEligibleDays.Text = "";
                chkExcludeHolidays.Checked = false;
                DtGrdEmpHistory.Rows.Clear();
                FillExcludGrid();
            }
            catch { }
       }

       private void ClearPartially()
       {
            try
            {
                cboParticulars.Text = "";
                txtAmount.Text = "";
                TxtTotalEarning.Text = "";
                TxtTotalDeduction.Text = "";
                NetAmountTextBox.Text = "";
                ErrEmployeeSettlement.Clear();
                EmployeeSettlementDetailDataGridView.Rows.Clear();
                EmployeeSettlementDetailDataGridView.RowCount = 1;
            }
            catch { }
        }

        private void EmployeeSettlementBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try 
            {
                if (MbChangeStatus == true )
                {
                    if( FormValidation() == true )
                    {
                        blnConfirm = false;
                        Fillparameters(1); 

                        if (MobjclsBLLSettlementProcess.SaveEmployeeSettlement() == true)
                        {
                            LblType.Tag = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID;
                            DisplayInfo(1);
                            EnableDisablePartially();
                            LblEmployeeSettlement.Text = "";
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LblEmployeeSettlement.Text = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon).ToString().Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                               
                            MbAddStatus = false;
                            BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                            BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                            BtnPrint.Enabled = btnEmail.Enabled = MbPrintEmailPermission;
                        }
                    }
                }
            }
            catch { }
        }

        private void EnableDisablePartially()
        {
            dtpResignationDate.Enabled = DtpLastWorkingDate.Enabled = CboType.Enabled = CboEmployee.Enabled = btnShow.Enabled = txtLeavePayDays.Enabled = false;
        }

        private bool FormValidation()
        {                
            ErrEmployeeSettlement.Clear();

            if (MbAddStatus == false)
            {
                if (Convert.ToInt32(LblEmployee.Tag) > 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 671, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrEmployeeSettlement.Enabled = true;
                    return false;
                }
            }  

            if (String.IsNullOrEmpty(CboEmployee.Text) == true )
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9124, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ErrEmployeeSettlement.SetError(CboEmployee, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                TmrEmployeeSettlement.Enabled = true;
                CboEmployee.Focus();
                return false;
            }

            if (CboEmployee.SelectedIndex == -1 )
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 16, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ErrEmployeeSettlement.SetError(CboEmployee, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                TmrEmployeeSettlement.Enabled = true;
                CboEmployee.Focus();
                return false;
            }
            else
                ErrEmployeeSettlement.SetError(CboEmployee, "");

            if (CboType.SelectedIndex == -1)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 656, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ErrEmployeeSettlement.SetError(CboType, MstrMessageCommon);
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                CboEmployee.Focus();
                TmrEmployeeSettlement.Enabled = true;
                return false;
            }
            else
                ErrEmployeeSettlement.SetError(CboType, "");

            if (cboTransactionType.SelectedValue.ToInt32() <= 0)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 681, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ErrEmployeeSettlement.SetError(cboTransactionType, MstrMessageCommon);
                LblEmployeeSettlement.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                TmrEmployeeSettlement.Enabled = true;
                cboTransactionType.Focus();
                return false;
            }

            if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Cheque)
            {
                if (txtChequeNo.Text.ToStringCustom() == string.Empty)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3088, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LblEmployeeSettlement.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    TmrEmployeeSettlement.Enabled = true;
                    return false;
                }
            }

            MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Date = DateDateTimePicker.Value.Date.ToString("dd-MMM-yyy");

            if (DtpLastWorkingDate.Value.Date < DtpFromDate.Value.Date)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 19, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmrEmployeeSettlement.Enabled = true;
                DtpFromDate.Focus();
                return false;
            }
            if (DtpLastWorkingDate.Value.Date < dtpResignationDate.Value.Date)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 19, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmrEmployeeSettlement.Enabled = true;
                dtpResignationDate.Focus();
                return false;
            }
            if (MobjclsBLLSettlementProcess.IsDateOfJoinGreater()==1 )
            {
                if (DtpLastWorkingDate.Value.Date < Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.dateofjoining))
                {
                   MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ErrEmployeeSettlement.SetError(DtpLastWorkingDate, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    TmrEmployeeSettlement.Enabled = true;
                    DtpLastWorkingDate.Focus();
                    return false;
                }
            }

            if (MbAddStatus)
            {
                if (MobjclsBLLSettlementProcess.IsEmployeeSettled() == 1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 651, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }

            if (bClickProcess == false && MbAddStatus == true)
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 684, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                TmrEmployeeSettlement.Enabled = true;
                BtnProcess.Focus();
                return false;
            }

            TxtTotalEarning.Text = (TotalEarningSettlementAmt()).ToString();
            TxtTotalDeduction.Text = (TotalDedSettlementAmt()).ToString();                
            NetAmountTextBox.Text =Convert.ToString(TotalEarningSettlementAmt() - TotalDedSettlementAmt()); 
                
            if ((TotalEarningSettlementAmt() - TotalDedSettlementAmt()) < 0 )
            {
                 MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 657, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblPayBack.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim() + " " + NetAmountTextBox.Text + "";
            }
            else
                lblPayBack.Text = "";

            return true;
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(LblType.Tag) != 0 && Convert.ToInt32(LblEmployee.Tag) == 0 )
            {
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 683, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (MobjclsBLLSettlementProcess.DeleteSettlementDetail() == true)
                    {
                        BindingNavigatorAddNewItem_Click(sender, e);
                        btnSearch_Click(sender, e);
                    }
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            BindingNavigatorAddNewItem_Click(sender, e);
        }

        private void CboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void DtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void CboType_KeyPress(object sender, KeyPressEventArgs e)
        {
            CboType.DroppedDown = false;
            lblToDate.Text = ClsCommonSettings.IsArabicView ? "إلى تاريخ" : "To Date";
        }

        private void CboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrEmployeeSettlement.Clear();
            MbChangeStatus = true;

            if (MbAddStatus)
                BtnProcess.Enabled = MbAddPermission;
            else
            {
                ChangeStatus();
                BtnProcess.Enabled = false;
            }

            lblToDate.Text = CboType.Text + (ClsCommonSettings.IsArabicView ? " تاريخ" : " Date");
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed == true)
                    return;

                if (MbUpdatePermission == false)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 17, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                Application.DoEvents();
                bProcess = true;

                if (lblNoofMonths.Text.Trim() == "")
                {
                    return; 
                }



                if (CboEmployee.SelectedIndex != -1)
                {
                    if (MbAddStatus == false)
                    {
                        if (Convert.ToInt32(LblEmployee.Tag) > 0)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 668, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TmrEmployeeSettlement.Enabled = true;
                            return;
                        }
                    }

                    if (cboTransactionType.SelectedValue.ToInt32() <= 0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 681, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LblEmployeeSettlement.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                        TmrEmployeeSettlement.Enabled = true;
                        cboTransactionType.Focus();
                        return;
                    }

                    //if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Bank)
                    //{
                    //    if (txtChequeNo.Text.ToStringCustom() == string.Empty)
                    //    {
                    //        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3088, out MmessageIcon);
                    //        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //        LblEmployeeSettlement.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                    //        TmrEmployeeSettlement.Enabled = true;
                    //        txtChequeNo.Focus();
                    //        return;
                    //    }
                    //}

                    BtnSave.Enabled = OKSaveButton.Enabled = EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbUpdatePermission;
                    //calculateEligibleDays();
                    Fillparameters(0);

                    if (MobjclsBLLSettlementProcess.IsTransferDateGreater() == 1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 672, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ErrEmployeeSettlement.SetError(DtpLastWorkingDate, MsMessageCommon.Replace("#", "").Trim());
                        LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                        TmrEmployeeSettlement.Enabled = true;
                        DtpLastWorkingDate.Focus();
                        return;
                    }

                    if (MobjclsBLLSettlementProcess.IsDateOfJoinGreater() == 1)
                    {
                        if (DtpLastWorkingDate.Value.Date < Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.dateofjoining))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ErrEmployeeSettlement.SetError(DtpLastWorkingDate, MsMessageCommon.Replace("#", "").Trim());
                            LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TmrEmployeeSettlement.Enabled = true;
                            DtpLastWorkingDate.Focus();
                            return;
                        }

                        if (DtpLastWorkingDate.Value.Date < Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.dateofjoining))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ErrEmployeeSettlement.SetError(DtpLastWorkingDate, MsMessageCommon.Replace("#", "").Trim());
                            LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                            TmrEmployeeSettlement.Enabled = true;
                            DtpLastWorkingDate.Focus();
                            return;
                        }
                    }

                    if (MobjclsBLLSettlementProcess.IsEmployeeSettled() == 1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 651, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (CboType.SelectedIndex == -1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 656, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ErrEmployeeSettlement.SetError(CboType, MstrMessageCommon);
                        LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        CboEmployee.Focus();
                        TmrEmployeeSettlement.Enabled = true;
                        return;
                    }
                    else
                        ErrEmployeeSettlement.SetError(CboType, "");

                    if (CboType.SelectedValue.ToInt32() == 3)
                    {
                        if (dtpChequeDate.Value < System.DateTime.Now.Date)// Validation for previous cheque date.
                        {
                            int intDiffDays;
                            intDiffDays = Convert.ToInt32(System.DateTime.Now.Date.DayOfYear - dtpChequeDate.Value.Date.DayOfYear);
                            int intYearDiff = System.DateTime.Now.Year - dtpChequeDate.Value.Year;

                            if (System.DateTime.Now.Year != dtpChequeDate.Value.Year)
                                if (System.DateTime.Now.Year > dtpChequeDate.Value.Year)
                                    intDiffDays = 365 * intYearDiff;

                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3161, out MmessageIcon);
                            MstrMessageCommon = MstrMessageCommon.Replace("*", Convert.ToString(intDiffDays));
                            TmrEmployeeSettlement.Enabled = true;

                            if (MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                                return;
                        }
                    }
                    //Removed as per Regal Laxman Request
                    //if (ClsCommonSettings.IsHrPowerEnabled)
                    //{
                    //    if (MbAddStatus == true)
                    //    {
                    //        int RetVal = IsValidSet(CboType.SelectedValue.ToInt32(), CboEmployee.SelectedValue.ToInt32());
                    //        if (RetVal == 0)
                    //        {
                    //            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 20013, out MmessageIcon);
                    //            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //            LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                    //            TmrEmployeeSettlement.Enabled = true;
                    //            return;
                    //        }
                    //    }
                    //}

                    if (DtpLastWorkingDate.Value.Date < DtpFromDate.Value.Date)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 19, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmrEmployeeSettlement.Enabled = true;
                        DtpFromDate.Focus();
                        return;
                    }

                    if (MobjclsBLLSettlementProcess.IsPaymentReleaseCheckingPrevious() == 1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 674, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        BtnProcess.Focus();
                        return;
                    }

                    if (MobjclsBLLSettlementProcess.IsPaymentReleaseChecking() == 1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 678, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DtpLastWorkingDate.Focus();
                        return;
                    }

                    if (MobjclsBLLSettlementProcess.IsAttendanceExisting() == 1)
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 685, out MmessageIcon);

                    Fillparameters(0);
                    //MobjclsBLLSettlementProcess.ProcessSettlementBegin();
                    FillProcessDetails();
                    BtnCancel.Enabled = false;

                    if (EmployeeSettlementDetailDataGridView.RowCount >= 0)
                        EmployeeSettlementDetailDataGridView.ClearSelection();

                    if ((TotalEarningSettlementAmt() - TotalDedSettlementAmt()) < 0)
                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 657, out MmessageIcon);
                    else
                        lblPayBack.Text = "";
                }

                bProcess = true;
                bClickProcess = true;
                EmployeeSettlementBindingNavigatorSaveItem_Click(sender, e);
                EmployeeSettlementBindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnProcess.Enabled = false;
                //MobjclsBLLSettlementProcess.ProcessSettlementCommit();
            }
            catch
            {
                //MobjclsBLLSettlementProcess.ProcessSettlementRollBack();
            
            }
        }

        private void DateSetts()
        {
            try
            {
                //if (SysDate < DtpLastWorkingDate.Value.Date)
                //{
                    MobjclsBLLSettlementProcess.EmployeeSetAttTransaction(CboEmployee.SelectedValue.ToInt32(), DtpLastWorkingDate.Value.ToString("dd-MMM-yyyy"));
                //}
            }
            catch { }
        }

        private void  FillProcessDetails()
        {
            try
            {
                //MobjclsBLLSettlementProcess.UpdateEmpTransAction();
                DateSetts();
                datTempSalary = MobjclsBLLSettlementProcess.SettlementProcess();
                EmployeeSettlementDetailDataGridView.Rows.Clear();
                DtGrdGratuity.Rows.Clear(); 

                if (datTempSalary != null)
                {
                    if (datTempSalary.Rows.Count > 0)
                    {
                        for (int i = 0; datTempSalary.Rows.Count - 1 >= i; ++i)
                        {
                            EmployeeSettlementDetailDataGridView.Rows.Add();
                            EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value = datTempSalary.Rows[i]["AdditionDeductionID"].ToInt32(); // "Salary (" + Convert.ToDateTime(datTempSalary.Rows[0]["PeriodFrom"].ToString()).ToString("dd MMM yyyy") + " - " + Convert.ToDateTime(datTempSalary.Rows[0]["PeriodTo"].ToString()).ToString("dd MMM yyyy") + ")";

                            if (datTempSalary.Rows[i]["IsAddition"].ToBoolean()==true)
                            {
                                EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value = 1;
                                EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value = datTempSalary.Rows[i]["Amount"].ToDecimal(); 
                                EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value = 0;
                            }
                            else
                            {
                                EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value = 0;
                                EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value = 0;
                                EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value = Math.Abs(datTempSalary.Rows[i]["Amount"].ToDecimal()); 
                            }

                            EmployeeSettlementDetailDataGridView.Rows[i].Cells["LoanID"].Value = 0;
                            EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].ReadOnly = true;
                            EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].ReadOnly = true;
                            EmployeeSettlementDetailDataGridView.Rows[i].Cells["Flag"].Value = 1;
                            EmployeeSettlementDetailDataGridView.Rows[i].Cells["SalaryPaymentID"].Value = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.PaymentID;
                        }
                    }
                }
    
                DataSet DTSet;
                DTSet = MobjclsBLLSettlementProcess.SettlementProcessDetail();

                if (DTSet.Tables[0].Rows.Count > 0)
                {
                    int iRow;

                    for (int i = 0; DTSet.Tables[0].Rows.Count - 1 >= i; ++i)
                    {
                        EmployeeSettlementDetailDataGridView.RowCount = EmployeeSettlementDetailDataGridView.RowCount + 1;
                        iRow = EmployeeSettlementDetailDataGridView.RowCount -1;

                        if (iRow <= 0)
                            iRow = 0;

                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Particulars"].Value = DTSet.Tables[0].Rows[i]["Particulars"].ToInt32();
                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["IsAddition"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["IsAddition"]);
                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["Credit"]);
                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["LoanID"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["LoanID"]);

                        if (DTSet.Tables[0].Rows[i]["IsAddition"].ToInt32() == 1)
                        {
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["Credit"]);
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Debit"].Value = 0;
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].ReadOnly = false;
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Debit"].ReadOnly = true;
                        }
                        else
                        {
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Debit"].Value = Convert.ToDouble(DTSet.Tables[0].Rows[i]["Debit"]);
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].Value = 0;
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Credit"].ReadOnly = true;
                            EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Debit"].ReadOnly = false;
                        }

                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["Flag"].Value = 1;
                        EmployeeSettlementDetailDataGridView.Rows[iRow].Cells["SalaryPaymentID"].Value = "0";
                    }
                }

                if (DTSet.Tables[1].Rows.Count > 0)
                {
                    int iRow;

                    for (int i = 0; DTSet.Tables[1].Rows.Count - 1 >= i; ++i)
                    {
                        DtGrdGratuity.RowCount = DtGrdGratuity.RowCount + 1;
                        iRow = DtGrdGratuity.RowCount - 1;

                        if (iRow <= 0)
                            iRow = 0;

                        DtGrdGratuity.Rows[iRow].Cells["ColYears"].Value = Convert.ToString(DTSet.Tables[1].Rows[i]["Years"]);
                        DtGrdGratuity.Rows[iRow].Cells["ColCalDays"].Value = Convert.ToDouble(DTSet.Tables[1].Rows[i]["CalculationDays"]);
                        DtGrdGratuity.Rows[iRow].Cells["ColGRTYDays"].Value = Convert.ToDouble(DTSet.Tables[1].Rows[i]["GratuityDays"]);
                        DtGrdGratuity.Rows[iRow].Cells["ColGratuityAmt"].Value = Convert.ToDouble(DTSet.Tables[1].Rows[i]["GratuityAmount"]);
                        DtGrdGratuity.Rows[iRow].Cells["PerDayGratuity"].Value = Convert.ToDouble(DTSet.Tables[1].Rows[i]["PerDayGratuity"]);
                    }
                }
            }
            catch
            { }
        }

        private void DtpToDate_ValueChanged(object sender, EventArgs e)
        {
            //ErrEmployeeSettlement.Clear();
            ChangeStatus();
            //ClearPartially();
            //GetSettlementPolicyAndExperience();

            //if (CboEmployee.SelectedIndex >= 0 && CboType.SelectedIndex >= 0 && MbAddStatus == true)
            //    BtnProcess.Enabled = MbAddPermission;

            //CalculateLeavePayDays();
            //DateDiff();
        }

        private void ChangeStatus()
        {
            LblEmployeeSettlement.Text = "";

            if (MbAddStatus == true)
                MbChangeStatus = MbAddPermission; 
            else
                MbChangeStatus = MbUpdatePermission;

            try
            {
                if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed == false)
                {
                    OKSaveButton.Enabled = MbUpdatePermission;
                    BtnSave.Enabled = MbChangeStatus;
                    EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;
                }
            }
            catch { }
        }
  
        private void DateDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            if (bLoad == false && bProcess == false )
                lblPayBack.Text = "";

            ErrEmployeeSettlement.Clear();
            ChangeStatus();
            MbChangeStatus = true;
        }

        private void txtAbsentDays_TextChanged(object sender, EventArgs e)
        {
            if (txtAbsentDays.Text == "" )
                txtAbsentDays.Text = "0";
        }

        private void txtLeavePayDays_TextChanged(object sender, EventArgs e)
        {
            if (txtLeavePayDays.Text == "" )
                txtLeavePayDays.Text = "0";
        }

        private bool ColDuplicateChk(string ParaString) 
        {
            try
            {
                for (int i = 0; EmployeeSettlementDetailDataGridView.RowCount > i;i++)
                {
                    if (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value != null)
                    {
                        if (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value.ToString() != "")
                        {
                            if (Convert.ToString(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value).Trim().ToUpper()   == ParaString.Trim().ToUpper()  )
                                return true;
                        }
                    }
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            try 
            {
                if ( txtAmount.Text.Trim() == "" || txtAmount.Text.Trim() == ".")
                    return;

                if (cboParticulars.SelectedIndex == -1 || Convert.ToDouble(txtAmount.Text) == 0)
                    return; 

                if (ColDuplicateChk(Convert.ToString(cboParticulars.Text)) == true)
                    return;
            
                ChangeStatus();
                int i = 0;
                i = EmployeeSettlementDetailDataGridView.RowCount;                
                EmployeeSettlementDetailDataGridView.RowCount = EmployeeSettlementDetailDataGridView.RowCount + 1;
                EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value = cboParticulars.SelectedValue.ToInt32();
                if (MobjclsBLLSettlementProcess.IsAddition(cboParticulars.SelectedValue.ToInt32()) == 1)
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value = 1;
                else
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value = 0;

                if (Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value) == 1)
                {
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value= Convert.ToDouble(txtAmount.Text);
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value= 0;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].ReadOnly = false;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].ReadOnly = true;
                }
                else
                {
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].ReadOnly = true;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].ReadOnly = false;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value= Convert.ToDouble(txtAmount.Text);
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value= 0;
                }

                EmployeeSettlementDetailDataGridView.Rows[i].Cells["Flag"].Value= 1;
                TxtTotalEarning.Text = (TotalEarningSettlementAmt()).ToString();
                TxtTotalDeduction.Text = (TotalDedSettlementAmt()).ToString();
                NetAmountTextBox.Text = (TotalEarningSettlementAmt() - TotalDedSettlementAmt()).ToString();          
            }
            catch { }
        }

        private void DocumentReturnCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            MbChangeStatus = true;

            if (MbAddStatus)
                BtnSave.Enabled = EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbAddPermission;
            else
                OKSaveButton.Enabled = BtnSave.Enabled = EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbUpdatePermission;
        }

        private void OKSaveButton_Click(object sender, EventArgs e)
        {
            try 
            {
                if (ClsMainSettings.blnProductStatus == false)
                {
                    return; 
                }


                if (MbUpdatePermission == false)
                    return;

                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 0;
                if (FormValidation() == true)
                {
                    blnConfirm = true;
                    MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 680, out MmessageIcon);

                    if (MessageBox.Show(MsMessageCommon, MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;

                    blnConfirm = true;
                    Fillparameters(1);

                    if (MobjclsBLLSettlementProcess.SaveEmployeeSettlement() == true)
                    {
                        if (ClsCommonSettings.ThirdPartyIntegration)
                            new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy").ToDateTime());

                        MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TmrEmployeeSettlement.Enabled = true;
                        MbChangeStatus = false;
                        DisplayInfo(0);

                        OKSaveButton.Enabled = BtnSave.Enabled = EmployeeSettlementBindingNavigatorSaveItem.Enabled = false;
                        BindingNavigatorDeleteItem.Enabled = false;
                        BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                        BtnCancel.Enabled = false;
                    }

                    EnableDisablePartially();
                }
            }
            catch { }
        }

        private double  TotalEarningSettlementAmt()
        {
            try
            {
                double dEmployeeSettlementAmt = 0;
                int i;

                if (EmployeeSettlementDetailDataGridView.RowCount > 0)
                {
                    for( i = 0;EmployeeSettlementDetailDataGridView.Rows.Count - 1>=i;++i)
                    {
                        dEmployeeSettlementAmt = dEmployeeSettlementAmt + 
                            (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value==null ? 0 : 
                            Convert.ToDouble(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value));
                    }
                }

                return dEmployeeSettlementAmt;
            }
            catch
            {
                return 0;
            }
        }

        private double TotalDedSettlementAmt()
        {
            try
            {
                double dblEmployeeDedSettlementAmt = 0;

                if (EmployeeSettlementDetailDataGridView.RowCount > 0)
                {
                    int i;

                    for (i = 0;EmployeeSettlementDetailDataGridView.Rows.Count - 1>=i;++i)
                    {
                        dblEmployeeDedSettlementAmt = dblEmployeeDedSettlementAmt +
                            ((EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value) == null ? 0 : 
                            Convert.ToDouble(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value));
                    }
                }

                return dblEmployeeDedSettlementAmt;
            }
            catch 
            {
                return 0;
            }
        }

        private string fillremarks() 
        {
            try
            {
                string  SetMessage;
                if (ClsCommonSettings.IsArabicView)
                    SetMessage = "عامل تسوية";
                else
                    SetMessage = "Employee Settlement ";
                NetAmountTextBox.Text = NetAmountTextBox.Text.Replace("(", "").Replace(")", "");

                if (RemarksTextBox1.Text.Trim() == "")
                {
                    if (Convert.ToDouble((NetAmountTextBox.Text.Trim() == "" ? "0" : NetAmountTextBox.Text)) > 0)
                    {
                        if (Convert.ToDouble((TxtTotalEarning.Text.Trim() == "" ? "0" : TxtTotalEarning.Text)) - Convert.ToDouble((TxtTotalDeduction.Text == "" ? "0" : TxtTotalDeduction.Text.Trim())) > 0)
                            return SetMessage;
                        else
                            return CboEmployee.Text.Trim();
                    }
                }
                else
                {
                    return RemarksTextBox1.Text.Trim();
                }

                return RemarksTextBox1.Text.Trim();
            }
            catch
            {
                return RemarksTextBox1.Text.Trim();
            }
        }

        private void GetSettlementPolicyAndExperience()
        {
            string SetPolicyDescStr = ""; string lblNoofMonthsStr = ""; string txtNofDaysExperienceStr = ""; string txtLeavePayDaysStr = "";
            string lblTakenLeavePayDaysStr = ""; string lblEligibleLeavePayDaysStr = ""; string txtAbsentDaysStr = "";

            try
            {
                if (CboEmployee.SelectedIndex > -1)
                {
                    MobjclsBLLSettlementProcess.GetSettlementPolicyAndExperience(Convert.ToInt32(CboEmployee.SelectedValue),
                        DtpFromDate.Value.ToString("dd-MMM-yyyy"), DtpLastWorkingDate.Value.ToString("dd-MMM-yyyy"), ref SetPolicyDescStr, ref lblNoofMonthsStr,
                        ref txtNofDaysExperienceStr, ref txtLeavePayDaysStr, ref lblTakenLeavePayDaysStr, ref lblEligibleLeavePayDaysStr, ref txtAbsentDaysStr);
                    gmajilgmai.Value = gmajilgmai.Value + 10;
                    lblSettlementPolicy.Text = SetPolicyDescStr;
                    lblNoofMonths.Text = lblNoofMonthsStr;
                    txtNofDaysExperience.Text = Math.Abs(txtNofDaysExperienceStr.ToDecimal()).ToString();
                    txtLeavePayDays.Text = txtLeavePayDaysStr;
                    lblEligibleLeavePayDays.Text = lblEligibleLeavePayDaysStr;
                    lblTakenLeavePayDays.Text = lblTakenLeavePayDaysStr;
                    lblEligibleLeavePayDays.Text = lblEligibleLeavePayDaysStr;
                    txtAbsentDays.Text = txtAbsentDaysStr;
                    gmajilgmai.Value = 50;
                   GetEmployeeLeaveHolidays();
                   gmajilgmai.Value = 60;
                }
            }
            catch { }
        }

        private void EmployeeHistoryDetail()
        {
            try
            {
                DataTable DT;
                if (CboEmployee.SelectedIndex != -1)
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID = Convert.ToInt32(CboEmployee.SelectedValue);

                DT = MobjclsBLLSettlementProcess.EmployeeHistoryDetail();

                if (DT.Rows.Count > 0)
                {
                    DtGrdEmpHistory.Rows.Clear();
                    DtGrdEmpHistory.RowCount = 0;

                    for (int i = 0; DT.Rows.Count > i; ++i)
                    {
                        DtGrdEmpHistory.RowCount = DtGrdEmpHistory.RowCount + 1;
                        DtGrdEmpHistory.Rows[i].Cells[0].Value = DT.Rows[i]["Descriptio"];
                        DtGrdEmpHistory.Rows[i].Cells[1].Value = DT.Rows[i]["PeriodFrom"];

                        if (DT.Rows[i]["PeriodTo"] != System.DBNull.Value)
                            DtGrdEmpHistory.Rows[i].Cells[2].Value = DT.Rows[i]["PeriodTo"];

                        DtGrdEmpHistory.Rows[i].Cells[3].Value = DT.Rows[i]["NetAmount"];
                    }
                }
            }
            catch { }
        }

        private void DisplayInfo(int flg)
        {
            if (flg == 0)
            {
                IsFormLoad = false;
                LoadCombos(1);
                MobjclsBLLSettlementProcess.DisplayInfo();

                if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID > 0)
                {
                    CboEmployee.Focus();
                    LblEmployee.Tag = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed;
                    LblType.Tag = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID;
                    CboEmployee.SelectedValue = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID;
                    CboType.SelectedValue = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementType;
                    DtpFromDate.Value = Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.FromDate);
                    DtpLastWorkingDate.Value = Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ToDate);
                    DateDateTimePicker.Value =Convert.ToDateTime( MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Date);
                    DocumentReturnCheckBox.Checked = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.DocumentReturn;
                    RemarksTextBox1.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Remarks;
                    cboTransactionType.SelectedValue = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.TransactionTypeID;
                    txtChequeNo.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ChequeNumber;
                    dtpChequeDate.Value = Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ChequeDate);
                    txtNofDaysExperience.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ExperienceDays.ToString();
                    txtNoticePeriod.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.decNoticePeriod.ToString();
                    dtpResignationDate.Value = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.strResignationDate.ToDateTime();
                    string SetPolicyDescStr = ""; string lblNoofMonthsStr = ""; string txtNofDaysExperienceStr = ""; string txtLeavePayDaysStr = "";
                    string lblTakenLeavePayDaysStr = ""; string lblEligibleLeavePayDaysStr = ""; string txtAbsentDaysStr = "";

                    MobjclsBLLSettlementProcess.GetSettlementPolicyAndExperience(Convert.ToInt32(CboEmployee.SelectedValue),
                        DtpFromDate.Value.ToString("dd-MMM-yyyy"), DtpLastWorkingDate.Value.ToString("dd-MMM-yyyy"), ref SetPolicyDescStr, ref lblNoofMonthsStr,
                        ref txtNofDaysExperienceStr, ref txtLeavePayDaysStr, ref lblTakenLeavePayDaysStr, ref lblEligibleLeavePayDaysStr, ref txtAbsentDaysStr);

                    lblNoofMonths.Text = lblNoofMonthsStr;                    
                    txtLeavePayDays.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.LeavePayDays.ToString(); ;
                    chkAbsent.Checked = (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ConsiderAbsentDays.ToBoolean() == false? false : true);
                    txtAbsentDays.Text = txtAbsentDaysStr;
                    CalculateLeavePayDays();
                    txtLeavePayDays.Text = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.LeavePayDays.ToString();
                    miCurrencyId = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.CurrencyID; 
                }
            }

            if (flg == 1)
                MobjclsBLLSettlementProcess.FillDetails(Convert.ToInt32(LblType.Tag), Convert.ToInt32(CboEmployee.SelectedValue));

            EmployeeSettlementDetailDataGridView.Rows.Clear();
            int i=0;

            if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOSettlementDetails != null)
            {
                foreach (clsDTOSettlementDetails objclsDTOSettlementDetails in MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOSettlementDetails)
                {
                    EmployeeSettlementDetailDataGridView.RowCount = EmployeeSettlementDetailDataGridView.RowCount + 1;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["EmployeeSettlementDetailID"].Value = objclsDTOSettlementDetails.EmployeeSettlementDetailID;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value = objclsDTOSettlementDetails.ParticularsID;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value = objclsDTOSettlementDetails.IsAddition;

                    if (objclsDTOSettlementDetails.IsAddition == true)
                    {
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value = objclsDTOSettlementDetails.Amount;
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value = 0;
                    }
                    else
                    {
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value = 0;
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value = objclsDTOSettlementDetails.Amount;
                    }

                    if (objclsDTOSettlementDetails.IsAddition == true)
                    {
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].ReadOnly = false;
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].ReadOnly = true;
                    }
                    else
                    {
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].ReadOnly = true;
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].ReadOnly = false;
                    }

                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value = EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value.ToDecimal().ToString("F" + 0);
                        EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value = EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value.ToDecimal().ToString("F" + 0);
                    }

                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["LoanID"].Value = objclsDTOSettlementDetails.LoanID;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["Flag"].Value = 1;
                    EmployeeSettlementDetailDataGridView.Rows[i].Cells["SalaryPaymentID"].Value = objclsDTOSettlementDetails.SalaryPaymentID;
                    i = ++i;
                }
                
                //-----------------------------------------------------------------------------------------------
                DtGrdGratuity.Rows.Clear();
                i = 0;

                if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOGratuityDetails != null)
                {
                    foreach (clsDTOGratuityDetails objclsDTOGratuityDetails in MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOGratuityDetails)
                    {
                        DtGrdGratuity.RowCount = DtGrdGratuity.RowCount + 1;
                        DtGrdGratuity.Rows[i].Cells["ColYears"].Value = objclsDTOGratuityDetails.ColYears;
                        DtGrdGratuity.Rows[i].Cells["ColCalDays"].Value = objclsDTOGratuityDetails.ColCalDays;
                        DtGrdGratuity.Rows[i].Cells["ColGRTYDays"].Value = objclsDTOGratuityDetails.ColGRTYDays;
                        DtGrdGratuity.Rows[i].Cells["PerDayGratuity"].Value = objclsDTOGratuityDetails.ColPerDayGratuity;
                        DtGrdGratuity.Rows[i].Cells["ColGratuityAmt"].Value = objclsDTOGratuityDetails.ColGratuityAmt;
                        i = i + 1;
                    }
                }
                //-----------------------------------------------------------------------------------------------
            }
            FillExcludGrid();
           
            IsFormLoad = true;
            TxtTotalEarning.Text = (TotalEarningSettlementAmt()).ToString();
            TxtTotalDeduction.Text = (TotalDedSettlementAmt()).ToString();
            NetAmountTextBox.Text = Convert.ToString(Convert.ToDecimal(TxtTotalEarning.Text) - Convert.ToDecimal(TxtTotalDeduction.Text));
            BtnCancel.Enabled = false;
        }
        private void FillExcludGrid()
        {
            try
            {
                GrdLeaveType.Rows.Clear();  
                DataTable DTT;
                DTT = MobjclsBLLSettlementProcess.SettlementLeave(CboEmployee.SelectedValue.ToInt32());
          
                for (int L = 0; L <= DTT.Rows.Count - 1; L++)
                {
                    GrdLeaveType.RowCount = GrdLeaveType.RowCount + 1;

                    if (DTT.Rows[L]["Sel"].ToInt32() == 1)
                        GrdLeaveType.Rows[L].Cells["Select"].Value = true;

                    GrdLeaveType.Rows[L].Cells["LeaveTypeID"].Value = DTT.Rows[L]["LeaveTypeID"].ToInt32();
                    GrdLeaveType.Rows[L].Cells["LeaveType"].Value = DTT.Rows[L]["LeaveType"].ToString();
                    GrdLeaveType.Rows[L].Cells["Days"].Value = DTT.Rows[L]["NoOfDays"].ToString();
                }
            }
            catch
            { }
        }
        
        private void Fillparameters(int TypeIDD)
        {
            try
            {
                if (MbAddStatus == true)
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 1;
                else
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AddEditMode = 2;                

                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID = Convert.ToInt32(LblType.Tag);
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID = Convert.ToInt32(CboEmployee.SelectedValue);
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Date = DateDateTimePicker.Value.Date.ToString("dd-MMM-yyy");
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.DocumentReturn = DocumentReturnCheckBox.Checked;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AssetReturn = DocumentReturnCheckBox.Checked;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Remarks = fillremarks();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.NetAmount = Convert.ToDecimal(TotalEarningSettlementAmt().ToDecimal()  - TotalDedSettlementAmt().ToDecimal() );
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.CurrencyID = miCurrencyId;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementType = Convert.ToInt32(CboType.SelectedValue);
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.FromDate = DtpFromDate.Value.Date.ToString("dd-MMM-yyyy") ;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ToDate = DtpLastWorkingDate.Value.Date.ToString("dd-MMM-yyyy");
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed = blnConfirm;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.TransactionTypeID=Convert.ToInt32(cboTransactionType.SelectedValue);  
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ChequeNumber=txtChequeNo.Text;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ChequeDate = dtpChequeDate.Value.Date.ToString("dd-MMM-yyyy");
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.strResignationDate = dtpResignationDate.Value.Date.ToString("dd-MMM-yyyy");
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.CreatedBy = ClsCommonSettings.UserID;
                MiCompanyID = MobjclsBLLSettlementProcess.GetEmpCompanyID( MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID);

                if (MiCompanyID <= 0)
                    MiCompanyID = ClsCommonSettings.CurrentCompanyID;

                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.CompanyID = MiCompanyID;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.PaymentID = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.PaymentIdTemp  ;   
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ExperienceDays  =  Convert.ToDecimal(txtNofDaysExperience.Text);
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EligibleLeavePayDays=  Convert.ToDecimal(lblEligibleLeavePayDays.Text.ToDecimal())  ;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.TakenLeavePayDays = Convert.ToDecimal(lblTakenLeavePayDays.Text.ToDecimal());
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.VacationDays = Convert.ToDecimal(txtLeavePayDays.Text.ToDecimal());
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.LeavePayDays = Convert.ToDecimal(txtLeavePayDays.Text.ToDecimal());
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.AbsentDays = Convert.ToDecimal((txtAbsentDays.Text =="" ? "0" : txtAbsentDays.Text));
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.ConsiderAbsentDays = chkAbsent.Checked;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.decNoticePeriod = txtNoticePeriod.Text.ToDecimal();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.blnIsExcludeHolidays = chkExcludeHolidays.Checked;
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.decExcludeHolidays = txtExcludeHolidays.Text.ToDecimal();

                if (txtExcludeLeaveDays.Text.ToDecimal() > 0)
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.blnIsExcludeLeaveDays = true;
                else
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.blnIsExcludeLeaveDays = false;

                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.decExcludeLeaveDays = txtExcludeLeaveDays.Text.ToDecimal();
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.decEligibleDays = txtEligibleDays.Text.ToDecimal();

                if (TypeIDD == 1)
                {
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOSettlementDetails = new System.Collections.Generic.List<clsDTOSettlementDetails>();
                    
                    for (int i = 0; Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows.Count) - 1 >= i; ++i)
                    {
                        clsDTOSettlementDetails objclsDTOSettlementDetails = new clsDTOSettlementDetails();

                        if (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value != null)
                        {
                            if (EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value.ToString() != "")
                            {
                                objclsDTOSettlementDetails.EmployeeSettlementDetailID = Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["EmployeeSettlementDetailID"].Value);
                                objclsDTOSettlementDetails.ParticularsID = EmployeeSettlementDetailDataGridView.Rows[i].Cells["Particulars"].Value.ToInt32();

                                if (Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["IsAddition"].Value) == 1)
                                {
                                    objclsDTOSettlementDetails.IsAddition = true;
                                    objclsDTOSettlementDetails.AmountCredit = Convert.ToDecimal(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value);
                                    objclsDTOSettlementDetails.Amount = Convert.ToDecimal(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Credit"].Value);
                                    objclsDTOSettlementDetails.AmountDebit =0;
                                }
                                else
                                {
                                    objclsDTOSettlementDetails.IsAddition = false;
                                    objclsDTOSettlementDetails.AmountDebit = Convert.ToDecimal(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value);
                                    objclsDTOSettlementDetails.Amount = Convert.ToDecimal(EmployeeSettlementDetailDataGridView.Rows[i].Cells["Debit"].Value);
                                    objclsDTOSettlementDetails.AmountCredit = 0;
                                }

                                objclsDTOSettlementDetails.LoanID = Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["LoanID"].Value);
                                objclsDTOSettlementDetails.SalaryPaymentID = Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["SalaryPaymentID"].Value);
                                objclsDTOSettlementDetails.flag = Convert.ToInt32(EmployeeSettlementDetailDataGridView.Rows[i].Cells["flag"].Value);
                                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOSettlementDetails.Add(objclsDTOSettlementDetails);
                            }
                        }
                    }
                }

                if (TypeIDD == 1)
                {
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOGratuityDetails = new System.Collections.Generic.List<clsDTOGratuityDetails>();
                    for (int i = 0; Convert.ToInt32(DtGrdGratuity.Rows.Count) - 1 >= i; ++i)
                    {
                        clsDTOGratuityDetails objclsDTOGratuityDetails = new clsDTOGratuityDetails();
                        objclsDTOGratuityDetails.ColYears = Convert.ToString(DtGrdGratuity.Rows[i].Cells["ColYears"].Value);
                        objclsDTOGratuityDetails.ColCalDays = Convert.ToDecimal(DtGrdGratuity.Rows[i].Cells["ColCalDays"].Value);
                        objclsDTOGratuityDetails.ColGRTYDays = Convert.ToDecimal(DtGrdGratuity.Rows[i].Cells["ColGRTYDays"].Value);
                        objclsDTOGratuityDetails.ColGratuityAmt = Convert.ToDecimal(DtGrdGratuity.Rows[i].Cells["ColGratuityAmt"].Value);
                        objclsDTOGratuityDetails.ColPerDayGratuity = Convert.ToDecimal(DtGrdGratuity.Rows[i].Cells["PerDayGratuity"].Value);
                        MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOGratuityDetails.Add(objclsDTOGratuityDetails);
                    }
                }
                FillLeaveTypeParameters();
            }
            catch { }
        }

        private void FillLeaveTypeParameters()
        {
            try
            {
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOLeaveTypeDetail = new System.Collections.Generic.List<clsDTOLeaveTypeDetail>();
                GrdLeaveType.EndEdit();

                for (int i = 0; Convert.ToInt32(GrdLeaveType.Rows.Count) - 1 >= i; ++i)
                {
                    if (GrdLeaveType.Rows[i].Cells["Select"].Value.ToBoolean())
                    {
                        clsDTOLeaveTypeDetail objclsDTOLeaveType = new clsDTOLeaveTypeDetail();
                        objclsDTOLeaveType.LeaveTypeID = Convert.ToString(GrdLeaveType.Rows[i].Cells["LeaveTypeID"].Value);
                        MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOLeaveTypeDetail.Add(objclsDTOLeaveType);
                    }
                }
            }
            catch
            { }
        }
        private void cboTransactionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTransactionType();
            ChangeStatus(); 

            if (cboTransactionType.SelectedValue.ToInt32() > 0)
            {
                if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Bank && CboEmployee.SelectedValue.ToInt32() > 0)
                    LoadCombos(3);//Bank
            }
        }

        private void SetTransactionType()
        {
            if (cboTransactionType.SelectedValue.ToInt32() == (int)PaymentTransactionType.Cheque)
            {
                txtChequeNo.Enabled = true;
                dtpChequeDate.Enabled = true;
                txtChequeNo.BackColor = System.Drawing.SystemColors.Info;
            }
            else
            {
                txtChequeNo.Enabled = false;
                dtpChequeDate.Enabled = false;
                txtChequeNo.Text = "";
                dtpChequeDate.Value = DateTime.Now;
                txtChequeNo.BackColor = System.Drawing.SystemColors.Window;
            }
        }

        private void cboAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtChequeNo_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboTransactionType_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboTransactionType.DroppedDown = false; 
        }

        private void EmployeeSettlementDetailDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (EmployeeSettlementDetailDataGridView.CurrentCell != null)
                {
                    if (EmployeeSettlementDetailDataGridView.CurrentCell.ColumnIndex == Credit.Index || EmployeeSettlementDetailDataGridView.CurrentCell.ColumnIndex == Debit.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                }
            }
            catch { }
        }

        void txtDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            e.Handled = false;

            if (ClsCommonSettings.IsAmountRoundByZero)
                strInvalidChars = strInvalidChars + ".";

            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0) || (e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
                e.Handled = true;
        }

        private void RemarksTextBox1_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpChequeDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void EmployeeSettlementDetailDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            ChangeStatus();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            EmployeeSettlementBindingNavigatorSaveItem_Click(sender, e); 
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                BtnPrintRpt_Click(sender, e);
            }
            catch { }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
           try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "EmployeeSettlement";
                    ObjEmailPopUp.EmailFormType = EmailFormID.SettlementProcess;
                    ObjEmailPopUp.EmailSource = MobjclsBLLSettlementProcess.EmailDetail(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID, MobjclsBLLSettlementProcess.clsDTOSettlementProcess.EmployeeID);
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch { }
        }

        private void BtnBottomCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CalculateLeavePayDays()
        {
            if (CboEmployee.SelectedIndex >= 0)
            {
                int intEmpID = CboEmployee.SelectedValue.ToInt32();
                //CboEmployee.SelectedIndex = -1;
                //CboEmployee.SelectedValue = intEmpID;

                if (chkAbsent.Checked == true)
                {
                    decimal decLeavePayDays = (Convert.ToDecimal(txtLeavePayDays.Text) - Convert.ToDecimal(txtAbsentDays.Text));

                    if (decLeavePayDays > 0)
                        txtLeavePayDays.Text = decLeavePayDays.ToString();
                    else
                        txtLeavePayDays.Text = "0";
                }
            }
            else
                txtLeavePayDays.Text = "0";
        }

        private void chkAbsent_CheckedChanged(object sender, EventArgs e)
        {
            CalculateLeavePayDays();
            ChangeStatus();
        }

         private void FrmSettlementEntry_KeyDown(object sender, KeyEventArgs e)
         {
             try
             {
                 switch (e.KeyData)
                 {
                     case Keys.F1:
                         FrmHelp objHelp = new FrmHelp();
                         objHelp.strFormName = "Settlement process";
                         objHelp.ShowDialog();
                         objHelp = null;
                         break;
                     case Keys.Escape:
                         this.Close();
                         break;
                     case Keys.Control | Keys.Enter:
                         BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                         break;
                     case Keys.Alt | Keys.R:
                         BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                         break;
                     case Keys.Control | Keys.E:
                         BtnCancel_Click(sender, new EventArgs());//Clear
                         break;
                     case Keys.Control | Keys.P:
                         BtnPrint_Click(sender, new EventArgs());
                         break;
                     case Keys.Control | Keys.M:
                         btnEmail_Click(sender, new EventArgs());
                         break;
                 }
             }
             catch { }
         }

         private void EmployeeSettlementDetailDataGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
         {
             ChangeStatus();
         }

         private void EmployeeSettlementDetailDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
         {
             try
             {
                 //if (e.ColumnIndex > -1 && e.RowIndex > -1)
                 //{
                 //    if (EmployeeSettlementDetailDataGridView.Rows[e.RowIndex].Cells[SalaryPaymentID.Index].Value.ToInt32() > 0)
                 //    {
                 //        e.Cancel = true;
                 //        return;
                 //    }
                 //}
             }
             catch { }
         }

         private void EmployeeSettlementDetailDataGridView_KeyDown(object sender, KeyEventArgs e)
         {
             try
             {
                 if (e.KeyData == Keys.Delete && EmployeeSettlementDetailDataGridView.CurrentCell != null)
                 {
                     if (EmployeeSettlementDetailDataGridView.CurrentRow.Cells[SalaryPaymentID.Index].Value.ToInt32() > 0)
                     {
                         if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID > 0)
                         {
                             MobjclsBLLSettlementProcess.DeletePaymentDetails();
                         }
                     }
                 }
             }
             catch { }
         }

         private void BtnHelp_Click(object sender, EventArgs e)
         {
             try
             {
                 FrmHelp objHelp = new FrmHelp();
                 objHelp.strFormName = "Settlement process";
                 objHelp.ShowDialog();
                 objHelp = null;
             }
             catch { }
         }

         private void FrmSettlementEntry_FormClosing(object sender, FormClosingEventArgs e)
         {
             if (MbChangeStatus)
             {
                 // Checking the changes are not saved and shows warning to the user
                 MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 8, out MmessageIcon);
                 if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo,
                     MessageBoxIcon.Question) == DialogResult.Yes)
                     e.Cancel = false;
                 else
                     e.Cancel = true;
             }
         }

         private void EmployeeSettlementDetailDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
         {
             try
             {
                 if (EmployeeSettlementDetailDataGridView.IsCurrentCellDirty)
                     EmployeeSettlementDetailDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
             }
             catch { }
         }

         private void EmployeeSettlementDetailDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
         {
             try { } 
             catch { }
         }

        private void GetEmployeeLeaveHolidays()
        {
            FillLeaveTypeParameters();
            DataTable datTemp = MobjclsBLLSettlementProcess.GetEmployeeLeaveHolidays(CboEmployee.SelectedValue.ToInt32(),
            DtpFromDate.Value.ToString("dd-MMM-yyyy"), DtpLastWorkingDate.Value.ToString("dd-MMM-yyyy"));
            //string Str = "";

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (datTemp.Rows[1]["Days"].ToDecimal() > 0) // Holidays Days
                    {
                        if (datTemp.Rows[1]["Days"].ToString().Contains("."))
                        {
                            if ((datTemp.Rows[1]["Days"].ToString().Split('.').Last()).ToDecimal() > 0)
                                txtExcludeHolidays.Text = datTemp.Rows[1]["Days"].ToString();
                            else
                                txtExcludeHolidays.Text = datTemp.Rows[1]["Days"].ToString().Split('.').First();
                        }
                        else
                            txtExcludeHolidays.Text = datTemp.Rows[1]["Days"].ToString();
                    }
                    else
                    {
                        if (ClsCommonSettings.IsArabicView)
                            txtExcludeHolidays.Text = "لا شيء";
                        else
                            txtExcludeHolidays.Text = "NIL";                        
                    }

                    calculateEligibleDays();
                }
            }
        }
        
        private void chkExcludeHolidays_CheckedChanged(object sender, EventArgs e)
        {
            calculateEligibleDays();
        }

        private void calculateEligibleDays()
        {
            txtEligibleDays.Text = txtNofDaysExperience.Text;
            decimal DecLeaveDays;
            DecLeaveDays = 0;
            GrdLeaveType.EndEdit(); 

            for (int i = 0; Convert.ToInt32(GrdLeaveType.Rows.Count) - 1 >= i; ++i)
            {
                if (GrdLeaveType.Rows[i].Cells["Select"].Value.ToBoolean())
                {
                    DecLeaveDays = DecLeaveDays + GrdLeaveType.Rows[i].Cells["Days"].Value.ToDecimal();
                }
            }
            
            txtExcludeLeaveDays.Text = DecLeaveDays.ToString(); 

            if (DecLeaveDays > 0)
                 txtEligibleDays.Text = (txtEligibleDays.Text.ToDecimal() - DecLeaveDays).ToString();
            
            if (chkExcludeHolidays.Checked)
            {
                if (txtExcludeHolidays.Text.ToDecimal() > 0)
                    txtEligibleDays.Text = (txtEligibleDays.Text.ToDecimal() - txtExcludeHolidays.Text.ToDecimal()).ToString();
            }
        }

        private void BtnPrintRpt_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmReportSelector objReportSelector = new FrmReportSelector())
                {
                    objReportSelector.datTempSalary = datTempSalary;
                    objReportSelector.PiSettlementID = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID;
                    objReportSelector.ShowDialog();
                }
            }
            catch { }
        }

        private void dtpResignationDate_ValueChanged(object sender, EventArgs e)
        {
            DateDiff();
        }

        private void DateDiff()
        {
            try
            {
                int DateDiff;
                DateDiff = (DtpLastWorkingDate.Value.Date - dtpResignationDate.Value.Date).TotalDays.ToInt32();

                if (DateDiff > 0)
                    txtNoticePeriod.Text = DateDiff.ToStringCustom();  
                else
                    txtNoticePeriod.Text = "0";
            }
            catch 
            {}
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cboSearchEmployee.SelectedIndex != -1 && cboSearchCode.SelectedIndex ==-1)
            {
                cboSearchCode.SelectedValue = cboSearchEmployee.SelectedValue.ToInt32();  
            }

            dgvSearch.DataSource = MobjclsBLLSettlementProcess.GetSettlementSearch(cboSearchCompany.SelectedValue.ToInt32(), 
                cboSearchCode.SelectedValue.ToInt32(), dtpSearchFromDate.Value.ToString("dd MMM yyyy").ToDateTime(), 
                dtpSearchToDate.Value.ToString("dd MMM yyyy").ToDateTime());
        }

        private void dgvSearch_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                MbAddStatus = false;
                CboEmployee.SelectedIndex = -1;

                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID = dgvSearch.Rows[e.RowIndex].Cells["SettlementID"].Value.ToInt32();
                DisplayInfo(0);

                TmrEmployeeSettlement.Enabled = true;
                this.BindingNavigatorAddNewItem.Enabled = MbAddPermission;
                this.BindingNavigatorDeleteItem.Enabled = MbDeletePermission;
                BtnPrint.Enabled = btnEmail.Enabled = MbPrintEmailPermission;
                OKSaveButton.Enabled = MbUpdatePermission;
                MbAddStatus = false;
                MbChangeStatus = false;
                BtnSave.Enabled = MbChangeStatus;
                EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;

                ((Control)this.SettlementTabEmpInfo).Enabled = true;
                ((Control)this.SettlementTabParticulars).Enabled = true;
                ((Control)this.SettlementTabHistory).Enabled = true;

                if (Convert.ToInt32(LblEmployee.Tag) == 1)// '' When Closed Settlement
                {
                    ((Control)this.SettlementTabEmpInfo).Enabled = false;
                    ((Control)this.SettlementTabParticulars).Enabled = false;
                    ((Control)this.SettlementTabHistory).Enabled = false;

                    BindingNavigatorDeleteItem.Enabled = OKSaveButton.Enabled = false;
                }

                SettlementTab.SelectedTab = SettlementTabEmpInfo;
                EnableDisablePartially();
                CboEmployee.Enabled = btnShow.Enabled = false;
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {

            try
            {
                int miCurrencyIdt = 0; int MiCompanyIDt = 0; string LblCurrencyt = ""; string DtpFromDatet = ""; int TransactionTypeIDt = 0;
                gmajilgmai.Visible = true;
                gmajilgmai.Value = 10;
                Application.DoEvents();
                ErrEmployeeSettlement.Clear();
                gmajilgmai.Value = 20;
                gmajilgmai.Refresh();
                if (IsFormLoad == true)
                {
                    lblPayBack.Text = "";
                    DateTime TempDateTime = new DateTime(1900, 1, 1, 0, 0, 0);
                    bLoad = true;
                    ChangeStatus();
                 
                    if (CboEmployee.SelectedIndex != -1)
                    {
                        if (MbAddStatus == true)
                        {
                            if (CboEmployee.SelectedIndex >= 0)
                                MobjclsBLLSettlementProcess.GetSalaryReleaseDate(Convert.ToInt32(CboEmployee.SelectedValue), ref TempDateTime);
                        }
                    }

                    MobjclsBLLSettlementProcess.DisplayCurrencyEmployee(Convert.ToInt32(CboEmployee.SelectedValue), ref  miCurrencyIdt, ref  MiCompanyIDt, ref  LblCurrencyt, ref  DtpFromDatet, ref TransactionTypeIDt);
                    gmajilgmai.Value = 30;
                    miCurrencyId = miCurrencyIdt;
                    MiCompanyID = MiCompanyIDt;
                    LblCurrency.Text = LblCurrencyt;

                    if (DtpFromDatet != "")
                    {
                        if (Convert.ToDateTime(DtpFromDatet) > Convert.ToDateTime("01-Jan-1900"))
                            DtpFromDate.Value = DtpFromDatet.ToDateTime();
                    }

                    FillExcludGrid();
                    gmajilgmai.Value = 40;
                    GetSettlementPolicyAndExperience();
                    gmajilgmai.Value = 70;
                    CalculateLeavePayDays();
                    gmajilgmai.Value = 80;
                    DateDiff();
                    EmployeeHistoryDetail();
                    gmajilgmai.Value = 90;
                    if (MbAddStatus == true)
                        cboTransactionType.SelectedValue = TransactionTypeIDt;

                    if (CboEmployee.SelectedIndex >= 0 && CboType.SelectedIndex >= 0 && MbAddStatus == true)
                        BtnProcess.Enabled = MbAddPermission;
                }
                else if (CboEmployee.SelectedIndex > -1)
                {
                    ///4 getting currencyid
                    MobjclsBLLSettlementProcess.DisplayCurrencyEmployee(Convert.ToInt32(CboEmployee.SelectedValue), ref  miCurrencyIdt, ref  MiCompanyIDt, ref  LblCurrencyt, ref  DtpFromDatet, ref TransactionTypeIDt);
                    miCurrencyId = miCurrencyIdt;
                    MiCompanyID = MiCompanyIDt;
                    LblCurrency.Text = LblCurrencyt;
                }
                System.Threading.Thread.Sleep(1000);
                gmajilgmai.Value = 100;
               
                gmajilgmai.Visible = false;
                Application.DoEvents();
            }
            catch { }
        }

        private void cboSearchCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCombos(2);
            LoadCombos(5);
        }

        private void btnTemplate1_Click(object sender, EventArgs e)
        {
            PrintForm(0);
        }

        private void btnTemplate2_Click(object sender, EventArgs e)
        {
            PrintForm(1);
        }

        private void btnTemplate3_Click(object sender, EventArgs e)
        {
            PrintForm(2);
        }

        private void btnTemplate4_Click(object sender, EventArgs e)
        {
            PrintForm(3);
        }

        private void PrintForm(int Template)
        {
            try
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = "Employee Settlement";
                ObjViewer.PiRecId = MobjclsBLLSettlementProcess.clsDTOSettlementProcess.SettlementID;
                ObjViewer.PiFormID = (int)FormID.SettlementProcess;

                if (Template == 0)
                    ObjViewer.OperationTypeID = 1000;
                else if (Template == 1)
                    ObjViewer.OperationTypeID = 2000;
                else if (Template == 2)
                    ObjViewer.OperationTypeID = 3000;
                else if (Template == 3)
                    ObjViewer.OperationTypeID = -1;
                else if (Template == 4)
                    ObjViewer.OperationTypeID = 4000;

                ObjViewer.No = 0;

                if (datTempSalary != null)
                {
                    if (datTempSalary.Rows.Count > 0)
                    {
                        ObjViewer.dteFromDate = Convert.ToDateTime(datTempSalary.Rows[0]["PeriodFrom"]);
                        ObjViewer.dteToDate = Convert.ToDateTime(datTempSalary.Rows[0]["PeriodTo"]);
                        ObjViewer.No = 1;
                    }
                }

                ObjViewer.ShowDialog();
            }
            catch { }
        }

        void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
        }

        private void GrdLeaveType_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == GrdLeaveType.Columns["Days"].Index)
                {
                    calculateEligibleDays();
                }
            }
        }

        private void GrdLeaveType_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void GrdLeaveType_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            GrdLeaveType.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }


        private void btnAdditionDeductions_Click(object sender, EventArgs e)
        {
            Program.objMain.ShowDialogForm(new FrmAdditionDeduction(), true);
            LoadCombos(4);
            LoadCombos(6);
        }

        private void BtnPrint_ButtonClick(object sender, EventArgs e)
        {

        }

        private void cboParticulars_KeyDown(object sender, KeyEventArgs e)
        {
            cboParticulars.DroppedDown = false; 
        }


    }
}