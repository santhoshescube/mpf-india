﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using BLL;
using DTO;
/* Design By        : Hariram 
 * Designed Date    : 08 Oct 2015
 * Developed By     : Hariram
 * Develop St.Date  : 08 Oct 2015
 * Develop End Date : 08 Oct 2015
 * Purpose          : Settlement Process and Confirm Group wise
*/
namespace MyPayfriend
{
    public partial class FrmSettlementProcessGroup : DevComponents.DotNetBar.Office2007Form
    {

        bool MbChangeStatus;
        bool MbAddStatus;
        int MiCompanyID;
        int miCurrencyId;
        bool MbPrintEmailPermission = true;
        bool MbAddPermission = true;
        bool MbUpdatePermission = true;
        bool MbDeletePermission = true;
        string MsMessageCommon = "";
        bool bProcess = false;
        bool bClickProcess = false;
        bool bLoad = false;
        public int Pscale;
        bool blnConfirm = false;
        DataTable datTempSalary = new DataTable();
        Double dblTempSalary;
        public bool IsFormLoad;
        private string MstrMessageCommon; //Messagebox display
        private string MstrMessageCaption = ClsCommonSettings.MessageCaption;              //Message caption
        private ArrayList MsarMessageArr; // Error Message display

        public int PiCompanyID = 0;// Reference From Work policy
        private int MintTimerInterval;                  // To set timer interval
        private int MintCompanyId;                      // current companyid
        private int MintUserId;

        private MessageBoxIcon MmessageIcon;            // to set the message icon
        private bool Glb24HourFormat;
        private DateTime SysDate;
        ClsNotification mObjNotification;
        clsBLLSettlementProcess MobjclsBLLSettlementProcess = new clsBLLSettlementProcess();


        private bool MbFirst = false;
        private string NavClicked = "";
        private string RecordID1 = "0";
        private string RecordID2 = "0";
        private int CurrentPage = 1;
        private int TotalPage = 1;
        private DataSet dsTemp = new DataSet();
        private string MstrCommonMessage;
        
        public string strProcessDate;
        public string FillDate;
        public bool FlagSelect;
        public int PBankNameIDSalRel;
        public int PCompanyID;
        public int PProcessYear;

        public string lblNoofMonths;
        public string lblSettlementPolicy;
        public string txtNofDaysExperience;
        public string txtEligibleDays;
        public string txtLeavePayDays;
        public string lblEligibleLeavePayDays;
        public string lblTakenLeavePayDays;
        public string txtAbsentDays;
        public string txtExcludeHolidays;
        public int PDepartmentID;
        public int PDesignationID;

     
        clsConnection mObjCon = new clsConnection();
        clsBLLSalaryProcessAndRelease mobjclsBLLSalaryProcessAndRelease = new clsBLLSalaryProcessAndRelease();
        clsBLLSalaryProcess mobjProcess = new clsBLLSalaryProcess();
        private clsMessage ObjUserMessage = null;

        bool norecord;
        int pageRows = 25;
        bool FlgBlk = true;
        bool MChangeStatus; // Check state of the page
        bool MViewPermission;
        bool MAddPermission;
        bool MUpdatePermission;
        bool MDeletePermission;
        bool blnSalaryDayIsEditable = false;
        bool MblnPrintEmailPermission; bool MblnAddPermission; bool MblnUpdatePermission; bool MblnDeletePermission;
        int CurrentMonth;
        int CurrentYear;
        int MCompanyID;
        int MEmployeeID;        
        int MPaymentClassificationID;
        int BranchIndicator;
        int TemplateID;
        int TransactionTypeIDRel;
        int MCurrencyID;
        int intCompanyID = ClsCommonSettings.CurrentCompanyID;
        
        string MFromDate;
        string MToDate;
        string mdtFrom = "";
        string mDtTo = "";
        string mLOP = "";
        string mHLOP = "";
        string mLLOP = "";
        string mSType = "";
        string GetHostName = "";

        DateTime LoadDate;

        DataTable DtProcessEmployee = new DataTable();
        DataTable datEmployeeSalaryDetails = new DataTable();
      
              
        TreeNode TvRoot;
        TreeNode TvChild;
        TreeNode TvChildChild;
        bool CobFlg = false;
        string FprocessDate = "";
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.SalaryRelease);
                return this.ObjUserMessage;
            }
        }

        public FrmSettlementProcessGroup()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
                expPnlSettlementProcess.TitleText  = "عملية الراتب ";
                Label1.Text = "شركة";
                Label7.Text = "الفرع";
                lblDept.Text = "قسم";
                lblDesg.Text = "تعيين";
                btnShow.Text = "عرض";
                chkIncludeCompany.Text = "تشمل شركة ";
                chkAdvanceSearch.Text = "بحث متقدم ";
              
            }
        }

        enum NavButton
        {
            First = 1,
            Next = 2,
            Previous = 3,
            Last = 4
        }

        private void FrmSettlementProcessGroup_Load(object sender, EventArgs e)
        {

            expPnlSettlementProcess.Visible = true;
      
            
           
            if (ClsCommonSettings.IsArabicView)
            {
                btnProcess.Text = "عملية";
            }
            else
            {
                btnProcess.Text = "&Process";
            }
            btnProcess.Image = Properties.Resources.Salary_processing;
            pnlLeft.Width = 250;
            //this.ToolStripDes.SetToolTip(this.Label16, "Bank");



            cboCountItem.Items.Add("All");
            cboCountItem.Items.Add("25");
            cboCountItem.Items.Add("50");
            cboCountItem.Items.Add("100");
            cboCountItem.Text = "25";
            SetPermissions();
            SetPermissionsRelease();
            ClearControls();
            LoadCombos();

            dgvSettlementDetails.DataSource = getNewTableForProcess();
            datTempSalary = (DataTable)dgvSettlementDetails.DataSource;

            GridColumnDisabled();

            GetHostName = System.Net.Dns.GetHostName().Trim();
        }
        private void SetPermissionsRelease()
        {
            //Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Payroll, (Int32)eMenuID.SalaryPayment, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

    
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            
                objDAL.SetArabicVersion((int)FormID.SalaryProcess, this);
           
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetPermissions()
        {
            if (ClsCommonSettings.RoleID <= 3)
                MViewPermission = MAddPermission = MUpdatePermission = MDeletePermission = true;
            else
            {
                clsBLLPermissionSettings objPermission = new clsBLLPermissionSettings();
                int intMenuID = 0;
              
                    intMenuID = (int)eMenuID.SalaryProcess;
        

                objPermission.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (int)eModuleID.Payroll, intMenuID,
                        out MViewPermission, out MAddPermission, out MUpdatePermission, out MDeletePermission);
            }
        }

        private void ClearControls()
        {
            if (!datEmployeeSalaryDetails.Columns.Contains("EmployeeID"))
                datEmployeeSalaryDetails.Columns.Add("EmployeeID");

            if (!datEmployeeSalaryDetails.Columns.Contains("CompanyID"))
                datEmployeeSalaryDetails.Columns.Add("CompanyID");

            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(12, 0); // Get Current Month & Year
            CurrentMonth = datTemp.Rows[0]["MonthNo"].ToInt32();
            CurrentYear = datTemp.Rows[0]["YearNo"].ToInt32();
            LoadDate = ("01/" + GetCurrentMonth(CurrentMonth) + "/" + CurrentYear + "").ToDateTime();

          
            MCompanyID = -1;
            MEmployeeID = -1;
                
                btnProcess.Enabled = false;
           
            chkAdvanceSearch.Checked = false;
            getAdvanceSearch();
            
           
        }

        

        private void getAdvanceSearch()
        {
            lblDept.Visible = chkAdvanceSearch.Checked;
            cboDepartment.Visible = chkAdvanceSearch.Checked;
            lblDesg.Visible = chkAdvanceSearch.Checked;
            cboDesignation.Visible = chkAdvanceSearch.Checked;
            chkAdvanceSearch.Checked = true;
            if (chkAdvanceSearch.Checked == false)
            {
                btnShow.Location = new Point(75, DtpLastWorkingDate.Location.Y);
                showProgressBar.Location = new Point(30, (btnShow.Location.Y + 45));
            }
            else
            {
                btnShow.Location = new Point(75, (DtpLastWorkingDate.Location.Y + 30));
                showProgressBar.Location = new Point(30, (btnShow.Location.Y + 45));
            }

            cboDepartment.SelectedIndex = -1;
            cboDesignation.SelectedIndex = -1;
        }

        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }

        

        private void LoadCombos()
        {
            try
            {
                CobFlg = true;
                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(1, 0); // Get Company
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datTemp;
                cboCompany.SelectedIndex = -1;

                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(2, 0); // Get Department
                cboDepartment.ValueMember = "DepartmentID";
                cboDepartment.DisplayMember = "Department";
                cboDepartment.DataSource = datTemp;
                cboDepartment.SelectedIndex = -1;

            


                datTemp = null;
                datTemp = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(3, 0); // Get Designation
                cboDesignation.ValueMember = "DesignationID";
                cboDesignation.DisplayMember = "Designation";
                cboDesignation.DataSource = datTemp;
                cboDesignation.SelectedIndex = -1;


                datTemp = MobjclsBLLSettlementProcess.FillCombos(new string[] { 
                    "WorkstatusID," + (ClsCommonSettings.IsArabicView ? "WorkstatusArb" : "Workstatus") + " AS Workstatus", 
                    "WorkStatusReference", " workstatusID <6" });
                CboType.ValueMember = "WorkstatusID";
                CboType.DisplayMember = "Workstatus";
                CboType.DataSource = datTemp;
             

               
                cboCompany.SelectedValue = intCompanyID;


                CobFlg = false; 
            }
            catch{
                CobFlg = false; 
            }
        }

        private DataTable getNewTableForProcess()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("SelectAll");
            datTemp.Columns.Add("EmployeeID");
            datTemp.Columns.Add("EmployeeName");
            datTemp.Columns.Add("CompanyID");
            datTemp.Columns.Add("CompanyName");
            datTemp.Columns.Add("DepartmentID");
            datTemp.Columns.Add("Department");
            datTemp.Columns.Add("DesignationID");
            datTemp.Columns.Add("Designation");
            datTemp.Columns.Add("EmploymentTypeID");
            datTemp.Columns.Add("EmploymentType");
            datTemp.Columns.Add("TransactionTypeID");
            datTemp.Columns.Add("TransactionType");
            //datTemp.Columns.Add("PaymentClassificationID");
            datTemp.Columns.Add("TotalMonths");
            datTemp.Columns.Add("TotalDays");
            datTemp.Columns.Add("EligibleDays");
            datTemp.Columns.Add("NoticeDays");
            datTemp.Columns.Add("BalanceDays");
            return datTemp;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex != -1)
            {
                DataTable datTempBranch = new DataTable();
                datTempBranch.Columns.Add("BranchID");
                datTempBranch.Columns.Add("BranchName");

                if (cboCompany.SelectedIndex >= 0)
                    datTempBranch = mobjclsBLLSalaryProcessAndRelease.getDetailsInComboBox(7, cboCompany.SelectedValue.ToInt32()); // Get Branch

                cboBranch.ValueMember = "BranchID";
                cboBranch.DisplayMember = "BranchName";
                cboBranch.DataSource = datTempBranch;
                cboBranch.SelectedIndex = 0;

                cboDepartment.SelectedIndex = -1;
                cboDesignation.SelectedIndex = -1;
                cboDepartment.Text = "";
                cboDesignation.Text = "";
            }
        }

      
        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany_SelectedIndexChanged(sender, e);
            ComboBox_KeyPress(sender, e);
        }
        private void PointSelectAll(int Status)
        {
            try
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    int X1 = dgvSettlementDetails.Location.X.ToInt32();
                    int Y1 = dgvSettlementDetails.Location.Y.ToInt32();
                   
                        X1 = X1 + dgvSettlementDetails.Width - 23;
                        chkSelectAll.Location = new Point(X1, Y1);
                   
                }

            }
            catch
            {

            }
        }
        
        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {

                PointSelectAll(0);
                if (MAddPermission == false)
                {
                    MstrCommonMessage = UserMessage.GetMessageByCode(9131);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                CurrentPage = 0;
                if (FormValidationProcess())
                {
                    //if (SaveDetails())
                        Application.DoEvents();
                }

                btnProcess.Enabled = true;
            }
            catch
            {
                btnProcess.Enabled = false;
            }
        }

        private bool FormValidationProcess()
        {
            try
            {
                int CompanyID = 0;
                if (cboCompany.SelectedIndex != -1)
                {
                    intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                    if (cboBranch.SelectedValue.ToInt32() > 0)
                    {
                        CompanyID = Convert.ToInt32(cboBranch.SelectedValue);
                        BranchIndicator = chkIncludeCompany.Checked ? -1 : -2;

                        if (BranchIndicator == -2)
                            intCompanyID = Convert.ToInt32(cboBranch.SelectedValue);
                    }
                    else if (cboBranch.SelectedValue.ToInt32() == -2)
                    {
                        CompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                        BranchIndicator = -2;
                    }
                    else if (cboBranch.SelectedValue.ToInt32() == -1)
                    {
                        CompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                        BranchIndicator = chkIncludeCompany.Checked ? 0 : -3;
                    }
                }
                else
                {
                    CompanyID = 0;
                    MstrCommonMessage = UserMessage.GetMessageByCode(14);
                    MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                MCompanyID = CompanyID;
                MEmployeeID = 0;
                
              //  MPaymentClassificationID = cboPaymentClassification.SelectedValue.ToInt32();
                GridList(); // Showing Processed and Non Processed Data in the grid


                //if (intSalaryProcessForm == 1)
                //{
                //    GridColumnDisabled();
                //}

                if (dgvSettlementDetails.RowCount > 0)
                {
                    Application.DoEvents();
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool DateCheckingForPartial()
        {
            long DateDiffCnt = 0;
            DateDiffCnt = 0;

          /*  if (cboPaymentClassification.SelectedIndex != -1)
            {
                switch (cboPaymentClassification.SelectedValue.ToInt32())
                {
                    case 1:
                        DateDiffCnt = DateAndTime.DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        if (DateDiffCnt >= 1)
                            return false;
                        else
                            return true;
                    case 2:
                        DateDiffCnt = DateAndTime.DateDiff(DateInterval.Weekday, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        if (DateDiffCnt >= 1)
                            return false;
                        else
                            return true;
                    case 3:
                        DateDiffCnt = DateAndTime.DateDiff(DateInterval.Weekday, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        if (DateDiffCnt * 2 >= 1)
                            return false;
                        else
                            return true;
                    case 4:
                        long DateDiffCnt2 = 0;
                        long DateDiffCnt1 = 0;

                        DateDiffCnt2 = DateAndTime.DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
                        DateDiffCnt1 = DateAndTime.DateDiff(DateInterval.Day, dtpFromDate.Value.Date, DateAndTime.DateAdd(DateInterval.Month, 1, dtpFromDate.Value.Date), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);

                        if (DateDiffCnt1 < DateDiffCnt2)
                            DateDiffCnt = 1;

                        if (DateDiffCnt >= 1)
                            return false;
                        else
                            return true;
                    default:
                        return false;
                }
            }
            else
                return false;
           */
            return false;
        }

        private void GridList()
        {
            try
            {
                int intTempDept = 0;
                int intTempDesg = 0;
                int BranchID = 0;
                if (cboDepartment.SelectedIndex >= 0)
                    intTempDept = cboDepartment.SelectedValue.ToInt32();

                if (cboDesignation.SelectedIndex >= 0)
                    intTempDesg = cboDesignation.SelectedValue.ToInt32();
                BranchIndicator = -2;

                if (cboCompany.SelectedIndex != -1)
                {
                    MCompanyID = Convert.ToInt32(cboCompany.SelectedValue);
                }
                else
                {
                    MCompanyID = ClsCommonSettings.CurrentCompanyID;
                }

                if (cboBranch.SelectedIndex != -1)
                {
                    BranchID = Convert.ToInt32(cboBranch.SelectedValue);
                }
                else
                {
                    BranchID = ClsCommonSettings.CurrentCompanyID;
                }

                if (cboBranch.SelectedValue.ToInt32() > 0)
                {
                    BranchIndicator = chkIncludeCompany.Checked ? -1 : -2;
                }
                else if (cboBranch.SelectedValue.ToInt32() == -2)
                {
                    BranchIndicator = -2;
                }
                else if (cboBranch.SelectedValue.ToInt32() == -1)
                {
                    BranchIndicator = chkIncludeCompany.Checked ? 0 : -3;
                }
                datTempSalary = (DataTable)dgvSettlementDetails.DataSource;

                DataTable dt = new DataTable();
                dt = MobjclsBLLSettlementProcess.GetSettlementSearchByCompany(MCompanyID, BranchID, intTempDept, intTempDesg, BranchIndicator);
                    //, GetHostName, ClsCommonSettings.UserID, BranchIndicator,intTempDept, intTempDesg, BranchID);
                //DataTable dt1;
               // dt1 = new DataTable();
               // dgvSettlementDetails.DataSource = dt1;
                if (dt.Rows.Count > 0)
                {
                    dgvSettlementDetails.DataSource = dt;
                    datTempSalary = (DataTable)dgvSettlementDetails.DataSource; // For Paging
                    if (dt.Rows.Count > 0)
                    {
                        fillDataGrid_dtgBrowse();
                        //GridColumnColor();
                    }
                    
                    showDetails();
                  
                }
                else
                {
                    dgvSettlementDetails.DataSource = getNewTableForProcess();
                    datTempSalary = (DataTable)dgvSettlementDetails.DataSource;

                    GridColumnDisabled();
                }


                dgvSettlementDetails.ClearSelection();
            }
            catch
            {
            }
        }

        private void GridColumnColor()
        {
            GridColumnDisabled();

            for (int i = 0; i <= dgvSettlementDetails.Rows.Count - 1; i++)
            {
                
                    if (dgvSettlementDetails.Columns.Contains("Reason"))
                    {
                        if (dgvSettlementDetails.Rows[i].Cells["Reason"].Value.ToString() != "" &&
                            dgvSettlementDetails.Rows[i].Cells["Released"].Value.ToString() != "1")
                        {
                            dgvSettlementDetails.Rows[i].DefaultCellStyle.ForeColor = System.Drawing.Color.Firebrick;
                            dgvSettlementDetails.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke;
                            dgvSettlementDetails.Rows[i].ReadOnly = true;
                        }
                        else if (dgvSettlementDetails.Rows[i].Cells["Reason"].Value.ToString() == "" &&
                            dgvSettlementDetails.Rows[i].Cells["Released"].Value.ToString() == "1")
                        {
                            dgvSettlementDetails.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.LightSteelBlue;
                            dgvSettlementDetails.Rows[i].ReadOnly = true;
                        }
                    }
              
            }
        }

        private void GridColumnDisabled()
        {
            try
            {
                if (dgvSettlementDetails.Columns.Contains("SelectAll"))
                {
                    dgvSettlementDetails.Columns["SelectAll"].Visible = true;
                    dgvSettlementDetails.Columns["SelectAll"].Width = 30;
                    dgvSettlementDetails.Columns["SelectAll"].HeaderText = "";
                    dgvSettlementDetails.Columns["SelectAll"].ValueType = typeof(bool);
                }
                if (dgvSettlementDetails.Columns.Contains("TotalMonths"))
                {
                    dgvSettlementDetails.Columns["TotalMonths"].ValueType = typeof(string);
                }
                if (dgvSettlementDetails.Columns.Contains("BalanceDays"))
                {
                    dgvSettlementDetails.Columns["BalanceDays"].ValueType = typeof(string);
                }

                if (dgvSettlementDetails.Columns.Contains("EmployeeID"))
                    dgvSettlementDetails.Columns["EmployeeID"].Visible = false;

                if (dgvSettlementDetails.Columns.Contains("CompanyID"))
                    dgvSettlementDetails.Columns["CompanyID"].Visible = false;

                if (dgvSettlementDetails.Columns.Contains("DepartmentID"))
                    dgvSettlementDetails.Columns["DepartmentID"].Visible = false;

                if (dgvSettlementDetails.Columns.Contains("DesignationID"))
                    dgvSettlementDetails.Columns["DesignationID"].Visible = false;

                if (dgvSettlementDetails.Columns.Contains("EmploymentTypeID"))
                    dgvSettlementDetails.Columns["EmploymentTypeID"].Visible = false;

                if (dgvSettlementDetails.Columns.Contains("TransactionTypeID"))
                    dgvSettlementDetails.Columns["TransactionTypeID"].Visible = false;

                if (dgvSettlementDetails.Columns.Contains("RecordID"))
                    dgvSettlementDetails.Columns["RecordID"].Visible = false;

                if (dgvSettlementDetails.Columns.Contains("ProcessTypeID"))
                    dgvSettlementDetails.Columns["ProcessTypeID"].Visible = false;

                if (dgvSettlementDetails.Columns.Contains("ProcessType"))
                    dgvSettlementDetails.Columns["ProcessType"].Visible = false;
                if (ClsCommonSettings.IsArabicView)
                {
                    dgvSettlementDetails.Columns["Employee"].Visible = false;
                    dgvSettlementDetails.Columns["Department"].Visible = false;
                    dgvSettlementDetails.Columns["Designation"].Visible = false;
                }
                else
                {
                    dgvSettlementDetails.Columns["EmployeeArb"].Visible = false;
                    dgvSettlementDetails.Columns["DepartmentArb"].Visible = false;
                    dgvSettlementDetails.Columns["DesignationArb"].Visible = false;
                }

            }
            catch { }
        }
        private void GetSettlementPolicyAndExperience(int EmployeeID, string dojng)
        {
            string SetPolicyDescStr = ""; string lblNoofMonthsStr = ""; string txtNofDaysExperienceStr = ""; string txtLeavePayDaysStr = "";
            string lblTakenLeavePayDaysStr = ""; string lblEligibleLeavePayDaysStr = ""; string txtAbsentDaysStr = "";

            try
            {
               
                            if (EmployeeID != 0)
                            {
                                doj.Value = dojng.ToDateTime();
                                MobjclsBLLSettlementProcess.GetSettlementPolicyAndExperience(Convert.ToInt32(EmployeeID),
                                doj.Value.ToString("dd-MMM-yyyy"), DtpLastWorkingDate.Value.ToString("dd-MMM-yyyy"), ref SetPolicyDescStr, ref lblNoofMonthsStr,
                                ref txtNofDaysExperienceStr, ref txtLeavePayDaysStr, ref lblTakenLeavePayDaysStr, ref lblEligibleLeavePayDaysStr, ref txtAbsentDaysStr);
                                //showProgressBar.Value = showProgressBar.Value + 10;
                                lblSettlementPolicy = SetPolicyDescStr;
                                lblNoofMonths = lblNoofMonthsStr;
                                txtNofDaysExperience = Math.Abs(txtNofDaysExperienceStr.ToDecimal()).ToString();
                                txtLeavePayDays = txtLeavePayDaysStr;
                                lblEligibleLeavePayDays = lblEligibleLeavePayDaysStr;
                                lblTakenLeavePayDays = lblTakenLeavePayDaysStr;
                                lblEligibleLeavePayDays = lblEligibleLeavePayDaysStr;
                                txtAbsentDays = txtAbsentDaysStr;
                                //showProgressBar.Value = 50;
                                GetEmployeeLeaveHolidays(EmployeeID, dojng);
                                //showProgressBar.Value = 60;
                            }
         
            }
            catch { }
        }
        private void FillLeaveTypeParameters(int EmployeeID)
        {
            try
            {
                MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOLeaveTypeDetail = new System.Collections.Generic.List<clsDTOLeaveTypeDetail>();

                DataTable DTT;
                DTT = MobjclsBLLSettlementProcess.SettlementLeave(EmployeeID);

                for (int L = 0; L <= DTT.Rows.Count - 1; L++)
                {
                    /*GrdLeaveType.RowCount = GrdLeaveType.RowCount + 1;

                    if (DTT.Rows[L]["Sel"].ToInt32() == 1)
                        GrdLeaveType.Rows[L].Cells["Select"].Value = true;

                    objclsDTOLeaveType.LeaveTypeID = DTT.Rows[L]["LeaveTypeID"].ToInt32();
                    clsDTOLeaveTypeDetail objclsDTOLeaveType = DTT.Rows[L]["LeaveType"].ToString();
                    GrdLeaveType.Rows[L].Cells["Days"].Value = DTT.Rows[L]["NoOfDays"].ToString();*/

                    clsDTOLeaveTypeDetail objclsDTOLeaveType = new clsDTOLeaveTypeDetail();
                    objclsDTOLeaveType.LeaveTypeID = Convert.ToString(DTT.Rows[L]["LeaveTypeID"].ToInt32());
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOLeaveTypeDetail.Add(objclsDTOLeaveType);
                }
                
                /* GrdLeaveType.EndEdit();

                for (int i = 0; Convert.ToInt32(GrdLeaveType.Rows.Count) - 1 >= i; ++i)
                {
                    if (GrdLeaveType.Rows[i].Cells["Select"].Value.ToBoolean())
                    {
                        clsDTOLeaveTypeDetail objclsDTOLeaveType = new clsDTOLeaveTypeDetail();
                        objclsDTOLeaveType.LeaveTypeID = Convert.ToString(GrdLeaveType.Rows[i].Cells["LeaveTypeID"].Value);
                        MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOLeaveTypeDetail.Add(objclsDTOLeaveType);
                    }
                }*/
            }
            catch
            { }
        }
            private void GetEmployeeLeaveHolidays(int EmployeeID,string doj)
        {
            FillLeaveTypeParameters(EmployeeID);


            DataTable datTemp = MobjclsBLLSettlementProcess.GetEmployeeLeaveHolidays(EmployeeID, doj, DtpLastWorkingDate.Value.ToString());
            string Str = "";

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (datTemp.Rows[1]["Days"].ToDecimal() > 0) // Holidays Days
                    {
                        if (datTemp.Rows[1]["Days"].ToString().Contains("."))
                        {
                            if ((datTemp.Rows[1]["Days"].ToString().Split('.').Last()).ToDecimal() > 0)
                                txtExcludeHolidays = datTemp.Rows[1]["Days"].ToString();
                            else
                                txtExcludeHolidays = datTemp.Rows[1]["Days"].ToString().Split('.').First();
                        }
                        else
                            txtExcludeHolidays = datTemp.Rows[1]["Days"].ToString();
                    }
                    else
                    {
                        if (ClsCommonSettings.IsArabicView)
                            txtExcludeHolidays = "لا شيء";
                        else
                            txtExcludeHolidays = "NIL";                        
                    }

                    calculateEligibleDays();
                }
            }
        }
            private void calculateEligibleDays()
            {
                txtEligibleDays = txtNofDaysExperience;
                decimal DecLeaveDays;
                DecLeaveDays = 0;

                 MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOLeaveTypeDetail = new System.Collections.Generic.List<clsDTOLeaveTypeDetail>();
 /*
                DataTable DTT;
                DTT = MobjclsBLLSettlementProcess.SettlementLeave(EmployeeID);

                for (int L = 0; L <= DTT.Rows.Count - 1; L++)
                {
                    clsDTOLeaveTypeDetail objclsDTOLeaveType = new clsDTOLeaveTypeDetail();
                    objclsDTOLeaveType.LeaveTypeID = Convert.ToString(DTT.Rows[L]["Days"].Value.ToDecimal());
                    MobjclsBLLSettlementProcess.clsDTOSettlementProcess.lstclsDTOLeaveTypeDetail.Add(objclsDTOLeaveType);
                   
                      for (int i = 0; Convert.ToInt32(GrdLeaveType.Rows.Count) - 1 >= i; ++i)
                      {
                          if (GrdLeaveType.Rows[i].Cells["Select"].Value.ToBoolean())
                          {
                              DecLeaveDays = DecLeaveDays + GrdLeaveType.Rows[i].Cells["Days"].Value.ToDecimal();
                          }
                      }

                      txtExcludeLeaveDays.Text = DecLeaveDays.ToString();

                      if (DecLeaveDays > 0)
                          txtEligibleDays.Text = (txtEligibleDays.Text.ToDecimal() - DecLeaveDays).ToString();

                      if (chkExcludeHolidays.Checked)
                      {
                          if (txtExcludeHolidays.Text.ToDecimal() > 0)
                              txtEligibleDays.Text = (txtEligibleDays.Text.ToDecimal() - txtExcludeHolidays.Text.ToDecimal()).ToString();
                      }
                }*/
            }
     /*   private bool SaveDetails()
        {
            
            try
            {
                if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed == true)
                    return false;
                 if (MbUpdatePermission == false)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 17, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                Application.DoEvents();
                bProcess = true;

                if (lblNoofMonths == "")
                {
                    return false; 
                }
                int EmployeeID = 0;

                for (int II = 0; II <= dgvSettlementDetails.RowCount - 1; II++)
                {
                    if (dgvSettlementDetails.Rows[II].Cells["SelectAll"].Value.ToBoolean() == true &&
                    dgvSettlementDetails.Rows[II].Cells["Status"].Value.ToBoolean() == false)
                    {
                        EmployeeID = dgvSettlementDetails.Rows[II].Cells["EmployeeID"].Value.ToInt32();
                        if (EmployeeID != 0)
                        {



                            if (MbAddStatus == false)
                            {
                                if (Convert.ToInt32(LblEmployee.Tag) > 0)
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 668, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                    TmrEmployeeSettlement.Enabled = true;
                                    return;
                                }
                            }

                            if (cboTransactionType.SelectedValue.ToInt32() <= 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 681, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                LblEmployeeSettlement.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1).Trim();
                                TmrEmployeeSettlement.Enabled = true;
                                cboTransactionType.Focus();
                                return;
                            }


                            BtnSave.Enabled = OKSaveButton.Enabled = EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbUpdatePermission;
                            //calculateEligibleDays();
                            Fillparameters(0);

                            if (MobjclsBLLSettlementProcess.IsTransferDateGreater() == 1)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 672, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ErrEmployeeSettlement.SetError(DtpLastWorkingDate, MsMessageCommon.Replace("#", "").Trim());
                                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                TmrEmployeeSettlement.Enabled = true;
                                DtpLastWorkingDate.Focus();
                                return;
                            }

                            if (MobjclsBLLSettlementProcess.IsDateOfJoinGreater() == 1)
                            {
                                if (DtpLastWorkingDate.Value.Date < Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.dateofjoining))
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ErrEmployeeSettlement.SetError(DtpLastWorkingDate, MsMessageCommon.Replace("#", "").Trim());
                                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                    TmrEmployeeSettlement.Enabled = true;
                                    DtpLastWorkingDate.Focus();
                                    return;
                                }

                                if (DtpLastWorkingDate.Value.Date < Convert.ToDateTime(MobjclsBLLSettlementProcess.clsDTOSettlementProcess.dateofjoining))
                                {
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3081, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ErrEmployeeSettlement.SetError(DtpLastWorkingDate, MsMessageCommon.Replace("#", "").Trim());
                                    LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                    TmrEmployeeSettlement.Enabled = true;
                                    DtpLastWorkingDate.Focus();
                                    return;
                                }
                            }

                            if (MobjclsBLLSettlementProcess.IsEmployeeSettled() == 1)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 651, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            if (CboType.SelectedIndex == -1)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 656, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ErrEmployeeSettlement.SetError(CboType, MstrMessageCommon);
                                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                                CboEmployee.Focus();
                                TmrEmployeeSettlement.Enabled = true;
                                return;
                            }
                            else
                                ErrEmployeeSettlement.SetError(CboType, "");

                            if (CboType.SelectedValue.ToInt32() == 3)
                            {
                                if (dtpChequeDate.Value < System.DateTime.Now.Date)// Validation for previous cheque date.
                                {
                                    int intDiffDays;
                                    intDiffDays = Convert.ToInt32(System.DateTime.Now.Date.DayOfYear - dtpChequeDate.Value.Date.DayOfYear);
                                    int intYearDiff = System.DateTime.Now.Year - dtpChequeDate.Value.Year;

                                    if (System.DateTime.Now.Year != dtpChequeDate.Value.Year)
                                        if (System.DateTime.Now.Year > dtpChequeDate.Value.Year)
                                            intDiffDays = 365 * intYearDiff;

                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3161, out MmessageIcon);
                                    MstrMessageCommon = MstrMessageCommon.Replace("*", Convert.ToString(intDiffDays));
                                    TmrEmployeeSettlement.Enabled = true;

                                    if (MessageBox.Show(MstrMessageCommon.Replace("#", ""), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                                        return;
                                }
                            }

                            if (ClsCommonSettings.IsHrPowerEnabled)
                            {
                                if (MbAddStatus == true)
                                {
                                    int RetVal = IsValidSet(CboType.SelectedValue.ToInt32(), EmployeeID.ToInt32());
                                    if (RetVal == 0)
                                    {
                                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 20013, out MmessageIcon);
                                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1).Trim();
                                        TmrEmployeeSettlement.Enabled = true;
                                        return;
                                    }
                                }
                            }

                            if (DtpLastWorkingDate.Value.Date < DtpFromDate.Value.Date)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 19, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                LblEmployeeSettlement.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                                TmrEmployeeSettlement.Enabled = true;
                                DtpFromDate.Focus();
                                return;
                            }

                            if (MobjclsBLLSettlementProcess.IsPaymentReleaseCheckingPrevious() == 1)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 674, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                BtnProcess.Focus();
                                return;
                            }

                            if (MobjclsBLLSettlementProcess.IsPaymentReleaseChecking() == 1)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 678, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                DtpLastWorkingDate.Focus();
                                return;
                            }

                            if (MobjclsBLLSettlementProcess.IsAttendanceExisting() == 1)
                                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 685, out MmessageIcon);

                            Fillparameters(0);
                            //MobjclsBLLSettlementProcess.ProcessSettlementBegin();
                            FillProcessDetails();
                            BtnCancel.Enabled = false;

                            if (EmployeeSettlementDetailDataGridView.RowCount >= 0)
                                EmployeeSettlementDetailDataGridView.ClearSelection();

                            if ((TotalEarningSettlementAmt() - TotalDedSettlementAmt()) < 0)
                                MsMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 657, out MmessageIcon);
                            else
                                lblPayBack.Text = "";
                        }
                    }
                }
            
                bProcess = true;
                bClickProcess = true;
                EmployeeSettlementBindingNavigatorSaveItem_Click(sender, e);
                EmployeeSettlementBindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnProcess.Enabled = false;
                //MobjclsBLLSettlementProcess.ProcessSettlementCommit();
            }
            catch
            {
                //MobjclsBLLSettlementProcess.ProcessSettlementRollBack();
            
            }

            /*
             try
            {
                int ParaPaymentClassificationID = 0;
                int TypeIndex = 0;
                string ParaProcessDateFrom = "";
                string ParaProcessDateTo = "";

               
                
                TypeIndex = 1;
                if (datEmployeeSalaryDetails.Rows.Count > 0)
                {
                    lblStatus.Text = "Settlement Processing Initiated";
                    tmrSalryProcessRelease.Enabled = true;

                    tsProgressBar.Visible = true;
                    tsProgressBar.Minimum = 0;
                    tsProgressBar.Maximum = 100;

                    for (int i = 0; i <= datEmployeeSalaryDetails.Rows.Count - 1; i++)
                    {
                        /* mobjclsBLLSalaryProcessAndRelease.SalaryProcess(Convert.ToInt32(datEmployeeSalaryDetails.Rows[i]["EmployeeID"].ToString()), 
                             Convert.ToInt32(datEmployeeSalaryDetails.Rows[i]["CompanyID"].ToString()), ParaProcessDateFrom, ParaProcessDateTo, 
                             chkPartial.Checked, TypeIndex); 
                        tsProgressBar.Value = Convert.ToInt32((i * 100) / datEmployeeSalaryDetails.Rows.Count); 
                    }

                    tsProgressBar.Value = 100;
                    tsProgressBar.Visible = false;
                    tsProgressBar.Value = 0;

                    lblStatus.Text = "Settlement Processed Successfully";

                    MessageBox.Show(lblStatus.Text, MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information); 

                    tmrSalryProcessRelease.Enabled = true;
            
                }
               
                    if (MblnAddPermission == true || MblnPrintEmailPermission == true || MblnUpdatePermission == true || MblnDeletePermission == true)
                    {
                        btnProcess.Enabled = true;
                    }
                    else
                    {
                        btnProcess.Enabled = false;
                    }
                
             

                return true;
            }
            catch
            {
                return false;
            }
              
        }*/

        
        

       
        private void dgvSalaryDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                dgvSettlementDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch
            {
            }
        }

        /*private void dgvSalaryDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (e.ColumnIndex >= 0)
                    {
                       
                            if (dgvSettlementDetails.Rows[e.RowIndex].Cells["Reason"].Value.ToString() != "" &&
                                dgvSettlementDetails.Rows[e.RowIndex].Cells["Released"].Value.ToString() != "1")
                                lblStatus.Text = "Not able to process this Employee. Reason : " +
                                    dgvSettlementDetails.Rows[e.RowIndex].Cells["Reason"].Value.ToString();
                            else if (dgvSettlementDetails.Rows[e.RowIndex].Cells["Reason"].Value.ToString() == "" &&
                                dgvSettlementDetails.Rows[e.RowIndex].Cells["Released"].Value.ToString() == "1")
                                lblStatus.Text = "This Month Salary of this Employee is Processed and Released";
                            else
                            {
                                lblStatus.Text = "";

                                for (int iCounter = 0; iCounter <= datTempSalary.Rows.Count - 1; iCounter++)
                                {
                                    if (datTempSalary.Rows[iCounter]["EmployeeID"].ToInt32() == dgvSettlementDetails.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToInt32())
                                    {
                                        datTempSalary.Rows[iCounter]["SelectAll"] = true;
                                        break;
                                    }
                                }
                            }
                                         
                    }
                }
                //-------


                //TreeNode tr = new TreeNode();
                //tr = trvSettlementProcessGroup.SelectedNode;      
                //if (tr != null)
                //{
                //    trvSettlementProcessGroup.LabelEdit = true;
                //    tr.BeginEdit();
                //    trvSettlementProcessGroup.SelectedNode = tr;
                //}
            }
            catch
            {
            }

        }
        */
        /*
        private void dgvSalaryDetails_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                GridColumnColor();
            }
            catch
            {
            }
        }
        */
      /*
        private bool CheckReleased(int PaymentID)
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] {"PaymentID", "PayEmployeePayment", "" +
                "PaymentID = isnull(" + PaymentID + ",0) and isnull(Released,0) = 1"});

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        */

        private void btnProcess_Click(object sender, EventArgs e)
        {
            fillSalaryProcessAndReleaseTable();

           // showDetails();
                /*if (SaveDetails())
                {
                    btnShow_Click(sender, e); 
                }*/

          
        }

        private void fillSalaryProcessAndReleaseTable()
        {
            datEmployeeSalaryDetails.Rows.Clear();
            
            for (int i = 0; i <= datTempSalary.Rows.Count - 1; i++)
            {
                if (Convert.ToBoolean(datTempSalary.Rows[i]["SelectAll"].ToBoolean()))
                {
                    DataRow dr = datEmployeeSalaryDetails.NewRow();

                    if (datTempSalary.Columns.Contains("EmployeeID"))
                        dr["EmployeeID"] = datTempSalary.Rows[i]["EmployeeID"].ToString();

                    if (datTempSalary.Columns.Contains("CompanyID"))
                        dr["CompanyID"] = datTempSalary.Rows[i]["CompanyID"].ToString();

                    datEmployeeSalaryDetails.Rows.Add(dr);
                }
            }
        }
        private void GridSetUp()
        {
            try
            {
                FlgBlk = false;
                int I = 0;
                int IntScale = 0;

                if (dgvSettlementDetails.Columns.Count > 1)
                {
                    if (dgvSettlementDetails.Columns.Contains("SelectAll"))
                    {
                        dgvSettlementDetails.Columns["SelectAll"].Width = 30;
                        dgvSettlementDetails.Columns["SelectAll"].HeaderText = "";
                    }
                    if (dgvSettlementDetails.Columns.Contains("EmployeeName"))
                    {
                        dgvSettlementDetails.Columns["EmployeeName"].Width = 150;
                        dgvSettlementDetails.Columns["EmployeeName"].Frozen = true;
                    }

                    if (dgvSettlementDetails.Columns.Contains("CompanyName"))
                        dgvSettlementDetails.Columns["CompanyName"].Width = 175;
                                    
                    if (dgvSettlementDetails.Columns.Contains("VendorName"))
                        dgvSettlementDetails.Columns["VendorName"].Visible = false;

                    if (dgvSettlementDetails.Columns.Contains("PaymentMode"))
                        dgvSettlementDetails.Columns["PaymentMode"].Visible = false;                    

                   if (dgvSettlementDetails.Columns.Contains("TransactionType"))
                        dgvSettlementDetails.Columns["TransactionType"].Width = 125;

                   if (dgvSettlementDetails.Columns.Contains("TotalMonths"))
                        dgvSettlementDetails.Columns["TotalMonths"].Width = 125;

                    if (dgvSettlementDetails.Columns.Contains("TotalDays"))
                        dgvSettlementDetails.Columns["TotalDays"].Width = 125;

                    if (dgvSettlementDetails.Columns.Contains("EligibleDays"))
                        dgvSettlementDetails.Columns["EligibleDays"].Width = 125;

                    if (dgvSettlementDetails.Columns.Contains("NoticeDays"))
                        dgvSettlementDetails.Columns["NoticeDays"].Width = 125;

                    if (dgvSettlementDetails.Columns.Contains("BalanceDays"))
                        dgvSettlementDetails.Columns["BalanceDays"].Width = 125;
                    
                   // if (dgvSettlementDetails.Columns.Contains("PaymentDate"))
                    //    dgvSettlementDetails.Columns["PaymentDate"].Width = 125;

                   // if (dgvSettlementDetails.Columns.Contains("VendorID"))
                    //    dgvSettlementDetails.Columns["VendorID"].Visible = false;

                  //  if (dgvSettlementDetails.Columns.Contains("EmpVenFlag"))
                   //     dgvSettlementDetails.Columns["EmpVenFlag"].Visible = false;

                  //  if (dgvSettlementDetails.Columns.Contains("PaymentID"))
                   //     dgvSettlementDetails.Columns["PaymentID"].Visible = false;

                    if (dgvSettlementDetails.Columns.Contains("EmployeeID"))
                        dgvSettlementDetails.Columns["EmployeeID"].Visible = false;

                    if (dgvSettlementDetails.Columns.Contains("TransactionTypeID"))
                        dgvSettlementDetails.Columns["TransactionTypeID"].Visible = false;

                   if (dgvSettlementDetails.Columns.Contains("RecordID"))
                        dgvSettlementDetails.Columns["RecordID"].Visible = false;

                   if (dgvSettlementDetails.Columns.Contains("ProcessTypeID"))
                       dgvSettlementDetails.Columns["ProcessTypeID"].Visible = false;

                   if (dgvSettlementDetails.Columns.Contains("ProcessType"))
                       dgvSettlementDetails.Columns["ProcessType"].Visible = false;
                    
                    //dgvSalaryDetails.Columns[23].ReadOnly = true;

                    for (I = 0; I <= dgvSettlementDetails.ColumnCount - 1; I++)
                    {
                        dgvSettlementDetails.Columns[I].SortMode = DataGridViewColumnSortMode.NotSortable;

                        if (I != 0)
                        {
                            dgvSettlementDetails.Columns[I].ReadOnly = true;
                            dgvSettlementDetails.Columns[I].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                        }
                        else
                            dgvSettlementDetails.Columns[I].ReadOnly = false;
                    }
                }


                IntScale = 2;

                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos( new string[] {"isnull(Y.Scale,2) as Scale", "" +
                    "CurrencyReference AS Y INNER JOIN CompanyMaster AS C ON C.CurrencyId = Y.CurrencyID", "C.CompanyID = " + PCompanyID + ""});

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        IntScale = datTemp.Rows[0]["Scale"].ToInt32();
                }

                

                PointSelectAll(0); 
            }
            catch { }
        }

        private bool SetTempTableValue()
        {
            string ComputerName = GetHostName;
            string ProcessDate = "";
            int EmployeeID = 0;
            long PaymentID = 0;
            FlagSelect = false;

            mobjclsBLLSalaryProcessAndRelease.DeleteTempEmployeeIDForPaymentRelease(ComputerName);

            for (int II = 0; II <= dgvSettlementDetails.RowCount - 1; II++)
            {
                if (dgvSettlementDetails.Rows[II].Cells["PaymentID"].Value != null)
                {
                    if (dgvSettlementDetails.Rows[II].Cells["SelectAll"].Value.ToBoolean() == true &&
                        dgvSettlementDetails.Rows[II].Cells["Status"].Value.ToBoolean() == false)
                    {
                        PaymentID = dgvSettlementDetails.Rows[II].Cells["PaymentID"].Value.ToInt64();
                        ProcessDate = dtpResignationDate.Value.ToString(); //dgvSettlementDetails.Rows[II].Cells["ProcessDate"].Value.ToStringCustom();
                        EmployeeID = dgvSettlementDetails.Rows[II].Cells["EmployeeID"].Value.ToInt32();
                        FlagSelect = true;
                        mobjclsBLLSalaryProcessAndRelease.InsertTempEmployeeIDForPaymentRelease(EmployeeID, ProcessDate, ComputerName, PaymentID);
                    }
                }
            }

            if (FlagSelect)
                return true;
            else
                return false;
        }

       /* private bool CheckPaymentRelease()
        {
            string sPaymentIds = "";

            for (int i = 0; i<= dgvSettlementDetails.RowCount - 1; i++)
            {
                if (dgvSettlementDetails.Rows[i].Cells["PaymentID"].Value != null)
                {
                    if (dgvSettlementDetails.Rows[i].Cells["SelectAll"].Value.ToBoolean() == true &&
                        dgvSettlementDetails.Rows[i].Cells["Status"].Value.ToBoolean() == false)
                        sPaymentIds += dgvSettlementDetails.Rows[i].Cells["PaymentID"].Value.ToStringCustom() + ",";
                }
            }

            if (sPaymentIds != "")
            {
                sPaymentIds = sPaymentIds.Substring(0, sPaymentIds.Length - 1);
                DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] { "PaymentID", "PayEmployeePayment", "" +
                    "Released = 1 AND PaymentID IN (" + sPaymentIds + ")"});

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return false;
        }
        */
        /*
        private bool checkPaymentIDUse()
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.checkPaymentIDUse(GetHostName);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }
        */
        /*
        private bool IsPartialChk(long PaymentID)
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.CheckPaymentIsPartial(PaymentID);

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        */
        /*
        private void tmrSalryProcessRelease_Tick(object sender, EventArgs e)
        {
            tmrSalryProcessRelease.Enabled = false;
            lblStatus.Text = "";
        }

      */
        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (MViewPermission == false)
                    return;

                if ((dgvSettlementDetails.RowCount - 1) < 0)
                    return;

                if (dgvSettlementDetails.CurrentRow.Index < 0)
                    return;

                ClsWebform objClsWebform = new ClsWebform();
                using (FrmEmailPopup objemail = new FrmEmailPopup())
                {
                    objemail.MsSubject = "Payment";
                    objemail.EmailFormType = EmailFormID.SalaryPayment;
                    objemail.MiRecordID = Convert.ToInt32(dgvSettlementDetails.Rows[dgvSettlementDetails.CurrentRow.Index].Cells["PaymentID"].Value);
                    objemail.ShowDialog();
                }
            }
            catch
            {
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (MViewPermission == false)
                return;

            FillDate = FillDate.Replace("@", "").Trim();

            if (FillDate == "01 Jan 1900")
                return;

            if (FillDate.Trim().Length < 7)
                return;

            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = "Salary Release";
            ObjViewer.sDate = FillDate.ToString().Trim();
            ObjViewer.PiFormID = (int)FormID.SalaryRelease;
            ObjViewer.PintCompany = Convert.ToInt32(cboCompany.SelectedValue);
            ObjViewer.ShowDialog();
        }

        private void btnSalarySlip_Click(object sender, EventArgs e)
        {
            //Salary Slip Employee Currency
            string sPaymentID;
            int iStype = 0; 

            if (dgvSettlementDetails.RowCount == 0)
                return;

            int iRowIndex = dgvSettlementDetails.CurrentRow.Index;
            
            if (iRowIndex > -1)
            {
                FrmPayRoll obj = new FrmPayRoll();
                obj.PstrType = "salslip";
                obj.EmpVenFlag = "1";
                sPaymentID = "";

                obj.CompanyID = GetCompanyID(Convert.ToInt32(dgvSettlementDetails.CurrentRow.Cells["PaymentID"].Value));

                dgvSettlementDetails.Columns["PeriodFrom"].Width = 125;
                dgvSettlementDetails.Columns["PeriodTo"].Width = 125;

                mdtFrom = dgvSettlementDetails.Rows[iRowIndex].Cells["PeriodFrom"].Value.ToString();
                mDtTo = dgvSettlementDetails.Rows[iRowIndex].Cells["PeriodTo"].Value.ToString();
                SalaryType(iStype);

                String sYear = "";
                sYear = sYear + DateAndTime.Year(Convert.ToDateTime(dgvSettlementDetails.CurrentRow.Cells["PeriodFrom"].Value));
                String sMonth = "";

                sMonth = Strings.Format(Convert.ToDateTime(dgvSettlementDetails.CurrentRow.Cells["PeriodFrom"].Value), "MMMM");

                obj.PiSelRowCount = dgvSettlementDetails.SelectedRows.Count;
                obj.piYear = Convert.ToInt32(sYear);
                obj.psMonth = sMonth;
                mLLOP = "";
                mHLOP = "";

                sPaymentID = "";
                foreach (DataGridViewRow drPAyId in dgvSettlementDetails.SelectedRows)
                    sPaymentID = sPaymentID + drPAyId.Cells["PaymentID"].Value + ",";

                if (sPaymentID.Trim().Length > 0)
                {
                    if (Strings.Len(sPaymentID) > 5000)
                        sPaymentID = sPaymentID.Substring(0, 5000);

                    sPaymentID = sPaymentID.Substring(0, sPaymentID.LastIndexOf(","));
                    obj.CompanyPayFlag = 0;
                    obj.pPayID = sPaymentID;
                    obj.pLOP = mHLOP;
                    obj.pLblLOP = mLLOP;
                    obj.pMonthlyMonth = iStype;
                    obj.pSType = "";
                    obj.pFormName = "Pay Slip";
                    obj.ShowDialog();
                }
                obj.Dispose();
            }
        }

        private int GetCompanyID(int intPaymentID)
        {
            DataTable datTemp = mobjclsBLLSalaryProcessAndRelease.FillCombos(new string[] { "CompanyID", "PayEmployeePayment", "PaymentID=" + intPaymentID + ""});

            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    return Convert.ToInt32(datTemp.Rows[0]["CompanyID"]);
                else
                    return 0;
            }
            else
                return 0;
        }

        private void SalaryType(int sType)
        {
            string mLOP = "";
            switch (sType)
            {
                case 1:
                    mSType = "SALARY SLIP DAILY-";
                    break;
                case 2:
                    mSType = "WEEKLY-" + mdtFrom + "-" + mDtTo;
                    mLLOP = "LOP";
                    mHLOP = Convert.ToString(mLOP);
                    break;
                case 3:
                    mSType = "FORTNIGHT-" + mdtFrom + "-" + mDtTo;
                    mLLOP = "LOP";
                    mHLOP = Convert.ToString(mLOP);
                    break;
                case 4:
                    mSType = "PAYSLIP FOR THE MONTH OF " + Strings.UCase(Strings.Format(Convert.ToDateTime(dgvSettlementDetails.CurrentRow.Cells["PeriodFrom"].Value), "MMMM")) + " " + DateAndTime.Year(Convert.ToDateTime(dgvSettlementDetails.CurrentRow.Cells["PeriodFrom"].Value));
                    mLLOP = "LOP";
                    mHLOP = Convert.ToString(mLOP);
                    break;
            }
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (dgvSalaryDetails.Rows.Count < 1)
            //        return;

            //    if (trvSettlementProcessGroup.SelectedNode.Tag == null)
            //        return;

            //    if (cboFilterCompany.SelectedIndex == -1)
            //    {
            //        MstrCommonMessage = UserMessage.GetMessageByCode(9100);
            //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }



            //    if (cboFilterTransactionType.SelectedIndex == -1)
            //    {
            //        MstrCommonMessage = UserMessage.GetMessageByCode(1831);
            //        MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }

            //    using (FrmBankTransfer mobj = new FrmBankTransfer())
            //    {
            //        mobj.PiCompanyID = Convert.ToInt32(cboFilterCompany.SelectedValue);
            //        mobj.PstrCompanyName = cboFilterCompany.Text.ToString();
            //        //mobj.PiCurrencyID = Convert.ToInt32(cboFilterCurrency.SelectedValue);
            //        mobj.PstrType = "bankpayment";
            //        mobj.MachineName = GetHostName;
            //        mobj.pFormName = this.Text;
            //        //mobj.PiCurrencyIDFilter = Convert.ToInt32(cboFilterCurrency.SelectedValue);
            //        mobj.PdtProcessedDate = Convert.ToString(trvSettlementProcessGroup.SelectedNode.Text.Trim());
            //        mobj.ShowDialog();
            //    }
            //}
            //catch
            //{
            //}
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (dgvSettlementDetails.Rows.Count > 0)
            {
                for (int i = 0; i <= dgvSettlementDetails.Rows.Count - 1; i++)
                {
                    if (dgvSettlementDetails.Rows[i].ReadOnly == false)
                    {
                        dgvSettlementDetails.Rows[i].Cells["SelectAll"].Value = chkSelectAll.Checked;
                        datTempSalary.Rows[i]["SelectAll"] = chkSelectAll.Checked;
                    }
                }
            }
        }
   
        private void chkAdvanceSearch_CheckedChanged(object sender, EventArgs e)
        {
            getAdvanceSearch();
        }

        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            
            //dgvSalaryDetails.DataSource = null;
      
          
            expPnlSettlementProcess.Visible = true;
            this.Text = "Settlement Process " + this.Text.Replace("Settlement Confirm ", "");
            if (ClsCommonSettings.IsArabicView)
            {
                btnProcess.Text = "عملية";
            }
            else
            {
                btnProcess.Text = "&Process";
            }
            btnProcess.Image = Properties.Resources.Salary_processing;
       
            pnlLeft.Width = 250;

            dgvSettlementDetails.DataSource = getNewTableForProcess();
            datTempSalary = (DataTable)dgvSettlementDetails.DataSource;
            GridColumnDisabled();
            btnBack.Visible = false;
            btnShow_Click(sender, e);
            chkSelectAll.Checked = true;
            chkSelectAll.Checked = false;
        }

        private void btnFirstGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.First.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                showDetails();
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void btnPreviousGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.Previous.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                showDetails();
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void btnNextGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.Next.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                showDetails();
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void btnLastGridCollection_Click(object sender, EventArgs e)
        {
            try
            {
                FlgBlk = false;
                NavClicked = NavButton.Last.ToString();
                fillDataGrid_dtgBrowse();
                lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                showDetails();
                FlgBlk = true;
            }
            catch { }
            Cursor = Cursors.Default;
        }

        private void fillDataGrid_dtgBrowse()
        {
            try
            {
                DataTable dt = datTempSalary;
              
                if (dt == null)
                    return;

                DeterminePageBoundaries();
                int iGrdrowcount = (dt.Rows.Count);

                if (iGrdrowcount >= 24)
                    ItemPanelMiddileBottom.Visible = true;

                if (norecord == false)
                {
                    dgvSettlementDetails.Enabled = true;
                    dt.DefaultView.RowFilter = "RecordID >= " + RecordID1 + " and RecordID <= " + RecordID2;
                    dgvSettlementDetails.DataSource = dt.DefaultView.ToTable();
                    GridColumnDisabled();
                    lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
                }
              
                GridColumnColor();
            }
            catch { }
        }

        private void DeterminePageBoundaries()
        {
            try
            {
                DataTable datPages = datTempSalary;
                int TotalRowCount = datPages.Rows.Count;
                if (TotalRowCount == 0)
                {
                    //MsgBox("No records to Display.")
                    norecord = true;
                    //Exit Sub
                }
                else
                    norecord = false;
                //This is the maximum rows to display on a page. So we want paging to be implemented at 100 rows a page 

                int pages = 0;

                //if the rows per page are less than the total row count do the following: 
                if (pageRows < TotalRowCount)
                {
                    //if the Modulus returns > 0  there should be another page. 
                    if ((TotalRowCount % pageRows) > 0)
                        pages = ((TotalRowCount / pageRows) - ((TotalRowCount % pageRows) / pageRows) + 1);
                    else
                    {
                        //There is nothing left after the Modulus, so the pageRows divide exactly... 
                        //...into TotalRowCount leaving no rest, thus no extra page needs to be addd. 
                        pages = TotalRowCount / pageRows;
                    }
                }
                else
                {
                    //if the rows per page are more than the total row count, we will obviously only ever have 1 page 
                    pages = 1;
                }
                TotalPage = pages;

                //We now need to determine the LowerBoundary and UpperBoundary in order to correctly... 
                //...determine the Correct RecordID//s to use in the bindingSource1.Filter property. 
                int LowerBoundary = 0;
                int UpperBoundary = 0;

                //We now need to know what button was clicked, if any (First, Last, Next, Previous) 
                switch (NavClicked)
                {
                    case "First":
                        {
                            //First clicked, the Current Page will always be 1 
                            CurrentPage = 1;
                            //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            //if the rows per page are less than the total row count do the following: 
                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                            {
                                //if the rows per page are more than the total row count do the following: 
                                //There is only one page, so get the total row count as an UpperBoundary 
                                UpperBoundary = TotalRowCount;
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "Last":
                        {
                            //Last clicked, the CurrentPage will always be = to the variable pages 
                            CurrentPage = pages;
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));
                            //The UpperBoundary will always be the sum total of all the rows 
                            UpperBoundary = TotalRowCount;
                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "Next":
                        {
                            //Next clicked 
                            if (CurrentPage != pages)
                            {
                                //if we arent on the last page already, add another page 
                                CurrentPage += 1;
                            }

                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            if (CurrentPage == pages)
                            {
                                //if we are on the last page, the UpperBoundary will always be the sum total of all the rows 
                                UpperBoundary = TotalRowCount;
                            }
                            else
                            {
                                //else if we have a pageRow of 50 and we are on page 3, the UpperBoundary = 150 
                                UpperBoundary = (pageRows * CurrentPage);
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }

                            break;
                        }
                    case "Previous":
                        {
                            //Previous clicked 
                            if (CurrentPage != 1)
                            {
                                //if we aren//t on the first page already, subtract 1 from the CurrentPage 
                                CurrentPage -= 1;
                            }
                            //Get the LowerBoundary 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                                UpperBoundary = TotalRowCount;

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "1": //25
                    case "2": //50
                    case "3": //100
                        {
                            //First clicked, the Current Page will always be 1 
                            CurrentPage = 1;
                            //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            //if the rows per page are less than the total row count do the following: 
                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                            {
                                //if the rows per page are more than the total row count do the following: 
                                //There is only one page, so get the total row count as an UpperBoundary 
                                UpperBoundary = TotalRowCount;
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                    case "0": // All
                        {
                            //First clicked, the Current Page will always be 1 
                            CurrentPage = 1;
                            //The LowerBoundary will thus be ((50 * 1) - (50 - 1)) = 1 
                            LowerBoundary = 1;
                            UpperBoundary = TotalRowCount;

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }

                            break;
                        }
                    default:
                        {
                            //No button was clicked. 
                            LowerBoundary = ((pageRows * CurrentPage) - (pageRows - 1));

                            //if the rows per page are less than the total row count do the following: 
                            if (pageRows < TotalRowCount)
                                UpperBoundary = (pageRows * CurrentPage);
                            else
                            {
                                //if the rows per page are more than the total row count do the following: 
                                //Therefore there is only one page, so get the total row count as an UpperBoundary 
                                UpperBoundary = TotalRowCount;
                            }

                            //Now using these boundaries, get the appropriate RecordID//s (min & max)... 
                            //...for this specific page. 
                            //Remember, .NET is 0 based, so subtract 1 from both boundaries 
                            if (datPages.Rows.Count > 0)
                            {
                                RecordID1 = datPages.Rows[LowerBoundary - 1]["RecordID"].ToString();
                                RecordID2 = datPages.Rows[UpperBoundary - 1]["RecordID"].ToString();
                            }
                            break;
                        }
                }
            }
            catch { }
        }

        private void cboCountItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            FlgBlk = false;

            if (cboCountItem.SelectedIndex > 0)
                pageRows = Convert.ToInt32(cboCountItem.Text);
            else
                pageRows = datTempSalary.Rows.Count;

            NavClicked = cboCountItem.SelectedIndex.ToString();
            fillDataGrid_dtgBrowse();
            lblPageCount.Text = "Page " + CurrentPage + " of " + TotalPage;
            FlgBlk = true;
        }

      

      

       /* private void btnSalarySlipCompany_Click(object sender, EventArgs e)
        {

            string sPaymentID;
            int iStype = 0;

            if (dgvSettlementDetails.RowCount == 0)
                return;

            int iRowIndex = dgvSettlementDetails.CurrentRow.Index;

            if (iRowIndex > -1)
            {
                FrmPayRoll obj = new FrmPayRoll();
                obj.PstrType = "salslip";
                obj.EmpVenFlag = "1";
                sPaymentID = "";

                obj.CompanyID = GetCompanyID(Convert.ToInt32(dgvSettlementDetails.CurrentRow.Cells["PaymentID"].Value));

                dgvSettlementDetails.Columns["PeriodFrom"].Width = 125;
                dgvSettlementDetails.Columns["PeriodTo"].Width = 125;

                mdtFrom = dgvSettlementDetails.Rows[iRowIndex].Cells["PeriodFrom"].Value.ToString();
                mDtTo = dgvSettlementDetails.Rows[iRowIndex].Cells["PeriodTo"].Value.ToString();
                SalaryType(iStype);

                String sYear = "";
                sYear = sYear + DateAndTime.Year(Convert.ToDateTime(dgvSettlementDetails.CurrentRow.Cells["PeriodFrom"].Value));
                String sMonth = "";

                sMonth = Strings.Format(Convert.ToDateTime(dgvSettlementDetails.CurrentRow.Cells["PeriodFrom"].Value), "MMMM");

                obj.PiSelRowCount = dgvSettlementDetails.SelectedRows.Count;
                obj.piYear = Convert.ToInt32(sYear);
                obj.psMonth = sMonth;
                mLLOP = "";
                mHLOP = "";

                sPaymentID = "";
                foreach (DataGridViewRow drPAyId in dgvSettlementDetails.SelectedRows)
                    sPaymentID = sPaymentID + drPAyId.Cells["PaymentID"].Value + ",";

                if (sPaymentID.Trim().Length > 0)
                {
                    if (Strings.Len(sPaymentID) > 5000)
                        sPaymentID = sPaymentID.Substring(0, 5000);

                    sPaymentID = sPaymentID.Substring(0, sPaymentID.LastIndexOf(","));
                    obj.pPayID = sPaymentID;
                    obj.CompanyPayFlag = 1;
                    obj.pLOP = mHLOP;
                    obj.pLblLOP = mLLOP;
                    obj.pMonthlyMonth = iStype;
                    obj.pSType = "";
                    obj.pFormName = "Pay Slip";
                    obj.ShowDialog();
                }
                obj.Dispose();
            }



        }

   */

       

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();


            objHelp.strFormName = "Settlement process";

            objHelp.ShowDialog();
            objHelp = null;
        }

        private void FrmSettlementProcessGroup_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();


                        objHelp.strFormName = "Settlement process";
                      
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.P:
                        btnPrint_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.M:
                        btnEmail_Click(sender, new EventArgs());
                        break;
                }
            }
            catch (Exception)
            {
            }
        }


        

        private void dgvSalaryDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void expandableSplitter1_ExpandedChanged(object sender, DevComponents.DotNetBar.ExpandedChangeEventArgs e)
        {
            PointSelectAll(1);
        }

        private void FrmSettlementProcessGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void FrmSettlementProcessGroup_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void FrmSettlementProcessGroup_Deactivate(object sender, EventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void pnlSalaryReleaseBottom_Click(object sender, EventArgs e)
        {

        }

        private void ChangeStatus()
        {
           // LblEmployeeSettlement.Text = "";

            if (MbAddStatus == true)
                MbChangeStatus = MbAddPermission;
            else
                MbChangeStatus = MbUpdatePermission;

            try
            {
                if (MobjclsBLLSettlementProcess.clsDTOSettlementProcess.Closed == false)
                {
                   btnConfirm.Enabled = MbUpdatePermission;
                   btnProcess.Enabled = MbChangeStatus;
                   //EmployeeSettlementBindingNavigatorSaveItem.Enabled = MbChangeStatus;
                } 
            }
            catch { }
        }
        private void showDetails()
        {

            try
            {
                int miCurrencyIdt = 0; int MiCompanyIDt = 0; string LblCurrencyt = ""; string DtpFromDatet = ""; int TransactionTypeIDt = 0;
                showProgressBar.Visible = true;
                showProgressBar.Value = 10;
                

                showProgressBar.Refresh();
             
                    //lblPayBack.Text = "";
                    DateTime TempDateTime = new DateTime(1900, 1, 1, 0, 0, 0);
                    bLoad = true;
                    ChangeStatus();
                    int EmployeeID = 0;
                    int colIndex = 0, colIndex1 = 0, colIndex2 = 0, colIndex3 = 0, colIndex4 = 0;
                    /*if (!dgvSettlementDetails.Columns.Contains("Total Months"))
                    {
                        DataGridViewColumn col = new DataGridViewTextBoxColumn();
                        col.HeaderText = "Total Months";
                        colIndex = dgvSettlementDetails.Columns.Add(col);
                    }
                    if (!dgvSettlementDetails.Columns.Contains("Total Days"))
                    {
                        DataGridViewColumn col = new DataGridViewTextBoxColumn();
                        col.HeaderText = "Total Days";
                        colIndex1 = dgvSettlementDetails.Columns.Add(col);
                    }
                    if (!dgvSettlementDetails.Columns.Contains("Eligible Days"))
                    {
                        DataGridViewColumn col = new DataGridViewTextBoxColumn();
                        col.HeaderText = "Eligible Days";
                        colIndex2 = dgvSettlementDetails.Columns.Add(col);
                    }
                    if (!dgvSettlementDetails.Columns.Contains("Notice Days"))
                    {
                        DataGridViewColumn col = new DataGridViewTextBoxColumn();
                        col.HeaderText = "Notice Days";
                        colIndex3 = dgvSettlementDetails.Columns.Add(col);
                    }
                    if (!dgvSettlementDetails.Columns.Contains("Balance Days"))
                    {
                        DataGridViewColumn col = new DataGridViewTextBoxColumn();
                        col.HeaderText = "Balance Days";
                        colIndex4 = dgvSettlementDetails.Columns.Add(col);
                    }*/
                    for (int II = 0; II <= dgvSettlementDetails.RowCount - 1; II++)
                    {
                        Application.DoEvents();
                        //if (dgvSettlementDetails.Rows[II].Cells["SelectAll"].Value.ToBoolean() == true &&
                       // dgvSettlementDetails.Rows[II].Cells["Status"].Value.ToBoolean() == false)
                       // {
                            EmployeeID = dgvSettlementDetails.Rows[II].Cells["EmployeeID"].Value.ToInt32();
                            if (EmployeeID != 0)
                            {
                                if (MbAddStatus == true)
                                {
                                    if (EmployeeID > 0)
                                        MobjclsBLLSettlementProcess.GetSalaryReleaseDate(Convert.ToInt32(EmployeeID), ref TempDateTime);
                                }
                           // }

                            MobjclsBLLSettlementProcess.DisplayCurrencyEmployee(Convert.ToInt32(EmployeeID), ref  miCurrencyIdt, ref  MiCompanyIDt, ref  LblCurrencyt, ref  DtpFromDatet, ref TransactionTypeIDt);
                            //showProgressBar.Value = showProgressBar.Value + II;
                            miCurrencyId = miCurrencyIdt;
                            MiCompanyID = MiCompanyIDt;
                            //LblCurrency = LblCurrencyt;

                            if (DtpFromDatet != "")
                            {
                                //if (Convert.ToDateTime(DtpFromDatet) > Convert.ToDateTime("01-Jan-1900"))
                                //DtpFromDate.Value = DtpFromDatet.ToDateTime();
                            }

                            //FillExcludGrid();
                            //showProgressBar.Value = 40;
                            GetSettlementPolicyAndExperience(EmployeeID, dgvSettlementDetails.Rows[II].Cells["DateOfJoining"].Value.ToString());
                            //showProgressBar.Value = 70;
                           // CalculateLeavePayDays();
                            //showProgressBar.Value = 80;
                            // DateDiff();
                            // EmployeeHistoryDetail();
                            //showProgressBar.Value = 90;
                            //if (MbAddStatus == true)
                            //cboTransactionType.SelectedValue = TransactionTypeIDt;

                            //if (CboEmployee.SelectedIndex >= 0 && CboType.SelectedIndex >= 0 && MbAddStatus == true)
                            // BtnProcess.Enabled = MbAddPermission;
                        }
                        else if (EmployeeID > 0)
                        {
                            ///4 getting currencyid
                            MobjclsBLLSettlementProcess.DisplayCurrencyEmployee(Convert.ToInt32(EmployeeID), ref  miCurrencyIdt, ref  MiCompanyIDt, ref  LblCurrencyt, ref  DtpFromDatet, ref TransactionTypeIDt);
                            miCurrencyId = miCurrencyIdt;
                            MiCompanyID = MiCompanyIDt;
                           // LblCurrency = LblCurrencyt;
                        }

                        System.Threading.Thread.Sleep(10);

                        dgvSettlementDetails.Rows[II].Cells["TotalMonths"].Value = (lblNoofMonths.ToString());
                        dgvSettlementDetails.Rows[II].Cells["TotalMonths"].Value = dgvSettlementDetails.Rows[II].Cells["TotalMonths"].FormattedValue;
                        dgvSettlementDetails.Rows[II].Cells["TotalDays"].Value = txtNofDaysExperience;
                        dgvSettlementDetails.Rows[II].Cells["EligibleDays"].Value = txtEligibleDays;
                        //dgvSettlementDetails.Rows[II].Cells["NoticeDays"].Value = 0;
                        dgvSettlementDetails.Rows[II].Cells["BalanceDays"].Value = (txtLeavePayDays.ToString());
                        
                        if(II>0 && showProgressBar.Value<100)
                        showProgressBar.Value = showProgressBar.Value + (dgvSettlementDetails.RowCount/II);
                    }

                    showProgressBar.Value = 100;
                    Application.DoEvents();
                    showProgressBar.Visible = false;
                    
                }
            //}
            catch { }
            
             
        }

        private void CboType_SelectedIndexChanged(object sender, EventArgs e)
        {
           // ErrEmployeeSettlement.Clear();
   //         MbChangeStatus = true;
          lblToDate.Text = CboType.Text + (ClsCommonSettings.IsArabicView ? " تاريخ" : " Date");
        }

       

    }
}