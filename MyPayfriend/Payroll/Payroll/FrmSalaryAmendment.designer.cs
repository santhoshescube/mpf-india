﻿namespace MyPayfriend
{
    partial class FrmSalaryAmendment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalaryAmendment));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ProjectCreationStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.DeductionPolicyStatusStrip = new System.Windows.Forms.StatusStrip();
            this.cmsApplyToAll = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.errAddParicularsToAll = new System.Windows.Forms.ErrorProvider(this.components);
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.pnlRight = new DevComponents.DotNetBar.PanelEx();
            this.dgvEmployeeDetails = new System.Windows.Forms.DataGridView();
            this.ColEmployee = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColEffectiveDate = new DemoClsDataGridview.CalendarColumn();
            this.ColRepetitive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColParticular = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBottom = new DevComponents.DotNetBar.PanelEx();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlTop = new DevComponents.DotNetBar.PanelEx();
            this.btnExcelExport = new DevComponents.DotNetBar.ButtonX();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.NavSalary = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.btnImport = new System.Windows.Forms.ToolStripButton();
            this.cboApprovalStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LblRequestStaus = new System.Windows.Forms.Label();
            this.txtAmendmentCode = new System.Windows.Forms.TextBox();
            this.dtpDateValidate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.expSplitter = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlLeft = new DevComponents.DotNetBar.PanelEx();
            this.dgvSalaryStructureAmendment = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.SalaryStructureAmendmentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalaryStructureAmendmentCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expTop = new DevComponents.DotNetBar.ExpandablePanel();
            this.label2 = new System.Windows.Forms.Label();
            this.cboEmployee = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CboDesignation = new System.Windows.Forms.ComboBox();
            this.CboGrade = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CboBusinessUnit = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboDepartment = new System.Windows.Forms.ComboBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new DemoClsDataGridview.CalendarColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteSingleRowContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteThisRowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeductionPolicyStatusStrip.SuspendLayout();
            this.cmsApplyToAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errAddParicularsToAll)).BeginInit();
            this.pnlRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeDetails)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NavSalary)).BeginInit();
            this.NavSalary.SuspendLayout();
            this.pnlLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalaryStructureAmendment)).BeginInit();
            this.expTop.SuspendLayout();
            this.DeleteSingleRowContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProjectCreationStatusLabel
            // 
            this.ProjectCreationStatusLabel.Name = "ProjectCreationStatusLabel";
            this.ProjectCreationStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 17);
            this.lblStatus.Text = "Status";
            // 
            // DeductionPolicyStatusStrip
            // 
            this.DeductionPolicyStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.ProjectCreationStatusLabel});
            this.DeductionPolicyStatusStrip.Location = new System.Drawing.Point(0, 549);
            this.DeductionPolicyStatusStrip.Name = "DeductionPolicyStatusStrip";
            this.DeductionPolicyStatusStrip.Size = new System.Drawing.Size(1233, 22);
            this.DeductionPolicyStatusStrip.TabIndex = 1016;
            this.DeductionPolicyStatusStrip.Text = "StatusStrip1";
            // 
            // cmsApplyToAll
            // 
            this.cmsApplyToAll.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemCopy});
            this.cmsApplyToAll.Name = "CopyDetailsContextMenuStrip";
            this.cmsApplyToAll.Size = new System.Drawing.Size(140, 26);
            // 
            // ToolStripMenuItemCopy
            // 
            this.ToolStripMenuItemCopy.Name = "ToolStripMenuItemCopy";
            this.ToolStripMenuItemCopy.Size = new System.Drawing.Size(139, 22);
            this.ToolStripMenuItemCopy.Text = "Apply To All";
            // 
            // errAddParicularsToAll
            // 
            this.errAddParicularsToAll.ContainerControl = this;
            this.errAddParicularsToAll.RightToLeft = true;
            // 
            // pnlRight
            // 
            this.pnlRight.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlRight.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlRight.Controls.Add(this.dgvEmployeeDetails);
            this.pnlRight.Controls.Add(this.pnlBottom);
            this.pnlRight.Controls.Add(this.pnlTop);
            this.pnlRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRight.Location = new System.Drawing.Point(353, 0);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(880, 549);
            this.pnlRight.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlRight.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlRight.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlRight.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlRight.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlRight.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlRight.Style.GradientAngle = 90;
            this.pnlRight.TabIndex = 1019;
            this.pnlRight.Text = "panelEx1";
            // 
            // dgvEmployeeDetails
            // 
            this.dgvEmployeeDetails.AllowUserToResizeRows = false;
            this.dgvEmployeeDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmployeeDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployeeDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColEmployee,
            this.ColEffectiveDate,
            this.ColRepetitive,
            this.ColParticular,
            this.ColAmount,
            this.ColRemarks});
            this.dgvEmployeeDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEmployeeDetails.Location = new System.Drawing.Point(0, 75);
            this.dgvEmployeeDetails.Name = "dgvEmployeeDetails";
            this.dgvEmployeeDetails.RowHeadersWidth = 25;
            this.dgvEmployeeDetails.Size = new System.Drawing.Size(880, 416);
            this.dgvEmployeeDetails.TabIndex = 1026;
            this.dgvEmployeeDetails.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvEmployeeDetails_CellMouseClick);
            this.dgvEmployeeDetails.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvEmployeeDetails_CellBeginEdit);
            this.dgvEmployeeDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployeeDetails_CellEndEdit);
            this.dgvEmployeeDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvEmployeeDetails_EditingControlShowing);
            this.dgvEmployeeDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvEmployeeDetails_CurrentCellDirtyStateChanged);
            this.dgvEmployeeDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvEmployeeDetails_DataError);
            // 
            // ColEmployee
            // 
            this.ColEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColEmployee.DataPropertyName = "ColEmployee";
            this.ColEmployee.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ColEmployee.FillWeight = 50F;
            this.ColEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ColEmployee.HeaderText = "Employee";
            this.ColEmployee.MinimumWidth = 200;
            this.ColEmployee.Name = "ColEmployee";
            this.ColEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColEffectiveDate
            // 
            this.ColEffectiveDate.DataPropertyName = "ColEffectiveDate";
            this.ColEffectiveDate.HeaderText = "EffectiveDate";
            this.ColEffectiveDate.Name = "ColEffectiveDate";
            this.ColEffectiveDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColEffectiveDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColRepetitive
            // 
            this.ColRepetitive.DataPropertyName = "ColRepetitive";
            this.ColRepetitive.HeaderText = "Repetitive No";
            this.ColRepetitive.MinimumWidth = 80;
            this.ColRepetitive.Name = "ColRepetitive";
            this.ColRepetitive.Width = 80;
            // 
            // ColParticular
            // 
            this.ColParticular.DataPropertyName = "ColParticular";
            this.ColParticular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ColParticular.HeaderText = "Particular";
            this.ColParticular.MinimumWidth = 180;
            this.ColParticular.Name = "ColParticular";
            this.ColParticular.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColParticular.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColParticular.Width = 180;
            // 
            // ColAmount
            // 
            this.ColAmount.DataPropertyName = "ColAmount";
            this.ColAmount.HeaderText = "Value";
            this.ColAmount.MinimumWidth = 100;
            this.ColAmount.Name = "ColAmount";
            // 
            // ColRemarks
            // 
            this.ColRemarks.DataPropertyName = "ColRemarks";
            this.ColRemarks.HeaderText = "Remarks";
            this.ColRemarks.Name = "ColRemarks";
            this.ColRemarks.Width = 200;
            // 
            // pnlBottom
            // 
            this.pnlBottom.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlBottom.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnOk);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 491);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(880, 58);
            this.pnlBottom.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlBottom.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlBottom.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlBottom.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlBottom.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlBottom.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlBottom.Style.GradientAngle = 90;
            this.pnlBottom.TabIndex = 1058;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(19, 21);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1025;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(708, 21);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1023;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(789, 21);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1024;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pnlTop
            // 
            this.pnlTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlTop.Controls.Add(this.btnExcelExport);
            this.pnlTop.Controls.Add(this.txtFile);
            this.pnlTop.Controls.Add(this.NavSalary);
            this.pnlTop.Controls.Add(this.cboApprovalStatus);
            this.pnlTop.Controls.Add(this.LblRequestStaus);
            this.pnlTop.Controls.Add(this.txtAmendmentCode);
            this.pnlTop.Controls.Add(this.dtpDateValidate);
            this.pnlTop.Controls.Add(this.label6);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(880, 75);
            this.pnlTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlTop.Style.GradientAngle = 90;
            this.pnlTop.TabIndex = 1057;
            // 
            // btnExcelExport
            // 
            this.btnExcelExport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExcelExport.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnExcelExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcelExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExcelExport.Image")));
            this.btnExcelExport.ImageTextSpacing = 5;
            this.btnExcelExport.Location = new System.Drawing.Point(403, 43);
            this.btnExcelExport.Name = "btnExcelExport";
            this.btnExcelExport.Size = new System.Drawing.Size(16, 16);
            this.btnExcelExport.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnExcelExport.TabIndex = 1058;
            this.btnExcelExport.Tooltip = "Export to Excel";
            this.btnExcelExport.Click += new System.EventHandler(this.btnExcelExport_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(498, 32);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(46, 20);
            this.txtFile.TabIndex = 1057;
            this.txtFile.Visible = false;
            // 
            // NavSalary
            // 
            this.NavSalary.AddNewItem = null;
            this.NavSalary.BackColor = System.Drawing.Color.Transparent;
            this.NavSalary.CountItem = null;
            this.NavSalary.DeleteItem = null;
            this.NavSalary.Dock = System.Windows.Forms.DockStyle.None;
            this.NavSalary.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnClear,
            this.btnImport});
            this.NavSalary.Location = new System.Drawing.Point(0, 0);
            this.NavSalary.MoveFirstItem = null;
            this.NavSalary.MoveLastItem = null;
            this.NavSalary.MoveNextItem = null;
            this.NavSalary.MovePreviousItem = null;
            this.NavSalary.Name = "NavSalary";
            this.NavSalary.PositionItem = null;
            this.NavSalary.Size = new System.Drawing.Size(104, 25);
            this.NavSalary.TabIndex = 1030;
            this.NavSalary.Text = "BindingNavigator1";
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.ToolTipText = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.ToolTipText = "Delete";
            this.BindingNavigatorDeleteItem.Visible = false;
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnClear.Image = ((System.Drawing.Image)(resources.GetObject("BtnClear.Image")));
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(23, 22);
            this.BtnClear.ToolTipText = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // btnImport
            // 
            this.btnImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnImport.Image = global::MyPayfriend.Properties.Resources.Tiles_Icons;
            this.btnImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(23, 22);
            this.btnImport.Text = "Import";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // cboApprovalStatus
            // 
            this.cboApprovalStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboApprovalStatus.DisplayMember = "Text";
            this.cboApprovalStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboApprovalStatus.FormattingEnabled = true;
            this.cboApprovalStatus.ItemHeight = 14;
            this.cboApprovalStatus.Location = new System.Drawing.Point(636, 39);
            this.cboApprovalStatus.Name = "cboApprovalStatus";
            this.cboApprovalStatus.Size = new System.Drawing.Size(147, 20);
            this.cboApprovalStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboApprovalStatus.TabIndex = 1055;
            this.cboApprovalStatus.Visible = false;
            this.cboApprovalStatus.WatermarkText = "Status";
            // 
            // LblRequestStaus
            // 
            this.LblRequestStaus.AutoSize = true;
            this.LblRequestStaus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRequestStaus.Location = new System.Drawing.Point(425, 42);
            this.LblRequestStaus.Name = "LblRequestStaus";
            this.LblRequestStaus.Size = new System.Drawing.Size(0, 17);
            this.LblRequestStaus.TabIndex = 1056;
            // 
            // txtAmendmentCode
            // 
            this.txtAmendmentCode.BackColor = System.Drawing.SystemColors.Info;
            this.txtAmendmentCode.Location = new System.Drawing.Point(78, 39);
            this.txtAmendmentCode.MaxLength = 30;
            this.txtAmendmentCode.Name = "txtAmendmentCode";
            this.txtAmendmentCode.Size = new System.Drawing.Size(307, 20);
            this.txtAmendmentCode.TabIndex = 1027;
            this.txtAmendmentCode.TextChanged += new System.EventHandler(this.txtAmendmentCode_TextChanged);
            // 
            // dtpDateValidate
            // 
            this.dtpDateValidate.Location = new System.Drawing.Point(809, 28);
            this.dtpDateValidate.Name = "dtpDateValidate";
            this.dtpDateValidate.Size = new System.Drawing.Size(49, 20);
            this.dtpDateValidate.TabIndex = 1029;
            this.dtpDateValidate.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 1028;
            this.label6.Text = "Code";
            // 
            // expSplitter
            // 
            this.expSplitter.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitter.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitter.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expSplitter.ExpandableControl = this.pnlLeft;
            this.expSplitter.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitter.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitter.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSplitter.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSplitter.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSplitter.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSplitter.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expSplitter.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expSplitter.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(151)))), ((int)(((byte)(61)))));
            this.expSplitter.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(94)))));
            this.expSplitter.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expSplitter.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expSplitter.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitter.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitter.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSplitter.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSplitter.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.expSplitter.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSplitter.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.expSplitter.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expSplitter.Location = new System.Drawing.Point(348, 0);
            this.expSplitter.Name = "expSplitter";
            this.expSplitter.Size = new System.Drawing.Size(5, 549);
            this.expSplitter.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expSplitter.TabIndex = 1031;
            this.expSplitter.TabStop = false;
            // 
            // pnlLeft
            // 
            this.pnlLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLeft.Controls.Add(this.dgvSalaryStructureAmendment);
            this.pnlLeft.Controls.Add(this.expTop);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(348, 549);
            this.pnlLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeft.Style.GradientAngle = 90;
            this.pnlLeft.TabIndex = 1021;
            this.pnlLeft.Text = "panelEx1";
            // 
            // dgvSalaryStructureAmendment
            // 
            this.dgvSalaryStructureAmendment.AllowUserToAddRows = false;
            this.dgvSalaryStructureAmendment.AllowUserToDeleteRows = false;
            this.dgvSalaryStructureAmendment.AllowUserToResizeColumns = false;
            this.dgvSalaryStructureAmendment.AllowUserToResizeRows = false;
            this.dgvSalaryStructureAmendment.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvSalaryStructureAmendment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSalaryStructureAmendment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SalaryStructureAmendmentID,
            this.SalaryStructureAmendmentCode});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSalaryStructureAmendment.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSalaryStructureAmendment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSalaryStructureAmendment.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSalaryStructureAmendment.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvSalaryStructureAmendment.Location = new System.Drawing.Point(0, 202);
            this.dgvSalaryStructureAmendment.Name = "dgvSalaryStructureAmendment";
            this.dgvSalaryStructureAmendment.ReadOnly = true;
            this.dgvSalaryStructureAmendment.RowHeadersVisible = false;
            this.dgvSalaryStructureAmendment.Size = new System.Drawing.Size(348, 347);
            this.dgvSalaryStructureAmendment.TabIndex = 0;
            this.dgvSalaryStructureAmendment.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalaryStructureAmendment_CellClick);
            // 
            // SalaryStructureAmendmentID
            // 
            this.SalaryStructureAmendmentID.DataPropertyName = "SalaryStructureAmendmentID";
            this.SalaryStructureAmendmentID.HeaderText = "SalaryStructureAmendmentID";
            this.SalaryStructureAmendmentID.Name = "SalaryStructureAmendmentID";
            this.SalaryStructureAmendmentID.ReadOnly = true;
            this.SalaryStructureAmendmentID.Visible = false;
            // 
            // SalaryStructureAmendmentCode
            // 
            this.SalaryStructureAmendmentCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SalaryStructureAmendmentCode.DataPropertyName = "SalaryStructureAmendmentCode";
            this.SalaryStructureAmendmentCode.HeaderText = "Amendment Code";
            this.SalaryStructureAmendmentCode.Name = "SalaryStructureAmendmentCode";
            this.SalaryStructureAmendmentCode.ReadOnly = true;
            // 
            // expTop
            // 
            this.expTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.expTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expTop.Controls.Add(this.label2);
            this.expTop.Controls.Add(this.cboEmployee);
            this.expTop.Controls.Add(this.btnSearch);
            this.expTop.Controls.Add(this.label1);
            this.expTop.Controls.Add(this.CboDesignation);
            this.expTop.Controls.Add(this.CboGrade);
            this.expTop.Controls.Add(this.label5);
            this.expTop.Controls.Add(this.CboBusinessUnit);
            this.expTop.Controls.Add(this.label7);
            this.expTop.Controls.Add(this.label8);
            this.expTop.Controls.Add(this.cboDepartment);
            this.expTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expTop.Location = new System.Drawing.Point(0, 0);
            this.expTop.Name = "expTop";
            this.expTop.Size = new System.Drawing.Size(348, 202);
            this.expTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expTop.Style.GradientAngle = 90;
            this.expTop.TabIndex = 1022;
            this.expTop.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expTop.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expTop.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expTop.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expTop.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expTop.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expTop.TitleStyle.GradientAngle = 90;
            this.expTop.TitleText = " ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Employee";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.DropDownHeight = 70;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.Location = new System.Drawing.Point(91, 142);
            this.cboEmployee.MaxDropDownItems = 10;
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(237, 21);
            this.cboEmployee.TabIndex = 32;
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboEmployee_KeyDown);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(257, 170);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(71, 28);
            this.btnSearch.TabIndex = 24;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Grade";
            // 
            // CboDesignation
            // 
            this.CboDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboDesignation.DropDownHeight = 70;
            this.CboDesignation.FormattingEnabled = true;
            this.CboDesignation.IntegralHeight = false;
            this.CboDesignation.Location = new System.Drawing.Point(91, 61);
            this.CboDesignation.MaxDropDownItems = 10;
            this.CboDesignation.Name = "CboDesignation";
            this.CboDesignation.Size = new System.Drawing.Size(237, 21);
            this.CboDesignation.TabIndex = 30;
            this.CboDesignation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboDesignation_KeyDown);
            // 
            // CboGrade
            // 
            this.CboGrade.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboGrade.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboGrade.DropDownHeight = 70;
            this.CboGrade.FormattingEnabled = true;
            this.CboGrade.IntegralHeight = false;
            this.CboGrade.Location = new System.Drawing.Point(91, 34);
            this.CboGrade.MaxDropDownItems = 10;
            this.CboGrade.Name = "CboGrade";
            this.CboGrade.Size = new System.Drawing.Size(237, 21);
            this.CboGrade.TabIndex = 29;
            this.CboGrade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboGrade_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Designation";
            // 
            // CboBusinessUnit
            // 
            this.CboBusinessUnit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboBusinessUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboBusinessUnit.DropDownHeight = 70;
            this.CboBusinessUnit.FormattingEnabled = true;
            this.CboBusinessUnit.IntegralHeight = false;
            this.CboBusinessUnit.Location = new System.Drawing.Point(91, 88);
            this.CboBusinessUnit.MaxDropDownItems = 10;
            this.CboBusinessUnit.Name = "CboBusinessUnit";
            this.CboBusinessUnit.Size = new System.Drawing.Size(237, 21);
            this.CboBusinessUnit.TabIndex = 27;
            this.CboBusinessUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CboBusinessUnit_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 91);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Business Unit";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Department";
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.DropDownHeight = 70;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.IntegralHeight = false;
            this.cboDepartment.Location = new System.Drawing.Point(91, 115);
            this.cboDepartment.MaxDropDownItems = 10;
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(237, 21);
            this.cboDepartment.TabIndex = 23;
            this.cboDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboDepartment_KeyDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ColEmployeeNo";
            this.dataGridViewTextBoxColumn1.HeaderText = "EmployeeNumber";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // calendarColumn1
            // 
            this.calendarColumn1.DataPropertyName = "ColEffectiveDate";
            this.calendarColumn1.HeaderText = "EffectiveDate";
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ColRepetitive";
            this.dataGridViewTextBoxColumn2.HeaderText = "Repetitive No";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ColAmount";
            this.dataGridViewTextBoxColumn3.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "SalaryStructureAmendmentID";
            this.dataGridViewTextBoxColumn4.HeaderText = "SalaryStructureAmendmentID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "SalaryStructureAmendmentCode";
            this.dataGridViewTextBoxColumn5.HeaderText = "Amedment Code";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // DeleteSingleRowContextMenuStrip
            // 
            this.DeleteSingleRowContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteThisRowToolStripMenuItem});
            this.DeleteSingleRowContextMenuStrip.Name = "DeleteSingleRowContextMenuStrip";
            this.DeleteSingleRowContextMenuStrip.Size = new System.Drawing.Size(159, 26);
            // 
            // deleteThisRowToolStripMenuItem
            // 
            this.deleteThisRowToolStripMenuItem.Name = "deleteThisRowToolStripMenuItem";
            this.deleteThisRowToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deleteThisRowToolStripMenuItem.Text = "Delete This Row";
            this.deleteThisRowToolStripMenuItem.Click += new System.EventHandler(this.deleteThisRowToolStripMenuItem_Click);
            // 
            // FrmSalaryAmendment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1233, 571);
            this.Controls.Add(this.pnlRight);
            this.Controls.Add(this.expSplitter);
            this.Controls.Add(this.pnlLeft);
            this.Controls.Add(this.DeductionPolicyStatusStrip);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSalaryAmendment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Salary Amendment";
            this.Load += new System.EventHandler(this.FrmSalaryAmendment_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAddParicularsToAll_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAddParicularsToAll_KeyDown);
            this.DeductionPolicyStatusStrip.ResumeLayout(false);
            this.DeductionPolicyStatusStrip.PerformLayout();
            this.cmsApplyToAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errAddParicularsToAll)).EndInit();
            this.pnlRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeDetails)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NavSalary)).EndInit();
            this.NavSalary.ResumeLayout(false);
            this.NavSalary.PerformLayout();
            this.pnlLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalaryStructureAmendment)).EndInit();
            this.expTop.ResumeLayout(false);
            this.expTop.PerformLayout();
            this.DeleteSingleRowContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

   
        internal System.Windows.Forms.ToolStripStatusLabel ProjectCreationStatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.StatusStrip DeductionPolicyStatusStrip;
        internal System.Windows.Forms.ContextMenuStrip cmsApplyToAll;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemCopy;
        private System.Windows.Forms.ErrorProvider errAddParicularsToAll;
        private System.Windows.Forms.Timer tmrClear;
        private DevComponents.DotNetBar.PanelEx pnlRight;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpDateValidate;
        private System.Windows.Forms.TextBox txtAmendmentCode;
        private System.Windows.Forms.DataGridView dgvEmployeeDetails;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        private DevComponents.DotNetBar.PanelEx pnlLeft;
        private DevComponents.DotNetBar.ExpandableSplitter expSplitter;
        internal System.Windows.Forms.BindingNavigator NavSalary;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        private DevComponents.DotNetBar.ExpandablePanel expTop;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvSalaryStructureAmendment;
        private System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.ComboBox CboDesignation;
        internal System.Windows.Forms.ComboBox CboGrade;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.ComboBox CboBusinessUnit;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.ComboBox cboDepartment;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.ComboBox cboEmployee;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboApprovalStatus;
        private System.Windows.Forms.Label LblRequestStaus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DemoClsDataGridview.CalendarColumn calendarColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DevComponents.DotNetBar.PanelEx pnlBottom;
        private DevComponents.DotNetBar.PanelEx pnlTop;
        private System.Windows.Forms.ContextMenuStrip DeleteSingleRowContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteThisRowToolStripMenuItem;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColEmployee;
        private DemoClsDataGridview.CalendarColumn ColEffectiveDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRepetitive;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColParticular;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRemarks;
        private System.Windows.Forms.ToolStripButton btnImport;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalaryStructureAmendmentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalaryStructureAmendmentCode;
        private DevComponents.DotNetBar.ButtonX btnExcelExport;
    }
}