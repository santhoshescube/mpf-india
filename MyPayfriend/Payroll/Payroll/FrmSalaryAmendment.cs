﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.OleDb;
using System.Data.SqlClient;
using DemoClsDataGridview;
/*****************************************************
    * Created By       : Sachin K S
    * Creation Date    : 12-08-2013
    * Description      : To Add Addtion and Deduction Particulars 
    * ***************************************************/
namespace MyPayfriend
{
    public partial class FrmSalaryAmendment : DevComponents.DotNetBar.Office2007Form
    {
        #region Declarations

        public FrmMain objFrmMain;
        private string MstrMessageCommon;                      //  variable for assigning message
        private bool MblnIsEditMode = false;  //  To Find Whether Is add mode or Edit Mode
        clsBLLAddParicularsToAll MobjSalaryAmendment;
        ClsLogWriter MObjLogs;                                 //  Object for Class Clslogs
        ClsNotification mObjNotification;                      //  Object for Class ClsNotification
        clsBLLCommonUtility MobjclsBLLCommonUtility;           //  Object for Class clsBLLCommonUtility 
        ClsExportDatagridviewToExcel MobjExportToExcel;
        private int MintTimerInterval;                         // To set timer interval


        private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
        private bool MblnAddPermission = false;           //To set Add Permission
        private bool MblnUpdatePermission = false;      //To set Update Permission
        private bool MblnDeletePermission = false;      //To set Delete Permission
        private bool MblnAddUpdatePermission = false;   //To set Add Update Permission

        // Error Message display
        private ArrayList MaMessageArr;                 
        private ArrayList MaStatusMessage;
        private string MsMessageCaption;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        string strBindingOf = "Of ";
        private bool MblnChangeStatus = false; //To Specify Change Status
        int MintCompanyId;
        int MintUserId;
        public string strProcessDate;     //to get Salary Process Date
        public int intCompanyID;
        public int PintAmendmentCodeID = 0;
        public bool boolAddmode;
        private int MintRecordCnt = 0;     //Total Record Count
        private int MintCurrentRecCnt = 0; // Current Record Count
        public int intStatus = 0;
        #endregion
        public FrmSalaryAmendment()
        {
            //Constructorv 
            InitializeComponent();
            MobjSalaryAmendment = new clsBLLAddParicularsToAll();
            MObjLogs = new ClsLogWriter(Application.StartupPath);
            mObjNotification = new ClsNotification();
            MobjclsBLLCommonUtility = new clsBLLCommonUtility();
            MintCompanyId = ClsCommonSettings.CurrentCompanyID;
            MintUserId = ClsCommonSettings.UserID;
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MsMessageCaption = ClsCommonSettings.MessageCaption;
            MobjExportToExcel = new ClsExportDatagridviewToExcel();
            DataSet dtpAmendmentDetails;
            FillEmployeeDetails();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        #region Events
        private void FrmSalaryAmendment_Load(object sender, EventArgs e)
        {
            LoadMessage(); // Method for loading messages 
            LoadCombos(0); // Method for loading comboboxes 
            boolAddmode = false;// To block index changing on Load
            BindingNavigatorAddNewItem_Click(sender, e);
            //FillEmployeeDetails();
            //txtPercentage.Enabled = false;
            FillGrid();
            if (PintAmendmentCodeID > 0)
            {
                MblnIsEditMode = true;

                LoadCombos(8);
                expSplitter.Expanded = false;
                
                MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.AmendmentCodeID = PintAmendmentCodeID;
                FillEmployeeDetails();
                if (intStatus == 5 && txtAmendmentCode.Tag.ToInt32() > 0)
                {
                    cboApprovalStatus.Visible = false;
                    ChangeStatus();
                }
                else
                {
                    cboApprovalStatus.Visible = true;
                    ChangeStatus();
                }
                //btnSave.Enabled = btnOk.Enabled = true;
            }
            
            if (ClsCommonSettings.IsArabicView)
            {
                lblStatus.Text = "المبلغ في 'شركة العملات'";
                this.Text = "إضافة إضافات / الخصومات";
            }
            else
            {
                lblStatus.Text = "Amount in 'Company Currency'";
                this.Text = "Add Additions/Deductions ";
            }
        }


        private void BtnSave_Click(object sender, EventArgs e)
        {
            BindingNavigatorSaveItem_Click(sender, e);
        }


        private void dgvEmployeeDetails_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                MObjLogs.WriteLog("Error on dgvVacationDetails_CellBeginEdit " + this.Name + ex.Message.ToString(), 1);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveSalaryAmendment())
            {
                MblnChangeStatus = false;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }

        private void txtDecimal_KeyPress(object sender, KeyPressEventArgs e) // Key Press For TextDecimal
        {
            string strInvalidChars;

            strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[]";
            if (ClsCommonSettings.IsAmountRoundByZero)
            {
                strInvalidChars = strInvalidChars + ".";
            }

            e.Handled = false;
            if ((e.KeyChar == '.' && ((TextBox)sender).Text.Contains(".")))
            {
                e.Handled = true;
            }
            //else if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            //{
            //    e.Handled = true;
            //}
        }
        private void txtint_KeyPress(object sender, KeyPressEventArgs e) // Key Press For txtint_KeyPress
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            e.Handled = false;
            if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }


        private void dgvEmployeeDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvEmployeeDetails.IsCurrentCellDirty)
            {
                if (dgvEmployeeDetails.CurrentCell != null)
                    dgvEmployeeDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void FrmAddParicularsToAll_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:

                        break;
                    case Keys.Escape:
                        this.Close();
                        break;

                }
            }
            catch (Exception)
            {
            }
        }
        /// <summary>
        /// show confirmation message before closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmAddParicularsToAll_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus)
            {
                // Checking the changes are not saved and shows warning to the user
                MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        #endregion Events

        #region Methods
        #region LoadCombos
        /// <summary>
        /// Load the combos-AdditionDeductionReference,DepartmentReference
        /// </summary>
        /// <param name="intType">
        /// intType=0->Load all combos
        /// intType=1->Loads AdditionDeductionReference combo only
        /// intType=2->Loads DepartmentReference combo only
        /// </param>
        /// <returns>success/failure</returns>
        private void LoadCombos(int intType)
        {
            DataTable dataTable1 = new DataTable();
            if (intType == 0 || intType == 1)
            {
                DataTable dataTable2 = this.MobjSalaryAmendment.FillCombos(new string[3]
        {
          "AdditionDeductionID,AdditionDeduction + (CASE WHEN IsAddition = 1 THEN ' (+)' ELSE ' (-)' END) as Particular",
          "PayAdditionDeductionReference where (IsPredefined = 0  Or AdditionDeductionID in(1,26,6,10))",
          ""
        });
                ((DataGridViewComboBoxColumn)this.dgvEmployeeDetails.Columns["ColParticular"]).ValueMember = "AdditionDeductionID";
                ((DataGridViewComboBoxColumn)this.dgvEmployeeDetails.Columns["ColParticular"]).DisplayMember = "Particular";
                ((DataGridViewComboBoxColumn)this.dgvEmployeeDetails.Columns["ColParticular"]).DataSource = (object)dataTable2;
            }
            if (intType == 0 || intType == 2)
            {
                DataTable dataTable2 = this.MobjSalaryAmendment.FillCombos(new string[3]
        {
          "DepartmentID,Department",
          "DepartmentReference",
          ""
        });
                this.cboDepartment.ValueMember = "DepartmentID";
                this.cboDepartment.DisplayMember = "Department";
                DataRow row = dataTable2.NewRow();
                row["DepartmentID"] = (object)-1;
                row["Department"] = (object)"ALL";
                dataTable2.Rows.InsertAt(row, 0);
                this.cboDepartment.DataSource = (object)dataTable2;
            }
            if (intType == 0 || intType == 3)
            {
                DataTable dataTable2 = this.MobjSalaryAmendment.FillCombos(new string[3]
        {
          "DesignationID,Designation",
          "DesignationReference",
          ""
        });
                this.CboDesignation.ValueMember = "DesignationID";
                this.CboDesignation.DisplayMember = "Designation";
                DataRow row = dataTable2.NewRow();
                row["DesignationID"] = (object)-1;
                row["Designation"] = (object)"ALL";
                dataTable2.Rows.InsertAt(row, 0);
                this.CboDesignation.DataSource = (object)dataTable2;
            }
            if (intType == 0 || intType == 6)
            {
                DataTable dataTable2 = this.MobjSalaryAmendment.FillCombos(new string[3]
        {
          "EmployeeID,EmployeeFullName+ '(' +EmployeeNumber+')'  As Employee",
          "EmployeeMaster",
          ""
        });
                this.cboEmployee.ValueMember = "EmployeeID";
                this.cboEmployee.DisplayMember = "Employee";
                DataRow row = dataTable2.NewRow();
                row["EmployeeID"] = (object)-1;
                row["Employee"] = (object)"ALL";
                dataTable2.Rows.InsertAt(row, 0);
                this.cboEmployee.DataSource = (object)dataTable2;
            }
            if (intType == 0 || intType == 7)
            {
                DataTable dataTable2 = this.MobjSalaryAmendment.FillCombos(new string[3]
        {
          "EmployeeID,EmployeeNumber +'-'+ EmployeeFullName   As Employee,EmployeeNumber",
          "EmployeeMaster",
          ""
        });
                ((DataGridViewComboBoxColumn)this.dgvEmployeeDetails.Columns["ColEmployee"]).ValueMember = "EmployeeID";
                ((DataGridViewComboBoxColumn)this.dgvEmployeeDetails.Columns["ColEmployee"]).DisplayMember = "Employee";
                ((DataGridViewComboBoxColumn)this.dgvEmployeeDetails.Columns["ColEmployee"]).DataSource = (object)dataTable2;
            }
            if (intType == 0 || intType == 8)
            {
                DataTable dataTable2;
                if (intType == 0)
                    dataTable2 = this.MobjSalaryAmendment.FillCombos(new string[3]
          {
            "StatusId,Description",
            "HRRequestStatusReference",
            "StatusId <6 order by Description"
          });
                else
                    dataTable2 = this.MobjSalaryAmendment.FillCombos(new string[3]
          {
            "StatusId,Description",
            "HRRequestStatusReference",
            "StatusId in(3,4,5)order by Description"
          });
                this.cboApprovalStatus.ValueMember = "StatusId";
                this.cboApprovalStatus.DisplayMember = "Description";
                this.cboApprovalStatus.DataSource = (object)dataTable2;
                dataTable1 = (DataTable)null;
            }
            this.MblnChangeStatus = false;
        }
        #endregion LoadCombos

        #region LoadMessage

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.SalaryAmendment, this);
        }


        /// <summary>
        /// Method to fill the message array according to form
        /// </summary>
        private void LoadMessage()
        {
            // Loading Message
            try
            {
                MaMessageArr = new ArrayList();
                MaStatusMessage = new ArrayList();
                MaMessageArr = mObjNotification.FillMessageArray((int)FormID.SalaryAmendment, ClsCommonSettings.ProductID);
                MaStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.AddParicularsToAll, ClsCommonSettings.ProductID);
            }
            catch (Exception Ex)
            {
                MObjLogs.WriteLog("Error on dgvVacationDetails_CellBeginEdit " + this.Name + Ex.Message.ToString(), 1);
            }

        }
        #endregion LoadMessage

        #region FillEmployeeDetails
        /// <summary>
        /// Method to Fill Employee Payment Details
        /// </summary>
        private void FillEmployeeDetails()
        {
            try
            {

                DataSet dataSet = this.MobjSalaryAmendment.DisplaySalaryAmendment();
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    this.txtAmendmentCode.Tag = (object)dataSet.Tables[0].Rows[0][0].ToInt32();
                    this.txtAmendmentCode.Text = dataSet.Tables[0].Rows[0][1].ToString();
                    this.dgvEmployeeDetails.DataSource = (object)dataSet.Tables[1];
                    this.DefineGrid();
                    for (int index = 0; index <= this.dgvEmployeeDetails.Rows.Count - 2; ++index)
                    {
                        if (this.dgvEmployeeDetails.Rows[index].Cells[7].Value.ToInt32() == 1)
                        {
                            this.dgvEmployeeDetails.Rows[index].DefaultCellStyle.BackColor = Color.DimGray;
                            this.dgvEmployeeDetails.Rows[index].ReadOnly = true;
                        }
                    }
                }
                else
                {
                    this.txtAmendmentCode.Tag = (object)0;
                    this.txtAmendmentCode.Text = "";
                    this.LoadCombos(1);
                    this.LoadCombos(7);
                    this.dgvEmployeeDetails.DataSource = (object)dataSet.Tables[1];
                    this.DefineGrid();
                }
            }
            catch
            {
            }
        }
        #endregion FillEmployeeDetails

        #region SaveEmployeeDetails
        /// <summary>
        /// Method to save Employee Payment Details
        /// </summary>
        private bool SaveSalaryAmendment()
        {
            this.MsMessageCommon = this.mObjNotification.GetErrorMessage(this.MaMessageArr, (object)1, out this.MmessageIcon);
            if (MessageBox.Show(this.MsMessageCommon.Replace("#", "").Trim(), this.MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No || !this.SaveDetails())
                return false;
            this.FillEmployeeDetails();
            if (ClsCommonSettings.IsArabicView)
                this.lblStatus.Text = "حفظ بنجاح.";
            else
                this.lblStatus.Text = "Saved successfully.";
            this.FillGrid();
            this.btnOk.Enabled = this.btnSave.Enabled = this.BindingNavigatorDeleteItem.Enabled = this.BindingNavigatorSaveItem.Enabled = false;
            return true;
        }
        #endregion SaveEmployeeDetails

        #region SaveDetails
        /// <summary>
        /// Method to save Employee Payment Details and Validations 
        /// </summary>
        private bool SaveDetails()
        {
            if (!this.SalaryAmendmentvalidation())
                return false;

            this.MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.AmendmentCode = this.txtAmendmentCode.Text;
            this.MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.AmendmentCodeID = this.txtAmendmentCode.Tag.ToInt32();
            this.MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.StatusID = this.cboApprovalStatus.Enabled ? this.cboApprovalStatus.SelectedValue.ToInt32() : 1;
            this.FillDetails();
            return this.MobjSalaryAmendment.SaveSalaryAmendment();
        }

        private bool EmployeeDetailsvalidation()
        {
            throw new NotImplementedException();
        }
        #endregion SaveDetails
        #region SendForApproval
        private void SendMessages(bool blnEdit)
        {
             //public static void SendMessage(int? RequestedBy, int ReferenceID, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action, string Type)
            

        }
        #endregion endofApproval
        #region FillDetails
        /// <summary>
        /// Method to Fill Employee Payment Details 
        /// </summary>
        private void FillDetails()
        {
            this.MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.DTOSalaryAmendmentDetail = new List<clsDTOSalaryAmendmentDetail>();
            for (int index = 0; index < this.dgvEmployeeDetails.Rows.Count; ++index)
            {
                if (this.dgvEmployeeDetails.Rows[index].Cells["ColAmount"].Value.ToDecimal() > new Decimal(0) && this.IsValidDate(this.dgvEmployeeDetails.Rows[index].Cells["ColEffectiveDate"].Value.ToString()) && this.dgvEmployeeDetails.Rows[index].Cells["ColEffectiveDate"].Value.ToDateTime() > "01-Jan-2000".ToDateTime())
                {
                    clsDTOSalaryAmendmentDetail salaryAmendmentDetail = new clsDTOSalaryAmendmentDetail();
                    salaryAmendmentDetail.intEmployee = this.dgvEmployeeDetails.Rows[index].Cells["ColEmployee"].Value.ToInt32();
                    string source = this.dgvEmployeeDetails.Rows[index].Cells["ColEffectiveDate"].Value.ToString();
                    salaryAmendmentDetail.strEffectiveDate = source.ToDateTime().ToString("dd-MMM-yyyy");
                    salaryAmendmentDetail.intRepetitive = this.dgvEmployeeDetails.Rows[index].Cells["ColRepetitive"].Value.ToInt32();
                    salaryAmendmentDetail.intParticular = this.dgvEmployeeDetails.Rows[index].Cells["ColParticular"].Value.ToInt32();
                    salaryAmendmentDetail.intAmount = this.dgvEmployeeDetails.Rows[index].Cells["ColAmount"].Value.ToDecimal();
                    salaryAmendmentDetail.strRemarks = this.dgvEmployeeDetails.Rows[index].Cells["ColRemarks"].Value.ToString();
                    this.MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.DTOSalaryAmendmentDetail.Add(salaryAmendmentDetail);
                }
            }
        }
        #endregion FillDetails
        private bool IsValidDate(string Date)
        {

            try
            {

                if (Date != "")
                {
                    DateTime dt = DateTime.Parse(Date);
                    dtpDateValidate.Value = dt;
                    return true;
                }
                else
                {
                    return false;
                }
               
            }
            catch
            {
                return false;
            }

        }


        #region ChangeStatus
        /// <summary>
        /// Method to change buttons Enability
        /// </summary>
        private void ChangeStatus()
        {
          try
           {
            errAddParicularsToAll.Clear();
            if (ClsCommonSettings.IsArabicView)
            {
                lblStatus.Text = "المبلغ في 'شركة العملات'";
            }
            else
            {
                lblStatus.Text = "Amount in 'Company Currency'";
            }
            MblnChangeStatus = true;

            if (intStatus == 5 && txtAmendmentCode.Tag.ToInt32() > 0)
            {   
                btnOk.Enabled = btnSave.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorSaveItem.Enabled = false;
            }
            else
            {
                //txtAmendmentCode.Tag = 0;
                btnOk.Enabled = btnSave.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorSaveItem.Enabled = true;
            }

         }
            catch { }
        }
        #endregion ChangeStatus

        #region EmployeeDetailsvalidation
        /// <summary>
        /// Method to do Employee Details validation
        /// </summary>
        private bool SalaryAmendmentvalidation()
        {    
            try
            {
                errAddParicularsToAll.Clear();
                lblStatus.Text = "";
                //if (ClsCommonSettings.intEmployeeID.ToInt32() <= 0.ToInt32() || ClsCommonSettings.intEmployeeID.ToString()==null)
                //{
                //    MstrMessageCommon ="Employee Does not Exists For This User";
                //    //errAddParicularsToAll.SetError(txtAmendmentCode, MstrMessageCommon.Replace("#", "").Trim());
                //    MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    tmrClear.Enabled = true;
                //    return false;

                //}
                if (txtAmendmentCode.Text == "")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 21685, out MmessageIcon);
                    errAddParicularsToAll.SetError(txtAmendmentCode, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrClear.Enabled = true;
                    txtAmendmentCode.Focus();
                    return false;

                }
             
                else if (dgvEmployeeDetails.Rows.Count == 0)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 9992, out MmessageIcon);
                    errAddParicularsToAll.SetError(dgvEmployeeDetails, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrClear.Enabled = true;
                    dgvEmployeeDetails.Focus();
                    return false;
                }
                else if (MobjSalaryAmendment.Duplication(txtAmendmentCode.Tag.ToInt32(),txtAmendmentCode.Text.ToString()).ToBoolean())
                {
                    //MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 222, out MmessageIcon);
                    MstrMessageCommon = "Please check duplication of values.";
                    //errAddParicularsToAll.SetError(dgvEmployeeDetails, MstrMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrClear.Enabled = true;
                   txtAmendmentCode.Focus();
                    return false;
                }
               
                //DataTable dtTest = MobjSalaryAmendment.FillCombos(new string[] { "isnull(max(D.LevelID),0) as LevelID ", "HRRequestSettingsMaster M inner join  HRRequestSettingsDetail D ON M.RequestID=D.RequestID  where M.RequestTypeID=26 AND M.CompanyID=2","" });
                //if (dtTest.Rows[0]["LevelID"].ToInt32()== 0.ToInt32())
                //{
                //    //MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 222, out MmessageIcon);
                //    MstrMessageCommon = "Amendment Settings Are Not Set.";
                //    //errAddParicularsToAll.SetError(dgvEmployeeDetails, MstrMessageCommon.Replace("#", "").Trim());
                //    MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //    tmrClear.Enabled = true;
                //    return false;
                //}
                // else if(true) 
                //{
                    if (dgvEmployeeDetails.Rows.Count > 0)
                    {
                        string effDate = dgvEmployeeDetails.Rows[0].Cells["ColEffectiveDate"].Value.ToDateTime().ToString("dd-MMM-yyyy");
                        if (new DataLayer().ExecuteScalar("Select count(*) from PayEmployeePayment where datediff(day,PeriodTo,N'" + effDate + "')=0").ToInt32() > 0)
                        {
                            MstrMessageCommon = "Salary processed/released on " + effDate;
                            MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            tmrClear.Enabled = true;
                            return false;
                        }
                        int TempValue=0;
                        if (dgvEmployeeDetails.Rows.Count == 1)
                            TempValue = dgvEmployeeDetails.Rows.Count - 1;
                        else if (dgvEmployeeDetails.Rows.Count>1)
                            TempValue = dgvEmployeeDetails.Rows.Count -2;
                        for (int IIndex = 0; IIndex <=TempValue; IIndex++)
                        {
                            if (dgvEmployeeDetails.Rows[IIndex].Cells["ColAmount"].Value.ToDecimal() <= 0 || dgvEmployeeDetails.Rows[IIndex].Cells["ColRepetitive"].Value.ToInt32() < 1 ||
                                (IsValidDate(dgvEmployeeDetails.Rows[IIndex].Cells["ColEffectiveDate"].Value.ToString())).ToBoolean() == false || dgvEmployeeDetails.Rows[IIndex].Cells["ColEmployee"].Value.ToInt32() <= 0)
                            {
                                //MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 222, out MmessageIcon);
                                MstrMessageCommon = "Please Fill All Fields.";
                                //errAddParicularsToAll.SetError(dgvEmployeeDetails, MstrMessageCommon.Replace("#", "").Trim());
                                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                tmrClear.Enabled = true;
                                return false;
                            }
                            //else if (dgvEmployeeDetails.Rows[IIndex].Cells["ColEffectiveDate"].Value.ToDateTime() < "01-Jan-2000".ToDateTime())
                            //{
                            //    //MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 222, out MmessageIcon);
                            //    MstrMessageCommon = "Effective Date Cannot be Less than 01-Jan-2000 ";
                            //    //errAddParicularsToAll.SetError(dgvEmployeeDetails, MstrMessageCommon.Replace("#", "").Trim());
                            //    MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            //    lblStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            //    tmrClear.Enabled = true;
                            //    return false;

                            //}
                        }
                    }
                    return true;
                //}
              
                //else
                //    return true;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in EmployeeDetailsvalidation() " + ex.Message);
                MObjLogs.WriteLog("Error in EmployeeDetailsvalidation() " + ex.Message, 2);
                return false;
            }

        }
        #endregion EmployeeDetailsvalidation

        #region DeleteAmendment
        public bool DeleteAmendment()
        {
            bool DelValue = false; 
            if (txtAmendmentCode.Text != "")
            {
                MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.AmendmentCode = txtAmendmentCode.Text;
                MsMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                if (MobjSalaryAmendment.DeleteSalaryAmendment())
                {
                    DelValue = true;
                }
                
            }
            return DelValue;
        }
        #endregion DeleteAmendment

        #region Display
        private void DisplaySalaryAmendment() //To Display Detail Info
        {

            //FillSalaryAmendmentInfo();

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            MblnIsEditMode = true;
            MblnChangeStatus = false;
            BindingNavigatorSaveItem.Enabled = MblnChangeStatus;
            btnOk.Enabled = MblnChangeStatus;
            btnSave.Enabled = MblnChangeStatus;

            cboDepartment.SelectedIndex = CboDesignation.SelectedIndex = CboBusinessUnit.SelectedIndex = CboGrade.SelectedIndex = -1;

        }
        #endregion Display
        #region Clear
        private void Clear()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ColEmployee",typeof (Int64));
                dt.Columns.Add("ColEffectiveDate",typeof( DateTime));
                dt.Columns.Add("ColRepetitive",typeof (int));
                dt.Columns.Add("ColParticular",typeof(int));
                dt.Columns.Add("ColAmount",typeof(float));
                dt.Columns.Add("ColRemarks", typeof(string));
                txtAmendmentCode.Text = "";
                LoadCombos(7);
                //dgvEmployeeDetails.Rows.Clear();
                dgvEmployeeDetails.DataSource = dt;
                cboDepartment.SelectedIndex = -1;
                CboDesignation.SelectedIndex = -1;
                CboBusinessUnit.SelectedIndex = -1;
                CboGrade.SelectedIndex = -1;
                txtAmendmentCode.Tag = 0;
                LblRequestStaus.Text = "";
            }
            catch
            {
            }
        }
        #endregion Clear
        #region FillGrid
        public void FillGrid()
        {
            DataTable dataTable = this.MobjSalaryAmendment.FillGrid(this.CboDesignation.SelectedValue.ToInt32(), this.cboDepartment.SelectedValue.ToInt32(), this.cboEmployee.SelectedValue.ToInt32());
            if (dataTable.Rows.Count > 0)
            {
                this.dgvSalaryStructureAmendment.DataSource = (object)dataTable;
                this.Clear();
                this.DefineGrid();
                this.dgvSalaryStructureAmendment.ClearSelection();
            }
            else
            {
                this.dgvSalaryStructureAmendment.DataSource = (object)new DataTable()
                {
                    Columns = {
            {
              "SalaryStructureAmendmentID",
              typeof (int)
            },
            {
              "SalaryStructureAmendmentCode",
              typeof (string)
            }
          }
                };
                this.DefineGrid();
                this.Clear();
                this.dgvSalaryStructureAmendment.ClearSelection();
            }

        }
        #endregion FillGrid

        private void DefineGrid()
        {
            try
            {
                DataGridViewComboBoxColumn viewComboBoxColumn1 = new DataGridViewComboBoxColumn();
                CalendarColumn calendarColumn = new CalendarColumn();
                if (!this.dgvEmployeeDetails.Columns.Contains("ColEmployee"))
                {
                    this.dgvEmployeeDetails.Columns.Add((DataGridViewColumn)viewComboBoxColumn1);
                    viewComboBoxColumn1.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    viewComboBoxColumn1.DataPropertyName = "ColEmployee";
                    viewComboBoxColumn1.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                    viewComboBoxColumn1.FillWeight = 50f;
                    viewComboBoxColumn1.FlatStyle = FlatStyle.Flat;
                    viewComboBoxColumn1.HeaderText = "Employee";
                    viewComboBoxColumn1.MinimumWidth = 200;
                    viewComboBoxColumn1.Name = "ColEmployee";
                    viewComboBoxColumn1.Resizable = DataGridViewTriState.True;
                    viewComboBoxColumn1.SortMode = DataGridViewColumnSortMode.Automatic;
                }
                if (!this.dgvEmployeeDetails.Columns.Contains("ColEffectiveDate"))
                {
                    this.dgvEmployeeDetails.Columns.Add((DataGridViewColumn)calendarColumn);
                    calendarColumn.DataPropertyName = "ColEffectiveDate";
                    calendarColumn.HeaderText = "EffectiveDate";
                    calendarColumn.Name = "ColEffectiveDate";
                    calendarColumn.Resizable = DataGridViewTriState.True;
                    calendarColumn.SortMode = DataGridViewColumnSortMode.Automatic;
                }
                if (!this.dgvEmployeeDetails.Columns.Contains("ColRepetitive"))
                {
                    DataGridViewTextBoxColumn viewTextBoxColumn = new DataGridViewTextBoxColumn();
                    this.dgvEmployeeDetails.Columns.Add((DataGridViewColumn)viewTextBoxColumn);
                    viewTextBoxColumn.DataPropertyName = "ColRepetitive";
                    viewTextBoxColumn.HeaderText = "Repetitive No";
                    viewTextBoxColumn.MinimumWidth = 80;
                    viewTextBoxColumn.Name = "ColRepetitive";
                    viewTextBoxColumn.Width = 80;
                }
                if (!this.dgvEmployeeDetails.Columns.Contains("ColParticular"))
                {
                    DataGridViewComboBoxColumn viewComboBoxColumn2 = new DataGridViewComboBoxColumn();
                    this.dgvEmployeeDetails.Columns.Add((DataGridViewColumn)viewComboBoxColumn2);
                    viewComboBoxColumn2.DataPropertyName = "ColParticular";
                    viewComboBoxColumn2.FlatStyle = FlatStyle.Flat;
                    viewComboBoxColumn2.HeaderText = "Particular";
                    viewComboBoxColumn2.MinimumWidth = 180;
                    viewComboBoxColumn2.Name = "ColParticular";
                    viewComboBoxColumn2.Resizable = DataGridViewTriState.True;
                    viewComboBoxColumn2.SortMode = DataGridViewColumnSortMode.Automatic;
                    viewComboBoxColumn2.Width = 180;
                }
                if (!this.dgvEmployeeDetails.Columns.Contains("ColAmount"))
                {
                    DataGridViewTextBoxColumn viewTextBoxColumn = new DataGridViewTextBoxColumn();
                    this.dgvEmployeeDetails.Columns.Add((DataGridViewColumn)viewTextBoxColumn);
                    viewTextBoxColumn.DataPropertyName = "ColAmount";
                    viewTextBoxColumn.HeaderText = "Value";
                    viewTextBoxColumn.MinimumWidth = 100;
                    viewTextBoxColumn.Name = "ColAmount";
                }
                if (!this.dgvEmployeeDetails.Columns.Contains("ColRemarks"))
                {
                    DataGridViewTextBoxColumn viewTextBoxColumn = new DataGridViewTextBoxColumn();
                    viewTextBoxColumn.DataPropertyName = "ColRemarks";
                    viewTextBoxColumn.HeaderText = "Remarks";
                    viewTextBoxColumn.Name = "ColRemarks";
                    viewTextBoxColumn.Width = 200;
                }
                if (!this.dgvEmployeeDetails.Columns.Contains("Status"))
                {
                    DataGridViewTextBoxColumn viewTextBoxColumn = new DataGridViewTextBoxColumn();
                    viewTextBoxColumn.DataPropertyName = "Status";
                    viewTextBoxColumn.HeaderText = "Status";
                    viewTextBoxColumn.Name = "Status";
                    viewTextBoxColumn.Width = 200;
                    viewTextBoxColumn.Visible = false;
                }
                this.dgvEmployeeDetails.RowsDefaultCellStyle.BackColor = Color.Wheat;
                this.dgvEmployeeDetails.AlternatingRowsDefaultCellStyle.BackColor = Color.White;
                this.dgvEmployeeDetails.Columns["ColAmount"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                if (this.dgvSalaryStructureAmendment.Columns.Contains("SalaryStructureAmendmentID"))
                    this.dgvSalaryStructureAmendment.Columns["SalaryStructureAmendmentID"].Width = 0;
                if (this.dgvSalaryStructureAmendment.Columns.Contains("SalaryStructureAmendmentCode"))
                    this.dgvSalaryStructureAmendment.Columns["SalaryStructureAmendmentCode"].Width = 339;
                this.dgvEmployeeDetails.Columns["ColEffectiveDate"].DefaultCellStyle.Format = "dd-MMM-yyyy";
                this.dgvSalaryStructureAmendment.RowsDefaultCellStyle.BackColor = Color.Wheat;
                this.dgvSalaryStructureAmendment.AlternatingRowsDefaultCellStyle.BackColor = Color.White;
                this.dgvSalaryStructureAmendment.Columns["SalaryStructureAmendmentCode"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            }
            catch
            {
            }
        }

      
        #endregion Methods

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            try
            {
                boolAddmode = false;
                Clear();
                boolAddmode = true;
            }
            catch (Exception ex)
            {
            }
        }
        private void dgvEmployeeDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveSalaryAmendment())
                {
                    if (PintAmendmentCodeID > 0)
                        this.Close();
                    
                }
                lblStatus.Text = "Saved Successfully";
            }
            catch (Exception ex)
            {
            }
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            //if (DeleteAmendment())
            //{
            //    MstrMessageCommon = mObjNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Remove(MstrMessageCommon.IndexOf("#"), 1), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    boolAddmode = false;
            //    Clear();
            //    boolAddmode = true;
            //}
            //FillGrid();
       
        }

        private void CboGrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            try {
                if (boolAddmode==true)
                {
                FillEmployeeDetails();
                }
            }
            catch (Exception ex)
            { }
        }

        private void CboBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (boolAddmode == true)
                {
                    FillEmployeeDetails();
                }
            }
            catch (Exception ex)
            { }
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (boolAddmode == true)
                {
                    FillEmployeeDetails();
                }
            }
            catch (Exception ex)
            { }
        }

        private void CboDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (boolAddmode == true)
                {
                    FillEmployeeDetails();
                }
            }
            catch (Exception ex)
            { }
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            boolAddmode = false;
            Clear();
            MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.AmendmentCodeID = 0;
            FillEmployeeDetails();
            boolAddmode = true;

        }

        private void GetRecordCount()   // TO Get Record Count
        {
           
            //MintRecordCnt = MobjSalaryAmendment.GetRecordCountAmendment();
            
        }

       

        private void btnShow_Click(object sender, EventArgs e)
        {
            FillEmployeeDetails();
        }
      
      
    
        private void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void dgvSalaryStructureAmendment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvSalaryStructureAmendment.Rows.Count > 0)
                MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.AmendmentCodeID = dgvSalaryStructureAmendment.Rows[dgvSalaryStructureAmendment.CurrentCell.RowIndex].Cells["SalaryStructureAmendmentID"].Value.ToInt32();
            else
                MobjSalaryAmendment.PobjClsDTOAddParicularsToAll.AmendmentCodeID = 0;
            FillEmployeeDetails();
            ChangeStatus();
        }

        private void CboGrade_KeyDown(object sender, KeyEventArgs e)
        {
            CboGrade.DroppedDown = false;
        }

        private void CboDesignation_KeyDown(object sender, KeyEventArgs e)
        {
            CboDesignation.DroppedDown = false;
        }

        private void CboBusinessUnit_KeyDown(object sender, KeyEventArgs e)
        {
            CboBusinessUnit.DroppedDown = false;
        }

        private void cboDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            cboDepartment.DroppedDown = false;
        }

        private void cboEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void dgvEmployeeDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (dgvEmployeeDetails.CurrentCell != null)
                {
                    if (dgvEmployeeDetails.CurrentCell.ColumnIndex == ColEmployee.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                    else if (dgvEmployeeDetails.CurrentCell.ColumnIndex == 4)
                    {
                        this.dgvEmployeeDetails.EditingControl.KeyPress -= new KeyPressEventHandler(txtint_KeyPress);
                        this.dgvEmployeeDetails.EditingControl.KeyPress += new KeyPressEventHandler(txtDecimal_KeyPress);
                    }
                    else if (dgvEmployeeDetails.CurrentCell.ColumnIndex == 2)
                    {
                        this.dgvEmployeeDetails.EditingControl.KeyPress -= new KeyPressEventHandler(txtDecimal_KeyPress);
                        this.dgvEmployeeDetails.EditingControl.KeyPress += new KeyPressEventHandler(txtint_KeyPress);
                    }
                    else
                    {
                        this.dgvEmployeeDetails.EditingControl.KeyPress -= new KeyPressEventHandler(txtint_KeyPress);
                        this.dgvEmployeeDetails.EditingControl.KeyPress -= new KeyPressEventHandler(txtDecimal_KeyPress);
                    }

                }

            }
            catch
            {
            }
            

        }

        private void txtAmendmentCode_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dgvEmployeeDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ChangeStatus();
        }

        private void deleteThisRowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool bTempFlg = false;

                bTempFlg = MobjSalaryAmendment.DeleteSingleRow(dgvEmployeeDetails.CurrentRow.Cells["ColEmployee"].Value.ToInt32(), txtAmendmentCode.Tag.ToInt32(), dgvEmployeeDetails.CurrentRow.Cells["ColParticular"].Value.ToInt32());
                if (bTempFlg == true)
                {
                    dgvEmployeeDetails.Rows.Remove(dgvEmployeeDetails.CurrentRow);
                }
                else
                {
                    //MstrCommonMessage = UserMessage.GetMessageByCode(9017);
                    //MessageBox.Show(MstrCommonMessage.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
            catch { }
        }

        private void dgvEmployeeDetails_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

                if (e.Button == MouseButtons.Right)
                {
                    if (e.RowIndex >= 0)
                    {
                        this.DeleteSingleRowContextMenuStrip.Show(this.dgvEmployeeDetails, this.dgvEmployeeDetails.PointToClient(Cursor.Position));
                    }
                }

            }
            catch { }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog FileBrowser = new OpenFileDialog())
            {
                FileBrowser.Filter = "Excel files (.xls)|*.xls|(.xlsx)|*.xlsx";
                FileBrowser.ShowDialog();
                txtFile.Text = FileBrowser.FileName;
                GetDataFromExcel();
            }
        }

        private void GetDataFromExcel()
        {
            try
            {

//                public static string path = @"C:\src\RedirectApplication\RedirectApplication\301s.xlsx";
//public static string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";

                string connectionStringTemplate = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + txtFile.Text.Trim() + ";Extended Properties=Excel 12.0;";
                OleDbConnection MyConnection;
                OleDbDataAdapter MyCommand;
                DataTable worksheet;
                MyConnection = new OleDbConnection(connectionStringTemplate);
                string sSheetName = "Sheet1$"; // "SalaryAmendment$";
                string sqlSelect = "SELECT * FROM [" + sSheetName + "];";
                MyCommand = new OleDbDataAdapter("SELECT * FROM [" + sSheetName + "]", MyConnection);

                DataSet workbook = new DataSet();
                MyCommand.Fill(workbook, "dgvExcel");
                worksheet = workbook.Tables["dgvExcel"];

                int intBlank = 0;

                for (int j = 0; j <= worksheet.Columns.Count - 1; ++j)
                {
                    intBlank = 0;
                    for (int i = 0; i <= worksheet.Rows.Count - 1; ++i)
                    {
                        if (Convert.ToString(worksheet.Rows[i][j]) != "")
                        {
                            intBlank = 1;
                        }
                        if (intBlank == 1)
                        {
                            break;
                        }
                    }
                    if (intBlank == 0)
                    {
                        worksheet.Columns.RemoveAt(j);
                    }

                }

                DataTable DTe;
                int iRowIndex = 0;
                DataTable DTEmp; DataTable DTAddDed;

                DTEmp = MobjSalaryAmendment.GetAllEmployee();
                DTAddDed = MobjSalaryAmendment.GetAllAddDed();

                DTe = (DataTable)(dgvEmployeeDetails.DataSource);

                for (int i = 0; i < worksheet.Rows.Count; i++)//
                {
                    if (worksheet.Rows[i][5].ToStringCustom().ToDecimal() > 0 && IsValidDate((worksheet.Rows[i][2].ToStringCustom()).ToString()))
                    {
                        DataRow dtRow;
                        DataRow[] dtRowEmp;
                        DataRow[] dtRowAddDed;

                        dtRow = DTe.NewRow();

                        dtRowEmp = DTEmp.Select("EmployeeNumber='" + worksheet.Rows[i][0].ToStringCustom() + "'");
                        dtRowAddDed = DTAddDed.Select("AdditionDeduction='" + worksheet.Rows[i][4].ToStringCustom() + "'");

                        if (dtRowEmp.Any() && dtRowAddDed.Any())
                        {
                            if (dtRowEmp[0].Table.Rows.Count > 0 && dtRowAddDed[0].Table.Rows.Count > 0)
                            {
                                Int32 intEmp = dtRowEmp[0].ItemArray[0].ToInt32();
                                Int32 intAddDed = dtRowAddDed[0].ItemArray[0].ToInt32();
                                dtRow["ColEmployee"] = intEmp;
                                dtRow["ColEffectiveDate"] = worksheet.Rows[i][2].ToDateTime();
                                dtRow["ColRepetitive"] = worksheet.Rows[i][3].ToStringCustom();
                                dtRow["ColParticular"] = intAddDed;
                                dtRow["ColAmount"] = worksheet.Rows[i][5].ToStringCustom();
                                if (worksheet.Columns.Count > 6)
                                    dtRow["ColRemarks"] = worksheet.Rows[i][6].ToStringCustom();
                                else
                                    dtRow["ColRemarks"] = "";
                                DTe.Rows.InsertAt(dtRow, 0);
                            }
                        }
                    }
                }

                dgvEmployeeDetails.DataSource = null;
                DefineGrid();
                LoadCombos(0);
                dgvEmployeeDetails.DataSource = DTe;
            
        
                return;
            }
            catch
            {
            }
        }

        private void btnExcelExport_Click(object sender, EventArgs e)
        {
            string sFileName = "";

            if (dgvEmployeeDetails.RowCount <= 0)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";

                if (sfd.ShowDialog() == DialogResult.OK)
                    sFileName = sfd.FileName;
                else
                    return;
            }

            try
            {
                if (sFileName != "")
                {
                    if (dgvEmployeeDetails.RowCount > 0)
                        MobjExportToExcel.ExportToExcelForAmendment(dgvEmployeeDetails, sFileName, 0, dgvEmployeeDetails.Columns.Count - 1, true);
                }
                else
                    MessageBox.Show("Please Enter Name");
            }
            catch { }
        }



    }
}
