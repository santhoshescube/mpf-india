﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data.SqlClient;

/* 
=================================================
Author:		<Bijoy>
Create date: <Create Date,21 Sep 2013>
Description:	<Description,to send support email from client>
================================================
*/
namespace MyPayfriend
{
    public partial class FrmSupport : DevComponents.DotNetBar.Office2007Form
    {
        #region Variables Declaration
              
        string MstrAttachFileName, MstrMessageCaption;

        ClsLogWriter MobjLogWriter;
       
        clsBLLEmailPopUp MobjclsBLLEmailPopUp;

        #endregion Variables Declaration

        public FrmSupport()
        {
            #region Constructor

            InitializeComponent();
                
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            MobjLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjclsBLLEmailPopUp = new clsBLLEmailPopUp();
            MstrAttachFileName = "";

            #endregion //Constructor
        }


        private void FrmSupport_Load(object sender, EventArgs e)
        {
           
        }

        private bool Validation()
        {
            if (ClsCommonSettings.RoleID > 3)
            {
                MessageBox.Show("You have no permissions to send support mail.\n User have Admin role can send support mail.", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtbody.Focus();
                return false;
            }
            if (txtbody.Text.Trim () == "")
            {
                MessageBox.Show("Please enter message.", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtbody.Focus();
                return false;
            }
            return true;
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation())
                {
                    DataTable dtMailSettings = MobjclsBLLEmailPopUp.GetMailSettings();
                    if (dtMailSettings.Rows.Count > 0)
                    {
                        if (SendSupportMail(dtMailSettings.Rows[0]["OutgoingServer"].ToStringCustom(), dtMailSettings.Rows[0]["PortNumber"].ToInt32(), dtMailSettings.Rows[0]["UserName"].ToStringCustom(), dtMailSettings.Rows[0]["Password"].ToStringCustom(), Convert.ToBoolean(dtMailSettings.Rows[0]["EnableSsl"])))
                        {
                            MessageBox.Show("Mail send successfully. ES3 Technovations team will contact you shortly.", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Failed to send Mail. Please contact regional support center.\n Refer www.escubetech.com", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Failed to send Mail. Please configure your mail settings to send support mail.", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Failed to send Mail. Please configure your mail settings to send support mail.", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public bool SendSupportMail(string host, int portno, string sFromId, string sFPwd, bool bEnSsl)
        {
            try
            {
                SmtpClient mailClient = new SmtpClient();
                mailClient.UseDefaultCredentials = false;
                mailClient.Credentials = new System.Net.NetworkCredential(sFromId, sFPwd);
                if (portno > 0)
                    mailClient.Port = portno;

                mailClient.Host = host;
                if (bEnSsl)
                    mailClient.EnableSsl = true;

                MailMessage objMail = new MailMessage();
                objMail.From = new MailAddress(sFromId);
                objMail.To.Add("support@escubetech.com");
                objMail.IsBodyHtml = true;
                objMail.Priority = MailPriority.Normal;
                if (MstrAttachFileName.Trim() != "")
                    objMail.Attachments.Add(new Attachment(MstrAttachFileName));
                objMail.Subject = TxtSubject.Text.Trim () + "(" + ClsMainSettings.strProductSerial + ")";
                objMail.Body = txtbody.Text.Trim();
                mailClient.Send(objMail);

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            TxtSubject.Text = "";
            lblFileName.Text = "";
            MstrAttachFileName = "";
            txtbody.Text = "";
            btnRemove.Visible = false;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAttach_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog objBrowser = new OpenFileDialog();
                objBrowser.Title = "Select a file to send";
                if (objBrowser.ShowDialog() == DialogResult.OK)
                {
                    lblFileName.Text = objBrowser.SafeFileName;
                    MstrAttachFileName = objBrowser.FileName;
                    btnRemove.Visible = true;
                }
                objBrowser.Dispose();
            }
            catch (Exception)
            {
                lblFileName.Text = "";
                MstrAttachFileName = "";
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            lblFileName.Text = "";
            MstrAttachFileName = "";
            btnRemove.Visible = false;
        }

      
    }
}