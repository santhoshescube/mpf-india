﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.VisualBasic;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,1 Mar 2011>
Description:	<Description,UserInformation Form>
 * 
 * 
 * 
 * Modified By :Rajesh
 * Modified On :12-Aug-2013
================================================
*/

namespace MyPayfriend
{
    public partial class FrmConfigurationSetting : DevComponents.DotNetBar.Office2007Form
    {
        #region Variables Declaration

        private bool MblnAddPermission = true;//To Set Add Permission
        private bool MblnUpdatePermission = true;//To Set Update Permission
        private bool MblnDeletePermission = true;//To Set Delete Permission
        private bool MblnPrintEmailPermission = true;
       
     
        private string MstrMessageCommon;
        private string MstrMessageCaption;
        private string YesOrNo;//Grid validation



        private ArrayList MaMessageArr;
        private MessageBoxIcon MmessageIcon;

         ClsLogWriter MobjClsLogWriter;
        ClsNotification MobjClsNotification;
        clsBLLConfigurationSettings MobjclsBLLConfigurationSettings;
     

        private TextBox TxtB;

        #endregion //Variables Declaration

        #region Constructor

        public FrmConfigurationSetting(int intModuleID)
        {
            InitializeComponent();
            //MblnShowErrorMess = true;
            MstrMessageCommon = "";
            ModuleID = intModuleID;
            
        

            MstrMessageCaption = ClsCommonSettings.MessageCaption; //Message caption

            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLConfigurationSettings = new clsBLLConfigurationSettings();
         
        }

        public FrmConfigurationSetting()
        {
            InitializeComponent();
            //MblnShowErrorMess = true;
            MstrMessageCommon = "";

         
            MstrMessageCaption = ClsCommonSettings.MessageCaption; //Message caption

            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjClsNotification = new ClsNotification();
            MmessageIcon = MessageBoxIcon.Information;
            MobjclsBLLConfigurationSettings = new clsBLLConfigurationSettings();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }

        
        }
        #endregion //Constructor

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.Configuration, this);

        }

        public int ModuleID { get; set; }

        private void FrmConfigurationSetting_Load(object sender, EventArgs e)
        {
            SetPermissions();
            LoadMessage();
            DisplayConfigurationDetail(Type.ConfigValue);
        }
      
        private void SetPermissions()
        {

            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Setings, (Int32)eMenuID.ConfigurationSetting,
                 out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }

        
        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaMessageArr = MobjClsNotification.FillMessageArray((int)FormID.Configuration, ClsCommonSettings.ProductID);

        }

        //Display the configuration details 
        private void DisplayConfigurationDetail(Type eType)
        {
            DgvConfigurationSetting.Rows.Clear();

            if (ClsCommonSettings.IsArabicView)
            {
                DgvConfigurationSetting.Columns["ColCofigItemArb"].Visible = true;
                DgvConfigurationSetting.Columns["ColCofigItem"].Visible = false;
                DgvConfigurationSetting.Columns["ColCofigItemArb"].ReadOnly = true;
                DgvConfigurationSetting.Columns["ColCofigItemArb"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            else
            {
                DgvConfigurationSetting.Columns["ColCofigItemArb"].Visible = false;
                DgvConfigurationSetting.Columns["ColCofigItem"].Visible = true;
                DgvConfigurationSetting.Columns["ColCofigItem"].ReadOnly = true; 
            }



            using (DataTable DtGrid = clsBLLConfigurationSettings.DisplayConfigurationDetails())
            {
                if (DtGrid.Rows.Count > 0)
                {
                    for (int i = 0; i < DtGrid.Rows.Count; i++)
                    {
                        DgvConfigurationSetting.RowCount = DgvConfigurationSetting.RowCount + 1;
                        DgvConfigurationSetting.Rows[i].Cells[0].Value = DtGrid.Rows[i]["ConfigurationID"];

        
                        DgvConfigurationSetting.Rows[i].Cells[1].Value = DtGrid.Rows[i]["ConfigurationItem"];
                        DgvConfigurationSetting.Rows[i].Cells["ColCofigItemArb"].Value = DtGrid.Rows[i]["ConfigurationItemArb"];

                        if (eType == Type.ConfigValue)
                            DgvConfigurationSetting.Rows[i].Cells[2].Value = DtGrid.Rows[i]["ConfigurationValue"];
                        else
                            DgvConfigurationSetting.Rows[i].Cells[2].Value = DtGrid.Rows[i]["DefaultValue"];
                        DgvConfigurationSetting.Rows[i].Cells[3].Value = DtGrid.Rows[i]["Predefined"];
                        DgvConfigurationSetting.Rows[i].Cells[4].Value = DtGrid.Rows[i]["ValueLimt"];


                        DgvConfigurationSetting.Rows[i].Cells[5].Value = DtGrid.Rows[i]["DefaultValue"];
                        if (DtGrid.Rows[i]["Predefined"].ToBoolean() && (ClsCommonSettings.RoleID != 1))
                            DgvConfigurationSetting.Rows[i].ReadOnly = true;
                        else
                            DgvConfigurationSetting.Rows[i].ReadOnly = false;
                    }
                }
            }
        }


        private void btnReset_Click(object sender, EventArgs e)
        {
            DisplayConfigurationDetail(Type.DefaultValue);
            //update the table with the default values
            SaveConfiguration(Type.DefaultValue);

            //MsMessageCommon = "Default values restored";
            MstrMessageCommon = MobjClsNotification.GetErrorMessage(MaMessageArr, 3001, out MmessageIcon);
            LblConfiguration.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

        }

  

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (ValidateSave())
            {
                if (SaveConfiguration(Type.ConfigValue))
                {
                    MstrMessageCommon = MobjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    LblConfiguration.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    this.Close();
                }
            }
        }

        public bool SaveConfiguration(Type eType)
        {

            List<clsDTOConfigurationSettings> DTOList = new List<clsDTOConfigurationSettings>();

            if (eType == Type.ConfigValue)
            {
                MstrMessageCommon = MobjClsNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
            }

            if (DgvConfigurationSetting.Rows.Count > 0)
            {
                for (int i = 0; i < DgvConfigurationSetting.Rows.Count; i++)
                {

                    DTOList.Add(new clsDTOConfigurationSettings()
                    {
                        ConfigurationID = Convert.ToInt32(DgvConfigurationSetting.Rows[i].Cells[0].Value)
                       ,
                        ConfigurationValue = Convert.ToString(DgvConfigurationSetting.Rows[i].Cells[2].Value)
                    });
                }
                MobjclsBLLConfigurationSettings.UpdateConfiguration(DTOList);//Updating grid Values 
                SetCommonSettingsConfigurationSettingsInfo();
            }
            return true;
        }


        private string GetConfigurationValue(string strConfigurationItem, DataTable dtConfigurationSettings)
        {
            dtConfigurationSettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "'";
            if (dtConfigurationSettings.DefaultView.ToTable().Rows.Count > 0)
                return Convert.ToString(dtConfigurationSettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"]).Trim();
            return "";
        }


        public void SetCommonSettingsConfigurationSettingsInfo()
        {
            DataTable dtConfigurationSettings = clsBLLConfigurationSettings.DisplayConfigurationDetails();
            string strValue = "";

            ClsCommonSettings.MessageCaption = GetConfigurationValue("MessageCaption", dtConfigurationSettings);
            ClsCommonSettings.ReportFooter = GetConfigurationValue("ReportFooter", dtConfigurationSettings);

            strValue = GetConfigurationValue("BackupInterval", dtConfigurationSettings);
            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.intBackupInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.intBackupInterval = 0;

            strValue = GetConfigurationValue("DefaultStatusBarMessageTime", dtConfigurationSettings);
            if (!string.IsNullOrEmpty(strValue))
                ClsCommonSettings.TimerInterval = Convert.ToInt32(strValue);
            else
                ClsCommonSettings.TimerInterval = 1000;

            if (Strings.UCase(GetConfigurationValue("SalaryDayIsEditable", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.GlbSalaryDayIsEditable = true;
            }
            else
            {
                ClsCommonSettings.GlbSalaryDayIsEditable = false;
            }
            if (Strings.UCase(GetConfigurationValue("SIFFile50Percentage", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.SIFFile50Percentage = true;
            }
            else
            {
                ClsCommonSettings.SIFFile50Percentage = false;
            }


            if (Strings.UCase(GetConfigurationValue("AmountRoundByZero", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.IsAmountRoundByZero = true;
            }
            else
            {
                ClsCommonSettings.IsAmountRoundByZero = false;
            }

            if (Strings.UCase(GetConfigurationValue("SalarySlipIsEditable", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.GlSalarySlipIsEditable = true;
            }
            else
            {
                ClsCommonSettings.GlSalarySlipIsEditable = false;
            }
            if (Strings.UCase(GetConfigurationValue("24HourTimeFormat", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.Glb24HourFormat = true;
            }
            else
            {
                ClsCommonSettings.Glb24HourFormat = false;
            }


            if (Strings.UCase(GetConfigurationValue("DisplayLeaveSummaryInSalarySlip", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip = true;
            }
            else
            {
                ClsCommonSettings.GlDisplayLeaveSummaryInSalarySlip = false;
            }

            if (Strings.UCase(GetConfigurationValue("EnableLiveAttendance", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.AttendanceLiveEnable = true;
            }
            else
            {
                ClsCommonSettings.AttendanceLiveEnable = false;
            } 
     
            if (Strings.UCase(GetConfigurationValue("SendMailToAll", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.SendMailToAll = true;
            }
            else
            {
                ClsCommonSettings.SendMailToAll = false;
            }

            ClsCommonSettings.intLoanAmountPercentage = GetConfigurationValue("LoanInstallmentAmountPercentage", dtConfigurationSettings).ToInt32();

            if (Strings.UCase(GetConfigurationValue("HRPowerEnabled", dtConfigurationSettings)) == "YES")
                ClsCommonSettings.IsHrPowerEnabled = true;
            else
                ClsCommonSettings.IsHrPowerEnabled = false;


            if (Strings.UCase(GetConfigurationValue("VacationBasedOnActualRejoinDate", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.IsVacationBasedOnActualRejoinDate = true;
            }
            else
            {
                ClsCommonSettings.IsVacationBasedOnActualRejoinDate = false;
            }


            if (Strings.UCase(GetConfigurationValue("AttendanceAutofillForSinglePunch", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.AttendanceAutofillForSinglePunch = true;
            }
            else
            {
                ClsCommonSettings.AttendanceAutofillForSinglePunch = false;
            }

            if (Strings.UCase(GetConfigurationValue("ThirdPartyIntegration", dtConfigurationSettings)) == "YES")
            {
                ClsCommonSettings.ThirdPartyIntegration = true;
            }
            else
            {
                ClsCommonSettings.ThirdPartyIntegration = false;
            }
        }



        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            BtnOk_Click(new object(), new EventArgs());

        }


        private bool ValidateSave()
        {
            if (DgvConfigurationSetting.Rows.Count > 0)
            {
                Int64 iValueLimit;
                Int64 iCurrentValue;


                for (int i = 0; i < DgvConfigurationSetting.Rows.Count; i++)
                {
                    iValueLimit = Convert.ToInt64(DgvConfigurationSetting.Rows[i].Cells[4].Value);
                    if (iValueLimit > 0)
                    {
                        iCurrentValue = DgvConfigurationSetting.Rows[i].Cells[2].Value.ToInt64(); 

                        if (iCurrentValue > iValueLimit)
                        {
                            MstrMessageCommon = "Configuration Value Exceeds Limit";
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LblConfiguration.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            DgvConfigurationSetting.CurrentCell = DgvConfigurationSetting.Rows[i].Cells[2];
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void DgvConfigurationSetting_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)//For UpperCase
        {
            if (DgvConfigurationSetting.CurrentCell.ColumnIndex == 2)
            {
                this.TxtB = (TextBox)e.Control;
                this.TxtB.KeyPress -= this.TextBox_KeyPress;
                this.TxtB.KeyPress += this.TextBox_KeyPress;
            }
        }

        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((YesOrNo.ToUpper() == "YES") || (YesOrNo.ToUpper() == "NO"))
            {
                TxtB.CharacterCasing = CharacterCasing.Upper;
                TxtB.Text = TxtB.Text;

            }
            else
            {
                TxtB.CharacterCasing = CharacterCasing.Normal;
            }
            if (Convert.ToInt32(DgvConfigurationSetting.CurrentRow.Cells[4].Value) > 0)
            {
                if (!((Char.IsDigit(e.KeyChar)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
            }
        }

        private void DgvConfigurationSetting_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex > -1)
            {
                YesOrNo = Convert.ToString(DgvConfigurationSetting.CurrentCell.Value);
            }
        }

        private void DgvConfigurationSetting_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > -1)
            {
                DgvConfigurationSetting.CurrentCell.Value = TxtB.Text;
                TxtB.CharacterCasing = CharacterCasing.Normal;
                YesOrNo = "";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      

        private void FrmConfigurationSetting_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        BtnHelp_Click(sender, new EventArgs());
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Alt | Keys.S:
                        BindingNavigatorSaveItem_Click(sender, new EventArgs());//save
                        break;
               }
            }
            catch (Exception)
            {
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            new FrmHelp() { strFormName = "Configuration" }.ShowDialog();

        }
        public enum Type
        {
            DefaultValue = 1,
            ConfigValue = 2
        }
    }
}
