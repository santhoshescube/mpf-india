﻿namespace MyPayfriend
{
    partial class FrmCompanySettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCompanySettings));
            this.SettingsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorClearItem = new System.Windows.Forms.ToolStripButton();
            this.lblUserStatus = new DevComponents.DotNetBar.LabelItem();
            this.dgvCompanySettings = new System.Windows.Forms.DataGridView();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.toolStripStatusLabel11 = new DevComponents.DotNetBar.LabelItem();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.TmrFocus = new System.Windows.Forms.Timer(this.components);
            this.TmrSettings = new System.Windows.Forms.Timer(this.components);
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.ErrorProSettings = new System.Windows.Forms.ErrorProvider(this.components);
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFormID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AutoNo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PreFix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColtxtStartNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.SettingsBindingNavigator)).BeginInit();
            this.SettingsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompanySettings)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProSettings)).BeginInit();
            this.SuspendLayout();
            // 
            // SettingsBindingNavigator
            // 
            this.SettingsBindingNavigator.AddNewItem = null;
            this.SettingsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.SettingsBindingNavigator.CountItem = null;
            this.SettingsBindingNavigator.DeleteItem = null;
            this.SettingsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BindingNavigatorClearItem});
            this.SettingsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.SettingsBindingNavigator.MoveFirstItem = null;
            this.SettingsBindingNavigator.MoveLastItem = null;
            this.SettingsBindingNavigator.MoveNextItem = null;
            this.SettingsBindingNavigator.MovePreviousItem = null;
            this.SettingsBindingNavigator.Name = "SettingsBindingNavigator";
            this.SettingsBindingNavigator.PositionItem = null;
            this.SettingsBindingNavigator.Size = new System.Drawing.Size(556, 25);
            this.SettingsBindingNavigator.TabIndex = 90;
            this.SettingsBindingNavigator.Text = "bindingNavigator1";
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.Text = "Save Data";
            this.BindingNavigatorSaveItem.ToolTipText = "Save ";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BindingNavigatorClearItem
            // 
            this.BindingNavigatorClearItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorClearItem.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.BindingNavigatorClearItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BindingNavigatorClearItem.Name = "BindingNavigatorClearItem";
            this.BindingNavigatorClearItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorClearItem.ToolTipText = "Clear";
            this.BindingNavigatorClearItem.Click += new System.EventHandler(this.BindingNavigatorClearItem_Click);
            // 
            // lblUserStatus
            // 
            this.lblUserStatus.Name = "lblUserStatus";
            // 
            // dgvCompanySettings
            // 
            this.dgvCompanySettings.AllowUserToAddRows = false;
            this.dgvCompanySettings.BackgroundColor = System.Drawing.Color.White;
            this.dgvCompanySettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompanySettings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColFormID,
            this.FormName,
            this.AutoNo,
            this.PreFix,
            this.ColtxtStartNumber,
            this.NoLength});
            this.dgvCompanySettings.Location = new System.Drawing.Point(3, 41);
            this.dgvCompanySettings.Name = "dgvCompanySettings";
            this.dgvCompanySettings.RowHeadersWidth = 24;
            this.dgvCompanySettings.Size = new System.Drawing.Size(539, 221);
            this.dgvCompanySettings.TabIndex = 20;
            this.dgvCompanySettings.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvCompanySettings_CellBeginEdit);
            this.dgvCompanySettings.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvCompanySettings_EditingControlShowing);
            this.dgvCompanySettings.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvCompanySettings_CurrentCellDirtyStateChanged);
            this.dgvCompanySettings.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvCompanySettings_DataError);
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 13;
            this.cboCompany.Location = new System.Drawing.Point(97, 14);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(342, 21);
            this.cboCompany.TabIndex = 3;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCompany_KeyDown);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.dgvCompanySettings);
            this.panel1.Controls.Add(this.cboCompany);
            this.panel1.Controls.Add(this.lblEmployee);
            this.panel1.Location = new System.Drawing.Point(0, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(550, 276);
            this.panel1.TabIndex = 91;
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(12, 14);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(51, 13);
            this.lblEmployee.TabIndex = 6;
            this.lblEmployee.Text = "Company";
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Text = "Status:";
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(475, 310);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 94;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // TmrFocus
            // 
            this.TmrFocus.Interval = 10;
            // 
            // bar1
            // 
            this.bar1.AntiAlias = true;
            this.bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.toolStripStatusLabel11,
            this.lblUserStatus});
            this.bar1.Location = new System.Drawing.Point(0, 345);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(556, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bar1.TabIndex = 95;
            this.bar1.TabStop = false;
            // 
            // ErrorProSettings
            // 
            this.ErrorProSettings.ContainerControl = this;
            this.ErrorProSettings.RightToLeft = true;
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(398, 310);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 93;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(0, 310);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 92;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Form";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Start Number";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "PreFix";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "Start Number";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ColFormID
            // 
            this.ColFormID.HeaderText = "FormID";
            this.ColFormID.Name = "ColFormID";
            this.ColFormID.Visible = false;
            // 
            // FormName
            // 
            this.FormName.HeaderText = "Form";
            this.FormName.MinimumWidth = 100;
            this.FormName.Name = "FormName";
            this.FormName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FormName.Width = 135;
            // 
            // AutoNo
            // 
            this.AutoNo.HeaderText = "AutoNo";
            this.AutoNo.Name = "AutoNo";
            // 
            // PreFix
            // 
            this.PreFix.HeaderText = "PreFix";
            this.PreFix.Name = "PreFix";
            // 
            // ColtxtStartNumber
            // 
            this.ColtxtStartNumber.HeaderText = "Start Number";
            this.ColtxtStartNumber.Name = "ColtxtStartNumber";
            this.ColtxtStartNumber.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // NoLength
            // 
            this.NoLength.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NoLength.HeaderText = "Length";
            this.NoLength.MaxInputLength = 1;
            this.NoLength.Name = "NoLength";
            // 
            // FrmCompanySettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 364);
            this.ControlBox = false;
            this.Controls.Add(this.SettingsBindingNavigator);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmCompanySettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company Settings";
            this.Load += new System.EventHandler(this.FrmCompanySettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SettingsBindingNavigator)).EndInit();
            this.SettingsBindingNavigator.ResumeLayout(false);
            this.SettingsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompanySettings)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProSettings)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator SettingsBindingNavigator;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton BindingNavigatorClearItem;
        private DevComponents.DotNetBar.LabelItem lblUserStatus;
        private System.Windows.Forms.DataGridView dgvCompanySettings;
        private System.Windows.Forms.ComboBox cboCompany;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblEmployee;
        private DevComponents.DotNetBar.LabelItem toolStripStatusLabel11;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Timer TmrFocus;
        private System.Windows.Forms.Timer TmrSettings;
        private DevComponents.DotNetBar.Bar bar1;
        private System.Windows.Forms.ErrorProvider ErrorProSettings;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFormID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AutoNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreFix;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColtxtStartNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoLength;
    }
}