﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web.UI.WebControls;
using System.Collections;

namespace MyPayfriend
{
    public partial class frmRole : DevComponents.DotNetBar.Office2007Form
    {
        #region Declarations

        clsBLLRoleSettings objclsRoleSettingsBLL;

        clsBLLPermissionSettings objClsBLLPermissionSettings;
        clsDTORoleSettings objRoleSettings;
        ClsNotification MobjNotification;

        ArrayList MaStatusMessage;
        private ArrayList MaMessageArr;
        private string MstrMessageCommon;
        private MessageBoxIcon MmsgMessageIcon;

        private bool MblnPrintEmailPermission = false; //To set Print Email Permission
        private bool MblnAddPermission = false;//To set Add Permission
        private bool MblnUpdatePermission = false;//To set Update Permission
        private bool MblnDeletePermission = false;

        private bool MblnChangeStatus = false;

        private int intAddWidth = 0, intUpdateWidth = 0, intDeleteWidth = 0, intViewWidth = 0, intPrintWidth = 0;
        private bool blnLoad = true;

        private bool MblnIsHRPowerEnabled;
        #endregion

        #region Constructor
        public frmRole()
        {
            InitializeComponent();
            objclsRoleSettingsBLL = new clsBLLRoleSettings();
            MobjNotification = new ClsNotification();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }


        }
        #endregion
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.RoleSettings, this);

        }

        #region Events
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmRole_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            LoadCombo();
            BindRoleDetails();
            if (dgvRoleSettings.Columns.Count > 0)
            {
                intAddWidth = chkAlladd.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsCreate.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
                intUpdateWidth = chkAllupdate.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsUpdate.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
                intDeleteWidth = chkalldelete.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsDelete.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
                intViewWidth = chkallview.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsView.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
                intPrintWidth = chkAllPrintEmail.Left - (dgvRoleSettings.GetCellDisplayRectangle(clmIsPrintEmail.HeaderCell.ColumnIndex, -1, false).Left + dgvRoleSettings.Left);
            }

            MblnIsHRPowerEnabled = ClsCommonSettings.IsHrPowerEnabled;
            blnLoad = false;
            EnableButtons(false);
            tmrRoles.Interval = ClsCommonSettings.TimerInterval;
        }

        private void frmRole_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 8, out MmsgMessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void frmRole_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        ShowHelp();
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                }
            }
            catch
            {
            }
        }

        private void cboRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindRoleDetails();
            EnableButtons(false);
            lblStatus.Text = string.Empty;
        }

        private void btnBottomCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lstModules_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.Index >= 0)
            {
                int intModuleID = ((clsListItem)(lstModules.Items[e.Index])).Value.ToInt32();

                chkAlladd.Checked = chkalldelete.Checked = chkAllPrintEmail.Checked = chkAllupdate.Checked = chkallview.Checked = false;

                if (e.NewValue == CheckState.Unchecked)
                {
                    RemoveGridRow(intModuleID);
                    EnableButtons(true);
                }
                else
                {
                    BindRoleDetails(intModuleID);
                }

                if (dgvRoleSettings.Rows.Count > 0)
                {
                    if (fnChalladdtick())
                        chkAlladd.Checked = true;

                    if (fnChallviewtick())
                        chkallview.Checked = true;

                    if (fnChallupdatetick())
                        chkAllupdate.Checked = true;

                    if (fnChalldeletetick())
                        chkalldelete.Checked = true;

                    if (fnChallPrintEmailtick())
                        chkAllPrintEmail.Checked = true;
                }
            }
        }

        private void chkAlladd_Click(object sender, EventArgs e)
        {
            checkOrUncheckAllRows("clmIsCreate", chkAlladd);
        }

        private void chkallview_Click(object sender, EventArgs e)
        {
            checkOrUncheckAllRows("clmIsView", chkallview);
        }

        private void chkAllupdate_Click(object sender, EventArgs e)
        {
            checkOrUncheckAllRows("clmIsUpdate", chkAllupdate);
        }

        private void chkalldelete_Click(object sender, EventArgs e)
        {
            checkOrUncheckAllRows("clmIsDelete", chkalldelete);
        }

        private void chkAllPrintEmail_Click(object sender, EventArgs e)
        {
            checkOrUncheckAllRows("clmIsPrintEmail", chkAllPrintEmail);
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            chkSelectAll.Text = chkSelectAll.Checked ? "UnSelect All" : "Select All";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveRoleDetails())
            {
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveRoleDetails();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            SaveRoleDetails();
        }

        private void dgvRoleSettings_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            EnableButtons(true);
        }

        private void dgvRoleSettings_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (dgvRoleSettings.Rows[e.RowIndex].Cells[1].Value.ToInt32() == (int)eModuleID.Reports)
                    {
                        if (e.ColumnIndex == 5 || e.ColumnIndex == 7 || e.ColumnIndex == 8)
                        {
                            dgvRoleSettings.CancelEdit();
                        }
                    }
                    else
                    {
                        dgvRoleSettings.CommitEdit(DataGridViewDataErrorContexts.Commit);

                        if (e.ColumnIndex == 5)
                        {
                            if (fnChalladdtick() == false)
                                chkAlladd.Checked = false;
                            else
                                chkAlladd.Checked = true;

                        }
                        else if (e.ColumnIndex == 6)
                        {
                            if (fnChallviewtick() == false)
                                chkallview.Checked = false;
                            else
                                chkallview.Checked = true;
                        }
                        else if (e.ColumnIndex == 7)
                        {
                            if (fnChallupdatetick() == false)
                                chkAllupdate.Checked = false;
                            else
                                chkAllupdate.Checked = true;
                        }
                        else if (e.ColumnIndex == 8)
                        {
                            if (fnChalldeletetick() == false)
                                chkalldelete.Checked = false;
                            else
                                chkalldelete.Checked = true;
                        }

                        else if (e.ColumnIndex == 9)
                        {
                            if (fnChallPrintEmailtick() == false)
                                chkAllPrintEmail.Checked = false;
                            else
                                chkAllPrintEmail.Checked = true;
                        }

                    }

                }
            }
            catch (Exception)
            {

            }
        }

        private void btnNewRole_Click(object sender, EventArgs e)
        {
            try
            {
                int intComboID = cboRole.SelectedValue.ToInt32();
                FrmCommonRef objCommon = new FrmCommonRef("Role", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "RoleID,IsPredefined, RoleName,RoleNameArb", "RoleReference", "RoleID > 5");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCombo();

                if (objCommon.NewID != 0)
                {
                    cboRole.SelectedValue = objCommon.NewID;
                }
                else
                {
                    cboRole.SelectedValue = intComboID;
                }
            }
            catch (Exception)
            {
            }

        }

        private void cboRole_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void dgvRoleSettings_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            if (!blnLoad)
            {
                int iPointAddLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsCreate.HeaderCell.ColumnIndex, -1, false).Left + intAddWidth;
                int iPointViewLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsView.HeaderCell.ColumnIndex, -1, false).Left + intViewWidth;
                int iPointUpadateLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsUpdate.HeaderCell.ColumnIndex, -1, false).Left + intUpdateWidth;
                int iPointDeleteLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsDelete.HeaderCell.ColumnIndex, -1, false).Left + intDeleteWidth;
                int iPointPrintEMLeft = dgvRoleSettings.Left + dgvRoleSettings.GetCellDisplayRectangle(clmIsPrintEmail.HeaderCell.ColumnIndex, -1, false).Left + intPrintWidth;

                chkAlladd.Left = iPointAddLeft;
                chkAllupdate.Left = iPointUpadateLeft;
                chkalldelete.Left = iPointDeleteLeft;
                chkallview.Left = iPointViewLeft;
                chkAllPrintEmail.Left = iPointPrintEMLeft;

                int iDisplayCount = dgvRoleSettings.DisplayedColumnCount(false);

                chkAlladd.Visible = iDisplayCount > 2 ? true : false;
                chkallview.Visible = iDisplayCount > 3 ? true : false;
                chkAllupdate.Visible = iDisplayCount > 4 ? true : false;
                chkalldelete.Visible = iDisplayCount > 5 ? true : false;
                chkAllPrintEmail.Visible = iDisplayCount > 6 ? true : false;

                chkAllPrintEmail.Visible = clmIsPrintEmail.Width > 70 ? true : false;
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }

        private void chkSelectAll_Click(object sender, EventArgs e)
        {
            for (int iCounter = 0; iCounter < lstModules.Items.Count; iCounter++)
            {
                lstModules.SetItemChecked(iCounter, chkSelectAll.Checked);
            }

            if (!chkSelectAll.Checked)
                chkAlladd.Checked = chkalldelete.Checked = chkAllPrintEmail.Checked = chkAllupdate.Checked = chkallview.Checked = false;
        }

        private void lstModules_Click(object sender, EventArgs e)
        {
            if (lstModules.SelectedItem != null)
            {
                int intCount = 1;

                if (lstModules.GetItemCheckState(lstModules.Items.IndexOf(lstModules.SelectedItem)) == CheckState.Checked)
                {
                    intCount = -1;
                }

                chkSelectAll.Checked = lstModules.CheckedItems.Count + intCount == lstModules.Items.Count ? true : false;
            }
        }

        private void cboRole_KeyDown(object sender, KeyEventArgs e)
        {
            cboRole.DroppedDown = false;
        }

        private void tmrRoles_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = string.Empty;
            tmrRoles.Enabled = false;
        }

        #endregion

        #region Functions

        /// <summary>
        /// Load Role Combo
        /// </summary>
        private void LoadCombo()
        {
            cboRole.ValueMember = "RoleID";
            cboRole.DisplayMember = "RoleName";
            cboRole.DataSource = objclsRoleSettingsBLL.BindRoles();
        }

        /// <summary>
        /// Load Messages
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MmsgMessageIcon = MessageBoxIcon.Information;
            MaMessageArr = MobjNotification.FillMessageArray((int)FormID.RoleSettings, ClsCommonSettings.ProductID);
            MaStatusMessage = MobjNotification.FillStatusMessageArray((int)FormID.RoleSettings, ClsCommonSettings.ProductID);
        }

        /// <summary>
        /// Set Permissions
        /// </summary>
        private void SetPermissions()
        {
            objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (int)eModuleID.Setings, (int)eMenuID.Role, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = true;

            btnSave.Enabled = btnSaveItem.Enabled = btnOk.Enabled = MblnAddPermission;

        }

        /// <summary>
        /// Check or Uncheck Grid Items
        /// </summary>
        private void GetListItemChecked()
        {
            DataTable dtModuleIDs = objclsRoleSettingsBLL.GetModuleIDs();
            for (int i = 0; i < dtModuleIDs.Rows.Count; i++)
            {
                for (int j = 0; j < this.lstModules.Items.Count; j++)
                {
                    if (dtModuleIDs.Rows[i]["ModuleID"].ToInt32() == ((clsListItem)(lstModules.Items[j])).Value.ToInt32())
                        this.lstModules.SetItemChecked(j, true);
                }
            }

            chkSelectAll.Checked = dtModuleIDs.Rows.Count == lstModules.Items.Count ? true : false;
        }

        /// <summary>
        /// Check Or Uncheck checkboxes in Grid
        /// </summary>
        /// <param name="blnExcludeReports">true then exclude reports</param>
        /// <param name="strColumnName">Column Name</param>
        /// <param name="chkItem">checkbox</param>
        private void checkOrUncheckAllRows(string strColumnName, System.Windows.Forms.CheckBox chkItem)
        {
            if (dgvRoleSettings.Rows.Count == 0)
            {
                chkItem.Checked = false;
                return;
            }

            for (int iCounter = 0; iCounter < dgvRoleSettings.Rows.Count; iCounter++)
            {
                if (dgvRoleSettings.Rows[iCounter].Cells[strColumnName].ReadOnly)
                {
                    dgvRoleSettings.Rows[iCounter].Cells[strColumnName].Value = false;
                }
                else
                    dgvRoleSettings.Rows[iCounter].Cells[strColumnName].Value = chkItem.Checked;
            }

        }


        /// <summary>
        /// Bind Role Details Grid 
        /// </summary>
        private void BindRoleDetails()
        {
            try
            {
                objclsRoleSettingsBLL.objclsRoleSettings.RoleID = cboRole.SelectedValue.ToInt32();
                chkAlladd.Checked = chkallview.Checked = chkAllupdate.Checked = chkalldelete.Checked = chkAllPrintEmail.Checked = chkSelectAll.Checked = false;

                DataTable dtModules = objclsRoleSettingsBLL.BindModules();
                lstModules.Items.Clear();
                if (dtModules.Rows.Count > 0)
                {
                    for (int i = 0; i < dtModules.Rows.Count; i++)
                    {
                        lstModules.Items.Add(new clsListItem(dtModules.Rows[i]["ModuleId"], dtModules.Rows[i]["Description"].ToString()));
                    }
                }

                DataTable dtRoleDetails = objclsRoleSettingsBLL.BindRoleDetails();

                dgvRoleSettings.Rows.Clear();

                if (dtRoleDetails.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRoleDetails.Rows.Count; i++)
                    {
                        dgvRoleSettings.RowCount = dgvRoleSettings.RowCount + 1;
                        dgvRoleSettings.Rows[i].Cells["clmRoleID"].Value = dtRoleDetails.Rows[i]["RoleID"];
                        dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value = dtRoleDetails.Rows[i]["ModuleID"];
                        dgvRoleSettings.Rows[i].Cells["clmMenuID"].Value = dtRoleDetails.Rows[i]["MenuID"];
                        dgvRoleSettings.Rows[i].Cells["clmModuleName"].Value = dtRoleDetails.Rows[i]["ModuleName"];
                        dgvRoleSettings.Rows[i].Cells["clmMenuName"].Value = dtRoleDetails.Rows[i]["MenuName"];
                        dgvRoleSettings.Rows[i].Cells["clmIsCreate"].Value = dtRoleDetails.Rows[i]["IsCreate"];
                        dgvRoleSettings.Rows[i].Cells["clmIsView"].Value = dtRoleDetails.Rows[i]["IsView"];
                        dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].Value = dtRoleDetails.Rows[i]["IsUpdate"];
                        dgvRoleSettings.Rows[i].Cells["clmIsDelete"].Value = dtRoleDetails.Rows[i]["IsDelete"];
                        dgvRoleSettings.Rows[i].Cells["clmIsPrintEmail"].Value = dtRoleDetails.Rows[i]["IsPrintEmail"];

                    }
                    DisableMenus();
                    GetListItemChecked();

                    if (fnChalladdtick() == false)
                        chkAlladd.Checked = false;
                    else
                        chkAlladd.Checked = true;

                    if (fnChallviewtick() == false)
                        chkallview.Checked = false;
                    else
                        chkallview.Checked = true;

                    if (fnChallupdatetick() == false)
                        chkAllupdate.Checked = false;
                    else
                        chkAllupdate.Checked = true;

                    if (fnChalldeletetick() == false)
                        chkalldelete.Checked = false;
                    else
                        chkalldelete.Checked = true;

                    if (fnChallPrintEmailtick() == false)
                        chkAllPrintEmail.Checked = false;
                    else
                        chkAllPrintEmail.Checked = true;
                }


            }
            catch (Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }
        }

        private void DisableMenus()
        {

            foreach (DataGridViewRow dgvRow in dgvRoleSettings.Rows)
            {
                if (dgvRow.Cells["clmModuleID"].Value.ToInt32() == (int)eModuleID.Reports)
                {
                    dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                }
                else
                {
                    switch (dgvRow.Cells["clmMenuID"].Value.ToInt32())
                    {
                        case (int)eMenuID.DocumentRegister: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = true;
                            break;
                        case (int)eMenuID.Role: dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = true;
                            break;
                        case (int)eMenuID.ProcessingTime: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.LeaveStructure: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = true;
                            break;
                        case (int)eMenuID.LeaveOpening: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = true;
                            break;
                        case (int)eMenuID.HolidayCalender: dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.NavigationCompany: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.NavigationDocuments: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.NavigationEasyView: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.NavigationEmployee: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.ViewCompanyDocuments: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.ViewEmployeeDocuments: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.Attendance: dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.SalaryProcess: dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.SalaryRelease: dgvRow.Cells["clmIsUpdate"].ReadOnly = true;
                            break;
                        case (int)eMenuID.SalaryPayment: dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.AttendanceMapping: dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.ChangePassword: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.ConfigurationSetting: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.EmailSettings: dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.User: dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.Backup: dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.AttendanceWizard: dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.DocumentRenew: dgvRow.Cells["clmIsView"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.DepositRefund: dgvRow.Cells["clmIsView"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.VacationExtension: dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                        case (int)eMenuID.EmployeeDocumentIssue: 
                                if (MblnIsHRPowerEnabled)
                                dgvRow.Cells["clmIsCreate"].ReadOnly = dgvRow.Cells["clmIsUpdate"].ReadOnly = dgvRow.Cells["clmIsDelete"].ReadOnly = dgvRow.Cells["clmIsPrintEmail"].ReadOnly = true;
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Bind Role Details
        /// </summary>
        /// <param name="iModuleId">ModuleID</param>
        private void BindRoleDetails(int iModuleId)
        {
            try
            {
                if (dgvRoleSettings.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["clmModuleID"].Value.ToString().Equals(iModuleId.ToString())).Count() == 0)
                {
                    objclsRoleSettingsBLL.objclsRoleSettings.RoleID = cboRole.SelectedValue.ToInt32();
                    objclsRoleSettingsBLL.objclsRoleSettings.ModuleID = iModuleId;
                    DataTable dtRoleDetails = objclsRoleSettingsBLL.BindaModulePermissions();
                    if (dtRoleDetails.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtRoleDetails.Rows.Count; i++)
                        {
                            dgvRoleSettings.RowCount = dgvRoleSettings.RowCount + 1;
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmRoleID"].Value = dtRoleDetails.Rows[i]["RoleID"];
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmModuleID"].Value = dtRoleDetails.Rows[i]["ModuleID"];
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmMenuID"].Value = dtRoleDetails.Rows[i]["MenuID"];
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmModuleName"].Value = dtRoleDetails.Rows[i]["ModuleName"];
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmMenuName"].Value = dtRoleDetails.Rows[i]["MenuName"];
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsCreate"].Value = dtRoleDetails.Rows[i]["IsCreate"];
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsView"].Value = dtRoleDetails.Rows[i]["IsView"];
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsUpdate"].Value = dtRoleDetails.Rows[i]["IsUpdate"];
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsDelete"].Value = dtRoleDetails.Rows[i]["IsDelete"];
                            dgvRoleSettings.Rows[dgvRoleSettings.Rows.Count - 1].Cells["clmIsPrintEmail"].Value = dtRoleDetails.Rows[i]["IsPrintEmail"];
                        }
                    }
                }
                dgvRoleSettings.Sort(dgvRoleSettings.Columns[1], ListSortDirection.Ascending);
                DisableMenus();
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Remove GridView Rows of Unchecked Modules
        /// </summary>
        /// <param name="iModuleId"></param>
        private void RemoveGridRow(int iModuleId)
        {
            try
            {
                for (int i = dgvRoleSettings.Rows.Count - 1; i >= 0; i--)
                {
                    if (dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value.ToInt32() == iModuleId)
                    {
                        dgvRoleSettings.Rows.Remove(dgvRoleSettings.Rows[i]);
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void EnableButtons(bool blnEnable)
        {
            btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = blnEnable;
            if (blnEnable)
                btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
            MblnChangeStatus = blnEnable;
        }

        /// <summary>
        /// Save Role Details
        /// </summary>
        /// <returns></returns>
        private bool SaveRoleDetails()
        {
            if (ValidateRoleDetails())
            {
                this.dgvRoleSettings.Focus();
                try
                {
                    objclsRoleSettingsBLL.objclsRoleSettings.RoleID = cboRole.SelectedValue.ToInt32();
                    objclsRoleSettingsBLL.objclsRoleSettings.lstRoleDetails = new List<clsDTORoleSettings>();


                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1, out MmsgMessageIcon).Replace("#", "").Trim();
                    lblStatus.Text = MobjNotification.GetErrorMessage(MaStatusMessage, 1, out MmsgMessageIcon).Replace("#", "").Trim();
                    tmrRoles.Enabled = true;

                    if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return false;
                    }

                    for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
                    {
                        objRoleSettings = new clsDTORoleSettings();
                        objRoleSettings.RoleID = cboRole.SelectedValue.ToInt32();
                        objRoleSettings.ModuleID = dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value.ToInt32();
                        objRoleSettings.MenuID = dgvRoleSettings.Rows[i].Cells["clmMenuID"].Value.ToInt32();
                        objRoleSettings.IsCreate = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsCreate"].Value);
                        objRoleSettings.IsView = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsView"].Value);
                        objRoleSettings.IsUpdate = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].Value);

                        if (((objRoleSettings.IsCreate || objRoleSettings.IsUpdate) && dgvRoleSettings.Rows[i].Cells["clmIsView"].ReadOnly == false))
                            objRoleSettings.IsView = true;

                        objRoleSettings.IsDelete = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsDelete"].Value);
                        objRoleSettings.IsPrintEmail = Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsPrintEmail"].Value);
                        objclsRoleSettingsBLL.objclsRoleSettings.lstRoleDetails.Add(objRoleSettings);
                    }

                    if (objclsRoleSettingsBLL.SaveRoleDetails())
                    {
                        for (int j = 0; j < this.lstModules.Items.Count; j++)
                        {
                            if (!(this.lstModules.GetItemChecked(j)))
                            {
                                objclsRoleSettingsBLL.DeleteModulePermissions(cboRole.SelectedValue.ToInt32(), ((clsListItem)(lstModules.Items[j])).Value.ToInt32());
                            }
                        }

                        MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 2, out MmsgMessageIcon).Replace("#", "").Trim();
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MobjNotification.GetErrorMessage(MaStatusMessage, 2, out MmsgMessageIcon).Replace("#", "").Trim();
                        tmrRoles.Enabled = true;

                        BindRoleDetails();

                        btnSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = false;
                        MblnChangeStatus = false;

                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Error Occured", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }


        /// <summary>
        /// Show Help 
        /// </summary>
        private void ShowHelp()
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "Role"; Help.ShowDialog(); }
        }

        private bool ValidateRoleDetails()
        {
            if (cboRole.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select a Role", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (dgvRoleSettings.Rows.Count > 0)
            {
                foreach (DataGridViewRow dgvRow in dgvRoleSettings.Rows)
                {
                    if (Convert.ToBoolean(dgvRow.Cells[clmIsCreate.Index].Value) == true || Convert.ToBoolean(dgvRow.Cells[clmIsDelete.Index].Value) == true || Convert.ToBoolean(dgvRow.Cells[clmIsPrintEmail.Index].Value) == true || Convert.ToBoolean(dgvRow.Cells[clmIsUpdate.Index].Value) == true || Convert.ToBoolean(dgvRow.Cells[clmIsView.Index].Value) == true)
                    {
                        return true;
                    }
                }
                MessageBox.Show("Please Select Atleast an Item", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Check all Add checkboxes
        /// </summary>
        /// <returns>true if success otherwise returns false</returns>
        private bool fnChalladdtick()
        {
            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value.ToInt32() != (int)eModuleID.Reports && Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsCreate"].Value) == false && dgvRoleSettings.Rows[i].Cells["clmIsCreate"].ReadOnly == false)
                {
                    return false;
                }

            }
            return true;
        }

        /// <summary>
        /// Check all View checkboxes
        /// </summary>
        /// <returns>true if success otherwise returns false</returns>
        private bool fnChallviewtick()
        {
            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsView"].Value) == false && dgvRoleSettings.Rows[i].Cells["clmIsView"].ReadOnly == false)
                {
                    return false;
                }

            }
            return true;
        }

        /// <summary>
        /// Check all Update checkboxes
        /// </summary>
        /// <returns>true if success otherwise returns false</returns>
        private bool fnChallupdatetick()
        {
            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value.ToInt32() != (int)eModuleID.Reports && Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].Value) == false && dgvRoleSettings.Rows[i].Cells["clmIsUpdate"].ReadOnly == false)
                {
                    return false;
                }

            }
            return true;
        }

        /// <summary>
        /// Check all Delete checkboxes
        /// </summary>
        /// <returns>true if success otherwise returns false</returns>
        private bool fnChalldeletetick()
        {
            dgvRoleSettings.CommitEdit(DataGridViewDataErrorContexts.Commit);

            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (dgvRoleSettings.Rows[i].Cells["clmModuleID"].Value.ToInt32() != (int)eModuleID.Reports && Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsDelete"].Value) == false && dgvRoleSettings.Rows[i].Cells["clmIsDelete"].ReadOnly == false)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Check all Print/Email checkboxes
        /// </summary>
        /// <returns>true if success otherwise returns false</returns>
        private bool fnChallPrintEmailtick()
        {
            dgvRoleSettings.CommitEdit(DataGridViewDataErrorContexts.Commit);

            for (int i = 0; i < dgvRoleSettings.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvRoleSettings.Rows[i].Cells["clmIsPrintEmail"].Value) == false && dgvRoleSettings.Rows[i].Cells["clmIsPrintEmail"].ReadOnly == false)
                {
                    return false;
                }
            }
            return true;
        }

        #endregion
    }
}
