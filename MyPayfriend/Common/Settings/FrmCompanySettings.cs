﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;


namespace MyPayfriend
{
    public partial class FrmCompanySettings : Form // DevComponents.DotNetBar.Office2007Form
    {

        private bool MblnAddStatus;
        private bool MblnChangeStatus;
        private bool MblnAddPermission = true;//To Set Add Permission
        private bool MblnUpdatePermission = true;//To Set Update Permission
        private bool MblnDeletePermission = true;//To Set Delete Permission
        private bool MblnPrintEmailPermission = true;


        private string MstrMessageCaption;
        private string MstrMessageCommon;
        private MessageBoxIcon MmessageIcon;

        private int MintCurrentRecCnt;
        private int MintRecordCnt;

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        ClsBLLCompanySettings MObjclsBLLCompanySettings;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;


       #region Constructor
        public FrmCompanySettings()
		{
			InitializeComponent();
			MstrMessageCaption = ClsCommonSettings.MessageCaption;
			TmrSettings.Interval = ClsCommonSettings.TimerInterval;
			MmessageIcon = MessageBoxIcon.Information;

            MObjclsBLLCompanySettings = new ClsBLLCompanySettings();
			MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
			MObjClsNotification = new ClsNotification();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }


		}
		#endregion

        private void FrmCompanySettings_Load(object sender, EventArgs e)
        {
            MblnChangeStatus = false;
            LoadCombos(0);
            LoadMessage();
            AddNewItem();
            TmrFocus.Enabled = true;
            TmrSettings.Enabled = true;
            SetPermissions();
            ChangeStatus();
            MblnChangeStatus = false;
        }


        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.CompanySettings, this);

        }


        private void ClearControls()
        {
            cboCompany.SelectedIndex = -1;
            dgvCompanySettings.Rows.Clear();  
        }

        public bool AddNewItem()
        {
            MblnAddStatus = true;
            TmrSettings.Enabled = true;
            LoadCombos(1);
            ClearControls();
            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3456, out MmessageIcon);
            lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            GetRecordCount();
            BindingAddNew();
            EnableDisableButtons();
            return true;
        }
        private void EnableDisableButtons()
        {
          BindingNavigatorDeleteItem.Enabled = BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = MblnChangeStatus = false;
            BindingNavigatorClearItem.Enabled = true;
        }
        private void BindingAddNew()
        {

            MintCurrentRecCnt = MintRecordCnt + 1;

        }

        private void GetRecordCount()
        {
            MintRecordCnt = MObjclsBLLCompanySettings.RecCountNavigate();
        }


        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Setings, (Int32)eMenuID.User, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                DataTable datPermission = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID);
            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            }

        }


        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.CompanySettings, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.CompanySettings, ClsCommonSettings.ProductID);
        }
        private void ChangeStatus()
        {
 
            MblnChangeStatus = true;
            if (MblnAddStatus == true)
            {
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnAddPermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            }
            else
            {
                BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            }
            ErrorProSettings.Clear();
        }
        private bool LoadCombos(int intType)
        {
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    datCombos = MObjclsBLLCompanySettings.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                }
               
                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        private void cboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            cboCompany.DroppedDown = false; 
        }
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MObjclsBLLCompanySettings.clsDTOCompanySettingsMaster.iCompanyID = cboCompany.SelectedValue.ToInt32();   
                FillDetailList();
                ChangeStatus(); 
            }
            catch
            {
            }
        }
        private void FillDetailList()
        {
            try
            {
                dgvCompanySettings.Rows.Clear();
                DataTable dt = MObjclsBLLCompanySettings.getFormDetails();
                if (dt.Rows.Count > 0)
                {
                        cboCompany.SelectedValue = dt.Rows[0]["CompanyID"].ToInt32();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dgvCompanySettings.RowCount = dgvCompanySettings.RowCount + 1;
                            dgvCompanySettings.Rows[i].Cells["ColFormID"].Value = dt.Rows[i]["FormID"];
                            dgvCompanySettings.Rows[i].Cells["FormName"].Value = dt.Rows[i]["FormName"];
                            dgvCompanySettings.Rows[i].Cells["AutoNo"].Value = dt.Rows[i]["IsAuto"].ToBoolean();
                            dgvCompanySettings.Rows[i].Cells["ColtxtStartNumber"].Value = dt.Rows[i]["StartNumber"];
                            dgvCompanySettings.Rows[i].Cells["PreFix"].Value = dt.Rows[i]["Prefix"];
                            dgvCompanySettings.Rows[i].Cells["NoLength"].Value = dt.Rows[i]["NoLength"];
                        }
                }
            }
            catch
            {
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            BindingNavigatorSaveItem_Click(sender, e); 
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {

            if (CompanySettingsValidation())
            {

                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                if (SaveInformation())
                {
                    if (MblnAddStatus)
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                        lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSettings.Enabled = true;
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetRecordCount();
                    }
                    else
                    {
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
                        lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSettings.Enabled = true;
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    MblnChangeStatus = false;
                    MblnAddStatus = false;

                    SetBindingNavigatorButtons();
                }
            }
        }


        private void SetBindingNavigatorButtons()
        {

            int iRecordCnt = MintRecordCnt;  // Total count of the records


            BindingNavigatorDeleteItem.Enabled = true;


            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;

            lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            BtnSave.Enabled = false;
            BtnOk.Enabled = false;
            BindingNavigatorClearItem.Enabled = false;
            BindingNavigatorSaveItem.Enabled = false;
        }

        private bool CompanySettingsValidation()
        {
            ErrorProSettings.Clear();


            if (cboCompany.SelectedIndex ==-1)
            {

                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrorProSettings.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSettings.Enabled = true;
                    return false;
            }


            return true;
        }
        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }

        private bool SaveInformation()
        {
            try
            {
                MObjclsBLLCompanySettings.clsDTOCompanySettingsMaster.iCompanyID = cboCompany.SelectedValue.ToInt32();
                MObjclsBLLCompanySettings.DeleteDetails();
                MObjclsBLLCompanySettings.clsDTOCompanySettingsMaster.lstCompanySettingsDetails = new List<clsCompanySettingsDetails>();

                for (int i = 0; i < dgvCompanySettings.Rows.Count; i++)
                {

                    if (dgvCompanySettings["AutoNo", i].Value == null)
                    {
                        dgvCompanySettings["AutoNo", i].Value = false; 
                    }
                    if (dgvCompanySettings["PreFix", i].Value == null)
                    {
                        dgvCompanySettings["PreFix", i].Value = "";
                    }
                    if (dgvCompanySettings["AutoNo", i].Value.ToBoolean() || dgvCompanySettings["PreFix", i].Value.ToString().Trim() !="")
                    {
                        clsCompanySettingsDetails objDTOUserCompanyDetails = new clsCompanySettingsDetails();
                        if (dgvCompanySettings["AutoNo", i].Value.ToBoolean())
                        {
                            objDTOUserCompanyDetails.bIsAuto = 1;
                        }
                        else
                        {
                            objDTOUserCompanyDetails.bIsAuto = 0;
                        }
                        objDTOUserCompanyDetails.iFormID = dgvCompanySettings["ColFormID", i].Value.ToInt32();
                        objDTOUserCompanyDetails.iStartNumber = dgvCompanySettings["ColtxtStartNumber", i].Value.ToInt32();
                        objDTOUserCompanyDetails.strPreFix = dgvCompanySettings["PreFix", i].Value.ToString();
                        if (dgvCompanySettings["NoLength", i].Value.ToInt32() == 0)
                        {
                            dgvCompanySettings["NoLength", i].Value = objDTOUserCompanyDetails.iStartNumber.ToStringCustom().Length;   
                        }
                        objDTOUserCompanyDetails.NoLength = dgvCompanySettings["NoLength", i].Value.ToInt32(); 
                        MObjclsBLLCompanySettings.clsDTOCompanySettingsMaster.lstCompanySettingsDetails.Add(objDTOUserCompanyDetails);
                    }

                }
         

                if (MObjclsBLLCompanySettings.SaveDetails())
                {
                    return true;
                }
                return false;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form Save:SaveInformation() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Save SaveInformation() " + Ex.Message.ToString());
                return false;
            }

        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void BindingNavigatorClearItem_Click(object sender, EventArgs e)
        {
            ClearControls(); 
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (MObjclsBLLCompanySettings.DeleteDetails())
                    {
                        MintRecordCnt = MintRecordCnt - 1;
                        MintCurrentRecCnt = MintCurrentRecCnt - 1;
                        MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                        lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrSettings.Enabled = true;
                        AddNewItem();
                    }
                }
            }
            catch
            {
            }
        }

        private void dgvCompanySettings_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {

            }
            catch
            {
            }

        }

        //private void dgvCompanySettings_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        //{
        //    try
        //    {
        //        ChangeStatus();
        //    }
        //    catch
        //    {
        //    }


        //}

        private void BtnOk_Click(object sender, EventArgs e)
        {

            MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
            if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                this.Close(); 
            if (CompanySettingsValidation())
            {
                if (SaveInformation())
                {

                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
                    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrSettings.Enabled = true;
                    MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GetRecordCount();

                    MblnChangeStatus = false;
                    MblnAddStatus = false;
                    this.Close(); 

                }
            }
        }

        private void dgvCompanySettings_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void dgvCompanySettings_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvCompanySettings.IsCurrentCellDirty)
                {
                    dgvCompanySettings.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        void txtInt_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}.{:?></,`-=\\[]";
            e.Handled = false;
            if ((strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0))
            {
                e.Handled = true;
            }
        }

        private void dgvCompanySettings_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (dgvCompanySettings.CurrentCell != null)
                {

                    if (dgvCompanySettings.CurrentCell.ColumnIndex == ColtxtStartNumber.Index || dgvCompanySettings.CurrentCell.ColumnIndex == NoLength.Index)
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress += new KeyPressEventHandler(txtInt_KeyPress);
                    }
                    else
                    {
                        TextBox txt = (TextBox)(e.Control);
                        txt.KeyPress -= new KeyPressEventHandler(txtInt_KeyPress);
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
        }

    }
}
