﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data.SqlClient;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,9 Mar 2011>
Description:	<Description,EmailPopUp Form>
================================================
*/
namespace MyPayfriend
{
    public partial class FrmEmailPopup : DevComponents.DotNetBar.Office2007Form
    {
        #region Variables Declaration

        public string MsSubject;
        private string FromMailID = "";
        private string PWD = "";
        private string SmtpServerAddress = "";
        private bool  EnableSSL = false;
        private int PortNumber = 0;

      
        public string mstrscanfilepath  = "";

        public long MiRecordID;
      
        public EmailFormID EmailFormType;

        public DataSet EmailSource;
        public DataTable DtHistory;

        string MstrMessageCommon, MstrMessageCaption;

        bool MblnStatus, MblnLoad = false;

        ArrayList MaMessageArr, MaStatusMessage;

        MessageBoxIcon MsMessageBoxIcon;

        ClsNotification MobjNotification;
        ClsLogWriter MobjLogWriter;
        ClsWebform MobjWebForm;
        //ClsEmailSettings MobjEmailSettings;
        clsBLLEmailPopUp MobjclsBLLEmailPopUp;

        #endregion Variables Declaration

        public FrmEmailPopup()
        {
            #region Constructor

            InitializeComponent();
            MiRecordID = 0;
            MsSubject = string.Empty;        
            MstrMessageCaption = ClsCommonSettings.MessageCaption;
            MobjLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjNotification = new ClsNotification();
            MobjWebForm = new ClsWebform();
            MobjclsBLLEmailPopUp = new clsBLLEmailPopUp();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }




            #endregion //Constructor
        }


        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.EmailPopup, this);

        }

        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MobjNotification.FillMessageArray((int)FormID.EmailPopup, 4);
            MaStatusMessage = MobjNotification.FillStatusMessageArray((int)FormID.EmailPopup, 4);
        }

        private void enable()
        {
            CmbEmail.SelectedIndex = -1;
            CmbEmail.Text = string.Empty;
            ListEmail.Items.Clear();
            TxtTo.Clear();
            TxtTo.ReadOnly = !ClsCommonSettings.SendMailToAll;
        }

        private void FrmEmailPopup_Load(object sender, EventArgs e)
        {
            try
            {
                LoadMessage();
                LoadReportBody();
                LoadCombos(0);
                enable();

                TxtSubject.Text = MsSubject;

                if (mstrscanfilepath != "")
                {
                    TxtPath.Text = mstrscanfilepath;
                }
                else
                {
                    txtbody.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Load:FrmEmailPopup_Load" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmEmailPopup_Load " + Ex.Message.ToString());
            }
        }

        // Function
        private void LoadReportBody()
        {
            MobjWebForm.PRecID = Convert.ToInt32(MiRecordID);
            WebEmail.DocumentText = MobjWebForm.GetWebFormData(MsSubject, EmailFormType, EmailSource);
        }

        private bool LoadCombos(int intType)
        {
            // 0 - Loading EmailID Combo
            try
            {
                DataTable datCombos = new DataTable();
                if (intType == 0 || intType == 1)
                {
                    datCombos = MobjclsBLLEmailPopUp.FillCombos(new string[] { "EmployeeID,OfficialEmailID", "EmployeeDetails", "OfficialEmailID is not null and OfficialEmailID<> ''" });
                    CmbEmail.ValueMember = "EmployeeID";
                    CmbEmail.DisplayMember = "OfficialEmailID";
                    CmbEmail.DataSource = datCombos;
                }
                MobjLogWriter.WriteLog("Combobox filled successfully:LoadCombos " + this.Name + "", 0);
                return true;
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }

        private void CmbEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            CmbEmail.DroppedDown = false;
        }

        private void CmbEmail_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!MblnLoad)
            {
                MblnStatus = false;
                if (ListEmail.Items.Count == 0)
                {
                    if (CmbEmail.Text != "")
                    {
                        ListEmail.Items.Add(new ListItem(CmbEmail.Text), true);                       
                        sbSetaddress();
                    }
                }
                else
                {
                    for (int i = 0; i <= ListEmail.Items.Count - 1; i++)
                    {
                        if (ListEmail.Items[i].ToString() == CmbEmail.Text)
                        {
                            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4052, out MsMessageBoxIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                            MobjNotification.GetErrorMessage(MaMessageArr, 4052, out MsMessageBoxIcon);
                            MblnStatus = false;
                            break;
                        }
                        else
                        {
                            MblnStatus = true;
                        }
                    }
                    if (MblnStatus == true)
                    {
                        ListEmail.Items.Add(new ListItem(CmbEmail.Text.ToString()), true);
                        sbSetaddress();
                    }
                }
            }
        }

        private void sbSetaddress()
        {
            string strEmailId = "";

            for (int i = 0; i <= ListEmail.CheckedItems.Count - 1; i++)
            {
                strEmailId = strEmailId + ListEmail.CheckedItems[i].ToString().Trim() + ",";
            }
            if (strEmailId != "")
            {
                strEmailId = strEmailId.Substring(0, strEmailId.Length - 1);
            }
            TxtTo.Text = strEmailId;
        }

        private void ListEmail_SelectedValueChanged(object sender, EventArgs e)
        {
            sbSetaddress();
        }

        private bool Validation()
        {
            if (TxtTo.Text.Trim () == "")
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4051, out MsMessageBoxIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                MobjNotification.GetErrorMessage(MaMessageArr, 4051, out MsMessageBoxIcon);
                CmbEmail.Focus();
                return false;
            }
            //if (!new ClsInternet().IsInternetConnected())
            //{
            //    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4054, out MsMessageBoxIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            //    return false;
            //}
            return true;
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            try
            {
                string strBody = string.Empty;
                if (Validation())
                {
                    strBody = txtbody.Visible ? txtbody.Text.Trim() : WebEmail.DocumentText;
                    DataTable dtMailSettings = MobjclsBLLEmailPopUp.GetMailSettings();
                    if (dtMailSettings.Rows.Count > 0)
                    {
                        if (new clsSendmail().SendMail(dtMailSettings.Rows[0]["OutgoingServer"].ToStringCustom(), dtMailSettings.Rows[0]["PortNumber"].ToInt32(), 
                            dtMailSettings.Rows[0]["UserName"].ToStringCustom(), dtMailSettings.Rows[0]["Password"].ToStringCustom(), TxtTo.Text.Trim(), 
                            TxtSubject.Text.Trim(), strBody,Convert.ToBoolean(dtMailSettings.Rows[0]["EnableSsl"]),TxtPath.Text.Trim()))
                        {
                            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4053, out MsMessageBoxIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                        }
                        else
                        {
                            MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4055, out MsMessageBoxIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                        }
                    }
                }
            }
            catch
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4054, out MsMessageBoxIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            }
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            enable();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TxtPath.Text = "";
            TxtPath.Clear();
        }

      
    }
}