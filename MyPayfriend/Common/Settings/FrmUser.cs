﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

/* 
=================================================
Author      : <Sruthy K>
Create date : <31 August 2013>
Description : <UserInformation Form>
================================================
*/

namespace MyPayfriend
{
	public partial class FrmUser : DevComponents.DotNetBar.Office2007Form
	{
		#region Variables Declaration

		private bool MblnAddStatus;
		private bool MblnChangeStatus;
		private bool MblnAddPermission = true;//To Set Add Permission
		private bool MblnUpdatePermission = true;//To Set Update Permission
		private bool MblnDeletePermission = true;//To Set Delete Permission
		private bool MblnPrintEmailPermission = true;


		private string MstrMessageCaption;
		private string MstrMessageCommon;
		private MessageBoxIcon MmessageIcon;

		private int MintCurrentRecCnt;
		private int MintRecordCnt;

		private ArrayList MaMessageArr;                  // Error Message display
		private ArrayList MaStatusMessage;

		clsBLLUserInformation MObjclsBLLUserInformation;
		ClsLogWriter MObjClsLogWriter;
		ClsNotification MObjClsNotification;

        private bool MblnSearchStatus = false;

		#endregion

		#region Constructor
		public FrmUser()
		{
			InitializeComponent();
			MstrMessageCaption = ClsCommonSettings.MessageCaption;
			TmrUser.Interval = ClsCommonSettings.TimerInterval;
			MmessageIcon = MessageBoxIcon.Information;

			MObjclsBLLUserInformation = new clsBLLUserInformation();
			MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
			MObjClsNotification = new ClsNotification();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }


		}
		#endregion
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.User, this);

        }

		#region Events

		/// <summary>
		/// Page Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FrmUser_Load(object sender, EventArgs e)
		{
			MblnChangeStatus = false;
            SetAutoCompleteList();
			LoadCombos(0);
			LoadMessage();
			AddNewItem();
			TmrUser.Enabled = true;
			TmrFocus.Enabled = true;
			SetPermissions();
			ChangeStatus();
			MblnChangeStatus = false;
		}

        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dtSuggestions = MObjclsBLLUserInformation.GetAutoCompleteList(txtSearch.Text.Trim());
            this.txtSearch.AutoCompleteCustomSource = clsBLLInsuranceCard.ConvertToAutoCompleteCollection(dtSuggestions);
        }


		private void UserMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
		{
			if (UserInformationValidation())
			{
				if (SaveUserInformation())
				{
					if (MblnAddStatus)
					{
						MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
						lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrUser.Enabled = true;
                        MessageBox.Show(MstrMessageCommon.Replace("#",""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetRecordCount();
					}
					else
					{
						MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
						lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
						TmrUser.Enabled = true;
                        MessageBox.Show(MstrMessageCommon.Replace("#", ""), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
                    MblnChangeStatus = false;
					MblnAddStatus = false;
                    SetAutoCompleteList();
                    SetBindingNavigatorButtons();
				}
			}
		}

		private void BtnSave_Click(object sender, EventArgs e)
		{
			if (UserInformationValidation())
			{
				if (SaveUserInformation())
				{
					if (MblnAddStatus)
					{
						MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
						lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrUser.Enabled = true;
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetRecordCount();
					}
					else
					{
						MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
						lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrUser.Enabled = true;
                        MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
                    MblnChangeStatus = false;
                    MblnAddStatus = false;
                    SetAutoCompleteList();
                    SetBindingNavigatorButtons();
				}
			}
		}

		private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
		{
			try
			{
                if (!MblnSearchStatus)
                    txtSearch.Text = string.Empty;

                GetRecordCount();

				if (MintRecordCnt > 0)
				{
					MintCurrentRecCnt = MintCurrentRecCnt + 1;
					MblnAddStatus = false;
					if (MintCurrentRecCnt >= MintRecordCnt)
					{
						MintCurrentRecCnt = MintRecordCnt;
					}
				}
				else
				{
					MintCurrentRecCnt = 0;
				}

				DisplayUserInformation();
				MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 11, out MmessageIcon);
				SetBindingNavigatorButtons();
				MblnChangeStatus = false;
			}
			catch (Exception Ex)
			{
                MObjClsLogWriter.WriteLog("Error on MoveNextItem:MoveNextItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveNextItem_Click " + Ex.Message.ToString());
			}
		}

		private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
		{
			try
			{
                if (!MblnSearchStatus)
                    txtSearch.Text = string.Empty;

				ErrorProUser.Clear();
                GetRecordCount();

				if (MintRecordCnt > 0)
				{
					MintCurrentRecCnt = MintRecordCnt;
					MblnAddStatus = false;
				}
				else
				{
					MintCurrentRecCnt = 0;
				}

				DisplayUserInformation();
				MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 12, out MmessageIcon);
				SetBindingNavigatorButtons();
				MblnChangeStatus = false;
			}
			catch (Exception Ex)
			{
                MObjClsLogWriter.WriteLog("Error on MoveLastItem:MoveLastItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveLastItem_Click " + Ex.Message.ToString());
			}
		}

		private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
		{
			try
			{
                if (!MblnSearchStatus)
                    txtSearch.Text = string.Empty;

                GetRecordCount();
                if (MintRecordCnt > 0)
				{
					MblnAddStatus = false;
					MintCurrentRecCnt = MintCurrentRecCnt - 1;

					if (MintCurrentRecCnt <= 0)
					{
						MintCurrentRecCnt = 1;
					}
				}
				else
				{
					bindingNavigatorPositionItem.Text = "of 0";
					MintRecordCnt = 0;
				}

				DisplayUserInformation();
				MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 10, out MmessageIcon);
				SetBindingNavigatorButtons();
				MblnChangeStatus = false;
			}
			catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MovePreviousItem:MovePreviousItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MovePreviousItem_Click " + Ex.Message.ToString());
			}
		}

		private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
		{
			try
			{
                if (!MblnSearchStatus)
                    txtSearch.Text = string.Empty;

				ErrorProUser.Clear();
                GetRecordCount();
                if (MintRecordCnt > 0)
				{
					MblnAddStatus = false;
					MintCurrentRecCnt = 1;
				}
				else
					MintCurrentRecCnt = 0;

				DisplayUserInformation();
				MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9, out MmessageIcon);
				SetBindingNavigatorButtons();
				MblnChangeStatus = false;
			}
			catch (Exception Ex)
			{
                MObjClsLogWriter.WriteLog("Error on MoveFirstItem:MoveFirstItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveFirstItem_Click " + Ex.Message.ToString());
			}
		}

		private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
		{
			AddNewItem();
		}

		private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
		{
			bool blnDeleteStatus = true;
			try
			{
				int MintUserID = MObjclsBLLUserInformation.clsDTOUserInformation.intUserID;
				if (MintUserID == ClsCommonSettings.UserID)
				{
					MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3459, out MmessageIcon);
					MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
					lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
					TmrUser.Enabled = true;
					return;

				}
				else
				{
					if (DeleteValidation(MintUserID))
					{
						MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
						if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
						{
							if (MObjclsBLLUserInformation.DeleteUserInformation())
							{
								MintRecordCnt = MintRecordCnt - 1;
								MintCurrentRecCnt = MintCurrentRecCnt - 1;

								bindingNavigatorCountItem.Text = MintRecordCnt.ToString();
								bindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
								MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
								lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
								TmrUser.Enabled = true;
                                SetAutoCompleteList();
								AddNewItem();
							}
						}
					}
				}
			}
			catch (Exception Ex)
			{
				blnDeleteStatus = false;
				MObjClsLogWriter.WriteLog("Error on form Delete:DeleteItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

				if (ClsCommonSettings.ShowErrorMess)
					MessageBox.Show("Error on DeleteItem_Click " + Ex.Message.ToString());
			}
			if (blnDeleteStatus == false)
			{
				MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9004, out MmessageIcon);
				lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
				MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
			}
		}

		private void BtnOk_Click(object sender, EventArgs e)
		{
			if (UserInformationValidation())
			{
				if (SaveUserInformation())
				{
					if (MblnAddStatus == true)
					{
						MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2, out MmessageIcon);
						lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
						TmrUser.Enabled = true;
					}
					else
					{
						MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 21, out MmessageIcon);
						lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
						TmrUser.Enabled = true;
					}
					MblnChangeStatus = false;
					this.Close();
				}
			}
		}

        private void cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cboSender = (ComboBox)sender;
            if (cboSender != null)
                cboSender.DroppedDown = false;
        }

		private void BtnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}


        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            MblnSearchStatus = false;
        }

		private void FrmUser_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (MblnChangeStatus)
			{
				MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
				if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption,
					MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
				{
					e.Cancel = true;
				}
				else
				{
					e.Cancel = false;
				}
			}
		}

		private void TmUser_Tick(object sender, EventArgs e)
		{
			TmrUser.Enabled = false;
			lblUserStatus.Text = "";
		}

		private void TmFocus_Tick(object sender, EventArgs e)
		{
			txtUserName.Focus();
			TmrFocus.Enabled = false;
		}

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch_Click(sender, null);
            }
        }

		private void btnUserRoles_Click(object sender, EventArgs e)
		{
            //try
            //{
            //    // Calling Reference form for UserRoles
            //    FrmCommonRef objCommon = new FrmCommonRef("RoleName", new int[] { 1, 0 }, "RoleID, RoleName As RoleName", "RoleReference", "Type<>0");
            //    objCommon.ShowDialog();
            //    objCommon.Dispose();
            //    int intWorkingAt = Convert.ToInt32(ColCboRole.SelectedValue);
            //    LoadCombos(1);
            //    if (objCommon.NewID != 0)
            //        ColCboRole.SelectedValue = objCommon.NewID;
            //    else
            //        ColCboRole.SelectedValue = intWorkingAt;

            //}
            //catch (Exception Ex)
            //{
            //    MObjClsLogWriter.WriteLog("Error on Role_Click " + this.Name + " " + Ex.Message.ToString(), 3);

            //    if (ClsCommonSettings.ShowErrorMess)
            //        MessageBox.Show("Error on Role_Click " + Ex.Message.ToString());
            //}
		}

		private void FrmUser_KeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				switch (e.KeyData)
				{
					case Keys.F1:
						FrmHelp objHelp = new FrmHelp();
						objHelp.strFormName = "User";
						objHelp.ShowDialog();
						objHelp = null;
						break;
					case Keys.Escape:
						this.Close();
						break;

					case Keys.Control | Keys.Enter:
						if(BindingNavigatorAddNewItem.Enabled)
						bindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
						break;
					case Keys.Alt | Keys.R:
						if (BindingNavigatorDeleteItem.Enabled)
						bindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
						break;
					case Keys.Control | Keys.E:
						if (BindingNavigatorClearItem.Enabled)
						BindingNavigatorClearItem_Click(sender, new EventArgs());//Clear
						break;
					case Keys.Control | Keys.Left:
						if (BindingNavigatorMovePreviousItem.Enabled)
						bindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
						break;
					case Keys.Control | Keys.Right:
						if(BindingNavigatorMoveNextItem.Enabled)
						bindingNavigatorMoveNextItem_Click(sender, new EventArgs());
						break;
					case Keys.Control | Keys.Up:
						if (BindingNavigatorMoveFirstItem.Enabled)
						bindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
						break;
					case Keys.Control | Keys.Down:
						if (BindingNavigatorMoveLastItem.Enabled)
						bindingNavigatorMoveLastItem_Click(sender, new EventArgs());
						break;

				}
			}
			catch (Exception)
			{
			}
		}

		private void btnEmployee_Click(object sender, EventArgs e)
		{
			using (FrmEmployee objEmployee = new FrmEmployee())
			{
				objEmployee.PintEmployeeID = Convert.ToInt32(cboEmployee.SelectedValue);
				objEmployee.ShowDialog();
			}

			int intEmployeeID = Convert.ToInt32(cboEmployee.SelectedValue);
			LoadCombos(2);
			cboEmployee.SelectedValue = intEmployeeID;
		}

		private void bnRoleSettings_Click(object sender, EventArgs e)
		{
			try
			{
				frmRole objRoleSettings = new frmRole();
				objRoleSettings.ShowDialog();
				objRoleSettings.Dispose();
                //int WorkingID = Convert.ToInt32(ColCboRole.SelectedValue);
				LoadCombos(1);
			}
			catch (Exception)
			{
			}
		}


		private void BindingNavigatorClearItem_Click(object sender, EventArgs e)
		{
			txtUserName.Focus();
			txtUserName.Text = "";
			txtPassword.Text = "";
			txtEmailID.Text = "";
			cboEmployee.Text = "";
            txtSearch.Text = "";
			cboEmployee.SelectedIndex = -1;
            //ColCboRole.SelectedIndex = -1;
		}

		private void btnHelp_Click(object sender, EventArgs e)
		{
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "User"; Help.ShowDialog(); }
		}

		#endregion

		#region Functions
		/// <summary>
		/// Set Permissions for buttons
		/// </summary>
		private void ChangeStatus()
		{
            //if (Convert.ToString(ColCboRole.SelectedValue) == "1" || Convert.ToString(ColCboRole.SelectedValue) == "2")
            //{
            //    cboEmployee.BackColor = SystemColors.Window;
            //}
            //else
            //{
            //    cboEmployee.BackColor = SystemColors.Info;
            //}
			MblnChangeStatus = true;
			if (MblnAddStatus == true)
			{
				BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnAddPermission;
				BindingNavigatorDeleteItem.Enabled = false;
			}
			else
			{
				BtnOk.Enabled = BtnSave.Enabled = BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
				BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
			}
			ErrorProUser.Clear();
		}

		/// <summary>
		/// Validate User Deletion
		/// </summary>
		/// <param name="intUserID"></param>
		/// <returns></returns>
		private bool DeleteValidation(int intUserID)
		{
            int intDel =0;
			DataTable dtIds = MObjclsBLLUserInformation.CheckExistReferences(intUserID);
			if (dtIds.Rows.Count > 0)
			{
				if (Convert.ToString(dtIds.Rows[0]["FormName"]) != "0")
				{
					MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon).Replace("*", "User");

					MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
					TmrUser.Enabled = true;
					return false;
				}
			}
            intDel = MObjclsBLLUserInformation.DeleteUserValidation();

            if (intDel != 0)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9001, out MmessageIcon).Replace("*", "User");

                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                TmrUser.Enabled = true;
                return false;

            }
            
			return true;
		}


		/// <summary>
		/// Change Status
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ChangeStatus(object sender, EventArgs e) // to set the event
		{
			ChangeStatus();
		}

		/// <summary>
		/// Set Permissions 
		/// </summary>
		private void SetPermissions()
		{
			clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();

			if (ClsCommonSettings.RoleID > 3)
			{
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Setings, (Int32)eMenuID.User, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

                DataTable datPermission = objClsBLLPermissionSettings.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID);

				if (datPermission != null)
				{
					//datPermission.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.Employee;
					//btnEmployee.Enabled = (datPermission.DefaultView.ToTable().Rows.Count > 0);

					datPermission.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.Role;
					btnRoleSettings.Enabled = (datPermission.DefaultView.ToTable().Rows.Count > 0);
				}

			}
			else
			{
				MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
				btnRoleSettings.Enabled =  true;
			}

		}

		/// <summary>
		/// Add New User
		/// </summary>
		/// <returns></returns>
		public bool AddNewItem()
		{
			MblnAddStatus = true;
			TmrUser.Enabled = true;
			LoadCombos(2);
			ClearControls();
			MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3456, out MmessageIcon);
			lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            GetRecordCount();
			BindingAddNew();
			EnableDisableButtons();
			txtUserName.Focus();
			return true;
		}

        private void GetRecordCount()
        {
            MintRecordCnt = MObjclsBLLUserInformation.RecCountNavigate(txtSearch.Text.Trim());
        }

		/// <summary>
		/// Bind Navigator in Add New Mode
		/// </summary>
		private void BindingAddNew()
		{
			bindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt + 1) + "";
			bindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
			MintCurrentRecCnt = MintRecordCnt + 1;
			BindingNavigatorMoveFirstItem.Enabled = true;
			BindingNavigatorMovePreviousItem.Enabled = true;
			BindingNavigatorMoveNextItem.Enabled = false;
			BindingNavigatorMoveLastItem.Enabled = false;
		}

		/// <summary>
		/// Set Navigator Buttons
		/// </summary>
		private void SetBindingNavigatorButtons()
		{
			bindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
			bindingNavigatorCountItem.Text = "of " + Convert.ToString(MintRecordCnt) + "";

			BindingNavigatorMoveNextItem.Enabled = true;
			BindingNavigatorMoveLastItem.Enabled = true;
			BindingNavigatorMovePreviousItem.Enabled = true;
			BindingNavigatorMoveFirstItem.Enabled = true;

			int iCurrentRec = Convert.ToInt32(bindingNavigatorPositionItem.Text); // Current position of the record
			int iRecordCnt = MintRecordCnt;  // Total count of the records

			if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
			{
				BindingNavigatorMoveNextItem.Enabled = false;
				BindingNavigatorMoveLastItem.Enabled = false;
			}
			if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
			{
				BindingNavigatorMoveFirstItem.Enabled = false;
				BindingNavigatorMovePreviousItem.Enabled = false;
			}
			BindingNavigatorAddNewItem.Enabled = true;
			BindingNavigatorDeleteItem.Enabled = true;

			BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
			BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;

			lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
			BtnSave.Enabled = false;
			BtnOk.Enabled = false;
            BindingNavigatorClearItem.Enabled = false;
			BindingNavigatorSaveItem.Enabled = false;
		}

		/// <summary>
		/// Enable/disable buttons according to the action
		/// </summary>
		private void EnableDisableButtons()
		{
            BindingNavigatorAddNewItem.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = MblnChangeStatus = false;
            BindingNavigatorClearItem.Enabled = true;
		}

		/// <summary>
		/// Clear Form Controls
		/// </summary>
		private void ClearControls()
		{
			txtUserName.Text = "";
			txtPassword.Text = "";
			txtEmailID.Text = "";
			cboEmployee.Text = "";
			cboEmployee.SelectedIndex = -1;
            //ColCboRole.SelectedIndex = -1;
			txtUserName.Tag = 0;
            txtSearch.Text = "";
            MblnSearchStatus = false;
            ClearCompanyList(); 
		}

		/// <summary>
		/// Load Message
		/// </summary>
		private void LoadMessage()
		{
			MaMessageArr = new ArrayList();
			MaStatusMessage = new ArrayList();
			MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.User, ClsCommonSettings.ProductID);
			MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.User, ClsCommonSettings.ProductID);
		}

		/// <summary>
		/// Loading comboboxes with values
		/// </summary>
		/// <returns></returns>
		/// 
		private bool LoadCombos(int intType)
		{
			//  0 - For Loading All Combo
			//  1 - For Loading CboRoleID 
			//  2-  For Loading CboEmployeeID

			try
			{
				DataTable datCombos = new DataTable();

				if (intType == 0 || intType == 1)
				{
                    //if (ClsCommonSettings.IsHrPowerEnabled)
                    //{
                        datCombos = MObjclsBLLUserInformation.FillCombos(new string[] { "RoleID,RoleName", "RoleReference", "RoleID NOT IN(1) ORDER BY RoleName" });
                    //}
                    //else
                    //{
                    //    datCombos = MObjclsBLLUserInformation.FillCombos(new string[] { "RoleID,RoleName", "RoleReference", "RoleID NOT IN(1,3,4,5) ORDER BY RoleName" });
                    //}
                    ////else
                    ////    datCombos = MObjclsBLLUserInformation.FillCombos(new string[] { "DISTINCT RR.RoleID,RR.RoleName", "RoleReference RR INNER JOIN RoleDetails RD ON RR.RoleID=RD.RoleID UNION ALL SELECT DISTINCT 2 AS RoleID,'Admin' AS RoleName FROM RoleReference RR", "RR.RoleID NOT IN(1) ORDER BY RR.RoleName" });

					ColCboRole.ValueMember = "RoleID";
					ColCboRole.DisplayMember = "RoleName";
					ColCboRole.DataSource = datCombos;
				}

				if (intType == 0 || intType == 2)
				{
                    datCombos = MObjclsBLLUserInformation.FillCombos(new string[] { "EmployeeID,EmployeeFullName +'-'+ EmployeeNumber + ' ['+C.ShortName+ ']' AS FirstName", "EmployeeMaster E INNER JOIN CompanyMaster C ON E.CompanyID=C.CompanyID ", "E.WorkStatusID > 5 AND E.EmployeeID NOT IN (SELECT DISTINCT ISNULL(EmployeeID,0) FROM UserMaster) Order by E.CompanyID" });
					cboEmployee.ValueMember = "EmployeeID";
					cboEmployee.DisplayMember = "FirstName";
					cboEmployee.DataSource = datCombos;
				}
                if (intType == 0 || intType == 3)
                {
                    datCombos = MObjclsBLLUserInformation.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "" });
                    CboCompany.ValueMember = "CompanyID";
                    CboCompany.DisplayMember = "CompanyName";
                    CboCompany.DataSource = datCombos;
                }

				return true;
			}

			catch (Exception)
			{
				return false;
			}
		}

		/// <summary>
		/// Fill Employee in Update Mode
		/// </summary>
		private void FillEmployee()
		{
            DataTable datCombos = MObjclsBLLUserInformation.FillCombos(new string[] { "EmployeeFullName +'-'+ EmployeeNumber + ' ['+C.ShortName+ ']'  AS FirstName,EmployeeID,C.CompanyID", "" +
                "EmployeeMaster INNER JOIN CompanyMaster C ON EmployeeMaster.CompanyID=C.CompanyID", "EmployeeID = " + MObjclsBLLUserInformation.clsDTOUserInformation.intEmployeeID + "UNION " +
                "SELECT EmployeeFullName +'-'+ EmployeeNumber + ' ['+C.ShortName+ ']' AS FirstName,EmployeeID,C.CompanyID FROM EmployeeMaster  INNER JOIN CompanyMaster C ON EmployeeMaster.CompanyID=C.CompanyID WHERE " +
                "EmployeeID NOT IN (SELECT DISTINCT ISNULL(EmployeeID,0) FROM UserMaster)  ORDER BY C.CompanyID "});
			cboEmployee.DisplayMember = "FirstName";
			cboEmployee.ValueMember = "EmployeeID";
			cboEmployee.DataSource = datCombos;
		}

		/// <summary>
		/// Validate Form
		/// </summary>
		/// <returns></returns>
		private bool UserInformationValidation()
		{
			ErrorProUser.Clear();

               
			if (txtUserName.Text.Trim() == "")
			{
				MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3451, out MmessageIcon);
				MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
				ErrorProUser.SetError(txtUserName, MstrMessageCommon.Replace("#", "").Trim());
				lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
				TmrUser.Enabled = true;
				txtUserName.Focus();
				return false;
			}

			if (MObjclsBLLUserInformation.CheckDuplication(MblnAddStatus, txtUserName.Text.Trim(), txtUserName.Tag.ToInt32()))
			{
				MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3457, out MmessageIcon);
				MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
				ErrorProUser.SetError(txtUserName, MstrMessageCommon.Replace("#", "").Trim());
				lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
				TmrUser.Enabled = true;
				txtUserName.Focus();
				return false;
			}

			if (txtPassword.Text.Trim() == "")
			{
				MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3452, out MmessageIcon);
				MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
				ErrorProUser.SetError(txtPassword, MstrMessageCommon.Replace("#", "").Trim());
				lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
				TmrUser.Enabled = true;

				txtPassword.Focus();
				return false;
			}
            if (cboEmployee.SelectedValue.ToInt32() == 0)
            {
                if (txtUserName.Tag.ToString() != "1" && txtUserName.Tag.ToString() != "2")
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3458, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrorProUser.SetError(cboEmployee, MstrMessageCommon.Replace("#", "").Trim());
                    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrUser.Enabled = true;
                    return false;
                }
            }


            if (cboEmployee.SelectedIndex > -1)
            {
                MObjclsBLLUserInformation.clsDTOUserInformation.intEmployeeID = cboEmployee.SelectedValue.ToInt32();
                if (MObjclsBLLUserInformation.IsEmployeeExists().ToInt32()>0)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 20012, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    ErrorProUser.SetError(cboEmployee, MstrMessageCommon.Replace("#", "").Trim());
                    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrUser.Enabled = true;
                    cboEmployee.Focus();
                    return false;
                }
            }



            bool  bRole = false;
            for (int i = 0; i < dgvCompany.Rows.Count-1; i++)
            {
                if (dgvCompany["CboCompany", i].Value.ToInt32() == 0)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrUser.Enabled = true;
                    return false;
                }
                if (dgvCompany["ColCboRole", i].Value.ToInt32() == 0)
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3453, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrUser.Enabled = true;
                    return false;
                }
                if(CheckGridDuplication(dgvCompany.Rows[i],i))
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 31, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrUser.Enabled = true;
                    return false;
                }
                bRole = true;
            }

            if (!bRole)
            {
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3453, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrUser.Enabled = true;
                return false;
            }


            //if (txtEmailID.Text.Trim().Length > 0 && !MObjclsBLLUserInformation.CheckValidEmail(txtEmailID.Text.Trim()))
            //{
            //    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3454, out MmessageIcon);
            //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
            //    ErrorProUser.SetError(txtEmailID, MstrMessageCommon.Replace("#", "").Trim());
            //    lblUserStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            //    TmrUser.Enabled = true;
            //    txtEmailID.Focus();
            //    return false;
            //}

			return true;
		}

        private bool CheckGridDuplication(DataGridViewRow rowToCompare,int iRowindex)
        {
            for (int iRow = iRowindex + 1; iRow < dgvCompany.Rows.Count; iRow++)
            {
                DataGridViewRow row = dgvCompany.Rows[iRow];
                bool bDuplicate = false;
                if (rowToCompare.Cells["CboCompany"].Value.Equals(row.Cells["CboCompany"].Value))
                    bDuplicate = true;
                if (bDuplicate)
                {
                    rowToCompare.DefaultCellStyle.BackColor = Color.Maroon;
                    rowToCompare.DefaultCellStyle.ForeColor = Color.White;
                    return true;
                }
                else
                {
                    rowToCompare.DefaultCellStyle.BackColor = Color.White;
                    rowToCompare.DefaultCellStyle.ForeColor = Color.Black;
                }
            }
            return false;
        }
		/// <summary>
		/// Save User Information
		/// </summary>
		/// <returns></returns>
		private bool SaveUserInformation()
		{
			try
			{
				if (MblnAddStatus == true)//insert 
				{
					MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);
					MObjclsBLLUserInformation.clsDTOUserInformation.intUserID = 0;
				}
				//Update
				else
				{
					MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
					MObjclsBLLUserInformation.clsDTOUserInformation.intUserID = txtUserName.Tag.ToInt32();
				}

				if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
					return false;

                //MObjclsBLLUserInformation.clsDTOUserInformation.intRoleID = Convert.ToInt32(ColCboRole.SelectedValue);


				MObjclsBLLUserInformation.clsDTOUserInformation.strUserName = Convert.ToString(txtUserName.Text.Trim());
				MObjclsBLLUserInformation.clsDTOUserInformation.strPassword = clsBLLCommonUtility.Encrypt(txtPassword.Text.Trim(), ClsCommonSettings.strEncryptionKey);
				MObjclsBLLUserInformation.clsDTOUserInformation.strEmailID = Convert.ToString(txtEmailID.Text.Trim());
				MObjclsBLLUserInformation.clsDTOUserInformation.intEmployeeID = Convert.ToInt32(cboEmployee.SelectedValue);




                MObjclsBLLUserInformation.clsDTOUserInformation.lstUserInformation = new List<clsDTOUserCompanyDetails>();


                for (int i = 0; i < dgvCompany.Rows.Count-1; i++)
                {
                    if (dgvCompany["ColCboRole", i].Value.ToInt32() > 0)
                    {
                        clsDTOUserCompanyDetails objDTOUserCompanyDetails = new clsDTOUserCompanyDetails();
                        objDTOUserCompanyDetails.CompanyID = dgvCompany["CboCompany", i].Value.ToInt32();
                        objDTOUserCompanyDetails.RoleID = dgvCompany["ColCboRole", i].Value.ToInt32();
                        MObjclsBLLUserInformation.clsDTOUserInformation.lstUserInformation.Add(objDTOUserCompanyDetails);
                    }
                    
                }



				if (MObjclsBLLUserInformation.SaveUser(MblnAddStatus))
				{
					txtUserName.Tag = MObjclsBLLUserInformation.clsDTOUserInformation.intUserID;
					return true;
				}
				return false;
			}
			catch (Exception Ex)
			{
				MObjClsLogWriter.WriteLog("Error on form Save:SaveUserInformation() " + this.Name + " " + Ex.Message.ToString(), 2);

				if (ClsCommonSettings.ShowErrorMess)
					MessageBox.Show("Error on Save SaveUserInformation() " + Ex.Message.ToString());
				return false;
			}

		}

		/// <summary>
		/// Display User Information
		/// </summary>
		public void DisplayUserInformation()
		{
			int intRowNumber = MintCurrentRecCnt;
            if (MObjclsBLLUserInformation.DisplayUser(intRowNumber,txtSearch.Text.Trim()))
			{
				txtUserName.Tag = MObjclsBLLUserInformation.clsDTOUserInformation.intUserID;
				txtUserName.Text = MObjclsBLLUserInformation.clsDTOUserInformation.strUserName.ToString();
				txtPassword.Text = clsBLLCommonUtility.Decrypt(MObjclsBLLUserInformation.clsDTOUserInformation.strPassword.ToString(), ClsCommonSettings.strEncryptionKey);
				FillEmployee();
                if (MObjclsBLLUserInformation.clsDTOUserInformation.intEmployeeID.ToInt32() > 0)
                {
                    cboEmployee.SelectedValue = MObjclsBLLUserInformation.clsDTOUserInformation.intEmployeeID.ToString();
                }
                else
                {
                    cboEmployee.SelectedIndex = -1;
                    cboEmployee.Text = "";
                }
				txtEmailID.Text = MObjclsBLLUserInformation.clsDTOUserInformation.strEmailID.ToString();

                ClearCompanyList();
			}
		}
		#endregion

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text == string.Empty)
                MblnSearchStatus = false;
            else
                MblnSearchStatus = true;

            GetRecordCount();

            if (MintRecordCnt == 0) // No records
            {
                MessageBox.Show(MObjClsNotification.GetErrorMessage(MaMessageArr, 40, out MmessageIcon).Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSearch.Text = string.Empty;
            }
            else if (MintRecordCnt > 0)
            {
                BindingAddNew();
                bindingNavigatorMoveFirstItem_Click(sender, e);
            }
        }
        private void ClearCompanyList()
        {
            try
            {
                dgvCompany.Rows.Clear();
                DataTable dt = MObjclsBLLUserInformation.getUserCompanyDetails();
                
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgvCompany.Rows.Add(dt.Rows[i]["CompanyID"],dt.Rows[i]["RoleID"]);
                    //dgvCompany.Rows[dgvCompany.Rows.Count - 1].Cells[0].Value = dt.Rows[intCnt]["CompanyID"];
                    //dgvCompany.Rows[dgvCompany.Rows.Count - 1].Cells[1].Value = dt.Rows[intCnt]["Company"];
                    //dgvCompany.Rows[dgvCompany.Rows.Count - 1].Cells[2].Value = dt.Rows[intCnt]["RoleID"];
                }
                
                
            }
            catch
            {
            }
        }

        private void chkPermittedCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dgvCompany_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                dgvCompany.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch
            {
            }
        }

        private void dgvCompany_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void dgvCompany_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    ChangeStatus();
                    if (dgvCompany.CurrentCell.ColumnIndex == 2)
                    {
                        dgvCompany.CurrentCell.Value = null;
                        dgvCompany.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
            catch 
            {
            }
        }

       
	}
}









