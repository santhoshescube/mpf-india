﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace MyPayfriend
{
    public partial class FrmChangePassword : Form
    {
        #region DECLARATIONS
        //FormID=95
        private int MintTimerInterval;
        private string MstrMessageCommon = "";
        private string MstrMessageCaption;
        private MessageBoxIcon MmessageIcon;
        private clsBLLChangePassword MobjclsBLLChangePassword;
        private ClsLogWriter MobjClsLogWriter;
        private ClsNotification MObjClsNotification;
        ArrayList MaMessageArr;
        ArrayList MaStatusMessage;
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false; // To set add/update permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission
        #endregion
        #region CONSTRUCTOR
        public FrmChangePassword()
        {
            InitializeComponent();
            MobjclsBLLChangePassword = new clsBLLChangePassword();
            MobjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();
            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MstrMessageCaption = ClsCommonSettings.MessageCaption;    //Message caption
            tmrChangePassowrd.Interval = MintTimerInterval;   // Setting timer interval
            MmessageIcon = MessageBoxIcon.Information;
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }



        }
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.ChangePassword, this);

        }
        #endregion
        #region METHODS
        private bool FillParameter()
        {
            MobjclsBLLChangePassword.clsDTOChangepassword.strNewPassword = clsBLLCommonUtility.Encrypt(txtxRetypeNewpassword.Text.Trim(), ClsCommonSettings.strEncryptionKey);
            return true;
        }
        private bool ChangePassword()
        {
            //Updating PAssWord
            if (FormValidation())
            {
                FillParameter();
                if (MobjclsBLLChangePassword.ChangePassword())
                {
                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9453, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblChangePasswordStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrChangePassowrd.Enabled = true;
                    ClearAll();
                    GetUserDetails();
                }
            }
            return true;
        }
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Setings, (Int32)eMenuID.ChangePassword, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            }
            if (MblnAddPermission == true || MblnUpdatePermission == true)
                MblnAddUpdatePermission = true;
        }
        private void ClearAll()
        {
            txtxOldPAssword.Text = txtxNewPassword.Text = txtxRetypeNewpassword.Text = "";
        }
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.ChangePassword, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.ChangePassword, ClsCommonSettings.ProductID);
        }
        private bool GetUserDetails()
        {
            //For Initializing User Detials
            MobjclsBLLChangePassword.clsDTOChangepassword.intUserID = ClsCommonSettings.UserID;

            MobjclsBLLChangePassword.GetUserDetails();
            txtxUserName.Text = MobjclsBLLChangePassword.clsDTOChangepassword.strUserName;
            btnSave.Enabled = MblnAddUpdatePermission;
            BtnCancel.Enabled = MblnAddUpdatePermission;
            return true;
        }
        private void ShowHelp()
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "ChangePassword"; Help.ShowDialog(); }
        }

        private bool CurrentPassWordValidating()
        {
            bool blnretvalue = true;

            if (txtxOldPAssword.Text.Trim() != "")
            {
                if (txtxOldPAssword.Text.Trim() != clsBLLCommonUtility.Decrypt(MobjclsBLLChangePassword.clsDTOChangepassword.strCurrentPassword, ClsCommonSettings.strEncryptionKey))
                {
                    blnretvalue = false;
                }
            }
            else
            {
                blnretvalue = false;
            }
            return blnretvalue;
        }
        private bool ValidatingNewPassWord()
        {
            bool blnretvalue = false;
            if (txtxNewPassword.Text.Trim() != "" && txtxRetypeNewpassword.Text.Trim() != "")
            {
                if (txtxNewPassword.Text.Trim() == txtxRetypeNewpassword.Text.Trim())
                {
                    blnretvalue = true;
                }

            }

            return blnretvalue;
        }
        private bool FormValidation()
        {
            if (MobjclsBLLChangePassword.clsDTOChangepassword.intUserID == 0)
            {

                //Error message:Not a valid user
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9452, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errorProChangePassword.SetError(txtxUserName, MstrMessageCommon.Replace("#", "").Trim());
                lblChangePasswordStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrChangePassowrd.Enabled = true;
                txtxUserName.Focus();
                return false;
            }
            if (CurrentPassWordValidating() == false)
            {
                //Error message: Password entered is incorrect
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9450, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errorProChangePassword.SetError(txtxRetypeNewpassword, MstrMessageCommon.Replace("#", "").Trim());
                lblChangePasswordStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrChangePassowrd.Enabled = true;
                txtxRetypeNewpassword.Focus();
                return false;
            }
            if (ValidatingNewPassWord() == false)
            {
                //Error message: Mismatch in newpassword and retyped Password
                MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9451, out MmessageIcon);
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errorProChangePassword.SetError(txtxRetypeNewpassword, MstrMessageCommon.Replace("#", "").Trim());
                lblChangePasswordStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrChangePassowrd.Enabled = true;
                txtxRetypeNewpassword.Focus();
                return false;
            }

            return true;
        }
        #endregion
        #region EVENTS
        private void FrmChangePassword_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            GetUserDetails();
        }
       
        private void txtxOldPAssword_Leave(object sender, EventArgs e)
        {
            if (txtxOldPAssword.Text.Length > 0)
            {
                if (!CurrentPassWordValidating())
                {

                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9450, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    errorProChangePassword.SetError(txtxOldPAssword, MstrMessageCommon.Replace("#", "").Trim());
                    lblChangePasswordStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrChangePassowrd.Enabled = true;
                    txtxOldPAssword.Focus();


                }
            }
        }
        private void txtxRetypeNewpassword_Leave(object sender, EventArgs e)
        {
            if (txtxRetypeNewpassword.Text.Length > 0)
            {
                if (!ValidatingNewPassWord())
                {

                    MstrMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9451, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    errorProChangePassword.SetError(txtxRetypeNewpassword, MstrMessageCommon.Replace("#", "").Trim());
                    lblChangePasswordStatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    tmrChangePassowrd.Enabled = true;
                    txtxRetypeNewpassword.Focus();
                }
            }
        }
        private void tmrChangePassowrd_Tick(object sender, EventArgs e)
        { 
            tmrChangePassowrd.Enabled = false;
            errorProChangePassword.Clear();
            lblChangePasswordStatus.Text = "";
           
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            ChangePassword();
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void FrmChangePassword_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        ShowHelp();
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                                     
                }
            }
            catch (Exception)
            {
            }
        }
        private void FrmChangePassword_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            ShowHelp();
        }

        #endregion

       
    }
}
