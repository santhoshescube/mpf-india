﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System.Net.Mail;

using System.Data.SqlClient;

namespace MyPayfriend
{
    public partial class FrmEmailSettings : DevComponents.DotNetBar.Office2007Form
    {
        #region DECLARATIONS
        string MsMessageCommon;
        string MsMessageCaption;
        ArrayList MaMessageArray;
        ArrayList MaStatusMessage;
        MessageBoxIcon MsMessageBoxIcon;
        ClsNotification MObjNotification;
        ClsCommonUtility MObjCommonUtility;
        clsBLLEmailSettings MobjclsBLLEmailSettings;

        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false; // To set add/update permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission
        bool IsformClosed = false;
        #endregion
        #region CONSTRUCTOR
        public FrmEmailSettings()
        {
            InitializeComponent();

            MsMessageCaption = ClsCommonSettings.MessageCaption;
            MObjNotification = new ClsNotification();
            MObjCommonUtility = new ClsCommonUtility();
            MobjclsBLLEmailSettings = new clsBLLEmailSettings();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }


        }
        #endregion


        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.EmailSettings, this);

        }



        #region METHODS
        /// <summary>
        /// Loading the variables for messages
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArray = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArray = MObjNotification.FillMessageArray((int)FormID.EmailSettings, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjNotification.FillStatusMessageArray((int)FormID.EmailSettings, ClsCommonSettings.ProductID);
        }
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Setings, (Int32)eMenuID.EmailSettings, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
            {
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
            }
            if (MblnAddPermission == true || MblnUpdatePermission == true)
                MblnAddUpdatePermission = true;
        }
        /// <summary>
        ///Fill details of EMAIL and SMS settings
        /// </summary>
        public void GetEmailSmsSettings()
        {
            // Getting Email settings
            if (MobjclsBLLEmailSettings.GetEmailSmsSettings(1))
            {
                TxtEmailUserName.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.UserName;
                TxtEmailPassword.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.PassWord;
                TxtEmailIncomingServer.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.IncomingServer;
                TxtEmailOutgoingServer.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.OutgoingServer;
                TxtEmailPortNumber.Text = (MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber == 0 ? string.Empty : MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber.ToString());
                chkEmailSsl.Checked = MobjclsBLLEmailSettings.clsDTOEmailSettings.EnableSsl;
            }
            // Getting SMS settings
            
            if (MobjclsBLLEmailSettings.GetEmailSmsSettings(2))
            {
                TxtSmsUsername.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.UserName;
                TxtSmsPassword.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.PassWord;
                TxtSmsServer.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.IncomingServer;
                TxtSmsOutgoingserver.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.OutgoingServer;
                TxtSmsPortNumber.Text = MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber.ToString();
                chkSmsSsl.Checked = MobjclsBLLEmailSettings.clsDTOEmailSettings.EnableSsl;
            }
        }
        enum ErrorCode
        {
            UserNameValidate = 4001,
            PasswordValidate = 4002,
            IncomingServerValidate = 4003,
            OutgoingServerValidate = 4004,
            PortNumberValidate = 4005,
            UserNameValidEmail = 4006,
            SaveEmailMessage = 4007,
            SaveSmsMessage = 4008,
            TestFailed = 4009,
            TestSucceeded = 4010
        }
       /// <summary>
       /// Validation for Email settings
       /// </summary>
       /// <returns></returns>
        private bool ValidateEmailSettings()
        {
            bool bValidationOk = true;

            if (String.IsNullOrEmpty(TxtEmailUserName.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.UserNameValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailUserName, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailUserName.Focus();
                bValidationOk = false;
            }
            else if (!MObjCommonUtility.CheckValidEmail(TxtEmailUserName.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.UserNameValidEmail, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailUserName, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailUserName.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtEmailPassword.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PasswordValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailPassword, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailPassword.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtEmailIncomingServer.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.IncomingServerValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailIncomingServer, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailIncomingServer.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtEmailOutgoingServer.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.OutgoingServerValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtEmailOutgoingServer, MsMessageCommon.Replace("#", "").Trim());
                TxtEmailOutgoingServer.Focus();
                bValidationOk = false;
            }
            //else if (String.IsNullOrEmpty(TxtEmailPortNumber.Text))
            //{
            //    MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PortNumberValidate, out MsMessageBoxIcon);
            //    ErrorEmailSettings.SetError(TxtEmailPortNumber, MsMessageCommon.Replace("#", "").Trim());
            //    TxtEmailPortNumber.Focus();
            //    bValidationOk = false;
            //}
            //else if ((TxtEmailPortNumber.Text.Trim().Length > 0 ? Convert.ToInt32(TxtEmailPortNumber.Text) : 0) == 0)
            //{
            //    MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PortNumberValidate, out MsMessageBoxIcon);
            //    ErrorEmailSettings.SetError(TxtEmailPortNumber, MsMessageCommon.Replace("#", "").Trim());
            //    TxtEmailPortNumber.Focus();
            //    bValidationOk = false;
            //}
            if (!bValidationOk)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            }
            return bValidationOk;
        }
        /// <summary>
        /// Validation for SMS settings
        /// </summary>
        /// <returns></returns>
        private bool ValidateSmsSettings()
        {
            bool bValidationOk = true;
            if (String.IsNullOrEmpty(TxtSmsUsername.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.UserNameValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsUsername, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsUsername.Focus();
                bValidationOk = false;
            }
            //else if (!MObjCommonUtility.CheckValidEmail(TxtSmsUsername.Text))
            //{
            //    MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.UserNameValidEmail, out MsMessageBoxIcon);
            //    ErrorEmailSettings.SetError(TxtSmsUsername, MsMessageCommon.Replace("#", "").Trim());
            //    TxtSmsUsername.Focus();
            //    bValidationOk = false;
            //}
            else if (String.IsNullOrEmpty(TxtSmsPassword.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PasswordValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsPassword, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsPassword.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtSmsServer.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.IncomingServerValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsServer, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsServer.Focus();
                bValidationOk = false;
            }
            else if (String.IsNullOrEmpty(TxtSmsOutgoingserver.Text))
            {
                MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.OutgoingServerValidate, out MsMessageBoxIcon);
                ErrorEmailSettings.SetError(TxtSmsOutgoingserver, MsMessageCommon.Replace("#", "").Trim());
                TxtSmsOutgoingserver.Focus();
                bValidationOk = false;
            }
            //else if (String.IsNullOrEmpty(TxtSmsPortNumber.Text))
            //{
            //    MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PortNumberValidate, out MsMessageBoxIcon);
            //    ErrorEmailSettings.SetError(TxtSmsPortNumber, MsMessageCommon.Replace("#", "").Trim());
            //    TxtSmsPortNumber.Focus();
            //    bValidationOk = false;
            //}
            //else if ((TxtSmsPortNumber.Text.Trim().Length > 0 ? Convert.ToInt32(TxtSmsPortNumber.Text) : 0) == 0)
            //{
            //    MsMessageCommon = MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.PortNumberValidate, out MsMessageBoxIcon);
            //    ErrorEmailSettings.SetError(TxtSmsPortNumber, MsMessageCommon.Replace("#", "").Trim());
            //    TxtSmsPortNumber.Focus();
            //    bValidationOk = false;
            //}
            if (!bValidationOk)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            }
            return bValidationOk;
        }
        #endregion
        #region EVENTS

        private void FrmEmailSettings_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            GetEmailSmsSettings();
            BtnSave.Enabled = MblnAddUpdatePermission;
            BtnCancel.Enabled = MblnAddUpdatePermission;
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (TabEmailSettings.SelectedTab == TpEmail)
            {
                if (ValidateEmailSettings())
                {
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.UserName = TxtEmailUserName.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.PassWord = TxtEmailPassword.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.IncomingServer = TxtEmailIncomingServer.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.OutgoingServer = TxtEmailOutgoingServer.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber = TxtEmailPortNumber.Text.Trim().Length > 0 ? Convert.ToInt32(TxtEmailPortNumber.Text) : 0;
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.EnableSsl = chkEmailSsl.Checked==true ? true:false;
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.AccountType = "Email";

                    if (MobjclsBLLEmailSettings.SaveEmailSettings(1))
                        MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.SaveEmailMessage, out MsMessageBoxIcon).Replace("#", "").Trim(),
                            MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                }
            }
            else
            {
                if (ValidateSmsSettings())
                {
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.UserName = TxtSmsUsername.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.PassWord = TxtSmsPassword.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.IncomingServer = TxtSmsServer.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.OutgoingServer = TxtSmsOutgoingserver.Text.Trim();
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.PortNumber = TxtSmsPortNumber.Text.Trim().Length > 0 ? Convert.ToInt32(TxtSmsPortNumber.Text) : 0;
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.EnableSsl = chkSmsSsl.Checked == true ? true : false;
                    MobjclsBLLEmailSettings.clsDTOEmailSettings.AccountType = "SMS";
                    if (MobjclsBLLEmailSettings.SaveEmailSettings(2))
                        MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.SaveSmsMessage, out MsMessageBoxIcon).Replace("#", "").Trim(),
                            MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                }
            }
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void ClearErrorProvider(object sender, EventArgs e)
        {
            ErrorEmailSettings.Clear();
        }
        private void FrmEmailSettings_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            using (FrmHelp Help = new FrmHelp())
            { Help.strFormName = "EmailSettings"; Help.ShowDialog(); }
        }
        private void FrmEmailSettings_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {

                    case Keys.Escape:
                        this.Close();
                        break;

                }
            }
            catch (Exception)
            {
            }
        }
        private void FrmEmailSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsformClosed = true;
        }

        /// <summary>
        /// Testing with internet connections -EMAIL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnTestSettings_Click(object sender, EventArgs e)
        {
            BtnTestSettings.Enabled = false;

            if (ValidateEmailSettings())
            {
                //this.Cursor = Cursors.WaitCursor;
                //ClsInternet objInternet = new ClsInternet();
                // if connected to internet
                //if (objInternet.IsInternetConnected())
                //{
                int iPort = 0;
                if (TxtEmailPortNumber.Text.Trim().Length > 0)
                    iPort = Convert.ToInt32(TxtEmailPortNumber.Text.Trim());
                bool bSsl = false;
                if (chkEmailSsl.Checked)
                    bSsl = true;

                bgEmail = new BackgroundWorker();
                bgEmail.DoWork -= new DoWorkEventHandler(bgEmail_DoWork);
                bgEmail.DoWork += new DoWorkEventHandler(bgEmail_DoWork);

                bgEmail.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(bgEmail_RunWorkerCompleted);
                bgEmail.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgEmail_RunWorkerCompleted);
                DataTable dt = new DataTable();
                dt.Columns.Add("OutgoingServer");
                dt.Columns.Add("UserName");
                dt.Columns.Add("Password");
                dt.Columns.Add("bSsl");
                dt.Columns.Add("iPort");

                DataRow dr = dt.NewRow();
                dr["OutgoingServer"] = TxtEmailOutgoingServer.Text;
                dr["UserName"] = TxtEmailUserName.Text;
                dr["Password"] = TxtEmailPassword.Text;
                dr["bSsl"] = bSsl;
                dr["iPort"] = iPort;
                bgEmail.RunWorkerAsync(dr);

                //if (new clsSendmail().SendMail(TxtEmailOutgoingServer.Text.Trim(), iPort, TxtEmailUserName.Text.Trim(), TxtEmailPassword.Text.Trim(),
                //        TxtEmailUserName.Text.Trim(), "Test Mail", "This is a test mail", bSsl))
                //{
                //    MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestSucceeded, out MsMessageBoxIcon).Replace("#", "").Trim(),
                //                           MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                //}
                //else
                //{
                //    MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestFailed, out MsMessageBoxIcon).Replace("#", "").Trim(),
                //       MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                //}
                //}
                //else
                //{
                //    MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestFailed, out MsMessageBoxIcon).Replace("#", "").Trim(),
                //       MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                //}

                //this.Cursor = Cursors.Default;
            }
        }
        void bgEmail_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                object result = e.Result;
                if (result != null)
                {
                    if ((result.ToInt32() == 1) && (!IsformClosed))
                        MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestSucceeded, out MsMessageBoxIcon).Replace("#", "").Trim(),
                                                MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    else
                        if ((result.ToInt32() == 2) && (!IsformClosed))
                            MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestFailed, out MsMessageBoxIcon).Replace("#", "").Trim(),
                          MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);

                }
                BtnTestSettings.Enabled = true;
            }
            catch (Exception)
            {

            }
        }
        void bgEmail_DoWork(object sender, DoWorkEventArgs e)
        {
            object status = 0;
            if (new ClsInternet().IsInternetConnected())
            {

                DataRow dr = ((DataRow)e.Argument);
                status = new clsSendmail().SendMail(dr["OutgoingServer"].ToString(), dr["iPort"].ToInt32(), dr["UserName"].ToString(), dr["Password"].ToString(),
                   dr["UserName"].ToString(), "Test Mail", "This is a test mail", dr["bSsl"].ToBoolean(),"");
                status = 1;
            }
            else
                status = 2;

            e.Result = status;
        }

        /// <summary>
        /// Testing with internet connections-SMS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void btnTestSms_Click(object sender, EventArgs e)
        {
            if (ValidateSmsSettings())
            {
                if (String.IsNullOrEmpty(txtSmsMessage.Text))
                {
                    MessageBox.Show("Please enter SMS message", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSmsMessage.Focus();
                    return;
                }
                else if (String.IsNullOrEmpty(txtSmsMobileNo.Text))
                {
                    MessageBox.Show("Please enter mobile number", MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSmsMobileNo.Focus();
                    return;
                }
                else
                {
                    this.Cursor = Cursors.WaitCursor;
                    ClsInternet objInternet = new ClsInternet();
                    if (objInternet.IsInternetConnected())
                    {
                        int iPort = 0;
                        if (TxtSmsPortNumber.Text.Trim().Length > 0)
                            iPort = Convert.ToInt32(TxtSmsPortNumber.Text.Trim());
                        bool bSsl = false;
                        if (chkSmsSsl.Checked)
                            bSsl = true;
                        string smsTo = txtSmsMobileNo.Text.Trim() + TxtSmsServer.Text.Trim();
                        if (new clsSendmail().SendSmsMail(TxtSmsUsername.Text.Trim(), TxtSmsPassword.Text.Trim(), TxtSmsOutgoingserver.Text.Trim(), iPort,
                                TxtSmsUsername.Text.Trim(), smsTo, "", txtSmsMessage.Text.Trim(), bSsl))
                        {
                            MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestSucceeded, out MsMessageBoxIcon).Replace("#", "").Trim(),
                                                   MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                        }
                        else
                        {
                            MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestFailed, out MsMessageBoxIcon).Replace("#", "").Trim(),
                               MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                        }
                    }
                    else
                    {
                        MessageBox.Show(MObjNotification.GetErrorMessage(MaMessageArray, (object)ErrorCode.TestFailed, out MsMessageBoxIcon).Replace("#", "").Trim(),
                           MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    }

                    this.Cursor = Cursors.Default;
                }
            }
        }
       
        #endregion

       
    }
}
