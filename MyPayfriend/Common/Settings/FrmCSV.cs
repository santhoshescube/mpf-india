﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
namespace MyPayfriend
{
    public partial class FrmCSV : Form
    {
        #region Declarations
      
        string MsMessageCaption;
       
              
        #endregion
        public FrmCSV()
        {
            InitializeComponent();

            
            MsMessageCaption = ClsCommonSettings.MessageCaption;
            
        }

        private void FrmUpdateAccruals_Load(object sender, EventArgs e)
        {
       
        }

       

        private void btnShow_Click(object sender, EventArgs e)
        {
            if (ClsCommonSettings.ThirdPartyIntegration)
            {
                DataTable dt = new DataTable();

                ArrayList alParameters = new ArrayList();
                alParameters.Add(new System.Data.SqlClient.SqlParameter("@TransactionDate", dtpToDate.Value.ToDateTime()));
                dt = new DataLayer().ExecuteDataTable("spPayIntegrationFile", alParameters);

                dgvEmployee.DataSource = dt;

            }
            btnCSV.Enabled = true;
            btnShow.Enabled = true;
            
        }
        

        

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            

        }
       

        private void dgvEmployee_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvEmployee_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void btnCSV_Click(object sender, EventArgs e)
        {
            if (ClsCommonSettings.ThirdPartyIntegration)
            {
                new clsDALGLCodeMapping(new DataLayer()).ExportToCSV(dtpToDate.Value.ToDateTime());
            }
        }
    }
}
