﻿namespace MyPayfriend
{
    partial class frmAlert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlert));
            this.chkAlladd = new System.Windows.Forms.CheckBox();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.btnNotify = new System.Windows.Forms.Button();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dgvAlerts = new System.Windows.Forms.DataGridView();
            this.lblAlertType = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.cboAlertType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtSearchKey = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnPrint = new DevComponents.DotNetBar.ButtonX();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.BarNavigation = new DevComponents.DotNetBar.Bar();
            this.ToolStripStatusLabel = new DevComponents.DotNetBar.LabelItem();
            this.GridNavigation = new DevComponents.DotNetBar.ItemContainer();
            this.cboPageCount = new DevComponents.DotNetBar.ComboBoxItem();
            this.BtnFirst = new DevComponents.DotNetBar.ButtonItem();
            this.BtnPrevious = new DevComponents.DotNetBar.ButtonItem();
            this.lblPageCount = new DevComponents.DotNetBar.LabelItem();
            this.BtnNext = new DevComponents.DotNetBar.ButtonItem();
            this.BtnLast = new DevComponents.DotNetBar.ButtonItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Message = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlerts)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BarNavigation)).BeginInit();
            this.SuspendLayout();
            // 
            // chkAlladd
            // 
            this.chkAlladd.AutoSize = true;
            this.chkAlladd.Location = new System.Drawing.Point(7, 4);
            this.chkAlladd.Name = "chkAlladd";
            this.chkAlladd.Size = new System.Drawing.Size(15, 14);
            this.chkAlladd.TabIndex = 21;
            this.chkAlladd.UseVisualStyleBackColor = true;
            this.chkAlladd.CheckedChanged += new System.EventHandler(this.chkAlladd_CheckedChanged);
            // 
            // dtpTo
            // 
            this.dtpTo.Checked = false;
            this.dtpTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(823, 28);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.ShowCheckBox = true;
            this.dtpTo.Size = new System.Drawing.Size(99, 20);
            this.dtpTo.TabIndex = 20;
            // 
            // lblFrom
            // 
            this.lblFrom.Location = new System.Drawing.Point(595, 28);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(58, 20);
            this.lblFrom.TabIndex = 18;
            this.lblFrom.Text = "From Date";
            this.lblFrom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTo
            // 
            this.lblTo.Location = new System.Drawing.Point(764, 28);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(58, 20);
            this.lblTo.TabIndex = 17;
            this.lblTo.Text = "To Date";
            this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNotify
            // 
            this.btnNotify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNotify.Location = new System.Drawing.Point(1076, 37);
            this.btnNotify.Name = "btnNotify";
            this.btnNotify.Size = new System.Drawing.Size(116, 23);
            this.btnNotify.TabIndex = 16;
            this.btnNotify.Text = "Do not notify again";
            this.btnNotify.UseVisualStyleBackColor = true;
            this.btnNotify.Click += new System.EventHandler(this.btnNotify_Click);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(1284, 25);
            this.ToolStrip1.TabIndex = 14;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(1195, 37);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dgvAlerts
            // 
            this.dgvAlerts.AllowUserToAddRows = false;
            this.dgvAlerts.AllowUserToDeleteRows = false;
            this.dgvAlerts.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvAlerts.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAlerts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAlerts.BackgroundColor = System.Drawing.Color.White;
            this.dgvAlerts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAlerts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk,
            this.Message,
            this.CommonID,
            this.DocumentTypeID});
            this.dgvAlerts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAlerts.Location = new System.Drawing.Point(0, 0);
            this.dgvAlerts.MultiSelect = false;
            this.dgvAlerts.Name = "dgvAlerts";
            this.dgvAlerts.RowHeadersVisible = false;
            this.dgvAlerts.Size = new System.Drawing.Size(1284, 433);
            this.dgvAlerts.TabIndex = 12;
            this.dgvAlerts.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvAlerts_CellMouseDoubleClick);
            // 
            // lblAlertType
            // 
            this.lblAlertType.Location = new System.Drawing.Point(390, 28);
            this.lblAlertType.Name = "lblAlertType";
            this.lblAlertType.Size = new System.Drawing.Size(55, 20);
            this.lblAlertType.TabIndex = 23;
            this.lblAlertType.Text = "Alert Type";
            this.lblAlertType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtpTo);
            this.panel1.Controls.Add(this.lblTo);
            this.panel1.Controls.Add(this.dtpFrom);
            this.panel1.Controls.Add(this.lblFrom);
            this.panel1.Controls.Add(this.cboAlertType);
            this.panel1.Controls.Add(this.txtSearchKey);
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Controls.Add(this.btnShow);
            this.panel1.Controls.Add(this.lblAlertType);
            this.panel1.Controls.Add(this.ToolStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1284, 75);
            this.panel1.TabIndex = 24;
            // 
            // dtpFrom
            // 
            this.dtpFrom.Checked = false;
            this.dtpFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(659, 28);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.ShowCheckBox = true;
            this.dtpFrom.Size = new System.Drawing.Size(99, 20);
            this.dtpFrom.TabIndex = 19;
            // 
            // cboAlertType
            // 
            this.cboAlertType.DisplayMember = "Text";
            this.cboAlertType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboAlertType.FormattingEnabled = true;
            this.cboAlertType.ItemHeight = 14;
            this.cboAlertType.Location = new System.Drawing.Point(451, 28);
            this.cboAlertType.Name = "cboAlertType";
            this.cboAlertType.Size = new System.Drawing.Size(121, 20);
            this.cboAlertType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboAlertType.TabIndex = 29;
            // 
            // txtSearchKey
            // 
            this.txtSearchKey.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearchKey.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearchKey.Border.Class = "TextBoxBorder";
            this.txtSearchKey.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearchKey.Location = new System.Drawing.Point(12, 28);
            this.txtSearchKey.Name = "txtSearchKey";
            this.txtSearchKey.Size = new System.Drawing.Size(364, 20);
            this.txtSearchKey.TabIndex = 28;
            this.txtSearchKey.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearchKey.WatermarkText = "Search by Employee Name/code/document type/document number";
            this.txtSearchKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchKey_KeyDown);
            this.txtSearchKey.TextChanged += new System.EventHandler(this.txtSearchKey_TextChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPrint.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPrint.Location = new System.Drawing.Point(1014, 22);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(68, 32);
            this.btnPrint.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPrint.TabIndex = 27;
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(940, 22);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(68, 32);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 27;
            this.btnShow.Text = "Show";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.chkAlladd);
            this.panel3.Controls.Add(this.dgvAlerts);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 75);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1284, 501);
            this.panel3.TabIndex = 26;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BarNavigation);
            this.panel2.Controls.Add(this.btnNotify);
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 433);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1284, 68);
            this.panel2.TabIndex = 27;
            // 
            // BarNavigation
            // 
            this.BarNavigation.AccessibleDescription = "DotNetBar Bar (BarNavigation)";
            this.BarNavigation.AccessibleName = "DotNetBar Bar";
            this.BarNavigation.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.BarNavigation.BackColor = System.Drawing.Color.White;
            this.BarNavigation.BarType = DevComponents.DotNetBar.eBarType.StatusBar;
            this.BarNavigation.CanCustomize = false;
            this.BarNavigation.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarNavigation.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.BarNavigation.DockSide = DevComponents.DotNetBar.eDockSide.Bottom;
            this.BarNavigation.DockTabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Right;
            this.BarNavigation.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle;
            this.BarNavigation.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ToolStripStatusLabel,
            this.GridNavigation});
            this.BarNavigation.Location = new System.Drawing.Point(0, 0);
            this.BarNavigation.Name = "BarNavigation";
            this.BarNavigation.Size = new System.Drawing.Size(1284, 34);
            this.BarNavigation.Stretch = true;
            this.BarNavigation.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.BarNavigation.TabIndex = 33;
            this.BarNavigation.TabStop = false;
            this.BarNavigation.Text = "Navigation";
            // 
            // ToolStripStatusLabel
            // 
            this.ToolStripStatusLabel.CanCustomize = false;
            this.ToolStripStatusLabel.Name = "ToolStripStatusLabel";
            this.ToolStripStatusLabel.PaddingLeft = 5;
            this.ToolStripStatusLabel.SingleLineColor = System.Drawing.SystemColors.MenuHighlight;
            this.ToolStripStatusLabel.Stretch = true;
            // 
            // GridNavigation
            // 
            // 
            // 
            // 
            this.GridNavigation.BackgroundStyle.Class = "";
            this.GridNavigation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GridNavigation.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.GridNavigation.Name = "GridNavigation";
            this.GridNavigation.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cboPageCount,
            this.BtnFirst,
            this.BtnPrevious,
            this.lblPageCount,
            this.BtnNext,
            this.BtnLast});
            this.GridNavigation.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle;
            // 
            // cboPageCount
            // 
            this.cboPageCount.DropDownHeight = 106;
            this.cboPageCount.ItemHeight = 17;
            this.cboPageCount.Name = "cboPageCount";
            this.cboPageCount.SelectedIndexChanged += new System.EventHandler(this.cboPageCount_SelectedIndexChanged);
            // 
            // BtnFirst
            // 
            this.BtnFirst.Image = global::MyPayfriend.Properties.Resources.ArrowLeftStart1;
            this.BtnFirst.Name = "BtnFirst";
            this.BtnFirst.Text = "Move First";
            this.BtnFirst.Tooltip = "Move First";
            this.BtnFirst.Click += new System.EventHandler(this.BtnFirst_Click);
            // 
            // BtnPrevious
            // 
            this.BtnPrevious.Image = global::MyPayfriend.Properties.Resources.ArrowLeft1;
            this.BtnPrevious.Name = "BtnPrevious";
            this.BtnPrevious.Text = "Move Previous";
            this.BtnPrevious.Tooltip = "Move Previous";
            this.BtnPrevious.Click += new System.EventHandler(this.BtnPrevious_Click);
            // 
            // lblPageCount
            // 
            this.lblPageCount.BeginGroup = true;
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.PaddingLeft = 3;
            this.lblPageCount.PaddingRight = 3;
            this.lblPageCount.Text = "{0} of {0}";
            this.lblPageCount.Width = 80;
            // 
            // BtnNext
            // 
            this.BtnNext.BeginGroup = true;
            this.BtnNext.Image = global::MyPayfriend.Properties.Resources.ArrowRight1;
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Text = "Next";
            this.BtnNext.Tooltip = "Move Next";
            this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // BtnLast
            // 
            this.BtnLast.Image = global::MyPayfriend.Properties.Resources.ArrowRightStart1;
            this.BtnLast.Name = "BtnLast";
            this.BtnLast.Text = "Last";
            this.BtnLast.Tooltip = "Move Last";
            this.BtnLast.Click += new System.EventHandler(this.BtnLast_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ExpiryDate";
            this.dataGridViewTextBoxColumn1.HeaderText = "ExpiryDate";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 314;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Message";
            this.dataGridViewTextBoxColumn2.HeaderText = "Message";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 629;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "EmployeeName";
            this.dataGridViewTextBoxColumn3.HeaderText = "Employee Name";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            this.dataGridViewTextBoxColumn3.Width = 628;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ReferenceID";
            this.dataGridViewTextBoxColumn4.HeaderText = "ReferenceID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            this.dataGridViewTextBoxColumn4.Width = 314;
            // 
            // chk
            // 
            this.chk.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.chk.HeaderText = "";
            this.chk.Name = "chk";
            this.chk.Width = 24;
            // 
            // Message
            // 
            this.Message.DataPropertyName = "Message";
            this.Message.HeaderText = "Message";
            this.Message.Name = "Message";
            this.Message.ReadOnly = true;
            // 
            // CommonID
            // 
            this.CommonID.DataPropertyName = "CommonID";
            this.CommonID.HeaderText = "CommonID";
            this.CommonID.Name = "CommonID";
            this.CommonID.ReadOnly = true;
            this.CommonID.Visible = false;
            // 
            // DocumentTypeID
            // 
            this.DocumentTypeID.DataPropertyName = "DocumentTypeID";
            this.DocumentTypeID.HeaderText = "DocumentTypeID";
            this.DocumentTypeID.Name = "DocumentTypeID";
            this.DocumentTypeID.ReadOnly = true;
            this.DocumentTypeID.Visible = false;
            // 
            // frmAlert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 576);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmAlert";
            this.Text = "Alert";
            this.Load += new System.EventHandler(this.frmAlert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlerts)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BarNavigation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.CheckBox chkAlladd;
        internal System.Windows.Forms.DateTimePicker dtpTo;
        internal System.Windows.Forms.Label lblFrom;
        internal System.Windows.Forms.Label lblTo;
        internal System.Windows.Forms.Button btnNotify;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.DataGridView dgvAlerts;
        internal System.Windows.Forms.Label lblAlertType;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        internal DevComponents.DotNetBar.Bar BarNavigation;
        internal DevComponents.DotNetBar.LabelItem ToolStripStatusLabel;
        internal DevComponents.DotNetBar.ItemContainer GridNavigation;
        internal DevComponents.DotNetBar.ComboBoxItem cboPageCount;
        internal DevComponents.DotNetBar.ButtonItem BtnFirst;
        internal DevComponents.DotNetBar.ButtonItem BtnPrevious;
        internal DevComponents.DotNetBar.LabelItem lblPageCount;
        internal DevComponents.DotNetBar.ButtonItem BtnNext;
        internal DevComponents.DotNetBar.ButtonItem BtnLast;
        internal System.Windows.Forms.DateTimePicker dtpFrom;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private DevComponents.DotNetBar.ButtonX btnPrint;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSearchKey;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboAlertType;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk;
        private System.Windows.Forms.DataGridViewTextBoxColumn Message;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentTypeID;
    }
}