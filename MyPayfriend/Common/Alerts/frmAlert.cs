﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;

/// <summary>
/// Modified By Shruthi 
/// Modified On 21-Aug-2013
/// 
/// Reviewed By Rajesh
/// Reviewed on 24-Aug-2013
/// </summary>
namespace MyPayfriend
{
    public partial class frmAlert : Form
    {
        #region Properties
        /// <summary>
        /// Gets or sets the total records of the pager
        /// </summary>
        private int TotalRecords { get; set; }

        /// <summary>
        /// Gets or sets the current page index
        /// </summary>
        private int CurrentIndex { get; set; }

        /// <summary>
        /// Gets or sets the page size 
        /// </summary>
        private int PageSize { get; set; }

        /// <summary>
        /// True:To avoid loading the grid more than 1 time during page load
        /// </summary>
        private bool IsFirstTime { get; set; }

        /// <summary>
        /// Get or set the user message of the alert form
        /// </summary>
        private ArrayList MaMessageArr { get; set; }

        /// <summary>
        /// Get or set the message icon for the user message
        /// </summary>
        private MessageBoxIcon MmsgMessageIcon;

        #endregion

        #region Constructor
        public frmAlert()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
                PointSelectAll();
            }


        }
        #endregion
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.AlertInfo, this);
        }
        private void PointSelectAll()
        {
            try
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    int X1 = dgvAlerts.Location.X.ToInt32();
                    int Y1 = dgvAlerts.Location.Y.ToInt32() + 4;

                    X1 = X1 + dgvAlerts.Width + 28;
                    chkAlladd.Location = new Point(X1, Y1);
                }
            }
            catch
            {

            }
        }
        #endregion SetArabicControls


        #region FormEvents
        private void frmAlert_Load(object sender, EventArgs e)
        {
            SetInitials();

        }

        #endregion

        #region FormControls Events

        /// <summary>
        /// Form closing events
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Selecting or un-selecting all the alerts in the alert grid
        /// </summary>
        private void chkAlladd_CheckedChanged(object sender, EventArgs e)
        {
            int intCount = dgvAlerts.Rows.Count;

            for (int intICounter = 0; intICounter < intCount; intICounter++)
            {
                dgvAlerts.Rows[intICounter].Cells["chk"].Value = chkAlladd.Checked;

            }
        }

        /// <summary>
        /// Do not notify again button click
        /// </summary>
        private void btnNotify_Click(object sender, EventArgs e)
        {

            List<AlertsDetails> Alerts = new List<AlertsDetails>();
            foreach (DataGridViewRow row in dgvAlerts.Rows)
            {
                if (row.Cells["chk"].Value.ToBoolean() == true)
                {
                    Alerts.Add(new AlertsDetails()
                    {
                        CommonId = row.Cells["CommonID"].Value.ToInt32()
                        ,
                        DocumentTypeID = row.Cells["DocumentTypeID"].Value.ToInt32()
                    }
                   );
                }
            }

            if (Alerts.Count == 0)
            {
                MessageBox.Show("Please select atleast 1 alert ", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            string MstrMessageCommon = new ClsNotification().GetErrorMessage(MaMessageArr, 10523, out MmsgMessageIcon).Replace("#", "").Trim();
            if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                return;
            if (new clsAlerts().DeleteAlerts(Alerts))
            {
                MstrMessageCommon = new ClsNotification().GetErrorMessage(MaMessageArr, 10524, out MmsgMessageIcon).Replace("#", "").Trim();
                MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkAlladd.Checked = false;
                BindAlerts();

            }
        }

        /// <summary>
        /// Printing the alert report
        /// </summary>
        private void btnprint_Click(object sender, EventArgs e)
        {
            FrmReportviewer ObjViewer = new FrmReportviewer();
            ObjViewer.PsFormName = this.Text;
            ObjViewer.intAlertType = cboAlertType.SelectedValue.ToInt32();
            if (dtpFrom.Checked)
                ObjViewer.strFromDate = dtpFrom.Value.ToString("dd MMM yyyy");
            else
                ObjViewer.strFromDate = null;
            if (dtpTo.Checked)
                ObjViewer.strToDate = dtpTo.Value.ToString("dd MMM yyyy");
            else
                ObjViewer.strToDate = null;
            ObjViewer.PiFormID = (int)FormID.AlertInfo;
            ObjViewer.strSearchKey = txtSearchKey.Text.Trim();

            ObjViewer.ShowDialog();


        }

        /// <summary>
        /// Show appropriate document form when user double click on the alert grid
        /// </summary>
        private void dgvAlerts_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == Message.Index)
            {
                ShowDocument(e.RowIndex);
            }
        }

        /// <summary>
        /// Show alerts based on the filteration
        /// </summary>
        private void btnShow_Click(object sender, EventArgs e)
        {
            if (FormValidations())
            {
                CurrentIndex = 0;
                BindAlerts();
            }
        }

        private bool FormValidations()
        {
            string strMessageCommon = string.Empty;

            if (cboAlertType.SelectedIndex == -1)
            {
                strMessageCommon = new ClsNotification().GetErrorMessage(MaMessageArr, 10525, out MmsgMessageIcon).Replace("#", "").Trim();
                MessageBox.Show(strMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else if (dtpFrom.Checked && dtpTo.Checked && dtpTo.Value.Date < dtpFrom.Value.Date)
            {
                strMessageCommon = new ClsNotification().GetErrorMessage(MaMessageArr, 1761, out MmsgMessageIcon).Replace("#", "").Trim();
                MessageBox.Show(strMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Search text box key down event
        /// </summary>
        private void txtSearchKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BindAlerts();
            }
        }

        private void txtSearchKey_TextChanged(object sender, EventArgs e)
        {
            if (txtSearchKey.Text == "")
            {
                CurrentIndex = 0;
                BindAlerts();
            }
        }
        #endregion

        #region Pager Control Events

        /// <summary>
        /// Show Previous page records
        /// </summary>
        private void BtnPrevious_Click(object sender, EventArgs e)
        {
            GetPreviousRecord();
        }

        /// <summary>
        /// Show first page records
        /// </summary>
        private void BtnFirst_Click(object sender, EventArgs e)
        {
            GetFirstRecord();

        }

        /// <summary>
        /// Show next page records
        /// </summary>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            GetNextRecord();
        }

        /// <summary>
        /// Show last page records
        /// </summary>
        private void BtnLast_Click(object sender, EventArgs e)
        {
            GetLastRecord();
        }

        /// <summary>
        /// Page size change event
        /// </summary>
        private void cboPageCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            //If IsFirstTime = true then the following code will not execute to make sure that 
            //the same function will not be called so many times during the form load
            if (!IsFirstTime)
            {
                PageSize = Convert.ToInt32(cboPageCount.Text);
                CurrentIndex = 0;
                BindAlerts();
            }
        }
        #endregion

        #region Pager Functions

        //Get previous record
        private void GetPreviousRecord()
        {
            CurrentIndex = CurrentIndex - 1;
            chkAlladd.Checked = false;
            BindAlerts();
        }

        //Get first record
        private void GetFirstRecord()
        {
            chkAlladd.Checked = false;
            CurrentIndex = CurrentIndex - 1;
            BindAlerts();
        }

        //Get next record
        private void GetNextRecord()
        {
            chkAlladd.Checked = false;
            CurrentIndex = CurrentIndex + 1;
            BindAlerts();
        }

        //Get last record
        private void GetLastRecord()
        {
            chkAlladd.Checked = false;
            CurrentIndex = (TotalRecords / PageSize);
            BindAlerts();
        }


        //Enable disable all paging controls
        private void EnableDisableAllPagingControl(bool status)
        {
            BtnLast.Enabled = BtnFirst.Enabled = BtnNext.Enabled = BtnPrevious.Enabled = status;
        }

        /// <summary>
        /// Enables or disable the paging controls based on the current page index and total records
        /// </summary>
        private void EnableOrDisablePagingControls()
        {
            BtnFirst.Enabled = BtnPrevious.Enabled = (CurrentIndex > 0);

            BtnNext.Enabled = BtnLast.Enabled = (CurrentIndex != (TotalRecords / PageSize));
        }

        /// <summary>
        /// Setting the current page number 
        /// </summary>
        private void SetPageDetailsCaption()
        {
            int TotalIndex = (TotalRecords / PageSize) + ((TotalRecords % PageSize) > 0 ? 1 : 0);

            lblPageCount.Text = string.Format("{0} of {1}", CurrentIndex < TotalIndex ? CurrentIndex + 1 : CurrentIndex, TotalIndex);
            // BarNavigation.Visible = ((TotalRecords / PageSize) > 0);
        }

        #endregion

        #region Functions

        /// <summary>
        /// Get all messages of the alert form
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaMessageArr = new ClsNotification().FillMessageArray((int)FormID.AlertInfo, ClsCommonSettings.ProductID);
        }

        /// <summary>
        /// Initialise the page 
        /// </summary>
        private void SetInitials()
        {
            //Setting the current page index to 0
            CurrentIndex = 0;

            //load messages for the alert form
            LoadMessage();

            //Load alert type combo
            LoadAlertTypes();

            //Load page size combo
            LoadPageSize();

            //Load all the auto complete suggestions for the alerts
            SetAutoCompleteList();

            //Getting all the alerts 
            BindAlerts();
        }

        /// <summary>
        /// Load Alert Type Combo
        /// </summary>
        private void LoadAlertTypes()
        {
            DataTable datCombos;
            DataRow dr;


            datCombos = new DataTable();
            datCombos.Columns.Add("AlertType", typeof(string));
            datCombos.Columns.Add("TypeID", typeof(string));
            dr = datCombos.NewRow();
            dr["AlertType"] = "All";
            dr["TypeID"] = "-1";
            datCombos.Rows.Add(dr);

            dr = datCombos.NewRow();
            dr["AlertType"] = "Expiry";
            dr["TypeID"] = "1";
            datCombos.Rows.Add(dr);

            dr = datCombos.NewRow();
            dr["AlertType"] = "Return";
            dr["TypeID"] = "0";
            datCombos.Rows.Add(dr);

            cboAlertType.DisplayMember = "AlertType";
            cboAlertType.ValueMember = "TypeID";
            cboAlertType.DataSource = datCombos;
        }

        /// <summary>
        /// Load page size combo
        /// </summary>
        private void LoadPageSize()
        {
            IsFirstTime = true;

            cboPageCount.Items.Add("25");
            cboPageCount.Items.Add("50");
            cboPageCount.Items.Add("100");
            cboPageCount.Text = "25";

            IsFirstTime = false;
            //setting IsFirstTime = false to execute the page size selected index change event after the form load

            PageSize = 25; ;
        }

        /// <summary>
        /// Loading the auto complete list for the logged in user
        /// </summary>
        private void SetAutoCompleteList()
        {
            txtSearchKey.AutoCompleteCustomSource = clsAlerts.GetAutoCompleteList(ClsCommonSettings.UserID);
            
        }

        /// <summary>
        /// Fetching all the alerts for the logged in user based on the filteration provided
        /// </summary>
        private void BindAlerts()
        {
            dgvAlerts.AutoGenerateColumns = false;
            DateTime? dtFromDate = null;
            DateTime? dtToDate = null;
            chkAlladd.Checked = false;

            if (dtpFrom.Checked)
                dtFromDate = dtpFrom.Value;

            if (dtpTo.Checked)
                dtToDate = dtpTo.Value;

            EnableDisableAllPagingControl(false);

            //Getting the total record count  matching the conditions
            TotalRecords = clsAlerts.GetTotalRecordCount(dtFromDate, dtToDate, ClsCommonSettings.UserID, cboAlertType.SelectedValue.ToInt32(), txtSearchKey.Text.Trim());

            //Getting the total alert records matching the conditions
            dgvAlerts.DataSource = clsAlerts.GetAlerts(dtFromDate, dtToDate, ClsCommonSettings.UserID, cboAlertType.SelectedValue.ToInt32(), txtSearchKey.Text.Trim(), PageSize, CurrentIndex);

            //Enable all the paging controls
            EnableDisableAllPagingControl(true);

            //Based on the current index and total record count enable and disable the 
            //paging controls 
            EnableOrDisablePagingControls();

            //Enable or disable the select all check box and print button based on the records in the alert
            EnableDisableAlertControls();

            //Setting the pager controls page count
            SetPageDetailsCaption();

        }

        /// <summary>
        /// Show appropriate forms when user click the alerts grid

        /// </summary>
        private void ShowDocument(int intRowIndex)
        {
            int intDocumentTypeID = dgvAlerts.Rows[intRowIndex].Cells["DocumentTypeID"].Value.ToInt32();

            switch (intDocumentTypeID)
            {
                // Trade License
                case (int)DocumentType.Trading_License:
                    new frmCompanyTradeLicense()
                    {
                        StartPosition = FormStartPosition.CenterScreen,
                        PintTradeLicenseID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32()
                    }.ShowDialog();
                    break;
                //Lease Agreement
                case (int)DocumentType.Lease_Agreement:
                    new frmCompanyLeaseAgreement()
                    {
                        StartPosition = FormStartPosition.CenterScreen,
                        LeaseID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32()
                    }.ShowDialog();
                    break;

                //Employee Passport Form
                case (int)DocumentType.Passport:
                    new frmPassport()
                    {
                        StartPosition = FormStartPosition.CenterScreen
                        ,
                        PiPassportID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32()
                    }.ShowDialog();
                    break;

                //Employee visa form
                case (int)DocumentType.Visa:
                    new frmVisa()
                    {
                        StartPosition = FormStartPosition.CenterScreen
                        ,
                        piVisaID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32()
                    }.ShowDialog();
                    break;

                //Employee driving license form
                case (int)DocumentType.Driving_License:
                    new frmDrivingLicense()
                      {
                          StartPosition = FormStartPosition.CenterScreen
                          ,
                          PiDrivingLicense = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32()
                      }.ShowDialog();
                    break;

                //Employee national id card form
                case (int)DocumentType.National_ID_Card:
                    new EmiratesHealthLabourCard(166) //166->Emirates FormID
                     {
                         StartPosition = FormStartPosition.CenterScreen
                      ,
                         PiEmCardID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32()

                     }.ShowDialog();
                    break;

                //Employee insurance card form        
                case (int)DocumentType.Insurance_Card:
                    using (InsuranceCard objInsuranceCard = new InsuranceCard())
                    {
                        objInsuranceCard.StartPosition = FormStartPosition.CenterScreen;
                        objInsuranceCard.PintInsuranceCardID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32();
                        objInsuranceCard.ShowDialog();
                    }
                    break;

                //Employee labour card form
                case (int)DocumentType.Labour_Card:
                    using (FrmEmployeeLabourCard objFrmEmployeeLabourCard = new FrmEmployeeLabourCard())
                    {
                        objFrmEmployeeLabourCard.StartPosition = FormStartPosition.CenterScreen;
                        objFrmEmployeeLabourCard.PLabourCardId = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32();
                        objFrmEmployeeLabourCard.ShowDialog();
                    }
                    break;

                //Employee health card form
                case (int)DocumentType.Health_Card:
                    using (EmployeeHealthCard objHealthCard = new EmployeeHealthCard())
                    {
                        objHealthCard.StartPosition = FormStartPosition.CenterScreen;
                        objHealthCard.MiHealthCardID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32();
                        objHealthCard.ShowDialog();
                    }
                    break;

                //Employee qualification form
                case (int)DocumentType.Qualification:
                    using (EmployeeQualification objEmployeeQualification = new EmployeeQualification())
                    {
                        objEmployeeQualification.StartPosition = FormStartPosition.CenterScreen;
                        objEmployeeQualification.PQualificationID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32();
                        objEmployeeQualification.ShowDialog();
                    }
                    break;
                    //Employee Probation 
                case (int)DocumentType.Probation:
                    using (FrmEmployee objEmployee = new FrmEmployee())
                    {
                        objEmployee.StartPosition = FormStartPosition.CenterScreen;
                        objEmployee.PintEmployeeID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32();
                        objEmployee.ShowDialog();
                    }
                    break;
                    //Company Assets
                case (int)DocumentType.Asset:
                    using (FrmCompanyAssets objAssets = new FrmCompanyAssets())
                    {
                        objAssets.StartPosition = FormStartPosition.CenterScreen;
                        objAssets.AssetID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32();
                        objAssets.ShowDialog();
                    }
                    break;
                    // Insurance Details
                case (int)DocumentType.InsuranceDetails:
                    using (InsuranceDetails objInsurance = new InsuranceDetails())
                    {
                        objInsurance.StartPosition = FormStartPosition.CenterScreen;
                        objInsurance.PintInsuranceID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32();
                        objInsurance.ShowDialog();
                    }
                    break;
                case 22:
                case 23:
                    break;
                //Other documents form
                default:
                    using (frmDocumentMasterNew objfrmDocumentMasterNew = new frmDocumentMasterNew())
                    {
                        objfrmDocumentMasterNew.StartPosition = FormStartPosition.CenterScreen;
                        objfrmDocumentMasterNew.DocumentID = dgvAlerts.Rows[intRowIndex].Cells[CommonID.Index].Value.ToInt32();
                        DataTable datDoc = new clsAlerts().FillCombos(new string[] { "DocumentTypeID", "DocumentsMaster", "DocumentID = " + objfrmDocumentMasterNew.DocumentID });
                        if (datDoc != null && datDoc.Rows.Count > 0)
                            objfrmDocumentMasterNew.ePDocumentType = (DocumentType)datDoc.Rows[0]["DocumentTypeID"].ToInt32();

                        objfrmDocumentMasterNew.ShowDialog();
                    }
                    break;
            }

        }



        private void EnableDisableAlertControls()
        {
            chkAlladd.Enabled = btnPrint.Enabled = dgvAlerts.Rows.Count > 0;
        }

        #endregion

     
    }
}