﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public partial class FrmReportReference : Form
    {
        clsBLLReportReference objBLLReportRef;
        public FrmReportReference()
        {
            objBLLReportRef = new clsBLLReportReference();
            InitializeComponent();
        }

        private void FrmReportReference_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void BindGrid()
        {
            DGVReport.DataSource = objBLLReportRef.GetReportDetails();
        }

       
    }
}
