﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public partial class frmCompanySelection : DevComponents.DotNetBar.Office2007Form 
    {
        clsBLLLogin MobjClsBLLLogin;
        bool blnCancel = false;
        public frmCompanySelection()
        {
            InitializeComponent();
            MobjClsBLLLogin = new clsBLLLogin();
        }


        private bool FormValidation()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a company", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboCompany.Focus();
                return false;
            }

            return true;
        }

        private void getCompanyDetails()
        {
            ClsCommonSettings.CurrentCompanyID = cboCompany.SelectedValue.ToInt32();
            ClsCommonSettings.CurrentCompany = cboCompany.Text.Trim();
            DataTable datTemp = MobjClsBLLLogin.GetCompanyDetails(ClsCommonSettings.CurrentCompanyID);
            ClsCommonSettings.ParentCompanyID = datTemp.Rows[0]["CompanyID"].ToInt32();
            ClsCommonSettings.CurrencyID = datTemp.Rows[0]["CurrencyID"].ToInt32();
            ClsCommonSettings.Currency = datTemp.Rows[0]["Currency"].ToString();
            ClsCommonSettings.RoleID = datTemp.Rows[0]["RoleID"].ToInt32();
        }

        private void LoadCombos(int intType)
        {
            if (intType == 0)
            {
                cboCompany.DataSource = MobjClsBLLLogin.getCompanyList(ClsCommonSettings.UserID);
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
            }
        }

        private void frmCompanySelection_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            //btnEnter.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            blnCancel = true;
            getCompanyDetails();
            this.Close();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedIndex >= 0)
                LoadCombos(1);
        }

        private void frmCompanySelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ClsCommonSettings.CurrentCompanyID == 0)
                getCompanyDetails();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (FormValidation())
            {
                getCompanyDetails();
                this.Close();
            }
        }
    }
}