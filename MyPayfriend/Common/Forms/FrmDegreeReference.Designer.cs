﻿namespace MyPayfriend
{
    partial class FrmDegreeReference
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDegreeReference));
            this.TStripTop = new System.Windows.Forms.ToolStrip();
            this.bnAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.stsStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnQualLevel = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.grdCommon = new System.Windows.Forms.DataGridView();
            this.txtDegree = new System.Windows.Forms.TextBox();
            this.cboRefType = new System.Windows.Forms.ComboBox();
            this.tmDegree = new System.Windows.Forms.Timer(this.components);
            this.errDegree = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblRefName = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TStripTop.SuspendLayout();
            this.stsStatus.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errDegree)).BeginInit();
            this.SuspendLayout();
            // 
            // TStripTop
            // 
            this.TStripTop.BackColor = System.Drawing.Color.Transparent;
            this.TStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnAddNewItem,
            this.btnSaveItem,
            this.btnDelete,
            this.btnClear});
            this.TStripTop.Location = new System.Drawing.Point(0, 0);
            this.TStripTop.Name = "TStripTop";
            this.TStripTop.Size = new System.Drawing.Size(393, 25);
            this.TStripTop.TabIndex = 4;
            this.TStripTop.Text = "toolStrip1";
            // 
            // bnAddNewItem
            // 
            this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
            this.bnAddNewItem.Name = "bnAddNewItem";
            this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bnAddNewItem.ToolTipText = "Add New";
            this.bnAddNewItem.Click += new System.EventHandler(this.bnAddNewItem_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Remove";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(38, 22);
            this.btnClear.Text = "Clear";
            this.btnClear.Visible = false;
            // 
            // stsStatus
            // 
            this.stsStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.stsStatus.Location = new System.Drawing.Point(0, 345);
            this.stsStatus.Name = "stsStatus";
            this.stsStatus.Size = new System.Drawing.Size(393, 22);
            this.stsStatus.TabIndex = 70;
            this.stsStatus.Text = "StatusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblName);
            this.groupBox1.Controls.Add(this.lblRefName);
            this.groupBox1.Controls.Add(this.BtnQualLevel);
            this.groupBox1.Controls.Add(this.BtnCancel);
            this.groupBox1.Controls.Add(this.BtnOk);
            this.groupBox1.Controls.Add(this.grdCommon);
            this.groupBox1.Controls.Add(this.txtDegree);
            this.groupBox1.Controls.Add(this.cboRefType);
            this.groupBox1.Location = new System.Drawing.Point(7, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 303);
            this.groupBox1.TabIndex = 71;
            this.groupBox1.TabStop = false;
            // 
            // BtnQualLevel
            // 
            this.BtnQualLevel.Location = new System.Drawing.Point(336, 17);
            this.BtnQualLevel.Name = "BtnQualLevel";
            this.BtnQualLevel.Size = new System.Drawing.Size(32, 23);
            this.BtnQualLevel.TabIndex = 115;
            this.BtnQualLevel.Text = "...";
            this.BtnQualLevel.UseVisualStyleBackColor = true;
            this.BtnQualLevel.Click += new System.EventHandler(this.BtnQualLevel_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(295, 74);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 114;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(214, 74);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 113;
            this.BtnOk.Text = "&Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // grdCommon
            // 
            this.grdCommon.AllowUserToAddRows = false;
            this.grdCommon.AllowUserToDeleteRows = false;
            this.grdCommon.AllowUserToOrderColumns = true;
            this.grdCommon.AllowUserToResizeColumns = false;
            this.grdCommon.AllowUserToResizeRows = false;
            this.grdCommon.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdCommon.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdCommon.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colName,
            this.Column2,
            this.Column1});
            this.grdCommon.Location = new System.Drawing.Point(8, 103);
            this.grdCommon.MultiSelect = false;
            this.grdCommon.Name = "grdCommon";
            this.grdCommon.RowHeadersVisible = false;
            this.grdCommon.RowHeadersWidth = 30;
            this.grdCommon.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdCommon.Size = new System.Drawing.Size(365, 195);
            this.grdCommon.TabIndex = 112;
            this.grdCommon.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCommon_CellClick);
            this.grdCommon.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdCommon_DataError);
            // 
            // txtDegree
            // 
            this.txtDegree.BackColor = System.Drawing.SystemColors.Info;
            this.txtDegree.Location = new System.Drawing.Point(98, 48);
            this.txtDegree.MaxLength = 50;
            this.txtDegree.Name = "txtDegree";
            this.txtDegree.Size = new System.Drawing.Size(270, 20);
            this.txtDegree.TabIndex = 110;
            this.txtDegree.TextChanged += new System.EventHandler(this.txtDegree_TextChanged);
            // 
            // cboRefType
            // 
            this.cboRefType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRefType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRefType.BackColor = System.Drawing.SystemColors.Info;
            this.cboRefType.DropDownHeight = 134;
            this.cboRefType.FormattingEnabled = true;
            this.cboRefType.IntegralHeight = false;
            this.cboRefType.Location = new System.Drawing.Point(98, 19);
            this.cboRefType.Name = "cboRefType";
            this.cboRefType.Size = new System.Drawing.Size(232, 21);
            this.cboRefType.TabIndex = 71;
            this.cboRefType.SelectedIndexChanged += new System.EventHandler(this.cboRefType_SelectedIndexChanged);
            this.cboRefType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboRefType_KeyDown);
            // 
            // tmDegree
            // 
            this.tmDegree.Interval = 2000;
            // 
            // errDegree
            // 
            this.errDegree.ContainerControl = this;
            this.errDegree.RightToLeft = true;
            // 
            // lblRefName
            // 
            this.lblRefName.AutoSize = true;
            this.lblRefName.Location = new System.Drawing.Point(7, 22);
            this.lblRefName.Name = "lblRefName";
            this.lblRefName.Size = new System.Drawing.Size(33, 13);
            this.lblRefName.TabIndex = 116;
            this.lblRefName.Text = "Level";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(7, 55);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(65, 13);
            this.lblName.TabIndex = 117;
            this.lblName.Text = "Qualification";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn1.HeaderText = "Name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 362;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "ID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = " RefID";
            this.dataGridViewTextBoxColumn3.HeaderText = "RefID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Name";
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "ID";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = " RefID";
            this.Column1.HeaderText = "RefID";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // FrmDegreeReference
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 367);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.stsStatus);
            this.Controls.Add(this.TStripTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmDegreeReference";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Qualification";
            this.Load += new System.EventHandler(this.FrmDegreeReference_Load);
            this.TStripTop.ResumeLayout(false);
            this.TStripTop.PerformLayout();
            this.stsStatus.ResumeLayout(false);
            this.stsStatus.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errDegree)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip TStripTop;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.StatusStrip stsStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.ComboBox cboRefType;
        internal System.Windows.Forms.TextBox txtDegree;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.DataGridView grdCommon;
        internal System.Windows.Forms.Timer tmDegree;
        internal System.Windows.Forms.ErrorProvider errDegree;
        internal System.Windows.Forms.ToolStripButton bnAddNewItem;
        internal System.Windows.Forms.Button BtnQualLevel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblRefName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}