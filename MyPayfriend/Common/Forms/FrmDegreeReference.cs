﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MyPayfriend

{
    /// <summary>
    ///  Created By         : Laxmi
    ///  Created Date       : 19/8/2013
    ///  Purpose            : Insertion/updation deletion degree based on qualificationtype
    ///  FormID             : 166 (Qualification and Degree Reference)
    ///  ErrorCode          : 5001 -
    ///  Functionalities    : checks duplication for degree  
    /// </summary>

    public partial class FrmDegreeReference : Form
    {
        #region Variable declarations

        // Public variable declarations

        public string PstrFormName;             // Form name (Ex:::: Qualification)
        public string PstrRefTableName;          //  Reference combo tablename (QualificationTypeReference)
        public string PstrRefLblName;           // Reference combo label name ( Level)
        public string PstrRefTextField;         //Reference table datatext field name (QualificationType)
        public string PstrRefValueField;        // Reference table datavaluefieldname (QualificationTypeID)
        public string PstrTableName;            //   tablename (PayDegreeReference)
        public string PstrLblName;              //  combo label name (Qualification)
        public string PstrTextField;            // table datatext field name (Degree)
        public string PstrValueField;           //  table datavaluefieldname (Ex:MCA)
        public string PstrValue1Field;          //(Ex: QualificationTypeID)
        public int PintValue;                   //  (DegreeID value)
        public int PintRefValue;                // Reference combo value (QualificationTypeID value)

        // Private variable declarations
      
        private int MintTimerInterval;                  // To set timer interval
    
        private string MsMessageCommon;
        private string MsMessageCaption;
        private Boolean MblnAddSattus; // Addnew mode -true else update

        private ArrayList MsMessageArray;   // Error Message display
     
        MessageBoxIcon MsMessageBoxIcon;

        #endregion declarations

        clsBLLDegreeRef objClsBLLDegreeRef;
        ClsLogWriter objClsLogWriter;
        ClsNotification objClsNotification; // Object of the Notification class

        #region Constructor
        public FrmDegreeReference()
        {

            InitializeComponent();

            objClsBLLDegreeRef = new clsBLLDegreeRef();
            objClsBLLDegreeRef.DegreeFer = new clsDTODegreeRef();
            objClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
            objClsNotification = new ClsNotification();

            tmDegree.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer
            MintTimerInterval = ClsCommonSettings.TimerInterval; // current companyid
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                lblRefName.Text = "مستوى";
                lblName.Text = "المؤهل";
                BtnOk.Text = "موافق";
                BtnCancel.Text = "إلغاء";
                grdCommon.Columns["colName"].HeaderText = "اسم";
                this.Text = "المؤهل";
            }
        }
        #endregion Constructor

        #region Events
        //Form Events
        private void FrmDegreeReference_Load(object sender, EventArgs e)
        {
            // SetFormName
            this.Text = PstrFormName;
            LoadMessage();          //get all message
            FillCombo();
            FillGrid();
            grdCommon.ClearSelection();
            txtDegree.Select();
            MblnAddSattus = true;
            if (PintValue > 0) // insertion of new degree
            {
                // Lamda  expression for Datagridview cell selection 

                int Rowindex = -1;
                DataGridViewRow row = grdCommon.Rows.Cast<DataGridViewRow>()
                    .Where(r => r.Cells["ID"].Value.ToString().Equals(PintValue.ToString())).First();
                Rowindex = row.Index;
                grdCommon.ClearSelection();
                grdCommon.Rows[Rowindex].Selected = true;
                grdCommon.CurrentCell = grdCommon["colName", Rowindex];
                DataGridViewCellEventArgs eg = new DataGridViewCellEventArgs(0, Rowindex);
                grdCommon_CellClick(sender, eg);
                MblnAddSattus = false;
              
            }
           
        }

        //ButtinEvents
        private void bnAddNewItem_Click(object sender, EventArgs e)
        {
            grdCommon.ClearSelection();
            ClearData();
            MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 655, out MsMessageBoxIcon);
            lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            tmDegree.Enabled = true;
        }
        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                FillGrid();
            }
        }
        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                this.Close();
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtDegree.Tag.ToInt32() > 0)
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 13, out MsMessageBoxIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                DeleteDegreee();
                FillGrid();
                grdCommon.ClearSelection();
                ClearData();
            }
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Datagridview Events
        private void grdCommon_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                ClearData();
                txtDegree.Tag = grdCommon.CurrentRow.Cells["ID"].Value.ToInt32();
                txtDegree.Text = Convert.ToString(grdCommon.CurrentRow.Cells["colName"].Value);
                cboRefType.SelectedValue = grdCommon.CurrentRow.Cells["RefID"].Value;
                MblnAddSattus = false;
            }
        }
        /// <summary>
        /// Exception handling of gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdCommon_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch (Exception ex) { }
        }

        //Combo and textbox events
        private void cboRefType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
            FillGrid();
        }
        private void cboRefType_KeyDown(object sender, KeyEventArgs e)
        {
            cboRefType.DroppedDown = false;
        }
        private void txtDegree_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        // Reference Button Events
        private void BtnQualLevel_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Qualification Level", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "QualificationTypeID,IsPredefined,QualificationType", "QualificationTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intLevelID = Convert.ToInt32(cboRefType.SelectedValue);

                FillCombo();

                if (objCommon.NewID != 0)
                    cboRefType.SelectedValue = objCommon.NewID;
                else
                    cboRefType.SelectedValue = intLevelID;
            }

            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on BtnQualLevel_Click() " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        #endregion Events

        #region Functions
        /// <summary>
        /// Fill Reference Combo
        /// Pass tablename,fields
        /// </summary>
        private void FillCombo()
        {
            cboRefType.DataSource = clsBLLDegreeRef.FillCombo(PstrRefTableName,PstrRefValueField,PstrRefTextField );

            cboRefType.ValueMember = "ID";
            cboRefType.DisplayMember = "Name";
            if (PintRefValue > 0)
                cboRefType.SelectedValue = PintRefValue;

        }

        /// <summary>
        /// Fill degree based on Qualification
        /// 
        /// </summary>
        private void FillGrid()
        {
            grdCommon.DataSource = clsBLLDegreeRef.FillGrid(cboRefType.SelectedValue.ToInt32() ,PstrRefTableName, PstrTableName, PstrRefValueField,PstrValue1Field, PstrValueField, PstrTextField);
            grdCommon.Columns["ID"].Visible = false;
            grdCommon.Columns["RefID"].Visible = false;
            grdCommon.Columns["colName"].HeaderText = PstrFormName;          
        }

        /// <summary>
        /// Save degree
        /// </summary>
        private bool Save()
        {
            int RetID = 0;
            try
            {
                if (FormValidation())
                {
                    if (MblnAddSattus)
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 1, out MsMessageBoxIcon);
                    else
                        MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 3, out MsMessageBoxIcon);

                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)

                        return false;


                    if (txtDegree.Tag.ToInt32() > 0) // Updation
                        objClsBLLDegreeRef.DegreeFer.PintDataValue = txtDegree.Tag.ToInt32();
                    else
                        objClsBLLDegreeRef.DegreeFer.PintDataValue = 0;

                    objClsBLLDegreeRef.DegreeFer.PstrTableName = PstrTableName;
                    objClsBLLDegreeRef.DegreeFer.PstrDataValueField = PstrValueField;
                    objClsBLLDegreeRef.DegreeFer.PstrDataTextField = PstrTextField;
                    objClsBLLDegreeRef.DegreeFer.PstrDataValueField1 = PstrValue1Field;
                    objClsBLLDegreeRef.DegreeFer.PstrDataTextValue = txtDegree.Text.Trim();
                    objClsBLLDegreeRef.DegreeFer.PintRefDataValue = cboRefType.SelectedValue.ToInt32();

                    RetID = objClsBLLDegreeRef.Save();

                    if (RetID > 0)
                    {
                      
                        txtDegree.Text = string.Empty;

                        if (MblnAddSattus)//insert
                        {
                            MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 2, out MsMessageBoxIcon);
                        }
                        else
                        {
                            MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 21, out MsMessageBoxIcon);
                        }

                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        tmDegree.Enabled = true;
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on SaveDegree() " + this.Name + " " + ex.Message.ToString(), 1);
                return false;
            }
        }

        /// <summary>
        /// Form Validations .
        /// checked duplication for degree
        /// </summary>
        /// <returns> returns true when validation is ok</returns>
        private bool FormValidation()
        {
            errDegree.Clear();
            bool bReturnValue = true;
            Control cControl = new Control();

            if (Convert.ToInt32(cboRefType.SelectedValue) == 0) // Qualification type
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 5004, out MsMessageBoxIcon);
                cControl = cboRefType;
                bReturnValue = false;
            }
            else if (String.IsNullOrEmpty(txtDegree.Text.Trim()))// Degree
            {
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 5005, out MsMessageBoxIcon);
                cControl = txtDegree;
                bReturnValue = false;
            }
            else
            {
                
                objClsBLLDegreeRef.DegreeFer.PintDataValue = txtDegree.Tag.ToInt32();
                objClsBLLDegreeRef.DegreeFer.PstrTableName = PstrTableName;
                objClsBLLDegreeRef.DegreeFer.PstrDataValueField = PstrValueField;
                objClsBLLDegreeRef.DegreeFer.PstrDataTextField = PstrTextField;
                objClsBLLDegreeRef.DegreeFer.PstrDataTextValue = txtDegree.Text.Trim();
       
                if (objClsBLLDegreeRef.CheckDuplication())
                {
                    MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 5006, out MsMessageBoxIcon);
                    cControl = txtDegree;
                    bReturnValue = false;
                }
            }

        
            if (bReturnValue == false)
            {
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (cControl != null)
                {
                    errDegree.SetError(cControl, MsMessageCommon);
                    cControl.Focus();
                }
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmDegree.Enabled = true;
            }
            return bReturnValue;
        }

        /// <summary>
        /// Loads all message
        /// </summary>
        private void LoadMessage()
        {
            try
            {
                // Loading Message
                objClsNotification  = new ClsNotification();
                MsMessageArray = new ArrayList();
                MsMessageArray = objClsNotification.FillMessageArray((int)FormID.Qualification, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on LoadMessage() " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        private void ChangeStatus()
        {
            errDegree.Clear();

            if (MblnAddSattus) // add mode
            {
                BtnOk.Enabled = btnSaveItem.Enabled = true;
            }
            else  //edit mode
            {
                BtnOk.Enabled = btnSaveItem.Enabled = true ;
            }
        }
        /// <summary>
        /// Clearing control values
        /// </summary>
        private void ClearData()
        {
           
            MblnAddSattus = true;
            txtDegree.Tag = 0;
            txtDegree.Text = string.Empty;
           
        }

        /// <summary>
        /// Delete Degree  by passing degreeID
        /// 
        /// </summary>
        private void DeleteDegreee()
        {
            try
            {
                objClsBLLDegreeRef.DegreeFer.PstrTableName = PstrTableName;
                objClsBLLDegreeRef.DegreeFer.PstrDataValueField = PstrValueField;
                objClsBLLDegreeRef.DegreeFer.PintDataValue = txtDegree.Tag.ToInt32();
                objClsBLLDegreeRef.DeleteDegree();
                MsMessageCommon = objClsNotification.GetErrorMessage(MsMessageArray, 4, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmDegree.Enabled = true;
            }
            catch (Exception ex)
            {
                objClsLogWriter.WriteLog("Error on DeleteDegreee() " + this.Name + " " + ex.Message.ToString(), 1);
                string strExceptionType = ex.GetType().ToString();

                if (strExceptionType == "System.Data.SqlClient.SqlException")
                    MessageBox.Show("This item cannot be deleted. Details exists in the system", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                //MessageBox.Show("Detai", MstrMessCaption, MessageBoxButtons.OK);
                //return;

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorDeleteItem_Click " + ex.Message.ToString());
            }
        }

        #endregion Functions
    }
}
