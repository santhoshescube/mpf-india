﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public partial class FrmHelp : Form
    {
        public string strFormName = "";

        public FrmHelp()
        {
            InitializeComponent();
        }

        private void FrmHelp_Load(object sender, EventArgs e)
        {
            SetPosition();
            GetHelp();
        }

        private void SetPosition()
        {
            Rectangle WorkingArea = SystemInformation.WorkingArea;
            int intXaxis = WorkingArea.Left + WorkingArea.Width - this.Width;
            int intYaxis = WorkingArea.Top + 100;
            this.Location = new Point(intXaxis, intYaxis);
        }

        private void GetHelp()
        {
            switch (strFormName)
            {
                case "LeaveEntry":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpTaskModule.html#1");
                    break;
                case "LeaveExtension":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpTaskModule.html#9");
                    break;
                case "ShiftSchedule":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpTaskModule.html#8");
                    break;
                case "Deposit":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpTaskModule.html#4");
                    break;
                case "SalaryAdvance":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpTaskModule.html#7");
                    break;
                case "LoanRepayment":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpTaskModule.html#6");
                    break;
                case "Expense":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpTaskModule.html#44");
                    break;
                case "EmployeeTransfer":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpTaskModule.html#2");
                    break;
                case "Attendance_Auto":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpPayrollModule.html#14");
                    break;
                case "AssetHandover":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpTaskModule.html");
                    break;

                case "AttendanceWizard":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpSettingsModule.html#5");
                    break;
               case "Backup":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpSettingsModule.html#4404");
                    break;
               case "Configuration":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpSettingsModule.html#2");
                    break;
                case "Currency":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#903");
                    break;
                case "EasyView":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\Help\\Easy_View.html");
                    break;
                case "Employee":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpEmployeeModule.html#1");
                    break;
              case "Newcompany":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#2");
                    break;
              case "Asset":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#902");
                    break; 
                case "Bank":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#900");
                    break;
                case "Worklocation":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#901");
                    break;
                case "UnPolicy":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#25");
                    break;
                case "ShiftPolicy":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#8");
                    break;
                case "HolidayCalender":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#17");
                    break;
                case "VacationPolicy":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#20");
                    break;
                case "SettlementPolicy":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#24");
                    break;
                case "Reports":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpReportsModule.html");
                    break;
                case "User":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpSettingsModule.html#3");
                    break;
                case "Role":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpSettingsModule.html#4");
                    break;
                case "EmailSettings":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpSettingsModule.html#1");
                    break;
                case "ChangePassword":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpSettingsModule.html#444");
                    break;

                    // Documents module
                case "EmiratesCard":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#907");
                    break;
                case "HealthCard":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#908");
                    break;
                case "Qualification":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#906");
                    break;
                case "Lease":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#15");
                    break;
                case "TradeLicense":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#14");
                    break;
                case "ReceiptIssue":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#908");
                    break;
                case "OtherDocuments":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#911");
                    break;
                case "DrivingLicense":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#905");
                    break;
                case "LabourCard":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#910");
                    break;
                case "Passport":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#16");
                    break;
                case "ProcessingTime":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#913");
                    break;
                case "StockRegister":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#912");
                    break;
                case "Visa":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#904");
                    break;
                case "InsuranceCard":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#909");
                    break;
                case "InsuranceDetails":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpDoccumentModule.html#914");
                    break;
                case "SalaryStructure":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpEmployeeModule.html#11");
                    break;
                case "Loan":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpEmployeeModule.html#14");
                    break;
                case "LeaveStructure":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpEmployeeModule.html#906");
                    break;
                case "LeaveOpening":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpEmployeeModule.html#907");
                    break;
                case "Attendance":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpPayrollModule.html#14");
                    break;
                case "Salary Process":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpPayrollModule.html#15");
                    break;
                case "Salary Release":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpPayrollModule.html#16");
                    break;
                case "Vacation process":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpPayrollModule.html#904");
                    break;
                case "Settlement process":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpPayrollModule.html#905");
                    break;
                case "LeavePolicy":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#13");
                    break;
                case "WorkPolicy":
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpcompanyModule.html#10");
                    break;
                default :
                    webBrowserHelp.Navigate(Application.StartupPath + "\\HTML\\hlpmpfhome.html");
                    break;
            }
        }
    }
}
