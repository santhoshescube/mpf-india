﻿namespace MyPayfriend
{
    partial class FrmCompanyAssets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label Label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCompanyAssets));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlCompanyAssets = new System.Windows.Forms.Panel();
            this.btnbenefitType = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.lblAssetStatus = new System.Windows.Forms.Label();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.dgvCompanyAssets = new System.Windows.Forms.DataGridView();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.txtDepreciation = new System.Windows.Forms.TextBox();
            this.lblDepreciation = new System.Windows.Forms.Label();
            this.txtPurchaseValue = new System.Windows.Forms.TextBox();
            this.lblPurchaseValue = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblBenefitType = new System.Windows.Forms.Label();
            this.cboBenefitType = new System.Windows.Forms.ComboBox();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.lblPurchaseDate = new System.Windows.Forms.Label();
            this.lblExpiryDate = new System.Windows.Forms.Label();
            this.dtpPurchaseDate = new System.Windows.Forms.DateTimePicker();
            this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.bnCompanyAssets = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnAdd = new System.Windows.Forms.ToolStripButton();
            this.bnSave = new System.Windows.Forms.ToolStripButton();
            this.bnDelete = new System.Windows.Forms.ToolStripButton();
            this.bnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDocument = new System.Windows.Forms.ToolStripButton();
            this.bnInsurense = new System.Windows.Forms.ToolStripButton();
            this.bnOtherDocuments = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.ssCompanyAssets = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblstatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrCompanyAssets = new System.Windows.Forms.Timer(this.components);
            this.errCompanyAssets = new System.Windows.Forms.ErrorProvider(this.components);
            this.dgvColCompanyAssetID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColBenefitTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColBenefitTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RefNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColPurchaseDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColPurchaseValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColDepreciation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColStatusID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            Label2 = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            this.pnlCompanyAssets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompanyAssets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnCompanyAssets)).BeginInit();
            this.bnCompanyAssets.SuspendLayout();
            this.ssCompanyAssets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errCompanyAssets)).BeginInit();
            this.SuspendLayout();
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label2.Location = new System.Drawing.Point(7, 182);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(81, 13);
            Label2.TabIndex = 19;
            Label2.Text = "Asset Details";
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label1.Location = new System.Drawing.Point(7, 3);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(64, 13);
            Label1.TabIndex = 10;
            Label1.Text = "Asset Info";
            // 
            // pnlCompanyAssets
            // 
            this.pnlCompanyAssets.Controls.Add(this.btnbenefitType);
            this.pnlCompanyAssets.Controls.Add(this.label4);
            this.pnlCompanyAssets.Controls.Add(this.txtRefNo);
            this.pnlCompanyAssets.Controls.Add(this.label3);
            this.pnlCompanyAssets.Controls.Add(this.cboCompany);
            this.pnlCompanyAssets.Controls.Add(this.lblAssetStatus);
            this.pnlCompanyAssets.Controls.Add(this.cboStatus);
            this.pnlCompanyAssets.Controls.Add(Label2);
            this.pnlCompanyAssets.Controls.Add(this.dgvCompanyAssets);
            this.pnlCompanyAssets.Controls.Add(this.txtRemarks);
            this.pnlCompanyAssets.Controls.Add(this.lblRemarks);
            this.pnlCompanyAssets.Controls.Add(this.txtDepreciation);
            this.pnlCompanyAssets.Controls.Add(this.lblDepreciation);
            this.pnlCompanyAssets.Controls.Add(this.txtPurchaseValue);
            this.pnlCompanyAssets.Controls.Add(this.lblPurchaseValue);
            this.pnlCompanyAssets.Controls.Add(this.lblDescription);
            this.pnlCompanyAssets.Controls.Add(this.txtDescription);
            this.pnlCompanyAssets.Controls.Add(Label1);
            this.pnlCompanyAssets.Controls.Add(this.lblBenefitType);
            this.pnlCompanyAssets.Controls.Add(this.cboBenefitType);
            this.pnlCompanyAssets.Controls.Add(this.dtpExpiryDate);
            this.pnlCompanyAssets.Controls.Add(this.lblPurchaseDate);
            this.pnlCompanyAssets.Controls.Add(this.lblExpiryDate);
            this.pnlCompanyAssets.Controls.Add(this.dtpPurchaseDate);
            this.pnlCompanyAssets.Controls.Add(this.ShapeContainer2);
            this.pnlCompanyAssets.Location = new System.Drawing.Point(0, 34);
            this.pnlCompanyAssets.Name = "pnlCompanyAssets";
            this.pnlCompanyAssets.Size = new System.Drawing.Size(769, 405);
            this.pnlCompanyAssets.TabIndex = 0;
            // 
            // btnbenefitType
            // 
            this.btnbenefitType.Location = new System.Drawing.Point(317, 49);
            this.btnbenefitType.Name = "btnbenefitType";
            this.btnbenefitType.Size = new System.Drawing.Size(28, 22);
            this.btnbenefitType.TabIndex = 24;
            this.btnbenefitType.Text = "...";
            this.btnbenefitType.Click += new System.EventHandler(this.btnbenefitType_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Reference Number";
            // 
            // txtRefNo
            // 
            this.txtRefNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtRefNo.Location = new System.Drawing.Point(120, 76);
            this.txtRefNo.MaxLength = 50;
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Size = new System.Drawing.Size(220, 20);
            this.txtRefNo.TabIndex = 2;
            this.txtRefNo.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.BackColor = System.Drawing.SystemColors.Info;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.Location = new System.Drawing.Point(120, 23);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(220, 21);
            this.cboCompany.TabIndex = 0;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCompany_KeyDown);
            // 
            // lblAssetStatus
            // 
            this.lblAssetStatus.AutoSize = true;
            this.lblAssetStatus.Location = new System.Drawing.Point(375, 77);
            this.lblAssetStatus.Name = "lblAssetStatus";
            this.lblAssetStatus.Size = new System.Drawing.Size(37, 13);
            this.lblAssetStatus.TabIndex = 17;
            this.lblAssetStatus.Text = "Status";
            // 
            // cboStatus
            // 
            this.cboStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboStatus.BackColor = System.Drawing.SystemColors.Info;
            this.cboStatus.DropDownHeight = 134;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.IntegralHeight = false;
            this.cboStatus.Items.AddRange(new object[] {
            "On Hand",
            "Issued",
            "Blocked"});
            this.cboStatus.Location = new System.Drawing.Point(459, 74);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(134, 21);
            this.cboStatus.TabIndex = 8;
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.cboStatus_SelectedIndexChanged);
            this.cboStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboStatus_KeyDown);
            // 
            // dgvCompanyAssets
            // 
            this.dgvCompanyAssets.AllowUserToAddRows = false;
            this.dgvCompanyAssets.BackgroundColor = System.Drawing.Color.White;
            this.dgvCompanyAssets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompanyAssets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColCompanyAssetID,
            this.dgvColBenefitTypeID,
            this.dgvColBenefitTypeName,
            this.dgvColDescription,
            this.RefNo,
            this.dgvColPurchaseDate,
            this.dgvColExpiryDate,
            this.dgvColPurchaseValue,
            this.dgvColDepreciation,
            this.dgvColStatusID,
            this.dgvColStatus,
            this.dgvColRemarks,
            this.CompanyID});
            this.dgvCompanyAssets.Location = new System.Drawing.Point(16, 198);
            this.dgvCompanyAssets.Name = "dgvCompanyAssets";
            this.dgvCompanyAssets.ReadOnly = true;
            this.dgvCompanyAssets.RowHeadersVisible = false;
            this.dgvCompanyAssets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCompanyAssets.Size = new System.Drawing.Size(740, 200);
            this.dgvCompanyAssets.TabIndex = 9;
            this.dgvCompanyAssets.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCompanyAssets_CellClick);
            this.dgvCompanyAssets.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvCompanyAssets_CurrentCellDirtyStateChanged);
            this.dgvCompanyAssets.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvCompanyAssets_DataError);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(459, 101);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRemarks.Size = new System.Drawing.Size(297, 72);
            this.txtRemarks.TabIndex = 9;
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(375, 103);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(49, 13);
            this.lblRemarks.TabIndex = 18;
            this.lblRemarks.Text = "Remarks";
            // 
            // txtDepreciation
            // 
            this.txtDepreciation.Location = new System.Drawing.Point(120, 155);
            this.txtDepreciation.MaxLength = 3;
            this.txtDepreciation.Name = "txtDepreciation";
            this.txtDepreciation.Size = new System.Drawing.Size(120, 20);
            this.txtDepreciation.TabIndex = 5;
            this.txtDepreciation.TextChanged += new System.EventHandler(this.ChangeStatus);
            this.txtDepreciation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDepreciation_KeyPress);
            // 
            // lblDepreciation
            // 
            this.lblDepreciation.AutoSize = true;
            this.lblDepreciation.Location = new System.Drawing.Point(13, 157);
            this.lblDepreciation.Name = "lblDepreciation";
            this.lblDepreciation.Size = new System.Drawing.Size(84, 13);
            this.lblDepreciation.TabIndex = 16;
            this.lblDepreciation.Text = "Depreciation (%)";
            // 
            // txtPurchaseValue
            // 
            this.txtPurchaseValue.Location = new System.Drawing.Point(120, 128);
            this.txtPurchaseValue.MaxLength = 15;
            this.txtPurchaseValue.Name = "txtPurchaseValue";
            this.txtPurchaseValue.Size = new System.Drawing.Size(120, 20);
            this.txtPurchaseValue.TabIndex = 4;
            this.txtPurchaseValue.TextChanged += new System.EventHandler(this.ChangeStatus);
            this.txtPurchaseValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPurchaseValue_KeyPress);
            // 
            // lblPurchaseValue
            // 
            this.lblPurchaseValue.AutoSize = true;
            this.lblPurchaseValue.Location = new System.Drawing.Point(13, 130);
            this.lblPurchaseValue.Name = "lblPurchaseValue";
            this.lblPurchaseValue.Size = new System.Drawing.Size(34, 13);
            this.lblPurchaseValue.TabIndex = 15;
            this.lblPurchaseValue.Text = "Value";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(13, 104);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(35, 13);
            this.lblDescription.TabIndex = 12;
            this.lblDescription.Text = "Name";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Info;
            this.txtDescription.Location = new System.Drawing.Point(120, 102);
            this.txtDescription.MaxLength = 50;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(220, 20);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // lblBenefitType
            // 
            this.lblBenefitType.AutoSize = true;
            this.lblBenefitType.Location = new System.Drawing.Point(13, 49);
            this.lblBenefitType.Name = "lblBenefitType";
            this.lblBenefitType.Size = new System.Drawing.Size(31, 13);
            this.lblBenefitType.TabIndex = 11;
            this.lblBenefitType.Text = "Type";
            // 
            // cboBenefitType
            // 
            this.cboBenefitType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBenefitType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBenefitType.BackColor = System.Drawing.SystemColors.Info;
            this.cboBenefitType.DropDownHeight = 134;
            this.cboBenefitType.FormattingEnabled = true;
            this.cboBenefitType.IntegralHeight = false;
            this.cboBenefitType.Location = new System.Drawing.Point(120, 49);
            this.cboBenefitType.Name = "cboBenefitType";
            this.cboBenefitType.Size = new System.Drawing.Size(191, 21);
            this.cboBenefitType.TabIndex = 1;
            this.cboBenefitType.SelectedIndexChanged += new System.EventHandler(this.cboBenefitType_SelectedIndexChanged);
            this.cboBenefitType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboBenefitType_KeyDown);
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(459, 47);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.ShowCheckBox = true;
            this.dtpExpiryDate.Size = new System.Drawing.Size(120, 20);
            this.dtpExpiryDate.TabIndex = 7;
            this.dtpExpiryDate.ValueChanged += new System.EventHandler(this.dtpExpiryDate_ValueChanged);
            // 
            // lblPurchaseDate
            // 
            this.lblPurchaseDate.AutoSize = true;
            this.lblPurchaseDate.Location = new System.Drawing.Point(375, 25);
            this.lblPurchaseDate.Name = "lblPurchaseDate";
            this.lblPurchaseDate.Size = new System.Drawing.Size(78, 13);
            this.lblPurchaseDate.TabIndex = 13;
            this.lblPurchaseDate.Text = "Purchase Date";
            // 
            // lblExpiryDate
            // 
            this.lblExpiryDate.AutoSize = true;
            this.lblExpiryDate.Location = new System.Drawing.Point(375, 50);
            this.lblExpiryDate.Name = "lblExpiryDate";
            this.lblExpiryDate.Size = new System.Drawing.Size(61, 13);
            this.lblExpiryDate.TabIndex = 14;
            this.lblExpiryDate.Text = "Expiry Date";
            // 
            // dtpPurchaseDate
            // 
            this.dtpPurchaseDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpPurchaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPurchaseDate.Location = new System.Drawing.Point(459, 22);
            this.dtpPurchaseDate.Name = "dtpPurchaseDate";
            this.dtpPurchaseDate.Size = new System.Drawing.Size(120, 20);
            this.dtpPurchaseDate.TabIndex = 6;
            this.dtpPurchaseDate.ValueChanged += new System.EventHandler(this.dtpPurchaseDate_ValueChanged);
            // 
            // ShapeContainer2
            // 
            this.ShapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer2.Name = "ShapeContainer2";
            this.ShapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape2,
            this.LineShape1});
            this.ShapeContainer2.Size = new System.Drawing.Size(769, 405);
            this.ShapeContainer2.TabIndex = 4;
            this.ShapeContainer2.TabStop = false;
            // 
            // LineShape2
            // 
            this.LineShape2.BorderColor = System.Drawing.Color.Silver;
            this.LineShape2.Name = "LineShape2";
            this.LineShape2.X1 = 70;
            this.LineShape2.X2 = 760;
            this.LineShape2.Y1 = 189;
            this.LineShape2.Y2 = 189;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.Color.Silver;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 54;
            this.LineShape1.X2 = 760;
            this.LineShape1.Y1 = 10;
            this.LineShape1.Y2 = 10;
            // 
            // bnCompanyAssets
            // 
            this.bnCompanyAssets.AddNewItem = null;
            this.bnCompanyAssets.CountItem = null;
            this.bnCompanyAssets.DeleteItem = null;
            this.bnCompanyAssets.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnAdd,
            this.bnSave,
            this.bnDelete,
            this.bnClear,
            this.ToolStripSeparator1,
            this.btnDocument,
            this.bnInsurense,
            this.bnOtherDocuments,
            this.toolStripSeparator2,
            this.BtnPrint,
            this.BtnEmail,
            this.ToolStripSeparator3,
            this.BtnHelp,
            this.toolStripButton1});
            this.bnCompanyAssets.Location = new System.Drawing.Point(0, 0);
            this.bnCompanyAssets.MoveFirstItem = null;
            this.bnCompanyAssets.MoveLastItem = null;
            this.bnCompanyAssets.MoveNextItem = null;
            this.bnCompanyAssets.MovePreviousItem = null;
            this.bnCompanyAssets.Name = "bnCompanyAssets";
            this.bnCompanyAssets.PositionItem = null;
            this.bnCompanyAssets.Size = new System.Drawing.Size(770, 25);
            this.bnCompanyAssets.TabIndex = 1;
            this.bnCompanyAssets.Text = "BindingNavigator1";
            // 
            // bnAdd
            // 
            this.bnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnAdd.Image = ((System.Drawing.Image)(resources.GetObject("bnAdd.Image")));
            this.bnAdd.Name = "bnAdd";
            this.bnAdd.RightToLeftAutoMirrorImage = true;
            this.bnAdd.Size = new System.Drawing.Size(23, 22);
            this.bnAdd.Text = "Add new";
            this.bnAdd.Click += new System.EventHandler(this.bnAdd_Click);
            // 
            // bnSave
            // 
            this.bnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSave.Image = ((System.Drawing.Image)(resources.GetObject("bnSave.Image")));
            this.bnSave.Name = "bnSave";
            this.bnSave.Size = new System.Drawing.Size(23, 22);
            this.bnSave.Text = "Save Data";
            this.bnSave.Click += new System.EventHandler(this.bnSave_Click);
            // 
            // bnDelete
            // 
            this.bnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.bnDelete.Name = "bnDelete";
            this.bnDelete.RightToLeftAutoMirrorImage = true;
            this.bnDelete.Size = new System.Drawing.Size(23, 22);
            this.bnDelete.Text = "Delete";
            this.bnDelete.Click += new System.EventHandler(this.bnDelete_Click);
            // 
            // bnClear
            // 
            this.bnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.bnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnClear.Name = "bnClear";
            this.bnClear.Size = new System.Drawing.Size(23, 22);
            this.bnClear.Text = "Cancel";
            this.bnClear.ToolTipText = "Clear";
            this.bnClear.Click += new System.EventHandler(this.bnClear_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDocument
            // 
            this.btnDocument.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDocument.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.btnDocument.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDocument.Name = "btnDocument";
            this.btnDocument.Size = new System.Drawing.Size(23, 22);
            this.btnDocument.Text = "Attach Document";
            this.btnDocument.Click += new System.EventHandler(this.btnDocument_Click);
            // 
            // bnInsurense
            // 
            this.bnInsurense.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnInsurense.Image = global::MyPayfriend.Properties.Resources.Insurance16x16;
            this.bnInsurense.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnInsurense.Name = "bnInsurense";
            this.bnInsurense.Size = new System.Drawing.Size(23, 22);
            this.bnInsurense.Text = "Insurance";
            this.bnInsurense.Click += new System.EventHandler(this.bnInsurense_Click);
            // 
            // bnOtherDocuments
            // 
            this.bnOtherDocuments.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnOtherDocuments.Image = global::MyPayfriend.Properties.Resources.MandatoryDocuments;
            this.bnOtherDocuments.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnOtherDocuments.Name = "bnOtherDocuments";
            this.bnOtherDocuments.Size = new System.Drawing.Size(23, 22);
            this.bnOtherDocuments.Text = "Other Documents";
            this.bnOtherDocuments.Click += new System.EventHandler(this.bnOtherDocuments_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Visible = false;
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Visible = false;
            // 
            // ssCompanyAssets
            // 
            this.ssCompanyAssets.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.lblstatus});
            this.ssCompanyAssets.Location = new System.Drawing.Point(0, 442);
            this.ssCompanyAssets.Name = "ssCompanyAssets";
            this.ssCompanyAssets.Size = new System.Drawing.Size(770, 22);
            this.ssCompanyAssets.TabIndex = 2;
            this.ssCompanyAssets.Text = "StatusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // lblstatus
            // 
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 17);
            // 
            // tmrCompanyAssets
            // 
            this.tmrCompanyAssets.Tick += new System.EventHandler(this.tmrCompanyAssets_Tick);
            // 
            // errCompanyAssets
            // 
            this.errCompanyAssets.ContainerControl = this;
            // 
            // dgvColCompanyAssetID
            // 
            this.dgvColCompanyAssetID.HeaderText = "CompanyAssetID";
            this.dgvColCompanyAssetID.Name = "dgvColCompanyAssetID";
            this.dgvColCompanyAssetID.ReadOnly = true;
            this.dgvColCompanyAssetID.Visible = false;
            // 
            // dgvColBenefitTypeID
            // 
            this.dgvColBenefitTypeID.HeaderText = "BenefitTypeID";
            this.dgvColBenefitTypeID.Name = "dgvColBenefitTypeID";
            this.dgvColBenefitTypeID.ReadOnly = true;
            this.dgvColBenefitTypeID.Visible = false;
            // 
            // dgvColBenefitTypeName
            // 
            this.dgvColBenefitTypeName.HeaderText = "Asset Type";
            this.dgvColBenefitTypeName.Name = "dgvColBenefitTypeName";
            this.dgvColBenefitTypeName.ReadOnly = true;
            this.dgvColBenefitTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvColDescription
            // 
            this.dgvColDescription.HeaderText = "Description";
            this.dgvColDescription.Name = "dgvColDescription";
            this.dgvColDescription.ReadOnly = true;
            this.dgvColDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // RefNo
            // 
            this.RefNo.HeaderText = "Refrence No.";
            this.RefNo.Name = "RefNo";
            this.RefNo.ReadOnly = true;
            this.RefNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvColPurchaseDate
            // 
            this.dgvColPurchaseDate.HeaderText = "Purchase Date";
            this.dgvColPurchaseDate.Name = "dgvColPurchaseDate";
            this.dgvColPurchaseDate.ReadOnly = true;
            this.dgvColPurchaseDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvColPurchaseDate.Width = 90;
            // 
            // dgvColExpiryDate
            // 
            this.dgvColExpiryDate.HeaderText = "Expiry Date";
            this.dgvColExpiryDate.Name = "dgvColExpiryDate";
            this.dgvColExpiryDate.ReadOnly = true;
            this.dgvColExpiryDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvColExpiryDate.Width = 80;
            // 
            // dgvColPurchaseValue
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColPurchaseValue.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvColPurchaseValue.HeaderText = "Value";
            this.dgvColPurchaseValue.Name = "dgvColPurchaseValue";
            this.dgvColPurchaseValue.ReadOnly = true;
            this.dgvColPurchaseValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvColPurchaseValue.Width = 80;
            // 
            // dgvColDepreciation
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvColDepreciation.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvColDepreciation.HeaderText = "Depreciation(%)";
            this.dgvColDepreciation.Name = "dgvColDepreciation";
            this.dgvColDepreciation.ReadOnly = true;
            this.dgvColDepreciation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvColDepreciation.Width = 85;
            // 
            // dgvColStatusID
            // 
            this.dgvColStatusID.HeaderText = "StatusID";
            this.dgvColStatusID.Name = "dgvColStatusID";
            this.dgvColStatusID.ReadOnly = true;
            this.dgvColStatusID.Visible = false;
            // 
            // dgvColStatus
            // 
            this.dgvColStatus.HeaderText = "Status";
            this.dgvColStatus.Name = "dgvColStatus";
            this.dgvColStatus.ReadOnly = true;
            this.dgvColStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvColRemarks
            // 
            this.dgvColRemarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvColRemarks.HeaderText = "Remarks";
            this.dgvColRemarks.Name = "dgvColRemarks";
            this.dgvColRemarks.ReadOnly = true;
            this.dgvColRemarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvColRemarks.Visible = false;
            // 
            // CompanyID
            // 
            this.CompanyID.HeaderText = "Company";
            this.CompanyID.Name = "CompanyID";
            this.CompanyID.ReadOnly = true;
            this.CompanyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CompanyID.Visible = false;
            // 
            // FrmCompanyAssets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 464);
            this.Controls.Add(this.ssCompanyAssets);
            this.Controls.Add(this.bnCompanyAssets);
            this.Controls.Add(this.pnlCompanyAssets);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCompanyAssets";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company Assets";
            this.Load += new System.EventHandler(this.FrmCompanyAssets_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCompanyAssets_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCompanyAssets_KeyDown);
            this.pnlCompanyAssets.ResumeLayout(false);
            this.pnlCompanyAssets.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompanyAssets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnCompanyAssets)).EndInit();
            this.bnCompanyAssets.ResumeLayout(false);
            this.bnCompanyAssets.PerformLayout();
            this.ssCompanyAssets.ResumeLayout(false);
            this.ssCompanyAssets.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errCompanyAssets)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel pnlCompanyAssets;
        internal System.Windows.Forms.Label lblAssetStatus;
        internal System.Windows.Forms.ComboBox cboStatus;
        internal System.Windows.Forms.DataGridView dgvCompanyAssets;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.Label lblRemarks;
        internal System.Windows.Forms.TextBox txtDepreciation;
        internal System.Windows.Forms.Label lblDepreciation;
        internal System.Windows.Forms.TextBox txtPurchaseValue;
        internal System.Windows.Forms.Label lblPurchaseValue;
        internal System.Windows.Forms.Label lblDescription;
        internal System.Windows.Forms.TextBox txtDescription;
        internal System.Windows.Forms.Label lblBenefitType;
        internal System.Windows.Forms.ComboBox cboBenefitType;
        internal System.Windows.Forms.DateTimePicker dtpExpiryDate;
        internal System.Windows.Forms.Label lblPurchaseDate;
        internal System.Windows.Forms.Label lblExpiryDate;
        internal System.Windows.Forms.DateTimePicker dtpPurchaseDate;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.BindingNavigator bnCompanyAssets;
        internal System.Windows.Forms.ToolStripButton bnAdd;
        internal System.Windows.Forms.ToolStripButton bnSave;
        internal System.Windows.Forms.ToolStripButton bnDelete;
        internal System.Windows.Forms.ToolStripButton bnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.StatusStrip ssCompanyAssets;
        internal System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        internal System.Windows.Forms.ToolStripStatusLabel lblstatus;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        internal System.Windows.Forms.Timer tmrCompanyAssets;
        internal System.Windows.Forms.ErrorProvider errCompanyAssets;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txtRefNo;
        private System.Windows.Forms.Button btnbenefitType;
        private System.Windows.Forms.ToolStripButton btnDocument;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton bnInsurense;
        private System.Windows.Forms.ToolStripButton bnOtherDocuments;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColCompanyAssetID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColBenefitTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColBenefitTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn RefNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColPurchaseDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColPurchaseValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColDepreciation;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColStatusID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColRemarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyID;
    }
}