﻿namespace MyPayfriend
{
    partial class FrmBankDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label1;
            System.Windows.Forms.Label DescriptionLabel;
            System.Windows.Forms.Label BankCodeLabel;
            System.Windows.Forms.Label AddressLabel;
            System.Windows.Forms.Label TelephoneLabel;
            System.Windows.Forms.Label FaxLabel;
            System.Windows.Forms.Label CountryIDLabel;
            System.Windows.Forms.Label EmailLabel;
            System.Windows.Forms.Label WebsiteLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBankDetails));
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BankDetailsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCountry = new System.Windows.Forms.Button();
            this.txtUAEBankCode = new System.Windows.Forms.TextBox();
            this.btnBankBranch = new System.Windows.Forms.Button();
            this.CboBankBranchID = new System.Windows.Forms.ComboBox();
            this.CboCountry = new System.Windows.Forms.ComboBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtBankCode = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtWebsite = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.ErrProBank = new System.Windows.Forms.ErrorProvider(this.components);
            this.TmBank = new System.Windows.Forms.Timer(this.components);
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblBankStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblIBANCode = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            DescriptionLabel = new System.Windows.Forms.Label();
            BankCodeLabel = new System.Windows.Forms.Label();
            AddressLabel = new System.Windows.Forms.Label();
            TelephoneLabel = new System.Windows.Forms.Label();
            FaxLabel = new System.Windows.Forms.Label();
            CountryIDLabel = new System.Windows.Forms.Label();
            EmailLabel = new System.Windows.Forms.Label();
            WebsiteLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BankDetailsBindingNavigator)).BeginInit();
            this.BankDetailsBindingNavigator.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrProBank)).BeginInit();
            this.ssStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(8, 13);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(63, 13);
            Label1.TabIndex = 45;
            Label1.Text = "Bank Name";
            // 
            // DescriptionLabel
            // 
            DescriptionLabel.AutoSize = true;
            DescriptionLabel.Location = new System.Drawing.Point(8, 41);
            DescriptionLabel.Name = "DescriptionLabel";
            DescriptionLabel.Size = new System.Drawing.Size(72, 13);
            DescriptionLabel.TabIndex = 25;
            DescriptionLabel.Text = "Branch Name";
            // 
            // BankCodeLabel
            // 
            BankCodeLabel.AutoSize = true;
            BankCodeLabel.Location = new System.Drawing.Point(8, 68);
            BankCodeLabel.Name = "BankCodeLabel";
            BankCodeLabel.Size = new System.Drawing.Size(60, 13);
            BankCodeLabel.TabIndex = 28;
            BankCodeLabel.Text = "Bank Code";
            // 
            // AddressLabel
            // 
            AddressLabel.AutoSize = true;
            AddressLabel.Location = new System.Drawing.Point(8, 95);
            AddressLabel.Name = "AddressLabel";
            AddressLabel.Size = new System.Drawing.Size(45, 13);
            AddressLabel.TabIndex = 32;
            AddressLabel.Text = "Address";
            // 
            // TelephoneLabel
            // 
            TelephoneLabel.AutoSize = true;
            TelephoneLabel.Location = new System.Drawing.Point(8, 152);
            TelephoneLabel.Name = "TelephoneLabel";
            TelephoneLabel.Size = new System.Drawing.Size(58, 13);
            TelephoneLabel.TabIndex = 35;
            TelephoneLabel.Text = "Telephone";
            // 
            // FaxLabel
            // 
            FaxLabel.AutoSize = true;
            FaxLabel.Location = new System.Drawing.Point(8, 178);
            FaxLabel.Name = "FaxLabel";
            FaxLabel.Size = new System.Drawing.Size(24, 13);
            FaxLabel.TabIndex = 37;
            FaxLabel.Text = "Fax";
            // 
            // CountryIDLabel
            // 
            CountryIDLabel.AutoSize = true;
            CountryIDLabel.Location = new System.Drawing.Point(8, 204);
            CountryIDLabel.Name = "CountryIDLabel";
            CountryIDLabel.Size = new System.Drawing.Size(46, 13);
            CountryIDLabel.TabIndex = 41;
            CountryIDLabel.Text = "Country ";
            // 
            // EmailLabel
            // 
            EmailLabel.AutoSize = true;
            EmailLabel.Location = new System.Drawing.Point(8, 231);
            EmailLabel.Name = "EmailLabel";
            EmailLabel.Size = new System.Drawing.Size(32, 13);
            EmailLabel.TabIndex = 43;
            EmailLabel.Text = "Email";
            // 
            // WebsiteLabel
            // 
            WebsiteLabel.AutoSize = true;
            WebsiteLabel.Location = new System.Drawing.Point(8, 257);
            WebsiteLabel.Name = "WebsiteLabel";
            WebsiteLabel.Size = new System.Drawing.Size(46, 13);
            WebsiteLabel.TabIndex = 44;
            WebsiteLabel.Text = "Website";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BankDetailsBindingNavigator
            // 
            this.BankDetailsBindingNavigator.AddNewItem = null;
            this.BankDetailsBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.BankDetailsBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.BankDetailsBindingNavigator.DeleteItem = null;
            this.BankDetailsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.toolStripSeparator2,
            this.btnClear,
            this.btnPrint,
            this.btnEmail,
            this.toolStripSeparator,
            this.btnHelp});
            this.BankDetailsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.BankDetailsBindingNavigator.MoveFirstItem = null;
            this.BankDetailsBindingNavigator.MoveLastItem = null;
            this.BankDetailsBindingNavigator.MoveNextItem = null;
            this.BankDetailsBindingNavigator.MovePreviousItem = null;
            this.BankDetailsBindingNavigator.Name = "BankDetailsBindingNavigator";
            this.BankDetailsBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.BankDetailsBindingNavigator.Size = new System.Drawing.Size(459, 25);
            this.BankDetailsBindingNavigator.TabIndex = 0;
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.bindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.bindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.bindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.bindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.ToolTipText = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.ToolTipText = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.ToolTipText = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.ToolTipText = "Print";
            this.btnPrint.Click += new System.EventHandler(this.PrintStripButton_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.AutoToolTip = false;
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = ((System.Drawing.Image)(resources.GetObject("btnEmail.Image")));
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.ToolTipText = "Email";
            this.btnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.ToolTipText = "Help";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblIBANCode);
            this.panel1.Controls.Add(this.btnCountry);
            this.panel1.Controls.Add(this.txtUAEBankCode);
            this.panel1.Controls.Add(this.btnBankBranch);
            this.panel1.Controls.Add(Label1);
            this.panel1.Controls.Add(this.CboBankBranchID);
            this.panel1.Controls.Add(this.CboCountry);
            this.panel1.Controls.Add(DescriptionLabel);
            this.panel1.Controls.Add(this.txtDescription);
            this.panel1.Controls.Add(BankCodeLabel);
            this.panel1.Controls.Add(this.txtBankCode);
            this.panel1.Controls.Add(AddressLabel);
            this.panel1.Controls.Add(this.txtAddress);
            this.panel1.Controls.Add(TelephoneLabel);
            this.panel1.Controls.Add(this.txtTelephone);
            this.panel1.Controls.Add(FaxLabel);
            this.panel1.Controls.Add(this.txtFax);
            this.panel1.Controls.Add(CountryIDLabel);
            this.panel1.Controls.Add(EmailLabel);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Controls.Add(WebsiteLabel);
            this.panel1.Controls.Add(this.txtWebsite);
            this.panel1.Location = new System.Drawing.Point(5, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(448, 312);
            this.panel1.TabIndex = 0;
            // 
            // btnCountry
            // 
            this.btnCountry.Location = new System.Drawing.Point(404, 199);
            this.btnCountry.Name = "btnCountry";
            this.btnCountry.Size = new System.Drawing.Size(32, 23);
            this.btnCountry.TabIndex = 8;
            this.btnCountry.Text = "...";
            this.btnCountry.UseVisualStyleBackColor = true;
            this.btnCountry.Click += new System.EventHandler(this.btnCountry_Click);
            // 
            // txtUAEBankCode
            // 
            this.txtUAEBankCode.Location = new System.Drawing.Point(97, 280);
            this.txtUAEBankCode.MaxLength = 23;
            this.txtUAEBankCode.Name = "txtUAEBankCode";
            this.txtUAEBankCode.Size = new System.Drawing.Size(190, 20);
            this.txtUAEBankCode.TabIndex = 12;
            this.txtUAEBankCode.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // btnBankBranch
            // 
            this.btnBankBranch.Location = new System.Drawing.Point(404, 10);
            this.btnBankBranch.Name = "btnBankBranch";
            this.btnBankBranch.Size = new System.Drawing.Size(32, 23);
            this.btnBankBranch.TabIndex = 2;
            this.btnBankBranch.Text = "...";
            this.btnBankBranch.UseVisualStyleBackColor = true;
            this.btnBankBranch.Click += new System.EventHandler(this.btnBankBranch_Click);
            // 
            // CboBankBranchID
            // 
            this.CboBankBranchID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboBankBranchID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboBankBranchID.BackColor = System.Drawing.SystemColors.Info;
            this.CboBankBranchID.DropDownHeight = 134;
            this.CboBankBranchID.DropDownWidth = 257;
            this.CboBankBranchID.FormattingEnabled = true;
            this.CboBankBranchID.IntegralHeight = false;
            this.CboBankBranchID.Location = new System.Drawing.Point(97, 12);
            this.CboBankBranchID.Name = "CboBankBranchID";
            this.CboBankBranchID.Size = new System.Drawing.Size(296, 21);
            this.CboBankBranchID.TabIndex = 0;
            this.CboBankBranchID.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.CboBankBranchID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // CboCountry
            // 
            this.CboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCountry.DropDownHeight = 134;
            this.CboCountry.DropDownWidth = 257;
            this.CboCountry.FormattingEnabled = true;
            this.CboCountry.IntegralHeight = false;
            this.CboCountry.Location = new System.Drawing.Point(97, 201);
            this.CboCountry.Name = "CboCountry";
            this.CboCountry.Size = new System.Drawing.Size(296, 21);
            this.CboCountry.TabIndex = 8;
            this.CboCountry.SelectedIndexChanged += new System.EventHandler(this.ChangeStatus);
            this.CboCountry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Info;
            this.txtDescription.Location = new System.Drawing.Point(97, 39);
            this.txtDescription.MaxLength = 50;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(296, 20);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtBankCode
            // 
            this.txtBankCode.Location = new System.Drawing.Point(97, 65);
            this.txtBankCode.MaxLength = 15;
            this.txtBankCode.Name = "txtBankCode";
            this.txtBankCode.Size = new System.Drawing.Size(128, 20);
            this.txtBankCode.TabIndex = 4;
            this.txtBankCode.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(97, 92);
            this.txtAddress.MaxLength = 100;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAddress.Size = new System.Drawing.Size(296, 50);
            this.txtAddress.TabIndex = 5;
            this.txtAddress.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(97, 149);
            this.txtTelephone.MaxLength = 40;
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(128, 20);
            this.txtTelephone.TabIndex = 6;
            this.txtTelephone.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(97, 175);
            this.txtFax.MaxLength = 25;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(128, 20);
            this.txtFax.TabIndex = 7;
            this.txtFax.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(97, 228);
            this.txtEmail.MaxLength = 40;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(296, 20);
            this.txtEmail.TabIndex = 10;
            this.txtEmail.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // txtWebsite
            // 
            this.txtWebsite.Location = new System.Drawing.Point(97, 254);
            this.txtWebsite.MaxLength = 40;
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(296, 20);
            this.txtWebsite.TabIndex = 11;
            this.txtWebsite.TextChanged += new System.EventHandler(this.ChangeStatus);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(5, 346);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(294, 346);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 14;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(375, 346);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ErrProBank
            // 
            this.ErrProBank.ContainerControl = this;
            this.ErrProBank.RightToLeft = true;
            // 
            // TmBank
            // 
            this.TmBank.Tick += new System.EventHandler(this.TmBank_Tick);
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblBankStatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 375);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(459, 22);
            this.ssStatus.TabIndex = 17;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblBankStatus
            // 
            this.lblBankStatus.Name = "lblBankStatus";
            this.lblBankStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lblIBANCode
            // 
            this.lblIBANCode.AutoSize = true;
            this.lblIBANCode.Location = new System.Drawing.Point(8, 283);
            this.lblIBANCode.Name = "lblIBANCode";
            this.lblIBANCode.Size = new System.Drawing.Size(60, 13);
            this.lblIBANCode.TabIndex = 46;
            this.lblIBANCode.Text = "IBAN Code";
            // 
            // FrmBankDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 397);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.BankDetailsBindingNavigator);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBankDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bank Information";
            this.Load += new System.EventHandler(this.FrmBankDetails_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmBankDetails_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmBankDetails_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.BankDetailsBindingNavigator)).EndInit();
            this.BankDetailsBindingNavigator.ResumeLayout(false);
            this.BankDetailsBindingNavigator.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrProBank)).EndInit();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        private System.Windows.Forms.BindingNavigator BankDetailsBindingNavigator;
        private System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton btnHelp;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Button btnCountry;
        internal System.Windows.Forms.TextBox txtUAEBankCode;
        internal System.Windows.Forms.Button btnBankBranch;
        internal System.Windows.Forms.ComboBox CboBankBranchID;
        internal System.Windows.Forms.ComboBox CboCountry;
        internal System.Windows.Forms.TextBox txtDescription;
        internal System.Windows.Forms.TextBox txtBankCode;
        internal System.Windows.Forms.TextBox txtAddress;
        internal System.Windows.Forms.TextBox txtTelephone;
        internal System.Windows.Forms.TextBox txtFax;
        internal System.Windows.Forms.TextBox txtEmail;
        internal System.Windows.Forms.TextBox txtWebsite;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripButton btnPrint;
        private System.Windows.Forms.ErrorProvider ErrProBank;
        private System.Windows.Forms.Timer TmBank;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblBankStatus;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label lblIBANCode;

    }
}