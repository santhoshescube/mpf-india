﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

using System.IO;
/* 
=================================================
Author      :		 <Author,,thasni>
Create date :        <Create Date,26 Aug 2013>
Description :	     <Description,, EmployeeInformation Form>
================================================
*/
namespace MyPayfriend
{
    public partial class FrmEmployee : Form
    {
        #region Private variables
        private bool MblnAddStatus; //Add/Update mode 

        // Permissions

        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        private bool MblnAddUpdatePermission = false; // To set add/update permission
        private bool MblnPrintEmailPermission = false;//To Set PrintEmail Permission

        private string PstrEmployeeNumberPrefix = "";
        public bool MblnIsFromExistingForm = false;
        private bool MblnIsEditMode = false;
        private bool MblnChangeStatus;
        //private bool PhotoAttach;
        private int MintCurrentRecCnt;    // Current recourd count
        private int MintRecordCnt;      // Total record count
        private long MintEmployeeID;
        private int MintRowNumber = 0;    // Row number

        private string MsMessageCommon;
        private string MsMessageCaption;
        private string RecentFilePath;

        //private string strEmployeeCodePrefix;  //Generate Employee code
        private Image imgImageFile;
        private Image imgImageFileThamnail;
        private ArrayList MsMessageArray;  // Error Message display
        private ArrayList MaStatusMessage;

        private DateTime dDOJ;

        public long PintEmployeeID = 0; //To Set EmployeeID Externally
        private int iLeavePolicyID = 0; // Assigning leave policy to this variable for checking

        ClsBLLEmployeeInformation MObjClsBLLEmployeeInformation;
        ClsLogWriter MObjClsLogWriter;      // Object of the LogWriter class
        ClsNotification MObjClsNotification; // Object of the Notification class
        MessageBoxIcon MsMessageBoxIcon;
        string strBindingOf = "Of ";
        #endregion // Private variables

        #region Constructor

        public FrmEmployee()
        {
            InitializeComponent();
            MsMessageCaption = ClsCommonSettings.MessageCaption; //Message caption
            MObjClsBLLEmployeeInformation = new ClsBLLEmployeeInformation();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);   // Application startup path
            MObjClsNotification = new ClsNotification();
            TmEmployee.Interval = ClsCommonSettings.TimerInterval; // Set time interval of the timer

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.Employee, this);

            strBindingOf = "من ";
            label10.Text = "شركة";
            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            ToolStripMenuAttach.Text = "تعلق";
            ToolStripMenuScan.Text = "المسح الضوئي";
            ToolStripMenuRemove.Text = "نزع";
            bnMoreActions.Text = " الإجراءات";
            mnuLeave.Text = "ترك دخول";
            mnuLoan.Text = "قرض";
            mnuSalaryAdvance.Text = "المقدمة الراتب";
            btnSalaryStructure.Text = "هيكل المرتبات";
            mnuLeaveStructure.Text = "ترك هيكل";
            passportToolStripMenuItem.Text = "جواز سفر";
            visaToolStripMenuItem.Text = "تأشيرة";
            licenseToolStripMenuItem.Text = "رخصة القيادة";
            qualificationToolStripMenuItem.Text = "المؤهل";
            nationalIDCardToolStripMenuItem.Text = "بطاقة الهوية الوطنية";
            insuranceCardToolStripMenuItem.Text = "بطاقة التأمين";
            labourCardToolStripMenuItem.Text = "بطاقة العمل";
            healthCardToolStripMenuItem.Text = "البطاقة الصحية";
            otherDocumentsToolStripMenuItem.Text = "وثائق أخرى";
            groupBox2.Text = "المقبل موعد تقييم";
            chkOCom.Text = chkOther.Text = "اختر من شركة أخرى";
            //label32.Text = "رئيس قسم";
        }
        #endregion // Constructor

        private void FrmEmployee_Load(object sender, EventArgs e)
        {
            try
            {
                nationalIDCardToolStripMenuItem.Text = ClsCommonSettings.NationalityCard;
                SetPermissions();
                LoadCombos(0);            // Method for loading comboboxes  
              
                LoadMessage();
                AddNewItem();
                //cboCompany.SelectedValue = ClsCommonSettings.CurrentCompanyID;
                dtpDateofBirth.MaxDate = ClsCommonSettings.GetServerDate();
                SetActionPermission();
                //lblDeviceUser.Visible = ClsCommonSettings.AttendanceLiveEnable;
                //txtDeviceUserID.Visible = ClsCommonSettings.AttendanceLiveEnable;
                if (PintEmployeeID > 0)
                {
                    MblnAddStatus = false;
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = PintEmployeeID;
                    MintCurrentRecCnt = MObjClsBLLEmployeeInformation.GetRowNumber();
                    DisplayEmployeeInformation();
                    SetBindingNavigatorButtons();
                    
                }
                SetAutoCompleteList();

                //if (ClsCommonSettings.IsHrPowerEnabled)
                //    BtnDesignation.Visible = false;
                //else
                //    BtnDesignation.Visible = true;

                if (!ClsCommonSettings.IsArabicViewEnabled)
                {
                    TxtFirstNameArb.BackColor = Color.White;
                }


               
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form Load:FrmEmployee_Load " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmEmployee_Load() " + Ex.Message.ToString());
            }
        }

        /// <summary>
        /// Loading the variables for messages
        /// </summary>
        private void LoadMessage()
        {
            MsMessageArray = new ArrayList();
            MaStatusMessage = new ArrayList();
            MsMessageArray = MObjClsNotification.FillMessageArray((int)FormID.Employee, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.Employee, ClsCommonSettings.ProductID);
        }
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.Employee, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }

        private void ChangeStatus(object sender, DataGridViewCellValidatingEventArgs e)
        {

        }

        private void changestatus(object sender, EventArgs e)
        {
           // this.SetAutoCompleteList();
            ChangeStatus();
        }

        private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void SetBindingNavigatorButtons()
        {
            BindingNavigatornPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";

            BindingNavigatorMoveFirstItem.Enabled =BindingNavigatorMovePreviousItem.Enabled =BindingNavigatorMoveLastItem.Enabled =BindingNavigatorMoveNextItem.Enabled = true;
        

            int iCurrentRec = Convert.ToInt32(BindingNavigatornPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                BindingNavigatorMoveLastItem.Enabled =BindingNavigatorMoveNextItem.Enabled = false;
              
            
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                BindingNavigatorMovePreviousItem.Enabled =BindingNavigatorMoveFirstItem.Enabled = false;
          
            
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            BtnEmail.Enabled =BtnPrint.Enabled = MblnPrintEmailPermission;         
            BindingNavigatorSaveItem.Enabled =BtnOk.Enabled = BtnSave.Enabled =BtnClear.Enabled = false;
            bnMoreActions.Enabled = !MblnAddStatus;

            
        }

        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
        private void EnableDisableButtons(bool bEnable)
        {
             bnMoreActions.Enabled =BindingNavigatorDeleteItem.Enabled =BindingNavigatorAddNewItem.Enabled = !bEnable;
            if (bEnable)
            {
                 MblnChangeStatus =BtnSave.Enabled =  BtnClear.Enabled =BtnOk.Enabled =BindingNavigatorSaveItem.Enabled =BtnEmail.Enabled =BtnPrint.Enabled = !bEnable;
                 
            }
            else
            {
               BtnEmail.Enabled =BtnPrint.Enabled = MblnPrintEmailPermission;
               MblnChangeStatus = BtnSave.Enabled = BtnClear.Enabled = BtnOk.Enabled =BindingNavigatorSaveItem.Enabled = bEnable;
                
            }
        }

        /// <summary>
        /// Setting the form for adding new Employee
        /// </summary>
        /// <returns></returns>
        private bool AddNewItem()
        {
            try
            {

                MblnAddStatus = true;
                MblnIsEditMode = false;

                ClearControls();    // Clear all fields in the form
                MintEmployeeID = 0;
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath = null;

                if (File.Exists(Application.StartupPath + "\\Images\\sample.png"))
                {
                    RecentPhotoPictureBox.Image = Image.FromFile(Application.StartupPath + "\\Images\\sample.png");
                    RecentPhotoPictureBoxThamNail.Image = Image.FromFile(Application.StartupPath + "\\Images\\sample.png");
                }

                MintRowNumber = 0;
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.AddNew, out MsMessageBoxIcon);
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                DisplayRecordCount();
                BindingAddNew();
                cboWorkingAt.SelectedValue = ClsCommonSettings.CurrentCompanyID.ToInt32();
                GenerateEmployeeCode();
                EnableDisableButtons(true);
                btnWorkLocation.Enabled = cboWorkLocation.Enabled =cboWorkingAt.Enabled = true;
                lblTransfer.Visible = lblTransferDate.Visible = false;
                DtpDateofJoining.Enabled = true;
                cboReportingTo.Text = "";
                btnConfirm.Visible = DtpProbationEndDate.Enabled = cboWorkStatus.SelectedValue.ToInt32() == 7;
                cboWorkingAt.SelectedValue = ClsCommonSettings.CurrentCompanyID;
                //cboWorkingAt.Enabled = false;
                cboCommissionStructure.SelectedIndex = -1;
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form AddNewItem:AddNewItem " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());
                return false;
            }
        }
        private void DisplayRecordCount()//Dispalying Total Record Count
        {
            MintRecordCnt = MObjClsBLLEmployeeInformation.RecCountNavigate(cboCompany.SelectedValue.ToInt32());
        }

        private void CompanySettings()
        {
            try
            {
                int CompanyID = 0;

                DataTable DT;

                if (cboWorkingAt.SelectedIndex != -1)
                {
                    CompanyID = cboWorkingAt.SelectedValue.ToInt32();
                }
                else
                {
                    CompanyID = ClsCommonSettings.CurrentCompanyID;
                }


                DT = MObjClsBLLEmployeeInformation.GetCompanySetting(CompanyID);
                if (DT.Rows.Count > 0)
                {
                    PstrEmployeeNumberPrefix = DT.Rows[0]["Prefix"].ToString();
                }
                else
                {
                    PstrEmployeeNumberPrefix = "";
                }
            }
            catch
            {
            }
           

        }
        private void GenerateEmployeeCode()
        {
            try
            {
                CompanySettings();
                TxtEmployeeNumber.Text = PstrEmployeeNumberPrefix + (MObjClsBLLEmployeeInformation.GenerateEmployeeCode()).ToString();

            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in GenerateEmployeeCode() " + ex.Message);
                MObjClsLogWriter.WriteLog("Error in GenerateEmployeeCode) " + ex.Message, 2);
            }
        }

        private void BindingAddNew()
        {
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt + 1) + "";
            BindingNavigatornPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
            MintCurrentRecCnt = MintRecordCnt + 1;
            if (MintCurrentRecCnt == 1)
            {
                 BindingNavigatorMoveLastItem.Enabled =BindingNavigatorMoveNextItem.Enabled =BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = false;
               
            }
            else
            {
                BindingNavigatorMovePreviousItem.Enabled =BindingNavigatorMoveFirstItem.Enabled = true;
              
                BindingNavigatorMoveLastItem.Enabled =BindingNavigatorMoveNextItem.Enabled = false;
                
            }
        }

        public void GetFullName()
        {
            txtEmployeeFullName.Text = TxtFirstName.Text.Trim() + " " + TxtMiddleName.Text.Trim() + " " + TxtLastName.Text.Trim();
            txtEmployeeFullNameArb.Text = TxtFirstNameArb.Text.Trim() + " " + TxtMiddleNameArb.Text.Trim() + " " + TxtLastNameArb.Text.Trim();
        }

        /// <summary>
        /// Clearing fields in the form
        /// </summary>
        /// 
        private void ClearControls()
        {
            // Setting default values 
            ErrEmployee.Clear();
            //cboCompany.SelectedIndex = 0;
            cboMaritalStatus.SelectedIndex = -1;
            rdbMale.Checked = true;
            txtDeviceUserID.Text = string.Empty;
            cboBloodGroup.SelectedIndex = -1;
             txtSearch.Text = AccountNumberTextBox.Text = cboBloodGroup.Text = txtNotes.Text = txtPhone.Text = txtLocalMobile.Text = txtAddress1.Text =  txtIdentificationMarks.Text = txtPersonID.Text = txtSpouseName.Text = txtMothersName.Text = txtFathersName.Text = txtEmail.Text = TxtOfficialEmail.Text = txtEmployeeFullName.Text = txtEmployeeFullName.Text = TxtLastName.Text = TxtMiddleName.Text = TxtFirstName.Text = string.Empty;
             dtpDateofBirth.Value = DtpProbationEndDate.Value = DtpDateofJoining.Value = ClsCommonSettings.GetServerDate();  
            cboVacationPolicy.SelectedIndex=cboReportingTo.SelectedIndex=cboWorkLocation.SelectedIndex = cboLeavePolicy.SelectedIndex = cboWorkPolicy.SelectedIndex = TransactionTypeReferenceComboBox.SelectedIndex = cboReligion.SelectedIndex = cboNationality.SelectedIndex = cboMothertongue.SelectedIndex = cboEmploymentType.SelectedIndex = CboSalutation.SelectedIndex = cboWorkingAt.SelectedIndex = cboDepartment.SelectedIndex = cboDesignation.SelectedIndex = -1;
            ComBankAccIdComboBox.SelectedIndex = CboEmployeeBankname.SelectedIndex = cboGrade.SelectedIndex = BankNameReferenceComboBox.SelectedIndex = -1;
            BtnWorkstatus.Enabled =cboWorkStatus.Enabled = true;
            TxtFirstNameArb.Text = TxtMiddleNameArb.Text = TxtLastNameArb.Text = txtEmployeeFullNameArb.Text = txtEmergencyAddress.Text = txtEmergencyPhone.Text = txtLocalAddress.Text = txtLocalPhone.Text = string.Empty;
            cboReportingTo.Text=cboVacationPolicy.Text=TransactionTypeReferenceComboBox.Text =cboReligion.Text =cboNationality.Text =cboMothertongue.Text =cboEmploymentType.Text = string.Empty;
            cboCountry.SelectedIndex = -1;
            cboWorkLocation.SelectedIndex = CboVisaObtained.SelectedIndex = -1;
            txtNoticePeriod.Text=TxtMiddleNameArb.Text = "";
            txtAddress2.Text = txtCity.Text = txtState.Text = txtZipCode.Text = "";
            RecentPhotoPictureBox.Image = null;
            RecentPhotoPictureBoxThamNail.Image = null;
            RecentPhotoPictureBox.Refresh();
            RecentPhotoPictureBoxThamNail.Refresh(); 
            TbEmployee.SelectedTab = TpGeneral;
            TxtEmployeeNumber.Focus();
            dgvLanguages.Rows.Clear();
            //dgvLanguages.Rows.Count = 1; 
            TxtEmployeeNumber.Tag = 0;
            DtpDateofJoining.Enabled = DtpProbationEndDate.Enabled = true;
            chkOther.Checked = false;
            chkOCom.Checked = false;
            cboHOD.SelectedIndex = -1; 
            if (cboWorkStatus.Items.Count >= 6) cboWorkStatus.SelectedValue = 6;
            EnableDisableButtons(false);
        }
        /// <summary>
        /// Loading comboboxes with values
        /// </summary>
        /// <returns></returns>
        /// 
        private bool LoadCombos(int intType)
        {
            try
            {
                DataTable datCombos = new DataTable();
                if (ClsCommonSettings.IsArabicView)
                {
                    if (intType == 0 || intType == 1)
                    {
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "DepartmentID, DepartmentArb AS Department", "DepartmentReference", "" });
                        cboDepartment.ValueMember = "DepartmentID";
                        cboDepartment.DisplayMember = "Department";
                        cboDepartment.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 2)
                    {
                        datCombos = null;
                        //if (ClsCommonSettings.IsHrPowerEnabled)
                        //{
                        //    datCombos = MObjClsBLLEmployeeInformation.GetDesignation(cboWorkingAt.SelectedValue.ToInt32());
                        //}
                        //else
                        //{
                            datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "DesignationID, DesignationArb AS Designation", "DesignationReference", "" });
                        //}
                        
                        cboDesignation.ValueMember = "DesignationID";
                        cboDesignation.DisplayMember = "Designation";
                        cboDesignation.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 4)
                    {
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "ReligionID, ReligionArb AS Religion", "ReligionReference", "" });
                        cboReligion.ValueMember = "ReligionID";
                        cboReligion.DisplayMember = "Religion";
                        cboReligion.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 5)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "NationalityID, NationalityArb AS Nationality", "NationalityReference", "" });
                        cboNationality.ValueMember = "NationalityID";
                        cboNationality.DisplayMember = "Nationality";
                        cboNationality.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 6)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "SalutationID, SalutationArb AS Salutation", "SalutationReference", "" });
                        CboSalutation.ValueMember = "SalutationID";
                        CboSalutation.DisplayMember = "Salutation";
                        CboSalutation.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 7)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "distinct CompanyID, CompanyName", "CompanyMaster", "CompanyID IN "+ 
		"(SELECT CompanyID FROM UserCompanyDetails WHERE UserID ="+ ClsCommonSettings.UserID+") " });
                        cboWorkingAt.ValueMember = "CompanyID";
                        cboWorkingAt.DisplayMember = "CompanyName";
                        cboWorkingAt.DataSource = datCombos;
                        cboCompany.ValueMember = "CompanyID";
                        cboCompany.DisplayMember = "CompanyName";
                        DataTable dtSearch = datCombos.Copy();
                        DataRow dr = dtSearch.NewRow();
                        dr["CompanyID"] = 0;
                        if (ClsCommonSettings.IsArabicView)
                            dr["CompanyName"] = "أي";
                        else
                            dr["CompanyName"] = "All";
                        dtSearch.Rows.InsertAt(dr, 0);
                        cboCompany.DataSource = dtSearch;
                    }



                    if (intType == 0 || intType == 9)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "MotherTongueID, MotherTongueArb AS MotherTongue", "MotherTongueReference", "" });
                        cboMothertongue.ValueMember = "MotherTongueID";
                        cboMothertongue.DisplayMember = "MotherTongue";
                        cboMothertongue.DataSource = datCombos;

                    }

                    if (intType == 0 || intType == 9)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "MotherTongueID, MotherTongueArb AS MotherTongue", "MotherTongueReference", "" });
                        ColLanguages.ValueMember = "MotherTongueID";
                        ColLanguages.DisplayMember = "MotherTongue";
                        ColLanguages.DataSource = datCombos;
                    }

                    if (intType == 0 || intType == 10)
                    {
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "WorkStatusID, WorkStatusArb AS WorkStatus", "WorkStatusReference", "" });
                        cboWorkStatus.ValueMember = "WorkStatusID";
                        cboWorkStatus.DisplayMember = "WorkStatus";
                        cboWorkStatus.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 11)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "TransactionTypeID, TransactionTypeArb AS TransactionType", "TransactionTypeReference", "" });
                        TransactionTypeReferenceComboBox.ValueMember = "TransactionTypeID";
                        TransactionTypeReferenceComboBox.DisplayMember = "TransactionType";
                        TransactionTypeReferenceComboBox.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 16)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "EmploymentTypeID,EmploymentTypeArb AS EmploymentType ", "EmploymentTypeReference", "" });
                        cboEmploymentType.ValueMember = "EmploymentTypeID";
                        cboEmploymentType.DisplayMember = "EmploymentType";
                        cboEmploymentType.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 18)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "WorkPolicyID, WorkPolicy", "PayWorkPolicyMaster", "" });
                        cboWorkPolicy.ValueMember = "WorkPolicyID";
                        cboWorkPolicy.DisplayMember = "WorkPolicy";
                        cboWorkPolicy.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 19)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "LeavePolicyID, LeavePolicyName", "PayLeavePolicyMaster", "" });
                        cboLeavePolicy.ValueMember = "LeavePolicyID";
                        cboLeavePolicy.DisplayMember = "LeavePolicyName";
                        cboLeavePolicy.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 20)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "BankBranchReference.BankBranchID, isnull(BankReference.BankNameArb,'')+ ' - ' + isnull(BankBranchReference.BankBranchName,'')   as Description  ", "BankBranchReference INNER JOIN BankReference ON BankReference.BankID = BankBranchReference.BankID", "" });
                        CboEmployeeBankname.ValueMember = "BankBranchID";
                        CboEmployeeBankname.DisplayMember = "Description";
                        CboEmployeeBankname.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 21)
                    {
                        cboWorkLocation.Text = "";
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "WorkLocationID,LocationName", "WorkLocationReference", " CompanyID=" + cboWorkingAt.SelectedValue.ToInt32() });
                        cboWorkLocation.ValueMember = "WorkLocationID";
                        cboWorkLocation.DisplayMember = "LocationName";
                        cboWorkLocation.DataSource = datCombos;
                        cboWorkLocation.Text = "";
                    }
                    if (intType == 0 || intType == 22)
                    {

                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "VacationPolicyID,VacationPolicy", "PayVacationPolicy", "" });
                        cboVacationPolicy.ValueMember = "VacationPolicyID";
                        cboVacationPolicy.DisplayMember = "VacationPolicy";
                        cboVacationPolicy.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 24)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "MaritalStatusID,MaritalStatusArb AS MaritalStatus ", "MaritalStatusReference", "" });
                        cboMaritalStatus.ValueMember = "MaritalStatusID";
                        cboMaritalStatus.DisplayMember = "MaritalStatus";
                        cboMaritalStatus.DataSource = datCombos;
                    }

                    if (intType == 0 || intType == 25)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "CountryID,CountryNameArb AS CountryName", "CountryReference", "" });
                        cboCountry.ValueMember = "CountryID";
                        cboCountry.DisplayMember = "CountryName";
                        cboCountry.DataSource = datCombos;
                    }


                    if (intType == 0 || intType == 26)
                    {
                         
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "GradeID,GradeArb AS Grade", "GradeReference", "" });
                        cboGrade.ValueMember = "GradeID";
                        cboGrade.DisplayMember = "Grade";
                        cboGrade.DataSource = datCombos;
                    }



                    if (intType == 0 || intType == 27)
                    {
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "CompanyID, CompanyName", "CompanyMaster", "" });
                        CboVisaObtained.ValueMember = "CompanyID";
                        CboVisaObtained.DisplayMember = "CompanyName";
                        CboVisaObtained.DataSource = datCombos;
                    }

                    if (intType == 0 || intType == 28)
                    {
                        datCombos = this.MObjClsBLLEmployeeInformation.FillCombos(new string[] { "DivisionID, DivisionArb AS Division", "DivisionReference", "" });
                        this.cboDivision.ValueMember = "DivisionID";
                        this.cboDivision.DisplayMember = "Division";
                        this.cboDivision.DataSource = datCombos;
                    }


                }
                else
                {
                    if (intType == 0 || intType == 1)
                    {
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "DepartmentID, Department", "DepartmentReference", "" });
                        cboDepartment.ValueMember = "DepartmentID";
                        cboDepartment.DisplayMember = "Department";
                        cboDepartment.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 2)
                    {
                        datCombos = null;
                        //if (ClsCommonSettings.IsHrPowerEnabled)
                        //{
                        //    datCombos = MObjClsBLLEmployeeInformation.GetDesignation(cboWorkingAt.SelectedValue.ToInt32());
                        //}
                        //else
                        //{
                            datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "DesignationID, Designation", "DesignationReference", "" });
                        //}

                        cboDesignation.ValueMember = "DesignationID";
                        cboDesignation.DisplayMember = "Designation";
                        cboDesignation.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 4)
                    {
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "ReligionID, Religion", "ReligionReference", "" });
                        cboReligion.ValueMember = "ReligionID";
                        cboReligion.DisplayMember = "Religion";
                        cboReligion.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 5)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "NationalityID, Nationality", "NationalityReference", "" });
                        cboNationality.ValueMember = "NationalityID";
                        cboNationality.DisplayMember = "Nationality";
                        cboNationality.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 6)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "SalutationID, Salutation", "SalutationReference", "" });
                        CboSalutation.ValueMember = "SalutationID";
                        CboSalutation.DisplayMember = "Salutation";
                        CboSalutation.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 7)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "CompanyID, CompanyName", "CompanyMaster", "CompanyID IN "+ 
		"(SELECT CompanyID FROM UserCompanyDetails WHERE UserID ="+ ClsCommonSettings.UserID+") " });
                        cboWorkingAt.ValueMember = "CompanyID";
                        cboWorkingAt.DisplayMember = "CompanyName";
                        cboWorkingAt.DataSource = datCombos;
                        cboCompany.ValueMember = "CompanyID";
                        cboCompany.DisplayMember = "CompanyName";
                        DataTable dtSearch = datCombos.Copy();
                        DataRow dr = dtSearch.NewRow();
                        dr["CompanyID"] = 0;
                        dr["CompanyName"] = "All";
                        dtSearch.Rows.InsertAt(dr, 0);
                        cboCompany.DataSource = dtSearch;
                    }
                    if (intType == 0 || intType == 9)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "MotherTongueID, MotherTongue", "MotherTongueReference", "" });
                        cboMothertongue.ValueMember = "MotherTongueID";
                        cboMothertongue.DisplayMember = "MotherTongue";
                        cboMothertongue.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 9)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "MotherTongueID, MotherTongue", "MotherTongueReference", "" });

                        ColLanguages.ValueMember = "MotherTongueID";
                        ColLanguages.DisplayMember = "MotherTongue";
                        ColLanguages.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 10)
                    {
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "WorkStatusID, WorkStatus", "WorkStatusReference", "" });
                        cboWorkStatus.ValueMember = "WorkStatusID";
                        cboWorkStatus.DisplayMember = "WorkStatus";
                        cboWorkStatus.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 11)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "TransactionTypeID, TransactionType", "TransactionTypeReference", "" });
                        TransactionTypeReferenceComboBox.ValueMember = "TransactionTypeID";
                        TransactionTypeReferenceComboBox.DisplayMember = "TransactionType";
                        TransactionTypeReferenceComboBox.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 16)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "EmploymentTypeID,EmploymentType ", "EmploymentTypeReference", "" });
                        cboEmploymentType.ValueMember = "EmploymentTypeID";
                        cboEmploymentType.DisplayMember = "EmploymentType";
                        cboEmploymentType.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 18)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "WorkPolicyID, WorkPolicy", "PayWorkPolicyMaster", "" });
                        cboWorkPolicy.ValueMember = "WorkPolicyID";
                        cboWorkPolicy.DisplayMember = "WorkPolicy";
                        cboWorkPolicy.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 19)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "LeavePolicyID, LeavePolicyName", "PayLeavePolicyMaster", "" });
                        cboLeavePolicy.ValueMember = "LeavePolicyID";
                        cboLeavePolicy.DisplayMember = "LeavePolicyName";
                        cboLeavePolicy.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 20)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "BankBranchReference.BankBranchID, isnull(BankReference.BankName,'')+ ' - ' + isnull(BankBranchReference.BankBranchName,'')   as Description  ", "BankBranchReference INNER JOIN BankReference ON BankReference.BankID = BankBranchReference.BankID ORDER BY BankBranchReference.BankBranchName  ", "" });
                        CboEmployeeBankname.ValueMember = "BankBranchID";
                        CboEmployeeBankname.DisplayMember = "Description";
                        CboEmployeeBankname.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 21)
                    {
                        cboWorkLocation.Text = "";
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "WorkLocationID,LocationName", "WorkLocationReference", " CompanyID=" + cboWorkingAt.SelectedValue.ToInt32() });
                        cboWorkLocation.ValueMember = "WorkLocationID";
                        cboWorkLocation.DisplayMember = "LocationName";
                        cboWorkLocation.DataSource = datCombos;
                        cboWorkLocation.Text = "";
                    }
                    if (intType == 0 || intType == 22)
                    {

                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "VacationPolicyID,VacationPolicy", "PayVacationPolicy", "" });
                        cboVacationPolicy.ValueMember = "VacationPolicyID";
                        cboVacationPolicy.DisplayMember = "VacationPolicy";
                        cboVacationPolicy.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 24)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "MaritalStatusID,MaritalStatus", "MaritalStatusReference", "" });
                        cboMaritalStatus.ValueMember = "MaritalStatusID";
                        cboMaritalStatus.DisplayMember = "MaritalStatus";
                        cboMaritalStatus.DataSource = datCombos;
                    }

                    if (intType == 0 || intType == 25)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "" });
                        cboCountry.ValueMember = "CountryID";
                        cboCountry.DisplayMember = "CountryName";
                        cboCountry.DataSource = datCombos;
                    }

                    if (intType == 0 || intType == 26)
                    {
                        datCombos = null;
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "GradeID, Grade", "GradeReference", "" });
                        cboGrade.ValueMember = "GradeID";
                        cboGrade.DisplayMember = "Grade";
                        cboGrade.DataSource = datCombos;
                    }
                    if (intType == 0 || intType == 27)
                    {
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "CompanyID, CompanyName", "CompanyMaster", "" });
                        CboVisaObtained.ValueMember = "CompanyID";
                        CboVisaObtained.DisplayMember = "CompanyName";
                        CboVisaObtained.DataSource = datCombos;
                    }

                    if (intType == 0 || intType == 28)
                    {
                        datCombos = this.MObjClsBLLEmployeeInformation.FillCombos(new string[] { "DivisionID, DivisionArb AS Division", "DivisionReference", "" });
                        this.cboDivision.ValueMember = "DivisionID";
                        this.cboDivision.DisplayMember = "Division";
                        this.cboDivision.DataSource = datCombos;
                    }
                }

                if (intType == 0)
                {
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { 
                        "CommissionStructureID, StructureName", "CommissionStructureMaster", "" });
                    cboCommissionStructure.ValueMember = "CommissionStructureID";
                    cboCommissionStructure.DisplayMember = "StructureName";
                    cboCommissionStructure.DataSource = datCombos;

                    datCombos = null;
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { 
                        "ProcessTypeid, ProcessType", "payprocesstypereference", "" });
                    cboProcessType.ValueMember = "ProcessTypeid";
                    cboProcessType.DisplayMember = "ProcessType";
                    cboProcessType.DataSource = datCombos;

                    if (cboProcessType.Items.Count > 0)
                        cboProcessType.SelectedValue = 1;
                }

                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());
                return false;
            }
        }

        private void LoadReportingto()
        {
           
                DataTable datTemp = new DataTable();
                if (ClsCommonSettings.IsArabicView)
                    datTemp = MObjClsBLLEmployeeInformation.GetReportingto(TxtEmployeeNumber.Tag.ToInt32(),chkOther.Checked? -1: cboWorkingAt.SelectedValue.ToInt32());
                else
                    datTemp = MObjClsBLLEmployeeInformation.GetReportingto(TxtEmployeeNumber.Tag.ToInt32(), chkOther.Checked ? -1 : cboWorkingAt.SelectedValue.ToInt32());

                cboReportingTo.DataSource = datTemp;
                cboReportingTo.ValueMember = "EmployeeID";
                cboReportingTo.DisplayMember = "EmployeeName";
            
        }


        private void LoadHOD()
        {

            DataTable datTemp = new DataTable();
            if (ClsCommonSettings.IsArabicView)
                datTemp = MObjClsBLLEmployeeInformation.GetHOD(TxtEmployeeNumber.Tag.ToInt32(), chkOCom.Checked ? -1 : cboWorkingAt.SelectedValue.ToInt32());
            else
                datTemp = MObjClsBLLEmployeeInformation.GetHOD(TxtEmployeeNumber.Tag.ToInt32(), chkOCom.Checked ? -1 : cboWorkingAt.SelectedValue.ToInt32());


            if (datTemp.Rows.Count > 0)
            {
                cboHOD.DataSource = datTemp;
                cboHOD.ValueMember = "EmployeeID";
                cboHOD.DisplayMember = "EmployeeName";
            }
        }

        /// <summary>
        /// Validating Employee information
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmployeeFields()
        {
            // Employee number
            if (!TxtEmployeeNumber.ReadOnly && MObjClsBLLEmployeeInformation.CheckDuplication(MblnAddStatus, new string[] { TxtEmployeeNumber.Text.Replace("'", "").Trim() }, MintEmployeeID, cboWorkingAt.SelectedValue.ToInt32()))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 413, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TxtEmployeeNumber, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmEmployee.Enabled = true;
                TxtEmployeeNumber.Focus();
                return false;
            }
            if (TxtEmployeeNumber.Text.Trim().Length == 0)
            {
                //MsMessageCommon = "Please enter First Name";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.EmpNoValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TxtEmployeeNumber, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TxtEmployeeNumber.Focus();
                return false;
            }


            if (MObjClsBLLEmployeeInformation.CheckDuplication(MblnAddStatus, new string[] { TxtEmployeeNumber.Text.Replace("'", "").Trim() }, MintEmployeeID, cboWorkingAt.SelectedValue.ToInt32()))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 413, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TxtEmployeeNumber, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmEmployee.Enabled = true;
                TxtEmployeeNumber.Focus();
                return false;
            }

            // Salutation
            if (Convert.ToInt32(CboSalutation.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Salutation";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.SalutationValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(CboSalutation, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                CboSalutation.Focus();
                return false;
            }
            // First name
            if (TxtFirstName.Text.Trim().Length == 0)
            {
                //MsMessageCommon = "Please enter First Name";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.FirstNameValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TxtFirstName, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TxtFirstName.Focus();
                return false;
            }
            if (ClsCommonSettings.IsArabicViewEnabled)
            {
                // First name Arabic
                if (TxtFirstNameArb.Text.Trim().Length == 0)
                {
                    //MsMessageCommon = "Please enter First Name Arabic";
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.FirstNameValidation, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    ErrEmployee.SetError(TxtFirstNameArb, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TxtFirstNameArb.Focus();
                    return false;
                }
            }
            // Working company
            if (Convert.ToInt32(cboWorkingAt.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Working At";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.WorkingAtValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboWorkingAt, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboWorkingAt.Focus();
                return false;
            }

            if (Convert.ToInt32(cboWorkStatus.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Working At";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.WorkstatusValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboWorkStatus, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboWorkStatus.Focus();
                return false;
            }
            else if (cboWorkStatus.SelectedValue.ToInt32() < 6 && cboWorkStatus.Enabled)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.SettlementWorkStayus, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboWorkStatus, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboWorkStatus.Focus();
                return false;
            }

            // Department
            if (Convert.ToInt32(cboDepartment.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Department";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.DepartmentValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboDepartment, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboDepartment.Focus();
                return false;
            }
            if (Convert.ToInt32(this.cboDivision.SelectedValue) == 0)
            {
                this.MsMessageCommon = "Please select Division";
                MessageBox.Show(this.MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, this.MsMessageBoxIcon);
                this.ErrEmployee.SetError((Control)this.cboDivision, this.MsMessageCommon.Replace("#", "").Trim());
                this.LblEmployeeStatus.Text = this.MsMessageCommon.Remove(0, this.MsMessageCommon.IndexOf("#") + 1);
                this.TbEmployee.SelectedTab = this.TpGeneral;
                this.cboDivision.Focus();
                return false;
            }
            // Designation
            if (Convert.ToInt32(cboDesignation.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Designation";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.DesignationValidation, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboDesignation, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboDesignation.Focus();
                return false;
            }
            TxtFirstName.Text = TxtFirstName.Text.Trim().Replace("'", "`");
            TxtMiddleName.Text = TxtMiddleName.Text.Trim().Replace("'", "`");
            TxtLastName.Text = TxtLastName.Text.Trim().Replace("'", "`");
            txtEmployeeFullName.Text = txtEmployeeFullName.Text.Trim().Replace("'", "`");

            TxtFirstNameArb.Text = TxtFirstNameArb.Text.Trim().Replace("'", "`");
            TxtMiddleNameArb.Text = TxtMiddleNameArb.Text.Trim().Replace("'", "`");
            TxtLastNameArb.Text = TxtLastNameArb.Text.Trim().Replace("'", "`");
            txtEmployeeFullNameArb.Text = txtEmployeeFullNameArb.Text.Trim().Replace("'", "`");  
            // Work Policy
            if (Convert.ToInt32(cboWorkPolicy.SelectedValue) == 0)
            {
                //MsMessageCommon = "Please select Designation";
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.WorkPolicy, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(cboWorkPolicy, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboWorkPolicy.Focus();
                return false;
            }

            

            if (TxtOfficialEmail.Text.Trim().Length > 0 && !MObjClsBLLEmployeeInformation.CheckValidEmail(TxtOfficialEmail.Text.Trim()))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 411, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TxtOfficialEmail, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TbContacts;
                TxtOfficialEmail.Focus();
                return false;
            }
            if (txtEmail.Text.Trim().Length > 0 && !MObjClsBLLEmployeeInformation.CheckValidEmail(txtEmail.Text.Trim()))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 411, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(txtEmail, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TbContacts;
                txtEmail.Focus();
                return false;
            }
            

            //Age should be greater than or equal to 18 years.
            if (Convert.ToDateTime(DtpDateofJoining.Value.ToString()) < Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Year, 18, dtpDateofBirth.Value.Date))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 431, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(dtpDateofBirth, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                dtpDateofBirth.Focus();
                return false;
            }
            //The joining date cannot be before the 'Date of Birth'.(c)
            if (Convert.ToDateTime(DtpDateofJoining.Value.ToString()) < Convert.ToDateTime(dtpDateofBirth.Value.ToString()))
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 432, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(DtpDateofJoining, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                DtpDateofJoining.Focus();
                return false;
            }

            //The 'Date of Joining' should be less than the 'Current Date'.
            if (Convert.ToDateTime(DtpDateofJoining.Value.Date.ToString()) > ClsCommonSettings.GetServerDate().Date)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 433, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(DtpDateofJoining, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                DtpDateofJoining.Focus();
                return false;
            }

            if (DtpDateofJoining.Value.Date.ToString().ToDateTime() > DtpProbationEndDate.Value.Date.ToString().ToDateTime() && DtpProbationEndDate.Enabled)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 430, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(DtpProbationEndDate, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                DtpProbationEndDate.Focus();
                return false;
            }

            
            
            string strJoingDate = DtpDateofJoining.Value.Date.ToString("dd MMM yyyy");
            string strFinYear = MObjClsBLLEmployeeInformation.GetCompanyStartDate(cboWorkingAt.SelectedValue.ToInt32());
            if (strFinYear != "" && strJoingDate != "")
            {
                DateTime jJoingDate = Convert.ToDateTime(strJoingDate);
                DateTime jFinYear = Convert.ToDateTime(strFinYear);

                if (jJoingDate < jFinYear && (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strTransferDate == "" || MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strTransferDate == null))
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 445, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    ErrEmployee.SetError(DtpDateofJoining, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TbEmployee.SelectedTab = TpGeneral;
                    DtpDateofJoining.Focus();
                    return false;


                }
            }
            string strEmployeeTransferDate = MObjClsBLLEmployeeInformation.GetEmployeeTransferDate(cboWorkingAt.SelectedValue.ToInt32(), MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID);
            //if (strEmployeeTransferDate != "" && strJoingDate != "")
            //{

            //    if (Convert.ToDateTime(strJoingDate) < Convert.ToDateTime(strEmployeeTransferDate))
            //    {
            //        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 455, out MsMessageBoxIcon);

            //        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            //        ErrEmployee.SetError(DtpDateofJoining, MsMessageCommon.Replace("#", "").Trim());
            //        LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            //        TbEmployee.SelectedTab = TpGeneral;
            //        DtpDateofJoining.Focus();
            //        return false;

            //    }
            //}
            

            if (Convert.ToInt32(TransactionTypeReferenceComboBox.SelectedValue) == 0)
            {
                //Please select the 'Employer's bank name'.
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 443, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                ErrEmployee.SetError(TransactionTypeReferenceComboBox, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = Tpwork;
                TransactionTypeReferenceComboBox.Focus();
                return false;
            }
            
            if (!MblnAddStatus && MObjClsBLLEmployeeInformation.FillCombos(new string[] { "AttendanceID", "PayAttendanceMaster", "EmployeeID = " + MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID }).Rows.Count > 0 &&( DtpDateofJoining.Value.Date!= Convert.ToDateTime (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofJoining.ToString("dd-MMM-yyyy"))))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 447, out MsMessageBoxIcon);
                ErrEmployee.SetError(DtpDateofJoining, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                DtpDateofJoining.Focus();
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);

                return false;
            }

            if (!MblnAddStatus && MObjClsBLLEmployeeInformation.FillCombos(new string[] { "LeaveID", "PayEmployeeLeaveDetails", "EmployeeID = " + MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID }).Rows.Count > 0 && (Convert.ToDateTime(DtpDateofJoining.Value.Date.ToString("dd-MMM-yyyy")) != Convert.ToDateTime(MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofJoining.ToString("dd-MMM-yyyy"))))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 448, out MsMessageBoxIcon);
                ErrEmployee.SetError(DtpDateofJoining, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                DtpDateofJoining.Focus();
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);

                return false;
            }

            if (!MblnAddStatus && (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intPolicyID!=cboWorkPolicy.SelectedValue.ToInt32() && MObjClsBLLEmployeeInformation.AttendancetExists(MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID)))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 449, out MsMessageBoxIcon);
                ErrEmployee.SetError(cboWorkPolicy, MsMessageCommon.Replace("#", "").Trim());
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TbEmployee.SelectedTab = TpGeneral;
                cboWorkPolicy.Focus();
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);

                return false;
            }
            if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intLeavePolicyID > 0)
            {
                if (!MblnAddStatus && (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intLeavePolicyID != cboLeavePolicy.SelectedValue.ToInt32() && MObjClsBLLEmployeeInformation.AttendancetExists(MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID)))
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 450, out MsMessageBoxIcon);
                    ErrEmployee.SetError(cboLeavePolicy, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TbEmployee.SelectedTab = TpGeneral;
                    cboLeavePolicy.Focus();
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);

                    return false;
                }
            }


            if (TransactionTypeReferenceComboBox.SelectedValue.ToInt32() == 1/*Bank*/ || TransactionTypeReferenceComboBox.SelectedValue.ToInt32() == 3/*WPS*/)
            {
                if (Convert.ToInt32(BankNameReferenceComboBox.SelectedValue) == 0)
                {
                    //Please select the 'Employer's bank name'.
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 446, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    ErrEmployee.SetError(BankNameReferenceComboBox, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TbEmployee.SelectedTab = Tpwork;
                    BankNameReferenceComboBox.Focus();
                    return false;
                }


                if (Convert.ToInt32(ComBankAccIdComboBox.SelectedValue) == 0)
                {
                    //Please select the 'Employer's bank account'.
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 441, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    ErrEmployee.SetError(ComBankAccIdComboBox, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TbEmployee.SelectedTab = Tpwork;
                    ComBankAccIdComboBox.Focus();
                    return false;
                }
                if (Convert.ToInt32(CboEmployeeBankname.SelectedValue) == 0)
                {
                    //Please select the 'Employee's bank account'.
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 440, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    ErrEmployee.SetError(CboEmployeeBankname, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TbEmployee.SelectedTab = Tpwork;
                    CboEmployeeBankname.Focus();
                    return false;
                }



                if (AccountNumberTextBox.Text.Trim() == "")
                {
                    //Please enter the Account number.
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 442, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    ErrEmployee.SetError(AccountNumberTextBox, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TbEmployee.SelectedTab = Tpwork;
                    AccountNumberTextBox.Focus();
                    return false;
                }
                if (TransactionTypeReferenceComboBox.SelectedValue.ToInt32() == 3 /*WPS*/ && cboEmploymentType.SelectedValue.ToInt32() != 5 /*Permanent*/)
                {
                    if (ClsCommonSettings.IsArabicView)
                        MsMessageCommon = "الموظف الدائم فقط يمكن أن يكون لها نظام حماية الأجور نوع المعاملة";
                    else
                        MsMessageCommon = "Only Permanent employee can have 'WPS' transaction type";
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);

                    ErrEmployee.SetError(TransactionTypeReferenceComboBox, MsMessageCommon.Replace("#", "").Trim());
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TbEmployee.SelectedTab = Tpwork;
                    TransactionTypeReferenceComboBox.Focus();
                    return false;
                }


            }
          



            return true;
        }

        /// <summary>
        /// Validating and saving item master details
        /// </summary>
        private bool SaveEmployeeInformation()
        {
            // Validation for the fields
            if (ClsMainSettings.blnProductStatus)
            {

                if (ValidateEmployeeFields())
                {
                    // Saving new employee details
                    if (SaveEmployeeDetails())
                    {
                        if (MblnAddStatus == true)
                        {
                            MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 444, out MsMessageBoxIcon);
                            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                            {
                                using (FrmSalaryStructure objSalaryStructure = new FrmSalaryStructure())
                                {
                                    objSalaryStructure.PintCompanyID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID;
                                    objSalaryStructure.PintEmployeeID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                                    objSalaryStructure.ShowDialog();
                                }
                            }
                        }
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, MblnAddStatus ? 2 : 21, out MsMessageBoxIcon);
                        if (MblnAddStatus)
                            MintRecordCnt = MintRecordCnt + 1;
                        //MintCurrentRecCnt = MintCurrentRecCnt + 1;
                        // BindingNavigatorMovePreviousItem_Click(null, null);

                        LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmEmployee.Enabled = true;
                        cboWorkingAt.Enabled = false;
                        SetAutoCompleteList();
                        MblnAddStatus = false;
                        MblnIsEditMode = true;
                        if (MintRowNumber > 0)
                            MintCurrentRecCnt = BindingNavigatornPositionItem.Text.ToInt32();
                        SetBindingNavigatorButtons();
                        TxtEmployeeNumber.Tag = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                        TbEmployee.SelectedTab = TpGeneral;


                    }
                    return true;
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Setting values to the Employee object and saves
        /// </summary>
        /// <returns></returns>
        private bool SaveEmployeeDetails()
        {
            try
            {
                if (MblnAddStatus)//insert
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 1, out MsMessageBoxIcon);
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = 0;
                }
                else
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 3, out MsMessageBoxIcon);
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = MintEmployeeID;
                }
               
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID == 0 && TxtEmployeeNumber.ReadOnly && MObjClsBLLEmployeeInformation.CheckDuplication(MblnAddStatus, new string[] { TxtEmployeeNumber.Text.Replace("'", "").Trim() }, MintEmployeeID, cboWorkingAt.SelectedValue.ToInt32()))
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 9002, out MsMessageBoxIcon);
                    if (ClsCommonSettings.IsArabicView)
                        MsMessageCommon = MsMessageCommon.Replace("* no","عدد الموظفين");
                    else
                        MsMessageCommon = MsMessageCommon.Replace("* no", "Employee No");
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return false;
                    else
                        GenerateEmployeeCode();
                }
                // Setting values to the business object
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = MintEmployeeID;
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofBirth = dtpDateofBirth.Value.ToString("dd MMM yyyy").ToDateTime();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofJoining = DtpDateofJoining.Value.ToString("dd MMM yyyy").ToDateTime();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtProbationEndDate = DtpProbationEndDate.Value.ToString("dd MMM yyyy").ToDateTime();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.blnGender = rdbMale.Checked;
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDepartmentID = Convert.ToInt32(cboDepartment.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDivisionID = Convert.ToInt32(cboDivision.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDesignationID = Convert.ToInt32(cboDesignation.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strOfficialEmail = TxtOfficialEmail.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeNumber = TxtEmployeeNumber.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strFathersName = txtFathersName.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strFirstName = TxtFirstName.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strIdentificationMarks = txtIdentificationMarks.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLastName = TxtLastName.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMiddleName = TxtMiddleName.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMothersName = txtMothersName.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intNationalityID = Convert.ToInt32(cboNationality.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intReligionID = Convert.ToInt32(cboReligion.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strSpouseName = Convert.ToString(txtSpouseName.Text.Trim());
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeFullName = Convert.ToString(txtEmployeeFullName.Text.Trim());
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intWorkStatusID = Convert.ToInt32(cboWorkStatus.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID = Convert.ToInt32(cboWorkingAt.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intGradeID = Convert.ToInt32(cboGrade.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.NoticePeriod = txtNoticePeriod.Text.ToInt32();    
                if (cboMaritalStatus.SelectedIndex != -1)
                {
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intMaritalStatusID = Convert.ToInt32(cboMaritalStatus.SelectedValue);
                }
                else
                {
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intMaritalStatusID = 0;
                }

                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmploymentType = Convert.ToInt32(cboEmploymentType.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeFullName = Convert.ToString(txtEmployeeFullName.Text.Trim());
                //MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLanguagesKnown = label32.Text.Trim().ToString();    
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMothersName = txtMothersName.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strSpouseName = txtSpouseName.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intMothertongueID = Convert.ToInt32(cboMothertongue.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strNotes = txtNotes.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPermanentPhone = txtPhone.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalMobile = txtLocalMobile.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAddressLine1 = txtAddress1.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAddressLine2 = txtAddress2.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmail = txtEmail.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strNextAppraisalDate =dtpNextAppraisalDate.Value.ToString("dd-MMM-yyyy");    
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strCity = txtCity.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strStateProvinceRegion = txtState.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strZipCode = txtZipCode.Text.Trim();



                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strBloodGroup = cboBloodGroup.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intSalutation = Convert.ToInt32(CboSalutation.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intPolicyID = Convert.ToInt32(cboWorkPolicy.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intLeavePolicyID = Convert.ToInt32(cboLeavePolicy.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intWorkLocationID = (cboWorkLocation.SelectedValue).ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strDeviceUserID = txtDeviceUserID.Text.Trim();


                //Transaction type

                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intTransactionTypeID = Convert.ToInt32(TransactionTypeReferenceComboBox.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intBankNameID = Convert.ToInt32(BankNameReferenceComboBox.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intBankBranchID = Convert.ToInt32(CboEmployeeBankname.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAccountNumber = AccountNumberTextBox.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intComBankAccId = Convert.ToInt32(ComBankAccIdComboBox.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intVacationPolicyID = cboVacationPolicy.SelectedValue.ToInt32();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.ReportingTo = cboReportingTo.SelectedValue.ToInt32();

                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intVisaObtained  = CboVisaObtained.SelectedValue.ToInt32();

                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strFirstNameArb=TxtFirstNameArb.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMiddleNameArb = TxtMiddleNameArb.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLastNameArb = TxtLastNameArb.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeFullNameArb = txtEmployeeFullNameArb.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCountryID = Convert.ToInt32(cboCountry.SelectedValue);
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmergencyAddress = txtEmergencyAddress.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmergencyPhone = txtEmergencyPhone.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalAddress = txtLocalAddress.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalPhone = txtLocalPhone.Text.Trim();
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intHODID = cboHOD.SelectedValue.ToInt32();   

                if (txtPersonID.Text != string.Empty)
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPersonID = txtPersonID.Text.Trim();


                if (RecentPhotoPictureBox.Image != null)
                {
                    ClsImages GetByteImage = new ClsImages();
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath = GetByteImage.GetImageDataStream(RecentPhotoPictureBox.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else
                {
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath = null;
                }

                if (RecentPhotoPictureBox.Image != null)
                {
                    ResizeEmpImageThumb(RecentPhotoPictureBoxThamNail.Image); 
                    ClsImages GetByteImage = new ClsImages();
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPathThamNail = GetByteImage.GetImageDataStream(RecentPhotoPictureBoxThamNail.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else
                {
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPathThamNail = null;
                }

                int CommissionStructureID = cboCommissionStructure.SelectedValue.ToInt32();

                if (CommissionStructureID > 0)
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.CommissionStructureID = CommissionStructureID;

                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.ProcessTypeID = cboProcessType.SelectedValue.ToInt32();

                FillParameterLanguage();
                if (MObjClsBLLEmployeeInformation.SaveEmployeeInformation(MblnAddStatus))
                {
                    MintEmployeeID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                    if(DtpProbationEndDate.Enabled)
                        new clsAlerts().AlertMessage((int)DocumentType.Probation, "Probation", "probation end date", MintEmployeeID.ToInt32(), TxtEmployeeNumber.Text, DtpProbationEndDate.Value, "Employee",MintEmployeeID.ToInt32(),txtEmployeeFullName.Text.Trim(),false,0);
                    


                    if (MintEmployeeID > 0 && MblnAddStatus)
                    {
                        MObjClsBLLEmployeeInformation.SaveLeavePolicySummary();
                    }
                    if ((DtpDateofJoining.Value != dDOJ) && !(MblnAddStatus))
                    {
                        if (MObjClsBLLEmployeeInformation.DeleteLeavePolicy())
                        {
                            MObjClsBLLEmployeeInformation.SaveLeavePolicySummary();
                        }
                    }
                    if (iLeavePolicyID != Convert.ToInt32(cboLeavePolicy.SelectedValue))
                    {
                        // ------Calculate Carryforward leave
                        MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = MintEmployeeID;
                        DataTable dt = MObjClsBLLEmployeeInformation.CheckCarryForwardLeaveExists();
                        if (dt.Rows.Count >= 1)
                        {
                            MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID = Convert.ToInt32(cboWorkingAt.SelectedValue);
                            MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID = MintEmployeeID;

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                MObjClsBLLEmployeeInformation.UpdateLeaveSummary(Convert.ToInt32(dt.Rows[i]["leaveTypeID"]), Convert.ToInt32(dt.Rows[i]["LeavePolicyID"]));
                            }
                        }

                        // ------Update leave summary

                        MObjClsBLLEmployeeInformation.SaveLeavePolicySummary();
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Save:SaveEmployeeDetails() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Saving SaveEmployeeDetails()" + Ex.Message.ToString());
                return false;
            }
        }
        /// <summary>
        /// Displaying item details of the selected row number
        /// </summary>
        /// 


        private void FillParameterLanguage()
        {
            if (dgvLanguages.RowCount > 0)
            {
                MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.DTOLanuages = new List<clsDTOLanuages>();
                for (int intICounter = 0; intICounter < dgvLanguages.Rows.Count - 1; intICounter++)
                {
                    clsDTOLanuages objLDTOLanuages = new clsDTOLanuages();
                    objLDTOLanuages.LanguageID = dgvLanguages.Rows[intICounter].Cells["ColLanguages"].Value.ToInt32();
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.DTOLanuages.Add(objLDTOLanuages);
                }
            }
        }


        private void DisplayEmployeeInformation()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
           
            if (MObjClsBLLEmployeeInformation.DisplayEmployeeInformation(MintCurrentRecCnt,cboCompany.SelectedValue.ToInt32()))
            {

                ClearControls();
                SetEmployeeInformation();
                iLeavePolicyID = cboLeavePolicy.SelectedValue.ToInt32();
            }
        }

        /// <summary>
        /// Getting Employee information from the object
        /// </summary>
        private void SetEmployeeInformation()
        {
            try
            {
                MblnAddStatus = false;

                btnConfirm.Visible = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intWorkStatusID == 7;
               
                TxtEmployeeNumber.Tag = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                MintEmployeeID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                dtpDateofBirth.Value = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofBirth;
                DtpDateofJoining.Value = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtDateofJoining;
                DtpProbationEndDate.Value = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.dtProbationEndDate;
                rdbMale.Checked = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.blnGender;
                rdbFemale.Checked = !rdbMale.Checked;
                TxtEmployeeNumber.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeNumber;
                cboDepartment.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDepartmentID;
                cboDivision.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDivisionID;
                cboGrade.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intGradeID;
          
                TxtOfficialEmail.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strOfficialEmail;
                txtEmployeeFullName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeNumber;
                txtFathersName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strFathersName;
                TxtFirstName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strFirstName;
                txtIdentificationMarks.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strIdentificationMarks;
                TxtLastName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLastName;
                TxtMiddleName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMiddleName;
                CboSalutation.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intSalutation;
                txtMothersName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMothersName;
                cboNationality.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intNationalityID;
                cboReligion.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intReligionID;
                txtSpouseName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strSpouseName;
                txtEmployeeFullName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeFullName;
                txtPersonID.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPersonID;
                txtDeviceUserID.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strDeviceUserID;

                txtNoticePeriod.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.NoticePeriod.ToStringCustom(); 

                cboWorkingAt.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID;
                cboWorkingAt.Enabled = false;
                cboDesignation.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intDesignationID;
   
                cboEmploymentType.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmploymentType;
                txtEmployeeFullName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeFullName;
                if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strTransferDate != "")
                {
                    lblTransferDate.Visible = lblTransfer.Visible = true;
                    lblTransferDate.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strTransferDate;
                }
                else
                {
                    lblTransferDate.Visible = lblTransfer.Visible = false;
                    lblTransferDate.Text = string.Empty;
                }
                if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRejoinDate != "")
                {
                    lblRejoinDateDesc.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRejoinDate;
                    lblRejoinDateDesc.Visible =  lblRejoinDate.Visible= true;
                }
                else
                {
                    lblRejoinDateDesc.Visible = lblRejoinDate.Visible = false;
                    lblRejoinDateDesc.Text = string.Empty;
                }
                //txtLanguagesKnown.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLanguagesKnown;
                txtMothersName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMothersName;
                txtSpouseName.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strSpouseName;
                cboMothertongue.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intMothertongueID;
                txtNotes.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strNotes;
                txtPhone.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strPermanentPhone;
                txtLocalMobile.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalMobile;
                txtAddress1.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAddressLine1;
                txtAddress2.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAddressLine2;
                txtCity.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strCity;
                txtState.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strStateProvinceRegion;
                txtZipCode.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strZipCode;
                txtEmail.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmail;
                cboBloodGroup.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strBloodGroup;
                dtpNextAppraisalDate.Value = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strNextAppraisalDate.ToDateTime();    

                //FillPolicyCombo(Convert.ToInt32(cboWorkingAt.SelectedValue));

                cboWorkPolicy.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intPolicyID;
                cboLeavePolicy.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intLeavePolicyID;

                FillWorkLocation();
                cboWorkLocation.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intWorkLocationID;
                CboVisaObtained.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intVisaObtained;

                if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath != null)
                {
                    ClsImages objimg = new ClsImages();
                    RecentPhotoPictureBox.Image = objimg.GetImage(MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath);
                }
                else if (File.Exists(Application.StartupPath + "\\Images\\sample.png"))
                {
                    RecentPhotoPictureBox.Image = Image.FromFile(Application.StartupPath + "\\Images\\sample.png");
                }


                if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath != null)
                {
                    ClsImages objimg = new ClsImages();
                    RecentPhotoPictureBoxThamNail.Image = objimg.GetImage(MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath);
                }
                else if (File.Exists(Application.StartupPath + "\\Images\\sample.png"))
                {
                    RecentPhotoPictureBoxThamNail.Image = Image.FromFile(Application.StartupPath + "\\Images\\sample.png");
                }

                TransactionTypeReferenceComboBox.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intTransactionTypeID;
                SetAccountBankDetails();

                BankNameReferenceComboBox.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intBankNameID;
                CboEmployeeBankname.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intBankBranchID;
                AccountNumberTextBox.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strAccountNumber;
                ComBankAccIdComboBox.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intComBankAccId;
                cboWorkStatus.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intWorkStatusID;
                if (MObjClsBLLEmployeeInformation.CheckIfSettlementExists(MintEmployeeID) && !MblnAddStatus)
                {
                    BtnWorkstatus.Enabled = cboWorkStatus.Enabled = false;

                }
                else
                {
                    BtnWorkstatus.Enabled = cboWorkStatus.Enabled = true;

                }
                dDOJ = DtpDateofJoining.Value;
                // Enable buttons
                EnableDisableButtons(false);
                //if (!MblnAddStatus && MObjClsBLLEmployeeInformation.FillCombos(new string[] { "AttendanceID", "PayAttendanceMaster", "EmployeeID = " + MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID }).Rows.Count > 0)
                //{
                //     btnWorkLocation.Enabled =cboWorkLocation.Enabled = false;
                //}
                //else
                //{
                //    btnWorkLocation.Enabled =cboWorkLocation.Enabled = true;
                //}
                if (!MblnAddStatus && MObjClsBLLEmployeeInformation.FillCombos(new string[] { "PaymentID", "PayEmployeePayment", "EmployeeID = " + MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID }).Rows.Count > 0)
                {
                    DtpDateofJoining.Enabled = false;
                }
                else if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strTransferDate != "")
                {
                    DtpDateofJoining.Enabled = false;
                }
                else if (!MblnAddStatus && MObjClsBLLEmployeeInformation.FillCombos(new string[] { "VacationID", "PayVacationMaster", "EmployeeID = " + MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID }).Rows.Count > 0)
                {
                    DtpDateofJoining.Enabled = false;
                }
                else
                {
                    DtpDateofJoining.Enabled = true;
                }
                TxtEmployeeNumber.Focus();
                cboVacationPolicy.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intVacationPolicyID;

                LoadReportingto();
                cboReportingTo.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.ReportingTo;

                LoadHOD(); 
                cboHOD.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intHODID;
                DtpProbationEndDate.Enabled = cboWorkStatus.SelectedValue.ToInt32() == 7;
                cboMaritalStatus.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intMaritalStatusID;

                TxtFirstNameArb.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strFirstNameArb ;
                TxtMiddleNameArb.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strMiddleNameArb;
                TxtLastNameArb.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLastNameArb;
                txtEmployeeFullNameArb.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmployeeFullNameArb;
                cboCountry.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCountryID;
                txtEmergencyAddress.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmergencyAddress;
                txtEmergencyPhone.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strEmergencyPhone;
                txtLocalAddress.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalAddress;
                txtLocalPhone.Text = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strLocalPhone;


                DataTable DT;
                DT = MObjClsBLLEmployeeInformation.GetLanguage(MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID.ToInt32());


                dgvLanguages.RowCount = 1;
                if (dgvLanguages != null && DT.Rows.Count>0)
                {
                    for (int iCounter = 0; iCounter <= DT.Rows.Count-1; iCounter++)
                    {
                        dgvLanguages.RowCount = dgvLanguages.RowCount + 1;
                        dgvLanguages.Rows[iCounter].Cells["ColLanguages"].Value = DT.Rows[iCounter]["LanguageID"].ToInt32();

                    }
                }

                cboCommissionStructure.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.CommissionStructureID;
                cboProcessType.SelectedValue = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.ProcessTypeID;
                MblnAddStatus = false;
            }
            catch (Exception ex)
            {
                MObjClsLogWriter.WriteLog("Error on setemployee " + this.Name + " " + ex.Message.ToString(), 2);
            }
        }

        /// <summary>
        /// Getting previous/first Employee Information
        /// </summary>
        /// <param name="bFirstItem"></param>
        private void GetPreviousEmployeeInformation(bool blnFirstItem)
        {
            DisplayRecordCount();
            if (MintRowNumber > 0)
            {
                if (blnFirstItem)
                    MintRowNumber = 1;
                else
                    MintRowNumber--;
                EventArgs e = new EventArgs();
                BnSearchButton_Click(btnSearch, e);
            }
            else
            {
                // Getting previous item in the details

                //MintRecordCnt = MObjClsBLLEmployeeInformation.RecCountNavigate();   // Getting total record count
                DisplayRecordCount();
                if (MintRecordCnt > 0)
                {
                    if (blnFirstItem)
                    {
                        MintCurrentRecCnt = 1;
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 9, out MsMessageBoxIcon);
                    }
                    else
                    {
                        MintCurrentRecCnt = MintCurrentRecCnt - 1;
                        if (MintCurrentRecCnt <= 0)
                        {
                            MintCurrentRecCnt = 1;
                        }
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 10, out MsMessageBoxIcon);
                    }
                }
                else
                {
                    BindingNavigatornPositionItem.Text = strBindingOf + "0";
                }
                DisplayEmployeeInformation();
                BindingNavigatornPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt);
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();
                txtSearch.Clear();
                // CboSearchOption.SelectedIndex = -1;
                MintRowNumber = 0;
                TbEmployee.SelectedTab = TpGeneral;

            }
        }

        private void SetAccountBankDetails()
        {
            {
                //if (TransactionTypeReferenceComboBox.Text == "Bank" )
                //{
                //    //BankNameReferenceComboBox.BackColor = Color.LightYellow;
                //    CboEmployeeBankname.BackColor = Color.LightYellow;
                //    //ComBankAccIdComboBox.BackColor = Color.LightYellow;
                //    AccountNumberTextBox.BackColor = Color.LightYellow;
                //    BankNameReferenceComboBox.Enabled = true;
                //    btnBankName.Enabled = true;
                //    AccountNumberTextBox.Enabled = true;

                //}
                if (TransactionTypeReferenceComboBox.SelectedValue.ToInt32() == 1 /*Bank*/ || TransactionTypeReferenceComboBox.SelectedValue.ToInt32() == 3 /*WPS*/)
                {
                    ComBankAccIdComboBox.DataSource = null;
                    BankNameReferenceComboBox.BackColor = Color.LightYellow;
                    CboEmployeeBankname.BackColor = Color.LightYellow;
                    ComBankAccIdComboBox.BackColor = Color.LightYellow;
                    AccountNumberTextBox.BackColor = Color.LightYellow;
                    BankNameReferenceComboBox.Enabled = true;
                    btnBankName.Enabled = true;
                    AccountNumberTextBox.Enabled = true;
                }
                else
                {
                    BankNameReferenceComboBox.BackColor = Color.White;
                    CboEmployeeBankname.BackColor = Color.White;
                    ComBankAccIdComboBox.BackColor = Color.White;
                    AccountNumberTextBox.BackColor = Color.White;
                    BankNameReferenceComboBox.Enabled = false;
                    btnBankName.Enabled = false;
                    AccountNumberTextBox.Enabled = false;
                    //btnEmployerBankName.Enabled = false;
                    //txtPersonID.BackColor = Color.White;

                }


            }

            DataTable datCombos = new DataTable();

            if (TransactionTypeReferenceComboBox.SelectedIndex.ToInt32() >= 0)
            {
                if (Convert.ToInt32(cboWorkingAt.SelectedValue) != 0)
                {
                    //BankNameReferenceComboBox.Enabled = true;
                    //btnBankName.Enabled = true;
                    //ComBankAccIdComboBox.Enabled = true;
                    //AccountNumberTextBox.Enabled = true;
                    BankNameReferenceComboBox.DataSource = null;
                    CboEmployeeBankname.DataSource = null;

                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "bnr.BankBranchID, BB.BankName + '-' + bnr.BankBranchName +' - ' + C.CompanyName  AS Description", "CompanyMaster AS C INNER JOIN CompanyBankAccountDetails AS abd ON C.CompanyID = abd.CompanyID RIGHT OUTER JOIN BankBranchReference AS bnr  LEFT OUTER JOIN BankReference AS BB ON bnr.BankID = BB.BankID ON abd.BankBranchID = bnr.BankBranchID ", "" });
                    //else

                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "bnr.BankBranchID, isnull(BB.BankName,'') + '-' + isnull(bnr.BankBranchName,'') +' - ' + isnull(C.CompanyName,'')  AS Description", "CompanyMaster AS C INNER JOIN CompanyBankAccountDetails AS abd ON C.CompanyID = abd.CompanyID INNER JOIN BankBranchReference AS bnr  INNER JOIN BankReference AS BB ON bnr.BankID = BB.BankID ON abd.BankBranchID = bnr.BankBranchID", "abd.CompanyID=" + cboWorkingAt.SelectedValue + "" });
                    BankNameReferenceComboBox.ValueMember = "BankBranchID";
                    BankNameReferenceComboBox.DisplayMember = "Description";
                    BankNameReferenceComboBox.DataSource = datCombos;

                    datCombos = new DataTable();

                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "BankBranchReference.BankBranchID,isnull(BankReference.BankNameArb,'')+ ' - ' + isnull(BankBranchReference.BankBranchName,'')   as Description", "BankBranchReference INNER JOIN BankReference ON BankBranchReference.BankID = BankReference.BankID ORDER BY BankBranchReference.BankBranchName", "" });   // CboEmployeeBankname, "SELECT   ,  ", "BankBranchID", "BankBranch");
                    else
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "BankBranchReference.BankBranchID,isnull(BankReference.BankName,'')+ ' - ' + isnull(BankBranchReference.BankBranchName,'')   as Description", "BankBranchReference INNER JOIN BankReference ON BankBranchReference.BankID = BankReference.BankID ORDER BY BankBranchReference.BankBranchName", "" });   // CboEmployeeBankname, "SELECT   ,  ", "BankBranchID", "BankBranch");
                    CboEmployeeBankname.ValueMember = "BankBranchID";
                    CboEmployeeBankname.DisplayMember = "Description";
                    CboEmployeeBankname.DataSource = datCombos;
                }

                ComBankAccIdComboBox.DataSource = null;
                AccountNumberTextBox.Text = "";
                //BankNameReferenceComboBox.Enabled = true;
                //btnBankName.Enabled = true;
                //AccountNumberTextBox.Enabled = true;
            }


            if (BankNameReferenceComboBox.Items.Count > 0)
                BankNameReferenceComboBox.SelectedIndex = -1;

            if (CboEmployeeBankname.Items.Count > 0)
                CboEmployeeBankname.SelectedIndex = -1;

            if (ComBankAccIdComboBox.Items.Count > 0)
                ComBankAccIdComboBox.SelectedIndex = -1;

        }

        /// <summary>
        /// Getting previous/first Employee Information
        /// </summary>
        /// <param name="bFirstItem"></param>
        private void GetNextExecutiveInformation(bool bLastItem)
        {

            if (MintRowNumber > 0)    // Getting next item in the search results
            {
                if (bLastItem)
                {
                    MintRowNumber = MintRecordCnt;
                }
                else if (Convert.ToInt32(BindingNavigatornPositionItem.Text) < MintRecordCnt)
                {
                    MintRowNumber++;
                }
                EventArgs e = new EventArgs();
                // Calling searching event
                BnSearchButton_Click(btnSearch, e);
            }
            else
            {
                DisplayRecordCount();
                MblnAddStatus = false;
                ErrEmployee.Clear();
                DisplayRecordCount();
                if (MintRecordCnt > 0)
                {
                    if (bLastItem)
                    {
                        MintCurrentRecCnt = MintRecordCnt;
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 12, out MsMessageBoxIcon);
                    }
                    else
                    {
                        MintCurrentRecCnt = MintCurrentRecCnt + 1;
                        if (MintCurrentRecCnt >= MintRecordCnt)
                        {
                            MintCurrentRecCnt = MintRecordCnt;
                        }
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 11, out MsMessageBoxIcon);
                    }
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }
                DisplayEmployeeInformation();  // Displaying employee information
                BindingNavigatornPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt);
                LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();   // Setting Binding navigator buttons
                txtSearch.Clear();
                //CboSearchOption.SelectedIndex = -1;
                MintRowNumber = 0;
                TbEmployee.SelectedTab = TpGeneral;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveEmployeeInformation();
            }
            catch(Exception ex)
            {
                MObjClsLogWriter.WriteLog("Error on Save:BtnSave_Click " + this.Name + " " + ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Save:BtnSave_Click " + ex.Message.ToString());

            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            //Validating fields before saving
            try
            {
                if (ValidateEmployeeFields())
                {
                    // Saving new employee details
                    if (SaveEmployeeDetails())
                    {
                        if (MblnAddStatus == true)
                        {
                            MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 444, out MsMessageBoxIcon);
                            if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                            {
                                using (FrmSalaryStructure objSalaryStructure = new FrmSalaryStructure())
                                {
                                    objSalaryStructure.PintCompanyID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID;
                                    objSalaryStructure.PintEmployeeID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                                    objSalaryStructure.ShowDialog();
                                }
                            }
                        }
                        BtnSave.Enabled = false;
                        this.Close();

                    }
                }
            }
            catch (Exception ex)
            {
                MObjClsLogWriter.WriteLog("Error on Save:BtnSave_Click " + this.Name + " " + ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Save:BtnSave_Click " + ex.Message.ToString());

            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            try
            { 
                this.Close();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Cancel:BtnCancel_Click " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Cancel:BtnCancel_Click " + Ex.Message.ToString());

            }
        }

        private void BnHelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "Employee";
                objHelp.ShowDialog();
                objHelp = null;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Help:BnHelpButton_Click " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Help:BnHelpButton_Click" + Ex.Message.ToString());

            }

        }

        private void BnSearchButton_Click(object sender, EventArgs e)
        {
            //if (this.txtSearch.Text.Trim() == string.Empty)
            //    return;
            try
            {


                int TotalRows;
                MintRowNumber = MintRowNumber == 0 ? 1 : MintRowNumber;
                txtSearch.Text = txtSearch.Text.Trim().Replace("'", "");  
                if (MObjClsBLLEmployeeInformation.SearchEmployee(txtSearch.Text.Trim(), cboCompany.SelectedValue.ToInt32(), MintRowNumber, out TotalRows))
                {
                    BindingNavigatornPositionItem.Text = MintRowNumber.ToString();
                    BindingNavigatorCountItem.Text = strBindingOf + TotalRows.ToString();

                    MintRecordCnt = TotalRows;
                    MblnAddStatus = false;

                    SetEmployeeInformation();  // Displaying employee information
                    BindingNavigatornPositionItem.Text = Convert.ToString(MintRowNumber);
                    BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";

                    BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = true;
                    int iCurrentRec = Convert.ToInt32(BindingNavigatornPositionItem.Text); // Current position of the record
                    int iRecordCnt = MintRecordCnt;  // Total count of the records

                    if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
                    {
                        BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = false;

                    }
                    if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
                    {
                        BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = false;

                    }

                    BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                    BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    BtnEmail.Enabled = BtnPrint.Enabled = MblnPrintEmailPermission;
                    BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = BtnClear.Enabled = false;
                }
                else
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.NoResultMessage, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmEmployee.Enabled = true;
                    txtSearch.Text = string.Empty;
                    MintRowNumber = 0;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Search:BnSearchButton_Click " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Search:BnSearchButton_Click  " + Ex.Message.ToString());

            }
        }

        private void TmExecutive_Tick(object sender, EventArgs e)
        {
            try
            {
                TmEmployee.Enabled = false;
                LblEmployeeStatus.Text = "";
            }
            catch (Exception EX)
            {
                MObjClsLogWriter.WriteLog("Error on TmExecutive:TmExecutive_Tick " + this.Name + " " + EX.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on TmExecutive_Tick:TmExecutive_Tick  " + EX.Message.ToString());

            }
        }

        private void ClearErrorProvider(object sender, EventArgs e)
        {
            try
            {
                ErrEmployee.Clear();
                EnableButtons(sender, e);
            }
            catch (Exception EX)
            {
                MObjClsLogWriter.WriteLog("Error on ClearErrorProvider:ClearErrorProvider_Click " + this.Name + " " + EX.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmEmployee_Load() " + EX.Message.ToString());

            }
        }

        private void EnableButtons(object sender, EventArgs e)
        {
            BtnSave.Enabled = MblnUpdatePermission;
            BtnOk.Enabled = MblnUpdatePermission;
            BtnClear.Enabled = MblnUpdatePermission;
            BindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            MblnChangeStatus = true;
        }

        private void ClearSearchCount(object sender, EventArgs e)
        {

            if (txtSearch.Text != string.Empty) MintRowNumber = 1;
        }

        private enum ErrorCode
        {
            EmpNoValidation = 401, // Please enter Employee Number
            SalutationValidation = 402, // Please select Salutation
            FirstNameValidation = 403, // Please enter First Name
            WorkingAtValidation = 404, // Please select Working At
            DepartmentValidation = 405, // Please select Department
            DivisionValidation = 9130, // Please select Division
            DesignationValidation = 406, //	Please select Designation
            MaritalStatusValidation = 407, // Please select Marital Status
            NationalityValidation = 408, // Please select Nationality
            ReligionValidation = 409, // Please select Religion
            DateOfJoinValidation = 410, // Please select valid Date of Joining
            EmailValidation = 411, // Please enter valid Email
            DateOfBirthValidation = 412, // Please select valid Date of Birth
            DuplicateEmpNo = 413, // Duplicate Employee Number not permitted
            SearchOptionValidation = 414, // Please select a Search Option
            SearchTextValidation = 415, // Please enter Search Text
            NoResultMessage = 416, // No result for the search
            SaveConfirm = 417,
            UpdateConfirm = 418,
            AddNew = 419,
            CouldNotDelete = 420,
            SaveMessage = 421, // Executive Information saved successfully
            UpdateMessage = 422, // Executive Information updated successfully
            DeleteConfirm = 423, // Are you sure you want to delete the selected Executive Information ?
            DeleteMessage = 424, // Executive Information deleted successfully
            FormClosing = 425, // Your changes will be lost. Please confirm if you wish to cancel ?  
            WorkstatusValidation = 426,// Please select Workstaus
            CountryoforginValidation = 427, // Please select Countryoforgin
            ReviewDateValidation = 430, //Reviewdate should be greater than joining date
            ProbationEndDatevalidation = 429, //ProbationDate should be greater than joining date
            WorkPolicy = 438,
            LeavePolicy = 439,
            WorkLocation = 500,
            SettlementWorkStayus=456



        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                SaveEmployeeInformation();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on BindingNavigatorSaveItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorSaveItem_Click  " + Ex.Message.ToString());

            }
        }

        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            MblnIsEditMode = true;
            try
            {
                GetPreviousEmployeeInformation(false);

            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MovePreviousItem:MovePreviousItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MovePreviousItem_Click " + Ex.Message.ToString());
            }

        }

        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            MblnIsEditMode = true;
            try
            {
                GetPreviousEmployeeInformation(true);
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveFirstItem:MoveFirstItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveFirstItem_Click " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            MblnIsEditMode = true;
            try
            {
                GetNextExecutiveInformation(false);
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveNextItem:MoveNextItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveNextItem_Click " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            MblnIsEditMode = true;
            try
            {
                GetNextExecutiveInformation(true);
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveLastItem:MoveLastItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveLastItem_Click " + Ex.Message.ToString());
            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            try
            {
                AddNewItem();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on AddNew:BindingNavigatorAddNewItem_Click " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingNavigatorAddNewItem_Click " + Ex.Message.ToString());
            }

        }

        private bool DeleteValidation(long intEmployee)
        {
            bool blnretvalue = true;
            DataTable dtIds = MObjClsBLLEmployeeInformation.CheckExistReferences();
            if (dtIds.Rows.Count > 0)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 420, out MsMessageBoxIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                TmEmployee.Enabled = true;
                blnretvalue = false;

            }
            //bool  blnScheduleMaster= MObjClsBLLEmployeeInformation.DisplayExistence(intEmployee);
            //if (blnretvalue == true && blnScheduleMaster==false)
            // {
            //     MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 420, out MsMessageBoxIcon);
            //     MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
            //     TmEmployee.Enabled = true;
            //     blnretvalue = false;
            // }

            return blnretvalue;
        }

        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                long intEmployee = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                //if (DeleteValidation(intEmployee))
                //{
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 13, out MsMessageBoxIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;

                    
                    if (MObjClsBLLEmployeeInformation.DeleteEmployeeInformation())
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 4, out MsMessageBoxIcon);
                        LblEmployeeStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        TmEmployee.Enabled = true;
                        AddNewItem();
                        SetAutoCompleteList();
                    }

               // }
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 420, out MsMessageBoxIcon);
                else
                    MsMessageCommon = "Error on DeleteItem_Click() " + Ex.Message.ToString();

                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                TmEmployee.Enabled = true;

            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DeleteItem:DeleteItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteItem_Click() " + Ex.Message.ToString());
            }
        }

        private void FrmEmployee_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID > 0)
                    PintEmployeeID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                if (BtnSave.Enabled)
                {
                    // Checking the changes are not saved and shows warning to the user
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, (object)ErrorCode.FormClosing, out MsMessageBoxIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim().Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        MObjClsBLLEmployeeInformation = null;
                        MObjClsLogWriter = null;
                        MObjClsNotification = null;
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on FrmEmployee_FormClosing:FrmEmployee_FormClosing " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmEmployee_FormClosing " + Ex.Message.ToString());
            }

        }


        private void BtnWorkstatus_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("WorkStatus", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "WorkStatusID,IsPredifined,WorkStatus,WorkStatusArb", "WorkStatusReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkStatusID = Convert.ToInt32(cboWorkStatus.SelectedValue);
                LoadCombos(10);
                if (objCommon.NewID != 0)
                    cboWorkStatus.SelectedValue = objCommon.NewID;
                else
                    cboWorkStatus.SelectedValue = intWorkStatusID;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnWorkstatus" + Ex.Message.ToString());
            }
        }

        private void BtnWorkingat_Click(object sender, EventArgs e)
        {
            try
            {
                int intWorkingAt = Convert.ToInt32(cboWorkingAt.SelectedValue);
                using (FrmCompany objCompany = new FrmCompany())
                {
                    objCompany.PintCompany = Convert.ToInt32(cboWorkingAt.SelectedValue);
                    objCompany.ShowDialog();
                    LoadCombos(7);
                    if (objCompany.PintCompany != 0)
                        cboWorkingAt.SelectedValue = objCompany.PintCompany;
                    else
                        cboWorkingAt.SelectedValue = intWorkingAt;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnWorkingat" + Ex.Message.ToString());
            }
        }

        private void BtnSalutation_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Salutation", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "SalutationID, Salutation, SalutationArb", "SalutationReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                int intWorkingAt = Convert.ToInt32(CboSalutation.SelectedValue);
                LoadCombos(6);
                if (objCommon.NewID != 0)
                    CboSalutation.SelectedValue = objCommon.NewID;
                else
                    CboSalutation.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnSalutation" + Ex.Message.ToString());
            }
        }

        private void BtnTransactiontype_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("TransactionType", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "TransactionTypeID, TransactionType As [Transaction Type]", "TransactionTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                int intWorkingAt = Convert.ToInt32(TransactionTypeReferenceComboBox.SelectedValue);
                LoadCombos(11);
                if (objCommon.NewID != 0)
                    TransactionTypeReferenceComboBox.SelectedValue = objCommon.NewID;
                else
                    TransactionTypeReferenceComboBox.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnTransactiontype" + Ex.Message.ToString());
            }
        }




        private void BtnEmptype_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Employment Type", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "EmploymentTypeID,IsPredefined, EmploymentType,EmploymentTypeArb", "EmploymentTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                int intWorkingAt = Convert.ToInt32(cboEmploymentType.SelectedValue);
                LoadCombos(16);
                if (objCommon.NewID != 0)
                    cboEmploymentType.SelectedValue = objCommon.NewID;
                else
                    cboEmploymentType.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmptype" + Ex.Message.ToString());
            }
        }


        private void BtnNationality_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Nationality", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "NationalityID,IsPredefined, Nationality,NationalityArb", "NationalityReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboNationality.SelectedValue);
                LoadCombos(5);
                if (objCommon.NewID != 0)
                    cboNationality.SelectedValue = objCommon.NewID;
                else
                    cboNationality.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  BtnNationality " + Ex.Message.ToString());
            }
        }

        private void BtnDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                int DepartmentID = Convert.ToInt32(cboDepartment.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Department", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DepartmentID,IsPredefined,Department,DepartmentArb", "DepartmentReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboDepartment.SelectedValue);
                LoadCombos(1);
                if (objCommon.NewID != 0)
                    cboDepartment.SelectedValue = objCommon.NewID;
                else
                    cboDepartment.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnDepartment " + Ex.Message.ToString());
            }
        }

        private void BtnDesignation_Click(object sender, EventArgs e)
        {
            try
            {
                int DesignationID = Convert.ToInt32(cboDesignation.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Designation",
                    new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DesignationID,IsPredefined, Designation,DesignationArb", "DesignationReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboDesignation.SelectedValue);
                LoadCombos(2);
                if (objCommon.NewID != 0)
                    cboDesignation.SelectedValue = objCommon.NewID;
                else
                    cboDesignation.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnDesignation " + Ex.Message.ToString());
            }
        }

        private void BtnReligion_Click(object sender, EventArgs e)
        {
            try
            {
                int intReligion = Convert.ToInt32(cboReligion.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Religion",
                    new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "ReligionID,IsPredefined,Religion,ReligionArb", "ReligionReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboReligion.SelectedValue);
                LoadCombos(4);
                if (objCommon.NewID != 0)
                    cboReligion.SelectedValue = objCommon.NewID;
                else
                    cboReligion.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnReligion " + Ex.Message.ToString());
            }
        }

        private void Btnmothertongue_Click(object sender, EventArgs e)
        {
            try
            {

                int intMothertongue = Convert.ToInt32(cboMothertongue.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Mothertongue",
                    new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "MotherTongueID,IsPredefined,MotherTongue,MotherTongueArb", "MotherTongueReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                 intMothertongue = Convert.ToInt32(cboMothertongue.SelectedValue);
                LoadCombos(9);
                if (objCommon.NewID != 0)
                    cboMothertongue.SelectedValue = objCommon.NewID;
                else
                    cboMothertongue.SelectedValue = intMothertongue;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Btnmothertongue " + Ex.Message.ToString());
            }
        }


        private void BtnClear_Click(object sender, EventArgs e)
        {
            try
            {
                //SetEnable();
                AddNewItem();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form BtnClear_Click:BtnClear_Click " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnClear_Click " + Ex.Message.ToString());
            }
        }

        /// <summary>
        /// Loading report viewer with employee information
        /// </summary>
        private void LoadReport()
        {
            if (MintEmployeeID > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PintCompany = cboWorkingAt.SelectedValue.ToInt32();
                ObjViewer.PiRecId = MintEmployeeID;
                ObjViewer.PiFormID = (int)FormID.Employee;
                ObjViewer.ShowDialog();
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmEmployee: LoadReport() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on   LoadReport()" + Ex.Message.ToString());
            }
        }

        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Employee Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.Employee;
                    ObjEmailPopUp.EmailSource = MObjClsBLLEmployeeInformation.GetEmployeeReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmEmployee: BtnEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  BtnEmail_Click  " + Ex.Message.ToString());
            }
        }

        private void ClearPicture()
        {
            try
            {


                if (!File.Exists(Application.StartupPath + "\\Images\\sample.png"))
                {
                }
                else
                {
                    //RemoveRecPicture = "Sample.png";
                    RecentPhotoPictureBox.Image = Image.FromFile(Application.StartupPath + "\\Images\\sample.png");
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath = null;
                }

                ChangeStatus();

            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmEmployee:ClearPicture() " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  ClearPicture()  " + Ex.Message.ToString());
            }
        }
        private void ScanDocs()
        {
            try
            {
                if (BtnAttach.Text == ToolStripMenuAttach.Text)
                {

                    //RemoveRecPicture = "";
                    OpenFileDialog Dlg = new OpenFileDialog();
                    Dlg.Filter = "EmployeeImages (*.jpg;*.jpeg;*.tif;*.png;*.bmp)|*.jpg;*.tif;*.jpeg;*.png;*.bmp";
                    Dlg.ShowDialog();
                    RecentFilePath = Dlg.FileName;
                    if (File.Exists(RecentFilePath) == false)
                    {
                        return;
                    }

                    else
                    {
                        string strExtention = "";
                        FileInfo fInfo = new FileInfo(RecentFilePath);

                        strExtention = fInfo.Extension.ToUpper();

                        if (strExtention == ".jpg".ToUpper() || strExtention == ".jpeg".ToUpper() || strExtention == ".tif".ToUpper() ||
                            strExtention == ".png".ToUpper() || strExtention == ".bmp".ToUpper())
                        {
                            imgImageFile = Image.FromFile(RecentFilePath.ToString());
                        }
                        else
                            return;







                        imgImageFile = Image.FromFile(RecentFilePath.ToString());
                        //imgImageFileThamnail = Image.FromFile(RecentFilePath.ToString());
                    }
                    RecentPhotoPictureBox.Image = imgImageFile;
                    RecentPhotoPictureBoxThamNail.Image = imgImageFile;
                    //string[] sarFileName = Dlg.SafeFileName.Split(Convert.ToChar("."));
                    //string strImageFileName = sarFileName[0] + "_" + ClsCommonSettings.GetServerDate().Date.Year +
                    //    ClsCommonSettings.GetServerDate().Date.Month + ClsCommonSettings.GetServerDate().Date.Day + "_" + ClsCommonSettings.GetServerDate().Hour + ClsCommonSettings.GetServerDate().Minute + ClsCommonSettings.GetServerDate().Second +
                    //    "." + sarFileName[1];
                    //if (!Directory.Exists(ClsCommonSettings.strServerPath + @"\EmployeeImages"))
                    //    Directory.CreateDirectory(ClsCommonSettings.strServerPath + @"\EmployeeImages");
                    //File.Copy(Dlg.FileName, ClsCommonSettings.strServerPath + @"\EmployeeImages\" + strImageFileName, true);
                    //MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.strRecentPhotoPath = strImageFileName;
                    ResizeEmpImage(RecentPhotoPictureBox.Image);


                }
                else
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 437, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmEmployee: ScanDocs()" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  ScanDocs() " + Ex.Message.ToString());
            }
        }

        private void BtnAttach_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(BtnAttach.Text.Trim()) == "Remove")
            {
                ClearPicture();
            }
            else
            {
                ScanDocs();
            }
            ChangeStatus();
        }

        /// </summary>
        //private Image ResizeEmpImage(string strFileName)
        //{
        //    Bitmap btmImage = new Bitmap(ClsCommonSettings.strServerPath + @"\EmployeeImages\" + strFileName);
        //    //(pbItemPhoto.Image);

        //    int intWidth = 293; // image width
        //    int intHeight = 192; // image height

        //    Bitmap btmThumb = new Bitmap(intWidth, intHeight);
        //    Graphics grhImage = Graphics.FromImage(btmThumb);

        //    grhImage.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        //    grhImage.DrawImage(btmImage, new Rectangle(0, 0, intWidth, intHeight), new Rectangle(0, 0, btmImage.Width, btmImage.Height),
        //        GraphicsUnit.Pixel);
        //    if (Convert.ToString(TbPhoto.SelectedTab.Text) == "Recent")
        //    {
        //        RecentPhotoPictureBox.Image = btmThumb;
        //    }
        //    else
        //    {
        //        PassportPhotoPictureBox.Image = btmThumb;
        //    }
        //    grhImage.Dispose();
        //    btmImage.Dispose();
        //    return btmThumb;
        //}
        private void ResizeEmpImage(Image Img)
        {
            //function for resize emp image
            Bitmap bm = new Bitmap(Img);
            int width = Img.Width;
            int height = Img.Height;
            Bitmap thumb = new Bitmap(width, height);
            Graphics g;
            g = Graphics.FromImage(thumb);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(bm, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel);
            RecentPhotoPictureBox.Image = thumb;
            g.Dispose();
            bm.Dispose();
        }

        private void ResizeEmpImageThumb(Image Img)
        {
            //function for resize emp image
            Bitmap bm = new Bitmap(Img);
            int width = 50;
            int height = 60;
            Bitmap thumb = new Bitmap(width, height);
            Graphics g;
            g = Graphics.FromImage(thumb);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High; 
            g.DrawImage(bm, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel);
            RecentPhotoPictureBoxThamNail.Image = thumb;
            g.Dispose();
            bm.Dispose();
        }


        private void ChangeVisiblityPhoto(int intfrmbtn)
        {
            switch (intfrmbtn)
            {
                case 1:
                    BtnAttach.Text = ToolStripMenuAttach.Text;
                    break;
                case 2:
                    BtnAttach.Text = ToolStripMenuScan.Text;
                    break;
                case 3:
                    BtnAttach.Text = ToolStripMenuRemove.Text;
                    break;
            }
        }
        private void ChangeStatus()
        {


            if (!MblnIsEditMode)
            {
                BindingNavigatorSaveItem.Enabled = BtnClear.Enabled = BtnOk.Enabled = BtnSave.Enabled = MblnAddPermission;

            }
            else
            {
                BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
                BindingNavigatorSaveItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = MblnUpdatePermission;
                BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                BtnClear.Enabled = false;
            }
            ErrEmployee.Clear();
            LblEmployeeStatus.Text = string.Empty;


        }
        private void ToolStripMenuAttach_Click(object sender, EventArgs e)
        {
            ChangeStatus();
            ChangeVisiblityPhoto(1);
            ScanDocs();
        }
        private void ToolStripMenuScan_Click(object sender, EventArgs e)
        {
            ChangeStatus();
            ChangeVisiblityPhoto(2);
            ScanDocs();
        }
        private void ToolStripMenuRemove_Click(object sender, EventArgs e)
        {
            ChangeStatus();
            ChangeVisiblityPhoto(3);
            ClearPicture();
        }
        private void BtnContxtMenu1_Click(object sender, EventArgs e)
        {
            ContextMenuStrip2.Show(BtnContxtMenu1, BtnContxtMenu1.PointToClient(System.Windows.Forms.Cursor.Position));
        }
        private void TxtFirstName_TextChanged(object sender, EventArgs e)
        {
            GetFullName();
            ChangeStatus();
        }
        private void TxtMiddleName_TextChanged(object sender, EventArgs e)
        {
            GetFullName();
            ChangeStatus();
        }
        private void TxtLastName_TextChanged(object sender, EventArgs e)
        {
            GetFullName();
            ChangeStatus();
        }



        private void btnWorkPolicy_Click(object sender, EventArgs e)
        {
            try
            {
                int intWorkpolicyID = Convert.ToInt32(cboWorkPolicy.SelectedValue);
                using (FrmWorkPolicy objwPolicy = new FrmWorkPolicy())
                {
                    objwPolicy.PintCompanyId = Convert.ToInt32(cboWorkingAt.SelectedValue);
                    MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intPolicyID = intWorkpolicyID;
                    objwPolicy.PintWorkPolicyID = MObjClsBLLEmployeeInformation.GetCurRowNumber();
                    objwPolicy.ShowDialog();
                    LoadCombos(18);
                    //if (objwPolicy.PintWorkPolicyID != 0)
                    //    cboWorkPolicy.SelectedValue = objwPolicy.PintWorkPolicyID;
                    //else
                    cboWorkPolicy.SelectedValue = intWorkpolicyID;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmWorkpolicy" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnWorkPolicy" + Ex.Message.ToString());
            }
        }

        private void btnLeavePolicy_Click(object sender, EventArgs e)
        {
            try
            {
                int intLeavepolicyID = Convert.ToInt32(cboLeavePolicy.SelectedValue);
                using (FrmLeavePolicy objwPolicy = new FrmLeavePolicy())
                {
                    objwPolicy.PintCompanyID = Convert.ToInt32(cboWorkingAt.SelectedValue);
                    objwPolicy.PintLeavePolicyID = Convert.ToInt32(cboLeavePolicy.SelectedValue);
                    objwPolicy.ShowDialog();
                    LoadCombos(19);

                    if (objwPolicy.PintLeavePolicyID != 0)
                        cboLeavePolicy.SelectedValue = objwPolicy.PintLeavePolicyID;
                    else
                        cboLeavePolicy.SelectedValue = intLeavepolicyID;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmLeavePolicy" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnLeavePolicy" + Ex.Message.ToString());
            }
        }

        private void cboWorkPolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboLeavePolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboWorkPolicy_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void cboLeavePolicy_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void TransactionTypeReferenceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SetAccountBankDetails();
                ErrEmployee.SetError(TransactionTypeReferenceComboBox, "");
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form TransactionTypeReferenceComboBox_SelectedIndexChanged" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on TransactionTypeReferenceComboBox_SelectedIndexChanged" + Ex.Message.ToString());
            }
        }

        private void cboWorkingAt_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ComBankAccIdComboBox.DataSource = null;
                ErrEmployee.SetError(cboWorkingAt, "");


                if (cboWorkingAt.SelectedIndex != -1)
                {
                    cboDesignation.Text = "";
                    LoadCombos(2);
                    LoadReportingto();
                    LoadHOD(); 
                    LoadCombos(21);
                    LoadCombos(23);
                    cboReportingTo.SelectedIndex = -1;
                    cboHOD.SelectedIndex = -1; 
                    if (MblnAddStatus)
                    {
                        MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID = cboWorkingAt.SelectedValue.ToInt32();
                        GenerateEmployeeCode();
                    }

                    //DataTable datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "bnr.BankBranchID ,(BB.BankName + '-' + bnr.BankBranchName) as Description", "BankBranchReference bnr  LEFT OUTER JOIN BankReference BB on bnr.BankID=BB.BankID left outer join CompanyBankAccountDetails abd on bnr.BankBranchID=abd.BankBranchID ", "" });
                    DataTable datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "bnr.BankBranchID, isnull(BB.BankName,'') + '-' + isnull(bnr.BankBranchName,'') +' - ' + isnull(C.CompanyName,'')  AS Description", "CompanyMaster AS C INNER JOIN CompanyBankAccountDetails AS abd ON C.CompanyID = abd.CompanyID INNER JOIN BankBranchReference AS bnr  INNER JOIN BankReference AS BB ON bnr.BankID = BB.BankID ON abd.BankBranchID = bnr.BankBranchID", "abd.CompanyID=" + cboWorkingAt.SelectedValue + "" });
                    BankNameReferenceComboBox.ValueMember = "BankBranchID";
                    BankNameReferenceComboBox.DisplayMember = "Description";
                    BankNameReferenceComboBox.DataSource = datCombos;

                    BankNameReferenceComboBox.SelectedIndex = -1;
                    BankNameReferenceComboBox.Text = "";


                    ChangeStatus();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboWorkingAt_SelectedIndexChanged" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on cboWorkingAt_SelectedIndexChanged" + Ex.Message.ToString());
            }
        }

        private void BankNameReferenceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (BankNameReferenceComboBox.SelectedIndex != -1 && cboWorkingAt.SelectedIndex != -1)
                {
                    ComBankAccIdComboBox.DataSource = null;

                    DataTable datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "abd.CompanyBankAccountID,abd.BankAccountNo", "BankBranchReference bnr inner join CompanyBankAccountDetails abd on bnr.BankBranchID=abd.BankBranchID ", "CompanyID=" + cboWorkingAt.SelectedValue + " and bnr.BankBranchID=" + Convert.ToInt32(BankNameReferenceComboBox.SelectedValue) + "" });

                    ComBankAccIdComboBox.ValueMember = "CompanyBankAccountID";
                    ComBankAccIdComboBox.DisplayMember = "BankAccountNo";
                    ComBankAccIdComboBox.DataSource = datCombos;

                    if (MblnAddStatus == true)
                    {
                        ComBankAccIdComboBox.SelectedIndex = -1;
                        ComBankAccIdComboBox.Text = "";
                    }
                    if (TransactionTypeReferenceComboBox.SelectedValue.ToInt32() == 1 /*Bank*/ || TransactionTypeReferenceComboBox.SelectedValue.ToInt32() == 3 /*WPS*/)
                    {
                    }
                    else
                    {

                        ComBankAccIdComboBox.Text = "";
                    }

                    if (TransactionTypeReferenceComboBox.SelectedValue.ToInt32() != 3 /*WPS*/)
                    {
                        //if ( BankNameReferenceComboBox.SelectedIndex != -1 )
                        //    CboEmployeeBankname.SelectedValue = Convert.ToInt32(BankNameReferenceComboBox.SelectedValue);

                    }
                    else
                    {
                        CboEmployeeBankname.Enabled = true;
                        BtnEmployeeBankname.Enabled = true;
                    }
                }
                ErrEmployee.SetError(BankNameReferenceComboBox, "");
                // this.SetAutoCompleteList();
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form BankNameReferenceComboBox_SelectedIndexChanged" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BankNameReferenceComboBox_SelectedIndexChanged" + Ex.Message.ToString());
            }
        }


     

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            try
            {
                ErrEmployee.SetError(cboDepartment, "");
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboDepartment_SelectedIndexChanged" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on cboDepartment_SelectedIndexChanged" + Ex.Message.ToString());
            }

        }

        private void cboDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                
                ErrEmployee.SetError(cboDesignation, "");
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboDesignation_SelectedIndexChanged" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on cboDesignation_SelectedIndexChanged" + Ex.Message.ToString());
            }
        }

        private void cboEmploymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            try
            {
                ErrEmployee.SetError(cboEmploymentType, "");
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboEmploymentType_SelectedIndexChanged" + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on cboEmploymentType_SelectedIndexChanged" + Ex.Message.ToString());
            }

        }

        private void btnBankName_Click(object sender, EventArgs e)
        {
            DataTable datCombos = new DataTable();
            try
            {

                int intBank = Convert.ToInt32(BankNameReferenceComboBox.SelectedValue);
                using (FrmCompany objCompany = new FrmCompany())
                {
                    objCompany.PintCompany = Convert.ToInt32(cboWorkingAt.SelectedValue);
                    objCompany.PblnEmployerBankStatus = true;
                    objCompany.ShowDialog();

                    //if (ClsCommonSettings.IsArabicView)
                    //    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "bnr.BankBranchID,(BB.BankNameArb + '-' + bnr.BankBranchName) as Description", "BankBranchReference bnr  LEFT OUTER JOIN BankReference BB on bnr.BankID=BB.BankID left outer join CompanyBankAccountDetails abd on bnr.BankBranchID=abd.BankBranchID ", "" });
                    //else
                    //    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "bnr.BankBranchID,(BB.BankName + '-' + bnr.BankBranchName) as Description", "BankBranchReference bnr  LEFT OUTER JOIN BankReference BB on bnr.BankID=BB.BankID left outer join CompanyBankAccountDetails abd on bnr.BankBranchID=abd.BankBranchID ", "" });
                    datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "bnr.BankBranchID, BB.BankName + '-' + bnr.BankBranchName +' - ' + C.CompanyName  AS Description", "CompanyMaster AS C INNER JOIN CompanyBankAccountDetails AS abd ON C.CompanyID = abd.CompanyID INNER JOIN BankBranchReference AS bnr  INNER JOIN BankReference AS BB ON bnr.BankID = BB.BankID ON abd.BankBranchID = bnr.BankBranchID", "C.CompanyID=" + cboWorkingAt.SelectedValue + "" });

                    BankNameReferenceComboBox.ValueMember = "BankBranchID";
                    BankNameReferenceComboBox.DisplayMember = "Description";
                    BankNameReferenceComboBox.DataSource = datCombos;

                    datCombos = new DataTable();

                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "BankBranchReference.BankBranchID,isnull(BankReference.BankNameArb,'')+ ' - ' + isnull(BankBranchReference.BankBranchName,'')   as Description", "BankBranchReference INNER JOIN BankReference ON BankBranchReference.BankID = BankReference.BankID ORDER BY BankBranchReference.BankBranchName", "" });   // CboEmployeeBankname, "SELECT   ,  ", "BankBranchID", "BankBranch");
                    else
                        datCombos = MObjClsBLLEmployeeInformation.FillCombos(new string[] { "BankBranchReference.BankBranchID,isnull(BankReference.BankName,'')+ ' - ' + isnull(BankBranchReference.BankBranchName,'')   as Description", "BankBranchReference INNER JOIN BankReference ON BankBranchReference.BankID = BankReference.BankID ORDER BY BankBranchReference.BankBranchName", "" });   // CboEmployeeBankname, "SELECT   ,  ", "BankBranchID", "BankBranch");
                    CboEmployeeBankname.ValueMember = "BankBranchID";
                    CboEmployeeBankname.DisplayMember = "Description";
                    CboEmployeeBankname.DataSource = datCombos;

                    if (objCompany.PintCompany != 0)
                    {
                    }
                    else
                    {
                        BankNameReferenceComboBox.SelectedValue = intBank;
                    }

                }


            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form btnBankName_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnBankName_Click" + Ex.Message.ToString());
            }


        }

        private void BtnEmployeeBankname_Click(object sender, EventArgs e)
        {
            try
            {
                int iBankID = Convert.ToInt32(CboEmployeeBankname.SelectedValue);
                FrmBankDetails objCommon = new FrmBankDetails(iBankID);
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(20);
                CboEmployeeBankname.SelectedValue = iBankID;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form BtnEmployeeBankname_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmployeeBankname_Click" + Ex.Message.ToString());
            }
        }

        private void BankNameReferenceComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void CboEmployeeBankname_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void ComBankAccIdComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        private void txtBankAccountName_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void AccountNumberTextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void ComBankAccIdComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrEmployee.SetError(ComBankAccIdComboBox, "");
        }

        private void btnSalaryStructure_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmSalaryStructure objfrmSalaryStructure = new FrmSalaryStructure())
                {
                    objfrmSalaryStructure.PintEmployeeID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intEmployeeID;
                    objfrmSalaryStructure.PintCompanyID = MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intCompanyID;
                    objfrmSalaryStructure.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form btnSalaryStructure_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnSalaryStructure_Click" + Ex.Message.ToString());
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                BnSearchButton_Click(sender, e);
        }

        private void SetAutoCompleteList()
        {
            
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteListWithCompany(cboCompany.SelectedValue.ToInt32(), txtSearch.Text.Trim(), 0, "", "");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);            
        }

       
        private void tsBtnSalaryStructure_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmployeeLeaveStructure objFrmEmployeeLeaveStructure = new FrmEmployeeLeaveStructure())
                {
                    objFrmEmployeeLeaveStructure.pintCompanyID = cboWorkingAt.SelectedValue.ToInt32();
                    objFrmEmployeeLeaveStructure.pintEmployeeID = MintEmployeeID;
                    objFrmEmployeeLeaveStructure.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form btnSalaryStructure_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnSalaryStructure_Click" + Ex.Message.ToString());
            }
        }

        private void btnWorkLocation_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboWorkingAt.SelectedValue.ToInt32() != 0)
                {
                    int WorkLocationId = 0;
                    if (cboWorkLocation.SelectedValue.ToInt32() != 0)
                        WorkLocationId = Convert.ToInt32(cboWorkLocation.SelectedValue);
                    using (FrmWorkLocation objWorkLocation = new FrmWorkLocation())
                    {
                        objWorkLocation.CompanyId = Convert.ToInt32(cboWorkingAt.SelectedValue);
                        objWorkLocation.blnWorkLocationFromOtherPage = true;
                        objWorkLocation.intWorkLocationIDFromOtherPage = cboWorkLocation.SelectedValue.ToInt32();
                        objWorkLocation.ShowDialog();
                    }
                    FillWorkLocation();
                    if (WorkLocationId > 0)
                        cboWorkLocation.SelectedValue = WorkLocationId;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form btnWorkLocation_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnWorkLocation_Click" + Ex.Message.ToString());
            }

        }

        #region FillWorkLocation

        private void FillWorkLocation()
        {
            DataTable dt = new DataTable();
            if (cboWorkingAt.SelectedIndex != -1)
            {

                LoadCombos(21);
                cboWorkLocation.SelectedIndex = -1;
            }

        }

        #endregion

        private void cboWorkLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrEmployee.SetError(cboWorkLocation, "");
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form btnWorkLocation_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnWorkLocation_Click" + Ex.Message.ToString());
            }
        }

        private void txtDeviceUserID_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void passportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new frmPassport() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form passportToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on passportToolStripMenuItem_Click" + Ex.Message.ToString());
            }
          
        }

        private void visaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new frmVisa() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form visaToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on visaToolStripMenuItem_Click" + Ex.Message.ToString());
            }


           
        }

        private void licenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new frmDrivingLicense() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form licenseToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on licenseToolStripMenuItem_Click" + Ex.Message.ToString());
            }

          


        }

        private void qualificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new EmployeeQualification() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form qualificationToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on qualificationToolStripMenuItem_Click" + Ex.Message.ToString());
            }

        }

        private void nationalIDCardToolStripMenuItem_Click(object sender, EventArgs e)

        {
            try
            {
                new EmiratesHealthLabourCard() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form nationalIDCardToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on nationalIDCardToolStripMenuItem_Click" + Ex.Message.ToString());
            }

           
        }

        private void insuranceCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new InsuranceCard() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form insuranceCardToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on insuranceCardToolStripMenuItem_Click" + Ex.Message.ToString());
            }

           
        }

        private void labourCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new FrmEmployeeLabourCard() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form labourCardToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on labourCardToolStripMenuItem_Click" + Ex.Message.ToString());
            }


        }

        private void healthCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new EmployeeHealthCard() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form healthCardToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on healthCardToolStripMenuItem_Click" + Ex.Message.ToString());
            }

        
        }

        private void otherDocumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new frmDocumentMasterNew() { PintEmployeeID = TxtEmployeeNumber.Tag.ToInt64(), eOperationType = OperationType.Employee }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form otherDocumentsToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on otherDocumentsToolStripMenuItem_Click" + Ex.Message.ToString());
            }
        }

        private void bnMoreActions_Click(object sender, EventArgs e)
        {
           
        }

        public bool SetActionPermission()
        {
            clsBLLPermissionSettings ObjPermission = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                DataTable dt = ObjPermission.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID);
                if (dt.Rows.Count == 0)
                {

                   mnuSalaryAdvance.Enabled=mnuLoan.Enabled= mnuLeave.Enabled = btnSalaryStructure.Enabled = mnuLeaveStructure.Enabled = passportToolStripMenuItem.Enabled = visaToolStripMenuItem.Enabled = licenseToolStripMenuItem.Enabled = qualificationToolStripMenuItem.Enabled = nationalIDCardToolStripMenuItem.Enabled =
                     insuranceCardToolStripMenuItem.Enabled = labourCardToolStripMenuItem.Enabled = healthCardToolStripMenuItem.Enabled = otherDocumentsToolStripMenuItem.Enabled = false;
                    return false;
                }
                else
                {

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalaryAdvance).ToString();
                    mnuSalaryAdvance.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Loan).ToString();
                    mnuLoan.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaveEntry).ToString();
                    mnuLeave.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.SalaryStructure).ToString();
                    btnSalaryStructure.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaveStructure).ToString();
                    mnuLeaveStructure.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Passport).ToString();
                    passportToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Visa).ToString();
                    visaToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.DrivingLicense).ToString();
                    licenseToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Qualification).ToString();
                    qualificationToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.NationalIDCard).ToString();
                    nationalIDCardToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.InsuranceCard).ToString();
                    insuranceCardToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LabourCard).ToString();
                    labourCardToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.HealthCard).ToString();
                    healthCardToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.OtherDocuments).ToString();
                    otherDocumentsToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        private void mnuLeave_Click(object sender, EventArgs e)
        {
            try
            {
                new FrmLeaveEntry() { EmployeeID = TxtEmployeeNumber.Tag.ToInt32(),CompanyID=cboWorkingAt.SelectedValue.ToInt32() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form otherDocumentsToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on otherDocumentsToolStripMenuItem_Click" + Ex.Message.ToString());
            }

        }

       

       
        private void cboCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                txtSearch.Text = string.Empty;
                SetAutoCompleteList();
                BnSearchButton_Click(btnSearch, new EventArgs());
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboCompany_SelectionChangeCommitted:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on cboCompany_SelectionChangeCommitted" + Ex.Message.ToString());
            }

        }

        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DtpProbationEndDate.Enabled = cboWorkStatus.SelectedValue.ToInt32() == 7;
                changestatus(sender, e);
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboCompany_SelectionChangeCommitted:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on cboCompany_SelectionChangeCommitted" + Ex.Message.ToString());
            }

        }

        private void mnuLoan_Click(object sender, EventArgs e)
        {
            try
            {
                new FrmEmployeeLoan() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboCompany_SelectionChangeCommitted:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on cboCompany_SelectionChangeCommitted" + Ex.Message.ToString());
            }

        }

        private void mnuSalaryAdvance_Click(object sender, EventArgs e)
        {
            try
            {
                new FrmSalaryAdvance() { EmployeeID = TxtEmployeeNumber.Tag.ToInt64() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboCompany_SelectionChangeCommitted:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on cboCompany_SelectionChangeCommitted" + Ex.Message.ToString());
            }

        }

        private void mnuLeaveStructure_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmployeeLeaveStructure objFrmEmployeeLeaveStructure = new FrmEmployeeLeaveStructure())
                {
                    objFrmEmployeeLeaveStructure.pintCompanyID = cboWorkingAt.SelectedValue.ToInt32();
                    objFrmEmployeeLeaveStructure.pintEmployeeID = MintEmployeeID;
                    //objFrmEmployeeLeaveStructure.objfrm = objForm;
                    //objFrmEmployeeLeaveStructure.objFrmemp = this;
                    objFrmEmployeeLeaveStructure.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form mnuLeaveStructure_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on mnuLeaveStructure_Click" + Ex.Message.ToString());
            }

        }

        private void btnVacationPolicy_Click(object sender, EventArgs e)
        {
            try
            {
                int VacationPolicyID = cboVacationPolicy.SelectedValue.ToInt32();

                using (frmVacationPolicy objVacationPolicy = new frmVacationPolicy())
                {
                    if (cboVacationPolicy.SelectedValue.ToInt32() > 0)
                    {
                        objVacationPolicy.intVacationPolicyID = cboVacationPolicy.SelectedValue.ToInt32();
                    }
                    else
                        objVacationPolicy.intVacationPolicyID = 0;

                    objVacationPolicy.ShowDialog();
                    LoadCombos(22);
                    cboVacationPolicy.SelectedValue = VacationPolicyID;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form btnVacationPolicy_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnVacationPolicy_Click" + Ex.Message.ToString());
            }
        }

        private void FrmEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "Employee";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;

                    case Keys.Control | Keys.Enter:
                        if(BindingNavigatorAddNewItem.Enabled)
                        BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (MblnDeletePermission)
                            BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if(BtnClear.Enabled)
                            BtnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if(BindingNavigatorMovePreviousItem.Enabled)
                        BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        if(BindingNavigatorMoveNextItem.Enabled)
                        BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        if(BindingNavigatorMoveFirstItem.Enabled)
                        BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveLastItem.Enabled)
                        BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (BtnPrint.Enabled)
                        BtnPrint_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        if (BtnEmail.Enabled)
                        BtnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmEmployee_KeyDown:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmEmployee_KeyDown" + Ex.Message.ToString());
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnCountry_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryID, CountryName,CountryNameArb  ", "CountryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intCountryID = Convert.ToInt32(cboCountry.SelectedValue);
                LoadCombos(25);
                if (objCommon.NewID != 0)
                    cboCountry.SelectedValue = objCommon.NewID;
                else
                    cboCountry.SelectedValue = intCountryID;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  BtnNationality " + Ex.Message.ToString());
            }
        }

        private void TxtLastNameArb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetFullName();
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void TxtMiddleNameArb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetFullName();
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void TxtFirstNameArb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetFullName();
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void cboCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void txtEmergencyAddress_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void txtEmergencyPhone_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void txtLocalAddress_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void txtLocalPhone_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ChangeStatus();
            }
            catch
            {
            }
        }

        private void TxtLastNameArb_Enter(object sender, EventArgs e)
        {
            try
            {
                //SetKeyboardLayout();
            }
            catch
            {
            }
        }



        public void SetKeyboardLayout(InputLanguage layout)
        {
            //InputLanguage.CurrentInputLanguage = layout;
            try
            {
                foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
                {
                    if (lang.LayoutName.ToLower() == "arabic (101)")
                    {
                        InputLanguage.CurrentInputLanguage = lang;
                    }
                }
            }
            catch
            {
            }
        }

        private void TxtMiddleNameArb_Enter(object sender, EventArgs e)
        {
            try
            {
                //SetKeyboardLayout();
            }
            catch
            {
            }
        }

        private void TxtFirstNameArb_Enter(object sender, EventArgs e)
        {
            try
            {
                //SetKeyboardLayout();
            }
            catch
            {
            }
        }

        private void btnGrade_Click(object sender, EventArgs e)
        {
            try
            {
                int GradeID = Convert.ToInt32(cboDesignation.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Grade", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "GradeID, Grade,GradeArb", "GradeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(cboGrade.SelectedValue);
                LoadCombos(26);
                if (objCommon.NewID != 0)
                    cboGrade.SelectedValue = objCommon.NewID;
                else
                    cboGrade.SelectedValue = GradeID;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Grade " + Ex.Message.ToString());
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            BtnSave_Click(sender, e);
            if (MObjClsBLLEmployeeInformation.clsDTOEmployeeInformation.intWorkStatusID == 6)
            {
                btnConfirm.Visible = false;
                btnSalaryStructure_Click(sender, e);
            }


        }

        private void chkOther_CheckedChanged(object sender, EventArgs e)
        {
            changestatus(null, null);
            long ReportingTo = cboReportingTo.SelectedValue.ToInt64();
            LoadReportingto();
            if (ReportingTo > 0)
                cboReportingTo.SelectedValue = ReportingTo;
            else
                cboReportingTo.SelectedIndex = -1;

        }


        private void CboVisaObtained_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }


        private void cboBloodGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dtpNextAppraisalDate_ValueChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtAddress2_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtCity_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtState_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void txtZipCode_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }
        void cb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            cb.DroppedDown = false;
        }
        private void dgvLanguages_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

            try
            {
                if (dgvLanguages.CurrentCell != null)
                {
                    if (dgvLanguages.CurrentCell.ColumnIndex == ColLanguages.Index)
                    {
                        ComboBox cb = (ComboBox)e.Control;
                        cb.KeyPress += new KeyPressEventHandler(cb_KeyPress);
                        cb.DropDownHeight = 120;
                        cb.DropDownStyle = ComboBoxStyle.DropDown;
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        cb.AutoCompleteSource = AutoCompleteSource.ListItems;
                    }
                
                }
            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }



        }

        private void dgvLanguages_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void dgvLanguages_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvLanguages.IsCurrentCellDirty)
                {
                    dgvLanguages.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        private void cboMothertongue_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void dgvLanguages_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            ChangeStatus();
        }

        private void chkOCom_CheckedChanged(object sender, EventArgs e)
        {
            changestatus(null, null);
            long HOD = cboReportingTo.SelectedValue.ToInt64();
            LoadHOD();
            if (HOD > 0)
                cboReportingTo.SelectedValue = HOD;
            else
                cboReportingTo.SelectedIndex = -1;
        }


        private void txtint_KeyPress(object sender, KeyPressEventArgs e)
        {
            string strInvalidChars = " abcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+|}{:?></,`-=\\[].";
            e.Handled = false;
            if (strInvalidChars.IndexOf(e.KeyChar.ToString().ToLower()) >= 0)
            {
                e.Handled = true;
            }
        }

        private void txtNoticePeriod_TextChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboCommissionStructure_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void cboProcessType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrEmployee.SetError(cboProcessType, "");
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboProcessType_SelectedIndexChanged:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on cboProcessType_SelectedIndexChanged" + Ex.Message.ToString());
            }
        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            try
            {
                int DivisionID = Convert.ToInt32(cboDivision.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Division", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "DivisionID,IsPredefined,Division,DivisionArb", "DivisionReference", ""); 
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(28);
                if (objCommon.NewID != 0)
                    cboDivision.SelectedValue = objCommon.NewID;
                else
                    cboDivision.SelectedValue = DivisionID;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);
                
            }
        }

        private void cboDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrEmployee.SetError(cboDivision, "");
                ChangeStatus();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form cboDivision_SelectedIndexChanged" + this.Name + " " + Ex.Message.ToString(), 2);
               
            }
        }
    }
}