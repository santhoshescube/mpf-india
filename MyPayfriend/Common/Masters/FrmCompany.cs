﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;


/* 
================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,18 Feb 2011>
   Description:	<Description,,Company Form>
 *
 *  Author:		<Author,,Thasni>
    Modified date: <Modified Date,,12 August 2013>
    Description:	<Description,,Company Form> 
================================================
*/

namespace MyPayfriend
{
    public partial class FrmCompany : Form
    {

        #region Public variable decleration
            public int PintCompanyID;                     //ID pass from navigation
            public int PintCompany = 0;                       // To set Company Externally
            public bool PblnEmployerBankStatus = false;
            ClsLogWriter mObjLogs;
            ClsNotification mObjNotification;
            clsBLLCompanyInformation MobjClsBLLCompanyInformation;
            bool blnIsEditMode = false;
            clsBLLCommonUtility MobjClsBLLCommonUtility = new clsBLLCommonUtility();         


        #endregion

        #region Private variable decleration
            private string OffDayIds=string.Empty;// for setting the offday ids
            
            private bool MblnAddStatus;                   //Add/Update mode 
            private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
            private bool MblnAddPermission = false;           //To set Add Permission
            private bool MblnUpdatePermission = false;      //To set Update Permission
            private bool MblnDeletePermission = false;      //To set Delete Permission
            private bool MblnShowErrorMess;                //set to Show Error Message or not
            private int MintCurrentRecCnt;                  //Contains Current Record count

            private int MintRecordCnt;                      //Contains Total Record count
            private int MintTimerInterval;                  // To set timer interval
            private int MintPCompanyID = -1;               //Identity for present company selection
            private int MintComboID = 0;                    // To Setting SelectedValue for combo box temporarily
            private string MstrMessageCaption;              //Message caption
            private string MstrMessageCommon;               //to set the message
            private string MstrFilePath;                   //to set the company logo path 
            private MessageBoxIcon MmessageIcon;            // to set the message icon
            private DateTime MdteFinyearDate;              // to set financial year date
            private ArrayList MsarMessageArr;                  // Error Message display
            private ArrayList MsarStatusMessage;              // to set status message
            private List<int> lstDeletedBankIds;           // to set deleted companyAccId From Grid While Updating
            private bool MblnIsFromOK = true;
            private bool bnlGridRowDelete;
            string strBindingOf = "Of ";

            #endregion

        #region Constructor
            public FrmCompany()
            {
                //Constructor
                InitializeComponent();
                MblnShowErrorMess = true;
                MintTimerInterval = ClsCommonSettings.TimerInterval;
                MstrMessageCaption = ClsCommonSettings.MessageCaption;
                MmessageIcon = MessageBoxIcon.Information;

                mObjLogs = new ClsLogWriter(Application.StartupPath);
                mObjNotification = new ClsNotification();
                MobjClsBLLCompanyInformation = new clsBLLCompanyInformation();
                lstDeletedBankIds = new List<int>();

                if (ClsCommonSettings.IsArabicView)
                {
                    this.RightToLeftLayout = true;
                    this.RightToLeft = RightToLeft.Yes;
                    SetArabicControls();
                }
            }

            private void SetArabicControls()
            {
                ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
                objDAL.SetArabicVersion((int)FormID.CompanyInformation, this);

                strBindingOf = "من ";
                bnMoreActions.Text = "الإجراءات";
                bnMoreActions.ToolTipText = "الإجراءات";
                TradingLicenseToolStripMenuItem.Text = "الرخصة التجارية";
                TradingLicenseToolStripMenuItem.ToolTipText = "الرخصة التجارية";
                LeaseAgreementToolStripMenuItem.Text = "للإيجار التمويلي الاتفاق";
                LeaseAgreementToolStripMenuItem.ToolTipText = "للإيجار التمويلي الاتفاق";
                otherDocumentsToolStripMenuItem.Text = "وثائق أخرى";
                otherDocumentsToolStripMenuItem.ToolTipText = "وثائق أخرى";
                bnInsurance.Text = "تأمين";
                bnInsurance.ToolTipText = "تأمين";
                GrdCompanyBanks.Columns["BankNameId"].HeaderText = "اسم البنك";
                GrdCompanyBanks.Columns["AccountNumber"].HeaderText = "رقم الحساب";
                label4.Text = "بوصة عرض2 ارتفاع1";
            }
            #endregion

        #region Events
            // form Events
            private void FrmCompany_Load(object sender, EventArgs e)
            {
                try
                {
                    //Load Function
                    LoadCombos(0);
                    SetPermissions();
                    LoadInitial();
                    LoadMessage();
                    BindingEnableDisable(sender, e);

                    if (PintCompany > 0)
                    {
                        MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID = PintCompany;
                        MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate();
                        MintCurrentRecCnt = MobjClsBLLCompanyInformation.GetRowNumber();
                        lblcompanystatus.Text = string.Empty;

                        DisplayCompanyInfo();

                        BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                        BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);

                        SetEnableDisable();
                        SetBindingNavigatorButtons();

                        if (PblnEmployerBankStatus)
                        {
                            CompanyTab.SelectedTab = Accdates;
                        }

                    }
                    else
                    {

                        if (RBtnCompany.Checked)
                        {
                            NameTextBox.Enabled = true;
                            NameTextBox.Focus();
                        }
                        else
                            CboCompanyName.Focus();

                        TmrComapny.Interval = MintTimerInterval;
                        Fillcompanyinfo();

                        SetBindingNavigatorButtons();                      
                        CompanyMasterBindingNavigatorSaveItem.Enabled = BindingNavigatorAddNewItem.Enabled = BindingNavigatorDeleteItem.Enabled = BtnOk.Enabled = BtnSave.Enabled = false;
                        CboCompanyName.SelectedIndex = 0;
                       
                    }
                    MblnIsFromOK = true;
                    SetActionPermission();
                    NameTextBox.Select();
                    NameTextBox.Focus();
                    

                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on Form Load" + this.Name + " " + ex.Message.ToString(), 2);
                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on LoadCombos() " + ex.Message.ToString());


                }
            }
        // form cloding event for user confirmation
            private void FrmCompany_FormClosing(object sender, FormClosingEventArgs e)
            {
                if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID > 0)
                    PintCompany = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID;
                if (!MblnIsFromOK)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 8, out MmessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                       
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
        /// <summary>
        /// short cut ket settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
            private void FrmCompany_KeyDown(object sender, KeyEventArgs e)
            {
                try
                {
                    switch (e.KeyData)
                    {
                        case Keys.F1:
                            FrmHelp objHelp = new FrmHelp();
                            objHelp.strFormName = "Newcompany";
                            objHelp.ShowDialog();
                            objHelp = null;
                            break;
                        case Keys.Escape:
                            this.Close();
                            break;
                      
                        case Keys.Control | Keys.Enter:
                            if(BindingNavigatorAddNewItem.Enabled)
                                BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                            break;
                        case Keys.Alt | Keys.R:
                            if (BindingNavigatorDeleteItem.Enabled)
                               BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                            break;
                        case Keys.Control | Keys.E:
                            if(CancelToolStripButton.Enabled)
                            CancelToolStripButton_Click(sender, new EventArgs());//Clear
                            break;
                        case Keys.Control | Keys.Left:
                            if(BindingNavigatorMovePreviousItem.Enabled)
                                BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                            break;
                        case Keys.Control | Keys.Right:
                            if (BindingNavigatorMoveNextItem.Enabled)
                                BindingNavigatorMoveNextItem_Click(sender, new EventArgs());                       
                            break;
                        case Keys.Control | Keys.Up:
                            if(BindingNavigatorMoveFirstItem.Enabled)
                            BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                            break;
                        case Keys.Control | Keys.Down:
                            if(BindingNavigatorMoveLastItem.Enabled)
                            BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                            break;
                        case Keys.Control | Keys.P:
                            BtnPrint_Click(sender, new EventArgs());//Cancel
                            break;
                        case Keys.Control | Keys.M:
                            if(BtnEmail.Enabled)
                            BtnEmail_Click(sender, new EventArgs());//Cancel
                            break;
                    }
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error form key press" + this.Name + " " + ex.Message.ToString(), 2);

                }
            }

            //Combo Events
            private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
            {
                ComboBox cbo = (ComboBox)sender;
                cbo.DroppedDown = false;
            }
            private void cboDays_SelectedIndexChanged(object sender, EventArgs e)
            {
                Changestatus();
            }
         

            // Binding Navigator Events
            private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
            {

                AddNewCompany();
            }
            private void CompanyMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
            {
                BtnSave_Click(sender, e);
            }
            private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
            {
                //Deleting Data
                try
                {
                    if (DeleteValidation())
                    {
                        if (MobjClsBLLCompanyInformation.DeleteCompany(Convert.ToInt32(NameTextBox.Tag)))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 4, out MmessageIcon);
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            AddNewCompany();
                        }

                    }
                }
                catch (SqlException Ex)
                {
                    if (Ex.Number == (int)SqlErrorCodes.ForeignKeyErrorNumber)
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 226, out MmessageIcon);
                    else
                        MstrMessageCommon = "Error on DeleteItem_Click() " + Ex.Message.ToString();

                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TmrComapny.Enabled = true;

                }

                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BindingNavigatorDeleteItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BindingNavigatorDeleteItem_Click" + ex.Message.ToString());


                }
              
            }
            private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
            {
                try
                {
                    //Navigator Moving
                    MblnAddStatus = false;
                    ErrorProviderCompany.Clear();
                    MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate();
                    if (MintRecordCnt > 0)
                    {
                        BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                        MintCurrentRecCnt = 1;
                    }
                    else
                        MintCurrentRecCnt = 0;

                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 9, out MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                    DisplayCompanyInfo();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    ErrorProviderCompany.Clear();
                    SetEnableDisable();
                    SetBindingNavigatorButtons();
                    blnIsEditMode = MblnIsFromOK = true;


                    //if (NameTextBox.Tag.ToInt32() == ClsCommonSettings.CurrentCompanyID)
                    //    bnMoreActions.Enabled = true;
                    //else
                    //    bnMoreActions.Enabled = false;

                   
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BindingNavigatorMoveFirstItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BindingNavigatorMoveFirstItem_Click" + ex.Message.ToString());


                }
            }
            private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
            {
                try
                {
                    //Navigator Moving
                    MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate();
                    if (MintRecordCnt > 0)
                    {
                        BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                        MintCurrentRecCnt = MintCurrentRecCnt + 1;

                        if (MintCurrentRecCnt >= MintRecordCnt)
                        {
                            MintCurrentRecCnt = MintRecordCnt;
                        }
                    }
                    else
                    {
                        MintCurrentRecCnt = 0;
                    }
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 11, out MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                    DisplayCompanyInfo();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    SetEnableDisable();
                    SetBindingNavigatorButtons();
                    blnIsEditMode = MblnIsFromOK = true;
                    //if (NameTextBox.Tag.ToInt32() == ClsCommonSettings.CurrentCompanyID)
                    //    bnMoreActions.Enabled = true;
                    //else
                    //    bnMoreActions.Enabled = false;
                   
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BindingNavigatorMoveNextItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BindingNavigatorMoveNextItem_Click" + ex.Message.ToString());


                }
            }
            private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
            {
                try
                {
                    //Navigator Moving To Last Item
                    MblnAddStatus = false;
                    ErrorProviderCompany.Clear();
                    MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate();
                    if (MintRecordCnt > 0)
                    {
                        BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                        MintCurrentRecCnt = MintRecordCnt;
                    }
                    else
                    {
                        MintCurrentRecCnt = 0;
                    }
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 12, out MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                    DisplayCompanyInfo();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    SetEnableDisable();
                    SetBindingNavigatorButtons();
                    blnIsEditMode = MblnIsFromOK = true;


                    //if (NameTextBox.Tag.ToInt32() == ClsCommonSettings.CurrentCompanyID)
                    //    bnMoreActions.Enabled = true;
                    //else
                    //    bnMoreActions.Enabled = false;

                   
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BindingNavigatorMoveLastItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BindingNavigatorMoveLastItem_Click" + ex.Message.ToString());


                }
            }
            private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
            {
                try
                {
                    // Moving to previous item
                    MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate();
                    if (MintRecordCnt > 0)
                    {
                        BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                        MintCurrentRecCnt = MintCurrentRecCnt - 1;

                        if (MintCurrentRecCnt <= 0)
                        {
                            MintCurrentRecCnt = 1;
                        }
                    }
                    else
                    {
                        BindingNavigatorPositionItem.Text = strBindingOf + "0";
                        MintRecordCnt = 0;
                    }
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 10, out MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);

                    DisplayCompanyInfo();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    SetEnableDisable();
                    SetBindingNavigatorButtons();
                    MblnIsFromOK = blnIsEditMode = true;

                    //if (NameTextBox.Tag.ToInt32() == ClsCommonSettings.CurrentCompanyID)
                    //    bnMoreActions.Enabled = true;
                    //else
                    //    bnMoreActions.Enabled = false;
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BindingNavigatorMovePreviousItem_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on CompanyMasterBindingNavigatorMovePreviousItem_Click" + ex.Message.ToString());

                }
            }

            // Grid events
            private void GrdCompanyBanks_CurrentCellDirtyStateChanged(object sender, EventArgs e)
            {
                //   grdCompanyBanks State Changed
                if (GrdCompanyBanks.IsCurrentCellDirty)
                {
                    if (GrdCompanyBanks.CurrentCell != null)
                        GrdCompanyBanks.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            private void GrdCompanyBanks_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
            {
                // Cell Begin Edit
                if (e.RowIndex != -1)
                {
                    int iRowIndex = GrdCompanyBanks.CurrentCell.RowIndex;
                    if (e.ColumnIndex == 3)
                    {
                        if (GrdCompanyBanks.CurrentRow.Cells[2].Value == null)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 202, out MmessageIcon);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            GrdCompanyBanks.CurrentCell = GrdCompanyBanks[2, iRowIndex];
                            e.Cancel = true;
                        }
                    }
                    if (GrdCompanyBanks.Rows.Count >= 2)
                    {
                        if (e.ColumnIndex == 2)
                        {
                            int iCurRowIndex = GrdCompanyBanks.CurrentRow.Index - 1;
                            if (iCurRowIndex >= 0)
                            {
                                if (Convert.ToString(GrdCompanyBanks.Rows[iCurRowIndex].Cells[3].Value).Trim() == "")
                                {
                                    //MsMessageCommon = "Please enter Account number.";
                                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 203, out MmessageIcon);
                                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                    GrdCompanyBanks.CurrentCell = GrdCompanyBanks[3, iCurRowIndex];
                                    e.Cancel = true;
                                }
                            }
                        }
                    }
                }


                if (MblnAddStatus != true)
                {

                    if (GrdCompanyBanks.CurrentRow != null)
                    {
                        if (GrdCompanyBanks.CurrentRow.Cells["CompanyBankId"].Value.ToInt32() > 0)
                        {
                            if (MobjClsBLLCompanyInformation.GetBankBranchExistsInEmployee(GrdCompanyBanks.CurrentRow.Cells["BankNameId"].Value.ToInt32()))
                            {

                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 234, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrComapny.Enabled = true;
                                e.Cancel = true;
                                return;
                            }
                        }

                    }

                    Changestatus();
                }

            }
            private void GrdCompanyBanks_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
            {

                if (GrdCompanyBanks.Rows[e.Row.Index] != null && GrdCompanyBanks[0, e.Row.Index].Value != null && bnlGridRowDelete == false)
                    lstDeletedBankIds.Add(Convert.ToInt32(GrdCompanyBanks[0, e.Row.Index].Value));
            }
            private void GrdCompanyBanks_CellValueChanged(object sender, DataGridViewCellEventArgs e)
            {
                Changestatus();
            }
            private void GrdCompanyBanks_KeyDown(object sender, KeyEventArgs e)
            {
                if (MblnAddStatus != true)
                {
                    if (e.KeyCode == Keys.Delete)
                    {
                        bnlGridRowDelete = false;
                        if (GrdCompanyBanks.CurrentRow != null)
                        {
                            if (MobjClsBLLCompanyInformation.GetBankBranchExistsInEmployee(GrdCompanyBanks.CurrentRow.Cells["BankNameId"].Value.ToInt32()))
                            {
                                e.Handled = true;
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 231, out MmessageIcon);
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrComapny.Enabled = true;
                                bnlGridRowDelete = true;
                                return;
                            }
                            else
                            {
                                e.Handled = false;
                            }
                        }

                        Changestatus();
                    }
                }


            }
            private void GrdCompanyBanks_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
            {
                MobjClsBLLCommonUtility.SetSerialNo(GrdCompanyBanks, e.RowIndex, false);
            }
            private void GrdCompanyBanks_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
            {
                MobjClsBLLCommonUtility.SetSerialNo(GrdCompanyBanks, e.RowIndex, false);

            }

            //setting company/ baranch mode according to radio button
            private void RbtnBranch_CheckedChanged(object sender, EventArgs e)
            {
                //radio button branch checked changed
                if (RBtnCompany.Checked == true)
                {
                    NameTextBox.Location = new Point(140, 43);
                    CboCompanyName.Visible = false;
                    TxtBranchName.Visible = true;
                    ErrorProviderCompany.SetError(RBtnCompany, "");
                    CboCompanyName.SelectedIndex = -1;
                    NameTextBox.Focus();
                    CboCompanyName.Text = "";                           
                    TxtBranchName.Enabled= LblName.Enabled = false;

                    if (MblnAddStatus == true)
                        NameTextBox.Text = "";

                    NameTextBox.Visible = true;
                    BtnCurrency.Enabled= CboCurrency.Enabled = true;

                }
                else
                {
                    CboCompanyName.Visible = true;
                    NameTextBox.Location = new Point(139, 67); 
                    TxtBranchName.Visible = false;
                    ErrorProviderCompany.SetError(RBtnCompany, "");
                    NameTextBox.Visible = true;
                    CboCompanyName.Focus();
                    TxtBranchName.Visible = false;
                    LblName.Enabled = true;
                    if (MblnAddStatus == true)
                        NameTextBox.Text = "";
                    CboCompanyName.SelectedIndex = 0;
                    BtnCurrency.Enabled = CboCurrency.Enabled = false;
                }

                CboCompanyName.SelectedIndex = 0;

               
            }
        // settings the working days according to Year and month
            private void RBtnYear_CheckedChanged(object sender, EventArgs e)
            {
                Changestatus();
                //if (MblnAddStatus == true)
                //{
                    if (RBtnYear.Checked == true)
                    {
                        NumTxtWorkingDays.Text = "365";
                    }
                    else
                    {
                        NumTxtWorkingDays.Text = "30";
                    }
                //}

            }
         

            // Button Events
            private void CancelToolStripButton_Click(object sender, EventArgs e)
            {
                //Cancel Tool Strip Button Click
                CompanyTab.SelectedTab = CompanyInfoTab;

                if (PintCompanyID < 0)
                    BindingEnableDisable(sender, e);
                else
                    AddNewCompany();
            }
            private void BtnPrint_Click(object sender, EventArgs e)
            {
                //Printing
                try
                {
                    LoadReport();
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BtnPrint_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BtnPrint_Click" + ex.Message.ToString());


                }
            }
            private void BtnEmail_Click(object sender, EventArgs e)
            {
                //Email
                try
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Company Information";
                        ObjEmailPopUp.EmailFormType = EmailFormID.Company;
                        ObjEmailPopUp.EmailSource = MobjClsBLLCompanyInformation.GetCompanyReport();
                        ObjEmailPopUp.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BtnEmail_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BtnEmail_Click" + ex.Message.ToString());


                }
            }
            private void BtnSave_Click(object sender, EventArgs e)
            {
                //Saving Data
                try
                {
                    if (CompanyValidation())
                    {
                        if (SaveCompany())
                        {
                            MintCurrentRecCnt = MintCurrentRecCnt + 1;
                            string msg = mObjNotification.GetErrorMessage(MsarMessageArr, (MblnAddStatus ? 2 : 21), out MmessageIcon);
                            LoadCombos(2);
                            BindingNavigatorMovePreviousItem_Click(null, null);

                            lblcompanystatus.Text = msg.Remove(0, msg.IndexOf("#") + 1);
                            MessageBox.Show(msg.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                            //AddNewCompany();

                        }
                    }
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BtnSave_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BtnSave_Click" + ex.Message.ToString());


                }
            }
            private void BtnOk_Click(object sender, EventArgs e)
            {
                try
                {
                    //Saving and closing
                    if (CompanyValidation())
                    {
                        if (SaveCompany())
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 2, out MmessageIcon);
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                            MblnIsFromOK = true;
                            this.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BtnOk_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BtnOk_Click" + ex.Message.ToString());

                }
            }
            private void BtnCancel_Click(object sender, EventArgs e)
            {
                this.Close();
            }
            private void BtnRemove_Click(object sender, EventArgs e)
            {
                //Removing logo
                MstrFilePath = "";
                LogoFilePictureBox.Image = null;
                Changestatus();
            }
            private void FilePathButton_Click(object sender, EventArgs e)
            {
                try
                {
                    //Browing for logo
                    //MblnChangeStatus = true;
                    Image ImageFile;
                    OpenFileDialog Dlg;
                    Dlg = new OpenFileDialog();
                    Dlg.Filter = "Images (*.jpg;*.jpeg)|*.jpg;*.jpeg";
                    Dlg.ShowDialog();
                    MstrFilePath = Dlg.FileName;

                    if (Convert.ToString(MstrFilePath) != "")
                    {
                        if (System.IO.File.Exists(MstrFilePath) == true)
                            ImageFile = Image.FromFile(MstrFilePath.ToString());
                        LogoFilePictureBox.Image = Image.FromFile(MstrFilePath.ToString());

                        MstrMessageCommon = "Do you want to resize the logo, to above mentioned size?";
                        if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                            ResizeLogo();
                    }
                    Changestatus();
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on FilePathButton_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on FilePathButton_Click" + ex.Message.ToString());


                }
            }
            private void BtnCountry_Click(object sender, EventArgs e)
            {
                try
                {
                    MintComboID = Convert.ToInt32(CboCountry.SelectedValue);
                    FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0, MintTimerInterval }, "CountryID,CountryName As Country,CountryNameArb As CountryArb", "CountryReference", "");
                    objCommon.ShowDialog();
                    objCommon.Dispose();
                    LoadCombos(4);
                    if (objCommon.NewID != 0)
                        CboCountry.SelectedValue = objCommon.NewID;
                    else
                        CboCountry.SelectedValue = MintComboID;
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BtnCountry_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BtnCountry_Click" + ex.Message.ToString());


                }
            }
            private void CboComIndustry_Click(object sender, EventArgs e)
            {
                try
                {
                    MintComboID = Convert.ToInt32(CboCompanyIndustry.SelectedValue);
                    FrmCommonRef objCommon = new FrmCommonRef("Company Industry", new int[] { 1, 0, MintTimerInterval }, "CompanyIndustryID,CompanyIndustry As [Company Industry],CompanyIndustryArb As [Company IndustryArb]", "IndustryReference", "");
                    objCommon.ShowDialog();
                    objCommon.Dispose();
                    LoadCombos(1);
                    if (objCommon.NewID != 0)
                        CboCompanyIndustry.SelectedValue = objCommon.NewID;
                    else
                        CboCompanyIndustry.SelectedValue = MintComboID;
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on CboComIndustry_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on CboComIndustry_Click" + ex.Message.ToString());


                }
            }
            private void BtnProvince_Click(object sender, EventArgs e)
            {
                try
                {
                    MintComboID = Convert.ToInt32(CboProvince.SelectedValue);
                    FrmCommonRef objCommon = new FrmCommonRef("Province/State", new int[] { 1, 0, MintTimerInterval }, "ProvinceID,ProvinceName As Province,ProvinceNameArb As ProvinceArb", "ProvinceReference", "");
                    objCommon.ShowDialog();
                    objCommon.Dispose();
                    LoadCombos(7);
                    if (objCommon.NewID != 0)
                        CboProvince.SelectedValue = objCommon.NewID;
                    else
                        CboProvince.SelectedValue = MintComboID;
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BtnProvince_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BtnProvince_Click" + ex.Message.ToString());


                }
            }
            private void BtnCompanyType_Click(object sender, EventArgs e)
            {
                try
                {
                    MintComboID = Convert.ToInt32(CboCompanyType.SelectedValue);
                    FrmCommonRef objCommon = new FrmCommonRef("Company Type", new int[] { 1, 0, MintTimerInterval }, "CompanyTypeID,CompanyType,CompanyTypeArb", "CompanyTypeReference", "");
                    objCommon.ShowDialog();
                    objCommon.Dispose();
                    LoadCombos(3);
                    if (objCommon.NewID != 0)
                        CboCompanyType.SelectedValue = objCommon.NewID;
                    else
                        CboCompanyType.SelectedValue = MintComboID;
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BtnCompanyType_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BtnCompanyType_Click" + ex.Message.ToString());


                }
            }
           
            private void BtnBank_Click(object sender, EventArgs e)
            {
                try
                {
                    using (FrmBankDetails objBank = new FrmBankDetails(0))
                    {
                        objBank.ShowDialog();
                    }
                    LoadCombos(8);
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on  BtnBank_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BtnBank_Click" + ex.Message.ToString());

                }
            }
            private void BtnCurrency_Click(object sender, EventArgs e)
            {
                //try
                //{
                //    MintComboID = Convert.ToInt32(CboCurrency.SelectedValue);
                //    using (FrmCurrency  objCur = new FrmCurrency())
                //    {
                //       //objCur.MintCurrencyDetailId = Convert.ToInt32(CboCurrency.SelectedValue);
                //        objCur.ShowDialog();
                //    }
                //    LoadCombos(5);
                //    CboCurrency.SelectedValue = MintComboID;
                //}
                //catch (Exception ex)
                //{
                //    mObjLogs.WriteLog("Error on BtnCurrency_Click " + this.Name + " " + ex.Message.ToString(), 2);

                //    if (ClsCommonSettings.ShowErrorMess)
                //        MessageBox.Show("Error on BtnCurrency_Click" + ex.Message.ToString());

                //}
            }
            private void BtnHelp_Click(object sender, EventArgs e)
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "Newcompany";
                objHelp.ShowDialog();
                objHelp = null;
            }
            private void DtpFinyearDate_ValueChanged(object sender, EventArgs e)
            {
                //Financial year date value changed
                if (MblnAddStatus == true)
                    DtpBkStartDate.Value = DtpFinyearDate.Value;
                Changestatus();
            }           
            private void txt_TextChanged(object sender, EventArgs e)
            {
                Changestatus();

            }
            private void NumTxtWorkingDays_TextChanged(object sender, EventArgs e)
            {
                Changestatus();
            }
            private void btnUnearnedPolicy_Click(object sender, EventArgs e)
            {
                try
                {
                    MintComboID = Convert.ToInt32(cboUnearnedPolicyID.SelectedValue);
                    using (FrmUnearnedPolicy objUP = new FrmUnearnedPolicy())
                    {
                        objUP.PintUnEarnedPolicyID = MintComboID;
                        objUP.ShowDialog();
                    }
                    LoadCombos(6);
                    cboUnearnedPolicyID.SelectedValue = MintComboID;
                }
                catch (Exception ex)
                {
                    mObjLogs.WriteLog("Error on BtnCurrency_Click " + this.Name + " " + ex.Message.ToString(), 2);

                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error on BtnCurrency_Click" + ex.Message.ToString());

                }
            }

            private void chkSunday_CheckedChanged(object sender, EventArgs e)
            {
                // setting the offdayid string with comma separated while checked changed

                CheckBox chk = (CheckBox)sender;
                if (chk.Checked)
                    OffDayIds = OffDayIds + "," + Convert.ToString(chk.Tag);
                else
                    OffDayIds=OffDayIds.Replace("," + Convert.ToString(chk.Tag), string.Empty);
                Changestatus();
            }
            #endregion  

        #region Functions


        /// <summary>
        /// Function for setting the offday checkboxes
        /// </summary>
            private void SetCheckBoxes(string dayIds)
            {
                chkSunday.Checked = dayIds.Contains(Convert.ToString(chkSunday.Tag));
                chkMonday.Checked = dayIds.Contains(Convert.ToString(chkMonday.Tag));
                chkTuesDay.Checked = dayIds.Contains(Convert.ToString(chkTuesDay.Tag));
                chkWednesday.Checked = dayIds.Contains(Convert.ToString(chkWednesday.Tag));
                chkThursday.Checked = dayIds.Contains(Convert.ToString(chkThursday.Tag));
                chkFriday.Checked = dayIds.Contains(Convert.ToString(chkFriday.Tag));
                chkSaturday.Checked = dayIds.Contains(Convert.ToString(chkSaturday.Tag));
            }
        /// <summary>
        /// filling the company details
        /// </summary>
        private void Fillcompanyinfo()
        {
            try
            {
                // Filling Company Information
                if (Convert.ToInt32(MintPCompanyID) > 0)
                {
                    int ComID;
                    ComID = Convert.ToInt32(NameTextBox.Tag);
                    DisplayCompanyInfo();
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintPCompanyID);
                    MintCurrentRecCnt = Convert.ToInt32(MintPCompanyID);
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on FillCompanyInfo() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FillCompanyInfo() " + ex.Message.ToString());

            }
        }
        /// <summary>
        /// display company information
        /// </summary>

        private void DisplayCompanyInfo()
        {
            try
            {
            
                //Displaying Company Information
                int ParentID = 0;

                if (MobjClsBLLCompanyInformation.DisplayCompanyInfo(MintCurrentRecCnt))
                {
                    CancelToolStripButton.Enabled = false;
                    NameTextBox.Tag = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID;

                    ParentID = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intParentID;

                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnCompanyBranchIndicator)
                    {
                        CboCompanyName.SelectedValue = ParentID;
                        RbtnBranch.Checked = true;
                        RBtnCompany.Checked = false;
                        CboCompanyName.SelectedValue = ParentID;
                    }
                    else
                    {
                        RBtnCompany.Checked = true;
                        RbtnBranch.Checked = false;
                        CboCompanyName.SelectedIndex = -1;                     
                    }
                   
                    NameTextBox.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strName;

                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strFinYearStartDate != null)
                    {
                        DtpFinyearDate.Value = Convert.ToDateTime(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strFinYearStartDate);
                        MdteFinyearDate = DtpFinyearDate.Value.Date;
                    }

                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBookStartDate != null)
                        DtpBkStartDate.Value = Convert.ToDateTime(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBookStartDate);

                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strStartDate != null)
                        dtpStartDate.Value = Convert.ToDateTime(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strStartDate);
  
                    TxtEmployerCode.Text = Convert.ToString(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strEPID);
                    TxtShortName.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strShortName;
                    TxtPOBox.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPOBox;
                    CboCountry.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCountryID;
                    CboCurrency.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCurrencyId;
                    CboCompanyIndustry.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyIndustryID;
                    CboCompanyType.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyTypeID;
                    CboProvince.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intProvinceID;
                    TxtArea.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strArea;
                    CboProvince.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intProvinceID;
                    cboUnearnedPolicyID.SelectedValue = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intUnernedPolicyID;

                    TxtBlock.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBlock;
                    TxtStreet.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strRoad;
                    TxtCity.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCity;
                    TxtPrimaryEmail.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPrimaryEmail;
                    TxtSecondaryEmail.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strSecondaryEmail;
                    TxtContactPersonPhone.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strTelePhone;
                    TxtFaxNumber.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPABXNumber;
                    TxtWebSite.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strWebSite;
                    TxtOtherInfo.Text = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOtherInfo;


                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnIsMonth)
                    {
                        RBtnMonth.Checked = true;
                        RBtnYear.Checked = false;
                    }
                    else
                    {
                        RBtnYear.Checked = true;
                        RBtnMonth.Checked = false;
                    }

                    NumTxtWorkingDays.Text = Convert.ToString(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intWorkingDaysInMonth);


                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.byteLogoFile != null)
                    {
                        ClsImages objimg = new ClsImages();
                        LogoFilePictureBox.Image = objimg.GetImage(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.byteLogoFile);
                    }
                    else
                    {
                        LogoFilePictureBox.Image = null;
                    }
                  
                    SetCheckBoxes(","+MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOffDayIDs);
              
                    if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnCompanyBranchIndicator == false)
                    {
                        CboCompanyName.SelectedValue = ParentID;
                    }

                    DisplayCompanyBankInfo();
                    BtnPrint.Enabled = BtnEmail.Enabled=MblnPrintEmailPermission;
                  
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on DisplayCompanyInfo() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DisplayCompanyInfo()" + ex.Message.ToString());

            }
        }

        /// <summary>
        /// display company bank details
        /// </summary>
        private void DisplayCompanyBankInfo()
        {
            try
            {
                //Displaying Company Bank Information
                GrdCompanyBanks.Rows.Clear();
                DataTable DtCompanyBank = MobjClsBLLCompanyInformation.DisplayBankInformation(Convert.ToInt32(NameTextBox.Tag));

                if (DtCompanyBank.Rows.Count > 0)
                {
                    for (int i = 0; i < DtCompanyBank.Rows.Count; i++)
                    {
                        GrdCompanyBanks.RowCount = GrdCompanyBanks.RowCount + 1;
                        GrdCompanyBanks.Rows[i].Cells[0].Value = DtCompanyBank.Rows[i]["CompanyBankAccountID"];
                        GrdCompanyBanks.Rows[i].Cells[1].Value = DtCompanyBank.Rows[i]["CompanyID"];
                        GrdCompanyBanks.Rows[i].Cells[2].Value = DtCompanyBank.Rows[i]["BankBranchID"];
                        GrdCompanyBanks.Rows[i].Cells[3].Value = DtCompanyBank.Rows[i]["BankAccountNo"];
                    }
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on DisplayCompanyBankInfo() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DisplayCompanyBankInfo()" + ex.Message.ToString());

            }
        }

        /// <summary>
        /// load the initial settings
        /// </summary>
        private void LoadInitial()
        {
            // Function for Intialisation While Loading
            GrdCompanyBanks.ShowCellToolTips = false;
            this.ToolTip1.ShowAlways = true;
            this.ToolTip1.ReshowDelay = 2;
            this.ToolTip1.AutomaticDelay = 300;
            ToolTip1.SetToolTip(GrdCompanyBanks, "Name of the Bank and its account number for pay processing and release. \nYou can enter multiple bank names from which the pay can be released.");
            ErrorProviderCompany.Clear();
            lblcompanystatus.Text = "";
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            NameTextBox.Focus();
            MblnAddStatus = false;
        }
        
        /// <summary>
        /// filling the message array according the form
        /// </summary>
        private void LoadMessage()
        {
            try
            {
                // Loading Message
                MsarMessageArr = new ArrayList();
                MsarStatusMessage = new ArrayList();
                MsarMessageArr = mObjNotification.FillMessageArray((int)FormID.CompanyInformation, ClsCommonSettings.ProductID);
                MsarStatusMessage = mObjNotification.FillStatusMessageArray((int)FormID.CompanyInformation, ClsCommonSettings.ProductID);
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadMessage() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadMessage()" + ex.Message.ToString());
            }
        }
        
        /// <summary>
        /// setting sthe binding navigator controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BindingEnableDisable(Object sender, EventArgs e)
        {
            try
            {
                //seting Binding Enable Disable
                MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt + 1) + "";
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
                    MintCurrentRecCnt = MintRecordCnt + 1;
                }
                else
                {
                    BindingNavigatorCountItem.Text = strBindingOf + "0";
                    MintCurrentRecCnt = 0;
                }

                if (MintPCompanyID <= 0)
                {

                  this.BindingNavigatorPositionItem.Enabled = this.BindingNavigatorMovePreviousItem.Enabled =this.BindingNavigatorMoveFirstItem.Enabled = this.BindingNavigatorMoveNextItem.Enabled= this.BindingNavigatorMoveLastItem.Enabled=!(MintRecordCnt == 0);    
                   AddNewCompany();
                }
                else
                {
                    this.BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
                    this.BindingNavigatorAddNewItem.Visible = true;
                }
                CompanyTab.SelectedTab = CompanyInfoTab;

               
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on BindingEnableDisable() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BindingEnableDisable()" + ex.Message.ToString());
            }
        }

        /// <summary>
        /// set the add new mode
        /// </summary>
        private void AddNewCompany()
        {
            try
            {

                //Adding New Company
                MobjClsBLLCompanyInformation = new clsBLLCompanyInformation();
                lstDeletedBankIds = new List<int>();
                //CboCurrency.Enabled = true;

                ClearControls();
                MblnAddStatus = true;
                CompanyTab.SelectedTab = CompanyInfoTab;
                CompanyInfoTab.Focus();
                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 655, out MmessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrComapny.Enabled = true;
               
                //RBtnCompany.Visible = false;
                //RbtnBranch.Visible = true;
                //RbtnBranch.Location = RBtnCompany.Location;
              
                ErrorProviderCompany.Clear();
              
                MintRecordCnt = MobjClsBLLCompanyInformation.RecCountNavigate();
                bnMoreActions.Enabled= BindingNavigatorDeleteItem.Enabled = BindingNavigatorAddNewItem.Enabled = false;

                if (MintRecordCnt > 0)
                {
                    BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = true;

                    BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt + 1) + "";
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
                    MintCurrentRecCnt = MintRecordCnt + 1;
                }
                else
                {
                    BindingNavigatorCountItem.Text = strBindingOf + "1";
                    BindingNavigatorPositionItem.Text = Convert.ToString(1);
                    MintCurrentRecCnt = 1;
                }
                string StrDate;
                DateTime DateDate;

                NumTxtWorkingDays.Text = "365";
                 DateTime dt = ClsCommonSettings.GetServerDate();
                DateTime dtFinYear;
               
                    dtFinYear = MobjClsBLLCompanyInformation.GetFinyearDate(CboCompanyName.SelectedValue.ToInt32());
                    StrDate = "01/" + dtFinYear.ToString("MMM") + "/" + dt.Year;              
                            
                
                
                DtpBkStartDate.Value = DtpFinyearDate.Value = Convert.ToDateTime(StrDate);
               
                DtpBkStartDate.Enabled = true;
                NameTextBox.Focus();
                //MblnChangeStatus = false;
                BtnEmail.Enabled = BtnPrint.Enabled = CompanyMasterBindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = false;
                            
              
                GrdCompanyBanks.Rows.Clear();

                RbtnBranch.Checked = true;
                RBtnCompany.Checked = false;
                SetBindingNavigatorButtons();
                blnIsEditMode = false;
                CboCompanyName.SelectedIndex =0;
                MblnIsFromOK = true;
                //CboCompanyName.Text = "";
                CancelToolStripButton.Enabled = true;
                NameTextBox.Select();
                NameTextBox.Focus();


            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on AddNewCompany() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewCompany()" + ex.Message.ToString());
            }
        }

        /// <summary>
        /// Clear the controls
        /// </summary>
        private void ClearControls()
        {
            // function for clearing controls
            ErrorProviderCompany.Clear();
        
            CboCompanyName.SelectedIndex = -1;
            CboProvince.Text = TxtArea.Text = TxtBlock.Text = TxtStreet.Text = TxtCity.Text = CboCountry.Text = CboCompanyIndustry.Text = CboCompanyType.Text = lblcompanystatus.Text = CboCompanyName.Text = NameTextBox.Text = TxtEmployerCode.Text = TxtShortName.Text = TxtPOBox.Text = TxtWebSite.Text =cboUnearnedPolicyID.Text= string.Empty;
            NumTxtWorkingDays.Text = TxtContactPersonPhone.Text = TxtFaxNumber.Text = TxtOtherInfo.Text = TxtPrimaryEmail.Text = TxtSecondaryEmail.Text = string.Empty;
            NameTextBox.Tag = "0";
            cboUnearnedPolicyID.SelectedIndex= CboCompanyType.SelectedIndex = CboProvince.SelectedIndex = CboCompanyIndustry.SelectedIndex = CboCountry.SelectedIndex = -1;
            chkSunday.Checked = chkMonday.Checked = chkTuesDay.Checked = chkWednesday.Checked = chkThursday.Checked = chkFriday.Checked = chkSaturday.Checked = false;


            DateTime dtFinYear;
            string StrDate;
            DateTime dt=ClsCommonSettings.GetServerDate();
           
            dtFinYear = MobjClsBLLCompanyInformation.GetFinyearDate(ClsCommonSettings.CurrentCompanyID);
            StrDate = "01/" + dtFinYear.ToString("MMM") + "/" + dt.Year;

            DtpFinyearDate.Value =Convert.ToDateTime(StrDate);
            dtpStartDate.Value =Convert.ToDateTime(StrDate);
            DtpBkStartDate.Value = Convert.ToDateTime(StrDate);

            LogoFilePictureBox.Image = null;
            GrdCompanyBanks.ClearSelection();
            RBtnYear.Checked = true;
            CboCurrency.SelectedValue = MobjClsBLLCompanyInformation.GetCompanyCurrency(CboCompanyName.SelectedValue.ToInt32());
        }     
        /// <summary>
        /// Set the enability of buttons
        /// </summary>
        private void SetEnableDisable()
        {
            // function for setting enable and disable
            ErrorProviderCompany.Clear();
            CompanyTab.SelectedTab = CompanyInfoTab;
             TmrComapny.Enabled = true;
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
        
            if (MintCurrentRecCnt >= 1)
               BtnPrint.Enabled = BtnEmail.Enabled = MblnPrintEmailPermission;
            

            CompanyMasterBindingNavigatorSaveItem.Enabled = false;
            BtnOk.Enabled =BtnSave.Enabled=MblnAddStatus= false;
            
        }
        /// <summary>
        /// Resize the company logo
        /// </summary>
        private void ResizeLogo()
        {
            //function for resize emp image
            Bitmap bm = new Bitmap(LogoFilePictureBox.Image);
            int width = 192;
            int height = 96;
            Bitmap thumb = new Bitmap(width, height);
            Graphics g;
            g = Graphics.FromImage(thumb);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(bm, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel);
            LogoFilePictureBox.Image = thumb;
            g.Dispose();
            bm.Dispose();
        }

        /// <summary>
        /// validate record while deletion
        /// </summary>
        /// <returns></returns>
        private bool DeleteValidation()
        {
            try
            {   //Cc
                //function for deleting validation
               if (MobjClsBLLCompanyInformation.GetCompanyExists(Convert.ToInt32(NameTextBox.Tag)))
               {
                   MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 226, out MmessageIcon);
                   MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                   lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                   TmrComapny.Enabled = true;
                   return false;
               }

               if (MobjClsBLLCompanyInformation.GetCompanyExistsInCompanyTable(Convert.ToInt32(NameTextBox.Tag)))
               {
                   MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 226, out MmessageIcon);
                   MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                   lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                   TmrComapny.Enabled = true;
                   return false;
               }

                if (MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID == 1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 230, out MmessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    return false;
                }

                //a

                for (int i = 0; i < GrdCompanyBanks.RowCount - 1; i++)
                {
                
                    //if(MobjClsBLLCompanyInformation.GetVoucherDetailsExists(GrdCompanyBanks.Rows[i].Cells["BankNameId"].Value.ToInt32()))
                   // if (MobjClsBLLCompanyInformation.GetBankBranchExistsInEmployee(GrdCompanyBanks.CurrentRow.Cells["BankNameId"].Value.ToInt32()))

                    if (MobjClsBLLCompanyInformation.GetBankBranchExistsInEmployee(GrdCompanyBanks.Rows[i].Cells["BankNameId"].Value.ToInt32()))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 226, out MmessageIcon);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        return false;
                    }
                }


                //a

                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 13, out MmessageIcon);

                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on DeleteValidation() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteValidation()" + ex.Message.ToString());
                return false;
            }
        }

        /// <summary>
        /// form validation for saving the record
        /// </summary>
        /// <returns></returns>
        private bool CompanyValidation()
        {
            try
            {
                // function for company validation
                string strComNameOld = "";
                string strComNameNew = "";
                int TCompanyID = 0;
                //string strComBranch = "";
                ErrorProviderCompany.Clear();
                lblcompanystatus.Text = "";
                DateTime dtComFinYearDate;


                if (RbtnBranch.Checked)
                {
                    TCompanyID = CboCompanyName.SelectedValue.ToInt32();
                }
                else if (MblnAddStatus==false && RbtnBranch.Checked==false)
                {
                    TCompanyID = NameTextBox.Tag.ToInt32();
                }
                dtComFinYearDate = MobjClsBLLCompanyInformation.GetFinyearDate(TCompanyID);
                //else
                //{
                //    dtComFinYearDate = MobjClsBLLCompanyInformation.GetFinyearDate(1);
                //}

                if (!RBtnCompany.Checked )
                {
                    if (CboCompanyName.SelectedIndex == -1)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 201, out MmessageIcon);
                        ErrorProviderCompany.SetError(CboCompanyName, MstrMessageCommon.Replace("#", "").Trim());
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = CompanyInfoTab;
                        CboCompanyName.Focus();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }
                }

                if (RbtnBranch.Checked )
                {
                    //strComBranch = "Branch";
                    if (NameTextBox.Text.Trim() == "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 205, out MmessageIcon);
                        ErrorProviderCompany.SetError(NameTextBox, MstrMessageCommon.Replace("#", "").Trim());
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = CompanyInfoTab;
                        NameTextBox.Focus();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }
                }
                else
                {
                    //strComBranch = "Company";

                    if (NameTextBox.Text.Trim() == "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 201, out MmessageIcon);
                        ErrorProviderCompany.SetError(CboCompanyName, MstrMessageCommon.Replace("#", "").Trim());
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = CompanyInfoTab;
                        CboCompanyName.Focus();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }

                }

                strComNameOld = NameTextBox.Text.Trim();
                strComNameNew = NameTextBox.Text.Trim();


                if (MblnAddStatus == false)
                {
                    if (RbtnBranch.Checked == true)
                    {
                        if (MobjClsBLLCompanyInformation.GetCompanyExistsInCompanyTable(Convert.ToInt32(NameTextBox.Tag)))
                        {
                            //MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 226, out MmessageIcon);
                            //MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                            //lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            //TmrComapny.Enabled = true;
                            return false;
                        }
                    }
                }

                if (MobjClsBLLCompanyInformation.CheckDuplication(MblnAddStatus, new string[] { NameTextBox.Text.Replace("'", "").Trim() }, Convert.ToInt32(NameTextBox.Tag), 1))
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 206, out MmessageIcon);
                    ErrorProviderCompany.SetError(NameTextBox, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    NameTextBox.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }

                if (TxtEmployerCode.Text.Trim() == "")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 207, out MmessageIcon);
                    ErrorProviderCompany.SetError(TxtEmployerCode, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    TxtEmployerCode.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }

                if (TxtShortName.Text.Trim() == "")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 208, out MmessageIcon);
                    ErrorProviderCompany.SetError(TxtShortName, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    TxtShortName.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }

                if (TxtPOBox.Text.Trim() == "")
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 209, out MmessageIcon);
                    ErrorProviderCompany.SetError(TxtPOBox, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    TxtPOBox.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }

                if (CboCountry.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 5, out MmessageIcon);
                    ErrorProviderCompany.SetError(CboCountry, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    CboCountry.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }

                if (CboCurrency.SelectedIndex == -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 6, out MmessageIcon);
                    ErrorProviderCompany.SetError(CboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = CompanyInfoTab;
                    CboCurrency.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }
                if (OffDayIds.Length > 12)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 241, out MmessageIcon);
                    ErrorProviderCompany.SetError(groupBox1, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = GeneralTab;
                    groupBox1.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    return false;
                }

               if (RBtnMonth.Checked == true)
                {
                    if ((NumTxtWorkingDays.Text.ToInt32() ==0) || (Convert.ToInt32(NumTxtWorkingDays.Text) > 31))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 227, out MmessageIcon);
                        ErrorProviderCompany.SetError(NumTxtWorkingDays, MstrMessageCommon.Replace("#", "").Trim());
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = CompanyInfoTab;
                        NumTxtWorkingDays.Focus();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }
                }
                else
                {
                    if (DateTime.IsLeapYear(System.DateTime.Now.Year) == true)
                    {
                        if ((Convert.ToInt32(NumTxtWorkingDays.Text) > 366) || (Convert.ToInt32(NumTxtWorkingDays.Text) < 100))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 228, out MmessageIcon);
                            ErrorProviderCompany.SetError(NumTxtWorkingDays, MstrMessageCommon.Replace("#", "").Trim());
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrComapny.Enabled = true;
                            CompanyTab.SelectedTab = CompanyInfoTab;
                            NumTxtWorkingDays.Focus();
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                            return false;
                        }
                        //else
                        //{
                        //    if ((Convert.ToInt32(NumTxtWorkingDays.Text) > 365) || (Convert.ToInt32(NumTxtWorkingDays.Text) < 100))
                        //    {
                        //        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 229, out MmessageIcon);
                        //        ErrorProviderCompany.SetError(NumTxtWorkingDays, MstrMessageCommon.Replace("#", "").Trim());
                        //        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        //        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        //        TmrComapny.Enabled = true;
                        //        CompanyTab.SelectedTab = CompanyInfoTab;
                        //        NumTxtWorkingDays.Focus();
                        //        return false;
                        //    }

                        //}

                    }
                    else
                    {
                        if ((Convert.ToInt32(NumTxtWorkingDays.Text) > 365) || (Convert.ToInt32(NumTxtWorkingDays.Text) < 100))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 229, out MmessageIcon);
                            ErrorProviderCompany.SetError(NumTxtWorkingDays, MstrMessageCommon.Replace("#", "").Trim());
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrComapny.Enabled = true;
                            CompanyTab.SelectedTab = CompanyInfoTab;
                            NumTxtWorkingDays.Focus();
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                            return false;
                        }
                    }
                }

                //if (CboProvince.SelectedIndex == -1)
                //{
                //    if (CboProvince.Text.Trim() != "")
                //    {
                //        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 211, out MmessageIcon);
                //        ErrorProviderCompany.SetError(CboProvince, MstrMessageCommon.Replace("#", "").Trim());
                //        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //        TmrComapny.Enabled = true;
                //        CompanyTab.SelectedTab = GeneralTab;
                //        CboProvince.Focus();
                //        return false;
                //    }
                //}

                //if (CboCompanyType.SelectedIndex == -1)
                //{
                //    if (CboCompanyType.Text != "")
                //    {
                //        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 212, out MmessageIcon);
                //        ErrorProviderCompany.SetError(CboCompanyType, MstrMessageCommon.Replace("#", "").Trim());
                //        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                //        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                //        TmrComapny.Enabled = true;
                //        CompanyTab.SelectedTab = CompanyInfoTab;
                //        CboCompanyType.Focus();
                //        return false;
                //    }
                //}

                if (TxtPrimaryEmail.Text != "")
                {
                    if (MobjClsBLLCompanyInformation.CheckValidEmail(Convert.ToString(TxtPrimaryEmail.Text.Trim())) == false)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 213, out MmessageIcon);
                        ErrorProviderCompany.SetError(TxtPrimaryEmail, MstrMessageCommon.Replace("#", "").Trim());
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = GeneralTab;
                        TxtPrimaryEmail.Focus();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }
                }

                if (TxtSecondaryEmail.Text != "")
                {
                    if (MobjClsBLLCompanyInformation.CheckValidEmail(Convert.ToString(TxtSecondaryEmail.Text.Trim())) == false)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 213, out MmessageIcon);
                        ErrorProviderCompany.SetError(TxtSecondaryEmail, MstrMessageCommon.Replace("#", "").Trim());
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = GeneralTab;
                        TxtSecondaryEmail.Focus();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }
                }





                if (MblnAddStatus == false)
                {
                    if (RBtnCompany.Checked)
                    {
                        if (MdteFinyearDate != DtpFinyearDate.Value.Date)
                        {

                            if (MobjClsBLLCommonUtility.FillCombos(new string[] { "LeavePolicyID", "PayLeavePolicyMaster", "" }).Rows.Count > 0)
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 215, out MmessageIcon);
                                ErrorProviderCompany.SetError(DtpFinyearDate, MstrMessageCommon.Replace("#", "").Trim());
                                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrComapny.Enabled = true;
                                CompanyTab.SelectedTab = Accdates;
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                                return false;
                            }

                            else if (MobjClsBLLCompanyInformation.IsBranchExistswithFinyear(DtpFinyearDate.Value.Date))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 236, out MmessageIcon);
                                ErrorProviderCompany.SetError(DtpFinyearDate, MstrMessageCommon.Replace("#", "").Trim());
                                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrComapny.Enabled = true;
                                CompanyTab.SelectedTab = Accdates;
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                                return false;
                            }
                        }
                        else if(MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strStartDate.ToDateTime() != dtpStartDate.Value.Date)
                        {
                            if (MobjClsBLLCompanyInformation.IsBranchExistswithStartDate(dtpStartDate.Value.Date))
                            {
                                MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 240, out MmessageIcon);
                                ErrorProviderCompany.SetError(dtpStartDate, MstrMessageCommon.Replace("#", "").Trim());
                                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                                TmrComapny.Enabled = true;
                                CompanyTab.SelectedTab = Accdates;
                                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                                return false;
                            }
                        }
                       
                    }

                    if (MobjClsBLLCompanyInformation.GetMaxDOJ(NameTextBox.Tag.ToInt32()).Date < dtpStartDate.Value.Date)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 242, out MmessageIcon);
                        ErrorProviderCompany.SetError(dtpStartDate, MstrMessageCommon.Replace("#", "").Trim());
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = Accdates;
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        return false;
                    }


                }

                

                if (Convert.ToInt32(NameTextBox.Tag) != 0 && Convert.ToInt32(CboCompanyName.SelectedValue) != 0)
                {
                    if (Convert.ToInt32(NameTextBox.Tag) == Convert.ToInt32(CboCompanyName.SelectedValue))
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 216, out MmessageIcon);
                        CboCompanyName.Focus();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }
                }

                if (RbtnBranch.Checked == true)
                {
                    DateTime dBSDate;
                    if (MobjClsBLLCompanyInformation.CheckDBSdateExists(new string[] { Convert.ToString(CboCompanyName.SelectedValue) }, out  dBSDate))
                    {
                        if (DtpBkStartDate.Value.Date < dBSDate)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 217, out MmessageIcon);
                            ErrorProviderCompany.SetError(DtpBkStartDate, MstrMessageCommon.Replace("#", "").Trim());
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrComapny.Enabled = true;
                            CompanyTab.SelectedTab = Accdates;
                            DtpBkStartDate.Focus();
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                            return false;
                        }
                    }
                    if (MobjClsBLLCompanyInformation.CheckDBFdateExists(new string[] { Convert.ToString(CboCompanyName.SelectedValue) }, out  dBSDate))
                    {
                        if (DtpFinyearDate.Value.Date < dBSDate)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 238, out MmessageIcon);
                            ErrorProviderCompany.SetError(DtpFinyearDate, MstrMessageCommon.Replace("#", "").Trim());
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrComapny.Enabled = true;
                            CompanyTab.SelectedTab = Accdates;
                            DtpFinyearDate.Focus();
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + dBSDate.ToString("dd MMM yyyy") + ")", MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                            return false;
                        }
                    }
                    if (MobjClsBLLCompanyInformation.CheckDBStartDateExists(new string[] { Convert.ToString(CboCompanyName.SelectedValue) }, out  dBSDate))
                    {
                        if (dtpStartDate.Value.Date < dBSDate)
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 239, out MmessageIcon);
                            ErrorProviderCompany.SetError(dtpStartDate, MstrMessageCommon.Replace("#", "").Trim());
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrComapny.Enabled = true;
                            CompanyTab.SelectedTab = Accdates;
                            dtpStartDate.Focus();
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " (" + dtComFinYearDate.ToString("dd MMM yyyy") + ")", MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                            return false;
                        }
                    }

                    if (dtComFinYearDate.Month != DtpFinyearDate.Value.Date.Month && TCompanyID!=0)
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 237, out MmessageIcon);
                        ErrorProviderCompany.SetError(DtpFinyearDate, MstrMessageCommon.Replace("#", "").Trim());
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        TmrComapny.Enabled = true;
                        CompanyTab.SelectedTab = Accdates;
                        DtpFinyearDate.Focus();
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim() + " ("+dtComFinYearDate.ToString("MMM")+")", MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }
                }

              

                if (DtpFinyearDate.Value.Day != 1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 219, out MmessageIcon);
                    ErrorProviderCompany.SetError(DtpFinyearDate, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = Accdates;
                    DtpFinyearDate.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }

                if (DtpBkStartDate.Value.Date < DtpFinyearDate.Value.Date)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 220, out MmessageIcon);
                    ErrorProviderCompany.SetError(DtpBkStartDate, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = Accdates;
                    DtpBkStartDate.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }

              

                // -------------------------------Company bank Account Details Validation---------------------------------------
                int iTempID = 0;
                iTempID = CheckDuplicationInGrid(GrdCompanyBanks, 2, 3);

                if (iTempID != -1)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 222, out MmessageIcon);
                    CompanyTab.SelectedTab = Accdates;
                    GrdCompanyBanks.Focus();
                    GrdCompanyBanks.CurrentCell = GrdCompanyBanks[2, iTempID];
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }

                for (int i = 0; i < GrdCompanyBanks.RowCount - 1; i++)
                {
                    int iRowIndex = GrdCompanyBanks.Rows[i].Index;

                    if (Convert.ToString(GrdCompanyBanks.Rows[i].Cells[2].Value).Trim() == "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 202, out MmessageIcon);
                        CompanyTab.SelectedTab = Accdates;
                        GrdCompanyBanks.Focus();
                        GrdCompanyBanks.CurrentCell = GrdCompanyBanks[2, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }

                    if (Convert.ToString(GrdCompanyBanks.Rows[i].Cells[3].Value).Trim() == "")
                    {
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 203, out MmessageIcon);
                        CompanyTab.SelectedTab = Accdates;
                        GrdCompanyBanks.Focus();
                        GrdCompanyBanks.CurrentCell = GrdCompanyBanks[3, iRowIndex];
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                        return false;
                    }

                    int iReturnID = 0;

                    //if (MobjClsBLLCompanyInformation.CheckAccountNoExists(MblnAddStatus, new string[] { Convert.ToString(GrdCompanyBanks.Rows[i].Cells["BankNameId"].Value), Convert.ToString(GrdCompanyBanks.Rows[i].Cells["AccountNumber"].Value).Trim() }, Convert.ToInt32(NameTextBox.Tag), out iReturnID))
                    //{
                    //    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 223, out MmessageIcon);
                    //    CompanyTab.SelectedTab = Accdates;
                    //    GrdCompanyBanks.Focus();
                    //    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    //    return false;
                    //}
                }
                if (dtpStartDate.Value.Date > DateTime.Now.Date)
                {
                    MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 235, out MmessageIcon);
                    ErrorProviderCompany.SetError(dtpStartDate, MstrMessageCommon.Replace("#", "").Trim());
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    TmrComapny.Enabled = true;
                    CompanyTab.SelectedTab = Accdates;
                    dtpStartDate.Focus();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                    return false;
                }
                if (!MblnAddStatus)
                {
                    if (CboCurrency.SelectedIndex != -1 && (CboCurrency.SelectedValue.ToInt32() != MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCurrencyId))
                    {
                        if (MobjClsBLLCompanyInformation.IsSalaryStructureExists(Convert.ToInt32(NameTextBox.Tag)))
                        {
                            MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 218, out MmessageIcon);
                            ErrorProviderCompany.SetError(CboCurrency, MstrMessageCommon.Replace("#", "").Trim());
                            lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                            TmrComapny.Enabled = true;
                            CompanyTab.SelectedTab = CompanyInfoTab;
                            CboCurrency.Focus();
                            MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);

                            return false;
                        }
                    }
                }


               
                return true;
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on CompanyValidation() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on CompanyValidation()" + ex.Message.ToString());
                return false;
            }
        }

        /// <summary>
        /// function for saving the company information
        /// </summary>
        /// <returns></returns>
        private bool SaveCompany()
        {
            // function for saving company information
            try
            {
                if (RBtnCompany.Checked == true)
                {
                    if (MblnAddStatus == true)
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 224,out MmessageIcon );
                    else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3,out MmessageIcon );
                }
                else
                {
                    if (MblnAddStatus == true)
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 225,out MmessageIcon );
                    else
                        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 3,out MmessageIcon );
                }
                
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                //if (RbtnBranch.Checked)
                //{
                //    if (MobjClsBLLCompanyInformation.FillCombos(new string[] { "1", "CurrencyDetails", "CompanyID = " + CboCompanyName.SelectedValue.ToInt32() + " And CurrencyID = " + CboCurrency.SelectedValue.ToInt32() }).Rows.Count == 0)
                //    {
                //        MstrMessageCommon = mObjNotification.GetErrorMessage(MsarMessageArr, 233, out MmessageIcon).Replace("*", CboCurrency.Text);
                //       MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), MstrMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                     
                //        using (FrmCurrency frmCurrency = new FrmCurrency())
                //       {
                //           frmCurrency.ShowDialog();
                //       }

                //        if (MobjClsBLLCompanyInformation.FillCombos(new string[] { "1", "CurrencyDetails", "CompanyID = " + CboCompanyName.SelectedValue.ToInt32() + " And CurrencyID = " + CboCurrency.SelectedValue.ToInt32() }).Rows.Count == 0)
                //        {
                //            CompanyTab.SelectedTab = CompanyInfoTab;
                //            CboCurrency.Focus();
                //            return false;
                //        }
                //    }

                   
                //}


                FillParameters();
                FillDetailParameters();

                if (MobjClsBLLCompanyInformation.SaveCompany(MblnAddStatus,lstDeletedBankIds))
                {
                    NameTextBox.Tag = MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID;

                    return true;
                }
                else
                    return false;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on Save:SaveCompany() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (MblnShowErrorMess)
                    MessageBox.Show("Error on SaveCompany() " + Ex.Message.ToString());
                return false;
            }
        }
        /// <summary>
        /// setting the company bank properties
        /// </summary>
        private void FillDetailParameters()
        {
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.lstCompanyBankDetails = new List<clsDTOCompanyBankDetails>();
            for (int i = 0; i < GrdCompanyBanks.RowCount - 1; i++)
            {
                clsDTOCompanyBankDetails objClsDTOCompanyBankDetails = new clsDTOCompanyBankDetails();
                if (string.IsNullOrEmpty(Convert.ToString(GrdCompanyBanks.Rows[i].Cells[0].Value).Trim()))
                    objClsDTOCompanyBankDetails.intCompanyBankId = 0;
                else
                    objClsDTOCompanyBankDetails.intCompanyBankId = Convert.ToInt32(GrdCompanyBanks.Rows[i].Cells[0].Value);

                objClsDTOCompanyBankDetails.intBankId = Convert.ToInt32(GrdCompanyBanks.Rows[i].Cells[2].Value);
                objClsDTOCompanyBankDetails.strAccountNumber = Convert.ToString(GrdCompanyBanks.Rows[i].Cells[3].Value).Replace("'", "’").Trim();
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.lstCompanyBankDetails.Add(objClsDTOCompanyBankDetails);
            }

        }
        /// <summary>
        /// Fill DTO properties of company class
        /// </summary>
        private void FillParameters()
        {
            if (MblnAddStatus)
            {
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID = 0;
            }
            else
            {
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyID = Convert.ToInt32(NameTextBox.Tag);
            }
            if (!RbtnBranch.Checked)
            {
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intParentID = 0;
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnCompanyBranchIndicator = false;
            }
            else
            {
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intParentID = Convert.ToInt32(CboCompanyName.SelectedValue);
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnCompanyBranchIndicator = true;
            }

            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strName = NameTextBox.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strShortName = TxtShortName.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPOBox = TxtPOBox.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strRoad = TxtStreet.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strArea = TxtArea.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBlock = TxtBlock.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strCity = TxtCity.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intProvinceID = CboProvince.SelectedValue.ToInt32();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intUnernedPolicyID = cboUnearnedPolicyID.SelectedValue.ToInt32();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCountryID = CboCountry.SelectedValue.ToInt32();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPrimaryEmail = TxtPrimaryEmail.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strSecondaryEmail = TxtSecondaryEmail.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strTelePhone = TxtContactPersonPhone.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strPABXNumber = TxtFaxNumber.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strWebSite = TxtWebSite.Text.Replace("'", "’").Trim();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyIndustryID = CboCompanyIndustry.SelectedValue.ToInt32();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCompanyTypeID =CboCompanyType.SelectedValue.ToInt32();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOtherInfo = TxtOtherInfo.Text.Replace("'", "’");
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intWorkingDaysInMonth = Convert.ToInt32(NumTxtWorkingDays.Text);
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.blnIsMonth = !(RBtnYear.Checked);
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.intCurrencyId = CboCurrency.SelectedValue.ToInt32();
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strFinYearStartDate = DtpFinyearDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strBookStartDate = DtpBkStartDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strStartDate = dtpStartDate.Value.ToString("dd-MMM-yyyy");
            MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strEPID = TxtEmployerCode.Text.Replace("'", "’").Trim();

            if (LogoFilePictureBox.Image != null)
            {
                ClsImages GetByteImage = new ClsImages();
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.byteLogoFile = GetByteImage.GetImageDataStream(LogoFilePictureBox.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            else
            {
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.byteLogoFile = null;
            }           
           // settings offdayIds the local variable contains a comma  at the beginning of the string
            if (OffDayIds != string.Empty)
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOffDayIDs = OffDayIds.Remove(0, 1);
            else
                MobjClsBLLCompanyInformation.clsDTOCompanyInformation.strOffDayIDs = string.Empty;
         

        }
        /// <summary>
        /// Seeting th ebuttom permission and change the save button enability
        /// </summary>
        private void Changestatus()
        {
            //function for changing status
            MblnIsFromOK = false;
            if (!blnIsEditMode)
            {
                BtnOk.Enabled = MblnAddPermission;
                BtnSave.Enabled = MblnAddPermission;
                CompanyMasterBindingNavigatorSaveItem.Enabled = MblnAddPermission; 
            }
            else
            {
                BtnOk.Enabled = MblnUpdatePermission;
                BtnSave.Enabled = MblnUpdatePermission;
                CompanyMasterBindingNavigatorSaveItem.Enabled = MblnUpdatePermission; 
            }
            ErrorProviderCompany.Clear();
            
        }
        /// <summary>
        /// check the duplication in bank grid
        /// </summary>
        /// <param name="Grd"></param>
        /// <param name="ColIndexfirst"></param>
        /// <param name="ColIndexsecond"></param>
        /// <returns></returns>
        private int CheckDuplicationInGrid(DataGridView Grd, int ColIndexfirst, int ColIndexsecond)
        {
            //function for checking duplication in grid
            string SearchValuefirst = "";
            string SearchValuesecond = "";
            int RowIndexTemp = 0;

            foreach (DataGridViewRow rowValue in Grd.Rows)
            {
                if (rowValue.Cells[ColIndexfirst].Value != null)
                {
                    SearchValuefirst = Convert.ToString(rowValue.Cells[ColIndexfirst].Value);
                    SearchValuesecond = Convert.ToString(rowValue.Cells[ColIndexsecond].Value);
                    RowIndexTemp = rowValue.Index;

                    foreach (DataGridViewRow row in Grd.Rows)
                    {
                        if (RowIndexTemp != row.Index)
                        {
                            if (row.Cells[ColIndexfirst].Value != null)
                            {
                                if (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() != null)
                                {
                                    if (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() != null)
                                    {
                                        if ((Convert.ToString(row.Cells[ColIndexfirst].Value).Trim() == Convert.ToString(SearchValuefirst).Trim()) && (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() == Convert.ToString(SearchValuesecond).Trim()))
                                            return row.Index;
                                    }

                                    if (Convert.ToString(row.Cells[ColIndexsecond].Value).Trim() == null)
                                    {
                                        if (Convert.ToString(row.Cells[ColIndexfirst].Value).Trim() == SearchValuefirst.Trim())
                                            return row.Index;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return -1;
        }
        public bool SetActionPermission()
        {
            clsBLLPermissionSettings ObjPermission = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                DataTable dt = ObjPermission.GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID);
                if (dt.Rows.Count == 0)
                {
                    bnInsurance.Enabled= TradingLicenseToolStripMenuItem.Enabled = LeaseAgreementToolStripMenuItem.Enabled = otherDocumentsToolStripMenuItem.Enabled = false;
                   
                    return false;
                }
                else
                {
                    dt.DefaultView.RowFilter = "MenuID=" + ((int)eMenuID.Bank).ToString();
                    BtnBank.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.TradeLicense).ToString();
                    TradingLicenseToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.LeaseAgreement).ToString();
                    LeaseAgreementToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    
                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.OtherDocuments).ToString();
                    otherDocumentsToolStripMenuItem.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Insurance).ToString();
                    bnInsurance.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);

                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Function for calling the print report
        /// </summary>
        private void LoadReport()
        {
            try
            {
                //function for loading report
                if (Convert.ToInt32(NameTextBox.Tag) > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = Convert.ToInt32(NameTextBox.Tag);
                    ObjViewer.PiFormID = (int)FormID.CompanyInformation;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                mObjLogs.WriteLog("Error on LoadReport() " + this.Name + " " + ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport()" + ex.Message.ToString());
            }
        }
        /// <summary>
        /// setting the permission variable according to role settings
        /// </summary>
        private void SetPermissions()
        {
            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.NewCompany, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
                
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

        }
        /// <summary>
        /// function for loading combos
        /// </summary>
        /// <param name="intType"></param>
        /// <returns></returns>
        private bool LoadCombos(int intType)
        {
            // function for loading combo
            // 0 - For Loading All Combo
            // 1 - For Loading CboCompanyIndustry
            // 2 - For Loading CboCompanyName
            // 3 - For Loading CboCompanyType
            // 4 - For Loading cboCountry 
            // 5 - For Loading cboCurrency
            // 6 - For Loading cboUnearnedPolicy
            // 7 - For Loading cboProvince
            // 8 - For Loading DataGridViewComboBoxColumn Bank Id

            try
            {
               
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CompanyIndustryID,CompanyIndustryArb AS CompanyIndustry", "IndustryReference", "", "CompanyIndustryID", "CompanyIndustry" });
                    else
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CompanyIndustryID,CompanyIndustry", "IndustryReference", "", "CompanyIndustryID", "CompanyIndustry" });
                    CboCompanyIndustry.ValueMember = "CompanyIndustryID";
                    CboCompanyIndustry.DisplayMember = "CompanyIndustry";
                    CboCompanyIndustry.DataSource = datCombos;
                }
                if (intType == 0 || intType == 2)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "IsBranch=0", "CompanyID", "CompanyName" });
                    CboCompanyName.ValueMember = "CompanyID";
                    CboCompanyName.DisplayMember = "CompanyName";
                    CboCompanyName.DataSource = datCombos;
                }

                if (intType == 0 || intType == 3)
                {
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CompanyTypeID,CompanyTypeArb AS CompanyType", "CompanyTypeReference", "", "CompanyTypeID", "CompanyType" });
                    else
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CompanyTypeID,CompanyType", "CompanyTypeReference", "", "CompanyTypeID", "CompanyType" });
                    CboCompanyType.ValueMember = "CompanyTypeID";
                    CboCompanyType.DisplayMember = "CompanyType";
                    CboCompanyType.DataSource = datCombos;
                }
                if (intType == 0 || intType == 4)
                {
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CountryID,CountryNameArb AS CountryName", "CountryReference", "" });
                    else
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "" });
                    CboCountry.ValueMember = "CountryID";
                    CboCountry.DisplayMember = "CountryName";
                    CboCountry.DataSource = datCombos;
                }
                if (intType == 0 || intType == 5)
                {
                    datCombos = null;
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CurrencyID,CurrencyNameArb AS CurrencyName", "CurrencyReference", "", "CurrencyID", "CurrencyName" });
                    else
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "CurrencyID,CurrencyName", "CurrencyReference", "", "CurrencyID", "CurrencyName" });
                    CboCurrency.ValueMember = "CurrencyID";
                    CboCurrency.DisplayMember = "CurrencyName";
                    CboCurrency.DataSource = datCombos;
                }

                if (intType == 0 || intType == 6)
                {
                    datCombos = null;
                    datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "UnearnedPolicyID,UnearnedPolicy", "PayUnearnedAmtPolicyMaster", "", "UnearnedPolicyID", "UnearnedPolicy" });
                    cboUnearnedPolicyID.ValueMember = "UnearnedPolicyID";
                    cboUnearnedPolicyID.DisplayMember = "UnearnedPolicy";
                    cboUnearnedPolicyID.DataSource = datCombos;
                }
                if (intType == 0 || intType == 7)
                {
                    datCombos = null;
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "ProvinceID,ProvinceNameArb AS ProvinceName", "ProvinceReference", "", "ProvinceID", "ProvinceName" });
                    else
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "ProvinceID,ProvinceName", "ProvinceReference", "", "ProvinceID", "ProvinceName" });
                    CboProvince.ValueMember = "ProvinceID";
                    CboProvince.DisplayMember = "ProvinceName";
                    CboProvince.DataSource = datCombos;
                }
                if (intType == 0 || intType == 8)
                {
                    datCombos = null;
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "BB.BankBranchID,(BB.BankBranchName + '-' +  BN.BankNameArb) As Description", "BankBranchReference BB LEFT JOIN BankReference BN on BN.BankID=BB.BankID", "", "BankBranchID", "Description" });
                    else
                        datCombos = MobjClsBLLCompanyInformation.FillCombos(new string[] { "BB.BankBranchID,(BB.BankBranchName + '-' +  BN.BankName) As Description", "BankBranchReference BB LEFT JOIN BankReference BN on BN.BankID=BB.BankID", "", "BankBranchID", "Description" });
                    BankNameId.ValueMember = "BankBranchID";
                    BankNameId.DisplayMember = "Description";
                    BankNameId.DataSource = datCombos;
                }

                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }
        /// <summary>
        /// setting the binding navigator enability
        /// </summary>
        private void SetBindingNavigatorButtons()
        {
           BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;
            BindingNavigatorMovePreviousItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
            bnMoreActions.Enabled = !MblnAddStatus;
        }


#endregion

        private void otherDocumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new frmDocumentMasterNew() { PintCompanyID = NameTextBox.Tag.ToInt32(), eOperationType = OperationType.Company }.ShowDialog();
            }

            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on form nationalIDCardToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

            }
        }

        private void TradingLicenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new frmCompanyTradeLicense() { CompanyID = NameTextBox.Tag.ToInt32() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on form nationalIDCardToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

            }
        }

        private void LeaseAgreementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new frmCompanyLeaseAgreement() { CompanyID = NameTextBox.Tag.ToInt32() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on form nationalIDCardToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

            }
        }

        private void bnInsurance_Click(object sender, EventArgs e)
        {
            try
            {
                new InsuranceDetails() { CompanyID = NameTextBox.Tag.ToInt32() }.ShowDialog();
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Error on form nationalIDCardToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

            }
        }

        private void CboCompanyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            Changestatus();
            CboCurrency.SelectedValue = MobjClsBLLCompanyInformation.GetCompanyCurrency(CboCompanyName.SelectedValue.ToInt32());
        }


    }
}
