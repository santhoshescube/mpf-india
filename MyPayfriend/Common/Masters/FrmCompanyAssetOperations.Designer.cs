﻿namespace MyPayfriend
{
    partial class FrmCompanyAssetOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCompanyAssetOperations));
            this.dgvEmployee = new System.Windows.Forms.DataGridView();
            this.CompanyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkStatusID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JoiningDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMain = new DevComponents.DotNetBar.PanelEx();
            this.dgvEmployeeAsset = new System.Windows.Forms.DataGridView();
            this.EmpBenefitID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AssetEmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BenefitTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyAssetID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AssetType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Asset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WithEffectDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpectedReturnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AssetStatusID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AssetRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Return = new DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn();
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatusShow = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.expnlTop = new DevComponents.DotNetBar.ExpandablePanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblJoiningDate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblEmployeeSelected = new System.Windows.Forms.Label();
            this.txtAssetInfo = new System.Windows.Forms.RichTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.dtpReturnDate = new System.Windows.Forms.DateTimePicker();
            this.Label6 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.RichTextBox();
            this.dtpExpectedReturnDate = new System.Windows.Forms.DateTimePicker();
            this.lblPeriodFrom = new System.Windows.Forms.Label();
            this.lblBenefitType = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dtpWithEffectFrom = new System.Windows.Forms.DateTimePicker();
            this.Label3 = new System.Windows.Forms.Label();
            this.cboAssetType = new System.Windows.Forms.ComboBox();
            this.dgvOtherInfoDetails = new System.Windows.Forms.DataGridView();
            this.AssetUsageTypeID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Date = new DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboAssets = new System.Windows.Forms.ComboBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.lblCompanyAssets = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnAddNew = new System.Windows.Forms.ToolStripButton();
            this.btnSaveItem = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnAddAssets = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOtherInfoType = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnEmail = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnMaintenance = new System.Windows.Forms.ToolStripButton();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.expSpltrLeft = new DevComponents.DotNetBar.ExpandableSplitter();
            this.pnlLeft = new DevComponents.DotNetBar.PanelEx();
            this.expnlSearch = new DevComponents.DotNetBar.ExpandablePanel();
            this.btnSearchClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cboFilterCompany = new System.Windows.Forms.ComboBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.tmrClear = new System.Windows.Forms.Timer(this.components);
            this.errValidate = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).BeginInit();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeAsset)).BeginInit();
            this.ssStatus.SuspendLayout();
            this.expnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOtherInfoDetails)).BeginInit();
            this.ToolStrip1.SuspendLayout();
            this.pnlLeft.SuspendLayout();
            this.expnlSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvEmployee
            // 
            this.dgvEmployee.AllowUserToAddRows = false;
            this.dgvEmployee.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CompanyID,
            this.EmployeeID,
            this.EmployeeName,
            this.WorkStatusID,
            this.JoiningDate});
            this.dgvEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEmployee.Location = new System.Drawing.Point(0, 116);
            this.dgvEmployee.MultiSelect = false;
            this.dgvEmployee.Name = "dgvEmployee";
            this.dgvEmployee.ReadOnly = true;
            this.dgvEmployee.RowHeadersVisible = false;
            this.dgvEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmployee.Size = new System.Drawing.Size(235, 348);
            this.dgvEmployee.TabIndex = 1;
            this.dgvEmployee.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployee_CellClick);
            this.dgvEmployee.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvEmployee_DataError);
            // 
            // CompanyID
            // 
            this.CompanyID.DataPropertyName = "CompanyID";
            this.CompanyID.HeaderText = "CompanyID";
            this.CompanyID.Name = "CompanyID";
            this.CompanyID.ReadOnly = true;
            this.CompanyID.Visible = false;
            // 
            // EmployeeID
            // 
            this.EmployeeID.DataPropertyName = "EmployeeID";
            this.EmployeeID.HeaderText = "EmployeeID";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.ReadOnly = true;
            this.EmployeeID.Visible = false;
            // 
            // EmployeeName
            // 
            this.EmployeeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EmployeeName.DataPropertyName = "EmployeeName";
            this.EmployeeName.HeaderText = "Employee";
            this.EmployeeName.Name = "EmployeeName";
            this.EmployeeName.ReadOnly = true;
            // 
            // WorkStatusID
            // 
            this.WorkStatusID.DataPropertyName = "WorkStatusID";
            this.WorkStatusID.HeaderText = "WorkStatusID";
            this.WorkStatusID.Name = "WorkStatusID";
            this.WorkStatusID.ReadOnly = true;
            this.WorkStatusID.Visible = false;
            // 
            // JoiningDate
            // 
            this.JoiningDate.DataPropertyName = "JoiningDate";
            this.JoiningDate.HeaderText = "JoiningDate";
            this.JoiningDate.Name = "JoiningDate";
            this.JoiningDate.ReadOnly = true;
            this.JoiningDate.Visible = false;
            // 
            // pnlMain
            // 
            this.pnlMain.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlMain.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.pnlMain.Controls.Add(this.dgvEmployeeAsset);
            this.pnlMain.Controls.Add(this.ssStatus);
            this.pnlMain.Controls.Add(this.expnlTop);
            this.pnlMain.Controls.Add(this.ToolStrip1);
            this.pnlMain.Controls.Add(this.expSpltrLeft);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(235, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(949, 464);
            this.pnlMain.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlMain.Style.BackColor1.Color = System.Drawing.Color.White;
            this.pnlMain.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnlMain.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlMain.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlMain.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlMain.Style.GradientAngle = 90;
            this.pnlMain.TabIndex = 1;
            // 
            // dgvEmployeeAsset
            // 
            this.dgvEmployeeAsset.AllowUserToAddRows = false;
            this.dgvEmployeeAsset.AllowUserToDeleteRows = false;
            this.dgvEmployeeAsset.AllowUserToResizeColumns = false;
            this.dgvEmployeeAsset.AllowUserToResizeRows = false;
            this.dgvEmployeeAsset.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmployeeAsset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployeeAsset.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmpBenefitID,
            this.ParentID,
            this.AssetEmployeeID,
            this.BenefitTypeID,
            this.CompanyAssetID,
            this.AssetType,
            this.Asset,
            this.WithEffectDate,
            this.ExpectedReturnDate,
            this.ReturnDate,
            this.AssetStatusID,
            this.AssetRemarks,
            this.Return});
            this.dgvEmployeeAsset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEmployeeAsset.Location = new System.Drawing.Point(4, 294);
            this.dgvEmployeeAsset.Name = "dgvEmployeeAsset";
            this.dgvEmployeeAsset.ReadOnly = true;
            this.dgvEmployeeAsset.RowHeadersVisible = false;
            this.dgvEmployeeAsset.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmployeeAsset.Size = new System.Drawing.Size(945, 148);
            this.dgvEmployeeAsset.TabIndex = 0;
            this.dgvEmployeeAsset.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployeeAsset_CellClick);
            // 
            // EmpBenefitID
            // 
            this.EmpBenefitID.DataPropertyName = "EmpBenefitID";
            this.EmpBenefitID.HeaderText = "EmpBenefitID";
            this.EmpBenefitID.Name = "EmpBenefitID";
            this.EmpBenefitID.ReadOnly = true;
            this.EmpBenefitID.Visible = false;
            // 
            // ParentID
            // 
            this.ParentID.DataPropertyName = "ParentID";
            this.ParentID.HeaderText = "ParentID";
            this.ParentID.Name = "ParentID";
            this.ParentID.ReadOnly = true;
            this.ParentID.Visible = false;
            // 
            // AssetEmployeeID
            // 
            this.AssetEmployeeID.DataPropertyName = "AssetEmployeeID";
            this.AssetEmployeeID.HeaderText = "EmployeeID";
            this.AssetEmployeeID.Name = "AssetEmployeeID";
            this.AssetEmployeeID.ReadOnly = true;
            this.AssetEmployeeID.Visible = false;
            // 
            // BenefitTypeID
            // 
            this.BenefitTypeID.DataPropertyName = "BenefitTypeID";
            this.BenefitTypeID.HeaderText = "BenefitTypeID";
            this.BenefitTypeID.Name = "BenefitTypeID";
            this.BenefitTypeID.ReadOnly = true;
            this.BenefitTypeID.Visible = false;
            // 
            // CompanyAssetID
            // 
            this.CompanyAssetID.DataPropertyName = "CompanyAssetID";
            this.CompanyAssetID.HeaderText = "CompanyAssetID";
            this.CompanyAssetID.Name = "CompanyAssetID";
            this.CompanyAssetID.ReadOnly = true;
            this.CompanyAssetID.Visible = false;
            // 
            // AssetType
            // 
            this.AssetType.DataPropertyName = "AssetType";
            this.AssetType.HeaderText = "Asset Type";
            this.AssetType.Name = "AssetType";
            this.AssetType.ReadOnly = true;
            // 
            // Asset
            // 
            this.Asset.DataPropertyName = "Asset";
            this.Asset.HeaderText = "Asset";
            this.Asset.Name = "Asset";
            this.Asset.ReadOnly = true;
            this.Asset.Width = 120;
            // 
            // WithEffectDate
            // 
            this.WithEffectDate.DataPropertyName = "WithEffectDate";
            this.WithEffectDate.HeaderText = "With Effect Date";
            this.WithEffectDate.Name = "WithEffectDate";
            this.WithEffectDate.ReadOnly = true;
            this.WithEffectDate.Width = 130;
            // 
            // ExpectedReturnDate
            // 
            this.ExpectedReturnDate.DataPropertyName = "ExpectedReturnDate";
            this.ExpectedReturnDate.HeaderText = "Exp. Return Date";
            this.ExpectedReturnDate.Name = "ExpectedReturnDate";
            this.ExpectedReturnDate.ReadOnly = true;
            this.ExpectedReturnDate.Width = 140;
            // 
            // ReturnDate
            // 
            this.ReturnDate.DataPropertyName = "ReturnDate";
            this.ReturnDate.HeaderText = "Return Date";
            this.ReturnDate.Name = "ReturnDate";
            this.ReturnDate.ReadOnly = true;
            this.ReturnDate.Width = 130;
            // 
            // AssetStatusID
            // 
            this.AssetStatusID.DataPropertyName = "AssetStatusID";
            this.AssetStatusID.HeaderText = "AssetStatusID";
            this.AssetStatusID.Name = "AssetStatusID";
            this.AssetStatusID.ReadOnly = true;
            this.AssetStatusID.Visible = false;
            // 
            // AssetRemarks
            // 
            this.AssetRemarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AssetRemarks.DataPropertyName = "AssetRemarks";
            this.AssetRemarks.HeaderText = "Remarks";
            this.AssetRemarks.Name = "AssetRemarks";
            this.AssetRemarks.ReadOnly = true;
            // 
            // Return
            // 
            this.Return.DataPropertyName = "Return";
            this.Return.HeaderText = "";
            this.Return.Image = ((System.Drawing.Image)(resources.GetObject("Return.Image")));
            this.Return.MinimumWidth = 50;
            this.Return.Name = "Return";
            this.Return.ReadOnly = true;
            this.Return.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Return.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Return.Text = null;
            this.Return.ToolTipText = "Return";
            this.Return.Width = 50;
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusShow,
            this.lblStatus});
            this.ssStatus.Location = new System.Drawing.Point(4, 442);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(945, 22);
            this.ssStatus.TabIndex = 3;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblStatusShow
            // 
            this.lblStatusShow.Name = "lblStatusShow";
            this.lblStatusShow.Size = new System.Drawing.Size(48, 17);
            this.lblStatusShow.Text = "Status : ";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // expnlTop
            // 
            this.expnlTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.expnlTop.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expnlTop.Controls.Add(this.label9);
            this.expnlTop.Controls.Add(this.label8);
            this.expnlTop.Controls.Add(this.label4);
            this.expnlTop.Controls.Add(this.label2);
            this.expnlTop.Controls.Add(this.lblJoiningDate);
            this.expnlTop.Controls.Add(this.label1);
            this.expnlTop.Controls.Add(this.lblEmployeeSelected);
            this.expnlTop.Controls.Add(this.txtAssetInfo);
            this.expnlTop.Controls.Add(this.label19);
            this.expnlTop.Controls.Add(this.btnSave);
            this.expnlTop.Controls.Add(this.label18);
            this.expnlTop.Controls.Add(this.lblEmployee);
            this.expnlTop.Controls.Add(this.dtpReturnDate);
            this.expnlTop.Controls.Add(this.Label6);
            this.expnlTop.Controls.Add(this.txtRemarks);
            this.expnlTop.Controls.Add(this.dtpExpectedReturnDate);
            this.expnlTop.Controls.Add(this.lblPeriodFrom);
            this.expnlTop.Controls.Add(this.lblBenefitType);
            this.expnlTop.Controls.Add(this.lblEndDate);
            this.expnlTop.Controls.Add(this.dtpWithEffectFrom);
            this.expnlTop.Controls.Add(this.Label3);
            this.expnlTop.Controls.Add(this.cboAssetType);
            this.expnlTop.Controls.Add(this.dgvOtherInfoDetails);
            this.expnlTop.Controls.Add(this.cboAssets);
            this.expnlTop.Controls.Add(this.Label10);
            this.expnlTop.Controls.Add(this.lblCompanyAssets);
            this.expnlTop.Controls.Add(this.shapeContainer2);
            this.expnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.expnlTop.Location = new System.Drawing.Point(4, 25);
            this.expnlTop.Name = "expnlTop";
            this.expnlTop.Size = new System.Drawing.Size(945, 269);
            this.expnlTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlTop.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlTop.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlTop.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expnlTop.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expnlTop.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlTop.Style.GradientAngle = 90;
            this.expnlTop.TabIndex = 0;
            this.expnlTop.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlTop.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlTop.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlTop.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expnlTop.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlTop.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expnlTop.TitleStyle.GradientAngle = 90;
            this.expnlTop.TitleText = "Asset Transaction";
            this.expnlTop.Click += new System.EventHandler(this.expnlTop_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(55, 245);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Returned Asset";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Firebrick;
            this.label8.Location = new System.Drawing.Point(19, 247);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 10);
            this.label8.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(111, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(111, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = ":";
            // 
            // lblJoiningDate
            // 
            this.lblJoiningDate.AutoSize = true;
            this.lblJoiningDate.Location = new System.Drawing.Point(127, 116);
            this.lblJoiningDate.Name = "lblJoiningDate";
            this.lblJoiningDate.Size = new System.Drawing.Size(66, 13);
            this.lblJoiningDate.TabIndex = 19;
            this.lblJoiningDate.Text = "Joining Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Joining Date";
            // 
            // lblEmployeeSelected
            // 
            this.lblEmployeeSelected.AutoSize = true;
            this.lblEmployeeSelected.Location = new System.Drawing.Point(127, 89);
            this.lblEmployeeSelected.Name = "lblEmployeeSelected";
            this.lblEmployeeSelected.Size = new System.Drawing.Size(53, 13);
            this.lblEmployeeSelected.TabIndex = 16;
            this.lblEmployeeSelected.Text = "Employee";
            // 
            // txtAssetInfo
            // 
            this.txtAssetInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAssetInfo.BackColor = System.Drawing.Color.White;
            this.txtAssetInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAssetInfo.Location = new System.Drawing.Point(509, 59);
            this.txtAssetInfo.MaxLength = 5000;
            this.txtAssetInfo.Name = "txtAssetInfo";
            this.txtAssetInfo.ReadOnly = true;
            this.txtAssetInfo.Size = new System.Drawing.Size(426, 50);
            this.txtAssetInfo.TabIndex = 12;
            this.txtAssetInfo.Text = "";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(11, 62);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(87, 13);
            this.label19.TabIndex = 13;
            this.label19.Text = "Employee Info";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(366, 240);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(499, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 13);
            this.label18.TabIndex = 11;
            this.label18.Text = "Asset Info";
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(19, 89);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(53, 13);
            this.lblEmployee.TabIndex = 14;
            this.lblEmployee.Text = "Employee";
            // 
            // dtpReturnDate
            // 
            this.dtpReturnDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpReturnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpReturnDate.Location = new System.Drawing.Point(335, 142);
            this.dtpReturnDate.Name = "dtpReturnDate";
            this.dtpReturnDate.Size = new System.Drawing.Size(106, 20);
            this.dtpReturnDate.TabIndex = 4;
            this.dtpReturnDate.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(243, 146);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(65, 13);
            this.Label6.TabIndex = 22;
            this.Label6.Text = "Return Date";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(114, 165);
            this.txtRemarks.MaxLength = 500;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(327, 70);
            this.txtRemarks.TabIndex = 5;
            this.txtRemarks.Text = "";
            this.txtRemarks.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // dtpExpectedReturnDate
            // 
            this.dtpExpectedReturnDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpExpectedReturnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpectedReturnDate.Location = new System.Drawing.Point(114, 139);
            this.dtpExpectedReturnDate.Name = "dtpExpectedReturnDate";
            this.dtpExpectedReturnDate.ShowCheckBox = true;
            this.dtpExpectedReturnDate.Size = new System.Drawing.Size(120, 20);
            this.dtpExpectedReturnDate.TabIndex = 3;
            this.dtpExpectedReturnDate.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // lblPeriodFrom
            // 
            this.lblPeriodFrom.AutoSize = true;
            this.lblPeriodFrom.Location = new System.Drawing.Point(243, 119);
            this.lblPeriodFrom.Name = "lblPeriodFrom";
            this.lblPeriodFrom.Size = new System.Drawing.Size(86, 13);
            this.lblPeriodFrom.TabIndex = 20;
            this.lblPeriodFrom.Text = "With Effect Date";
            // 
            // lblBenefitType
            // 
            this.lblBenefitType.AutoSize = true;
            this.lblBenefitType.Location = new System.Drawing.Point(19, 35);
            this.lblBenefitType.Name = "lblBenefitType";
            this.lblBenefitType.Size = new System.Drawing.Size(60, 13);
            this.lblBenefitType.TabIndex = 9;
            this.lblBenefitType.Text = "Asset Type";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(19, 143);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(89, 13);
            this.lblEndDate.TabIndex = 21;
            this.lblEndDate.Text = "Exp. Return Date";
            // 
            // dtpWithEffectFrom
            // 
            this.dtpWithEffectFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpWithEffectFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpWithEffectFrom.Location = new System.Drawing.Point(335, 116);
            this.dtpWithEffectFrom.Name = "dtpWithEffectFrom";
            this.dtpWithEffectFrom.Size = new System.Drawing.Size(106, 20);
            this.dtpWithEffectFrom.TabIndex = 2;
            this.dtpWithEffectFrom.ValueChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(19, 168);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(49, 13);
            this.Label3.TabIndex = 23;
            this.Label3.Text = "Remarks";
            // 
            // cboAssetType
            // 
            this.cboAssetType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAssetType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAssetType.BackColor = System.Drawing.SystemColors.Info;
            this.cboAssetType.DropDownHeight = 134;
            this.cboAssetType.FormattingEnabled = true;
            this.cboAssetType.IntegralHeight = false;
            this.cboAssetType.Location = new System.Drawing.Point(114, 31);
            this.cboAssetType.Name = "cboAssetType";
            this.cboAssetType.Size = new System.Drawing.Size(150, 21);
            this.cboAssetType.TabIndex = 0;
            this.cboAssetType.SelectedIndexChanged += new System.EventHandler(this.cboAssetType_SelectedIndexChanged);
            this.cboAssetType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboAssetType.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // dgvOtherInfoDetails
            // 
            this.dgvOtherInfoDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOtherInfoDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvOtherInfoDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOtherInfoDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AssetUsageTypeID,
            this.Date,
            this.Amount,
            this.Remarks});
            this.dgvOtherInfoDetails.Location = new System.Drawing.Point(467, 139);
            this.dgvOtherInfoDetails.Name = "dgvOtherInfoDetails";
            this.dgvOtherInfoDetails.Size = new System.Drawing.Size(468, 124);
            this.dgvOtherInfoDetails.TabIndex = 7;
            this.dgvOtherInfoDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvOtherInfoDetails_EditingControlShowing);
            // 
            // AssetUsageTypeID
            // 
            this.AssetUsageTypeID.DataPropertyName = "AssetUsageTypeID";
            this.AssetUsageTypeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AssetUsageTypeID.HeaderText = "Maintenance Type";
            this.AssetUsageTypeID.MinimumWidth = 150;
            this.AssetUsageTypeID.Name = "AssetUsageTypeID";
            this.AssetUsageTypeID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AssetUsageTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.AssetUsageTypeID.Width = 150;
            // 
            // Date
            // 
            // 
            // 
            // 
            this.Date.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.Date.BackgroundStyle.Class = "DataGridViewDateTimeBorder";
            this.Date.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Date.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText;
            this.Date.CustomFormat = "dd MMM yyyy";
            this.Date.DataPropertyName = "Date";
            this.Date.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.Date.HeaderText = "Date";
            this.Date.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            this.Date.MinimumWidth = 80;
            // 
            // 
            // 
            this.Date.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.Date.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.Date.MonthCalendar.BackgroundStyle.Class = "";
            this.Date.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.Date.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.Date.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Date.MonthCalendar.DisplayMonth = new System.DateTime(2013, 8, 1, 0, 0, 0, 0);
            this.Date.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.Date.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.Date.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.Date.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Date.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.Date.Name = "Date";
            this.Date.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Date.Width = 80;
            // 
            // Amount
            // 
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            // 
            // Remarks
            // 
            this.Remarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remarks.DataPropertyName = "Remarks";
            this.Remarks.HeaderText = "Remarks";
            this.Remarks.MaxInputLength = 500;
            this.Remarks.Name = "Remarks";
            // 
            // cboAssets
            // 
            this.cboAssets.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAssets.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAssets.BackColor = System.Drawing.SystemColors.Info;
            this.cboAssets.DropDownHeight = 134;
            this.cboAssets.FormattingEnabled = true;
            this.cboAssets.IntegralHeight = false;
            this.cboAssets.Location = new System.Drawing.Point(325, 31);
            this.cboAssets.Name = "cboAssets";
            this.cboAssets.Size = new System.Drawing.Size(150, 21);
            this.cboAssets.TabIndex = 1;
            this.cboAssets.SelectedIndexChanged += new System.EventHandler(this.cboAssets_SelectedIndexChanged);
            this.cboAssets.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            this.cboAssets.TextChanged += new System.EventHandler(this.ChangeControlStatus);
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.Location = new System.Drawing.Point(464, 119);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(106, 13);
            this.Label10.TabIndex = 26;
            this.Label10.Text = "Maintenance Info";
            // 
            // lblCompanyAssets
            // 
            this.lblCompanyAssets.AutoSize = true;
            this.lblCompanyAssets.Location = new System.Drawing.Point(277, 35);
            this.lblCompanyAssets.Name = "lblCompanyAssets";
            this.lblCompanyAssets.Size = new System.Drawing.Size(38, 13);
            this.lblCompanyAssets.TabIndex = 10;
            this.lblCompanyAssets.Text = "Assets";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 26);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape6,
            this.lineShape5,
            this.lineShape4});
            this.shapeContainer2.Size = new System.Drawing.Size(945, 243);
            this.shapeContainer2.TabIndex = 8;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape6
            // 
            this.lineShape6.BorderColor = System.Drawing.Color.Silver;
            this.lineShape6.Name = "lineShape6";
            this.lineShape6.X1 = 87;
            this.lineShape6.X2 = 472;
            this.lineShape6.Y1 = 44;
            this.lineShape6.Y2 = 44;
            // 
            // lineShape5
            // 
            this.lineShape5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape5.BorderColor = System.Drawing.Color.Silver;
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 550;
            this.lineShape5.X2 = 935;
            this.lineShape5.Y1 = 98;
            this.lineShape5.Y2 = 98;
            // 
            // lineShape4
            // 
            this.lineShape4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape4.BorderColor = System.Drawing.Color.Silver;
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 550;
            this.lineShape4.X2 = 935;
            this.lineShape4.Y1 = 17;
            this.lineShape4.Y2 = 17;
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddNew,
            this.btnSaveItem,
            this.btnDelete,
            this.btnClear,
            this.ToolStripSeparator3,
            this.bnMoreActions,
            this.ToolStripSeparator1,
            this.btnPrint,
            this.btnEmail,
            this.ToolStripSeparator2,
            this.btnMaintenance,
            this.btnHelp});
            this.ToolStrip1.Location = new System.Drawing.Point(4, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(945, 25);
            this.ToolStrip1.TabIndex = 1;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // btnAddNew
            // 
            this.btnAddNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddNew.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNew.Image")));
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.RightToLeftAutoMirrorImage = true;
            this.btnAddNew.Size = new System.Drawing.Size(23, 22);
            this.btnAddNew.Text = "Add new";
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnSaveItem
            // 
            this.btnSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveItem.Image")));
            this.btnSaveItem.Name = "btnSaveItem";
            this.btnSaveItem.Size = new System.Drawing.Size(23, 22);
            this.btnSaveItem.Text = "Save Data";
            this.btnSaveItem.Click += new System.EventHandler(this.btnSaveItem_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::MyPayfriend.Properties.Resources.Delete;
            this.btnDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnClear.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "ToolStripButton6";
            this.btnClear.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddAssets,
            this.btnOtherInfoType});
            this.bnMoreActions.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.bnMoreActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "Actions";
            this.bnMoreActions.ToolTipText = "Actions";
            // 
            // btnAddAssets
            // 
            this.btnAddAssets.Image = global::MyPayfriend.Properties.Resources.AddAssect;
            this.btnAddAssets.Name = "btnAddAssets";
            this.btnAddAssets.Size = new System.Drawing.Size(157, 22);
            this.btnAddAssets.Text = "Assets";
            this.btnAddAssets.Click += new System.EventHandler(this.btnAddAssets_Click);
            // 
            // btnOtherInfoType
            // 
            this.btnOtherInfoType.Image = ((System.Drawing.Image)(resources.GetObject("btnOtherInfoType.Image")));
            this.btnOtherInfoType.Name = "btnOtherInfoType";
            this.btnOtherInfoType.Size = new System.Drawing.Size(157, 22);
            this.btnOtherInfoType.Text = "Other Info Type";
            this.btnOtherInfoType.Click += new System.EventHandler(this.btnOtherInfoType_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = global::MyPayfriend.Properties.Resources.Print;
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "Print";
            this.btnPrint.ToolTipText = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEmail.Image = global::MyPayfriend.Properties.Resources.SendMail;
            this.btnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(23, 22);
            this.btnEmail.Text = "Email";
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnMaintenance
            // 
            this.btnMaintenance.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMaintenance.Image = global::MyPayfriend.Properties.Resources.Module;
            this.btnMaintenance.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMaintenance.Name = "btnMaintenance";
            this.btnMaintenance.Size = new System.Drawing.Size(23, 22);
            this.btnMaintenance.Text = "Maintenance Type";
            this.btnMaintenance.Click += new System.EventHandler(this.btnMaintenance_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(23, 22);
            this.btnHelp.Text = "He&lp";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // expSpltrLeft
            // 
            this.expSpltrLeft.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expSpltrLeft.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSpltrLeft.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expSpltrLeft.ExpandableControl = this.pnlLeft;
            this.expSpltrLeft.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expSpltrLeft.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSpltrLeft.ExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSpltrLeft.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSpltrLeft.GripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSpltrLeft.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSpltrLeft.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expSpltrLeft.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expSpltrLeft.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(244)))), ((int)(((byte)(252)))));
            this.expSpltrLeft.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(179)))), ((int)(((byte)(219)))));
            this.expSpltrLeft.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expSpltrLeft.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expSpltrLeft.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expSpltrLeft.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSpltrLeft.HotExpandLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.expSpltrLeft.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expSpltrLeft.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(110)))), ((int)(((byte)(121)))));
            this.expSpltrLeft.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expSpltrLeft.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(254)))), ((int)(((byte)(255)))));
            this.expSpltrLeft.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expSpltrLeft.Location = new System.Drawing.Point(0, 0);
            this.expSpltrLeft.Name = "expSpltrLeft";
            this.expSpltrLeft.Size = new System.Drawing.Size(4, 464);
            this.expSpltrLeft.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007;
            this.expSpltrLeft.TabIndex = 1;
            this.expSpltrLeft.TabStop = false;
            // 
            // pnlLeft
            // 
            this.pnlLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnlLeft.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnlLeft.Controls.Add(this.dgvEmployee);
            this.pnlLeft.Controls.Add(this.expnlSearch);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(235, 464);
            this.pnlLeft.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnlLeft.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnlLeft.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnlLeft.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnlLeft.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnlLeft.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnlLeft.Style.GradientAngle = 90;
            this.pnlLeft.TabIndex = 2;
            // 
            // expnlSearch
            // 
            this.expnlSearch.CanvasColor = System.Drawing.SystemColors.Control;
            this.expnlSearch.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expnlSearch.Controls.Add(this.btnSearchClear);
            this.expnlSearch.Controls.Add(this.btnSearch);
            this.expnlSearch.Controls.Add(this.txtSearch);
            this.expnlSearch.Controls.Add(this.cboFilterCompany);
            this.expnlSearch.Controls.Add(this.lblCompany);
            this.expnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.expnlSearch.Location = new System.Drawing.Point(0, 0);
            this.expnlSearch.Name = "expnlSearch";
            this.expnlSearch.Size = new System.Drawing.Size(235, 116);
            this.expnlSearch.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlSearch.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlSearch.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlSearch.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expnlSearch.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expnlSearch.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expnlSearch.Style.GradientAngle = 90;
            this.expnlSearch.TabIndex = 0;
            this.expnlSearch.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expnlSearch.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expnlSearch.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expnlSearch.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expnlSearch.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expnlSearch.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expnlSearch.TitleStyle.GradientAngle = 90;
            this.expnlSearch.TitleText = "Employee Search";
            // 
            // btnSearchClear
            // 
            this.btnSearchClear.Image = global::MyPayfriend.Properties.Resources.Clear;
            this.btnSearchClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearchClear.Location = new System.Drawing.Point(91, 86);
            this.btnSearchClear.Name = "btnSearchClear";
            this.btnSearchClear.Size = new System.Drawing.Size(60, 25);
            this.btnSearchClear.TabIndex = 3;
            this.btnSearchClear.Text = "Clear";
            this.btnSearchClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearchClear.UseVisualStyleBackColor = true;
            this.btnSearchClear.Click += new System.EventHandler(this.btnSearchClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(157, 86);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(70, 25);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(11, 60);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(216, 20);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            // 
            // cboFilterCompany
            // 
            this.cboFilterCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFilterCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFilterCompany.FormattingEnabled = true;
            this.cboFilterCompany.Location = new System.Drawing.Point(77, 33);
            this.cboFilterCompany.Name = "cboFilterCompany";
            this.cboFilterCompany.Size = new System.Drawing.Size(150, 21);
            this.cboFilterCompany.TabIndex = 0;
            this.cboFilterCompany.SelectionChangeCommitted += new System.EventHandler(this.cboFilterCompany_SelectionChangeCommitted);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Location = new System.Drawing.Point(8, 35);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(51, 13);
            this.lblCompany.TabIndex = 4;
            this.lblCompany.Text = "Company";
            // 
            // tmrClear
            // 
            this.tmrClear.Interval = 2000;
            // 
            // errValidate
            // 
            this.errValidate.ContainerControl = this;
            this.errValidate.RightToLeft = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "EmployeeID";
            this.dataGridViewTextBoxColumn1.HeaderText = "EmployeeID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "EmployeeName";
            this.dataGridViewTextBoxColumn2.HeaderText = "Employee";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "WorkStatusID";
            this.dataGridViewTextBoxColumn3.HeaderText = "EmpBenefitID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "JoiningDate";
            this.dataGridViewTextBoxColumn4.HeaderText = "EmployeeID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "EmpBenefitID";
            this.dataGridViewTextBoxColumn5.HeaderText = "BenefitTypeID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ParentID";
            this.dataGridViewTextBoxColumn6.HeaderText = "CompanyAssetID";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "AssetEmployeeID";
            this.dataGridViewTextBoxColumn7.HeaderText = "Asset Type";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "BenefitTypeID";
            this.dataGridViewTextBoxColumn8.HeaderText = "Asset";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Visible = false;
            this.dataGridViewTextBoxColumn8.Width = 120;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "CompanyAssetID";
            this.dataGridViewTextBoxColumn9.HeaderText = "With Effect Date";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            this.dataGridViewTextBoxColumn9.Width = 130;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "AssetType";
            this.dataGridViewTextBoxColumn10.HeaderText = "Exp. Return Date";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 140;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Asset";
            this.dataGridViewTextBoxColumn11.HeaderText = "Return Date";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 130;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn12.DataPropertyName = "WithEffectDate";
            this.dataGridViewTextBoxColumn12.HeaderText = "Remarks";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "ExpectedReturnDate";
            this.dataGridViewTextBoxColumn13.HeaderText = "Exp. Return Date";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 140;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "ReturnDate";
            this.dataGridViewTextBoxColumn14.HeaderText = "Return Date";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 130;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "AssetStatusID";
            this.dataGridViewTextBoxColumn15.HeaderText = "AssetStatusID";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn16.DataPropertyName = "AssetRemarks";
            this.dataGridViewTextBoxColumn16.HeaderText = "Remarks";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Remarks";
            this.dataGridViewTextBoxColumn17.HeaderText = "Remarks";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 500;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // FrmCompanyAssetOperations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1184, 464);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlLeft);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmCompanyAssetOperations";
            this.Text = "Asset Handover";
            this.Load += new System.EventHandler(this.FrmCompanyAssetOperations_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCompanyAssetOperations_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCompanyAssetOperations_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeAsset)).EndInit();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.expnlTop.ResumeLayout(false);
            this.expnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOtherInfoDetails)).EndInit();
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.pnlLeft.ResumeLayout(false);
            this.expnlSearch.ResumeLayout(false);
            this.expnlSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errValidate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DataGridView dgvEmployee;
        internal DevComponents.DotNetBar.PanelEx pnlMain;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.RichTextBox txtRemarks;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label lblEndDate;
        internal System.Windows.Forms.DateTimePicker dtpWithEffectFrom;
        internal System.Windows.Forms.Label lblPeriodFrom;
        internal System.Windows.Forms.DateTimePicker dtpExpectedReturnDate;
        internal System.Windows.Forms.Label lblEmployee;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.DateTimePicker dtpReturnDate;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal DevComponents.DotNetBar.PanelEx pnlLeft;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripButton btnClear;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal DevComponents.DotNetBar.ExpandableSplitter expSpltrLeft;
        internal System.Windows.Forms.DataGridView dgvOtherInfoDetails;
        internal System.Windows.Forms.DataGridView dgvEmployeeAsset;
        internal System.Windows.Forms.Label lblBenefitType;
        internal System.Windows.Forms.ComboBox cboAssetType;
        private System.Windows.Forms.Label lblCompanyAssets;
        private System.Windows.Forms.ComboBox cboAssets;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.ComboBox cboFilterCompany;
        private DevComponents.DotNetBar.ExpandablePanel expnlTop;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatusShow;
        internal System.Windows.Forms.ToolStripStatusLabel lblStatus;
        internal System.Windows.Forms.Button btnSave;
        private DevComponents.DotNetBar.ExpandablePanel expnlSearch;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.RichTextBox txtAssetInfo;
        private System.Windows.Forms.ToolStripButton btnPrint;
        private System.Windows.Forms.ToolStripButton btnEmail;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.ToolStripButton btnAddNew;
        private System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        private System.Windows.Forms.ToolStripMenuItem btnAddAssets;
        private System.Windows.Forms.ToolStripMenuItem btnOtherInfoType;
        internal System.Windows.Forms.Label lblEmployeeSelected;
        internal System.Windows.Forms.Timer tmrClear;
        private System.Windows.Forms.ErrorProvider errValidate;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        internal System.Windows.Forms.Button btnSearch;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        internal System.Windows.Forms.ToolStripButton btnSaveItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        internal System.Windows.Forms.Button btnSearchClear;
        internal System.Windows.Forms.Label lblJoiningDate;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmpBenefitID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AssetEmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BenefitTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyAssetID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AssetType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Asset;
        private System.Windows.Forms.DataGridViewTextBoxColumn WithEffectDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpectedReturnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn AssetStatusID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AssetRemarks;
        private DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn Return;
        internal System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkStatusID;
        private System.Windows.Forms.DataGridViewTextBoxColumn JoiningDate;
        private System.Windows.Forms.DataGridViewComboBoxColumn AssetUsageTypeID;
        private DevComponents.DotNetBar.Controls.DataGridViewDateTimeInputColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remarks;
        private System.Windows.Forms.ToolStripButton btnMaintenance;
    }
}