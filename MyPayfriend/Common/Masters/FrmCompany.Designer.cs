﻿namespace MyPayfriend
{
    partial class FrmCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label3;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label21;
            System.Windows.Forms.Label label25;
            System.Windows.Forms.Label label26;
            System.Windows.Forms.Label label27;
            System.Windows.Forms.Label label28;
            System.Windows.Forms.Label label29;
            System.Windows.Forms.Label label30;
            System.Windows.Forms.Label label31;
            System.Windows.Forms.Label label32;
            System.Windows.Forms.Label label33;
            System.Windows.Forms.Label label34;
            System.Windows.Forms.Label label37;
            System.Windows.Forms.Label label38;
            System.Windows.Forms.Label label39;
            System.Windows.Forms.Label label40;
            System.Windows.Forms.Label label41;
            System.Windows.Forms.Label label45;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label lblDailiyPayBasedOn;
            System.Windows.Forms.Label lblWorkingDays;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label lblWeeklyOffday;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label6;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCompany));
            this.CompanyMasterBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.CompanyMasterBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.CancelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnBank = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.TradingLicenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LeaseAgreementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherDocumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bnInsurance = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.DtpBookStartDate = new System.Windows.Forms.DateTimePicker();
            this.Label11 = new System.Windows.Forms.Label();
            this.DtpFinYearStartDate = new System.Windows.Forms.DateTimePicker();
            this.ComboBoxBankNameIDD = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ErrorProviderCompany = new System.Windows.Forms.ErrorProvider(this.components);
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.TxtPOBox = new System.Windows.Forms.TextBox();
            this.CboCompanyIndustry = new System.Windows.Forms.ComboBox();
            this.CboCurrency = new System.Windows.Forms.ComboBox();
            this.CboCompanyName = new System.Windows.Forms.ComboBox();
            this.CboCountry = new System.Windows.Forms.ComboBox();
            this.TxtShortName = new System.Windows.Forms.TextBox();
            this.TxtEmployerCode = new System.Windows.Forms.TextBox();
            this.cboUnearnedPolicyID = new System.Windows.Forms.ComboBox();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.LogoFilePictureBox = new System.Windows.Forms.PictureBox();
            this.TxtCity = new System.Windows.Forms.TextBox();
            this.TxtBlock = new System.Windows.Forms.TextBox();
            this.TxtFaxNumber = new System.Windows.Forms.TextBox();
            this.TxtContactPersonPhone = new System.Windows.Forms.TextBox();
            this.TxtSecondaryEmail = new System.Windows.Forms.TextBox();
            this.TxtPrimaryEmail = new System.Windows.Forms.TextBox();
            this.TxtArea = new System.Windows.Forms.TextBox();
            this.CboProvince = new System.Windows.Forms.ComboBox();
            this.CboCompanyType = new System.Windows.Forms.ComboBox();
            this.TxtStreet = new System.Windows.Forms.TextBox();
            this.TxtOtherInfo = new System.Windows.Forms.TextBox();
            this.TxtWebSite = new System.Windows.Forms.TextBox();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.RBtnCompany = new System.Windows.Forms.RadioButton();
            this.RbtnBranch = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.DtpFinyearDate = new System.Windows.Forms.DateTimePicker();
            this.CompanyTab = new System.Windows.Forms.TabControl();
            this.CompanyInfoTab = new System.Windows.Forms.TabPage();
            this.TxtBranchName = new System.Windows.Forms.TextBox();
            this.BtnCompanyType = new System.Windows.Forms.Button();
            this.BtnComIndustry = new System.Windows.Forms.Button();
            this.BtnCurrency = new System.Windows.Forms.Button();
            this.BtnCountry = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.LblName = new System.Windows.Forms.Label();
            this.GeneralTab = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkMonday = new System.Windows.Forms.CheckBox();
            this.chkSaturday = new System.Windows.Forms.CheckBox();
            this.chkTuesDay = new System.Windows.Forms.CheckBox();
            this.chkFriday = new System.Windows.Forms.CheckBox();
            this.chkSunday = new System.Windows.Forms.CheckBox();
            this.chkWednesday = new System.Windows.Forms.CheckBox();
            this.chkThursday = new System.Windows.Forms.CheckBox();
            this.BtnProvince = new System.Windows.Forms.Button();
            this.Accdates = new System.Windows.Forms.TabPage();
            this.GrdCompanyBanks = new DemoClsDataGridview.ClsDataGirdView();
            this.CompanyBankId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankNameId = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.AccountNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnUnearnedPolicy = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RBtnYear = new System.Windows.Forms.RadioButton();
            this.RBtnMonth = new System.Windows.Forms.RadioButton();
            this.NumTxtWorkingDays = new NumericTextBox();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.DtpBkStartDate = new System.Windows.Forms.DateTimePicker();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TabLogo = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.FilePathButton = new System.Windows.Forms.Button();
            this.BtnRemove = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.CompanyAccIDD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyIDD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountNoo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADDFLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TmrComapny = new System.Windows.Forms.Timer(this.components);
            this.ssStatus = new System.Windows.Forms.StatusStrip();
            this.lblcompanystatus = new System.Windows.Forms.ToolStripStatusLabel();
            Label3 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label21 = new System.Windows.Forms.Label();
            label25 = new System.Windows.Forms.Label();
            label26 = new System.Windows.Forms.Label();
            label27 = new System.Windows.Forms.Label();
            label28 = new System.Windows.Forms.Label();
            label29 = new System.Windows.Forms.Label();
            label30 = new System.Windows.Forms.Label();
            label31 = new System.Windows.Forms.Label();
            label32 = new System.Windows.Forms.Label();
            label33 = new System.Windows.Forms.Label();
            label34 = new System.Windows.Forms.Label();
            label37 = new System.Windows.Forms.Label();
            label38 = new System.Windows.Forms.Label();
            label39 = new System.Windows.Forms.Label();
            label40 = new System.Windows.Forms.Label();
            label41 = new System.Windows.Forms.Label();
            label45 = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            lblDailiyPayBasedOn = new System.Windows.Forms.Label();
            lblWorkingDays = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            lblWeeklyOffday = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyMasterBindingNavigator)).BeginInit();
            this.CompanyMasterBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoFilePictureBox)).BeginInit();
            this.Panel1.SuspendLayout();
            this.CompanyTab.SuspendLayout();
            this.CompanyInfoTab.SuspendLayout();
            this.GeneralTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Accdates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdCompanyBanks)).BeginInit();
            this.panel2.SuspendLayout();
            this.TabLogo.SuspendLayout();
            this.ssStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label3
            // 
            Label3.AutoSize = true;
            Label3.Location = new System.Drawing.Point(29, 25);
            Label3.Name = "Label3";
            Label3.Size = new System.Drawing.Size(125, 13);
            Label3.TabIndex = 0;
            Label3.Text = "Financial Year Start Date";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(10, 36);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(99, 13);
            label13.TabIndex = 4;
            label13.Text = "Financial Year Start";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(11, 112);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(78, 13);
            label14.TabIndex = 8;
            label14.Text = "Employer Code";
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Location = new System.Drawing.Point(11, 249);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(49, 13);
            label21.TabIndex = 17;
            label21.Text = "Currency";
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Location = new System.Drawing.Point(11, 214);
            label25.Name = "label25";
            label25.Size = new System.Drawing.Size(43, 13);
            label25.TabIndex = 14;
            label25.Text = "Country";
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Location = new System.Drawing.Point(11, 284);
            label26.Name = "label26";
            label26.Size = new System.Drawing.Size(91, 13);
            label26.TabIndex = 20;
            label26.Text = "Company Industry";
            // 
            // label27
            // 
            label27.AutoSize = true;
            label27.Location = new System.Drawing.Point(11, 146);
            label27.Name = "label27";
            label27.Size = new System.Drawing.Size(63, 13);
            label27.TabIndex = 10;
            label27.Text = "Short Name";
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Location = new System.Drawing.Point(11, 180);
            label28.Name = "label28";
            label28.Size = new System.Drawing.Size(43, 13);
            label28.TabIndex = 12;
            label28.Text = "PO Box";
            // 
            // label29
            // 
            label29.AutoSize = true;
            label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label29.Location = new System.Drawing.Point(11, 274);
            label29.Name = "label29";
            label29.Size = new System.Drawing.Size(94, 13);
            label29.TabIndex = 27;
            label29.Text = "Other Information";
            // 
            // label30
            // 
            label30.AutoSize = true;
            label30.Location = new System.Drawing.Point(11, 154);
            label30.Name = "label30";
            label30.Size = new System.Drawing.Size(69, 13);
            label30.TabIndex = 14;
            label30.Text = "Primary Email";
            // 
            // label31
            // 
            label31.AutoSize = true;
            label31.Location = new System.Drawing.Point(11, 319);
            label31.Name = "label31";
            label31.Size = new System.Drawing.Size(78, 13);
            label31.TabIndex = 11;
            label31.Text = "Company Type";
            // 
            // label32
            // 
            label32.AutoSize = true;
            label32.Location = new System.Drawing.Point(11, 123);
            label32.Name = "label32";
            label32.Size = new System.Drawing.Size(79, 13);
            label32.TabIndex = 8;
            label32.Text = "Province/State";
            // 
            // label33
            // 
            label33.AutoSize = true;
            label33.Location = new System.Drawing.Point(11, 15);
            label33.Name = "label33";
            label33.Size = new System.Drawing.Size(29, 13);
            label33.TabIndex = 0;
            label33.Text = "Area";
            // 
            // label34
            // 
            label34.AutoSize = true;
            label34.Location = new System.Drawing.Point(11, 185);
            label34.Name = "label34";
            label34.Size = new System.Drawing.Size(86, 13);
            label34.TabIndex = 16;
            label34.Text = "Secondary Email";
            // 
            // label37
            // 
            label37.AutoSize = true;
            label37.Location = new System.Drawing.Point(11, 215);
            label37.Name = "label37";
            label37.Size = new System.Drawing.Size(75, 13);
            label37.TabIndex = 23;
            label37.Text = "Telephone No";
            // 
            // label38
            // 
            label38.AutoSize = true;
            label38.Location = new System.Drawing.Point(11, 244);
            label38.Name = "label38";
            label38.Size = new System.Drawing.Size(64, 13);
            label38.TabIndex = 25;
            label38.Text = "Fax Number";
            // 
            // label39
            // 
            label39.AutoSize = true;
            label39.Location = new System.Drawing.Point(11, 42);
            label39.Name = "label39";
            label39.Size = new System.Drawing.Size(34, 13);
            label39.TabIndex = 2;
            label39.Text = "Block";
            // 
            // label40
            // 
            label40.AutoSize = true;
            label40.Location = new System.Drawing.Point(11, 69);
            label40.Name = "label40";
            label40.Size = new System.Drawing.Size(35, 13);
            label40.TabIndex = 4;
            label40.Text = "Street";
            // 
            // label41
            // 
            label41.AutoSize = true;
            label41.Location = new System.Drawing.Point(11, 96);
            label41.Name = "label41";
            label41.Size = new System.Drawing.Size(24, 13);
            label41.TabIndex = 6;
            label41.Text = "City";
            // 
            // label45
            // 
            label45.AutoSize = true;
            label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label45.Location = new System.Drawing.Point(12, 31);
            label45.Name = "label45";
            label45.Size = new System.Drawing.Size(90, 13);
            label45.TabIndex = 106;
            label45.Text = "Company Logo";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(11, 354);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(51, 13);
            label16.TabIndex = 29;
            label16.Text = "Web Site";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label12.Location = new System.Drawing.Point(10, 13);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(88, 13);
            label12.TabIndex = 171;
            label12.Text = "Payroll Details";
            // 
            // lblDailiyPayBasedOn
            // 
            lblDailiyPayBasedOn.AutoSize = true;
            lblDailiyPayBasedOn.Location = new System.Drawing.Point(10, 135);
            lblDailiyPayBasedOn.Name = "lblDailiyPayBasedOn";
            lblDailiyPayBasedOn.Size = new System.Drawing.Size(101, 13);
            lblDailiyPayBasedOn.TabIndex = 30;
            lblDailiyPayBasedOn.Text = "Daily Pay Based On";
            // 
            // lblWorkingDays
            // 
            lblWorkingDays.AutoSize = true;
            lblWorkingDays.Location = new System.Drawing.Point(10, 168);
            lblWorkingDays.Name = "lblWorkingDays";
            lblWorkingDays.Size = new System.Drawing.Size(74, 13);
            lblWorkingDays.TabIndex = 124;
            lblWorkingDays.Text = "Working Days";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(10, 69);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(102, 13);
            label1.TabIndex = 173;
            label1.Text = "Company Start Date";
            // 
            // lblWeeklyOffday
            // 
            lblWeeklyOffday.AutoSize = true;
            lblWeeklyOffday.Location = new System.Drawing.Point(11, 338);
            lblWeeklyOffday.Name = "lblWeeklyOffday";
            lblWeeklyOffday.Size = new System.Drawing.Size(87, 13);
            lblWeeklyOffday.TabIndex = 128;
            lblWeeklyOffday.Text = "Weekly Off Days";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(10, 102);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(85, 13);
            label2.TabIndex = 128;
            label2.Text = "Unearned Policy";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.Location = new System.Drawing.Point(10, 195);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(77, 13);
            label6.TabIndex = 176;
            label6.Text = "Bank Details";
            // 
            // CompanyMasterBindingNavigator
            // 
            this.CompanyMasterBindingNavigator.AddNewItem = null;
            this.CompanyMasterBindingNavigator.BackColor = System.Drawing.Color.Transparent;
            this.CompanyMasterBindingNavigator.CountItem = this.BindingNavigatorCountItem;
            this.CompanyMasterBindingNavigator.DeleteItem = null;
            this.CompanyMasterBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BindingNavigatorSeparator,
            this.BindingNavigatorPositionItem,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorSeparator1,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BindingNavigatorSeparator2,
            this.BindingNavigatorAddNewItem,
            this.CompanyMasterBindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.CancelToolStripButton,
            this.ToolStripSeparator1,
            this.BtnBank,
            this.toolStripSeparator4,
            this.bnMoreActions,
            this.ToolStripSeparator2,
            this.BtnPrint,
            this.BtnEmail,
            this.toolStripSeparator3,
            this.BtnHelp});
            this.CompanyMasterBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.CompanyMasterBindingNavigator.MoveFirstItem = null;
            this.CompanyMasterBindingNavigator.MoveLastItem = null;
            this.CompanyMasterBindingNavigator.MoveNextItem = null;
            this.CompanyMasterBindingNavigator.MovePreviousItem = null;
            this.CompanyMasterBindingNavigator.Name = "CompanyMasterBindingNavigator";
            this.CompanyMasterBindingNavigator.PositionItem = this.BindingNavigatorPositionItem;
            this.CompanyMasterBindingNavigator.Size = new System.Drawing.Size(506, 25);
            this.CompanyMasterBindingNavigator.TabIndex = 0;
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BindingNavigatorSeparator
            // 
            this.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator";
            this.BindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorPositionItem
            // 
            this.BindingNavigatorPositionItem.AccessibleName = "Position";
            this.BindingNavigatorPositionItem.AutoSize = false;
            this.BindingNavigatorPositionItem.Enabled = false;
            this.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem";
            this.BindingNavigatorPositionItem.ReadOnly = true;
            this.BindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatorPositionItem.Text = "0";
            this.BindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // BindingNavigatorSeparator1
            // 
            this.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1";
            this.BindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BindingNavigatorSeparator2
            // 
            this.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2";
            this.BindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.Text = "Add";
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New Company";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // CompanyMasterBindingNavigatorSaveItem
            // 
            this.CompanyMasterBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CompanyMasterBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("CompanyMasterBindingNavigatorSaveItem.Image")));
            this.CompanyMasterBindingNavigatorSaveItem.Name = "CompanyMasterBindingNavigatorSaveItem";
            this.CompanyMasterBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.CompanyMasterBindingNavigatorSaveItem.Text = "Save";
            this.CompanyMasterBindingNavigatorSaveItem.Click += new System.EventHandler(this.CompanyMasterBindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.Text = "Remove";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // CancelToolStripButton
            // 
            this.CancelToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CancelToolStripButton.Image")));
            this.CancelToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CancelToolStripButton.Name = "CancelToolStripButton";
            this.CancelToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CancelToolStripButton.ToolTipText = "Clear";
            this.CancelToolStripButton.Click += new System.EventHandler(this.CancelToolStripButton_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnBank
            // 
            this.BtnBank.Image = global::MyPayfriend.Properties.Resources.Bank;
            this.BtnBank.Name = "BtnBank";
            this.BtnBank.Size = new System.Drawing.Size(23, 22);
            this.BtnBank.ToolTipText = "Bank";
            this.BtnBank.Click += new System.EventHandler(this.BtnBank_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TradingLicenseToolStripMenuItem,
            this.LeaseAgreementToolStripMenuItem,
            this.otherDocumentsToolStripMenuItem,
            this.bnInsurance});
            this.bnMoreActions.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.bnMoreActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "Actions";
            this.bnMoreActions.ToolTipText = "Actions";
            // 
            // TradingLicenseToolStripMenuItem
            // 
            this.TradingLicenseToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.EmiritsCard;
            this.TradingLicenseToolStripMenuItem.Name = "TradingLicenseToolStripMenuItem";
            this.TradingLicenseToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.TradingLicenseToolStripMenuItem.Text = "&Trading License";
            this.TradingLicenseToolStripMenuItem.ToolTipText = "Trading License";
            this.TradingLicenseToolStripMenuItem.Click += new System.EventHandler(this.TradingLicenseToolStripMenuItem_Click);
            // 
            // LeaseAgreementToolStripMenuItem
            // 
            this.LeaseAgreementToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.InsuranceCard;
            this.LeaseAgreementToolStripMenuItem.Name = "LeaseAgreementToolStripMenuItem";
            this.LeaseAgreementToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.LeaseAgreementToolStripMenuItem.Text = "&Lease Agreement";
            this.LeaseAgreementToolStripMenuItem.ToolTipText = "Lease Agreement";
            this.LeaseAgreementToolStripMenuItem.Click += new System.EventHandler(this.LeaseAgreementToolStripMenuItem_Click);
            // 
            // otherDocumentsToolStripMenuItem
            // 
            this.otherDocumentsToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.otherDocumentsToolStripMenuItem.Name = "otherDocumentsToolStripMenuItem";
            this.otherDocumentsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.otherDocumentsToolStripMenuItem.Text = "&Other Documents";
            this.otherDocumentsToolStripMenuItem.Click += new System.EventHandler(this.otherDocumentsToolStripMenuItem_Click);
            // 
            // bnInsurance
            // 
            this.bnInsurance.Image = global::MyPayfriend.Properties.Resources.Insurance16x16;
            this.bnInsurance.Name = "bnInsurance";
            this.bnInsurance.Size = new System.Drawing.Size(168, 22);
            this.bnInsurance.Text = "&Insurance";
            this.bnInsurance.ToolTipText = "Insurance";
            this.bnInsurance.Click += new System.EventHandler(this.bnInsurance_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.Text = "Print/Fax";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.Text = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.Text = "He&lp";
            this.BtnHelp.Click += new System.EventHandler(this.BtnHelp_Click);
            // 
            // DtpBookStartDate
            // 
            this.DtpBookStartDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpBookStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpBookStartDate.Location = new System.Drawing.Point(178, 56);
            this.DtpBookStartDate.Name = "DtpBookStartDate";
            this.DtpBookStartDate.Size = new System.Drawing.Size(104, 20);
            this.DtpBookStartDate.TabIndex = 3;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(29, 60);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(83, 13);
            this.Label11.TabIndex = 2;
            this.Label11.Text = "Book Start Date";
            // 
            // DtpFinYearStartDate
            // 
            this.DtpFinYearStartDate.Location = new System.Drawing.Point(0, 0);
            this.DtpFinYearStartDate.Name = "DtpFinYearStartDate";
            this.DtpFinYearStartDate.Size = new System.Drawing.Size(200, 20);
            this.DtpFinYearStartDate.TabIndex = 0;
            // 
            // ComboBoxBankNameIDD
            // 
            this.ComboBoxBankNameIDD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ComboBoxBankNameIDD.HeaderText = "Bank Name";
            this.ComboBoxBankNameIDD.Name = "ComboBoxBankNameIDD";
            this.ComboBoxBankNameIDD.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ComboBoxBankNameIDD.ToolTipText = "Name of the Bank and its account number for pay processing and release.";
            this.ComboBoxBankNameIDD.Width = 300;
            // 
            // ErrorProviderCompany
            // 
            this.ErrorProviderCompany.ContainerControl = this;
            this.ErrorProviderCompany.RightToLeft = true;
            // 
            // NameTextBox
            // 
            this.NameTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.NameTextBox.Location = new System.Drawing.Point(140, 42);
            this.NameTextBox.MaxLength = 50;
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(291, 20);
            this.NameTextBox.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.NameTextBox, "Enter the name of the company you wish to create.");
            this.NameTextBox.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtPOBox
            // 
            this.TxtPOBox.BackColor = System.Drawing.SystemColors.Info;
            this.TxtPOBox.Location = new System.Drawing.Point(139, 176);
            this.TxtPOBox.MaxLength = 10;
            this.TxtPOBox.Name = "TxtPOBox";
            this.TxtPOBox.Size = new System.Drawing.Size(104, 20);
            this.TxtPOBox.TabIndex = 4;
            this.TxtPOBox.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // CboCompanyIndustry
            // 
            this.CboCompanyIndustry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompanyIndustry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompanyIndustry.BackColor = System.Drawing.SystemColors.Window;
            this.CboCompanyIndustry.DropDownHeight = 75;
            this.CboCompanyIndustry.FormattingEnabled = true;
            this.CboCompanyIndustry.IntegralHeight = false;
            this.CboCompanyIndustry.Location = new System.Drawing.Point(139, 280);
            this.CboCompanyIndustry.MaxDropDownItems = 10;
            this.CboCompanyIndustry.Name = "CboCompanyIndustry";
            this.CboCompanyIndustry.Size = new System.Drawing.Size(257, 21);
            this.CboCompanyIndustry.TabIndex = 9;
            this.CboCompanyIndustry.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboCompanyIndustry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCurrency
            // 
            this.CboCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCurrency.BackColor = System.Drawing.SystemColors.Info;
            this.CboCurrency.DropDownHeight = 75;
            this.CboCurrency.FormattingEnabled = true;
            this.CboCurrency.IntegralHeight = false;
            this.CboCurrency.Location = new System.Drawing.Point(139, 245);
            this.CboCurrency.MaxDropDownItems = 10;
            this.CboCurrency.Name = "CboCurrency";
            this.CboCurrency.Size = new System.Drawing.Size(257, 21);
            this.CboCurrency.TabIndex = 7;
            this.CboCurrency.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboCurrency.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCompanyName
            // 
            this.CboCompanyName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompanyName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompanyName.BackColor = System.Drawing.SystemColors.Info;
            this.CboCompanyName.DropDownHeight = 75;
            this.CboCompanyName.FormattingEnabled = true;
            this.CboCompanyName.IntegralHeight = false;
            this.CboCompanyName.Location = new System.Drawing.Point(139, 41);
            this.CboCompanyName.MaxDropDownItems = 10;
            this.CboCompanyName.Name = "CboCompanyName";
            this.CboCompanyName.Size = new System.Drawing.Size(292, 21);
            this.CboCompanyName.TabIndex = 1;
            this.CboCompanyName.SelectedIndexChanged += new System.EventHandler(this.CboCompanyName_SelectedIndexChanged);
            this.CboCompanyName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCountry
            // 
            this.CboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCountry.BackColor = System.Drawing.SystemColors.Info;
            this.CboCountry.DropDownHeight = 75;
            this.CboCountry.FormattingEnabled = true;
            this.CboCountry.IntegralHeight = false;
            this.CboCountry.Location = new System.Drawing.Point(139, 210);
            this.CboCountry.MaxDropDownItems = 10;
            this.CboCountry.Name = "CboCountry";
            this.CboCountry.Size = new System.Drawing.Size(257, 21);
            this.CboCountry.TabIndex = 5;
            this.CboCountry.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboCountry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // TxtShortName
            // 
            this.TxtShortName.AcceptsReturn = true;
            this.TxtShortName.BackColor = System.Drawing.SystemColors.Info;
            this.TxtShortName.Location = new System.Drawing.Point(139, 142);
            this.TxtShortName.MaxLength = 20;
            this.TxtShortName.Name = "TxtShortName";
            this.TxtShortName.Size = new System.Drawing.Size(104, 20);
            this.TxtShortName.TabIndex = 3;
            this.TxtShortName.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtEmployerCode
            // 
            this.TxtEmployerCode.BackColor = System.Drawing.SystemColors.Info;
            this.TxtEmployerCode.Location = new System.Drawing.Point(139, 108);
            this.TxtEmployerCode.MaxLength = 14;
            this.TxtEmployerCode.Name = "TxtEmployerCode";
            this.TxtEmployerCode.Size = new System.Drawing.Size(104, 20);
            this.TxtEmployerCode.TabIndex = 2;
            this.TxtEmployerCode.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // cboUnearnedPolicyID
            // 
            this.cboUnearnedPolicyID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboUnearnedPolicyID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboUnearnedPolicyID.BackColor = System.Drawing.Color.White;
            this.cboUnearnedPolicyID.DropDownHeight = 75;
            this.cboUnearnedPolicyID.FormattingEnabled = true;
            this.cboUnearnedPolicyID.IntegralHeight = false;
            this.cboUnearnedPolicyID.Location = new System.Drawing.Point(138, 98);
            this.cboUnearnedPolicyID.MaxDropDownItems = 10;
            this.cboUnearnedPolicyID.Name = "cboUnearnedPolicyID";
            this.cboUnearnedPolicyID.Size = new System.Drawing.Size(182, 21);
            this.cboUnearnedPolicyID.TabIndex = 4;
            this.cboUnearnedPolicyID.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn1.HeaderText = "Bank Name";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.ToolTipText = "Name of the Bank and its account number for pay processing and release.";
            this.dataGridViewComboBoxColumn1.Width = 300;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn2.HeaderText = "Bank Name";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.ToolTipText = "Name of the Bank and its account number for pay processing and release.";
            this.dataGridViewComboBoxColumn2.Width = 300;
            // 
            // LogoFilePictureBox
            // 
            this.LogoFilePictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LogoFilePictureBox.Location = new System.Drawing.Point(146, 116);
            this.LogoFilePictureBox.Name = "LogoFilePictureBox";
            this.LogoFilePictureBox.Size = new System.Drawing.Size(192, 96);
            this.LogoFilePictureBox.TabIndex = 3;
            this.LogoFilePictureBox.TabStop = false;
            // 
            // TxtCity
            // 
            this.TxtCity.Location = new System.Drawing.Point(132, 96);
            this.TxtCity.MaxLength = 50;
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Size = new System.Drawing.Size(296, 20);
            this.TxtCity.TabIndex = 3;
            this.TxtCity.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtBlock
            // 
            this.TxtBlock.Location = new System.Drawing.Point(132, 41);
            this.TxtBlock.MaxLength = 50;
            this.TxtBlock.Name = "TxtBlock";
            this.TxtBlock.Size = new System.Drawing.Size(296, 20);
            this.TxtBlock.TabIndex = 1;
            this.TxtBlock.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtFaxNumber
            // 
            this.TxtFaxNumber.Location = new System.Drawing.Point(132, 244);
            this.TxtFaxNumber.MaxLength = 40;
            this.TxtFaxNumber.Name = "TxtFaxNumber";
            this.TxtFaxNumber.Size = new System.Drawing.Size(257, 20);
            this.TxtFaxNumber.TabIndex = 9;
            this.TxtFaxNumber.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtContactPersonPhone
            // 
            this.TxtContactPersonPhone.Location = new System.Drawing.Point(132, 215);
            this.TxtContactPersonPhone.MaxLength = 40;
            this.TxtContactPersonPhone.Name = "TxtContactPersonPhone";
            this.TxtContactPersonPhone.Size = new System.Drawing.Size(257, 20);
            this.TxtContactPersonPhone.TabIndex = 8;
            this.TxtContactPersonPhone.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtSecondaryEmail
            // 
            this.TxtSecondaryEmail.Location = new System.Drawing.Point(132, 185);
            this.TxtSecondaryEmail.MaxLength = 50;
            this.TxtSecondaryEmail.Name = "TxtSecondaryEmail";
            this.TxtSecondaryEmail.Size = new System.Drawing.Size(296, 20);
            this.TxtSecondaryEmail.TabIndex = 7;
            this.TxtSecondaryEmail.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtPrimaryEmail
            // 
            this.TxtPrimaryEmail.Location = new System.Drawing.Point(132, 154);
            this.TxtPrimaryEmail.MaxLength = 50;
            this.TxtPrimaryEmail.Name = "TxtPrimaryEmail";
            this.TxtPrimaryEmail.Size = new System.Drawing.Size(296, 20);
            this.TxtPrimaryEmail.TabIndex = 6;
            this.TxtPrimaryEmail.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtArea
            // 
            this.TxtArea.BackColor = System.Drawing.SystemColors.Window;
            this.TxtArea.Location = new System.Drawing.Point(132, 12);
            this.TxtArea.MaxLength = 50;
            this.TxtArea.Name = "TxtArea";
            this.TxtArea.Size = new System.Drawing.Size(296, 20);
            this.TxtArea.TabIndex = 0;
            this.TxtArea.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // CboProvince
            // 
            this.CboProvince.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboProvince.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboProvince.BackColor = System.Drawing.Color.White;
            this.CboProvince.DropDownHeight = 134;
            this.CboProvince.FormattingEnabled = true;
            this.CboProvince.IntegralHeight = false;
            this.CboProvince.Location = new System.Drawing.Point(132, 123);
            this.CboProvince.MaxDropDownItems = 10;
            this.CboProvince.Name = "CboProvince";
            this.CboProvince.Size = new System.Drawing.Size(238, 21);
            this.CboProvince.TabIndex = 4;
            this.CboProvince.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboProvince.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // CboCompanyType
            // 
            this.CboCompanyType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCompanyType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCompanyType.DropDownHeight = 75;
            this.CboCompanyType.FormattingEnabled = true;
            this.CboCompanyType.IntegralHeight = false;
            this.CboCompanyType.Location = new System.Drawing.Point(139, 315);
            this.CboCompanyType.MaxDropDownItems = 10;
            this.CboCompanyType.Name = "CboCompanyType";
            this.CboCompanyType.Size = new System.Drawing.Size(257, 21);
            this.CboCompanyType.TabIndex = 11;
            this.CboCompanyType.SelectedIndexChanged += new System.EventHandler(this.txt_TextChanged);
            this.CboCompanyType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // TxtStreet
            // 
            this.TxtStreet.Location = new System.Drawing.Point(132, 69);
            this.TxtStreet.MaxLength = 50;
            this.TxtStreet.Name = "TxtStreet";
            this.TxtStreet.Size = new System.Drawing.Size(296, 20);
            this.TxtStreet.TabIndex = 2;
            this.TxtStreet.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtOtherInfo
            // 
            this.TxtOtherInfo.Location = new System.Drawing.Point(131, 274);
            this.TxtOtherInfo.MaxLength = 200;
            this.TxtOtherInfo.Multiline = true;
            this.TxtOtherInfo.Name = "TxtOtherInfo";
            this.TxtOtherInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TxtOtherInfo.Size = new System.Drawing.Size(297, 51);
            this.TxtOtherInfo.TabIndex = 10;
            this.TxtOtherInfo.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // TxtWebSite
            // 
            this.TxtWebSite.Location = new System.Drawing.Point(139, 350);
            this.TxtWebSite.MaxLength = 50;
            this.TxtWebSite.Name = "TxtWebSite";
            this.TxtWebSite.Size = new System.Drawing.Size(292, 20);
            this.TxtWebSite.TabIndex = 13;
            this.TxtWebSite.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // Panel1
            // 
            this.Panel1.Controls.Add(this.RBtnCompany);
            this.Panel1.Controls.Add(this.RbtnBranch);
            this.Panel1.Location = new System.Drawing.Point(140, 0);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(215, 32);
            this.Panel1.TabIndex = 0;
            // 
            // RBtnCompany
            // 
            this.RBtnCompany.AutoSize = true;
            this.RBtnCompany.Checked = true;
            this.RBtnCompany.Location = new System.Drawing.Point(3, 13);
            this.RBtnCompany.Name = "RBtnCompany";
            this.RBtnCompany.Size = new System.Drawing.Size(69, 17);
            this.RBtnCompany.TabIndex = 0;
            this.RBtnCompany.TabStop = true;
            this.RBtnCompany.Text = "Company";
            this.RBtnCompany.UseVisualStyleBackColor = true;
            this.RBtnCompany.CheckedChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // RbtnBranch
            // 
            this.RbtnBranch.AutoSize = true;
            this.RbtnBranch.Location = new System.Drawing.Point(99, 13);
            this.RbtnBranch.Name = "RbtnBranch";
            this.RbtnBranch.Size = new System.Drawing.Size(59, 17);
            this.RbtnBranch.TabIndex = 1;
            this.RbtnBranch.Text = "Branch";
            this.RbtnBranch.UseVisualStyleBackColor = true;
            this.RbtnBranch.CheckedChanged += new System.EventHandler(this.RbtnBranch_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(270, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Book Start Date";
            // 
            // DtpFinyearDate
            // 
            this.DtpFinyearDate.CustomFormat = "MMM-yyyy";
            this.DtpFinyearDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFinyearDate.Location = new System.Drawing.Point(140, 34);
            this.DtpFinyearDate.Name = "DtpFinyearDate";
            this.DtpFinyearDate.Size = new System.Drawing.Size(104, 20);
            this.DtpFinyearDate.TabIndex = 0;
            this.DtpFinyearDate.ValueChanged += new System.EventHandler(this.DtpFinyearDate_ValueChanged);
            // 
            // CompanyTab
            // 
            this.CompanyTab.Controls.Add(this.CompanyInfoTab);
            this.CompanyTab.Controls.Add(this.GeneralTab);
            this.CompanyTab.Controls.Add(this.Accdates);
            this.CompanyTab.Controls.Add(this.TabLogo);
            this.CompanyTab.Location = new System.Drawing.Point(6, 34);
            this.CompanyTab.Name = "CompanyTab";
            this.CompanyTab.SelectedIndex = 0;
            this.CompanyTab.Size = new System.Drawing.Size(492, 427);
            this.CompanyTab.TabIndex = 0;
            // 
            // CompanyInfoTab
            // 
            this.CompanyInfoTab.AutoScroll = true;
            this.CompanyInfoTab.Controls.Add(this.TxtBranchName);
            this.CompanyInfoTab.Controls.Add(this.BtnCompanyType);
            this.CompanyInfoTab.Controls.Add(this.BtnComIndustry);
            this.CompanyInfoTab.Controls.Add(this.BtnCurrency);
            this.CompanyInfoTab.Controls.Add(this.BtnCountry);
            this.CompanyInfoTab.Controls.Add(label14);
            this.CompanyInfoTab.Controls.Add(this.TxtEmployerCode);
            this.CompanyInfoTab.Controls.Add(this.TxtShortName);
            this.CompanyInfoTab.Controls.Add(label16);
            this.CompanyInfoTab.Controls.Add(this.TxtWebSite);
            this.CompanyInfoTab.Controls.Add(label31);
            this.CompanyInfoTab.Controls.Add(label21);
            this.CompanyInfoTab.Controls.Add(this.CboCompanyType);
            this.CompanyInfoTab.Controls.Add(this.NameTextBox);
            this.CompanyInfoTab.Controls.Add(this.label22);
            this.CompanyInfoTab.Controls.Add(this.TxtPOBox);
            this.CompanyInfoTab.Controls.Add(this.label23);
            this.CompanyInfoTab.Controls.Add(this.LblName);
            this.CompanyInfoTab.Controls.Add(this.Panel1);
            this.CompanyInfoTab.Controls.Add(label25);
            this.CompanyInfoTab.Controls.Add(label26);
            this.CompanyInfoTab.Controls.Add(label27);
            this.CompanyInfoTab.Controls.Add(label28);
            this.CompanyInfoTab.Controls.Add(this.CboCompanyIndustry);
            this.CompanyInfoTab.Controls.Add(this.CboCurrency);
            this.CompanyInfoTab.Controls.Add(this.CboCompanyName);
            this.CompanyInfoTab.Controls.Add(this.CboCountry);
            this.CompanyInfoTab.Location = new System.Drawing.Point(4, 22);
            this.CompanyInfoTab.Name = "CompanyInfoTab";
            this.CompanyInfoTab.Padding = new System.Windows.Forms.Padding(3);
            this.CompanyInfoTab.Size = new System.Drawing.Size(484, 401);
            this.CompanyInfoTab.TabIndex = 0;
            this.CompanyInfoTab.Tag = "0";
            this.CompanyInfoTab.Text = "Company Info";
            this.CompanyInfoTab.UseVisualStyleBackColor = true;
            // 
            // TxtBranchName
            // 
            this.TxtBranchName.BackColor = System.Drawing.SystemColors.Window;
            this.TxtBranchName.Enabled = false;
            this.TxtBranchName.Location = new System.Drawing.Point(139, 74);
            this.TxtBranchName.MaxLength = 50;
            this.TxtBranchName.Name = "TxtBranchName";
            this.TxtBranchName.Size = new System.Drawing.Size(292, 20);
            this.TxtBranchName.TabIndex = 1;
            this.TxtBranchName.Visible = false;
            this.TxtBranchName.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // BtnCompanyType
            // 
            this.BtnCompanyType.Location = new System.Drawing.Point(402, 314);
            this.BtnCompanyType.Name = "BtnCompanyType";
            this.BtnCompanyType.Size = new System.Drawing.Size(28, 22);
            this.BtnCompanyType.TabIndex = 12;
            this.BtnCompanyType.Text = "...";
            this.BtnCompanyType.Click += new System.EventHandler(this.BtnCompanyType_Click);
            // 
            // BtnComIndustry
            // 
            this.BtnComIndustry.Location = new System.Drawing.Point(402, 279);
            this.BtnComIndustry.Name = "BtnComIndustry";
            this.BtnComIndustry.Size = new System.Drawing.Size(28, 22);
            this.BtnComIndustry.TabIndex = 10;
            this.BtnComIndustry.Text = "...";
            this.BtnComIndustry.Click += new System.EventHandler(this.CboComIndustry_Click);
            // 
            // BtnCurrency
            // 
            this.BtnCurrency.Location = new System.Drawing.Point(402, 244);
            this.BtnCurrency.Name = "BtnCurrency";
            this.BtnCurrency.Size = new System.Drawing.Size(28, 22);
            this.BtnCurrency.TabIndex = 8;
            this.BtnCurrency.Text = "...";
            this.BtnCurrency.Visible = false;
            this.BtnCurrency.Click += new System.EventHandler(this.BtnCurrency_Click);
            // 
            // BtnCountry
            // 
            this.BtnCountry.Location = new System.Drawing.Point(402, 209);
            this.BtnCountry.Name = "BtnCountry";
            this.BtnCountry.Size = new System.Drawing.Size(28, 22);
            this.BtnCountry.TabIndex = 6;
            this.BtnCountry.Text = "...";
            this.BtnCountry.Click += new System.EventHandler(this.BtnCountry_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Creation Mode ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(11, 46);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(82, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Company Name";
            // 
            // LblName
            // 
            this.LblName.AutoSize = true;
            this.LblName.Location = new System.Drawing.Point(11, 78);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(72, 13);
            this.LblName.TabIndex = 6;
            this.LblName.Text = "Branch Name";
            // 
            // GeneralTab
            // 
            this.GeneralTab.AutoScroll = true;
            this.GeneralTab.Controls.Add(lblWeeklyOffday);
            this.GeneralTab.Controls.Add(this.groupBox1);
            this.GeneralTab.Controls.Add(this.BtnProvince);
            this.GeneralTab.Controls.Add(this.TxtOtherInfo);
            this.GeneralTab.Controls.Add(this.TxtStreet);
            this.GeneralTab.Controls.Add(label29);
            this.GeneralTab.Controls.Add(this.TxtArea);
            this.GeneralTab.Controls.Add(label30);
            this.GeneralTab.Controls.Add(this.TxtPrimaryEmail);
            this.GeneralTab.Controls.Add(this.TxtSecondaryEmail);
            this.GeneralTab.Controls.Add(label32);
            this.GeneralTab.Controls.Add(label33);
            this.GeneralTab.Controls.Add(this.TxtContactPersonPhone);
            this.GeneralTab.Controls.Add(label34);
            this.GeneralTab.Controls.Add(this.TxtFaxNumber);
            this.GeneralTab.Controls.Add(this.TxtBlock);
            this.GeneralTab.Controls.Add(this.TxtCity);
            this.GeneralTab.Controls.Add(label37);
            this.GeneralTab.Controls.Add(label38);
            this.GeneralTab.Controls.Add(label39);
            this.GeneralTab.Controls.Add(label40);
            this.GeneralTab.Controls.Add(label41);
            this.GeneralTab.Controls.Add(this.CboProvince);
            this.GeneralTab.Location = new System.Drawing.Point(4, 22);
            this.GeneralTab.Name = "GeneralTab";
            this.GeneralTab.Padding = new System.Windows.Forms.Padding(3);
            this.GeneralTab.Size = new System.Drawing.Size(484, 401);
            this.GeneralTab.TabIndex = 1;
            this.GeneralTab.Tag = "1";
            this.GeneralTab.Text = "General Info";
            this.GeneralTab.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkMonday);
            this.groupBox1.Controls.Add(this.chkSaturday);
            this.groupBox1.Controls.Add(this.chkTuesDay);
            this.groupBox1.Controls.Add(this.chkFriday);
            this.groupBox1.Controls.Add(this.chkSunday);
            this.groupBox1.Controls.Add(this.chkWednesday);
            this.groupBox1.Controls.Add(this.chkThursday);
            this.groupBox1.Location = new System.Drawing.Point(129, 328);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 59);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // chkMonday
            // 
            this.chkMonday.AutoSize = true;
            this.chkMonday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkMonday.Location = new System.Drawing.Point(80, 14);
            this.chkMonday.Name = "chkMonday";
            this.chkMonday.Size = new System.Drawing.Size(64, 17);
            this.chkMonday.TabIndex = 12;
            this.chkMonday.Tag = "2";
            this.chkMonday.Text = "Monday";
            this.chkMonday.UseVisualStyleBackColor = true;
            this.chkMonday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkSaturday
            // 
            this.chkSaturday.AutoSize = true;
            this.chkSaturday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkSaturday.Location = new System.Drawing.Point(149, 37);
            this.chkSaturday.Name = "chkSaturday";
            this.chkSaturday.Size = new System.Drawing.Size(68, 17);
            this.chkSaturday.TabIndex = 17;
            this.chkSaturday.Tag = "7";
            this.chkSaturday.Text = "Saturday";
            this.chkSaturday.UseVisualStyleBackColor = true;
            this.chkSaturday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkTuesDay
            // 
            this.chkTuesDay.AutoSize = true;
            this.chkTuesDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkTuesDay.Location = new System.Drawing.Point(149, 14);
            this.chkTuesDay.Name = "chkTuesDay";
            this.chkTuesDay.Size = new System.Drawing.Size(67, 17);
            this.chkTuesDay.TabIndex = 13;
            this.chkTuesDay.Tag = "3";
            this.chkTuesDay.Text = "Tuesday";
            this.chkTuesDay.UseVisualStyleBackColor = true;
            this.chkTuesDay.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkFriday
            // 
            this.chkFriday.AutoSize = true;
            this.chkFriday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkFriday.Location = new System.Drawing.Point(80, 37);
            this.chkFriday.Name = "chkFriday";
            this.chkFriday.Size = new System.Drawing.Size(54, 17);
            this.chkFriday.TabIndex = 16;
            this.chkFriday.Tag = "6";
            this.chkFriday.Text = "Friday";
            this.chkFriday.UseVisualStyleBackColor = true;
            this.chkFriday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkSunday
            // 
            this.chkSunday.AutoSize = true;
            this.chkSunday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkSunday.Location = new System.Drawing.Point(8, 14);
            this.chkSunday.Name = "chkSunday";
            this.chkSunday.Size = new System.Drawing.Size(62, 17);
            this.chkSunday.TabIndex = 11;
            this.chkSunday.Tag = "1";
            this.chkSunday.Text = "Sunday";
            this.chkSunday.UseVisualStyleBackColor = true;
            this.chkSunday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkWednesday
            // 
            this.chkWednesday.AutoSize = true;
            this.chkWednesday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkWednesday.Location = new System.Drawing.Point(221, 14);
            this.chkWednesday.Name = "chkWednesday";
            this.chkWednesday.Size = new System.Drawing.Size(83, 17);
            this.chkWednesday.TabIndex = 14;
            this.chkWednesday.Tag = "4";
            this.chkWednesday.Text = "Wednesday";
            this.chkWednesday.UseVisualStyleBackColor = true;
            this.chkWednesday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // chkThursday
            // 
            this.chkThursday.AutoSize = true;
            this.chkThursday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.chkThursday.Location = new System.Drawing.Point(8, 37);
            this.chkThursday.Name = "chkThursday";
            this.chkThursday.Size = new System.Drawing.Size(70, 17);
            this.chkThursday.TabIndex = 15;
            this.chkThursday.Tag = "5";
            this.chkThursday.Text = "Thursday";
            this.chkThursday.UseVisualStyleBackColor = true;
            this.chkThursday.CheckedChanged += new System.EventHandler(this.chkSunday_CheckedChanged);
            // 
            // BtnProvince
            // 
            this.BtnProvince.Location = new System.Drawing.Point(375, 122);
            this.BtnProvince.Name = "BtnProvince";
            this.BtnProvince.Size = new System.Drawing.Size(28, 22);
            this.BtnProvince.TabIndex = 5;
            this.BtnProvince.Text = "...";
            this.BtnProvince.Click += new System.EventHandler(this.BtnProvince_Click);
            // 
            // Accdates
            // 
            this.Accdates.Controls.Add(label6);
            this.Accdates.Controls.Add(this.GrdCompanyBanks);
            this.Accdates.Controls.Add(this.btnUnearnedPolicy);
            this.Accdates.Controls.Add(lblDailiyPayBasedOn);
            this.Accdates.Controls.Add(this.panel2);
            this.Accdates.Controls.Add(lblWorkingDays);
            this.Accdates.Controls.Add(label2);
            this.Accdates.Controls.Add(this.NumTxtWorkingDays);
            this.Accdates.Controls.Add(this.cboUnearnedPolicyID);
            this.Accdates.Controls.Add(label1);
            this.Accdates.Controls.Add(this.dtpStartDate);
            this.Accdates.Controls.Add(label12);
            this.Accdates.Controls.Add(this.DtpBkStartDate);
            this.Accdates.Controls.Add(label13);
            this.Accdates.Controls.Add(this.label8);
            this.Accdates.Controls.Add(this.DtpFinyearDate);
            this.Accdates.Controls.Add(this.shapeContainer1);
            this.Accdates.Location = new System.Drawing.Point(4, 22);
            this.Accdates.Name = "Accdates";
            this.Accdates.Padding = new System.Windows.Forms.Padding(3);
            this.Accdates.Size = new System.Drawing.Size(484, 401);
            this.Accdates.TabIndex = 3;
            this.Accdates.Tag = "2";
            this.Accdates.Text = "Bank Info & Payroll ";
            this.Accdates.UseVisualStyleBackColor = true;
            // 
            // GrdCompanyBanks
            // 
            this.GrdCompanyBanks.AddNewRow = true;
            this.GrdCompanyBanks.AlphaNumericCols = new int[0];
            this.GrdCompanyBanks.BackgroundColor = System.Drawing.Color.White;
            this.GrdCompanyBanks.CapsLockCols = new int[0];
            this.GrdCompanyBanks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdCompanyBanks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CompanyBankId,
            this.CompanyId,
            this.BankNameId,
            this.AccountNumber});
            this.GrdCompanyBanks.DecimalCols = new int[0];
            this.GrdCompanyBanks.HasSlNo = false;
            this.GrdCompanyBanks.LastRowIndex = 0;
            this.GrdCompanyBanks.Location = new System.Drawing.Point(12, 215);
            this.GrdCompanyBanks.Name = "GrdCompanyBanks";
            this.GrdCompanyBanks.NegativeValueCols = new int[0];
            this.GrdCompanyBanks.NumericCols = new int[0];
            this.GrdCompanyBanks.RowHeadersWidth = 25;
            this.GrdCompanyBanks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GrdCompanyBanks.Size = new System.Drawing.Size(451, 180);
            this.GrdCompanyBanks.TabIndex = 7;
            this.GrdCompanyBanks.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrdCompanyBanks_CellValueChanged);
            this.GrdCompanyBanks.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.GrdCompanyBanks_UserDeletingRow);
            this.GrdCompanyBanks.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.GrdCompanyBanks_CellBeginEdit);
            this.GrdCompanyBanks.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.GrdCompanyBanks_RowsAdded);
            this.GrdCompanyBanks.CurrentCellDirtyStateChanged += new System.EventHandler(this.GrdCompanyBanks_CurrentCellDirtyStateChanged);
            this.GrdCompanyBanks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GrdCompanyBanks_KeyDown);
            this.GrdCompanyBanks.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.GrdCompanyBanks_RowsRemoved);
            // 
            // CompanyBankId
            // 
            this.CompanyBankId.HeaderText = "CompanyAccIDD";
            this.CompanyBankId.Name = "CompanyBankId";
            this.CompanyBankId.Visible = false;
            // 
            // CompanyId
            // 
            this.CompanyId.HeaderText = "CompanyIDD";
            this.CompanyId.Name = "CompanyId";
            this.CompanyId.Visible = false;
            // 
            // BankNameId
            // 
            this.BankNameId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BankNameId.HeaderText = "Bank Name";
            this.BankNameId.Name = "BankNameId";
            this.BankNameId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BankNameId.Width = 280;
            // 
            // AccountNumber
            // 
            this.AccountNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AccountNumber.HeaderText = "Account Number";
            this.AccountNumber.MaxInputLength = 49;
            this.AccountNumber.Name = "AccountNumber";
            this.AccountNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnUnearnedPolicy
            // 
            this.btnUnearnedPolicy.Location = new System.Drawing.Point(322, 97);
            this.btnUnearnedPolicy.Name = "btnUnearnedPolicy";
            this.btnUnearnedPolicy.Size = new System.Drawing.Size(28, 22);
            this.btnUnearnedPolicy.TabIndex = 5;
            this.btnUnearnedPolicy.Text = "...";
            this.btnUnearnedPolicy.Click += new System.EventHandler(this.btnUnearnedPolicy_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RBtnYear);
            this.panel2.Controls.Add(this.RBtnMonth);
            this.panel2.Location = new System.Drawing.Point(138, 131);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(216, 25);
            this.panel2.TabIndex = 17;
            // 
            // RBtnYear
            // 
            this.RBtnYear.AutoSize = true;
            this.RBtnYear.Checked = true;
            this.RBtnYear.Location = new System.Drawing.Point(3, 5);
            this.RBtnYear.Name = "RBtnYear";
            this.RBtnYear.Size = new System.Drawing.Size(47, 17);
            this.RBtnYear.TabIndex = 0;
            this.RBtnYear.TabStop = true;
            this.RBtnYear.Text = "Year";
            this.RBtnYear.UseVisualStyleBackColor = true;
            this.RBtnYear.CheckedChanged += new System.EventHandler(this.RBtnYear_CheckedChanged);
            // 
            // RBtnMonth
            // 
            this.RBtnMonth.AutoSize = true;
            this.RBtnMonth.Location = new System.Drawing.Point(75, 5);
            this.RBtnMonth.Name = "RBtnMonth";
            this.RBtnMonth.Size = new System.Drawing.Size(55, 17);
            this.RBtnMonth.TabIndex = 1;
            this.RBtnMonth.Text = "Month";
            this.RBtnMonth.UseVisualStyleBackColor = true;
            // 
            // NumTxtWorkingDays
            // 
            this.NumTxtWorkingDays.BackColor = System.Drawing.SystemColors.Info;
            this.NumTxtWorkingDays.DecimalPlaces = 0;
            this.NumTxtWorkingDays.Location = new System.Drawing.Point(138, 168);
            this.NumTxtWorkingDays.MaxLength = 3;
            this.NumTxtWorkingDays.Name = "NumTxtWorkingDays";
            this.NumTxtWorkingDays.ShortcutsEnabled = false;
            this.NumTxtWorkingDays.Size = new System.Drawing.Size(88, 20);
            this.NumTxtWorkingDays.TabIndex = 6;
            this.NumTxtWorkingDays.Text = "0";
            this.NumTxtWorkingDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumTxtWorkingDays.TextChanged += new System.EventHandler(this.NumTxtWorkingDays_TextChanged);
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(140, 66);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(104, 20);
            this.dtpStartDate.TabIndex = 2;
            this.dtpStartDate.ValueChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // DtpBkStartDate
            // 
            this.DtpBkStartDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpBkStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpBkStartDate.Location = new System.Drawing.Point(359, 34);
            this.DtpBkStartDate.Name = "DtpBkStartDate";
            this.DtpBkStartDate.Size = new System.Drawing.Size(104, 20);
            this.DtpBkStartDate.TabIndex = 1;
            this.DtpBkStartDate.ValueChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1,
            this.lineShape2});
            this.shapeContainer1.Size = new System.Drawing.Size(478, 395);
            this.shapeContainer1.TabIndex = 20;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.Visible = false;
            this.lineShape1.X1 = 69;
            this.lineShape1.X2 = 457;
            this.lineShape1.Y1 = 202;
            this.lineShape1.Y2 = 202;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.Visible = false;
            this.lineShape2.X1 = 70;
            this.lineShape2.X2 = 458;
            this.lineShape2.Y1 = 18;
            this.lineShape2.Y2 = 18;
            // 
            // TabLogo
            // 
            this.TabLogo.AutoScroll = true;
            this.TabLogo.Controls.Add(this.label4);
            this.TabLogo.Controls.Add(this.FilePathButton);
            this.TabLogo.Controls.Add(this.BtnRemove);
            this.TabLogo.Controls.Add(label45);
            this.TabLogo.Controls.Add(this.LogoFilePictureBox);
            this.TabLogo.Location = new System.Drawing.Point(4, 22);
            this.TabLogo.Name = "TabLogo";
            this.TabLogo.Padding = new System.Windows.Forms.Padding(3);
            this.TabLogo.Size = new System.Drawing.Size(484, 401);
            this.TabLogo.TabIndex = 2;
            this.TabLogo.Tag = "3";
            this.TabLogo.Text = "Logo       ";
            this.TabLogo.ToolTipText = "Logo";
            this.TabLogo.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(175, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 13);
            this.label4.TabIndex = 107;
            this.label4.Text = "Width 2 , Height 1 (Inches)";
            // 
            // FilePathButton
            // 
            this.FilePathButton.Location = new System.Drawing.Point(273, 216);
            this.FilePathButton.Name = "FilePathButton";
            this.FilePathButton.Size = new System.Drawing.Size(65, 23);
            this.FilePathButton.TabIndex = 2;
            this.FilePathButton.Text = "&Browse";
            this.FilePathButton.Click += new System.EventHandler(this.FilePathButton_Click);
            // 
            // BtnRemove
            // 
            this.BtnRemove.Location = new System.Drawing.Point(147, 216);
            this.BtnRemove.Name = "BtnRemove";
            this.BtnRemove.Size = new System.Drawing.Size(65, 23);
            this.BtnRemove.TabIndex = 1;
            this.BtnRemove.Text = "&Remove";
            this.BtnRemove.Click += new System.EventHandler(this.BtnRemove_Click);
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewComboBoxColumn3.HeaderText = "Bank Name";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.ToolTipText = "Name of the Bank and its account number for pay processing and release.";
            this.dataGridViewComboBoxColumn3.Width = 300;
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(335, 466);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "&OK";
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(415, 466);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 3;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(6, 466);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 1;
            this.BtnSave.Text = "&Save";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // CompanyAccIDD
            // 
            this.CompanyAccIDD.HeaderText = "CompanyAccIDD";
            this.CompanyAccIDD.Name = "CompanyAccIDD";
            this.CompanyAccIDD.Visible = false;
            // 
            // CompanyIDD
            // 
            this.CompanyIDD.HeaderText = "CompanyIDD";
            this.CompanyIDD.Name = "CompanyIDD";
            this.CompanyIDD.Visible = false;
            // 
            // AccountNoo
            // 
            this.AccountNoo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AccountNoo.HeaderText = "Account Number";
            this.AccountNoo.MaxInputLength = 49;
            this.AccountNoo.Name = "AccountNoo";
            this.AccountNoo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ADDFLAG
            // 
            this.ADDFLAG.HeaderText = "ADDFLAG";
            this.ADDFLAG.Name = "ADDFLAG";
            this.ADDFLAG.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "CompanyAccIDD";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "CompanyIDD";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Account Number";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 49;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "ADDFLAG";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "CompanyAccIDD";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "CompanyIDD";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.HeaderText = "Account Number";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 49;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "ADDFLAG";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "CompanyAccIDD";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "CompanyIDD";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn11.HeaderText = "Account Number";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 49;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "ADDFLAG";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // ssStatus
            // 
            this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblcompanystatus});
            this.ssStatus.Location = new System.Drawing.Point(0, 496);
            this.ssStatus.Name = "ssStatus";
            this.ssStatus.Size = new System.Drawing.Size(506, 22);
            this.ssStatus.TabIndex = 6;
            this.ssStatus.Text = "StatusStrip1";
            // 
            // lblcompanystatus
            // 
            this.lblcompanystatus.Name = "lblcompanystatus";
            this.lblcompanystatus.Size = new System.Drawing.Size(0, 17);
            // 
            // FrmCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 518);
            this.Controls.Add(this.ssStatus);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.CompanyTab);
            this.Controls.Add(this.CompanyMasterBindingNavigator);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCompany";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company Information";
            this.Load += new System.EventHandler(this.FrmCompany_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCompany_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCompany_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.CompanyMasterBindingNavigator)).EndInit();
            this.CompanyMasterBindingNavigator.ResumeLayout(false);
            this.CompanyMasterBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProviderCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoFilePictureBox)).EndInit();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.CompanyTab.ResumeLayout(false);
            this.CompanyInfoTab.ResumeLayout(false);
            this.CompanyInfoTab.PerformLayout();
            this.GeneralTab.ResumeLayout(false);
            this.GeneralTab.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Accdates.ResumeLayout(false);
            this.Accdates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrdCompanyBanks)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.TabLogo.ResumeLayout(false);
            this.TabLogo.PerformLayout();
            this.ssStatus.ResumeLayout(false);
            this.ssStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.BindingNavigator CompanyMasterBindingNavigator;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatorPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BindingNavigatorSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton CompanyMasterBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton CancelToolStripButton;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripButton BtnBank;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        internal System.Windows.Forms.DateTimePicker DtpBookStartDate;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.DateTimePicker DtpFinYearStartDate;
        internal System.Windows.Forms.DataGridViewTextBoxColumn CompanyAccIDD;
        internal System.Windows.Forms.DataGridViewTextBoxColumn CompanyIDD;
        internal System.Windows.Forms.DataGridViewComboBoxColumn ComboBoxBankNameIDD;
        internal System.Windows.Forms.DataGridViewTextBoxColumn AccountNoo;
        internal System.Windows.Forms.DataGridViewTextBoxColumn ADDFLAG;
        internal System.Windows.Forms.ErrorProvider ErrorProviderCompany;
        internal System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        internal System.Windows.Forms.TextBox TxtEmployerCode;
        internal System.Windows.Forms.TextBox TxtShortName;
        internal System.Windows.Forms.TextBox TxtWebSite;
        internal System.Windows.Forms.TextBox NameTextBox;
        internal System.Windows.Forms.TextBox TxtPOBox;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.RadioButton RBtnCompany;
        internal System.Windows.Forms.RadioButton RbtnBranch;
        internal System.Windows.Forms.ComboBox CboCountry;
        internal System.Windows.Forms.ComboBox CboCompanyName;
        internal System.Windows.Forms.ComboBox CboCurrency;
        internal System.Windows.Forms.ComboBox CboCompanyIndustry;
        internal System.Windows.Forms.TextBox TxtOtherInfo;
        internal System.Windows.Forms.TextBox TxtStreet;
        internal System.Windows.Forms.TextBox TxtArea;
        internal System.Windows.Forms.TextBox TxtPrimaryEmail;
        internal System.Windows.Forms.TextBox TxtSecondaryEmail;
        internal System.Windows.Forms.TextBox TxtContactPersonPhone;
        internal System.Windows.Forms.TextBox TxtFaxNumber;
        internal System.Windows.Forms.TextBox TxtBlock;
        internal System.Windows.Forms.TextBox TxtCity;
        internal System.Windows.Forms.ComboBox CboCompanyType;
        internal System.Windows.Forms.ComboBox CboProvince;
        internal System.Windows.Forms.PictureBox LogoFilePictureBox;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.DateTimePicker DtpFinyearDate;
        internal System.Windows.Forms.TabControl CompanyTab;
        internal System.Windows.Forms.TabPage CompanyInfoTab;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.Label LblName;
        internal System.Windows.Forms.TabPage GeneralTab;
        internal System.Windows.Forms.TabPage Accdates;
        internal System.Windows.Forms.TabPage TabLogo;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        internal System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        internal System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DemoClsDataGridview.ClsDataGirdView GrdCompanyBanks;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal System.Windows.Forms.DateTimePicker DtpBkStartDate;
        internal System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.RadioButton RBtnYear;
        internal System.Windows.Forms.RadioButton RBtnMonth;
        private NumericTextBox NumTxtWorkingDays;
        internal System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Timer TmrComapny;
        internal System.Windows.Forms.TextBox TxtBranchName;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.CheckBox chkMonday;
        internal System.Windows.Forms.CheckBox chkSaturday;
        internal System.Windows.Forms.CheckBox chkTuesDay;
        internal System.Windows.Forms.CheckBox chkFriday;
        internal System.Windows.Forms.CheckBox chkSunday;
        internal System.Windows.Forms.CheckBox chkWednesday;
        internal System.Windows.Forms.CheckBox chkThursday;
        internal System.Windows.Forms.ComboBox cboUnearnedPolicyID;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyBankId;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyId;
        private System.Windows.Forms.DataGridViewComboBoxColumn BankNameId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountNumber;
        private System.Windows.Forms.Button BtnCountry;
        private System.Windows.Forms.Button BtnCompanyType;
        private System.Windows.Forms.Button BtnComIndustry;
        private System.Windows.Forms.Button BtnCurrency;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button BtnProvince;
        private System.Windows.Forms.Button FilePathButton;
        private System.Windows.Forms.Button BtnRemove;
        private System.Windows.Forms.Button btnUnearnedPolicy;
        internal System.Windows.Forms.StatusStrip ssStatus;
        internal System.Windows.Forms.ToolStripStatusLabel lblcompanystatus;
        private System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        private System.Windows.Forms.ToolStripMenuItem LeaseAgreementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherDocumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TradingLicenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bnInsurance;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Label label4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
    }
}