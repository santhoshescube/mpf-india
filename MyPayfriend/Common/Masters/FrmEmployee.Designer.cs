﻿namespace MyPayfriend
{
    partial class FrmEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label OfficialEmailLabel;
            System.Windows.Forms.Label DesignationIDLabel;
            System.Windows.Forms.Label DepartmentIDLabel;
            System.Windows.Forms.Label Label8;
            System.Windows.Forms.Label WorkingAtLabel;
            System.Windows.Forms.Label WorkStatusIDLabel;
            System.Windows.Forms.Label DateofJoiningLabel;
            System.Windows.Forms.Label EmploymentTypeLabel;
            System.Windows.Forms.Label ProbationEndDateLabel;
            System.Windows.Forms.Label Label27;
            System.Windows.Forms.Label IdentificationMarksLabel;
            System.Windows.Forms.Label Label13;
            System.Windows.Forms.Label DateofBirthLabel;
            System.Windows.Forms.Label SpouseNameLabel;
            System.Windows.Forms.Label ReligionIDLabel;
            System.Windows.Forms.Label NationalityIDLabel;
            System.Windows.Forms.Label MothersNameLabel;
            System.Windows.Forms.Label FathersNameLabel;
            System.Windows.Forms.Label Label25;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label Label26;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label Label16;
            System.Windows.Forms.Label TransactionTypeIDLabel;
            System.Windows.Forms.Label LocalMobileLabel;
            System.Windows.Forms.Label lblWorkLocation;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label15;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label label18;
            System.Windows.Forms.Label label22;
            System.Windows.Forms.Label EmailLabel;
            System.Windows.Forms.Label LocalAddressLabel;
            System.Windows.Forms.Label LocalPhoneLabel;
            System.Windows.Forms.Label label23;
            System.Windows.Forms.Label lblVisaObtained;
            System.Windows.Forms.Label label28;
            System.Windows.Forms.Label label29;
            System.Windows.Forms.Label label30;
            System.Windows.Forms.Label label31;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label MothertongueIDLabel;
            System.Windows.Forms.Label lblNoticePeriod;
            System.Windows.Forms.Label lblCommissionStructure;
            System.Windows.Forms.Label label24;
            System.Windows.Forms.Label label32;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployee));
            System.Windows.Forms.Label label33;
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.BnEmployee = new System.Windows.Forms.BindingNavigator(this.components);
            this.BindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.BindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.BnSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatornPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.BnSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.BnSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.BindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.BtnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoreActions = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuLeave = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLoan = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSalaryAdvance = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSalaryStructure = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLeaveStructure = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.passportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.licenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qualificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nationalIDCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insuranceCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labourCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.healthCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherDocumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnPrint = new System.Windows.Forms.ToolStripButton();
            this.BtnEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.BtnHelp = new System.Windows.Forms.ToolStripButton();
            this.ErrEmployee = new System.Windows.Forms.ErrorProvider(this.components);
            this.dtpDateofBirth = new System.Windows.Forms.DateTimePicker();
            this.TmEmployee = new System.Windows.Forms.Timer(this.components);
            this.TxtEmployeeNumber = new System.Windows.Forms.TextBox();
            this.LblEmployeeNumber = new System.Windows.Forms.Label();
            this.LblSalutation = new System.Windows.Forms.Label();
            this.TxtFirstName = new System.Windows.Forms.TextBox();
            this.LblFirstName = new System.Windows.Forms.Label();
            this.TxtMiddleName = new System.Windows.Forms.TextBox();
            this.LblMiddleName = new System.Windows.Forms.Label();
            this.TxtLastName = new System.Windows.Forms.TextBox();
            this.LblLastName = new System.Windows.Forms.Label();
            this.CboSalutation = new System.Windows.Forms.ComboBox();
            this.PnlEmployee = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.TbEmployee = new System.Windows.Forms.TabControl();
            this.TpGeneral = new System.Windows.Forms.TabPage();
            this.chkOCom = new System.Windows.Forms.CheckBox();
            this.cboHOD = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpNextAppraisalDate = new System.Windows.Forms.DateTimePicker();
            this.lblRejoinDateDesc = new System.Windows.Forms.Label();
            this.lblRejoinDate = new System.Windows.Forms.Label();
            this.chkOther = new System.Windows.Forms.CheckBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.txtEmployeeFullNameArb = new System.Windows.Forms.TextBox();
            this.cboReportingTo = new System.Windows.Forms.ComboBox();
            this.btnVacationPolicy = new System.Windows.Forms.Button();
            this.cboVacationPolicy = new System.Windows.Forms.ComboBox();
            this.DtpDateofJoining = new System.Windows.Forms.DateTimePicker();
            this.DtpProbationEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblTransferDate = new System.Windows.Forms.Label();
            this.lblTransfer = new System.Windows.Forms.Label();
            this.RecentPhotoPictureBox = new System.Windows.Forms.PictureBox();
            this.txtEmployeeFullName = new System.Windows.Forms.TextBox();
            this.BtnAttach = new System.Windows.Forms.Button();
            this.BtnContxtMenu1 = new System.Windows.Forms.Button();
            this.ContextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuAttach = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuScan = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.cboDepartment = new System.Windows.Forms.ComboBox();
            this.cboEmploymentType = new System.Windows.Forms.ComboBox();
            this.cboDesignation = new System.Windows.Forms.ComboBox();
            this.BtnDesignation = new System.Windows.Forms.Button();
            this.BtnEmptype = new System.Windows.Forms.Button();
            this.BtnDepartment = new System.Windows.Forms.Button();
            this.btnLeavePolicy = new System.Windows.Forms.Button();
            this.cboLeavePolicy = new System.Windows.Forms.ComboBox();
            this.BtnWorkstatus = new System.Windows.Forms.Button();
            this.btnWorkPolicy = new System.Windows.Forms.Button();
            this.cboWorkStatus = new System.Windows.Forms.ComboBox();
            this.cboWorkPolicy = new System.Windows.Forms.ComboBox();
            this.cboWorkingAt = new System.Windows.Forms.ComboBox();
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Tpwork = new System.Windows.Forms.TabPage();
            this.cboBloodGroup = new System.Windows.Forms.ComboBox();
            this.cboGrade = new System.Windows.Forms.ComboBox();
            this.btnGrade = new System.Windows.Forms.Button();
            this.cboCountry = new System.Windows.Forms.ComboBox();
            this.btnCountry = new System.Windows.Forms.Button();
            this.cboMaritalStatus = new System.Windows.Forms.ComboBox();
            this.lblDeviceUser = new System.Windows.Forms.Label();
            this.txtDeviceUserID = new System.Windows.Forms.TextBox();
            this.cboNationality = new System.Windows.Forms.ComboBox();
            this.rdbMale = new System.Windows.Forms.RadioButton();
            this.rdbFemale = new System.Windows.Forms.RadioButton();
            this.cboReligion = new System.Windows.Forms.ComboBox();
            this.BtnReligion = new System.Windows.Forms.Button();
            this.BtnNationality = new System.Windows.Forms.Button();
            this.txtPersonID = new System.Windows.Forms.TextBox();
            this.txtSpouseName = new System.Windows.Forms.TextBox();
            this.txtIdentificationMarks = new System.Windows.Forms.TextBox();
            this.txtMothersName = new System.Windows.Forms.TextBox();
            this.txtFathersName = new System.Windows.Forms.TextBox();
            this.BtnEmployeeBankname = new System.Windows.Forms.Button();
            this.CboEmployeeBankname = new System.Windows.Forms.ComboBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.TransactionTypeReferenceComboBox = new System.Windows.Forms.ComboBox();
            this.AccountNumberTextBox = new System.Windows.Forms.TextBox();
            this.ComBankAccIdComboBox = new System.Windows.Forms.ComboBox();
            this.LblBankAccount = new System.Windows.Forms.Label();
            this.AccountNumberLabel = new System.Windows.Forms.Label();
            this.btnBankName = new System.Windows.Forms.Button();
            this.BankNameReferenceComboBox = new System.Windows.Forms.ComboBox();
            this.LblBankName = new System.Windows.Forms.Label();
            this.btnTranType = new System.Windows.Forms.Button();
            this.ShapeContainer4 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TbContacts = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtZipCode = new System.Windows.Forms.TextBox();
            this.txtState = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtLocalPhone = new System.Windows.Forms.TextBox();
            this.txtLocalAddress = new System.Windows.Forms.TextBox();
            this.txtEmergencyPhone = new System.Windows.Forms.TextBox();
            this.txtEmergencyAddress = new System.Windows.Forms.TextBox();
            this.TxtOfficialEmail = new System.Windows.Forms.TextBox();
            this.txtLocalMobile = new System.Windows.Forms.TextBox();
            this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.LineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TbOtherInfo = new System.Windows.Forms.TabPage();
            this.cboProcessType = new System.Windows.Forms.ComboBox();
            this.cboCommissionStructure = new System.Windows.Forms.ComboBox();
            this.txtNoticePeriod = new System.Windows.Forms.TextBox();
            this.cboWorkLocation = new System.Windows.Forms.ComboBox();
            this.btnWorkLocation = new System.Windows.Forms.Button();
            this.CboVisaObtained = new System.Windows.Forms.ComboBox();
            this.dgvLanguages = new System.Windows.Forms.DataGridView();
            this.ColLanguages = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cboMothertongue = new System.Windows.Forms.ComboBox();
            this.Btnmothertongue = new System.Windows.Forms.Button();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.shapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TxtFirstNameArb = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtLastNameArb = new System.Windows.Forms.TextBox();
            this.TxtMiddleNameArb = new System.Windows.Forms.TextBox();
            this.BtnSalutation = new System.Windows.Forms.Button();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TipEmployee = new System.Windows.Forms.ToolTip(this.components);
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblEmployeeStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.bSearch = new DevComponents.DotNetBar.Bar();
            this.label10 = new System.Windows.Forms.Label();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.txtSearch = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.btnSearch = new DevComponents.DotNetBar.ButtonItem();
            this.RecentPhotoPictureBoxThamNail = new System.Windows.Forms.PictureBox();
            this.directorySearcher1 = new System.DirectoryServices.DirectorySearcher();
            this.cboDivision = new System.Windows.Forms.ComboBox();
            this.btnDivision = new System.Windows.Forms.Button();
            OfficialEmailLabel = new System.Windows.Forms.Label();
            DesignationIDLabel = new System.Windows.Forms.Label();
            DepartmentIDLabel = new System.Windows.Forms.Label();
            Label8 = new System.Windows.Forms.Label();
            WorkingAtLabel = new System.Windows.Forms.Label();
            WorkStatusIDLabel = new System.Windows.Forms.Label();
            DateofJoiningLabel = new System.Windows.Forms.Label();
            EmploymentTypeLabel = new System.Windows.Forms.Label();
            ProbationEndDateLabel = new System.Windows.Forms.Label();
            Label27 = new System.Windows.Forms.Label();
            IdentificationMarksLabel = new System.Windows.Forms.Label();
            Label13 = new System.Windows.Forms.Label();
            DateofBirthLabel = new System.Windows.Forms.Label();
            SpouseNameLabel = new System.Windows.Forms.Label();
            ReligionIDLabel = new System.Windows.Forms.Label();
            NationalityIDLabel = new System.Windows.Forms.Label();
            MothersNameLabel = new System.Windows.Forms.Label();
            FathersNameLabel = new System.Windows.Forms.Label();
            Label25 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            Label26 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            Label16 = new System.Windows.Forms.Label();
            TransactionTypeIDLabel = new System.Windows.Forms.Label();
            LocalMobileLabel = new System.Windows.Forms.Label();
            lblWorkLocation = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label15 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label18 = new System.Windows.Forms.Label();
            label22 = new System.Windows.Forms.Label();
            EmailLabel = new System.Windows.Forms.Label();
            LocalAddressLabel = new System.Windows.Forms.Label();
            LocalPhoneLabel = new System.Windows.Forms.Label();
            label23 = new System.Windows.Forms.Label();
            lblVisaObtained = new System.Windows.Forms.Label();
            label28 = new System.Windows.Forms.Label();
            label29 = new System.Windows.Forms.Label();
            label30 = new System.Windows.Forms.Label();
            label31 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            MothertongueIDLabel = new System.Windows.Forms.Label();
            lblNoticePeriod = new System.Windows.Forms.Label();
            lblCommissionStructure = new System.Windows.Forms.Label();
            label24 = new System.Windows.Forms.Label();
            label32 = new System.Windows.Forms.Label();
            label33 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.BnEmployee)).BeginInit();
            this.BnEmployee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployee)).BeginInit();
            this.PnlEmployee.SuspendLayout();
            this.panel2.SuspendLayout();
            this.TbEmployee.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecentPhotoPictureBox)).BeginInit();
            this.ContextMenuStrip2.SuspendLayout();
            this.Tpwork.SuspendLayout();
            this.TbContacts.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.TbOtherInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLanguages)).BeginInit();
            this.panel1.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).BeginInit();
            this.bSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecentPhotoPictureBoxThamNail)).BeginInit();
            this.SuspendLayout();
            // 
            // OfficialEmailLabel
            // 
            OfficialEmailLabel.AutoSize = true;
            OfficialEmailLabel.Location = new System.Drawing.Point(355, 293);
            OfficialEmailLabel.Name = "OfficialEmailLabel";
            OfficialEmailLabel.Size = new System.Drawing.Size(67, 13);
            OfficialEmailLabel.TabIndex = 0;
            OfficialEmailLabel.Text = "Official Email";
            // 
            // DesignationIDLabel
            // 
            DesignationIDLabel.AutoSize = true;
            DesignationIDLabel.Location = new System.Drawing.Point(16, 143);
            DesignationIDLabel.Name = "DesignationIDLabel";
            DesignationIDLabel.Size = new System.Drawing.Size(63, 13);
            DesignationIDLabel.TabIndex = 123;
            DesignationIDLabel.Text = "Designation";
            // 
            // DepartmentIDLabel
            // 
            DepartmentIDLabel.AutoSize = true;
            DepartmentIDLabel.Location = new System.Drawing.Point(16, 88);
            DepartmentIDLabel.Name = "DepartmentIDLabel";
            DepartmentIDLabel.Size = new System.Drawing.Size(62, 13);
            DepartmentIDLabel.TabIndex = 122;
            DepartmentIDLabel.Text = "Department";
            // 
            // Label8
            // 
            Label8.AutoSize = true;
            Label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label8.Location = new System.Drawing.Point(5, 8);
            Label8.Name = "Label8";
            Label8.Size = new System.Drawing.Size(51, 13);
            Label8.TabIndex = 1;
            Label8.Text = "General";
            // 
            // WorkingAtLabel
            // 
            WorkingAtLabel.AutoSize = true;
            WorkingAtLabel.Location = new System.Drawing.Point(16, 32);
            WorkingAtLabel.Name = "WorkingAtLabel";
            WorkingAtLabel.Size = new System.Drawing.Size(60, 13);
            WorkingAtLabel.TabIndex = 63;
            WorkingAtLabel.Text = "Working At";
            // 
            // WorkStatusIDLabel
            // 
            WorkStatusIDLabel.AutoSize = true;
            WorkStatusIDLabel.Location = new System.Drawing.Point(16, 60);
            WorkStatusIDLabel.Name = "WorkStatusIDLabel";
            WorkStatusIDLabel.Size = new System.Drawing.Size(66, 13);
            WorkStatusIDLabel.TabIndex = 500;
            WorkStatusIDLabel.Text = "Work Status";
            // 
            // DateofJoiningLabel
            // 
            DateofJoiningLabel.AutoSize = true;
            DateofJoiningLabel.Location = new System.Drawing.Point(380, 260);
            DateofJoiningLabel.Name = "DateofJoiningLabel";
            DateofJoiningLabel.Size = new System.Drawing.Size(78, 13);
            DateofJoiningLabel.TabIndex = 136;
            DateofJoiningLabel.Text = "Date of Joining";
            // 
            // EmploymentTypeLabel
            // 
            EmploymentTypeLabel.AutoSize = true;
            EmploymentTypeLabel.Location = new System.Drawing.Point(16, 228);
            EmploymentTypeLabel.Name = "EmploymentTypeLabel";
            EmploymentTypeLabel.Size = new System.Drawing.Size(91, 13);
            EmploymentTypeLabel.TabIndex = 134;
            EmploymentTypeLabel.Text = "Employment Type";
            // 
            // ProbationEndDateLabel
            // 
            ProbationEndDateLabel.AutoSize = true;
            ProbationEndDateLabel.Location = new System.Drawing.Point(380, 286);
            ProbationEndDateLabel.Name = "ProbationEndDateLabel";
            ProbationEndDateLabel.Size = new System.Drawing.Size(100, 13);
            ProbationEndDateLabel.TabIndex = 84;
            ProbationEndDateLabel.Text = "Probation End Date";
            // 
            // Label27
            // 
            Label27.AutoSize = true;
            Label27.Location = new System.Drawing.Point(384, 114);
            Label27.Name = "Label27";
            Label27.Size = new System.Drawing.Size(66, 13);
            Label27.TabIndex = 504;
            Label27.Text = "Blood Group";
            // 
            // IdentificationMarksLabel
            // 
            IdentificationMarksLabel.AutoSize = true;
            IdentificationMarksLabel.Location = new System.Drawing.Point(384, 145);
            IdentificationMarksLabel.Name = "IdentificationMarksLabel";
            IdentificationMarksLabel.Size = new System.Drawing.Size(67, 13);
            IdentificationMarksLabel.TabIndex = 505;
            IdentificationMarksLabel.Text = "Identification";
            // 
            // Label13
            // 
            Label13.AutoSize = true;
            Label13.Location = new System.Drawing.Point(13, 144);
            Label13.Name = "Label13";
            Label13.Size = new System.Drawing.Size(42, 13);
            Label13.TabIndex = 128;
            Label13.Text = "Gender";
            // 
            // DateofBirthLabel
            // 
            DateofBirthLabel.AutoSize = true;
            DateofBirthLabel.Location = new System.Drawing.Point(380, 234);
            DateofBirthLabel.Name = "DateofBirthLabel";
            DateofBirthLabel.Size = new System.Drawing.Size(66, 13);
            DateofBirthLabel.TabIndex = 127;
            DateofBirthLabel.Text = "Date of Birth";
            // 
            // SpouseNameLabel
            // 
            SpouseNameLabel.AutoSize = true;
            SpouseNameLabel.Location = new System.Drawing.Point(13, 114);
            SpouseNameLabel.Name = "SpouseNameLabel";
            SpouseNameLabel.Size = new System.Drawing.Size(74, 13);
            SpouseNameLabel.TabIndex = 126;
            SpouseNameLabel.Text = "Spouse Name";
            // 
            // ReligionIDLabel
            // 
            ReligionIDLabel.AutoSize = true;
            ReligionIDLabel.Location = new System.Drawing.Point(384, 84);
            ReligionIDLabel.Name = "ReligionIDLabel";
            ReligionIDLabel.Size = new System.Drawing.Size(45, 13);
            ReligionIDLabel.TabIndex = 124;
            ReligionIDLabel.Text = "Religion";
            // 
            // NationalityIDLabel
            // 
            NationalityIDLabel.AutoSize = true;
            NationalityIDLabel.Location = new System.Drawing.Point(384, 54);
            NationalityIDLabel.Name = "NationalityIDLabel";
            NationalityIDLabel.Size = new System.Drawing.Size(56, 13);
            NationalityIDLabel.TabIndex = 123;
            NationalityIDLabel.Text = "Nationality";
            // 
            // MothersNameLabel
            // 
            MothersNameLabel.AutoSize = true;
            MothersNameLabel.Location = new System.Drawing.Point(13, 84);
            MothersNameLabel.Name = "MothersNameLabel";
            MothersNameLabel.Size = new System.Drawing.Size(78, 13);
            MothersNameLabel.TabIndex = 50;
            MothersNameLabel.Text = "Mother\'s Name";
            // 
            // FathersNameLabel
            // 
            FathersNameLabel.AutoSize = true;
            FathersNameLabel.Location = new System.Drawing.Point(13, 54);
            FathersNameLabel.Name = "FathersNameLabel";
            FathersNameLabel.Size = new System.Drawing.Size(75, 13);
            FathersNameLabel.TabIndex = 16;
            FathersNameLabel.Text = "Father\'s Name";
            // 
            // Label25
            // 
            Label25.AutoSize = true;
            Label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label25.Location = new System.Drawing.Point(5, 15);
            Label25.Name = "Label25";
            Label25.Size = new System.Drawing.Size(77, 13);
            Label25.TabIndex = 39;
            Label25.Text = "Contact Info";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(16, 171);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(64, 13);
            label1.TabIndex = 503;
            label1.Text = "Work Policy";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(16, 199);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(68, 13);
            label2.TabIndex = 506;
            label2.Text = "Leave Policy";
            // 
            // Label26
            // 
            Label26.AutoSize = true;
            Label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label26.Location = new System.Drawing.Point(558, 9);
            Label26.Name = "Label26";
            Label26.Size = new System.Drawing.Size(40, 13);
            Label26.TabIndex = 100;
            Label26.Text = "Photo";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label7.Location = new System.Drawing.Point(1, 3);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(82, 13);
            label7.TabIndex = 101;
            label7.Text = "Personal Info";
            // 
            // Label16
            // 
            Label16.AutoSize = true;
            Label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            Label16.Location = new System.Drawing.Point(3, 212);
            Label16.Name = "Label16";
            Label16.Size = new System.Drawing.Size(84, 13);
            Label16.TabIndex = 119;
            Label16.Text = "Bank Identity";
            // 
            // TransactionTypeIDLabel
            // 
            TransactionTypeIDLabel.AutoSize = true;
            TransactionTypeIDLabel.Location = new System.Drawing.Point(14, 246);
            TransactionTypeIDLabel.Name = "TransactionTypeIDLabel";
            TransactionTypeIDLabel.Size = new System.Drawing.Size(93, 13);
            TransactionTypeIDLabel.TabIndex = 925;
            TransactionTypeIDLabel.Text = "Transaction Type ";
            // 
            // LocalMobileLabel
            // 
            LocalMobileLabel.AutoSize = true;
            LocalMobileLabel.Location = new System.Drawing.Point(6, 294);
            LocalMobileLabel.Name = "LocalMobileLabel";
            LocalMobileLabel.Size = new System.Drawing.Size(38, 13);
            LocalMobileLabel.TabIndex = 4;
            LocalMobileLabel.Text = "Mobile";
            // 
            // lblWorkLocation
            // 
            lblWorkLocation.AutoSize = true;
            lblWorkLocation.Location = new System.Drawing.Point(325, 20);
            lblWorkLocation.Name = "lblWorkLocation";
            lblWorkLocation.Size = new System.Drawing.Size(77, 13);
            lblWorkLocation.TabIndex = 100014;
            lblWorkLocation.Text = "Work Location";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(363, 246);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(51, 13);
            label5.TabIndex = 928;
            label5.Text = "PersonID";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(374, 212);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(40, 13);
            label4.TabIndex = 100015;
            label4.Text = "Dates";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(16, 259);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(80, 13);
            label6.TabIndex = 100027;
            label6.Text = "Vacation Policy";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(16, 299);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(69, 13);
            label3.TabIndex = 100029;
            label3.Text = "Reporting To";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(13, 24);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(71, 13);
            label11.TabIndex = 100026;
            label11.Text = "Marital Status";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(348, 81);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(101, 13);
            label12.TabIndex = 515;
            label12.Text = "Emergency Address";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Location = new System.Drawing.Point(348, 129);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(94, 13);
            label15.TabIndex = 518;
            label15.Text = "Emergency Phone";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(348, 233);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(67, 13);
            label17.TabIndex = 522;
            label17.Text = "Local Phone";
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Location = new System.Drawing.Point(348, 181);
            label18.Name = "label18";
            label18.Size = new System.Drawing.Size(74, 13);
            label18.TabIndex = 519;
            label18.Text = "Local Address";
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Location = new System.Drawing.Point(384, 22);
            label22.Name = "label22";
            label22.Size = new System.Drawing.Size(43, 13);
            label22.TabIndex = 100029;
            label22.Text = "Country";
            // 
            // EmailLabel
            // 
            EmailLabel.AutoSize = true;
            EmailLabel.Location = new System.Drawing.Point(6, 191);
            EmailLabel.Name = "EmailLabel";
            EmailLabel.Size = new System.Drawing.Size(32, 13);
            EmailLabel.TabIndex = 14;
            EmailLabel.Text = "Email";
            // 
            // LocalAddressLabel
            // 
            LocalAddressLabel.AutoSize = true;
            LocalAddressLabel.Location = new System.Drawing.Point(6, 23);
            LocalAddressLabel.Name = "LocalAddressLabel";
            LocalAddressLabel.Size = new System.Drawing.Size(69, 13);
            LocalAddressLabel.TabIndex = 10;
            LocalAddressLabel.Text = "Address Ln 1";
            // 
            // LocalPhoneLabel
            // 
            LocalPhoneLabel.AutoSize = true;
            LocalPhoneLabel.Location = new System.Drawing.Point(6, 163);
            LocalPhoneLabel.Name = "LocalPhoneLabel";
            LocalPhoneLabel.Size = new System.Drawing.Size(38, 13);
            LocalPhoneLabel.TabIndex = 12;
            LocalPhoneLabel.Text = "Phone";
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Location = new System.Drawing.Point(13, 174);
            label23.Name = "label23";
            label23.Size = new System.Drawing.Size(36, 13);
            label23.TabIndex = 100032;
            label23.Text = "Grade";
            // 
            // lblVisaObtained
            // 
            lblVisaObtained.AutoSize = true;
            lblVisaObtained.Location = new System.Drawing.Point(325, 47);
            lblVisaObtained.Name = "lblVisaObtained";
            lblVisaObtained.Size = new System.Drawing.Size(73, 13);
            lblVisaObtained.TabIndex = 100038;
            lblVisaObtained.Text = "Visa Obtained";
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Location = new System.Drawing.Point(6, 51);
            label28.Name = "label28";
            label28.Size = new System.Drawing.Size(69, 13);
            label28.TabIndex = 19;
            label28.Text = "Address Ln 2";
            // 
            // label29
            // 
            label29.AutoSize = true;
            label29.Location = new System.Drawing.Point(6, 79);
            label29.Name = "label29";
            label29.Size = new System.Drawing.Size(24, 13);
            label29.TabIndex = 20;
            label29.Text = "City";
            // 
            // label30
            // 
            label30.AutoSize = true;
            label30.Location = new System.Drawing.Point(6, 107);
            label30.Name = "label30";
            label30.Size = new System.Drawing.Size(79, 13);
            label30.TabIndex = 21;
            label30.Text = "State/Province";
            // 
            // label31
            // 
            label31.AutoSize = true;
            label31.Location = new System.Drawing.Point(6, 135);
            label31.Name = "label31";
            label31.Size = new System.Drawing.Size(50, 13);
            label31.TabIndex = 22;
            label31.Text = "Zip Code";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label9.Location = new System.Drawing.Point(10, 220);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(65, 13);
            label9.TabIndex = 516;
            label9.Text = "Other Info";
            // 
            // MothertongueIDLabel
            // 
            MothertongueIDLabel.AutoSize = true;
            MothertongueIDLabel.Location = new System.Drawing.Point(10, 15);
            MothertongueIDLabel.Name = "MothertongueIDLabel";
            MothertongueIDLabel.Size = new System.Drawing.Size(73, 13);
            MothertongueIDLabel.TabIndex = 520;
            MothertongueIDLabel.Text = "Mothertongue";
            // 
            // lblNoticePeriod
            // 
            lblNoticePeriod.AutoSize = true;
            lblNoticePeriod.Location = new System.Drawing.Point(325, 74);
            lblNoticePeriod.Name = "lblNoticePeriod";
            lblNoticePeriod.Size = new System.Drawing.Size(71, 13);
            lblNoticePeriod.TabIndex = 100039;
            lblNoticePeriod.Text = "Notice Period";
            // 
            // lblCommissionStructure
            // 
            lblCommissionStructure.AutoSize = true;
            lblCommissionStructure.Location = new System.Drawing.Point(325, 101);
            lblCommissionStructure.Name = "lblCommissionStructure";
            lblCommissionStructure.Size = new System.Drawing.Size(108, 13);
            lblCommissionStructure.TabIndex = 100042;
            lblCommissionStructure.Text = "Commission Structure";
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.Location = new System.Drawing.Point(325, 130);
            label24.Name = "label24";
            label24.Size = new System.Drawing.Size(72, 13);
            label24.TabIndex = 100043;
            label24.Text = "Process Type";
            // 
            // label32
            // 
            label32.AutoSize = true;
            label32.Location = new System.Drawing.Point(16, 327);
            label32.Name = "label32";
            label32.Size = new System.Drawing.Size(31, 13);
            label32.TabIndex = 100031;
            label32.Text = "HOD";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(7, 528);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 1;
            this.BtnSave.Text = "&Save";
            this.TipEmployee.SetToolTip(this.BtnSave, "Click here to save the Executive information");
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(657, 528);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 3;
            this.BtnCancel.Text = "&Cancel";
            this.TipEmployee.SetToolTip(this.BtnCancel, "Click here to close the form");
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(576, 528);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "&Ok";
            this.TipEmployee.SetToolTip(this.BtnOk, "Click here to save the Executive information and close the form");
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // BnEmployee
            // 
            this.BnEmployee.AddNewItem = null;
            this.BnEmployee.BackColor = System.Drawing.Color.Transparent;
            this.BnEmployee.CountItem = this.BindingNavigatorCountItem;
            this.BnEmployee.DeleteItem = null;
            this.BnEmployee.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BindingNavigatorMoveFirstItem,
            this.BindingNavigatorMovePreviousItem,
            this.BnSeparator,
            this.BindingNavigatornPositionItem,
            this.BnSeparator1,
            this.BindingNavigatorCountItem,
            this.BindingNavigatorMoveNextItem,
            this.BindingNavigatorMoveLastItem,
            this.BnSeparator2,
            this.BindingNavigatorAddNewItem,
            this.BindingNavigatorSaveItem,
            this.BindingNavigatorDeleteItem,
            this.BtnClear,
            this.toolStripSeparator3,
            this.bnMoreActions,
            this.toolStripSeparator2,
            this.BtnPrint,
            this.BtnEmail,
            this.toolStripSeparator4,
            this.BtnHelp});
            this.BnEmployee.Location = new System.Drawing.Point(0, 0);
            this.BnEmployee.MoveFirstItem = null;
            this.BnEmployee.MoveLastItem = null;
            this.BnEmployee.MoveNextItem = null;
            this.BnEmployee.MovePreviousItem = null;
            this.BnEmployee.Name = "BnEmployee";
            this.BnEmployee.PositionItem = this.BindingNavigatornPositionItem;
            this.BnEmployee.Size = new System.Drawing.Size(740, 25);
            this.BnEmployee.TabIndex = 0;
            this.BnEmployee.Text = "BindingNavigator1";
            // 
            // BindingNavigatorCountItem
            // 
            this.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem";
            this.BindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.BindingNavigatorCountItem.Text = "of {0}";
            this.BindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // BindingNavigatorMoveFirstItem
            // 
            this.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveFirstItem.Image")));
            this.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem";
            this.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveFirstItem.Text = "Move first";
            this.BindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.BindingNavigatorMoveFirstItem_Click);
            // 
            // BindingNavigatorMovePreviousItem
            // 
            this.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMovePreviousItem.Image")));
            this.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem";
            this.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMovePreviousItem.Text = "Move previous";
            this.BindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.BindingNavigatorMovePreviousItem_Click);
            // 
            // BnSeparator
            // 
            this.BnSeparator.Name = "BnSeparator";
            this.BnSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatornPositionItem
            // 
            this.BindingNavigatornPositionItem.AccessibleName = "Position";
            this.BindingNavigatornPositionItem.AutoSize = false;
            this.BindingNavigatornPositionItem.Name = "BindingNavigatornPositionItem";
            this.BindingNavigatornPositionItem.Size = new System.Drawing.Size(50, 23);
            this.BindingNavigatornPositionItem.Text = "0";
            this.BindingNavigatornPositionItem.ToolTipText = "Current position";
            // 
            // BnSeparator1
            // 
            this.BnSeparator1.Name = "BnSeparator1";
            this.BnSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorMoveNextItem
            // 
            this.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveNextItem.Image")));
            this.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem";
            this.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveNextItem.Text = "Move next";
            this.BindingNavigatorMoveNextItem.Click += new System.EventHandler(this.BindingNavigatorMoveNextItem_Click);
            // 
            // BindingNavigatorMoveLastItem
            // 
            this.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorMoveLastItem.Image")));
            this.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem";
            this.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorMoveLastItem.Text = "Move last";
            this.BindingNavigatorMoveLastItem.Click += new System.EventHandler(this.BindingNavigatorMoveLastItem_Click);
            // 
            // BnSeparator2
            // 
            this.BnSeparator2.Name = "BnSeparator2";
            this.BnSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BindingNavigatorAddNewItem
            // 
            this.BindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorAddNewItem.Image")));
            this.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem";
            this.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorAddNewItem.ToolTipText = "Add New";
            this.BindingNavigatorAddNewItem.Click += new System.EventHandler(this.BindingNavigatorAddNewItem_Click);
            // 
            // BindingNavigatorSaveItem
            // 
            this.BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorSaveItem.Image")));
            this.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem";
            this.BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorSaveItem.ToolTipText = "Save";
            this.BindingNavigatorSaveItem.Click += new System.EventHandler(this.BindingNavigatorSaveItem_Click);
            // 
            // BindingNavigatorDeleteItem
            // 
            this.BindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("BindingNavigatorDeleteItem.Image")));
            this.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem";
            this.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.BindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.BindingNavigatorDeleteItem.ToolTipText = "Delete";
            this.BindingNavigatorDeleteItem.Click += new System.EventHandler(this.BindingNavigatorDeleteItem_Click);
            // 
            // BtnClear
            // 
            this.BtnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnClear.Image = ((System.Drawing.Image)(resources.GetObject("BtnClear.Image")));
            this.BtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(23, 22);
            this.BtnClear.ToolTipText = "Clear";
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoreActions
            // 
            this.bnMoreActions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuLeave,
            this.mnuLoan,
            this.mnuSalaryAdvance,
            this.toolStripSeparator5,
            this.btnSalaryStructure,
            this.mnuLeaveStructure,
            this.toolStripSeparator1,
            this.passportToolStripMenuItem,
            this.visaToolStripMenuItem,
            this.licenseToolStripMenuItem,
            this.qualificationToolStripMenuItem,
            this.nationalIDCardToolStripMenuItem,
            this.insuranceCardToolStripMenuItem,
            this.labourCardToolStripMenuItem,
            this.healthCardToolStripMenuItem,
            this.otherDocumentsToolStripMenuItem});
            this.bnMoreActions.Image = global::MyPayfriend.Properties.Resources.Add1;
            this.bnMoreActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnMoreActions.Name = "bnMoreActions";
            this.bnMoreActions.Size = new System.Drawing.Size(76, 22);
            this.bnMoreActions.Text = "Actions";
            this.bnMoreActions.ToolTipText = "Actions";
            this.bnMoreActions.Click += new System.EventHandler(this.bnMoreActions_Click);
            // 
            // mnuLeave
            // 
            this.mnuLeave.Image = global::MyPayfriend.Properties.Resources.leave_entry;
            this.mnuLeave.Name = "mnuLeave";
            this.mnuLeave.Size = new System.Drawing.Size(168, 22);
            this.mnuLeave.Text = "&Leave";
            this.mnuLeave.Click += new System.EventHandler(this.mnuLeave_Click);
            // 
            // mnuLoan
            // 
            this.mnuLoan.Image = global::MyPayfriend.Properties.Resources.loans;
            this.mnuLoan.Name = "mnuLoan";
            this.mnuLoan.Size = new System.Drawing.Size(168, 22);
            this.mnuLoan.Text = "L&oan";
            this.mnuLoan.Click += new System.EventHandler(this.mnuLoan_Click);
            // 
            // mnuSalaryAdvance
            // 
            this.mnuSalaryAdvance.Image = global::MyPayfriend.Properties.Resources.Salary_Advance2;
            this.mnuSalaryAdvance.Name = "mnuSalaryAdvance";
            this.mnuSalaryAdvance.Size = new System.Drawing.Size(168, 22);
            this.mnuSalaryAdvance.Text = "Salary &Advance";
            this.mnuSalaryAdvance.Click += new System.EventHandler(this.mnuSalaryAdvance_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(165, 6);
            // 
            // btnSalaryStructure
            // 
            this.btnSalaryStructure.Image = global::MyPayfriend.Properties.Resources.salarystructure;
            this.btnSalaryStructure.Name = "btnSalaryStructure";
            this.btnSalaryStructure.Size = new System.Drawing.Size(168, 22);
            this.btnSalaryStructure.Text = "&Salary Structure";
            this.btnSalaryStructure.Click += new System.EventHandler(this.btnSalaryStructure_Click);
            // 
            // mnuLeaveStructure
            // 
            this.mnuLeaveStructure.Image = global::MyPayfriend.Properties.Resources.LeaveStructure;
            this.mnuLeaveStructure.Name = "mnuLeaveStructure";
            this.mnuLeaveStructure.Size = new System.Drawing.Size(168, 22);
            this.mnuLeaveStructure.Text = "L&eave Structure";
            this.mnuLeaveStructure.Click += new System.EventHandler(this.mnuLeaveStructure_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(165, 6);
            // 
            // passportToolStripMenuItem
            // 
            this.passportToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.Passport;
            this.passportToolStripMenuItem.Name = "passportToolStripMenuItem";
            this.passportToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.passportToolStripMenuItem.Text = "&Passport";
            this.passportToolStripMenuItem.Click += new System.EventHandler(this.passportToolStripMenuItem_Click);
            // 
            // visaToolStripMenuItem
            // 
            this.visaToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.Visas;
            this.visaToolStripMenuItem.Name = "visaToolStripMenuItem";
            this.visaToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.visaToolStripMenuItem.Text = "&Visa";
            this.visaToolStripMenuItem.Click += new System.EventHandler(this.visaToolStripMenuItem_Click);
            // 
            // licenseToolStripMenuItem
            // 
            this.licenseToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.DrivingLicense;
            this.licenseToolStripMenuItem.Name = "licenseToolStripMenuItem";
            this.licenseToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.licenseToolStripMenuItem.Text = "Li&cense";
            this.licenseToolStripMenuItem.Click += new System.EventHandler(this.licenseToolStripMenuItem_Click);
            // 
            // qualificationToolStripMenuItem
            // 
            this.qualificationToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.Qualification;
            this.qualificationToolStripMenuItem.Name = "qualificationToolStripMenuItem";
            this.qualificationToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.qualificationToolStripMenuItem.Text = "&Qualification";
            this.qualificationToolStripMenuItem.Click += new System.EventHandler(this.qualificationToolStripMenuItem_Click);
            // 
            // nationalIDCardToolStripMenuItem
            // 
            this.nationalIDCardToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.EmiritsCard;
            this.nationalIDCardToolStripMenuItem.Name = "nationalIDCardToolStripMenuItem";
            this.nationalIDCardToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.nationalIDCardToolStripMenuItem.Text = "&National ID  Card";
            this.nationalIDCardToolStripMenuItem.Click += new System.EventHandler(this.nationalIDCardToolStripMenuItem_Click);
            // 
            // insuranceCardToolStripMenuItem
            // 
            this.insuranceCardToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.InsuranceCard;
            this.insuranceCardToolStripMenuItem.Name = "insuranceCardToolStripMenuItem";
            this.insuranceCardToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.insuranceCardToolStripMenuItem.Text = "&Insurance Card";
            this.insuranceCardToolStripMenuItem.Click += new System.EventHandler(this.insuranceCardToolStripMenuItem_Click);
            // 
            // labourCardToolStripMenuItem
            // 
            this.labourCardToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.LaburCard;
            this.labourCardToolStripMenuItem.Name = "labourCardToolStripMenuItem";
            this.labourCardToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.labourCardToolStripMenuItem.Text = "La&bour Card";
            this.labourCardToolStripMenuItem.Click += new System.EventHandler(this.labourCardToolStripMenuItem_Click);
            // 
            // healthCardToolStripMenuItem
            // 
            this.healthCardToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.HealthCard;
            this.healthCardToolStripMenuItem.Name = "healthCardToolStripMenuItem";
            this.healthCardToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.healthCardToolStripMenuItem.Text = "&Health Card";
            this.healthCardToolStripMenuItem.Click += new System.EventHandler(this.healthCardToolStripMenuItem_Click);
            // 
            // otherDocumentsToolStripMenuItem
            // 
            this.otherDocumentsToolStripMenuItem.Image = global::MyPayfriend.Properties.Resources.OtherDocument;
            this.otherDocumentsToolStripMenuItem.Name = "otherDocumentsToolStripMenuItem";
            this.otherDocumentsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.otherDocumentsToolStripMenuItem.Text = "&Other Documents";
            this.otherDocumentsToolStripMenuItem.Click += new System.EventHandler(this.otherDocumentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnPrint
            // 
            this.BtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(23, 22);
            this.BtnPrint.ToolTipText = "Print";
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnEmail
            // 
            this.BtnEmail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnEmail.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmail.Image")));
            this.BtnEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnEmail.Name = "BtnEmail";
            this.BtnEmail.Size = new System.Drawing.Size(23, 22);
            this.BtnEmail.ToolTipText = "Email";
            this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // BtnHelp
            // 
            this.BtnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnHelp.Image = ((System.Drawing.Image)(resources.GetObject("BtnHelp.Image")));
            this.BtnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnHelp.Name = "BtnHelp";
            this.BtnHelp.Size = new System.Drawing.Size(23, 22);
            this.BtnHelp.ToolTipText = "Help";
            this.BtnHelp.Click += new System.EventHandler(this.BnHelpButton_Click);
            // 
            // ErrEmployee
            // 
            this.ErrEmployee.ContainerControl = this;
            this.ErrEmployee.RightToLeft = true;
            // 
            // dtpDateofBirth
            // 
            this.dtpDateofBirth.CustomFormat = "dd-MMM-yyyy";
            this.dtpDateofBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ErrEmployee.SetIconAlignment(this.dtpDateofBirth, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.dtpDateofBirth.Location = new System.Drawing.Point(480, 228);
            this.dtpDateofBirth.Name = "dtpDateofBirth";
            this.dtpDateofBirth.Size = new System.Drawing.Size(102, 20);
            this.dtpDateofBirth.TabIndex = 21;
            this.dtpDateofBirth.ValueChanged += new System.EventHandler(this.changestatus);
            // 
            // TmEmployee
            // 
            this.TmEmployee.Interval = 1000;
            this.TmEmployee.Tick += new System.EventHandler(this.TmExecutive_Tick);
            // 
            // TxtEmployeeNumber
            // 
            this.TxtEmployeeNumber.BackColor = System.Drawing.SystemColors.Info;
            this.TxtEmployeeNumber.Location = new System.Drawing.Point(14, 21);
            this.TxtEmployeeNumber.MaxLength = 15;
            this.TxtEmployeeNumber.Name = "TxtEmployeeNumber";
            this.TxtEmployeeNumber.Size = new System.Drawing.Size(93, 20);
            this.TxtEmployeeNumber.TabIndex = 0;
            this.TxtEmployeeNumber.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // LblEmployeeNumber
            // 
            this.LblEmployeeNumber.AutoSize = true;
            this.LblEmployeeNumber.Location = new System.Drawing.Point(14, 5);
            this.LblEmployeeNumber.Name = "LblEmployeeNumber";
            this.LblEmployeeNumber.Size = new System.Drawing.Size(93, 13);
            this.LblEmployeeNumber.TabIndex = 0;
            this.LblEmployeeNumber.Text = "Employee Number";
            // 
            // LblSalutation
            // 
            this.LblSalutation.AutoSize = true;
            this.LblSalutation.Location = new System.Drawing.Point(130, 5);
            this.LblSalutation.Name = "LblSalutation";
            this.LblSalutation.Size = new System.Drawing.Size(54, 13);
            this.LblSalutation.TabIndex = 1;
            this.LblSalutation.Text = "Salutation";
            // 
            // TxtFirstName
            // 
            this.TxtFirstName.BackColor = System.Drawing.SystemColors.Info;
            this.TxtFirstName.Location = new System.Drawing.Point(247, 21);
            this.TxtFirstName.MaxLength = 25;
            this.TxtFirstName.Name = "TxtFirstName";
            this.TxtFirstName.Size = new System.Drawing.Size(152, 20);
            this.TxtFirstName.TabIndex = 3;
            this.TxtFirstName.TextChanged += new System.EventHandler(this.TxtFirstName_TextChanged);
            // 
            // LblFirstName
            // 
            this.LblFirstName.AutoSize = true;
            this.LblFirstName.Location = new System.Drawing.Point(245, 5);
            this.LblFirstName.Name = "LblFirstName";
            this.LblFirstName.Size = new System.Drawing.Size(57, 13);
            this.LblFirstName.TabIndex = 7;
            this.LblFirstName.Text = "First Name";
            // 
            // TxtMiddleName
            // 
            this.TxtMiddleName.Location = new System.Drawing.Point(404, 21);
            this.TxtMiddleName.MaxLength = 25;
            this.TxtMiddleName.Name = "TxtMiddleName";
            this.TxtMiddleName.Size = new System.Drawing.Size(152, 20);
            this.TxtMiddleName.TabIndex = 4;
            this.TxtMiddleName.TextChanged += new System.EventHandler(this.TxtMiddleName_TextChanged);
            // 
            // LblMiddleName
            // 
            this.LblMiddleName.AutoSize = true;
            this.LblMiddleName.Location = new System.Drawing.Point(402, 5);
            this.LblMiddleName.Name = "LblMiddleName";
            this.LblMiddleName.Size = new System.Drawing.Size(69, 13);
            this.LblMiddleName.TabIndex = 9;
            this.LblMiddleName.Text = "Middle Name";
            // 
            // TxtLastName
            // 
            this.TxtLastName.Location = new System.Drawing.Point(561, 21);
            this.TxtLastName.MaxLength = 25;
            this.TxtLastName.Name = "TxtLastName";
            this.TxtLastName.Size = new System.Drawing.Size(158, 20);
            this.TxtLastName.TabIndex = 5;
            this.TxtLastName.TextChanged += new System.EventHandler(this.TxtLastName_TextChanged);
            // 
            // LblLastName
            // 
            this.LblLastName.AutoSize = true;
            this.LblLastName.Location = new System.Drawing.Point(561, 5);
            this.LblLastName.Name = "LblLastName";
            this.LblLastName.Size = new System.Drawing.Size(58, 13);
            this.LblLastName.TabIndex = 11;
            this.LblLastName.Text = "Last Name";
            // 
            // CboSalutation
            // 
            this.CboSalutation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSalutation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSalutation.BackColor = System.Drawing.SystemColors.Info;
            this.CboSalutation.DropDownHeight = 75;
            this.CboSalutation.DropDownWidth = 60;
            this.CboSalutation.FormattingEnabled = true;
            this.CboSalutation.IntegralHeight = false;
            this.CboSalutation.Location = new System.Drawing.Point(133, 21);
            this.CboSalutation.Name = "CboSalutation";
            this.CboSalutation.Size = new System.Drawing.Size(62, 21);
            this.CboSalutation.TabIndex = 1;
            this.CboSalutation.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.CboSalutation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // PnlEmployee
            // 
            this.PnlEmployee.Controls.Add(this.panel2);
            this.PnlEmployee.Controls.Add(this.panel1);
            this.PnlEmployee.Location = new System.Drawing.Point(0, 53);
            this.PnlEmployee.Name = "PnlEmployee";
            this.PnlEmployee.Size = new System.Drawing.Size(739, 469);
            this.PnlEmployee.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TbEmployee);
            this.panel2.Location = new System.Drawing.Point(3, 82);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(733, 384);
            this.panel2.TabIndex = 19;
            // 
            // TbEmployee
            // 
            this.TbEmployee.Controls.Add(this.TpGeneral);
            this.TbEmployee.Controls.Add(this.Tpwork);
            this.TbEmployee.Controls.Add(this.TbContacts);
            this.TbEmployee.Controls.Add(this.TbOtherInfo);
            this.TbEmployee.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TbEmployee.Location = new System.Drawing.Point(0, 3);
            this.TbEmployee.Name = "TbEmployee";
            this.TbEmployee.SelectedIndex = 0;
            this.TbEmployee.Size = new System.Drawing.Size(733, 381);
            this.TbEmployee.TabIndex = 0;
            // 
            // TpGeneral
            // 
            this.TpGeneral.AutoScroll = true;
            this.TpGeneral.Controls.Add(label33);
            this.TpGeneral.Controls.Add(this.cboDivision);
            this.TpGeneral.Controls.Add(this.btnDivision);
            this.TpGeneral.Controls.Add(label32);
            this.TpGeneral.Controls.Add(this.chkOCom);
            this.TpGeneral.Controls.Add(this.cboHOD);
            this.TpGeneral.Controls.Add(this.groupBox2);
            this.TpGeneral.Controls.Add(this.lblRejoinDateDesc);
            this.TpGeneral.Controls.Add(this.lblRejoinDate);
            this.TpGeneral.Controls.Add(this.chkOther);
            this.TpGeneral.Controls.Add(this.btnConfirm);
            this.TpGeneral.Controls.Add(this.txtEmployeeFullNameArb);
            this.TpGeneral.Controls.Add(label3);
            this.TpGeneral.Controls.Add(this.cboReportingTo);
            this.TpGeneral.Controls.Add(label6);
            this.TpGeneral.Controls.Add(this.btnVacationPolicy);
            this.TpGeneral.Controls.Add(this.cboVacationPolicy);
            this.TpGeneral.Controls.Add(this.DtpDateofJoining);
            this.TpGeneral.Controls.Add(ProbationEndDateLabel);
            this.TpGeneral.Controls.Add(DateofJoiningLabel);
            this.TpGeneral.Controls.Add(this.DtpProbationEndDate);
            this.TpGeneral.Controls.Add(this.dtpDateofBirth);
            this.TpGeneral.Controls.Add(DateofBirthLabel);
            this.TpGeneral.Controls.Add(this.lblTransferDate);
            this.TpGeneral.Controls.Add(this.lblTransfer);
            this.TpGeneral.Controls.Add(this.RecentPhotoPictureBox);
            this.TpGeneral.Controls.Add(this.txtEmployeeFullName);
            this.TpGeneral.Controls.Add(Label26);
            this.TpGeneral.Controls.Add(label4);
            this.TpGeneral.Controls.Add(this.BtnAttach);
            this.TpGeneral.Controls.Add(this.BtnContxtMenu1);
            this.TpGeneral.Controls.Add(DepartmentIDLabel);
            this.TpGeneral.Controls.Add(DesignationIDLabel);
            this.TpGeneral.Controls.Add(this.cboDepartment);
            this.TpGeneral.Controls.Add(this.cboEmploymentType);
            this.TpGeneral.Controls.Add(this.cboDesignation);
            this.TpGeneral.Controls.Add(EmploymentTypeLabel);
            this.TpGeneral.Controls.Add(this.BtnDesignation);
            this.TpGeneral.Controls.Add(this.BtnEmptype);
            this.TpGeneral.Controls.Add(this.BtnDepartment);
            this.TpGeneral.Controls.Add(label2);
            this.TpGeneral.Controls.Add(this.btnLeavePolicy);
            this.TpGeneral.Controls.Add(this.cboLeavePolicy);
            this.TpGeneral.Controls.Add(this.BtnWorkstatus);
            this.TpGeneral.Controls.Add(this.btnWorkPolicy);
            this.TpGeneral.Controls.Add(WorkStatusIDLabel);
            this.TpGeneral.Controls.Add(this.cboWorkStatus);
            this.TpGeneral.Controls.Add(this.cboWorkPolicy);
            this.TpGeneral.Controls.Add(label1);
            this.TpGeneral.Controls.Add(Label8);
            this.TpGeneral.Controls.Add(this.cboWorkingAt);
            this.TpGeneral.Controls.Add(WorkingAtLabel);
            this.TpGeneral.Controls.Add(this.ShapeContainer1);
            this.TpGeneral.Location = new System.Drawing.Point(4, 22);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.TpGeneral.Size = new System.Drawing.Size(725, 355);
            this.TpGeneral.TabIndex = 1;
            this.TpGeneral.Text = "Employment";
            this.TpGeneral.UseVisualStyleBackColor = true;
            // 
            // chkOCom
            // 
            this.chkOCom.AutoSize = true;
            this.chkOCom.Location = new System.Drawing.Point(340, 332);
            this.chkOCom.Name = "chkOCom";
            this.chkOCom.Size = new System.Drawing.Size(152, 17);
            this.chkOCom.TabIndex = 20;
            this.chkOCom.Text = "Select from other company";
            this.chkOCom.UseVisualStyleBackColor = true;
            this.chkOCom.CheckedChanged += new System.EventHandler(this.chkOCom_CheckedChanged);
            // 
            // cboHOD
            // 
            this.cboHOD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboHOD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboHOD.BackColor = System.Drawing.Color.White;
            this.cboHOD.DropDownHeight = 134;
            this.cboHOD.FormattingEnabled = true;
            this.cboHOD.IntegralHeight = false;
            this.cboHOD.Location = new System.Drawing.Point(126, 327);
            this.cboHOD.Name = "cboHOD";
            this.cboHOD.Size = new System.Drawing.Size(205, 21);
            this.cboHOD.TabIndex = 19;
            this.cboHOD.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboHOD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtpNextAppraisalDate);
            this.groupBox2.Location = new System.Drawing.Point(588, 229);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(133, 50);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Next Appraisal Date";
            // 
            // dtpNextAppraisalDate
            // 
            this.dtpNextAppraisalDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpNextAppraisalDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNextAppraisalDate.Location = new System.Drawing.Point(9, 19);
            this.dtpNextAppraisalDate.Name = "dtpNextAppraisalDate";
            this.dtpNextAppraisalDate.Size = new System.Drawing.Size(107, 20);
            this.dtpNextAppraisalDate.TabIndex = 0;
            this.dtpNextAppraisalDate.ValueChanged += new System.EventHandler(this.dtpNextAppraisalDate_ValueChanged);
            // 
            // lblRejoinDateDesc
            // 
            this.lblRejoinDateDesc.AutoSize = true;
            this.lblRejoinDateDesc.Location = new System.Drawing.Point(451, 312);
            this.lblRejoinDateDesc.Name = "lblRejoinDateDesc";
            this.lblRejoinDateDesc.Size = new System.Drawing.Size(60, 13);
            this.lblRejoinDateDesc.TabIndex = 24;
            this.lblRejoinDateDesc.Text = "RejoinDate";
            this.lblRejoinDateDesc.Visible = false;
            // 
            // lblRejoinDate
            // 
            this.lblRejoinDate.AutoSize = true;
            this.lblRejoinDate.Location = new System.Drawing.Point(380, 312);
            this.lblRejoinDate.Name = "lblRejoinDate";
            this.lblRejoinDate.Size = new System.Drawing.Size(63, 13);
            this.lblRejoinDate.TabIndex = 23;
            this.lblRejoinDate.Text = "Rejoin Date";
            this.lblRejoinDate.Visible = false;
            // 
            // chkOther
            // 
            this.chkOther.AutoSize = true;
            this.chkOther.Location = new System.Drawing.Point(126, 280);
            this.chkOther.Name = "chkOther";
            this.chkOther.Size = new System.Drawing.Size(152, 17);
            this.chkOther.TabIndex = 17;
            this.chkOther.Text = "Select from other company";
            this.chkOther.UseVisualStyleBackColor = true;
            this.chkOther.CheckedChanged += new System.EventHandler(this.chkOther_CheckedChanged);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirm.Image")));
            this.btnConfirm.Location = new System.Drawing.Point(588, 286);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(20, 20);
            this.btnConfirm.TabIndex = 25;
            this.TipEmployee.SetToolTip(this.btnConfirm, "Confirm Probation");
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // txtEmployeeFullNameArb
            // 
            this.txtEmployeeFullNameArb.Enabled = false;
            this.txtEmployeeFullNameArb.Location = new System.Drawing.Point(479, 97);
            this.txtEmployeeFullNameArb.MaxLength = 150;
            this.txtEmployeeFullNameArb.Multiline = true;
            this.txtEmployeeFullNameArb.Name = "txtEmployeeFullNameArb";
            this.txtEmployeeFullNameArb.Size = new System.Drawing.Size(12, 20);
            this.txtEmployeeFullNameArb.TabIndex = 100030;
            this.txtEmployeeFullNameArb.Visible = false;
            // 
            // cboReportingTo
            // 
            this.cboReportingTo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReportingTo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReportingTo.BackColor = System.Drawing.Color.White;
            this.cboReportingTo.DropDownHeight = 134;
            this.cboReportingTo.FormattingEnabled = true;
            this.cboReportingTo.IntegralHeight = false;
            this.cboReportingTo.Location = new System.Drawing.Point(126, 299);
            this.cboReportingTo.Name = "cboReportingTo";
            this.cboReportingTo.Size = new System.Drawing.Size(204, 21);
            this.cboReportingTo.TabIndex = 18;
            this.cboReportingTo.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboReportingTo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // btnVacationPolicy
            // 
            this.btnVacationPolicy.Location = new System.Drawing.Point(338, 252);
            this.btnVacationPolicy.Name = "btnVacationPolicy";
            this.btnVacationPolicy.Size = new System.Drawing.Size(28, 22);
            this.btnVacationPolicy.TabIndex = 16;
            this.btnVacationPolicy.Text = "...";
            this.btnVacationPolicy.UseVisualStyleBackColor = true;
            this.btnVacationPolicy.Click += new System.EventHandler(this.btnVacationPolicy_Click);
            // 
            // cboVacationPolicy
            // 
            this.cboVacationPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboVacationPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboVacationPolicy.BackColor = System.Drawing.SystemColors.Window;
            this.cboVacationPolicy.DropDownHeight = 134;
            this.cboVacationPolicy.FormattingEnabled = true;
            this.cboVacationPolicy.IntegralHeight = false;
            this.cboVacationPolicy.Location = new System.Drawing.Point(126, 252);
            this.cboVacationPolicy.Name = "cboVacationPolicy";
            this.cboVacationPolicy.Size = new System.Drawing.Size(205, 21);
            this.cboVacationPolicy.TabIndex = 15;
            this.cboVacationPolicy.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboVacationPolicy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // DtpDateofJoining
            // 
            this.DtpDateofJoining.CustomFormat = "dd-MMM-yyyy";
            this.DtpDateofJoining.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpDateofJoining.Location = new System.Drawing.Point(480, 256);
            this.DtpDateofJoining.Name = "DtpDateofJoining";
            this.DtpDateofJoining.Size = new System.Drawing.Size(102, 20);
            this.DtpDateofJoining.TabIndex = 23;
            this.DtpDateofJoining.ValueChanged += new System.EventHandler(this.changestatus);
            // 
            // DtpProbationEndDate
            // 
            this.DtpProbationEndDate.AllowDrop = true;
            this.DtpProbationEndDate.CustomFormat = "dd-MMM-yyyy";
            this.DtpProbationEndDate.Enabled = false;
            this.DtpProbationEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpProbationEndDate.Location = new System.Drawing.Point(480, 285);
            this.DtpProbationEndDate.Name = "DtpProbationEndDate";
            this.DtpProbationEndDate.Size = new System.Drawing.Size(102, 20);
            this.DtpProbationEndDate.TabIndex = 24;
            this.DtpProbationEndDate.ValueChanged += new System.EventHandler(this.changestatus);
            // 
            // lblTransferDate
            // 
            this.lblTransferDate.AutoSize = true;
            this.lblTransferDate.Location = new System.Drawing.Point(628, 312);
            this.lblTransferDate.Name = "lblTransferDate";
            this.lblTransferDate.Size = new System.Drawing.Size(45, 13);
            this.lblTransferDate.TabIndex = 26;
            this.lblTransferDate.Text = "trnsferdt";
            this.lblTransferDate.Visible = false;
            // 
            // lblTransfer
            // 
            this.lblTransfer.AutoSize = true;
            this.lblTransfer.Location = new System.Drawing.Point(550, 312);
            this.lblTransfer.Name = "lblTransfer";
            this.lblTransfer.Size = new System.Drawing.Size(72, 13);
            this.lblTransfer.TabIndex = 25;
            this.lblTransfer.Text = "Transfer Date";
            this.lblTransfer.Visible = false;
            // 
            // RecentPhotoPictureBox
            // 
            this.RecentPhotoPictureBox.Location = new System.Drawing.Point(562, 29);
            this.RecentPhotoPictureBox.Name = "RecentPhotoPictureBox";
            this.RecentPhotoPictureBox.Size = new System.Drawing.Size(130, 154);
            this.RecentPhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RecentPhotoPictureBox.TabIndex = 1;
            this.RecentPhotoPictureBox.TabStop = false;
            // 
            // txtEmployeeFullName
            // 
            this.txtEmployeeFullName.Enabled = false;
            this.txtEmployeeFullName.Location = new System.Drawing.Point(480, 53);
            this.txtEmployeeFullName.MaxLength = 150;
            this.txtEmployeeFullName.Multiline = true;
            this.txtEmployeeFullName.Name = "txtEmployeeFullName";
            this.txtEmployeeFullName.Size = new System.Drawing.Size(12, 20);
            this.txtEmployeeFullName.TabIndex = 1;
            this.txtEmployeeFullName.Visible = false;
            this.txtEmployeeFullName.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // BtnAttach
            // 
            this.BtnAttach.Location = new System.Drawing.Point(597, 186);
            this.BtnAttach.Name = "BtnAttach";
            this.BtnAttach.Size = new System.Drawing.Size(75, 23);
            this.BtnAttach.TabIndex = 20;
            this.BtnAttach.Text = "Attach";
            this.BtnAttach.UseVisualStyleBackColor = true;
            this.BtnAttach.Click += new System.EventHandler(this.BtnAttach_Click);
            // 
            // BtnContxtMenu1
            // 
            this.BtnContxtMenu1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.BtnContxtMenu1.ContextMenuStrip = this.ContextMenuStrip2;
            this.BtnContxtMenu1.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.BtnContxtMenu1.Location = new System.Drawing.Point(673, 201);
            this.BtnContxtMenu1.Name = "BtnContxtMenu1";
            this.BtnContxtMenu1.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.BtnContxtMenu1.Size = new System.Drawing.Size(20, 23);
            this.BtnContxtMenu1.TabIndex = 21;
            this.BtnContxtMenu1.Text = "6";
            this.BtnContxtMenu1.UseVisualStyleBackColor = true;
            this.BtnContxtMenu1.Click += new System.EventHandler(this.BtnContxtMenu1_Click);
            // 
            // ContextMenuStrip2
            // 
            this.ContextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuAttach,
            this.ToolStripMenuScan,
            this.ToolStripMenuRemove});
            this.ContextMenuStrip2.Name = "ContextMenuStrip2";
            this.ContextMenuStrip2.Size = new System.Drawing.Size(121, 70);
            // 
            // ToolStripMenuAttach
            // 
            this.ToolStripMenuAttach.Name = "ToolStripMenuAttach";
            this.ToolStripMenuAttach.Size = new System.Drawing.Size(120, 22);
            this.ToolStripMenuAttach.Text = "Attach";
            this.ToolStripMenuAttach.Click += new System.EventHandler(this.ToolStripMenuAttach_Click);
            // 
            // ToolStripMenuScan
            // 
            this.ToolStripMenuScan.Name = "ToolStripMenuScan";
            this.ToolStripMenuScan.Size = new System.Drawing.Size(120, 22);
            this.ToolStripMenuScan.Text = "Scan";
            this.ToolStripMenuScan.Visible = false;
            this.ToolStripMenuScan.Click += new System.EventHandler(this.ToolStripMenuScan_Click);
            // 
            // ToolStripMenuRemove
            // 
            this.ToolStripMenuRemove.Name = "ToolStripMenuRemove";
            this.ToolStripMenuRemove.Size = new System.Drawing.Size(120, 22);
            this.ToolStripMenuRemove.Text = "Remove ";
            this.ToolStripMenuRemove.Click += new System.EventHandler(this.ToolStripMenuRemove_Click);
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.BackColor = System.Drawing.SystemColors.Info;
            this.cboDepartment.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboDepartment.DropDownHeight = 134;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.IntegralHeight = false;
            this.cboDepartment.Location = new System.Drawing.Point(126, 84);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(204, 21);
            this.cboDepartment.TabIndex = 3;
            this.cboDepartment.SelectedIndexChanged += new System.EventHandler(this.cboDepartment_SelectedIndexChanged);
            this.cboDepartment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // cboEmploymentType
            // 
            this.cboEmploymentType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmploymentType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmploymentType.DropDownHeight = 134;
            this.cboEmploymentType.FormattingEnabled = true;
            this.cboEmploymentType.IntegralHeight = false;
            this.cboEmploymentType.Location = new System.Drawing.Point(126, 224);
            this.cboEmploymentType.Name = "cboEmploymentType";
            this.cboEmploymentType.Size = new System.Drawing.Size(205, 21);
            this.cboEmploymentType.TabIndex = 13;
            this.cboEmploymentType.SelectedIndexChanged += new System.EventHandler(this.cboEmploymentType_SelectedIndexChanged);
            this.cboEmploymentType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // cboDesignation
            // 
            this.cboDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDesignation.BackColor = System.Drawing.SystemColors.Info;
            this.cboDesignation.DropDownHeight = 134;
            this.cboDesignation.FormattingEnabled = true;
            this.cboDesignation.IntegralHeight = false;
            this.cboDesignation.Location = new System.Drawing.Point(126, 139);
            this.cboDesignation.Name = "cboDesignation";
            this.cboDesignation.Size = new System.Drawing.Size(204, 21);
            this.cboDesignation.TabIndex = 7;
            this.cboDesignation.SelectedIndexChanged += new System.EventHandler(this.cboDesignation_SelectedIndexChanged);
            this.cboDesignation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // BtnDesignation
            // 
            this.BtnDesignation.Location = new System.Drawing.Point(338, 138);
            this.BtnDesignation.Name = "BtnDesignation";
            this.BtnDesignation.Size = new System.Drawing.Size(28, 22);
            this.BtnDesignation.TabIndex = 8;
            this.BtnDesignation.Text = "...";
            this.BtnDesignation.UseVisualStyleBackColor = true;
            this.BtnDesignation.Click += new System.EventHandler(this.BtnDesignation_Click);
            // 
            // BtnEmptype
            // 
            this.BtnEmptype.Location = new System.Drawing.Point(338, 223);
            this.BtnEmptype.Name = "BtnEmptype";
            this.BtnEmptype.Size = new System.Drawing.Size(28, 22);
            this.BtnEmptype.TabIndex = 14;
            this.BtnEmptype.Text = "...";
            this.BtnEmptype.UseVisualStyleBackColor = true;
            this.BtnEmptype.Click += new System.EventHandler(this.BtnEmptype_Click);
            // 
            // BtnDepartment
            // 
            this.BtnDepartment.Location = new System.Drawing.Point(338, 83);
            this.BtnDepartment.Name = "BtnDepartment";
            this.BtnDepartment.Size = new System.Drawing.Size(28, 22);
            this.BtnDepartment.TabIndex = 4;
            this.BtnDepartment.Text = "...";
            this.BtnDepartment.UseVisualStyleBackColor = true;
            this.BtnDepartment.Click += new System.EventHandler(this.BtnDepartment_Click);
            // 
            // btnLeavePolicy
            // 
            this.btnLeavePolicy.Location = new System.Drawing.Point(338, 194);
            this.btnLeavePolicy.Name = "btnLeavePolicy";
            this.btnLeavePolicy.Size = new System.Drawing.Size(28, 22);
            this.btnLeavePolicy.TabIndex = 12;
            this.btnLeavePolicy.Text = "...";
            this.btnLeavePolicy.UseVisualStyleBackColor = true;
            this.btnLeavePolicy.Click += new System.EventHandler(this.btnLeavePolicy_Click);
            // 
            // cboLeavePolicy
            // 
            this.cboLeavePolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLeavePolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLeavePolicy.BackColor = System.Drawing.SystemColors.Window;
            this.cboLeavePolicy.DropDownHeight = 134;
            this.cboLeavePolicy.FormattingEnabled = true;
            this.cboLeavePolicy.IntegralHeight = false;
            this.cboLeavePolicy.Location = new System.Drawing.Point(126, 195);
            this.cboLeavePolicy.Name = "cboLeavePolicy";
            this.cboLeavePolicy.Size = new System.Drawing.Size(205, 21);
            this.cboLeavePolicy.TabIndex = 11;
            this.cboLeavePolicy.SelectedIndexChanged += new System.EventHandler(this.cboLeavePolicy_SelectedIndexChanged);
            this.cboLeavePolicy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboLeavePolicy_KeyPress);
            // 
            // BtnWorkstatus
            // 
            this.BtnWorkstatus.Location = new System.Drawing.Point(338, 55);
            this.BtnWorkstatus.Name = "BtnWorkstatus";
            this.BtnWorkstatus.Size = new System.Drawing.Size(28, 22);
            this.BtnWorkstatus.TabIndex = 2;
            this.BtnWorkstatus.Text = "...";
            this.BtnWorkstatus.UseVisualStyleBackColor = true;
            this.BtnWorkstatus.Click += new System.EventHandler(this.BtnWorkstatus_Click);
            // 
            // btnWorkPolicy
            // 
            this.btnWorkPolicy.Location = new System.Drawing.Point(338, 166);
            this.btnWorkPolicy.Name = "btnWorkPolicy";
            this.btnWorkPolicy.Size = new System.Drawing.Size(28, 22);
            this.btnWorkPolicy.TabIndex = 10;
            this.btnWorkPolicy.Text = "...";
            this.btnWorkPolicy.UseVisualStyleBackColor = true;
            this.btnWorkPolicy.Click += new System.EventHandler(this.btnWorkPolicy_Click);
            // 
            // cboWorkStatus
            // 
            this.cboWorkStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkStatus.BackColor = System.Drawing.SystemColors.Info;
            this.cboWorkStatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboWorkStatus.DropDownHeight = 134;
            this.cboWorkStatus.FormattingEnabled = true;
            this.cboWorkStatus.IntegralHeight = false;
            this.cboWorkStatus.Location = new System.Drawing.Point(126, 56);
            this.cboWorkStatus.Name = "cboWorkStatus";
            this.cboWorkStatus.Size = new System.Drawing.Size(204, 21);
            this.cboWorkStatus.TabIndex = 1;
            this.cboWorkStatus.SelectedIndexChanged += new System.EventHandler(this.cboWorkStatus_SelectedIndexChanged);
            this.cboWorkStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // cboWorkPolicy
            // 
            this.cboWorkPolicy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkPolicy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkPolicy.BackColor = System.Drawing.SystemColors.Info;
            this.cboWorkPolicy.DropDownHeight = 134;
            this.cboWorkPolicy.FormattingEnabled = true;
            this.cboWorkPolicy.IntegralHeight = false;
            this.cboWorkPolicy.Location = new System.Drawing.Point(126, 167);
            this.cboWorkPolicy.Name = "cboWorkPolicy";
            this.cboWorkPolicy.Size = new System.Drawing.Size(204, 21);
            this.cboWorkPolicy.TabIndex = 9;
            this.cboWorkPolicy.SelectedIndexChanged += new System.EventHandler(this.cboWorkPolicy_SelectedIndexChanged);
            this.cboWorkPolicy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboWorkPolicy_KeyPress);
            // 
            // cboWorkingAt
            // 
            this.cboWorkingAt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkingAt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkingAt.BackColor = System.Drawing.SystemColors.Info;
            this.cboWorkingAt.DropDownHeight = 134;
            this.cboWorkingAt.FormattingEnabled = true;
            this.cboWorkingAt.IntegralHeight = false;
            this.cboWorkingAt.Location = new System.Drawing.Point(126, 28);
            this.cboWorkingAt.Name = "cboWorkingAt";
            this.cboWorkingAt.Size = new System.Drawing.Size(205, 21);
            this.cboWorkingAt.TabIndex = 0;
            this.cboWorkingAt.SelectedIndexChanged += new System.EventHandler(this.cboWorkingAt_SelectedIndexChanged);
            this.cboWorkingAt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape8,
            this.lineShape5});
            this.ShapeContainer1.Size = new System.Drawing.Size(719, 349);
            this.ShapeContainer1.TabIndex = 19;
            this.ShapeContainer1.TabStop = false;
            // 
            // lineShape8
            // 
            this.lineShape8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape8.Name = "lineShape8";
            this.lineShape8.X1 = 392;
            this.lineShape8.X2 = 700;
            this.lineShape8.Y1 = 221;
            this.lineShape8.Y2 = 221;
            // 
            // lineShape5
            // 
            this.lineShape5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 28;
            this.lineShape5.X2 = 704;
            this.lineShape5.Y1 = 14;
            this.lineShape5.Y2 = 14;
            // 
            // Tpwork
            // 
            this.Tpwork.Controls.Add(this.cboBloodGroup);
            this.Tpwork.Controls.Add(this.cboGrade);
            this.Tpwork.Controls.Add(label23);
            this.Tpwork.Controls.Add(this.btnGrade);
            this.Tpwork.Controls.Add(this.cboCountry);
            this.Tpwork.Controls.Add(this.btnCountry);
            this.Tpwork.Controls.Add(label22);
            this.Tpwork.Controls.Add(this.cboMaritalStatus);
            this.Tpwork.Controls.Add(label11);
            this.Tpwork.Controls.Add(this.lblDeviceUser);
            this.Tpwork.Controls.Add(this.txtDeviceUserID);
            this.Tpwork.Controls.Add(this.cboNationality);
            this.Tpwork.Controls.Add(this.rdbMale);
            this.Tpwork.Controls.Add(Label13);
            this.Tpwork.Controls.Add(this.rdbFemale);
            this.Tpwork.Controls.Add(this.cboReligion);
            this.Tpwork.Controls.Add(this.BtnReligion);
            this.Tpwork.Controls.Add(ReligionIDLabel);
            this.Tpwork.Controls.Add(this.BtnNationality);
            this.Tpwork.Controls.Add(NationalityIDLabel);
            this.Tpwork.Controls.Add(this.txtPersonID);
            this.Tpwork.Controls.Add(label5);
            this.Tpwork.Controls.Add(this.txtSpouseName);
            this.Tpwork.Controls.Add(SpouseNameLabel);
            this.Tpwork.Controls.Add(Label27);
            this.Tpwork.Controls.Add(this.txtIdentificationMarks);
            this.Tpwork.Controls.Add(IdentificationMarksLabel);
            this.Tpwork.Controls.Add(this.txtMothersName);
            this.Tpwork.Controls.Add(this.txtFathersName);
            this.Tpwork.Controls.Add(FathersNameLabel);
            this.Tpwork.Controls.Add(MothersNameLabel);
            this.Tpwork.Controls.Add(TransactionTypeIDLabel);
            this.Tpwork.Controls.Add(this.BtnEmployeeBankname);
            this.Tpwork.Controls.Add(this.CboEmployeeBankname);
            this.Tpwork.Controls.Add(this.Label14);
            this.Tpwork.Controls.Add(this.TransactionTypeReferenceComboBox);
            this.Tpwork.Controls.Add(this.AccountNumberTextBox);
            this.Tpwork.Controls.Add(this.ComBankAccIdComboBox);
            this.Tpwork.Controls.Add(this.LblBankAccount);
            this.Tpwork.Controls.Add(this.AccountNumberLabel);
            this.Tpwork.Controls.Add(this.btnBankName);
            this.Tpwork.Controls.Add(this.BankNameReferenceComboBox);
            this.Tpwork.Controls.Add(this.LblBankName);
            this.Tpwork.Controls.Add(this.btnTranType);
            this.Tpwork.Controls.Add(Label16);
            this.Tpwork.Controls.Add(label7);
            this.Tpwork.Controls.Add(this.ShapeContainer4);
            this.Tpwork.Location = new System.Drawing.Point(4, 22);
            this.Tpwork.Name = "Tpwork";
            this.Tpwork.Size = new System.Drawing.Size(725, 355);
            this.Tpwork.TabIndex = 4;
            this.Tpwork.Text = "Personal";
            this.Tpwork.UseVisualStyleBackColor = true;
            // 
            // cboBloodGroup
            // 
            this.cboBloodGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBloodGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBloodGroup.BackColor = System.Drawing.SystemColors.Window;
            this.cboBloodGroup.DropDownHeight = 134;
            this.cboBloodGroup.FormattingEnabled = true;
            this.cboBloodGroup.IntegralHeight = false;
            this.cboBloodGroup.Items.AddRange(new object[] {
            "O-",
            "O+",
            "A-",
            "A+",
            "B-",
            "B+",
            "AB-",
            "AB+"});
            this.cboBloodGroup.Location = new System.Drawing.Point(452, 110);
            this.cboBloodGroup.Name = "cboBloodGroup";
            this.cboBloodGroup.Size = new System.Drawing.Size(70, 21);
            this.cboBloodGroup.TabIndex = 14;
            this.cboBloodGroup.SelectedIndexChanged += new System.EventHandler(this.cboBloodGroup_SelectedIndexChanged);
            this.cboBloodGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // cboGrade
            // 
            this.cboGrade.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboGrade.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboGrade.BackColor = System.Drawing.SystemColors.Window;
            this.cboGrade.DropDownHeight = 134;
            this.cboGrade.FormattingEnabled = true;
            this.cboGrade.IntegralHeight = false;
            this.cboGrade.Location = new System.Drawing.Point(93, 168);
            this.cboGrade.Name = "cboGrade";
            this.cboGrade.Size = new System.Drawing.Size(215, 21);
            this.cboGrade.TabIndex = 6;
            this.cboGrade.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            // 
            // btnGrade
            // 
            this.btnGrade.Location = new System.Drawing.Point(312, 169);
            this.btnGrade.Name = "btnGrade";
            this.btnGrade.Size = new System.Drawing.Size(28, 22);
            this.btnGrade.TabIndex = 7;
            this.btnGrade.Text = "...";
            this.btnGrade.UseVisualStyleBackColor = true;
            this.btnGrade.Click += new System.EventHandler(this.btnGrade_Click);
            // 
            // cboCountry
            // 
            this.cboCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCountry.BackColor = System.Drawing.SystemColors.Window;
            this.cboCountry.DropDownHeight = 134;
            this.cboCountry.FormattingEnabled = true;
            this.cboCountry.IntegralHeight = false;
            this.cboCountry.Location = new System.Drawing.Point(452, 20);
            this.cboCountry.Name = "cboCountry";
            this.cboCountry.Size = new System.Drawing.Size(215, 21);
            this.cboCountry.TabIndex = 8;
            this.cboCountry.SelectedIndexChanged += new System.EventHandler(this.cboCountry_SelectedIndexChanged);
            // 
            // btnCountry
            // 
            this.btnCountry.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btnCountry.Location = new System.Drawing.Point(671, 19);
            this.btnCountry.Name = "btnCountry";
            this.btnCountry.Size = new System.Drawing.Size(28, 22);
            this.btnCountry.TabIndex = 9;
            this.btnCountry.Text = "...";
            this.btnCountry.UseVisualStyleBackColor = true;
            this.btnCountry.Click += new System.EventHandler(this.btnCountry_Click);
            // 
            // cboMaritalStatus
            // 
            this.cboMaritalStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboMaritalStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboMaritalStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cboMaritalStatus.DropDownHeight = 134;
            this.cboMaritalStatus.FormattingEnabled = true;
            this.cboMaritalStatus.IntegralHeight = false;
            this.cboMaritalStatus.Location = new System.Drawing.Point(96, 20);
            this.cboMaritalStatus.Name = "cboMaritalStatus";
            this.cboMaritalStatus.Size = new System.Drawing.Size(265, 21);
            this.cboMaritalStatus.TabIndex = 0;
            this.cboMaritalStatus.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboMaritalStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // lblDeviceUser
            // 
            this.lblDeviceUser.AutoSize = true;
            this.lblDeviceUser.Location = new System.Drawing.Point(528, 114);
            this.lblDeviceUser.Name = "lblDeviceUser";
            this.lblDeviceUser.Size = new System.Drawing.Size(66, 13);
            this.lblDeviceUser.TabIndex = 100025;
            this.lblDeviceUser.Text = "Device User";
            // 
            // txtDeviceUserID
            // 
            this.txtDeviceUserID.Location = new System.Drawing.Point(600, 110);
            this.txtDeviceUserID.MaxLength = 30;
            this.txtDeviceUserID.Name = "txtDeviceUserID";
            this.txtDeviceUserID.Size = new System.Drawing.Size(99, 20);
            this.txtDeviceUserID.TabIndex = 15;
            this.txtDeviceUserID.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // cboNationality
            // 
            this.cboNationality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboNationality.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboNationality.BackColor = System.Drawing.SystemColors.Window;
            this.cboNationality.DropDownHeight = 134;
            this.cboNationality.FormattingEnabled = true;
            this.cboNationality.IntegralHeight = false;
            this.cboNationality.Location = new System.Drawing.Point(452, 50);
            this.cboNationality.Name = "cboNationality";
            this.cboNationality.Size = new System.Drawing.Size(215, 21);
            this.cboNationality.TabIndex = 10;
            this.cboNationality.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboNationality.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // rdbMale
            // 
            this.rdbMale.AutoSize = true;
            this.rdbMale.BackColor = System.Drawing.Color.White;
            this.rdbMale.Checked = true;
            this.rdbMale.Location = new System.Drawing.Point(96, 138);
            this.rdbMale.Name = "rdbMale";
            this.rdbMale.Size = new System.Drawing.Size(48, 17);
            this.rdbMale.TabIndex = 4;
            this.rdbMale.TabStop = true;
            this.rdbMale.Text = "Male";
            this.rdbMale.UseVisualStyleBackColor = false;
            this.rdbMale.CheckedChanged += new System.EventHandler(this.changestatus);
            // 
            // rdbFemale
            // 
            this.rdbFemale.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.rdbFemale.AutoSize = true;
            this.rdbFemale.BackColor = System.Drawing.Color.White;
            this.rdbFemale.Location = new System.Drawing.Point(182, 138);
            this.rdbFemale.Name = "rdbFemale";
            this.rdbFemale.Size = new System.Drawing.Size(59, 17);
            this.rdbFemale.TabIndex = 5;
            this.rdbFemale.Text = "Female";
            this.rdbFemale.UseVisualStyleBackColor = false;
            this.rdbFemale.CheckedChanged += new System.EventHandler(this.changestatus);
            // 
            // cboReligion
            // 
            this.cboReligion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReligion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReligion.BackColor = System.Drawing.SystemColors.Window;
            this.cboReligion.DropDownHeight = 134;
            this.cboReligion.FormattingEnabled = true;
            this.cboReligion.IntegralHeight = false;
            this.cboReligion.Location = new System.Drawing.Point(452, 80);
            this.cboReligion.Name = "cboReligion";
            this.cboReligion.Size = new System.Drawing.Size(215, 21);
            this.cboReligion.TabIndex = 12;
            this.cboReligion.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.cboReligion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // BtnReligion
            // 
            this.BtnReligion.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.BtnReligion.Location = new System.Drawing.Point(671, 79);
            this.BtnReligion.Name = "BtnReligion";
            this.BtnReligion.Size = new System.Drawing.Size(28, 22);
            this.BtnReligion.TabIndex = 13;
            this.BtnReligion.Text = "...";
            this.BtnReligion.UseVisualStyleBackColor = true;
            this.BtnReligion.Click += new System.EventHandler(this.BtnReligion_Click);
            // 
            // BtnNationality
            // 
            this.BtnNationality.Location = new System.Drawing.Point(671, 49);
            this.BtnNationality.Name = "BtnNationality";
            this.BtnNationality.Size = new System.Drawing.Size(28, 22);
            this.BtnNationality.TabIndex = 11;
            this.BtnNationality.Text = "...";
            this.BtnNationality.UseVisualStyleBackColor = true;
            this.BtnNationality.Click += new System.EventHandler(this.BtnNationality_Click);
            // 
            // txtPersonID
            // 
            this.txtPersonID.Location = new System.Drawing.Point(484, 245);
            this.txtPersonID.MaxLength = 49;
            this.txtPersonID.Name = "txtPersonID";
            this.txtPersonID.Size = new System.Drawing.Size(181, 20);
            this.txtPersonID.TabIndex = 22;
            this.txtPersonID.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // txtSpouseName
            // 
            this.txtSpouseName.Location = new System.Drawing.Point(96, 110);
            this.txtSpouseName.MaxLength = 100;
            this.txtSpouseName.Name = "txtSpouseName";
            this.txtSpouseName.Size = new System.Drawing.Size(265, 20);
            this.txtSpouseName.TabIndex = 3;
            this.txtSpouseName.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // txtIdentificationMarks
            // 
            this.txtIdentificationMarks.Location = new System.Drawing.Point(452, 140);
            this.txtIdentificationMarks.MaxLength = 200;
            this.txtIdentificationMarks.Multiline = true;
            this.txtIdentificationMarks.Name = "txtIdentificationMarks";
            this.txtIdentificationMarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtIdentificationMarks.Size = new System.Drawing.Size(245, 69);
            this.txtIdentificationMarks.TabIndex = 16;
            this.txtIdentificationMarks.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // txtMothersName
            // 
            this.txtMothersName.Location = new System.Drawing.Point(96, 80);
            this.txtMothersName.MaxLength = 50;
            this.txtMothersName.Name = "txtMothersName";
            this.txtMothersName.Size = new System.Drawing.Size(265, 20);
            this.txtMothersName.TabIndex = 2;
            this.txtMothersName.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // txtFathersName
            // 
            this.txtFathersName.Location = new System.Drawing.Point(96, 50);
            this.txtFathersName.MaxLength = 25;
            this.txtFathersName.Name = "txtFathersName";
            this.txtFathersName.Size = new System.Drawing.Size(265, 20);
            this.txtFathersName.TabIndex = 1;
            this.txtFathersName.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // BtnEmployeeBankname
            // 
            this.BtnEmployeeBankname.Location = new System.Drawing.Point(671, 270);
            this.BtnEmployeeBankname.Name = "BtnEmployeeBankname";
            this.BtnEmployeeBankname.Size = new System.Drawing.Size(28, 22);
            this.BtnEmployeeBankname.TabIndex = 24;
            this.BtnEmployeeBankname.Text = "...";
            this.BtnEmployeeBankname.UseVisualStyleBackColor = true;
            this.BtnEmployeeBankname.Click += new System.EventHandler(this.BtnEmployeeBankname_Click);
            // 
            // CboEmployeeBankname
            // 
            this.CboEmployeeBankname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboEmployeeBankname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboEmployeeBankname.DropDownHeight = 134;
            this.CboEmployeeBankname.FormattingEnabled = true;
            this.CboEmployeeBankname.IntegralHeight = false;
            this.CboEmployeeBankname.Location = new System.Drawing.Point(484, 272);
            this.CboEmployeeBankname.Name = "CboEmployeeBankname";
            this.CboEmployeeBankname.Size = new System.Drawing.Size(183, 21);
            this.CboEmployeeBankname.TabIndex = 23;
            this.CboEmployeeBankname.SelectedIndexChanged += new System.EventHandler(this.changestatus);
            this.CboEmployeeBankname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboEmployeeBankname_KeyPress);
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(362, 276);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(112, 13);
            this.Label14.TabIndex = 927;
            this.Label14.Text = "Employee Bank Name";
            // 
            // TransactionTypeReferenceComboBox
            // 
            this.TransactionTypeReferenceComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TransactionTypeReferenceComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.TransactionTypeReferenceComboBox.BackColor = System.Drawing.SystemColors.Info;
            this.TransactionTypeReferenceComboBox.DropDownHeight = 134;
            this.TransactionTypeReferenceComboBox.FormattingEnabled = true;
            this.TransactionTypeReferenceComboBox.IntegralHeight = false;
            this.TransactionTypeReferenceComboBox.Location = new System.Drawing.Point(136, 243);
            this.TransactionTypeReferenceComboBox.Name = "TransactionTypeReferenceComboBox";
            this.TransactionTypeReferenceComboBox.Size = new System.Drawing.Size(180, 21);
            this.TransactionTypeReferenceComboBox.TabIndex = 17;
            this.TransactionTypeReferenceComboBox.SelectedIndexChanged += new System.EventHandler(this.TransactionTypeReferenceComboBox_SelectedIndexChanged);
            this.TransactionTypeReferenceComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // AccountNumberTextBox
            // 
            this.AccountNumberTextBox.Location = new System.Drawing.Point(484, 303);
            this.AccountNumberTextBox.MaxLength = 40;
            this.AccountNumberTextBox.Name = "AccountNumberTextBox";
            this.AccountNumberTextBox.Size = new System.Drawing.Size(213, 20);
            this.AccountNumberTextBox.TabIndex = 25;
            this.AccountNumberTextBox.TextChanged += new System.EventHandler(this.AccountNumberTextBox_TextChanged);
            // 
            // ComBankAccIdComboBox
            // 
            this.ComBankAccIdComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ComBankAccIdComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComBankAccIdComboBox.DropDownHeight = 134;
            this.ComBankAccIdComboBox.FormattingEnabled = true;
            this.ComBankAccIdComboBox.IntegralHeight = false;
            this.ComBankAccIdComboBox.Location = new System.Drawing.Point(136, 303);
            this.ComBankAccIdComboBox.Name = "ComBankAccIdComboBox";
            this.ComBankAccIdComboBox.Size = new System.Drawing.Size(214, 21);
            this.ComBankAccIdComboBox.TabIndex = 21;
            this.ComBankAccIdComboBox.SelectedIndexChanged += new System.EventHandler(this.ComBankAccIdComboBox_SelectedIndexChanged);
            this.ComBankAccIdComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComBankAccIdComboBox_KeyPress);
            // 
            // LblBankAccount
            // 
            this.LblBankAccount.AutoSize = true;
            this.LblBankAccount.Location = new System.Drawing.Point(14, 307);
            this.LblBankAccount.Name = "LblBankAccount";
            this.LblBankAccount.Size = new System.Drawing.Size(119, 13);
            this.LblBankAccount.TabIndex = 922;
            this.LblBankAccount.Text = "Employer Bank A/c No.";
            // 
            // AccountNumberLabel
            // 
            this.AccountNumberLabel.AutoSize = true;
            this.AccountNumberLabel.Location = new System.Drawing.Point(362, 308);
            this.AccountNumberLabel.Name = "AccountNumberLabel";
            this.AccountNumberLabel.Size = new System.Drawing.Size(122, 13);
            this.AccountNumberLabel.TabIndex = 24;
            this.AccountNumberLabel.Text = "Employee Bank A/c No.";
            // 
            // btnBankName
            // 
            this.btnBankName.Location = new System.Drawing.Point(322, 272);
            this.btnBankName.Name = "btnBankName";
            this.btnBankName.Size = new System.Drawing.Size(28, 22);
            this.btnBankName.TabIndex = 20;
            this.btnBankName.Text = "...";
            this.btnBankName.UseVisualStyleBackColor = true;
            this.btnBankName.Click += new System.EventHandler(this.btnBankName_Click);
            // 
            // BankNameReferenceComboBox
            // 
            this.BankNameReferenceComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.BankNameReferenceComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.BankNameReferenceComboBox.DropDownHeight = 134;
            this.BankNameReferenceComboBox.FormattingEnabled = true;
            this.BankNameReferenceComboBox.IntegralHeight = false;
            this.BankNameReferenceComboBox.Location = new System.Drawing.Point(136, 272);
            this.BankNameReferenceComboBox.Name = "BankNameReferenceComboBox";
            this.BankNameReferenceComboBox.Size = new System.Drawing.Size(180, 21);
            this.BankNameReferenceComboBox.TabIndex = 19;
            this.BankNameReferenceComboBox.SelectedIndexChanged += new System.EventHandler(this.BankNameReferenceComboBox_SelectedIndexChanged);
            this.BankNameReferenceComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BankNameReferenceComboBox_KeyPress);
            // 
            // LblBankName
            // 
            this.LblBankName.AutoSize = true;
            this.LblBankName.Location = new System.Drawing.Point(14, 276);
            this.LblBankName.Name = "LblBankName";
            this.LblBankName.Size = new System.Drawing.Size(109, 13);
            this.LblBankName.TabIndex = 923;
            this.LblBankName.Text = "Employer Bank Name";
            // 
            // btnTranType
            // 
            this.btnTranType.Location = new System.Drawing.Point(322, 242);
            this.btnTranType.Name = "btnTranType";
            this.btnTranType.Size = new System.Drawing.Size(28, 23);
            this.btnTranType.TabIndex = 18;
            this.btnTranType.Text = "...";
            this.btnTranType.UseVisualStyleBackColor = true;
            this.btnTranType.Visible = false;
            // 
            // ShapeContainer4
            // 
            this.ShapeContainer4.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer4.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer4.Name = "ShapeContainer4";
            this.ShapeContainer4.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape6,
            this.lineShape4});
            this.ShapeContainer4.Size = new System.Drawing.Size(725, 355);
            this.ShapeContainer4.TabIndex = 0;
            this.ShapeContainer4.TabStop = false;
            // 
            // lineShape6
            // 
            this.lineShape6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape6.Name = "lineShape6";
            this.lineShape6.X1 = 76;
            this.lineShape6.X2 = 700;
            this.lineShape6.Y1 = 220;
            this.lineShape6.Y2 = 220;
            // 
            // lineShape4
            // 
            this.lineShape4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 45;
            this.lineShape4.X2 = 700;
            this.lineShape4.Y1 = 12;
            this.lineShape4.Y2 = 12;
            // 
            // TbContacts
            // 
            this.TbContacts.Controls.Add(this.groupBox1);
            this.TbContacts.Controls.Add(this.txtLocalPhone);
            this.TbContacts.Controls.Add(label17);
            this.TbContacts.Controls.Add(this.txtLocalAddress);
            this.TbContacts.Controls.Add(label18);
            this.TbContacts.Controls.Add(this.txtEmergencyPhone);
            this.TbContacts.Controls.Add(label15);
            this.TbContacts.Controls.Add(this.txtEmergencyAddress);
            this.TbContacts.Controls.Add(label12);
            this.TbContacts.Controls.Add(this.TxtOfficialEmail);
            this.TbContacts.Controls.Add(OfficialEmailLabel);
            this.TbContacts.Controls.Add(this.txtLocalMobile);
            this.TbContacts.Controls.Add(LocalMobileLabel);
            this.TbContacts.Controls.Add(Label25);
            this.TbContacts.Controls.Add(this.ShapeContainer2);
            this.TbContacts.Location = new System.Drawing.Point(4, 22);
            this.TbContacts.Name = "TbContacts";
            this.TbContacts.Padding = new System.Windows.Forms.Padding(3);
            this.TbContacts.Size = new System.Drawing.Size(725, 355);
            this.TbContacts.TabIndex = 2;
            this.TbContacts.Text = "Contacts";
            this.TbContacts.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(label31);
            this.groupBox1.Controls.Add(label30);
            this.groupBox1.Controls.Add(label29);
            this.groupBox1.Controls.Add(label28);
            this.groupBox1.Controls.Add(this.txtZipCode);
            this.groupBox1.Controls.Add(this.txtState);
            this.groupBox1.Controls.Add(this.txtCity);
            this.groupBox1.Controls.Add(this.txtAddress2);
            this.groupBox1.Controls.Add(this.txtAddress1);
            this.groupBox1.Controls.Add(EmailLabel);
            this.groupBox1.Controls.Add(LocalAddressLabel);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.txtPhone);
            this.groupBox1.Controls.Add(LocalPhoneLabel);
            this.groupBox1.Location = new System.Drawing.Point(6, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(336, 221);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Permanent  Info";
            // 
            // txtZipCode
            // 
            this.txtZipCode.Location = new System.Drawing.Point(93, 131);
            this.txtZipCode.MaxLength = 20;
            this.txtZipCode.Multiline = true;
            this.txtZipCode.Name = "txtZipCode";
            this.txtZipCode.Size = new System.Drawing.Size(237, 20);
            this.txtZipCode.TabIndex = 4;
            this.txtZipCode.TextChanged += new System.EventHandler(this.txtZipCode_TextChanged);
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(93, 103);
            this.txtState.MaxLength = 50;
            this.txtState.Multiline = true;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(237, 20);
            this.txtState.TabIndex = 3;
            this.txtState.TextChanged += new System.EventHandler(this.txtState_TextChanged);
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(93, 75);
            this.txtCity.MaxLength = 50;
            this.txtCity.Multiline = true;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(237, 20);
            this.txtCity.TabIndex = 2;
            this.txtCity.TextChanged += new System.EventHandler(this.txtCity_TextChanged);
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(93, 47);
            this.txtAddress2.MaxLength = 100;
            this.txtAddress2.Multiline = true;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(237, 20);
            this.txtAddress2.TabIndex = 1;
            this.txtAddress2.TextChanged += new System.EventHandler(this.txtAddress2_TextChanged);
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(93, 19);
            this.txtAddress1.MaxLength = 100;
            this.txtAddress1.Multiline = true;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(237, 20);
            this.txtAddress1.TabIndex = 0;
            this.txtAddress1.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(93, 187);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(237, 20);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(93, 159);
            this.txtPhone.MaxLength = 15;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(237, 20);
            this.txtPhone.TabIndex = 5;
            this.txtPhone.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // txtLocalPhone
            // 
            this.txtLocalPhone.Location = new System.Drawing.Point(455, 229);
            this.txtLocalPhone.MaxLength = 15;
            this.txtLocalPhone.Name = "txtLocalPhone";
            this.txtLocalPhone.Size = new System.Drawing.Size(237, 20);
            this.txtLocalPhone.TabIndex = 4;
            this.txtLocalPhone.TextChanged += new System.EventHandler(this.txtLocalPhone_TextChanged);
            // 
            // txtLocalAddress
            // 
            this.txtLocalAddress.Location = new System.Drawing.Point(455, 155);
            this.txtLocalAddress.MaxLength = 300;
            this.txtLocalAddress.Multiline = true;
            this.txtLocalAddress.Name = "txtLocalAddress";
            this.txtLocalAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLocalAddress.Size = new System.Drawing.Size(237, 64);
            this.txtLocalAddress.TabIndex = 3;
            this.txtLocalAddress.TextChanged += new System.EventHandler(this.txtLocalAddress_TextChanged);
            // 
            // txtEmergencyPhone
            // 
            this.txtEmergencyPhone.Location = new System.Drawing.Point(455, 127);
            this.txtEmergencyPhone.MaxLength = 15;
            this.txtEmergencyPhone.Name = "txtEmergencyPhone";
            this.txtEmergencyPhone.Size = new System.Drawing.Size(237, 20);
            this.txtEmergencyPhone.TabIndex = 2;
            this.txtEmergencyPhone.TextChanged += new System.EventHandler(this.txtEmergencyPhone_TextChanged);
            // 
            // txtEmergencyAddress
            // 
            this.txtEmergencyAddress.Location = new System.Drawing.Point(455, 55);
            this.txtEmergencyAddress.MaxLength = 300;
            this.txtEmergencyAddress.Multiline = true;
            this.txtEmergencyAddress.Name = "txtEmergencyAddress";
            this.txtEmergencyAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtEmergencyAddress.Size = new System.Drawing.Size(237, 64);
            this.txtEmergencyAddress.TabIndex = 1;
            this.txtEmergencyAddress.TextChanged += new System.EventHandler(this.txtEmergencyAddress_TextChanged);
            // 
            // TxtOfficialEmail
            // 
            this.TxtOfficialEmail.Location = new System.Drawing.Point(455, 287);
            this.TxtOfficialEmail.MaxLength = 50;
            this.TxtOfficialEmail.Name = "TxtOfficialEmail";
            this.TxtOfficialEmail.Size = new System.Drawing.Size(237, 20);
            this.TxtOfficialEmail.TabIndex = 5;
            this.TxtOfficialEmail.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // txtLocalMobile
            // 
            this.txtLocalMobile.Location = new System.Drawing.Point(99, 290);
            this.txtLocalMobile.MaxLength = 15;
            this.txtLocalMobile.Name = "txtLocalMobile";
            this.txtLocalMobile.Size = new System.Drawing.Size(237, 20);
            this.txtLocalMobile.TabIndex = 0;
            this.txtLocalMobile.TextChanged += new System.EventHandler(this.changestatus);
            // 
            // ShapeContainer2
            // 
            this.ShapeContainer2.Location = new System.Drawing.Point(3, 3);
            this.ShapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer2.Name = "ShapeContainer2";
            this.ShapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.LineShape1});
            this.ShapeContainer2.Size = new System.Drawing.Size(719, 349);
            this.ShapeContainer2.TabIndex = 0;
            this.ShapeContainer2.TabStop = false;
            // 
            // LineShape1
            // 
            this.LineShape1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.LineShape1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LineShape1.Name = "LineShape1";
            this.LineShape1.X1 = 51;
            this.LineShape1.X2 = 698;
            this.LineShape1.Y1 = 20;
            this.LineShape1.Y2 = 20;
            // 
            // TbOtherInfo
            // 
            this.TbOtherInfo.Controls.Add(this.cboProcessType);
            this.TbOtherInfo.Controls.Add(label24);
            this.TbOtherInfo.Controls.Add(lblCommissionStructure);
            this.TbOtherInfo.Controls.Add(this.cboCommissionStructure);
            this.TbOtherInfo.Controls.Add(this.txtNoticePeriod);
            this.TbOtherInfo.Controls.Add(lblNoticePeriod);
            this.TbOtherInfo.Controls.Add(this.cboWorkLocation);
            this.TbOtherInfo.Controls.Add(this.btnWorkLocation);
            this.TbOtherInfo.Controls.Add(lblWorkLocation);
            this.TbOtherInfo.Controls.Add(lblVisaObtained);
            this.TbOtherInfo.Controls.Add(this.CboVisaObtained);
            this.TbOtherInfo.Controls.Add(this.dgvLanguages);
            this.TbOtherInfo.Controls.Add(this.cboMothertongue);
            this.TbOtherInfo.Controls.Add(MothertongueIDLabel);
            this.TbOtherInfo.Controls.Add(this.Btnmothertongue);
            this.TbOtherInfo.Controls.Add(this.txtNotes);
            this.TbOtherInfo.Controls.Add(label9);
            this.TbOtherInfo.Controls.Add(this.shapeContainer3);
            this.TbOtherInfo.Location = new System.Drawing.Point(4, 22);
            this.TbOtherInfo.Name = "TbOtherInfo";
            this.TbOtherInfo.Padding = new System.Windows.Forms.Padding(3);
            this.TbOtherInfo.Size = new System.Drawing.Size(725, 355);
            this.TbOtherInfo.TabIndex = 5;
            this.TbOtherInfo.Text = " Other Info";
            this.TbOtherInfo.UseVisualStyleBackColor = true;
            // 
            // cboProcessType
            // 
            this.cboProcessType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboProcessType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboProcessType.FormattingEnabled = true;
            this.cboProcessType.Location = new System.Drawing.Point(454, 127);
            this.cboProcessType.Name = "cboProcessType";
            this.cboProcessType.Size = new System.Drawing.Size(228, 21);
            this.cboProcessType.TabIndex = 100044;
            this.cboProcessType.SelectedIndexChanged += new System.EventHandler(this.cboProcessType_SelectedIndexChanged);
            // 
            // cboCommissionStructure
            // 
            this.cboCommissionStructure.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCommissionStructure.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCommissionStructure.BackColor = System.Drawing.SystemColors.Window;
            this.cboCommissionStructure.DropDownHeight = 134;
            this.cboCommissionStructure.FormattingEnabled = true;
            this.cboCommissionStructure.IntegralHeight = false;
            this.cboCommissionStructure.Location = new System.Drawing.Point(454, 99);
            this.cboCommissionStructure.Name = "cboCommissionStructure";
            this.cboCommissionStructure.Size = new System.Drawing.Size(228, 21);
            this.cboCommissionStructure.TabIndex = 100041;
            this.cboCommissionStructure.SelectedIndexChanged += new System.EventHandler(this.cboCommissionStructure_SelectedIndexChanged);
            // 
            // txtNoticePeriod
            // 
            this.txtNoticePeriod.Location = new System.Drawing.Point(454, 72);
            this.txtNoticePeriod.MaxLength = 3;
            this.txtNoticePeriod.Name = "txtNoticePeriod";
            this.txtNoticePeriod.Size = new System.Drawing.Size(102, 20);
            this.txtNoticePeriod.TabIndex = 100040;
            this.txtNoticePeriod.TextChanged += new System.EventHandler(this.txtNoticePeriod_TextChanged);
            this.txtNoticePeriod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtint_KeyPress);
            // 
            // cboWorkLocation
            // 
            this.cboWorkLocation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkLocation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkLocation.BackColor = System.Drawing.SystemColors.Window;
            this.cboWorkLocation.DropDownHeight = 134;
            this.cboWorkLocation.FormattingEnabled = true;
            this.cboWorkLocation.IntegralHeight = false;
            this.cboWorkLocation.Location = new System.Drawing.Point(454, 16);
            this.cboWorkLocation.Name = "cboWorkLocation";
            this.cboWorkLocation.Size = new System.Drawing.Size(230, 21);
            this.cboWorkLocation.TabIndex = 2;
            this.cboWorkLocation.SelectedIndexChanged += new System.EventHandler(this.cboWorkLocation_SelectedIndexChanged);
            this.cboWorkLocation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // btnWorkLocation
            // 
            this.btnWorkLocation.Location = new System.Drawing.Point(686, 15);
            this.btnWorkLocation.Name = "btnWorkLocation";
            this.btnWorkLocation.Size = new System.Drawing.Size(28, 22);
            this.btnWorkLocation.TabIndex = 3;
            this.btnWorkLocation.Text = "...";
            this.btnWorkLocation.UseVisualStyleBackColor = true;
            this.btnWorkLocation.Click += new System.EventHandler(this.btnWorkLocation_Click);
            // 
            // CboVisaObtained
            // 
            this.CboVisaObtained.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboVisaObtained.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboVisaObtained.BackColor = System.Drawing.SystemColors.Window;
            this.CboVisaObtained.DropDownHeight = 134;
            this.CboVisaObtained.FormattingEnabled = true;
            this.CboVisaObtained.IntegralHeight = false;
            this.CboVisaObtained.Location = new System.Drawing.Point(454, 44);
            this.CboVisaObtained.Name = "CboVisaObtained";
            this.CboVisaObtained.Size = new System.Drawing.Size(230, 21);
            this.CboVisaObtained.TabIndex = 4;
            this.CboVisaObtained.SelectedIndexChanged += new System.EventHandler(this.CboVisaObtained_SelectedIndexChanged);
            // 
            // dgvLanguages
            // 
            this.dgvLanguages.AllowUserToResizeRows = false;
            this.dgvLanguages.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvLanguages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLanguages.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColLanguages});
            this.dgvLanguages.Location = new System.Drawing.Point(30, 67);
            this.dgvLanguages.Name = "dgvLanguages";
            this.dgvLanguages.RowHeadersWidth = 24;
            this.dgvLanguages.Size = new System.Drawing.Size(265, 148);
            this.dgvLanguages.TabIndex = 5;
            this.dgvLanguages.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLanguages_CellValueChanged);
            this.dgvLanguages.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvLanguages_EditingControlShowing);
            this.dgvLanguages.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvLanguages_CurrentCellDirtyStateChanged);
            this.dgvLanguages.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvLanguages_DataError);
            // 
            // ColLanguages
            // 
            this.ColLanguages.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColLanguages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ColLanguages.HeaderText = "Languages";
            this.ColLanguages.Name = "ColLanguages";
            this.ColLanguages.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColLanguages.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cboMothertongue
            // 
            this.cboMothertongue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboMothertongue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboMothertongue.DropDownHeight = 134;
            this.cboMothertongue.FormattingEnabled = true;
            this.cboMothertongue.IntegralHeight = false;
            this.cboMothertongue.Location = new System.Drawing.Point(30, 38);
            this.cboMothertongue.Name = "cboMothertongue";
            this.cboMothertongue.Size = new System.Drawing.Size(236, 21);
            this.cboMothertongue.TabIndex = 0;
            this.cboMothertongue.SelectedIndexChanged += new System.EventHandler(this.cboMothertongue_SelectedIndexChanged);
            // 
            // Btnmothertongue
            // 
            this.Btnmothertongue.Location = new System.Drawing.Point(267, 37);
            this.Btnmothertongue.Name = "Btnmothertongue";
            this.Btnmothertongue.Size = new System.Drawing.Size(28, 22);
            this.Btnmothertongue.TabIndex = 1;
            this.Btnmothertongue.Text = "...";
            this.Btnmothertongue.UseVisualStyleBackColor = true;
            this.Btnmothertongue.Click += new System.EventHandler(this.Btnmothertongue_Click);
            // 
            // txtNotes
            // 
            this.txtNotes.Location = new System.Drawing.Point(10, 244);
            this.txtNotes.MaxLength = 1000;
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNotes.Size = new System.Drawing.Size(703, 85);
            this.txtNotes.TabIndex = 6;
            // 
            // shapeContainer3
            // 
            this.shapeContainer3.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer3.Name = "shapeContainer3";
            this.shapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape7});
            this.shapeContainer3.Size = new System.Drawing.Size(719, 349);
            this.shapeContainer3.TabIndex = 517;
            this.shapeContainer3.TabStop = false;
            // 
            // lineShape7
            // 
            this.lineShape7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape7.Cursor = System.Windows.Forms.Cursors.Default;
            this.lineShape7.Name = "lineShape7";
            this.lineShape7.X1 = 57;
            this.lineShape7.X2 = 703;
            this.lineShape7.Y1 = 225;
            this.lineShape7.Y2 = 225;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.TxtFirstNameArb);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.TxtEmployeeNumber);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.LblEmployeeNumber);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.LblSalutation);
            this.panel1.Controls.Add(this.TxtLastNameArb);
            this.panel1.Controls.Add(this.TxtFirstName);
            this.panel1.Controls.Add(this.TxtMiddleNameArb);
            this.panel1.Controls.Add(this.LblFirstName);
            this.panel1.Controls.Add(this.TxtMiddleName);
            this.panel1.Controls.Add(this.BtnSalutation);
            this.panel1.Controls.Add(this.LblMiddleName);
            this.panel1.Controls.Add(this.TxtLastName);
            this.panel1.Controls.Add(this.CboSalutation);
            this.panel1.Controls.Add(this.LblLastName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(739, 78);
            this.panel1.TabIndex = 18;
            // 
            // TxtFirstNameArb
            // 
            this.TxtFirstNameArb.BackColor = System.Drawing.SystemColors.Info;
            this.TxtFirstNameArb.Location = new System.Drawing.Point(562, 56);
            this.TxtFirstNameArb.MaxLength = 25;
            this.TxtFirstNameArb.Name = "TxtFirstNameArb";
            this.TxtFirstNameArb.Size = new System.Drawing.Size(158, 20);
            this.TxtFirstNameArb.TabIndex = 8;
            this.TxtFirstNameArb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtFirstNameArb.TextChanged += new System.EventHandler(this.TxtFirstNameArb_TextChanged);
            this.TxtFirstNameArb.Enter += new System.EventHandler(this.TxtFirstNameArb_Enter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(342, 43);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "اسم العائلة ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(493, 43);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "الاسم الأوسط";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(662, 43);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 15;
            this.label21.Text = " الأسم الأول";
            // 
            // TxtLastNameArb
            // 
            this.TxtLastNameArb.Location = new System.Drawing.Point(247, 56);
            this.TxtLastNameArb.MaxLength = 25;
            this.TxtLastNameArb.Name = "TxtLastNameArb";
            this.TxtLastNameArb.Size = new System.Drawing.Size(152, 20);
            this.TxtLastNameArb.TabIndex = 7;
            this.TxtLastNameArb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtLastNameArb.TextChanged += new System.EventHandler(this.TxtLastNameArb_TextChanged);
            this.TxtLastNameArb.Enter += new System.EventHandler(this.TxtLastNameArb_Enter);
            // 
            // TxtMiddleNameArb
            // 
            this.TxtMiddleNameArb.Location = new System.Drawing.Point(404, 56);
            this.TxtMiddleNameArb.MaxLength = 25;
            this.TxtMiddleNameArb.Name = "TxtMiddleNameArb";
            this.TxtMiddleNameArb.Size = new System.Drawing.Size(152, 20);
            this.TxtMiddleNameArb.TabIndex = 7;
            this.TxtMiddleNameArb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtMiddleNameArb.TextChanged += new System.EventHandler(this.TxtMiddleNameArb_TextChanged);
            this.TxtMiddleNameArb.Enter += new System.EventHandler(this.TxtMiddleNameArb_Enter);
            // 
            // BtnSalutation
            // 
            this.BtnSalutation.Location = new System.Drawing.Point(201, 20);
            this.BtnSalutation.Name = "BtnSalutation";
            this.BtnSalutation.Size = new System.Drawing.Size(26, 23);
            this.BtnSalutation.TabIndex = 2;
            this.BtnSalutation.Text = "...";
            this.BtnSalutation.UseVisualStyleBackColor = true;
            this.BtnSalutation.Click += new System.EventHandler(this.BtnSalutation_Click);
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 40;
            this.lineShape3.X2 = 700;
            this.lineShape3.Y1 = 234;
            this.lineShape3.Y2 = 234;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 41;
            this.lineShape2.X2 = 701;
            this.lineShape2.Y1 = 118;
            this.lineShape2.Y2 = 118;
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblEmployeeStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 558);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(740, 22);
            this.StatusStrip1.TabIndex = 67;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // LblEmployeeStatus
            // 
            this.LblEmployeeStatus.Name = "LblEmployeeStatus";
            this.LblEmployeeStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // bSearch
            // 
            this.bSearch.AntiAlias = true;
            this.bSearch.BackColor = System.Drawing.Color.Transparent;
            this.bSearch.Controls.Add(this.label10);
            this.bSearch.Controls.Add(this.cboCompany);
            this.bSearch.Controls.Add(this.txtSearch);
            this.bSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.bSearch.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.btnSearch});
            this.bSearch.Location = new System.Drawing.Point(0, 25);
            this.bSearch.Name = "bSearch";
            this.bSearch.PaddingLeft = 10;
            this.bSearch.Size = new System.Drawing.Size(740, 25);
            this.bSearch.Stretch = true;
            this.bSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.bSearch.TabIndex = 90;
            this.bSearch.TabStop = false;
            this.bSearch.Text = "bar1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "Company";
            // 
            // cboCompany
            // 
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(70, 3);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(203, 23);
            this.cboCompany.TabIndex = 1;
            this.cboCompany.SelectionChangeCommitted += new System.EventHandler(this.cboCompany_SelectionChangeCommitted);
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            // 
            // 
            // 
            this.txtSearch.Border.Class = "TextBoxBorder";
            this.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtSearch.Location = new System.Drawing.Point(285, 2);
            this.txtSearch.MaxLength = 100;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(320, 23);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.WatermarkFont = new System.Drawing.Font("Tahoma", 7.75F, System.Drawing.FontStyle.Italic);
            this.txtSearch.WatermarkText = "Search by Employee Name | Code";
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // labelItem1
            // 
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "                                                                                 " +
                "                                                                                " +
                "                   ";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::MyPayfriend.Properties.Resources.Search;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Text = "buttonItem1";
            this.btnSearch.Tooltip = "Search";
            this.btnSearch.Click += new System.EventHandler(this.BnSearchButton_Click);
            // 
            // RecentPhotoPictureBoxThamNail
            // 
            this.RecentPhotoPictureBoxThamNail.Location = new System.Drawing.Point(943, 207);
            this.RecentPhotoPictureBoxThamNail.Name = "RecentPhotoPictureBoxThamNail";
            this.RecentPhotoPictureBoxThamNail.Size = new System.Drawing.Size(19, 21);
            this.RecentPhotoPictureBoxThamNail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RecentPhotoPictureBoxThamNail.TabIndex = 91;
            this.RecentPhotoPictureBoxThamNail.TabStop = false;
            // 
            // directorySearcher1
            // 
            this.directorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // label33
            // 
            label33.AutoSize = true;
            label33.Location = new System.Drawing.Point(16, 115);
            label33.Name = "label33";
            label33.Size = new System.Drawing.Size(44, 13);
            label33.TabIndex = 100034;
            label33.Text = "Division";
            // 
            // cboDivision
            // 
            this.cboDivision.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDivision.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDivision.BackColor = System.Drawing.SystemColors.Info;
            this.cboDivision.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboDivision.DropDownHeight = 134;
            this.cboDivision.FormattingEnabled = true;
            this.cboDivision.IntegralHeight = false;
            this.cboDivision.Location = new System.Drawing.Point(126, 111);
            this.cboDivision.Name = "cboDivision";
            this.cboDivision.Size = new System.Drawing.Size(204, 21);
            this.cboDivision.TabIndex = 5;
            this.cboDivision.SelectedIndexChanged += new System.EventHandler(this.cboDivision_SelectedIndexChanged);
            this.cboDivision.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbo_KeyPress);
            // 
            // btnDivision
            // 
            this.btnDivision.Location = new System.Drawing.Point(338, 110);
            this.btnDivision.Name = "btnDivision";
            this.btnDivision.Size = new System.Drawing.Size(28, 22);
            this.btnDivision.TabIndex = 6;
            this.btnDivision.Text = "...";
            this.btnDivision.UseVisualStyleBackColor = true;
            this.btnDivision.Click += new System.EventHandler(this.btnDivision_Click);
            // 
            // FrmEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 580);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.RecentPhotoPictureBoxThamNail);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.BnEmployee);
            this.Controls.Add(this.PnlEmployee);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Employee";
            this.Load += new System.EventHandler(this.FrmEmployee_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEmployee_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmEmployee_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.BnEmployee)).EndInit();
            this.BnEmployee.ResumeLayout(false);
            this.BnEmployee.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErrEmployee)).EndInit();
            this.PnlEmployee.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.TbEmployee.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.TpGeneral.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RecentPhotoPictureBox)).EndInit();
            this.ContextMenuStrip2.ResumeLayout(false);
            this.Tpwork.ResumeLayout(false);
            this.Tpwork.PerformLayout();
            this.TbContacts.ResumeLayout(false);
            this.TbContacts.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.TbOtherInfo.ResumeLayout(false);
            this.TbOtherInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLanguages)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bSearch)).EndInit();
            this.bSearch.ResumeLayout(false);
            this.bSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecentPhotoPictureBoxThamNail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOk;
        internal System.Windows.Forms.BindingNavigator BnEmployee;
        internal System.Windows.Forms.ToolStripLabel BindingNavigatorCountItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveFirstItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMovePreviousItem;
        internal System.Windows.Forms.ToolStripSeparator BnSeparator;
        internal System.Windows.Forms.ToolStripTextBox BindingNavigatornPositionItem;
        internal System.Windows.Forms.ToolStripSeparator BnSeparator1;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveNextItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorMoveLastItem;
        internal System.Windows.Forms.ToolStripSeparator BnSeparator2;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorAddNewItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorDeleteItem;
        internal System.Windows.Forms.ToolStripButton BindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripButton BtnClear;
        internal System.Windows.Forms.ToolStripButton BtnPrint;
        internal System.Windows.Forms.ToolStripButton BtnEmail;
        internal System.Windows.Forms.ToolStripButton BtnHelp;
        private System.Windows.Forms.ErrorProvider ErrEmployee;
        private System.Windows.Forms.Timer TmEmployee;
        private System.Windows.Forms.Panel PnlEmployee;
        private System.Windows.Forms.ComboBox CboSalutation;
        private System.Windows.Forms.Label LblLastName;
        private System.Windows.Forms.TextBox TxtLastName;
        private System.Windows.Forms.Label LblMiddleName;
        private System.Windows.Forms.TextBox TxtMiddleName;
        private System.Windows.Forms.Label LblFirstName;
        private System.Windows.Forms.TextBox TxtFirstName;
        private System.Windows.Forms.Label LblSalutation;
        private System.Windows.Forms.Label LblEmployeeNumber;
        private System.Windows.Forms.TextBox TxtEmployeeNumber;
        private System.Windows.Forms.ToolTip TipEmployee;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.TabControl TbEmployee;
        internal System.Windows.Forms.TabPage TpGeneral;
        internal System.Windows.Forms.Button BtnContxtMenu1;
        internal System.Windows.Forms.Button BtnAttach;
        internal System.Windows.Forms.TextBox TxtOfficialEmail;
        internal System.Windows.Forms.ComboBox cboDepartment;
        internal System.Windows.Forms.Button BtnDesignation;
        internal System.Windows.Forms.Button BtnDepartment;
        internal System.Windows.Forms.ComboBox cboDesignation;
        internal System.Windows.Forms.TextBox txtEmployeeFullName;
        internal System.Windows.Forms.ComboBox cboWorkingAt;
        internal System.Windows.Forms.Button BtnWorkstatus;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        internal System.Windows.Forms.TabPage Tpwork;
        internal System.Windows.Forms.DateTimePicker DtpDateofJoining;
        internal System.Windows.Forms.Button BtnEmptype;
        internal System.Windows.Forms.ComboBox cboEmploymentType;
        internal System.Windows.Forms.DateTimePicker DtpProbationEndDate;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer4;
        internal System.Windows.Forms.TextBox txtIdentificationMarks;
        internal System.Windows.Forms.TextBox txtSpouseName;
        internal System.Windows.Forms.TextBox txtFathersName;
        internal System.Windows.Forms.TextBox txtMothersName;
        internal System.Windows.Forms.Button BtnReligion;
        internal System.Windows.Forms.Button BtnNationality;
        internal System.Windows.Forms.RadioButton rdbFemale;
        internal System.Windows.Forms.RadioButton rdbMale;
        internal System.Windows.Forms.ComboBox cboReligion;
        internal System.Windows.Forms.ComboBox cboNationality;
        internal System.Windows.Forms.DateTimePicker dtpDateofBirth;
        internal System.Windows.Forms.TabPage TbContacts;
        internal Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape LineShape1;
        internal System.Windows.Forms.Button BtnSalutation;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip2;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuAttach;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuScan;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuRemove;
        private System.Windows.Forms.ToolStripDropDownButton bnMoreActions;
        internal System.Windows.Forms.Button btnWorkPolicy;
        internal System.Windows.Forms.ComboBox cboWorkPolicy;
        internal System.Windows.Forms.Button btnLeavePolicy;
        internal System.Windows.Forms.ComboBox cboLeavePolicy;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        internal System.Windows.Forms.Button BtnEmployeeBankname;
        internal System.Windows.Forms.ComboBox CboEmployeeBankname;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.ComboBox TransactionTypeReferenceComboBox;
        internal System.Windows.Forms.TextBox AccountNumberTextBox;
        internal System.Windows.Forms.ComboBox ComBankAccIdComboBox;
        internal System.Windows.Forms.Label LblBankAccount;
        internal System.Windows.Forms.Label AccountNumberLabel;
        internal System.Windows.Forms.Button btnBankName;
        internal System.Windows.Forms.ComboBox BankNameReferenceComboBox;
        internal System.Windows.Forms.Label LblBankName;
        internal System.Windows.Forms.Button btnTranType;
        private System.Windows.Forms.ComboBox cboWorkStatus;
        private System.Windows.Forms.ToolStripMenuItem btnSalaryStructure;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel LblEmployeeStatus;
        internal System.Windows.Forms.TextBox txtLocalMobile;
        internal System.Windows.Forms.Button btnWorkLocation;
        internal System.Windows.Forms.ComboBox cboWorkLocation;
        private System.Windows.Forms.ToolStripMenuItem mnuLeaveStructure;
        internal System.Windows.Forms.TextBox txtPersonID;
        private System.Windows.Forms.ToolStripMenuItem passportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qualificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nationalIDCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insuranceCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem labourCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem healthCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherDocumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label lblTransferDate;
        private System.Windows.Forms.Label lblTransfer;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape8;
        internal System.Windows.Forms.PictureBox RecentPhotoPictureBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        internal System.Windows.Forms.Button btnVacationPolicy;
        internal System.Windows.Forms.ComboBox cboVacationPolicy;
        private System.Windows.Forms.ToolStripMenuItem mnuLeave;
        private System.Windows.Forms.ToolStripMenuItem mnuLoan;
        private System.Windows.Forms.ToolStripMenuItem mnuSalaryAdvance;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        internal System.Windows.Forms.ComboBox cboReportingTo;
        private DevComponents.DotNetBar.Bar bSearch;
        internal DevComponents.DotNetBar.Controls.TextBoxX txtSearch;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.ButtonItem btnSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.TextBox txtDeviceUserID;
        private System.Windows.Forms.Label lblDeviceUser;
        internal System.Windows.Forms.ComboBox cboMaritalStatus;
        internal System.Windows.Forms.TextBox txtLocalPhone;
        internal System.Windows.Forms.TextBox txtLocalAddress;
        internal System.Windows.Forms.TextBox txtEmergencyPhone;
        internal System.Windows.Forms.TextBox txtEmergencyAddress;
        private System.Windows.Forms.TextBox TxtLastNameArb;
        private System.Windows.Forms.TextBox TxtMiddleNameArb;
        private System.Windows.Forms.TextBox TxtFirstNameArb;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        internal System.Windows.Forms.ComboBox cboCountry;
        internal System.Windows.Forms.Button btnCountry;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.TextBox txtAddress1;
        internal System.Windows.Forms.TextBox txtEmail;
        internal System.Windows.Forms.TextBox txtPhone;
        internal System.Windows.Forms.TextBox txtEmployeeFullNameArb;
        internal System.Windows.Forms.PictureBox RecentPhotoPictureBoxThamNail;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.ComboBox cboGrade;
        internal System.Windows.Forms.Button btnGrade;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.CheckBox chkOther;
        private System.DirectoryServices.DirectorySearcher directorySearcher1;
        private System.Windows.Forms.Label lblRejoinDateDesc;
        private System.Windows.Forms.Label lblRejoinDate;
        internal System.Windows.Forms.ComboBox CboVisaObtained;
        internal System.Windows.Forms.ComboBox cboBloodGroup;
        internal System.Windows.Forms.TextBox txtZipCode;
        internal System.Windows.Forms.TextBox txtState;
        internal System.Windows.Forms.TextBox txtCity;
        internal System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.DateTimePicker dtpNextAppraisalDate;
        private System.Windows.Forms.TabPage TbOtherInfo;
        internal System.Windows.Forms.ComboBox cboMothertongue;
        internal System.Windows.Forms.Button Btnmothertongue;
        internal System.Windows.Forms.TextBox txtNotes;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer3;
        internal Microsoft.VisualBasic.PowerPacks.LineShape lineShape7;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColLanguages;
        internal System.Windows.Forms.DataGridView dgvLanguages;
        internal System.Windows.Forms.ComboBox cboHOD;
        private System.Windows.Forms.CheckBox chkOCom;
        private System.Windows.Forms.TextBox txtNoticePeriod;
        internal System.Windows.Forms.ComboBox cboCommissionStructure;
        private System.Windows.Forms.ComboBox cboProcessType;
        internal System.Windows.Forms.ComboBox cboDivision;
        internal System.Windows.Forms.Button btnDivision;
    }
}