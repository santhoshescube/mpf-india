﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
/* 
================================================
   *  Author:		<Author,,Thasni>
    Modified date: <Modified Date,,21 August 2013>
    Description:	<Description,,WORKLOCATION Form>
================================================
*/

namespace MyPayfriend
{
   
    public partial class FrmWorkLocation : Form
    {
        #region Declarations

        #region Class Objects

        clsBLLWorkLocation MobjClsBLLWorkLocation;
        ClsLogWriter MObjLogs;                          //  Object for Class Clslogs
        ClsCommonUtility MobjClsCommonUtility;
        ClsNotification MObjClsNotification;

        #endregion
        private clsMessage objUserMessage = null;
        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.WorkLocation);

                return this.objUserMessage;
            }
        }

        private bool MblnAddStatus;       //Status for Addition/Updation mode 
        private int MintTimerInterval;                  // To set timer interval

        //To set permissions
        bool MblnPrintEmailPermission = false;
        bool MblnAddPermission = false;
        bool MblnUpdatePermission = false;
        bool MblnDeletePermission = false;
        private int MintRecordCnt = 0;
        private int MintCurrentRecCnt = 0;

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;
        private string MsMessageCaption;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;
        private bool MblnChangeStatus = false; //To Specify Change Status

        public int CompanyId { get; set; }
        public bool blnWorkLocationFromOtherPage = false;
        public int intWorkLocationIDFromOtherPage = 0;
        string strBindingOf = "Of ";
        #endregion
        #region Constructor
        public FrmWorkLocation()
        {
            InitializeComponent();

           tmrClear.Interval= ClsCommonSettings.TimerInterval;
            MsMessageCaption = ClsCommonSettings.MessageCaption;
            MmessageIcon = MessageBoxIcon.Information;

            MobjClsBLLWorkLocation = new clsBLLWorkLocation();
            MObjLogs = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();
            MobjClsCommonUtility = new ClsCommonUtility();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.WorkLocation, this);

            strBindingOf = "من ";
        }
        #endregion
        #region Events
        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Work Location";
                    ObjEmailPopUp.EmailFormType = EmailFormID.WorkLocation;
                    ObjEmailPopUp.EmailSource = MobjClsBLLWorkLocation.DisplayWorkLocation();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjLogs.WriteLog("Error on Email:BtnEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmail_Click " + Ex.Message.ToString());
            }
        }

        private void FrmWorkLocation_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "WorkLocation";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    //case Keys.Control | Keys.S:
                    //    btnSave_Click(sender,new EventArgs());//save
                    //  break;
                    //case Keys.Control | Keys.O:
                    //    btnOk_Click(sender, new EventArgs());//OK
                    //    break; 
                    //case Keys.Control | Keys.L:
                    //    btnCancel_Click(sender, new EventArgs());//Cancel
                    //    break;
                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled) BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled) BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (btnClear.Enabled) btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (BindingNavigatorMovePreviousItem.Enabled) BindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        if (BindingNavigatorMoveNextItem.Enabled) BindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        if (BindingNavigatorMoveFirstItem.Enabled) BindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveLastItem.Enabled) BindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (btnPrint.Enabled) btnPrint_Click(sender, new EventArgs());//print
                        break;
                    case Keys.Control | Keys.M:
                        if (btnEmail.Enabled) btnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void FrmWorkLocation_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                txtLocationName.Focus();

                SaveWorkLocation();
               
                    

            }
            catch (Exception ex)
            {
                MObjLogs.WriteLog("Following error occured in bnSaveItem_Click : " + ex.Message, 1);
            }
        }
        private void FrmWorkLocation_Load(object sender, EventArgs e)
        {
            LoadMessage();
            SetPermissions();
            ChangeStatus();
            LoadCompany();
            AddNewWorkLocation();

            if (blnWorkLocationFromOtherPage && intWorkLocationIDFromOtherPage > 0)
            {
                DataTable datTemp = MobjClsBLLWorkLocation.GetCurrentIndex(intWorkLocationIDFromOtherPage);
                if (datTemp != null && datTemp.Rows.Count > 0)
                {
                    MintCurrentRecCnt = datTemp.Rows[0]["RowIndex"].ToInt32() + 1;
                    BindingNavigatorMovePreviousItem_Click(sender, e);
                }
            }
            MblnChangeStatus = false;
            //cboCompany.Enabled = false;
        }
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.lngWorkLocationID > 0)
            {
                try
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    if (MobjClsBLLWorkLocation.LocationAlreadyExists())
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2236, out MmessageIcon);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        tmrClear.Enabled = true;
                        return;
                    }
                    if (MobjClsBLLWorkLocation.DeleteWorkLocation())
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                        lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        tmrClear.Enabled = true;
                        AddNewWorkLocation();
                    }

                }
                catch (Exception ex)
                {
                    if (ClsCommonSettings.ShowErrorMess)
                        MessageBox.Show("Error in BindingNavigatorDeleteItem_CLick() " + ex.Message);
                    MObjLogs.WriteLog("Error in BindingNavigatorDeleteItem_Click() " + ex.Message, 2);
                }

            }
        }

        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            try
            {
                AddNewWorkLocation();
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in bnAddNewItem_Click() " + ex.Message);
                MObjLogs.WriteLog("Error in bnAddNewItem_Click() " + ex.Message, 2);
            }
        }
        private void BindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                MintCurrentRecCnt = MintCurrentRecCnt - 1;
                if (MintCurrentRecCnt <= 0)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    DisplayWorkLocationInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(10);
                    tmrClear.Enabled = true;
                    MblnChangeStatus = false;
                }
            }
            else
            {
                MintCurrentRecCnt = 1;
            }
        }
        private void BindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {

            GetRecordCount();
            if (MintCurrentRecCnt > 1)
            {
                MintCurrentRecCnt = 1;
                DisplayWorkLocationInfo();
                lblStatus.Text = UserMessage.GetMessageByCode(9);
                tmrClear.Enabled = true;
                MblnChangeStatus = false;
            }
        }

        private void BindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt == 1)
                {
                    MintCurrentRecCnt = 1;
                }
                else
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                }

                if (MintCurrentRecCnt > MintRecordCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                }
                else
                {
                    DisplayWorkLocationInfo();
                    lblStatus.Text = UserMessage.GetMessageByCode(11);
                    MblnChangeStatus = false;
                    tmrClear.Enabled = true;
                }
            }
            else
            {
                MintCurrentRecCnt = 0;
            }
        }

        private void BindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            GetRecordCount();
            if (MintRecordCnt > 0)
            {
                if (MintRecordCnt != MintCurrentRecCnt)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    DisplayWorkLocationInfo();
                    MblnChangeStatus = false;
                    lblStatus.Text = UserMessage.GetMessageByCode(12);
                    tmrClear.Enabled = true;
                }
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (SaveWorkLocation())
            {
                //UserMessage.ShowMessage(2);
                lblStatus.Text = UserMessage.GetMessageByCode(2);
                btnSave.Enabled = MblnChangeStatus = false;
                this.Close();

            }
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNewWorkLocation();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            LoadReport();
        }



        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region Functions
        /// <summary>
        /// Load Company Combo
        /// </summary>
        private void LoadCompany()
        {
            DataTable datCombo = MobjClsCommonUtility.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID in (Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });
            cboCompany.DisplayMember = "CompanyName";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = datCombo;
            
        }
        /// <summary>
        /// Set permission variables according to the Role settings
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3 )
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.WorkLocation, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }
        /// <summary>
        /// save work location
        /// </summary>
        /// <returns></returns>
        private bool SaveWorkLocation()
        {
            try
            {
                if (WorkLocationValidation())
                {
                    if (MblnAddStatus == true)
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);

                    }
                    else
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);
                    }
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return false;

                    FillParameters();
                    MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.lngWorkLocationID = MobjClsBLLWorkLocation.SaveWorkLocation(MblnAddStatus);
                    if (MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.lngWorkLocationID>0)
                    {

                        MblnAddStatus = false;
                        SetBindingNavigatorButtons();                      
                        btnOk.Enabled = BindingNavigatorSaveItem.Enabled = btnSave.Enabled = false;                       
                        lblStatus.Text = UserMessage.GetMessageByCode(2);

                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error in SaveWorkLocation() " + ex.Message);
                MObjLogs.WriteLog("Error in SaveWorkLocation() " + ex.Message, 2);
                return false;
            }
        }
        /// <summary>
        /// Set DTO properties
        /// </summary>
        private void FillParameters()
        {
            MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.strLocationName = txtLocationName.Text.ToString();
            MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.intCompanyID = cboCompany.SelectedValue.ToInt32();
            MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.strAddress = txtAddress.Text.ToString();
            MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.strPhone = txtPhone.Text.ToString();
            MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.strEmail = txtEmail.Text.ToString();
        }
        /// <summary>
        /// Load message array
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.WorkLocation, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.WorkLocation, ClsCommonSettings.ProductID);
        }
        /// <summary>
        /// Add new Mode
        /// </summary>
        private void AddNewWorkLocation()
        {
            MblnAddStatus = true;
            MobjClsBLLWorkLocation = new clsBLLWorkLocation();
            errWorkLocation.Clear();
            ClearControls();
            MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2228, out MmessageIcon);
            lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            tmrClear.Enabled = true;
            GetRecordCount();
            MintRecordCnt = MintRecordCnt + 1;
            MintCurrentRecCnt = MintRecordCnt;
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorPositionItem.Text = MintRecordCnt.ToString();
            BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = BindingNavigatorSaveItem.Enabled = btnEmail.Enabled = btnPrint.Enabled = btnSave.Enabled = btnOk.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorAddNewItem.Enabled = false;
            btnClear.Enabled = BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = true;
            btnClear.Enabled= true;
            MblnChangeStatus = false;
            cboCompany.SelectedValue = ClsCommonSettings.CurrentCompanyID;
           // cboCompany.Enabled = false;
            
        }
        /// <summary>
        /// Clear Controls
        /// </summary>
        private void ClearControls()
        {
            txtEmail.Text = txtPhone.Text = txtAddress.Text = txtLocationName.Text = string.Empty;
            //cboCompany.SelectedIndex = -1;
        }

       /// <summary>
       /// Display worklocation details
       /// </summary>
        private void DisplayWorkLocationInfo()
        {
            FillWorkLocationInfo();
            MblnAddStatus = false;
            BindingNavigatorPositionItem.Text = MintCurrentRecCnt.ToString();
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            btnClear.Enabled = btnOk.Enabled = btnSave.Enabled = BindingNavigatorSaveItem.Enabled = false;
            SetBindingNavigatorButtons();
        }
        /// <summary>
        /// Fill worklocation details
        /// </summary>
        private void FillWorkLocationInfo()
        {
            if (MobjClsBLLWorkLocation.DisplayWorkLocation(MintCurrentRecCnt))
            {
                //if (IsWorkLocationExists())
                //    cboCompany.Enabled = false;
                //else
                //    cboCompany.Enabled = true;

                txtLocationName.Text = MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.strLocationName.ToString().Trim();
                cboCompany.SelectedValue = MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.intCompanyID;
                txtAddress.Text = MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.strAddress.ToString().Trim();
                txtPhone.Text = MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.strPhone.ToString().Trim();
                txtEmail.Text = MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.strEmail.ToString().Trim();
            }
        }
        /// <summary>
        /// Checking worklocation exists for employee
        /// </summary>
        /// <returns></returns>
        private bool IsWorkLocationExists()
        {
            DataTable datTemp = MobjClsCommonUtility.FillCombos(new string[] { "EmployeeID", "EmployeeMaster", "WorkLocationID = " + MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.lngWorkLocationID });
            if (datTemp != null && datTemp.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Set the binding navigator record count varable
        /// </summary>
        private void GetRecordCount()
        {
            MintRecordCnt = 0;

            MintRecordCnt = MobjClsBLLWorkLocation.GetRecordCount();
            if (MintRecordCnt < 0)
            {
                BindingNavigatorCountItem.Text = strBindingOf + "0";
                MintRecordCnt = 0;
            }

        }
        /// <summary>
        /// Set the bindnavaigator buttons
        /// </summary>        
        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMoveLastItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = false;
            }
            btnEmail.Enabled = btnPrint.Enabled = MblnPrintEmailPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            btnClear.Enabled = false;

        }
        /// <summary>
        /// form Validation
        /// </summary>
        /// <returns></returns>
        private bool WorkLocationValidation()
        {
            errWorkLocation.Clear();

            if (txtLocationName.Text.Trim() == "")
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2222, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errWorkLocation.SetError(txtLocationName, MsMessageCommon.Replace("#", "").Trim());
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmrClear.Enabled = true;
                txtLocationName.Focus();
                return false;
            }
            else if (MobjClsBLLWorkLocation.WorkLocationNameAlreadyExists(MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.lngWorkLocationID, txtLocationName.Text.Trim().ToString(), cboCompany.SelectedValue.ToInt32()))
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 2245, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errWorkLocation.SetError(txtLocationName, MsMessageCommon.Replace("#", "").Trim());
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmrClear.Enabled = true;
                txtLocationName.Focus();
                return false;
            }
            else if (cboCompany.SelectedIndex == -1)
            {
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 14, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                errWorkLocation.SetError(cboCompany, MsMessageCommon.Replace("#", "").Trim());
                lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                tmrClear.Enabled = true;
                cboCompany.Focus();
                return false;
            }
            else if (txtEmail.Text != "")
            {
                if (MobjClsCommonUtility.CheckValidEmail(Convert.ToString(txtEmail.Text.Trim())) == false)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9715, out MmessageIcon);
                    errWorkLocation.SetError(txtEmail, MsMessageCommon.Replace("#", "").Trim());
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    lblStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    tmrClear.Enabled = true;
                    txtEmail.Focus();
                    return false;
                }
            }

            return true;
        }
        /// <summary>
        /// Change status while edit any of the value inside the form
        /// </summary>
        private void ChangeStatus()
        {
            errWorkLocation.Clear();
            MblnChangeStatus = true;
            lblStatus.Text = string.Empty;
            if (MblnAddStatus)
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;

            }
            else
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;

            }
        }
        public void ChangeStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }
        /// <summary>
        /// load report for Print work location 
        /// </summary>

        private void LoadReport()
        {
            try
            {
                if (MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.lngWorkLocationID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MobjClsBLLWorkLocation.PobjClsDTOWorkLocation.lngWorkLocationID;
                    ObjViewer.intCompanyID = cboCompany.SelectedValue.ToInt32();
                    ObjViewer.PiFormID = (int)FormID.WorkLocation;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjLogs.WriteLog("Error on form FrmWorkLocation:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }
        #endregion     

        private void tmrClear_Tick(object sender, EventArgs e)
        {
            tmrClear.Enabled = false;
            lblStatus.Text = string.Empty;
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            try
            {
                FrmHelp objHelp = new FrmHelp();
                objHelp.strFormName = "Worklocation";
                objHelp.ShowDialog();
                objHelp = null;
            }
            catch (Exception Ex)
            {
                

            }
        }


    }
}
