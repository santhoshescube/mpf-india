﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date, 5 April 2011>
Description:	<Description, CurrencyReference Form>
 Author:		<Author,Thasni>
Modified date: <Modified Date, 19 August 2013>
Description:	<Description, CurrencyReference Form>
================================================
*/
namespace MyPayfriend
{
    public partial class FrmCurrencyReference : Form
    {
        #region Variables Declaration
        public int PintCurrency = 0;

        private bool MblnChangeStatus = false;            //Check state of the page
        private bool MblnAddStatus;
        //private bool MblnViewPermission = true;//To Set View Permission
        private bool MblnAddPermission = true;//To Set Add Permission
        private bool MblnUpdatePermission = true;//To Set Update Permission
        private bool MblnDeletePermission = true;//To Set Delete Permission        
        private bool MblnPrintEmailPermission = true;//To Set Delete Permission        

        private int MiTimerInterval;
        private int MiCurrencyDetID = 0;
        private int MiCompanyId = 0;

        private string MstrMessageCommon;

        private MessageBoxIcon MmsgMessageIcon;
        
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        clsBLLCurrencyReference MobjclsBLLCurrencyReference;
        ClsLogWriter MobjLogWriter;
        ClsNotification MobjNotification;
        #endregion //Variables Declaration
        #region Constructor
        public FrmCurrencyReference()
        {
            InitializeComponent();
            //MbShowErrorMess = true;
            MiCompanyId = ClsCommonSettings.CurrentCompanyID;         // current companyid
            MiTimerInterval = ClsCommonSettings.TimerInterval;   // current companyid
            MmsgMessageIcon = MessageBoxIcon.Information;
            MobjclsBLLCurrencyReference = new clsBLLCurrencyReference();
            MobjLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjNotification = new ClsNotification();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.Currency, this);
        }
        #endregion //Constructor
        #region Events
        private void DgvCurrencyRef_KeyDown(object sender, KeyEventArgs e)
        {

            DisplayCurrency();
        }

        private void TxtDecimalPlace_KeyPress(object sender, KeyPressEventArgs e)
        {
            Boolean nonNumericEntered = true;

            if (((e.KeyChar) >= 48 && (e.KeyChar) <= 57) || (e.KeyChar) == 8)
                nonNumericEntered = false;
            if (nonNumericEntered == true)
                e.Handled = true;
            else
                e.Handled = false;
        }

        private void DgvCurrencyRef_KeyUp(object sender, KeyEventArgs e)
        {
            DisplayCurrency();
        }
        private void FrmCurrencyReference_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "Currency";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled)
                            BindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled)
                            BindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if(BtnClear.Enabled)
                        BtnClear_Click(sender, new EventArgs());//Clear
                        break;


                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form DeleteItem:DeleteItem_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteItem_Click " + Ex.Message.ToString());

            }
        }
        private void FrmCurrencyReference_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus == true)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 8, out MmsgMessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }


        private void BtnClear_Click(object sender, EventArgs e)
        {
            AddNewCurrency();
        }

        private void CboCountry_KeyDown(object sender, KeyEventArgs e)
        {
            CboCountry.DroppedDown = false;
        }

        private void TxtDecimalPlace_TextChanged(object sender, EventArgs e)
        {
            TxtDecimalPlace.Text = TxtDecimalPlace.Text.ToInt32().ToString();
            ChangeStatus();
        }
        private void BindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (DeleteValidation(MiCurrencyDetID))
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 13, out MmsgMessageIcon);
                    if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    if (MobjclsBLLCurrencyReference.DeleteCurrency(MiCurrencyDetID))
                    {                       
                        MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4, out MmsgMessageIcon);
                        lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);

                        AddNewCurrency();
                        FillCurrencyDetails();
                    }
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form DeleteItem:DeleteItem_Click" + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteItem_Click " + Ex.Message.ToString());
            }

        }


        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Currency";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (Currencyvalidation())
            {
                if (SaveCurrency())
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 2, out MmsgMessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    FillCurrencyDetails();
                    AddNewCurrency();
                }
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (Currencyvalidation())
            {
                if (SaveCurrency())
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 2, out MmsgMessageIcon);
                    lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                    FillCurrencyDetails();
                    AddNewCurrency();
                    this.Close();
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnCountry_Click(object sender, EventArgs e)
        {
            try
            {
                MiCompanyId = Convert.ToInt32(CboCountry.SelectedValue);
                FrmCommonRef objCommon = new FrmCommonRef("Country", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryID,CountryName As Country,CountryNameArb As CountryArb", "CountryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCombos((MblnAddStatus) ? 1 : 2);
                if (objCommon.NewID != 0)
                    CboCountry.SelectedValue = objCommon.NewID;
                else
                    CboCountry.SelectedValue = MiCompanyId;
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnCurrency " + Ex.Message.ToString());
            }

        }

        private void DgvCurrencyRef_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DisplayCurrency();
        } 
        private void FrmCurrencyReference_Load(object sender, EventArgs e)
        {
            try
            {
                MblnChangeStatus = false;
                MblnAddStatus = true;
                LoadMessage();
                SetPermissions();
                FillCurrencyDetails();
                if (PintCurrency > 0)
                {                  
                    SetCurrency();
                   
                }
                else
                {
                   
                    AddNewCurrency();
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form Load:FrmCurrencyReference_Load " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmCurrencyReference_Load() " + Ex.Message.ToString());
            }
        }
        
        private void BindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            AddNewCurrency();
        }

        
        private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }

        #endregion
        #region Functions
        /// <summary>
        /// Set the grid accoprding to the selected currency
        /// </summary>
        private void SetCurrency()
        {
            int Rowindex = -1;
            // lamda expression for finding the currency grid
            DataGridViewRow row = DgvCurrencyRef.Rows.Cast<DataGridViewRow>()
                .Where(r => r.Cells["CurrencyID"].Value.ToString().Equals(PintCurrency.ToString())).First();

            Rowindex = row.Index;
            DgvCurrencyRef.ClearSelection();
            DgvCurrencyRef.Rows[Rowindex].Selected = true;
            DgvCurrencyRef.CurrentCell = DgvCurrencyRef["Currency", Rowindex];
            DisplayCurrencyDetails();
        }
        /// <summary>
        ///  change the form to add new mode
        /// </summary>
        private void AddNewCurrency()
        {
            try
            {
                LoadCombos(0);
                MblnAddStatus = true;
                ClearControls();
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 3160, out MmsgMessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrCurrency.Enabled = true;
                EnableDisableButtons(true);
                DgvCurrencyRef.ClearSelection();

                if (DgvCurrencyRef.RowCount > 0)
                {
                    BtnPrint.Enabled = MblnPrintEmailPermission;
                }
                else
                {
                    BtnPrint.Enabled = false;
                }
                BindingNavigatorAddNewItem.Enabled = false;
                BtnClear.Enabled = true;
                TxtCurrency.Focus();
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form AddNewCurrency:AddNewCurrency() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewCurrency() " + Ex.Message.ToString());
            }
        }
        /// <summary>
        /// Laod message array  according to formid
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MobjNotification.FillMessageArray((int)FormID.Currency, ClsCommonSettings.ProductID);
            MaStatusMessage = MobjNotification.FillStatusMessageArray((int)FormID.Currency, ClsCommonSettings.ProductID);
        }
        /// <summary>
        /// Clear all the controls in add mode
        /// </summary>
        private void ClearControls()
        {
            TxtShortName.Text = TxtDecimalPlace.Text = TxtCurrency.Text = string.Empty;
            CboCountry.SelectedIndex = -1;
            DgvCurrencyRef.ClearSelection();
            ErrCurrency.Clear();
        }
        /// <summary>
        /// Change the status while editing the form
        /// </summary>
        private void ChangeStatus()
        {
            MblnChangeStatus = true;
            if (MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID > 0)
            {
                CurrencyRefBindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
                BtnSave.Enabled = MblnUpdatePermission;
                BtnOk.Enabled = MblnUpdatePermission;
                BindingNavigatorAddNewItem.Enabled = MblnAddPermission;

            }
            else
            {
                CurrencyRefBindingNavigatorSaveItem.Enabled = MblnAddPermission;
                BtnSave.Enabled = MblnAddPermission;
                BtnOk.Enabled = MblnAddPermission;
            }
            ErrCurrency.Clear();
        }
        /// <summary>
        /// setting the permission according to the role settings of the logged user
        /// </summary>
        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID > 3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.Currency, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            CurrencyRefBindingNavigatorSaveItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            BtnOk.Enabled = MblnAddPermission;
        }
        /// <summary>
        /// Change status Event while changing any of the control inside the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChangeStatus(object sender, EventArgs e) // to set the event
        {
            ChangeStatus();
        }
        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>
        private void EnableDisableButtons(bool bEnable)
        {
            BtnEmail.Enabled = BtnPrint.Enabled = BindingNavigatorDeleteItem.Enabled = !bEnable;

            if (bEnable)
            {
                MblnChangeStatus = BtnSave.Enabled = BtnOk.Enabled = !bEnable; CurrencyRefBindingNavigatorSaveItem.Enabled = !bEnable;
            }
            else
            {
                BtnSave.Enabled = BtnOk.Enabled = CurrencyRefBindingNavigatorSaveItem.Enabled = MblnAddStatus;
                MblnChangeStatus = bEnable;
            }
        }
        /// <summary>
        /// existency  checking  while deleting a currency 
        /// </summary>
        /// <param name="intCurrentID"></param>
        /// <returns></returns>

        private bool DeleteValidation(int intCurrentID)
        {
            DataTable dtIds = MobjclsBLLCurrencyReference.CurrencyExists(intCurrentID);
            if (dtIds.Rows.Count > 0)
            {
                if (Convert.ToString(dtIds.Rows[0][0]) != "0")
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 558, out MmsgMessageIcon);
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                    TmrCurrency.Enabled = true;
                    return false;
                }
            }

            //if (dtIds.Rows.Count > 0)
            //{
            //    if (Convert.ToString(dtIds.Rows[0][0]) != "0")
            //    {
            //        MstrMessageCommon = MobjNotification.GetErrorMessage(MdatMessages, 3059, out MmsgMessageIcon);
            //        MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
            //        TmUOM.Enabled = true;
            //        return false;
            //    }
            //}
            return true;
        }
        /// <summary>
        /// Loading Combos

        /// </summary>
        /// <param name="intType"></param>
        /// <returns></returns>

        private bool LoadCombos(int intType)
        {

            try
            {
                DataTable datCombos = new DataTable();
                // intType -- 1 for add new mode avoiding alredy entered country names

                if (ClsCommonSettings.IsArabicView)
                {
                    if (intType == 0 || intType == 1)
                        datCombos = MobjclsBLLCurrencyReference.FillCombos(new string[] { "CountryID,CountryNameArb AS CountryName", "CountryReference", "CountryID not in ( select isnull(CountryID,0) as CountryID from  CurrencyReference)" });
                    if (intType == 2)
                        datCombos = MobjclsBLLCurrencyReference.FillCombos(new string[] { "CountryID,CountryNameArb AS CountryName", "CountryReference", "" });
                }
                else
                {
                    if (intType == 0 || intType == 1)
                        datCombos = MobjclsBLLCurrencyReference.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "CountryID not in ( select isnull(CountryID,0) as CountryID from  CurrencyReference)" });
                    if (intType == 2)
                        datCombos = MobjclsBLLCurrencyReference.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "" });
                }

                CboCountry.ValueMember = "CountryID";
                CboCountry.DisplayMember = "CountryName";
                CboCountry.DataSource = datCombos;

                return true;
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }


        /// <summary>
        /// Form validation function
        /// </summary>
        /// <returns></returns>

        private bool Currencyvalidation()
        {
            ErrCurrency.Clear();
            lblcompanystatus.Text = "";
            if (TxtCurrency.Text.Trim() == "")
            {
                // MstrMessageCommon = "Please enter a Currency name.";
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 552, out MmsgMessageIcon);
                ErrCurrency.SetError(TxtCurrency, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrCurrency.Enabled = true;
                TxtCurrency.Focus();
                return false;
            }
            if (TxtShortName.Text.Trim() == "")
            {
                //MstrMessageCommon = "Please enter a Short name.";
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 553, out MmsgMessageIcon);
                ErrCurrency.SetError(TxtShortName, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrCurrency.Enabled = true;
                TxtShortName.Focus();
                return false;
            }
            if (CboCountry.SelectedIndex == -1 || Convert.ToInt32(CboCountry.SelectedValue) <= 0)
            {
                //MstrMessageCommon = "Please select a valid Country.";
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 5, out MmsgMessageIcon);
                ErrCurrency.SetError(CboCountry, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrCurrency.Enabled = true;
                CboCountry.Focus();
                return false;
            }
            if (TxtDecimalPlace.Text.Trim() == "")
            {
                //MstrMessageCommon = "Please enter decimal place.";
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 555, out MmsgMessageIcon);
                ErrCurrency.SetError(TxtDecimalPlace, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrCurrency.Enabled = true;
                TxtDecimalPlace.Focus();
                return false;
            }

            if (Convert.ToInt32(TxtDecimalPlace.Text) > 3)
            {
                //MstrMessageCommon = "Scale should not be greater than 3";
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 556, out MmsgMessageIcon);
                ErrCurrency.SetError(TxtDecimalPlace, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrCurrency.Enabled = true;
                TxtDecimalPlace.Focus();
                return false;
            }
            int iCurID = 0;
            int iCountryID = 0;
            if (MblnAddStatus == true)
            {
                iCurID = MobjclsBLLCurrencyReference.CheckDuplicateCurrency(TxtCurrency.Text.Trim());//duplicate curr name add new mode
            }
            else
            {
                iCurID = MobjclsBLLCurrencyReference.CheckDuplicateCurrency(TxtCurrency.Text.Trim(), MiCurrencyDetID);//duplicate curr name update mode
                iCountryID = MobjclsBLLCurrencyReference.CheckDuplicateCountry(MiCurrencyDetID, Convert.ToInt32(CboCountry.SelectedValue));


            }
            if (iCurID > 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 559, out MmsgMessageIcon);
                ErrCurrency.SetError(TxtCurrency, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrCurrency.Enabled = true;
                TxtDecimalPlace.Focus();
                return false;
            }
            if (iCountryID > 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 560, out MmsgMessageIcon);
                ErrCurrency.SetError(CboCountry, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblcompanystatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                TmrCurrency.Enabled = true;
                TxtDecimalPlace.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// save currency
        /// </summary>
        /// <returns></returns>
        private bool SaveCurrency()
        {
            try
            {
                if (MblnAddStatus == true)

                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 557, out MmsgMessageIcon);
                else
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 3, out MmsgMessageIcon);


                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                if (MblnAddStatus == true)
                {
                    MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID = 0;
                }
                else
                {
                    MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID = MiCurrencyDetID;
                }
                MobjclsBLLCurrencyReference.clsDTOCurrencyReference.strDescription = TxtCurrency.Text.Replace("'", "’").Trim();
                MobjclsBLLCurrencyReference.clsDTOCurrencyReference.strShortDescription = TxtShortName.Text.Replace("'", "’").Trim();
                MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCountryID = Convert.ToInt32(CboCountry.SelectedValue);
                MobjclsBLLCurrencyReference.clsDTOCurrencyReference.shtScale = Convert.ToInt16(TxtDecimalPlace.Text);

                if (MobjclsBLLCurrencyReference.SaveCurrency(MblnAddStatus))
                {
                    //Mbblnfill = true;
                    //TxtCurrency.Tag = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID;

                    return true;

                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form save:SaveCurrency() " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on saving SaveCurrency() " + Ex.Message.ToString());
                return false;
            }
        }

        /// <summary>
        /// fill currenct grid
        /// </summary>
        private void FillCurrencyDetails()
        {
            DgvCurrencyRef.DataSource = null;
            DgvCurrencyRef.DataSource = MobjclsBLLCurrencyReference.GetCurrencyDetails();

            if (ClsCommonSettings.IsArabicView)
            {
                DgvCurrencyRef.Columns["Currency"].HeaderText = "عملة";
                DgvCurrencyRef.Columns["ShortName"].HeaderText = "اسم قصير";
                DgvCurrencyRef.Columns["Country"].HeaderText = "بلد";
                DgvCurrencyRef.Columns["Decimal"].HeaderText = "عشري";
            }

            DgvCurrencyRef.Columns[0].Visible = false;
            DgvCurrencyRef.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            DgvCurrencyRef.Columns[2].Width = 70;
            DgvCurrencyRef.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            DgvCurrencyRef.Columns[4].Width = 70;

        }

        /// <summary>
        /// display the currency details according to the selected currency from the grid
        /// </summary>
        private void DisplayCurrency()
        {

            if (DgvCurrencyRef.CurrentRow != null && Convert.ToInt32(DgvCurrencyRef.CurrentRow.Cells[0].Value) != 0)
            {
                BtnSave.Enabled = MblnUpdatePermission;
                DisplayCurrencyDetails();
            }
        }
        private void DisplayCurrencyDetails()
        {
            LoadCombos(2);//loading all Country
            lblcompanystatus.Text = string.Empty;
            ErrCurrency.Clear();
            if (PintCurrency > 0)
            {
                MiCurrencyDetID = PintCurrency;
                PintCurrency = 0;
            }
            else
            {
                MiCurrencyDetID = Convert.ToInt32(DgvCurrencyRef.CurrentRow.Cells[0].Value);
            }
            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            if (MobjclsBLLCurrencyReference.DisplayCurrency(MiCurrencyDetID))
            {
                TxtCurrency.Tag = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCurrencyID;
                TxtCurrency.Text = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.strDescription;
                TxtShortName.Text = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.strShortDescription;
                CboCountry.SelectedValue = MobjclsBLLCurrencyReference.clsDTOCurrencyReference.intCountryID;
                TxtDecimalPlace.Text = Convert.ToString(MobjclsBLLCurrencyReference.clsDTOCurrencyReference.shtScale);

            }
            CurrencyRefBindingNavigatorSaveItem.Enabled = MblnUpdatePermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            MblnChangeStatus = false;
            CurrencyRefBindingNavigatorSaveItem.Enabled = BtnSave.Enabled = BtnOk.Enabled = MblnChangeStatus;
            BtnClear.Enabled = MblnAddStatus = false;

        }
        #endregion

        private void TmrCurrency_Tick(object sender, EventArgs e)
        {
            lblcompanystatus.Text = string.Empty;
            TmrCurrency.Enabled = false;
        }

     

   
      
  
     

   


   
   

   
   

    
             
 

   

   
    }
}
