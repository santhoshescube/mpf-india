﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;


/* 
=================================================
Author:		 <Author,Ranju Mathew>
Create date: <Create Date, 20 june 2013>
Description: <Description, CompanyAssets Form>
================================================
*/

namespace MyPayfriend
{
    public partial class FrmCompanyAssets :Form
    {
        #region Variables Declaration

      private bool MbViewPermission = false; //To set view Permission
      private bool MbAddPermission = false; //To set Add Permission
      private bool MbUpdatePermission = false; //To set Update Permission
      private bool MbDeletePermission = false;  //To set Delete Permission
      private bool MbAddUpdatePermission = false; //To set AddUpdate Permission
      private bool MblnPrintEmailPermission = false; //To set PrintEmail Permission
      private int iFill = 0;
      private bool Search = false;
      public int AssetID { get; set; }

        private string MstrMessageCommon;
        private MessageBoxIcon MmsgMessageIcon;  // Object for MessageBoxIcon (Enum)

        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        clsBLLCompanyAssets MobjclsBLLCompanyAssets;   //Object Of Bll
        ClsLogWriter MobjLogWriter;    
        ClsNotification MobjNotification;  //Object Of Notification (Meassage Info)
        #endregion //Variables Declaration

        #region Constructor
        public FrmCompanyAssets()
        {
            InitializeComponent();
            MmsgMessageIcon = MessageBoxIcon.Information;
            MobjclsBLLCompanyAssets = new clsBLLCompanyAssets();
            MobjLogWriter = new ClsLogWriter(Application.StartupPath);
            MobjNotification = new ClsNotification();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.CompanyAssets, this);
        }
        #endregion //Constructor

        #region Methods

        #region LoadCombos
        /// <summary>
        /// To Load Combos 
        /// </summary>

        private void LoadCombos(int Type)
        {
             DataTable datcombos;
            if (Type == 0 || Type == 1)
            {
                datcombos=null;
                if (ClsCommonSettings.IsArabicView)
                    datcombos = MobjclsBLLCompanyAssets.FillCombos(new string[] { "BenefitTypeID,BenefitTypeNameArb As AssetType ", "BenefitTypeReference", "" });
                else
                    datcombos = MobjclsBLLCompanyAssets.FillCombos(new string[] { "BenefitTypeID,BenefitTypeName As AssetType ", "BenefitTypeReference", "" });
                cboBenefitType.ValueMember = "BenefitTypeID";
                cboBenefitType.DisplayMember = "AssetType";
                cboBenefitType.DataSource = datcombos;

            }
            if (Type == 0 || Type == 2)
            {
                datcombos = null;
                datcombos = MobjclsBLLCompanyAssets.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID in (Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ")" });
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datcombos;
               // cboCompany.SelectedIndex = 0;
            }


        }
        #endregion LoadCombos

        #region SetPermissions
        /// <summary>
        ///  Set Permission For View ,Add , Edit ,Email,Print
        /// </summary>
        private void SetPermissions()
        {

            // Function for setting permissions
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID >3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.CompanyAssets, out MblnPrintEmailPermission, out MbAddPermission, out MbUpdatePermission, out MbDeletePermission);

            }
            else
                MbAddPermission = MblnPrintEmailPermission = MbUpdatePermission = MbDeletePermission = true;

            if (MbAddPermission == true | MbUpdatePermission == true)
            {
                MbViewPermission = true;
            }
            else
            {
                MbViewPermission = false;
            }
            if (MbAddPermission == true | MbUpdatePermission == true)
            {
                MbAddUpdatePermission = true;
            }
            bnAdd.Enabled = MbAddPermission;
            BtnPrint.Enabled = MblnPrintEmailPermission;

           }

        #endregion SetPermissions

        #region LoadMessage

        /// <summary>
        /// To Show Message Info
        /// FormID.CompanyAssets ---> 161 
        /// </summary>
        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MobjNotification.FillMessageArray((int)FormID.CompanyAssets, ClsCommonSettings.ProductID);
            MaStatusMessage = MobjNotification.FillStatusMessageArray(161, ClsCommonSettings.ProductID);
        }
        #endregion LoadMessage

        #region AddNew
        /// <summary>
        /// To Add New Asset 
        /// </summary>
        private void AddNew()
        {
            if (ClsCommonSettings.IsArabicView)
            {
                cboStatus.Items.Clear();
                cboStatus.Items.Add("خى اشىي");
                cboStatus.Items.Add("هسسعثي");
                cboStatus.Items.Add("لامخؤنثي");
            }

            ClearControls();
            bnOtherDocuments.Enabled = bnInsurense.Enabled= btnDocument.Enabled= bnSave.Enabled =bnDelete.Enabled = false;
            bnClear.Enabled = true;
            errCompanyAssets.Clear();
          
            tmrCompanyAssets.Enabled = true;
            cboBenefitType.Focus();
            if (MobjclsBLLCompanyAssets.clsDTOCompanyAssets != null)
                MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID = 0;

            cboCompany.SelectedValue = ClsCommonSettings.CurrentCompanyID;
            //cboCompany.Enabled = false;
        }

        #endregion AddNew

        #region ClearControls
        /// <summary>
        /// Fuction  To Clear All the Fields
        /// </summary>
        private void ClearControls()
        {
            cboCompany.SelectedIndex = 0;
            cboBenefitType.SelectedIndex = -1;
            cboBenefitType_SelectedIndexChanged(null, null);
            txtDescription.Clear();
            dtpPurchaseDate.Value = DateTime.Now;
            dtpExpiryDate.Value = DateTime.Now;
            txtPurchaseValue.Clear();
            txtDepreciation.Clear();
            txtRefNo.Text = string.Empty;
            txtRemarks.Clear();
            cboStatus.SelectedIndex = 0;           
            cboBenefitType.Focus();
          btnbenefitType.Enabled= cboCompany.Enabled=cboBenefitType.Enabled = true;
        }
        #endregion ClearControls

        #region FormValidation
        /// <summary>
        ///  For From Validation 
        /// </summary>
        /// <returns></returns>

        private bool FormValidation()
        {

            if (cboCompany.SelectedValue == null)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10187, out MmsgMessageIcon).Replace("#", "").Trim();
                errCompanyAssets.SetError(cboCompany, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrCompanyAssets.Enabled = true;
                cboCompany.Focus();
                return false;
            }
            if (cboBenefitType.SelectedValue == null)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10171, out MmsgMessageIcon).Replace("#", "").Trim();
                errCompanyAssets.SetError(cboBenefitType, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrCompanyAssets.Enabled = true;
                cboBenefitType.Focus();
                return false;
            }

            if (txtRefNo.Text.Trim() == string.Empty)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr,10188, out MmsgMessageIcon).Replace("#", "").Trim();
                errCompanyAssets.SetError(txtRefNo, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrCompanyAssets.Enabled = true;
                txtRefNo.Focus();
                return false;
            }
            if (txtDescription.Text.Trim() == string.Empty)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10172, out MmsgMessageIcon).Replace("#", "").Trim();
                errCompanyAssets.SetError(txtDescription, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrCompanyAssets.Enabled = true;
                txtDescription.Focus();
                return false;
            }
            if (MobjclsBLLCompanyAssets.FillCombos(new string[] { "1", "CompanyAssets", "BenefitTypeID = " + cboBenefitType.SelectedValue + " and CompanyID="+cboCompany.SelectedValue.ToInt32() +" And Description = '" + txtDescription.Text.Trim() + "' and  CompanyAssetID <> " + MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID }).Rows.Count > 0)
            //if (objConnection.FillDataSet("Select 1 From CompanyAssets Where BenefitTypeID = " + cboBenefitType.SelectedValue + " And Description = '" + txtDescription.Text + "' And CompanyAssetID <> " + objCompanyAssets.CompanyAssetID + "").Tables(0).Rows.Count > 0)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10174, out MmsgMessageIcon).Replace("#", "").Trim();
                errCompanyAssets.SetError(txtDescription, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrCompanyAssets.Enabled = true;
                txtDescription.Focus();
                return false;
            }

            if (dtpExpiryDate.Enabled && dtpPurchaseDate.Value > dtpExpiryDate.Value)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10173, out MmsgMessageIcon).Replace("#", "").Trim();
                errCompanyAssets.SetError(dtpPurchaseDate, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrCompanyAssets.Enabled = true;
                dtpPurchaseDate.Focus();
                return false;
            }

            if (cboStatus.SelectedIndex == -1)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10176, out MmsgMessageIcon).Replace("#", "").Trim();
                errCompanyAssets.SetError(cboStatus, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrCompanyAssets.Enabled = true;
                cboStatus.Focus();
                return false;
            }

            if (cboStatus.SelectedIndex == 1 && cboStatus.Enabled == true)
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10175, out MmsgMessageIcon).Replace("#", "").Trim();
                errCompanyAssets.SetError(cboStatus, MstrMessageCommon.Replace("#", "").Trim());
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmsgMessageIcon);
                lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                tmrCompanyAssets.Enabled = true;
                cboStatus.Focus();
                return false;
            }
           
            
            //if (Information.IsNumeric(txtPurchaseValue.Text) == false)
            //{
            //    txtPurchaseValue.Text = 0;
            //}
            //if (Information.IsNumeric(txtDepreciation.Text) == false)
            //{
            //    txtDepreciation.Text = 0;
            //}
            return true;
        }
        #endregion FormValidation

        #region FillCompanyAssets
        /// <summary>
        /// To Fiil All The Details to Grid (dgvCompanyAssets)
        /// </summary>
        private void FillCompanyAssets()
        {
            if (Search) return;
            
            MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intBenefitTypeID = Convert.ToInt32(cboBenefitType.SelectedValue);
            MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyID = Convert.ToInt32(cboCompany.SelectedValue);

            if (MobjclsBLLCompanyAssets == null)
                MobjclsBLLCompanyAssets = new clsBLLCompanyAssets();
            DataTable datCompanyAssets = MobjclsBLLCompanyAssets.GetCompanyAssetDetails();
            dgvCompanyAssets.Rows.Clear();

            for (int i = 0; i <= datCompanyAssets.Rows.Count - 1; i++)
            {
                dgvCompanyAssets.Rows.Add();

                dgvCompanyAssets.Rows[i].Cells[dgvColCompanyAssetID.Index].Value = datCompanyAssets.Rows[i]["CompanyAssetID"];
                dgvCompanyAssets.Rows[i].Cells[dgvColBenefitTypeID.Index].Value = datCompanyAssets.Rows[i]["BenefitTypeID"];
                dgvCompanyAssets.Rows[i].Cells[dgvColBenefitTypeName.Index].Value = datCompanyAssets.Rows[i]["BenefitTypeName"];
                dgvCompanyAssets.Rows[i].Cells[dgvColDescription.Index].Value = datCompanyAssets.Rows[i]["Description"];
                dgvCompanyAssets.Rows[i].Cells[dgvColPurchaseDate.Index].Value = datCompanyAssets.Rows[i]["PurchaseDate"];
                dgvCompanyAssets.Rows[i].Cells[dgvColExpiryDate.Index].Value = datCompanyAssets.Rows[i]["ExpiryDate"];
                dgvCompanyAssets.Rows[i].Cells[dgvColPurchaseValue.Index].Value = datCompanyAssets.Rows[i]["PurchaseValue"];
                dgvCompanyAssets.Rows[i].Cells[dgvColDepreciation.Index].Value = datCompanyAssets.Rows[i]["Depreciation"];
                dgvCompanyAssets.Rows[i].Cells[dgvColRemarks.Index].Value = datCompanyAssets.Rows[i]["Remarks"];
                dgvCompanyAssets.Rows[i].Cells[dgvColStatusID.Index].Value = datCompanyAssets.Rows[i]["Status"];

                string strTemp = datCompanyAssets.Rows[i]["StatusDescription"].ToString();
                if (ClsCommonSettings.IsArabicView)
                {
                    strTemp = strTemp.Replace("On Hand", "خى اشىي");
                    strTemp = strTemp.Replace("Issued", "هسسعثي");
                    strTemp = strTemp.Replace("Blocked", "لامخؤنثي");
                }
                dgvCompanyAssets.Rows[i].Cells[dgvColStatus.Index].Value = strTemp;

                dgvCompanyAssets.Rows[i].Cells[CompanyID.Index].Value = datCompanyAssets.Rows[i]["CompanyID"];
                dgvCompanyAssets.Rows[i].Cells[RefNo.Index].Value = datCompanyAssets.Rows[i]["ReferenceNumber"];

            }
            //dgvCompanyAssets.EndEdit(); 
            BtnPrint.Enabled = (datCompanyAssets.Rows.Count > 0?MblnPrintEmailPermission:false);
         

        }
        #endregion FillCompanyAssets

        #region LoadReport
        /// <summary>
        /// Function To Load The Details (Print)
        /// </summary>
        private void LoadReport()
        {
            try
            {
                //if (cboBenefitType.SelectedValue.ToInt32() > 0)
                //{
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PintCompany = cboCompany.SelectedValue.ToInt32();
                    ObjViewer.PiRecId = cboBenefitType.SelectedValue.ToInt32();
                    ObjViewer.PiFormID = (int)FormID.CompanyAssets;
                    ObjViewer.ShowDialog();
                //}
            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form FrmCompanyAssets:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                //if (ClsCommonSettings.ShowErrorMess)
                //    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }
        #endregion LoadReport

        private void LoadAssets()
        {

        }


        #endregion Methods

        #region Events

        private void FrmCompanyAssets_Load(object sender, EventArgs e) // FrmCompanyAssets_Load 
        {
            try
            {
                LoadMessage();
                SetPermissions();
                LoadCombos(0);
                AddNew();
                cboBenefitType.Focus();
                cboBenefitType.Select();

                // Modified by laxmi for setting focus in grid and display data based on AssetID
                if (AssetID > 0)
                {
                    cboCompany.SelectedValue = MobjclsBLLCompanyAssets.GetAssetCompanyID(AssetID);
                    FillCompanyAssets();

                    int Rowindex = -1;

                    DataGridViewRow row = dgvCompanyAssets.Rows.Cast<DataGridViewRow>()
                        .Where(r => r.Cells["dgvColCompanyAssetID"].Value.ToString().Equals(AssetID.ToString())).First();
                    Rowindex = row.Index;
                    dgvCompanyAssets.ClearSelection();
                    dgvCompanyAssets.Rows[Rowindex].Selected = true;
                    dgvCompanyAssets.CurrentCell = dgvCompanyAssets["dgvColBenefitTypeName", Rowindex];
                    DataGridViewCellEventArgs eg = new DataGridViewCellEventArgs(0, Rowindex);
                    dgvCompanyAssets_CellClick(sender, eg);
                 
                }
                //cboCompany.SelectedValue = ClsCommonSettings.CurrentCompanyID;
                //cboCompany.Enabled = false;
            }
            catch (Exception Ex)
            {

               
            }
        }

     

        private void bnAdd_Click(object sender, EventArgs e) // Button Click To add New Asset
        {
            AddNew();
        } 

        private void bnSave_Click(object sender, EventArgs e) // Button Click To Save Details 
        {

             try
        {
            if (FormValidation() == false)
            {
                return;
            }
            if ((MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID == 0))
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 1, out MmsgMessageIcon).Trim();
               // lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
             
            }
            else
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 3, out MmsgMessageIcon).Trim();
               // lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
                
               
            }

            if (MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            if ((MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID == 0))
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 2, out MmsgMessageIcon).Trim();
                
            }
            else
            {
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 21, out MmsgMessageIcon).Trim();
                
            }
            MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyID = cboCompany.SelectedValue.ToInt32();
            MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intBenefitTypeID= Convert.ToInt32(cboBenefitType.SelectedValue);
            MobjclsBLLCompanyAssets.clsDTOCompanyAssets.strDescription = txtDescription.Text.Trim();
            MobjclsBLLCompanyAssets.clsDTOCompanyAssets.strRefeNo = txtRefNo.Text.Trim();

            MobjclsBLLCompanyAssets.clsDTOCompanyAssets.strPurchaseDate = dtpPurchaseDate.Value.ToString("dd-MMM-yyyy");
            if ((dtpExpiryDate.Checked))
            {
                MobjclsBLLCompanyAssets.clsDTOCompanyAssets.strExpiryDate = dtpExpiryDate.Value.ToString("dd-MMM-yyyy");
            }
            else
            {
                MobjclsBLLCompanyAssets.clsDTOCompanyAssets.strExpiryDate = "";
            }
            if (!string.IsNullOrEmpty(txtPurchaseValue.Text))
            {
                MobjclsBLLCompanyAssets.clsDTOCompanyAssets.dblPurchaseValue = Convert.ToDouble(txtPurchaseValue.Text);
            }
            else
            {
                MobjclsBLLCompanyAssets.clsDTOCompanyAssets.dblPurchaseValue = 0;
            }
            if (!string.IsNullOrEmpty(txtDepreciation.Text))
            {
                MobjclsBLLCompanyAssets.clsDTOCompanyAssets.dblDepreciation = Convert.ToDouble(txtDepreciation.Text);
            }
            else
            {
                MobjclsBLLCompanyAssets.clsDTOCompanyAssets.dblDepreciation = 0;
            }
            MobjclsBLLCompanyAssets.clsDTOCompanyAssets.strRemarks = txtRemarks.Text.Trim();

            MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intStatus = cboStatus.SelectedIndex + 1;


            int AssetID= MobjclsBLLCompanyAssets.SaveCompanyAssets();
            if ((AssetID > 0)&& (dtpExpiryDate.Checked))
                new clsAlerts().AlertMessage((int)DocumentType.Asset, "Asset", "Asset", AssetID, txtRefNo.Text.Trim(), dtpExpiryDate.Value.Date, "Comp", cboCompany.SelectedValue.ToInt32(), cboCompany.Text, true,0);

            if (MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intStatus == 3)
            {
                List<AlertsDetails> Alerts = new List<AlertsDetails>();
                 Alerts.Add(new AlertsDetails()
                    {
                        CommonId = AssetID
                        ,
                        DocumentTypeID = (int)DocumentType.Asset
                    });
                 new clsAlerts().DeleteAlerts(Alerts);
            }

            lblstatus.Text = MstrMessageCommon.Remove(0, MstrMessageCommon.IndexOf("#") + 1);
            tmrCompanyAssets.Enabled = true;

            MessageBox.Show(MstrMessageCommon, ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            AddNew();
        }
        catch (Exception ex)
        {
            //MessageBox.Show(ex.Message)
        }
    }  
         
        private void bnClear_Click(object sender, EventArgs e)  // Button Click to clear The Details 
        {
            ClearControls();
        }

        private void BtnPrint_Click(object sender, EventArgs e) // BtnPrint_Click to Print Report
        {
            LoadReport();
        }

        private void BtnEmail_Click(object sender, EventArgs e) //BtnEmail_Click to Email the details
        {

        }

        private void ChangeStatus(object sender, EventArgs e) // fuction used to enable / disable save button 
        {
            if (MobjclsBLLCompanyAssets.clsDTOCompanyAssets != null)
                if (MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID > 0)
                    bnSave.Enabled = MbUpdatePermission;
                else
                    bnSave.Enabled = MbAddPermission;

            //bnSave.Enabled = MbAddPermission;
            
        }

        private void cboBenefitType_SelectedIndexChanged(object sender, EventArgs e) // cboBenefitType_SelectedIndexChanged
        {
            if (!ClsCommonSettings.IsArabicView)
            {
                if (iFill == 0)
                {
                    FillCompanyAssets();
                    ChangeStatus(sender, e);
                }
            }
        }
        
        private void dgvCompanyAssets_CellClick(object sender, DataGridViewCellEventArgs e) // dgvCompanyAssets_CellClick
        {
            try
            {
                iFill = 1;
                if (e.RowIndex >= 0)
                {

                    MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID = Convert.ToInt32(dgvCompanyAssets[dgvColCompanyAssetID.Index, e.RowIndex].Value);

                    txtDescription.Text = Convert.ToString(dgvCompanyAssets[dgvColDescription.Index, e.RowIndex].Value);
                    dtpPurchaseDate.Value = Convert.ToDateTime(dgvCompanyAssets[dgvColPurchaseDate.Index, e.RowIndex].Value);
                    if (!string.IsNullOrEmpty(Convert.ToString(dgvCompanyAssets[dgvColExpiryDate.Index, e.RowIndex].Value)))
                    {
                        dtpExpiryDate.Checked = true;
                        dtpExpiryDate.Value = Convert.ToDateTime(dgvCompanyAssets[dgvColExpiryDate.Index, e.RowIndex].Value);
                    }
                    else
                    {
                        dtpExpiryDate.Checked = false;
                    }
                    txtPurchaseValue.Text = Convert.ToString(dgvCompanyAssets[dgvColPurchaseValue.Index, e.RowIndex].Value);
                    txtDepreciation.Text = Convert.ToString(dgvCompanyAssets[dgvColDepreciation.Index, e.RowIndex].Value);

                    if (ClsCommonSettings.IsAmountRoundByZero)
                    {
                        txtDepreciation.Text = txtDepreciation.Text.ToDecimal().ToString("F" + 0);
                        txtPurchaseValue.Text = txtPurchaseValue.Text.ToDecimal().ToString("F" + 0);
                    }

                    txtRefNo.Text = Convert.ToString(dgvCompanyAssets[RefNo.Index, e.RowIndex].Value);
                    txtRemarks.Text = Convert.ToString(dgvCompanyAssets[dgvColRemarks.Index, e.RowIndex].Value);
                    cboStatus.SelectedIndex = Convert.ToInt32(dgvCompanyAssets[dgvColStatusID.Index, e.RowIndex].Value) - 1;
                    Search = true;
                    cboCompany.SelectedValue = dgvCompanyAssets[CompanyID.Index, e.RowIndex].Value.ToInt32();
                    cboBenefitType.SelectedValue = Convert.ToInt32(dgvCompanyAssets[dgvColBenefitTypeID.Index, e.RowIndex].Value);
                    Search = false;
                    if (cboStatus.SelectedIndex == 1)
                    {
                        cboStatus.Enabled = false;
                    }
                    else
                    {
                        cboStatus.Enabled = true;
                    }
                    btnDocument.Enabled = MbAddPermission;
                    if (ClsCommonSettings.RoleID > 3)
                    {
                        DataTable dt = new clsBLLPermissionSettings().GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID);

                        dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.Insurance).ToString();
                        bnInsurense.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    }
                    else
                        bnInsurense.Enabled = true;


                    if (ClsCommonSettings.RoleID > 3)
                    {
                        DataTable dt = new clsBLLPermissionSettings().GetMenuPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID);

                        dt.DefaultView.RowFilter = "MenuID =" + ((int)eMenuID.OtherDocuments).ToString();
                        bnOtherDocuments.Enabled = (dt.DefaultView.ToTable().Rows.Count > 0);
                    }
                    else
                        bnOtherDocuments.Enabled = true;


                    bnDelete.Enabled = MbDeletePermission;
                    bnClear.Enabled = bnSave.Enabled = false;
                    btnbenefitType.Enabled = cboBenefitType.Enabled = cboCompany.Enabled = MobjclsBLLCompanyAssets.FillCombos(new string[] { "EmpBenefitID", "EmployeeBenefitMaster", "CompanyAssetID = " + MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID }).Rows.Count == 0;
                    cboCompany.Enabled = MobjclsBLLCompanyAssets.FillCombos(new string[] { "InsuranceID", "InsuranceDetails", "CompanyAssetID = " + MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID }).Rows.Count == 0;
                }
                iFill = 0;
            }

            catch (Exception ex)
            {
                iFill = 0;
                Search = false;
            }
        }


       

        private void bnDelete_Click(object sender, EventArgs e)  // Button Click For Deleting Details 
        {
            try
            {

                DataTable datIssueDetails =  MobjclsBLLCompanyAssets.FillCombos(new string[] { "1","EmployeeBenefitMaster ", "CompanyAssetID = " + MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID });
                   

                if (datIssueDetails.Rows.Count > 0)
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10177, out MmsgMessageIcon).Replace("#", "").Trim();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                
                    return;
                }

                datIssueDetails = MobjclsBLLCompanyAssets.FillCombos(new string[] { "1", "InsuranceDetails ", "CompanyAssetID = " + MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID });
                if (datIssueDetails.Rows.Count > 0)
                {
                    MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 10177, out MmsgMessageIcon).Replace("#", "").Trim();
                    MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }

                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 13, out MmsgMessageIcon).Replace("#", "").Trim();
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                MobjclsBLLCompanyAssets.DeleteCompanyAssets();
                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 4, out MmsgMessageIcon).Replace("#", "").Trim();
                MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                AddNew();

            
        }
            catch (Exception ex)
            {
            }
        } 

        private void tmrCompanyAssets_Tick(object sender, EventArgs e)  
        {
            lblstatus.Text = "";
            tmrCompanyAssets.Enabled = false;
            errCompanyAssets.Clear();
        }

        private void bnBenefitType_Click(object sender, EventArgs e)  // Button Click To Add Asset Type 
        {
          
            try
            {
                int  BenefitTypeID = Convert.ToInt32(cboBenefitType.SelectedValue);

                FrmCommonRef objCommon = new FrmCommonRef("Asset Types", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "BenefitTypeID,Ispredefine, BenefitTypeName As AssetType", "BenefitTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                
                LoadCombos(1);
                
                cboBenefitType.SelectedValue = BenefitTypeID;
            }
            catch (Exception Ex)
            {
                
                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  BnBenefitTypes " + Ex.Message.ToString());
            }
        }

        private void txtPurchaseValue_KeyPress(object sender, KeyPressEventArgs e) //txtPurchaseValue_KeyPress
        {
            try
            {

                if (ClsCommonSettings.IsAmountRoundByZero==true && e.KeyChar == 46)
                {
                    e.Handled = true;  
                }
                else if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void txtDepreciation_KeyPress(object sender, KeyPressEventArgs e) //txtDepreciation_KeyPress
        {
            try
            {
                if (ClsCommonSettings.IsAmountRoundByZero == true && e.KeyChar == 46)
                {
                    e.Handled = true;
                }
                else if (!((Char.IsDigit(e.KeyChar)) || ((e.KeyChar == 46)) || (e.KeyChar == 08) || (e.KeyChar == 13)))//Cheking Digit,".",backspace
                {
                    e.Handled = true;
                }
                if (((System.Windows.Forms.TextBox)sender).Text.Contains(".") && (e.KeyChar == 46))//checking more than one "."
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
               
            }
        }

        private void cboBenefitType_KeyDown(object sender, KeyEventArgs e) //cboBenefitType_KeyDown
        {
            cboBenefitType.DroppedDown = false;
        }

        private void cboStatus_KeyDown(object sender, KeyEventArgs e) //cboStatus_KeyDown
        {
            cboStatus.DroppedDown = false;
        }

        private void cboStatus_SelectedIndexChanged(object sender, EventArgs e) //cboStatus_SelectedIndexChanged
        {
            ChangeStatus(sender, e);
        }

        private void dtpPurchaseDate_ValueChanged(object sender, EventArgs e) //dtpPurchaseDate_ValueChanged
        {
            ChangeStatus(sender, e);
        }

        private void dtpExpiryDate_ValueChanged(object sender, EventArgs e) // dtpExpiryDate_ValueChanged
        {
            ChangeStatus(sender, e);
        }

        #endregion Events

        private void FrmCompanyAssets_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (bnSave.Enabled)
            {

                MstrMessageCommon = MobjNotification.GetErrorMessage(MaMessageArr, 8, out MmsgMessageIcon);
                if (MessageBox.Show(MstrMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Asset";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void FrmCompanyAssets_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        BtnHelp_Click(null, null);
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;

                    case Keys.Control | Keys.Enter:
                        if (bnAdd.Enabled)
                            bnAdd_Click(null,null);//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (bnDelete.Enabled)
                            bnDelete_Click(null,null);//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (bnClear.Enabled)
                            bnClear_Click(sender, null);//Clear
                        break;                
                    
                    case Keys.Control | Keys.P:
                        if(BtnPrint.Enabled)
                        BtnPrint_Click(null, null);//Cancel
                        break;
                    case Keys.Alt | Keys.S:
                        if (bnSave.Enabled)
                            bnSave_Click(null, null);//delete
                        break;
                   
                }
            }
            catch (Exception ex)
            {
                MobjLogWriter.WriteLog("Error form key press" + this.Name + " " + ex.Message.ToString(), 2);

            }
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                FillCompanyAssets();
                ChangeStatus(sender, e);
            
        }

        private void btnbenefitType_Click(object sender, EventArgs e)
        {
            try
            {
                int BenefitTypeID = Convert.ToInt32(cboBenefitType.SelectedValue);

                FrmCommonRef objCommon = new FrmCommonRef("Asset Types", new int[] { 2, 2, ClsCommonSettings.TimerInterval }, "BenefitTypeID,Ispredefine, BenefitTypeName As AssetType,BenefitTypeNameArb As AssetTypeArb", "BenefitTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();

                LoadCombos(1);

                cboBenefitType.SelectedValue = BenefitTypeID;
            }
            catch (Exception Ex)
            {

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on  BnBenefitTypes " + Ex.Message.ToString());
            }

        }

        private void btnDocument_Click(object sender, EventArgs e)
        {
            using (FrmScanning objScanning = new FrmScanning())
            {
                objScanning.MintDocumentTypeid = (int)DocumentType.Asset;
                objScanning.MlngReferenceID =cboCompany.SelectedValue.ToInt32() ;
                objScanning.MintOperationTypeID = (int)OperationType.Company;
                objScanning.MstrReferenceNo = txtRefNo.Text.Trim();
                objScanning.PblnEditable = true;
                objScanning.MintDocumentID = MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID;
                objScanning.ShowDialog();

            }
        }

        private void bnInsurense_Click(object sender, EventArgs e)
        {
            try
            {
                new InsuranceDetails() { CompanyID = cboCompany.SelectedValue.ToInt32(), AssetID = MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID }.ShowDialog();
                cboCompany.Enabled = MobjclsBLLCompanyAssets.FillCombos(new string[] { "InsuranceID", "InsuranceDetails", "CompanyAssetID = " + MobjclsBLLCompanyAssets.clsDTOCompanyAssets.intCompanyAssetID }).Rows.Count == 0;

            }
            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error on form nationalIDCardToolStripMenuItem_Click:Employee " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on nationalIDCardToolStripMenuItem_Click" + Ex.Message.ToString());
            }
        }

        private void cboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void bnOtherDocuments_Click(object sender, EventArgs e)
        {
            try
            {
                new frmDocumentMasterNew() { PintCompanyID = cboCompany.SelectedValue.ToInt32(), eOperationType = OperationType.Company }.ShowDialog();
            }

            catch (Exception Ex)
            {
                MobjLogWriter.WriteLog("Error form key press" + this.Name + " " + Ex.Message.ToString(), 2);
            }
        }

        private void dgvCompanyAssets_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvCompanyAssets.IsCurrentCellDirty)
                {
                    dgvCompanyAssets.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            {
            }
        }

        private void dgvCompanyAssets_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }

        }
    }
}
