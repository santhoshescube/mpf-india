﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Globalization;
/*
 * Created By       : Tijo
 * Created Date     : 27 Aug 2013
 * Purpose          : For Asset Handover
*/
namespace MyPayfriend
{
    public partial class FrmCompanyAssetOperations : DevComponents.DotNetBar.Office2007Form
    {
        clsBLLCompanyAssetOperations MobjclsBLLCompanyAssetOperations = new clsBLLCompanyAssetOperations();
        bool MblnAddPermission, MblnPrintEmailPermission, MblnUpdatePermission, MblnDeletePermission;
        bool mblnAddStatus = true;
        private clsMessage objUserMessage = null;
        bool blnIsReturn = false; // When return is click from grid
        int intAssetIssueID = 0; // when return is click from grid EmpBenefitID want to store in this for return Parent ID
        private int MintTimerInterval;// To set timer interval
        private clsMessage UserMessage
        {
            get
            {
                if (this.objUserMessage == null)
                    this.objUserMessage = new clsMessage(FormID.AssetHandover);

                return this.objUserMessage;
            }
        }

        public FrmCompanyAssetOperations()
        {
            InitializeComponent();
            tmrClear.Interval = ClsCommonSettings.TimerInterval;

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.AssetHandover, this);

            txtSearch.WatermarkText = "البحث عن طريق اسم الموظف / الرمز";
            lblStatusShow.Text = "حالة :";
            expnlSearch.Text = "الموظف البحث";
            lblCompany.Text = "شركة";
            btnSearchClear.Text = "مسح";
            btnSearch.Text = "بحث";
            bnMoreActions.Text = "الإجراءات";
            btnAddAssets.Text = "ممتلكات";
            btnOtherInfoType.Text = "معلومات أخرى نوع";
            expnlTop.Text = "عملية الأصول";
            lblBenefitType.Text = "أنواع الأصول";
            lblCompanyAssets.Text = "ممتلكات";
            label19.Text = "معلومات الموظف";
            lblEmployee.Text = "عامل";
            label18.Text = "معلومات الأصول";
            Label10.Text = "معلومات الاستخدام / الأضرار";
            label1.Text = lblJoiningDate.Text = "تاريخ الالتحاق بالعمل";
            lblPeriodFrom.Text = "مع تأثير التسجيل";
            lblEndDate.Text = "المتوقع تاريخ العودة";
            Label6.Text = "العودة التسجيل";
            Label3.Text = "تصريحات";
            label9.Text = "عاد الأصول";
            btnSave.Text = "حفظ";

            dgvOtherInfoDetails.Columns["AssetUsageTypeID"].HeaderText = "معلومات أخرى نوع";
            dgvOtherInfoDetails.Columns["Date"].HeaderText = "تاريخ";
            dgvOtherInfoDetails.Columns["Remarks"].HeaderText = "تصريحات";
        }

        private void FrmCompanyAssetOperations_Load(object sender, EventArgs e)
        {
            SetPermissions();
            MintTimerInterval = ClsCommonSettings.TimerInterval; // current companyid
            AddNew();
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objclsBLLPermissionSettings = new clsBLLPermissionSettings();

            if (ClsCommonSettings.RoleID > 3)
            {
                objclsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, 0, (int)eModuleID.Task, (int)eMenuID.AssetHandover,
                    out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void AddNew()
        {
            mblnAddStatus = true;
            blnIsReturn = false;
                        
            LoadCombos(0);
            cboAssetType.SelectedIndex = cboAssets.SelectedIndex = -1;
            //cboFilterCompany.SelectedValue = ClsCommonSettings.CurrentCompanyID;
            cboAssetType.Text = cboAssets.Text = "";
            txtAssetInfo.Text = lblEmployeeSelected.Text = txtRemarks.Text = txtSearch.Text = "";
            dtpWithEffectFrom.Value = dtpExpectedReturnDate.Value = dtpReturnDate.Value = ClsCommonSettings.GetServerDate();
            dgvOtherInfoDetails.Rows.Clear();
            dgvEmployeeAsset.Rows.Clear();
                        
            intAssetIssueID = 0;
            cboAssetType.Tag = 0;
            lblEmployeeSelected.Tag = 0;
            txtAssetInfo.Tag = cboAssets.Tag = dtpWithEffectFrom.Value;
            
            lblStatus.Text = "";
            tmrClear.Enabled = false;
            errValidate.Clear();

            SetAutoCompleteList();            
            ControlEnableDisable();
            LoadEmployee();

            dgvEmployee.ClearSelection();
            dgvOtherInfoDetails.ClearSelection();
            dgvEmployeeAsset.ClearSelection();
            
            btnClear.Enabled = true;
            btnAddNew.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnDelete.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false;

            cboAssetType.Focus();            
        }

        private void LoadCombos(int intType)
        {
            DataTable datTemp = new DataTable();

            if (intType == 0 || intType == 1)
            {
                if (ClsCommonSettings.IsArabicView)
                    datTemp = MobjclsBLLCompanyAssetOperations.FillCombos(new string[] { "BenefitTypeID,BenefitTypeNameArb AS BenefitTypeName ", "BenefitTypeReference", "1 = 1 ORDER BY BenefitTypeName" });
                else
                    datTemp = MobjclsBLLCompanyAssetOperations.FillCombos(new string[] { "BenefitTypeID,BenefitTypeName ", "BenefitTypeReference", "1 = 1 ORDER BY BenefitTypeName" });
                cboAssetType.ValueMember = "BenefitTypeID";
                cboAssetType.DisplayMember = "BenefitTypeName";
                cboAssetType.DataSource = datTemp;
            }

            if (intType == 0 || intType == 2)
            {
                if (mblnAddStatus == true && blnIsReturn == false)
                {
                    try
                    {
                        if (dgvEmployee.CurrentRow != null)
                        {
                            datTemp = MobjclsBLLCompanyAssetOperations.FillCombos(new string[] { "CompanyAssetID,Description", "CompanyAssets", "" +
                                "Status = " + (int)AssetStausType.OnHand + " AND BenefitTypeID = " + cboAssetType.SelectedValue.ToInt32() + " AND " +
                                "CompanyID = " + dgvEmployee.Rows[dgvEmployee.CurrentRow.Index].Cells["CompanyID"].Value.ToInt32() + " ORDER BY Description" });
                        }
                        else
                        {
                            datTemp = MobjclsBLLCompanyAssetOperations.FillCombos(new string[] { "CompanyAssetID,Description", "CompanyAssets", "" +
                                "Status = " + (int)AssetStausType.OnHand + " AND BenefitTypeID = " + cboAssetType.SelectedValue.ToInt32() + " AND " +
                                "CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID +  ") ORDER BY Description" });
                        }
                    }
                    catch { }
                }
                else
                {
                    datTemp = MobjclsBLLCompanyAssetOperations.FillCombos(new string[] { "CompanyAssetID,Description", "CompanyAssets", "" +
                    "1 = 1 ORDER BY Description" });
                }
                cboAssets.ValueMember = "CompanyAssetID";
                cboAssets.DisplayMember = "Description";
                cboAssets.DataSource = datTemp;
            }

            if (intType == 0 || intType == 3)
            {
                datTemp = MobjclsBLLCompanyAssetOperations.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "CompanyID in(Select CompanyID from UserCompanyDetails where UserID=" + ClsCommonSettings.UserID + ")" });
                cboFilterCompany.ValueMember = "CompanyID";
                cboFilterCompany.DisplayMember = "CompanyName";
                cboFilterCompany.DataSource = datTemp;                
            }

            if (intType == 0 || intType == 4)
            {
                if (ClsCommonSettings.IsArabicView)
                    datTemp = MobjclsBLLCompanyAssetOperations.FillCombos(new string[] { "AssetUsageTypeID,DescriptionArb AS Description", "AssetUsageTypeReference", "" });
                else
                    datTemp = MobjclsBLLCompanyAssetOperations.FillCombos(new string[] { "AssetUsageTypeID,Description ", "AssetUsageTypeReference", "" });
                AssetUsageTypeID.ValueMember = "AssetUsageTypeID";
                AssetUsageTypeID.DisplayMember = "Description";
                AssetUsageTypeID.DataSource = datTemp;
            }
        }

        private void SetAutoCompleteList()
        {
            this.txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            DataTable dt = clsUtilities.GetAutoCompleteListWithCompany(ClsCommonSettings.CurrentCompanyID, txtSearch.Text.Trim(), 0, "", "EmployeeBenefitMaster");
            this.txtSearch.AutoCompleteCustomSource = clsUtilities.ConvertAutoCompleteCollection(dt);
        }

        private void ControlEnableDisable()
        {
            if (blnIsReturn)
            {
                cboAssetType.Enabled = cboAssets.Enabled = false;
                dgvOtherInfoDetails.Enabled = true;
                dtpWithEffectFrom.Enabled = dtpExpectedReturnDate.Enabled = false;
                dtpReturnDate.Enabled = true;
            }
            else
            {
                if (mblnAddStatus)
                {
                    cboAssetType.Enabled = cboAssets.Enabled = true;
                    dgvOtherInfoDetails.Enabled = false;
                }
                else
                {
                    cboAssetType.Enabled = cboAssets.Enabled = false;
                    dgvOtherInfoDetails.Enabled = true;
                }

                dtpWithEffectFrom.Enabled = dtpExpectedReturnDate.Enabled = true;
                dtpReturnDate.Enabled = false;
            }
        }

        private void LoadEmployee()
        {
            DataTable datTemp = MobjclsBLLCompanyAssetOperations.getEmployeeDetails(cboFilterCompany.SelectedValue.ToInt32(), txtSearch.Text.Trim());
            dgvEmployee.DataSource = datTemp;
        }

        private void cboAssetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAssetType.SelectedIndex >= 0)
            {
                LoadCombos(2);
                cboAssets.SelectedIndex = -1;
                cboAssets.Text = "";
            }
        }

        private void cboAssets_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtAssetInfo.Text = "";
            txtAssetInfo.Tag = cboAssets.Tag = dtpWithEffectFrom.Value;

            if (cboAssets.SelectedIndex >= 0)
                getAssetDetails();
        }

        private void getAssetDetails()
        {
            DataSet dsTemp = MobjclsBLLCompanyAssetOperations.getAssetDetails(cboAssetType.Tag.ToInt32(), cboAssets.SelectedValue.ToInt32());

            if (dsTemp != null)
            {
                if (dsTemp.Tables.Count > 0)
                {
                    if (dsTemp.Tables[0] != null)
                    {
                        if (dsTemp.Tables[0].Rows.Count > 0)
                        {
                            txtAssetInfo.Text = dsTemp.Tables[0].Rows[0]["AssetDetails"].ToString();

                            if (ClsCommonSettings.IsArabicView)
                            {
                                txtAssetInfo.Text = txtAssetInfo.Text.Replace("Purchase Date", "تاريخ الشراء");
                                txtAssetInfo.Text = txtAssetInfo.Text.Replace("Purchase Value", "شراء القيمة");
                                txtAssetInfo.Text = txtAssetInfo.Text.Replace("Expiry Date", "تاريخ انتهاء الصلاحية");
                                txtAssetInfo.Text = txtAssetInfo.Text.Replace("Depreciation", "خفض");
                                txtAssetInfo.Text = txtAssetInfo.Text.Replace("Status", "حالة");
                                txtAssetInfo.Text = txtAssetInfo.Text.Replace("Last Return Date", "مشاركة تاريخ العودة");
                                txtAssetInfo.Text = txtAssetInfo.Text.Replace("Not Issued", "لم تصدر");
                                txtAssetInfo.Text = txtAssetInfo.Text.Replace("Remarks", "تصريحات");
                            }

                            txtAssetInfo.Tag = dsTemp.Tables[0].Rows[0]["PurchaseDate"].ToDateTime(); // For With Effect Date Validation
                        }
                    }

                    if (dsTemp.Tables[1] != null)
                    {
                        if (dsTemp.Tables[1].Rows.Count > 0)
                            cboAssets.Tag = dsTemp.Tables[1].Rows[0]["ReturnDate"].ToDateTime(); // For With Effect Date Validation
                    }
                }
            }
        }

        private void cboFilterCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetAutoCompleteList();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadEmployee();
        }

        private void btnSearchClear_Click(object sender, EventArgs e)
        {
           // cboFilterCompany.SelectedValue = ClsCommonSettings.CurrentCompanyID;
            txtSearch.Text = "";
            LoadEmployee();
        }

        private void dgvEmployee_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ClearControls();

            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvEmployee.Columns["EmployeeName"].Index)
                {
                    long lngTempEmployeeID = dgvEmployee.Rows[e.RowIndex].Cells["EmployeeID"].Value.ToInt64();
                    lblEmployeeSelected.Tag = lngTempEmployeeID;
                    lblEmployeeSelected.Text = dgvEmployee.Rows[e.RowIndex].Cells["EmployeeName"].Value.ToString();
                    lblJoiningDate.Text = dgvEmployee.Rows[e.RowIndex].Cells["JoiningDate"].Value.ToString();
                    LoadCombos(2);
                    getEmployeeAssets(lngTempEmployeeID);
                }
            }
        }

        private void ClearControls()
        {
            mblnAddStatus = true;
            blnIsReturn = false;

            cboAssetType.SelectedIndex = cboAssets.SelectedIndex = -1;
           // cboFilterCompany.SelectedValue = ClsCommonSettings.CurrentCompanyID;
            cboAssetType.Text = cboAssets.Text = "";
            txtAssetInfo.Text = lblEmployeeSelected.Text = txtRemarks.Text = txtSearch.Text = "";
            dtpWithEffectFrom.Value = dtpExpectedReturnDate.Value = dtpReturnDate.Value = ClsCommonSettings.GetServerDate();
            dgvOtherInfoDetails.Rows.Clear();
            dgvEmployeeAsset.Rows.Clear();

            intAssetIssueID = 0;
            cboAssetType.Tag = 0;
            txtAssetInfo.Tag = cboAssets.Tag = dtpWithEffectFrom.Value;

            lblStatus.Text = "";
            tmrClear.Enabled = false;
            errValidate.Clear();

            ControlEnableDisable();

            dgvOtherInfoDetails.ClearSelection();
            dgvEmployeeAsset.ClearSelection();

            btnClear.Enabled = true;
            btnAddNew.Enabled = btnSaveItem.Enabled = btnSave.Enabled = btnDelete.Enabled = false;
            btnPrint.Enabled = btnEmail.Enabled = false;
        }

        private void getEmployeeAssets(long lngTempEmployeeID)
        {
            dgvEmployeeAsset.Rows.Clear();
            DataTable datTemp = MobjclsBLLCompanyAssetOperations.getEmployeeAssetDetails(lngTempEmployeeID);

            for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
            {
                dgvEmployeeAsset.Rows.Add();
                dgvEmployeeAsset.Rows[iCounter].Cells["EmpBenefitID"].Value = datTemp.Rows[iCounter]["EmpBenefitID"].ToInt32();
                dgvEmployeeAsset.Rows[iCounter].Cells["AssetEmployeeID"].Value = datTemp.Rows[iCounter]["AssetEmployeeID"].ToInt64();
                dgvEmployeeAsset.Rows[iCounter].Cells["BenefitTypeID"].Value = datTemp.Rows[iCounter]["BenefitTypeID"].ToInt32();
                dgvEmployeeAsset.Rows[iCounter].Cells["CompanyAssetID"].Value = datTemp.Rows[iCounter]["CompanyAssetID"].ToInt32();
                dgvEmployeeAsset.Rows[iCounter].Cells["AssetType"].Value = datTemp.Rows[iCounter]["AssetType"].ToString();
                dgvEmployeeAsset.Rows[iCounter].Cells["Asset"].Value = datTemp.Rows[iCounter]["Asset"].ToString();
                dgvEmployeeAsset.Rows[iCounter].Cells["WithEffectDate"].Value = datTemp.Rows[iCounter]["WithEffectDate"].ToString();
                dgvEmployeeAsset.Rows[iCounter].Cells["ExpectedReturnDate"].Value = datTemp.Rows[iCounter]["ExpectedReturnDate"].ToString();
                dgvEmployeeAsset.Rows[iCounter].Cells["ReturnDate"].Value = datTemp.Rows[iCounter]["ReturnDate"].ToString();
                int intTempAssetStatusID = datTemp.Rows[iCounter]["AssetStatusID"].ToInt32();
                dgvEmployeeAsset.Rows[iCounter].Cells["AssetStatusID"].Value = intTempAssetStatusID;
                dgvEmployeeAsset.Rows[iCounter].Cells["AssetRemarks"].Value = datTemp.Rows[iCounter]["AssetRemarks"].ToString();

                if (intTempAssetStatusID == (int)AssetStausType.OnHand)
                {
                    DataGridViewTextBoxCell txtCell = new DataGridViewTextBoxCell();
                    txtCell.Value = "";
                    dgvEmployeeAsset["Return", iCounter] = txtCell;
                    dgvEmployeeAsset.Rows[iCounter].Cells["Return"].ReadOnly = true;
                    dgvEmployeeAsset.Rows[iCounter].DefaultCellStyle.ForeColor = Color.Firebrick;
                }
            }

            dgvEmployeeAsset.ClearSelection();
        }

        private void dgvEmployee_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox obj = (ComboBox)sender;
            obj.DroppedDown = false;
        }

        private void ChangeControlStatus(object sender, EventArgs e)
        {
            ChangeStatus();
        }

        private void ChangeStatus()
        {
            if (mblnAddStatus)
            {
                btnAddNew.Enabled = false;
                btnClear.Enabled = true;
                if (MobjclsBLLCompanyAssetOperations.CheckEmployeeExistance(lblEmployeeSelected.Tag.ToInt64()) == 1)
                    btnSaveItem.Enabled = btnSave.Enabled = false;
                else
                    btnSaveItem.Enabled = btnSave.Enabled = MblnAddPermission;
                btnDelete.Enabled = btnPrint.Enabled = btnEmail.Enabled = false;
            }
            else
            {
                btnAddNew.Enabled = MblnAddPermission;
                btnClear.Enabled = false;

                if (blnIsReturn) // Asset Return
                {
                    // Check the asset is issued to other employee
                    if (MobjclsBLLCompanyAssetOperations.CheckAssetIsIssued(cboAssets.SelectedValue.ToInt32(), (int)AssetStausType.Issued) == 1)
                        btnSaveItem.Enabled = btnSave.Enabled = btnDelete.Enabled = false;
                    else
                    {
                        btnSaveItem.Enabled = btnSave.Enabled = MblnUpdatePermission;
                        btnDelete.Enabled = MblnDeletePermission;
                    }
                }
                else // Asset Issue
                {
                    // if employee is settled don't want to update / delete the deposit details
                    if (MobjclsBLLCompanyAssetOperations.CheckEmployeeExistance(lblEmployeeSelected.Tag.ToInt64()) == 1)
                        btnSaveItem.Enabled = btnSave.Enabled = btnDelete.Enabled = false;
                    else
                    {
                        btnSaveItem.Enabled = btnSave.Enabled = MblnUpdatePermission;
                        btnDelete.Enabled = MblnDeletePermission;
                    }
                    // END
                }

                btnPrint.Enabled = btnEmail.Enabled = MblnPrintEmailPermission;                
            }

            btnAddAssets.Enabled = btnOtherInfoType.Enabled = (MblnAddPermission || MblnUpdatePermission) ? true : false;
            errValidate.Clear();
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            AddNew();
        }

        private void btnSaveItem_Click(object sender, EventArgs e)
        {
            SaveAsset();
        }

        private bool SaveAsset()
        {
            if (FormValidation())
            {
                bool blnSave = false;

                if (mblnAddStatus)
                {
                    if (this.UserMessage.ShowMessage(1) == true) // Save
                        blnSave = true;
                }
                else
                {
                    if (this.UserMessage.ShowMessage(3) == true) // Update
                        blnSave = true;
                }

                if (blnSave)
                {
                    FillParameters(); // fill parameters for saving the content
                    FillDetailParameters(); // fill usage/damage parameters

                    int intRetValue = MobjclsBLLCompanyAssetOperations.SaveAssetIssueReturn();

                    if (intRetValue > 0)
                    {
                        if (mblnAddStatus)
                        {
                            lblStatus.Text = this.UserMessage.GetMessageByCode(2);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(2); // Saved Successfully
                        }
                        else
                        {
                            lblStatus.Text = this.UserMessage.GetMessageByCode(21);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(21); // Updated Successfully
                        }

                        SetAutoCompleteList();
                        getEmployeeAssets(lblEmployeeSelected.Tag.ToInt64());

                        // for load last add/ update asset details
                        int intIndex = getEmployeeAssetgridIndex(intRetValue);

                        if (intIndex >= 0)
                            DisplayAssetDetails(intIndex, dgvEmployeeAsset.Columns["AssetType"].Index);
                        // End

                        return true;
                    }
                }
            }

            return false;
        }

        private bool FormValidation()
        {
            dgvEmployee.Focus();
            errValidate.Clear();

            if (lblEmployeeSelected.Tag.ToInt64() == 0) // if employee is not selected properly
            {
                lblStatus.Text = this.UserMessage.GetMessageByCode(9124);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(9124);
                return false;
            }

            if (cboAssetType.SelectedIndex == -1) // if Asset type is not selected properly
            {
                lblStatus.Text = this.UserMessage.GetMessageByCode(1861);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1861, cboAssetType, errValidate);
                return false;
            }

            if (cboAssets.SelectedIndex == -1) // if Asset is not selected properly
            {
                lblStatus.Text = this.UserMessage.GetMessageByCode(1862);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1862, cboAssets, errValidate);
                return false;
            }

            if (dtpWithEffectFrom.Value.Date > ClsCommonSettings.GetServerDate()) // if With Effect date is greater than current date
            {
                lblStatus.Text = this.UserMessage.GetMessageByCode(1863);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1863, dtpWithEffectFrom, errValidate);
                return false;
            }

            if (dtpExpectedReturnDate.Checked)
            {
                if (dtpWithEffectFrom.Value.Date > dtpExpectedReturnDate.Value.Date) // if Expected Return Date date is less than With Effect date
                {
                    lblStatus.Text = this.UserMessage.GetMessageByCode(1864);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(1864, dtpExpectedReturnDate, errValidate);
                    return false;
                }
            }

            if (dtpWithEffectFrom.Value.Date < lblJoiningDate.Text.ToDateTime().Date) // if With Effect date is less than Employee Joining Date
            {
                lblStatus.Text = this.UserMessage.GetMessageByCode(1865);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1865, dtpWithEffectFrom, errValidate);
                return false;
            }

            if (dtpWithEffectFrom.Value.Date < txtAssetInfo.Tag.ToDateTime().Date) // if With Effect date is less than Asset Purchase Date
            {
                lblStatus.Text = this.UserMessage.GetMessageByCode(1866);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1866, dtpWithEffectFrom, errValidate);
                return false;
            }

            if (dtpWithEffectFrom.Value.Date < cboAssets.Tag.ToDateTime().Date) // if With Effect date is less than Asset Last Return Date
            {
                lblStatus.Text = this.UserMessage.GetMessageByCode(1867);
                tmrClear.Enabled = true;
                this.UserMessage.ShowMessage(1867, dtpWithEffectFrom, errValidate);
                return false;
            }

            if (blnIsReturn)
            {
                if (dtpWithEffectFrom.Value.Date > dtpReturnDate.Value.Date) // if ReturnDate date is less than With Effect date
                {
                    lblStatus.Text = this.UserMessage.GetMessageByCode(1868);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(1868, dtpReturnDate, errValidate);
                    return false;
                }

                if (dtpReturnDate.Value.Date > ClsCommonSettings.GetServerDate()) // if return date is greater than current date
                {
                    lblStatus.Text = this.UserMessage.GetMessageByCode(1869);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(1869, dtpWithEffectFrom, errValidate);
                    return false;
                }
            }

            for (int iCounter = 0; iCounter <= dgvOtherInfoDetails.Rows.Count - 2; iCounter++)
            {
                if (dgvOtherInfoDetails.Rows[iCounter].Cells["AssetUsageTypeID"].Value.ToInt32() == 0)
                {
                    lblStatus.Text = this.UserMessage.GetMessageByCode(18612);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(18612);
                    return false;
                }

                if (dgvOtherInfoDetails.Rows[iCounter].Cells["Date"].Value.ToStringCustom() != "")
                {
                    if ((dgvOtherInfoDetails.Rows[iCounter].Cells["Date"].Value.ToDateTime().ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime() >
                            ClsCommonSettings.GetServerDate()) // if Usage date is greater than current date
                    {
                        lblStatus.Text = this.UserMessage.GetMessageByCode(18614);
                        tmrClear.Enabled = true;
                        this.UserMessage.ShowMessage(18614);
                        return false;
                    }

                    if (blnIsReturn)
                    {
                        if (dtpReturnDate.Value.Date < // if Usage date is greater than return date
                            ((dgvOtherInfoDetails.Rows[iCounter].Cells["Date"].Value.ToDateTime().ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime()))
                        {
                            lblStatus.Text = this.UserMessage.GetMessageByCode(18616);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(18616);
                            return false;
                        }
                    }
                    else
                    {
                        if (dtpWithEffectFrom.Value.Date > // if Usage date is less than With Effect date
                            ((dgvOtherInfoDetails.Rows[iCounter].Cells["Date"].Value.ToDateTime().ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime()))
                        {
                            lblStatus.Text = this.UserMessage.GetMessageByCode(18613);
                            tmrClear.Enabled = true;
                            this.UserMessage.ShowMessage(18613);
                            return false;
                        }
                    }
                }
                else if (dgvOtherInfoDetails.Rows[iCounter].Cells["Date"].Value.ToStringCustom() == "")
                {
                    lblStatus.Text = this.UserMessage.GetMessageByCode(18615);
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(18615);
                    return false;
                }
            }

            return true;
        }

        private void FillParameters()
        {
            if (mblnAddStatus)
                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intEmpBenefitID = 0;
            else
                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intEmpBenefitID = cboAssetType.Tag.ToInt32();

            MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.lngEmployeeID = lblEmployeeSelected.Tag.ToInt64();
            MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intBenefitTypeID = cboAssetType.SelectedValue.ToInt32();
            MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intCompanyAssetID = cboAssets.SelectedValue.ToInt32();
            MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.dtWithEffectDate = (dtpWithEffectFrom.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();

            if (dtpExpectedReturnDate.Checked)
            {
                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.blnIsExpectedReturnDate = true;
                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.dtExpectedReturnDate = (dtpExpectedReturnDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
            }
            else
                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.blnIsExpectedReturnDate = false;

            MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.strRemarks = txtRemarks.Text;

            if (blnIsReturn) // Asset Return 
            {
                if (mblnAddStatus) // When Insert Asset Return
                    MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intParentID = cboAssetType.Tag.ToInt32();
                else // When Update Asset Return
                    MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intParentID = intAssetIssueID;

                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.dtReturnDate = (dtpReturnDate.Value.ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();
                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intAssetStatusID = (int)AssetStausType.OnHand;
            }
            else // Asset Issue 
                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intAssetStatusID = (int)AssetStausType.Issued;
        }

        private void FillDetailParameters()
        {
            MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.IlstclsDTOEmployeeBenefitDetail = new System.Collections.Generic.List<clsDTOEmployeeBenefitDetail>();

            for (int iCounter = 0; iCounter <= dgvOtherInfoDetails.Rows.Count - 2; iCounter++)
            {
                clsDTOEmployeeBenefitDetail objclsDTOEmployeeBenefitDetail = new clsDTOEmployeeBenefitDetail();
                objclsDTOEmployeeBenefitDetail.intSerialNo = iCounter + 1;
                objclsDTOEmployeeBenefitDetail.intAssetUsageTypeID = dgvOtherInfoDetails.Rows[iCounter].Cells["AssetUsageTypeID"].Value.ToInt32();
                
                if (dgvOtherInfoDetails.Rows[iCounter].Cells["Date"].Value.ToStringCustom() != "")
                    objclsDTOEmployeeBenefitDetail.dtDate = (dgvOtherInfoDetails.Rows[iCounter].Cells["Date"].Value.ToDateTime().ToString("dd MMM yyyy", CultureInfo.InvariantCulture)).ToDateTime();

                if (dgvOtherInfoDetails.Rows[iCounter].Cells["Remarks"].Value.ToStringCustom() != "")
                    objclsDTOEmployeeBenefitDetail.strRemarks = dgvOtherInfoDetails.Rows[iCounter].Cells["Remarks"].Value.ToString();

                if (dgvOtherInfoDetails.Rows[iCounter].Cells["Amount"].Value.ToStringCustom() != "")
                    objclsDTOEmployeeBenefitDetail.decAmount = dgvOtherInfoDetails.Rows[iCounter].Cells["Amount"].Value.ToDecimal(); 

                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.IlstclsDTOEmployeeBenefitDetail.Add(objclsDTOEmployeeBenefitDetail);
            }
        }

        private int getEmployeeAssetgridIndex(int intEmpBenefitID) // After save get Employee Asset details from the grid for that getting that index
        {
            for (int iCounter = 0; iCounter <= dgvEmployeeAsset.Rows.Count - 1; iCounter++)
            {
                if (dgvEmployeeAsset.Rows[iCounter].Cells["EmpBenefitID"].Value.ToInt32() == intEmpBenefitID)
                    return iCounter;
            }

            return -1;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.UserMessage.ShowMessage(13) == true)
            {
                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intEmpBenefitID = cboAssetType.Tag.ToInt32();
                MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intCompanyAssetID = cboAssets.SelectedValue.ToInt32();

                if (blnIsReturn) // When Asset Return is going to delete then the Asset status should be 'Issued'.
                    MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intAssetStatusID = (int)AssetStausType.Issued;
                else  // When Asset Issue is going to Delete then Asset Status should be 'OnHand'
                    MobjclsBLLCompanyAssetOperations.PobjclsDTOCompanyAssetOperations.intAssetStatusID = (int)AssetStausType.OnHand;

                if (MobjclsBLLCompanyAssetOperations.DeleteAssetIssueReturn() == true)
                {
                    lblStatus.Text = this.UserMessage.GetMessageByCode(4); // Deleted Successfully
                    tmrClear.Enabled = true;
                    this.UserMessage.ShowMessage(4);

                    AddNew();
                }
            }
        }

        private void btnAddAssets_Click(object sender, EventArgs e)
        {
            // For new asset type entry
            int intTempID = cboAssets.SelectedValue.ToInt32(); // For setting selected asset after reference form closing
            FrmCompanyAssets objCompanyAssets = new FrmCompanyAssets();
            objCompanyAssets.ShowDialog();
            LoadCombos(1);
            LoadCombos(2);

            if (intTempID > 0)
                cboAssets.SelectedValue = intTempID;
            else
                cboAssets.SelectedIndex = -1;

            objCompanyAssets.Dispose();
            objCompanyAssets = null;
        }

        private void btnOtherInfoType_Click(object sender, EventArgs e)
        {
            FrmCommonRef objFrmCommonRef = new FrmCommonRef("Other Info Type", new int[] { 1, 0 }, "AssetUsageTypeID,Description,DescriptionArb", "AssetUsageTypeReference", "");
            objFrmCommonRef.ShowDialog();
            LoadCombos(4);
            objFrmCommonRef.Dispose();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboAssetType.Tag.ToInt32() > 0)
            {
                FrmReportviewer ObjViewer = new FrmReportviewer();
                ObjViewer.PsFormName = this.Text;
                ObjViewer.PiRecId = cboAssetType.Tag.ToInt64();

                DataTable datTemp = MobjclsBLLCompanyAssetOperations.FillCombos(new string[] { "TOP 1 CompanyID AS CompanyID", "EmployeeMaster", "" +
                    "EmployeeID = " + lblEmployeeSelected.Tag.ToInt64() + "" });

                if (datTemp != null)
                {
                    if (datTemp.Rows.Count > 0)
                        ObjViewer.PintCompany = datTemp.Rows[0]["CompanyID"].ToInt32();
                }

                ObjViewer.PiFormID = (int)FormID.AssetHandover;
                ObjViewer.ShowDialog();
            }
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboAssetType.Tag.ToInt32() > 0)
                {
                    using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                    {
                        ObjEmailPopUp.MsSubject = "Asset Handover";
                        ObjEmailPopUp.EmailFormType = EmailFormID.AssetHandover;
                        ObjEmailPopUp.EmailSource = MobjclsBLLCompanyAssetOperations.GetPrintAssetIssueReturn(cboAssetType.Tag.ToInt32());
                        ObjEmailPopUp.ShowDialog();
                    }
                }
            }
            catch { }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "AssetHandover";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void dgvEmployeeAsset_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
                DisplayAssetDetails(e.RowIndex, e.ColumnIndex);
        }

        private void DisplayAssetDetails(int intRowIndex, int intColumnIndex)
        {
            if (intColumnIndex == dgvEmployeeAsset.Columns["Return"].Index)
            {
                if (dgvEmployeeAsset.Rows[intRowIndex].Cells["AssetStatusID"].Value.ToInt32() == (int)AssetStausType.Issued) // Going to Asset Return
                    mblnAddStatus = blnIsReturn = true;
                else
                {
                    mblnAddStatus = false;
                    blnIsReturn = true;
                }
            }
            else
            {
                mblnAddStatus = false;

                if (dgvEmployeeAsset.Rows[intRowIndex].Cells["AssetStatusID"].Value.ToInt32() == (int)AssetStausType.Issued) // Asset Issue Update
                    blnIsReturn = false;
                else if (dgvEmployeeAsset.Rows[intRowIndex].Cells["AssetStatusID"].Value.ToInt32() == (int)AssetStausType.OnHand) // Asset Return Update
                    blnIsReturn = true;
            }

            int intEmpBenefitID = dgvEmployeeAsset.Rows[intRowIndex].Cells["EmpBenefitID"].Value.ToInt32();
            cboAssetType.Tag = intEmpBenefitID;
            intAssetIssueID = dgvEmployeeAsset.Rows[intRowIndex].Cells["ParentID"].Value.ToInt32();
            cboAssetType.SelectedValue = dgvEmployeeAsset.Rows[intRowIndex].Cells["BenefitTypeID"].Value.ToInt32();
            LoadCombos(2);
            cboAssets.SelectedValue = dgvEmployeeAsset.Rows[intRowIndex].Cells["CompanyAssetID"].Value.ToInt32();
            dtpWithEffectFrom.Value = dgvEmployeeAsset.Rows[intRowIndex].Cells["WithEffectDate"].Value.ToDateTime();

            if (dgvEmployeeAsset.Rows[intRowIndex].Cells["ExpectedReturnDate"].Value.ToStringCustom() != "")
            {
                dtpExpectedReturnDate.Checked = true;
                dtpExpectedReturnDate.Value = dgvEmployeeAsset.Rows[intRowIndex].Cells["ExpectedReturnDate"].Value.ToDateTime();
            }
            else
                dtpExpectedReturnDate.Checked = false;

            txtRemarks.Text = dgvEmployeeAsset.Rows[intRowIndex].Cells["AssetRemarks"].Value.ToString();

            if (dgvEmployeeAsset.Rows[intRowIndex].Cells["ReturnDate"].Value.ToStringCustom() != "")
                dtpReturnDate.Value = dgvEmployeeAsset.Rows[intRowIndex].Cells["ReturnDate"].Value.ToDateTime();
            else
                dtpReturnDate.Value = ClsCommonSettings.GetServerDate();

            // To fill Asset Usage Details
            DataTable datTemp = MobjclsBLLCompanyAssetOperations.getAssetUsageDetails(intEmpBenefitID);
            dgvOtherInfoDetails.Rows.Clear();

            for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
            {
                dgvOtherInfoDetails.Rows.Add();
                dgvOtherInfoDetails.Rows[iCounter].Cells["AssetUsageTypeID"].Value = datTemp.Rows[iCounter]["AssetUsageTypeID"].ToInt32();
                dgvOtherInfoDetails.Rows[iCounter].Cells["Date"].Value = datTemp.Rows[iCounter]["Date"].ToString();
                dgvOtherInfoDetails.Rows[iCounter].Cells["Remarks"].Value = datTemp.Rows[iCounter]["Remarks"].ToString();
                dgvOtherInfoDetails.Rows[iCounter].Cells["Amount"].Value = datTemp.Rows[iCounter]["Amount"].ToString();
            }
            // End

            ChangeStatus();
            ControlEnableDisable();

            // if we are going to Asset Return then save button want to enable but all other cases save button don't want to enable defualtly
            if (!(mblnAddStatus == true && blnIsReturn == true))
                btnSaveItem.Enabled = btnSave.Enabled = false;
        }

        private void dgvOtherInfoDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            ChangeStatus();
        }

        private void FrmCompanyAssetOperations_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "AssetHandover";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Control | Keys.Enter:
                        if (MblnAddPermission)
                            btnAddNew_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (MblnDeletePermission)
                            btnDelete_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.P:
                        if (MblnPrintEmailPermission)
                            btnPrint_Click(sender, new EventArgs());//Print
                        break;
                    case Keys.Control | Keys.M:
                        if (MblnPrintEmailPermission)
                            btnEmail_Click(sender, new EventArgs());//Email
                        break;
                }
            }
            catch { }
        }

        private void FrmCompanyAssetOperations_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (btnSave.Enabled)
            {
                if (this.UserMessage.ShowMessage(8) == true) // Your changes will be lost. Please confirm if you wish to cancel?
                {
                    MobjclsBLLCompanyAssetOperations = null;
                    objUserMessage = null;
                    e.Cancel = false;
                }
                else
                    e.Cancel = true;
            }
        }

        private void btnMaintenance_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("Maintenance Info", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "AssetUsageTypeID,Description As MaintenanceType,DescriptionArb As MaintenanceTypeArb ", "AssetUsageTypeReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                LoadCombos(4);
               
            }
            catch (Exception ex)
            {

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnMaintenance_Click" + ex.Message.ToString());


            }
        }

        private void expnlTop_Click(object sender, EventArgs e)
        {

        }        
    }
}