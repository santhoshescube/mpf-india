﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;

/* 
=================================================
 * Author:		<Author,,Thasni>
Create date: <Create Date,,20 Aug 2013>
Description:	<Description,, BankDetails Form>
================================================
*/
namespace MyPayfriend
{
    public partial class FrmBankDetails : Form
    {
        #region Variables Declaration

        private bool MblnAddStatus; //To Specify Add/Update mode 
        private bool MblnChangeStatus = false; //To Specify Change Status
        private bool MblnAddPermission = false;//To Set Add Permission
        private bool MblnUpdatePermission = false;//To Set Update Permission
        private bool MblnDeletePermission = false;//To Set Delete Permission
        //private bool MblnAddUpdatePermission = false;//To Set Add/Update Permission
        private bool MblnPrintEmailPermission = false;
        private string MsMessageCaption;
        private string MsMessageCommon;
        private MessageBoxIcon MmessageIcon;

        private int MintCurrentRecCnt;
        private int MintRecordCnt;
        private int MintTimerInterval;
        private int MintBankNameID;
        private int MintBankID;
       
        private ArrayList MaMessageArr;                  // Error Message display
        private ArrayList MaStatusMessage;

        bool blnIsEditMode = false;

        // ArrayList sarrFormnames; ////showing webbrowser in toolstripsplitbtn
 
        clsBLLBankDetails MobjclsBLLBankDetails;
        ClsLogWriter MObjClsLogWriter;
        ClsNotification MObjClsNotification;
        string strBindingOf = "Of ";
        #endregion //Variables Declaration

        #region Constructor
        public FrmBankDetails(int intBankID)
        {

            InitializeComponent();

            MintTimerInterval = ClsCommonSettings.TimerInterval;
            MmessageIcon = MessageBoxIcon.Information;
            MintBankID = intBankID;

            MsMessageCaption = ClsCommonSettings.MessageCaption;    //Message caption
            TmBank.Interval = MintTimerInterval;   // Setting timer interval

            MobjclsBLLBankDetails = new clsBLLBankDetails();
            MObjClsLogWriter = new ClsLogWriter(Application.StartupPath);
            MObjClsNotification = new ClsNotification();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.BankInformation, this);

            strBindingOf = "من ";
            lblIBANCode.Text = "قانون رقم الحساب المصرفي الدولي";
        }
        #endregion //Constructor

        

        #region Events
        private void PrintStripButton_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        private void BtnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (FrmEmailPopup ObjEmailPopUp = new FrmEmailPopup())
                {
                    ObjEmailPopUp.MsSubject = "Bank Information";
                    ObjEmailPopUp.EmailFormType = EmailFormID.Bank;
                    ObjEmailPopUp.EmailSource = MobjclsBLLBankDetails.GetBankReport();
                    ObjEmailPopUp.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Email:BtnEmail_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on BtnEmail_Click " + Ex.Message.ToString());
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            AddNewItem();
        }

        private void FrmBankDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyData)
                {
                    case Keys.F1:
                        FrmHelp objHelp = new FrmHelp();
                        objHelp.strFormName = "Bank";
                        objHelp.ShowDialog();
                        objHelp = null;
                        break;

                    case Keys.Escape:
                        this.Close();
                        break;

                    case Keys.Control | Keys.Enter:
                        if (BindingNavigatorAddNewItem.Enabled)
                            bindingNavigatorAddNewItem_Click(sender, new EventArgs());//addnew item
                        break;
                    case Keys.Alt | Keys.R:
                        if (BindingNavigatorDeleteItem.Enabled)
                            bindingNavigatorDeleteItem_Click(sender, new EventArgs());//delete
                        break;
                    case Keys.Control | Keys.E:
                        if (btnClear.Enabled)
                            btnClear_Click(sender, new EventArgs());//Clear
                        break;
                    case Keys.Control | Keys.Left:
                        if (BindingNavigatorMovePreviousItem.Enabled)
                            bindingNavigatorMovePreviousItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Right:
                        if (BindingNavigatorMoveNextItem.Enabled)
                            bindingNavigatorMoveNextItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Up:
                        if (BindingNavigatorMoveFirstItem.Enabled)
                            bindingNavigatorMoveFirstItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.Down:
                        if (BindingNavigatorMoveLastItem.Enabled)
                            bindingNavigatorMoveLastItem_Click(sender, new EventArgs());
                        break;
                    case Keys.Control | Keys.P:
                        if (btnPrint.Enabled)
                            PrintStripButton_Click(sender, new EventArgs());//Cancel
                        break;
                    case Keys.Control | Keys.M:
                        if (btnEmail.Enabled)
                            BtnEmail_Click(sender, new EventArgs());//Cancel
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp objHelp = new FrmHelp();
            objHelp.strFormName = "Bank";
            objHelp.ShowDialog();
            objHelp = null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void FrmBankDetails_Load(object sender, EventArgs e)
        {
            try
            {
                MblnChangeStatus = false;
                SetPermissions();
                LoadCombos(0);
                LoadMessage();
                ChangeStatus();
                AddNewItem();
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form Load:FrmBankDetails_Load " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on FrmBankDetails_Load() " + Ex.Message.ToString());
            }
        }
        private void Cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox Cbo = (ComboBox)sender;
            Cbo.DroppedDown = false;
        }
        private void btnBankBranch_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("BankName", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "BankID,BankName,BankNameArb", "BankReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(CboBankBranchID.SelectedValue);
                LoadCombos(1);
                if (objCommon.NewID != 0)
                    CboBankBranchID.SelectedValue = objCommon.NewID;
                else
                    CboBankBranchID.SelectedValue = intWorkingAt;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnBankBranch " + Ex.Message.ToString());
            }

        }

        private void btnCountry_Click(object sender, EventArgs e)
        {
            try
            {
                FrmCommonRef objCommon = new FrmCommonRef("CountryName", new int[] { 1, 0, ClsCommonSettings.TimerInterval }, "CountryID,CountryName,CountryNameArb", "CountryReference", "");
                objCommon.ShowDialog();
                objCommon.Dispose();
                int intWorkingAt = Convert.ToInt32(CboCountry.SelectedValue);
                LoadCombos(2);
                if (objCommon.NewID != 0)
                    CboCountry.SelectedValue = objCommon.NewID;
                else
                    CboCountry.SelectedValue = intWorkingAt;

            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmCommonRef:FrmCommonRef " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on btnCountry " + Ex.Message.ToString());
            }

        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (BankDetailsFormValidation())
            {
                if (SaveBank())
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    string Msg = MObjClsNotification.GetErrorMessage(MaMessageArr, (MblnAddStatus) ? 2 : 666, out MmessageIcon);
                    bindingNavigatorMovePreviousItem_Click(null, null);
                    lblBankStatus.Text = Msg.Remove(0, Msg.IndexOf("#") + 1);
                    TmBank.Enabled = true;
                    MessageBox.Show(Msg.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);


                }
            }
        }

        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {

                MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();
                if (MintRecordCnt > 0)
                {
                    MintCurrentRecCnt = MintCurrentRecCnt + 1;
                    MblnAddStatus = false;
                    if (MintCurrentRecCnt >= MintRecordCnt)
                    {
                        MintCurrentRecCnt = MintRecordCnt;
                    }
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }


                DisplayBank();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 11, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();
                TmBank.Enabled = blnIsEditMode = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveNextItem:MoveNextItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveNextItem_Click " + Ex.Message.ToString());
            }


        }


        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            try
            {
                MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MblnAddStatus = false;
                    MintCurrentRecCnt = MintCurrentRecCnt - 1;

                    if (MintCurrentRecCnt <= 0)
                    {
                        MintCurrentRecCnt = 1;

                    }
                }
                else
                {
                    BindingNavigatorPositionItem.Text = strBindingOf + "0";
                    MintRecordCnt = 0;
                }

                DisplayBank();
                SetBindingNavigatorButtons();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 10, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();
                TmBank.Enabled = blnIsEditMode = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MovePreviousItem:MovePreviousItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MovePreviousItem_Click " + Ex.Message.ToString());
            }

        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            try
            {
                ErrProBank.Clear();
                MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MintCurrentRecCnt = MintRecordCnt;
                    MblnAddStatus = false;
                }
                else
                {
                    MintCurrentRecCnt = 0;
                }

                //if (MintBankID > 0)
                //    DisplayBankID();
                //else
                DisplayBank();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 12, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                SetBindingNavigatorButtons();
                TmBank.Enabled = blnIsEditMode = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveLastItem:MoveLastItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveLastItem_Click() " + Ex.Message.ToString());
            }


        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            try
            {
                ErrProBank.Clear();
                //if (MintBankID > 0)
                //    MintRecordCnt = MobjclsBLLBankDetails.RecCountBankNavigate(MintBankID);
                //else
                MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();

                if (MintRecordCnt > 0)
                {
                    MblnAddStatus = false;
                    MintCurrentRecCnt = 1;
                }
                else
                    MintCurrentRecCnt = 0;

                DisplayBank();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 9, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);

                SetBindingNavigatorButtons();
                TmBank.Enabled = blnIsEditMode = true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on MoveFirstItem:MoveFirstItem_Click() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on MoveFirstItem_Click() " + Ex.Message.ToString());
            }



        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            MintBankID = 0;
            AddNewItem();

        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                int intBankNameID = MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID;
                if (DeleteValidation(intBankNameID))
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 13, out MmessageIcon);
                    if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                        return;
                    if (MobjclsBLLBankDetails.DeleteBankDetails())
                    {
                        MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 4, out MmessageIcon);
                        lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                        MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                        TmBank.Enabled = true;
                        AddNewItem();
                    }

                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on DeleteItem:DeleteItem_Click " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on DeleteItem_Click() " + Ex.Message.ToString());
            }

        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (BankDetailsFormValidation())
            {
                if (SaveBank())
                {

                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, (MblnAddStatus) ? 2 : 666, out MmessageIcon);
                    lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                    TmBank.Enabled = true;

                    MblnChangeStatus = false;
                    this.Close();
                }
            }
        }





        private void FrmBankDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MblnChangeStatus)
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 8, out MmessageIcon);
                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void TmBank_Tick(object sender, EventArgs e)
        {
            TmBank.Enabled = false;
            lblBankStatus.Text = "";
        }
        #endregion

        #region Function
        private void LoadReport()
        {
            try
            {
                if (MintBankNameID > 0)
                {
                    FrmReportviewer ObjViewer = new FrmReportviewer();
                    ObjViewer.PsFormName = this.Text;
                    ObjViewer.PiRecId = MintBankNameID;
                    ObjViewer.PiFormID = (int)FormID.BankInformation;
                    ObjViewer.ShowDialog();
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form FrmBankDetails:LoadReport " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadReport() " + Ex.Message.ToString());
            }
        }
        private void ChangeStatus()
        {
            ErrProBank.Clear();
            MblnChangeStatus = true;
            if (!blnIsEditMode)
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnAddPermission;
            }
            else
            {
                BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = MblnUpdatePermission;

            }
        }

        public void ChangeStatus(object sender, EventArgs e) // to set the event
        {
            ChangeStatus();
        }



        public bool AddNewItem()
        {
            try
            {
                btnClear.Enabled = MblnAddStatus = true;
                SetEnable();
                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 655, out MmessageIcon);
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmBank.Enabled = true;
                if (MintBankID > 0)
                {
                    MobjclsBLLBankDetails.clsDTOBankDetails.intBankBranchID = MintBankID;
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();
                    MintCurrentRecCnt = MobjclsBLLBankDetails.GetRowNumber();
                    DisplayBankID();
                    BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";
                    BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
                    SetBindingNavigatorButtons();
                    blnIsEditMode = true;
                    MblnAddStatus = false;
                }
                else
                {
                    MintRecordCnt = MobjclsBLLBankDetails.RecCountNavigate();
                    BindingAddNew();
                    blnIsEditMode = MblnChangeStatus = BindingNavigatorSaveItem.Enabled = btnSave.Enabled = btnOk.Enabled = btnEmail.Enabled = btnPrint.Enabled = BindingNavigatorDeleteItem.Enabled = BindingNavigatorAddNewItem.Enabled = false;
                    CboBankBranchID.Focus();
                    MblnAddStatus = true;
                }
                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on form AddNewItem:AddNewItem " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on AddNewItem() " + Ex.Message.ToString());

                return false;
            }

        }

        private void BindingAddNew()
        {

            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt + 1) + "";
            BindingNavigatorPositionItem.Text = Convert.ToString(MintRecordCnt + 1);
            MintCurrentRecCnt = MintRecordCnt + 1;
            if (MintRecordCnt >= 1)
            {
                BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = true;
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = false;
            }
            else
            {
                BindingNavigatorMoveNextItem.Enabled = BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = false;

            }
        }

        private void SetBindingNavigatorButtons()
        {
            BindingNavigatorPositionItem.Text = Convert.ToString(MintCurrentRecCnt);
            BindingNavigatorCountItem.Text = strBindingOf + Convert.ToString(MintRecordCnt) + "";

            BindingNavigatorMoveFirstItem.Enabled = BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = true;

            int iCurrentRec = Convert.ToInt32(BindingNavigatorPositionItem.Text); // Current position of the record
            int iRecordCnt = MintRecordCnt;  // Total count of the records

            if (iCurrentRec >= iRecordCnt)  // If the current is last record, disables move next and last button
            {
                BindingNavigatorMoveLastItem.Enabled = BindingNavigatorMoveNextItem.Enabled = false;

            }
            if (iCurrentRec == 1)  // If the current is first record, disables previous and first buttons
            {
                BindingNavigatorMovePreviousItem.Enabled = BindingNavigatorMoveFirstItem.Enabled = false;

            }

            BindingNavigatorAddNewItem.Enabled = MblnAddPermission;
            BindingNavigatorDeleteItem.Enabled = MblnDeletePermission;
            lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
            btnEmail.Enabled = btnPrint.Enabled = MblnPrintEmailPermission;
            BindingNavigatorSaveItem.Enabled = btnOk.Enabled = btnSave.Enabled = false;

        }

        /// <summary>
        /// Enable/disable buttons according to the action
        /// </summary>



        private bool LoadCombos(int intType)
        {
            // 0 - For Loading All Combo
            // 1 - For Loading CboBankBranchID 
            // 2 - For Loading CboCountry 
            // 3 - For Loading CboAccountID 
            try
            {
                DataTable datCombos = new DataTable();

                if (intType == 0 || intType == 1)
                {
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjclsBLLBankDetails.FillCombos(new string[] { "BankID,BankNameArb AS BankName", "BankReference", "" });
                    else
                        datCombos = MobjclsBLLBankDetails.FillCombos(new string[] { "BankID,BankName", "BankReference", "" });
                    CboBankBranchID.ValueMember = "BankID";
                    CboBankBranchID.DisplayMember = "BankName";
                    CboBankBranchID.DataSource = datCombos;
                }
                if (intType == 0 || intType == 2)
                {
                    datCombos = null;
                    if (ClsCommonSettings.IsArabicView)
                        datCombos = MobjclsBLLBankDetails.FillCombos(new string[] { "CountryID,CountryNameArb AS CountryName", "CountryReference", "" });
                    else
                        datCombos = MobjclsBLLBankDetails.FillCombos(new string[] { "CountryID,CountryName", "CountryReference", "" });
                    CboCountry.ValueMember = "CountryID";
                    CboCountry.DisplayMember = "CountryName";
                    CboCountry.DataSource = datCombos;
                }

                return true;
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on filling combobox:LoadCombos " + this.Name + " " + Ex.Message.ToString(), 2);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on LoadCombos() " + Ex.Message.ToString());

                return false;
            }
        }


        private void LoadMessage()
        {
            MaMessageArr = new ArrayList();
            MaStatusMessage = new ArrayList();
            MaMessageArr = MObjClsNotification.FillMessageArray((int)FormID.BankInformation, ClsCommonSettings.ProductID);
            MaStatusMessage = MObjClsNotification.FillStatusMessageArray((int)FormID.BankInformation, ClsCommonSettings.ProductID);
        }

        private void SetPermissions()
        {
            clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
            if (ClsCommonSettings.RoleID>3)
            {
                objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CurrentCompanyID, (Int32)eModuleID.Masters, (Int32)eMenuID.Bank, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);

            }
            else
                MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;
        }

        private void SetEnable()
        {
            CboBankBranchID.Text = CboCountry.Text = string.Empty;
            CboCountry.SelectedIndex = CboBankBranchID.SelectedIndex = -1;
            txtUAEBankCode.Text = txtWebsite.Text = txtEmail.Text = txtFax.Text = txtTelephone.Text = txtAddress.Text = txtBankCode.Text = txtDescription.Text = string.Empty;

        }





        // To check Validtion
        private bool BankDetailsFormValidation()
        {
            lblBankStatus.Text = "";
            if (Convert.ToInt32(CboBankBranchID.SelectedValue) == 0)
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 651, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProBank.SetError(CboBankBranchID, MsMessageCommon.Replace("#", "").Trim());
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmBank.Enabled = true;
                CboBankBranchID.Focus();
                return false;
            }
            if (txtDescription.Text.Trim() == "")
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 652, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProBank.SetError(txtDescription, MsMessageCommon.Replace("#", "").Trim());
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmBank.Enabled = true;
                txtDescription.Focus();
                return false;
            }


            if (txtEmail.Text.Trim().Length > 0 && !MobjclsBLLBankDetails.CheckValidEmail(txtEmail.Text.Trim()))
            {

                MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 653, out MmessageIcon);
                MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MmessageIcon);
                ErrProBank.SetError(txtEmail, MsMessageCommon.Replace("#", "").Trim());
                lblBankStatus.Text = MsMessageCommon.Remove(0, MsMessageCommon.IndexOf("#") + 1);
                TmBank.Enabled = true;
                txtEmail.Focus();
                return false;
            }



            return true;
        }

        private bool DeleteValidation(int intBankNameID)
        {
            DataTable dtIds = MobjclsBLLBankDetails.CheckExistReferences(intBankNameID);
            if (dtIds.Rows.Count > 0)
            {
                if (Convert.ToString(dtIds.Rows[0]["FormName"]) != "0")
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 667, out MmessageIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.OK, MmessageIcon);
                    TmBank.Enabled = true;
                    return false;
                }
            }
            return true;
        }





        /// <summary>
        /// Save Bankdetails
        /// </summary>
        /// <returns></returns>

        private bool SaveBank()
        {
            try
            {

                if (MblnAddStatus == true)//insert
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 1, out MmessageIcon);

                    MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID = 0;
                }
                else
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MaMessageArr, 3, out MmessageIcon);

                    MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID = MintBankNameID;
                }

                if (MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), MsMessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return false;

                MobjclsBLLBankDetails.clsDTOBankDetails.intBankBranchID = Convert.ToInt32(CboBankBranchID.SelectedValue);
                MobjclsBLLBankDetails.clsDTOBankDetails.strDescription = Convert.ToString(txtDescription.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strBankCode = Convert.ToString(txtBankCode.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strAddress = Convert.ToString(txtAddress.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strTelephone = Convert.ToString(txtTelephone.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strFax = Convert.ToString(txtFax.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.intCountryID = Convert.ToInt32(CboCountry.SelectedValue);
                MobjclsBLLBankDetails.clsDTOBankDetails.strEmail = Convert.ToString(txtEmail.Text.Replace("'", "`").Trim());
                MobjclsBLLBankDetails.clsDTOBankDetails.strWebsite = Convert.ToString(txtWebsite.Text.Replace("'", "`").Trim());
                // MobjclsBLLBankDetails.clsDTOBankDetails.intAccountID = Convert.ToInt32(CboAccountID.SelectedValue);
                //MobjclsBLLBankDetails.clsDTOBankDetails.strBankSortCode = null;


                if (txtUAEBankCode.Text == "")
                    MobjclsBLLBankDetails.clsDTOBankDetails.strUAEBankCode = "0";
                else
                    MobjclsBLLBankDetails.clsDTOBankDetails.strUAEBankCode = Convert.ToString(txtUAEBankCode.Text.Replace("'", "`").Trim());

                if (MobjclsBLLBankDetails.SaveBankDetails(MblnAddStatus))
                {
                    MintBankNameID = MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID;

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                MObjClsLogWriter.WriteLog("Error on Save:SaveBank() " + this.Name + " " + Ex.Message.ToString(), 3);

                if (ClsCommonSettings.ShowErrorMess)
                    MessageBox.Show("Error on Saving SaveBank()" + Ex.Message.ToString());
                return false;
            }
        }


        /// <summary>
        /// Display bank details
        /// </summary>

        public void DisplayBank()
        {

            if (MobjclsBLLBankDetails.DisplayBankDetails(MintCurrentRecCnt))
            {
                GetBankDetails();
            }
        }
        /// <summary>
        /// Display Bankdetails by ID
        /// </summary>
        public void DisplayBankID()
        {
            if (MobjclsBLLBankDetails.DisplayBankIDDetails(MintCurrentRecCnt, MintBankID))
            {
                GetBankDetails();
            }
        }
        /// <summary>
        /// Set Form controls while editing
        /// </summary>
        private void GetBankDetails()
        {
            MintBankNameID = MobjclsBLLBankDetails.clsDTOBankDetails.intBankNameID;
            CboBankBranchID.SelectedValue = MobjclsBLLBankDetails.clsDTOBankDetails.intBankBranchID.ToString();
            txtDescription.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strDescription;
            txtBankCode.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strBankCode.ToString();
            txtAddress.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strAddress.ToString();
            txtTelephone.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strTelephone.ToString();
            txtFax.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strFax.ToString();
            CboCountry.SelectedValue = MobjclsBLLBankDetails.clsDTOBankDetails.intCountryID.ToString();
            txtEmail.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strEmail.ToString();
            txtWebsite.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strWebsite.ToString();
            if (MobjclsBLLBankDetails.clsDTOBankDetails.strUAEBankCode == "0")
                txtUAEBankCode.Text = string.Empty;
            else
                txtUAEBankCode.Text = MobjclsBLLBankDetails.clsDTOBankDetails.strUAEBankCode.ToString();


            btnClear.Enabled = MblnChangeStatus = false;

        }

        #endregion

        
       

  

       


      



       

      

      

    



    }
}

