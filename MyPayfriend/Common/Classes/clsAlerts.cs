﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace MyPayfriend
{
    class clsAlerts
    {

        public bool AlertMessage(int DoctypeID, string DocType, string Description, int CommonID, string RefNo, DateTime ExpiryDate, string Filter, int iReferenceId, string sEmpName, bool IsCompany,int UserID)
        {
            try
            {
               
                DateTime DueDate;
                string AlertMess;
                int ProcessingTime=0;
                ArrayList parameters = new ArrayList();
                int uId = ClsCommonSettings.UserID; ;
                bool bAlertExists = false; 
                bool isExpiry = true;
                DataTable DsProcess = null;
                if (Filter == "Handover")
                {
                    isExpiry = false;

                    DsProcess = FillCombos(new string[] { "userid,ReturnTime", "processingtimemaster", "ReturnTime<>0 and documenttypeid=" + DoctypeID + "", "", "" });

                    if (DsProcess.Rows.Count > 0)
                    {

                        ProcessingTime = DsProcess.Rows[0]["ReturnTime"].ToInt32();

                    }
                }
                else


                    DsProcess = FillCombos(new string[] { "UserID,processingtime", "processingtimemaster", "(ProcessingTime<>0 and documenttypeid=" + DoctypeID + " and ( " + UserID + "=0 or UserID= " + UserID + "))", "", "" });

               
                if (DsProcess.Rows.Count > 0)
                {
                    for (int i = 0; i <= DsProcess.Rows.Count - 1; i++)
                    {
                        uId = Convert.ToInt32(DsProcess.Rows[i]["userid"]);
                        if (isExpiry)
                        {
                            ProcessingTime = Convert.ToInt32(DsProcess.Rows[i]["processingtime"]);

                        }
                        if (ProcessingTime <= 0) continue;

                        //DueDate = (DateAdd(DateInterval.Day, -(ProcessingTime), ExpiryDate));
                       
                        DueDate = ExpiryDate.AddDays(-(ProcessingTime));
                        
                        AlertMess = GenerateAlertMess(Filter, DueDate, Description, CommonID, RefNo, ExpiryDate, iReferenceId, sEmpName);

                        DataTable datData = FillCombos(new string[] { "1", "Alerts", "CommonId=" + CommonID + " and DocumentTYpeID=" + DoctypeID + " and UserID=" + uId + " and IsExpiry= " + (isExpiry ? 1 : 0),  "", "" });

                        if (datData.Rows.Count > 0)
                        {
                            bAlertExists = true;
                        }
                        else
                        {
                            bAlertExists = false;
                        }
                        if (bAlertExists)
                        {
                            parameters = new ArrayList();
                            parameters.Add(new SqlParameter("@Mode", 2));
                            parameters.Add(new SqlParameter("@userID", uId));
                            parameters.Add(new SqlParameter("@CommonID", CommonID));
                            parameters.Add(new SqlParameter("@DocumentTypeID", DoctypeID));
                            parameters.Add(new SqlParameter("@ReferenceNo", RefNo));
                            parameters.Add(new SqlParameter("@AlertMessage", AlertMess));
                            parameters.Add(new SqlParameter("@AlertDueDate", DueDate));
                            parameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
                            parameters.Add(new SqlParameter("@IsCompany", IsCompany));
                            parameters.Add(new SqlParameter("@IsExpiry", isExpiry));
                            parameters.Add(new SqlParameter("@EmployeeID", iReferenceId));
                            new  DataLayer().ExecuteNonQuery("SpAlertMessages",parameters );
                        }
                        else
                        {
                            parameters = new ArrayList();
                            parameters.Add(new SqlParameter("@Mode", 1));
                            parameters.Add(new SqlParameter("@userID", uId));
                            parameters.Add(new SqlParameter("@CommonID", CommonID));
                            parameters.Add(new SqlParameter("@DocumentTypeID", DoctypeID));
                            parameters.Add(new SqlParameter("@ReferenceNo", RefNo));
                            parameters.Add(new SqlParameter("@AlertMessage", AlertMess));
                            parameters.Add(new SqlParameter("@AlertDueDate", DueDate));
                            parameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
                            parameters.Add(new SqlParameter("@IsCompany", IsCompany));
                            parameters.Add(new SqlParameter("@IsExpiry", isExpiry ));
                            parameters.Add(new SqlParameter("@EmployeeID", iReferenceId));
                            new DataLayer().ExecuteNonQuery("SpAlertMessages", parameters);
                        }
                    }
                }
                
               
                DsProcess = null;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //// Issue Alert
        //public bool AlertMessage(int DoctypeID, string DocType, string Description, int CommonID, string RefNo, DateTime ExpiryDate, string Filter, int iReferenceId, string sEmpName, bool IsCompany,bool IsExpiry)
        //{
        //    try
        //    {

        //        DateTime DueDate;
        //        string AlertMess;
        //        int ProcessingTime = 0;
        //        ArrayList parameters = new ArrayList();
        //        int uId = ClsCommonSettings.UserID; ;
        //        bool bAlertExists = false;
        //        bool isExpiry = true;
        //        if (Filter == "Handover")
        //        {
        //            isExpiry = false;

        //            DataTable dtConfig = FillCombos(new string[] { "userid,ReturnTime", "processingtimemaster", "ReturnTime<>0 and documenttypeid=" + DoctypeID + "", "", "" });

        //            if (dtConfig.Rows.Count > 0)
        //            {

        //                ProcessingTime = dtConfig.Rows[0]["ReturnTime"].ToInt32();

        //            }
        //        }

        //        DataTable DsProcess = FillCombos(new string[] { "userid,processingtime", "processingtimemaster", " documenttypeid=" + DoctypeID + "", "", "" });


        //        if (DsProcess.Rows.Count > 0)
        //        {
        //            for (int i = 0; i <= DsProcess.Rows.Count - 1; i++)
        //            {
        //                uId = Convert.ToInt32(DsProcess.Rows[i]["userid"]);
        //                if (isExpiry)
        //                {
        //                    ProcessingTime = Convert.ToInt32(DsProcess.Rows[i]["processingtime"]);

        //                }
        //                if (ProcessingTime <= 0) continue;

        //                //DueDate = (DateAdd(DateInterval.Day, -(ProcessingTime), ExpiryDate));

        //                DueDate = ExpiryDate.AddDays(-(ProcessingTime));

        //                AlertMess = "" + Description + " - " + RefNo + " issued to " + sEmpName.Trim() + " and expected to return on " + ExpiryDate.ToString("dd/MMM/yyyy") + " . ";

        //                DataTable datData = FillCombos(new string[] { "1", "Alerts", "CommonId=" + CommonID + " and DocumentTYpeID=" + DoctypeID + " and UserID=" + uId + " and IsExpiry =0", "", "" });

        //                if (datData.Rows.Count > 0)
        //                {
        //                    bAlertExists = true;
        //                }
        //                else
        //                {
        //                    bAlertExists = false;
        //                }
        //                if (bAlertExists)
        //                {
        //                    parameters = new ArrayList();
        //                    parameters.Add(new SqlParameter("@Mode", 2));
        //                    parameters.Add(new SqlParameter("@userID", uId));
        //                    parameters.Add(new SqlParameter("@CommonID", CommonID));
        //                    parameters.Add(new SqlParameter("@DocumentTypeID", DoctypeID));
        //                    parameters.Add(new SqlParameter("@ReferenceNo", RefNo));
        //                    parameters.Add(new SqlParameter("@AlertMessage", AlertMess));
        //                    parameters.Add(new SqlParameter("@AlertDueDate", DueDate));
        //                    parameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        //                    parameters.Add(new SqlParameter("@IsExpiry", isExpiry));
        //                    new DataLayer().ExecuteNonQuery("SpAlertMessages", parameters);
        //                }
        //                else
        //                {
        //                    parameters = new ArrayList();
        //                    parameters.Add(new SqlParameter("@Mode", 1));
        //                    parameters.Add(new SqlParameter("@userID", uId));
        //                    parameters.Add(new SqlParameter("@CommonID", CommonID));
        //                    parameters.Add(new SqlParameter("@DocumentTypeID", DoctypeID));
        //                    parameters.Add(new SqlParameter("@ReferenceNo", RefNo));
        //                    parameters.Add(new SqlParameter("@AlertMessage", AlertMess));
        //                    parameters.Add(new SqlParameter("@AlertDueDate", DueDate));
        //                    parameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        //                    parameters.Add(new SqlParameter("@IsCompany", IsCompany));
        //                    parameters.Add(new SqlParameter("@IsExpiry", isExpiry));
        //                    parameters.Add(new SqlParameter("@EmployeeID", iReferenceId));
        //                    new DataLayer().ExecuteNonQuery("SpAlertMessages", parameters);
        //                }
        //            }
        //        }


        //        DsProcess = null;

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

     
     
        private string GenerateAlertMess(string filterpass, DateTime DueDate, string Description, int CommonID, string RefNo, DateTime ExpiryDate, int EmpId, string EmpName)
        {
           
            string AlertMsg="";
            try
            {
                switch (filterpass)
                {
                    case "Emp":

                        //'Message 
                        AlertMsg = "Employee: " + EmpName + ", " + Description + " " + RefNo + " expires on " + ExpiryDate.ToString("dd/MMM/yyyy") + "";// .Please start the renewal process by " + DueDate.ToString("dd/MMM/yyyy") + "";
                        break;

                    case "Comp":
                        //'Message 
                        AlertMsg = "Company: " + EmpName + " , " + Description + " " + RefNo + " expires on " + ExpiryDate.ToString("dd/MMM/yyyy") + "";// .Please start the renewal process by " + DueDate.ToString("dd/MMM/yyyy") + "";
                        break;

                    case "Others":
                        AlertMsg = "" + Description + " - " + RefNo + " expires on " + ExpiryDate.ToString("dd/MMM/yyyy") + "";// . Please start the renewal process by  " + DueDate.ToString("dd/MMM/yyyy") + "";
                        break;
                    case "Employee":

                        AlertMsg = "Employee: " + EmpName + ", " + Description + " is " + ExpiryDate.ToString("dd/MMM/yyyy") + " ";
                        break;
                    case "Handover":
                        AlertMsg = "" + Description + " - " + RefNo + " issued to " + EmpName.Trim() + " and expected to return on " + ExpiryDate.ToString("dd/MMM/yyyy") + " . ";
                break;
                }
                return AlertMsg;
            }
            catch (Exception)
            {
                return AlertMsg;
            }

        }

        public int GetUserAlertCount()
        {
            int iCount=0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "UC"));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            SqlDataReader sdr=  new DataLayer().ExecuteReader("AlertExpiryInfoSummary", parameters,CommandBehavior.CloseConnection);
            if (sdr.Read())
                iCount = sdr["AlCount"].ToInt32();
            sdr.Close();
            return iCount;
        }

        public DataTable GetUserAlerts()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "UTS"));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("AlertExpiryInfoSummary", parameters);
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            ClsCommonUtility MobjClsCommonUtility = new ClsCommonUtility();
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        /* FOR ALERT REPORT */

        /* Modified By : Sruthy K
         * Date        : 14 Aug 2013
         * */

        //Function for getting all the alerts based on the filteration
        public static DataTable GetAlerts(DateTime? FromDate, DateTime? ToDate, int UserID, int AlertTypeID, string SearchKey, int PageSize, int PageIndex)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 2));
            prmCommon.Add(new SqlParameter("@UserID", UserID));
            prmCommon.Add(new SqlParameter("@SearchKey", SearchKey));
            if (FromDate != null)
                prmCommon.Add(new SqlParameter("@FromDate", FromDate));
            if (ToDate != null)
                prmCommon.Add(new SqlParameter("@ToDate", ToDate));

            prmCommon.Add(new SqlParameter("@AlertTypeID", AlertTypeID));
            prmCommon.Add(new SqlParameter("@PageIndex", PageIndex));
            prmCommon.Add(new SqlParameter("@PageSize", PageSize));
            return new DataLayer().ExecuteDataTable("spAlertReport", prmCommon);

        }


        //Function for getting the total record count in the alert based on the parameters specified
        public static int GetTotalRecordCount(DateTime? FromDate, DateTime? ToDate, int UserID, int AlertTypeID, string SearchKey)
        {
            ArrayList prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 4));
            prmCommon.Add(new SqlParameter("@UserID", UserID));
            prmCommon.Add(new SqlParameter("@SearchKey", SearchKey));
            if (FromDate != null)
                prmCommon.Add(new SqlParameter("@FromDate", FromDate));
            if (ToDate != null)
                prmCommon.Add(new SqlParameter("@ToDate", ToDate));
            prmCommon.Add(new SqlParameter("@AlertTypeID", AlertTypeID));
            return new DataLayer().ExecuteScalar("spAlertReport", prmCommon).ToInt32();
        }
        /// <summary>
        /// Delete all the alerts of the particular user
        /// </summary>
        /// <param name="datAlert"></param>
        /// <param name="intUserID"></param>
        /// <returns></returns>
        public bool DeleteAlerts(List<AlertsDetails> Alerts)
        {
            ArrayList prmCommon;
            foreach (AlertsDetails alert in Alerts)
            {
                prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", 3));
                prmCommon.Add(new SqlParameter("@UserID",ClsCommonSettings.UserID ));
                prmCommon.Add(new SqlParameter("@CommonID", alert.CommonId));
                prmCommon.Add(new SqlParameter("@DocumentTypeID",alert.DocumentTypeID ));
                new  DataLayer().ExecuteNonQuery("spAlertReport",prmCommon);
            }
           
            return true;
        }

        //Function for setting the auto complete suggestion list
        public static AutoCompleteStringCollection GetAutoCompleteList(int intUserID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@UserID", intUserID));

            DataTable dt = new DataLayer().ExecuteDataTable("spAlertReport", alParameters);
            string[] array = { };
            array = Array.ConvertAll(dt.Select(), row => (string)row[1]);

            var collection = new AutoCompleteStringCollection();
            collection.AddRange(array);

            return collection;
        }
        public void UpdateAlerts(int UserID)
        {
            try
            {

                System.Data.DataTable Dt;

                Dt = new  DataLayer().ExecuteDataTable("spGetDocumentsDtls");

                if (Dt.Rows.Count >= 0)
                {
                    for (int i = 0; i <= Dt.Rows.Count - 1; ++i)
                    {
                        AlertMessage(Dt.Rows[i]["DocumentTypeID"].ToInt32(), Dt.Rows[i]["AB"].ToString(), Dt.Rows[i]["AC"].ToString(), Dt.Rows[i]["DocumentID"].ToInt32(), Dt.Rows[i]["DocumentNumber"].ToString(), Dt.Rows[i]["ExpiryDate"].ToDateTime(), Dt.Rows[i]["FIL"].ToString(), Dt.Rows[i]["ReferenceID"].ToInt32(), Dt.Rows[i]["EMPname"].ToString(), Dt.Rows[i]["OperationTypeID"].ToBoolean(), UserID);
                    }
                }
            }
            catch
            {
            }
        }
       
    }

    public class AlertsDetails
    {
        public int CommonId { get; set; }

        public int DocumentTypeID { get; set; }
    }
}
