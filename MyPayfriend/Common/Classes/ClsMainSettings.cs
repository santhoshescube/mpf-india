﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyPayfriend;


public static class ClsMainSettings
{
    public static bool blnLoginStatus { get; set; }
    public static bool blnProductStatus { get; set; }
    public static bool blnActivationStatus { get; set; }
    public static string strProductSerial { get; set; }
}
