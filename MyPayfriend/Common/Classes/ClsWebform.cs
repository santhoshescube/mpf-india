﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Microsoft.VisualBasic;
class ClsWebform
{
    public int PRecID;
    long MintRecordID;
    int MintFormType;
    int intType = 0;
    //string CollectionType;

    string MstrCaption;
    string imgPath = Application.StartupPath + "\\NoInfo.jpg";
    string strColor = "#fff2eb";
    string sAltColor = "#ffffff";
    string Type;
    string strName;
    string strValue;
    string strTypeName;
    string strTypeValue;
    string strRefNoName;
    string strRefNoValue;
    string strMainNoName;
    string strMainNoValue;
    string strDateName;
    string strDateValue;
    string strWareHouseValue;
    string strEmployeeValue;
    bool mFlag;
    public DataTable datHistory = null;

    DataSet EmailSource;

    StringBuilder MsbHtml;
    clsConnection MobjConnection;
    //ClsCommonUtility MObjCommonUtility;

    public ClsWebform()
    {
        MobjConnection = new clsConnection();
        //MObjCommonUtility = new ClsCommonUtility();

        MsbHtml = new StringBuilder();
    }

    public string GetWebFormData(string sCaption, EmailFormID iFormType, DataSet dtsEmail)
    {
        EmailSource = dtsEmail;
        MstrCaption = sCaption;

        string strNoInformation = "<html><head></head><body oncontextmenu=return false ondragstart=return false onselectstart=return false><center><img src='" + imgPath + "' border=none alt=No Information Found></img></center></body></html>";
        string strHtml = string.Empty;

        switch (iFormType)
        {
            case EmailFormID.Company:
                strHtml = GetCompanyEmail();
                break;
            case EmailFormID.Bank:
                strHtml = GetBankEmail();
                break;
            case EmailFormID.Employee:
                strHtml = GetEmployeeEmail();
                break;              
            case EmailFormID.ExchangeCurrency:
                strHtml = GetExchangeCurrencyEmail();
                break;
            case EmailFormID.Documents:
                strHtml = GetDocumentEmail();
                break;
            case EmailFormID.RoleSettings:
                strHtml = GetRoleSettingsEmail();
                break;
            case EmailFormID.Projects:
                strHtml = GetProjectEmail();
                break;
            case EmailFormID.SalaryStructure:
                strHtml = GetSalaryStructureEmail();
                break;
            case EmailFormID.ShiftPolicy:
                strHtml = GetShiftPolicyEmail();
                break;
            case EmailFormID.WorkPolicy:
                strHtml = GetWorkPolicyEmail();
                break;
            case EmailFormID.LeavePolicy:
                strHtml = GetLeavePolicyEmail();
                break;
            case EmailFormID.SalaryPayment:
                strHtml = GetPayment(PRecID);
                break;
            case EmailFormID.WorkLocation:
                strHtml = GetWorkLocation();
                break;
            case EmailFormID.LocationSchedule:
                strHtml = GetLocationSchedule();
                break;
            case EmailFormID.DeductionPolicy:
                strHtml = GetDeduction();
                break;
            case EmailFormID.LeaveEntry:
                strHtml = GetLeaveEntry();
                break;
            case EmailFormID.LeaveExtension:
                strHtml = GetLeaveExtension();
                break;
            case EmailFormID.PFPolicy:
                strHtml = GetPFPolicy();
                break;

            case EmailFormID.Qualification:
                strHtml = GetQualification();
                break;

            case EmailFormID.Passport:
                strHtml = GetPassportEmail();
                break;

            case EmailFormID.Visa:
                strHtml = GetEmployeeVisa();
                break;

            case EmailFormID.DrivingLicense:
                strHtml = GetDrivingLicenseEmail();
                break;

            case EmailFormID.EmployeeLoan:
                strHtml = GetEmployeeLoanEmail();
                break;
            case EmailFormID.EmirateHealthCard:
                strHtml = GetEmirateHealthCard();
                break;
            case EmailFormID.HealthCard:
                strHtml = GetHealthCard();
                break;
            case EmailFormID.InsuranceCardID:
                strHtml = GetInsuranceCard();
                break;
            case EmailFormID.LabourCard:
                strHtml = GetLabourCard();
                break;
            case EmailFormID.LeaveRequest:
                strHtml = GetLeaveRequest();
                break;
            case EmailFormID.ShiftSchedule:
                strHtml = GetShiftSchedule();
                break;
            case EmailFormID.Expense:
                strHtml = GetEmployeeExpenseEmail();
                break;
            case EmailFormID.Deposit:
                strHtml = GetDepositEmail();
                break;
            case EmailFormID.LoanRepayment:
                strHtml = GetLoanRepaymentEmail();
                break;
            case EmailFormID.SalaryAdvance:
                strHtml = GetSalaryAdvanceEmail();
                break;
            case EmailFormID.LeaveStructure:
                strHtml = GetLeaveStructureEmail();
                break;
            case EmailFormID.SettlementProcess:
                strHtml = GetEmployeeSettlement();
                break;
            case EmailFormID.TradeLicense:
                strHtml = GetTradeLicenseEmail();
                break;
            case EmailFormID.LeaseAgreement:
                strHtml = GetLeaseAgreementEmail();
                break;
            case EmailFormID.VacationProcess:
                strHtml = GetEmployeeVacationEmail();
                break;
            case EmailFormID.VacationPolicy:
                strHtml = GetVacationPolicyEmail();
                break;
            case EmailFormID.AssetHandover:
                strHtml = GetAssetHandover();
                break;
            case EmailFormID.InsuranceDetails:
                strHtml = GetInsuranceDetails();
                break;

            case EmailFormID.CompanyAsset:
                strHtml = GetCompanyAssetsDetails();
                break;

        }
        return strHtml;
    }



    private string GetCompanyAssetsDetails()
    {

        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Company Asset Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data

            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Description</td><td width=70%>" + Convert.ToString(DrWebInfo["Description"]) + "</td></tr>" +
           "<tr   height=25px><td width=30%>Benefit Type</td><td width=70%>" + Convert.ToString(DrWebInfo["BenefitType"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Reference Number</td><td width=70%>" + Convert.ToString(DrWebInfo["ReferenceNumber"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Purchase Date</td><td width=70%>" + Convert.ToString(DrWebInfo["PurchaseDate"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Purchase Value</td><td width=70%>" + Convert.ToString(DrWebInfo["PurchaseValue"]) + "</td></tr>" +
           "<tr   height=25px><td width=30%>Depreciation</td><td width=70%>" + Convert.ToString(DrWebInfo["Depreciation"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExpiryDate"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%> Status</td><td width=70%>" + Convert.ToString(DrWebInfo["AssetStatus"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30% valign='top'>Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    



    private string GetInsuranceDetails()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Insurance Policy Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data

            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Asset</td><td width=70%>" + Convert.ToString(DrWebInfo["AssetName"]) + "</td></tr>" +
           "<tr   height=25px><td width=30%>Insurance Company</td><td width=70%>" + Convert.ToString(DrWebInfo["InsuranceCompany"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Policy Number</td><td width=70%>" + Convert.ToString(DrWebInfo["PolicyNumber"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Policy Name</td><td width=70%>" + Convert.ToString(DrWebInfo["PolicyName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Policy Amount</td><td width=70%>" + Convert.ToString(DrWebInfo["PolicyAmount"]) + "</td></tr>" +
           "<tr   height=25px><td width=30%>Issue Date</td><td width=70%>" + Convert.ToString(DrWebInfo["IssueDate"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExpiryDate"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%> Renewed</td><td width=70%>" + Convert.ToString(DrWebInfo["IsRenewed"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30% valign='top'>Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetEmployeeVacationEmail()
    {
        mFlag = false;
        GetWebformHeaderInfo();

        string sType = "";
        DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
      
            MsbHtml.Append("<table border='0' cellspading='0' cellspacing='0' width='100%'> " +
                            "<tr height='25px'><td colspan='2' align='left'><font size='2' color='#3a3a53'><b>General Information</b></font></td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Employee</td><td align='left'>" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Process Date</td><td align='left'>" + Convert.ToString(DrWebInfo["Date"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Vacation From Date</td><td align='left'>" + Convert.ToString(DrWebInfo["FromDate"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Vacation To Date</td><td align='left'>" + Convert.ToString(DrWebInfo["ToDate"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Experience Days</td><td align='left'>" + Convert.ToString(DrWebInfo["ExperienceDays"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Consider Absent Days</td><td align='left'>" + Convert.ToString(DrWebInfo["ConsiderAbsentDays"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Absent Days</td><td align='left'>" + Convert.ToString(DrWebInfo["AbsentDays"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Taken Leaves</td><td align='left'>" + Convert.ToString(DrWebInfo["TakenLeaves"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>OverTaken Leaves</td><td align='left'>" + Convert.ToString(DrWebInfo["OvertakenLeaves"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Issued Tickets</td><td align='left'>" + Convert.ToString(DrWebInfo["IssuedTickets"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Ticket Expense</td><td align='left'>" + Convert.ToString(DrWebInfo["TicketExpense"]) + "</td></tr>" +
                             "<tr height='25px'><td width='30%' align='left'>Earnings</td><td align='left'>" + Convert.ToString(DrWebInfo["Credit"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Deductions</td><td align='left'>" + Convert.ToString(DrWebInfo["Debit"]) + "</td></tr> " +
                             "<tr height='25px'><td width='30%' align='left'>Net Amount</td><td align='left'>" + Convert.ToString(DrWebInfo["NetAmount"]) + "</td></tr> " +
                             "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Currency</td><td align='left'>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                             "<tr height='25px'><td width='30%' align='left'>Remarks</td><td align='left'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr> " +
                         " </table>");

            mFlag = true;
        }
        else
        {
            mFlag = false;
        }


        if (mFlag == true)
        {
            MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><b>Vacation Info</b></font></td></tr>" +
                      "<tr><td colspan=2>" + GetEmployeeVacationDetails() + " </td></tr>");
        }
        else
        {
            MsbHtml.Append("<html><head></head><body bgcolor=White oncontextmenu=return false ondragstart=return false onselectstart=return false><center><img src='" + imgPath + "' border=none alt=No Information Found></img></center></body></html>");
        }
        return MsbHtml.ToString();
    }

    private string GetEmployeeVacationDetails()
    {
        StringBuilder sbHtmlEarning = new StringBuilder();

        sbHtmlEarning.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>" +
                             "<tr bgcolor=#3a3a53><td width=35% align=left><font size=2 color=white>Particulars</td><td align=left><font size=2 color=white>Earning</td>" +
                             "<td align=left><font size=2 color=white>Deduction</td>"+
                             "<td width=35% align=left><font size=2 color=white>NoOfDays</td></tr>");

        string color = "#e5e5ef";
        bool blExists = false;


        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            sbHtmlEarning.Append(" <tr bgcolor=" + color + "><td align='left'>" + Convert.ToString(drowEmail["Description"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["Credit"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["Debit"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["PaidDays"]) + "</td> " +
                               " </tr> ");
            if (color == "#e5e5ef")
            {
                color = "#FFFFFF";
            }
            else
            {
                color = "#e5e5ef";
            }

        }

        blExists = true;

        if (blExists == false)
        {
            sbHtmlEarning.Append("<table border=0 cellspading=0 cellspacing=0 width=100%><tr><td align=left><body bgcolor=White><font color=blue>No Earning Found</font></td></tr></table>");
        }
        else
        {
            sbHtmlEarning.Append("</table>");
        }
        return sbHtmlEarning.ToString();

    }

    private string GetEmployeeSettlement()
    {
        mFlag = false;
        GetWebformHeaderInfo();

        string sType  = "";
        DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
        if (EmailSource.Tables[0].Rows.Count >0)
           {
               if (DrWebInfo["WorkStatus"] != System.DBNull.Value)
                {
                    sType = Convert.ToString(DrWebInfo["WorkStatus"]);
                }
                 MsbHtml.Append("<table border='0' cellspading='0' cellspacing='0' width='100%'> " +
                                 "<tr height='25px'><td colspan='2' align='left'><font size='2' color='#3a3a53'><b>General Information</b></font></td></tr> " +
                                  "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Employee</td><td align='left'>"+ Convert.ToString(DrWebInfo["EmployeeFullName"]) +"</td></tr> " +
                                  "<tr height='25px'><td width='30%' align='left'>Joining Date</td><td align='left'>"+   Convert.ToString(DrWebInfo["FromDate"])+"</td></tr> " +
                                  "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Type</td><td align='left'>" + Convert.ToString(DrWebInfo["WorkStatus"]) + "</td></tr> " +
                                  "<tr height='25px'><td width='30%' align='left'>To Date</td><td align='left'>"+  Convert.ToString(DrWebInfo["ToDate"])+"</td></tr> " +
                                  "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Process Date</td><td align='left'>"+   Convert.ToString(DrWebInfo["Date"])+"</td></tr> " +
                                  "<tr height='25px'><td width='30%' align='left'>Document Return</td><td align='left'>"+   Convert.ToString(DrWebInfo["DocumentReturn"])+"</td></tr> " +
                                  "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Earnings</td><td align='left'>"+   Convert.ToString(DrWebInfo["Credit"])+"</td></tr> " +
                                  "<tr height='25px'><td width='30%' align='left'>Deductions</td><td align='left'>"+   Convert.ToString(DrWebInfo["Debit"])+"</td></tr> " +
                                  "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Net Amount</td><td align='left'>"+   Convert.ToString(DrWebInfo["NetAmount"])+"</td></tr> " +
                                  "<tr height='25px'><td width='30%' align='left'>Currency</td><td align='left'>"+  Convert.ToString(DrWebInfo["Currency"])+"</td></tr> " +
                                  "<tr bgcolor='#e5e5ef' height='25px'><td width='30%' align='left'>Remarks</td><td align='left'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>" +
                              " </table>");
                mFlag = true;
           }
            else
           {
                mFlag = false;
           }


        if (mFlag == true)
        {
            MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><b>Settlement Info</b></font></td></tr>" +
                      "<tr><td colspan=2>" + GetEmployeeSettlementDetails() + " </td></tr>");
        }
        else
        {
            MsbHtml.Append("<html><head></head><body bgcolor=White oncontextmenu=return false ondragstart=return false onselectstart=return false><center><img src='" + imgPath + "' border=none alt=No Information Found></img></center></body></html>");
        }
         return MsbHtml.ToString();
     }
    private string GetEmployeeSettlementDetails()
    {
        StringBuilder sbHtmlEarning =new StringBuilder() ;

        sbHtmlEarning.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>" +
                             "<tr bgcolor=#3a3a53><td width=35% align=left><font size=2 color=white>Particulars</td><td align=left><font size=2 color=white>Earning</td>" +
                             "<td align=left><font size=2 color=white>Deduction</td></tr>");

        string color = "#e5e5ef";
        bool blExists = false;


        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            sbHtmlEarning.Append(" <tr bgcolor=" + color + "><td align='left'>" + Convert.ToString(drowEmail["Particulars"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["Credit"]) + "</td> " +
                               " <td align='left'>" + Convert.ToString(drowEmail["Debit"]) + "</td> " +
                               " </tr> ");
            if (color == "#e5e5ef")
            {
                color = "#FFFFFF";
            }
            else
            {
                color = "#e5e5ef";
            }

        }

        blExists = true;

        if (blExists == false)
        {
            sbHtmlEarning.Append("<table border=0 cellspading=0 cellspacing=0 width=100%><tr><td align=left><body bgcolor=White><font color=blue>No Earning Found</font></td></tr></table>");
        }
        else
        {
            sbHtmlEarning.Append("</table>");
        }
        return sbHtmlEarning.ToString();

    }

    private string GetLeaveStructureEmail()
    {
        decimal decBalance = 0;
        GetWebformHeaderInfo();
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];


            MsbHtml.Append("<tr height=25px><td width=30%> Employee Name </td><td width=70%>" + Convert.ToString(DrWebInfo["Name"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Employee Number</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeNumber"]) + "</td></tr>" +
            "<tr height=25px><td width=30%>Financial Year</td><td width=70%>" + Convert.ToString(DrWebInfo["FinYear"]) + "</td></tr>");

            strColor = "#ffffff";

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Leave Structure Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Leave Type</b></font></td>" +
                           "<td><font color=#FFFFF><b>Days</b></font></td><td><font color=#FFFFF><b>Carry Forward</b></font></td></font>" +
                           "<td><font color=#FFFFF><b>Taken Leaves</b></font></td><td><font color=#FFFFF><b>Extended Leaves</b></font></td></font>" +
                           "<td><font color=#FFFFF><b>Balance</b></font></td><td><font color=#FFFFF><b>Month Leave</b></font></td></font>" +
                           "<td><font color=#FFFFF><b>Encash Leaves</b></font></td></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                decBalance = 0;
                decBalance = drowEmail["BalanceLeave"].ToDecimal() + drowEmail["CarryForwardDays"].ToDecimal() + drowEmail["ExtendedLeaves"].ToDecimal() - drowEmail["TakenLeaves"].ToDecimal();
                if (decBalance < 0) decBalance = 0;
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["LeaveType"]) + "</td>" +
                              "<td style='text-align:center'>" + drowEmail["BalanceLeave"].ToDecimal().ToString("0.0") + "</td> " +
                               "<td style='text-align:center'>" + drowEmail["CarryForwardDays"].ToDecimal().ToString("0.0") + "</td> " +
                               "<td style='text-align:center'>" + drowEmail["TakenLeaves"].ToDecimal().ToString("0.0") + "</td> " +
                               "<td style='text-align:center'>" + drowEmail["ExtendedLeaves"].ToDecimal().ToString("0.0") + "</td>" +
                               "<td style='text-align:center'>" + decBalance.ToString("0.0") + "</td>" +
                               "<td style='text-align:center'>" + drowEmail["MonthLeaves"].ToDecimal().ToString("0.0") + "</td>" +
                               "<td style='text-align:center'>" + drowEmail["EncashLeaves"].ToDecimal().ToString("0.0") + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Leave Structure Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetLabourCard()
    {
        GetWebformHeaderInfo();

        // Heading
        //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Labour Card Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>Employee Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeFullname"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Card Number</td><td width=70%>" + Convert.ToString(DrWebInfo["CardNumber"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Card Personal Number</td><td width=70%>" + Convert.ToString(DrWebInfo["CardPersonalNumber"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Issue Date</td><td width=70%>" + Convert.ToString(DrWebInfo["IssueDate"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExpiryDate"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Renewed</td><td width=70%>" + Convert.ToString(DrWebInfo["IsRenewed"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetLeaveRequest()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Employee Leave-Request Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data
            //<tr height=25px><td width=30%>Plan Type</td><td word-wrap:break-all width=70%>" + Convert.ToString(DrWebInfo["PlanType"]) + "</td>" +
            MsbHtml.Append("<tr height=25px><td width=30%>Employee Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeFullname"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Leave Type</td><td width=70%>" + Convert.ToString(DrWebInfo["LeaveType"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Requested Date</td><td width=70%>" + Convert.ToString(DrWebInfo["RequestDate"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>From Date</td><td width=70%>" + Convert.ToString(DrWebInfo["FromDate"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>To Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ToDate"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px> <td width=30%>Is HalfDay</td><td width=70%>" + Convert.ToString(DrWebInfo["HalfDay"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>No: of Holidays</td><td width=70%>" + Convert.ToString(DrWebInfo["NoofHolidays"]) + "</td></tr>" +
            "<tr  bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td width=70%>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>" +
           "<tr   height=25px><td width=30%>Status</td><td width=70%>" + Convert.ToString(DrWebInfo["LeaveRequestStatus"]) + "</td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetHealthCard()
    {
        GetWebformHeaderInfo();

        // Heading
        //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Health Card Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>Employee Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeFullname"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Card Number</td><td width=70%>" + Convert.ToString(DrWebInfo["CardNumber"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Card Personal Number</td><td width=70%>" + Convert.ToString(DrWebInfo["CardPersonalNumber"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Issue Date</td><td width=70%>" + Convert.ToString(DrWebInfo["IssueDate"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExpiryDate"]) + "</td></tr>" +

           "<tr height=25px><td width=30%>Renewed</td><td width=70%>" + Convert.ToString(DrWebInfo["IsRenewed"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetInsuranceCard()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Insurance Card Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data

            MsbHtml.Append("<tr height=25px><td width=30%>Employee Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Card Number</td><td width=70%>" + Convert.ToString(DrWebInfo["CardNumber"]) + "</td></tr>" +
           "<tr   height=25px><td width=30%>Insurance Company</td><td width=70%>" + Convert.ToString(DrWebInfo["InsuranceCompany"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Policy Number</td><td width=70%>" + Convert.ToString(DrWebInfo["PolicyNumber"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Policy Name</td><td width=70%>" + Convert.ToString(DrWebInfo["PolicyName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Issued By</td><td width=70%>" + Convert.ToString(DrWebInfo["IssuedBy"]) + "</td></tr>" +
           "<tr   height=25px><td width=30%>Issue Date</td><td width=70%>" + Convert.ToString(DrWebInfo["IssueDate"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExpiryDate"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%> Renewed</td><td width=70%>" + Convert.ToString(DrWebInfo["IsRenewed"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30% valign='top'>Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetTradeLicenseEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Trade License Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data

            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Sponsor</td><td width=70%>" + Convert.ToString(DrWebInfo["Sponsor"]) + "</td></tr>" +
           "<tr   height=25px><td width=30%>Sponsor Fee</td><td width=70%>" + Convert.ToString(DrWebInfo["SponserFee"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Licence Number</td><td width=70%>" + Convert.ToString(DrWebInfo["LicenceNumber"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>City</td><td width=70%>" + Convert.ToString(DrWebInfo["City"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Province</td><td width=70%>" + Convert.ToString(DrWebInfo["ProvinceName"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Partner</td><td width=70%>" + Convert.ToString(DrWebInfo["Partner"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Issue Date</td><td width=70%>" + Convert.ToString(DrWebInfo["IssueDate"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExpiryDate"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Renewed</td><td width=70%>" + Convert.ToString(DrWebInfo["IsRenewed"]) + "</td></tr>" +
           "<tr   height=25px><td width=30% valign='top'>Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetLeaseAgreementEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Lease Agreement Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data

            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Agreement Number</td><td width=70%>" + Convert.ToString(DrWebInfo["AgreementNo"]) + "</td></tr>" +
           "<tr   height=25px><td width=30%>Lessor</td><td width=70%>" + Convert.ToString(DrWebInfo["Lessor"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Start Date</td><td width=70%>" + Convert.ToString(DrWebInfo["StartDate"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>End Date</td><td width=70%>" + Convert.ToString(DrWebInfo["EndDate"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Document Date</td><td width=70%>" + Convert.ToString(DrWebInfo["DocumentDate"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Lease Period</td><td width=70%>" + Convert.ToString(DrWebInfo["LeasePeriod"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Rental value</td><td width=70%>" + Convert.ToString(DrWebInfo["RentalValue"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Deposit Held</td><td width=70%>" + Convert.ToString(DrWebInfo["DepositHeld"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Tenant</td><td width=70%>" + Convert.ToString(DrWebInfo["Tenant"]) + "</td></tr>" +
           "<tr   height=25px><td width=30% valign='top'>Contact Numbers</td><td width=70% >" + Convert.ToString(DrWebInfo["ContactNumbers"]) + "</td></tr>" +
           "<tr   bgcolor=#fff2eb height=25px><td width=30% valign='top'>Address</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Address"]) + "</td></tr>" +
           "<tr   height=25px><td width=30% valign='top'>Land Lord</td><td width=70% >" + Convert.ToString(DrWebInfo["Landlord"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30% valign='top'>Unit Number</td><td width=70% >" + Convert.ToString(DrWebInfo["UnitNumber"]) + "</td></tr>" +
           "<tr   height=25px><td width=30% valign='top'>Lease UnitType</td><td width=70% >" + Convert.ToString(DrWebInfo["LeaseUnitType"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30% valign='top'>Building Number</td><td width=70% >" + Convert.ToString(DrWebInfo["BuildingNumber"]) + "</td></tr>" +
           "<tr   height=25px><td width=30% valign='top'>Location</td><td width=70% >" + Convert.ToString(DrWebInfo["Location"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30% valign='top'>Plot Number</td><td width=70% >" + Convert.ToString(DrWebInfo["PlotNumber"]) + "</td></tr>" +
           "<tr  height=25px><td width=30% valign='top'>Total</td><td width=70% >" + Convert.ToString(DrWebInfo["Total"]) + "</td></tr>" +
           "<tr   bgcolor=#fff2eb height=25px><td width=30% valign='top'>Renewed</td><td width=70% >" + Convert.ToString(DrWebInfo["IsRenewed"]) + "</td></tr>" +
           "<tr   height=25px><td width=30% valign='top'>Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetEmployeeLoanEmail()
    {
        GetWebformHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Employee Loan Details</h2></font></td></tr>");
        strColor = "#ffffff";
        DataRow drEmail = null;

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            drEmail = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><b>General Information</b></font></td></tr>" +
                                  "<tr bgcolor=#fff2eb height=25px><td width=30% align=left>Employee Name</td><td align=left>" + Convert.ToString(drEmail["EmployeeName"]) + "</td></tr>" +
                                  "<tr height=25px><td align=left>Loan Number</td><td align=left>" + Convert.ToString(drEmail["LoanNumber"]) + "</td></tr>" +
                                  "<tr bgcolor=#fff2eb height=25px><td align=left>Loan Type</td><td align=left>" + Convert.ToString(drEmail["LoanType"]) + "</td></tr>" +
                                  "<tr height=25px><td align=left>Status</td><td align=left>" + drEmail["Status"].ToString() + "</td></tr>" +
                                  "<tr bgcolor=#fff2eb height=25px><td align=left>Loan Date</td><td align=left>" + Convert.ToString(drEmail["LoanDate"]) + "</td></tr>" +
                                  "<tr height=25px><td align=left>Amount</td><td align=left>" + drEmail["Amount"].ToDecimal().ToString("F" + 2) + "</td></tr>" +
                                  "<tr bgcolor=#fff2eb  height=25px><td align=left>Interest Rate</td><td align=left>" + drEmail["InterestRate"].ToDecimal().ToString("F" + 2) + "</td></tr>" +
                                  "<tr height=25px><td align=left>Payment Amount</td><td align=left>" + drEmail["PaymentAmount"].ToDecimal().ToString("F" + 2) + "</td></tr>" +
                                  "<tr bgcolor=#fff2eb height=25px><td align=left>Deduction Start Date</td><td align=left>" + drEmail["DeductStartDate"].ToString() + "</td></tr>" +
                                  "<tr height=25px><td align=left>Deduction End Date</td><td align=left>" + drEmail["DeductEndDate"].ToString() + "</td></tr>" +
                                  "<tr bgcolor=#fff2eb height=25px><td align=left>No Of Installment</td><td align=left>" + drEmail["NoOfInstalment"].ToString() + "</td></tr>" +
                                  "<tr height=25px><td align=left>Currency</td><td align=left>" + drEmail["CurrencyName"].ToString() + "</td></tr>" +
                                  "<tr bgcolor=#fff2eb  height=25px><td align=left>Reason</td><td align=left style='word-break:break-all'>" + drEmail["Reason"].ToString() + "</td></tr>");
        }
        if (EmailSource.Tables[1].Rows.Count > 0)
        {
            MsbHtml.Append("<tr height=25px><td colspan=3 align=left><font size=2 color=#3a3a53><b>Installment Details</b></font></td></tr>" +
                            "<tr><td colspan=2><table border=0 cellspading=0 cellspacing=0 width=100%>" +
                            "<tr bgcolor=#6d6e71><td width=33% align=left  height=25px><font size=2 color=white>Installment No</td><td align=left width=33%  height=25px><font size=2 color=white>Installment Date</td>" +
                            "<td align=left  height=25px><font size=2 color=white>Amount</td></tr>");
            for (int iCounter = 0; iCounter < EmailSource.Tables[1].Rows.Count; iCounter++)
            {
                drEmail = EmailSource.Tables[1].Rows[iCounter];
                MsbHtml.Append("<tr bgcolor=" + strColor + "><td width=33% align=left  height=25px>" + drEmail["InstalmentNo"].ToString() + "</td><td width=33% align=left  height=25px>" + drEmail["InstalmentDate"].ToString() + "</td>" +
                                   "<td align=left  height=25px>" + drEmail["Amount"].ToDecimal().ToString("F" + 2) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";

            }
            MsbHtml.Append("</table></td></tr>");
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }


    private string GetDrivingLicenseEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Driving License Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data
            //<tr height=25px><td width=30%>Plan Type</td><td word-wrap:break-all width=70%>" + Convert.ToString(DrWebInfo["PlanType"]) + "</td>" +
            MsbHtml.Append("<tr height=25px><td width=30%>Employee Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>License Number</td><td width=70%>" + Convert.ToString(DrWebInfo["LicenceNumber"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Name in License</td><td width=70%>" + Convert.ToString(DrWebInfo["LicenceHoldersName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>License Type</td><td width=70%>" + Convert.ToString(DrWebInfo["LicenceType"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Country</td><td width=70%>" + Convert.ToString(DrWebInfo["LicencedCountry"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Issue Date</td><td width=70%>" + Convert.ToString(DrWebInfo["IssueDate"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExpiryDate"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Renewed </td><td width=70%>" + Convert.ToString(DrWebInfo["RenewStatus"]) + "</td></tr>" +
           "<tr   height=25px><td width=30% valign='top'>Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>"
           );
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }
    private string GetPassportEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Passport Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data
            //<tr height=25px><td width=30%>Plan Type</td><td word-wrap:break-all width=70%>" + Convert.ToString(DrWebInfo["PlanType"]) + "</td>" +
            MsbHtml.Append("<tr height=25px><td width=30%>Employee Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Passport Number</td><td width=70%>" + Convert.ToString(DrWebInfo["PassportNumber"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Name in Passport</td><td width=70%  style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["NameInPassport"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Place of Issue</td><td width=70%  style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["PlaceOfIssue"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Country</td><td width=70%>" + Convert.ToString(DrWebInfo["Country"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Place of Birth</td><td width=70%>" + Convert.ToString(DrWebInfo["PlaceOfBirth"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Pages available</td><td width=70%>" + Convert.ToString(DrWebInfo["PassportPagesAvailable"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%> Renewed </td><td width=70%>" + Convert.ToString(DrWebInfo["RenewStatus"]) + "</td></tr>" +

           "<tr  height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Dates</h2></font></td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Issue Date</td><td width=70%>" + Convert.ToString(DrWebInfo["IssueDate"]) + "</td></tr>" +
            "<tr  height=25px><td width=30%>Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExpiryDate"]) + "</td></tr>" +


            "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Other Information</h2></font></td></tr>" +
            "<tr height=25px><td width=30% valign='top'>Residence Permits</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["ResidencePermits"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30% valign='top'>Old Passports</td><td width=70%  style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["OldPassportNumbers"]) + "</td></tr>" +
            "<tr  height=25px><td width=30% valign='top'>Entry Restriction</td><td width=70%  style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["EntryRestrictions"]) + "</td></tr>");

        }



        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }

    private string GetQualification()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Employee Qualification Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            MsbHtml.Append("<tr height=25px><td width=30%> Employee Name </td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>University </td><td width=70%>" + Convert.ToString(DrWebInfo["University"]) + "</td></tr>" +
                            "<tr  height=25px><td width=30%>School or College</td><td width=70%>" + Convert.ToString(DrWebInfo["SchoolOrCollege"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td width=30%> Qualification </td><td width=70%>" + Convert.ToString(DrWebInfo["Degree"]) + "</td></tr>" +
                             "<tr  height=25px><td width=30%> Reference Number </td><td width=70%>" + Convert.ToString(DrWebInfo["ReferenceNumber"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>From </td><td width=70%>" + Convert.ToString(DrWebInfo["FromDate"]) + "</td></tr>" +
                            "<tr  height=25px><td width=30%>To </td><td width=70%>" + Convert.ToString(DrWebInfo["ToDate"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%> Certificate Title </td><td width=70%>" + Convert.ToString(DrWebInfo["CertificateTitle"]) + "</td></tr>" +
                            "<tr height=25px><td width=30%> Grade or Percentage    </td><td width=70%>" + Convert.ToString(DrWebInfo["GradeorPercentage"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Certificates Attested?  </td><td width=70%>" + Convert.ToString(DrWebInfo["CertificatesAttested"]) + "</td></tr>" +
                            "<tr height=25px><td width=30%>  Certificates Verified?   </td><td width=70%>" + Convert.ToString(DrWebInfo["CertificatesVerified"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%> University Clearence Required? </td><td width=70%>" + Convert.ToString(DrWebInfo["UniversityClearenceRequired"]) + "</td></tr>" +
                           "<tr  height=25px><td width=30%>  Clearence Completed?   </td><td width=70%>" + Convert.ToString(DrWebInfo["ClearenceCompleted"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30% valign='top'>  Remarks  </td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table>");
        return MsbHtml.ToString();
    }

    private string GetEmployeeVisa()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Visa Details</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data
            //<tr height=25px><td width=30%>Plan Type</td><td word-wrap:break-all width=70%>" + Convert.ToString(DrWebInfo["PlanType"]) + "</td>" +
            MsbHtml.Append("<tr height=25px><td width=30%>Employee Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeName"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Visa Number</td><td width=70%>" + Convert.ToString(DrWebInfo["VisaNumber"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Place of issue</td><td width=70%>" + Convert.ToString(DrWebInfo["PlaceOfIssue"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Visa Issue Date</td><td width=70%>" + Convert.ToString(DrWebInfo["VisaIssueDate"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Visa Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["VisaExpiryDate"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Issued Country</td><td width=70%>" + Convert.ToString(DrWebInfo["IssueCountry"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Visa Company </td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Visa Designation </td><td width=70%>" + Convert.ToString(DrWebInfo["Designation"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>UID </td><td width=70%>" + Convert.ToString(DrWebInfo["UID"]) + "</td></tr>" +
           "<tr  height=25px><td width=30%>Visa Type </td><td width=70%>" + Convert.ToString(DrWebInfo["VisaType"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Renewed </td><td width=70%>" + Convert.ToString(DrWebInfo["RenewStatus"]) + "</td></tr>" +
           "<tr  height=25px><td width=30% valign='top'>Remarks</td><td width=70% style='word-break:break-all;' >" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetEmirateHealthCard()
    {
        GetWebformHeaderInfo();

        // Heading
        //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>" + ClsCommonSettings.NationalityCard + "</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //Plan Master data

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td width=30%>Employee Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeFullname"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Card Number</td><td width=70%>" + Convert.ToString(DrWebInfo["CardNumber"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Personal Number</td><td width=70%>" + Convert.ToString(DrWebInfo["IDNumber"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Issue Date</td><td width=70%>" + Convert.ToString(DrWebInfo["IssueDate"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>Expiry Date</td><td width=70%>" + Convert.ToString(DrWebInfo["ExpiryDate"]) + "</td></tr>" +
           "<tr height=25px><td width=30%>Renewed</td><td width=70%>" + Convert.ToString(DrWebInfo["IsRenewed"]) + "</td></tr>" +
           "<tr  bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }
    private string GetExchangeCurrencyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td>" +
                "<tr height=25px><td width=30%>Company Currency</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyCurrency"]) + "</td></tr>");

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#fff2eb";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Currency Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Currency</b></font></td>" +
                       "<td><font color=#FFFFF><b>Exchange Rate</b></font></td><td><font color=#FFFFF><b>Exchange Date</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["Currency"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ExchangeRate"]) + "</td>" +
                           "<td>" + Convert.ToString(drowEmail["ExchangeDate"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }



    private string GetAlertSettingsEmail()
    {
        string strRolename = string.Empty;

        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            MsbHtml.Append("<tr height=25px><td width=30%> Alert Name </td><td width=70%>" + Convert.ToString(DrWebInfo["AlertName"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>Alert type</td><td width=70%>" + Convert.ToString(DrWebInfo["AlertType"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>Operation Type </td><td width=70%>" + Convert.ToString(DrWebInfo["OperationType"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Alert settings Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Role Name/ User Name</b></font></td>" +

                       "<td><font color=#FFFFF><b>Processing Interval</b></font></td><td><font color=#FFFFF><b>Repeat Interval</b></font></td>" +
                       "<td><font color=#FFFFF><b>Is Repeat</b></font></td><td><font color=#FFFFF><b>Is Alert ON</b></font></td>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (!strRolename.Trim().Equals(Convert.ToString(drowEmail["Rolename"]).Trim()))
            {

                MsbHtml.Append("<tr height=25px><td><b>" + Convert.ToString(drowEmail["Rolename"]) + "</b></td>");
                //"<td><b>" + Convert.ToString(drowEmail["RoleProcessingInterval"]) + "</b></td>" +
                //"<td><b>" + Convert.ToString(drowEmail["RoleRepeatInterval"]) + "</b></td>" +
                // "<td><b>" + Convert.ToString(drowEmail["RoleIsRepeat"]) + "</b></td>" +
                //"<td><b>" + Convert.ToString(drowEmail["RoleIsAlertON"]) + "</b></td></tr>");//bgcolor=" + strColor + "

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["username"]) + "</td>" +
               "<td>" + Convert.ToString(drowEmail["userProcessingInterval"]) + "</td>" +
               "<td>" + Convert.ToString(drowEmail["userRepeatInterval"]) + "</td>" +
                "<td>" + Convert.ToString(drowEmail["userIsRepeat"]) + "</td>" +
                "<td>" + Convert.ToString(drowEmail["userIsAlertON"]) + "</td></tr>");
            }
            else
            {
                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["username"]) + "</td>" +
                              "<td>" + Convert.ToString(drowEmail["userProcessingInterval"]) + "</td>" +
                              "<td>" + Convert.ToString(drowEmail["userRepeatInterval"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["userIsRepeat"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["userIsAlertON"]) + "</td></tr>");
            }

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            strRolename = Convert.ToString(drowEmail["Rolename"]);
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");

        return MsbHtml.ToString();
    }



    private string GetCompanySettingsEmail()
    {
        string strConfigurationaItem = string.Empty;

        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Company Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Company Name</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%></td><td width=70%>" + Convert.ToString(DrWebInfo["HPOBox"]) + "</td></tr>" +
               "<tr height=25px><td width=30%></td><td width=70%>" + Convert.ToString(DrWebInfo["PhonePABXNo"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["RoadArea"]) + "</td></tr>" +
               "<tr height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["BlockCity"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%></td><td>" + Convert.ToString(DrWebInfo["WebSite"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Company Settings Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Configuration Item</b></font></td>" +
                       "<td width=50%><font color=#FFFFF><b>Sub Item</b></font></td><td><font color=#FFFFF><b>Configuration Value</b></font></td></tr>");


        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {

            if (!strConfigurationaItem.Trim().Equals(Convert.ToString(drowEmail["ConfigurationItem"]).Trim()))
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ConfigurationItem"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["SubItem"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ConfigurationValue"]) + "</td></tr>");
            }
            else
            {
                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>&nbsp;</td>" +
                               "<td>" + Convert.ToString(drowEmail["SubItem"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ConfigurationValue"]) + "</td></tr>");
            }
            strConfigurationaItem = Convert.ToString(drowEmail["ConfigurationItem"]);
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    }

    private string GetRoleSettingsEmail()
    {
        int intIndex = 0;
        string strFieldName = string.Empty;
        string strPermissions = string.Empty;
        string strCompany = string.Empty, strMenu = string.Empty, strModule = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
            {
                if (intIndex == 0)
                {
                    MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td align=left><font size=2><h2>Role:</h2></font></td><td align=left><font size=2><h2>" +
                        Convert.ToString(drowEmail["RoleName"]) + "</h2></font></td></tr></table>");
                    MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
                    MsbHtml.Append("<tr height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Permission Settings</h2></font></td></tr>");

                    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px>");
                    for (int i = 1; i < 5; i++)
                    {
                        strFieldName = EmailSource.Tables[0].Columns[i].ColumnName;
                        MsbHtml.Append("<td align=left><h2><font color=#FFFFF>" + strFieldName + "</font></h2></td>");
                    }
                    MsbHtml.Append("<td align=left><h2><font color=#FFFFF>Permissions</font></h2></td></tr>");
                }

                if (!strCompany.Trim().Equals(Convert.ToString(drowEmail["Company"]).Trim()))
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=5>" + Convert.ToString(drowEmail["Company"]).Trim() +
                        "</td></tr>");
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }

                if (!(strCompany + strModule).Trim().Equals(Convert.ToString(drowEmail["Company"]).Trim() +
                    Convert.ToString(drowEmail["Module"]).Trim()))
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=1>&nbsp;</td><td colspan=4>" +
                        Convert.ToString(drowEmail["Module"]).Trim() + "</td></tr>");
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }

                if (!(strCompany + strModule + strMenu).Trim().Equals(Convert.ToString(drowEmail["Company"]).Trim() +
                    Convert.ToString(drowEmail["Module"]).Trim() + Convert.ToString(drowEmail["Menu"]).Trim()))
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2>&nbsp;</td><td colspan=2>" +
                        Convert.ToString(drowEmail["Menu"]).Trim() + "</td><td>" + Convert.ToString(drowEmail["MenuPermissions"]).Trim() +
                        "</td></tr>");
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                }

                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=3>&nbsp;</td><td>" +
                    Convert.ToString(drowEmail["Fields"]).Trim() + "</td><td>" + Convert.ToString(drowEmail["FieldPermissions"]).Trim() +
                    "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

                strCompany = Convert.ToString(drowEmail["Company"]);
                strModule = Convert.ToString(drowEmail["Module"]);
                strMenu = Convert.ToString(drowEmail["Menu"]);
                strFieldName = Convert.ToString(drowEmail["Fields"]);
                strPermissions = Convert.ToString(drowEmail["FieldPermissions"]);
                intIndex++;
            }
        }

        MsbHtml.Append("</table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetDocumentEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Expense Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td width=30% >Document Type</td><td width=70% >" + Convert.ToString(drowEmail["DocumentType"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30% >Operation Type</td><td width=70%>" + Convert.ToString(drowEmail["OperationType"]) + "</td></tr>" +
                           "<tr height=25px><td width=30% >Document Number</td><td width=70%>" + Convert.ToString(drowEmail["DocumentNumber"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30% >Issued Date</td><td width=70%>" + Convert.ToString(drowEmail["IssuedDate"]) + "</td></tr>" +
                           "<tr height=25px><td width=30% >Expiry Date</td><td width=70%>" + Convert.ToString(drowEmail["ExpiryDate"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30% >Renewed</td><td width=70%>" + Convert.ToString(drowEmail["IsRenewed"]) + "</td></tr>" +
                           "<tr height=25px><td width=30% >Remarks</td><td width=70% style='word-break:break-all;'>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>"
                           );
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetCompanyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Company Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            if (Convert.ToBoolean(DrWebInfo["CompanyBranchIndicator"]) == false)
            {
                MsbHtml.Append("<tr height=25px><td width=30%> Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["PCompanyName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Employer Code</td><td width=70%>" + Convert.ToString(DrWebInfo["EPID"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Company Industry</td><td>" + Convert.ToString(DrWebInfo["CompanyIndustry"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Company Type</td><td>" + Convert.ToString(DrWebInfo["CompanyType"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Company Or Branch</td><td>" + (Convert.ToBoolean(DrWebInfo["CompanyBranchIndicator"]) != true ? "Company" : "Branch") + "</td></tr>" +
                "<tr  height=25px><td width=30%>Fax</td><td>" + Convert.ToString(DrWebInfo["PABXNumber"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Website</td><td>" + Convert.ToString(DrWebInfo["WebSite"]) + "</td></tr>" +

               "<tr  height=25px><td width=30%>Daily Pay based On</td><td>" + (Convert.ToBoolean(DrWebInfo["DailyPaymentType"]) == true ? "Month" : "Year") + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Working Days</td><td>" + Convert.ToString(DrWebInfo["WorkingDaysInMonth"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>OffDays</td><td>" + Convert.ToString(DrWebInfo["OffDay"]) + "</td></tr>" +
                "<tr height=25px bgcolor=#fff2eb><td width=30%>Unearned Policy</td><td>" + Convert.ToString(DrWebInfo["UnearnedPolicy"]) + "</td></tr>" +

                "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Contact Information</h2></font></td></tr>" +
                "<tr height=25px><td>Primary Email</td><td>" + Convert.ToString(DrWebInfo["PrimaryEmail"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Secondary Email</td><td>" + Convert.ToString(DrWebInfo["SecondaryEmail"]) + "</td></tr>" +
                "<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Postal Address</h2></font></td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>PO Box</td><td>" + Convert.ToString(DrWebInfo["POBox"]) + "</td></tr>" +
                "<tr height=25px><td>Street</td><td>" + Convert.ToString(DrWebInfo["Road"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Area</td><td>" + Convert.ToString(DrWebInfo["Area"]) + "</td></tr>" +
                "<tr height=25px><td>Block</td><td>" + Convert.ToString(DrWebInfo["Block"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>City</td><td>" + Convert.ToString(DrWebInfo["City"]) + "</td></tr>" +
                "<tr height=25px><td>Province</td><td>" + Convert.ToString(DrWebInfo["Province"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Country</td><td>" + Convert.ToString(DrWebInfo["Country"]) + "</td></tr>" +
                "<tr height=25px><td>Other Info</td><td>" + Convert.ToString(DrWebInfo["OtherInfo"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
                "<tr height=25px ><td>Company Start Date</td><td>" + Convert.ToDateTime(DrWebInfo["StartDate"]).ToString("MMM yyyy") + "</td></tr>" +
                "<tr height=25px bgcolor=#fff2eb><td>Financial Year Start</td><td>" + Convert.ToString(DrWebInfo["FinacialYearStartDt"]) + "</td></tr>" +
                "<tr height=25px><td>Book Start Date</td><td>" + Convert.ToString(DrWebInfo["BookStartDate"]) + "</td></tr>");
                strColor = "#ffffff";
            }
            else
            {
                MsbHtml.Append("<tr height=25px><td width=30%>Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["PCompanyName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Branch Name</td><td width=70%>" + Convert.ToString(DrWebInfo["Name"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>Employer Code</td><td width=70%>" + Convert.ToString(DrWebInfo["EPID"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
               "<tr  height=25px><td width=30%>Company Industry</td><td>" + Convert.ToString(DrWebInfo["CompanyIndustry"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Company Type</td><td>" + Convert.ToString(DrWebInfo["CompanyType"]) + "</td></tr>" +
               "<tr  height=25px><td width=30%>Company Or Branch</td><td>" + (Convert.ToBoolean(DrWebInfo["CompanyBranchIndicator"]) != true ? "Company" : "Branch") + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Telephone</td><td>" + Convert.ToString(DrWebInfo["ContactPersonPhone"]) + "</td></tr>" +
               "<tr  height=25px><td width=30%>Fax</td><td>" + Convert.ToString(DrWebInfo["PABXNumber"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Website</td><td>" + Convert.ToString(DrWebInfo["WebSite"]) + "</td></tr>" +

               "<tr  height=25px><td width=30%>Daily Pay based On</td><td>" + (Convert.ToBoolean(DrWebInfo["DailyPaymentType"]) == true ? "Month" : "Year") + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td width=30%>Working Days</td><td>" + Convert.ToString(DrWebInfo["WorkingDaysInMonth"]) + "</td></tr>" +
               "<tr height=25px><td width=30%>OffDays</td><td>" + Convert.ToString(DrWebInfo["OffDay"]) + "</td></tr>" +
               "<tr height=25px bgcolor=#fff2eb><td width=30%>Unearned Policy</td><td>" + Convert.ToString(DrWebInfo["UnearnedPolicy"]) + "</td></tr>" +

               "<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Contact Information</h2></font></td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Primary Email</td><td>" + Convert.ToString(DrWebInfo["PrimaryEmail"]) + "</td></tr>" +
               "<tr  height=25px><td>Secondary Email</td><td>" + Convert.ToString(DrWebInfo["SecondaryEmail"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Postal Address</h2></font></td></tr>" +
               "<tr  height=25px><td>PO Box</td><td>" + Convert.ToString(DrWebInfo["POBox"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Street</td><td>" + Convert.ToString(DrWebInfo["Road"]) + "</td></tr>" +
               "<tr  height=25px><td>Area</td><td>" + Convert.ToString(DrWebInfo["Area"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Block</td><td>" + Convert.ToString(DrWebInfo["Block"]) + "</td></tr>" +
               "<tr  height=25px><td>City</td><td>" + Convert.ToString(DrWebInfo["City"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Province</td><td>" + Convert.ToString(DrWebInfo["Province"]) + "</td></tr>" +
               "<tr  height=25px><td>Country</td><td>" + Convert.ToString(DrWebInfo["Country"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Other Info</td><td>" + Convert.ToString(DrWebInfo["OtherInfo"]) + "</td></tr>" +
               "<tr  height=25px><td>Currency</td><td>" + Convert.ToString(DrWebInfo["Currency"]) + "</td></tr>" +
               "<tr bgcolor=#fff2eb height=25px><td>Financial Year Start Date</td><td>" + Convert.ToString(DrWebInfo["FinacialYearStartDt"]) + "</td></tr>" +
               "<tr height=25px><td>Book Start Date</td><td>" + Convert.ToString(DrWebInfo["BookStartDate"]) + "</td></tr>");
                strColor = "#fff2eb";
            }
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Company Details
        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Bank Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>BankName</b></font></td>" +
                           "<td><font color=#FFFFF><b>Account Number</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["BankName"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["AccountNo"]) + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Bank Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }








    private string GetEmployeeEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%> Employee Number </td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeNumber"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Full Name</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Official Email</td><td>" + Convert.ToString(DrWebInfo["OfficialEmail"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Date of Joining</td><td>" + Convert.ToString(DrWebInfo["doj"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Work Status</td><td width=70%>" + Convert.ToString(DrWebInfo["workstatusdescription"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Working At</td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
                "<tr  height=25px><td width=30%>Department</td><td>" + Convert.ToString(DrWebInfo["dept"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Designation</td><td>" + Convert.ToString(DrWebInfo["Designation"]) + "</td></tr>" +
                "<tr  height=25px><td width=30%>Work Location</td><td>" + Convert.ToString(DrWebInfo["LocationName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb  height=25px><td width=30%>Reporting To</td><td>" + Convert.ToString(DrWebInfo["ReportingTo"]) + "</td></tr>" +
                "<tr  height=25px><td width=30%>Work Policy</td><td>" + Convert.ToString(DrWebInfo["WorkPolicy"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb  height=25px><td width=30%>Leave Policy</td><td>" + Convert.ToString(DrWebInfo["LeavePolicy"]) + "</td></tr>" +
                "<tr  height=25px><td width=30%>Vacation Policy</td><td>" + Convert.ToString(DrWebInfo["VacationPolicy"]) + "</td></tr>" );
            if (Convert.ToString(DrWebInfo["TransferDate"])!="")
               MsbHtml.Append("<tr  height=25px><td width=30%>Transfer Date</td><td>" + Convert.ToString(DrWebInfo["TransferDate"]) + "</td></tr>");
               MsbHtml.Append( "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Bank Identity</h2></font></td></tr>" +
                "<tr height=25px><td width=30%>Transaction Type</td><td>" + Convert.ToString(DrWebInfo["transactiontype"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Employer BankName</td><td>" + Convert.ToString(DrWebInfo["employerbankname"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Employee BankName</td><td>" + Convert.ToString(DrWebInfo["employeebankname"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Employer Bank Acc</td><td>" + Convert.ToString(DrWebInfo["Employerbankacc"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Account Number</td><td>" + Convert.ToString(DrWebInfo["AccountNumber"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Personal Information</h2></font></td></tr>" +
                "<tr  height=25px><td width=30%>Gender</td><td>" + Convert.ToString(DrWebInfo["Gender"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td width=30%>Date Of Birth</td><td>" + Convert.ToString(DrWebInfo["dob"]) + "</td></tr>" +

                "<tr height=25px><td width=30%>Nationality</td><td>" + Convert.ToString(DrWebInfo["NationalityDescription"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Country</td><td>" + Convert.ToString(DrWebInfo["CountryName"]) + "</td></tr>" +

                "<tr  height=25px><td width=30%>Religion</td><td>" + Convert.ToString(DrWebInfo["Religion"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td>Blood Group</td><td>" + Convert.ToString(DrWebInfo["BloodGroup"]) + "</td></tr>" +
                "<tr  height=25px><td width=30%>Languages known</td><td>" + Convert.ToString(DrWebInfo["LanguagesKnown"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td width=30%>Identification Marks</td><td>" + Convert.ToString(DrWebInfo["IdentificationMarks"]) + "</td></tr>" +
                "<tr  height=25px><td>Mother Tongue</td><td>" + Convert.ToString(DrWebInfo["mothertongue"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td>Spouse Name</td><td>" + Convert.ToString(DrWebInfo["SpouseName"]) + "</td></tr>" +
                "<tr  height=25px><td>Father's Name</td><td>" + Convert.ToString(DrWebInfo["FathersName"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td>Mother's Name</td><td>" + Convert.ToString(DrWebInfo["MothersName"]) + "</td></tr>" +
                "<tr  height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Contacts Info</h2></font></td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td width=25%>Permanent Address</td><td style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["LocalAddress"]) + "</td></tr>" +
                "<tr  height=25px><td>Phone</td><td>" + Convert.ToString(DrWebInfo["LocalPhone"]) + "</td></tr>" +
                "<tr  bgcolor=#fff2eb height=25px><td>Mobile</td><td>" + Convert.ToString(DrWebInfo["LocalMobile"]) + "</td></tr>" +
                "<tr  height=25px><td>Email</td><td>" + Convert.ToString(DrWebInfo["LocalEmailID"]) + "</td></tr>" +

                "<tr bgcolor=#fff2eb height=25px><td width=25%>Emergency Address</td><td style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["EmergencyAddress"]) + "</td></tr>" +
                "<tr height=25px><td width=25%>Emergency Phone</td><td style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["EmergencyPhone"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=25%>Local Address</td><td style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["LocalAddressG"]) + "</td></tr>" +
                "<tr height=25px><td width=25%>Local Phone</td><td style='word-break:break-all;'>" + Convert.ToString(DrWebInfo["LocalPhoneG"]) + "</td></tr>" +

                "<tr  bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Other Informations</h2></font></td></tr>" +
                "<tr  height=25px><td width=25%>Other Info</td><td style='word-break:break-all;' >" + Convert.ToString(DrWebInfo["Notes"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }



    private string GetNormalEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td>Discount Name</td><td>" + Convert.ToString(drowEmail["DiscountShortName"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Description</td><td>" + Convert.ToString(drowEmail["Description"]) + "</td></tr>" +
                     "<tr height=25px><td>Discount Mode</td><td>" + Convert.ToString(drowEmail["DiscountMode"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Discount</td><td>" + Convert.ToString(drowEmail["DiscountForAmountDetails"]) + "</td></tr>" +
                     "<tr height=25px><td>Discount Type</td><td>" + Convert.ToString(drowEmail["IsSaleType"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Value</td><td>" + Convert.ToString(drowEmail["value"]) + "</td></tr>" +
                     "<tr height=25px><td>Mode</td><td>" + Convert.ToString(drowEmail["PercentOrAmount"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Period Applicable</td><td>" + Convert.ToString(drowEmail["PeriodApplicable"]) + "</td></tr>" +
                     "<tr height=25px><td>Period From</td><td>" + ((Convert.ToBoolean(drowEmail["PeriodApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["PeriodFrom"]) : "") + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Period To</td><td>" + ((Convert.ToBoolean(drowEmail["PeriodApplicable"].ToString()) == true) ? Convert.ToString(drowEmail["PeriodTo"]) : "") + "</td></tr>" +
                     "<tr height=25px><td>Slab Applicable</td><td>" + Convert.ToString(drowEmail["SlabApplicable"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Applicable For Total Amount</td><td>" + Convert.ToString(drowEmail["DiscountForAmount"]) + "</td></tr>" +
                     "<tr height=25px><td>Minimum Amount</td><td>" + Convert.ToString(drowEmail["MinSlab"]) + "</td></tr>" +
                     "<tr bgcolor=#fff2eb height=25px><td>Currency</td><td>" + Convert.ToString(drowEmail["Currency"]) + "</td></tr>" +
                      "<tr height=25px><td>Active</td><td>" + Convert.ToString(drowEmail["Active"]) + "</td></tr>"
                             );
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }





    public string GetWebFormData(string sCaption, int iFormType, long iID)
    {
        MstrCaption = sCaption;
        MintRecordID = iID;
        MintFormType = iFormType;

        string sHtml = "<html><head></head><body oncontextmenu=return false ondragstart=return false onselectstart=return false><center><img src='" + imgPath + "' border=none alt=No Information Found></img></center></body></html>";
        SqlDataReader DrWebInfo = GetWebformInfo();
        switch (iFormType)
        {


           
          
           
          
           
          
            
            case (int)EmailFormID.RoleSettings:
                sHtml = GetWebformRoleSettings(DrWebInfo);
                break;
            
            case (int)EmailFormID.Bank:
                sHtml = GetWebformCommonInformation(DrWebInfo);
                break;
            
        }
        DrWebInfo.Close();
        return sHtml;
    }

    private SqlDataReader GetWebformInfo()
    {
        ArrayList arrParameters = new ArrayList();
        SqlDataReader sdrWebInfo;
        if (MintFormType == (int)EmailFormID.RoleSettings)
        {
            arrParameters.Add(new SqlParameter("@RoleID", MintRecordID));
            sdrWebInfo = MobjConnection.ExecutesReader("STReportRoleSettings", arrParameters, CommandBehavior.CloseConnection);
        }
        //else if (MintFormType == (int)EmailFormID.Payments)
        //{
        //    arrParameters.Add(new SqlParameter("PaymentID", MintRecordID));
        //    sdrWebInfo = MobjConnection.ExecutesReader("STPaymentTransaction", arrParameters, CommandBehavior.CloseConnection);
        //}
        else
        {
            arrParameters.Add(new SqlParameter("@FormType", MintFormType));
            arrParameters.Add(new SqlParameter("@ID", MintRecordID));
            sdrWebInfo = MobjConnection.ExecutesReader("STGetWebformInformation", arrParameters, CommandBehavior.CloseConnection);
        }

        return sdrWebInfo;
    }

    private string GetWebformCommonInformation(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        if (DrWebInfo.Read())
        {
            GetWebformHeaderInfo();

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
            MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(iIndex);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(iIndex).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
                iIndex++;
            }
            MsbHtml.Append("</table></td></tr></table></div></body></html>");
        }
        return MsbHtml.ToString();
    }

    private string GetWebformInformationInGrid(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        if (DrWebInfo.HasRows)
            GetWebformGridHeaderInfo();

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                }
                MsbHtml.Append("</tr>");
            }

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td>");
            }
            if (strColor == "#fff2eb")
                strColor = "#ffffff";
            else
                strColor = "#fff2eb";

            MsbHtml.Append("</tr>");
            iIndex++;
        }
        MsbHtml.Append("</table></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    public string GetUsedFormsInformation(string sCaption, int iPrimeryId, string sCondition, string sFileds)
    {
        ArrayList fmParameters = new ArrayList();
        fmParameters.Add(new SqlParameter("@PrimeryID", iPrimeryId));
        fmParameters.Add(new SqlParameter("@Condition", sCondition));
        fmParameters.Add(new SqlParameter("@FieldName", sFileds));
        DataTable dtForms = MobjConnection.FillDataSet("STCheckIDExists", fmParameters).Tables[0];

        StringBuilder MsbHtml = new StringBuilder();

        MsbHtml.Append("<html><head>" +
                       "<style>" +
                      "td {font-family:arial;font-size:12px;border:none;color:black;padding:5px}" +
                      "a.link{font-family:arial;color:Blue;text-decoration: none}" +
                      "a.link:hover{font-family:arial;color: #3399CC;text-decoration: none}" +
                      "</style></head><body bgcolor=whiteSmoke topmargin=0 leftmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>" +
                      "<tr bgcolor=#3a3a53><td colspan=2 align=left height=30px><font size=1 color=white><b>" + sCaption + "</b></font></td></tr>");

        MsbHtml.Append("<tr bgcolor=#fff2eb height=15px><td colspan=2 align=left><font size=1 color=#3a3a53><h2>Forms</h2></font></td></tr>");
        strColor = "#ffaaee";
        if (dtForms.Rows.Count > 0)
        {
            if (Convert.ToString(dtForms.Rows[0]["FormName"]) != "0")
            {
                for (int i = 0; i < dtForms.Rows.Count; i++)
                {
                    MsbHtml.Append("<tr bgcolor=" + strColor + "><td width=70% align=left>" + Convert.ToString(dtForms.Rows[i]["FormName"]) + "</td></tr>");
                    if (strColor == "#ffaaee")
                        strColor = "#ffffff";
                    else
                        strColor = "#ffaaee";
                }
            }
            else
                MsbHtml.Append("<tr><td width=70% align=left> No Information found</td></tr>");
        }
        else
        {
            MsbHtml.Append("<tr><td width=70% align=left> No Information found</td></tr>");
        }

        MsbHtml.Append("</table></div></Body></HTML>");

        return MsbHtml.ToString(); ;
    }

    private string GetWebformCompanySettings(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sConfigItem = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
            }
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=3 align=left><font size=2 color=#3a3a53><h2>Company Settings</h2></font></td></tr>");
        strColor = "#ffffff";
        while (DrWebInfo.Read())
        {
            if (!sConfigItem.Trim().Equals(Convert.ToString(DrWebInfo["ConfigurationItem"]).Trim()))
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=3>" + Convert.ToString(DrWebInfo["ConfigurationItem"]).Trim() + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }

            sConfigItem = Convert.ToString(DrWebInfo["ConfigurationItem"]);

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td width=10%>&nbsp;</td>");
            for (int i = 1; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td>");
            }
            if (strColor == "#fff2eb")
                strColor = "#ffffff";
            else
                strColor = "#fff2eb";
            MsbHtml.Append("</tr>");
            iIndex++;
        }
        MsbHtml.Append("</table></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetWebformRoleSettings(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sPermissions = string.Empty;
        string sCompany = string.Empty, sMenu = string.Empty, sModule = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td align=left><font size=2><h2>Role:</h2></font></td><td align=left><font size=2><h2>" +
                    Convert.ToString(DrWebInfo["RoleName"]) + "</h2></font></td></tr></table>");
                MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
                MsbHtml.Append("<tr height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Permission Settings</h2></font></td></tr>");

                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px>");
                for (int i = 1; i < 5; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                }
                MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Permissions</h2></font></td></tr>");
            }

            if (!sCompany.Trim().Equals(Convert.ToString(DrWebInfo["Company"]).Trim()))
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=5>" + Convert.ToString(DrWebInfo["Company"]).Trim() + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }

            if (!(sCompany + sModule).Trim().Equals(Convert.ToString(DrWebInfo["Company"]).Trim() + Convert.ToString(DrWebInfo["Module"]).Trim()))
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=1>&nbsp;</td><td colspan=4>" + Convert.ToString(DrWebInfo["Module"]).Trim() + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }

            if (!sMenu.Trim().Equals(Convert.ToString(DrWebInfo["Menu"]).Trim()))
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2>&nbsp;</td><td colspan=2>" + Convert.ToString(DrWebInfo["Menu"]).Trim() + "</td><td>" + Convert.ToString(DrWebInfo["MenuPermissions"]).Trim() + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=3>&nbsp;</td><td>" + Convert.ToString(DrWebInfo["Fields"]).Trim() + "</td><td>" + Convert.ToString(DrWebInfo["FieldPermissions"]).Trim() + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            sCompany = Convert.ToString(DrWebInfo["Company"]);
            sModule = Convert.ToString(DrWebInfo["Module"]);
            sMenu = Convert.ToString(DrWebInfo["Menu"]);
            sFieldName = Convert.ToString(DrWebInfo["Fields"]);
            sPermissions = Convert.ToString(DrWebInfo["FieldPermissions"]);
            iIndex++;
        }
        MsbHtml.Append("</table></div></body></html>");
        DrWebInfo.Close();
        return MsbHtml.ToString();
    }

    private string GetWebformWithCompanyInfoInGrid(SqlDataReader DrWebInfo, string sHeader)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr></table>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
            }
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + DrWebInfo.VisibleFieldCount.ToString() + " align=left><font size=2 color=#3a3a53><h2>" + sHeader + "</h2></font></td></tr>");

        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                MsbHtml.Append("<tr height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                }
                MsbHtml.Append("</tr>");
            }

            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
            }
            MsbHtml.Append("</tr>");
            iIndex++;
        }
        MsbHtml.Append("</table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetWebformChartofAccounts(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sConfigItem = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr></table>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
            }
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Chart Of Accounts</h2></font></td></tr>");
        strColor = "#ffffff";

        while (DrWebInfo.Read())
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=2>" + (Convert.ToInt32(DrWebInfo["AccountType"]) == 0 ? "<h2>" : "") + Convert.ToString(DrWebInfo["Description"]) + (Convert.ToInt32(DrWebInfo["AccountType"]) == 0 ? "</h2>" : "") + "</td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            iIndex++;
        }
        MsbHtml.Append("</table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetWebformVendorHistory(string sAssociateType)
    {
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%><tr bgcolor=#fff2eb height=25px>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Company</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sAssociateType + "</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Date</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Invoice No</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Amount</h2></font></td>");
        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>Status</h2></font></td></tr>");

        foreach (DataRow Row in datHistory.Rows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");

            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["Name"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["VendorName"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["InvoiceDate"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["InvoiceNo"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["InvoiceAmount"]) + "</td>");
            MsbHtml.Append("<td align=left>" + Convert.ToString(Row["Status"]) + "</td></tr>");

            if (strColor == "#fff2eb")
                strColor = "#ffffff";
            else
                strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetWebformMasterDetails(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        if (DrWebInfo.Read())
        {
            GetWebformGridHeaderInfo();

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        }


        DrWebInfo.NextResult();
        if (DrWebInfo.HasRows)
        {
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=" + DrWebInfo.FieldCount + " align=left><font size=2 color=#3a3a53><h2>" + MstrCaption + "</h2></font></td></tr>");

            while (DrWebInfo.Read())
            {
                if (iIndex == 0)
                {
                    if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                    MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
                    for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                    {
                        sFieldName = DrWebInfo.GetName(i);
                        MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                    }
                    MsbHtml.Append("</tr>");
                }

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td>" + Convert.ToString(DrWebInfo[sFieldName]).Trim() + "</td>");

                }
                MsbHtml.Append("</tr>");
                iIndex++;
            }
            MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        }
        return MsbHtml.ToString();
    }


    private void GetWebformHeaderInfo()
    {
        MsbHtml = new StringBuilder();
        MsbHtml.Append("<html><head><style type=text/css> <!-- body,td,th { font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000; }" +
            " h1{ font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#FFF; padding:0px; margin:0px; } " +
            " h2{ font-family:Arial, Helvetica, sans-serif; font-size:13px; color: #000; padding:0 0 0 0px; margin:0px; } " +
            " body { margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; } " +
            " td { padding-left:10px; } --> </style></head><body bgcolor=White topmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>");
        MsbHtml.Append("<table width=100% border=0 cellspacing=0 cellpadding=0><tr><td style=padding:10px;><table width=100% border=0 cellspacing=0 cellpadding=0>" +
            " <tr><td height=34 colspan=2 bgcolor=#6d6e71><h1>" + MstrCaption + "</h1></td></tr><tr><td height=5 colspan=2>&nbsp;</td></tr>");
    }

    private string GetWebformDiscountNormal(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        strColor = "#ffffff";

        if (DrWebInfo.Read())
        {
            GetWebformHeaderInfo();

            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
            MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");

            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(iIndex);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(iIndex).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb")
                    strColor = "#ffffff";
                else
                    strColor = "#fff2eb";
                iIndex++;
            }
            MsbHtml.Append("</table></td></tr></table></div></body></html>");
        }
        return MsbHtml.ToString();
    }
    private string GetWebformDiscountitemwise(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sConfigItem = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Itemwise Details</h2></font></td></tr>");

        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                }
                MsbHtml.Append("</tr>");
            }

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td>" + Convert.ToString(DrWebInfo[sFieldName]).Trim() + "</td>");

            }
            MsbHtml.Append("</tr>");
            iIndex++;
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();

    }

    private string GetWebformDiscountcustomerwise(SqlDataReader DrWebInfo)
    {
        int iIndex = 0;
        string sFieldName = string.Empty;
        string sConfigItem = string.Empty;
        strColor = "#ffffff";

        GetWebformGridHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (DrWebInfo.Read())
        {
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<tr height=25px bgcolor=" + strColor + "><td width=30% align=left> " + DrWebInfo.GetName(i).ToString() + " </td><td width=70% align=left>" + Convert.ToString(DrWebInfo[sFieldName]) + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
        DrWebInfo.NextResult();
        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td colspan=5 align=left><font size=2 color=#3a3a53><h2>Customerwise Details</h2></font></td></tr>");

        while (DrWebInfo.Read())
        {
            if (iIndex == 0)
            {
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
                for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
                {
                    sFieldName = DrWebInfo.GetName(i);
                    MsbHtml.Append("<td align=left><font color=#3a3a53><h2>" + sFieldName + "</h2></font></td>");
                }
                MsbHtml.Append("</tr>");
            }

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px>");
            for (int i = 0; i < DrWebInfo.VisibleFieldCount; i++)
            {
                sFieldName = DrWebInfo.GetName(i);
                MsbHtml.Append("<td>" + Convert.ToString(DrWebInfo[sFieldName]).Trim() + "</td>");

            }
            MsbHtml.Append("</tr>");
            iIndex++;
        }
        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();

    }



    private string GetWebformWarehouseInformation(SqlDataReader DrWebInfo)
    {
        GetWebformHeaderInfo();

        if (DrWebInfo.Read())
        {
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
            MsbHtml.Append("<tr height=25px><td width=30%> Name </td><td width=70%>" + Convert.ToString(DrWebInfo["WarehouseName"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>Short Name</td><td width=70%>" + Convert.ToString(DrWebInfo["ShortName"]) + "</td></tr>" +
              "<tr height=25px><td width=30%>Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["Company"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%> Address1 </td><td>" + Convert.ToString(DrWebInfo["Address1"]) + "</td></tr>" +
              "<tr height=25px><td width=30%> Address2 </td><td>" + Convert.ToString(DrWebInfo["Address2"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>Zip Code</td><td>" + Convert.ToString(DrWebInfo["ZipCode"]) + "</td></tr>" +
              "<tr height=25px><td width=30%>City</td><td>" + Convert.ToString(DrWebInfo["CityState"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>Country</td><td>" + Convert.ToString(DrWebInfo["Country"]) + "</td></tr>" +
              "<tr height=25px><td width=30%>Description</td><td>" + Convert.ToString(DrWebInfo["Description"]) + "</td></tr>" +
              "<tr bgcolor=#fff2eb height=25px><td width=30%>In Charge</td><td>" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr>" +
              "<tr height=25px><td width=30%>Phone No</td><td>" + Convert.ToString(DrWebInfo["Phone"]) + "</td></tr>");
            MsbHtml.Append("</table>");
        }
        return MsbHtml.ToString();
    }




    private void GetWebformGridHeaderInfo()
    {
        MsbHtml = new StringBuilder();
        MsbHtml.Append("<html><head><style type=text/css> <!-- body,td,th { font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000; }" +
            " h1{ font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#FFF; padding:0px; margin:0px; } " +
            " h2{ font-family:Arial, Helvetica, sans-serif; font-size:13px; color: #000; padding:0 0 0 0px; margin:0px; } " +
            " body { margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; } " +
            " td { padding-left:10px; } --> </style></head><body bgcolor=White topmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>");
        MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%><tr><td style=padding:10px;><table border=0 cellspading=0 cellspacing=0 width=100%>" +
                      "<tr bgcolor=#6d6e71><td align=left height=34px colspan=2><h1>" + MstrCaption + "</h1></td></tr><tr><td height=5 colspan=2>&nbsp;</td></tr>");
    }


    private string GetProjectEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%> Company Name </td><td width=70%>" + Convert.ToString(DrWebInfo["CompanyName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Project Code</td><td width=70%>" + Convert.ToString(DrWebInfo["ProjectCode"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Description</td><td width=70%>" + Convert.ToString(DrWebInfo["Description"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Project Manager</td><td>" + Convert.ToString(DrWebInfo["ProjectManager"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Start Date</td><td>" + Convert.ToString(DrWebInfo["StartDate"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>End Date</td><td>" + Convert.ToString(DrWebInfo["EndDate"]) + "</td></tr>" +
                "<tr height=25px><td width=30%>Estimated Amount</td><td>" + Convert.ToString(DrWebInfo["EstimatedAmount"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }

    private string GetBankEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>Bank Name</td><td>" + Convert.ToString(drowEmail["BankName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Branch Name</td><td>" + Convert.ToString(drowEmail["BankBranchName"]) + "</td></tr>" +
                           "<tr height=25px><td>Bank Code</td><td>" + Convert.ToString(drowEmail["BankCode"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Address</td><td>" + Convert.ToString(drowEmail["Address"]) + "</td></tr>" +
                           "<tr height=25px><td>Telephone</td><td>" + Convert.ToString(drowEmail["Telephone"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Fax</td><td>" + Convert.ToString(drowEmail["Fax"]) + "</td></tr>" +
                           "<tr height=25px><td>Country</td><td>" + Convert.ToString(drowEmail["Country"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Email</td><td>" + Convert.ToString(drowEmail["Email"]) + "</td></tr>" +
                           "<tr height=25px><td>Website</td><td>" + Convert.ToString(drowEmail["Website"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>IBAN Code</td><td>" + Convert.ToString(drowEmail["BankRoutingCode"]) + "</td></tr>"
                           );
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
    private string GetSalaryStructureEmail()
    {
        decimal decAdditionAmount = 0, decDeductionAmount = 0, decRemunerationAmount = 0;
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Company Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr height=25px><td width=30%> Employee Name </td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeNumber"]) + "-" + Convert.ToString(DrWebInfo["EmployeeFullName"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Currency Name</td><td>" + Convert.ToString(DrWebInfo["CurrencyName"]) + "</td></tr>" +
            "<tr height=25px><td width=30%>Payment Mode</td><td width=70%>" + Convert.ToString(DrWebInfo["PaymentClassification"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Payment Calculation</td><td>" + Convert.ToString(DrWebInfo["PayCalculationType"]) + "</td></tr>" +
            "<tr  height=25px><td width=30%>Salary Process Day</td><td>" + Convert.ToString(DrWebInfo["SalaryDay"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Settlement Policy</td><td>" + Convert.ToString(DrWebInfo["SettlementPolicy"]) + "</td></tr>" +
            "<tr  height=25px><td width=30%>Absent Policy</td><td>" + Convert.ToString(DrWebInfo["AbsentPolicy"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td width=30%>OT Policy</td><td>" + Convert.ToString(DrWebInfo["OTPolicy"]) + "</td></tr>" +
            "<tr  height=25px><td width=30%>Encash Policy</td><td>" + Convert.ToString(DrWebInfo["EncashPolicy"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Holiday Policy</td><td>" + Convert.ToString(DrWebInfo["HolidayPolicy"]) + "</td></tr>" +
            "<tr  height=25px><td width=30%>Remarks</td><td style='word-break:break-all'>" + Convert.ToString(DrWebInfo["Remarks"]) + "</td></tr>");
            strColor = "#ffffff";
        }
        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // salary structure Details
        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Addition Deduction Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Addition/Deduction</b></font></td>" +
                           "<td><font color=#FFFFF><b>Deduction Policy</b></font></td></font>" + "<td><font color=#FFFFF><b>Category</b></font></td></font>" + "<td style='text-align:right'><font color=#FFFFF><b>Amount</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                if (drowEmail["IsAddtion"].ToBoolean())
                {
                    decAdditionAmount += drowEmail["Amount"].ToDecimal();
                }
                else
                {
                    decDeductionAmount += drowEmail["Amount"].ToDecimal();
                }

                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td><font color=black><b>" + Convert.ToString(drowEmail["AdditionDeduction"]) + "</b></font></td>" +
                             "<td><font color=black><b>" + Convert.ToString(drowEmail["PolicyName"]) + "</td>" + " <td><font color=black><b>" + Convert.ToString(drowEmail["AmountCategory"]) + "</td>" + "<td style='text-align:right'>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td><font color=black><b>Total Additions : " + Convert.ToString(decAdditionAmount) + "</b></font></td>" +
                             "<td><font color=black><b>Total Deductions : " + Convert.ToString(decDeductionAmount) + "</b></font></td>" + "<td style='text-align:right'><font color=black><b>Net Amount  : " + Convert.ToString(decAdditionAmount - decDeductionAmount) + "</b></font></td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Addition Deduction Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }
        MsbHtml.Append("</table>");

        // Other remuneration
        if (EmailSource.Tables[2].Rows.Count > 0)
        {
            MsbHtml.Append("<table border=0 cellspading=0 cellspacing=0 width=100%>");
            strColor = "#ffffff";
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Other Remunerations</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Particular</b></font></td>" +
                           "<td style='text-align:right'><font color=#FFFFF><b>Amount</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                decRemunerationAmount += drowEmail["Amount"].ToDecimal();
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td><font color=black><b>" + Convert.ToString(drowEmail["Particular"]) + "</b></font></td>" +
                             "<td style='text-align:right'><font color=black><b>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");


                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
            MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td style='text-align:right'><font color=black><b>Total :</b></font></td>" +
                             "<td style='text-align:right'><font color=black><b>" + Convert.ToString(decRemunerationAmount) + "</b></font></td></tr>");
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }
    private string GetShiftPolicyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td width=30%>Shift Name</td><td width=70%>" + Convert.ToString(drowEmail["Shiftname"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>From Time</td><td width=70%>" + Convert.ToString(drowEmail["Fromtime"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>To Time</td><td width=70%>" + Convert.ToString(drowEmail["Totime"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Shift Type</td><td width=70%>" + Convert.ToString(drowEmail["Shifttype"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>Duration</td><td width=70%>" + Convert.ToString(drowEmail["Duration"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Min WorkingHours</td><td width=70%>" + Convert.ToString(drowEmail["MinWorkingHours"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>Allowed BreakTime</td><td width=70%>" + Convert.ToString(drowEmail["AllowedBreakTime"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Late After</td><td width=70%>" + Convert.ToString(drowEmail["LateAfter"]) + "</td></tr>" +
                           "<tr height=25px><td width=30%>Early Before</td><td width=70%>" + Convert.ToString(drowEmail["EarlyBefore"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td width=30%>Buffer</td><td width=70%>" + Convert.ToString(drowEmail["Buffer"]) + "</td></tr>"
                           );
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
    private string GetWorkPolicyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Company Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];


            MsbHtml.Append("<tr height=25px><td width=30%> Policy Name </td><td width=70%>" + Convert.ToString(DrWebInfo["PolicyName"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Shift</td><td width=70%>" + Convert.ToString(DrWebInfo["OffDayshift"]) + "</td></tr>");

            strColor = "#ffffff";

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Work Policy Details
        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Working Days</b></font></td>" +
                           "<td><font color=#FFFFF><b>Shift</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["DayName"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["ShiftName"]) + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }
        if (EmailSource.Tables[2].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2> Policy Consequences</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Consequence</b></font></td>" +
                           "<td><font color=#FFFFF><b>LOP</b></font></td></font><td><font color=#FFFFF><b>Casual</b></font></td></font><td><font color=#FFFFF><b>Amount</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["Conseq"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["LOP"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["Casual"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["Amount"]) + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>WorkPolicy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }
    private string GetVacationPolicyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // vacation master
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];


            MsbHtml.Append("<tr height=25px><td width=30%> Policy Name </td><td width=70%>" + Convert.ToString(DrWebInfo["PolicyName"]) + "</td></tr>" +
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Calculation BasedOn</td><td width=70%>" + Convert.ToString(DrWebInfo["BasedOn"]) + "</td></tr>"+
            "<tr height=25px><td width=30%> OnTimeRejoinBenefit </td><td width=70%>" + Convert.ToString(DrWebInfo["OnTimeRejoinBenefit"]) + "</td></tr>"
            );

            strColor = "#ffffff";

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // vacation Policy Details
       
        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2> Policy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Parameter</b></font></td>" +
                           "<td><font color=#FFFFF><b>No Of Months</b></font></td><td><font color=#FFFFF><b>Leave/Payable Days</b></font></td><td><font color=#FFFFF><b>Tickets</b></font></td><td><font color=#FFFFF><b>TicketAmount</b></font></td></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["ParameterName"]) + "</td>" +
                               "<td>" + Convert.ToString(drowEmail["NoOfMonths"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["PayableDays"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["Tickets"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["TicketAmount"]) + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }

        //else
        //{
        //    MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>WorkPolicy Details</h2></font></td></tr>");
        //    MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        //}

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }




    private string GetWorkLocation()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>Name</td><td>" + Convert.ToString(drowEmail["LocationName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Company</td><td>" + Convert.ToString(drowEmail["CompanyName"]) + "</td></tr>" +
                           "<tr height=25px><td>Address</td><td>" + Convert.ToString(drowEmail["Address"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Phone</td><td>" + Convert.ToString(drowEmail["Phone"]) + "</td></tr>" +
                           "<tr height=25px><td>Email</td><td>" + Convert.ToString(drowEmail["Email"]) + "</td></tr>"
                           );
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
    private string GetPayment(long ID)
    {
        String htmStr = "<html><head><style>" +
                       "td {font-family:arial;font-size:12px;border:none;color:black;padding:5px}" +
                       "a.link{font-family:arial;color:Blue;text-decoration: none}" +
                       "a.link:hover{font-family:arial;color: #3399CC;text-decoration: none}" +
                       "</style></head><body bgcolor=White topmargin=0 oncontextmenu=return false ondragstart=return false onselectstart=return false><div align=center>" +
                       "<table border=0 cellspading=0 cellspacing=0 width=100%";

        Boolean blnFlag = false;
        MintRecordID = ID;
        //Try
        String TSQL = "";
        int intCompanyID = 0;

        SqlDataReader sdrValues;

        TSQL = "SELECT CompanyID FROM PayEmployeePayment WHERE PaymentID = " + MintRecordID + "";
        sdrValues = MobjConnection.ExecutesReader(TSQL);

        if (sdrValues.Read())
        {
            intCompanyID = Convert.ToInt32(sdrValues["CompanyId"]);
        }
        sdrValues.Close();

        mFlag = false;

        ArrayList paramCompany = new ArrayList();
        SqlDataReader sdrCompany;

        paramCompany.Add(new SqlParameter("@ids", intCompanyID));
        sdrCompany = MobjConnection.ExecutesReader("spPayGaReportCompanyForm", paramCompany, CommandBehavior.CloseConnection);

        if (sdrCompany.Read())
        {
            String htmMain = "<table border='0' cellspading='0' cellspacing='0' width='100%'>" +
                              "<tr>" +
                                  "<td align='center'><font size='2'><b>" + Convert.ToString(sdrCompany["CompanyName"]) + "</b></font></td>" +
                              "</tr>" +
                              "<tr>" +
                                  "<td align='center'> " + Convert.ToString(sdrCompany["FaxNumber"]) + " </td>" +
                              "</tr>" +
                              "<tr>" +
                                  "<td align='center'>" + Convert.ToString(sdrCompany["RoadArea"]) + "</td>" +
                              "</tr>" +
                             " <tr>" +
                                "  <td align='center'>" + Convert.ToString(sdrCompany["BlockCity"]) + "</td>" +
                              "</tr>" +
                         " </table>";
            htmStr += htmMain.ToString();
        }
        sdrCompany.Close();
        sdrCompany = null;

        ArrayList paramSalarySlipMain = new ArrayList();
        SqlDataReader sdrSalarySlipMain;
        String strDetailBottom = "";
        //String strDetailsTable = "";

        paramSalarySlipMain.Add(new SqlParameter("@PayID", MintRecordID));
        paramSalarySlipMain.Add(new SqlParameter("@CompanyPayFlag", 0));
        paramSalarySlipMain.Add(new SqlParameter("@DivisionID", 0));
        sdrSalarySlipMain = MobjConnection.ExecutesReader("spPayEmployeePaymentSalarySlipMaster", paramSalarySlipMain, CommandBehavior.CloseConnection);

        while (sdrSalarySlipMain.Read())
        {
            if (blnFlag == false)
            {
                String htmHead = "<table border='0' cellspading='0' cellspacing='0' width='100%'>" +
                                  "<tr>" +
                                      " <td style='border:solid 1 black' align='center'><font size='3'><b>" + Convert.ToString(sdrSalarySlipMain["Head"]) + "</b></font></td>" +
                                  "</tr>" +
                              "</table>";

                htmStr += htmHead.ToString();

                String htmMain = "<table border='0' cellspading='0' cellspacing='0' width='100%'>" +
                                  "<tr height='25px'>" +
                                      "<td style='border-left:solid 1 black' width='100px' align='left'>Employee Name  : </td>" +
                                      "<td style='border-right:solid 1 black' colspan='3' align='left'>" + Convert.ToString(sdrSalarySlipMain["EmployeeName"]) + "</td>" +
                                  "</tr>" +
                                  "<tr height='25px'>" +
                                      "<td style='border-left:solid 1 black' width='100px' align='left'>Employee Code  : </td>" +
                                      "<td style='border-right:solid 1 black' colspan='3' align='left'>" + Convert.ToString(sdrSalarySlipMain["EmployeeNo"]) + "</td>" +
                                  "</tr>" +
                                  "<tr height='25px'>" +
                                      "<td style='border-left:solid 1 black' width='100px' align='left'>Designation    : </td>" +
                                      "<td align='left'>" + Convert.ToString(sdrSalarySlipMain["Designation"]) + " </td>" +
                                      "<td width='100px' align='left'>Payment Mode   : </td>" +
                                      "<td style='border-right:solid 1 black' align='left'>" + Convert.ToString(sdrSalarySlipMain["WorkingMode"]) + "</td>" +
                                  "</tr>" +
                                  "<tr height='25px'>" +
                                      "<td style='border-left:solid 1 black' width='100px' align='left'>Bank Name      : </td>" +
                                      "<td align='left'>" + Convert.ToString(sdrSalarySlipMain["EmpBank"]) + "</td>" +
                                      "<td width='100px' align='left'>Account No     : </td>" +
                                      "<td style='border-right:solid 1 black' align='left'>" + (Convert.ToString(sdrSalarySlipMain["AccountNo"]) == "" ? " - " : Convert.ToString(sdrSalarySlipMain["AccountNo"])) + "</td>" +
                                  "</tr>" +
                              "</table>";
                htmStr += htmMain.ToString();

                String strDetailHead = "<table border=0 cellspading=0 cellspacing=0 width=100%" +
                                                  "<tr height=25px>" +
                                                      "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align=left><font size=2><b>Gross Pay</b></font></td>" +
                                                      "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align=right><font size=2><b>Amount( " + Convert.ToString(sdrSalarySlipMain["Currency"]) + ")</b></font></td> " +
                                                      "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align=left><font size=2><b>Deductions</b></font></td>" +
                                                      "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black;border-right:solid 1 black' align=right><font size=2><b>Amount( " + Convert.ToString(sdrSalarySlipMain["Currency"]) + ")</b></font></td>" +
                                                  "</tr> ";

                htmStr += strDetailHead.ToString();

                strDetailBottom = "<tr height=25px>" +
                                          "<td style='border-left:solid 1 black;border-top:solid 1 black' align=left><font size=2><b>Total</b></font></td>" +
                                          "<td style='border-left:solid 1 black;border-top:solid 1 black' align=right><font size=2><b>" + Convert.ToString(sdrSalarySlipMain["IsAddition"]) + "</b></font></td>" +
                                          "<td style='border-left:solid 1 black;border-top:solid 1 black' align=left><font size=2><b>Total</b></font></td>" +
                                          "<td style='border-left:solid 1 black;border-top:solid 1 black;border-right:solid 1 black' align=right><font size=2><b>" + Convert.ToString(sdrSalarySlipMain["Deduction"]) + "</b></font></td>" +
                                      "</tr> " +
                                      " <tr height=25px><tr height=25px>" +
                                          "<td colspan=3 style='border-left:solid 1 black;border-top:solid 1 black' align=right><font size=2><b>Net Pay</b></font></td>" +
                                          "<td style='border-left:solid 1 black;border-top:solid 1 black;border-right:solid 1 black' align=right><font size=2><b>" + Convert.ToString(sdrSalarySlipMain["NetAmount"]) + "</b></font></td>" +
                                      "</tr>" +
                                      "<tr height=25px><tr height=25px> " +
                                          "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align=left><font size=2><b>Amount in words</b></font></td>" +
                                          "<td colspan=3 style='border-left:solid 1 black;border-top:solid 1 black;border-right:solid 1 black;border-bottom:solid 1 black' align=left><font size=2><b>" + Convert.ToString(sdrSalarySlipMain["AmountInWords"]) + "</b></font></td>" +
                                      "</tr> </table>";
                blnFlag = true;
            }

            String strDetailes = "<tr height=25px>" +
                                      "<td style='border-left:solid 1 black' align=left>" + (Convert.ToString(sdrSalarySlipMain["AddItem"]).Trim() == "" ? "&nbsp" : Convert.ToString(sdrSalarySlipMain["AddItem"]).Trim()) + "</td>" +
                                      "<td style='border-left:solid 1 black' align=right>" + (Convert.ToString(sdrSalarySlipMain["AdAmount"]).Trim() == "0" ? "&nbsp" : Convert.ToString(sdrSalarySlipMain["AdAmount"]).Trim()) + "</td>" +
                                      "<td style='border-left:solid 1 black' align=left>" + (Convert.ToString(sdrSalarySlipMain["DedItem"]).Trim() == "" ? "&nbsp" : Convert.ToString(sdrSalarySlipMain["DedItem"]).Trim()) + "</td>" +
                                      "<td style='border-left:solid 1 black;border-right:solid 1 black' align=right>" + (Convert.ToString(sdrSalarySlipMain["DedAmount"]).Trim() == "0" ? "&nbsp" : Convert.ToString(sdrSalarySlipMain["DedAmount"]).Trim()) + "</td>" +
                                  "</tr>";
            htmStr += strDetailes;
        }

        htmStr += strDetailBottom.ToString();

        sdrSalarySlipMain.Close();
        sdrSalarySlipMain = null;

        ArrayList paramWorkInfo = new ArrayList();
        SqlDataReader sdrWorkInfo;
        Boolean blnStatus = false;

        paramWorkInfo.Add(new SqlParameter("@Type", 1));
        paramWorkInfo.Add(new SqlParameter("@Id", MintRecordID));
        sdrWorkInfo = MobjConnection.ExecutesReader("spPayGetInfoWebformMain", paramWorkInfo, CommandBehavior.CloseConnection);

        if (sdrWorkInfo.Read())
        {
            sdrWorkInfo.Close();
            String htmWorkInfo = GetWorkInfo(MintRecordID, blnStatus);

            if (blnStatus == true)
            {
                String htmMain = "<tr><td colspan=4 align=center><font size=3><b>Work Info</b></font></td></tr>" +
                "<tr><td colspan=8>" + htmWorkInfo + " </td></tr></table>";
                htmStr += htmMain.ToString();
                mFlag = true;
            }
        }
        else
        {
            sdrWorkInfo.Close();
            mFlag = false;
        }

        sdrWorkInfo = null;

        return htmStr;
    }

    private string GetLocationSchedule()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>Company Name</td><td>" + Convert.ToString(drowEmail["CompanyName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Schedule Name</td><td>" + Convert.ToString(drowEmail["ScheduleName"]) + "</td></tr>" +
                           "<tr height=25px><td>Location Name</td><td>" + Convert.ToString(drowEmail["LocationName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>From Date</td><td>" + Convert.ToString(drowEmail["FromDate"]) + "</td></tr>" +
                           "<tr height=25px><td>To Date</td><td>" + Convert.ToString(drowEmail["ToDate"]) + "</td></tr>"
                           );
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        strColor = "#ffffff";
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Work Location Schedule Details</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=50%><font color=#FFFFF><b>Employee Name</b></font></td>" +
            "<td width=50%><font color=#FFFFF><b>Employee Number</b></font></td>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {
            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["EmployeeName"]) + "</td>" +
                 "<td>" + Convert.ToString(drowEmail["EmployeeNumber"]) + "</td>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        return MsbHtml.ToString();
    }

    private string GetWorkInfo(long intPaymentId, bool blnStatus)
    {
        String htmStr = "";

        Boolean blExists = false;

        int IntScale = 0;


        ArrayList paramvehicle = new ArrayList();
        paramvehicle.Add(new SqlParameter("@PaymentID", intPaymentId));

        blnStatus = false;

        SqlDataReader sdV = MobjConnection.ExecutesReader("spPayGetSalarySlipWorkInfo", paramvehicle, CommandBehavior.CloseConnection);

        string htmHead = "<tr><td width='100%' colspan='4'><table border=0 cellspading=0 cellspacing=0 width=100%>" +
        "<tr>" +
            "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Total Work Days</b></td>" +
            "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Worked Days</b></td>" +
            "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Rest Days</b></td> " +
            "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2 ><b>HoliDays</b></td>" +
            "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Paid Leaves</b></td> " +
            "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2><b>Unpaid Leaves</b></td> " +
            "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2 ><b>Absent Hours</b></td> " +
            "<td style='border-left:solid 1 black;border-top:solid 1 black' align=center><font size=2 ><b>OT</b></td>" +
            "<td style='border-left:solid 1 black;border-top:solid 1 black;border-right:solid 1 black'  align=center><font size=2 ><b>Shortage</b></td> " +
        "</tr>";
        htmStr += htmHead.ToString();

        while (sdV.Read())
        {
            string htminner = "<tr><td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><" + Convert.ToString(sdV["TotalWorkDays"]) + "></td>" +
                               "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><" + GlobalNumFormat(Convert.ToString(sdV["DaysWorked"]), IntScale) + "></td>" +
                               "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><" + GlobalNumFormat(Convert.ToString(sdV["Rest"]), IntScale) + "></td>" +
                               "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><" + GlobalNumFormat(Convert.ToString(sdV["Holiday"]), IntScale) + "></td>" +
                               "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><" + GlobalNumFormat(Convert.ToString(sdV["Leave"]), IntScale) + "></td>" +
                               "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><" + GlobalNumFormat(Convert.ToString(sdV["UnPaidLeave"]), IntScale) + "></td>" +
                               "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><" + GlobalNumFormat(Convert.ToString(sdV["Absent"]), IntScale) + "></td>" +
                               "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black' align='center'><" + GlobalNumFormat(Convert.ToString(sdV["OT"]), IntScale) + "></td>" +
                               "<td style='border-left:solid 1 black;border-top:solid 1 black;border-bottom:solid 1 black;border-right:solid 1 black' align='center'><" + GlobalNumFormat(Convert.ToString(sdV["Shortage"]), IntScale) + "></td>" +
                           "</tr>";
            blnStatus = true;
            htmStr += htminner.ToString();
            blExists = true;
        }
        sdV.Close();
        sdV = null;
        htmStr += "</table></td></tr>";

        return htmStr;
    }



    public string GlobalNumFormat(string StrVal, int Scale)
    {

        string StrDecimal;
        int A;
        StrDecimal = "0.";


        for (A = 0; Scale - 1 >= A; ++A)
        {
            StrDecimal = StrDecimal + "0";
        }

        if (StrDecimal == "0.")
        {
            StrDecimal = "0";
        }
        StrVal = Strings.Format(Convert.ToDecimal(StrVal), StrDecimal);

        return StrVal;
    }
    private string GetLeavePolicyEmail()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        // Company Master details
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];


            MsbHtml.Append(
            "<tr bgcolor=#fff2eb height=25px><td width=30%>Policy Name</td><td width=70%>" + Convert.ToString(DrWebInfo["LeavePolicyName"]) + "</td></tr>" +
            "<tr height=25px><td width=30%>Applicable From</td><td width=70%>" + Convert.ToString(DrWebInfo["ApplicableFrom"]) + "</td></tr>");

            strColor = "#ffffff";

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        // Work Policy Details
        if (EmailSource.Tables[1].Rows.Count > 0)
        {

            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Leave Type</b></font></td>" +
                           "<td><font color=#FFFFF><b>No Of Leaves</b></font></td><td><font color=#FFFFF><b>Month Leaves</b></font></td></font>" +
                           "<td><font color=#FFFFF><b>Carryforwarded Leaves</b></font></td><td><font color=#FFFFF><b>Encashable Days</b></font></td></font></tr>");

            strColor = "#fff2eb";

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["lvtype"]) + "</td>" +
                                "<td>" + Convert.ToString(drowEmail["NoOfLeave"]) + "</td> " +
                                "<td>" + Convert.ToString(drowEmail["MonthLeave"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["CarryForwardLeave"]) + "</td> " +
                               "<td>" + Convert.ToString(drowEmail["EncashDays"]) + "</td></tr>");
                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        else
        {
            MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[1].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Details</h2></font></td></tr>");
            MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        }
        //if (EmailSource.Tables[2].Rows.Count > 0)
        //{

        //    MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2> Policy Consequences</h2></font></td></tr>");
        //    MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td><font color=#FFFFF><b>Leave Type</b></font></td>" +
        //                   "<td><font color=#FFFFF><b>Calculation Type</b></font></td></font><td><font color=#FFFFF><b>Calculation Percentage</b></font></td></font><td><font color=#FFFFF><b>Applicable Days</b></font></td></font></tr>");

        //    strColor = "#fff2eb";

        //    foreach (DataRow drowEmail in EmailSource.Tables[2].Rows)
        //    {
        //        MsbHtml.Append("<tr bgcolor=" + strColor + " height=25px><td>" + Convert.ToString(drowEmail["LeaveType"]) + "</td>" +
        //                       "<td>" + Convert.ToString(drowEmail["CalculationType"]) + "</td> " +
        //                       "<td>" + Convert.ToString(drowEmail["CalculationPercentage"]) + "</td> " +
        //                       "<td>" + Convert.ToString(drowEmail["ApplicableDays"]) + "</td></tr>");

        //        if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        //    }
        //}

        //else
        //{
        //    MsbHtml.Append("<tr bgcolor =" + strColor + "  height=25px><td colspan=" + EmailSource.Tables[2].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Policy Consequences</h2></font></td></tr>");
        //    MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>No Information Found</h2></font></td></tr>");

        //}

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }
    private string GetDeduction()
    {
        string strRateonly = "", strPercentage = "";
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            if ((drowEmail["RateOnly"]).ToInt32() == 1) { strRateonly = "Yes"; } else { strRateonly = "No"; strPercentage = "(%)"; }

            // Master data
            MsbHtml.Append("<tr height=25px><td>PolicyName</td><td>" + Convert.ToString(drowEmail["PolicyName"]) + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td>Deduction</td><td>" + Convert.ToString(drowEmail["Particulars"]) + "</td></tr>" +
                            "<tr height=25px><td>Deduct From</td><td>" + Convert.ToString(drowEmail["DeductFrom"]) + "</td></tr>" +
                             "<tr bgcolor=#fff2eb height=25px><td>Rate Only</td><td>" + strRateonly + "</td></tr>" +
                           "<tr height=25px><td>PolicyCondition</td><td>" + Convert.ToString(drowEmail["PolicyCondition"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Effect From</td><td>" + Convert.ToString(drowEmail["WithEffectFrom"]) + "</td></tr>" +
                           "<tr height=25px><td>Employer Contribution" + strPercentage + "</td><td>" + Convert.ToString(drowEmail["EmployerPart"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>Employee Contribution" + strPercentage + "</td><td>" + Convert.ToString(drowEmail["EmployeePart"]) + "</td></tr>");
            if (drowEmail["RateOnly"].ToInt32() == 3)
                MsbHtml.Append("<tr height=25px><td>Fixed Amount</td><td>" + Convert.ToString(drowEmail["FixedAmount"]) + "</td></tr>");
            if (drowEmail["RateOnly"].ToInt32() != 1)
                MsbHtml.Append("<tr height=25px><td>Particulars</td><td>" + Convert.ToString(EmailSource.Tables[1].Rows[0]["ParicularName"]) + "</td></tr>");

        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
    private string GetPFPolicy()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];
            DataRow drowEmail2 = EmailSource.Tables[1].Rows[0];
            // Master data
            MsbHtml.Append("<tr height=25px><td>PolicyName</td><td>" + Convert.ToString(drowEmail["PolicyName"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>Deduct From</td><td>" + Convert.ToString(drowEmail["DeductFrom"]) + "</td></tr>" +
                           "<tr height=25px><td>CalculationElements</td><td>" + Convert.ToString(drowEmail["CalculationElements"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>PolicyCondition</td><td>" + Convert.ToString(drowEmail["PolicyCondition"]) + "</td></tr>" +
                           "<tr height=25px><td>WithEffect From</td><td>" + Convert.ToString(drowEmail["WithEffectFrom"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>EmployerPart</td><td>" + Convert.ToString(drowEmail["EmployerPart"]) + "</td></tr>" +
                           "<tr height=25px><td>EmployeePart</td><td>" + Convert.ToString(drowEmail["EmployeePart"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>EPSEmployer</td><td>" + Convert.ToString(drowEmail["EPSEmployer"]) + "</td></tr>" +
                           "<tr height=25px><td>EDLI</td><td>" + Convert.ToString(drowEmail["EDLI"]) + "</td></tr>" +
                            "<tr bgcolor=#fff2eb height=25px><td>EPFAdmin</td><td>" + Convert.ToString(drowEmail["EPFAdmin"]) + "</td></tr>" +
                           "<tr height=25px><td>EDLIAdmin</td><td>" + Convert.ToString(drowEmail["EDLIAdmin"]) + "</td></tr>"
                           );
            if (Convert.ToString(drowEmail2["ParicularName"]) != "")
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td>Particulars</td><td>" + Convert.ToString(drowEmail2["ParicularName"]) + "</td></tr>");
            if (drowEmail["FixedAmount"].ToDecimal() != 0)
                MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td>FixedAmount</td><td>" + Convert.ToString(drowEmail["FixedAmount"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }

    private string GetLeaveEntry()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Leave Information</h2></font></td></tr>");

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow drowEmail = EmailSource.Tables[0].Rows[0];

            // Master data

            MsbHtml.Append("<tr height=25px><td>Employee Name</td><td>" + Convert.ToString(drowEmail["employeefullname"]) + "</td></tr>" +
                           "<tr bgcolor=#fff2eb height=25px><td>LeaveType</td><td>" + Convert.ToString(drowEmail["LeaveType"]) + "</td></tr>"
                          );
            if (Convert.ToString(drowEmail["HalfDay"]) == "Yes")
            {
                MsbHtml.Append("<tr height=25px><td>Date</td><td>" + Convert.ToString(drowEmail["ldtfrom"]) + "</td></tr>");
            }
            else
            {
                MsbHtml.Append("<tr height=25px><td>Date from</td><td>" + Convert.ToString(drowEmail["ldtfrom"]) + "</td></tr>" +
                                    "<tr bgcolor=#fff2eb height=25px><td>Date to</td><td>" + Convert.ToString(drowEmail["ldtTo"]) + "</td></tr>");
            }
            //"<tr bgcolor=#fff2eb height=25px><td>RejoinDate</td><td>" + Convert.ToString(drowEmail["ldtrejoin"]) + "</td></tr>"+
            MsbHtml.Append("<tr height=25px><td>HalfDay</td><td>" + Convert.ToString(drowEmail["HalfDay"]) + "</td></tr>" +
           "<tr bgcolor=#fff2eb height=25px><td>LeaveStatus</td><td>" + Convert.ToString(drowEmail["LeaveStatus"]) + "</td></tr>"

    );
            if (drowEmail["Remarks"].ToStringCustom().Trim() != "")
                MsbHtml.Append("<tr height=25px><td>Remarks</td><td>" + Convert.ToString(drowEmail["Remarks"]) + "</td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }

    private string GetLeaveExtension()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            //General Information
            //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
            MsbHtml.Append("<tr height=25px><td width=30%>Employee</td><td width=70%>" + Convert.ToString(DrWebInfo["EmployeeName"]) + "</td></tr>");

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");

        }
        strColor = "#fff2eb";

        //MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=" + EmailSource.Tables[0].Columns.Count + " align=left><font size=2 color=#3a3a53><h2>Activities</h2></font></td></tr>");
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=20%><font color=#FFFFF><b>Extension Type</b></font></td>" +
                       "<td width=15%><font color=#FFFFF><b>Leave Type</b></font></td><td width=30%><font color=#FFFFF><b>Applicable</b></font></td><td width=15%><font color=#FFFFF><b>No of Day(s) / Minute(s)</b></font></td><td width=10% style=display:none><font color=#FFFFF><b>Add. Minutes</b></font></td><td width=20%><font color=#FFFFF><b>Reason</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[0].Rows)
        {

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td width=20%>" + Convert.ToString(drowEmail["LeaveExtension"]) + "</td>" +
                           "<td width=15%>" + Convert.ToString(drowEmail["Leavetype"]) + "</td>" +
                           "<td width=30%>" + Convert.ToString(drowEmail["MonthYear"]) + "</td>" +
                           "<td width=15%>" + Convert.ToString(drowEmail["NoOfDays"]) + "</td>" +
                           "<td width=10%  style=display:none>" + Convert.ToString(drowEmail["AdditionalMinutes"]) + "</td>" +
                           "<td width=20%>" + Convert.ToString(drowEmail["Reason"]) + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }



        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }

    private string GetShiftSchedule()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];

            string strTemp = "";

            if (ClsCommonSettings.Glb24HourFormat)
            {
                strTemp = Strings.Format(Convert.ToDateTime(DrWebInfo["FromTime"]), "HH:mm ") + " To " +
                 "" + Strings.Format(Convert.ToDateTime(DrWebInfo["ToTime"]), "HH:mm tt") +
                 " [Duration = " + DrWebInfo["Duration"] + ", Min.WorkHrs = " + DrWebInfo["MinWorkingHours"] + "]";
            }
            else
            {
                strTemp = Strings.Format(Convert.ToDateTime(DrWebInfo["FromTime"]), "hh:mm tt") + " To " +
                  "" + Strings.Format(Convert.ToDateTime(DrWebInfo["ToTime"]), "hh:mm tt") +
                  " [Duration = " + DrWebInfo["Duration"] + ", Min.WorkHrs = " + DrWebInfo["MinWorkingHours"] + "]";
            }

            MsbHtml.Append("<tr height=25px><td width=30%>Schedule Name</td><td width=70%>" + DrWebInfo["ScheduleName"].ToStringCustom() + "</td>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Shift Policy</td><td width=70%>" + DrWebInfo["ShiftName"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td width=30%>Shift Time</td><td width=70%>" + strTemp.ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>From Date</td><td width=70%>" + DrWebInfo["FromDate"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td width=30%>To Date</td><td width=70%>" + DrWebInfo["ToDate"].ToStringCustom() + "</td></tr>");

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        }

        strColor = "#fff2eb";
        MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px><td width=100%><font color=#FFFFF><b>Employee Details</b></font></td></font></tr>");

        foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
        {

            MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td width=100%>" + drowEmail["EmployeeName"].ToStringCustom() + "</td></tr>");

            if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }

    private string GetEmployeeExpenseEmail()
    {
        GetWebformHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        strColor = "#ffffff";
        DataRow drEmail = null;

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            drEmail = EmailSource.Tables[0].Rows[0];

            string strType = "";
            if (drEmail["ExpenseTransType"].ToStringCustom() == "None")
                strType = "Actual Amount";
            else if (drEmail["ExpenseTransType"].ToStringCustom() == "Reimbursement")
                strType = "Reim. Amount";
            else if (drEmail["ExpenseTransType"].ToStringCustom() == "Deduction")
                strType = "Ded. Amount";

            MsbHtml.Append("<tr height=25px><td width=30% align=left>Employee Name</td><td align=left>" + drEmail["EmployeeName"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Date</td><td align=left>" + drEmail["ExpenseDate"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Expense Type</td><td align=left>" + drEmail["ExpenseType"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>From Date</td><td align=left>" + drEmail["ExpenseFromDate"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>To Date</td><td align=left>" + drEmail["ExpenseToDate"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Amount</td><td align=left>" + drEmail["ExpenseAmount"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Type</td><td align=left>" + drEmail["ExpenseTransType"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>" + strType + "</td><td align=left>" + drEmail["ActualAmount"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Currency</td><td align=left>" + drEmail["CurrencyName"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Remarks</td><td align=left>" + drEmail["Remarks"].ToStringCustom() + "</td></tr>");
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetDepositEmail()
    {
        GetWebformHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        strColor = "#ffffff";
        DataRow drEmail = null;

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            drEmail = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr height=25px><td width=30% align=left>Employee Name</td><td align=left>" + drEmail["EmployeeName"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Deposit Type</td><td align=left>" + drEmail["DepositType"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Deposit Amount</td><td align=left>" + drEmail["DepositAmount"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Currency</td><td align=left>" + drEmail["CurrencyName"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Deposit Date</td><td align=left>" + drEmail["DepositDate"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Is Refundable</td><td align=left>" + drEmail["IsRefundable"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Refundable Date</td><td align=left>" + drEmail["RefundableDate"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Is Refunded</td><td align=left>" + drEmail["IsRefunded"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Refund Date</td><td align=left>" + drEmail["RefundDate"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Remarks</td><td align=left>" + drEmail["Remarks"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Documents</td><td align=left>" + drEmail["DocumentAttached"].ToStringCustom() + "</td></tr>");
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetLoanRepaymentEmail()
    {
        GetWebformHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        strColor = "#ffffff";
        DataRow drEmail = null;

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            drEmail = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr height=25px><td width=30% align=left>Employee Name</td><td align=left>" + drEmail["EmployeeName"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Loan Number</td><td align=left>" + drEmail["LoanNumber"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Date</td><td align=left>" + drEmail["Date"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Amount</td><td align=left>" + drEmail["Amount"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Currency</td><td align=left>" + drEmail["CurrencyName"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Is From Salary</td><td align=left>" + drEmail["FromSalary"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Remarks</td><td align=left>" + drEmail["Remarks"].ToStringCustom() + "</td></tr>");

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        }

        MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Installment Details</h2></font></td></tr>");
        strColor = "#fff2eb";

        if (EmailSource.Tables[1].Rows.Count > 0)
        {
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px>" +
                    "<td width=20%><font color=#FFFFF><b>Installment No.</b></font></td>" +
                    "<td width=15%><font color=#FFFFF><b>Amount</b></font></td></tr>");

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td width=20%>" + drowEmail["InstalmentNo"].ToStringCustom() + "</td>" +
                               "<td width=15%>" + drowEmail["PrincipleAmount"].ToStringCustom() + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        else
        {
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px>" +
                    "<td width=100%><font color=#FFFFF><b>No Data Found</b></font></td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }

    private string GetSalaryAdvanceEmail()
    {
        GetWebformHeaderInfo();

        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        strColor = "#ffffff";
        DataRow drEmail = null;

        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            drEmail = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr height=25px><td width=30% align=left>Employee Name</td><td align=left>" + drEmail["EmployeeName"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Date</td><td align=left>" + drEmail["Date"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Amount</td><td align=left>" + drEmail["Amount"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Currency</td><td align=left>" + drEmail["CurrencyName"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td align=left>Remarks</td><td align=left>" + drEmail["Remarks"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td align=left>Is Settled</td><td align=left>" + drEmail["IsSettled"].ToStringCustom() + "</td></tr>");
        }

        MsbHtml.Append("</table></td></tr></td></tr></table></div></body></html>");
        return MsbHtml.ToString();
    }

    private string GetAssetHandover()
    {
        GetWebformHeaderInfo();

        // Heading
        MsbHtml.Append("<tr bgcolor=#fff2eb height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>General Information</h2></font></td></tr>");
        if (EmailSource.Tables[0].Rows.Count > 0)
        {
            DataRow DrWebInfo = EmailSource.Tables[0].Rows[0];
            MsbHtml.Append("<tr height=25px><td width=30%>Employee</td><td width=70%>" + DrWebInfo["EmployeeName"].ToStringCustom() + "</td>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Asset Type</td><td width=70%>" + DrWebInfo["AssetType"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td width=30%>Asset</td><td width=70%>" + DrWebInfo["Asset"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>With Effect Date</td><td width=70%>" + DrWebInfo["WithEffectDate"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td width=30%>Expected Return Date</td><td width=70%>" + DrWebInfo["ExpectedReturnDate"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Return Date</td><td width=70%>" + DrWebInfo["ReturnDate"].ToStringCustom() + "</td></tr>" +
                "<tr height=25px><td width=30%>Asset Status</td><td width=70%>" + DrWebInfo["AssetStatus"].ToStringCustom() + "</td></tr>" +
                "<tr bgcolor=#fff2eb height=25px><td width=30%>Remarks</td><td width=70%>" + DrWebInfo["AssetRemarks"].ToStringCustom() + "</td></tr>");

            MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        }

        MsbHtml.Append("<tr height=25px><td colspan=2 align=left><font size=2 color=#3a3a53><h2>Asset Usage/Damage Details</h2></font></td></tr>");
        strColor = "#fff2eb";

        if (EmailSource.Tables[1].Rows.Count > 0)
        {            
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px>" +
                    "<td width=20%><font color=#FFFFF><b>Serial No.</b></font></td>" +
                    "<td width=15%><font color=#FFFFF><b>Other Info Type</b></font></td>" +
                    "<td width=30%><font color=#FFFFF><b>Date</b></font></td>" +
                    "<td width=30%><font color=#FFFFF><b>Amount</b></font></td>" +
                    "<td width=15%><font color=#FFFFF><b>Remarks</b></font></td></tr>");

            foreach (DataRow drowEmail in EmailSource.Tables[1].Rows)
            {
                MsbHtml.Append("<tr  bgcolor=" + strColor + " height=25px><td width=20%>" + drowEmail["SerialNo"].ToStringCustom() + "</td>" +
                               "<td width=15%>" + drowEmail["AssetUsageType"].ToStringCustom() + "</td>" +
                               "<td width=30%>" + drowEmail["Date"].ToStringCustom() + "</td>" +
                               "<td width=30%>" + drowEmail["Amount"].ToStringCustom() + "</td>" +
                               "<td width=15%>" + drowEmail["Remarks"].ToStringCustom() + "</td></tr>");

                if (strColor == "#fff2eb") strColor = "#ffffff"; else strColor = "#fff2eb";
            }
        }
        else
        {
            MsbHtml.Append("<tr bgcolor=#6d6e71 height=25px>" +
                    "<td width=100%><font color=#FFFFF><b>No Data Found</b></font></td></tr>");
        }

        MsbHtml.Append("</table><table border=0 cellspading=0 cellspacing=0 width=100%>");
        return MsbHtml.ToString();
    }
}