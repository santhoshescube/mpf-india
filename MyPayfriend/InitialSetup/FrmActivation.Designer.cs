﻿namespace MyPayfriend
{
    partial class frmActivation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmActivation));
            this.lblRegister = new System.Windows.Forms.LinkLabel();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpbrowse = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDocPath = new System.Windows.Forms.TextBox();
            this.lblmessage = new System.Windows.Forms.Label();
            this.cmbProtocol = new System.Windows.Forms.ComboBox();
            this.lblProtocol = new System.Windows.Forms.Label();
            this.rbtnClient = new System.Windows.Forms.RadioButton();
            this.rbtnServer = new System.Windows.Forms.RadioButton();
            this.Label3 = new System.Windows.Forms.Label();
            this.btnSaveServersettings = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtServerName = new System.Windows.Forms.TextBox();
            this.picBrowse = new System.Windows.Forms.PictureBox();
            this.txtInitial = new System.Windows.Forms.TextBox();
            this.txtkey1 = new System.Windows.Forms.TextBox();
            this.txtkey6 = new System.Windows.Forms.TextBox();
            this.txtkey5 = new System.Windows.Forms.TextBox();
            this.txtkey4 = new System.Windows.Forms.TextBox();
            this.txtkey3 = new System.Windows.Forms.TextBox();
            this.txtkey2 = new System.Windows.Forms.TextBox();
            this.grpbrowse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBrowse)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRegister
            // 
            this.lblRegister.AutoSize = true;
            this.lblRegister.BackColor = System.Drawing.Color.Transparent;
            this.lblRegister.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRegister.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegister.ForeColor = System.Drawing.Color.White;
            this.lblRegister.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lblRegister.LinkColor = System.Drawing.Color.White;
            this.lblRegister.Location = new System.Drawing.Point(45, 319);
            this.lblRegister.Name = "lblRegister";
            this.lblRegister.Size = new System.Drawing.Size(187, 13);
            this.lblRegister.TabIndex = 19;
            this.lblRegister.TabStop = true;
            this.lblRegister.Text = "Not yet registered,  please click here!";
            this.lblRegister.VisitedLinkColor = System.Drawing.Color.Blue;
            this.lblRegister.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblRegister_LinkClicked);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.White;
            this.Label1.Location = new System.Drawing.Point(40, 279);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(175, 14);
            this.Label1.TabIndex = 18;
            this.Label1.Text = "Please enter Activation Key";
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.SystemColors.Control;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Location = new System.Drawing.Point(502, 362);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 21;
            this.btnOk.Text = "&Submit";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(12, 362);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 20;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // grpbrowse
            // 
            this.grpbrowse.Controls.Add(this.button1);
            this.grpbrowse.Controls.Add(this.txtDocPath);
            this.grpbrowse.Controls.Add(this.lblmessage);
            this.grpbrowse.Controls.Add(this.cmbProtocol);
            this.grpbrowse.Controls.Add(this.lblProtocol);
            this.grpbrowse.Controls.Add(this.rbtnClient);
            this.grpbrowse.Controls.Add(this.rbtnServer);
            this.grpbrowse.Controls.Add(this.Label3);
            this.grpbrowse.Controls.Add(this.btnSaveServersettings);
            this.grpbrowse.Controls.Add(this.Label2);
            this.grpbrowse.Controls.Add(this.txtServerName);
            this.grpbrowse.Location = new System.Drawing.Point(23, 85);
            this.grpbrowse.Name = "grpbrowse";
            this.grpbrowse.Size = new System.Drawing.Size(475, 139);
            this.grpbrowse.TabIndex = 22;
            this.grpbrowse.TabStop = false;
            this.grpbrowse.Text = "Server Settings";
            this.grpbrowse.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(416, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtDocPath
            // 
            this.txtDocPath.BackColor = System.Drawing.Color.White;
            this.txtDocPath.Location = new System.Drawing.Point(131, 20);
            this.txtDocPath.Name = "txtDocPath";
            this.txtDocPath.Size = new System.Drawing.Size(279, 20);
            this.txtDocPath.TabIndex = 0;
            // 
            // lblmessage
            // 
            this.lblmessage.AutoSize = true;
            this.lblmessage.Location = new System.Drawing.Point(20, 26);
            this.lblmessage.Name = "lblmessage";
            this.lblmessage.Size = new System.Drawing.Size(81, 13);
            this.lblmessage.TabIndex = 24;
            this.lblmessage.Text = "Document Path";
            // 
            // cmbProtocol
            // 
            this.cmbProtocol.FormattingEnabled = true;
            this.cmbProtocol.Items.AddRange(new object[] {
            "TCP/IP",
            "Named Pipes"});
            this.cmbProtocol.Location = new System.Drawing.Point(345, 52);
            this.cmbProtocol.Name = "cmbProtocol";
            this.cmbProtocol.Size = new System.Drawing.Size(105, 21);
            this.cmbProtocol.TabIndex = 4;
            this.cmbProtocol.Visible = false;
            // 
            // lblProtocol
            // 
            this.lblProtocol.AutoSize = true;
            this.lblProtocol.Location = new System.Drawing.Point(251, 56);
            this.lblProtocol.Name = "lblProtocol";
            this.lblProtocol.Size = new System.Drawing.Size(88, 13);
            this.lblProtocol.TabIndex = 17;
            this.lblProtocol.Text = "Network protocol";
            this.lblProtocol.Visible = false;
            // 
            // rbtnClient
            // 
            this.rbtnClient.AutoSize = true;
            this.rbtnClient.Enabled = false;
            this.rbtnClient.Location = new System.Drawing.Point(194, 53);
            this.rbtnClient.Name = "rbtnClient";
            this.rbtnClient.Size = new System.Drawing.Size(51, 17);
            this.rbtnClient.TabIndex = 3;
            this.rbtnClient.Text = "Client";
            this.rbtnClient.UseVisualStyleBackColor = true;
            // 
            // rbtnServer
            // 
            this.rbtnServer.AutoSize = true;
            this.rbtnServer.Checked = true;
            this.rbtnServer.Enabled = false;
            this.rbtnServer.Location = new System.Drawing.Point(131, 53);
            this.rbtnServer.Name = "rbtnServer";
            this.rbtnServer.Size = new System.Drawing.Size(56, 17);
            this.rbtnServer.TabIndex = 2;
            this.rbtnServer.TabStop = true;
            this.rbtnServer.Text = "Server";
            this.rbtnServer.UseVisualStyleBackColor = true;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(20, 55);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(85, 13);
            this.Label3.TabIndex = 11;
            this.Label3.Text = "Current Machine";
            // 
            // btnSaveServersettings
            // 
            this.btnSaveServersettings.BackColor = System.Drawing.SystemColors.Control;
            this.btnSaveServersettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveServersettings.Location = new System.Drawing.Point(375, 110);
            this.btnSaveServersettings.Name = "btnSaveServersettings";
            this.btnSaveServersettings.Size = new System.Drawing.Size(75, 23);
            this.btnSaveServersettings.TabIndex = 6;
            this.btnSaveServersettings.Text = "&Submit";
            this.btnSaveServersettings.UseVisualStyleBackColor = false;
            this.btnSaveServersettings.Click += new System.EventHandler(this.btnSaveServersettings_Click);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(20, 85);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(106, 13);
            this.Label2.TabIndex = 10;
            this.Label2.Text = "SQL Server Instance";
            // 
            // txtServerName
            // 
            this.txtServerName.Location = new System.Drawing.Point(131, 81);
            this.txtServerName.MaxLength = 150;
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(319, 20);
            this.txtServerName.TabIndex = 5;
            // 
            // picBrowse
            // 
            this.picBrowse.BackgroundImage = global::MyPayfriend.Properties.Resources.top_strip;
            this.picBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBrowse.Location = new System.Drawing.Point(-2, -2);
            this.picBrowse.Name = "picBrowse";
            this.picBrowse.Size = new System.Drawing.Size(593, 60);
            this.picBrowse.TabIndex = 24;
            this.picBrowse.TabStop = false;
            // 
            // txtInitial
            // 
            this.txtInitial.BackColor = System.Drawing.SystemColors.Control;
            this.txtInitial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInitial.Location = new System.Drawing.Point(225, 276);
            this.txtInitial.MaxLength = 30;
            this.txtInitial.Name = "txtInitial";
            this.txtInitial.Size = new System.Drawing.Size(50, 20);
            this.txtInitial.TabIndex = 25;
            this.txtInitial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtInitial.TextChanged += new System.EventHandler(this.txtInitial_TextChanged);
            // 
            // txtkey1
            // 
            this.txtkey1.BackColor = System.Drawing.SystemColors.Control;
            this.txtkey1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtkey1.Location = new System.Drawing.Point(225, 276);
            this.txtkey1.MaxLength = 4;
            this.txtkey1.Name = "txtkey1";
            this.txtkey1.Size = new System.Drawing.Size(50, 20);
            this.txtkey1.TabIndex = 31;
            this.txtkey1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtkey6
            // 
            this.txtkey6.BackColor = System.Drawing.SystemColors.Control;
            this.txtkey6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtkey6.Location = new System.Drawing.Point(502, 276);
            this.txtkey6.MaxLength = 4;
            this.txtkey6.Name = "txtkey6";
            this.txtkey6.Size = new System.Drawing.Size(50, 20);
            this.txtkey6.TabIndex = 30;
            this.txtkey6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtkey6.TextChanged += new System.EventHandler(this.txtkey6_TextChanged);
            // 
            // txtkey5
            // 
            this.txtkey5.BackColor = System.Drawing.SystemColors.Control;
            this.txtkey5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtkey5.Location = new System.Drawing.Point(446, 276);
            this.txtkey5.MaxLength = 4;
            this.txtkey5.Name = "txtkey5";
            this.txtkey5.Size = new System.Drawing.Size(50, 20);
            this.txtkey5.TabIndex = 29;
            this.txtkey5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtkey4
            // 
            this.txtkey4.BackColor = System.Drawing.SystemColors.Control;
            this.txtkey4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtkey4.Location = new System.Drawing.Point(391, 276);
            this.txtkey4.MaxLength = 4;
            this.txtkey4.Name = "txtkey4";
            this.txtkey4.Size = new System.Drawing.Size(50, 20);
            this.txtkey4.TabIndex = 28;
            this.txtkey4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtkey3
            // 
            this.txtkey3.BackColor = System.Drawing.SystemColors.Control;
            this.txtkey3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtkey3.Location = new System.Drawing.Point(337, 276);
            this.txtkey3.MaxLength = 4;
            this.txtkey3.Name = "txtkey3";
            this.txtkey3.Size = new System.Drawing.Size(50, 20);
            this.txtkey3.TabIndex = 27;
            this.txtkey3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtkey2
            // 
            this.txtkey2.BackColor = System.Drawing.SystemColors.Control;
            this.txtkey2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtkey2.Location = new System.Drawing.Point(281, 276);
            this.txtkey2.MaxLength = 4;
            this.txtkey2.Name = "txtkey2";
            this.txtkey2.Size = new System.Drawing.Size(50, 20);
            this.txtkey2.TabIndex = 26;
            this.txtkey2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // frmActivation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MyPayfriend.Properties.Resources.Activation1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(589, 389);
            this.Controls.Add(this.txtInitial);
            this.Controls.Add(this.txtkey1);
            this.Controls.Add(this.txtkey6);
            this.Controls.Add(this.txtkey5);
            this.Controls.Add(this.txtkey4);
            this.Controls.Add(this.txtkey3);
            this.Controls.Add(this.txtkey2);
            this.Controls.Add(this.picBrowse);
            this.Controls.Add(this.grpbrowse);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblRegister);
            this.Controls.Add(this.Label1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmActivation";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "E People Activation";
            this.Load += new System.EventHandler(this.frmActivation_Load);
            this.grpbrowse.ResumeLayout(false);
            this.grpbrowse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBrowse)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.LinkLabel lblRegister;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.GroupBox grpbrowse;
        internal System.Windows.Forms.ComboBox cmbProtocol;
        internal System.Windows.Forms.Label lblProtocol;
        internal System.Windows.Forms.RadioButton rbtnClient;
        internal System.Windows.Forms.RadioButton rbtnServer;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox txtServerName;
        internal System.Windows.Forms.Button btnSaveServersettings;
        internal System.Windows.Forms.PictureBox picBrowse;
        internal System.Windows.Forms.TextBox txtInitial;
        internal System.Windows.Forms.TextBox txtkey1;
        internal System.Windows.Forms.TextBox txtkey6;
        internal System.Windows.Forms.TextBox txtkey5;
        internal System.Windows.Forms.TextBox txtkey4;
        internal System.Windows.Forms.TextBox txtkey3;
        internal System.Windows.Forms.TextBox txtkey2;
        internal System.Windows.Forms.Button button1;
        internal System.Windows.Forms.TextBox txtDocPath;
        internal System.Windows.Forms.Label lblmessage;
    }
}

