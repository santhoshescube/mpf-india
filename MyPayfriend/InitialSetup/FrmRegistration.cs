﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;


namespace MyPayfriend
{
    public partial class frmRegistration : Form
    {
        string MstrMessageCaption = "EPeople";
        public frmRegistration()
        {
            InitializeComponent();
        }

        private void frmRegistration_Load(object sender, EventArgs e)
        {
            
            if (!IsAdminUser())
            {
                MessageBox.Show("Please login as administrator to register EPeople", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
        }

        private bool IsAdminUser()
        {
            try
            {
                System.Security.Principal.WindowsIdentity wIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();
                System.Security.Principal.WindowsPrincipal wP = new System.Security.Principal.WindowsPrincipal(wIdentity);
                return wP.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool ValidForm()
        {
            if (txtCompany.Text.Trim() == "")
            {
                MessageBox.Show("Please enter company", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCompany.Focus();
                return false;
            }
            if (txtName.Text.Trim() == "")
            {
                MessageBox.Show("Please enter name", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtName.Focus();
                return false;
            }
            if (txtJob.Text.Trim() == "")
            {
                MessageBox.Show("Please enter job title", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtJob.Focus();
                return false;
            }
           
            if (txtcountry.Text.Trim() == "")
            {
                MessageBox.Show("Please enter country", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtcountry.Focus();
                return false;
            }
            if (txtTelephone.Text.Trim() == "")
            {
                MessageBox.Show("Please enter telephone", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTelephone.Focus();
                return false;
            }
            if (txtEmail.Text.Trim() == "")
            {
                MessageBox.Show("Please enter email", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEmail.Focus();
                return false;
            }
            if (txtSlNo.Text.Trim() == "")
            {
                MessageBox.Show("Please enter serial number", MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSlNo.Focus();
                return false;
            }

            return true;
        }
        
        private string GenerateRegistrationReport(string strRequestNo) 
        {
            string strHtml="<html><head></head><body><table border=0 cellspacing=2 cellpadding=2 width=100%>" +
                            "<tr><td colspan=2 align=center><h2>Registration Form</h2></td></tr>";
            strHtml += "<tr><td width=20%>Company</td><td>" + txtCompany.Text.Trim() + "</td></tr>" +
                        "<tr><td width=20%>Customer Name</td><td>" + txtName.Text.Trim() + "</td></tr>" +
                       "<tr><td width=20%>Job Title</td><td>" + txtJob.Text.Trim() + "</td></tr>" +
                       "<tr><td width=20%>PO Box</td><td>" + POBoxTextBox.Text.Trim() + "</td></tr>" +
                       "<tr><td width=20%>Country</td><td>" + txtcountry.Text.Trim() + "</td></tr>" +
                       "<tr><td width=20%>Telephone</td><td>" + txtTelephone.Text.Trim() + "</td></tr>" +
                       "<tr><td width=20%>Email</td><td>" + txtEmail.Text.Trim() + "</td></tr>" +
                       "<tr><td width=20%>Mobile</td><td>" + txtMobile.Text.Trim() + "</td></tr>" +
                       "<tr><td width=20%>Website</td><td>" + txtWebSite.Text.Trim() + "</td></tr>" +
                       "<tr><td width=20%>Serial No</td><td>" + txtSlNo.Text.Trim() + "</td></tr>" +
                       "<tr><td width=20%>Request No</td><td>" + strRequestNo + "</td></tr>" +
                       "<tr><td colspan=2><hr></hr></td></tr>";
            strHtml += "<tr><td>Phone No </td><td>+971 42759000,+91 9895377900</td></tr></table></body></html>";

            return strHtml;
        }
         
        private string GetMailBody(string sKeyNo) 
        {
            int Ln  = sKeyNo.Length / 4;
            string sKeyRt = "";
            for (int i =0; i<Ln; i++)
            {
                sKeyRt += sKeyNo.Substring(0, 4).Trim() + "-";
                sKeyNo = sKeyNo.Remove(0, 4).Trim();
            }
            sKeyRt = sKeyRt.Substring(0, sKeyRt.Length - 1).ToUpper().Trim();

            string htmDc = "<html><head></head>"+
                        "<body><basefont face=arial,verdana,courier size=2>"+
                            "<table border=0 cellspacing=4 cellpadding=2 width=600 bgcolor=skyblue>"+
                                "<tr><td colspan=2 align=center><font face=arial size=4 color=red>Congratulations!</font></td></tr>"+
                                "<tr><td colspan=2 align=center><b>You are successfully registered with us. Please use Product Key for activation.</b></td></tr>"+
                                "<tr><td width=20%>Product Name</td><td>EPeople</td></tr>"+
                                "<tr><td>Serial No</td><td>" + txtSlNo.Text.Trim() + "</td></tr>"+
                                "<tr><td>Product Key</td><td><font size=3><b>" +  sKeyRt + "</b></font></td></tr>"+
                                "<tr><td>Company</td><td>" + txtCompany.Text.Trim() + "</td></tr>"+
                                "<tr><td>User Name</td><td>" + txtEmail.Text.Trim() + "</td></tr>"+
                                "<tr><td>Password</td><td>" + txtCompany.Text.Substring(0, 3) + "@123" +" </td></tr>"+
                                "<tr><td>Date</td><td>" + DateTime.Now.ToShortDateString() + "</td></tr>"+
                                "<tr><td colspan=2 align=center><h3>ES3 Technovations Corporate Support Help Line</h3></td></tr>" +
                                "<tr><td>Telephone No</td><td>+971 42759000,+91 9895377900</td></tr>" +
                                "<tr><td>Mail support</td><td>support@escubetech.com</td></tr>"+
                                "<tr><td>Web site</td><td>www.escubetech.com</td></tr>" +
                                "<tr bgcolor=black><td colspan=2 align=center><font size=2 color=white> Copyright(c) 2021 ES3 Technovations LLP. All rights reserved.</font></td></tr>" +
                                "</table></basefont></body></html>";

            return htmDc;
        }
                         
        private string GetRequestNumber()
        {
            ProjectSettings.MainSettings objMain = new ProjectSettings.MainSettings();
            string spKey = GetPrdKey();
            return objMain.GetProductRequestNumber(2, txtSlNo.Text.Trim(), spKey);
        }

        private string GetPrdKey()
        {
            string pKey = "WJ3B";
            switch (System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major)
            {
                case 1:
                    pKey = "QY1N";
                    break;
                case 2:
                    pKey = "L7GR";
                    break ;
                case 3:
                    pKey = "KSE6";
                    break ;
                case 4:
                    pKey = "WJ3B";
                    break ;
            }
            return pKey;
        }

        private string  GetKeyNumberLV(long  CustomerId , string sReqKey ) 
        {
            string strActKey= "";
            try
            {
                ProjectSettings.MainSettings objSettings = new ProjectSettings.MainSettings();
                string prKey = GetPrdKey();
                strActKey = objSettings.GetActKeyLive(2, txtSlNo.Text.Trim(), sReqKey, prKey, CustomerId);
                return strActKey;
            }
            catch (Exception)
            {
                return strActKey;
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ValidForm())
            {
                clsSendmail objMail=new clsSendmail ();

                string strReqNo = GetRequestNumber();

                string strHtmlForm = GenerateRegistrationReport(strReqNo);

                webPrintform.DocumentText =strHtmlForm ;

               
                MessageBox.Show("Submission failed !. \n Internet is required to register electronically. \n Please use print option to print registration form. \n " +
                                "Fax the printed form to the fax number mentioned in the form. " , MstrMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblstatus.Text = "License Request No :" + strReqNo;
                btnPrint.Enabled = true;
               
               

            }
        }

        private void btnBottomCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtName.Clear();
            txtJob.Clear();
            txtCompany.Clear();
            POBoxTextBox.Clear();
            txtcountry.Clear();
            txtTelephone.Clear();
            txtEmail.Clear();
            txtMobile.Clear();
            txtSlNo.Clear();
            btnPrint.Enabled = false;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            webPrintform.ShowPrintPreviewDialog();
        }

        


    }
}
