﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal; 


///
///Created By   : Bijoy
///Purpose      : To Activate MyPayfriend product
///Created Date : 19 Apr 2011
///

namespace MyPayfriend
{
    public partial class frmActivation : DevComponents.DotNetBar.Office2007Form
    {
        ProjectSettings.clsRegistry objRegistry;
        string MserverName;

        public frmActivation()
        {
            InitializeComponent();

            MserverName = "";
            objRegistry = new ProjectSettings.clsRegistry();
        }

        private void frmActivation_Load(object sender, EventArgs e)
        {
            //if (!IsAdminUser())
            //{
            //    MessageBox.Show("Please login as administrator to activate MyPayfriend", "MyPayfriend", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    Application.Exit();
            //}
            //else
            //{
                picBrowse.Visible = false;
                btnOk.Enabled = false;
                btnSaveServersettings.Visible = false;
                grpbrowse.Visible = false;
                txtServerName.Text = System.Environment.MachineName + "\\SQLEXPRESS";
            //}
                
        }

       

        private bool IsAdminUser()
        {
            try
            {
                System.Security.Principal.WindowsIdentity wIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();
                System.Security.Principal.WindowsPrincipal wP = new System.Security.Principal.WindowsPrincipal(wIdentity);
                return wP.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator);
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        private bool ActivateProduct()
        {
            try
            {
                string strKey = txtkey1.Text.Trim() + txtkey2.Text.Trim() + txtkey3.Text.Trim() + txtkey4.Text.Trim() + txtkey5.Text.Trim() + txtkey6.Text.Trim();
                ProjectSettings.MainSettings objMain = new ProjectSettings.MainSettings();
                if (objMain.ActivationSettings(2, strKey))
                {
                    ClsMainSettings.blnActivationStatus = true;
                    string strRegSlNo = "1111";
                    objMain.GetProductSerial(2, out strRegSlNo);

                    ProjectSettings.h objInitial = new ProjectSettings.h();
                    objInitial.CreateProductFile(2);
                    objInitial.UpdateInitialProduct(2, 60);

                    if (strRegSlNo.Substring(0, 1) == "0")      //Client serial
                    {
                        rbtnClient.Checked = true;
                        lblProtocol.Visible = true;
                        cmbProtocol.Visible = true;
                    }
                    else
                    {
                        rbtnServer.Checked = true;
                        MserverName = System.Environment.MachineName + "\\SQLEXPRESS";
                    }

                    txtServerName.Text = MserverName;

                    picBrowse.Visible = true;
                    grpbrowse.Visible = true;
                    btnSaveServersettings.Visible = true;
                    picBrowse.Visible = true;
                    this.BackgroundImage = null;
                    this.Width = 530;
                    this.Height = 290;
                    this.Top = this.Top + 70;
                    btnCancel.Visible = false;
                    this.Text = "Server settings";
                    return true;
                }
                else
                {
                    MessageBox.Show("Enter valid activation key", "MyPayfriend", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtkey1.Focus();
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

            if (ActivateProduct())
            {
                btnOk.Visible = false;
                btnSaveServersettings.Visible = true;
            }
        }

        

        private void lblRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmRegistration objReg = new frmRegistration();
            objReg.ShowDialog();
            objReg.Dispose();

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void txtInitial_TextChanged(object sender, EventArgs e)
        {
            int Ln = txtInitial.Text.Trim().Length;
            if (Ln == 29)
            {
                txtInitial.Visible = false;
                string[] strSp = txtInitial.Text.Split('-');
                txtkey1.Text = strSp[0];
                txtkey2.Text = strSp[1];
                txtkey3.Text = strSp[2];
                txtkey4.Text = strSp[3];
                txtkey5.Text = strSp[4];
                txtkey6.Text = strSp[5];
            }
            else
            {
                txtInitial.Visible = false;
                txtkey1.Text = txtInitial.Text.Trim();
                txtkey1.Focus();
            }
        }

        private void txtkey6_TextChanged(object sender, EventArgs e)
        {
            string strKey  = txtkey1.Text.Trim() + txtkey2.Text.Trim() + txtkey3.Text.Trim() + txtkey4.Text.Trim() + txtkey5.Text.Trim() + txtkey6.Text.Trim();
            if (strKey.Trim().Length != 24)
                btnOk.Enabled = false;
            else
                btnOk.Enabled = true;
        
        }

        private void btnSaveServersettings_Click(object sender, EventArgs e)
        {
           if (txtServerName.Text.Trim() == "" )
             {
                 MessageBox.Show("Please enter Server Name", "MyPayfriend", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return ;
             }
             else
             {
                if (CreateRegistryValues())
                {
                    if (rbtnServer.Checked )
                    {
                        AttchDbconnetionProcess();
                       
                        this.DialogResult =DialogResult.OK ;
                        this.Close();
                    }
                    else
                    {
                        SetClientsettings();
                        this.DialogResult =DialogResult.OK ;
                        this.Close();
                    }
                }
             }
        
        }

        private bool CreateRegistryValues()
        {
            object objSetVal = txtServerName.Text.Trim();
            objRegistry.WriteToRegistry("SOFTWARE\\Epeopleserver", "Epeopleserver", "Epeopleservername", objSetVal);
            ClsCommonSettings.ServerName = txtServerName.Text.Trim();

            objSetVal = txtDocPath.Text.Trim();
            objRegistry.WriteToRegistry("SOFTWARE\\Epeopleserver", "Epeopleserver", "Epeopleserverpath", objSetVal);
            ClsCommonSettings.strServerPath = txtDocPath.Text.Trim();

            if (rbtnServer.Checked)
                objSetVal = 0;
            else
                objSetVal = 1;

            objRegistry.WriteToRegistry("SOFTWARE\\ES3Tech\\MyProducts", "MyProducts", "Mypayfriend", objSetVal);

            ClsMainSettings.blnActivationStatus = true;
            
            DirectoryInfo dInfo=new DirectoryInfo(Application.StartupPath + "\\Logs");
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.Read | FileSystemRights.Write, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);

            return true;
        }

        private bool AttchDbconnetionProcess()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string sSqlQuery = "Use[Master] Create DataBase GoldenAnchor ON (Filename=N'" + Application.StartupPath + "\\Data\\GoldenAnchor.mdf'),(filename=N'" + Application.StartupPath + "\\Data\\GoldenAnchor_log.ldf') For Attach; " +
                                 "Alter Database GoldenAnchor set single_user with Rollback Immediate Alter Database GoldenAnchor set read_write Alter Database GoldenAnchor set multi_user " +
                                 "Create Login[msadmin] with password=N'msSoft!234',Default_database=[GoldenAnchor],Default_language=[us_english],Check_expiration=OFF,Check_policy=OFF";

                string ConnStr = "Data Source=" + txtServerName.Text.Trim() + ";Initial Catalog=master;Integrated Security=True;";
                SqlConnection objCnn = new SqlConnection(ConnStr);
                objCnn.Open();
                SqlCommand objCmd = new SqlCommand();

                objCmd.CommandType = CommandType.Text;
                objCmd.CommandTimeout = 0;
                objCmd.Connection = objCnn;
                objCmd.CommandText = sSqlQuery;
                objCmd.ExecuteNonQuery();

                sSqlQuery = "EXEC xp_instance_regwrite N'HKEY_LOCAL_MACHINE', N'Software\\Microsoft\\MSSQLServer\\MSSQLServer', N'LoginMode', REG_DWORD, 2";
                objCmd = new SqlCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandTimeout = 0;
                objCmd.Connection = objCnn;
                objCmd.CommandText = sSqlQuery;
                objCmd.ExecuteNonQuery();

                sSqlQuery = "Use[Master] EXEC sp_addsrvrolemember @loginame = N'msadmin', @rolename = N'sysadmin'";
                objCmd = new SqlCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandTimeout = 0;
                objCmd.Connection = objCnn;
                objCmd.CommandText = sSqlQuery;
                objCmd.ExecuteNonQuery();

                sSqlQuery = "Create role msadmin AUTHORIZATION db_owner";
                objCmd = new SqlCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandTimeout = 0;
                objCmd.Connection = objCnn;
                objCmd.CommandText = sSqlQuery;
                objCmd.ExecuteNonQuery();

                objCnn.Close();

                RestartService();

                new clsBulksetup().ProductdbStatus();

                Cursor.Current = Cursors.Default;

                return true;
            }
            catch (Exception)
            {
                Cursor.Current = Cursors.Default;
                return false;
            }
        }

        private void RestartService()
        {
            string sService = "MSSQL$SQLEXPRESS";
            ServiceController oServ = new ServiceController(sService, System.Environment.MachineName);
            if (oServ != null)
            {
                if (oServ.Status == ServiceControllerStatus.Running)
                {
                    oServ.Stop();
                    oServ.WaitForStatus(ServiceControllerStatus.Stopped);

                    System.Threading.Thread.Sleep(1000);

                }
                if (oServ.Status == ServiceControllerStatus.Stopped)
                {
                    oServ.Start();
                    oServ.WaitForStatus(ServiceControllerStatus.Running);

                    System.Threading.Thread.Sleep(1000);

                }
            }
        }

        private bool WriteToRegistrySql(string ParentKey,string subParent , string sKeyname, string subkeyname, object subkeyval)
        {
            try
            {
                //Get 'Everyone' account
                System.Security.Principal.SecurityIdentifier sid = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
                System.Security.Principal.NTAccount acct = sid.Translate(typeof(System.Security.Principal.NTAccount)) as System.Security.Principal.NTAccount;
                string strEveryoneAccount = acct.ToString();

                //Get parent key in HKLM
                RegistryKey keySLP = default(RegistryKey);
                RegistrySecurity rs = default(RegistrySecurity);
                RegistryKey rkey = Registry.LocalMachine.OpenSubKey(ParentKey, true);
                if (rkey == null)
                {
                    rkey = Registry.LocalMachine.OpenSubKey(subParent, true);
                    keySLP = rkey.CreateSubKey(sKeyname);
                    keySLP.SetValue(subkeyname, subkeyval);
                    rs = rkey.GetAccessControl(AccessControlSections.Access);
                    rs.AddAccessRule(new RegistryAccessRule(strEveryoneAccount, RegistryRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                    //Apply access control definition
                    keySLP.SetAccessControl(rs);

                }
                else
                {
                    keySLP = Registry.LocalMachine.OpenSubKey(ParentKey, true);
                    keySLP.SetValue(subkeyname, subkeyval);
                    rs = rkey.GetAccessControl(AccessControlSections.Access);
                    rs.AddAccessRule(new RegistryAccessRule(strEveryoneAccount, RegistryRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                    //Apply access control definition
                    keySLP.SetAccessControl(rs);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool SetClientsettings()
        {
            try
            {
                //string mName = txtServerName.Text.Trim();
                //if (mName.IndexOf("SQLEXPRESS") != -1)
                //    mName = mName.Substring(0, mName.IndexOf("SQLEXPRESS") - 1).Trim();

                //if (cmbProtocol.SelectedIndex == 0)
                //{
                //    WriteToRegistrySql("SOFTWARE\\Microsoft\\MSSQLServer\\Client\\ConnectTo", "SOFTWARE\\Microsoft\\MSSQLServer\\Client", "ConnectTo", txtServerName.Text.Trim(), "DBMSSOCN," + mName + "");
                //    WriteToRegistrySql("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.1\\MSSQLServer\\SuperSocketNetLib\\Tcp", "SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.1\\MSSQLServer\\SuperSocketNetLib", "Tcp", "Enabled", "1");
                //}
                //else
                //{
                //    WriteToRegistrySql("SOFTWARE\\Microsoft\\MSSQLServer\\Client\\ConnectTo", "SOFTWARE\\Microsoft\\MSSQLServer\\Client", "ConnectTo", txtServerName.Text.Trim(), "DBNMPNTW,\\" + mName + "\\PIPE\\MSSQL$SQLEXPRESS\\sql\\query");
                //    WriteToRegistrySql("SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.1\\MSSQLServer\\SuperSocketNetLib\\Np", "SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.1\\MSSQLServer\\SuperSocketNetLib", "Np", "Enabled", "1");
                //}



                if (System.IO.Directory.Exists(Application.StartupPath + "\\Data"))
                    System.IO.Directory.Delete(Application.StartupPath + "\\Data", true);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        
       }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog objBrowser = new FolderBrowserDialog();
                objBrowser.RootFolder = Environment.SpecialFolder.Desktop;
                objBrowser.Description = "Select Server folder";
                if (objBrowser.ShowDialog() == DialogResult.OK)
                {
                    txtDocPath.Text = objBrowser.SelectedPath;
                    if (txtDocPath.Text.IndexOf(":") != -1)
                        txtServerName.Text = System.Environment.MachineName + "\\SQLEXPRESS";
                    else
                    {
                        txtServerName.Text = txtDocPath.Text.Replace("\\\\", "").Trim();
                        txtServerName.Text = txtServerName.Text.Substring(0, txtServerName.Text.IndexOf("\\")).Trim() + "\\SQLEXPRESS";
                        txtServerName.Text = txtServerName.Text.ToUpper();
                    }
                }
                objBrowser.Dispose();
            }
            catch (Exception)
            {
                txtServerName.Text = System.Environment.MachineName + "\\SQLEXPRESS";
            }
        }

        

        
        
    }
}
