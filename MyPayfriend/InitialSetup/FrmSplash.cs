﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public partial class FrmSplash : Form
    {
        bool blnProductOK;
        public FrmSplash()
        {
            InitializeComponent();

            blnProductOK = true;
        }
                
        private void FrmSplash_Load(object sender, EventArgs e)
        {
            ClsMainSettings.blnActivationStatus = false;
            ClsMainSettings.strProductSerial = "0";
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            clsSettings objSettings = new clsSettings();
            string strTemp = "";
            if (objSettings.GetProductSerialNo(out strTemp))
            {
                if (strTemp == "")
                {
                    this.Opacity = 0;
                    frmRegistration objReg = new frmRegistration();
                    objReg.ShowDialog();
                    objReg.Dispose();

                    Application.Exit();
                }
                else
                {
                    ClsMainSettings.strProductSerial = strTemp;
                }
            }
            else
            {
                this.Opacity = 0;
                frmRegistration objReg = new frmRegistration();
                objReg.ShowDialog();
                objReg.Dispose();

                Application.Exit();
            }

            if (ClsMainSettings.strProductSerial == "0" || ClsMainSettings.strProductSerial.Length !=8)
                blnProductOK = false;

            ProjectSettings.MainSettings objMainsetting = new ProjectSettings.MainSettings();
            ClsMainSettings.blnActivationStatus = objMainsetting.CheckActivationSettings(2);

            if (!ClsMainSettings.blnActivationStatus)
            {
                this.Opacity = 0;
                frmActivation objActivation = new frmActivation();
                if (objActivation.ShowDialog() == DialogResult.OK && ClsMainSettings.blnActivationStatus == true)
                {
                    objActivation.Dispose();
                    tmSplash.Enabled = true;
                }
                else
                {
                    objActivation.Dispose();
                    Application.Exit();
                }
            }
            else if (!blnProductOK)       
            {
                CheckProductPaid();
                tmSplash.Enabled = true;
            }
            else
            {
                tmSplash.Enabled = true;
            }
        }

        private void CheckProductPaid()
        {
            try
            {
                clsFiles objSettings = new clsFiles();
                ClsMainSettings.blnProductStatus = objSettings.CheckProductMaxUsage("31-Mar-2020");
                   
            }
            catch (Exception)
            {
                return;
            }
        }

        private void CheckProductStatus()
        {
            try
            {
                short blStatus = 0;
                //ProjectSettings.MainSettings objMain = new ProjectSettings.MainSettings();
                //if (blStatus==0)
                //{
                //    blStatus = objMain.CheckProductStatus(2, System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + 5, Convert.ToDecimal(ClsMainSettings.strProductSerial), false);
                //}
                if (blStatus==1)
                {
                    Application.Exit();
                }
                else if (blStatus == 2)
                {
                    ClsMainSettings.blnProductStatus = false;
                    this.Close();
                }
                else
                    this.Close();
            }
            catch (Exception)
            {
                this.Close();
            }
        }

        private void tmSplash_Tick(object sender, EventArgs e)
        {
            tmSplash.Enabled = false;
            CheckProductStatus();
        }

       

    }
}
