﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace MyPayfriend
{
    class clsSettings
    {
        RegClass objRegistry;
        MachineSettings objMHSettings;
        public clsSettings()
        {
            objMHSettings = new MachineSettings();
            objRegistry = new RegClass();
        }

        public bool GetProductSerialNo(out string strSerialNo)
        {
            strSerialNo = "";
            return objRegistry.ReadFromRegistry("SOFTWARE\\ES3Tech\\EPeopleSlNo", "EPeopleSlNo", out strSerialNo);
        }

        public static string EncryptCrypto(string strText)
        {
            string strEncrKey = "Belives-in-God";
            byte[] byKey = null;
            byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };

            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));

                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());

            }
            catch (Exception)
            {
                return strText;
            }
        }

        public static string DecryptCrypto(string strText)
        {
            string sDecrKey = "Belives-in-God";
            byte[] byKey = null;
            byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };
            byte[] inputByteArray = new byte[strText.Length + 1];

            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(strText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;

                return encoding.GetString(ms.ToArray());

            }
            catch (Exception)
            {
                return strText;
            }
        }

    }

    static class Productval
    {
        public static int EmpLimit { get { return 1000; } }
        public static int CmpLimit { get { return 5; } }
        public static int BranchLimit { get { return 60; } }
    }

}
