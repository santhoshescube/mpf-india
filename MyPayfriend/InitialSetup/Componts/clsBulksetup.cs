﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MyPayfriend
{
    class clsBulksetup
    {
        clsSendmail objNet;

        public clsBulksetup()
        {
            objNet = new clsSendmail();
        }

        private bool CheckNetConnection()
        {
            return objNet.IsInternet_Connected();
        }

        public short GetProductStatusLive()
        {
            ProjectSettings.MainSettings objMainSetp = new ProjectSettings.MainSettings();
            return objMainSetp.CheckProductStatus(2, System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + 5, Convert.ToDecimal(ClsMainSettings.strProductSerial), true);

        }

        public void ProductdbStatus()
        {
            try
            {
                UpdateDb();
                DataLayer objCon = new DataLayer();
                string sqlS = "Select MnuID,MnuType,Description from MenuTypes";
                DataTable dtS = objCon.ExecuteDataTable(sqlS);
                string mnSl = "0";
                if (dtS.Rows.Count > 0)
                {
                    //mnSl = clsSettings.DecryptCrypto(Convert.ToString(dtS.Rows[0]["Description"]));
                    //if (mnSl != Archlist.GLSlno)
                    //    ClsMainSettings.blnProductStatus = false;

                    string mnuTy = clsSettings.DecryptCrypto(Convert.ToString(dtS.Rows[0]["MnuType"]));
                    if (objCon.ExecuteScalar("Select datediff(day,getdate(),N'" + mnuTy + "')").ToInt32() < 0)
                        ClsMainSettings.blnProductStatus = false;
                }
                else
                {
                    string mnT = clsSettings.EncryptCrypto(DateTime.Parse("21 Aug 2018").ToShortDateString());
                    mnSl = clsSettings.EncryptCrypto(ClsMainSettings.strProductSerial);
                    ArrayList prmDbs = new ArrayList();
                    prmDbs.Add(new SqlParameter("@mnType", mnT));
                    prmDbs.Add(new SqlParameter("@mnDes", mnSl));
                    sqlS = "INSERT INTO MenuTypes ([MnuID],[MnuType],[MnuDate],[CreatedBy],[Description]) VALUES (1,@mnType,GETDATE(),1,@mnDes)";
                    objCon.ExecuteNonQuery(prmDbs, sqlS);
                }
            }
            catch (Exception)
            {
                //ClsMainSettings.blnProductStatus = false;
            }
        }

        private void UpdateDb()
        {
            try
            {
                string sqlQ = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MenuTypes]') AND type in (N'U')) " +
                            "BEGIN CREATE TABLE [dbo].[MenuTypes]([MnuID] [int] NULL,[MnuType] [varchar](100) NULL," +
                            "[MnuDate] [datetime] NOT NULL CONSTRAINT [DF_MenuTypes_MnuDate]  DEFAULT (getdate())," +
                            "[CreatedBy] [int] NULL,[Description] [varchar](100) NULL) ON [PRIMARY] END";
                new DataLayer().ExecuteScalar(sqlQ);
            }
            catch (Exception)
            { }
        }

             
        private string GetLocalIP()
        {
            string ipAddress = "";
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {
                    IPAddress[] ipAddr = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
                    for (int i = 0; i < ipAddr.Length; i++)
                    {
                        if (ipAddr[i].AddressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6)
                        {
                            if (!IPAddress.IsLoopback(ipAddr[i]))
                            {
                                ipAddress = ipAddr[i].ToString();
                                break;
                            }
                        }
                    }
                }

                return ipAddress;
            }
            catch (Exception)
            {
                return ipAddress;
            }
        }

        private string GetExtIP()
        {
            string extIP = "";
            try
            {
                //extIP = new WebClient().DownloadString("http://wanip.info/");
                extIP = new WebClient().DownloadString("http://checkip.dyndns.org/");
                extIP = new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}").Matches(extIP)[0].ToString();
                if (extIP.Length > 30)
                    extIP = extIP.Substring(0, 30);

                return extIP;
            }
            catch (Exception)
            {
                return extIP;
            }
        }

    }

   

}
