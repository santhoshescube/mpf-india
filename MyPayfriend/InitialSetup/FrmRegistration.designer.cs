﻿namespace MyPayfriend
{
    partial class frmRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label CustomerNameLabel;
            System.Windows.Forms.Label JobTitleLabel;
            System.Windows.Forms.Label CompanyLabel;
            System.Windows.Forms.Label CountryIDLabel;
            System.Windows.Forms.Label MobileLabel;
            System.Windows.Forms.Label lblSlNo;
            System.Windows.Forms.Label WebsiteLabel;
            System.Windows.Forms.Label TelephoneLabel;
            System.Windows.Forms.Label EmailLabel;
            System.Windows.Forms.Label POBoxLabel;
            System.Windows.Forms.Label StreetLabel;
            System.Windows.Forms.Label CityLabel;
            System.Windows.Forms.Label AreraCodeLabel;
            System.Windows.Forms.Label FaxLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistration));
            this.txtcountry = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtJob = new System.Windows.Forms.TextBox();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.txtSlNo = new System.Windows.Forms.TextBox();
            this.txtWebSite = new System.Windows.Forms.TextBox();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.webPrintform = new System.Windows.Forms.WebBrowser();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBottomCancel = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lblstatus = new System.Windows.Forms.Label();
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.POBoxTextBox = new System.Windows.Forms.TextBox();
            this.StreetTextBox = new System.Windows.Forms.TextBox();
            this.CityTextBox = new System.Windows.Forms.TextBox();
            this.AreraCodeTextBox = new System.Windows.Forms.TextBox();
            this.FaxTextBox = new System.Windows.Forms.TextBox();
            CustomerNameLabel = new System.Windows.Forms.Label();
            JobTitleLabel = new System.Windows.Forms.Label();
            CompanyLabel = new System.Windows.Forms.Label();
            CountryIDLabel = new System.Windows.Forms.Label();
            MobileLabel = new System.Windows.Forms.Label();
            lblSlNo = new System.Windows.Forms.Label();
            WebsiteLabel = new System.Windows.Forms.Label();
            TelephoneLabel = new System.Windows.Forms.Label();
            EmailLabel = new System.Windows.Forms.Label();
            POBoxLabel = new System.Windows.Forms.Label();
            StreetLabel = new System.Windows.Forms.Label();
            CityLabel = new System.Windows.Forms.Label();
            AreraCodeLabel = new System.Windows.Forms.Label();
            FaxLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // CustomerNameLabel
            // 
            CustomerNameLabel.AutoSize = true;
            CustomerNameLabel.Location = new System.Drawing.Point(16, 105);
            CustomerNameLabel.Name = "CustomerNameLabel";
            CustomerNameLabel.Size = new System.Drawing.Size(35, 13);
            CustomerNameLabel.TabIndex = 52;
            CustomerNameLabel.Text = "Name";
            // 
            // JobTitleLabel
            // 
            JobTitleLabel.AutoSize = true;
            JobTitleLabel.Location = new System.Drawing.Point(16, 146);
            JobTitleLabel.Name = "JobTitleLabel";
            JobTitleLabel.Size = new System.Drawing.Size(47, 13);
            JobTitleLabel.TabIndex = 55;
            JobTitleLabel.Text = "Job Title";
            // 
            // CompanyLabel
            // 
            CompanyLabel.AutoSize = true;
            CompanyLabel.Location = new System.Drawing.Point(16, 64);
            CompanyLabel.Name = "CompanyLabel";
            CompanyLabel.Size = new System.Drawing.Size(121, 13);
            CompanyLabel.TabIndex = 58;
            CompanyLabel.Text = "Company / Organization";
            // 
            // CountryIDLabel
            // 
            CountryIDLabel.AutoSize = true;
            CountryIDLabel.Location = new System.Drawing.Point(16, 269);
            CountryIDLabel.Name = "CountryIDLabel";
            CountryIDLabel.Size = new System.Drawing.Size(43, 13);
            CountryIDLabel.TabIndex = 63;
            CountryIDLabel.Text = "Country";
            // 
            // MobileLabel
            // 
            MobileLabel.AutoSize = true;
            MobileLabel.Location = new System.Drawing.Point(212, 150);
            MobileLabel.Name = "MobileLabel";
            MobileLabel.Size = new System.Drawing.Size(38, 13);
            MobileLabel.TabIndex = 65;
            MobileLabel.Text = "Mobile";
            // 
            // lblSlNo
            // 
            lblSlNo.AutoSize = true;
            lblSlNo.Location = new System.Drawing.Point(16, 392);
            lblSlNo.Name = "lblSlNo";
            lblSlNo.Size = new System.Drawing.Size(50, 13);
            lblSlNo.TabIndex = 75;
            lblSlNo.Text = "Serial No";
            // 
            // WebsiteLabel
            // 
            WebsiteLabel.AutoSize = true;
            WebsiteLabel.Location = new System.Drawing.Point(212, 267);
            WebsiteLabel.Name = "WebsiteLabel";
            WebsiteLabel.Size = new System.Drawing.Size(46, 13);
            WebsiteLabel.TabIndex = 71;
            WebsiteLabel.Text = "Website";
            // 
            // TelephoneLabel
            // 
            TelephoneLabel.AutoSize = true;
            TelephoneLabel.Location = new System.Drawing.Point(16, 310);
            TelephoneLabel.Name = "TelephoneLabel";
            TelephoneLabel.Size = new System.Drawing.Size(58, 13);
            TelephoneLabel.TabIndex = 72;
            TelephoneLabel.Text = "Telephone";
            // 
            // EmailLabel
            // 
            EmailLabel.AutoSize = true;
            EmailLabel.Location = new System.Drawing.Point(16, 351);
            EmailLabel.Name = "EmailLabel";
            EmailLabel.Size = new System.Drawing.Size(196, 13);
            EmailLabel.TabIndex = 74;
            EmailLabel.Text = "Email (Product key will sent to this email)";
            // 
            // POBoxLabel
            // 
            POBoxLabel.AutoSize = true;
            POBoxLabel.Location = new System.Drawing.Point(16, 187);
            POBoxLabel.Name = "POBoxLabel";
            POBoxLabel.Size = new System.Drawing.Size(40, 13);
            POBoxLabel.TabIndex = 127;
            POBoxLabel.Text = "POBox";
            // 
            // StreetLabel
            // 
            StreetLabel.AutoSize = true;
            StreetLabel.Location = new System.Drawing.Point(212, 189);
            StreetLabel.Name = "StreetLabel";
            StreetLabel.Size = new System.Drawing.Size(35, 13);
            StreetLabel.TabIndex = 128;
            StreetLabel.Text = "Street";
            // 
            // CityLabel
            // 
            CityLabel.AutoSize = true;
            CityLabel.Location = new System.Drawing.Point(212, 228);
            CityLabel.Name = "CityLabel";
            CityLabel.Size = new System.Drawing.Size(24, 13);
            CityLabel.TabIndex = 7;
            CityLabel.Text = "City";
            // 
            // AreraCodeLabel
            // 
            AreraCodeLabel.AutoSize = true;
            AreraCodeLabel.Location = new System.Drawing.Point(16, 228);
            AreraCodeLabel.Name = "AreraCodeLabel";
            AreraCodeLabel.Size = new System.Drawing.Size(57, 13);
            AreraCodeLabel.TabIndex = 130;
            AreraCodeLabel.Text = "Area Code";
            // 
            // FaxLabel
            // 
            FaxLabel.AutoSize = true;
            FaxLabel.Location = new System.Drawing.Point(212, 306);
            FaxLabel.Name = "FaxLabel";
            FaxLabel.Size = new System.Drawing.Size(24, 13);
            FaxLabel.TabIndex = 132;
            FaxLabel.Text = "Fax";
            // 
            // txtcountry
            // 
            this.txtcountry.BackColor = System.Drawing.SystemColors.Info;
            this.txtcountry.Location = new System.Drawing.Point(16, 286);
            this.txtcountry.MaxLength = 40;
            this.txtcountry.Name = "txtcountry";
            this.txtcountry.Size = new System.Drawing.Size(175, 20);
            this.txtcountry.TabIndex = 8;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Info;
            this.txtName.Location = new System.Drawing.Point(16, 122);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(367, 20);
            this.txtName.TabIndex = 1;
            // 
            // txtJob
            // 
            this.txtJob.BackColor = System.Drawing.SystemColors.Info;
            this.txtJob.Location = new System.Drawing.Point(16, 163);
            this.txtJob.MaxLength = 50;
            this.txtJob.Name = "txtJob";
            this.txtJob.Size = new System.Drawing.Size(175, 20);
            this.txtJob.TabIndex = 2;
            // 
            // txtCompany
            // 
            this.txtCompany.BackColor = System.Drawing.SystemColors.Info;
            this.txtCompany.Location = new System.Drawing.Point(16, 81);
            this.txtCompany.MaxLength = 50;
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(367, 20);
            this.txtCompany.TabIndex = 0;
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(212, 166);
            this.txtMobile.MaxLength = 20;
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(171, 20);
            this.txtMobile.TabIndex = 3;
            // 
            // txtSlNo
            // 
            this.txtSlNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtSlNo.Location = new System.Drawing.Point(16, 409);
            this.txtSlNo.MaxLength = 8;
            this.txtSlNo.Name = "txtSlNo";
            this.txtSlNo.Size = new System.Drawing.Size(148, 20);
            this.txtSlNo.TabIndex = 13;
            // 
            // txtWebSite
            // 
            this.txtWebSite.Location = new System.Drawing.Point(212, 283);
            this.txtWebSite.MaxLength = 50;
            this.txtWebSite.Name = "txtWebSite";
            this.txtWebSite.Size = new System.Drawing.Size(171, 20);
            this.txtWebSite.TabIndex = 9;
            // 
            // txtTelephone
            // 
            this.txtTelephone.BackColor = System.Drawing.SystemColors.Info;
            this.txtTelephone.Location = new System.Drawing.Point(16, 327);
            this.txtTelephone.MaxLength = 20;
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(175, 20);
            this.txtTelephone.TabIndex = 10;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Info;
            this.txtEmail.Location = new System.Drawing.Point(16, 368);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(367, 20);
            this.txtEmail.TabIndex = 12;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(223, 416);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(185, 13);
            this.Label1.TabIndex = 116;
            this.Label1.Text = "Fields marked in yellow are mandatory";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.Label7.Font = new System.Drawing.Font("Wingdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.Label7.ForeColor = System.Drawing.SystemColors.Info;
            this.Label7.Location = new System.Drawing.Point(198, 414);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(17, 15);
            this.Label7.TabIndex = 115;
            this.Label7.Text = "n";
            // 
            // webPrintform
            // 
            this.webPrintform.Location = new System.Drawing.Point(516, 267);
            this.webPrintform.MinimumSize = new System.Drawing.Size(20, 20);
            this.webPrintform.Name = "webPrintform";
            this.webPrintform.Size = new System.Drawing.Size(157, 113);
            this.webPrintform.TabIndex = 114;
            this.webPrintform.Visible = false;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.Control;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Location = new System.Drawing.Point(74, 469);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(64, 23);
            this.btnClear.TabIndex = 17;
            this.btnClear.Text = "C&lear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBottomCancel
            // 
            this.btnBottomCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnBottomCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBottomCancel.Location = new System.Drawing.Point(13, 469);
            this.btnBottomCancel.Name = "btnBottomCancel";
            this.btnBottomCancel.Size = new System.Drawing.Size(58, 23);
            this.btnBottomCancel.TabIndex = 16;
            this.btnBottomCancel.Text = "&Cancel";
            this.btnBottomCancel.UseVisualStyleBackColor = false;
            this.btnBottomCancel.Click += new System.EventHandler(this.btnBottomCancel_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.SystemColors.Control;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Location = new System.Drawing.Point(281, 469);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(58, 23);
            this.btnPrint.TabIndex = 15;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.SystemColors.Control;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Location = new System.Drawing.Point(345, 469);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(58, 23);
            this.btnSubmit.TabIndex = 14;
            this.btnSubmit.Text = "&Submit";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lblstatus
            // 
            this.lblstatus.AutoSize = true;
            this.lblstatus.ForeColor = System.Drawing.Color.Red;
            this.lblstatus.Location = new System.Drawing.Point(17, 437);
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(0, 13);
            this.lblstatus.TabIndex = 122;
            // 
            // PictureBox2
            // 
            this.PictureBox2.Location = new System.Drawing.Point(0, 456);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(417, 46);
            this.PictureBox2.TabIndex = 117;
            this.PictureBox2.TabStop = false;
            // 
            // PictureBox1
            // 
            this.PictureBox1.BackgroundImage = global::MyPayfriend.Properties.Resources.Registration_form_2;
            this.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBox1.Location = new System.Drawing.Point(-1, -1);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(419, 58);
            this.PictureBox1.TabIndex = 47;
            this.PictureBox1.TabStop = false;
            // 
            // POBoxTextBox
            // 
            this.POBoxTextBox.Location = new System.Drawing.Point(16, 204);
            this.POBoxTextBox.MaxLength = 10;
            this.POBoxTextBox.Name = "POBoxTextBox";
            this.POBoxTextBox.Size = new System.Drawing.Size(175, 20);
            this.POBoxTextBox.TabIndex = 4;
            // 
            // StreetTextBox
            // 
            this.StreetTextBox.Location = new System.Drawing.Point(212, 205);
            this.StreetTextBox.MaxLength = 50;
            this.StreetTextBox.Name = "StreetTextBox";
            this.StreetTextBox.Size = new System.Drawing.Size(171, 20);
            this.StreetTextBox.TabIndex = 5;
            // 
            // CityTextBox
            // 
            this.CityTextBox.Location = new System.Drawing.Point(212, 244);
            this.CityTextBox.MaxLength = 50;
            this.CityTextBox.Name = "CityTextBox";
            this.CityTextBox.Size = new System.Drawing.Size(171, 20);
            this.CityTextBox.TabIndex = 7;
            // 
            // AreraCodeTextBox
            // 
            this.AreraCodeTextBox.Location = new System.Drawing.Point(16, 245);
            this.AreraCodeTextBox.MaxLength = 5;
            this.AreraCodeTextBox.Name = "AreraCodeTextBox";
            this.AreraCodeTextBox.Size = new System.Drawing.Size(175, 20);
            this.AreraCodeTextBox.TabIndex = 6;
            // 
            // FaxTextBox
            // 
            this.FaxTextBox.Location = new System.Drawing.Point(212, 322);
            this.FaxTextBox.MaxLength = 20;
            this.FaxTextBox.Name = "FaxTextBox";
            this.FaxTextBox.Size = new System.Drawing.Size(171, 20);
            this.FaxTextBox.TabIndex = 11;
            // 
            // frmRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 504);
            this.Controls.Add(FaxLabel);
            this.Controls.Add(this.FaxTextBox);
            this.Controls.Add(POBoxLabel);
            this.Controls.Add(this.POBoxTextBox);
            this.Controls.Add(StreetLabel);
            this.Controls.Add(this.StreetTextBox);
            this.Controls.Add(CityLabel);
            this.Controls.Add(this.CityTextBox);
            this.Controls.Add(AreraCodeLabel);
            this.Controls.Add(this.AreraCodeTextBox);
            this.Controls.Add(this.lblstatus);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnBottomCancel);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.webPrintform);
            this.Controls.Add(this.PictureBox2);
            this.Controls.Add(lblSlNo);
            this.Controls.Add(this.txtSlNo);
            this.Controls.Add(WebsiteLabel);
            this.Controls.Add(this.txtWebSite);
            this.Controls.Add(TelephoneLabel);
            this.Controls.Add(this.txtTelephone);
            this.Controls.Add(EmailLabel);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtcountry);
            this.Controls.Add(CustomerNameLabel);
            this.Controls.Add(this.txtName);
            this.Controls.Add(JobTitleLabel);
            this.Controls.Add(this.txtJob);
            this.Controls.Add(CompanyLabel);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(CountryIDLabel);
            this.Controls.Add(MobileLabel);
            this.Controls.Add(this.txtMobile);
            this.Controls.Add(this.PictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRegistration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Registration";
            this.Load += new System.EventHandler(this.frmRegistration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.TextBox txtcountry;
        internal System.Windows.Forms.TextBox txtName;
        internal System.Windows.Forms.TextBox txtJob;
        internal System.Windows.Forms.TextBox txtCompany;
        internal System.Windows.Forms.TextBox txtMobile;
        internal System.Windows.Forms.TextBox txtSlNo;
        internal System.Windows.Forms.TextBox txtWebSite;
        internal System.Windows.Forms.TextBox txtTelephone;
        internal System.Windows.Forms.TextBox txtEmail;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.WebBrowser webPrintform;
        internal System.Windows.Forms.PictureBox PictureBox2;
        internal System.Windows.Forms.Button btnClear;
        internal System.Windows.Forms.Button btnBottomCancel;
        internal System.Windows.Forms.Button btnPrint;
        internal System.Windows.Forms.Button btnSubmit;
        internal System.Windows.Forms.Label lblstatus;
        internal System.Windows.Forms.TextBox POBoxTextBox;
        internal System.Windows.Forms.TextBox StreetTextBox;
        internal System.Windows.Forms.TextBox CityTextBox;
        internal System.Windows.Forms.TextBox AreraCodeTextBox;
        internal System.Windows.Forms.TextBox FaxTextBox;
    }
}