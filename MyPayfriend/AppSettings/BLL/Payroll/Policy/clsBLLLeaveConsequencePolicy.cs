﻿using System;
using System.Collections.Generic;
using System.Data;

namespace MyPayfriend
{ /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : LeaveConsequence Policy BLL 
       * ******************************************/

    public class clsBLLLeaveConsequencePolicy
    {
        #region Declartions
        
        clsDALLeaveConsequencePolicy MobjDALLeaveConsequencePolicy = null;

        public clsBLLLeaveConsequencePolicy()
        {

        }

        private clsDALLeaveConsequencePolicy DALLeaveConsequencePolicy
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALLeaveConsequencePolicy == null)
                    this.MobjDALLeaveConsequencePolicy = new clsDALLeaveConsequencePolicy();

                return this.MobjDALLeaveConsequencePolicy;
            }
        }

        public clsDTOLeaveConsequencePolicy DTOLeaveConsequencePolicy
        {
            get { return this.DALLeaveConsequencePolicy.DTOLeaveConsequencePolicy; }
        }
        #endregion Declartions

        #region GetRowNumber
       
        public int GetRowNumber(int intLeaveConsequencePolicyID)
        {
            return DALLeaveConsequencePolicy.GetRowNumber(intLeaveConsequencePolicyID);
        }
        #endregion GetRowNumber

        #region GetRecordCount
        public int GetRecordCount()
        {
            return DALLeaveConsequencePolicy.GetRecordCount();
        }
        #endregion GetRecordCount

        #region LeaveConsequencePolicyMasterSave
        public bool LeaveConsequencePolicyMasterSave()
        {
            bool blnRetValue = false;
            try
            {
                DALLeaveConsequencePolicy.DataLayer.BeginTransaction();
                blnRetValue = DALLeaveConsequencePolicy.LeaveConsequencePolicyMasterSave();
                if (blnRetValue)
                {
                    DALLeaveConsequencePolicy.DeleteLeaveConsequencePolicyDetails();
                    //DALLeaveConsequencePolicy.LeaveConsequencePolicyDetailSave();
                    DALLeaveConsequencePolicy.SalaryPolicyDetailsSave();

                    DALLeaveConsequencePolicy.DataLayer.CommitTransaction();
                }
                else
                {
                    DALLeaveConsequencePolicy.DataLayer.RollbackTransaction();
                }
            }
            catch
            {
                DALLeaveConsequencePolicy.DataLayer.RollbackTransaction();
                throw;
            }
            return blnRetValue;

        }
        #endregion LeaveConsequencePolicyMasterSave

        #region DisplayLeaveConsequencePolicy
        
        public DataTable DisplayLeaveConsequencePolicy(int intRowNumber)
        {
            return DALLeaveConsequencePolicy.DisplayLeaveConsequencePolicy(intRowNumber);
        }
        #endregion DisplayLeaveConsequencePolicy

        #region DisplayAddDedDetails
       
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            return DALLeaveConsequencePolicy.DisplayAddDedDetails(iPolicyID, iType);
        }
        #endregion DisplayAddDedDetails

        #region DeleteLeaveConsequencePolicy
        public bool DeleteLeaveConsequencePolicy()
        {
            return DALLeaveConsequencePolicy.DeleteLeaveConsequencePolicy();
        }
        #endregion DeleteLeaveConsequencePolicy

        #region GetAdditionDeductions
        public DataTable GetAdditionDeductions()
        {
            return DALLeaveConsequencePolicy.GetAdditionDeductions();
        }
        #endregion GetAdditionDeductions

        #region CheckDuplicate
       
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            return DALLeaveConsequencePolicy.CheckDuplicate(iPolicyID, strPolicyName);
        }
        #endregion CheckDuplicate

        #region PolicyIDExists
       
        public bool PolicyIDExists(int iPolicyID)
        {
            return DALLeaveConsequencePolicy.PolicyIDExists(iPolicyID);
        }
        #endregion PolicyIDExists

        #region FillCombos
     
        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALLeaveConsequencePolicy.FillCombos(saFieldValues);
        }
        #endregion FillCombos

    }
}
