﻿using System;
using System.Collections.Generic;
using System.Data;

namespace MyPayfriend
{ /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : WorkConsequence Policy BLL 
       * ******************************************/

    public class clsBLLWorkConsequencePolicy
    {
        #region Declartions
        
        clsDALWorkConsequencePolicy MobjDALWorkConsequencePolicy = null;

        public clsBLLWorkConsequencePolicy()
        {

        }

        private clsDALWorkConsequencePolicy DALWorkConsequencePolicy
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALWorkConsequencePolicy == null)
                    this.MobjDALWorkConsequencePolicy = new clsDALWorkConsequencePolicy();

                return this.MobjDALWorkConsequencePolicy;
            }
        }

        public clsDTOWorkConsequencePolicy DTOWorkConsequencePolicy
        {
            get { return this.DALWorkConsequencePolicy.DTOWorkConsequencePolicy; }
        }
        #endregion Declartions

        #region GetRowNumber
       
        public int GetRowNumber(int intWorkConsequencePolicyID)
        {
            return DALWorkConsequencePolicy.GetRowNumber(intWorkConsequencePolicyID);
        }
        #endregion GetRowNumber

        #region GetRecordCount
        public int GetRecordCount()
        {
            return DALWorkConsequencePolicy.GetRecordCount();
        }
        #endregion GetRecordCount

        #region WorkConsequencePolicyMasterSave
        public bool WorkConsequencePolicyMasterSave()
        {
            bool blnRetValue = false;
            try
            {
                DALWorkConsequencePolicy.DataLayer.BeginTransaction();
                blnRetValue = DALWorkConsequencePolicy.WorkConsequencePolicyMasterSave();
                if (blnRetValue)
                {
                    DALWorkConsequencePolicy.DeleteWorkConsequencePolicyDetails();
                    //DALWorkConsequencePolicy.WorkConsequencePolicyDetailSave();
                    DALWorkConsequencePolicy.SalaryPolicyDetailsSave();

                    DALWorkConsequencePolicy.DataLayer.CommitTransaction();
                }
                else
                {
                    DALWorkConsequencePolicy.DataLayer.RollbackTransaction();
                }
            }
            catch
            {
                DALWorkConsequencePolicy.DataLayer.RollbackTransaction();
                throw;
            }
            return blnRetValue;

        }
        #endregion WorkConsequencePolicyMasterSave

        #region DisplayWorkConsequencePolicy
        
        public DataTable DisplayWorkConsequencePolicy(int intRowNumber)
        {
            return DALWorkConsequencePolicy.DisplayWorkConsequencePolicy(intRowNumber);
        }
        #endregion DisplayWorkConsequencePolicy

        #region DisplayAddDedDetails
       
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            return DALWorkConsequencePolicy.DisplayAddDedDetails(iPolicyID, iType);
        }
        #endregion DisplayAddDedDetails

        #region DeleteWorkConsequencePolicy
        public bool DeleteWorkConsequencePolicy()
        {
            return DALWorkConsequencePolicy.DeleteWorkConsequencePolicy();
        }
        #endregion DeleteWorkConsequencePolicy

        #region GetAdditionDeductions
        public DataTable GetAdditionDeductions()
        {
            return DALWorkConsequencePolicy.GetAdditionDeductions();
        }
        #endregion GetAdditionDeductions

        #region CheckDuplicate
       
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            return DALWorkConsequencePolicy.CheckDuplicate(iPolicyID, strPolicyName);
        }
        #endregion CheckDuplicate

        #region PolicyIDExists
       
        public bool PolicyIDExists(int iPolicyID)
        {
            return DALWorkConsequencePolicy.PolicyIDExists(iPolicyID);
        }
        #endregion PolicyIDExists

        #region FillCombos
     
        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALWorkConsequencePolicy.FillCombos(saFieldValues);
        }
        #endregion FillCombos

    }
}
