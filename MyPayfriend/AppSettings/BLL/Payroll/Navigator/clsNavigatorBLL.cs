﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace MyPayfriend
{
    public class clsNavigatorBLL
    {
        #region Controls Loading Functions

        /// <summary>
        /// Get the alerts based on the alert id
        /// </summary>
        public DataTable GetAlertData(Int32 intAlertID)
        {
            return new clsNavigatorDAL().GetAlertData(intAlertID);
        }

        /// <summary>
        /// Get All Companies and branch
        /// </summary>
        public static DataTable GetAllCompaniesAndBranch()
        {
            return clsNavigatorDAL.GetAllCompaniesAndBranch();
        }

        /// <summary>
        /// Get all companies document types
        /// </summary>
        public static DataTable GetAllCompaniesDocumentType()
        {
            return clsNavigatorDAL.GetAllCompaniesDocumentType();
        }

        /// <summary>
        /// Get all documents by document typeid and companyID
        /// </summary>
        public static DataTable GetAllDocuments(int DocumentTypeID, int CompanyID)
        {
            return clsNavigatorDAL.GetAllDocuments(DocumentTypeID, CompanyID);
        }

        /// <summary>
        /// Get all employees by company id
        /// </summary>
        public static DataTable GetAllEmployeesByCompanyID(int CompanyID,string SearchKey,int WorkStatus,eFilterTypes CurrentFilterType,int FilterBy)
        {
            return clsNavigatorDAL.GetAllEmployeesByCompanyID(CompanyID, SearchKey, WorkStatus, CurrentFilterType,FilterBy);
        }

        /// <summary>
        /// Auto Complete suggestions for employee
        /// </summary>
        public static DataTable GetAutoCompleteSuggestionsForEmployee(int CompanyID)
        {
            return clsNavigatorDAL.GetAutoCompleteSuggestionsForEmployee(CompanyID);
        }
        

        /// <summary>
        /// Get all document by operation type(company or employee)
        /// </summary>
        public static DataTable GetAllDocumentsByOperationTypeID(int OperationTypeID, int ReferenceID, int DocumentTypeID, string DocumentNumber, string SearchKey)
        {
            return clsNavigatorDAL.GetAllDocumentsByOperationTypeID(OperationTypeID, ReferenceID, DocumentTypeID, DocumentNumber, SearchKey);
        }

        /// <summary>
        /// Get all document types for the selected operation type
        /// </summary>
        public static DataTable GetAllDocumentTypesByOperationTypeID(int OperationTypeID, int ReferenceID, int DocumentTypeID, string DocumentNumber, string SearchKey)
        {
            return clsNavigatorDAL.GetAllDocumentTypesByOperationTypeID(OperationTypeID, ReferenceID, DocumentTypeID, DocumentNumber, SearchKey);
        }

        /// <summary>
        /// Get all the attached documents of the current document
        /// </summary>
        public static DataTable GetAttachedDocuments(int OperationTypeID, int ReferenceID, int DocumentTypeID, int DocumentID)
        {
            return clsNavigatorDAL.GetAttachedDocuments(OperationTypeID, ReferenceID, DocumentTypeID, DocumentID);
        }

        #endregion

        #region Webpage functions

        /// <summary>
        /// Employee information web page
        /// </summary>
        public static DataSet GetEmployeeWebPage(int EmployeeID)
        {
            return clsNavigatorDAL.GetEmployeeReport(EmployeeID);
        }

        /// <summary>
        /// Employee work policy web page
        /// </summary>
        public static DataSet GetWorkPolicyWebPage(int EmployeeID)
        {
            return clsNavigatorDAL.GetWorkPolicyWebPage(EmployeeID);
        }

        /// <summary>
        /// Employee leave structure web page
        /// </summary>
        public static DataSet GetLeaveStructureWebPage(int EmployeeID)
        {
            return clsNavigatorDAL.GetLeaveStructureWebPage(EmployeeID);
        }

        /// <summary>
        /// Employee leave policy web page
        /// </summary>
        public static DataSet GetLeavePolicyWebPage(int EmployeeID)
        {
            return clsNavigatorDAL.GetLeavePolicyWebPage(EmployeeID);
        }

        /// <summary>
        /// Employee vacation policy web page
        /// </summary>
        public static DataSet GetVacationPolicyWebPage(int EmployeeID)
        {
            return clsNavigatorDAL.GetVacationPolicyWebPage(EmployeeID);
        }

        /// <summary>
        /// Get all the assigned policies of the employee
        /// </summary>
        public static DataTable GetAllExistingPolicies(int EmployeeID)
        {
            return clsNavigatorDAL.GetAllExistingPolicie(EmployeeID);
        }


        #endregion
    }
}
