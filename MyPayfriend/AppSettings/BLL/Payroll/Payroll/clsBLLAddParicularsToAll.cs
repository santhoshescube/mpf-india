﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    class clsBLLAddParicularsToAll
    {
        private DataLayer MobjDataLayer;
        private clsDALAddParicularsToAll MobjClsDALAddParicularsToAll;

        public clsDTOAddParicularsToAll PobjClsDTOAddParicularsToAll { get; set; }

        public clsBLLAddParicularsToAll()
        {
            this.MobjDataLayer = new DataLayer();
            this.MobjClsDALAddParicularsToAll = new clsDALAddParicularsToAll(this.MobjDataLayer);
            this.PobjClsDTOAddParicularsToAll = new clsDTOAddParicularsToAll();
            this.MobjClsDALAddParicularsToAll.PobjClsDTOAddParicularsToAll = this.PobjClsDTOAddParicularsToAll;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return this.MobjClsDALAddParicularsToAll.FillCombos(saFieldValues);
        }

        public DataTable GetEmployeeDetails(
          int intAdditionDeductionID,
          string strProcessDate,
          int intCompanyID,
          int intDepartmentID)
        {
            return this.MobjClsDALAddParicularsToAll.GetEmployeeDetails(intAdditionDeductionID, strProcessDate, intCompanyID, intDepartmentID);
        }

        public bool SaveEmployeeDetails()
        {
            bool flag;
            try
            {
                this.MobjDataLayer.BeginTransaction();
                flag = this.MobjClsDALAddParicularsToAll.SaveEmployeeDetails();
                this.MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                this.MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return flag;
        }

        public DataTable GetEmployees(
          int GradeID,
          int DesignationID,
          int DepartmentID,
          int BusinessUnitID)
        {
            return this.MobjClsDALAddParicularsToAll.GetEmployees(GradeID, DesignationID, DepartmentID, BusinessUnitID);
        }

        public bool SaveSalaryAmendment()
        {
            return this.MobjClsDALAddParicularsToAll.SaveSalaryAmendment();
        }

        public bool DeleteSingleRow(int EmployeeID, int AmendmentID, int Particular)
        {
            return this.MobjClsDALAddParicularsToAll.DeleteSingleRow(EmployeeID, AmendmentID, Particular);
        }

        public bool DeleteSalaryAmendment()
        {
            return this.MobjClsDALAddParicularsToAll.DeleteSalaryAmendment();
        }

        public DataSet DisplaySalaryAmendment()
        {
            return this.MobjClsDALAddParicularsToAll.DisplaySalaryAmendment();
        }

        public DataTable FillGrid(int DesignationID, int DepartmentID, int EmployeeID)
        {
            return this.MobjClsDALAddParicularsToAll.FillGrid(DesignationID, DepartmentID, EmployeeID);
        }

        public bool Duplication(int txtAmendmentCodeID, string txtAmendmentCode)
        {
            return this.MobjClsDALAddParicularsToAll.Duplication(txtAmendmentCodeID, txtAmendmentCode);
        }

        public DataTable GetAllEmployee()
        {
            return this.MobjClsDALAddParicularsToAll.GetAllEmployee();
        }

        public DataTable GetAllAddDed()
        {
            return this.MobjClsDALAddParicularsToAll.GetAllAddDed();
        }

        public bool SaveEmployeeDetailsForPayment()
        {
            bool flag;
            try
            {
                this.MobjDataLayer.BeginTransaction();
                flag = this.MobjClsDALAddParicularsToAll.SaveEmployeeDetailsForPayment();
                this.MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                this.MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return flag;
        }
    }
}
