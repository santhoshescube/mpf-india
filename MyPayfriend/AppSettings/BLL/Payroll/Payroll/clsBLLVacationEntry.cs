﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;  
using System.Linq;
using System.Text;
using DTO;
using DAL;
namespace BLL
{
    public class clsBLLVacationEntry
    {
        private DataLayer MobjDataLayer;

        clsDALVacationEntry MobjclsDALVacationEntry;
        clsDTOVacationEntry MobjclsDTOVacationEntry;

        public clsBLLVacationEntry()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALVacationEntry = new clsDALVacationEntry(MobjDataLayer);
            MobjclsDTOVacationEntry = new clsDTOVacationEntry();
            MobjclsDALVacationEntry.DTOVacationEntry = MobjclsDTOVacationEntry;

        }

        public clsDTOVacationEntry clsDTOVacationEntry
        {
            get { return MobjclsDTOVacationEntry; }
            set { MobjclsDTOVacationEntry = value; }
        }

        public System.Data.DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALVacationEntry.FillCombos(saFieldValues);
        }

        public bool SearchVaction(int iRowNum, int iSerachType, string sSerchText, int iTotalCount)
        {
            return MobjclsDALVacationEntry.SearchVaction(iRowNum, iSerachType, sSerchText, iTotalCount);
        }
        public bool DeleteVacationMaster()
        {
            return MobjclsDALVacationEntry.DeleteVacationMaster();
        }
        public bool DeletePaymentDetails()
        {
            return MobjclsDALVacationEntry.DeletePaymentDetails();
        }
        public bool GetVacationMaster(int VacationID)
        {
            return MobjclsDALVacationEntry.GetVacationMaster(VacationID);
        }
        public DataSet GetVacationDetails()
        {
            return MobjclsDALVacationEntry.GetVacationDetails();
        }

        public bool IsVacationPolicyExists()
        {
            return MobjclsDALVacationEntry.IsVacationPolicyExists();
        }

        public bool IsEmployeeInService()
        {
            return MobjclsDALVacationEntry.IsEmployeeInService();
        }
        public bool IsSalaryIsProcessedOrClosed()
        {
            return MobjclsDALVacationEntry.IsSalaryIsProcessedOrClosed();
        }
        public bool IsPendingVacationProcessed()
        {
            return MobjclsDALVacationEntry.IsPendingVacationProcessed();
        }

        public int GetEmpCompanyID()
        {
            return MobjclsDALVacationEntry.GetEmpCompanyID();
        }

        public bool IsVacationAlreadyProcessed(string FromDateStr, string ToDateStr)
        {
            return MobjclsDALVacationEntry.IsVacationAlreadyProcessed(FromDateStr, ToDateStr);
        }

        public bool LastTransferDateCheck()
        {
            return MobjclsDALVacationEntry.LastTransferDateCheck();
        }
        public string GetDateofJoining()
        {
            return MobjclsDALVacationEntry.GetDateofJoining();
        }

        public bool SalaryStructureCheck()
        {
            return MobjclsDALVacationEntry.SalaryStructureCheck();
        }

        public DataTable FillProcessDetails()
        {
            return MobjclsDALVacationEntry.FillProcessDetails();
        }
        public bool SaveVacationMaster()
        {
            return MobjclsDALVacationEntry.SaveVacationMaster();
        }
        public void EmpDetail()
        {
            MobjclsDALVacationEntry.EmpDetail();
        }
        public string GetEligibleCombo(bool blnIsLeavePay)
        {
           return MobjclsDALVacationEntry.GetEligibleCombo(true);
        }
        public string GetEligibleLeavePayDays(bool blnIsLeavePay)
        {
            return MobjclsDALVacationEntry.GetEligibleLeavePayDays(blnIsLeavePay);
        }

        public bool UpdateOverTakenLeaves()
        {
            return MobjclsDALVacationEntry.UpdateOverTakenLeaves();
        }
        public bool chkdupicate(bool MbAddStatus)
        {
            return MobjclsDALVacationEntry.chkdupicate(MbAddStatus);
        }
        public bool IsAddition(int AddDedID)
        {
            return MobjclsDALVacationEntry.IsAddition(AddDedID);
        }
        public bool IsVacationProcessSalaryProcessed(string FromDateStr, string ToDateStr)
        {
            return MobjclsDALVacationEntry.IsVacationProcessSalaryProcessed(FromDateStr, ToDateStr);
        }
        public DataTable EmployeeHistoryDetail()
        {
            return MobjclsDALVacationEntry.EmployeeHistoryDetail();
        }
        public bool CheckLeaveExists()
        {
            return MobjclsDALVacationEntry.CheckLeaveExists();
        }
        public DataSet EmailDetail()
        {
            return MobjclsDALVacationEntry.EmailDetail();
        }
        public long CheckEmployeeSalaryIsProcessed(long lngEmployeeID, DateTime dtDate)
        {
            return MobjclsDALVacationEntry.CheckEmployeeSalaryIsProcessed(lngEmployeeID, dtDate);
        }
        public bool InsertParticulars(int intAdditionDeductionID, decimal decAmount)
        {
            return MobjclsDALVacationEntry.InsertParticulars(intAdditionDeductionID, decAmount);
        }
        public bool DeleteVacationDetailParticular(int intAdditionDeductionID)
        {
            return MobjclsDALVacationEntry.DeleteVacationDetailParticular(intAdditionDeductionID);
        }
        public bool UpdateParticularAmount(decimal decAmount, int intAdditionDeductionID)
        {
            return MobjclsDALVacationEntry.UpdateParticularAmount(decAmount, intAdditionDeductionID);
        }
        public bool CheckEmployeeVacationProcessedAfterDate()
        {
            return MobjclsDALVacationEntry.CheckEmployeeVacationProcessedAfterDate();
        }
        public bool UpdateNetAmount(decimal decNetAmount, decimal decGivenAmount)
        {
            return MobjclsDALVacationEntry.UpdateNetAmount(decNetAmount, decGivenAmount);
        }
        public DataTable GetVacationSearch(int CompanyID, int EmployeeID, DateTime FromDate, DateTime ToDate)
        {
            return MobjclsDALVacationEntry.GetVacationSearch(CompanyID, EmployeeID, FromDate, ToDate);
        }
        public bool AddParticularsToSalary(int AddDedId, decimal AmtAdd, int VacationID)
        {
            return MobjclsDALVacationEntry.AddParticularsToSalary(AddDedId, AmtAdd, VacationID);
        }
        public DataTable getDateType()
        {
            return MobjclsDALVacationEntry.getDateType();
        }

    }
}
