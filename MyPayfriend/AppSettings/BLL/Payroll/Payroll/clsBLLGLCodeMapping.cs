﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLGLCodeMapping
    {
        private clsDALGLCodeMapping MobjclsDALGLCodeMapping;
        private DataLayer MobjDataLayer;
        public clsDTOGLCodeMapping PobjclsDTOGLCodeMapping { get; set; }

        public clsBLLGLCodeMapping()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALGLCodeMapping = new clsDALGLCodeMapping(MobjDataLayer);
            PobjclsDTOGLCodeMapping = new clsDTOGLCodeMapping();
            MobjclsDALGLCodeMapping.objDTOGLCodeMapping = PobjclsDTOGLCodeMapping;
        }

        public DataTable getGLCodeMapping(bool IsSalary)
        {
            return MobjclsDALGLCodeMapping.getGLCodeMapping(IsSalary);
        }

        public bool SaveGLCodeMapping(bool IsSalary)
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALGLCodeMapping.SaveGLCodeMapping(IsSalary);
                MobjDataLayer.CommitTransaction();

            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }
            return blnRetValue;
        }
    }
}
