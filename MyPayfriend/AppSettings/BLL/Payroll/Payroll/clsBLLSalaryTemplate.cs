﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace MyPayfriend
{
   public class clsBLLSalaryTemplate
    {
        private clsDALSalaryTemplate MobjclsDALSalaryTemplate;
        private DataLayer MobjDataLayer;
        public clsDTOSalaryTemplate objclsDTOSalaryTemplate;

        public clsBLLSalaryTemplate()
        {
            objclsDTOSalaryTemplate = new clsDTOSalaryTemplate();
            MobjDataLayer = new DataLayer();
            MobjclsDALSalaryTemplate = new clsDALSalaryTemplate();
            MobjclsDALSalaryTemplate.objDTOSalaryTemplate = objclsDTOSalaryTemplate;
        }
       public DataTable FillCombos(string[] saFieldValues)
       {
           return MobjclsDALSalaryTemplate.FillCombos(saFieldValues);
       }
       public bool SavePolicy()
       {
           return MobjclsDALSalaryTemplate.SavePolicy();
       }
       public bool CheckDuplicate(int SalTempID, string SalTempName)
       {
           return MobjclsDALSalaryTemplate.CheckDuplicate(SalTempID, SalTempName);
       }
       public DataSet FillDetails(int CurrentRecCnt)
       {
           return MobjclsDALSalaryTemplate.FillDetails(CurrentRecCnt);
       }
       public int GetRecordCount()
       {
           return MobjclsDALSalaryTemplate.GetRecordCount();
       }
       public bool DeleteSalaryTemplate()
       {
           return MobjclsDALSalaryTemplate.DeleteSalaryTemplate();
       }
       public int GetRowNumber(int intSalTemplateID)
       {
           return MobjclsDALSalaryTemplate.GetRowNumber(intSalTemplateID);
       }
       public bool GetSalaryTemplateExists(int SalaryTempId)
       {
           return MobjclsDALSalaryTemplate.GetSalaryTemplateExists(SalaryTempId);
       }
    }
}
