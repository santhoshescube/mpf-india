﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLOffDayMark
    {
        clsDALOffDayMark MobjclsDALOffDayMark;
        clsDTOOffDayMark MobjclsDTOOffDayMark;
        private DataLayer MobjDataLayer;

        public clsDTOOffDayMark clsDTOOffDayMark
        {
            get { return MobjclsDTOOffDayMark; }
            set { MobjclsDTOOffDayMark = value; }
        }
        public clsBLLOffDayMark()
        {

            MobjclsDALOffDayMark = new clsDALOffDayMark();
            MobjclsDTOOffDayMark = new clsDTOOffDayMark();

            MobjDataLayer = new DataLayer();
            MobjclsDALOffDayMark.MobjclsDTOOffDayMark = clsDTOOffDayMark;
        }

        public DataTable GetOffDayMarkDetails()
        {
            MobjclsDALOffDayMark.objConnection = MobjDataLayer;
            return MobjclsDALOffDayMark.GetOffDayMarkDetails();
        }

        public bool SaveOffDayMarks()
        {
            MobjclsDALOffDayMark.objConnection = MobjDataLayer;
            return MobjclsDALOffDayMark.SaveOffDayMarks();

        }
        public void DeleteOffDayMarks()
        {
            MobjclsDALOffDayMark.objConnection = MobjDataLayer;
            MobjclsDALOffDayMark.DeleteOffDayMarks();
       }
        public DataTable FillCombos(string sQuery)
        {
            MobjclsDALOffDayMark.objConnection = MobjDataLayer;
            return MobjclsDALOffDayMark.FillCombos(sQuery);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            MobjclsDALOffDayMark.objConnection = MobjDataLayer;
            return MobjclsDALOffDayMark.FillCombos(saFieldValues);
        }
        public bool IsAttendance()
        {
            MobjclsDALOffDayMark.objConnection = MobjDataLayer;
            return MobjclsDALOffDayMark.IsAttendance();
        }
        public DataTable DeleteValid()
        {
            MobjclsDALOffDayMark.objConnection = MobjDataLayer;
            return MobjclsDALOffDayMark.DeleteValid();
        }



    }
}
