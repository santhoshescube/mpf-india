﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyPayfriend
{
    public class clsBLLWorkSheet
    {

        private DataLayer MobjDataLayer;

        clsDALWorkSheet MobjclsDALWorkSheet;
        clsDTOWorkSheet MobjclsDTOWorkSheet;

        public clsBLLWorkSheet()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALWorkSheet = new clsDALWorkSheet(MobjDataLayer);
            MobjclsDTOWorkSheet = new clsDTOWorkSheet();
            MobjclsDALWorkSheet.DTOWorkSheet = MobjclsDTOWorkSheet;

        }

        public clsDTOWorkSheet clsDTOWorkSheet
        {
            get { return MobjclsDTOWorkSheet; }
            set { MobjclsDTOWorkSheet = value; }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALWorkSheet.FillCombos(saFieldValues);
        }
        public bool IsSalaryReleased(int iEmpID, string sDate)
        {
            return MobjclsDALWorkSheet.IsSalaryReleased(iEmpID, sDate);
        }
        public DataTable GetWorkSheetDetails()
        {
            return MobjclsDALWorkSheet.GetWorkSheetDetails();
        }
        public bool SaveWorkSheet()
        {
            return MobjclsDALWorkSheet.SaveWorkSheet();
        }
        public bool DeleteWorkSheet()
        {
            return MobjclsDALWorkSheet.DeleteWorkSheet();
        }
        public bool DeleteWorkSheetEmpty()
        {
            return MobjclsDALWorkSheet.DeleteWorkSheetEmpty();
        }
    }
}
