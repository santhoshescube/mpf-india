﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/*
 * Created By       : Tijo
 * Created Date     : 14 Aug 2013
 * Purpose          : For Employee Deposit
*/
namespace MyPayfriend
{
    public class clsBLLDeposit
    {
        private clsDALDeposit MobjclsDALDeposit;
        private DataLayer MobjDataLayer;
        public clsDTODeposit PobjclsDTODeposit { get; set; }

        public clsBLLDeposit()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALDeposit = new clsDALDeposit(MobjDataLayer);
            PobjclsDTODeposit = new clsDTODeposit();
            MobjclsDALDeposit.objclsDTODeposit = PobjclsDTODeposit;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public long GetRecordCount()
        {
            return MobjclsDALDeposit.GetRecordCount();
        }

        public DataTable DisplayDepositDetails(long lngRowNum)
        {
            return MobjclsDALDeposit.DisplayDepositDetails(lngRowNum);
        }

        public long SaveDepositDetails()
        {
            long lngRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                lngRetValue = MobjclsDALDeposit.SaveDepositDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return lngRetValue;
        }

        public bool DeleteDepositDetails()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALDeposit.DeleteDepositDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataSet GetPrintDepositDetails(long lngDepositID)
        {
            return MobjclsDALDeposit.GetPrintDepositDetails(lngDepositID);
        }

        public long UpdateDepositRefundDetails()
        {
            long lngRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                lngRetValue = MobjclsDALDeposit.UpdateDepositRefundDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return lngRetValue;
        }

        public long getRecordRowNumber(long lngDepositID)
        {
            return MobjclsDALDeposit.getRecordRowNumber(lngDepositID);
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            return MobjclsDALDeposit.CheckEmployeeExistance(lngEmployeeID);
        }

        public string getAttchaedDocumentCount(long lngDepositID)
        {
            return MobjclsDALDeposit.getAttchaedDocumentCount(lngDepositID);
        }
    }
}
