﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/*
 * Created By       : Tijo
 * Created Date     : 19 Aug 2013
 * Purpose          : For Loan Repayment
*/
namespace MyPayfriend
{
    public class clsBLLLoanRepayment
    {
        private clsDALLoanRepayment MobjclsDALLoanRepayment;
        private DataLayer MobjDataLayer;
        public clsDTOLoanRepayment PobjclsDTOLoanRepayment { get; set; }

        public clsBLLLoanRepayment()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALLoanRepayment = new clsDALLoanRepayment(MobjDataLayer);
            PobjclsDTOLoanRepayment = new clsDTOLoanRepayment();
            MobjclsDALLoanRepayment.objclsDTOLoanRepayment = PobjclsDTOLoanRepayment;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public long GetRecordCount()
        {
            return MobjclsDALLoanRepayment.GetRecordCount();
        }

        public DataTable getLoanDetails(int intLoanID)
        {
            return MobjclsDALLoanRepayment.getLoanDetails(intLoanID);
        }

        public long SaveLoanRepaymentMaster(DataTable datTemp)
        {
            long lngRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                lngRetValue = MobjclsDALLoanRepayment.SaveLoanRepaymentMaster(datTemp);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return lngRetValue;
        }

        public long getRecordRowNumber(long lngRepaymentID)
        {
            return MobjclsDALLoanRepayment.getRecordRowNumber(lngRepaymentID);
        }

        public DataTable DisplayLoanRepaymentMaster(long lngRowNum)
        {
            return MobjclsDALLoanRepayment.DisplayLoanRepaymentMaster(lngRowNum);
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            return MobjclsDALLoanRepayment.CheckEmployeeExistance(lngEmployeeID);
        }

        public bool DeleteLoanRepaymentMaster()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALLoanRepayment.DeleteLoanRepaymentMaster();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataSet GetPrintLoanRepaymentDetails(long lngRepaymentID)
        {
            return MobjclsDALLoanRepayment.GetPrintLoanRepaymentDetails(lngRepaymentID);
        }

        public DataTable getLoanInstallmentDetails(int intLoanID, long lngRepaymentID)
        {
            return MobjclsDALLoanRepayment.getLoanInstallmentDetails(intLoanID, lngRepaymentID);
        }

        public int CheckLoanRepaymentExists(long lngRepaymentID, int intLoanID)
        {
            return MobjclsDALLoanRepayment.CheckLoanRepaymentExists(lngRepaymentID, intLoanID);
        }

        public DateTime getLastLoanRepaymentDate(long lngRepaymentID, int intLoanID)
        {
            return MobjclsDALLoanRepayment.getLastLoanRepaymentDate(lngRepaymentID, intLoanID);
        }

        public int CheckSalaryProcessedAgainstEmployee(long lngEmployeeID)
        {
            return MobjclsDALLoanRepayment.CheckSalaryProcessedAgainstEmployee(lngEmployeeID);
        }
        public int CheckSettlementProcessed(long lngEmployeeID)
        {
            return MobjclsDALLoanRepayment.CheckSettlementProcessed(lngEmployeeID);
        }
    }
}
