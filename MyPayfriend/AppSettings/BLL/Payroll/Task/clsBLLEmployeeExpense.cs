﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    public class clsBLLEmployeeExpense
    {
        private clsDALEmployeeExpense MobjclsDALEmployeeExpense;
        private DataLayer MobjDataLayer;
        public clsDTOEmployeeExpense PobjclsDTOEmployeeExpense { get; set; }

        public clsBLLEmployeeExpense()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALEmployeeExpense = new clsDALEmployeeExpense(MobjDataLayer);
            PobjclsDTOEmployeeExpense = new clsDTOEmployeeExpense();
            MobjclsDALEmployeeExpense.objclsDTOEmployeeExpense = PobjclsDTOEmployeeExpense;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public long GetRecordCount()
        {
            return MobjclsDALEmployeeExpense.GetRecordCount();
        }

        public DataTable DisplayExpenseDetails(long lngRowNum)
        {
            return MobjclsDALEmployeeExpense.DisplayExpenseDetails(lngRowNum);
        }

        public long SaveExpenseDetails()
        {
            long lngRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                lngRetValue = MobjclsDALEmployeeExpense.SaveExpenseDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return lngRetValue;
        }

        public bool DeleteExpenseDetails()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALEmployeeExpense.DeleteExpenseDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataSet GetPrintExpenseDetails(long lngEmployeeExpenseID)
        {
            return MobjclsDALEmployeeExpense.GetPrintExpenseDetails(lngEmployeeExpenseID);
        }

        public long getRecordRowNumber(long lngEmployeeExpenseID)
        {
            return MobjclsDALEmployeeExpense.getRecordRowNumber(lngEmployeeExpenseID);
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            return MobjclsDALEmployeeExpense.CheckEmployeeExistance(lngEmployeeID);
        }

        public string getAttchaedDocumentCount(long lngEmployeeExpenseID)
        {
            return MobjclsDALEmployeeExpense.getAttchaedDocumentCount(lngEmployeeExpenseID);
        }

        public long CheckEmployeeSalaryIsProcessed(long lngEmployeeID, DateTime dtDate)
        {
            return MobjclsDALEmployeeExpense.CheckEmployeeSalaryIsProcessed(lngEmployeeID, dtDate);
        }
    }
}
