﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    public class clsBLLEmployeePettyCash
    {
        private clsDALEmployeePettyCash MobjclsDALEmployeePettyCash;
        private DataLayer MobjDataLayer;
        public clsDTOEmployeePettyCash PobjclsDTOEmployeePettyCash { get; set; }

        public clsBLLEmployeePettyCash()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALEmployeePettyCash = new clsDALEmployeePettyCash(MobjDataLayer);
            PobjclsDTOEmployeePettyCash = new clsDTOEmployeePettyCash();
            MobjclsDALEmployeePettyCash.objclsDTOEmployeePettyCash = PobjclsDTOEmployeePettyCash;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public long GetRecordCount()
        {
            return MobjclsDALEmployeePettyCash.GetRecordCount();
        }

        public DataTable DisplayPettyCash(long lngRowNum)
        {
            return MobjclsDALEmployeePettyCash.DisplayPettyCash(lngRowNum);
        }

        public bool SavePettyCash()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALEmployeePettyCash.SavePettyCash();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public bool DeletePettyCash()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALEmployeePettyCash.DeletePettyCash();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataSet DisplayPettyCashDetails(long lngPettyCashID)
        {
            return MobjclsDALEmployeePettyCash.DisplayPettyCashDetails(lngPettyCashID);
        }
    }
}
