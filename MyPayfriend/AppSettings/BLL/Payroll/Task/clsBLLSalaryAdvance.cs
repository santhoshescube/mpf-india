﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/*
 * Created By       : Tijo
 * Created Date     : 14 Aug 2013
 * Purpose          : For Employee Deposit
*/
namespace MyPayfriend
{    
    public class clsBLLSalaryAdvance
    {
        private clsDALSalaryAdvance MobjclsDALSalaryAdvance;
        private DataLayer MobjDataLayer;
        public clsDTOSalaryAdvance PobjclsDTOSalaryAdvance { get; set; }

        public clsBLLSalaryAdvance()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALSalaryAdvance = new clsDALSalaryAdvance(MobjDataLayer);
            PobjclsDTOSalaryAdvance = new clsDTOSalaryAdvance();
            MobjclsDALSalaryAdvance.objclsDTOSalaryAdvance = PobjclsDTOSalaryAdvance;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public long GetRecordCount(long lngEmployeeID)
        {
            return MobjclsDALSalaryAdvance.GetRecordCount(lngEmployeeID);
        }

        public DataSet getSalaryStructureDetails(long lngEmployeeID)
        {
            return MobjclsDALSalaryAdvance.getSalaryStructureDetails(lngEmployeeID);
        }

        public int SaveSalaryAdvanceDetails()
        {
            int intRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                intRetValue = MobjclsDALSalaryAdvance.SaveSalaryAdvanceDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return intRetValue;
        }

        public int getRecordRowNumber(int intSalaryAdvanceID, long lngEmployeeID)
        {
            return MobjclsDALSalaryAdvance.getRecordRowNumber(intSalaryAdvanceID, lngEmployeeID);
        }

        public DataTable DisplaySalaryAdvanceDetails(int intRowNum,long lngEmployeeID)
        {
            return MobjclsDALSalaryAdvance.DisplaySalaryAdvanceDetails(intRowNum, lngEmployeeID);
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            return MobjclsDALSalaryAdvance.CheckEmployeeExistance(lngEmployeeID);
        }

        public bool DeleteSalaryAdvanceDetails()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALSalaryAdvance.DeleteSalaryAdvanceDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataSet GetPrintSalaryAdvanceDetails(int intSalaryAdvanceID)
        {
            return MobjclsDALSalaryAdvance.GetPrintSalaryAdvanceDetails(intSalaryAdvanceID);
        }

        public DataTable getTotalSalaryAdvance(long lngEmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            return MobjclsDALSalaryAdvance.getTotalSalaryAdvance(lngEmployeeID, dtFromDate, dtToDate);
        }

        public long CheckEmployeeSalaryIsProcessed(long lngEmployeeID, DateTime dtDate)
        {
            return MobjclsDALSalaryAdvance.CheckEmployeeSalaryIsProcessed(lngEmployeeID, dtDate);
        }
    }
}
