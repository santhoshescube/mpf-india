﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    class clsBLLUpdateAccurals
    {
         clsDALUpdateAccurals MobjDALUpdateAccurals = null;
         public clsBLLUpdateAccurals() { }
         private clsDALUpdateAccurals DALUpdateAccurals
         {
             get
             {
                 // Create instance of data access layer if null
                 if (this.MobjDALUpdateAccurals == null)
                     this.MobjDALUpdateAccurals = new clsDALUpdateAccurals();

                 return this.MobjDALUpdateAccurals;
             }
         }
         public clsDTOUpdateAccurals clsDTOUpdateAccurals
         {
             get { return this.DALUpdateAccurals.DTOUpdateAccurals; }
         }
         public DataTable GetEmployeeList(int CompanyID,string ToDate)
         {
             return DALUpdateAccurals.GetEmployeeList(CompanyID,ToDate);

         }
         public DataTable isEmployee(int employeeid, string ToDate, int companyid)
         {
             return this.DALUpdateAccurals.isEmployee(employeeid, ToDate, companyid);
         }
         public DataSet AccuralUpdateDetail(int EmployeeID, int CompanyID, string FromDate, string ToDate)
         {
             return MobjDALUpdateAccurals.AccuralUpdateDetail(EmployeeID, CompanyID, FromDate, ToDate);
         }

         public void SaveAccural(int empid,DateTime edate, float leavesal,float etotalsal,float etotalgra,float grtArr,int cmpid,int gdates,float Ticket,int isupdated)
         {
             MobjDALUpdateAccurals.SaveAccurals(empid, edate, leavesal, etotalsal, etotalgra,grtArr, cmpid, gdates, Ticket, isupdated);
         }
    }
}
