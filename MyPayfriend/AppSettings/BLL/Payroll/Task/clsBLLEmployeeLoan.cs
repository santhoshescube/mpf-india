﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLEmployeeLoan
    {
        private clsDALEmployeeLoan MobjclsDALEmployeeLoan;

        public clsDTOEmployeeLoan PobjClsDTOEmployeeLoan;

        private DataLayer MobjDataLayer;

        public clsBLLEmployeeLoan()
        {
            MobjDataLayer = new DataLayer();
            PobjClsDTOEmployeeLoan = new clsDTOEmployeeLoan();
            MobjclsDALEmployeeLoan = new clsDALEmployeeLoan();
            MobjclsDALEmployeeLoan.objDataLayer = MobjDataLayer;
            MobjclsDALEmployeeLoan.objClsDTOEmployeeLoan = PobjClsDTOEmployeeLoan;
        }
       
        public clsDTOEmployeeLoan clsDTOEmployeeLoan
        {
            get { return PobjClsDTOEmployeeLoan; }
            set { PobjClsDTOEmployeeLoan = value; }

        }

        public bool IsLoanNumberExists(int intLoanID, string strLoanNumber, int EmployeeID)
        {
            return MobjclsDALEmployeeLoan.IsLoanNumberExists(intLoanID, strLoanNumber, EmployeeID);
        }

        public bool SaveLoan()
        {
            bool blnStatus = true;
            try
            {
                MobjDataLayer.BeginTransaction();
                MobjclsDALEmployeeLoan.SaveLoan();
                MobjclsDALEmployeeLoan.SaveInstallmentDetails();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                blnStatus = false;
                MobjDataLayer.RollbackTransaction();
            }
            return blnStatus;
        }

        public clsDTOEmployeeLoan GetLoan(int intRecNo, long lngEmployeeID, string strSearchKey)
        {
            return MobjclsDALEmployeeLoan.GetLoan(intRecNo,lngEmployeeID,strSearchKey);
        }

        public bool DeleteLoan()
        {
            bool tBol = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                tBol= MobjclsDALEmployeeLoan.DeleteLoan();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                MobjDataLayer.RollbackTransaction(); 
            }
            return tBol;
        }

        public DataSet GetLoanDetails()
        {
            return MobjclsDALEmployeeLoan.GetLoanDetails();
        }

        public bool IsRepaymentExists()
        {
            return MobjclsDALEmployeeLoan.IsRepaymentExists();
        }

        public DataTable GetPreviousInstallments(int EmployeeID,int LoanID)
        {
            return MobjclsDALEmployeeLoan.GetPreviousInstallments(EmployeeID, LoanID);
        }
        public DataTable GetCompanySetting(int CompanyID)
        {
            return MobjclsDALEmployeeLoan.GetCompanySetting(CompanyID);
        }
        public string  GenerateLoanCode(int intCompanyID)
        {
            return MobjclsDALEmployeeLoan.GenerateLoanCode(intCompanyID);
        }
        public int GetCompanyID(int EmployeeID)
        {
            return MobjclsDALEmployeeLoan.GetCompanyID(EmployeeID);
        }

    }
}
