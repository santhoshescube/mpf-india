﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsBLLEmployeeTransfer
    {
        clsDALEmployeeTransfer MobjclsDALEmployeeTransfer;
        clsDTOEmployeeTransfer MobjclsDTOEmployeeTransfer;

        private DataLayer MobjDataLayer;
        public clsBLLEmployeeTransfer()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALEmployeeTransfer = new clsDALEmployeeTransfer(MobjDataLayer);
            MobjclsDTOEmployeeTransfer = new clsDTOEmployeeTransfer();
            MobjclsDALEmployeeTransfer.PobjclsDTOEmployeeTransfer = MobjclsDTOEmployeeTransfer; 
        }
        public clsDTOEmployeeTransfer clsDTOEmployeeTransfer
        {
            get { return MobjclsDTOEmployeeTransfer; }
            set { MobjclsDTOEmployeeTransfer = value; }
        }
        public System.Data.DataTable LoadCombos(string sQuery)
        {
            //function for getting datatable for filling combo
            return MobjclsDALEmployeeTransfer.LoadCombos(sQuery);
        }
        public System.Data.DataTable FillDetails(string sQuery)
        {
            //function for getting datatable for filling combo
            return MobjclsDALEmployeeTransfer.FillDetails(sQuery);
        }
        public System.Data.DataTable SetEmployeeInfo(int EmployeeID)
        {
            return MobjclsDALEmployeeTransfer.SetEmployeeInfo(EmployeeID);
        }
        public System.Data.DataTable FillPolicyCombo()
        {
            return MobjclsDALEmployeeTransfer.FillPolicyCombo();
        }
        public System.Data.DataTable FillLeavePolicyCombo()
        {
            return MobjclsDALEmployeeTransfer.FillLeavePolicyCombo();
        }
        public bool IsDefaultPolicy()
        {
            return MobjclsDALEmployeeTransfer.IsDefaultPolicy();
        }
        public System.Data.DataTable PaymentReleaseChecking(int EmployeeID, string strTransferDate)
        {
            return MobjclsDALEmployeeTransfer.PaymentReleaseChecking(EmployeeID, strTransferDate);
        }
        public string GetTransferDate(int EmployeeID)
        {
            return MobjclsDALEmployeeTransfer.GetTransferDate(EmployeeID);
        }
        public bool IsLeaveDateExists(int EmployeeID, string strTransferDate)
        {
            return MobjclsDALEmployeeTransfer.IsLeaveDateExists(EmployeeID, strTransferDate);
        }
        public bool IsLoanExists(int EmployeeID)
        {
            return MobjclsDALEmployeeTransfer.IsLoanExists(EmployeeID);
        }
        public int GetStatus(int Mode, int EmployeeID, string strFrom, string strTo)
        {
            return MobjclsDALEmployeeTransfer.GetStatus(Mode, EmployeeID, strFrom, strTo);
        }
        public int GetSalaryDay(int EmployeeID)
        {
            return MobjclsDALEmployeeTransfer.GetSalaryDay(EmployeeID);
        }
        public string EmpDOJValidation(int EmployeeID)
        {
            return MobjclsDALEmployeeTransfer.EmpDOJValidation(EmployeeID);
        }
        public bool PaymentExist(int EmployeeID)
        {
            return MobjclsDALEmployeeTransfer.PaymentExist(EmployeeID);
        }
        public bool IsSalaryExists(int EmployeeID, string FromDateStr)
        {
            return MobjclsDALEmployeeTransfer.IsSalaryExists(EmployeeID, FromDateStr);
        }
        public bool SaveEmployeeTransfer()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALEmployeeTransfer.SaveEmployeeTransfer();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public bool CheckEmployeeCurrencyExchangeRate(int MintTransferTypeID, int intEmployeeCurrencyID, int EmployeeID, int TransferToID, ref int iState, ref string strCompanyCurrency, ref string strEmployeeCurrency) 
        {
            return MobjclsDALEmployeeTransfer.CheckEmployeeCurrencyExchangeRate(MintTransferTypeID, intEmployeeCurrencyID, EmployeeID, TransferToID, ref  iState, ref  strCompanyCurrency, ref strEmployeeCurrency);
        }
        public bool SalaryProcess(int EmployeeID, string strTransferDatem, int GlUserId)
        {
            return MobjclsDALEmployeeTransfer.SalaryProcess(EmployeeID, strTransferDatem, GlUserId);
        }
        public void SalaryRelease(string TransferDate, int EmployeeID, int GlUserId)
        {
             MobjclsDALEmployeeTransfer.SalaryRelease(TransferDate, EmployeeID, GlUserId);
        }
        public bool DeleteAttendance(int intEmployeeID, DateTime dtTransferDate)
        {
            return MobjclsDALEmployeeTransfer.DeleteAttendance(intEmployeeID, dtTransferDate);
        }
        public System.Data.DataTable GetEmployeeInfo(int EmployeeID)
        {
            return MobjclsDALEmployeeTransfer.GetEmployeeInfo(EmployeeID);
        }
        public string GetApprovedDate(int EmployeeID)
        {
            return MobjclsDALEmployeeTransfer.GetApprovedDate(EmployeeID);
        }
        public bool IsTransferRequestExists(int EmployeeID ,int intTransferTypeID)
        {
            return MobjclsDALEmployeeTransfer.IsTransferRequestExists(EmployeeID, intTransferTypeID);
        }

    }
}
