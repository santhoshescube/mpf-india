﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


    public class clsReferneceTableBLL
    {
     
        DataLayer objDataLayer;
        clsReferneceTableDAL objRefTableDAL;

        public clsReferneceTableBLL()
        {
            objDataLayer = new DataLayer();
            this.objRefTableDAL = new clsReferneceTableDAL(objDataLayer);
        }

        public DataTable GetRefernceTable(ReferenceTables refTable)
        {
            return objRefTableDAL.GetRefernceTable(refTable);  
        }

        public DataTable GetVehicleModelByManufacturerID(int ManufacturerID)
        {
            return objRefTableDAL.GetVehicleModelByManufacturerID(ManufacturerID);
        }
    }


