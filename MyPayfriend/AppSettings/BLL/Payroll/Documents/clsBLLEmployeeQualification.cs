﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLEmployeeQualification
    {
        private DataLayer objDataLayer;
        private clsDALEmployeeQualification objclsDALEmployeeQualification;
        public clsDTOEmployeeQualification objclsDTOEmployeeQualification;

        public clsBLLEmployeeQualification()
        {
            objDataLayer = new DataLayer();
            objclsDALEmployeeQualification = new clsDALEmployeeQualification(objDataLayer);
            objclsDTOEmployeeQualification = new clsDTOEmployeeQualification();
            objclsDALEmployeeQualification.objclsDTOEmployeeQualification = objclsDTOEmployeeQualification;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return objclsDALEmployeeQualification.FillCombos(saFieldValues);
        }

        /// <summary>
        /// get no of records
        /// </summary>
        /// <returns>int</returns>
        public int GetRecordCount(long intEmpID, int intQid, string strSearchKey)
        {
            return objclsDALEmployeeQualification.GetRecordCount(intEmpID, intQid, strSearchKey);
        }

        /// <summary>
        /// save or update employee qualification
        /// </summary>
        /// <returns>int</returns>
        public int Save()
        {
            return objclsDALEmployeeQualification.Save();
        }

        /// <summary>
        /// Display employee qualification details
        /// </summary>
        /// <param name="intRowNumber"></param>
        /// <returns>bool</returns>
        public bool DisplayQualificationInfo(int intRowNumber, long intEmpID, int intQid,string strSearchKey)
        {
            return objclsDALEmployeeQualification.DisplayQualificationInfo(intRowNumber,intEmpID,intQid,strSearchKey);
        }
        
        /// <summary>
        /// Deletes employee qualification
        /// </summary>
        /// <returns>bool</returns>
        public bool DeleteEmployeeQualification()
        {
            return objclsDALEmployeeQualification.DeleteEmployeeQualification();
        }

        /// <summary>
        /// get Qualification details for email
        /// </summary>
        /// <returns>datatable</returns>
        public DataSet QualificationDetailsPrintEmail()
        {
            return objclsDALEmployeeQualification.QualificationDetailsPrintEmail();
        }

        public DataTable FillDegree()
        {
            return objclsDALEmployeeQualification.FillDegree();
        }
    }
}
