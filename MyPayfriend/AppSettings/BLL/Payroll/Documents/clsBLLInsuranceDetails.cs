﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data;
using System.Data.SqlClient;


namespace MyPayfriend
{
    public class clsBLLInsuranceDetails:DataLayer
    {

        public clsDTOInsuranceDetails objclsDTOInsuranceDetails  { get; set; }
        private clsDALInsuranceDetails objclsDALInsuranceDetails;
      
        public clsBLLInsuranceDetails()
        {
            objclsDALInsuranceDetails = new clsDALInsuranceDetails();
            objclsDTOInsuranceDetails = new clsDTOInsuranceDetails();
            //objclsconnection = new DataLayer();
            objclsDALInsuranceDetails.objclsDTOInsuranceDetails = objclsDTOInsuranceDetails;
        }

        public clsDTOInsuranceDetails objDTOInsuranceDetails
        {
            get { return objclsDTOInsuranceDetails; }
            set { objclsDTOInsuranceDetails = value; }

        }

        public int SaveInsuranceDetails(bool blnAddStatus)
        {
            return this.objclsDALInsuranceDetails.SaveInsuranceDetails(blnAddStatus);
        }

        public bool CheckDuplication(int intIcardID, string strCardnumber)
        {
            return this.objclsDALInsuranceDetails.CheckDuplication(intIcardID, strCardnumber);
        } 
 
        public int GetRecordCount(int intCmpID,int AssetID,int intIcardID,string strSearchKey)
        {
            return this.objclsDALInsuranceDetails.GetRecordCount(intCmpID, AssetID, intIcardID, strSearchKey);
        }

        public bool DeleteInsuranceDetails()
        {
            return this.objclsDALInsuranceDetails.DeleteInsuranceDetails();
        }

        public DataTable GetInsuranceDetails(int MintRowNumber,int intCmpID,int AssetID, int intIcardID,string SearchKey)
        {
            return this.objclsDALInsuranceDetails.GetInsuranceDetails(MintRowNumber, intCmpID, AssetID,intIcardID, SearchKey);
        }

        public DataSet DisplayInsuranceReport(int intRecId)  
        {
            return this.objclsDALInsuranceDetails.DisplayInsuranceReport(intRecId);
        }

        /// <summary>
        /// Update Insurance card set isrenewed as true
        /// </summary>
        /// <returns></returns>
        public int UpdateInsuranceForRenewal()
        {
            return this.objclsDALInsuranceDetails.UpdateInsuranceForRenewal();
        }
        /// <summary>
        /// Autocomplete for EmployeeFullname/Employee Number/DocumentNumber
        /// </summary>
        /// <returns> datatble format</returns>

        public static DataTable GetAutoCompleteList(string SearchKey,int CompanyID,string FieldName, string TableName,int AssetID)
        {
            return clsDALInsuranceDetails.GetAutoCompleteList(SearchKey, CompanyID, FieldName, TableName,AssetID);
        }
        public static AutoCompleteStringCollection ConvertToAutoCompleteCollection(DataTable dt)
        {
            string[] array = { };

            if (dt != null && dt.Rows.Count > 0)
                array = Array.ConvertAll(dt.Select(), row => (string)row[0]);

            var collection = new AutoCompleteStringCollection();
            collection.AddRange(array);

            return collection;
        }
    }
}
