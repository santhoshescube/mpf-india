﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsBLLLeaseAgreement
    {

        clsDTOLeaseAgreement  objclsDTOLeaseAgreement;
        clsDALLeaseAgreement   objclsDALLeaseAgreement;
        private DataLayer objclsconnection;

        public clsBLLLeaseAgreement()
        {
            objclsDALLeaseAgreement = new clsDALLeaseAgreement();
            objclsDTOLeaseAgreement = new clsDTOLeaseAgreement ();
            objclsconnection = new DataLayer();
            objclsDALLeaseAgreement.objclsDTOLeaseAgreement = objclsDTOLeaseAgreement;
        }

        public clsDTOLeaseAgreement objDTOLeaseAgreement
       {
           get { return objclsDTOLeaseAgreement; }
           set { objclsDTOLeaseAgreement = value; }

       }

        public int SaveLeaseAgreement(bool blnAddStatus)
        {
            return this.objclsDALLeaseAgreement.SaveLeaseAgreement(blnAddStatus);
        }
        public int GetRecordCount(int intCompanyID, int intLeaseAgreementID, string strSearchKey)
        {
            return this.objclsDALLeaseAgreement.GetRecordCount(intCompanyID, intLeaseAgreementID, strSearchKey);
        }
        public bool DeleteLeaseAgreement()
        {
            return this.objclsDALLeaseAgreement.DeleteLeaseAgreement();
        }

        public DataTable GetLeaseAgreementDetails(int MintRowNumber, int intCompanyID, int intLeaseAgreementID, string SearchKey)
        {
            return this.objclsDALLeaseAgreement.GetLeaseAgreementDetails(MintRowNumber, intCompanyID, intLeaseAgreementID, SearchKey);
        }
        public DataSet DisplayLeaseAgreementReport(int intRecId)
        {
            return this.objclsDALLeaseAgreement.DisplayLeaseAgreementReport(intRecId);
        }
        /// <summary>
        /// Update Lease Agreement details for renewal
        /// </summary>
        /// <returns></returns>
        public int UpdateLeaseForRenewal()
        {
            return this.objclsDALLeaseAgreement.UpdateLeaseForRenewal();
        }

    }
}
