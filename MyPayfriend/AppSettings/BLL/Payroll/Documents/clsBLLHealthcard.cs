﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace MyPayfriend
{
    public  class clsBLLHealthcard
    {
        private clsDALHealthCard MobjclsDALHealthCard;
        public clsDTOHealthCard PobjDTOHealthCard { get; set; }

        public clsBLLHealthcard()
        {
            MobjclsDALHealthCard = new clsDALHealthCard();
            PobjDTOHealthCard = new clsDTOHealthCard();
            MobjclsDALHealthCard.PobjDTOHealthCard = PobjDTOHealthCard;
        }

        #region GetRowCount
        public int GetRowCount(long EmployeeID, int HealthCardID, string strSearchKey)
        {
            return MobjclsDALHealthCard.GetRowCount(EmployeeID, HealthCardID, strSearchKey);
        }
        #endregion GetRowCount

        #region GetHealthCardDetails
        public DataTable GetHealthCardDetails(int RowNumber, long EmployeeID, int HealthCardID, string strSearchKey)
        {
            return MobjclsDALHealthCard.GetHealthCardDetails(RowNumber, EmployeeID, HealthCardID, strSearchKey);
        }
        #endregion GetHealthCardDetails

        #region DeleteHealthCard
        public bool DeleteHealthCard(int HealthCardID)
        {
            return MobjclsDALHealthCard.DeleteHealthCard(HealthCardID);
        }
        #endregion DeleteHealthCard        

        public int SaveHealthCardDetails(bool blAddStatus)
        {
            MobjclsDALHealthCard.PobjDTOHealthCard = PobjDTOHealthCard;
            return MobjclsDALHealthCard.SaveHealthCardDetails(blAddStatus);
        }

       
        #region DisplaHealthCardReport
        /// <summary>
        /// Get report for email
        /// </summary>
        /// <param name="intRecId">HealthcardID</param>
        /// <returns>Datatable of healthcard details</returns>
        public DataSet DisplaHealthCardReport(int intRecId)  //Healthcard Report
        {
            return MobjclsDALHealthCard.DisplaHealthCardReport(intRecId);
        }
        #endregion DisplaHealthCardReport

       
        #region AutoCompleteSearch

        /// <summary>
        /// Converts DataTable to AutoCompleteStringCollection.
        /// </summary>
        /// <param name="dt">Datatable to be converted</param>
        /// <returns>string array</returns>
        public AutoCompleteStringCollection ConvertToAutoCompleteCollection(DataTable dt)
        {
            string[] array = { };

            if (dt != null && dt.Rows.Count > 0)
                array = Array.ConvertAll(dt.Select(), row => (string)row[0]);

            var collection = new AutoCompleteStringCollection();
            collection.AddRange(array);

            return collection;
        }

        #endregion AutoCompleteSearch

        public int UpdateHealthCard(int HealthCardID)
        {
            return this.MobjclsDALHealthCard.UpdateHealthCard(HealthCardID);
        }

        public int GetRowNumber(int HealthCardID)
        {
            return this.MobjclsDALHealthCard.GetRowNumber(HealthCardID);
        }
    }
}
