﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace MyPayfriend
{
    public class clsBLLAlertreport
    {
        clsDALAlertReport MobjclsDALAlertreport;
         private DataLayer MobjDataLayer;

         public clsBLLAlertreport()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALAlertreport = new clsDALAlertReport(MobjDataLayer);
           
        }
         public DataTable FillCombos(string[] saFieldValues)
         {
             //function for getting datatable for filling combo
             return MobjclsDALAlertreport.FillCombos(saFieldValues);
         }

         public DataTable FillCombos(string sQuery)
         {
             //function for getting datatable for filling combo
             return MobjclsDALAlertreport.FillCombos(sQuery);
         }
         public DataTable GetAlerts(int intDocTypeID, int intUserID, string strFromDate, string strToDate, int intAlertType)

         {

             return MobjclsDALAlertreport.GetAlerts(intDocTypeID, intUserID, strFromDate, strToDate,intAlertType);
         }
    }
}
