﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
using MyPayfriend;
using System.Windows.Forms;


    /* 
=================================================
Description :	     <Document Transactions>
Modified By :		 <Laxmi>
Modified Date :      <13 Apr 2012>
================================================
*/

    public class clsBLLDocumentMasterNew
    {
        public clsDTODocumentMaster objClsDTODocumentMaster { get; set; }
        clsDALDocumentMasterNew objClsDALDocumentMaster;

        public clsBLLDocumentMasterNew()
        {
            
        }

        //public DataTable FillCombos(string[] saFieldValues)
        //{
        //    if (saFieldValues.Length == 3)
        //    {
        //        using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
        //        {
        //            objCommonUtility.PobjDataLayer = objClsDALDocumentMaster.objConnection;
        //            return objCommonUtility.FillCombos(saFieldValues);
        //        }
        //    }
        //    return null;
        //}

        //public bool GetDocumentInfo()
        //{
        //    return objClsDALDocumentMaster.GetDocumentInfo();
        //}
        //public bool GetExistingDocumentInfo(int PintVendorRefereceID, int intDocumentTypeID)
        //{
        //    return objClsDALDocumentMaster.GetExistingDocumentInfo(PintVendorRefereceID, intDocumentTypeID);
        //}
        //public int SaveDocumentInfo()
        //{
        //    DataTable dtAlert = null;
        //    int iDocID=objClsDALDocumentMaster.SaveDocumentInfo();
        //    if (iDocID > 0 & (objClsDTODocumentMaster.intStatusID == (int)DocDocumentStatus.Receipt))
        //    {
        //        if (this.objClsDTODocumentMaster.strExpiryDate != "")
        //        {
        //            dtAlert = this.SetDocumentAlert();
        //            if (dtAlert != null)
        //            {
        //                this.objClsDALDocumentMaster.DeleteAlert(iDocID);
        //                objClsDALDocumentMaster.SaveAlert(dtAlert);
        //            }
        //        }
        //        else
        //        {
        //            //delete alert
        //            this.objClsDALDocumentMaster.DeleteAlert(iDocID);
        //        }
        //    }

        //    return iDocID;
        //}
        //public int SaveMainDocument()
        //{
        //    DataTable dtAlert = null;
        //    int iDocID = objClsDALDocumentMaster.SaveMainDocument();
        //   return iDocID;
        //}
        //public bool DeleteDocumentInfo()
        //{
        //    return objClsDALDocumentMaster.DeleteDocumentInfo();
        //}

        //public bool CheckDuplicateDocument(int intVendorID)
        //{
        //    return objClsDALDocumentMaster.CheckDuplicateDocument(intVendorID);
        //}
        ////public bool CheckDuplicateDocumentAtSameOperationType(int Tag)
        ////{
        ////    return objClsDALDocumentMaster.CheckDuplicateDocumentAtSameOperationType(Tag);
        ////}
        //public bool CheckDuplicateDocumentDetailsForSameVendor(int intReferenceID,int intDocumentType)
        //{
        //    return objClsDALDocumentMaster.CheckDuplicateDocumentDetailsForSameVendor(intReferenceID, intDocumentType);
        //}
    
        //public int GetDocumentCount(int PintOperationType, long PlngOperationID)
        //{
        //    return objClsDALDocumentMaster.GetDocumentCount(PintOperationType,PlngOperationID);
        //}



        //public DataSet GetDocumentReport()
        //{
        //    return objClsDALDocumentMaster.GetDocumentReport();
        //}

        //public int GetOperationStatus(int PintOperationType, long PlngOperationID)
        //{
        //    return objClsDALDocumentMaster.GetOperationStatus(PintOperationType, PlngOperationID);
        //}
        //public bool GetStatusAndOrderNo()
        //{
        //    return objClsDALDocumentMaster.GetStatusAndOrderNo();
        //}

        //public int[] GetStatusAndOrderNo(int DocumentID,int OperationTypeID)
        //{
        //    return objClsDALDocumentMaster.GetStatusAndOrderNo(DocumentID,OperationTypeID);
        //}


        //public DataTable GetStockDocuments()
        //{
        //    return objClsDALDocumentMaster.GetStockDocuments();
        //}

        //public DataTable GetStockDocuments(int PageSize ,int PageIndex)
        //{
        //    return objClsDALDocumentMaster.GetStockDocuments(PageIndex,PageSize);
        //}

        //public static int  GetTotalCount(int StatusID, int OperationTypeID,int DocumentTypeID)
        //{
        //    return clsDALDocumentMasterNew.GetTotalCount(StatusID, OperationTypeID, DocumentTypeID);  
        //}

        //public DataTable GetStockDocumentDetails()
        //{
        //    return objClsDALDocumentMaster.GetStockDocumentDetails();
        //}

        //public bool DeleteSingleDocument()
        //{
        //    return objClsDALDocumentMaster.DeleteSingleDocument();
        //}
        //public long CheckAlertSetting()
        //{
        //    return objClsDALDocumentMaster.CheckAlertSetting();
        //}
        //private DataTable  SetDocumentAlert()
        //{
        //    string strAlertMsg, strFilter=null;
        //    DataRow[] dtUserRows = null;
        //    DataRow dtNewAlertRow;
        //    DataTable datAlert = null;

        //    DataTable dtAlertTemplates = objClsDALDocumentMaster.GetAlertSetting();

        //    DataTable dtAlertDocs = objClsDALDocumentMaster.GetAlertDocs();

        //    if (dtAlertTemplates == null || dtAlertTemplates.Rows.Count == 0 && dtAlertDocs == null)
        //    {
        //        return null;
        //    }


        //    datAlert = new DataTable();
        //    datAlert.Columns.Add("AlertID", typeof(long));
        //    datAlert.Columns.Add("AlertUserSettingID", typeof(long));
        //    datAlert.Columns.Add("ReferenceID", typeof(long));
        //    datAlert.Columns.Add("StartDate", typeof(string));
        //    datAlert.Columns.Add("AlertMessage", typeof(string));
        //    datAlert.Columns.Add("Status", typeof(int));

         
        //    foreach (DataRow dtRow in dtAlertTemplates.Rows)
        //    {
        //        //strFilter = "RoleID = " + dtRoleRow["RoleID"].ToString();
        //        strFilter += "IsAlertON = true";

        //        dtUserRows = dtAlertTemplates.Select(strFilter);

        //        if (dtUserRows.Count() == 0)
        //        {
        //            dtUserRows = null;
        //            continue;
        //        }

        //        strAlertMsg = dtRow["AlertMessage"].ToString();

        //        strAlertMsg = strAlertMsg.Replace("<OPERATIONTYPE>", dtAlertDocs.Rows[0]["OperationType"].ToString());
        //        strAlertMsg = strAlertMsg.Replace("<REFERENCENO>", dtAlertDocs.Rows[0]["Reference"].ToString());
        //        strAlertMsg = strAlertMsg.Replace("<DOCUMENTTYPE>", dtAlertDocs.Rows[0]["DocumentType"].ToString());
        //        strAlertMsg = strAlertMsg.Replace("<DOCUMENTNO>",  dtAlertDocs.Rows[0]["DocumentNo"].ToString());
        //        strAlertMsg = strAlertMsg.Replace("<EXPIRY>", dtAlertDocs.Rows[0]["Expiry"].ToString());

        //        //foreach (DataRow dtUserRow in dtUserRows)
        //        //{
        //            dtNewAlertRow = datAlert.NewRow();
        //            dtNewAlertRow["AlertID"] = 0;
        //            dtNewAlertRow["AlertUserSettingID"] = dtRow["AlertUserSettingID"];
        //            dtNewAlertRow["ReferenceID"] = this.objClsDTODocumentMaster.intDocumentID ;
        //            dtNewAlertRow["StartDate"] = objClsDTODocumentMaster.strExpiryDate;
        //            dtNewAlertRow["AlertMessage"] = strAlertMsg;
        //            dtNewAlertRow["Status"] = Convert.ToInt32(AlertStatus.Open);
        //            datAlert.Rows.Add(dtNewAlertRow);
        //            dtNewAlertRow = null;
        //        //}

        //        strAlertMsg = "";
        //        strFilter = "";
        //        dtUserRows = null;
        //    }

        //    datAlert.AcceptChanges();
        //    return datAlert;

        //}

        //public int  GetCurStatus() // Opened,Closed,Cancelled
        //{
        //    return objClsDALDocumentMaster.GetCurStatus();
        //}
        //public bool CheckDocumentExists()
        //{
        //    return objClsDALDocumentMaster.CheckDocumentExists();
        //}
        ///// <summary>
        ///// Modified by Ranil Kumar on 14/12/2012
        ///// Delete Alerts on Document deletion or Updation without entering Expiry date
        ///// </summary>
        ///// <param name="ReferenceID">Document ID</param>
        ///// <param name="AlertsettingID"></param>
        ///// <returns>bool</returns>
        //public void DeleteAlert(int ReferenceID)
        //{
        //    this.objClsDALDocumentMaster.DeleteAlert(ReferenceID);
        //}
        //public static bool DeleteDocuments(int OperationTypeID,Int64 ReferenceID)
        //{
        //  return   clsDALDocumentMasterNew.DeleteDocuments(OperationTypeID, ReferenceID);  
        //}

        //public static DateTime  GetServerDate()
        //{
        //    return clsDALDocumentMasterNew.GetServerDate();
        //}

        //public static int GetTotalCount(int OperationTypeID,Int64 ReferenceID)
        //{
        //    return clsDALDocumentMasterNew.GetTotalCount(OperationTypeID,ReferenceID );
        //}
        //public static int GetOperationTypeStatus(int OperationTypeID, Int64 ReferenceID)
        //{
        //    return clsDALDocumentMasterNew.GetOperationTypeStatus(OperationTypeID, ReferenceID);
        //}

        //public static DateTime  GetLastReceiptIssueDate(Int64 DocumentID)
        //{
        //    return clsDALDocumentMasterNew.GetLastReceiptIssueDate(DocumentID);
        //}
        //public static Int32  GetLastOrderNo(Int64 DocumentID)
        //{
        //    return clsDALDocumentMasterNew.GetLastOrderNo(DocumentID);
        //}        

        /*Latest Code from here*/

        public static DataTable  GetAllOperationTypes()
        {
            return clsDALDocumentMasterNew.GetAllOperationTypes();
        }

        public static DataTable GetAllReferenceNumberByOperationTypeID(int OperationTypeID,bool InserviceOnly)
        {
            return clsDALDocumentMasterNew.GetAllReferenceNumberByOperationTypeID(OperationTypeID,InserviceOnly );
        }

        public static DataTable GetAllDocumentTypes()
        {
            return clsDALDocumentMasterNew.GetAllDocumentTypes();
        }

        public static DataTable GetAllOtherDocumentTypes()
        {
            return clsDALDocumentMasterNew.GetAllOtherDocumentTypes();
        }

        public static DataTable GetAllBin()
        {
            return clsDALDocumentMasterNew.GetAllBin();
        }
        public static DataTable GetAllDocumentReceipts()
        {
            return clsDALDocumentMasterNew.GetAllDocumentReceipts();
        }
        public static DataTable GetAllDocumentIssues()
        {
            return clsDALDocumentMasterNew.GetAllDocumentIssues();
        }
        public static DataTable GetAllCustodian(bool InServiceOnly)
        {
            return clsDALDocumentMasterNew.GetAllCustodian(InServiceOnly);
        }

        public static int GetTotalRecordsCount()
        {
            return clsDALDocumentMasterNew.GetTotalRecordsCount();
        }


        public int SaveDocuments()
        {
            try
            {
                int DocumentId = 0;
                objClsDALDocumentMaster = new clsDALDocumentMasterNew();
                objClsDALDocumentMaster.objClsDTODocumentMaster = objClsDTODocumentMaster;
                objClsDALDocumentMaster.BeginTransaction();
                DocumentId = objClsDALDocumentMaster.SaveDocument();
                objClsDALDocumentMaster.CommitTransaction();
                return DocumentId;
            }
            catch (Exception ex)
            {
                objClsDALDocumentMaster.RollbackTransaction();
                return 0;
            }

        }

        public int  SaveDocumentReceiptIssue()
        {
            try
            {

                int OrderNo = 0;
                objClsDALDocumentMaster = new clsDALDocumentMasterNew();
                objClsDALDocumentMaster.objClsDTODocumentMaster = objClsDTODocumentMaster;
                objClsDALDocumentMaster.BeginTransaction();
                OrderNo = objClsDALDocumentMaster.SaveDocumentReceiptIssue();
                objClsDALDocumentMaster.CommitTransaction();
                return OrderNo;
            }
            catch (Exception ex)
            {
                objClsDALDocumentMaster.RollbackTransaction();
                return 0;
            }

        }

        /// <summary>
        /// Retrieves Document Details
        /// </summary>
        /// <param name="CurrentIndex"></param>
        /// <param name="DocumentTypeID"></param>
        /// <param name="DocumentID"></param>
        /// <param name="OperationTypeID"></param>
        /// <param name="ReferenceID"></param>
        /// <param name="PintCompanyID"></param>
        /// <param name="SearchKey"></param>
        /// <returns></returns>
        public static DataTable GetDocumentDetails(int CurrentIndex, int DocumentTypeID, int DocumentID, int OperationTypeID, long ReferenceID, int PintCompanyID, string SearchKey, long PEmpID)
        {
            return clsDALDocumentMasterNew.GetDocumentDetails(CurrentIndex, DocumentTypeID, DocumentID, OperationTypeID, ReferenceID, PintCompanyID, SearchKey, PEmpID);
        }
        //public static bool IsExistsDocumentNumber(int DocumentID, string DocumentNumber, int operationTypeID)
        //{
        //    return clsDALDocumentMasterNew.IsExistsDocumentNumber(DocumentID, DocumentNumber, operationTypeID);
        //}
        public static DateTime  GetCurrentDate()
        {
            return clsDALDocumentMasterNew.GetCurrentDate();
        }
        public static bool IsReceipted(int OperationTypeID, int DocumentTypeID, int DocumentId)
        {
            return clsDALDocumentMasterNew.IsReceipted(OperationTypeID, DocumentTypeID, DocumentId);
        }
        public static int GetBinID(int OperationTypeID, int DocumentTypeID, int DocumentId)
        {
            return clsDALDocumentMasterNew.GetBinID(OperationTypeID, DocumentTypeID, DocumentId);
        }
        public static int GetEmployeeID(int OperationTypeID, int DocumentTypeID, int DocumentId)
        {
            return clsDALDocumentMasterNew.GetEmployeeID(OperationTypeID, DocumentTypeID, DocumentId);
        }
        public static DataTable  GetStockRegisterDocuments(int StatusID,int PageIndex,int PageSize,int OperationTypeID,int DocumentTypeID,string DocumentNumber)
        {
            return clsDALDocumentMasterNew.GetStockRegisterDocuments(StatusID,PageIndex ,PageSize,OperationTypeID,DocumentTypeID ,DocumentNumber);
        }
        public static int GetStockRegisterTotalRecords(int StatusID, int OperationTypeID, int DocumentTypeID, string DocumentNumber)
        {
            return clsDALDocumentMasterNew.GetStockRegisterTotalRecords(StatusID,OperationTypeID,DocumentTypeID ,DocumentNumber );
        }
        public static DataTable  GetAllTransactionsByDocumentID(int OperationTypeID, int DocumentTypeID, int DocumentID)
        {
            return clsDALDocumentMasterNew.GetAllTransactionsByDocumentID(OperationTypeID, DocumentTypeID, DocumentID);
        }

        public static DataTable GetDocumentDetailsByOrderNo(int OperationTypeID, int DocumentTypeID, int DocumentID, int OrderNo)
        {
            return clsDALDocumentMasterNew.GetDocumentDetailsByOrderNo(OperationTypeID, DocumentTypeID, DocumentID,OrderNo );
        }


        public static bool DeleteOtherDocument(int OperationTypeID, int DocumentTypeID, int DocumentID)
        {
            return clsDALDocumentMasterNew.DelteOtherDocument(OperationTypeID, DocumentTypeID, DocumentID);
        }

        public static bool DeleteDocumentReceiptIssue(int OperationTypeID, int DocumentTypeID, int DocumentID,int OrderNo)
        {
            return clsDALDocumentMasterNew.DeleteDocumentReceiptIssue(OperationTypeID, DocumentTypeID, DocumentID,OrderNo);
        }
        public static int GetDocumentCurrentIndex(int DocumentID)
        {
            return clsDALDocumentMasterNew.GetDocumentCurrentIndex(DocumentID);
        }
        public static DataTable  GetAllDocumentNumber(int OperationTypeID,int DocumentTypeID)
        {
            return clsDALDocumentMasterNew.GetAllDocumentNumber(OperationTypeID,DocumentTypeID );
        }

        /// <summary>
        /// Gets Employee/Company based Documents depending on Condition
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <param name="PintCompanyID"></param>
        /// <returns></returns>
        public static Int32 GetFirstDocumentIDByEmployee(long EmployeeID, int PintCompanyID)
        {
            return clsDALDocumentMasterNew.GetFirstDocumentIDByEmployee(EmployeeID, PintCompanyID);
        }    
   
        /// <summary>
        /// Gets Total Record count
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <param name="DocumentID"></param>
        /// <param name="PintCompanyID"></param>
        /// <param name="SearchKey"></param>
        /// <returns></returns>
        public static Int32 GetTotalRecordCount(long EmployeeID, int DocumentID, int PintCompanyID, string SearchKey, int OperationTypeID, long PEmpID, int DocumentTypeID)
        {
            return clsDALDocumentMasterNew.GetTotalRecordCount(EmployeeID, DocumentID, PintCompanyID, SearchKey, OperationTypeID, PEmpID, DocumentTypeID);
        }

        /// <summary>
        /// Updates Renew Status
        /// </summary>
        /// <param name="DocumentID"></param>
        /// <param name="DocumentTypeID"></param>
        /// <returns></returns>
        public static int UpdateDocumentMaster(int DocumentID, int DocumentTypeID)
        {
            return clsDALDocumentMasterNew.UpdateDocumentMaster(DocumentID, DocumentTypeID);
        }

        public DataSet DocumentMasterEmail(int DocumentID)  //Document Master Report
        {
            clsDALDocumentMasterNew objClsDALDocumentMaster = new clsDALDocumentMasterNew();
            return objClsDALDocumentMaster.DocumentMasterEmail(DocumentID);
        }

        #region AutoCompleteSearch

        /// <summary>
        /// Converts DataTable to AutoCompleteStringCollection.
        /// </summary>
        /// <param name="dt">Datatable to be converted</param>
        /// <returns>string array</returns>
        public AutoCompleteStringCollection ConvertToAutoCompleteCollection(DataTable dt)
        {
            string[] array = { };

            if (dt != null && dt.Rows.Count > 0)
                array = Array.ConvertAll(dt.Select(), row => (string)row[0]);

            var collection = new AutoCompleteStringCollection();
            collection.AddRange(array);

            return collection;
        }

        /// <summary>
        /// Autocomplete for EmployeeFullname/Employee Number/DocumentNumber
        /// </summary>
        /// <returns> datatble format</returns>
        public static DataTable GetAutoCompleteList(string SearchKey, int ReferenceID)
        {
            return clsDALDocumentMasterNew.GetAutoCompleteList(SearchKey, ReferenceID);
        }

        #endregion AutoCompleteSearch

        public static Int32 GetDocRequestReferenceExists(int DocumentID, int DocumentTypeID)
        {
            return clsDALDocumentMasterNew.GetDocRequestReferenceExists(DocumentID, DocumentTypeID);
        }
    }

