﻿using System;
using System.Data;
using System.Collections.Generic;



namespace MyPayfriend
{
    public class clsDrivingLicenseBLL:IDisposable
    {
        private clsDrivingLicense objDrivingLicense;
        private DataLayer objDataLayer;
        private clsDrivingLicenseDAL objDrivingLicenseDAL;

        #region Constructor
        public clsDrivingLicenseBLL()
        {
            objDataLayer = new DataLayer();
            objDrivingLicenseDAL = new clsDrivingLicenseDAL(objDataLayer);
        }
        #endregion

        public clsDrivingLicense DrivingLicense { get { return objDrivingLicense; } set { objDrivingLicense = value; } }
        /// <summary>
        /// Inserts new Driving License.
        /// Addnewmode - blnMode is true else false
        /// </summary>
        /// <returns> returns true if License is inserted successfully else returns false. </returns>
        public Int32 InsertDrivingLicense(bool blnMode)
        {
            this.objDrivingLicenseDAL.DrivingLicense = this.objDrivingLicense;
            return this.objDrivingLicenseDAL.InsertDrivingLicense(blnMode);
        }

        /// <summary>
        /// Deletes a Driving License
        /// </summary>
        /// <returns> returns true if License is deleted successfully else returns false. </returns>
        public bool DeleteDrivingLicense()
        {
            this.objDrivingLicenseDAL.DrivingLicense = this.objDrivingLicense;
            return this.objDrivingLicenseDAL.DeleteDrivingLicense();

        }

        /// <summary>
        /// Get reference table data 
        /// </summary>
        /// <returns> returns dataset </returns>
        public DataSet GetReferenceTableData()
        {
            return this.objDrivingLicenseDAL.GetReferenceTableData();

        }

        /// <summary>
        /// Get licence details by row index
        /// </summary>
        /// <returns> returns true if get the data otherwise false </returns>
        public bool GetLicenseByRowIndex(int RowNumber, string strSearchKey)
        {
            objDrivingLicenseDAL.DrivingLicense = this.DrivingLicense;
            this.DrivingLicense = this.objDrivingLicenseDAL.GetLicenseByRowIndex(RowNumber,strSearchKey);
            return (DrivingLicense != null);
        }
        public bool GetLicenseByLicenseID(int IntLicenseID)
        {
            objDrivingLicenseDAL.DrivingLicense = this.DrivingLicense;
            this.DrivingLicense = this.objDrivingLicenseDAL.GetLicenseByLicenseID(IntLicenseID);
            return (DrivingLicense != null);
        }
        public DataSet GetEmployeeLicenseDetails()
        {
            this.objDrivingLicenseDAL.DrivingLicense = this.objDrivingLicense;
            return this.objDrivingLicenseDAL.GetEmployeeLicenseDetails();
        }
        public int GetEmployeeID(int IntLicenseID)
        {
            return this.objDrivingLicenseDAL.GetEmployeeID(IntLicenseID);
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            return objDrivingLicenseDAL.FillCombos(sarFieldValues);
        }
        /// <summary>
        /// Update Driving license isrenewed as true
        /// </summary>
        /// <returns></returns>
        public int UpdateLicenseForRenewal()
        {
            return this.objDrivingLicenseDAL.UpdateLicenseForRenewal();
        }
        public int GetRecordCount(long intEmployeeID, int intLicenseID, string strSearchKey)
        {
            return this.objDrivingLicenseDAL.GetRecordCount(intEmployeeID, intLicenseID, strSearchKey);
        }
        public DataTable GetRefernceTable(ReferenceTables refTable)
        {
            return objDrivingLicenseDAL.GetRefernceTable(refTable);
        }

         #region Dispose Members

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose object if it has not been disposed.
                if (!this.disposed)
                {
                    this.objDrivingLicense.Dispose();
                    this.objDrivingLicenseDAL.Dispose();
                    this.objDataLayer = null;
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~clsDrivingLicenseBLL()
        {
            Dispose(false);
        }
        #endregion
    }
}
