﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
   public class clsBLLTradeLicense
    {
        clsDTOTradeLicense  objclsDTOTradeLicense;
        clsDALTradeLicense  objclsDALTradeLicense;
        private DataLayer objclsconnection;

        public clsBLLTradeLicense()
        {
            objclsDALTradeLicense = new clsDALTradeLicense ();
            objclsDTOTradeLicense = new clsDTOTradeLicense ();
            objclsconnection = new DataLayer();
            objclsDALTradeLicense.objclsDTOTradeLicense = objclsDTOTradeLicense;
        }

        public clsDTOTradeLicense objDTOTradeLicense
       {
           get { return objclsDTOTradeLicense ; }
           set { objclsDTOTradeLicense = value; }

       }

        public int SaveTradeLicenseDetails(bool blnAddStatus)
        {
            return this.objclsDALTradeLicense.SaveTradeLicenseDetails(blnAddStatus);
        }
        public int GetRecordCount(int intCompanyID, int intTradeLicenseID, string strSearchKey)
        {
            return this.objclsDALTradeLicense.GetRecordCount(intCompanyID, intTradeLicenseID, strSearchKey);
        }
        /// <summary>
        /// Update Trade license details for renewal
        /// </summary>
        /// <returns></returns>
        public int UpdateTradeLicenseForRenewal()
        {
            return this.objclsDALTradeLicense.UpdateTradeLicenseForRenewal();
        }

        public bool CheckDuplication()
        {
            return this.objclsDALTradeLicense.CheckDuplication();
        }
        public bool DeleteTradeLicense()
        {
            return this.objclsDALTradeLicense.DeleteTradeLicense();
        }

        public DataTable GetTradeLicenseDetails(int MintRowNumber, int intCompanyID, int intLicenseID, string SearchKey)
        {
            return this.objclsDALTradeLicense.GetTradeLicenseDetails(MintRowNumber, intCompanyID, intLicenseID, SearchKey);
        }
        public DataSet DisplayTradeLicenseReport(int intRecId) 
        {
            return this.objclsDALTradeLicense.DisplayTradeLicenseReport(intRecId);
        }
     
        /// <summary>
        /// Autocomplete for EmployeeFullname/Employee Number/DocumentNumber
        /// </summary>
        /// <returns> datatble format</returns>

        public static DataTable GetAutoCompleteList(string SearchKey, int EmployeeID, string FieldName, string TableName)
        {
            return clsDALTradeLicense.GetAutoCompleteList(SearchKey, EmployeeID, FieldName, TableName);
        }
        public static AutoCompleteStringCollection ConvertToAutoCompleteCollection(DataTable dt)
        {
            string[] array = { };

            if (dt != null && dt.Rows.Count > 0)
                array = Array.ConvertAll(dt.Select(), row => (string)row[0]);

            var collection = new AutoCompleteStringCollection();
            collection.AddRange(array);

            return collection;
        }
    }
}
