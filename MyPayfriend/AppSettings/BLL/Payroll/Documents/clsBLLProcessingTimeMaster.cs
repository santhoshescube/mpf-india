﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsBLLProcessingTimeMaster
    {
        clsDALProcessingTimeMaster MobjclsDALProcessingTimeMaster = null;
        public clsBLLProcessingTimeMaster() { }

        private clsDALProcessingTimeMaster DALProcessingTimeMaster//clsDALOvertimePolicy DALOvertimePolicy
        {
            get
            {
                if (this.MobjclsDALProcessingTimeMaster == null)
                    this.MobjclsDALProcessingTimeMaster = new clsDALProcessingTimeMaster();

                return this.MobjclsDALProcessingTimeMaster;
            }
        }

        public clsDTOProcessingTimeMaster DTOProcessingTimeMaster
        {
            get
            {

                return this.DALProcessingTimeMaster.DTOProcessingTimeMaster;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALProcessingTimeMaster.FillCombos(saFieldValues);
        }
        public DataTable FillGrid()
        {
            return DALProcessingTimeMaster.FillGrid();
        }
        public bool docProcessingTime()
        {
            return DALProcessingTimeMaster.docProcessingTime();
        }
        public DataTable FillAlertTable()
        {
            return DALProcessingTimeMaster.FillAlertTable();
        }
       
        public bool UpdateAlert()
        {
            return DALProcessingTimeMaster.UpdateAlert();
        }
    }
}
