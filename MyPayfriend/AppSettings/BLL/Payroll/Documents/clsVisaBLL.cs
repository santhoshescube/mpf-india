﻿using System;
using System.Data;

    public class clsVisaBLL : IDisposable
    {
         #region Private Variables

        private clsVisa objVisa;
        private DataLayer objDataLayer;
        private clsVisaDAL objVisaDAL;

        #endregion

        #region Constructor
        public clsVisaBLL()
        {
            objDataLayer = new DataLayer();
            objVisaDAL = new clsVisaDAL(objDataLayer);
        }
        #endregion

#region functions


        public clsVisa Visa { get { return objVisa; } set { objVisa = value; } }

        /// <summary>
        /// Inserts new Visa.
        /// </summary>
        /// <returns> returns true if Visa is inserted successfully else returns false. </returns>
        public Int32 InsertVisa()
        {
            this.objVisaDAL.Visa = this.Visa;
            return this.objVisaDAL.InsertVisa();
        }

        /// <summary>
        /// Updates a Visa
        /// </summary>
        /// <returns> returns true if Visa is upddated successfully else returns false. </returns>
        public bool UpdateVisa()
        {
            this.objVisaDAL.Visa = this.Visa;
            return this.objVisaDAL.UpdateVisa();
        }

        /// <summary>
        /// Deletes a Visa
        /// </summary>
        /// <returns> returns true if Visa is deleted successfully else returns false. </returns>
        public bool DeleteVisa()
        {
            this.objVisaDAL.Visa = this.Visa;
            return this.objVisaDAL.DeleteVisa();
        }

        /// <summary>
        /// Checks whether registration number is already exist in the database.
        /// </summary>
        /// <remarks>
        /// This validation is neccessary to avoid multiple Visas with same registration number.
        /// </remarks>
        /// <param name="mode">Specifies the mode (Insert/Update) in which validation has to be done</param>
        /// <returns> returns true if registration number exist in the database else false. </returns>
        public bool IsVisaNumberExists(Mode mode)
        {
            this.objVisaDAL.Visa = this.Visa;
            return this.objVisaDAL.IsVisaNumberExists(mode);
        }

        /// <summary>
        /// Gets all data needed to fill ComboBoxes in Visa form.
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetReferenceTableData()
        {
            return this.objVisaDAL.GetReferenceTableData();
        }
        public int GetEmployeeID(int IntVisaID)
        {
            return objVisaDAL.GetEmployeeID(IntVisaID);
        }

        public DataSet GetEmployeeVisaReport()
        {
            return this.objVisaDAL.GetEmployeeVisaReport();
        }        

       
        /// <summary>
        /// Returns true if Issue Date is smaller than Date of Birth of the employee otherwise false.
        /// </summary>
        /// <returns></returns>
        public bool IsValidIssueDate(DateTime IssueDate, int EmployeeID)
        {
            return this.objVisaDAL.IsValidIssueDate(IssueDate, EmployeeID);
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {

            return objVisaDAL.FillCombos(sarFieldValues);

        }

        public int GetRecordCount(string strSearchKey, long iEmployeeId, int iVisaId)
        {
            return this.objVisaDAL.GetRecordCount(strSearchKey, iEmployeeId, iVisaId);
        }
        /// <summary>
        /// get visadetails by search criteria
        /// </summary>
        /// <param name="iVisaId"></param>
        /// <param name="iEmployeeId"></param>
        /// <param name="strSearchKey"></param>
        /// <returns></returns>
        public bool GetVisaDetails(int iVisaId, string strSearchKey, int iRowIndex)
        {
            this.objVisaDAL.Visa = this.Visa;
            // Get new visa object from DAL
            this.Visa = this.objVisaDAL.GetVisaDetails(iVisaId, strSearchKey,iRowIndex);
            return (Visa != null);
        }

        public bool Renew()
        {
            this.objVisaDAL.Visa = this.Visa;
            return this.objVisaDAL.RenewVisa();
        }
      
#endregion


        #region Dispose Members

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose object if it has not been disposed.
                if (!this.disposed)
                {
                    this.objVisa.Dispose();
                    this.objVisaDAL.Dispose();
                    this.objDataLayer = null;
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~clsVisaBLL()
        {
            Dispose(false);
        }
        #endregion
    }

