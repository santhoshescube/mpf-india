﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace MyPayfriend
{
    public class clsBLLEmiratesCard
    {
        clsDALEmiratesCard objDALEmiratesCard;
        public clsDTOEmiratesCard objDTOEmiratesCard { get; set; }
       
        public bool DeleteEmiratesCardInfo()
        {
            return this.objDALEmiratesCard.DeleteEmiratesCardInfo();
        }

        public int GetRowNumber()
        {
            return this.objDALEmiratesCard.GetRowNumber();
        }

        public int DisplayRecordCount(string strSearchKey)
        {
            return this.objDALEmiratesCard.DisplayRecordCount(strSearchKey);
        }

        public bool DisplayEmiratesCardInfo(int iRecordID, long empId,int PiEmCardID,string strSearchKey)
        {
            return this.objDALEmiratesCard.DisplayEmiratesCardInfo(iRecordID, empId, PiEmCardID, strSearchKey);
        }

        public bool SaveEmiratesCardInfo(bool bAddStatus)
        {
            return this.objDALEmiratesCard.SaveEmiratesCardInfo(bAddStatus);
        }

        public bool CheckDateofJoining()
        {
            return this.objDALEmiratesCard.CheckDateofJoining();
        }        
       
        public DataSet GetEmployeeEmirateHealthCardReport()
        {
            return objDALEmiratesCard.GetEmployeeEmirateHealthCardReport();
        }
        public clsBLLEmiratesCard()
        {
            objDTOEmiratesCard = new clsDTOEmiratesCard();
            objDALEmiratesCard = new clsDALEmiratesCard();
            objDALEmiratesCard.objDTOEmiratesCard = objDTOEmiratesCard;
        }
        public DataTable FillCombos(string[] sarFieldValues)
        {

            return objDALEmiratesCard.FillCombos(sarFieldValues);

        }

        /// <summary>
        /// Converts DataTable to AutoCompleteStringCollection.
        /// </summary>
        /// <param name="dt">Datatable to be converted</param>
        /// <returns>string array</returns>
        public AutoCompleteStringCollection ConvertToAutoCompleteCollection(DataTable dt)
        {
            string[] array = { };

            if (dt != null && dt.Rows.Count > 0)
                array = Array.ConvertAll(dt.Select(), row => (string)row[0]);

            var collection = new AutoCompleteStringCollection();
            collection.AddRange(array);

            return collection;
        }

        public int UpdateEmiratesCardRenewed()
        {
            return this.objDALEmiratesCard.UpdateEmiratesCardRenewed();
        }
    }
}
