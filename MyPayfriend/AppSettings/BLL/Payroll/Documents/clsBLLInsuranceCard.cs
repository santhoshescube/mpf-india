﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data;
using System.Data.SqlClient;


namespace MyPayfriend
{
    public class clsBLLInsuranceCard:DataLayer
    {

        clsDTOInsuranceCard objclsDTOInsuranceCard;
        clsDALInsuranceCard objclsDALInsuranceCard;
        private DataLayer objclsconnection;

       public clsBLLInsuranceCard()
        {
            objclsDALInsuranceCard = new clsDALInsuranceCard();
            objclsDTOInsuranceCard = new clsDTOInsuranceCard();
            objclsconnection = new DataLayer();
            objclsDALInsuranceCard.objclsDTOInsuranceCard = objclsDTOInsuranceCard;
        }
       public clsDTOInsuranceCard objDTOInsuranceCard
       {
           get { return objclsDTOInsuranceCard; }
           set { objclsDTOInsuranceCard = value; }

       }

        public int SaveInsuranceDetails(bool blnAddStatus)
        {
            return this.objclsDALInsuranceCard.SaveInsuranceDetails(blnAddStatus);
        }
        public bool CheckDuplication(int intIcardID, string strCardnumber)
        {
            return this.objclsDALInsuranceCard.CheckDuplication(intIcardID,strCardnumber);
        }  
        public int GetRecordCount(long intEmployeeID,int intIcardID,string strSearchKey)
        {
            return this.objclsDALInsuranceCard.GetRecordCount(intEmployeeID, intIcardID, strSearchKey);
        }

        public bool DeleteInsuranceCard()
        {
            return this.objclsDALInsuranceCard.DeleteInsuranceCard();
        }
        public DataTable GetInsuranceCardDetails(int MintRowNumber,long intEmpID, int intIcardID,string SearchKey)
        {
            return this.objclsDALInsuranceCard.GetInsuranceCardDetails(MintRowNumber, intEmpID, intIcardID, SearchKey);
        }
        public DataSet DisplayInsuranceCardReport(int intRecId)  //Healthcard Report
        {
            return this.objclsDALInsuranceCard.DisplayInsuranceCardReport(intRecId);
        }
        /// <summary>
        /// Update Insurance card set isrenewed as true
        /// </summary>
        /// <returns></returns>
        public int UpdateInsuranceForRenewal()
        {
            return this.objclsDALInsuranceCard.UpdateInsuranceForRenewal();
        }
        /// <summary>
        /// Autocomplete for EmployeeFullname/Employee Number/DocumentNumber
        /// </summary>
        /// <returns> datatble format</returns>

        public static DataTable GetAutoCompleteList(string SearchKey,long EmployeeID,string FieldName, string TableName)
        {
            return clsDALInsuranceCard.GetAutoCompleteList(SearchKey, EmployeeID,FieldName, TableName);
        }
        public static AutoCompleteStringCollection ConvertToAutoCompleteCollection(DataTable dt)
        {
            string[] array = { };

            if (dt != null && dt.Rows.Count > 0)
                array = Array.ConvertAll(dt.Select(), row => (string)row[0]);

            var collection = new AutoCompleteStringCollection();
            collection.AddRange(array);

            return collection;
        }
    }
}
