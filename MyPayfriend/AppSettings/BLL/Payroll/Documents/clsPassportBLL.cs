﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;




    public class clsPassportBLL:IDisposable
    {
        private clsDTOPassport objPassport;
        private DataLayer objDataLayer;
        private clsPassportDAL objPassportDAL;

        #region Constructor
        public clsPassportBLL()
        {
            objDataLayer = new DataLayer();
            objPassportDAL = new clsPassportDAL(objDataLayer);
        }
        #endregion

        public clsDTOPassport Passport { get { return objPassport; } set { objPassport = value; } }
        /// <summary>
        /// Inserts new Passport .
        /// </summary>
        /// <returns> returns true if Passport is inserted successfully else returns false. </returns>
        public Int32  InsertPassport()
        {
            this.objPassportDAL.Passport = this.objPassport;
            return this.objPassportDAL.InsertPassport();
        }

        /// <summary>
        /// Updates a  Passport
        /// </summary>
        /// <returns> returns true if Passport is upddated successfully else returns false. </returns>
        public bool UpdatePasspost()
        {
            this.objPassportDAL.Passport = this.objPassport;
             return this.objPassportDAL.UpdatePassport();
        }

        public bool RenewPassport()
        {
            this.objPassportDAL.Passport = this.objPassport;
            return this.objPassportDAL.Renew();
        }
        /// <summary>
        ///check whether passportnumber exists
        /// </summary>
        /// <returns> returns true if  passportnumber exists else returns false. </returns>
        public bool IsPassportNoExists()
        {
            this.objPassportDAL.Passport = this.objPassport;
            return this.objPassportDAL.IsPassportNoExists();
        }
        /// <summary>
        /// Deletes a Driving License
        /// </summary>
        /// <returns> returns true if License is deleted successfully else returns false. </returns>
        public bool DeletePassport()
        {
            this.objPassportDAL.Passport = this.objPassport;
            return this.objPassportDAL.DeletePassport();

        }

        /// <summary>
        /// Get reference table data 
        /// </summary>
        /// <returns> returns dataset </returns>
        public DataSet GetReferenceTableData()
        {
            return this.objPassportDAL.GetReferenceTableData();

        }
        public int GetRecordCount( string strSearchKey,long iEmployeeId,int iPassportId)
        {
            return this.objPassportDAL.GetRecordCount(strSearchKey,iEmployeeId,iPassportId);
        }

        public bool GetPassportDetails(string strSearchKey, int iPassportId, int iRowIndex)
        {
            this.objPassportDAL.Passport = this.objPassport;
            this.Passport = this.objPassportDAL.GetPassportDetails(strSearchKey, iPassportId, iRowIndex);
            return (Passport != null);          
        }
        public static bool IsEmployeeInService(long EmployeeID)
        {
            return clsPassportDAL.EmployeeWorkStatus(EmployeeID);
        }
       
         #region Dispose Members

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose object if it has not been disposed.
                if (!this.disposed)
                {
                    this.objPassport.Dispose();
                    this.objPassportDAL.Dispose();
                    this.objDataLayer = null;
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~clsPassportBLL()
        {
            Dispose(false);
        }
        #endregion
    }

