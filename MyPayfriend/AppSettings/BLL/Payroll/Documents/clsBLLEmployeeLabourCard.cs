﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace MyPayfriend
{
    public class clsBLLEmployeeLabourCard
    {
        #region Object Declaration

        private clsDALEmployeeLabourCard MobjclsDALEmployeeLabourCard;

        public clsDTOEmployeeLabourCard PobjClsDTOEmployeeLabourCard;

        private DataLayer MobjDataLayer;

        #endregion Object Declaration

        public clsBLLEmployeeLabourCard()
        {
            //Object Instatntiation
            MobjDataLayer = new DataLayer();
            PobjClsDTOEmployeeLabourCard = new clsDTOEmployeeLabourCard();
            MobjclsDALEmployeeLabourCard = new clsDALEmployeeLabourCard();
            MobjclsDALEmployeeLabourCard.objDataLayer = MobjDataLayer;
            MobjclsDALEmployeeLabourCard.objClsDTOEmployeeLabourCard = PobjClsDTOEmployeeLabourCard;
        }

        public clsDTOEmployeeLabourCard clsDTOEmployeeLabourCard
        {
            get { return PobjClsDTOEmployeeLabourCard; }
            set { PobjClsDTOEmployeeLabourCard = value; }
        }

        #region Save/Update/Delete

        public Int32 SaveLabourCard()
        {
            return MobjclsDALEmployeeLabourCard.SaveLabourCard();
        }

        public int UpdateLabourCard()
        {
            return this.MobjclsDALEmployeeLabourCard.UpdateLabourCard();
        }

        public bool DeleteLabourCard(int LabourCardID)
        {
            return MobjclsDALEmployeeLabourCard.DeleteLabourCard(LabourCardID);
        }

        #endregion Save/Update/Delete

        #region Retrieve Records

        public clsDTOEmployeeLabourCard GetLaburCardInfo(int intRecordNumber)
        {
            return MobjclsDALEmployeeLabourCard.GetLaburCardInfo(intRecordNumber);
        }

        public clsDTOEmployeeLabourCard DisplayEmployeeLabourCard(int intRecordNumber, long iEmployeeID, int iCardID, string strSearchKey)
        {
            return MobjclsDALEmployeeLabourCard.DisplayEmployeeLabourCard(intRecordNumber, iEmployeeID, iCardID, strSearchKey);
        }

        public DataSet GetReportDetails(int iCardID)
        {
            return MobjclsDALEmployeeLabourCard.GetReportDetails(iCardID);
        }

        #endregion Retrieve Records

        #region Get Count

        public int GetRecordCount(long iEmployeeID, int iCardID, string strSearchKey)
        {
            return MobjclsDALEmployeeLabourCard.GetRecordCount(iEmployeeID, iCardID, strSearchKey);
        }

        public int GetRowNumber()
        {
            return this.MobjclsDALEmployeeLabourCard.GetRowNumber();
        }

        #endregion Get Count        

        #region AutoCompleteSearch

        /// <summary>
        /// Converts DataTable to AutoCompleteStringCollection.
        /// </summary>
        /// <param name="dt">Datatable to be converted</param>
        /// <returns>string array</returns>
        public AutoCompleteStringCollection ConvertToAutoCompleteCollection(DataTable dt)
        {
            string[] array = { };

            if (dt != null && dt.Rows.Count > 0)
                array = Array.ConvertAll(dt.Select(), row => (string)row[0]);

            var collection = new AutoCompleteStringCollection();
            collection.AddRange(array);

            return collection;
        }

        #endregion AutoCompleteSearch
    }
}
