﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLSalesInvoice
    {
        private clsDALSalesInvoice MobjclsDALSalesInvoice;
        private DataLayer MobjDataLayer;
        public clsDTOSalesInvoice PobjclsDTOSalesInvoice { get; set; }

        public clsBLLSalesInvoice()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALSalesInvoice = new clsDALSalesInvoice(MobjDataLayer);
            PobjclsDTOSalesInvoice = new clsDTOSalesInvoice();
            MobjclsDALSalesInvoice.objclsDTOSalesInvoice = PobjclsDTOSalesInvoice;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public bool SaveForm()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALSalesInvoice.SaveForm();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }
            return blnRetValue;
        }

        public bool DeleteForm()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALSalesInvoice.DeleteForm();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }

            return blnRetValue;
        }

        public DataTable GetSearchSalesInvoice(int CompanyID, int ProfitCenterID, int EmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            return MobjclsDALSalesInvoice.GetSearchSalesInvoice(CompanyID, ProfitCenterID, EmployeeID, dtFromDate, dtToDate);
        }

        public DataTable DisplayInfo(int TargetID)
        {
            return MobjclsDALSalesInvoice.DisplayInfo(TargetID);
        }
    }
}