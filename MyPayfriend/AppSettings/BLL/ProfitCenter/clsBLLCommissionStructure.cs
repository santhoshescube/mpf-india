﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLCommissionStructure
    {
        private clsDALCommissionStructure MobjclsDALCommissionStructure;
        private DataLayer MobjDataLayer;
        public clsDTOCommissionStructure PobjclsDTOCommissionStructure { get; set; }

        public clsBLLCommissionStructure()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALCommissionStructure = new clsDALCommissionStructure(MobjDataLayer);
            PobjclsDTOCommissionStructure = new clsDTOCommissionStructure();
            MobjclsDALCommissionStructure.objclsDTOCommissionStructure = PobjclsDTOCommissionStructure;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public bool SaveForm()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALCommissionStructure.SaveForm();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }
            return blnRetValue;
        }

        public bool DeleteForm()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALCommissionStructure.DeleteForm();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }

            return blnRetValue;
        }

        public DataTable GetSearchCommissionStructure()
        {
            return MobjclsDALCommissionStructure.GetSearchCommissionStructure();
        }

        public DataSet DisplayInfo(int CommissionStructureID)
        {
            return MobjclsDALCommissionStructure.DisplayInfo(CommissionStructureID);
        }
    }
}
