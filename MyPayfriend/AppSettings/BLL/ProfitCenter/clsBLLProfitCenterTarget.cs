﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLProfitCenterTarget
    {
        private clsDALProfitCenterTarget MobjclsDALProfitCenterTarget;
        private DataLayer MobjDataLayer;
        public clsDTOProfitCenterTarget PobjclsDTOProfitCenterTarget { get; set; }

        public clsBLLProfitCenterTarget()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALProfitCenterTarget = new clsDALProfitCenterTarget(MobjDataLayer);
            PobjclsDTOProfitCenterTarget = new clsDTOProfitCenterTarget();
            MobjclsDALProfitCenterTarget.objclsDTOProfitCenterTarget = PobjclsDTOProfitCenterTarget;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public bool SaveForm()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALProfitCenterTarget.SaveForm();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }
            return blnRetValue;
        }

        public bool DeleteForm()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALProfitCenterTarget.DeleteForm();
                MobjDataLayer.CommitTransaction();
            }
            catch
            {
                MobjDataLayer.RollbackTransaction();
            }

            return blnRetValue;
        }

        public DataTable GetSearchProfitCenterTarget(int CompanyID, int ProfitCenterID, int EmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            return MobjclsDALProfitCenterTarget.GetSearchProfitCenterTarget(CompanyID, ProfitCenterID, EmployeeID, dtFromDate, dtToDate);
        }

        public DataTable DisplayInfo(int TargetID)
        {
            return MobjclsDALProfitCenterTarget.DisplayInfo(TargetID);
        }

        public bool CheckProfitCenterExists(int TargetID)
        {
            return MobjclsDALProfitCenterTarget.CheckProfitCenterExists(TargetID);
        }
    }
}
