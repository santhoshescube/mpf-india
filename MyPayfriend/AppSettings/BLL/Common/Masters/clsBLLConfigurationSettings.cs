﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,1 Mar 2011>
Description:	<Description,BLL for ConfigurationSettings>
 * 
 * 
 * Modified By :Rajesh.R
 * Modified on :12-Aug-2013
 * 
================================================
*/
namespace MyPayfriend
{
    public class clsBLLConfigurationSettings
    {
        public clsBLLConfigurationSettings()
        {
        }

        #region Methods
        public bool UpdateConfiguration(List<clsDTOConfigurationSettings> DTOConfiguration)
        {
            try
            {
                clsDALConfigurationSettings objDALConfig = new clsDALConfigurationSettings();
                objDALConfig.BeginTransaction();
                objDALConfig.UpdateConfigurationValues(DTOConfiguration);
                objDALConfig.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static  DataTable DisplayConfigurationDetails()
        {
            return clsDALConfigurationSettings.GetConfigurationDetails();
        }
        #endregion
    }
}