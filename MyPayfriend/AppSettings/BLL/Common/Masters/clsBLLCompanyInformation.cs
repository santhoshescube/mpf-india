﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
   Author:		<Author,,Midhun>
   Create date: <Create Date,,16 Feb 2011>
   Description:	<Description,,CompanyInformation BLL Class>
================================================
*/
namespace MyPayfriend
{
    public class clsBLLCompanyInformation
    {
        clsDALCompanyInformation MobjclsDALCompanyInformation;
        clsDTOCompanyInformation MobjclsDTOCompanyInformation;
        private DataLayer MobjDataLayer;

        public clsBLLCompanyInformation()
        {
            //Constructor
            MobjDataLayer = new DataLayer();
            MobjclsDALCompanyInformation = new clsDALCompanyInformation(MobjDataLayer);
            MobjclsDTOCompanyInformation = new clsDTOCompanyInformation();
            MobjclsDALCompanyInformation.PobjclsDTOCompanyInformation = MobjclsDTOCompanyInformation;
        }

        public clsDTOCompanyInformation clsDTOCompanyInformation
        {
            get { return MobjclsDTOCompanyInformation; }
            set { MobjclsDTOCompanyInformation = value; }
        }

        public int RecCountNavigate()
        {
            //function for recount navigator
            return MobjclsDALCompanyInformation.RecCountNavigate();
        }
        public bool IsSalaryStructureExists(int iComID)
        {
            return MobjclsDALCompanyInformation.IsSalaryStructureExists(iComID);
        }
        public DateTime GetFinyearDate(int CompanyID)
        {
            return MobjclsDALCompanyInformation.GetFinyearDate(CompanyID);

        }
        public bool SaveCompany(bool AddStatus,List<int> lstDeletedCompanyAccIds)
        {
            //function for adding and updating company information
            bool blnRetValue = false;

            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALCompanyInformation.DeleteCompanyAccountBankDetails(lstDeletedCompanyAccIds);
                blnRetValue = MobjclsDALCompanyInformation.SaveCompany(AddStatus);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }
        public bool IsBranchExistswithFinyear(DateTime dtFinYear)
        {
            return MobjclsDALCompanyInformation.IsBranchExistswithFinyear(dtFinYear);
        }

        public bool IsBranchExistswithStartDate(DateTime dtStartDAte)
        {
            return MobjclsDALCompanyInformation.IsBranchExistswithStartDate(dtStartDAte);
        }
        public bool DisplayCompanyInfo(int Rownum)
        {
            //function for displaying company information
            return MobjclsDALCompanyInformation.DisplayCompanyInfo(Rownum);
        }
        /// <summary>
        /// Get company currency
        /// </summary>
        /// <returns></returns>
        public int GetCompanyCurrency(int intCompanyID)
        {
            return MobjclsDALCompanyInformation.GetCompanyCurrency(intCompanyID);

        }

        

        public DataTable DisplayBankInformation(int Rownum)
        {
            //function for displaying bank details
            return MobjclsDALCompanyInformation.DisplayBankInformation(Rownum);
        }

        public bool GetCompanyExists(int iComID)
        {
            //function for checking company exists or not
            return MobjclsDALCompanyInformation.GetCompanyExists(iComID);
        }
        public bool GetCompanyExistsInCompanyTable(int iComID)
        {
            //function for checking company exists or not
            return MobjclsDALCompanyInformation.GetCompanyExistsInCompanyTable(iComID);
        }

        public bool CheckCompanyCurrencyUsed(int intCompanyID,int intCurrencyID)
        {
            return MobjclsDALCompanyInformation.CheckCompanyCurrencyUsed(intCompanyID,intCurrencyID);
        }

        public bool DeleteCompany(int Rownum)
        {
            //function for deleting company
            bool blnRetValue = false;

            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALCompanyInformation.DeleteCompany(Rownum);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception)
            {
                MobjDataLayer.RollbackTransaction();
            }
            return blnRetValue;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            //function for getting datatable for filling combo
            return MobjclsDALCompanyInformation.FillCombos(saFieldValues);
        }

        public bool CheckDuplication(bool bAddStatus, string[] saValues, int iId, int iType)
        {
            //function for checking duplication
            return MobjclsDALCompanyInformation.CheckDuplication(bAddStatus, saValues, iId, iType);
        }

       
        public bool CheckAccountNoExists(bool bAddStatus, string[] saValues, int iId, out int iReturnID)
        {
            //function for checking account no exists or not
            return MobjclsDALCompanyInformation.CheckAccountNoExists(bAddStatus, saValues, iId, out iReturnID);
        }
        
        public bool CheckDBSdateExists(string[] saValues, out DateTime dBSdate)
        {
            // function for checking book start date exists
            return MobjclsDALCompanyInformation.CheckDBSdateExists(saValues, out dBSdate);
        }
        public bool CheckDBStartDateExists(string[] saValues, out DateTime dBSdate)
        {
            return MobjclsDALCompanyInformation.CheckDBStartDateExists(saValues, out dBSdate);

        }
        public bool CheckDBFdateExists(string[] saValues, out DateTime dBSdate)
        {
            // function for checking book start date exists
            return MobjclsDALCompanyInformation.CheckDBFdateExists(saValues, out dBSdate);
        }
        public DateTime GetMaxDOJ(int CompanyID)
        {
            return MobjclsDALCompanyInformation.GetMaxDOJ(CompanyID);
        }
        
        public bool IsLeapYearDate(int Year)
        {
            //function for checking leap year
            return MobjclsDALCompanyInformation.IsLeapYearDate(Year);
        }

        public bool CheckValidEmail(string sEmail)
        {
            //function for checking valid email
            return MobjclsDALCompanyInformation.CheckValidEmail(sEmail);
        }

        public DataSet GetCompanyReport()
        {
            return MobjclsDALCompanyInformation.GetCompanyReport();
        }
        public int GetRowNumber()
        {
            //Function for getting Row Number
            return MobjclsDALCompanyInformation.GetRowNumber();
        }
        public bool GetBankBranchExistsInEmployee(int intBankBranchID)
        {
            return MobjclsDALCompanyInformation.GetBankBranchExistsInEmployee(intBankBranchID);
            
        }

       

    }
}
