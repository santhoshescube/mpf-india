﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsBLLWorkLocation
    {
        private DataLayer MobjDataLayer;
        private clsDALWorkLocation MobjClsDALWorkLocation;

        public clsDTOWorkLocation PobjClsDTOWorkLocation { get; set; }
        public clsBLLWorkLocation()
        {
            MobjDataLayer = new DataLayer();
            MobjClsDALWorkLocation = new clsDALWorkLocation(MobjDataLayer);
            PobjClsDTOWorkLocation = new clsDTOWorkLocation();
            MobjClsDALWorkLocation.PobjClsDTOWorkLocation = PobjClsDTOWorkLocation;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            return MobjClsDALWorkLocation.FillCombos(saFieldValues);
        }

        public int SaveWorkLocation(bool AddStatus)// Work Location Save
        {
            int WorkLocationID;

            try
            {
                MobjDataLayer.BeginTransaction();
                this.MobjClsDALWorkLocation.PobjClsDTOWorkLocation = this.PobjClsDTOWorkLocation;
                WorkLocationID=MobjClsDALWorkLocation.SaveWorkLocation(AddStatus);
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return WorkLocationID;
        }

        public bool DeleteWorkLocation()
        {
            bool blnReturnValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnReturnValue = MobjClsDALWorkLocation.DeleteWorkLocation();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnReturnValue;
        }


        public int GetRecordCount()
        {
            return MobjClsDALWorkLocation.GetRecordCount();
        }

        public bool DisplayWorkLocation(int intRowNumber)
        {
            return MobjClsDALWorkLocation.DisplayWorkLocation(intRowNumber);
        }

        public DataSet DisplayWorkLocation()  //Worl Location
        {
            return MobjClsDALWorkLocation.DisplayWorkLocation();
        }

        public DataTable GetCurrentIndex(int WorkLocationID)
        {
            return MobjClsDALWorkLocation.GetCurrentIndex(WorkLocationID);
        }

        public bool LocationAlreadyExists()
        {
            return MobjClsDALWorkLocation.LocationAlreadyExists();
        }

        public bool WorkLocationNameAlreadyExists(int WorkLocationID,string LocationName,int intCompanyID)
        {
            return MobjClsDALWorkLocation.WorkLocationNameAlreadyExists(WorkLocationID, LocationName, intCompanyID);
        }
    }
}
