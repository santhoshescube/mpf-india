﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsBLLDegreeRef
    {
       #region Private Variables

        private clsDTODegreeRef objDTODegreeRef;
        private DataLayer objDataLayer;
        private clsDALDegreeRef  objDegreeRefDAL;

        #endregion

        #region Constructor
        public clsBLLDegreeRef()
        {
            objDataLayer = new DataLayer();
            objDegreeRefDAL = new clsDALDegreeRef(objDataLayer);
        }
        #endregion

        public clsDTODegreeRef DegreeFer { get { return objDTODegreeRef; } set { objDTODegreeRef = value; } }
        /// <summary>
        /// Fill Reference Combo
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="ValueField"></param>
        /// <param name="TextField"></param>
        /// <returns></returns>
        public static DataTable FillCombo(string TableName, string ValueField, string TextField)
        {
            return clsDALDegreeRef.FillCombo(TableName ,ValueField ,TextField );
        }

        public static DataTable FillGrid(int refValue, string RefTableName, string TableName, string RefValueField, string Value1Field, string ValueField, string TextField)
        {
            return clsDALDegreeRef.FillGrid(refValue, RefTableName, TableName, RefValueField, Value1Field, ValueField, TextField);
        }
        /// <summary>
        /// Save function
        /// </summary>
        /// <returns>returns identity </returns>
        public int Save()
        {
            this.objDegreeRefDAL.DegreeRef  = this.DegreeFer;
            return objDegreeRefDAL.Save();

        }
        /// <summary>
        /// Check duplication for Degree
        /// </summary>
        /// <returns> returns true when duplication occurs</returns>
        public bool CheckDuplication()
        {
            this.objDegreeRefDAL.DegreeRef = this.DegreeFer;
            return this.objDegreeRefDAL.CheckDuplication();
        }
        /// <summary>
        /// Deletion of degree
        /// </summary>
        /// <returns>returns affectred Rowcount</returns>
        public bool DeleteDegree()
        {
            this.objDegreeRefDAL.DegreeRef = this.DegreeFer;
            return this.objDegreeRefDAL.DeleteDegree();
        }
    }
}
