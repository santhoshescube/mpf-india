﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
   public class clsBLLReportReference
    {
       clsDALReportReference objDALReportRef;
       public clsBLLReportReference()
       {
           objDALReportRef = new clsDALReportReference();
       }
       public DataSet GetReportDetails()
       {
           return objDALReportRef.GetReportDetails();
       }
    }
}
