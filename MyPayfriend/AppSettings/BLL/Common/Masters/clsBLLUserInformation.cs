﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;


/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,,BLL for UserInformation>
================================================
*/

namespace MyPayfriend
{
    public class clsBLLUserInformation: IDisposable
    {

        clsDTOUserInformation objclsDTOUserInformation;
        clsDALUserInfomation objclsDALUserInfomation;
        private DataLayer objclsconnection;


        public clsBLLUserInformation()
        {
            objclsconnection = new DataLayer();
            objclsDALUserInfomation = new clsDALUserInfomation();
            objclsDTOUserInformation = new clsDTOUserInformation();
            objclsDALUserInfomation.objclsDTOUserInformation = objclsDTOUserInformation;

        }

        public clsDTOUserInformation clsDTOUserInformation
        {
            get { return objclsDTOUserInformation; }
            set { objclsDTOUserInformation = value; }

        }

        public bool SaveUser(bool AddStatus)
        {
            bool blnRetValue = false;
            try
            {
                objclsconnection.BeginTransaction();
                objclsDALUserInfomation.objclsconnection = objclsconnection;
                blnRetValue = objclsDALUserInfomation.SaveUser(AddStatus);
                objclsconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objclsconnection.RollbackTransaction();
                throw ex;

            }
            return blnRetValue;

        }

        public int RecCountNavigate(string strSearchKey)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.RecCountNavigate(strSearchKey);
        }

        public bool DisplayUser(int intRowNumber, string strSearchKey)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            objclsDTOUserInformation = objclsDALUserInfomation.objclsDTOUserInformation;
            return objclsDALUserInfomation.DisplayUser(intRowNumber,strSearchKey);

        }
        public int DeleteUserValidation()
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.DeleteUserValidation();
        }

        public bool DeleteUserInformation()
        {
            bool blnRetValue = false;
            try
            {
                objclsconnection.BeginTransaction();
                objclsDALUserInfomation.objclsconnection = objclsconnection;
                blnRetValue = objclsDALUserInfomation.DeleteUserInformation();
                objclsconnection.CommitTransaction();
                blnRetValue = true;
            }
            catch (Exception ex)
            {
                objclsconnection.RollbackTransaction();
                throw ex;

            }
            return blnRetValue;

        }


        public DataTable FillCombos(string[] sarFieldValues)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.FillCombos(sarFieldValues);
        }


        public bool CheckDuplication(bool blnAddStatus, string strUserName, int intUserID)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.CheckDuplication(blnAddStatus, strUserName, intUserID);
        }
        public DataTable GetRoleCount(bool lnAddStatus)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.GetRoleCount(lnAddStatus);
        }
        public bool CheckValidEmail(string sEmailAddress)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.CheckValidEmail(sEmailAddress);
        }

        public DataTable CheckExistReferences(int intId)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.CheckExistReferences(intId);
        }

        public DataTable GetAutoCompleteList(string strSearchKey)
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.GetAutoCompleteList(strSearchKey);
        }
        public DataTable getUserCompanyDetails()
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.getUserCompanyDetails();
        }
        public int IsEmployeeExists()
        {
            objclsDALUserInfomation.objclsconnection = objclsconnection;
            return objclsDALUserInfomation.IsEmployeeExists();
        }
        #region IDisposable Members

        public void Dispose()
        {
            if (objclsDALUserInfomation != null)
                objclsDALUserInfomation = null;

            if (objclsDTOUserInformation != null)
                objclsDTOUserInformation = null;


        }

        #endregion
    }
}
