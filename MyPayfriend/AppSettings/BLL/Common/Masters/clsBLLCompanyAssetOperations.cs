﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
/*
 * Created By       : Tijo
 * Created Date     : 27 Aug 2013
 * Purpose          : For Asset Handover
*/
namespace MyPayfriend
{
    public class clsBLLCompanyAssetOperations
    {
        private clsDALCompanyAssetOperations MobjclsDALCompanyAssetOperations;
        private DataLayer MobjDataLayer;
        public clsDTOCompanyAssetOperations PobjclsDTOCompanyAssetOperations { get; set; }

        public clsBLLCompanyAssetOperations()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALCompanyAssetOperations = new clsDALCompanyAssetOperations(MobjDataLayer);
            PobjclsDTOCompanyAssetOperations = new clsDTOCompanyAssetOperations();
            MobjclsDALCompanyAssetOperations.objclsDTOCompanyAssetOperations = PobjclsDTOCompanyAssetOperations;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = MobjDataLayer;
                return objCommonUtility.FillCombos(saFieldValues);
            }
        }

        public DataSet getAssetDetails(int intEmpBenefitID, int intCompanyAssetID)
        {
            return MobjclsDALCompanyAssetOperations.getAssetDetails(intEmpBenefitID, intCompanyAssetID);
        }

        public DataTable getEmployeeDetails(int intCompanyID, string strSearchkey)
        {
            return MobjclsDALCompanyAssetOperations.getEmployeeDetails(intCompanyID, strSearchkey);
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            return MobjclsDALCompanyAssetOperations.CheckEmployeeExistance(lngEmployeeID);
        }

        public int SaveAssetIssueReturn()
        {
            int intRetValue = 0;
            try
            {
                MobjDataLayer.BeginTransaction();
                intRetValue = MobjclsDALCompanyAssetOperations.SaveAssetIssueReturn();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return intRetValue;
        }

        public bool DeleteAssetIssueReturn()
        {
            bool blnRetValue = false;
            try
            {
                MobjDataLayer.BeginTransaction();
                blnRetValue = MobjclsDALCompanyAssetOperations.DeleteAssetIssueReturn();
                MobjDataLayer.CommitTransaction();
            }
            catch (Exception ex)
            {
                MobjDataLayer.RollbackTransaction();
                throw ex;
            }
            return blnRetValue;
        }

        public DataTable getEmployeeAssetDetails(long lngEmployeeID)
        {
            return MobjclsDALCompanyAssetOperations.getEmployeeAssetDetails(lngEmployeeID);
        }

        public DataTable getAssetUsageDetails(int intEmpBenefitID)
        {
            return MobjclsDALCompanyAssetOperations.getAssetUsageDetails(intEmpBenefitID);
        }

        public int CheckAssetIsIssued(int intCompanyAssetID, int intAssetStatusID)
        {
            return MobjclsDALCompanyAssetOperations.CheckAssetIsIssued(intCompanyAssetID, intAssetStatusID);
        }

        public DataSet GetPrintAssetIssueReturn(int intEmpBenefitID)
        {
            return MobjclsDALCompanyAssetOperations.GetPrintAssetIssueReturn(intEmpBenefitID);
        }
    }
}
