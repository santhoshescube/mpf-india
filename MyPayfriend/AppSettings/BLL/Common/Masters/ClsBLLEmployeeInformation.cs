﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,22 Feb 2011>
Description:	<Description,,BLL for EmployeeInformation>
================================================
*/
namespace MyPayfriend
{
   public class ClsBLLEmployeeInformation:IDisposable
    {
       clsDTOEmployeeInformation objclsDTOEmployeeInformation;
       clsDALEmployeeInformation objclsDALEmployeeInformation;
       private DataLayer objclsconnection;

        
       public ClsBLLEmployeeInformation()
        {
            objclsDALEmployeeInformation = new clsDALEmployeeInformation();
            objclsDTOEmployeeInformation = new clsDTOEmployeeInformation();
            objclsconnection = new DataLayer();
            objclsDALEmployeeInformation.objclsDTOEmployeeInformation = objclsDTOEmployeeInformation;
        }
       public clsDTOEmployeeInformation clsDTOEmployeeInformation
       {
           get { return objclsDTOEmployeeInformation; }
           set { objclsDTOEmployeeInformation = value; }

       }

       public bool SaveEmployeeInformation(bool AddStatus)
       {
           bool blnRetValue = false;
           try
           {
               objclsconnection.BeginTransaction();
               objclsDALEmployeeInformation.objclsConnection = objclsconnection;
               blnRetValue = objclsDALEmployeeInformation.SaveEmployeeInformation(AddStatus);
               objclsconnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objclsconnection.RollbackTransaction();
               throw ex;

           }
           return blnRetValue;

       }

       public bool SaveLeavePolicySummary()
       {
           
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return  objclsDALEmployeeInformation.SaveLeavePolicySummary();

       }

       public int RecCountNavigate(int CompanyID)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.RecCountNavigate(CompanyID);
       }
      

       public int GetRowNumber()
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.GetRowNumber();
       }

       public int GetCurRowNumber()
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.GetCurRowNumber();
       }

       public DataTable GetLanguage(int iEmployeeID)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.GetLanguage(iEmployeeID);
       }

       public bool DisplayEmployeeInformation(int rowno,int CompanyID)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           objclsDTOEmployeeInformation = objclsDALEmployeeInformation.objclsDTOEmployeeInformation;
           return objclsDALEmployeeInformation.DisplayEmployeeInformation(rowno, CompanyID);

       }

       public bool SearchEmployee(string SearchCriteria,int CompanyID, int RowNum, out int TotalRows)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           objclsDTOEmployeeInformation = objclsDALEmployeeInformation.objclsDTOEmployeeInformation;
           return objclsDALEmployeeInformation.SearchItem(SearchCriteria,CompanyID, RowNum,out TotalRows);
           
       }


       public bool DeleteEmployeeInformation()
       {
           bool blnRetValue = false;
           try
           {
               objclsconnection.BeginTransaction();
               objclsDALEmployeeInformation.objclsConnection = objclsconnection;
               blnRetValue = objclsDALEmployeeInformation.DeleteEmployeeInformation();
               objclsconnection.CommitTransaction();
               blnRetValue = true;
           }
           catch (Exception ex)
           {
               objclsconnection.RollbackTransaction();
               throw ex;

           }
           return blnRetValue;

       }
       public DataTable CheckExistReferences()
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.CheckExistReferences();

       }

       public DataTable FillCombos(string[] sarFieldValues)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.FillCombos(sarFieldValues);
       }

       public bool AttendancetExists(long EmployeeId)
       {
           return objclsDALEmployeeInformation.AttendancetExists(EmployeeId);

       }

      

       public bool CheckDuplication(bool blnAddStatus, string[] sarValues, long intId,int intCompanyID)
       {
           return objclsDALEmployeeInformation.CheckDuplication(blnAddStatus, sarValues, intId, intCompanyID);
       }

       public bool CheckValidEmail(string sEmailAddress)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.CheckValidEmail(sEmailAddress);
       }

       public DataSet GetEmployeeReport()
       {
           return objclsDALEmployeeInformation.GetEmployeeReport();
       }

       public string  GenerateEmployeeCode()
       {
           return objclsDALEmployeeInformation.GenerateEmployeeCode();
       }

       /// <summary>
       /// Converts DataTable to AutoCompleteStringCollection.
       /// </summary>
       /// <param name="dt">Datatable to be converted</param>
       /// <returns>string array</returns>
      
      

       public bool DeleteLeavePolicy()
       {

           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return objclsDALEmployeeInformation.DeleteLeavePolicy();

       }
       public DataTable CheckCarryForwardLeaveExists()
       {
           return this.objclsDALEmployeeInformation.CheckCarryForwardLeaveExists();
       }


       public DataSet UpdateLeaveSummary(int iLeaveTypeID, int iLeavePolicyID)
       {
           return this.objclsDALEmployeeInformation.UpdateLeaveSummary(iLeaveTypeID, iLeavePolicyID);
       }

       public string GetCompanyStartDate(int intCompanyID)
       {
           return this.objclsDALEmployeeInformation.GetCompanyStartDate(intCompanyID);
       }

       public bool CheckIfSettlementExists(long EmployeeId)
       {
           return objclsDALEmployeeInformation.CheckIfSettlementExists(EmployeeId);
       }
       public string GetEmployeeTransferDate(int intCompanyID, long EmployeeId)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return this.objclsDALEmployeeInformation.GetEmployeeTransferDate(intCompanyID, EmployeeId);
       }

       public DataTable GetDesignation(int CompanyID)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return this.objclsDALEmployeeInformation.GetDesignation(CompanyID);
       }

       public DataTable GetReportingto(int intEmployeeID, int intCompanyID)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return this.objclsDALEmployeeInformation.GetReportingto(intEmployeeID,intCompanyID);
       }
       public DataTable GetHOD(int intEmployeeID, int intCompanyID)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return this.objclsDALEmployeeInformation.GetHOD(intEmployeeID, intCompanyID);
       }
       public DataTable GetCompanySetting(int CompanyID)
       {
           objclsDALEmployeeInformation.objclsConnection = objclsconnection;
           return this.objclsDALEmployeeInformation.GetCompanySetting(CompanyID);
       }
       #region IDisposable Members

        public void Dispose()
       {
           if (objclsDALEmployeeInformation != null)
               objclsDALEmployeeInformation = null;

           if (objclsDTOEmployeeInformation != null)
               objclsDTOEmployeeInformation=null;


       }

       #endregion

}
}
