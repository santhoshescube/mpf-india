﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{

   public class clsBLLCompanyAssets
   {
       #region Declaration

        clsDTOCompanyAssets objclsDTOCompanyAssets { get; set; } 
         clsDALCompanyAssets objclsDALCompanyAssets  { get; set; }
         private DataLayer objconnection { get; set; }


         public clsDTOCompanyAssets clsDTOCompanyAssets
         {
             get { return objclsDTOCompanyAssets; }
             set { objclsDTOCompanyAssets = value; }

         }

         #endregion Declaration

       #region Constructor
      
         public clsBLLCompanyAssets()
         {
             objclsDTOCompanyAssets = new clsDTOCompanyAssets();
             objclsDALCompanyAssets = new clsDALCompanyAssets();
             objconnection = new DataLayer();
             objclsDALCompanyAssets.objclsDTOCompanyAssets = objclsDTOCompanyAssets;

         }
      
         #endregion Constructor

       #region FillCombos
         public DataTable FillCombos(string[] sarFieldValues)
         {

             return objclsDALCompanyAssets.FillCombos(sarFieldValues);

         }
         #endregion FillCombos

       #region GetCompanyAssetDetails
         public DataTable GetCompanyAssetDetails()
         {

             return objclsDALCompanyAssets.GetCompanyAssetDetails();
         }
         #endregion GetCompanyAssetDetails

       #region SaveCompanyAssets
         public int SaveCompanyAssets()
         {

             return objclsDALCompanyAssets.SaveCompanyAssets();
         }
         #endregion SaveCompanyAssets

       #region DeleteCompanyAssets
         public bool DeleteCompanyAssets()
         {

             return objclsDALCompanyAssets.DeleteCompanyAssets();
         }
         #endregion DeleteCompanyAssets

       #region GetOtherInfoDetails
         public DataTable GetOtherInfoDetails()
         {
             return objclsDALCompanyAssets.GetOtherInfoDetails();
         }
         #endregion GetOtherInfoDetails
         public int GetAssetCompanyID(int CompanyAssetID) // Get Company for asset
         {
             return objclsDALCompanyAssets.GetAssetCompanyID(CompanyAssetID);
         }


       //Created By Rajesh
       //DEscription :For asset email

         public static DataSet GetCompanyAssetEmail(int CompanyAssetID)
         {
             return clsDALCompanyAssets.GetCompanyAssetEmail(CompanyAssetID); 

         }
    }
}
