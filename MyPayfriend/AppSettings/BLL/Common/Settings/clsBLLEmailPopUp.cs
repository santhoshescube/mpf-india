﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,9 Mar 2011>
Description:	<Description,BLL for EmailPopUp>
================================================
*/
namespace MyPayfriend
{
    public class clsBLLEmailPopUp : IDisposable
    {
        clsDTOEmailPopUp objclsDTOEmailPopUp;
        clsDALEmailPopUp objclsDALEmailPopUp;
        private DataLayer objConnection;

        public clsDTOEmailPopUp clsDTOEmailPopUp
        {
            get { return objclsDTOEmailPopUp; }
            set { objclsDTOEmailPopUp = value; }
        }

        public clsBLLEmailPopUp()
        {
            objclsDTOEmailPopUp = new clsDTOEmailPopUp();
            objclsDALEmailPopUp = new clsDALEmailPopUp();
            objConnection = new DataLayer();
            objclsDALEmailPopUp.objclsDTOEmailPopUp = objclsDTOEmailPopUp;
        }

        public DataTable GetMailSettings()
        {
            objclsDALEmailPopUp.objconnection = objConnection;
            //string Tomailid, string Subject, string Message, string argstrpath, bool bFl
            return objclsDALEmailPopUp.GetMailSettings();
           
        }

        public DataTable FillCombos(string[] sarFieldValues)
        {
            objclsDALEmailPopUp.objconnection = objConnection;
            return objclsDALEmailPopUp.FillCombos(sarFieldValues);
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (objclsDALEmailPopUp != null)
                objclsDALEmailPopUp = null;

            if (objclsDTOEmailPopUp != null)
                objclsDTOEmailPopUp = null;

        }

        #endregion

      
    }
}
