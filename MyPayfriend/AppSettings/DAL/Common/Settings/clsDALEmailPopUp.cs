﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,9 Mar 2011>
Description:	<Description,DAL for EmailPopUp>
================================================
*/

namespace MyPayfriend
{
    public class clsDALEmailPopUp : IDisposable
    {
        ArrayList parameters;
        DataTable dt;

        public clsDTOEmailPopUp objclsDTOEmailPopUp { get; set; }
        public DataLayer objconnection { get; set; }


        // To fill combo

        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objconnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }

        public DataTable GetMailSettings()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@AccountType", 1));
            return objconnection.ExecuteDataTable("spMailSettings", parameters);
        }



        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (parameters != null)
                parameters = null;

        }

        #endregion





    }
}
