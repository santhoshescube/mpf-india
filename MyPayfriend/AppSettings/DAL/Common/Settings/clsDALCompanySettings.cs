﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

/* ===============================================
  Author:		<Author,,Arun>
  Create date: <Create Date,,23 Feb 2011>
  Description:	<Description,,DTO for CompanySettings>
================================================*/
namespace MyPayfriend
{
    public class clsDALCompanySettings : IDisposable
    {
        public clsDTOCompanySettingsMaster objclsDTOCompanySettingsMaster { get; set; }
        public DataLayer objclsConnection { get; set; }
        string STCompanySettingsTransactions = "spCompanySettings";

        public DataTable DisplayCompanysettings(int CompanyID, int iMode, string sConfigurationItem)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", iMode));//mode=6
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@ConfigurationItem", sConfigurationItem));
            return objclsConnection.ExecuteDataTable(STCompanySettingsTransactions, parameters);
        }

        public bool Reset(int intType)  //RESET (A-Advanced,C-ChequeReceipt,P,P,P,R,S,S)
        {
            ArrayList prmcompany = new ArrayList();
            prmcompany.Add(new SqlParameter("@Mode", 11));
            prmcompany.Add(new SqlParameter("CompanyID", intType));

            if (objclsConnection.ExecuteNonQuery(STCompanySettingsTransactions, prmcompany) != 0)
            {
                return true;
            }
            return false;
        }

        //Function to insert all the details  according to company
        public bool Insert(int intID)
        {
            ArrayList prmcompany = new ArrayList();
            prmcompany.Add(new SqlParameter("@Mode", 10));
            prmcompany.Add(new SqlParameter("@CompanyID", intID));

            if (objclsConnection.ExecuteNonQuery(STCompanySettingsTransactions, prmcompany) != 0)
            {
                return true;
            }
            return false;
        }

        public bool SavePro(int SettingsId, int CompanyID, string ConfigurationValue)
        {
            ArrayList prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", 3));  // SAVE USING PROPERTYDESCRIPTOR
            prmCompany.Add(new SqlParameter("@SettingsID", SettingsId));
            prmCompany.Add(new SqlParameter("@CompanyID", CompanyID));
            prmCompany.Add(new SqlParameter("@ConfigurationValue", ConfigurationValue));

            if (objclsConnection.ExecuteNonQuery(STCompanySettingsTransactions, prmCompany) != 0)
            {
                return true;
            }
            return false;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataSet GetCompanySettingsReport(int CompanyID)
        {
            ArrayList prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@Mode", 12));
            prmCompany.Add(new SqlParameter("@CompanyID", CompanyID));
            return objclsConnection.ExecuteDataSet("spCompanySettings", prmCompany);
        }
        public int RecCountNavigate()
        {
            ArrayList prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@iMode", 4));
            return objclsConnection.ExecuteScalar("spPayCompanySettings", prmCompany).ToInt32();
        }

        public DataTable getFormDetails()
        {
            ArrayList prmCompany = new ArrayList();

            prmCompany.Add(new SqlParameter("@iMode", 1));
            prmCompany.Add(new SqlParameter("@iCompanyID", objclsDTOCompanySettingsMaster.iCompanyID ));
            return objclsConnection.ExecuteDataTable("spPayCompanySettings", prmCompany);
        }


        public bool DeleteDetails()
        {
            ArrayList prmCompany = new ArrayList();
            prmCompany.Add(new SqlParameter("@iMode", 3));
            prmCompany.Add(new SqlParameter("@iCompanyID", objclsDTOCompanySettingsMaster.iCompanyID));
            return objclsConnection.ExecuteScalar("spPayCompanySettings", prmCompany).ToBoolean(); 
        }

        public bool SaveDetails()
        {
            try
            {
                foreach (clsCompanySettingsDetails objCompanySettingsDetails in objclsDTOCompanySettingsMaster.lstCompanySettingsDetails)
                {
                    ArrayList prmCompany = new ArrayList();
                    prmCompany.Add(new SqlParameter("@iMode", 2));
                    prmCompany.Add(new SqlParameter("@iCompanyID", objclsDTOCompanySettingsMaster.iCompanyID));
                    prmCompany.Add(new SqlParameter("@bIsAuto", objCompanySettingsDetails.bIsAuto));
                    prmCompany.Add(new SqlParameter("@iFormID", objCompanySettingsDetails.iFormID));
                    prmCompany.Add(new SqlParameter("@iStartNumber", objCompanySettingsDetails.iStartNumber));
                    prmCompany.Add(new SqlParameter("@strPreFix", objCompanySettingsDetails.strPreFix));
                    prmCompany.Add(new SqlParameter("@NoLength", objCompanySettingsDetails.NoLength));
                    objclsConnection.ExecuteScalar("spPayCompanySettings", prmCompany);
                }
                return true;
            }
            catch
            {
                return false;
            }

        }


        void IDisposable.Dispose()
        { }
    }
}