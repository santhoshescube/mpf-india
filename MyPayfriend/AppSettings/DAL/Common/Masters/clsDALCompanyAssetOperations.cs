﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/*
 * Created By       : Tijo
 * Created Date     : 27 Aug 2013
 * Purpose          : For Asset Handover
*/
namespace MyPayfriend
{
    public class clsDALCompanyAssetOperations
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOCompanyAssetOperations objclsDTOCompanyAssetOperations { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spPayAssetHandover";

        public clsDALCompanyAssetOperations(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public DataSet getAssetDetails(int intEmpBenefitID, int intCompanyAssetID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@EmpBenefitID", intEmpBenefitID));
            parameters.Add(new SqlParameter("@CompanyAssetID", intCompanyAssetID));
            parameters.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }

        public DataTable getEmployeeDetails(int intCompanyID, string strSearchkey)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@Searchkey", strSearchkey));
            parameters.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 3));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public int SaveAssetIssueReturn()
        {
            if (objclsDTOCompanyAssetOperations.intEmpBenefitID > 0)
                DeleteAssetReturnDetails(objclsDTOCompanyAssetOperations.intEmpBenefitID);

            parameters = new ArrayList();

            if (objclsDTOCompanyAssetOperations.intAssetStatusID == (int)AssetStausType.OnHand)
            {
                if (objclsDTOCompanyAssetOperations.intEmpBenefitID == 0)
                    parameters.Add(new SqlParameter("@Mode", 6)); // Insert Asset Return
                else
                    parameters.Add(new SqlParameter("@Mode", 7)); // Update Asset Return
            }            
            else
            {
                if (objclsDTOCompanyAssetOperations.intEmpBenefitID == 0)
                    parameters.Add(new SqlParameter("@Mode", 4)); // Insert Asset Issue
                else
                    parameters.Add(new SqlParameter("@Mode", 5)); // Update Asset Issue
            }

            parameters.Add(new SqlParameter("@EmpBenefitID", objclsDTOCompanyAssetOperations.intEmpBenefitID));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOCompanyAssetOperations.lngEmployeeID));
            parameters.Add(new SqlParameter("@BenefitTypeID", objclsDTOCompanyAssetOperations.intBenefitTypeID));
            parameters.Add(new SqlParameter("@CompanyAssetID", objclsDTOCompanyAssetOperations.intCompanyAssetID));
            parameters.Add(new SqlParameter("@AssetStatusID", objclsDTOCompanyAssetOperations.intAssetStatusID));
            parameters.Add(new SqlParameter("@WithEffectDate", objclsDTOCompanyAssetOperations.dtWithEffectDate));

            if (objclsDTOCompanyAssetOperations.blnIsExpectedReturnDate)
                parameters.Add(new SqlParameter("@ExpectedReturnDate", objclsDTOCompanyAssetOperations.dtExpectedReturnDate));

            parameters.Add(new SqlParameter("@Remarks", objclsDTOCompanyAssetOperations.strRemarks));
            parameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));

            if (objclsDTOCompanyAssetOperations.intAssetStatusID == (int)AssetStausType.OnHand) // Asset Return
            {
                parameters.Add(new SqlParameter("@ParentID", objclsDTOCompanyAssetOperations.intParentID));
                parameters.Add(new SqlParameter("@ReturnDate", objclsDTOCompanyAssetOperations.dtReturnDate));                
            }

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objEmpBenefitID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objEmpBenefitID) != 0)
            {
                objclsDTOCompanyAssetOperations.intEmpBenefitID = objEmpBenefitID.ToInt32();

                if (objclsDTOCompanyAssetOperations.intEmpBenefitID > 0)
                {
                    if (objclsDTOCompanyAssetOperations.IlstclsDTOEmployeeBenefitDetail.Count > 0)
                    {
                        if (SaveAssetReturnDetails(objclsDTOCompanyAssetOperations.intEmpBenefitID) > 0)
                            return objclsDTOCompanyAssetOperations.intEmpBenefitID;
                        else
                            return 0;
                    }
                    else
                        return objclsDTOCompanyAssetOperations.intEmpBenefitID;
                }
            }
            return 0;
        }

        public int SaveAssetReturnDetails(int intEmpBenefitID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@EmpBenefitID", intEmpBenefitID));
            parameters.Add(new SqlParameter("@XmlEmployeeBenefitDetail", objclsDTOCompanyAssetOperations.IlstclsDTOEmployeeBenefitDetail.ToXml()));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objoutRowsAffected = 0;
            if (MobjDataLayer.ExecuteNonQuery(strProcName, parameters, out objoutRowsAffected) != 0)
                return objoutRowsAffected.ToInt32();

            return 0;
        }

        public bool DeleteAssetIssueReturn()
        {
            if (DeleteAssetReturnDetails(objclsDTOCompanyAssetOperations.intEmpBenefitID))
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 9));
                parameters.Add(new SqlParameter("@EmpBenefitID", objclsDTOCompanyAssetOperations.intEmpBenefitID));
                parameters.Add(new SqlParameter("@CompanyAssetID", objclsDTOCompanyAssetOperations.intCompanyAssetID));
                parameters.Add(new SqlParameter("@AssetStatusID", objclsDTOCompanyAssetOperations.intAssetStatusID));
                if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public bool DeleteAssetReturnDetails(int intEmpBenefitID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@EmpBenefitID", intEmpBenefitID));
            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                return true;

            return false;
        }

        public DataTable getEmployeeAssetDetails(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            parameters.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public DataTable getAssetUsageDetails(int intEmpBenefitID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@EmpBenefitID", intEmpBenefitID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public int CheckAssetIsIssued(int intCompanyAssetID, int intAssetStatusID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@CompanyAssetID", intCompanyAssetID));
            parameters.Add(new SqlParameter("@AssetStatusID", intAssetStatusID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public DataSet GetPrintAssetIssueReturn(int intEmpBenefitID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@EmpBenefitID", intEmpBenefitID));
            parameters.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }
    }
}
