﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.ComponentModel;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,DAL for ConfigurationSettings>
 * 
 * Modified By :Rajesh.r
 * Modified On :12-Aug-2013
================================================
*/

namespace MyPayfriend
{
    public class clsDALConfigurationSettings:DataLayer
    {

        #region Methods
        //Get Configuraiton values 
        public static DataTable GetConfigurationDetails()
        {
            ArrayList Parameter = new ArrayList();
            Parameter.Add(new SqlParameter("@Mode", 1));
            return new DataLayer().ExecuteDataTable("spConfigurationSetting", Parameter);

        }


        //Update the configuration table
        public bool UpdateConfigurationValues(List<clsDTOConfigurationSettings> DTOConfigurations)
        {
            foreach (clsDTOConfigurationSettings DTOConfig in DTOConfigurations)
            {

                ArrayList Parameter = new ArrayList();
                Parameter.Add(new SqlParameter("@Mode", 3));
                Parameter.Add(new SqlParameter("@ConfigurationID", DTOConfig.ConfigurationID));
                Parameter.Add(new SqlParameter("@ConfigurationValue", DTOConfig.ConfigurationValue));
                ExecuteScalar("spConfigurationSetting", Parameter);
            }
            return true;

        }
        #endregion
    }
}