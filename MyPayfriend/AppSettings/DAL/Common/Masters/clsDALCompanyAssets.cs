﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace MyPayfriend
{
   public class clsDALCompanyAssets
   {
       #region Declaration
       

       ArrayList parameters;
        SqlDataReader sdrDr;

        public clsDTOCompanyAssets objclsDTOCompanyAssets { get; set; }  // To get and Set values Of Dto 
        public DataLayer objclsconnection { get; set; }

       #endregion Declaration

       #region FillCombos
        public DataTable FillCombos(string[] sarFieldValues)     // To fill combo
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsconnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }
        #endregion FillCombos
       
       #region GetOtherInfoDetails
        public DataTable GetOtherInfoDetails() // Get otherInfo Details (Damage etc)
        {
            parameters = new ArrayList();
            objclsconnection = new DataLayer();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@CompanyAssetID", objclsDTOCompanyAssets.intCompanyAssetID));
            DataTable dtCompanyAssets = objclsconnection.ExecuteDataSet("PayCompanyAssets", parameters).Tables[0];
            return dtCompanyAssets;
        }
        #endregion GetOtherInfoDetails

       #region GetCompanyAssetDetails
        public DataTable GetCompanyAssetDetails() // To Select All Asset Info Display Mode
        {
            parameters = new ArrayList();
            objclsconnection = new DataLayer();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@BenefitTypeID", objclsDTOCompanyAssets.intBenefitTypeID));
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOCompanyAssets.intCompanyID));
            parameters.Add(new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView ));
            DataTable dtCompanyAssets = objclsconnection.ExecuteDataSet("PayCompanyAssets", parameters).Tables[0];
            return dtCompanyAssets;
        }
        #endregion GetCompanyAssetDetails

       #region SaveCompanyAssets
        public int SaveCompanyAssets() // To Save Details
        {
            parameters = new ArrayList();
            if (objclsDTOCompanyAssets.intCompanyAssetID == 0)
            {
                parameters.Add(new SqlParameter("@Mode", 2)); //Insert
            }
            else
            {
                parameters.Add(new SqlParameter("@Mode", 3));
                parameters.Add(new SqlParameter("@CompanyAssetID", objclsDTOCompanyAssets.intCompanyAssetID)); //Update
            }
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOCompanyAssets.intCompanyID));
            parameters.Add(new SqlParameter("@BenefitTypeID", objclsDTOCompanyAssets.intBenefitTypeID));
            parameters.Add(new SqlParameter("@Description", objclsDTOCompanyAssets.strDescription));
            parameters.Add(new SqlParameter("@ReferenceNumber", objclsDTOCompanyAssets.strRefeNo));

            parameters.Add(new SqlParameter("@PurchaseDate", objclsDTOCompanyAssets.strPurchaseDate));
            if (objclsDTOCompanyAssets.strExpiryDate != "")
                parameters.Add(new SqlParameter("@ExpiryDate", objclsDTOCompanyAssets.strExpiryDate));

            parameters.Add(new SqlParameter("@PurchaseValue", objclsDTOCompanyAssets.dblPurchaseValue));
            parameters.Add(new SqlParameter("@Depreciation", objclsDTOCompanyAssets.dblDepreciation));
            parameters.Add(new SqlParameter("@Remarks", objclsDTOCompanyAssets.strRemarks));
            parameters.Add(new SqlParameter("@Status", objclsDTOCompanyAssets.intStatus));

            int intResult = objclsconnection.ExecuteScalar("PayCompanyAssets", parameters).ToInt32();
            return intResult ;

        }
        #endregion SaveCompanyAssets

       #region DeleteCompanyAssets
        public bool DeleteCompanyAssets() // to Delete All Reference Exists
        {

            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@CompanyAssetID", objclsDTOCompanyAssets.intCompanyAssetID));
            int intResult = objclsconnection.ExecuteNonQuery("PayCompanyAssets", parameters);
            return intResult > 0;
        }
        #endregion DeleteCompanyAssets


        public int GetAssetCompanyID(int CompanyAssetID) // Get Company for asset
        {
            parameters = new ArrayList();
            objclsconnection = new DataLayer();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@CompanyAssetID", CompanyAssetID));
            return objclsconnection.ExecuteScalar("PayCompanyAssets", parameters).ToInt32();
           
        }

       //Created By Rajesh
       //Desription :For company asset email

        public static DataSet GetCompanyAssetEmail(int CompanyAssetID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@CompanyAssetID", CompanyAssetID));
            return new DataLayer().ExecuteDataSet("PayCompanyAssets", parameters); 
      
        }

   

}
}
