﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,21 Feb 2011>
Description:	<Description,,DAL for EmployeeInformation>
================================================
*/
namespace MyPayfriend
{
    public class clsDALEmployeeInformation : IDisposable
    {
        ArrayList parameters;
        ArrayList Param;
        ClsLogWriter objLogWriter;
        IList<SqlParameter> lstSqlParams;
        public clsDTOEmployeeInformation objclsDTOEmployeeInformation { get; set; }

        public DataLayer objclsConnection { get; set; }
       

        // To Save and Update

        public bool SaveEmployeeInformation(bool AddStatus)
        {
            object objoutEmployeeId = 0;

            parameters = new ArrayList();
            if (AddStatus)
                parameters.Add(new SqlParameter("@Mode", 2));
            else
                parameters.Add(new SqlParameter("@Mode", 3));

            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
            parameters.Add(new SqlParameter("@EmployeeNumber", objclsDTOEmployeeInformation.strEmployeeNumber));
            parameters.Add(new SqlParameter("@SalutationID", objclsDTOEmployeeInformation.intSalutation));

            parameters.Add(new SqlParameter("@FirstName", objclsDTOEmployeeInformation.strFirstName));
            parameters.Add(new SqlParameter("@MiddleName", objclsDTOEmployeeInformation.strMiddleName));
            parameters.Add(new SqlParameter("@LastName", objclsDTOEmployeeInformation.strLastName));
            parameters.Add(new SqlParameter("@EmployeeFullName", objclsDTOEmployeeInformation.strEmployeeFullName));

            parameters.Add(new SqlParameter("@FirstNameArb", objclsDTOEmployeeInformation.strFirstNameArb));
            parameters.Add(new SqlParameter("@MiddleNameArb", objclsDTOEmployeeInformation.strMiddleNameArb));
            parameters.Add(new SqlParameter("@LastNameArb", objclsDTOEmployeeInformation.strLastNameArb));
            parameters.Add(new SqlParameter("@EmployeeFullNameArb", objclsDTOEmployeeInformation.strEmployeeFullNameArb));

            parameters.Add(new SqlParameter("@Gender", objclsDTOEmployeeInformation.blnGender));
            parameters.Add(new SqlParameter("@CountryID", objclsDTOEmployeeInformation.intCountryID));
            parameters.Add(new SqlParameter("@NationalityID", objclsDTOEmployeeInformation.intNationalityID));
            parameters.Add(new SqlParameter("@ReligionID", objclsDTOEmployeeInformation.intReligionID));
            parameters.Add(new SqlParameter("@DateofBirth", objclsDTOEmployeeInformation.dtDateofBirth));
            parameters.Add(new SqlParameter("@DateofJoining", objclsDTOEmployeeInformation.dtDateofJoining));

            if(objclsDTOEmployeeInformation.intWorkStatusID==7)
                parameters.Add(new SqlParameter("@ProbationEndDate", objclsDTOEmployeeInformation.dtProbationEndDate));

            parameters.Add(new SqlParameter("@DepartmentID", objclsDTOEmployeeInformation.intDepartmentID));
            parameters.Add(new SqlParameter("@DivisionID", objclsDTOEmployeeInformation.intDivisionID));
            parameters.Add(new SqlParameter("@GradeID", objclsDTOEmployeeInformation.intGradeID));
            parameters.Add(new SqlParameter("@DesignationID", objclsDTOEmployeeInformation.intDesignationID));
            parameters.Add(new SqlParameter("@EmploymentTypeID", objclsDTOEmployeeInformation.intEmploymentType));
            parameters.Add(new SqlParameter("@SpouseName", Convert.ToString(objclsDTOEmployeeInformation.strSpouseName)));
            parameters.Add(new SqlParameter("@WorkStatusID", objclsDTOEmployeeInformation.intWorkStatusID));
            parameters.Add(new SqlParameter("@OfficialEmailID", objclsDTOEmployeeInformation.strOfficialEmail));
            parameters.Add(new SqlParameter("@FathersName", objclsDTOEmployeeInformation.strFathersName));
            parameters.Add(new SqlParameter("@MotherTongueID", objclsDTOEmployeeInformation.intMothertongueID));
            parameters.Add(new SqlParameter("@LanguagesKnown", objclsDTOEmployeeInformation.strLanguagesKnown));
            parameters.Add(new SqlParameter("@IdentificationMarks", objclsDTOEmployeeInformation.strIdentificationMarks));
            parameters.Add(new SqlParameter("@PermanentAddress", objclsDTOEmployeeInformation.strAddressLine1));

            parameters.Add(new SqlParameter("@AddressLine2", objclsDTOEmployeeInformation.strAddressLine2));
            parameters.Add(new SqlParameter("@City", objclsDTOEmployeeInformation.strCity));
            parameters.Add(new SqlParameter("@StateProvinceRegion", objclsDTOEmployeeInformation.strStateProvinceRegion ));
            parameters.Add(new SqlParameter("@ZipCode", objclsDTOEmployeeInformation.strZipCode ));
            parameters.Add(new SqlParameter("@NextAppraisalDate", objclsDTOEmployeeInformation.strNextAppraisalDate));

            parameters.Add(new SqlParameter("@PermanentPhone", objclsDTOEmployeeInformation.strPermanentPhone));
            parameters.Add(new SqlParameter("@LocalMobile", objclsDTOEmployeeInformation.strLocalMobile));
            parameters.Add(new SqlParameter("@LocalEmailID", objclsDTOEmployeeInformation.strEmail));
            parameters.Add(new SqlParameter("@MothersName", objclsDTOEmployeeInformation.strMothersName));
            parameters.Add(new SqlParameter("@Remarks", objclsDTOEmployeeInformation.strNotes));
            parameters.Add(new SqlParameter("@BloodGroup", objclsDTOEmployeeInformation.strBloodGroup));
            parameters.Add(new SqlParameter("@DeviceUserID", objclsDTOEmployeeInformation.strDeviceUserID));
            parameters.Add(new SqlParameter("@PersonID", objclsDTOEmployeeInformation.strPersonID));

            parameters.Add(new SqlParameter("@VisaObtainedFrom", objclsDTOEmployeeInformation.intVisaObtained));
            // Transaction type

            parameters.Add(new SqlParameter("@TransactionTypeID", objclsDTOEmployeeInformation.intTransactionTypeID));
            parameters.Add(new SqlParameter("@BankID", objclsDTOEmployeeInformation.intBankNameID));
            parameters.Add(new SqlParameter("@BankBranchID", objclsDTOEmployeeInformation.intBankBranchID));
            parameters.Add(new SqlParameter("@AccountNumber", objclsDTOEmployeeInformation.strAccountNumber));
            parameters.Add(new SqlParameter("@CompanyBankAccountID", objclsDTOEmployeeInformation.intComBankAccId));
            parameters.Add(new SqlParameter("@MaritalStatusID", objclsDTOEmployeeInformation.intMaritalStatusID));

            parameters.Add(new SqlParameter("@NoticePeriod", objclsDTOEmployeeInformation.NoticePeriod));

            parameters.Add(new SqlParameter("@EmergencyAddress", objclsDTOEmployeeInformation.strEmergencyAddress));
            parameters.Add(new SqlParameter("@EmergencyPhone", objclsDTOEmployeeInformation.strEmergencyPhone));
            parameters.Add(new SqlParameter("@LocalAddress", objclsDTOEmployeeInformation.strLocalAddress));
            parameters.Add(new SqlParameter("@LocalPhone", objclsDTOEmployeeInformation.strLocalPhone));
            parameters.Add(new SqlParameter("@HOD", objclsDTOEmployeeInformation.intHODID));

            SqlParameter objSqlParameter = new SqlParameter();
            objSqlParameter.DbType = DbType.Binary;
            objSqlParameter.Value = objclsDTOEmployeeInformation.strRecentPhotoPath;
            objSqlParameter.ParameterName = "@RecentPhoto";
            parameters.Add(objSqlParameter);

            SqlParameter objSqlParameterThp = new SqlParameter();
            objSqlParameterThp.DbType = DbType.Binary;
            objSqlParameterThp.Value = objclsDTOEmployeeInformation.strRecentPhotoPathThamNail;
            objSqlParameterThp.ParameterName = "@Thumbnail";
            parameters.Add(objSqlParameterThp);

            parameters.Add(new SqlParameter("@WorkPolicyID", objclsDTOEmployeeInformation.intPolicyID));
            parameters.Add(new SqlParameter("@LeavePolicyID", objclsDTOEmployeeInformation.intLeavePolicyID));

            if(objclsDTOEmployeeInformation.intVacationPolicyID>0)
                parameters.Add(new SqlParameter("@VacationPolicyID", objclsDTOEmployeeInformation.intVacationPolicyID));

            if(objclsDTOEmployeeInformation.ReportingTo>0)
                parameters.Add(new SqlParameter("@ReportingTo", objclsDTOEmployeeInformation.ReportingTo));

            if (objclsDTOEmployeeInformation.intWorkLocationID != 0)//since worklocation is not mandatory
                 parameters.Add(new SqlParameter("@WorkLocationID", objclsDTOEmployeeInformation.intWorkLocationID));

            if (objclsDTOEmployeeInformation.CommissionStructureID > 0)
                parameters.Add(new SqlParameter("@CommissionStructureID", objclsDTOEmployeeInformation.CommissionStructureID));

            if (objclsDTOEmployeeInformation.ProcessTypeID > 0)
                parameters.Add(new SqlParameter("@ProcessTypeID", objclsDTOEmployeeInformation.ProcessTypeID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            if (objclsConnection.ExecuteNonQueryWithTran("spEmployee", parameters, out objoutEmployeeId) != 0)
            {
                objclsDTOEmployeeInformation.intEmployeeID = objoutEmployeeId.ToInt64();
                if (objoutEmployeeId.ToInt64() > 0)
                {
                    objclsDTOEmployeeInformation.intEmployeeID = objoutEmployeeId.ToInt64();
                    LanguageSave();
                    return true;
                }
            }
            return false;
        }

        public DataTable GetLanguage(int iEmployeeID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 24));
            alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
            return objclsConnection.ExecuteDataTable("spEmployee", alParameters);
        }


        public void LanguageSave()
        {
            try
            {

                ArrayList alParameters = new ArrayList();
                alParameters.Add(new SqlParameter("@Mode", 26));
                alParameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
                objclsConnection.ExecuteNonQuery("spEmployee", alParameters);

                foreach (clsDTOLanuages clsDTOLanuages in objclsDTOEmployeeInformation.DTOLanuages)
                {
                    this.lstSqlParams = new List<SqlParameter>();
                    this.lstSqlParams.Add(new SqlParameter("@Mode", 25));
                    this.lstSqlParams.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
                    this.lstSqlParams.Add(new SqlParameter("@LanguageID", clsDTOLanuages.LanguageID));
                    objclsConnection.ExecuteNonQuery("spEmployee", lstSqlParams);

                }
            }
            catch
            {
            }
        }


        public bool SaveLeavePolicySummary()
        {//
            try
            {
                ArrayList parameters = new ArrayList();
                parameters.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
                parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
                parameters.Add(new SqlParameter("@LeavePolicyID", objclsDTOEmployeeInformation.intLeavePolicyID));
                parameters.Add(new SqlParameter("@Fromdate", objclsDTOEmployeeInformation.dtDateofJoining.ToString("dd-MMM-yyyy")));
                parameters.Add(new SqlParameter("@Type", 0.ToInt32()));

                objclsConnection.ExecuteNonQueryWithTran("spPayEmployeeLeaveSummary", parameters);
                return true;
            }
            catch 
            {
              
                return false;
            }
        }

        public int RecCountNavigate(int CompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return objclsConnection.ExecuteScalar("spEmployee", parameters).ToInt32();           
        }

        public DataTable GetCompanySetting(int CompanyID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@iMode", 5));
            parameters.Add(new SqlParameter("@iCompanyID", CompanyID));
            parameters.Add(new SqlParameter("@iFormID", (int)FormID.Employee));
            return objclsConnection.ExecuteDataTable("spPayCompanySettings", parameters);
        }

        public int GetRowNumber()
        {
        
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return objclsConnection.ExecuteScalar("spEmployee", parameters).ToInt32();
           
        }

       

        // Display 

        public bool DisplayEmployeeInformation(int introwno,int CompanyID)
        {
           
            parameters = new ArrayList();              
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));

            parameters.Add(new SqlParameter("@RowNum", introwno));           
              DataTable dt = objclsConnection.ExecuteDataTable("spEmployee", parameters);

            if(dt.Rows.Count>0)
               ReadEmployeeInformation(dt.Rows[0]);

            return (dt.Rows.Count > 0);
        }

        // Function to display employee

        public void ReadEmployeeInformation(DataRow drEmployee)
        {

            objclsDTOEmployeeInformation.intEmployeeID = Convert.ToInt32(drEmployee["EmployeeID"]);
            objclsDTOEmployeeInformation.intCompanyID = Convert.ToInt32(drEmployee["CompanyID"]);
            objclsDTOEmployeeInformation.intSalutation = Convert.ToInt32(drEmployee["SalutationID"]);
            objclsDTOEmployeeInformation.intNationalityID = drEmployee["NationalityID"] == System.DBNull.Value ? -1 : Convert.ToInt32(drEmployee["NationalityID"]);

            objclsDTOEmployeeInformation.intReligionID = drEmployee["ReligionID"].ToInt32();
            objclsDTOEmployeeInformation.intDepartmentID = drEmployee["DepartmentID"].ToInt32();
            objclsDTOEmployeeInformation.intDivisionID = drEmployee["DivisionID"].ToInt32();
            objclsDTOEmployeeInformation.intDesignationID = drEmployee["DesignationID"].ToInt32();
            objclsDTOEmployeeInformation.intEmploymentType = drEmployee["EmploymentTypeID"].ToInt32();
            objclsDTOEmployeeInformation.intWorkStatusID = drEmployee["WorkStatusID"].ToInt32();

            objclsDTOEmployeeInformation.intPolicyID = drEmployee["WorkPolicyID"].ToInt32();
            objclsDTOEmployeeInformation.intLeavePolicyID = drEmployee["LeavePolicyID"].ToInt32();
            objclsDTOEmployeeInformation.intWorkLocationID = (drEmployee["WorkLocationID"]).ToInt32();
            objclsDTOEmployeeInformation.intMothertongueID = drEmployee["MothertongueID"].ToInt32();
            objclsDTOEmployeeInformation.strEmployeeNumber = Convert.ToString(drEmployee["EmployeeNumber"]);
            objclsDTOEmployeeInformation.strFirstName = Convert.ToString(drEmployee["FirstName"]);
            objclsDTOEmployeeInformation.strMiddleName = Convert.ToString(drEmployee["MiddleName"]);
            objclsDTOEmployeeInformation.strLastName = Convert.ToString(drEmployee["LastName"]);
            objclsDTOEmployeeInformation.strEmployeeFullName = Convert.ToString(drEmployee["EmployeeFullName"]);
            objclsDTOEmployeeInformation.blnGender = Convert.ToBoolean(drEmployee["Gender"]);
            objclsDTOEmployeeInformation.strAccountNumber = Convert.ToString(drEmployee["AccountNumber"]);
            objclsDTOEmployeeInformation.strFathersName = Convert.ToString(drEmployee["FathersName"]);
            objclsDTOEmployeeInformation.strLanguagesKnown = Convert.ToString(drEmployee["LanguagesKnown"]);
            objclsDTOEmployeeInformation.strIdentificationMarks = Convert.ToString(drEmployee["IdentificationMarks"]);

            objclsDTOEmployeeInformation.strAddressLine1 = drEmployee["AddressLine1"].ToString();
            objclsDTOEmployeeInformation.strAddressLine2 = drEmployee["AddressLine2"].ToString();
            objclsDTOEmployeeInformation.strCity = drEmployee["City"].ToString();
            objclsDTOEmployeeInformation.strStateProvinceRegion = drEmployee["StateProvinceRegion"].ToString();
            objclsDTOEmployeeInformation.strZipCode = drEmployee["ZipCode"].ToString();
            objclsDTOEmployeeInformation.NoticePeriod = drEmployee["NoticePeriod"].ToInt32(); 
            objclsDTOEmployeeInformation.strPermanentPhone = Convert.ToString(drEmployee["Phone"]);
            objclsDTOEmployeeInformation.strLocalMobile = Convert.ToString(drEmployee["Mobile"]);
            objclsDTOEmployeeInformation.strEmail = Convert.ToString(drEmployee["EmailID"]);
            objclsDTOEmployeeInformation.strMothersName = Convert.ToString(drEmployee["MothersName"]);
            objclsDTOEmployeeInformation.strSpouseName = Convert.ToString(drEmployee["SpouseName"]);
            objclsDTOEmployeeInformation.strOfficialEmail = Convert.ToString(drEmployee["OfficialEmailID"]);
            objclsDTOEmployeeInformation.strNotes = Convert.ToString(drEmployee["Remarks"]);
            objclsDTOEmployeeInformation.strBloodGroup = Convert.ToString(drEmployee["BloodGroup"]);
            objclsDTOEmployeeInformation.strDeviceUserID = Convert.ToString(drEmployee["DeviceUserID"]);
            objclsDTOEmployeeInformation.strTransferDate = Convert.ToString(drEmployee["TransferDate"]);
            objclsDTOEmployeeInformation.strRejoinDate = Convert.ToString(drEmployee["RejoinDate"]);

            objclsDTOEmployeeInformation.strNextAppraisalDate = Convert.ToString(drEmployee["NextAppraisalDate"]);

            objclsDTOEmployeeInformation.strRecentPhotoPath = (drEmployee["Photo"]) != DBNull.Value?(byte[])drEmployee["Photo"]:null;
            objclsDTOEmployeeInformation.strRecentPhotoPathThamNail = (drEmployee["Thumbnail"]) != DBNull.Value ? (byte[])drEmployee["Thumbnail"] : null;

            objclsDTOEmployeeInformation.intVisaObtained = Convert.ToInt32(drEmployee["VisaObtainedFrom"].ToInt32());

            objclsDTOEmployeeInformation.dtDateofBirth = drEmployee["DateofBirth"].ToString().ToDateTime();
            objclsDTOEmployeeInformation.dtDateofJoining = drEmployee["DateofJoining"].ToString().ToDateTime();
            objclsDTOEmployeeInformation.dtProbationEndDate = drEmployee["ProbationEndDate"] == DBNull.Value ? DateTime.Now.Date : drEmployee["ProbationEndDate"].ToDateTime();
            objclsDTOEmployeeInformation.intTransactionTypeID = Convert.ToInt32(drEmployee["TransactionTypeID"]);
            objclsDTOEmployeeInformation.intBankNameID = Convert.ToInt32(drEmployee["BankID"]);
            objclsDTOEmployeeInformation.intBankBranchID = Convert.ToInt32(drEmployee["BankBranchID"]);
            objclsDTOEmployeeInformation.strAccountNumber = Convert.ToString(drEmployee["AccountNumber"]);
            objclsDTOEmployeeInformation.intComBankAccId = drEmployee["CompanyBankAccountID"].ToInt32();

            objclsDTOEmployeeInformation.strCurrentWorklocation = drEmployee["CurrentLocation"].ToString();
            objclsDTOEmployeeInformation.strPersonID =Convert.ToString(drEmployee["PersonID"]);
            objclsDTOEmployeeInformation.intVacationPolicyID = drEmployee["VacationPolicyID"].ToInt32();
            objclsDTOEmployeeInformation.ReportingTo = drEmployee["ReportingTo"].ToInt64();
            objclsDTOEmployeeInformation.intHODID = drEmployee["HOD"].ToInt32();
            objclsDTOEmployeeInformation.intMaritalStatusID = Convert.ToInt32(drEmployee["MaritalStatusID"]);

            objclsDTOEmployeeInformation.strFirstNameArb = Convert.ToString(drEmployee["FirstNameArb"]);
            objclsDTOEmployeeInformation.strMiddleNameArb = Convert.ToString(drEmployee["MiddleNameArb"]);
            objclsDTOEmployeeInformation.strLastNameArb = Convert.ToString(drEmployee["LastNameArb"]);
            objclsDTOEmployeeInformation.strEmployeeFullNameArb = Convert.ToString(drEmployee["EmployeeFullNameArb"]);
            objclsDTOEmployeeInformation.intCountryID = drEmployee["CountryID"]!=System.DBNull.Value ? Convert.ToInt32(drEmployee["CountryID"]) : -1;
            objclsDTOEmployeeInformation.intGradeID = drEmployee["GradeID"] != System.DBNull.Value ? Convert.ToInt32(drEmployee["GradeID"]) : -1;
            objclsDTOEmployeeInformation.strEmergencyAddress = Convert.ToString(drEmployee["EmergencyAddress"]);
            objclsDTOEmployeeInformation.strEmergencyPhone = Convert.ToString(drEmployee["EmergencyPhone"]);
            objclsDTOEmployeeInformation.strLocalAddress = Convert.ToString(drEmployee["LocalAddress"]);
            objclsDTOEmployeeInformation.strLocalPhone = Convert.ToString(drEmployee["LocalPhone"]);
            objclsDTOEmployeeInformation.CommissionStructureID = drEmployee["CommissionStructureID"].ToInt32();
            objclsDTOEmployeeInformation.ProcessTypeID = drEmployee["ProcessTypeID"].ToInt32();
        }


        public bool SearchItem(string SearchCriteria,int CompanyID,int RowNum, out int TotalRows)
        {            
            TotalRows = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@SearchCriteria", SearchCriteria));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@RowNum", RowNum));

            DataSet ds = objclsConnection.ExecuteDataSet("spEmployee", parameters);
            if (ds.Tables.Count > 0)
            {
                TotalRows = ds.Tables[1].Rows[0]["RecordCount"].ToInt32();
                if (ds.Tables[0].Rows.Count > 0)
                    ReadEmployeeInformation(ds.Tables[0].Rows[0]);

                return ds.Tables[0].Rows.Count > 0;
            }
            return false;
        }


        // Delete EmployeeInformation

        public bool DeleteEmployeeInformation()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
            if (objclsConnection.ExecuteNonQueryWithTran("spEmployee", parameters) != 0)
            {

                return true;
            }
            return false;
        }

        // To check the Existence of EmployeeID

        public DataTable CheckExistReferences()
        {

            string strCondition1 = "'EmployeeMaster','EmployeeDetails','PayWorkPolicyHistoryMaster','PayEmployeeLeaveSummary'";
            string strCondition2 = "'AssignedTo','EmployeeID','InChargeID'";

            Param = new ArrayList();
            Param.Add(new SqlParameter("@Mode", 1));
            Param.Add(new SqlParameter("@Value", objclsDTOEmployeeInformation.intEmployeeID));

            Param.Add(new SqlParameter("@ColumnsIn", strCondition2));
            Param.Add(new SqlParameter("@TablesNotIn", strCondition1));

            return objclsConnection.ExecuteDataSet("spCheckValueExists", Param).Tables[0];
        }


        //To Fill Combobox
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsConnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }
      

        // To check Validemail 

        public bool CheckValidEmail(string sEmailAddress)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                return objCommonUtility.CheckValidEmail(sEmailAddress);
            }
        }

        public bool CheckDuplication(bool blnAddStatus, string[] saValues, long intId, int intCompanyID)
        {
            string sField = "EmployeeNumber";
            string sTable = "EmployeeMaster";
            string sCondition = "";

            //if (blnAddStatus) // Add mode
            //    sCondition = "EmployeeNumber='" + saValues[0] + "' and CompanyID= " + intCompanyID + "";
            //else // Edit mode
            //    sCondition = "EmployeeNumber='" + saValues[0] + "' and CompanyID= " + intCompanyID + " and EmployeeID <> " + intId + "";

            //Same number and name display in loan,salaryadvance etc

            if (blnAddStatus) // Add mode
                sCondition = "EmployeeNumber='" + saValues[0] + "' and CompanyID= " + intCompanyID + "";
            else // Edit mode
                sCondition = "EmployeeNumber='" + saValues[0] + "' and EmployeeID <> " + intId + " and CompanyID= " + intCompanyID + "";

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objclsConnection;
                return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            }
        }

        public DataSet GetEmployeeReport()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
            return objclsConnection.ExecuteDataSet("spEmployee", parameters);
        }

        public string GenerateEmployeeCode()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            if (objclsDTOEmployeeInformation.intCompanyID == 0)
            {
                parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            }
            else
            {
                parameters.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
            }
            return objclsConnection.ExecuteScalar("spEmployee", parameters).ToString(); 
            
        }
        public int GetCurRowNumber()
        {


            ArrayList prmWorkPolicy = new ArrayList();
            prmWorkPolicy = new ArrayList();
            prmWorkPolicy.Add(new SqlParameter("@Mode", "GCRC"));
            prmWorkPolicy.Add(new SqlParameter("@PolicyID", objclsDTOEmployeeInformation.intPolicyID));

            return Convert.ToInt32(objclsConnection.ExecuteScalar("spPayWorkPolicy", prmWorkPolicy));


        }
       
               

        public bool DeleteLeavePolicy()
        {
            try
            {
                ArrayList parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 16));
                parameters.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
                parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));

                if (objclsConnection.ExecuteNonQuery("spEmployee", parameters) != 0)
                    return true;
                else
                    return false;

            }
            catch 
            {
                return false;
            }
        }

        public DataTable CheckCarryForwardLeaveExists()
        {
            return this.objclsConnection.ExecuteDataTable("spEmployee", new List<SqlParameter> {
                 new SqlParameter("@Mode",16),new SqlParameter("@EmployeeID",objclsDTOEmployeeInformation.intEmployeeID)});
        }
        public DataSet UpdateLeaveSummary(int iLeaveTypeID, int iLeavePolicyID)
        {

            ArrayList prmEmployee = new ArrayList();
            prmEmployee.Add(new SqlParameter("@Type", 0.ToInt32()));
            prmEmployee.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
            prmEmployee.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
            prmEmployee.Add(new SqlParameter("@LeavetypeID", iLeaveTypeID));
            prmEmployee.Add(new SqlParameter("@Leavepolicyid", iLeavePolicyID));
            prmEmployee.Add(new SqlParameter("@Fromdate", new clsConnection().GetSysDate()));
            prmEmployee.Add(new SqlParameter("@Todate", new clsConnection().GetSysDate()));
            return objclsConnection.ExecuteDataSet("spPayEmployeeLeaveSummaryFromLeaveEntry", prmEmployee);
        }


        public string GetCompanyStartDate(int intCompanyID)
        {
           
           return this.objclsConnection.ExecuteScalar("spEmployee", new List<SqlParameter>{
                new SqlParameter("@Mode",17),new SqlParameter("@CompanyID",intCompanyID)}).ToStringCustom();
        
        }

        public bool CheckIfSettlementExists(long EmployeeId)
        {

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 19));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
            return objclsConnection.ExecuteScalar("spEmployee", parameters).ToInt32() == 1 ;
            
        }
         public bool AttendancetExists(long EmployeeId)
        {

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 23));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
            return objclsConnection.ExecuteScalar("spEmployee", parameters).ToInt32() == 1 ;
            
        }
        public string GetEmployeeTransferDate(int intCompanyID, long EmployeeId)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 20));
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOEmployeeInformation.intCompanyID));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeInformation.intEmployeeID));
            return objclsConnection.ExecuteScalar("spEmployee", parameters).ToStringCustom();
        }

        public DataTable GetDesignation(int CompanyID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "DR"));
            alParameters.Add(new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            return objclsConnection.ExecuteDataTable("HRspEmployeeMaster", alParameters);
        }

        public DataTable GetReportingto(int intEmployeeID, int intCompanyID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "RTD"));
            alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
           // alParameters.Add(new SqlParameter("@DesignationID", intDesignationID));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alParameters.Add(new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView));
            return objclsConnection.ExecuteDataTable("HRspEmployeeMaster", alParameters);
        }
        public DataTable GetHOD(int intEmployeeID, int intCompanyID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "HOD"));
            alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            // alParameters.Add(new SqlParameter("@DesignationID", intDesignationID));
            alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            alParameters.Add(new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView));
            return objclsConnection.ExecuteDataTable("HRspEmployeeMaster", alParameters);
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (parameters != null)
                parameters = null;

           

            if (Param != null)
                Param = null;

        }

        #endregion
    }
}