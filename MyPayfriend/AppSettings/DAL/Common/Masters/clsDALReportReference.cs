﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALReportReference
    {
        public clsDTOReportReference objDTOReportRef { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        //ArrayList alParameters;
        string strProcName = "SPReportReference";

        public clsDALReportReference()
       {

       }
        public DataSet  GetReportDetails()
        {
            return MobjDataLayer.ExecuteDataSet(strProcName);
           
        }
    }
}
