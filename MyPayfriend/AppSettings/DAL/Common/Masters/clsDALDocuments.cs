﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

/* 
=================================================
Author:		<Author,Sawmya>
Create date: <Create Date,11 Mar 2011>
Description:	<Description,DAL for Scanning>
================================================
*/

namespace MyPayfriend 
{
    public class clsDALDocuments
    {
        ArrayList parameters;

        public clsDTODocuments objclsDTODocuments { get; set; }
        public DataLayer objclsconnection { get; set; }

        // count for  Print checking
        public int Print()
        {
            int intPrintCount = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            DataTable datTemp = objclsconnection.ExecuteDataTable("spScanning", parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    intPrintCount = Convert.ToInt32(datTemp.Rows[0]["cnt"].ToString());
            }
            return intPrintCount;
        }

        // count for  Addnew checking

        public int AddNew()
        {
            int intAddNewCount = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            DataTable datTemp = objclsconnection.ExecuteDataTable("spScanning", parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                    intAddNewCount = Convert.ToInt32(datTemp.Rows[0]["cnt"].ToString());
            }
            return intAddNewCount;
        }

        // To Save and Update
        public bool Save(bool AddStatus)
        {
            object objoutNodeId = 0;

            parameters = new ArrayList();
            if (AddStatus)
            {
                parameters.Add(new SqlParameter("@Mode", 3));
                parameters.Add(new SqlParameter("@ParentNode", objclsDTODocuments.intParentNode));
                parameters.Add(new SqlParameter("@RecordID", objclsDTODocuments.intDocumentID));
                parameters.Add(new SqlParameter("@DocumentTypeID", objclsDTODocuments.intDocumentTypeID));
                parameters.Add(new SqlParameter("@CommonId", objclsDTODocuments.lngCommonId));
                parameters.Add(new SqlParameter("@NavID", objclsDTODocuments.intNavID));
            }
            else
            {
                parameters.Add(new SqlParameter("@Mode", 14));
                parameters.Add(new SqlParameter("@Node", objclsDTODocuments.intNode));
            }

            parameters.Add(new SqlParameter("@Description", objclsDTODocuments.strDescription));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            if (objclsconnection.ExecuteNonQuery("spScanning", parameters, out objoutNodeId) != 0)
            {
                if ((int)objoutNodeId > 0)
                {
                    objclsDTODocuments.intNode = (int)objoutNodeId;
                    return true;
                }

            }
            return false;
        }

        public bool insertDetails(int intNode, string strFilename, string strMeta)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@Node", intNode));
            parameters.Add(new SqlParameter("@FileName", strFilename));
            parameters.Add(new SqlParameter("@MetaData", strMeta));
            objclsconnection.ExecuteNonQuery("spScanning", parameters);
            return true;
        }

        public void Insert()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@DocumentTypeID", objclsDTODocuments.intDocumentTypeID));
            parameters.Add(new SqlParameter("@CommonId", objclsDTODocuments.lngCommonId));
            parameters.Add(new SqlParameter("@NavID", objclsDTODocuments.intNavID));
            parameters.Add(new SqlParameter("@RecordID", objclsDTODocuments.intDocumentID));
            parameters.Add(new SqlParameter("@Description", objclsDTODocuments.strDescription));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objoutNodeId;

            if (objclsconnection.ExecuteNonQuery("spScanning", parameters, out objoutNodeId) != 0)
            {
                objclsDTODocuments.intNode = Convert.ToInt32(objoutNodeId);
            }
        }

        public bool selectforupdate(string description)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 15));
            parameters.Add(new SqlParameter("@Description", description));
            DataTable datTemp = objclsconnection.ExecuteDataTable("spScanning", parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objclsDTODocuments.intNode = Convert.ToInt32(datTemp.Rows[0]["Node"].ToString());
                    objclsDTODocuments.strDescription = Convert.ToString(datTemp.Rows[0]["Description"].ToString());
                    return true;
                }
            }
            return false;
        }


        public bool Selectnode(long lngCommonID, string strDescription, int intNavid)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@CommonId", lngCommonID));
            parameters.Add(new SqlParameter("@Description", strDescription));
            parameters.Add(new SqlParameter("@NavID", intNavid));
            DataTable datTemp = objclsconnection.ExecuteDataTable("spScanning", parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objclsDTODocuments.intNode = Convert.ToInt32(datTemp.Rows[0]["Node"].ToString());
                    return true;
                }
            }
            return false;
        }
        public bool selectDocumentType(int Document)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@DocumentTypeID", Document));
            DataTable datTemp = objclsconnection.ExecuteDataTable("spScanning", parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objclsDTODocuments.strDescription = datTemp.Rows[0]["description"].ToString();
                    return true;
                }
            }
            return false;
        }

        public bool SelectDetails(int intNode, int DocumentTypeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@Node", intNode));
            parameters.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            DataTable datTemp = objclsconnection.ExecuteDataTable("spScanning", parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objclsDTODocuments.strFileName = datTemp.Rows[0]["FileName"].ToString();
                    return true;
                }
            }
            return false;
        }


        public bool SelectfromTreeMaster(int documentid)
        {
            objclsDTODocuments.strDescription = "";
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@DocumentTypeID", documentid));
            DataTable datTemp = objclsconnection.ExecuteDataTable("spScanning", parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    objclsDTODocuments.strDescription = datTemp.Rows[0]["Description"].ToString();
                    return true;
                }
            }
            return false;

        }

        public bool Deletenodes(int mode, int Nodes)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@mode", mode));
            parameters.Add(new SqlParameter("@Node ", Nodes));
            if (objclsconnection.ExecuteNonQueryWithTran("spScanning", parameters) != 0)
            {

                return true;
            }
            return false;
        }



        public DataTable Display(int Navid, long CommonId, int Parentnode)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@NavID", Navid));
            parameters.Add(new SqlParameter("@CommonId", CommonId));
            parameters.Add(new SqlParameter("@parentNode", Parentnode));
            return objclsconnection.ExecuteDataTable("spScanning", parameters);
        }


        public DataTable Fill(int CompanyId, string strDescription, int Navid, int parentnode)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@CommonId", CompanyId));
            parameters.Add(new SqlParameter("@Description", strDescription));
            parameters.Add(new SqlParameter("@NavID", Navid));
            parameters.Add(new SqlParameter("@ParentNode", parentnode));
            return objclsconnection.ExecuteDataTable("spScanning", parameters);

        }

        public DataTable GetparentNodes(int Navid, long CommonId, int Parentnode, int intOperationTypeID, int intVendorID, int intDocumentTypeid, int RecordID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 20));
            parameters.Add(new SqlParameter("@NavID", Navid));
            parameters.Add(new SqlParameter("@CommonId", CommonId));
            parameters.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
            parameters.Add(new SqlParameter("@VendorID", intVendorID));
            parameters.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeid));
            parameters.Add(new SqlParameter("@parentNode", Parentnode));
            parameters.Add(new SqlParameter("@RecordID", RecordID));
            return objclsconnection.ExecuteDataTable("spScanning", parameters);
        }


        public bool DocumentStatus(int RecordID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 21));
            parameters.Add(new SqlParameter("@RecordID", RecordID));

            DataTable datTemp = objclsconnection.ExecuteDataTable("spScanning", parameters);
            if (datTemp != null)
            {
                if (datTemp.Rows.Count > 0)
                {
                    if (Convert.ToInt32(datTemp.Rows[0]["ApprovedBy"]) == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            return false;



        }




    }
}
