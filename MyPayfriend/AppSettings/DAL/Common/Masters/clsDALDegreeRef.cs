﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace MyPayfriend
{
    public class clsDALDegreeRef
    {
        public DataLayer DataLayer { get; set; }
        public clsDTODegreeRef DegreeRef { get; set; }
     
        private string strProcName = "spPayDegreeRef"; // Stored Procedure  Name

        #region Constructor

        public clsDALDegreeRef(DataLayer objDataLayer)
        {
            this.DataLayer = objDataLayer;
        }
        #endregion

        public static DataTable FillCombo(string TableName, string ValueField, string TextField)
        {
            IList<SqlParameter> lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "FC"));
            lstSqlParams.Add(new SqlParameter("@RefTableName", TableName));
            lstSqlParams.Add(new SqlParameter("@RefValueField", ValueField));
            lstSqlParams.Add(new SqlParameter("@RefTextField", TextField));

            return new DataLayer().ExecuteDataTable("spPayDegreeRef", lstSqlParams);
        }

        public static DataTable FillGrid(int refValue, string RefTableName, string TableName, string RefValueField, string Value1Field, string ValueField, string TextField)
        {
            IList<SqlParameter> lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "FG"));
            lstSqlParams.Add(new SqlParameter("@RefTableName", RefTableName));
            lstSqlParams.Add(new SqlParameter("@TableName", TableName));
            lstSqlParams.Add(new SqlParameter("@RefValueField", RefValueField));
            lstSqlParams.Add(new SqlParameter("@DataValueField", ValueField));
            lstSqlParams.Add(new SqlParameter("@DataValue1Field", Value1Field));
            lstSqlParams.Add(new SqlParameter("@DataTextField", TextField));
            lstSqlParams.Add(new SqlParameter("@DataRefValue", refValue));

            return new DataLayer().ExecuteDataTable("spPayDegreeRef", lstSqlParams);
        }

        public int Save()
        {
            IList<SqlParameter> lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "ID"));
            lstSqlParams.Add(new SqlParameter("@TableName", DegreeRef.PstrTableName));
            lstSqlParams.Add(new SqlParameter("@DataTextField", DegreeRef.PstrDataTextField));
            lstSqlParams.Add(new SqlParameter("@DataTextValue", DegreeRef.PstrDataTextValue));
            lstSqlParams.Add(new SqlParameter("@DataValue1Field", DegreeRef.PstrDataValueField1));
            lstSqlParams.Add(new SqlParameter("@DataRefValue", DegreeRef.PintRefDataValue));
            lstSqlParams.Add(new SqlParameter("@DataValueField", DegreeRef.PstrDataValueField));
            lstSqlParams.Add(new SqlParameter("@DataNoValue", DegreeRef.PintDataValue));

            return Convert.ToInt32(DataLayer.ExecuteScalar(strProcName, lstSqlParams));
        }

        public bool CheckDuplication()
        {

            IList<SqlParameter> lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "CD"));
            lstSqlParams.Add(new SqlParameter("@TableName", DegreeRef.PstrTableName));
            lstSqlParams.Add(new SqlParameter("@DataTextField", DegreeRef.PstrDataTextField));
            lstSqlParams.Add(new SqlParameter("@DataTextValue", DegreeRef.PstrDataTextValue));
            lstSqlParams.Add(new SqlParameter("@DataValueField", DegreeRef.PstrDataValueField));
            lstSqlParams.Add(new SqlParameter("@DataNoValue", DegreeRef.PintDataValue));

            return Convert.ToInt32(DataLayer.ExecuteScalar(strProcName, lstSqlParams)) > 0 ? true : false;

        }

        public bool DeleteDegree()
        {
            IList<SqlParameter> lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "DD"));
            lstSqlParams.Add(new SqlParameter("@TableName", DegreeRef.PstrTableName));
            lstSqlParams.Add(new SqlParameter("@DataValueField", DegreeRef.PstrDataValueField));
            lstSqlParams.Add(new SqlParameter("@DataNoValue", DegreeRef.PintDataValue));

            return Convert.ToInt32(DataLayer.ExecuteScalar(strProcName, lstSqlParams)) > 0 ? true : false;
        }
    }
}