﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALWorkLocation
    {
        ClsCommonUtility MobjClsCommonUtility; // object of clsCommonUtility
        DataLayer MobjDataLayer;
        ArrayList prmWorkLocation;

        public clsDTOWorkLocation PobjClsDTOWorkLocation { get; set; }

        public clsDALWorkLocation(DataLayer objDataLayer)
        {
            MobjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MobjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
      

        public int SaveWorkLocation(bool blnAddStatus)
        {
            prmWorkLocation = new ArrayList();
            if (blnAddStatus)//Add Mode
            {
                prmWorkLocation.Add(new SqlParameter("@Mode", 2));
            }
            else // Update Mode
            {
                prmWorkLocation.Add(new SqlParameter("@Mode", 3));
                prmWorkLocation.Add(new SqlParameter("@WorkLocationID", PobjClsDTOWorkLocation.lngWorkLocationID));
               
            }
            prmWorkLocation.Add(new SqlParameter("@LocationName", PobjClsDTOWorkLocation.strLocationName));
            prmWorkLocation.Add(new SqlParameter("@CompanyID", PobjClsDTOWorkLocation.intCompanyID));
            prmWorkLocation.Add(new SqlParameter("@Address", PobjClsDTOWorkLocation.strAddress));
            prmWorkLocation.Add(new SqlParameter("@Phone", PobjClsDTOWorkLocation.strPhone));
            prmWorkLocation.Add(new SqlParameter("@Email", PobjClsDTOWorkLocation.strEmail));
            PobjClsDTOWorkLocation.lngWorkLocationID = MobjDataLayer.ExecuteScalar("spPayWorkLocation", prmWorkLocation).ToInt32();
            return Convert.ToInt32(PobjClsDTOWorkLocation.lngWorkLocationID);
        }

        public bool DeleteWorkLocation()
        {
           
            prmWorkLocation = new ArrayList();
            prmWorkLocation.Add(new SqlParameter("@Mode", 4));
            prmWorkLocation.Add(new SqlParameter("@WorkLocationID", PobjClsDTOWorkLocation.lngWorkLocationID));
          

            return MobjDataLayer.ExecuteNonQuery("spPayWorkLocation", prmWorkLocation) > 0;
        }

        public int GetRecordCount()
        {
            prmWorkLocation = new ArrayList();
            prmWorkLocation.Add(new SqlParameter("@Mode", 5));
            prmWorkLocation.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            prmWorkLocation.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            object objResult = MobjDataLayer.ExecuteScalar("spPayWorkLocation", prmWorkLocation);
            return Convert.ToInt32(objResult);
        }

        public bool DisplayWorkLocation(int intRowNumber)
        {
            DataTable datWorkLocation = new DataTable();
            prmWorkLocation = new ArrayList();
            prmWorkLocation.Add(new SqlParameter("@Mode", 1));
            prmWorkLocation.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            prmWorkLocation.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            prmWorkLocation.Add(new SqlParameter("@RowNumber", intRowNumber));
            datWorkLocation = MobjDataLayer.ExecuteDataTable("spPayWorkLocation", prmWorkLocation);

            if (datWorkLocation.Rows.Count > 0)
            {
                PobjClsDTOWorkLocation.lngWorkLocationID = datWorkLocation.Rows[0]["WorkLocationID"].ToInt32();
                PobjClsDTOWorkLocation.strLocationName = datWorkLocation.Rows[0]["LocationName"].ToString();
                PobjClsDTOWorkLocation.intCompanyID = datWorkLocation.Rows[0]["CompanyID"].ToInt32();
                PobjClsDTOWorkLocation.strAddress = datWorkLocation.Rows[0]["Address"].ToString();
                PobjClsDTOWorkLocation.strPhone = datWorkLocation.Rows[0]["Phone"].ToString();
                PobjClsDTOWorkLocation.strEmail = datWorkLocation.Rows[0]["Email"].ToString();
                return true;
            }
            return false;
        }

        public DataSet DisplayWorkLocation()  //Work Location
        {
            DataTable datWorkLocation = new DataTable();
            prmWorkLocation = new ArrayList();
            prmWorkLocation.Add(new SqlParameter("@Mode", 6));
            prmWorkLocation.Add(new SqlParameter("@WorkLocationID", PobjClsDTOWorkLocation.lngWorkLocationID));
            return MobjDataLayer.ExecuteDataSet("spPayWorkLocation", prmWorkLocation);
        }

        public DataTable GetCurrentIndex(int WorkLocationID)
        {
            prmWorkLocation = new ArrayList();
            prmWorkLocation.Add(new SqlParameter("@Mode", 7));
            prmWorkLocation.Add(new SqlParameter("@WorkLocationID", WorkLocationID));
            return this.MobjDataLayer.ExecuteDataTable("spPayWorkLocation", prmWorkLocation);

        }

        public bool LocationAlreadyExists()
        {
            int result = 0;
            prmWorkLocation = new ArrayList();
            prmWorkLocation.Add(new SqlParameter("@Mode", 8));
            prmWorkLocation.Add(new SqlParameter("@WorkLocationID", PobjClsDTOWorkLocation.lngWorkLocationID));
            return MobjDataLayer.ExecuteScalar("spPayWorkLocation", prmWorkLocation).ToInt32() > 0;
        }

        public bool WorkLocationNameAlreadyExists(int WorkLocationID, string LocationName, int intCompanyID)
        {
            int result = 0;
            prmWorkLocation = new ArrayList();
            prmWorkLocation.Add(new SqlParameter("@Mode", 9));
            prmWorkLocation.Add(new SqlParameter("@WorkLocationID", WorkLocationID));
            prmWorkLocation.Add(new SqlParameter("@LocationName", LocationName));
            prmWorkLocation.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmWorkLocation.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return  MobjDataLayer.ExecuteScalar("spPayWorkLocation", prmWorkLocation).ToInt32() > 0;
        }
    }
}
