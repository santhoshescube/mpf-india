﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.ComponentModel;

/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,,DAL for UserInformation>
================================================
*/

namespace MyPayfriend
{
    public class clsDALUserInfomation : IDisposable
    {

        ArrayList alParameters;

        public clsDTOUserInformation objclsDTOUserInformation { get; set; }
        public DataLayer objclsconnection { get; set; }
        private string strProcedureName = "spUser";


        /// <summary>
        /// Save User Information
        /// </summary>
        /// <param name="AddStatus">true if save otherwise false</param>
        /// <returns>true if success otherwise returns false</returns>
        public bool SaveUser(bool AddStatus)
        {
            object objOutUserID = 0;

            alParameters = new ArrayList();

            if (AddStatus)
                alParameters.Add(new SqlParameter("@Mode", 2));// Insert
            else
                alParameters.Add(new SqlParameter("@Mode", 3));// Update
            alParameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));
            //alParameters.Add(new SqlParameter("@RoleID", objclsDTOUserInformation.intRoleID));

            if (objclsDTOUserInformation.intEmployeeID > 0)
                alParameters.Add(new SqlParameter("@EmployeeID ", objclsDTOUserInformation.intEmployeeID));

            alParameters.Add(new SqlParameter("@UserName", objclsDTOUserInformation.strUserName));
            alParameters.Add(new SqlParameter("@Password", objclsDTOUserInformation.strPassword));
            alParameters.Add(new SqlParameter("@EmailID", objclsDTOUserInformation.strEmailID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            alParameters.Add(objParam);


            if (objclsconnection.ExecuteNonQuery(strProcedureName, alParameters, out objOutUserID) != 0)//[STUserInformations]
            {
                if ((int)objOutUserID > 0)
                {
                    objclsDTOUserInformation.intUserID = (int)objOutUserID;
                    SaveUserCompanyDetails();
                    return true;
                }
            }


            return false;
        }

        /// <summary>
        /// Get Record Count 
        /// </summary>
        /// <returns>Count</returns>
        public int RecCountNavigate(string strSearchKey)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 5));
            alParameters.Add(new SqlParameter("@SearchKey", strSearchKey));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return objclsconnection.ExecuteScalar(strProcedureName, alParameters).ToInt32();
        }


        /// <summary>
        /// Display User Information
        /// </summary>
        /// <param name="rowno">Record Number</param>
        /// <returns>true if success otherwise returns false</returns>
        public bool DisplayUser(int intRowNumber, string strSearchKey)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@RowNumber", intRowNumber));
            alParameters.Add(new SqlParameter("@SearchKey", strSearchKey));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            alParameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            SqlDataReader sdrDr = objclsconnection.ExecuteReader(strProcedureName, alParameters, CommandBehavior.SingleRow);

            if (sdrDr.Read())
            {
                objclsDTOUserInformation.intUserID = Convert.ToInt32(sdrDr["UserID"]);
                //objclsDTOUserInformation.intRoleID = Convert.ToInt32(sdrDr["RoleID"]);

                if (sdrDr["EmployeeID"] != DBNull.Value)
                    objclsDTOUserInformation.intEmployeeID = Convert.ToInt32(sdrDr["EmployeeID"]);

                objclsDTOUserInformation.strUserName = Convert.ToString(sdrDr["UserName"]);
                objclsDTOUserInformation.strPassword = Convert.ToString(sdrDr["Password"]);
                objclsDTOUserInformation.strEmailID = Convert.ToString(sdrDr["EmailID"]);
                sdrDr.Close();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Delete User Information
        /// </summary>
        /// <returns>true if success otherwise returns false</returns>
        public bool DeleteUserInformation()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 4));// Delete
            alParameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));

            if (objclsconnection.ExecuteNonQuery(strProcedureName, alParameters) != 0)
            {
                return true;
            }
            return false;
        }


        public int DeleteUserValidation()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 15));// Delete
            alParameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));

            return objclsconnection.ExecuteScalar(strProcedureName, alParameters).ToInt32();
           
        }
        /// <summary>
        /// Fill Combos
        /// </summary>
        /// <param name="sarFieldValues"></param>
        /// <returns>datatable</returns>
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsconnection;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }
            }
            return null;
        }


        /// <summary>
        /// Validate Email
        /// </summary>
        /// <param name="sEmailAddress"></param>
        /// <returns>true if success otherwise returns false</returns>
        public bool CheckValidEmail(string sEmailAddress)
        {
            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                return objCommonUtility.CheckValidEmail(sEmailAddress);
            }
        }

        /// <summary>
        /// Check Duplicate User Name 
        /// </summary>
        /// <param name="blnAddStatus">true if save otherwise false</param>
        /// <param name="saValues">array that contains user name</param>
        /// <param name="intId"></param>
        /// <returns>true if success otherwise returns false</returns>
        public bool CheckDuplication(bool blnAddStatus, string strUserName, int intUserID)
        {
            string sField = "UserName";
            string sTable = "UserMaster";
            string sCondition = "";

            if (blnAddStatus) // Add New Mode           
                sCondition = "UserName='" + strUserName + "'";
            else // Update Mode                
                sCondition = "UserName='" + strUserName + "' and  UserID <>" + intUserID + "";

            using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
            {
                objCommonUtility.PobjDataLayer = objclsconnection;
                return objCommonUtility.CheckDuplication(new string[] { sField, sTable, sCondition });
            }
        }

        /// <summary>
        /// Check If Reference Exists while Deleting user
        /// </summary>
        /// <param name="intUserID">UserID</param>
        /// <returns>datatable with count of tables</returns>
        public DataTable CheckExistReferences(int intUserID)
        {
            alParameters = new ArrayList();
            string strCondition = "name <>'UserMaster' AND name <> 'ProcessingTimeMaster' and name <> 'Alerts'";
            alParameters.Add(new SqlParameter("@PrimeryID", intUserID));
            alParameters.Add(new SqlParameter("@Condition", strCondition));
            alParameters.Add(new SqlParameter("@FieldName", "UserID"));
            return objclsconnection.ExecuteDataSet("spCheckIDExists", alParameters).Tables[0];
        }

        public DataTable GetAutoCompleteList(string strSearchKey)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 13));
            alParameters.Add(new SqlParameter("@SearchKey", strSearchKey));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            alParameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return objclsconnection.ExecuteDataTable(strProcedureName, alParameters);
        }

        public DataTable GetRoleCount(bool  lnAddStatus)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 14));
            if (lnAddStatus == true)
            {
                alParameters.Add(new SqlParameter("@CategoryFlg", 1));
                //alParameters.Add(new SqlParameter("@RoleID", objclsDTOUserInformation.intRoleID));
            }
            else
            {
                alParameters.Add(new SqlParameter("@CategoryFlg", 0));
                //alParameters.Add(new SqlParameter("@RoleID", objclsDTOUserInformation.intRoleID));
                alParameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));
            }
            return objclsconnection.ExecuteDataTable(strProcedureName, alParameters);
        }
        private void SaveUserCompanyDetails()
        {
            DeleteUserCompany();

            object objoutRowsAffected = 2;
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 16));
            alParameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));
            alParameters.Add(new SqlParameter("@XmlCompanyDetails", objclsDTOUserInformation.lstUserInformation.ToXml()));
            objclsconnection.ExecuteNonQueryWithTran(strProcedureName, alParameters, out objoutRowsAffected);

        }
        private void DeleteUserCompany()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 17));
            alParameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));
            objclsconnection.ExecuteNonQuery(strProcedureName, alParameters);
        }
        public DataTable getUserCompanyDetails()
        {
            alParameters = new ArrayList();
           
            alParameters.Add(new SqlParameter("@Mode", 18));
            alParameters.Add(new SqlParameter("@UserID", objclsDTOUserInformation.intUserID));
            
            return objclsconnection.ExecuteDataTable(strProcedureName, alParameters);
        }



        public int IsEmployeeExists()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 20));
            alParameters.Add(new SqlParameter("@EmployeeID", objclsDTOUserInformation.intEmployeeID));
            return objclsconnection.ExecuteScalar(strProcedureName, alParameters).ToInt32() ;
        }

     
        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (alParameters != null)
                alParameters = null;
        }

        #endregion
    }
}