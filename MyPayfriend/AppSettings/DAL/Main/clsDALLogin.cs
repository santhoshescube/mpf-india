﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALLogin
    {
        ArrayList prmLogin;                                          // array list for storing parameters
        public DataLayer MobjDataLayer;                                  // obj of datalayer    

        public clsDALLogin(DataLayer objDataLayer)
        {
            //constructor
            MobjDataLayer = objDataLayer;
        }

        public DataTable GetUserInformation(string strUserName, string strPassword)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 1));
            prmLogin.Add(new SqlParameter("@UserName", strUserName));
            prmLogin.Add(new SqlParameter("@Password", strPassword));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }

        public DataTable GetTopMostCompany()
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 2));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }
        //public DataTable GetUserCompany()
        //{
        //    prmLogin = new ArrayList();
        //    prmLogin.Add(new SqlParameter("@Mode", 8));
        //    prmLogin.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CompanyID));
        //    return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        //}

        public DataTable GetDefaultConfigurationSettings()
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 3));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }
        public DataTable GetCompanySettingsData(int intCompanyID)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 4));
            prmLogin.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }
        public DataTable GetCompanySettings(int intCompanyID)
        
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 4));
            prmLogin.Add(new SqlParameter("@CompanyID", intCompanyID));
            DataTable dtCompanySettingsInfo = MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);

            string strValue = "";

           
            //---------------------------------------------------Create/Modify
            //ClsCommonSettings.strEmployeeNumberPrefix = GetCompanySettingsValue("Employee Number", "Prefix", dtCompanySettingsInfo);
            //ClsCommonSettings.blnEmployeeNumberAutogenerate = ConvertStringToBoolean(GetCompanySettingsValue("Employee Number", "Prefix Editable", dtCompanySettingsInfo));
            
           
            return dtCompanySettingsInfo;
        }
        private bool ConvertStringToBoolean(string strValue)
        {
            if (strValue == "Yes")
                return true;
            return false;
        }
        public string GetCompanySettingsValue(string strConfigurationItem, string strSubItem, DataTable dtCompanySettings)
        {
            dtCompanySettings.DefaultView.RowFilter = "ConfigurationItem = '" + strConfigurationItem + "' And SubItem = '" + strSubItem + "'";
            if (dtCompanySettings.DefaultView.ToTable().Rows.Count > 0)
                return dtCompanySettings.DefaultView.ToTable().Rows[0]["ConfigurationValue"].ToString();
            return "";
        }
        public void UpdateRemember(string strUserName, bool blnRemember)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 5));
            prmLogin.Add(new SqlParameter("@UserName", strUserName));
            prmLogin.Add(new SqlParameter("@IsRemember", blnRemember));
            prmLogin.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            MobjDataLayer.ExecuteNonQuery("spLoginOperations", prmLogin);
        }

        public string GetPassword(string strUserName)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 6));
            prmLogin.Add(new SqlParameter("@UserName", strUserName));
            return Convert.ToString(MobjDataLayer.ExecuteScalar("spLoginOperations", prmLogin));
        }
        public string GetNationalityIDDocType()
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 10));
            prmLogin.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return Convert.ToString(MobjDataLayer.ExecuteScalar("spLoginOperations", prmLogin));
        }

        public bool GetRememberStatus(string strUserName)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 7));
            prmLogin.Add(new SqlParameter("@UserName", strUserName));
            DataTable dtUser = MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
            if (dtUser.Rows.Count > 0)
                return Convert.ToBoolean(dtUser.Rows[0]["IsRemember"]);
            return false;
        }

        public bool GetArabicViewStatus()
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 9));
            return Convert.ToBoolean(MobjDataLayer.ExecuteScalar("spLoginOperations", prmLogin));
        }
        public DataTable getCompanyList(int intUserID)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 11));
            prmLogin.Add(new SqlParameter("@UserID", intUserID));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }


        public DataTable GetCompanyDetails(int intCompanyID)
        {
            prmLogin = new ArrayList();
            prmLogin.Add(new SqlParameter("@Mode", 12));
            prmLogin.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            prmLogin.Add(new SqlParameter("@CompanyID", intCompanyID));
            return MobjDataLayer.ExecuteDataTable("spLoginOperations", prmLogin);
        }
    }
}
