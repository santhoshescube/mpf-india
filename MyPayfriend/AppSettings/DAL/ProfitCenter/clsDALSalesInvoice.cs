﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALSalesInvoice
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOSalesInvoice objclsDTOSalesInvoice { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList alParameters;
        string strProcName = "spSalesInvoice";

        public clsDALSalesInvoice(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public bool SaveForm()
        {
            alParameters = new ArrayList();

            if (objclsDTOSalesInvoice.SalesInvoiceID == 0)
                alParameters.Add(new SqlParameter("@Mode", 1));
            else
                alParameters.Add(new SqlParameter("@Mode", 3));

            alParameters.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSalesInvoice.SalesInvoiceID));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOSalesInvoice.CompanyID));
            alParameters.Add(new SqlParameter("@ProfitCenterID", objclsDTOSalesInvoice.ProfitCenterID));
            alParameters.Add(new SqlParameter("@Month", objclsDTOSalesInvoice.Month));
            alParameters.Add(new SqlParameter("@Remarks", objclsDTOSalesInvoice.Remarks));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            alParameters.Add(new SqlParameter("@XmlDetails", objclsDTOSalesInvoice.SalesInvoiceDetails.ToXml()));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            alParameters.Add(objParam);

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, alParameters) != 0)
                return true;

            return false;
        }

        public bool DeleteForm()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 4));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", objclsDTOSalesInvoice.SalesInvoiceID));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, alParameters) != 0)
                return true;

            return false;
        }

        public DataTable GetSearchSalesInvoice(int CompanyID, int ProfitCenterID, int EmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 5));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@ProfitCenterID", ProfitCenterID));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@MonthFrom", dtFromDate));
            alParameters.Add(new SqlParameter("@MonthTo", dtToDate));
            return MobjDataLayer.ExecuteDataTable(strProcName, alParameters);
        }

        public DataTable DisplayInfo(int SalesInvoiceID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 6));
            alParameters.Add(new SqlParameter("@SalesInvoiceID", SalesInvoiceID));
            return MobjDataLayer.ExecuteDataTable(strProcName, alParameters);
        }
    }
}