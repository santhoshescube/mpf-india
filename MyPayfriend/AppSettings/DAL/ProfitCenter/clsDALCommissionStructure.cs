﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALCommissionStructure
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOCommissionStructure objclsDTOCommissionStructure { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList alParameters;
        string strProcName = "spCommissionStructure";

        public clsDALCommissionStructure(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public bool SaveForm()
        {
            alParameters = new ArrayList();

            if (objclsDTOCommissionStructure.CommissionStructureID == 0)
                alParameters.Add(new SqlParameter("@Mode", 1));
            else
                alParameters.Add(new SqlParameter("@Mode", 4));

            alParameters.Add(new SqlParameter("@CommissionStructureID", objclsDTOCommissionStructure.CommissionStructureID));
            alParameters.Add(new SqlParameter("@StructureName", objclsDTOCommissionStructure.StructureName));
            alParameters.Add(new SqlParameter("@Deduction", objclsDTOCommissionStructure.Deduction));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            alParameters.Add(new SqlParameter("@XmlGroupDetails", objclsDTOCommissionStructure.CommissionStructureGroupDetails.ToXml()));
            alParameters.Add(new SqlParameter("@XmlExcessDetails", objclsDTOCommissionStructure.CommissionStructureExcessTarget.ToXml()));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            alParameters.Add(objParam);

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, alParameters) != 0)
                return true;

            return false;
        }

        public bool DeleteForm()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 5));
            alParameters.Add(new SqlParameter("@CommissionStructureID", objclsDTOCommissionStructure.CommissionStructureID));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, alParameters) != 0)
                return true;

            return false;
        }

        public DataTable GetSearchCommissionStructure()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 6));
            return MobjDataLayer.ExecuteDataTable(strProcName, alParameters);
        }

        public DataSet DisplayInfo(int CommissionStructureID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 7));
            alParameters.Add(new SqlParameter("@CommissionStructureID", CommissionStructureID));
            return MobjDataLayer.ExecuteDataSet(strProcName, alParameters);
        }
    }
}
