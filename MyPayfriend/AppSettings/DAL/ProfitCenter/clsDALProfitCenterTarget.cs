﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALProfitCenterTarget
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOProfitCenterTarget objclsDTOProfitCenterTarget { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList alParameters;
        string strProcName = "spProfitCenterTarget";

        public clsDALProfitCenterTarget(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public bool SaveForm()
        {
            alParameters = new ArrayList();

            if (objclsDTOProfitCenterTarget.TargetID == 0)
                alParameters.Add(new SqlParameter("@Mode", 1));
            else
                alParameters.Add(new SqlParameter("@Mode", 3));

            alParameters.Add(new SqlParameter("@TargetID", objclsDTOProfitCenterTarget.TargetID));
            alParameters.Add(new SqlParameter("@CompanyID", objclsDTOProfitCenterTarget.CompanyID));
            alParameters.Add(new SqlParameter("@ProfitCenterID", objclsDTOProfitCenterTarget.ProfitCenterID));
            alParameters.Add(new SqlParameter("@MonthFrom", objclsDTOProfitCenterTarget.MonthFrom));
            alParameters.Add(new SqlParameter("@MonthTo", objclsDTOProfitCenterTarget.MonthTo));
            alParameters.Add(new SqlParameter("@Target", objclsDTOProfitCenterTarget.Target));
            alParameters.Add(new SqlParameter("@Remarks", objclsDTOProfitCenterTarget.Remarks));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            alParameters.Add(new SqlParameter("@XmlDetails", objclsDTOProfitCenterTarget.ProfitCenterTargetDetails.ToXml()));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.Int);
            objParam.Direction = ParameterDirection.ReturnValue;
            alParameters.Add(objParam);

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, alParameters) != 0)
                return true;

            return false;
        }

        public bool DeleteForm()
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 4));
            alParameters.Add(new SqlParameter("@TargetID", objclsDTOProfitCenterTarget.TargetID));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, alParameters) != 0)
                return true;

            return false;
        }

        public DataTable GetSearchProfitCenterTarget(int CompanyID, int ProfitCenterID, int EmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 5));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@ProfitCenterID", ProfitCenterID));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@MonthFrom", dtFromDate));
            alParameters.Add(new SqlParameter("@MonthTo", dtToDate));
            return MobjDataLayer.ExecuteDataTable(strProcName, alParameters);
        }

        public DataTable DisplayInfo(int TargetID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 6));
            alParameters.Add(new SqlParameter("@TargetID", TargetID));
            return MobjDataLayer.ExecuteDataTable(strProcName, alParameters);
        }

        public bool CheckProfitCenterExists(int TargetID)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 7));
            alParameters.Add(new SqlParameter("@TargetID", TargetID));
            return Convert.ToBoolean(MobjDataLayer.ExecuteScalar(strProcName, alParameters));
        }
    }
}
