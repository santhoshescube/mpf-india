﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic.Devices;
using System.Globalization;
using System.Windows.Forms;
namespace MyPayfriend
{
    public class clsDALGLCodeMapping
    {
        public clsDTOGLCodeMapping objDTOGLCodeMapping { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList alParameters;
        string strProcName = "spPayGLCodeMapping";

        public clsDALGLCodeMapping(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }

        public DataTable getGLCodeMapping(bool IsSalary)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@IsSalary", IsSalary));
            return MobjDataLayer.ExecuteDataTable(strProcName, alParameters);
        }

        public bool SaveGLCodeMapping(bool IsSalary)
        {
            alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", 2));
            alParameters.Add(new SqlParameter("@IsSalary", IsSalary));
            alParameters.Add(new SqlParameter("@XmlAdditionDeductionDetails", objDTOGLCodeMapping.AdditionDeductionDetails.ToXml()));
            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, alParameters) != 0)
                return true;
            return false;
        }

        public void ExportToCSV(DateTime dtTransactionDate)
        {            
            try
            {                
                DataTable dt = new DataTable();

                alParameters = new ArrayList();
                alParameters.Add(new SqlParameter("@TransactionDate", dtTransactionDate));
                dt = MobjDataLayer.ExecuteDataTable("spPayIntegrationFile", alParameters);

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        string csvFile = ClsCommonSettings.strServerPath + "\\CSVFiles\\HRM" + dtTransactionDate.ToString("ddMMyyyy", CultureInfo.InvariantCulture) + ".CSV";

      
                        System.IO.StreamWriter outFile;
                        Computer myC = new Computer();
                        outFile = myC.FileSystem.OpenTextFileWriter(csvFile, false, Encoding.ASCII);

                        for (int i = 0; dt.Rows.Count - 1 >= i; i++)
                        {
                            string strEDRRow = "";

                            for (int j = 0; dt.Columns.Count > j; ++j)
                            {
                                if (j == Convert.ToInt32(dt.Columns.Count) - 1)
                                    strEDRRow += Convert.ToString(dt.Rows[i][j]);
                                else
                                    strEDRRow += Convert.ToString(dt.Rows[i][j]) + ",";
                            }

                            outFile.WriteLine(strEDRRow);
                        }

                        outFile.Close();
                    }
                }

                
            }
            catch { }
        }
    }
}
