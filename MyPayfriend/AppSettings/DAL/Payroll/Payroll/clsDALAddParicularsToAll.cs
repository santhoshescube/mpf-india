﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    class clsDALAddParicularsToAll
    {
        private ClsCommonUtility MobjClsCommonUtility;
        private DataLayer MobjDataLayer;
        private ArrayList prmAddPariculars;

        public clsDTOAddParicularsToAll PobjClsDTOAddParicularsToAll { get; set; }

        public clsDALAddParicularsToAll(DataLayer objDataLayer)
        {
            this.MobjClsCommonUtility = new ClsCommonUtility();
            this.MobjDataLayer = objDataLayer;
            this.MobjClsCommonUtility.PobjDataLayer = this.MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
                return this.MobjClsCommonUtility.FillCombos(saFieldValues);
            return (DataTable)null;
        }

        public DataTable GetEmployeeDetails(
          int intAdditionDeductionID,
          string strProcessDate,
          int intCompanyID,
          int intDepartmentID)
        {
            return this.MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", new ArrayList()
      {
        (object) new SqlParameter("@Mode", (object) 3),
        (object) new SqlParameter("@AdditionDeductionID", (object) intAdditionDeductionID),
        (object) new SqlParameter("@ProcessDate", (object) strProcessDate),
        (object) new SqlParameter("@CompanyID", (object) intCompanyID),
        (object) new SqlParameter("@DepartmentID", (object) intDepartmentID)
      });
        }

        public bool SaveEmployeeDetails()
        {
            foreach (clsDTOAddParicularsToAllDetail paricularsToAllDetail in this.PobjClsDTOAddParicularsToAll.DTOAddParicularsToAllDetail)
            {
                this.prmAddPariculars = new ArrayList();
                this.prmAddPariculars.Add((object)new SqlParameter("@Mode", (object)1));
                this.prmAddPariculars.Add((object)new SqlParameter("@PaymentID", (object)paricularsToAllDetail.PaymentID));
                this.prmAddPariculars.Add((object)new SqlParameter("@CurrencyID", (object)paricularsToAllDetail.CurrencyID));
                this.prmAddPariculars.Add((object)new SqlParameter("@AddDedID", (object)paricularsToAllDetail.AddDedID));
                this.prmAddPariculars.Add((object)new SqlParameter("@Amount", (object)paricularsToAllDetail.Amount));
                this.prmAddPariculars.Add((object)new SqlParameter("@IsAddition", (object)paricularsToAllDetail.IsAddition));
                this.MobjDataLayer.ExecuteNonQueryWithTran("spPayAddAdditionDeduction", this.prmAddPariculars);
            }
            return true;
        }

        public DataSet DisplaySalaryAmendment()
        {
            try
            {
                return this.MobjDataLayer.ExecuteDataSet("spPayAddAdditionDeduction", new ArrayList()
        {
          (object) new SqlParameter("@Mode", (object) 9),
          (object) new SqlParameter("@SalaryStructureAmendmentID", (object) this.PobjClsDTOAddParicularsToAll.AmendmentCodeID)
        });
            }
            catch (Exception ex)
            {
                return (DataSet)null;
            }
        }

        public bool DeleteSalaryAmendment()
        {
            try
            {
                this.MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", new ArrayList()
        {
          (object) new SqlParameter("@Mode", (object) 7),
          (object) new SqlParameter("@SalaryStructureAmendmentID", (object) this.PobjClsDTOAddParicularsToAll.AmendmentCodeID)
        });
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataTable GetEmployees(
          int GradeID,
          int DesignationID,
          int DepartmentID,
          int BusinessUnitID)
        {
            return this.MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", new ArrayList()
      {
        (object) new SqlParameter("@Mode", (object) 5),
        (object) new SqlParameter("@GradeID", (object) GradeID),
        (object) new SqlParameter("@DesignationID", (object) DesignationID),
        (object) new SqlParameter("@DepartmentID", (object) DepartmentID),
        (object) new SqlParameter("@BusinessUnitID", (object) BusinessUnitID)
      });
        }

        public bool DeleteSingleRow(int EmployeeID, int AmendmentID, int Particular)
        {
            if (this.MobjDataLayer.ExecuteDataTable("SELECT 1 FROM PaySalaryStructureAmendment AS A INNER JOIN PaySalaryStructureAmendmentMaster AS B ON A.SalaryStructureAmendmentID = B.SalaryStructureAmendmentID  INNER JOIN PayEmployeePaymentAmendment AS EPA ON A.EffectiveDate = EPA.EffectiveDate AND A.AdditionDeductionID = EPA.AdditionDeductionID and EPA.SalaryStructureAmendmentID=A.SalaryStructureAmendmentID INNER JOIN PayEmployeePayment AS EP ON EPA.PaymentID = EP.PaymentID AND A.EmployeeID = EP.EmployeeID Where  A.SalaryStructureAmendmentID=" + (object)AmendmentID + " and A.EmployeeID =" + (object)EmployeeID).Rows.Count > 0)
                return false;
            if (this.MobjDataLayer.ExecuteDataTable("Select 1  from PaySalaryStructureAmendment where SalaryStructureAmendmentID= " + (object)AmendmentID + " and EmployeeID =" + (object)EmployeeID + " AND AdditionDeductionID=" + (object)Particular).Rows.Count <= 0)
                return false;
            this.MobjDataLayer.ExecuteQuery("Delete from PaySalaryStructureAmendment where SalaryStructureAmendmentID= " + (object)AmendmentID + " and EmployeeID =" + (object)EmployeeID + " AND AdditionDeductionID=" + (object)Particular);
            return true;
        }

        public DataTable GetAllEmployee()
        {
            return this.MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", new ArrayList()
      {
        (object) new SqlParameter("@Mode", (object) 12)
      });
        }

        public DataTable GetAllAddDed()
        {
            return this.MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", new ArrayList()
      {
        (object) new SqlParameter("@Mode", (object) 11)
      });
        }

        public DataTable FillGrid(int DesignationID, int DepartmentID, int EmployeeID)
        {
            return this.MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", new ArrayList()
      {
        (object) new SqlParameter("@Mode", (object) 8),
        (object) new SqlParameter("@DesignationID", (object) DesignationID),
        (object) new SqlParameter("@DepartmentID", (object) DepartmentID),
        (object) new SqlParameter("@EmployeeID", (object) EmployeeID)
      });
        }

        public bool Duplication(int txtAmendmentCodeID, string txtAmendmentCode)
        {
            DataTable dataTable = new DataTable();
            return this.MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", new ArrayList()
      {
        (object) new SqlParameter("@Mode", (object) 10),
        (object) new SqlParameter("@SalaryStructureAmendmentID", (object) txtAmendmentCodeID),
        (object) new SqlParameter("@SalaryAmendmentCode", (object) txtAmendmentCode)
      }).Rows.Count > 0;
        }

        public bool SaveSalaryAmendment()
        {
            try
            {
                DataTable dataTable = this.MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", new ArrayList()
        {
          (object) new SqlParameter("@Mode", (object) 6),
          (object) new SqlParameter("@SalaryAmendmentCode", (object) this.PobjClsDTOAddParicularsToAll.AmendmentCode),
          (object) new SqlParameter("@SalaryStructureAmendmentID", (object) this.PobjClsDTOAddParicularsToAll.AmendmentCodeID),
          (object) new SqlParameter("@EmployeeID", (object) ClsCommonSettings.intEmployeeID),
          (object) new SqlParameter("@XmlSalaryAmendment", (object) this.PobjClsDTOAddParicularsToAll.DTOSalaryAmendmentDetail.ToXml<clsDTOSalaryAmendmentDetail>())
        });
                if (dataTable.Rows[0]["SalaryStructureAmendmentID"].ToInt32() <= 0)
                    return false;
                this.PobjClsDTOAddParicularsToAll.AmendmentCodeID = dataTable.Rows[0]["SalaryStructureAmendmentID"].ToInt32();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataTable GetEmployeeDetails(string strProcessDate, int intCompanyID)
        {
            return this.MobjDataLayer.ExecuteDataTable("spPayAddAdditionDeduction", new ArrayList()
      {
        (object) new SqlParameter("@Mode", (object) 3),
        (object) new SqlParameter("@ProcessDate", (object) strProcessDate),
        (object) new SqlParameter("@CompanyID", (object) intCompanyID)
      });
        }

        public bool SaveEmployeeDetailsForPayment()
        {
            foreach (clsDTOAddParicularsToAllDetail paricularsToAllDetail in this.PobjClsDTOAddParicularsToAll.DTOAddParicularsToAllDetail)
            {
                this.prmAddPariculars = new ArrayList();
                this.prmAddPariculars.Add((object)new SqlParameter("@Mode", (object)4));
                this.prmAddPariculars.Add((object)new SqlParameter("@PaymentID", (object)paricularsToAllDetail.PaymentID));
                this.prmAddPariculars.Add((object)new SqlParameter("@CurrencyID", (object)paricularsToAllDetail.CurrencyID));
                this.prmAddPariculars.Add((object)new SqlParameter("@AddDedID", (object)paricularsToAllDetail.AddDedID));
                this.prmAddPariculars.Add((object)new SqlParameter("@Amount", (object)paricularsToAllDetail.Amount));
                this.prmAddPariculars.Add((object)new SqlParameter("@IsAddition", (object)paricularsToAllDetail.IsAddition));
                this.MobjDataLayer.ExecuteNonQueryWithTran("spPayAddAdditionDeduction", this.prmAddPariculars);
            }
            return true;
        }


    }
}
