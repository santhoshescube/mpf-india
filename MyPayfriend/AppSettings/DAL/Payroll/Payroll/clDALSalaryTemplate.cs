﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;


namespace MyPayfriend
{
    public class clsDALSalaryTemplate

    {

        #region Declartions

        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOSalaryTemplate objDTOSalaryTemplate { get; set; }
        public DataLayer MobjDataLayer;
        private List<SqlParameter> sqlParameters = null;
        #endregion Declartions 

        #region Properties
        public clsDALSalaryTemplate()
        {
            MobjDataLayer = new DataLayer();
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        #endregion Properties
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = MobjDataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        public bool GetSalaryTemplateExists(int SalaryTempId)
        {
            //function for checking company exists or not
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>();
            this.sqlParameters.Add(new SqlParameter("@Mode", "8"));
            this.sqlParameters.Add(new SqlParameter("@SalaryTempId", SalaryTempId));
            SqlDataReader sdr = MobjDataLayer.ExecuteReader("spSalaryTemplate", sqlParameters);
            if (sdr.Read())
            {
                if (Convert.ToInt32(sdr[0]) != 0)
                {
                    blnRetValue = true;
                }
                sdr.Close();
            }
            return blnRetValue;
        }
        public bool SavePolicy()
        {

            bool blnRetValue = false;

            this.sqlParameters = new List<SqlParameter>();
            {

                this.sqlParameters.Add(new SqlParameter("@Mode",1));
                this.sqlParameters.Add(new SqlParameter("@SalaryTempId", this.objDTOSalaryTemplate.SalaryTempId));
                this.sqlParameters.Add(new SqlParameter("@SalaryTempName", objDTOSalaryTemplate.SalaryTempName));
                //this.sqlParameters.Add(new SqlParameter("@SalaryTempGrossAmount", objDTOSalaryTemplate.SalaryTempGrossAmount));
                this.sqlParameters.Add(new SqlParameter("@OTPolicyID", objDTOSalaryTemplate.OTPolicyID));
                this.sqlParameters.Add(new SqlParameter("@AbsentPolicyID", objDTOSalaryTemplate.AbsentPolicyID));
                this.sqlParameters.Add(new SqlParameter("@HolidayPolicyID", objDTOSalaryTemplate.HolidayPolicyID));
                this.sqlParameters.Add(new SqlParameter("@EncashPolicyID", objDTOSalaryTemplate.EncashPolicyID));
                this.sqlParameters.Add(new SqlParameter("@SetPolicyID", objDTOSalaryTemplate.SetPolicyID));
                this.sqlParameters.Add(new SqlParameter("@XmlSalTemp", objDTOSalaryTemplate.lstclsDTOSalaryTemplateDetails.ToXml()));
                this.sqlParameters.Add(new SqlParameter("@XmlSalTempDed", objDTOSalaryTemplate.lstclsDTOSalaryTemplateDeduction.ToXml()));
            }
            if (this.MobjDataLayer.ExecuteNonQuery("spSalaryTemplate", this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        public bool CheckDuplicate(int SalTempID, string salTempName)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@SalaryTempName", salTempName),
                new SqlParameter("@SalaryTempId", SalTempID)
                 };
            intExists = this.MobjDataLayer.ExecuteScalar("spSalaryTemplate", this.sqlParameters).ToInt32();
            return (intExists > 0);
        }
        public DataSet FillDetails(int CurrentRecCnt)
        {

            DataSet dSet = new DataSet();
            this.sqlParameters = new List<SqlParameter>
            {
                new SqlParameter("@Mode",4),
                new SqlParameter("@RowNumber",CurrentRecCnt)
            };
            return dSet = this.MobjDataLayer.ExecuteDataSet("spSalaryTemplate", this.sqlParameters);
        }
        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 5) };
            intRecordCount = this.MobjDataLayer.ExecuteScalar("spSalaryTemplate", this.sqlParameters).ToInt32();
            return intRecordCount;
        }
        public bool DeleteSalaryTemplate()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>
            {
                 new SqlParameter("@Mode",6 ),
                new SqlParameter("@SalaryTempId",objDTOSalaryTemplate.SalaryTempId)

            };
            if (this.MobjDataLayer.ExecuteNonQuery("spSalaryTemplate", this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;

        }
        public int GetRowNumber(int intSalTemplateID)
        {
            int intRownum = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 7), new SqlParameter("@SalaryTempId", intSalTemplateID) };
            intRownum = this.MobjDataLayer.ExecuteScalar("spSalaryTemplate", this.sqlParameters).ToInt32();
            return intRownum;
        }
    }
}
