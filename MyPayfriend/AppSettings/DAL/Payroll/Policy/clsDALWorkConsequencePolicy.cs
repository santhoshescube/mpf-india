﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    /*********************************************
      * Author       : Arun
      * Created On   : 10 Apr 2012
      * Purpose      : WorkConsequence Policy Database transactions 
      * ******************************************/
    public class clsDALWorkConsequencePolicy
    {

        #region Declartions

        private clsDTOWorkConsequencePolicy MobjDTOWorkConsequencePolicy = null;
        private string strProcedureName = "spPayWorkConsequencePolicy";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALWorkConsequencePolicy() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOWorkConsequencePolicy DTOWorkConsequencePolicy
        {
            get
            {
                if (this.MobjDTOWorkConsequencePolicy == null)
                    this.MobjDTOWorkConsequencePolicy = new clsDTOWorkConsequencePolicy();

                return this.MobjDTOWorkConsequencePolicy;
            }
            set
            {
                this.MobjDTOWorkConsequencePolicy = value;
            }
        }

        #endregion Declartions

        #region GetRowNumber

        /// <summary>
        /// Get The RowNumber of given WorkConsequence policy
        /// </summary>
        /// <param name="intWorkConsequencePolicyID"></param>
        /// <returns></returns>


        public int GetRowNumber(int intWorkConsequencePolicyID)
        {
            int intRownum = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 11), new SqlParameter("@WorkConsequencePolicyID", intWorkConsequencePolicyID) };
            intRownum = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRownum;
        }
        #endregion GetRowNumber

        #region GetRecordCount

        /// <summary>
        /// Get the total record Count
        /// </summary>
        /// <returns></returns>
        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }

        #endregion GetRecordCount

        #region WorkConsequencePolicyMasterSave

        /// <summary>
        /// WorkConsequence Policy master Save
        /// </summary>
        /// <returns></returns>
        public bool WorkConsequencePolicyMasterSave()
        {
            bool blnRetValue = false;
            object objWorkConsequencePolicyID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (DTOWorkConsequencePolicy.WorkConsequencePolicyID > 0)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", 2));
                this.sqlParameters.Add(new SqlParameter("@WorkConsequencePolicyID", this.DTOWorkConsequencePolicy.WorkConsequencePolicyID));
            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 1));
            }
            this.sqlParameters.Add(new SqlParameter("@WorkConsequencePolicy", this.DTOWorkConsequencePolicy.WorkConsequencePolicy));
            //this.sqlParameters.Add(new SqlParameter("@WorkConsequencePolicyID", this.DTOWorkConsequencePolicy.WorkConsequencePolicyID));
            if (this.DTOWorkConsequencePolicy.CalculationID > 0)
                this.sqlParameters.Add(new SqlParameter("@CalculationID", this.DTOWorkConsequencePolicy.CalculationID));
            this.sqlParameters.Add(new SqlParameter("@IsCompanyBasedOn", this.DTOWorkConsequencePolicy.CompanyBasedOn));
            this.sqlParameters.Add(new SqlParameter("@Percentage", this.DTOWorkConsequencePolicy.Percentage));
            this.sqlParameters.Add(new SqlParameter("@IsRateOnly", this.DTOWorkConsequencePolicy.IsRateOnly));
            this.sqlParameters.Add(new SqlParameter("@WorkConsequenceRate", this.DTOWorkConsequencePolicy.WorkConsequenceRate));
            //this.sqlParameters.Add(new SqlParameter("@IsRateBasedOnHour", this.DTOWorkConsequencePolicy.IsRateBasedOnHour));
            //this.sqlParameters.Add(new SqlParameter("@HoursPerDay", this.DTOWorkConsequencePolicy.HoursPerDay));
            this.sqlParameters.Add(new SqlParameter("@WorkConsequenceType", this.DTOWorkConsequencePolicy.WorkConsequenceType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;

            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objWorkConsequencePolicyID) != 0)
            {
                this.DTOWorkConsequencePolicy.WorkConsequencePolicyID = (int)objWorkConsequencePolicyID;
                blnRetValue = true;
            }


            return blnRetValue;

        }
        #endregion WorkConsequencePolicyMasterSave


        #region SalaryPolicyDetailsSave

        /// <summary>
        /// Save SalaryPolicy Detils info Save
        /// </summary>
        /// <returns></returns>
        public bool SalaryPolicyDetailsSave()
        {
            bool blnRetValue = false;
            foreach (clsDTOSalaryPolicyDetailWorkConsequence clsDTOWorkConsequenceSalDet in DTOWorkConsequencePolicy.DTOSalaryPolicyDetailWorkConsequence)
            {
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@Mode", 7));
                this.sqlParameters.Add(new SqlParameter("@WorkConsequencePolicyID", this.DTOWorkConsequencePolicy.WorkConsequencePolicyID));
                this.sqlParameters.Add(new SqlParameter("@AdditionDeductionID", clsDTOWorkConsequenceSalDet.AdditionDeductionID));
                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }

        #endregion SalaryPolicyDetailsSave

        #region DisplayWorkConsequencePolicy
        public DataTable DisplayWorkConsequencePolicy(int intRowNumber)
        {

            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@RowNumber", intRowNumber) };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion DisplayWorkConsequencePolicy

        #region DisplayAddDedDetails
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 9),
                new SqlParameter("@WorkConsequencePolicyID", iPolicyID)
        };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion DisplayAddDedDetails

        #region DeleteWorkConsequencePolicy
        public bool DeleteWorkConsequencePolicy()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",4),
                                    new SqlParameter("@WorkConsequencePolicyID", this.DTOWorkConsequencePolicy.WorkConsequencePolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        #endregion DeleteWorkConsequencePolicy

        #region DeleteWorkConsequencePolicyDetails
        public bool DeleteWorkConsequencePolicyDetails()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",6),
                                    new SqlParameter("@WorkConsequencePolicyID", this.DTOWorkConsequencePolicy.WorkConsequencePolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        #endregion DeleteWorkConsequencePolicyDetails

        #region GetAdditionDeductions
   
        /// <summary>
        /// Selecting the Addition Deduction refernce
        /// </summary>
        /// <returns></returns>
        public DataTable GetAdditionDeductions()
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8) };
             return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        #endregion GetAdditionDeductions

        #region CheckDuplicate
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 12),
                new SqlParameter("@WorkConsequencePolicy", strPolicyName),
                new SqlParameter("@WorkConsequencePolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);
        }
        #endregion CheckDuplicate

        #region PolicyIDExists

        public bool PolicyIDExists(int iPolicyID)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 13),
                new SqlParameter("@WorkConsequencePolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }
        #endregion PolicyIDExists

        #region FillCombos

        /// <summary>
        /// 
        /// return datatable  for filling combos
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns></returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        #endregion FillCombos
    }
}
