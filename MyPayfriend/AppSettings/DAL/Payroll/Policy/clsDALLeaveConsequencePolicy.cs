﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    /*********************************************
      * Author       : Arun
      * Created On   : 10 Apr 2012
      * Purpose      : LeaveConsequence Policy Database transactions 
      * ******************************************/
    public class clsDALLeaveConsequencePolicy
    {

        #region Declartions

        private clsDTOLeaveConsequencePolicy MobjDTOLeaveConsequencePolicy = null;
        private string strProcedureName = "spPayLeaveConsequencePolicy";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALLeaveConsequencePolicy() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOLeaveConsequencePolicy DTOLeaveConsequencePolicy
        {
            get
            {
                if (this.MobjDTOLeaveConsequencePolicy == null)
                    this.MobjDTOLeaveConsequencePolicy = new clsDTOLeaveConsequencePolicy();

                return this.MobjDTOLeaveConsequencePolicy;
            }
            set
            {
                this.MobjDTOLeaveConsequencePolicy = value;
            }
        }

        #endregion Declartions

        #region GetRowNumber

        /// <summary>
        /// Get The RowNumber of given LeaveConsequence policy
        /// </summary>
        /// <param name="intLeaveConsequencePolicyID"></param>
        /// <returns></returns>


        public int GetRowNumber(int intLeaveConsequencePolicyID)
        {
            int intRownum = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 11), new SqlParameter("@LeaveConsequencePolicyID", intLeaveConsequencePolicyID) };
            intRownum = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRownum;
        }
        #endregion GetRowNumber

        #region GetRecordCount

        /// <summary>
        /// Get the total record Count
        /// </summary>
        /// <returns></returns>
        public int GetRecordCount()
        {
            int intRecordCount = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 10) };
            intRecordCount = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return intRecordCount;
        }

        #endregion GetRecordCount

        #region LeaveConsequencePolicyMasterSave

        /// <summary>
        /// LeaveConsequence Policy master Save
        /// </summary>
        /// <returns></returns>
        public bool LeaveConsequencePolicyMasterSave()
        {
            bool blnRetValue = false;
            object objLeaveConsequencePolicyID = 0;
            this.sqlParameters = new List<SqlParameter>();

            if (DTOLeaveConsequencePolicy.LeaveConsequencePolicyID > 0)
            {

                this.sqlParameters.Add(new SqlParameter("@Mode", 2));
                this.sqlParameters.Add(new SqlParameter("@LeaveConsequencePolicyID", this.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID));
            }
            else
            {
                this.sqlParameters.Add(new SqlParameter("@Mode", 1));
            }
            this.sqlParameters.Add(new SqlParameter("@LeaveConsequencePolicy", this.DTOLeaveConsequencePolicy.LeaveConsequencePolicy));
            //this.sqlParameters.Add(new SqlParameter("@LeaveConsequencePolicyID", this.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID));
            if (this.DTOLeaveConsequencePolicy.CalculationID > 0)
                this.sqlParameters.Add(new SqlParameter("@CalculationID", this.DTOLeaveConsequencePolicy.CalculationID));
            this.sqlParameters.Add(new SqlParameter("@IsCompanyBasedOn", this.DTOLeaveConsequencePolicy.CompanyBasedOn));
            this.sqlParameters.Add(new SqlParameter("@Percentage", this.DTOLeaveConsequencePolicy.Percentage));
            this.sqlParameters.Add(new SqlParameter("@IsRateOnly", this.DTOLeaveConsequencePolicy.IsRateOnly));
            this.sqlParameters.Add(new SqlParameter("@LeaveConsequenceRate", this.DTOLeaveConsequencePolicy.LeaveConsequenceRate));
            //this.sqlParameters.Add(new SqlParameter("@IsRateBasedOnHour", this.DTOLeaveConsequencePolicy.IsRateBasedOnHour));
            //this.sqlParameters.Add(new SqlParameter("@HoursPerDay", this.DTOLeaveConsequencePolicy.HoursPerDay));
            this.sqlParameters.Add(new SqlParameter("@LeaveConsequenceType", this.DTOLeaveConsequencePolicy.LeaveConsequenceType));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;

            this.sqlParameters.Add(objParam);
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters, out objLeaveConsequencePolicyID) != 0)
            {
                this.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID = (int)objLeaveConsequencePolicyID;
                blnRetValue = true;
            }


            return blnRetValue;

        }
        #endregion LeaveConsequencePolicyMasterSave


        #region SalaryPolicyDetailsSave

        /// <summary>
        /// Save SalaryPolicy Detils info Save
        /// </summary>
        /// <returns></returns>
        public bool SalaryPolicyDetailsSave()
        {
            bool blnRetValue = false;
            foreach (clsDTOSalaryPolicyDetailLeaveConsequence clsDTOLeaveConsequenceSalDet in DTOLeaveConsequencePolicy.DTOSalaryPolicyDetailLeaveConsequence)
            {
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@Mode", 7));
                this.sqlParameters.Add(new SqlParameter("@LeaveConsequencePolicyID", this.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID));
                this.sqlParameters.Add(new SqlParameter("@AdditionDeductionID", clsDTOLeaveConsequenceSalDet.AdditionDeductionID));
                if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, sqlParameters) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }

        #endregion SalaryPolicyDetailsSave

        #region DisplayLeaveConsequencePolicy
        public DataTable DisplayLeaveConsequencePolicy(int intRowNumber)
        {

            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@RowNumber", intRowNumber) };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion DisplayLeaveConsequencePolicy

        #region DisplayAddDedDetails
        public DataTable DisplayAddDedDetails(int iPolicyID, int iType)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 9),
                new SqlParameter("@LeaveConsequencePolicyID", iPolicyID)
        };
            return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }
        #endregion DisplayAddDedDetails

        #region DeleteLeaveConsequencePolicy
        public bool DeleteLeaveConsequencePolicy()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",4),
                                    new SqlParameter("@LeaveConsequencePolicyID", this.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        #endregion DeleteLeaveConsequencePolicy

        #region DeleteLeaveConsequencePolicyDetails
        public bool DeleteLeaveConsequencePolicyDetails()
        {
            bool blnRetValue = false;
            this.sqlParameters = new List<SqlParameter>{new SqlParameter("@Mode",6),
                                    new SqlParameter("@LeaveConsequencePolicyID", this.DTOLeaveConsequencePolicy.LeaveConsequencePolicyID)};
            if (this.DataLayer.ExecuteNonQuery(this.strProcedureName, this.sqlParameters) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }
        #endregion DeleteLeaveConsequencePolicyDetails

        #region GetAdditionDeductions
   
        /// <summary>
        /// Selecting the Addition Deduction refernce
        /// </summary>
        /// <returns></returns>
        public DataTable GetAdditionDeductions()
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8) };
             return DataLayer.ExecuteDataTable(this.strProcedureName, this.sqlParameters);
        }

        #endregion GetAdditionDeductions

        #region CheckDuplicate
        public bool CheckDuplicate(int iPolicyID, string strPolicyName)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 12),
                new SqlParameter("@LeaveConsequencePolicy", strPolicyName),
                new SqlParameter("@LeaveConsequencePolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);
        }
        #endregion CheckDuplicate

        #region PolicyIDExists

        public bool PolicyIDExists(int iPolicyID)
        {
            int intExists = 0;
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 13),
                new SqlParameter("@LeaveConsequencePolicyID", iPolicyID)
                 };
            intExists = this.DataLayer.ExecuteScalar(this.strProcedureName, this.sqlParameters).ToInt32();
            return (intExists > 0);

        }
        #endregion PolicyIDExists

        #region FillCombos

        /// <summary>
        /// 
        /// return datatable  for filling combos
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns></returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        #endregion FillCombos
    }
}
