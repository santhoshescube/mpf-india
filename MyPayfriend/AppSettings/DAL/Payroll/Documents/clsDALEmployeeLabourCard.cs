﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace MyPayfriend
{
    public class clsDALEmployeeLabourCard
    {
        public DataLayer objDataLayer { get; set; }
        public clsDTOEmployeeLabourCard objClsDTOEmployeeLabourCard { get; set; }
        private List<SqlParameter> lstSqlParameters;
        private static string strProcedureName = "spEmployeeLabourCard";

        #region Save/Update/Delete

        public Int32 SaveLabourCard()
        {
            lstSqlParameters = new List<SqlParameter>();

            if (objClsDTOEmployeeLabourCard.intLabourCardID == 0)
                lstSqlParameters.Add(new SqlParameter("@Mode", 1));
            else
                lstSqlParameters.Add(new SqlParameter("@Mode", 2));
            lstSqlParameters.Add(new SqlParameter("@LabourCardID", objClsDTOEmployeeLabourCard.intLabourCardID));
            lstSqlParameters.Add(new SqlParameter("@EmployeeID", objClsDTOEmployeeLabourCard.intEmployeeID));
            lstSqlParameters.Add(new SqlParameter("@CardNumber", objClsDTOEmployeeLabourCard.strCardNumber));
            lstSqlParameters.Add(new SqlParameter("@CardPersonalNumber", objClsDTOEmployeeLabourCard.strCardPersonalNumber));
            lstSqlParameters.Add(new SqlParameter("@IssueDate", objClsDTOEmployeeLabourCard.dtIssueDate));
            lstSqlParameters.Add(new SqlParameter("@ExpiryDate", objClsDTOEmployeeLabourCard.dtExpiryDate));
            lstSqlParameters.Add(new SqlParameter("@Remarks", objClsDTOEmployeeLabourCard.strRemarks));
            lstSqlParameters.Add(new SqlParameter("@IsRenewed", objClsDTOEmployeeLabourCard.blnIsRenewed));
            return objDataLayer.ExecuteScalar(strProcedureName, lstSqlParameters).ToInt32();
        }

        public int UpdateLabourCard()
        {
            lstSqlParameters = new List<SqlParameter>();
            lstSqlParameters.Add(new SqlParameter("@Mode", 9));
            lstSqlParameters.Add(new SqlParameter("@LabourCardID", objClsDTOEmployeeLabourCard.intLabourCardID));
            return objDataLayer.ExecuteScalar(strProcedureName, lstSqlParameters).ToInt32();

        }

        public bool DeleteLabourCard(int LabourCardID)
        {
            lstSqlParameters = new List<SqlParameter>();
            lstSqlParameters.Add(new SqlParameter("@Mode", 4));
            lstSqlParameters.Add(new SqlParameter("@LabourCardID", LabourCardID));
            return objDataLayer.ExecuteNonQuery(strProcedureName, lstSqlParameters).ToInt32() > 0;
        }

        #endregion Save/Update/Delete

        #region Retrieve Records

        public clsDTOEmployeeLabourCard GetLaburCardInfo(int intRecordNumber)
        {
            lstSqlParameters = new List<SqlParameter>();
            lstSqlParameters.Add(new SqlParameter("@Mode", 3));
            lstSqlParameters.Add(new SqlParameter("@RecordNumber", intRecordNumber));
            lstSqlParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            DataTable datLabourInfo = objDataLayer.ExecuteDataTable(strProcedureName, lstSqlParameters);
            if (datLabourInfo != null && datLabourInfo.Rows.Count > 0)
            {
                objClsDTOEmployeeLabourCard.intLabourCardID = datLabourInfo.Rows[0]["LabourCardID"].ToInt32();
                objClsDTOEmployeeLabourCard.intEmployeeID = datLabourInfo.Rows[0]["EmployeeID"].ToInt32();
                objClsDTOEmployeeLabourCard.strCardNumber = datLabourInfo.Rows[0]["CardNumber"].ToStringCustom();
                objClsDTOEmployeeLabourCard.strCardPersonalNumber = datLabourInfo.Rows[0]["CardPersonalNumber"].ToStringCustom();
                objClsDTOEmployeeLabourCard.dtIssueDate = datLabourInfo.Rows[0]["IssueDate"].ToDateTime();
                objClsDTOEmployeeLabourCard.dtExpiryDate = datLabourInfo.Rows[0]["ExpiryDate"].ToDateTime();
                objClsDTOEmployeeLabourCard.strRemarks = datLabourInfo.Rows[0]["Remarks"].ToStringCustom();

            }
            return objClsDTOEmployeeLabourCard;
        }

        public clsDTOEmployeeLabourCard DisplayEmployeeLabourCard(int intRecordNumber, long iEmployeeID, int iCardID, string strSearchKey)
        {
            lstSqlParameters = new List<SqlParameter>();
            lstSqlParameters.Add(new SqlParameter("@Mode", 5));
            lstSqlParameters.Add(new SqlParameter("@RowNum", intRecordNumber));
            lstSqlParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            if (iEmployeeID > 0)
                lstSqlParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
            if (iCardID > 0)
                lstSqlParameters.Add(new SqlParameter("@LabourCardId", iCardID));
            
            lstSqlParameters.Add(new SqlParameter("@SearchKey", strSearchKey));
            DataTable datLabourInfo = objDataLayer.ExecuteDataTable(strProcedureName, lstSqlParameters);
            if (datLabourInfo != null && datLabourInfo.Rows.Count > 0)
            {
                objClsDTOEmployeeLabourCard.intLabourCardID = datLabourInfo.Rows[0]["LabourCardID"].ToInt32();
                objClsDTOEmployeeLabourCard.intEmployeeID = datLabourInfo.Rows[0]["EmployeeID"].ToInt32();
                objClsDTOEmployeeLabourCard.strCardNumber = datLabourInfo.Rows[0]["CardNumber"].ToStringCustom();
                objClsDTOEmployeeLabourCard.strCardPersonalNumber = datLabourInfo.Rows[0]["CardPersonalNumber"].ToStringCustom();
                objClsDTOEmployeeLabourCard.dtIssueDate = datLabourInfo.Rows[0]["IssueDate"].ToDateTime();
                objClsDTOEmployeeLabourCard.dtExpiryDate = datLabourInfo.Rows[0]["ExpiryDate"].ToDateTime();
                objClsDTOEmployeeLabourCard.strRemarks = datLabourInfo.Rows[0]["Remarks"].ToStringCustom();
                objClsDTOEmployeeLabourCard.blnIsRenewed = datLabourInfo.Rows[0]["IsRenewed"].ToBoolean();


            }
            return objClsDTOEmployeeLabourCard;
        }

        public DataSet GetReportDetails(int iCardID)
        {
            lstSqlParameters = new List<SqlParameter>();
            lstSqlParameters.Add(new SqlParameter("@Mode", 8));
            lstSqlParameters.Add(new SqlParameter("@LabourCardID", iCardID));
            return objDataLayer.ExecuteDataSet(strProcedureName, lstSqlParameters);
        }

        #endregion Retrieve Records

        #region Get Count

        public int GetRecordCount(long iEmployeeID, int iCardID, string strSearchKey)
        {
            lstSqlParameters = new List<SqlParameter>();
            lstSqlParameters.Add(new SqlParameter("@Mode", 6));
            lstSqlParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            if (iEmployeeID > 0)
                lstSqlParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
            if (iCardID > 0)
                lstSqlParameters.Add(new SqlParameter("@LabourCardId", iCardID));
            
            lstSqlParameters.Add(new SqlParameter("@Searchkey", strSearchKey));
            int recCount = objDataLayer.ExecuteScalar(strProcedureName, lstSqlParameters).ToInt32();

            return recCount;
        }

        public int GetRowNumber()
        {
            try
            {
                int Rownum;
                lstSqlParameters = new List<SqlParameter>();
                SqlDataReader Reader;
                lstSqlParameters.Add(new SqlParameter("@Mode", 10));
                lstSqlParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
                lstSqlParameters.Add(new SqlParameter("@LabourCardID", objClsDTOEmployeeLabourCard.intLabourCardID));
                
                Reader = objDataLayer.ExecuteReader(strProcedureName, lstSqlParameters, CommandBehavior.CloseConnection);
                if (Reader.Read())
                {
                    Rownum = Reader["RowNum"].ToInt32();

                    Reader.Close();
                    return Rownum;
                }
                Reader.Close();
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        #endregion Get Count       

    }
}
