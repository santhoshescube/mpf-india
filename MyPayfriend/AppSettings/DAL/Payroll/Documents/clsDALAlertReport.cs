﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace MyPayfriend
{
     public class clsDALAlertReport
    {
         ArrayList prmAlertreport;
        DataLayer MobjDataLayer;
        public clsDALAlertReport(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }
        public DataTable GetAlerts(int intDocTypeID, int intUserID, string strFromDate, string strToDate, int intAlertType)
        {
            prmAlertreport = new ArrayList();
           
            prmAlertreport.Add(new SqlParameter("@DocumentTypeID", intDocTypeID));
            prmAlertreport.Add(new SqlParameter("@UserID", intUserID));
            prmAlertreport.Add(new SqlParameter("@FromDate", strFromDate));
            prmAlertreport.Add(new SqlParameter("@ToDate", strToDate));
            prmAlertreport.Add(new SqlParameter("@AlertType", intAlertType));

            return MobjDataLayer.ExecuteDataTable("GetAlertInfo", prmAlertreport);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                ArrayList prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", "0"));
                prmCommon.Add(new SqlParameter("@Fields", saFieldValues[0]));
                prmCommon.Add(new SqlParameter("@TableName", saFieldValues[1]));
                prmCommon.Add(new SqlParameter("@Condition", saFieldValues[2]));

                return MobjDataLayer.ExecuteDataTable("spCommonReferenceTransaction", prmCommon);
                // return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public DataTable FillCombos(string sQuery)
        {
            // function for getting datatable for filling combo

            return MobjDataLayer.ExecuteDataTable(sQuery);

        }
    }
}
