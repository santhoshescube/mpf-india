﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    public class clsDALTradeLicense : DataLayer
    {
        public clsDTOTradeLicense objclsDTOTradeLicense { get; set; }
        public DataLayer objConnection { get; set; }

        public string strProcName = "spPayTradeLicense";

        public int SaveTradeLicenseDetails(bool blnAddStatus)
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", (blnAddStatus == true ? 2 : 3)));
            parameters.Add(new SqlParameter("@TradeLicenseID", objclsDTOTradeLicense.TradeLicenseID));
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOTradeLicense.CompanyID));
            parameters.Add(new SqlParameter("@Sponsor", objclsDTOTradeLicense.Sponsor));
            parameters.Add(new SqlParameter("@SponserFee", objclsDTOTradeLicense.SponserFee));
            parameters.Add(new SqlParameter("@LicenceNumber", objclsDTOTradeLicense.LicenceNumber));
            parameters.Add(new SqlParameter("@City", objclsDTOTradeLicense.City));
            parameters.Add(new SqlParameter("@ProvinceID", objclsDTOTradeLicense.ProvinceID <= 0 ? null : objclsDTOTradeLicense.ProvinceID));
            parameters.Add(new SqlParameter("@Partner", objclsDTOTradeLicense.Partner));
            parameters.Add(new SqlParameter("@IssueDate", objclsDTOTradeLicense.IssueDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@ExpiryDate", objclsDTOTradeLicense.ExpiryDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@Remarks", objclsDTOTradeLicense.Remarks));
            parameters.Add(new SqlParameter("@IsRenewed ", objclsDTOTradeLicense.IsRenewed));
           
            return ExecuteScalar(strProcName, parameters).ToInt32();

        }
        public int GetRecordCount(int intCompanyID, int intTradeLicenseID, string strSearchKey)
        {
            int intRecordCount = 0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@TradeLicenseID", intTradeLicenseID));
            parameters.Add(new SqlParameter("@SearchKey", strSearchKey));

            SqlDataReader DrLicense = ExecuteReader(strProcName, parameters, CommandBehavior.CloseConnection);
            if (DrLicense.Read())
            {
                intRecordCount = DrLicense["RecordCount"].ToInt32();
            }
            DrLicense.Close();
            return intRecordCount;
        }
        public int UpdateTradeLicenseForRenewal()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "16"));
            parameters.Add(new SqlParameter("@TradeLicenseID", objclsDTOTradeLicense.TradeLicenseID));
            return Convert.ToInt32(ExecuteScalar(strProcName, parameters));
        }

        public bool CheckDuplication()
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@TradeLicenseID", objclsDTOTradeLicense.TradeLicenseID ));
            parameters.Add(new SqlParameter("@LicenceNumber", objclsDTOTradeLicense.LicenceNumber));

            return (ExecuteScalar(strProcName, parameters).ToInt32() > 0) ? true : false;
          
        }    
        public bool DeleteTradeLicense()
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@TradeLicenseID", objclsDTOTradeLicense.TradeLicenseID));

            return (ExecuteNonQuery(strProcName, parameters) != 0) ? true :false ;      
        }

        public DataTable GetTradeLicenseDetails(int MintRowNumber, int intCompanyID, int intLicenseID, string SearchKey)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@RowNumber", MintRowNumber));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID ));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@TradeLicenseID", intLicenseID ));
            parameters.Add(new SqlParameter("@SearchKey", SearchKey));

           return ExecuteDataTable(strProcName, parameters);  
        }
        public DataSet DisplayTradeLicenseReport(int intRecId)  //License Report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@TradeLicenseID", intRecId));
            return ExecuteDataSet(strProcName, parameters);
        }
      
        public static DataTable GetAutoCompleteList(string SearchKey, int CompanyID, string FieldName, string TableName)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "17"));
            parameters.Add(new SqlParameter("@SearchKey", ""));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@TableName", TableName));
            parameters.Add(new SqlParameter("@FieldName", FieldName));
            return new DataLayer().ExecuteDataTable("spPayTradeLicense", parameters);
        }

    }


}
