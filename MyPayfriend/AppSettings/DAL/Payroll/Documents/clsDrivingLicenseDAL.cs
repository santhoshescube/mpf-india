﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDrivingLicenseDAL:IDisposable
    {
        public DataLayer DataLayer { get; set; }
        public clsDrivingLicense DrivingLicense { get; set; }
        private IList<SqlParameter> lstSqlParams;

        #region Constructor
        public clsDrivingLicenseDAL(DataLayer objDataLayer)
        {
            this.DataLayer = objDataLayer;


        }
        #endregion

        #region GetReferenceTableData
        public DataSet GetReferenceTableData()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "FC"));
            lstSqlParams.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            lstSqlParams.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return this.DataLayer.ExecuteDataSet("spDrivingLicense", lstSqlParams);        }

        #endregion

        #region Insert
        /// <summary>
        /// Insert new drivinglicense.
        /// </summary>
        /// <returns> returns LicenseId if drivinglicense is inserted successfully </returns>
        public Int32 InsertDrivingLicense(bool blnMode)
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", blnMode ? "I": "U"));
            lstSqlParams.Add(new SqlParameter("@LicenseID", DrivingLicense.LicenseID));
            lstSqlParams.Add(new SqlParameter("@EmployeeID", DrivingLicense.EmployeeID));
            lstSqlParams.Add(new SqlParameter("@LicenceNumber", DrivingLicense.LicenceNumber));
            lstSqlParams.Add(new SqlParameter("@LicenceHoldersName", DrivingLicense.LicenceHoldersName));
            lstSqlParams.Add(new SqlParameter("@LicenceTypeID", DrivingLicense.LicenceTypeID));
            lstSqlParams.Add(new SqlParameter("@LicencedCountryID", DrivingLicense.LicencedCountryID));
            lstSqlParams.Add(new SqlParameter("@IssueDate", DrivingLicense.IssueDate));
            lstSqlParams.Add(new SqlParameter("@ExpiryDate", DrivingLicense.ExpiryDate));
            lstSqlParams.Add(new SqlParameter("@Remarks", DrivingLicense.Remarks));

            object objLicenseID = this.DataLayer.ExecuteScalar("spDrivingLicense", lstSqlParams);

            // Returns true if returned identity value is greater than one else false
            //return Convert.ToInt32((objLicenseID == null || objLicenseID == DBNull.Value) ? "-1" : objLicenseID) > 0;
            return objLicenseID.ToInt32();
        }
        #endregion

       

        #region Delete

        /// <summary>
        /// Deletes a Driving License
        /// </summary>
        /// <returns> returns true if Driving License is deleted successfully else returns false. </returns>
        public bool DeleteDrivingLicense()
        {

            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "D"));
            lstSqlParams.Add(new SqlParameter("@LicenseID", DrivingLicense.LicenseID));
            // Returns true if Rows Affected is greater than one else false
            return Convert.ToInt32(this.DataLayer.ExecuteScalar("spDrivingLicense", lstSqlParams)) > 0? true :false ;

        }

        #endregion
        public int GetRecordCount(long intEmployeeID, int intIcardID, string strSearchKey)
        {
            try
            {
                lstSqlParams = new List<SqlParameter>();
                lstSqlParams.Add(new SqlParameter("@Mode", "S"));
                lstSqlParams.Add(new SqlParameter("@RowNum", 1));
                lstSqlParams.Add(new SqlParameter("@SearchKey", strSearchKey));
                lstSqlParams.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
                lstSqlParams.Add(new SqlParameter("@LicenseID", intIcardID));
                lstSqlParams.Add(new SqlParameter("@EmployeeID", intEmployeeID));
                DataSet ds = this.DataLayer.ExecuteDataSet("spDrivingLicense", lstSqlParams);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return (ds.Tables[0].Rows[0][0] == DBNull.Value ? "0" : ds.Tables[0].Rows[0][0]).ToInt32();
                    }
                    else
                        return 0;

                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
            
        }

        #region GetLicenseByRowIndex
        public clsDrivingLicense GetLicenseByRowIndex(int RowNum,string strSearchKey)
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "S"));
            lstSqlParams.Add(new SqlParameter("@RowNum", RowNum));
            lstSqlParams.Add(new SqlParameter("@SearchKey", strSearchKey));
            lstSqlParams.Add(new SqlParameter("@LicenseID", DrivingLicense.LicenseID));
            lstSqlParams.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            lstSqlParams.Add(new SqlParameter("@EmployeeID", DrivingLicense.EmployeeID));
            using (DataSet ds = this.DataLayer.ExecuteDataSet("spDrivingLicense", lstSqlParams))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        this.DrivingLicense = new clsDrivingLicense();
                        this.DrivingLicense.Pager = new clsPager();
                        this.DrivingLicense.Pager.TotalRecords = Convert.ToInt32(ds.Tables[0].Rows[0][0] == DBNull.Value ? "0" : ds.Tables[0].Rows[0][0]);

                        if (this.DrivingLicense.Pager.TotalRecords == 0)
                            return null;

                        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                        {
                            DataRow Row = ds.Tables[1].Rows[0];
                            this.MapLicense(Row);
                        }

                        return this.DrivingLicense;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
        }
        #endregion

        #region GetLicenseByLicenseID
        public clsDrivingLicense GetLicenseByLicenseID(int IntLicenseID)
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "LDID"));
            lstSqlParams.Add(new SqlParameter("@LicenseID", IntLicenseID));
            using (DataSet ds = this.DataLayer.ExecuteDataSet("spDrivingLicense", lstSqlParams))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        this.DrivingLicense = new clsDrivingLicense();
                        this.DrivingLicense.Pager = new clsPager();
                        this.DrivingLicense.Pager.TotalRecords = Convert.ToInt32(ds.Tables[0].Rows[0][0] == DBNull.Value ? "0" : ds.Tables[0].Rows[0][0]);

                        if (this.DrivingLicense.Pager.TotalRecords == 0)
                            return null;

                        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                        {
                            DataRow Row = ds.Tables[1].Rows[0];
                            this.MapLicense(Row);
                        }

                        return this.DrivingLicense;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
        }
        #endregion

        #region MapLicense
        private void MapLicense(DataRow Row)
        {
            this.DrivingLicense.LicenseID = Row["LicenseID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["LicenseID"]);
            this.DrivingLicense.EmployeeID = Row["EmployeeID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["EmployeeID"]);
            this.DrivingLicense.ExpiryDate = Row["ExpiryDate"] == DBNull.Value ? ClsCommonSettings.NullDate : Convert.ToDateTime(Row["ExpiryDate"]);
            this.DrivingLicense.IssueDate = Row["IssueDate"] == DBNull.Value ? ClsCommonSettings.NullDate : Convert.ToDateTime(Row["IssueDate"]);
            this.DrivingLicense.LicencedCountryID = Row["LicencedCountryID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["LicencedCountryID"]);
            this.DrivingLicense.LicenceHoldersName = Row["LicenceHoldersName"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["LicenceHoldersName"]);
            this.DrivingLicense.LicenceNumber = Row["LicenceNumber"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["LicenceNumber"]);
            this.DrivingLicense.LicenceTypeID = Row["LicenceTypeID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["LicenceTypeID"]);
            this.DrivingLicense.IsRenew = Row["IsRenewed"] == DBNull.Value ? false: Convert.ToBoolean(Row["IsRenewed"]);
            this.DrivingLicense.Remarks = Row["Remarks"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["Remarks"]);
        }
        #endregion

        public DataSet GetEmployeeLicenseDetails()
        {
            lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "EDR"));
            lstSqlParams.Add(new SqlParameter("@LicenseID", DrivingLicense.LicenseID));

            return this.DataLayer.ExecuteDataSet("spDocumentReport", lstSqlParams);

        }
        public int GetEmployeeID(int IntLicenseID)
        {
            lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "EMPNO"));
            lstSqlParams.Add(new SqlParameter("@LicenseID", IntLicenseID));
            return DataLayer.ExecuteScalar("spDrivingLicense", lstSqlParams).ToInt32();

        }

        public int UpdateLicenseForRenewal()
        {
            lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "UlR"));
            lstSqlParams.Add(new SqlParameter("@LicenseID", DrivingLicense.LicenseID));

            return Convert.ToInt32(DataLayer. ExecuteScalar("spDrivingLicense", lstSqlParams));

        }
        public DataTable GetRefernceTable(ReferenceTables refTable)
        {
            IList<SqlParameter> lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "SRT"));

            switch (refTable)
            {
                case ReferenceTables.Manufacturer:
                    lstSqlParams.Add(new SqlParameter("@Manufacturer", true));
                    break;

                case ReferenceTables.LicenseType:
                    lstSqlParams.Add(new SqlParameter("@LicenseType", true));
                    break;

                case ReferenceTables.VehicleCategory:
                    lstSqlParams.Add(new SqlParameter("@VehicleCategory", true));
                    break;

                case ReferenceTables.VehicleModel:
                    lstSqlParams.Add(new SqlParameter("@Model", true));
                    break;

                case ReferenceTables.VehicleLocation:
                    lstSqlParams.Add(new SqlParameter("@VehicleLocation", true));
                    break;

                case ReferenceTables.Country:
                    lstSqlParams.Add(new SqlParameter("@Country", true));
                    break;

                case ReferenceTables.Nationality:
                    lstSqlParams.Add(new SqlParameter("@Nationality", true));
                    break;

                case ReferenceTables.Religion:
                    lstSqlParams.Add(new SqlParameter("@Religion", true));
                    break;






                case ReferenceTables.Language:
                    lstSqlParams.Add(new SqlParameter("@Language", true));
                    break;

                case ReferenceTables.InsuranceCompany:
                    lstSqlParams.Add(new SqlParameter("@InsuranceCompany", true));
                    break;








                default:
                    throw new ArgumentException("Invalid ReferenceTable");
            }

            return DataLayer.ExecuteDataTable("spDrivingLicense", lstSqlParams);
        }
        #region FillCombos
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }
        #endregion

         #region Dispose Members

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!this.disposed)
                {
                    this.DrivingLicense.Dispose();
                    this.DataLayer = null;
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~clsDrivingLicenseDAL()
        {
            Dispose(false);
        }
        #endregion









    }
}
