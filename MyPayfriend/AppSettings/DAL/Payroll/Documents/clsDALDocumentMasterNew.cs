﻿
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using MyPayfriend;
    /*=================================================
  Description :	     <Document Transactions>
  Modified By :		 <Laxmi>
  Modified Date :      <13 Apr 2012>
  ================================================*/


public class clsDALDocumentMasterNew : DataLayer
{
    public clsDTODocumentMaster objClsDTODocumentMaster { get; set; }
    //ArrayList prmDocuments;


    public static DataTable GetAllOperationTypes()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 1));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }

    public static DataTable GetAllReferenceNumberByOperationTypeID(int OperationTypeID, bool InserviceOnly)
    {
        ArrayList prmDocuments = new ArrayList();
        if (ClsCommonSettings.IsArabicView)
        {
            prmDocuments.Add(new SqlParameter("@Mode", 51));
        }
        else
        {
            prmDocuments.Add(new SqlParameter("@Mode", 2));
        }
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@InserviceOnly", InserviceOnly));
        prmDocuments.Add(new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }


    public static DataTable GetAllDocumentTypes()
    {
        ArrayList prmDocuments = new ArrayList();
        if (ClsCommonSettings.IsArabicView)
        {
            prmDocuments.Add(new SqlParameter("@Mode", 47));
        }
        else
        {
            prmDocuments.Add(new SqlParameter("@Mode", 3));
        }
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }


    public static DataTable GetAllBin()
    {
        ArrayList prmDocuments = new ArrayList();
        if (ClsCommonSettings.IsArabicView)
        {
            prmDocuments.Add(new SqlParameter("@Mode", 52));
        }
        else
        {
            prmDocuments.Add(new SqlParameter("@Mode", 4));
        }
        prmDocuments.Add(new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }


    public static DataTable GetAllDocumentReceipts()
    {
        ArrayList prmDocuments = new ArrayList();
        if (ClsCommonSettings.IsArabicView)
        {
            prmDocuments.Add(new SqlParameter("@Mode", 49));
        }
        else
        {
            prmDocuments.Add(new SqlParameter("@Mode", 5));
        }
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }

    public static DataTable GetAllDocumentIssues()
    {
        ArrayList prmDocuments = new ArrayList();
        if (ClsCommonSettings.IsArabicView)
        {
            prmDocuments.Add(new SqlParameter("@Mode", 50));
        }
        else
        {
            prmDocuments.Add(new SqlParameter("@Mode", 6));
        }
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }

    public static DataTable GetAllCustodian(bool InServiceOnly)
    {
        ArrayList prmDocuments = new ArrayList();        
        if (ClsCommonSettings.IsArabicView)
        {
            prmDocuments.Add(new SqlParameter("@Mode", 48));
        }
        else
        {
            prmDocuments.Add(new SqlParameter("@Mode", 7));
        }
        prmDocuments.Add(new SqlParameter("@InServiceOnly", InServiceOnly));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);
    }

    public static int GetTotalRecordsCount()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 8));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        prmDocuments.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();
    }

    /// <summary>
    /// Save document
    /// </summary>
    /// <returns></returns>
    public int SaveDocument()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 9));
        prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.DocumentID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", objClsDTODocumentMaster.DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", objClsDTODocumentMaster.OperationTypeID));
        prmDocuments.Add(new SqlParameter("@ReferenceID", objClsDTODocumentMaster.ReferenceID));
        prmDocuments.Add(new SqlParameter("@ReferenceNumber", objClsDTODocumentMaster.ReferenceNumber));
        prmDocuments.Add(new SqlParameter("@Remarks", objClsDTODocumentMaster.Remarks));
        prmDocuments.Add(new SqlParameter("@DocumentNumber", objClsDTODocumentMaster.DocumentNumber));
        prmDocuments.Add(new SqlParameter("@IssuedDate", objClsDTODocumentMaster.IssuedDate));
        prmDocuments.Add(new SqlParameter("@ExpiryDate", objClsDTODocumentMaster.ExpiryDate));
        prmDocuments.Add(new SqlParameter("@IsRenewed", objClsDTODocumentMaster.IsRenew));
        prmDocuments.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
        prmDocuments.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return ExecuteScalar("spDocuments", prmDocuments).ToInt32();

    }


    public int SaveDocumentReceiptIssue()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 15));
        prmDocuments.Add(new SqlParameter("@OrderNo", objClsDTODocumentMaster.OrderNo));
        prmDocuments.Add(new SqlParameter("@DocumentID", objClsDTODocumentMaster.DocumentID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", objClsDTODocumentMaster.DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", objClsDTODocumentMaster.OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentNumber", objClsDTODocumentMaster.DocumentNumber));
        prmDocuments.Add(new SqlParameter("@ReceiptIssueDate", objClsDTODocumentMaster.ReceiptIssueDate));
        prmDocuments.Add(new SqlParameter("@Custodian", objClsDTODocumentMaster.Custodian));
        prmDocuments.Add(new SqlParameter("@ReceivedFrom", objClsDTODocumentMaster.ReceivedFrom));
        prmDocuments.Add(new SqlParameter("@BinID", objClsDTODocumentMaster.BinID));
        prmDocuments.Add(new SqlParameter("@ExpiryDate", objClsDTODocumentMaster.ExpiryDate));

        if (!objClsDTODocumentMaster.IsReceipted)
        {

            if (objClsDTODocumentMaster.ExpectedReturnDate != null)
                prmDocuments.Add(new SqlParameter("@ExpectedReturnDate", objClsDTODocumentMaster.ExpectedReturnDate));
            prmDocuments.Add(new SqlParameter("@ReceiptIssueReasonID", objClsDTODocumentMaster.ReceiptIssueReasonID));
        }
        else
        {
            prmDocuments.Add(new SqlParameter("@DocumentStatusID", objClsDTODocumentMaster.DocumentStatusID));
        }
        prmDocuments.Add(new SqlParameter("@Remarks", objClsDTODocumentMaster.Remarks));
        prmDocuments.Add(new SqlParameter("@IsReceipted", objClsDTODocumentMaster.IsReceipted));
        prmDocuments.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
        prmDocuments.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return ExecuteScalar("spDocuments", prmDocuments).ToInt32();

    }

    /// <summary>
    /// Retrieves Document Details
    /// </summary>
    /// <param name="CurrentIndex"></param>
    /// <param name="DocumentTypeID"></param>
    /// <param name="DocumentID"></param>
    /// <param name="OperationTypeID"></param>
    /// <param name="ReferenceID"></param>
    /// <param name="PintCompanyID"></param>
    /// <param name="SearchKey"></param>
    /// <returns></returns>

    public static DataTable GetDocumentDetails(int CurrentIndex, int DocumentTypeID, int DocumentID, int OperationTypeID, long ReferenceID, int PintCompanyID, string SearchKey,long PEmpID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 10));
        prmDocuments.Add(new SqlParameter("@RowIndex", CurrentIndex));
      

        if (ReferenceID > 0)
            prmDocuments.Add(new SqlParameter("@ReferenceID", ReferenceID));
        else if (PintCompanyID > 0)
            prmDocuments.Add(new SqlParameter("@ReferenceID", PintCompanyID));
        else
            prmDocuments.Add(new SqlParameter("@ReferenceID", PintCompanyID));
        if (PintCompanyID > 0)
        {
            prmDocuments.Add(new SqlParameter("@CompanyID", PintCompanyID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        }
        else if (PEmpID >0)
        {
            prmDocuments.Add(new SqlParameter("@EmpID", PEmpID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        }
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        prmDocuments.Add(new SqlParameter("@SearchKey", SearchKey));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }

    ///// <summary>
    ///// Validation of Document Number
    ///// </summary>
    ///// <param name="DocumentID"></param>
    ///// <param name="DocumentNumber"></param>
    ///// <returns></returns>
    //public static bool IsExistsDocumentNumber(int DocumentID, string DocumentNumber, int operationTypeID)
    //{
    //    ArrayList prmDocuments = new ArrayList();
    //    prmDocuments.Add(new SqlParameter("@Mode", 11));
    //    prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID ));
    //    prmDocuments.Add(new SqlParameter("@DocumentNumber", DocumentNumber ));
    //    prmDocuments.Add(new SqlParameter("@OperationTypeID", operationTypeID));
    //    return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32() > 0;

    //}

    public static DateTime GetCurrentDate()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 12));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToDateTime();

    }

    public static bool IsReceipted(int OperationTypeID, int DocumentTypeID, int DocumentId)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 16));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentId));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32() > 0;

    }


    public static int GetBinID(int OperationTypeID, int DocumentTypeID, int DocumentId)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 17));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentId));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

    }


    public static int GetEmployeeID(int OperationTypeID, int DocumentTypeID, int DocumentId)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 18));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentId));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

    }


    public static DataTable GetStockRegisterDocuments(int StatusID, int PageIndex, int PageSize, int OperationTypeID, int DocumentTypeID, string DocumentNumber)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 19));
        prmDocuments.Add(new SqlParameter("@StatusID", StatusID));
        prmDocuments.Add(new SqlParameter("@PageIndex", PageIndex));
        prmDocuments.Add(new SqlParameter("@PageSize", PageSize));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentNumber", DocumentNumber));
        prmDocuments.Add(new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView));
        prmDocuments.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);
    }

    public static int GetStockRegisterTotalRecords(int StatusID, int OperationTypeID, int DocumentTypeID, string DocumentNumber)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 20));
        prmDocuments.Add(new SqlParameter("@StatusID", StatusID));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentNumber", DocumentNumber.Trim()));
        prmDocuments.Add(new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView));
        prmDocuments.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();
    }
    
    public static DataTable GetAllTransactionsByDocumentID(int OperationTypeID, int DocumentTypeID, int DocumentID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 21));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        prmDocuments.Add(new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }


    public static DataTable GetDocumentDetailsByOrderNo(int OperationTypeID, int DocumentTypeID, int DocumentID, int OrderNo)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 22));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        prmDocuments.Add(new SqlParameter("@OrderNo", OrderNo));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }


    /// <summary>
    /// Delete Documents
    /// </summary>
    /// <param name="OperationTypeID"></param>
    /// <param name="DocumentTypeID"></param>
    /// <param name="DocumentID"></param>
    /// <returns></returns>
    public static bool DelteOtherDocument(int OperationTypeID, int DocumentTypeID, int DocumentID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 25));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32() > 0;

    }


    public static bool DeleteDocumentReceiptIssue(int OperationTypeID, int DocumentTypeID, int DocumentID, int OrderNo)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 26));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        prmDocuments.Add(new SqlParameter("@OrderNo", OrderNo));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32() > 0;

    }


    public static DataTable GetAllOtherDocumentTypes()
    {
        ArrayList prmDocuments = new ArrayList();
        if (ClsCommonSettings.IsArabicView)
        {
            prmDocuments.Add(new SqlParameter("@Mode", 46));
            prmDocuments.Add(new SqlParameter("@Isarabic", 1));
        }
        else
        {
            prmDocuments.Add(new SqlParameter("@Isarabic", 0));
            prmDocuments.Add(new SqlParameter("@Mode", 27));
        }
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }

    public static int GetDocumentCurrentIndex(int DocumentID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 28));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        prmDocuments.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

    }

    public static DataTable GetAllDocumentNumber(int OperationTypeID, int DocumentTypeID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 29));
        prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

    }

    /// <summary>
    /// Gets Employee/Company based Documents depending on Condition
    /// </summary>
    /// <param name="EmployeeID"></param>
    /// <param name="PintCompanyID"></param>
    /// <returns></returns>
    public static Int32 GetFirstDocumentIDByEmployee(long EmployeeID, int PintCompanyID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 40));
        if (PintCompanyID > 0)
            prmDocuments.Add(new SqlParameter("@ReferenceID", PintCompanyID));
        else if (EmployeeID > 0)
            prmDocuments.Add(new SqlParameter("@ReferenceID", EmployeeID));
        else
            prmDocuments.Add(new SqlParameter("@ReferenceID", PintCompanyID));

        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

    }

    /// <summary>
    /// Gets Total Record Count
    /// </summary>
    /// <param name="EmployeeID"></param>
    /// <param name="DocumentID"></param>
    /// <param name="SearchKey"></param>
    /// <returns></returns>
    public static int GetTotalRecordCount(long EmployeeID, int DocumentID, int PintCompanyID, string SearchKey, int OperationTypeID, long PEmpID, int DocumentTypeID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 41));
        if (EmployeeID > 0)
            prmDocuments.Add(new SqlParameter("@ReferenceID", EmployeeID));
        else if (PintCompanyID > 0)
            prmDocuments.Add(new SqlParameter("@ReferenceID", PintCompanyID));
        else
            prmDocuments.Add(new SqlParameter("@ReferenceID", PintCompanyID));

        if (PintCompanyID > 0)
        {
            prmDocuments.Add(new SqlParameter("@CompanyID", PintCompanyID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        }
        else if (PEmpID > 0)
        {
            prmDocuments.Add(new SqlParameter("@EmpID", PEmpID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
        }
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        prmDocuments.Add(new SqlParameter("@SearchKey", SearchKey));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

    }

    /// <summary>
    /// Update IsRenewed in DocumentsMaster
    /// </summary>
    /// <param name="DocumentID"></param>
    /// <param name="DocumentTypeID"></param>
    /// <returns></returns>
    public static int UpdateDocumentMaster(int DocumentID, int DocumentTypeID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 42));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();
    }

    /// <summary>
    /// Get report for email
    /// </summary>
    /// <param name="DocumentID"></param>
    /// <returns></returns>
    public DataSet DocumentMasterEmail(int DocumentID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 38));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        return new DataLayer().ExecuteDataSet("spDocuments", prmDocuments);
    }

    /// <summary>
    /// Gets Entire Search list
    /// </summary>
    /// <param name="SearchKey"></param>
    /// <param name="EmployeeID"></param>
    /// <param name="FieldName"></param>
    /// <param name="TableName"></param>
    /// <returns></returns>
    public static DataTable GetAutoCompleteList(string SearchKey, int ReferenceID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", "43"));
        prmDocuments.Add(new SqlParameter("@SearchKey", SearchKey));
        prmDocuments.Add(new SqlParameter("@ReferenceID", ReferenceID));
        prmDocuments.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
        prmDocuments.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
        return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);
    }

    public static int GetDocRequestReferenceExists(int DocumentID, int DocumentTypeID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", "44"));
        prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();
    }
}




