﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    public class clsDALInsuranceDetails: DataLayer
    {

        public clsDTOInsuranceDetails objclsDTOInsuranceDetails { get; set; }
        public clsDTOHealthCard PobjDTOHealthCard { get; set; }
          
        public int SaveInsuranceDetails(bool blnAddStatus)
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", (blnAddStatus == true? 2: 3)));
            parameters.Add(new SqlParameter("@InsuranceId", objclsDTOInsuranceDetails.InsuranceID));
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOInsuranceDetails.CompanyID));
            parameters.Add(new SqlParameter("@CompanyAssetID", objclsDTOInsuranceDetails.CompanyAssetID));
            parameters.Add(new SqlParameter("@PolicyNumber", objclsDTOInsuranceDetails.PolicyNumber));
            parameters.Add(new SqlParameter("@PolicyName", objclsDTOInsuranceDetails.PolicyName));
            parameters.Add(new SqlParameter("@InsuranceCompanyID", objclsDTOInsuranceDetails.InsuranceCompanyID));
            parameters.Add(new SqlParameter("@PolicyAmount", objclsDTOInsuranceDetails.PolicyAmount));
            parameters.Add(new SqlParameter("@IssueDate", objclsDTOInsuranceDetails.IssueDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@ExpiryDate", objclsDTOInsuranceDetails.ExpiryDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@Remarks", objclsDTOInsuranceDetails.Remarks));
            parameters.Add(new SqlParameter("@IsRenewed ", objclsDTOInsuranceDetails.IsRenewed));

            return ExecuteScalar("spPayInsuranceDetails",parameters).ToInt32();
           
        }

        public bool CheckDuplication(int intIcardID,string strPolicynumber)
        {
            bool blnExists = false;
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@InsuranceID", intIcardID));
            parameters.Add(new SqlParameter("@PolicyNumber", strPolicynumber));
            parameters.Add(new SqlParameter("@Mode", 6));

            SqlDataReader DrInsuranceCard = ExecuteReader("spPayInsuranceDetails", parameters, CommandBehavior.CloseConnection);
            if (DrInsuranceCard.Read())
            {
                blnExists = (DrInsuranceCard["RecordCount"].ToInt32() > 0? true: false);
            }
            DrInsuranceCard.Close();
            return blnExists;
        }
        public int GetRecordCount(int intCompanyID,int AssetID,int intIcardID,string strSearchKey)
        {
            int intRecordCount=0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@CompanyAssetID", AssetID));
            parameters.Add(new SqlParameter("@InsuranceID", intIcardID));
            parameters.Add(new SqlParameter("@SearchKey", strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            SqlDataReader DrInsuranceCard = ExecuteReader("spPayInsuranceDetails", parameters, CommandBehavior.CloseConnection);
            if (DrInsuranceCard.Read())
            {
                intRecordCount = DrInsuranceCard["RecordCount"].ToInt32();
            }
            DrInsuranceCard.Close();
            return intRecordCount;
        }
        public bool DeleteInsuranceDetails()
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@InsuranceID", objclsDTOInsuranceDetails.InsuranceID));
            parameters.Add(new SqlParameter("@Mode", 4));

            if(ExecuteNonQuery("spPayInsuranceDetails",parameters)!=0)
                return true;
            return false;
        }
        public DataTable GetInsuranceDetails(int MintRowNumber, int intCmpID,int AssetID, int intIcardID,string SearchKey)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@RowNumber", MintRowNumber));
            parameters.Add(new SqlParameter("@CompanyID", intCmpID));
            parameters.Add(new SqlParameter("@CompanyAssetID", AssetID));
            parameters.Add(new SqlParameter("@InsuranceID",intIcardID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@SearchKey", SearchKey));

            DataTable DtInsuranceCard =ExecuteDataTable("spPayInsuranceDetails", parameters);

            return DtInsuranceCard;
        }
        public DataSet DisplayInsuranceReport(int intRecId)  //Insurance Report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@InsuranceID", intRecId));
            return ExecuteDataSet("spPayInsuranceDetails", parameters);
        }
        public int UpdateInsuranceForRenewal()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "21"));
            parameters.Add(new SqlParameter("@InsuranceID", objclsDTOInsuranceDetails.InsuranceID));
            return Convert.ToInt32(ExecuteScalar ("spPayInsuranceDetails", parameters));

        }
        public static DataTable GetAutoCompleteList(string SearchKey, int CompanyID, string FieldName, string TableName,int AssetID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "22"));
            parameters.Add(new SqlParameter("@SearchKey", SearchKey));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@TableName", TableName));
            parameters.Add(new SqlParameter("@FieldName", FieldName));
            parameters.Add(new SqlParameter("@CompanyAssetID", AssetID));
            return new DataLayer().ExecuteDataTable("spPayInsuranceDetails", parameters);
        }

    }
}
