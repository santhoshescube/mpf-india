﻿using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Collections.Generic;

namespace MyPayfriend
{
    public class clsDALEmiratesCard
    {
        DataLayer objDatalayer = new DataLayer();
        clsConnection objConnection = new clsConnection();

        public DataLayer objclsConnection { get; set; }
        public clsDTOEmiratesCard objDTOEmiratesCard { get; set; }


        public bool DeleteEmiratesCardInfo()
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@CardID", objDTOEmiratesCard.EmiratesID));

            if (objConnection.ExecutesQuery(parameters, "MvfEmployeeEmiratesCardTransactions"))
                return true;
            else
                return false;
        }

        public int GetRowNumber()
        {
            try
            {
                int Rownum;
                ArrayList parameters = new ArrayList();
                SqlDataReader Reader;
                parameters.Add(new SqlParameter("@Mode", 10));
                parameters.Add(new SqlParameter("@EmiratesID", objDTOEmiratesCard.EmiratesID));
                
                Reader = objDatalayer.ExecuteReader("MvfEmployeeEmiratesCardTransactions", parameters, CommandBehavior.CloseConnection);
                if (Reader.Read())
                {
                    Rownum = Reader["RowNumber"].ToInt32();
            
                    Reader.Close();
                    return Rownum;
                }
                Reader.Close();
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int DisplayRecordCount(string strSearchKey)
        {
            
            ArrayList parameters = new ArrayList();
            SqlDataReader Reader;
            int intRecordCount = 0;
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@EmployeeID", objDTOEmiratesCard.EmployeeID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@EmiratesID", objDTOEmiratesCard.EmiratesID));
            parameters.Add(new SqlParameter("@SearchKey", strSearchKey));
            Reader = objDatalayer.ExecuteReader("MvfEmployeeEmiratesCardTransactions", parameters, CommandBehavior.CloseConnection);
            if (Reader.Read())
            {
                intRecordCount = Reader["Count"].ToInt32();
                Reader.Close();
                objDTOEmiratesCard.iRecordCount = intRecordCount;
            }
            Reader.Close();
            return intRecordCount;
        }

        public bool DisplayEmiratesCardInfo(int iRecordID, long empId, int PiEmCardID, string strSearchKey)
        {
            ArrayList parameters = new ArrayList();
            ArrayList rParameters = new ArrayList();
            DataTable DtGetData = new DataTable();
            parameters.Add(new SqlParameter("@Mode", 3));
            parameters.Add(new SqlParameter("@EmployeeID", empId));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@EmiratesID", PiEmCardID));
            parameters.Add(new SqlParameter("@SearchKey", strSearchKey));
            parameters.Add(new SqlParameter("@RowNumber", iRecordID));
            DtGetData = objConnection.FillDataSet("MvfEmployeeEmiratesCardTransactions", parameters,out rParameters).Tables[0];
            if (DtGetData.Rows.Count > 0)
            {
                objDTOEmiratesCard.EmiratesID = (DtGetData.Rows[0]["EmiratesID"] == DBNull.Value) ? 0 : Convert.ToInt32(DtGetData.Rows[0]["EmiratesID"]);
                objDTOEmiratesCard.EmployeeID = (DtGetData.Rows[0]["EmployeeID"] == DBNull.Value) ? 0 : Convert.ToInt32(DtGetData.Rows[0]["EmployeeID"]);
                objDTOEmiratesCard.CardNumber = (DtGetData.Rows[0]["CardNumber"] == DBNull.Value) ? null : Convert.ToString(DtGetData.Rows[0]["CardNumber"]);
                objDTOEmiratesCard.CardPersonalIDNumber = (DtGetData.Rows[0]["IDNumber"] == DBNull.Value) ? null : Convert.ToString(DtGetData.Rows[0]["IDNumber"]);
                objDTOEmiratesCard.IssueDate = (DtGetData.Rows[0]["IssueDate"] == DBNull.Value) ? System.DateTime.Now.ToDateTime() : Convert.ToDateTime(DtGetData.Rows[0]["IssueDate"]);
                objDTOEmiratesCard.ExpiryDate = (DtGetData.Rows[0]["ExpiryDate"] == DBNull.Value) ? System.DateTime.Now.ToDateTime() : Convert.ToDateTime(DtGetData.Rows[0]["ExpiryDate"]);
                objDTOEmiratesCard.Remarks = (DtGetData.Rows[0]["Remarks"] == DBNull.Value) ? string.Empty : Convert.ToString(DtGetData.Rows[0]["Remarks"]);
                objDTOEmiratesCard.IsRenewed = (DtGetData.Rows[0]["IsRenewed"]== DBNull.Value) ? false : Convert.ToBoolean(DtGetData.Rows[0]["IsRenewed"]);
                return true;
            }
            return false;
        }
        public bool SaveEmiratesCardInfo(bool bAddStatus)
        {
            ArrayList parameters = new ArrayList();
            int ID = 0;
            if (bAddStatus)
            {
                
                parameters.Add(new SqlParameter("@Mode", 1)); //Insert
            }
            else
            {
                //Update
                parameters.Add(new SqlParameter("@Mode", 2));
                parameters.Add(new SqlParameter("@EmiratesID",objDTOEmiratesCard.EmiratesID));
                ID = objDTOEmiratesCard.EmiratesID;
            }
            parameters.Add(new SqlParameter("@EmployeeID", objDTOEmiratesCard.EmployeeID));
            parameters.Add(new SqlParameter("@CardNumber ", objDTOEmiratesCard.CardNumber));
            parameters.Add(new SqlParameter("@IDNumber", objDTOEmiratesCard.CardPersonalIDNumber));
            parameters.Add(new SqlParameter("@IssueDate", objDTOEmiratesCard.IssueDate));
            parameters.Add(new SqlParameter("@ExpiryDate", objDTOEmiratesCard.ExpiryDate));
            parameters.Add(new SqlParameter("@Remarks", objDTOEmiratesCard.Remarks));
            parameters.Add(new SqlParameter("@IsRenewed", objDTOEmiratesCard.IsRenewed));

            ID = objConnection.ExecuteScalar("MvfEmployeeEmiratesCardTransactions", parameters).ToInt32();

            if (ID > 0)
            {
                if (bAddStatus)
                {
                    objDTOEmiratesCard.EmiratesID = ID;
                }
            }
            return true;
        }

        

        public bool CheckDateofJoining()
        {
            ArrayList parameters = new ArrayList();
            SqlDataReader Reader;
            DateTime dtDateOfJoining;

            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@EmployeeID", objDTOEmiratesCard.EmployeeID));
            Reader = objConnection.ExecutesReader("MvfEmployeeEmiratesCardTransactions", parameters, CommandBehavior.CloseConnection);
            if (Reader.Read())
            {
                dtDateOfJoining = Reader["DateofJoining"].ToDateTime();
                Reader.Close();
                return true;
            }
            Reader.Close();
            return false;
        }             
       
        public DataSet GetEmployeeEmirateHealthCardReport()
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", "6"));
            parameters.Add(new SqlParameter("@CardID", objDTOEmiratesCard.EmiratesID));

            return this.objDatalayer.ExecuteDataSet("MvfEmployeeEmiratesCardTransactions", parameters);

        }
        #region FillCombos
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objDatalayer;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }
        #endregion
       
        public int UpdateEmiratesCardRenewed()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "13"));
            parameters.Add(new SqlParameter("@EmiratesID", objDTOEmiratesCard.EmiratesID));
            return Convert.ToInt32(this.objDatalayer.ExecuteScalar("MvfEmployeeEmiratesCardTransactions", parameters));

        }
    }
}
