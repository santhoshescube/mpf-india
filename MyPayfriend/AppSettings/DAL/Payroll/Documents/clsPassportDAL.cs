﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using MyPayfriend;
using System.Collections;


public class clsPassportDAL:IDisposable
    {
        public DataLayer DataLayer { get; set; }
        private IList<SqlParameter> lstSqlParams;
        

        public clsDTOPassport Passport
        {    get;          
            set;            
        }

        #region Constructor
        public clsPassportDAL(DataLayer objDataLayer)
        {
            this.DataLayer = objDataLayer;


        }
        #endregion

        #region GetReferenceTableData
        public DataSet GetReferenceTableData()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "FC"));
            return this.DataLayer.ExecuteDataSet("spEmployeePassport", lstSqlParams);        }

        #endregion

        #region Insert
        /// <summary>
        /// Insert new drivinglicense.
        /// </summary>
        /// <returns> returns LicenseId if drivinglicense is inserted successfully </returns>
        public Int32  InsertPassport()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "I"));
            lstSqlParams.Add(new SqlParameter("@EmployeeID", Passport.EmployeeID));
            lstSqlParams.Add(new SqlParameter("@NameinPassport", Passport.NameinPassport));
            lstSqlParams.Add(new SqlParameter("@PassportNumber", Passport.PassportNumber));
            lstSqlParams.Add(new SqlParameter("@PlaceofIssue", Passport.PlaceofIssue));
            lstSqlParams.Add(new SqlParameter("@CountryID", Passport.CountryID));
            lstSqlParams.Add(new SqlParameter("@PlaceOfBirth", Passport.PlaceOfBirth));
            lstSqlParams.Add(new SqlParameter("@IssueDate", Passport.IssueDate));
            lstSqlParams.Add(new SqlParameter("@ExpiryDate", Passport.ExpiryDate));
            //lstSqlParams.Add(new SqlParameter("@RenewDueDate", Passport.RenewDate));
            lstSqlParams.Add(new SqlParameter("@PassportPagesAvailable", Passport.PassportPagesAvailable));
            lstSqlParams.Add(new SqlParameter("@OldPassportNumbers", Passport.OldPassportNumbers));
            lstSqlParams.Add(new SqlParameter("@EntryRestrictions", Passport.EntryRestrictions));
           // lstSqlParams.Add(new SqlParameter("@RestrictionEndDate", Passport.RestrictionEndDate));
            lstSqlParams.Add(new SqlParameter("@ResidencePermits", Passport.ResidencePermits));
            lstSqlParams.Add(new SqlParameter("@IsRenewed", Passport.IsRenew));

            //lstSqlParams.Add(new SqlParameter("@DocumentID", Passport.DocumentID));
            //lstSqlParams.Add(new SqlParameter("@DocumentTypeID", Passport.DocumentTypeID));
            //lstSqlParams.Add(new SqlParameter("@OperationTypeID", Passport.OperationTypeID));
            //lstSqlParams.Add(new SqlParameter("@ReferenceNumber", Passport.ReferenceNumber));
            //lstSqlParams.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
            //


            // Get newly generated LicenseId (Identity)
            object objPassportID = this.DataLayer.ExecuteScalar("spEmployeePassport", lstSqlParams);

            // Returns true if returned identity value is greater than one else false
            //return Convert.ToInt32((objPassportID == null || objPassportID == DBNull.Value) ? "-1" : objPassportID) > 0;
            return objPassportID.ToInt32();
        }
        #endregion

        #region Update
        /// <summary>
        /// update drivinglicense.
        /// </summary>
        /// <returns> returns rowaffected if drivinglicense is inserted successfully else returns false. </returns>
        public bool UpdatePassport()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "U"));
            lstSqlParams.Add(new SqlParameter("@EmployeeID", Passport.EmployeeID));
            lstSqlParams.Add(new SqlParameter("@PassportID", Passport.PassportID));
            lstSqlParams.Add(new SqlParameter("@NameinPassport", Passport.NameinPassport));
            lstSqlParams.Add(new SqlParameter("@PassportNumber", Passport.PassportNumber));
            lstSqlParams.Add(new SqlParameter("@PlaceofIssue", Passport.PlaceofIssue));
            lstSqlParams.Add(new SqlParameter("@CountryID", Passport.CountryID));
            lstSqlParams.Add(new SqlParameter("@PlaceOfBirth", Passport.PlaceOfBirth));
            lstSqlParams.Add(new SqlParameter("@IssueDate", Passport.IssueDate));
            lstSqlParams.Add(new SqlParameter("@ExpiryDate", Passport.ExpiryDate));
            //lstSqlParams.Add(new SqlParameter("@RenewDueDate", Passport.RenewDate));
            lstSqlParams.Add(new SqlParameter("@PassportPagesAvailable", Passport.PassportPagesAvailable));
            lstSqlParams.Add(new SqlParameter("@OldPassportNumbers", Passport.OldPassportNumbers));
            lstSqlParams.Add(new SqlParameter("@EntryRestrictions", Passport.EntryRestrictions));
           // lstSqlParams.Add(new SqlParameter("@RestrictionEndDate", Passport.RestrictionEndDate));
            lstSqlParams.Add(new SqlParameter("@ResidencePermits", Passport.ResidencePermits));            
            lstSqlParams.Add(new SqlParameter("@IsRenewed", Passport.IsRenew));

            //lstSqlParams.Add(new SqlParameter("@DocumentID", Passport.DocumentID));

            // Get affected row
            object objRowAffected = this.DataLayer.ExecuteScalar("spEmployeePassport", lstSqlParams);

            // Returns true if returned identity value is greater than one else false
            return Convert.ToInt32(objRowAffected)>0;
        }
        #endregion

        #region Renew Passport
        public bool Renew()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "RNW"));
            lstSqlParams.Add(new SqlParameter("@PassportID", Passport.PassportID));
            object objRowAffected = this.DataLayer.ExecuteScalar("spEmployeePassport", lstSqlParams);

            // Returns true if returned identity value is greater than one else false
            return Convert.ToInt32(objRowAffected) > 0;
        }
        #endregion

        #region Delete

        /// <summary>
        /// Deletes a Driving License
        /// </summary>
        /// <returns> returns true if Driving License is deleted successfully else returns false. </returns>
        public bool DeletePassport()
        {

            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "D"));
            lstSqlParams.Add(new SqlParameter("@PassportID", Passport.PassportID));

            // Get Rows affected by the command exection.
            object objRowsAffected = this.DataLayer.ExecuteScalar("spEmployeePassport", lstSqlParams);

            // Returns true if Rows Affected is greater than one else false
            return Convert.ToInt32((objRowsAffected == null || objRowsAffected == DBNull.Value) ? "-1" : objRowsAffected) > 0;

        }

        #endregion
        public bool IsPassportNoExists()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "EXTS"));
            lstSqlParams.Add(new SqlParameter("@PassportID", Passport.PassportID));
            lstSqlParams.Add(new SqlParameter("@PassportNumber", Passport.PassportNumber));
            // Get Rows affected by the command exection.
            object Count = this.DataLayer.ExecuteScalar("spEmployeePassport", lstSqlParams);

            // Returns true if Rows Affected is greater than one else false
            return Convert.ToInt32((Count == null || Count == DBNull.Value) ? "-1" : Count) > 0;
        }
     
        #region GetPassportdetails
        public clsDTOPassport GetPassportDetails(string strSearchKey, int iPassportid, int iRowIndex)
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "SD"));
            lstSqlParams.Add(new SqlParameter("@PassportID", iPassportid));
            lstSqlParams.Add(new SqlParameter("@USERID", ClsCommonSettings.UserID));
            lstSqlParams.Add(new SqlParameter("@EmployeeId", this.Passport.EmployeeID));
            if (strSearchKey != "")
                lstSqlParams.Add(new SqlParameter("@SearchKey", strSearchKey));

            using (DataSet ds = this.DataLayer.ExecuteDataSet("spEmployeePassport", lstSqlParams))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        this.Passport = new clsDTOPassport();
                        this.Passport.Pager = new clsPager();
                        this.Passport.Pager.TotalRecords = Convert.ToInt64(ds.Tables[0].Rows[0][0] == DBNull.Value ? "0" : ds.Tables[0].Rows[0][0]);

                        if (this.Passport.Pager.TotalRecords == 0)
                            return null;

                        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                        {

                            iRowIndex = iRowIndex == 0 ? 0 : iRowIndex - 1;
                            DataRow Row = ds.Tables[1].Rows[iRowIndex];
                            this.MapPassport(Row);
                        }

                        return this.Passport;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }

        }
        #endregion

        #region MapPassport
        private void MapPassport(DataRow Row)
        {
           
           
            //this.Passport.DocumentID =  Row["DocumentID"]== DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["DocumentID"]);
            this.Passport.PassportID = Row["PassportID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["PassportID"]);
            this.Passport.EmployeeID = Row["EmployeeID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["EmployeeID"]);
            this.Passport.NameinPassport = Row["NameinPassport"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["NameinPassport"]);
            this.Passport.PassportNumber = Row["PassportNumber"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["PassportNumber"]);
            this.Passport.PlaceofIssue = Row["PlaceofIssue"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["PlaceofIssue"]);
            this.Passport.CountryID = Row["CountryID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["CountryID"]);
            this.Passport.PlaceOfBirth = Row["PlaceOfBirth"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["PlaceOfBirth"]);
            this.Passport.IssueDate = Row["IssueDate"] == DBNull.Value ? ClsCommonSettings.NullDate : Convert.ToDateTime(Row["IssueDate"]);
            this.Passport.ExpiryDate = Row["ExpiryDate"] == DBNull.Value ? ClsCommonSettings.NullDate : Convert.ToDateTime(Row["ExpiryDate"]);
            this.Passport.RenewDate = Row["RenewDueDate"] == DBNull.Value ? ClsCommonSettings.NullDate : Convert.ToDateTime(Row["RenewDueDate"]);
            this.Passport.PassportPagesAvailable = Convert.ToBoolean(Row["PassportPagesAvailable"]);
            this.Passport.OldPassportNumbers = Row["OldPassportNumbers"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["OldPassportNumbers"]);
            this.Passport.EntryRestrictions = Row["EntryRestrictions"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["EntryRestrictions"]);
            this.Passport.RestrictionEndDate = Row["RestrictionEndDate"] == DBNull.Value ? ClsCommonSettings.NullDate : Convert.ToDateTime(Row["RestrictionEndDate"]);
            this.Passport.ResidencePermits = Row["ResidencePermits"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["ResidencePermits"]);
          //  this.Passport.Remarks = Row["Remarks"] == DBNull.Value ? clsApplication.NullString : Convert.ToString(Row["Remarks"]);
            this.Passport.IsRenew = Convert.ToBoolean(Row["IsRenewed"]);
            this.Passport.EmpCompanyID = Row["CompanyID"].ToInt32();

        }
        #endregion

         #region Dispose Members

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!this.disposed)
                {
                    this.Passport.Dispose();
                    this.DataLayer = null;
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~clsPassportDAL()
        {
            Dispose(false);
        }
        #endregion
      

        /// <summary>
        /// Get total record count while searching as per search criteria
        /// </summary>       
        /// <param name="strSearchKey"></param>
        /// <returns>integert - number of records</returns>
        public int GetRecordCount( string strSearchKey , long iEmployeeid, int iPassportId)
        {
            int intRecordCount = 0;

            ArrayList lstSqlParams = new ArrayList();
           
            lstSqlParams.Add(new SqlParameter("@Mode", "SD"));
            lstSqlParams.Add(new SqlParameter("@EmployeeID", iEmployeeid));
            lstSqlParams.Add(new SqlParameter("@PassportID", iPassportId ));
            lstSqlParams.Add(new SqlParameter("@USERID", ClsCommonSettings.UserID));
            if(strSearchKey != " ")
                lstSqlParams.Add(new SqlParameter("@SearchKey", strSearchKey));

            SqlDataReader drPassport =  this.DataLayer.ExecuteReader("spEmployeePassport", lstSqlParams, CommandBehavior.CloseConnection);
            if (drPassport.Read())
            {
                intRecordCount = drPassport["RecordCount"].ToInt32();
            }
            drPassport.Close();
            return intRecordCount;
        }

        /// <summary>
        /// check whether the employee is in service
        /// </summary>
        /// <returns></returns>
        public static bool EmployeeWorkStatus(long EmployeeID)
        {
            ArrayList lstSqlParams = new ArrayList();

            lstSqlParams.Add(new SqlParameter("@Mode", "EWS"));
            lstSqlParams.Add(new SqlParameter("@EMPloyeeID", EmployeeID));

            object objCount = new DataLayer().ExecuteScalar("spVisa", lstSqlParams);

            return (objCount == DBNull.Value ? "0" : objCount).ToInt32() == 0;
        }


    }

