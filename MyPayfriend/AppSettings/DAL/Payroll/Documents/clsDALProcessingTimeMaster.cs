﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections;


namespace MyPayfriend
{
    /// <summary>
    /// Processing time db operations
    /// </summary>
    /// <Modified by >Jisha Bijoy</Modified>
    /// <Modified on> 14 Aug 2013</Modified>
    /// <purpose>Fine tuning </purpose>
    public class clsDALProcessingTimeMaster
    {
        private clsDTOProcessingTimeMaster MobjDTOProcessingTimeMaster = null;
        private string strProcedureName = "spDocProcessingTime";
        private List<SqlParameter> sqlParameters = null;

        DataLayer objDataLayer = null;
        ArrayList prmProcessTime; 
        public clsDALProcessingTimeMaster()
        { 
        }

        public DataLayer DataLayer
        {
            get
            {
                if (this.objDataLayer == null)
                    this.objDataLayer = new DataLayer();

                return this.objDataLayer;
            }
            set
            {
                this.objDataLayer = value;
            }
        }

        public clsDTOProcessingTimeMaster DTOProcessingTimeMaster
        {
            get
            {
                if (this.MobjDTOProcessingTimeMaster == null)
                    this.MobjDTOProcessingTimeMaster = new clsDTOProcessingTimeMaster();

                return this.MobjDTOProcessingTimeMaster;
            }
            set
            {
                this.MobjDTOProcessingTimeMaster = value;
            }
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        public DataTable FillGrid()
        {

            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", "1"),
                new SqlParameter("@UserID",MobjDTOProcessingTimeMaster.intUserID),
                new SqlParameter("@DocID", MobjDTOProcessingTimeMaster.intDocumentTypeID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView)
                 };
            return objDataLayer.ExecuteDataTable(strProcedureName, this.sqlParameters);

        }
        public bool docProcessingTime()
        {
            bool blnRetValue = false;
            foreach (clsDTOProcessingTimeMasterDetail objDetailDoc in DTOProcessingTimeMaster.DTOProcessingTimeMasterDetail)
            {
                prmProcessTime = new ArrayList();
                prmProcessTime.Add(new SqlParameter("@Mode", "2"));
                prmProcessTime.Add(new SqlParameter("@ProcessingId", objDetailDoc.intProcessingID));
                prmProcessTime.Add(new SqlParameter("@ProcessingTime",  objDetailDoc.intProcessingTime));
                prmProcessTime.Add(new SqlParameter("@AlertInterval", objDetailDoc.intAlertInterval));
                prmProcessTime.Add(new SqlParameter("@ReturnTime", objDetailDoc.intReturnTime));
                if (objDataLayer.ExecuteNonQuery(strProcedureName, prmProcessTime) != 0)
                {
                    blnRetValue = true;
                }
            }

            return blnRetValue;
        }
        public DataTable FillAlertTable()
        {
            prmProcessTime = new ArrayList();
            prmProcessTime.Add(new SqlParameter("@Mode", "3"));
            prmProcessTime.Add(new SqlParameter("@UserID", MobjDTOProcessingTimeMaster.intUserID));
            prmProcessTime.Add(new SqlParameter("@DocID", MobjDTOProcessingTimeMaster.intDocumentTypeID));

            return objDataLayer.ExecuteDataTable(strProcedureName, prmProcessTime);
        }

        public bool UpdateAlert()
        {
            bool blnRetValue = false;
            prmProcessTime = new ArrayList();
            prmProcessTime.Add(new SqlParameter("@Mode", "5"));       
            prmProcessTime.Add(new SqlParameter("@UserID", MobjDTOProcessingTimeMaster.intUserID));
            prmProcessTime.Add(new SqlParameter("@DocumentTypeID",MobjDTOProcessingTimeMaster.intDocumentTypeID));
            prmProcessTime.Add(new SqlParameter("@ProcessingTime", MobjDTOProcessingTimeMaster.intProcessingtime  ));
            prmProcessTime.Add(new SqlParameter("@ReturnTime", MobjDTOProcessingTimeMaster.intReturnTime));

            if (objDataLayer.ExecuteNonQuery(strProcedureName, prmProcessTime) != 0)
            {
                blnRetValue = true;
            }
            return blnRetValue;
        }

    }
}
