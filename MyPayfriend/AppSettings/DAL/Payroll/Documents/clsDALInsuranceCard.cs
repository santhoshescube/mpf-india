﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    public class clsDALInsuranceCard:DataLayer
    {

        public clsDTOInsuranceCard objclsDTOInsuranceCard { get; set; }
        public DataLayer objConnection { get; set; }
    
        public int SaveInsuranceDetails(bool blnAddStatus)
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", (blnAddStatus == true? 2: 3)));
            parameters.Add(new SqlParameter("@InsuranceCardId", objclsDTOInsuranceCard.InsuranceCardID));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOInsuranceCard.EmployeeID));
            parameters.Add(new SqlParameter("@CardNumber", objclsDTOInsuranceCard.CardNumber));
            parameters.Add(new SqlParameter("@PolicyNumber",objclsDTOInsuranceCard.PolicyNumber));
            parameters.Add(new SqlParameter("@PolicyName",objclsDTOInsuranceCard.PolicyName));
            parameters.Add(new SqlParameter("@InsuranceCompanyID", objclsDTOInsuranceCard.InsuranceCompanyID));
            parameters.Add(new SqlParameter("@IssueDate", objclsDTOInsuranceCard.IssueDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@ExpiryDate", objclsDTOInsuranceCard.ExpiryDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@Remarks",objclsDTOInsuranceCard.Remarks));
            parameters.Add(new SqlParameter("@IsRenewed ", objclsDTOInsuranceCard.IsRenewed));
            parameters.Add(new SqlParameter("@IssuedID", objclsDTOInsuranceCard.IssueID <= 0 ? null : objclsDTOInsuranceCard.IssueID));

            return ExecuteScalar("spPayInsuranceCard",parameters).ToInt32();
           
        }

        public bool CheckDuplication(int intIcardID,string strCardnumber)
        {
            bool blnExists = false;
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@InsuranceCardId", intIcardID));
            parameters.Add(new SqlParameter("@CardNumber",strCardnumber));
            parameters.Add(new SqlParameter("@Mode", 6));

            SqlDataReader DrInsuranceCard = ExecuteReader("spPayInsuranceCard", parameters, CommandBehavior.CloseConnection);
            if (DrInsuranceCard.Read())
            {
                blnExists = (DrInsuranceCard["RecordCount"].ToInt32() > 0? true: false);
            }
            DrInsuranceCard.Close();
            return blnExists;
        }
        public int GetRecordCount(long intEmployeeID,int intIcardID,string strSearchKey)
        {   
            int intRecordCount=0;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            parameters.Add(new SqlParameter("@InsuranceCardId", intIcardID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@SearchKey", strSearchKey));

            SqlDataReader DrInsuranceCard = ExecuteReader("spPayInsuranceCard", parameters, CommandBehavior.CloseConnection);
            if (DrInsuranceCard.Read())
            {
                intRecordCount = DrInsuranceCard["RecordCount"].ToInt32();
            }
            DrInsuranceCard.Close();
            return intRecordCount;
        }
        public bool DeleteInsuranceCard()
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@InsuranceCardId", objclsDTOInsuranceCard.InsuranceCardID));
            parameters.Add(new SqlParameter("@Mode", 4));

            if(ExecuteNonQuery("spPayInsuranceCard",parameters)!=0)
                return true;
            return false;
        }
        public DataTable GetInsuranceCardDetails(int MintRowNumber, long intEmpID, int intIcardID,string SearchKey)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@RowNumber", MintRowNumber));
            parameters.Add(new SqlParameter("@EmployeeID",intEmpID));
            parameters.Add(new SqlParameter("@InsuranceCardId",intIcardID));
            parameters.Add(new SqlParameter("@SearchKey", SearchKey));

            DataTable DtInsuranceCard =ExecuteDataTable("spPayInsuranceCard", parameters);

            return DtInsuranceCard;
        }
        public DataSet DisplayInsuranceCardReport(int intRecId)  //Insurance Report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@InsuranceCardID", intRecId));
            return ExecuteDataSet("spPayInsuranceCard", parameters);
        }
        public int UpdateInsuranceForRenewal()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "21"));
            parameters.Add(new SqlParameter("@InsuranceCardID", objclsDTOInsuranceCard.InsuranceCardID));
            return Convert.ToInt32(ExecuteScalar ("spPayInsuranceCard", parameters));

        }
        public static DataTable GetAutoCompleteList(string SearchKey,long EmployeeID, string FieldName, string TableName)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "22"));
            parameters.Add(new SqlParameter("@SearchKey", SearchKey));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));

            parameters.Add(new SqlParameter("@TableName", TableName));
            parameters.Add(new SqlParameter("@FieldName", FieldName));
            return new DataLayer().ExecuteDataTable("spPayInsuranceCard", parameters);
        }

    }
}
