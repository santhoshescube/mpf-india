﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace MyPayfriend
{
    public class clsDALLeaseAgreement : DataLayer
    {
        public clsDTOLeaseAgreement objclsDTOLeaseAgreement { get; set; }
        public DataLayer objConnection { get; set; }

        public string strProcName = "spPayLeaseAgreement";

        public int SaveLeaseAgreement(bool blnAddStatus)
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", (blnAddStatus == true ? 2 : 3)));
            parameters.Add(new SqlParameter("@LeaseID", objclsDTOLeaseAgreement.LeaseID));
            parameters.Add(new SqlParameter("@CompanyID", objclsDTOLeaseAgreement.CompanyID));
            parameters.Add(new SqlParameter("@DocumentDate", objclsDTOLeaseAgreement.DocumentDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@AgreementNo", objclsDTOLeaseAgreement.AgreementNo));
            parameters.Add(new SqlParameter("@Lessor", objclsDTOLeaseAgreement.Lessor));
            parameters.Add(new SqlParameter("@StartDate", objclsDTOLeaseAgreement.StartDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@EndDate", objclsDTOLeaseAgreement.EndDate.ToString("dd MMM yyyy")));
            parameters.Add(new SqlParameter("@RentalValue", objclsDTOLeaseAgreement.RentalValue));
            parameters.Add(new SqlParameter("@DepositHeld", objclsDTOLeaseAgreement.DepositHeld));
            parameters.Add(new SqlParameter("@Tenant", objclsDTOLeaseAgreement.Tenant));
            parameters.Add(new SqlParameter("@ContactNumbers", objclsDTOLeaseAgreement.ContactNumbers));
            parameters.Add(new SqlParameter("@Address", objclsDTOLeaseAgreement.Address));
            parameters.Add(new SqlParameter("@LandLord", objclsDTOLeaseAgreement.LandLord));
            parameters.Add(new SqlParameter("@UnitNumber", objclsDTOLeaseAgreement.UnitNumber));
            parameters.Add(new SqlParameter("@LeaseUnitTypeID", objclsDTOLeaseAgreement.LeaseUnitTypeID <= 0 ? null : objclsDTOLeaseAgreement.LeaseUnitTypeID));
            parameters.Add(new SqlParameter("@BuildingNumber", objclsDTOLeaseAgreement.BuildingNumber));
            parameters.Add(new SqlParameter("@Location", objclsDTOLeaseAgreement.Location));
            parameters.Add(new SqlParameter("@PlotNumber", objclsDTOLeaseAgreement.PlotNumber));
            parameters.Add(new SqlParameter("@Total", objclsDTOLeaseAgreement.Total));
            parameters.Add(new SqlParameter("@Remarks", objclsDTOLeaseAgreement.Remarks));
            parameters.Add(new SqlParameter("@LeasePeriod", objclsDTOLeaseAgreement.LeasePeriod));
            parameters.Add(new SqlParameter("@IsRenewed", objclsDTOLeaseAgreement.IsRenewed));
            return ExecuteScalar(strProcName, parameters).ToInt32();

        }

        public int GetRecordCount(int intCompanyID, int LeaseID, string strSearchKey)
        {

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@LeaseID", LeaseID));
            parameters.Add(new SqlParameter("@SearchKey", strSearchKey));

            return ExecuteScalar(strProcName, parameters).ToInt32();
        }


        public bool DeleteLeaseAgreement()
        {
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@LeaseID", objclsDTOLeaseAgreement.LeaseID));

            return (ExecuteNonQuery(strProcName, parameters) != 0) ? true : false;
        }

        public DataTable GetLeaseAgreementDetails(int MintRowNumber, int intCompanyID, int LeaseID, string SearchKey)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@RowNumber", MintRowNumber));
            parameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@LeaseID", LeaseID));
            parameters.Add(new SqlParameter("@SearchKey", SearchKey));

            return ExecuteDataTable(strProcName, parameters);
        }

        public DataSet DisplayLeaseAgreementReport(int intRecId)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@LeaseID", intRecId));
            return ExecuteDataSet(strProcName, parameters);
        }

        public int UpdateLeaseForRenewal()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "12"));
            parameters.Add(new SqlParameter("@LeaseID", objclsDTOLeaseAgreement.LeaseID));
            return Convert.ToInt32(ExecuteScalar(strProcName, parameters));
        }
      
    }
}
