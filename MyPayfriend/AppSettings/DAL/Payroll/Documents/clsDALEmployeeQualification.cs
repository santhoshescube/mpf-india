﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
namespace MyPayfriend
{
    public class clsDALEmployeeQualification
    {
        DataLayer DataLayer;
        private string ProcedureName = "spPayQualification";
        public clsDTOEmployeeQualification objclsDTOEmployeeQualification { get; set; }

        public clsDALEmployeeQualification(DataLayer objDataLayer)
        {
            this.DataLayer = objDataLayer;
        }
        /// <summary>
        /// Fill combo box
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns>datatable</returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        /// <summary>
        /// Get count of records
        /// </summary>
        /// <returns>int</returns>
        public int GetRecordCount(long intEmpID, int intQid, string strSearchKey)
        {
            int intRecordCount = 0;
            ArrayList parameters = new ArrayList();

            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@EmployeeID", intEmpID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@QualificationID", intQid));
            parameters.Add(new SqlParameter("@SearchKey", strSearchKey));

            SqlDataReader DrQCard = DataLayer.ExecuteReader(ProcedureName, parameters, CommandBehavior.CloseConnection);
            if (DrQCard.Read())
            {
                intRecordCount = DrQCard["Count"].ToInt32();
            }
            DrQCard.Close();
            return intRecordCount;
        }
        /// <summary>
        /// Saves or update employee qualification
        /// </summary>
        /// <returns>int</returns>
        public int Save()
        {
            IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

            if (objclsDTOEmployeeQualification.QualificationID == 0)
                lstSqlParameters.Add(new SqlParameter("@Mode", 1));
            else
            {
                lstSqlParameters.Add(new SqlParameter("@Mode", 2));
                lstSqlParameters.Add(new SqlParameter("@QualificationID", objclsDTOEmployeeQualification.QualificationID));
            }
            lstSqlParameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeQualification.EmployeeID));
            lstSqlParameters.Add(new SqlParameter("@ReferenceNumber", objclsDTOEmployeeQualification.ReferenceNumber));
            lstSqlParameters.Add(new SqlParameter("@UniversityID", objclsDTOEmployeeQualification.UniversityID.ToInt32() <= 0 ? null : objclsDTOEmployeeQualification.UniversityID));
            lstSqlParameters.Add(new SqlParameter("@FromDate", objclsDTOEmployeeQualification.FromDate));
            lstSqlParameters.Add(new SqlParameter("@ToDate", objclsDTOEmployeeQualification.ToDate));
            lstSqlParameters.Add(new SqlParameter("@SchoolOrCollege", objclsDTOEmployeeQualification.SchoolOrCollege));
            lstSqlParameters.Add(new SqlParameter("@DegreeID", objclsDTOEmployeeQualification.DegreeID));
            lstSqlParameters.Add(new SqlParameter("@CertificateTitle", objclsDTOEmployeeQualification.CertificateTitle));
            lstSqlParameters.Add(new SqlParameter("@GradeorPercentage", objclsDTOEmployeeQualification.GradeorPercentage));
            lstSqlParameters.Add(new SqlParameter("@CertificatesAttested", objclsDTOEmployeeQualification.CertificatesAttested));
            lstSqlParameters.Add(new SqlParameter("@CertificatesVerified", objclsDTOEmployeeQualification.CertificatesVerified));
            lstSqlParameters.Add(new SqlParameter("@UniversityClearenceRequired", objclsDTOEmployeeQualification.UniversityClearenceRequired));
            lstSqlParameters.Add(new SqlParameter("@ClearenceCompleted", objclsDTOEmployeeQualification.ClearenceCompleted));
            lstSqlParameters.Add(new SqlParameter("@Remarks", objclsDTOEmployeeQualification.Remarks));

            int QualificationID = Convert.ToInt32(DataLayer.ExecuteScalar(ProcedureName, lstSqlParameters));

            if (QualificationID > 0)
                return QualificationID;
            else
                return 0;
        }
        /// <summary>
        /// Display employee qualification details
        /// </summary>
        /// <param name="intRowNumber"></param>
        /// <returns>bool</returns>
        public bool DisplayQualificationInfo(int intRowNumber, long intEmpID, int intQid, string strSearchKey)
        {
            DataTable datQualificationDetails = new DataTable();

            IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

            lstSqlParameters.Add(new SqlParameter("@Mode", 3));
            lstSqlParameters.Add(new SqlParameter("@RowNumber", intRowNumber));
            lstSqlParameters.Add(new SqlParameter("@EmployeeID", intEmpID));
            lstSqlParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            lstSqlParameters.Add(new SqlParameter("@QualificationID", intQid));
            lstSqlParameters.Add(new SqlParameter("@SearchKey", strSearchKey));

            datQualificationDetails = DataLayer.ExecuteDataTable(ProcedureName, lstSqlParameters);

            if (datQualificationDetails.Rows.Count > 0)
            {

                objclsDTOEmployeeQualification.QualificationID = datQualificationDetails.Rows[0]["QualificationID"].ToInt32();
                objclsDTOEmployeeQualification.EmployeeID = datQualificationDetails.Rows[0]["EmployeeID"].ToInt32();
                objclsDTOEmployeeQualification.UniversityID = datQualificationDetails.Rows[0]["UniversityID"].ToInt32();
                objclsDTOEmployeeQualification.SchoolOrCollege = datQualificationDetails.Rows[0]["SchoolOrCollege"].ToString();
                objclsDTOEmployeeQualification.DegreeID = datQualificationDetails.Rows[0]["DegreeID"].ToInt32();
                objclsDTOEmployeeQualification.CertificateTitle = datQualificationDetails.Rows[0]["CertificateTitle"].ToString();
                objclsDTOEmployeeQualification.GradeorPercentage = datQualificationDetails.Rows[0]["GradeorPercentage"].ToString();
                objclsDTOEmployeeQualification.FromDate = datQualificationDetails.Rows[0]["FromDate"].ToDateTime();
                objclsDTOEmployeeQualification.ToDate = datQualificationDetails.Rows[0]["ToDate"].ToDateTime();
                objclsDTOEmployeeQualification.CertificatesVerified = datQualificationDetails.Rows[0]["CertificatesVerified"].ToBoolean() == true ? 1 : 0;
                objclsDTOEmployeeQualification.CertificatesAttested = datQualificationDetails.Rows[0]["CertificatesAttested"].ToBoolean() == true ? 1 : 0;
                objclsDTOEmployeeQualification.UniversityClearenceRequired = datQualificationDetails.Rows[0]["UniversityClearenceRequired"].ToBoolean() == true ? 1 : 0;
                objclsDTOEmployeeQualification.ClearenceCompleted = datQualificationDetails.Rows[0]["ClearenceCompleted"].ToBoolean() == true ? 1 : 0;
                objclsDTOEmployeeQualification.Remarks = datQualificationDetails.Rows[0]["Remarks"].ToString();
                objclsDTOEmployeeQualification.ReferenceNumber = datQualificationDetails.Rows[0]["ReferenceNumber"].ToString();

                return true;
            }
            return false;
        }
        /// <summary>
        /// Delete employee qualification details
        /// </summary>
        /// <returns>bool</returns>
        public bool DeleteEmployeeQualification()
        {
            IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

            lstSqlParameters.Add(new SqlParameter("@Mode", 4));
            lstSqlParameters.Add(new SqlParameter("@QualificationID", objclsDTOEmployeeQualification.QualificationID));

            return Convert.ToInt32(DataLayer.ExecuteScalar(ProcedureName, lstSqlParameters)) > 0 ? true : false;
        }
        /// <summary>
        /// get Qualification details for email
        /// </summary>
        /// <returns>datatable</returns>
        public DataSet QualificationDetailsPrintEmail()
        {
            IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();

            lstSqlParameters.Add(new SqlParameter("@Mode", 8));
            lstSqlParameters.Add(new SqlParameter("@QualificationID", objclsDTOEmployeeQualification.QualificationID));
            return DataLayer.ExecuteDataSet(ProcedureName, lstSqlParameters);
        }

        public DataTable FillDegree()
        {
            IList<SqlParameter> lstSqlParameters = new List<SqlParameter>();
            if (ClsCommonSettings.IsArabicView)
            {
                lstSqlParameters.Add(new SqlParameter("@Mode", 9));
            }
            else
            {
                lstSqlParameters.Add(new SqlParameter("@Mode", 7));
            }
            return DataLayer.ExecuteDataTable(ProcedureName, lstSqlParameters);
        }
    }
}
