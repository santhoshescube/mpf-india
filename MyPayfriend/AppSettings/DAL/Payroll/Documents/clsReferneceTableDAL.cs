﻿using System;
using System.Collections.Generic;
using System.Data;

using System.Data.SqlClient;



    public class clsReferneceTableDAL
    {
        DataLayer objDataLayer = null;

        public clsReferneceTableDAL(DataLayer ObjDataLayer)
        {
            this.objDataLayer = ObjDataLayer;
        }

        public DataTable GetRefernceTable(ReferenceTables refTable)
        {
            IList<SqlParameter> lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "SRT"));

            switch (refTable)
            {
                case ReferenceTables.Manufacturer:
                    lstSqlParams.Add(new SqlParameter("@Manufacturer", true));
                    break;

                case ReferenceTables.LicenseType:
                    lstSqlParams.Add(new SqlParameter("@LicenseType", true));
                    break;

                case ReferenceTables.VehicleCategory:
                    lstSqlParams.Add(new SqlParameter("@VehicleCategory", true));
                    break;

                case ReferenceTables.VehicleModel:
                    lstSqlParams.Add(new SqlParameter("@Model", true));
                    break;

                case ReferenceTables.VehicleLocation:
                    lstSqlParams.Add(new SqlParameter("@VehicleLocation", true));
                    break;

                case ReferenceTables.Country:
                    lstSqlParams.Add(new SqlParameter("@Country", true));
                    break;

                case ReferenceTables.Nationality:
                    lstSqlParams.Add(new SqlParameter("@Nationality", true));
                    break;

                case ReferenceTables.Religion:
                    lstSqlParams.Add(new SqlParameter("@Religion", true));
                    break;

              

              


                case ReferenceTables.Language:
                    lstSqlParams.Add(new SqlParameter("@Language", true));
                    break;

                case ReferenceTables.InsuranceCompany:
                    lstSqlParams.Add(new SqlParameter("@InsuranceCompany", true));
                    break;

              

               

              

              
                default:
                    throw new ArgumentException("Invalid ReferenceTable");
            }

            return objDataLayer.ExecuteDataTable("spReferenceTables", lstSqlParams);
        }

        public DataTable GetVehicleModelByManufacturerID(int ManufacturerID)
        {
            IList<SqlParameter> lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "SRT"));
            lstSqlParams.Add(new SqlParameter("@Model", true));
            lstSqlParams.Add(new SqlParameter("@ManufacturerID", ManufacturerID));

            return objDataLayer.ExecuteDataTable("spReferenceTables", lstSqlParams);
        }
    }
    
