﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    public class clsDALHealthCard : DataLayer
    {
        private clsDTOHealthCard MobjDTOHealthCard = null;
        private string strProcedureName = "MvfEmployeeHealthCardTransactions";
        public clsDTOHealthCard PobjDTOHealthCard { get; set; }
        clsConnection objConnection = new clsConnection();
        ArrayList parameters = null;

        #region GetHealthCardDetails
        public DataTable GetHealthCardDetails(int RowNumber, long EmployeeID, int HealthCardID, string strSearchKey)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@RowNumber", RowNumber));
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@FormType", 1));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@HealthCardId", HealthCardID));
            parameters.Add(new SqlParameter("@Searchkey", strSearchKey));
            return ExecuteDataTable(strProcedureName, parameters);
        }
        #endregion GetHealthCardDetails

        #region SaveHealthCardDetails
        public int SaveHealthCardDetails(bool blAddStatus)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", (blAddStatus ? 2 : 3)));
            parameters.Add(new SqlParameter("@HealthCardId", PobjDTOHealthCard.CardID));
            parameters.Add(new SqlParameter("@CardNumber", PobjDTOHealthCard.CardNumber));
            parameters.Add(new SqlParameter("@CardPersonalNumber", PobjDTOHealthCard.CardPersonalIDNumber));
            parameters.Add(new SqlParameter("@EmployeeId", PobjDTOHealthCard.EmployeeID));
            parameters.Add(new SqlParameter("@IssueDate", PobjDTOHealthCard.IssueDate));
            parameters.Add(new SqlParameter("@ExpiryDate",PobjDTOHealthCard.ExpiryDate));
            parameters.Add(new SqlParameter("@Remarks", PobjDTOHealthCard.Remarks));
            parameters.Add(new SqlParameter("@IsRenewed", PobjDTOHealthCard.IsRenewed));
           
            return (ExecuteScalar(strProcedureName, parameters)).ToInt32();
        }
        #endregion SaveHealthCardDetails

        #region DeleteHealthCard
        public bool DeleteHealthCard(int HealthCardID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@CardID", HealthCardID));
            return (ExecuteNonQuery(strProcedureName, parameters)) > 0 ? true : false;
        }
        #endregion DeleteHealthCard

        #region GetRowCount
        public int GetRowCount(long EmployeeID, int HealthCardID, string strSearchKey)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            if (EmployeeID > 0)
                parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            if (HealthCardID > 0)
                parameters.Add(new SqlParameter("@HealthCardId", HealthCardID));
           
            parameters.Add(new SqlParameter("@Searchkey", strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return (ExecuteScalar(strProcedureName, parameters)).ToInt32();
        }
        #endregion GetRowCount
                
        #region DisplaHealthCardReport
        /// <summary>
        /// Get report for email
        /// </summary>
        /// <param name="intRecId">HealthcardID</param>
        /// <returns>Datatable of healthcard details</returns>
        public DataSet DisplaHealthCardReport(int intRecId) 
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "9"));
            parameters.Add(new SqlParameter("@CardID", intRecId));
            return ExecuteDataSet("MvfEmployeeHealthCardTransactions", parameters);
        }
        #endregion DisplaHealthCardReport

       public int UpdateHealthCard(int HealthCardID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "20"));
            parameters.Add(new SqlParameter("@HealthCardId", HealthCardID));
            return ExecuteScalar("MvfEmployeeHealthCardTransactions", parameters).ToInt32();

        }

        public int GetRowNumber(int HealthCardID)
        {
            try
            {
                int Rownum;
                parameters = new ArrayList();
                SqlDataReader Reader;
                parameters.Add(new SqlParameter("@Mode", "21"));
                parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
                parameters.Add(new SqlParameter("@HealthCardId", HealthCardID));
                
                Reader = ExecuteReader("MvfEmployeeHealthCardTransactions", parameters, CommandBehavior.CloseConnection);
                if (Reader.Read())
                {
                    Rownum = Reader["RowNum"].ToInt32();

                    Reader.Close();
                    return Rownum;
                }
                Reader.Close();
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
