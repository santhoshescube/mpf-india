﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using MyPayFriend;
using System.Collections;

public class clsVisaDAL : IDisposable
    {
        public DataLayer DataLayer { get; set; }
        public clsVisa Visa { get; set; }
        private IList<SqlParameter> lstSqlParams;

        #region Constructor
        public clsVisaDAL(DataLayer objDataLayer)
        {
            this.DataLayer = objDataLayer;
        }
        #endregion

        #region InsertVisa

        /// <summary>
        /// Insert new Visa.
        /// </summary>
        /// <returns> returns true if Visa is inserted successfully else returns false. </returns>
        public Int32  InsertVisa()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "INS"));

            this.FillParams(ref lstSqlParams);

            // Get newly generated VisaID (Identity)
            object objVisaID = this.DataLayer.ExecuteScalar("spVisa", lstSqlParams);
            
            // Returns true if returned identity value is greater than one else false
            return objVisaID.ToInt32();
        }

        #endregion

        #region UpdateVisa

        /// <summary>
        /// Updates Visa
        /// </summary>
        /// <returns> returns true if Visa is upddated successfully else returns false. </returns>
        public bool UpdateVisa()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "UPD"));

            this.FillParams(ref lstSqlParams);

            // set ID of the Visa to be updated
            lstSqlParams.Add(new SqlParameter("@VisaID", Visa.VisaID));
           // lstSqlParams.Add(new SqlParameter("@DocumentID", Visa.DocumentID));

            // Get Rows affected by the command exection.
            object objRowsAffected = this.DataLayer.ExecuteScalar("spVisa", lstSqlParams);

            // Returns true if Rows Affected is greater than one else false
            return Convert.ToInt32((objRowsAffected == null || objRowsAffected == DBNull.Value) ? "-1" : objRowsAffected) > 0;
        }

        private void FillParams(ref IList<SqlParameter> lstParams)
        {
            lstParams.Add(new SqlParameter("@EmployeeID", Visa.EmployeeID));
            lstParams.Add(new SqlParameter("@VisaNumber", Visa.VisaNumber));
            lstParams.Add(new SqlParameter("@VisaIssueDate", Visa.VisaIssueDate));
            lstParams.Add(new SqlParameter("@PlaceofIssue", Visa.PlaceofIssue));
            lstParams.Add(new SqlParameter("@VisaExpiryDate", Visa.VisaExpiryDate));
            lstParams.Add(new SqlParameter("@IssueCountryID", Visa.IssueCountryID));
            lstParams.Add(new SqlParameter("@Remarks", Visa.Remarks));
            lstParams.Add(new SqlParameter("@IsRenewed", Visa.IsRenew));


            lstParams.Add(new SqlParameter("@DocumentID", Visa.DocumentID));
            lstParams.Add(new SqlParameter("@DocumentTypeID", Visa.DocumentTypeID));
            lstParams.Add(new SqlParameter("@OperationTypeID", Visa.OperationTypeID));
            lstParams.Add(new SqlParameter("@ReferenceNumber", Visa.ReferenceNumber));
            lstParams.Add(new SqlParameter("@VisaTypeID", Visa.VisaTypeID));
            lstParams.Add(new SqlParameter("@VisaCompanyID", Visa.visaCompanyID));
            lstParams.Add(new SqlParameter("@VisaDesignationID", Visa.visaDesignationID));
            lstParams.Add(new SqlParameter("@UID", Visa.UID));
            lstParams.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));


        }

        #endregion

        #region DeleteVisa

        /// <summary>
        /// Deletes a Visa
        /// </summary>
        /// <returns> returns true if Visa is deleted successfully else returns false. </returns>
        public bool DeleteVisa()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "DEL"));
            lstSqlParams.Add(new SqlParameter("@VisaID", Visa.VisaID));
            
            // Get Rows affected by the command exection.
            object objRowsAffected = this.DataLayer.ExecuteScalar("spVisa", lstSqlParams);

            // Returns true if Rows Affected is greater than one else false
            return Convert.ToInt32((objRowsAffected == null || objRowsAffected == DBNull.Value) ? "-1" : objRowsAffected) > 0;

        }

        #endregion

        #region IsVisaNumberExists

        public bool IsVisaNumberExists(Mode mode)
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "IVE"));
            lstSqlParams.Add(new SqlParameter("@VisaNumber", this.Visa.VisaNumber));
            if(mode == Mode.Update)
                lstSqlParams.Add(new SqlParameter("@VisaID", this.Visa.VisaID));
            
            // Get number of Visas having registration number equal to input registration number
            object objCount = this.DataLayer.ExecuteScalar("spVisa", lstSqlParams);

            // Returns true if objCount > 0 else returns false.
            return Convert.ToInt32( (objCount == null || objCount == DBNull.Value) ? "-1" : objCount ) > 0;
        }

        #endregion

        #region GetReferenceTableData

        public DataSet GetReferenceTableData()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "FC"));
            return this.DataLayer.ExecuteDataSet("spEmployeePassport", lstSqlParams);
        }

        #endregion        

        #region GetVisadetails
        public clsVisa GetVisaDetails(Int32 intVisaID, string strSearchKey,int iRowIndex)
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "SD"));
            lstSqlParams.Add(new SqlParameter("@VisaID", intVisaID));
            lstSqlParams.Add(new SqlParameter("@USERID", ClsCommonSettings.UserID));
            lstSqlParams.Add(new SqlParameter("@EmployeeId", this.Visa.EmployeeID));
            if (strSearchKey != "")
                lstSqlParams.Add(new SqlParameter("@SearchKey", strSearchKey));

            using (DataSet ds = this.DataLayer.ExecuteDataSet("spVisa", lstSqlParams))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        this.Visa = new clsVisa();
                        this.Visa.Pager = new clsPager();
                        this.Visa.Pager.TotalRecords = Convert.ToInt64(ds.Tables[0].Rows[0][0] == DBNull.Value ? "0" : ds.Tables[0].Rows[0][0]);

                        if (this.Visa.Pager.TotalRecords == 0)
                            return null;

                        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                        {

                            iRowIndex = iRowIndex == 0 ? 0 : iRowIndex - 1;
                            DataRow Row = ds.Tables[1].Rows[iRowIndex];
                            this.MapVisa(Row);
                        }

                        return this.Visa;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }

        }

        #endregion

        #region RenewVisa 

        /// <summary>
        /// renew visa
        /// </summary>
        /// <param name="iVisaID">int visaid</param>
        /// <returns>update status</returns>
        public bool RenewVisa()
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "RNW"));
            lstSqlParams.Add(new SqlParameter("@VisaID", this.Visa.VisaID));

            object objCount = this.DataLayer.ExecuteScalar("spVisa", lstSqlParams);
            return Convert.ToInt32((objCount == null || objCount == DBNull.Value) ? "-1" : objCount) > 0;
        }

        #endregion
       

       
        public int GetEmployeeID(int IntVisaID)
        {
            lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "EMPNO"));
            lstSqlParams.Add(new SqlParameter("@VisaID", IntVisaID));
            return DataLayer.ExecuteScalar("spVisa", lstSqlParams).ToInt32();

        }
      




        /// <summary>
        /// Get total record count while searching as per search criteria
        /// </summary>       
        /// <param name="strSearchKey"></param>
        /// <returns>integer - number of records</returns>
        public int GetRecordCount(string strSearchKey, long iEmployeeid, int iVisaId)
        {
            int intRecordCount = 0;

            ArrayList lstSqlParams = new ArrayList();

            lstSqlParams.Add(new SqlParameter("@Mode", "SD"));
            lstSqlParams.Add(new SqlParameter("@EmployeeID", iEmployeeid));
            lstSqlParams.Add(new SqlParameter("@USERID", ClsCommonSettings.UserID));
            lstSqlParams.Add(new SqlParameter("@VisaID", iVisaId));
            if (strSearchKey != " ")
                lstSqlParams.Add(new SqlParameter("@SearchKey", strSearchKey));

            using (DataSet ds = this.DataLayer.ExecuteDataSet("spVisa", lstSqlParams))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        intRecordCount = Convert.ToInt32(ds.Tables[0].Rows[0][0] == DBNull.Value ? "0" : ds.Tables[0].Rows[0][0]);
                    }
                }
            }
            return intRecordCount;
        }
     

        #region MapVisa
        private void MapVisa(DataRow Row)
        {
            this.Visa.VisaNumber = Row["VisaNumber"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["VisaNumber"]);
            this.Visa.VisaIssueDate = Row["VisaIssueDate"] == DBNull.Value ? ClsCommonSettings.NullDate : Convert.ToDateTime(Row["VisaIssueDate"]);
            this.Visa.VisaExpiryDate = Row["VisaExpiryDate"] == DBNull.Value ? ClsCommonSettings.NullDate : Convert.ToDateTime(Row["VisaExpiryDate"]);
            this.Visa.IssueCountryID = Row["IssueCountryID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["IssueCountryID"]);
            this.Visa.EmployeeID = Row["EmployeeID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["EmployeeID"]);
            this.Visa.VisaID = Row["VisaID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["VisaID"]);
            this.Visa.PlaceofIssue = Row["PlaceofIssue"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["PlaceofIssue"]);
            this.Visa.IsRenew = Row["IsRenewed"] == DBNull.Value ? false: Convert.ToBoolean(Row["IsRenewed"]);
            this.Visa.VisaTypeID = Convert.ToInt32(Row["VisaTypeID"]) <=0 ? -1 : Convert.ToInt32(Row["VisaTypeID"]);
            this.Visa.Remarks = Row["Remarks"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["Remarks"]);
            this.Visa.UID = Row["UID"] == DBNull.Value ? ClsCommonSettings.NullString : Convert.ToString(Row["UID"]);
            this.Visa.visaCompanyID = Row["VisaCompanyID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["VisaCompanyID"]);
            this.Visa.visaDesignationID = Row["VisaDesignationID"] == DBNull.Value ? ClsCommonSettings.NullInt : Convert.ToInt32(Row["VisaDesignationID"]);
        }
        #endregion

        # region GetVisaReport

        public  DataSet GetEmployeeVisaReport()
        {
            lstSqlParams = new List<SqlParameter>();

            lstSqlParams.Add(new SqlParameter("@Mode", "EVR"));
            lstSqlParams.Add(new SqlParameter("@VisaID", Visa.VisaID));

            return this.DataLayer.ExecuteDataSet("spDocumentReport", lstSqlParams);

        }

        #endregion

        #region FillCombos
        public DataTable FillCombos(string[] sarFieldValues)
        {
            if (sarFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(sarFieldValues);
                }

            }
            return null;
        }
        #endregion

        #region IsValidIssueDate
        public bool IsValidIssueDate(DateTime IssueDate, int EmployeeID)
        {
            lstSqlParams = new List<SqlParameter>();
            lstSqlParams.Add(new SqlParameter("@Mode", "DOBV"));
            lstSqlParams.Add(new SqlParameter("@EmployeeID", EmployeeID));
            lstSqlParams.Add(new SqlParameter("@VisaIssueDate", IssueDate));

            object objCount = this.DataLayer.ExecuteScalar("spVisa", lstSqlParams);

            // Returns true if vissa issue date is smaller than date of birth of employee
            return Convert.ToInt32(objCount == DBNull.Value ? "0" : objCount) == 0;
        }
        #endregion

   

        #region Dispose Members

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!this.disposed)
                {
                    this.Visa.Dispose();
                    this.DataLayer = null;
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~clsVisaDAL()
        {
            Dispose(false);
        }
        #endregion

      
    }

