﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data; 
using System.Data.SqlClient;
using Microsoft.VisualBasic; 

namespace MyPayfriend
{
           

    public class clsDALWorkSheet
    {

         ClsCommonUtility MObjClsCommonUtility;

         //public clsDTOCompanyInformation PobjclsDTOCompanyInformation { get; set; } // DTO Company Information Property

        private clsDTOWorkSheet MobjDTOWorkSheet = null;
        private string strProcedureName = "spPayWorkSheetTransaction";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALWorkSheet(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOWorkSheet DTOWorkSheet
        {
            get
            {
                if (this.MobjDTOWorkSheet == null)
                    this.MobjDTOWorkSheet = new clsDTOWorkSheet();

                return this.MobjDTOWorkSheet;
            }
            set
            {
                this.MobjDTOWorkSheet = value;
            }
        }

    public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }


    public DataTable GetWorkSheetDetails()
    {
        ArrayList ParameterArray = new ArrayList();
        ParameterArray.Add(new SqlParameter("@Mode", 1));
        ParameterArray.Add(new SqlParameter("@CompanyID", MobjDTOWorkSheet.iCompanyID ));
        ParameterArray.Add(new SqlParameter("@DepartmentID", MobjDTOWorkSheet.iDepartmentID));
        ParameterArray.Add(new SqlParameter("@FromDate1", MobjDTOWorkSheet.strFromDate));
        return DataLayer.ExecuteDataTable(strProcedureName, ParameterArray, 1000);
    }



    public bool SaveWorkSheet()
    {
        try
        {

            object objOutWorkSheetID = 0;

            if (MobjDTOWorkSheet.lstDTOWorkSheetDetail.Count > 0)
            {
                SaveWorkSheetDetail();
            }

            return true;
        }
        catch
        {
            return false;
        }

    }

    public void SaveWorkSheetDetail()
    {
            ArrayList prmWorkSheetDe;
            foreach (clsDTOWorkSheetDetail objClsWorkSheetDetail in MobjDTOWorkSheet.lstDTOWorkSheetDetail)
            {
                //if (objClsWorkSheetDetail.dblWorkedDays > 0)
                //{
                    prmWorkSheetDe = new ArrayList();
                    prmWorkSheetDe.Add(new SqlParameter("@Mode", 3));
                    prmWorkSheetDe.Add(new SqlParameter("@EmployeeID", objClsWorkSheetDetail.iEmployeeID));
                    prmWorkSheetDe.Add(new SqlParameter("@WorkedDays", objClsWorkSheetDetail.dblWorkedDays));
                    prmWorkSheetDe.Add(new SqlParameter("@AbsentDays", objClsWorkSheetDetail.decAbsentDays));
                    prmWorkSheetDe.Add(new SqlParameter("@OTHour", objClsWorkSheetDetail.decOTHour));
                    prmWorkSheetDe.Add(new SqlParameter("@FromDate1", objClsWorkSheetDetail.strFromDate));
                    prmWorkSheetDe.Add(new SqlParameter("@Remarks", objClsWorkSheetDetail.strRemarks));
                    prmWorkSheetDe.Add(new SqlParameter("@ToDate1", objClsWorkSheetDetail.strToDate.Trim()));
                    prmWorkSheetDe.Add(new SqlParameter("@DepartmentID", objClsWorkSheetDetail.iDepartmentID));
                    prmWorkSheetDe.Add(new SqlParameter("@CompanyID", objClsWorkSheetDetail.iCompanyID));
                    prmWorkSheetDe.Add(new SqlParameter("@TotalDays", objClsWorkSheetDetail.decTotalDays));
                    prmWorkSheetDe.Add(new SqlParameter("@HolidayWorkCnt", objClsWorkSheetDetail.decHolidayWorked));
                    MobjDataLayer.ExecuteNonQuery(strProcedureName, prmWorkSheetDe);
                //}
            }
    }

    public bool DeleteWorkSheet()
    {
            ArrayList ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 4));
            ParameterArray.Add(new SqlParameter("@FromDate1", MobjDTOWorkSheet.strFromDate));
            ParameterArray.Add(new SqlParameter("@DepartmentID", MobjDTOWorkSheet.iDepartmentID));
            ParameterArray.Add(new SqlParameter("@CompanyID", MobjDTOWorkSheet.iCompanyID));
            MobjDataLayer.ExecuteNonQuery(strProcedureName, ParameterArray);
            return true;
    }

    public bool DeleteWorkSheetEmpty()
    {
        ArrayList ParameterArray = new ArrayList();
        ParameterArray.Add(new SqlParameter("@Mode", 5));
        MobjDataLayer.ExecuteNonQuery(strProcedureName, ParameterArray);
        return true;
    }
    public bool IsSalaryReleased(int iEmpID, string sDate)
    {
        //Checking Salary Is Released or processed
        int iReturnID = 0;
        ArrayList ParameterArray = new ArrayList();
        ParameterArray = new ArrayList();
        ParameterArray.Add(new SqlParameter("@Mode", 6));
        ParameterArray.Add(new SqlParameter("@EmployeeID", iEmpID));
        ParameterArray.Add(new SqlParameter("@Date", sDate));

        iReturnID = Convert.ToInt32(this.MobjDataLayer.ExecuteScalar("spPayAttendanceManual", ParameterArray));
        return (iReturnID > 0);
    }

    }
}
