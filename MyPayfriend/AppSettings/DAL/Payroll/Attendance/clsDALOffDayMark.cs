﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
namespace MyPayfriend
{
    public class clsDALOffDayMark
    {
        ArrayList ParameterArray;
        public DataLayer objConnection { get; set; }
        public clsDTOOffDayMark MobjclsDTOOffDayMark { get; set; }
        ClsCommonUtility MobjClsCommonUtility;

        string strProcedureName = "spPayOffDayMark";

        public clsDALOffDayMark()
        {
            MobjClsCommonUtility = new ClsCommonUtility();
        }
        public DataTable GetOffDayMarkDetails()
        {
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 1));
            ParameterArray.Add(new SqlParameter("@CompanyID", MobjclsDTOOffDayMark.intCompanyID));
            ParameterArray.Add(new SqlParameter("@RosterDate1", MobjclsDTOOffDayMark.strRosterDate));
            ParameterArray.Add(new SqlParameter("@DepartmentID", MobjclsDTOOffDayMark.intDepartmentID));
            return this.objConnection.ExecuteDataTable(strProcedureName, ParameterArray,1000);
        }

        public bool SaveOffDayMarks()
        {
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 2));
            ParameterArray.Add(new SqlParameter("@CompanyID", MobjclsDTOOffDayMark.intCompanyID));
            ParameterArray.Add(new SqlParameter("@EmployeeID", MobjclsDTOOffDayMark.intEmployeeID));
            ParameterArray.Add(new SqlParameter("@RosterDate1", MobjclsDTOOffDayMark.strRosterDate));
            ParameterArray.Add(new SqlParameter("@RosterStatusID", MobjclsDTOOffDayMark.intRosterStatusID));
            ParameterArray.Add(new SqlParameter("@DepartmentID", MobjclsDTOOffDayMark.intDepartmentID));
            ParameterArray.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID ));
            this.objConnection.ExecuteNonQuery(strProcedureName, ParameterArray);
            return true;
        }

        public void DeleteOffDayMarks()
        {
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 3));
            ParameterArray.Add(new SqlParameter("@CompanyID", MobjclsDTOOffDayMark.intCompanyID));
            ParameterArray.Add(new SqlParameter("@RosterDate1", MobjclsDTOOffDayMark.strRosterDate));
            ParameterArray.Add(new SqlParameter("@DepartmentID", MobjclsDTOOffDayMark.intDepartmentID));
            this.objConnection.ExecuteNonQuery(strProcedureName, ParameterArray);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                return MobjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }
        public DataTable FillCombos(string sQuery)
        {
            // function for getting datatable for filling combo

            return objConnection.ExecuteDataTable(sQuery);

        }
        public DataTable DeleteValid()
        {
            ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 3));
            ParameterArray.Add(new SqlParameter("@CompanyID", MobjclsDTOOffDayMark.intCompanyID));
            ParameterArray.Add(new SqlParameter("@RosterDate1", MobjclsDTOOffDayMark.strRosterDate));
            ParameterArray.Add(new SqlParameter("@DepartmentID", MobjclsDTOOffDayMark.intDepartmentID));
            return this.objConnection.ExecuteDataTable(strProcedureName, ParameterArray);
        }
        public bool IsAttendance()
        {
            try
            {
                DataTable DT;
                ParameterArray = new ArrayList();
                ParameterArray.Add(new SqlParameter("@Mode", 5));
                ParameterArray.Add(new SqlParameter("@EmployeeID", MobjclsDTOOffDayMark.intEmployeeID));
                ParameterArray.Add(new SqlParameter("@RosterDate1", MobjclsDTOOffDayMark.strRosterDate));
                DT = objConnection.ExecuteDataTable(strProcedureName, ParameterArray, 1000);
                if (DT.Rows.Count > 0)
                {
                    if (DT.Rows[0][0].ToInt32() == 0)
                    {
                        return false; 
                    }
                    else
                    {
                        return true;
                    }

                }
                else
                {
                    return false; 
                }
            }
            catch
            { return false; }
        }
    }
}