﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    public class clsDALEmployeeExpense
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOEmployeeExpense objclsDTOEmployeeExpense { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spPayEmployeeExpense";

        public clsDALEmployeeExpense(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public long GetRecordCount()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOEmployeeExpense.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return Convert.ToInt64(MobjDataLayer.ExecuteScalar(strProcName, parameters));
        }

        public DataTable DisplayExpenseDetails(long lngRowNum)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@RowNum", lngRowNum));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOEmployeeExpense.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public long SaveExpenseDetails()
        {
            parameters = new ArrayList();
            if (objclsDTOEmployeeExpense.lngEmployeeExpenseID == 0)
                parameters.Add(new SqlParameter("@Mode", 3)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 4)); // Update

            parameters.Add(new SqlParameter("@EmployeeExpenseID", objclsDTOEmployeeExpense.lngEmployeeExpenseID));
            parameters.Add(new SqlParameter("@ExpenseNo", objclsDTOEmployeeExpense.lngExpenseNo));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeeExpense.lngEmployeeID));
            parameters.Add(new SqlParameter("@ExpenseHeadID", objclsDTOEmployeeExpense.intExpenseHeadID));
            parameters.Add(new SqlParameter("@ExpenseFromDate", objclsDTOEmployeeExpense.dtExpenseFromDate));
            parameters.Add(new SqlParameter("@ExpenseToDate", objclsDTOEmployeeExpense.dtExpenseToDate));
            parameters.Add(new SqlParameter("@ExpenseDate", objclsDTOEmployeeExpense.dtExpenseDate));
            parameters.Add(new SqlParameter("@ExpenseAmount", objclsDTOEmployeeExpense.decExpenseAmount));
            parameters.Add(new SqlParameter("@ExpenseTransID", objclsDTOEmployeeExpense.intExpenseTransID));
            parameters.Add(new SqlParameter("@ActualAmount", objclsDTOEmployeeExpense.decActualAmount));
            parameters.Add(new SqlParameter("@CurrencyID", objclsDTOEmployeeExpense.intCurrencyID));
            parameters.Add(new SqlParameter("@Remarks", objclsDTOEmployeeExpense.strRemarks));
            parameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objEmployeeExpenseID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objEmployeeExpenseID) != 0)
            {
                objclsDTOEmployeeExpense.lngEmployeeExpenseID = objEmployeeExpenseID.ToInt64();

                if (objclsDTOEmployeeExpense.lngEmployeeExpenseID > 0)
                    return objclsDTOEmployeeExpense.lngEmployeeExpenseID;
            }
            return 0;
        }

        public bool DeleteExpenseDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@EmployeeExpenseID", objclsDTOEmployeeExpense.lngEmployeeExpenseID));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                return true;

            return false;
        }

        public DataSet GetPrintExpenseDetails(long lngEmployeeExpenseID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@EmployeeExpenseID", lngEmployeeExpenseID));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }

        public long getRecordRowNumber(long lngEmployeeExpenseID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@EmployeeExpenseID", lngEmployeeExpenseID));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOEmployeeExpense.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt64();
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public string getAttchaedDocumentCount(long lngEmployeeExpenseID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@EmployeeExpenseID", lngEmployeeExpenseID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToString();
        }

        public long CheckEmployeeSalaryIsProcessed(long lngEmployeeID, DateTime dtDate)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            parameters.Add(new SqlParameter("@ExpenseDate", dtDate));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt64();
        }
    }
}
