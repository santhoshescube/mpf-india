﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    public class clsDALEmployeePettyCash
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOEmployeePettyCash objclsDTOEmployeePettyCash { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spPayPettyCash";

        public clsDALEmployeePettyCash(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public long GetRecordCount()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            return Convert.ToInt64(MobjDataLayer.ExecuteScalar(strProcName, parameters));
        }

        public DataTable DisplayPettyCash(long lngRowNum)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@RowNum", lngRowNum));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public bool SavePettyCash()
        {
            parameters = new ArrayList();
            if (objclsDTOEmployeePettyCash.lngPettyCashID == 0)
                parameters.Add(new SqlParameter("@Mode", 3)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 4)); // Update

            parameters.Add(new SqlParameter("@PettyCashID", objclsDTOEmployeePettyCash.lngPettyCashID));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOEmployeePettyCash.lngEmployeeID));
            parameters.Add(new SqlParameter("@PettyCashNo", objclsDTOEmployeePettyCash.strPettyCashNo));
            parameters.Add(new SqlParameter("@Date", objclsDTOEmployeePettyCash.dtDate));
            parameters.Add(new SqlParameter("@Amount", objclsDTOEmployeePettyCash.decAmount));
            parameters.Add(new SqlParameter("@ActualAmount", objclsDTOEmployeePettyCash.decActualAmount));
            parameters.Add(new SqlParameter("@EmployeeRemarks", objclsDTOEmployeePettyCash.strEmployeeRemarks));
            parameters.Add(new SqlParameter("@Description", objclsDTOEmployeePettyCash.strDescription));
            parameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objPettyCashID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objPettyCashID) != 0)
            {
                if (objPettyCashID.ToInt64() > 0)
                    return true;
            }
            return false;
        }

        public bool DeletePettyCash()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@PettyCashID", objclsDTOEmployeePettyCash.lngPettyCashID));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                return true;
            
            return false;
        }

        public DataSet DisplayPettyCashDetails(long lngPettyCashID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@PettyCashID", lngPettyCashID));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }
    }
}
