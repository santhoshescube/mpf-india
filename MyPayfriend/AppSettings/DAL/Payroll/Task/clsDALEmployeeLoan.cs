﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace MyPayfriend
{
    public class clsDALEmployeeLoan
    {
        public DataLayer objDataLayer { get; set; }
        public clsDTOEmployeeLoan objClsDTOEmployeeLoan { get; set; }
        private List<SqlParameter> sqlParameters = null;
        private static string strProcedureName = "spPayEmployeeLoan";

        public bool IsLoanNumberExists(int intLoanID,string strLoanNumber,int EmployeeID)
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 1));
            sqlParameters.Add(new SqlParameter("@LoanID", intLoanID));
            sqlParameters.Add(new SqlParameter("@LoanNumber",strLoanNumber));
            sqlParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            return objDataLayer.ExecuteScalar(strProcedureName, sqlParameters).ToInt32() > 0;
        }

        public bool SaveLoan()
        {
            sqlParameters = new List<SqlParameter>();

            if (objClsDTOEmployeeLoan.intLoanID == 0)
            {
                sqlParameters.Add(new SqlParameter("@Mode", 2));
                sqlParameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));

            }
            else
                sqlParameters.Add(new SqlParameter("@Mode", 4));

            sqlParameters.Add(new SqlParameter("@LoanID", objClsDTOEmployeeLoan.intLoanID));
            sqlParameters.Add(new SqlParameter("@LoanNumber", objClsDTOEmployeeLoan.strLoanNumber));
            sqlParameters.Add(new SqlParameter("@EmployeeID", objClsDTOEmployeeLoan.intEmployeeID));
            sqlParameters.Add(new SqlParameter("@LoanTypeID", objClsDTOEmployeeLoan.intLoanTypeID));
            sqlParameters.Add(new SqlParameter("@LoanDate", objClsDTOEmployeeLoan.dtLoanDate));
            sqlParameters.Add(new SqlParameter("@Amount", objClsDTOEmployeeLoan.decAmount));
            sqlParameters.Add(new SqlParameter("@InterestTypeID", objClsDTOEmployeeLoan.intInterestTypeID));
            sqlParameters.Add(new SqlParameter("@InterestRate", objClsDTOEmployeeLoan.decInterestRate));
            sqlParameters.Add(new SqlParameter("@PaymentAmount", objClsDTOEmployeeLoan.decPaymentAmount));
            sqlParameters.Add(new SqlParameter("@DeductStartDate", objClsDTOEmployeeLoan.dtDeductStartDate));
            sqlParameters.Add(new SqlParameter("@DeductEndDate", objClsDTOEmployeeLoan.dtDeductEndDate));
            sqlParameters.Add(new SqlParameter("@NoOfInstallment", objClsDTOEmployeeLoan.intNoOfInstallments));
            sqlParameters.Add(new SqlParameter("@CurrencyID", objClsDTOEmployeeLoan.intCurrencyID));
            sqlParameters.Add(new SqlParameter("@Reason", objClsDTOEmployeeLoan.strReason));
            sqlParameters.Add(new SqlParameter("@Closed", objClsDTOEmployeeLoan.blnClosed));
            if (objClsDTOEmployeeLoan.intClosedBy>0)
               sqlParameters.Add(new SqlParameter("@ClosedBy", objClsDTOEmployeeLoan.intClosedBy));
            if (objClsDTOEmployeeLoan.dtClosedDate!=DateTime.MinValue)
               sqlParameters.Add(new SqlParameter("@ClosedDate", objClsDTOEmployeeLoan.dtClosedDate));
            objClsDTOEmployeeLoan.intLoanID =  objDataLayer.ExecuteScalar(strProcedureName, sqlParameters).ToInt32();

            return objClsDTOEmployeeLoan.intLoanID > 0;
        }


        public bool SaveInstallmentDetails()
        {
            int iCounter = 1;

            if (DeleteInstallmentDetails())
            {
                foreach (clsDTOEmployeeLoanInstallmentDetails objDTOInstallmentDetails in objClsDTOEmployeeLoan.objClsDTOEmployeeLoanInstallmentDetails)
                {
                    sqlParameters = new List<SqlParameter>();
                    sqlParameters.Add(new SqlParameter("@Mode", 3));
                    sqlParameters.Add(new SqlParameter("@LoanID", objClsDTOEmployeeLoan.intLoanID));
                    sqlParameters.Add(new SqlParameter("@InstallmentNo", objDTOInstallmentDetails.InstallmentNo));
                    sqlParameters.Add(new SqlParameter("@InstallmentDate", objDTOInstallmentDetails.dtInstallmentDate));
                    sqlParameters.Add(new SqlParameter("@Amount", objDTOInstallmentDetails.decAmount));
                    sqlParameters.Add(new SqlParameter("@InterestAmount", objDTOInstallmentDetails.decInterestAmount));
                    objDataLayer.ExecuteNonQuery(strProcedureName, sqlParameters);
                    iCounter = iCounter + 1;
                }
            }

            if (iCounter - 1 == objClsDTOEmployeeLoan.objClsDTOEmployeeLoanInstallmentDetails.Count)
            {
                return true;
            }
            return false;
        }

        public bool DeleteInstallmentDetails()
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 6));
            sqlParameters.Add(new SqlParameter("@LoanID", objClsDTOEmployeeLoan.intLoanID));
            objDataLayer.ExecuteNonQuery(strProcedureName, sqlParameters);
            return true;
        }

        public clsDTOEmployeeLoan GetLoan(int intRecNo,long lngEmployeeID,string strSearchKey)
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 7));
            sqlParameters.Add(new SqlParameter("@RecordNumber",intRecNo));
            sqlParameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            sqlParameters.Add(new SqlParameter("@Searchkey", strSearchKey));
            sqlParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            DataSet dsLoan = objDataLayer.ExecuteDataSet(strProcedureName, sqlParameters);
            if (dsLoan != null)
            {
                if(dsLoan.Tables[0].Rows.Count > 0)
                {
                    objClsDTOEmployeeLoan = new clsDTOEmployeeLoan();
                    objClsDTOEmployeeLoan.intLoanID = dsLoan.Tables[0].Rows[0]["LoanID"].ToInt32();
                    objClsDTOEmployeeLoan.strLoanNumber = dsLoan.Tables[0].Rows[0]["LoanNumber"].ToStringCustom();
                    objClsDTOEmployeeLoan.intEmployeeID = dsLoan.Tables[0].Rows[0]["EmployeeID"].ToInt32();
                    objClsDTOEmployeeLoan.intLoanTypeID = dsLoan.Tables[0].Rows[0]["LoanTypeID"].ToInt32();
                    objClsDTOEmployeeLoan.intInterestTypeID = dsLoan.Tables[0].Rows[0]["InterestTypeID"].ToInt32();
                    objClsDTOEmployeeLoan.dtLoanDate = dsLoan.Tables[0].Rows[0]["LoanDate"].ToDateTime();
                    objClsDTOEmployeeLoan.decAmount = dsLoan.Tables[0].Rows[0]["Amount"].ToDecimal();
                    objClsDTOEmployeeLoan.decInterestRate = dsLoan.Tables[0].Rows[0]["InterestRate"].ToDecimal();
                    objClsDTOEmployeeLoan.decPaymentAmount = dsLoan.Tables[0].Rows[0]["PaymentAmount"].ToDecimal();
                    objClsDTOEmployeeLoan.dtDeductStartDate = dsLoan.Tables[0].Rows[0]["DeductStartDate"].ToDateTime();
                    objClsDTOEmployeeLoan.dtDeductEndDate = dsLoan.Tables[0].Rows[0]["DeductEndDate"].ToDateTime();
                    objClsDTOEmployeeLoan.intNoOfInstallments = dsLoan.Tables[0].Rows[0]["NoOfInstalment"].ToInt32();
                    objClsDTOEmployeeLoan.intCurrencyID = dsLoan.Tables[0].Rows[0]["CurrencyID"].ToInt32();
                    objClsDTOEmployeeLoan.strReason = dsLoan.Tables[0].Rows[0]["Reason"].ToStringCustom();
                    objClsDTOEmployeeLoan.blnClosed = dsLoan.Tables[0].Rows[0]["Closed"].ToBoolean();
                    objClsDTOEmployeeLoan.intClosedBy = dsLoan.Tables[0].Rows[0]["ClosedBy"].ToInt32();
                    objClsDTOEmployeeLoan.dtClosedDate = dsLoan.Tables[0].Rows[0]["ClosedDate"].ToDateTime();
                    objClsDTOEmployeeLoan.decBalanceAmount = dsLoan.Tables[0].Rows[0]["BalanceAmount"].ToDecimal(); 
                }
                if (dsLoan.Tables[1].Rows.Count > 0)
                {
                    objClsDTOEmployeeLoan.objClsDTOEmployeeLoanInstallmentDetails = new List<clsDTOEmployeeLoanInstallmentDetails>();

                    foreach (DataRow dr in dsLoan.Tables[1].Rows)
                    {
                        clsDTOEmployeeLoanInstallmentDetails objDTOInstallmentDetails = new clsDTOEmployeeLoanInstallmentDetails();
                        objDTOInstallmentDetails.InstallmentNo = dr["InstalmentNo"].ToInt32();
                        objDTOInstallmentDetails.dtInstallmentDate = dr["InstalmentDate"].ToDateTime();
                        objDTOInstallmentDetails.decAmount = dr["Amount"].ToDecimal();
                        objDTOInstallmentDetails.decInterestAmount = dr["InterestAmount"].ToDecimal();
                        objDTOInstallmentDetails.blnIsPaid = dr["IsPaid"].ToBoolean();
                        objClsDTOEmployeeLoan.objClsDTOEmployeeLoanInstallmentDetails.Add(objDTOInstallmentDetails);
                    }
                }
            }
            return objClsDTOEmployeeLoan;
        }

        public bool DeleteLoan()
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode",5));
            sqlParameters.Add(new SqlParameter("@LoanID", objClsDTOEmployeeLoan.intLoanID));
            if (objDataLayer.ExecuteScalar(strProcedureName, sqlParameters).ToInt32() > 0)
                return true;
            else
                return false;
        }

        public DataSet GetLoanDetails()
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 8));
            sqlParameters.Add(new SqlParameter("@LoanID", objClsDTOEmployeeLoan.intLoanID));
            sqlParameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return objDataLayer.ExecuteDataSet(strProcedureName, sqlParameters);
        }
        public DataTable GetCompanySetting(int CompanyID)
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@iMode", 5));
            sqlParameters.Add(new SqlParameter("@iCompanyID", CompanyID));
            sqlParameters.Add(new SqlParameter("@iFormID", (int)FormID.EmployeeLoan));
            return objDataLayer.ExecuteDataTable("spPayCompanySettings", sqlParameters);
        }
        public bool IsRepaymentExists()
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 9));
            sqlParameters.Add(new SqlParameter("@LoanID", objClsDTOEmployeeLoan.intLoanID));
            return objDataLayer.ExecuteScalar(strProcedureName, sqlParameters).ToInt32() > 0;
        }

        public DataTable GetPreviousInstallments(int EmployeeID,int LoanID)
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 10));
            sqlParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            sqlParameters.Add(new SqlParameter("@LoanID", LoanID));
            return objDataLayer.ExecuteDataTable(strProcedureName, sqlParameters);
        }

        public string  GenerateLoanCode(int intCompanyID)
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 12));
            sqlParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objDataLayer.ExecuteScalar(strProcedureName, sqlParameters).ToStringCustom(); 
        }

        public int GetCompanyID(int EmployeeID)
        {
            sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Mode", 13));
            sqlParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            return objDataLayer.ExecuteScalar(strProcedureName, sqlParameters).ToInt32();

        }



    }
}
