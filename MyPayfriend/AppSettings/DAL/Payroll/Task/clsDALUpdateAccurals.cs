﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
namespace MyPayfriend
{
    class clsDALUpdateAccurals
    {

        private clsDTOUpdateAccurals MobjDTOUpdateAccurals = null;
        private string strProcedureName = "spEmployeeAccurals";
        private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALUpdateAccurals() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }
        public clsDTOUpdateAccurals DTOUpdateAccurals
        {
            get
            {
                if (this.MobjDTOUpdateAccurals == null)
                    this.MobjDTOUpdateAccurals = new clsDTOUpdateAccurals();

                return this.MobjDTOUpdateAccurals;
            }
            set
            {
                this.MobjDTOUpdateAccurals = value;
            }
        }
        public DataTable GetEmployeeList(int CompanyID, string ToDate)
        {
            this.sqlParameters = new List<SqlParameter> 
           { 
            new SqlParameter("@Mode", 1) ,
           new SqlParameter("@CompanyID", CompanyID),
           new SqlParameter("@ToDate1", ToDate)
           };
            return this.DataLayer.ExecuteDataTable("spPayEmployee", this.sqlParameters);

        }
        public DataTable isEmployee(int employeeid, string ToDate, int companyid)
        {
            this.sqlParameters = new List<SqlParameter>()
              {
                new SqlParameter("@Mode", (object) 3),
                new SqlParameter("@CompanyID", (object) companyid),
                new SqlParameter("@FromDate1", (object) ToDate),
                new SqlParameter("@employeeid", (object) employeeid)
              };
            return this.MobjDataLayer.ExecuteDataTable("spPayEmployee", this.sqlParameters);
        }

        public void SaveAccurals(int empid, DateTime edate, float leavesal, float etotalsal, float etotalgra,float grtArr, int cmpid, int gdates, float Ticket, int isupdated)
        {
            try
            {
                new DataLayer().ExecuteNonQuery("spEmployeeAccurals", new List<SqlParameter>()
                {
                  new SqlParameter("@Mode", (object) 3),
                  new SqlParameter("@CompanyID", (object) cmpid),
                  new SqlParameter("@ProjectID1", SqlDbType.BigInt),
                  new SqlParameter("@DepartmentID1", SqlDbType.BigInt),
                  new SqlParameter("@IsUpdated1", (object) 1),
                  new SqlParameter("@GratuityDays1", (object) gdates),
                  new SqlParameter("@LeavePassageAmt1", (object) leavesal),
                  new SqlParameter("@GratuityAmt1", (object) etotalgra),
                   new SqlParameter("@GratuityarrAmt", (object) grtArr),
                  new SqlParameter("@MonthDate1", (object) edate),
                  new SqlParameter("@EmployeeID", (object) empid),
                  new SqlParameter("@Ticket", (object) Ticket),
                  new SqlParameter("@isupdated", (object) 1)
                });
            }
            catch (Exception ex)
            {
            }
        }

        public DataSet AccuralUpdateDetail(int EmployeeID, int CompanyID, string FromDate, string ToDate)
        {
            DataSet datTempSet;
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            /*
            @EmployeeID int = null,
	        @CompanyID int=null,
	        @CompID int=null,
	        @FromDate1 datetime=null,
	        @ToDate1 datetime=null,
             * */
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@CompID", CompanyID));
            parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            parameters.Add(new SqlParameter("@FromDate1", FromDate));
            parameters.Add(new SqlParameter("@ToDate1", ToDate));
            datTempSet = MobjDataLayer.ExecuteDataSet("spEmployeeAccurals", parameters);
            return datTempSet;
        }

    }
}
