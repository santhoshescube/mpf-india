﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
namespace MyPayfriend
{
    public class clsDALEmployeeTransfer : IDisposable
    {
        ClsCommonUtility MObjClsCommonUtility;
        DataLayer MobjDataLayer;
        private List<SqlParameter> sqlParameters = null;
        public clsDTOEmployeeTransfer PobjclsDTOEmployeeTransfer { get; set; } // 
        public clsDTOSalaryProcess PobjclsDTOSalaryProcess { get; set; } // DTO Salary Process Property
        public clsDALEmployeeTransfer(DataLayer objDataLayer)
        {
            //Constructor
            MObjClsCommonUtility = new ClsCommonUtility();
            MobjDataLayer = objDataLayer;
            MObjClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }
        public System.Data.DataTable LoadCombos(string sQuery)
        {
            // function for getting datatable for filling combo
            return MObjClsCommonUtility.FillCombos(sQuery);
        }

        #region IDisposable Members

        public void Dispose()
        {
            //disposing
            if (MObjClsCommonUtility != null)
                MObjClsCommonUtility = null;
            if (MobjDataLayer != null)
                MobjDataLayer = null;
        }

        #endregion


        internal System.Data.DataTable FillDetails(string sQuery)
        {
            DataTable DT;
            DT = MobjDataLayer.ExecuteDataTable(sQuery);

            return DT;
        }
        /// <summary>
        /// To get Employee Details
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>DataTable</returns>
        public DataTable SetEmployeeInfo(int EmployeeID)
        {
            DataTable DT;
            string sQuery = "";
            sQuery = "SELECT EM.EmployeeFullName,TT.TransactionType, WP.WorkPolicy as WorkPolicy,WL.LocationName,ISNULL(LP.LeavePolicyName,'') AS" +
                      " LeavePolicy, ISNULL(BR.BankName,'') + ' - ' +ISNULL(BB.BankBranchName,'') AS EmployerBank,ISNULL(CA.BankAccountNo,'') AS" +
                      " EmployerAccountNo,ISNULL(BB1.BankBranchName,'') + ' - ' + ISNULL(BR1.BankName,'') AS EmployeeBank, " +
                      " ISNULL(EM.AccountNumber,'') AS EmployeeAccountNo,EM.TransactionTypeID,EM.WorkPolicyID,ISNULL(EM.LeavePolicyID,0) AS " +
                      " LeavePolicyID,ISNULL(EM.BankID,0) AS BankNameID, ISNULL(EM.BankBranchID,0) AS BankBranchID,ISNULL(EM.CompanyBankAccountID,0) " +
                      " AS ComBankAccID,ISNULL(EM.WorkLocationID,0) AS WorkLocationID FROM EmployeeMaster EM  " +
                      " LEFT JOIN TransactionTypeReference TT ON TT.TransactionTypeID = EM.TransactionTypeID " +
                      " INNER JOIN PayWorkPolicyMaster WP ON WP.WorkPolicyID = EM.WorkPolicyID " +
                      " LEFT JOIN PayLeavePolicyMaster LP ON LP.LeavePolicyID = EM.LeavePolicyID " +
                      " LEFT JOIN BankBranchReference BB ON EM.BankID = BB.BankBranchID  " +
                      " LEFT JOIN BankReference BR ON BB.BankID =  BR.BankID " +
                      " LEFT JOIN  BankBranchReference BB1 ON EM.BankBranchID = BB1.BankBranchID " +
                      " LEFT JOIN BankReference BR1 ON BB1.BankID = BR1.BankID " +
                      " LEFT JOIN WorkLocationReference WL ON WL.WorkLocationID = EM.WorkLocationID " +
                      " LEFT JOIN CompanyBankAccountDetails CA ON CA.CompanyBankAccountID = EM.CompanyBankAccountID WHERE EmployeeID = " + EmployeeID + "";

            DT = MobjDataLayer.ExecuteDataTable(sQuery);
            return DT;
        }
        /// <summary>
        /// To Get Work Policies 
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable FillPolicyCombo()
        {
            try
            {
                string sQuery = "";

                sQuery = " SELECT WorkPolicyID,WorkPolicy FROM PayWorkPolicyMaster ";

                return MobjDataLayer.ExecuteDataTable(sQuery);
            }
            catch (Exception)
            {
                DataTable DTA = new DataTable();
                return DTA;
            }
        }
        /// <summary>
        /// To get Leave Policies
        /// </summary>
        /// <returns>DataTable</returns>
        public System.Data.DataTable FillLeavePolicyCombo()
        {
            string sQuery = "";
            DataSet ds;
            String s1 = "";

            if (PobjclsDTOEmployeeTransfer.TransferTypeID == 1)
            {
                s1 = " SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster  ";
            }
            else
            {
                s1 = " SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster  WHERE";
            }
            //else
            //    s1 = " SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster WHERE CompanyID = " + PobjclsDTOEmployeeTransfer.CompanyID + " ";

            if (PobjclsDTOEmployeeTransfer.TransferTypeID == 4)
            {
                s1 = s1 + "  EmploymentTypeID = " + PobjclsDTOEmployeeTransfer.TransferToID + " ";
            }

            if (PobjclsDTOEmployeeTransfer.TransferTypeID == 2)
            {
                s1 = s1 + "  DepartmentID =" + PobjclsDTOEmployeeTransfer.TransferToID + " ";
            }

            if (PobjclsDTOEmployeeTransfer.TransferTypeID == 3)
            {
                s1 = s1 + "  DesignationID =" + PobjclsDTOEmployeeTransfer.TransferToID + "";
            }

            ds = MobjDataLayer.ExecuteDataSet(s1);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (PobjclsDTOEmployeeTransfer.TransferTypeID == 1)
                {
                    sQuery = " SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster ";
                }
               else
                {
                    sQuery = " SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster WHERE";
                }

                if (PobjclsDTOEmployeeTransfer.TransferTypeID == 4)
                {
                    sQuery = sQuery + "  EmploymentTypeID = " + PobjclsDTOEmployeeTransfer.TransferToID + " ";
                }

                if (PobjclsDTOEmployeeTransfer.TransferTypeID == 2)
                {
                    sQuery = sQuery + "  DepartmentID =" + PobjclsDTOEmployeeTransfer.TransferToID + " ";
                }

                if (PobjclsDTOEmployeeTransfer.TransferTypeID == 3)
                {
                    sQuery = sQuery + " DesignationID =" + PobjclsDTOEmployeeTransfer.TransferToID + "";
                }
                return MobjDataLayer.ExecuteDataTable(sQuery);
            }
            else
            {
                //if (PobjclsDTOEmployeeTransfer.TransferTypeID == 1)
                //{
                    sQuery = " SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster ";
                //}

                //if (PobjclsDTOEmployeeTransfer.TransferTypeID == 4)
                //{
                //    sQuery = sQuery + " UNION select LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster  " +
                //           " WHERE  EmploymentTypeID = " + PobjclsDTOEmployeeTransfer.TransferToID + "  ";
                //}

                //if (PobjclsDTOEmployeeTransfer.TransferTypeID == 2)
                //{
                //    sQuery = sQuery + " UNION SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster  " +
                //             " WHERE DepartmentID =" + PobjclsDTOEmployeeTransfer.TransferToID + "  ";
                //}

                //if (PobjclsDTOEmployeeTransfer.TransferTypeID == 3)
                //{
                //    sQuery = sQuery + " UNION SELECT LeavePolicyID,LeavePolicyName FROM PayLeavePolicyMaster  " +
                //           " WHERE  DesignationID =" + PobjclsDTOEmployeeTransfer.TransferToID + "";
                //}

                if (sQuery != "")
                {
                    return MobjDataLayer.ExecuteDataTable(sQuery);

                }
                else
                {
                    DataTable Dt = new DataTable();
                    return Dt;
                }
            }
        }
        /// <summary>
        /// To check whether Policy Exists
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <returns>bool</returns>
        public bool IsDefaultPolicy()
        {
            DataTable DT;
            string TSQL = "SELECT  1 FROM  PayWorkPolicyMaster";
            DT = MobjDataLayer.ExecuteDataTable(TSQL);

            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// Payment Release Checking
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <param name="strTransferDate"></param>
        /// <returns>DataTable</returns>
        public DataTable PaymentReleaseChecking(int EmployeeID, string strTransferDate)
        {
            string TSQL = "EXEC spPaymentReleaseChecking  " + EmployeeID + ",'" + strTransferDate + "'";
            return MobjDataLayer.ExecuteDataTable(TSQL);
        }
        /// <summary>
        /// To get Transfer Date
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>string</returns>
        public string GetTransferDate(int EmployeeID)
        {
            string TSQL = "SELECT TOP 1 TransferDate FROM PayEmployeeTransfers WHERE EmployeeID=" + EmployeeID + "   ORDER BY TransferDate DESC";
            DataTable DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                return Convert.ToString(DT.Rows[0][0]);
            }
            else
            {
                return "01-Jan-1900";
            }

        }
        /// <summary>
        /// To check Leave Date Exists
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <param name="strTransferDate"></param>
        /// <returns>bool</returns>
        public bool IsLeaveDateExists(int EmployeeID, string strTransferDate)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters.Add(new SqlParameter("@FromDate1", strTransferDate));
            int res= MobjDataLayer.ExecuteScalar("spPayEmployeeTransferTransaction", parameters).ToInt32();
            if(res >0)
                return true;
            else
                return false;


        }
        /// <summary>
        /// Chescks for Employee Date of Joining
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>bool</returns>
        public string EmpDOJValidation(int EmployeeID)
        {
            string TSQL = "SELECT CONVERT(DATETIME,CONVERT(VARCHAR(10),DateofJoining,101),101) FROM Employeemaster WHERE EmployeeID=" + EmployeeID + "";

            DataTable DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                return Convert.ToString(DT.Rows[0][0]);
            }
            else
            {
                return "01-Jan-1900";
            }
        }
        /// <summary>
        /// To check loanID Exists for Employee
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>bool</returns>
        public bool IsLoanExists(int EmployeeID)
        {
            string TSQL = "SELECT * FROM Loan WHERE EmployeeID=" + EmployeeID + " AND ISNULL(Closed,0)=0";

            DataTable DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// To get Salary Day
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>int</returns>
        public int GetSalaryDay(int EmployeeID)
        {
            string TSQL = "SELECT SalaryDay FROM PaySalaryStructure WHERE EmployeeID=" + EmployeeID + "";

            DataTable DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                return Convert.ToInt32(DT.Rows[0][0]);
            }
            else
            {
                return 1;
            }

        }
        /// <summary>
        /// To save Employee Transfer
        /// </summary>
        /// <returns>bool</returns>
        public bool SaveEmployeeTransfer()
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@EmployeeID", PobjclsDTOEmployeeTransfer.EmployeeID));
            alParameters.Add(new SqlParameter("@TransferTypeID", PobjclsDTOEmployeeTransfer.TransferTypeID));
            alParameters.Add(new SqlParameter("@FromID", PobjclsDTOEmployeeTransfer.TransferFromID));
            alParameters.Add(new SqlParameter("@ToID", PobjclsDTOEmployeeTransfer.TransferToID));
            alParameters.Add(new SqlParameter("@TransferDate1", PobjclsDTOEmployeeTransfer.TransferDate.ToString("dd-MMM-yyyy")));
            alParameters.Add(new SqlParameter("@OtherInfo", PobjclsDTOEmployeeTransfer.OtherInfo));
            alParameters.Add(new SqlParameter("@IsFromRequest", PobjclsDTOEmployeeTransfer.IsFromRequest));
            alParameters.Add(new SqlParameter("@RequestId", PobjclsDTOEmployeeTransfer.RequestId));
            alParameters.Add(new SqlParameter("@Temp", PobjclsDTOEmployeeTransfer.iTemp));
            object intReturnID;
            SqlParameter sqlParameter = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sqlParameter.Direction = ParameterDirection.ReturnValue;
            alParameters.Add(sqlParameter);

            intReturnID = MobjDataLayer.ExecuteScalar("spPayEmployeeTransferTransaction", alParameters);


            if (Convert.ToInt32(intReturnID) > 0)
            {
                PobjclsDTOEmployeeTransfer.TransferID = Convert.ToInt32(intReturnID);
                PobjclsDTOEmployeeTransfer.FinYearStartDate = Convert.ToDateTime(GetFinyearStartDate(PobjclsDTOEmployeeTransfer.CompanyID, PobjclsDTOEmployeeTransfer.TransferDate.ToString("dd-MMM-yyyy")));
                SaveEmployeeTransferDetails(PobjclsDTOEmployeeTransfer.TransferID);
                UpdateEmployeeMaster(PobjclsDTOEmployeeTransfer.TransferID);
            }

            return true;
        }
        /// <summary>
        /// To save Employee Transfer Details
        /// </summary>
        /// <param name="intTransferID"></param>
        /// <returns>bool</returns>
        public bool SaveEmployeeTransferDetails(int intTransferID)
        {

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 2));
            alParameters.Add(new SqlParameter("@MachineName", PobjclsDTOEmployeeTransfer.MachineName));
            alParameters.Add(new SqlParameter("@PolicyID", PobjclsDTOEmployeeTransfer.PolicyID));
            alParameters.Add(new SqlParameter("@TransactionTypeID", PobjclsDTOEmployeeTransfer.TransactionTypeID));
            if (PobjclsDTOEmployeeTransfer.EmployerBankID != 0)
            {
                alParameters.Add(new SqlParameter("@BankNameID", PobjclsDTOEmployeeTransfer.EmployerBankID));
            }
            if (PobjclsDTOEmployeeTransfer.EmployeeBankID != 0)
            {
                alParameters.Add(new SqlParameter("@BankBranchID", PobjclsDTOEmployeeTransfer.EmployeeBankID));
            }
            alParameters.Add(new SqlParameter("@TransferID", PobjclsDTOEmployeeTransfer.TransferID));
            if (PobjclsDTOEmployeeTransfer.LeavePolicyID != 0)
            {
                alParameters.Add(new SqlParameter("@LeavePolicyID", PobjclsDTOEmployeeTransfer.LeavePolicyID));
            }

            if (PobjclsDTOEmployeeTransfer.EmployerAccountID != 0)
            {
                alParameters.Add(new SqlParameter("@ComBankAccID", PobjclsDTOEmployeeTransfer.EmployerAccountID));
            }
            if (PobjclsDTOEmployeeTransfer.LocationID != 0)
            {
                alParameters.Add(new SqlParameter("@LocationID", PobjclsDTOEmployeeTransfer.LocationID));
            }

            alParameters.Add(new SqlParameter("@AccountNo", PobjclsDTOEmployeeTransfer.EmployeeAccountNo));
            alParameters.Add(new SqlParameter("@EmployeeID", PobjclsDTOEmployeeTransfer.EmployeeID));
            MobjDataLayer.ExecuteNonQuery("spPayEmployeeTransferTransaction", alParameters);
            return true;
        }

        /// <summary>
        /// To update Employee Information
        /// </summary>
        /// <param name="intTransferID"></param>
        /// <returns>bool</returns>
        public bool UpdateEmployeeMaster(int intTransferID)
        {
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 3));
            alparameters.Add(new SqlParameter("@EmployeeID", PobjclsDTOEmployeeTransfer.EmployeeID));
            alparameters.Add(new SqlParameter("@TransferTypeID", PobjclsDTOEmployeeTransfer.TransferTypeID));
            alparameters.Add(new SqlParameter("@TransactionTypeID", PobjclsDTOEmployeeTransfer.TransactionTypeID));
            alparameters.Add(new SqlParameter("@CompanyID", PobjclsDTOEmployeeTransfer.CompanyID));
            alparameters.Add(new SqlParameter("@DepartmentID", PobjclsDTOEmployeeTransfer.DepartmentID));
            alparameters.Add(new SqlParameter("@DesignationID", PobjclsDTOEmployeeTransfer.DesignationID));
            alparameters.Add(new SqlParameter("@EmploymentTypeID", PobjclsDTOEmployeeTransfer.EmploymentTypeID));
            alparameters.Add(new SqlParameter("@TransDate1", PobjclsDTOEmployeeTransfer.TransferDate.ToString("dd-MMM-yyyy")));
            alparameters.Add(new SqlParameter("@CompanyFinYearStartDate1", PobjclsDTOEmployeeTransfer.FinYearStartDate.ToString("dd-MMM-yyyy")));
            alparameters.Add(new SqlParameter("@TransferID", intTransferID));
            MobjDataLayer.ExecuteNonQuery("spPayEmployeeTransferTransaction", alparameters);
            return true;
        }
        /// <summary>
        /// To get Employee Status
        /// </summary>
        /// <param name="Mode"></param>
        /// <param name="EmployeeID"></param>
        /// <param name="strFrom"></param>
        /// <param name="strTo"></param>
        /// <returns>int</returns>
        public int GetStatus(int Mode, int EmployeeID, string strFrom, string strTo)
        {
            try
            {
                object retVal;
                this.sqlParameters = new List<SqlParameter>();
                this.sqlParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
                if (Mode != 0)
                {
                    this.sqlParameters.Add(new SqlParameter("@Mode", Mode));
                }
                if (strFrom.Trim() != "")
                {
                    this.sqlParameters.Add(new SqlParameter("@FromDate1", strFrom));
                    this.sqlParameters.Add(new SqlParameter("@ToDate1", strTo));
                }

                retVal = MobjDataLayer.ExecuteNonQuery("spPayEmployeeTransferTransaction", sqlParameters);

                return (int)retVal;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// To check whether Payment Exists
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>bool</returns>
        public bool PaymentExist(int EmployeeID)
        {
            string TSQL = " SELECT PeriodTo,EmployeeID FROM PayEmployeePayment WHERE EmployeeID=" + EmployeeID + "";

            DataTable DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Checks Whether Salry Exists for the Employee
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <param name="FromDateStr"></param>
        /// <returns>bool</returns>
        public bool IsSalaryExists(int EmployeeID, string FromDateStr)
        {
            try
            {
                string TSQL = "SELECT 1 FROM PayEmployeePayment WHERE EmployeeID=" + EmployeeID + " AND(PeriodFrom>='" + FromDateStr + "' OR PeriodTo<='" + FromDateStr + "')";
                DataTable DT = MobjDataLayer.ExecuteDataTable(TSQL);
                if (DT.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch
            {
                return false;
            }


        }
        /// <summary>
        /// To update IncentiveType
        /// </summary>
        /// <param name="EmployeeID"></param>
        public void UpdateIncentiveType(int EmployeeID)
        {
            string TSQL = "UPDATE PaySalaryStructure SET IncentiveTypeID=NULL WHERE EmployeeID=" + EmployeeID + "";
            MobjDataLayer.ExecuteQuery(TSQL);

        }

        /// <summary>
        /// To get Fin year Start Date
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <param name="DateStr"></param>
        /// <returns>string</returns>
        public string GetFinyearStartDate(int CompanyID, string DateStr)
        {
            try
            {
                string TSQL = ""; DataTable DT; string TSQL1 = "";

                TSQL = "SELECT  dbo.fnSGetFinYearStartDate(" + CompanyID + ",'" + DateStr + "')";
                DT = MobjDataLayer.ExecuteDataTable(TSQL);
                TSQL1 = "";
                if (DT.Rows.Count > 0)
                {
                    TSQL1 = Convert.ToString(DT.Rows[0][0]);
                }
                else
                {
                    TSQL1 = "";
                }


                return TSQL1;
            }
            catch (Exception)
            {
                return "";
            }
        }
        /// <summary>
        /// To Check Employee Currency ExchangeRate
        /// </summary>
        /// <param name="MintTransferTypeID"></param>
        /// <param name="intEmployeeCurrencyID"></param>
        /// <param name="EmployeeID"></param>
        /// <param name="TransferToID"></param>
        /// <param name="iState"></param>
        /// <param name="strCompanyCurrency"></param>
        /// <param name="strEmployeeCurrency"></param>
        /// <returns></returns>
        public bool CheckEmployeeCurrencyExchangeRate(int MintTransferTypeID, int intEmployeeCurrencyID, int EmployeeID, int TransferToID, ref int iState, ref string strCompanyCurrency, ref string strEmployeeCurrency)
        {

            DataTable DT;
            if (MintTransferTypeID != 1)
            {
                iState = 1;
            }

            string TSQL = "SELECT H.CurrencyID,C.CurrencyName AS Currency FROM PaySalaryStructure H INNER JOIN EmployeeMaster EM ON EM.EmployeeID = H.EmployeeID INNER JOIN CurrencyReference C ON C.CurrencyID = H.CurrencyID WHERE EM.EmployeeID = " + EmployeeID + "";
            DT = MobjDataLayer.ExecuteDataTable(TSQL);

            if (DT.Rows.Count > 0)
            {

                if (DT.Rows[0]["CurrencyID"] != System.DBNull.Value)
                {
                    intEmployeeCurrencyID = Convert.ToInt32(DT.Rows[0]["CurrencyID"]);
                    strEmployeeCurrency = Convert.ToString(DT.Rows[0]["Currency"]);
                }
            }

            TSQL = "SELECT Count(CurrencyDetailID) AS ExchangeRateExists FROM CurrencyDetails WHERE CurrencyID = " + intEmployeeCurrencyID + " And CompanyID = " + TransferToID + "";
            DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                if (DT.Rows[0]["ExchangeRateExists"] != null)
                {
                    if (Convert.ToInt32(DT.Rows[0]["ExchangeRateExists"]) > 0)
                    {
                        iState = 1;
                    }
                }
            }

            TSQL = "SELECT C.CurrencyName AS Description FROM CompanyMaster CM INNER JOIN CurrencyReference C ON C.CurrencyID = CM.CurrencyID WHERE CM.CompanyID = " + TransferToID + "";

            DT = MobjDataLayer.ExecuteDataTable(TSQL);

            if (DT.Rows.Count > 0)
            {
                if (DT.Rows[0]["Description"] != System.DBNull.Value)
                {
                    strCompanyCurrency = Convert.ToString(DT.Rows[0]["Description"]);
                }
            }

            return true;

        }
        /// <summary>
        /// For SalaryProcess
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <param name="strTransferDatem"></param>
        /// <param name="GlUserId"></param>
        /// <returns>bool</returns>
        public bool SalaryProcess(int EmployeeID, string strTransferDatem, int GlUserId)
        {
            try
            {
                int ParaCompanyID;
                string ParaProcessDateFrom = "";
                string ParaProcessDateTo = "";
                int ParaEmployeeID = 0;

                DataTable DT;


                string TSQL; int intSalaryDay; int intTransferDay;

                DateTime PreviousMonthDate;

                ParaCompanyID = 0;
                ParaEmployeeID = EmployeeID;

                PreviousMonthDate = DateAndTime.DateAdd(DateInterval.Month, -1, Convert.ToDateTime(strTransferDatem));

                TSQL = "SELECT ISNULL(SalaryDay,1) AS SalaryDay FROM PaySalaryStructure WHERE EmployeeID=" + EmployeeID + "";
                DT = MobjDataLayer.ExecuteDataTable(TSQL);

                if (DT.Rows.Count > 0)
                {
                    intSalaryDay = Convert.ToInt32(DT.Rows[0]["SalaryDay"]);
                }
                else
                {
                    intSalaryDay = 1;
                }


                if (intSalaryDay < 1)
                {
                    intSalaryDay = 1;
                }

                intTransferDay = Convert.ToDateTime(strTransferDatem).Day;
                if (intSalaryDay <= intTransferDay)
                {
                    ParaProcessDateFrom = intSalaryDay.ToString() + "-" + GetCurrentMonth(DateAndTime.Month(Convert.ToDateTime(strTransferDatem))) + "-" + DateAndTime.Year(Convert.ToDateTime(strTransferDatem)) + "";
                }
                else
                {
                    ParaProcessDateFrom = intSalaryDay.ToString() + "-" + GetCurrentMonth(DateAndTime.Month(PreviousMonthDate)) + "-" + DateAndTime.Year(PreviousMonthDate) + "";
                }

                ParaProcessDateTo = strTransferDatem;

                ArrayList parameters = new ArrayList();
                parameters.Add(new SqlParameter("@CompanyID", ParaCompanyID));
                parameters.Add(new SqlParameter("@FromDate1", ParaProcessDateFrom));
                parameters.Add(new SqlParameter("@ToDate1", ParaProcessDateTo));
                parameters.Add(new SqlParameter("@EmployeeID", ParaEmployeeID));
                parameters.Add(new SqlParameter("@IsFromTransfer", 1));
                parameters.Add(new SqlParameter("@TransferFromDate1", ParaProcessDateTo));
                parameters.Add(new SqlParameter("@IsPartial", 0));
                parameters.Add(new SqlParameter("@UserID", GlUserId));
                parameters.Add(new SqlParameter("@AccrualSettlement", 0));
                SqlParameter sqlParam = new SqlParameter("@VacationPayAmt", SqlDbType.Float);
                sqlParam.Direction = ParameterDirection.Output;
                parameters.Add(sqlParam);
                MobjDataLayer.ExecuteNonQuery("[spPaySalaryProcess]", parameters);


                parameters = null;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// To Get Host Name
        /// </summary>
        /// <returns>string</returns>
        public string GetHostName()
        {
            return System.Net.Dns.GetHostName().ToString();
        }

        /// <summary>
        /// To update Particulars
        /// </summary>
        /// <param name="strTransferDate"></param>
        public void ParticularsUpdate(string strTransferDate)
        {
            string sQuery = "";

            sQuery = "EXEC spPayParticularsList  2, '" + strTransferDate + "','" + GetHostName() + "'," + 1 + "";
            MobjDataLayer.ExecuteQuery(sQuery);


            sQuery = "EXEC spPayParticularsList  3, '" + strTransferDate + "','" + GetHostName() + "'," + 1 + "";
            MobjDataLayer.ExecuteQuery(sQuery);
        }
        /// <summary>
        /// To set Temp Table Value
        /// </summary>
        /// <param name="intPaymentID"></param>
        /// <param name="intEmployeeID"></param>
        /// <param name="strProcessDate"></param>
        public void SetTempTableValue(int intPaymentID, int intEmployeeID, string strProcessDate)
        {
            string ComputerName; string TSQL;
            ComputerName = "";
            ComputerName = GetHostName().Trim();
            TSQL = "DELETE FROM PayTempEmployeeIDForPaymentRelease where  rtrim(ltrim(MachineName))='" + ComputerName + "'";
            MobjDataLayer.ExecuteQuery(TSQL);

            TSQL = "INSERT INTO PayTempEmployeeIDForPaymentRelease(EmployeeID,ProcessDate,MachineName,PaymentID) " +
            "Select  " + intEmployeeID + ",'" + strProcessDate + "','" + ComputerName + "'," + intPaymentID + "";
            MobjDataLayer.ExecuteQuery(TSQL);

        }
        /// <summary>
        /// For Salary Release
        /// </summary>
        /// <param name="TransferDate"></param>
        /// <param name="EmployeeID"></param>
        /// <param name="GlUserId"></param>
        public void SalaryRelease(string TransferDate, int EmployeeID, int GlUserId)
        {
            string MachineName;
            MachineName = GetHostName();

            this.ParticularsUpdate(TransferDate);

            DataTable datTemp = MobjDataLayer.ExecuteDataTable("SELECT M.CompanyID,M.TransactionTypeID,H.CurrencyID,0 AS PaymentID " +
                                                       "FROM EmployeeMaster M INNER JOIN PaySalaryStructure H ON M.EmployeeID=H.EmployeeID " +
                                                       "WHERE M.EmployeeID=" + EmployeeID + " UNION ALL " +
                                                       "SELECT 0 AS CompanyID,0 AS TransactionTypeID,0 AS CurrencyID,IsNull(MAX(PaymentID),0) FROM PayEmployeePayment " +
                                                       "WHERE EmployeeID=" + EmployeeID + " AND isnull(PaymentDate,0)=0 AND Released=0");

            SetTempTableValue(Convert.ToInt32(datTemp.Rows[1]["PaymentID"].ToString()), EmployeeID, TransferDate);

            string TSQL = "UPDATE PayEmployeePayment SET Released=1,PaymentDate='" + TransferDate + "' WHERE PaymentID=" + Convert.ToInt32(datTemp.Rows[1]["PaymentID"].ToString()) + "";
            MobjDataLayer.ExecuteQuery(TSQL);

            clsBLLSalaryPayment MobjClsBLLSalaryPayment = new clsBLLSalaryPayment();
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intCompanyID = ClsCommonSettings.CurrentCompanyID;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.strGetHostName = GetHostName();
            DataTable DT = MobjClsBLLSalaryPayment.NetAmount();
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.intUserId = ClsCommonSettings.UserID;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.Is50Percentage = 0;
            MobjClsBLLSalaryPayment.clsDTOSalaryPayment.strPaymentDate = ClsCommonSettings.GetServerDate().ToString("dd-MMM-yyyy");

            if (DT.Rows.Count > 0)
            {
                if (DT.Rows[0]["NetAmount"] != null)
                {
                    MobjClsBLLSalaryPayment.clsDTOSalaryPayment.dblNetAmount = DT.Rows[0]["NetAmount"].ToDouble();
                }
            }

            MobjClsBLLSalaryPayment.ConfirmAll();
        }

        /// <summary>
        /// Function to get Current Month
        /// </summary>
        /// <param name="MonthVal"></param>
        /// <returns></returns>
        private string GetCurrentMonth(int MonthVal)
        {
            string Months;
            Months = "";
            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }
            return Months;
        }
        /// <summary>
        /// To delete Transfered Emplyees Attendance
        /// </summary>
        /// <param name="intEmployeeID"></param>
        /// <param name="dtTransferDate"></param>
        /// <returns>bool</returns>
        public bool DeleteAttendance(int intEmployeeID, DateTime dtTransferDate)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            parameters.Add(new SqlParameter("@TransferDate1", dtTransferDate));
            return MobjDataLayer.ExecuteNonQuery("spPayEmployeeTransferTransaction", parameters).ToInt32() > 0;
        }

        public DataTable GetEmployeeInfo(int intEmployeeID)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            return MobjDataLayer.ExecuteDataTable("spPayEmployeeTransferTransaction", parameters);
        }
        /// <summary>
        /// To get Approed Date
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>string</returns>
        public string GetApprovedDate(int EmployeeID)
        {
            string TSQL = "SELECT ApprovedDate FROM HRTransferRequest WHERE EmployeeID=" + EmployeeID + " AND StatusId=5";
            DataTable DT = MobjDataLayer.ExecuteDataTable(TSQL);
            if (DT.Rows.Count > 0)
            {
                return Convert.ToString(DT.Rows[0][0]);
            }
            else
            {
                return "01-Jan-1900";
            }

        }
        /// <summary>
        /// To Check Transfer Request Exist For The Employee
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>string</returns>
        public bool IsTransferRequestExists(int EmployeeID, int intTransferTypeID)
        {  
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 15));
            alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alparameters.Add(new SqlParameter("@TransferTypeID", intTransferTypeID));
            int result = MobjDataLayer.ExecuteScalar("spPayEmployeeTransferTransaction", alparameters).ToInt32();
            if (result > 0)
                return true;
            else
                return false;

        }
    }
   

}
