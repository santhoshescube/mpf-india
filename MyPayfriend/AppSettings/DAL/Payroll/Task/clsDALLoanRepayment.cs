﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/*
 * Created By       : Tijo
 * Created Date     : 19 Aug 2013
 * Purpose          : For Loan Repayment
*/
namespace MyPayfriend
{
    public class clsDALLoanRepayment
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOLoanRepayment objclsDTOLoanRepayment { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spPayLoanRepayment";

        public clsDALLoanRepayment(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public long GetRecordCount()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOLoanRepayment.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return Convert.ToInt64(MobjDataLayer.ExecuteScalar(strProcName, parameters));
        }

        public DataTable getLoanDetails(int intLoanID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@LoanID", intLoanID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public long SaveLoanRepaymentMaster(DataTable datTemp)
        {
            if (objclsDTOLoanRepayment.lngRepaymentID > 0)
                DeleteLoanRepaymentDetails(objclsDTOLoanRepayment.lngRepaymentID, objclsDTOLoanRepayment.intLoanID);

            parameters = new ArrayList();
            if (objclsDTOLoanRepayment.lngRepaymentID == 0)
                parameters.Add(new SqlParameter("@Mode", 3)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 4)); // Update

            parameters.Add(new SqlParameter("@RepaymentID", objclsDTOLoanRepayment.lngRepaymentID));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOLoanRepayment.lngEmployeeID));
            parameters.Add(new SqlParameter("@LoanID", objclsDTOLoanRepayment.intLoanID));
            parameters.Add(new SqlParameter("@Date", objclsDTOLoanRepayment.dtDate));
            parameters.Add(new SqlParameter("@Amount", objclsDTOLoanRepayment.decAmount));
            parameters.Add(new SqlParameter("@Remarks", objclsDTOLoanRepayment.strRemarks));
            parameters.Add(new SqlParameter("@CurrencyID", objclsDTOLoanRepayment.intCurrencyID));
            parameters.Add(new SqlParameter("@CompanyAmount", objclsDTOLoanRepayment.decCompanyAmount));
            parameters.Add(new SqlParameter("@IsClosed", objclsDTOLoanRepayment.blnIsClosed));
            parameters.Add(new SqlParameter("@FromSalary", false));
            parameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objRepaymentID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objRepaymentID) != 0)
            {
                objclsDTOLoanRepayment.lngRepaymentID = objRepaymentID.ToInt64();

                if (objclsDTOLoanRepayment.lngRepaymentID > 0)
                {
                    if (datTemp != null)
                    {
                        if (datTemp.Rows.Count > 0)
                        {
                            if (SaveLoanRepaymentDetails(objclsDTOLoanRepayment.lngRepaymentID, objclsDTOLoanRepayment.intLoanID, datTemp) > 0)
                                return objclsDTOLoanRepayment.lngRepaymentID;
                        }
                    }

                    return 0;
                }
            }
            return 0;
        }

        public long getRecordRowNumber(long lngRepaymentID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@RepaymentID", lngRepaymentID));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOLoanRepayment.strSearchKey));

            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt64();
        }

        public DataTable DisplayLoanRepaymentMaster(long lngRowNum)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@RowNum", lngRowNum));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOLoanRepayment.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public bool DeleteLoanRepaymentMaster()
        {
            if (DeleteLoanRepaymentDetails(objclsDTOLoanRepayment.lngRepaymentID, objclsDTOLoanRepayment.intLoanID))
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 8));
                parameters.Add(new SqlParameter("@RepaymentID", objclsDTOLoanRepayment.lngRepaymentID));

                if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                    return true;
            }

            return false;
        }

        public DataSet GetPrintLoanRepaymentDetails(long lngRepaymentID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@RepaymentID", lngRepaymentID));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }

        public DataTable getLoanInstallmentDetails(int intLoanID, long lngRepaymentID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@LoanID", intLoanID));
            parameters.Add(new SqlParameter("@RepaymentID", lngRepaymentID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public long SaveLoanRepaymentDetails(long lngRepaymentID, int intLoanID, DataTable datTemp)
        {
            for (int iCounter = 0; iCounter <= datTemp.Rows.Count - 1; iCounter++)
            {
                parameters = new ArrayList();
                parameters.Add(new SqlParameter("@Mode", 11));
                parameters.Add(new SqlParameter("@RepaymentID", lngRepaymentID));
                parameters.Add(new SqlParameter("@LoanID", intLoanID));
                parameters.Add(new SqlParameter("@InstalmentNo", datTemp.Rows[iCounter]["InstalmentNo"].ToInt32()));
                parameters.Add(new SqlParameter("@PrincipleAmount", datTemp.Rows[iCounter]["PrincipleAmount"].ToDecimal()));
                MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters);
            }

            return lngRepaymentID;
        }

        public bool DeleteLoanRepaymentDetails(long lngRepaymentID, int intLoanID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 12));
            parameters.Add(new SqlParameter("@RepaymentID", lngRepaymentID));
            parameters.Add(new SqlParameter("@LoanID", intLoanID));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                return true;

            return false;
        }

        public int CheckLoanRepaymentExists(long lngRepaymentID, int intLoanID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@RepaymentID", lngRepaymentID));
            parameters.Add(new SqlParameter("@LoanID", intLoanID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public DateTime getLastLoanRepaymentDate(long lngRepaymentID, int intLoanID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@RepaymentID", lngRepaymentID));
            parameters.Add(new SqlParameter("@LoanID", intLoanID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToDateTime();
        }

        public int CheckSalaryProcessedAgainstEmployee(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 13));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public int CheckSettlementProcessed(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 19));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }


    }
}
