﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/*
 * Created By       : Tijo
 * Created Date     : 14 Aug 2013
 * Purpose          : For Employee Deposit
*/
namespace MyPayfriend
{    
    public class clsDALSalaryAdvance
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTOSalaryAdvance objclsDTOSalaryAdvance { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spPaySalaryAdvance";

        public clsDALSalaryAdvance(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public long GetRecordCount(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOSalaryAdvance.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            return Convert.ToInt64(MobjDataLayer.ExecuteScalar(strProcName, parameters));
        }

        public DataSet getSalaryStructureDetails(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }

        public int SaveSalaryAdvanceDetails()
        {
            parameters = new ArrayList();
            if (objclsDTOSalaryAdvance.intSalaryAdvanceID == 0)
                parameters.Add(new SqlParameter("@Mode", 3)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 4)); // Update

            parameters.Add(new SqlParameter("@SalaryAdvanceID", objclsDTOSalaryAdvance.intSalaryAdvanceID));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTOSalaryAdvance.lngEmployeeID));
            parameters.Add(new SqlParameter("@Date", objclsDTOSalaryAdvance.dtDate));
            parameters.Add(new SqlParameter("@Amount", objclsDTOSalaryAdvance.decAmount));
            parameters.Add(new SqlParameter("@CurrencyID", objclsDTOSalaryAdvance.intCurrencyID));
            parameters.Add(new SqlParameter("@Remarks", objclsDTOSalaryAdvance.strRemarks));
            parameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objSalaryAdvanceID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objSalaryAdvanceID) != 0)
            {
                objclsDTOSalaryAdvance.intSalaryAdvanceID = objSalaryAdvanceID.ToInt32();

                if (objclsDTOSalaryAdvance.intSalaryAdvanceID > 0)
                    return objclsDTOSalaryAdvance.intSalaryAdvanceID;
            }
            return 0;
        }

        public int getRecordRowNumber(int intSalaryAdvanceID,long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@SalaryAdvanceID", intSalaryAdvanceID));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOSalaryAdvance.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public DataTable DisplaySalaryAdvanceDetails(int intRowNum, long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@RowNum", intRowNum));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTOSalaryAdvance.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            parameters.Add(new SqlParameter("EmployeeID", lngEmployeeID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public bool DeleteSalaryAdvanceDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@SalaryAdvanceID", objclsDTOSalaryAdvance.intSalaryAdvanceID));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                return true;

            return false;
        }

        public DataSet GetPrintSalaryAdvanceDetails(int intSalaryAdvanceID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@SalaryAdvanceID", intSalaryAdvanceID));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }

        public DataTable getTotalSalaryAdvance(long lngEmployeeID, DateTime dtFromDate, DateTime dtToDate)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            parameters.Add(new SqlParameter("@FromDate", dtFromDate));
            parameters.Add(new SqlParameter("@ToDate", dtToDate));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public long CheckEmployeeSalaryIsProcessed(long lngEmployeeID, DateTime dtDate)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            parameters.Add(new SqlParameter("@Date", dtDate));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt64();
        }
    }
}
