﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/*
 * Created By       : Tijo
 * Created Date     : 14 Aug 2013
 * Purpose          : For Employee Deposit
*/
namespace MyPayfriend
{
    public class clsDALDeposit
    {
        public ClsCommonUtility objClsCommonUtility { get; set; }
        public clsDTODeposit objclsDTODeposit { get; set; }
        public DataLayer MobjDataLayer { get; set; }
        ArrayList parameters;
        string strProcName = "spPayDeposit";

        public clsDALDeposit(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
            objClsCommonUtility = new ClsCommonUtility();
            objClsCommonUtility.PobjDataLayer = MobjDataLayer;
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return objClsCommonUtility.FillCombos(saFieldValues);
        }

        public long GetRecordCount()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTODeposit.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return Convert.ToInt64(MobjDataLayer.ExecuteScalar(strProcName, parameters));
        }

        public DataTable DisplayDepositDetails(long lngRowNum)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@RowNum", lngRowNum));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTODeposit.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
           // parameters.Add(new SqlParameter("@CompanyID", ClsCommonSettings.CurrentCompanyID));
            return MobjDataLayer.ExecuteDataTable(strProcName, parameters);
        }

        public long SaveDepositDetails()
        {
            parameters = new ArrayList();
            if (objclsDTODeposit.lngDepositID == 0)
                parameters.Add(new SqlParameter("@Mode", 3)); // Insert
            else
                parameters.Add(new SqlParameter("@Mode", 4)); // Update

            parameters.Add(new SqlParameter("@DepositID", objclsDTODeposit.lngDepositID));
            parameters.Add(new SqlParameter("@DepositNo", objclsDTODeposit.lngDepositNo));
            parameters.Add(new SqlParameter("@EmployeeID", objclsDTODeposit.lngEmployeeID));
            parameters.Add(new SqlParameter("@DepositTypeID", objclsDTODeposit.intDepositTypeID));
            parameters.Add(new SqlParameter("@DepositAmount", objclsDTODeposit.decDepositAmount));
            parameters.Add(new SqlParameter("@CurrencyID", objclsDTODeposit.intCurrencyID));
            parameters.Add(new SqlParameter("@DepositDate", objclsDTODeposit.dtDepositDate));
            parameters.Add(new SqlParameter("@CompanyAmount", objclsDTODeposit.decCompanyAmount));

            parameters.Add(new SqlParameter("@IsRefundable", objclsDTODeposit.blnIsRefundable));

            if (objclsDTODeposit.blnIsRefundable)
                parameters.Add(new SqlParameter("@RefundableDate", objclsDTODeposit.dtRefundableDate));

            parameters.Add(new SqlParameter("@IsRefunded", objclsDTODeposit.blnIsRefunded));

            if (objclsDTODeposit.blnIsRefunded)
                parameters.Add(new SqlParameter("@RefundDate", objclsDTODeposit.dtRefundDate));

            parameters.Add(new SqlParameter("@Remarks", objclsDTODeposit.strRemarks));
            parameters.Add(new SqlParameter("@CreatedBy", ClsCommonSettings.UserID));

            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objDepositID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objDepositID) != 0)
            {
                objclsDTODeposit.lngDepositID = objDepositID.ToInt64();

                if (objclsDTODeposit.lngDepositID > 0)
                    return objclsDTODeposit.lngDepositID;
            }
            return 0;
        }

        public bool DeleteDepositDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 5));
            parameters.Add(new SqlParameter("@DepositID", objclsDTODeposit.lngDepositID));

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters) != 0)
                return true;

            return false;
        }

        public DataSet GetPrintDepositDetails(long lngDepositID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@DepositID", lngDepositID));
            return MobjDataLayer.ExecuteDataSet(strProcName, parameters);
        }

        public long UpdateDepositRefundDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 7));
            parameters.Add(new SqlParameter("@DepositID", objclsDTODeposit.lngDepositID));
            parameters.Add(new SqlParameter("@IsRefunded", objclsDTODeposit.blnIsRefunded));
            SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
            objParam.Direction = ParameterDirection.ReturnValue;
            parameters.Add(objParam);

            object objDepositID = null;

            if (MobjDataLayer.ExecuteNonQueryWithTran(strProcName, parameters, out objDepositID) != 0)
            {
                objclsDTODeposit.lngDepositID = objDepositID.ToInt64();

                if (objclsDTODeposit.lngDepositID > 0)
                    return objclsDTODeposit.lngDepositID;
            }
            return 0;
        }

        public long getRecordRowNumber(long lngDepositID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 8));
            parameters.Add(new SqlParameter("@DepositID", lngDepositID));
            parameters.Add(new SqlParameter("@SearchKey", objclsDTODeposit.strSearchKey));
            parameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt64();
        }

        public int CheckEmployeeExistance(long lngEmployeeID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToInt32();
        }

        public string getAttchaedDocumentCount(long lngDepositID)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 10));
            parameters.Add(new SqlParameter("@DepositID", lngDepositID));
            return MobjDataLayer.ExecuteScalar(strProcName, parameters).ToString();
        } 
    }
}
