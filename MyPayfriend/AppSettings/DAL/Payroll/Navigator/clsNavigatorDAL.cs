﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{

    public class clsNavigatorDAL
    {

        #region Controls Function

        /// <summary>
      ///  Get alert data based on the alert id
      /// </summary>
        public DataTable GetAlertData(Int32 intAlertID)
        {
            return new DataLayer().ExecuteDataTable("GaExpiryInfoSummaryUser", new List<SqlParameter>()
            { 
                new SqlParameter("@TYP","AD"),
                new SqlParameter("@AlertID", intAlertID)
            });
        }

        /// <summary>
        /// Get all the attached documents of the document
        /// </summary>
        public static DataTable GetAttachedDocumentsByDocumentID(int DocumentTypeID, int DocumentID)
        {
            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GASD"),
                new SqlParameter("@DocumentTypeID", DocumentTypeID),
                new SqlParameter("@DocumentID", DocumentID )
            });
        }

        /// <summary>
        /// Get all the documents based on the operation type (company or employee)
        /// </summary>
        public static DataTable GetAllDocumentByOperationType(int OperationTypeID, int EmployeeID)
        {
            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","GAD"),new SqlParameter("@OperationTypeID",OperationTypeID ),
            new SqlParameter("@EmployeeID",EmployeeID )
            });
        }

        /// <summary>
        /// Search documents 
        /// </summary>
        public static DataTable SearchDocumentNavigator(int OperationTypeID, int EmployeeID, string DocumentNumber)
        {
            List<SqlParameter> Param = new List<SqlParameter>();
            Param.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            Param.Add(new SqlParameter("@Mode", "GAD"));
            Param.Add(new SqlParameter("@EmployeeID", EmployeeID));
            Param.Add(new SqlParameter("@DocumentNumber", DocumentNumber));
            return new DataLayer().ExecuteDataTable("spNavigator", Param);
        }

        /// <summary>
        /// Get all companies and branch
        /// </summary>
        public static DataTable GetAllCompaniesAndBranch()
        {
            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() 
                { 
                  new SqlParameter("@TypeID",1),
                  new SqlParameter("@UserID",ClsCommonSettings.UserID)
                });
        }

        /// <summary>
        /// Get all companies document types
        /// </summary>
        public static DataTable GetAllCompaniesDocumentType()
        {
            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() 
                { 
                  new SqlParameter("@TypeID",2)
                });
        }

        /// <summary>
        /// Get all document based on the document typeid and company id
        /// </summary>
        public static DataTable GetAllDocuments(int DocumentTypeID, int CompanyID)
        {
            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() 
                { 
                   new SqlParameter("@DocumentTypeID",DocumentTypeID ),
                   new SqlParameter("@TypeID",3),
                   new SqlParameter("@CompanyID",CompanyID)

                });
        }

        /// <summary>
        /// Get all employees by company id
        /// </summary>
        public static DataTable GetAllEmployeesByCompanyID(int CompanyID, string SearchKey, int WorkStatus, eFilterTypes CurrentFilterType, int FilterBy)
        {
            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() 
                { 
                   new SqlParameter("@TypeID",4),
                   new SqlParameter("@CompanyID",CompanyID),
                   new SqlParameter("@SearchKey",SearchKey),
                   new SqlParameter("@WorkStatusID",WorkStatus),
                   new SqlParameter("@CurrentFilterType",(int)CurrentFilterType),
                   new SqlParameter("@FilterBy",FilterBy),

                });
        }
        /// <summary>
        /// Get employee information web page
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public static  DataSet GetEmployeeReport(int EmployeeID)
        {
            return  new DataLayer().ExecuteDataSet("spEmployee", new List<SqlParameter>() { 
                new SqlParameter("@Mode", 7),new SqlParameter("@EmployeeID",EmployeeID)
            });
        }
       
        /// <summary>
        /// Get all documents based on the operation type id 
        /// </summary>
        public static DataTable  GetAllDocumentsByOperationTypeID(int OperationTypeID,int ReferenceID,int DocumentTypeID,string DocumentNumber,string SearchKey)
        {
            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@TypeID", 5)
               ,new SqlParameter("@ReferenceID",ReferenceID )
               ,new SqlParameter("@OperationTypeID",OperationTypeID)
               ,new SqlParameter("@DocumentNumber",DocumentNumber)
               ,new SqlParameter("@DocumentTypeID",DocumentTypeID)
               ,new SqlParameter("@SearchKey",SearchKey)
                ,new SqlParameter("@UserID",ClsCommonSettings.UserID)
            });
        }

        /// <summary>
        /// Get all document types based on the operation type id 
        /// </summary>
        public static DataTable GetAllDocumentTypesByOperationTypeID(int OperationTypeID, int ReferenceID, int DocumentTypeID, string DocumentNumber,string SearchKey)
        {
            return GetAllDocumentsByOperationTypeID(OperationTypeID, ReferenceID, DocumentTypeID, DocumentNumber,SearchKey);
        }

        /// <summary>
        /// Get all the attached documents 
        /// </summary>
        public static DataTable GetAttachedDocuments(int OperationTypeID, int ReferenceID, int DocumentTypeID, int DocumentID)
        {

            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@TypeID", 7)
               ,new SqlParameter("@ReferenceID",ReferenceID )
               ,new SqlParameter("@OperationTypeID",OperationTypeID)
               ,new SqlParameter("@DocumentID",DocumentID)
               ,new SqlParameter("@DocumentTypeID",DocumentTypeID)
            });

        }

        /// <summary>
        /// AutoComplete suggestions for employee
        /// </summary>
        public static DataTable GetAutoCompleteSuggestionsForEmployee(int CompanyID)
        {
            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@TypeID", 9)
               ,new SqlParameter("@CompanyID",CompanyID )
               
            });

        }
        #endregion

        #region WebPage Functions

        /// <summary>
        /// Get leave structure web page
        /// </summary>
        public static DataSet GetLeaveStructureWebPage(int EmployeeID)
        {
            return new DataLayer().ExecuteDataSet("spNavigator", new List<SqlParameter>() { 
                new SqlParameter("@Mode","LSP"),new SqlParameter("@ParentID", EmployeeID)
               
            });
        }

        /// <summary>
        /// Get work policy web page
        /// </summary>
        public static DataSet GetWorkPolicyWebPage(int EmployeeID)
        {
            return new DataLayer().ExecuteDataSet("spPayWorkPolicy", new List<SqlParameter>() { 
                new SqlParameter("@Mode","WP"),new SqlParameter("@PolicyID", 0),
            new SqlParameter("@EmployeeID",EmployeeID)});

        }

        /// <summary>
        /// Get leave policy web page
        /// </summary>
         public static DataSet GetLeavePolicyWebPage(int EmployeeID)
        {
            return new DataLayer().ExecuteDataSet("spPayLeavePolicy", new List<SqlParameter>() {  
 
                new SqlParameter("@Mode",20),new SqlParameter("@LeavePolicyID", 0),
            new SqlParameter("@EmployeeID", EmployeeID)});


        }

        /// <summary>
        /// Get employee vacation web page
        /// </summary>
         public static DataSet GetVacationPolicyWebPage(int EmployeeID)
        {
            return new DataLayer().ExecuteDataSet("spPayVacationPolicy", new List<SqlParameter>() {  
 
                new SqlParameter("@Mode",24),
            new SqlParameter("@EmployeeID", EmployeeID)});

        }

        /// <summary>
        /// Get all policies assigned to the employee
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public static DataTable  GetAllExistingPolicie(int EmployeeID)
        {
            return new DataLayer().ExecuteDataTable("spNavigator", new List<SqlParameter>() {  
 
                new SqlParameter("@TypeID",8),
            new SqlParameter("@EmployeeID", EmployeeID)});

        }

        #endregion
    }
}
