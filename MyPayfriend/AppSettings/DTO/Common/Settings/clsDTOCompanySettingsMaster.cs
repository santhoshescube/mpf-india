﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOCompanySettingsMaster
    {

        public int iCompanyID { get; set; }
        public List<clsCompanySettingsDetails> lstCompanySettingsDetails = new List<clsCompanySettingsDetails>();


    }
    public class clsCompanySettingsDetails
    {
        public int bIsAuto { get; set; }
        public int iFormID { get; set; }
        public Int64 iStartNumber { get; set; }
        public string strPreFix { get; set; }
        public Int64 NoLength { get; set; }
    }

}
