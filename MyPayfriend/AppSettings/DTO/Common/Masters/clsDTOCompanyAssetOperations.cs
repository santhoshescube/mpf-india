﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
 * Created By       : Tijo
 * Created Date     : 27 Aug 2013
 * Purpose          : For Asset Handover
*/
namespace MyPayfriend
{
    public class clsDTOCompanyAssetOperations
    {
        public int intEmpBenefitID { get; set; }
        public int intParentID { get; set; }
        public long lngEmployeeID { get; set; }
        public int intBenefitTypeID { get; set; }
        public int intCompanyAssetID { get; set; }
        public DateTime dtWithEffectDate { get; set; }
        public DateTime dtExpectedReturnDate { get; set; }
        public bool blnIsExpectedReturnDate { get; set; }
        public DateTime dtReturnDate { get; set; }
        public int intAssetStatusID { get; set; }
        public string strRemarks { get; set; }
        public int intCreatedBy { get; set; }
        public string strSearchKey { get; set; }

        public List<clsDTOEmployeeBenefitDetail> IlstclsDTOEmployeeBenefitDetail { get; set; }
    }

    public class clsDTOEmployeeBenefitDetail
    {
        public int intSerialNo { get; set; }
        public int intAssetUsageTypeID { get; set; }
        public DateTime dtDate { get; set; }
        public string strRemarks { get; set; }
        public decimal  decAmount { get; set; }
    }
}


   