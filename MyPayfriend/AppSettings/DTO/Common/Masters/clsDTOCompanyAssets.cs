﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOCompanyAssets
{

public int intCompanyAssetID { get; set; }
public int intBenefitTypeID { get; set; }
public string strDescription { get; set; }
public string strPurchaseDate { get; set; }
public string strExpiryDate { get; set; }
public double dblPurchaseValue { get; set; }
public double dblDepreciation { get; set; }
public string strRemarks { get; set; }
public int intStatus { get; set; }
public int intCompanyID { get; set; }
public string strRefeNo { get; set; }


  }
}
