﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOWorkLocation
    {
        public int lngWorkLocationID { get; set; }
        public int intCompanyID { get; set; }
        public string strLocationName { get; set; }
        public string strAddress { get; set; }
        public string strPhone { get; set; }
        public string strEmail { get; set; }
    }

}