﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,21 Feb 2011>
Description:	<Description,,DTO for EmployeeInformation>
================================================
*/
namespace MyPayfriend
{
    public class clsDTOEmployeeInformation
    {
        public string strPersonID { get; set; }
        public long intEmployeeID { get; set; }
        public int intCompanyID { get; set; }
        public int intMaritalStatusID { get; set; }
        public int intSalutation { get; set; }
        public int intNationalityID { get; set; }
        public int intReligionID { get; set; }
        public int intDepartmentID { get; set; }
        public int intDivisionID { get; set; }
        public int intDesignationID { get; set; }
        public int intEmploymentType { get; set; }
        public int intWorkStatusID { get; set; }
        public int intPolicyID { get; set; }
        public int intNoticeperiod { get; set; }
        public int intAccountID { get; set; }
        public int intLeavePolicyID { get; set; }
        public int intWorkLocationID { get; set; }
        public int intMothertongueID { get; set; }
        public string strEmployeeNumber { get; set; }
        public string strFirstName { get; set; }
        public string strMiddleName { get; set; }
        public string strLastName { get; set; }
        public string strEmployeeFullName { get; set; }
        public bool blnGender { get; set; }
        public string strFathersName { get; set; }
        public string strLanguagesKnown { get; set; }
        public string strIdentificationMarks { get; set; }

        public string strAddressLine1 { get; set; }
        public string strAddressLine2 { get; set; }
        public string strCity { get; set; }
        public string strStateProvinceRegion { get; set; }
        public string strZipCode { get; set; }

        public string strPermanentPhone { get; set; }
        public string strLocalMobile { get; set; }
        public string strEmail { get; set; }
        public string strMothersName { get; set; }
        public string strSpouseName { get; set; }
        public string strOfficialEmail { get; set; }
        public string strNotes { get; set; }
        public string strBloodGroup { get; set; }
        public string strDeviceUserID { get; set; }
        public string strTransferDate { get; set; }
        public string strRejoinDate { get; set; }
        public string strNextAppraisalDate { get; set; }
        public byte[] strRecentPhotoPath { get; set; }
        public byte[] strRecentPhotoPathThamNail { get; set; }

        public DateTime dtDateofBirth { get; set; }
        public DateTime dtDateofJoining { get; set; }
        public DateTime dtProbationEndDate { get; set; }
        public DateTime dtPerformanceReviewDate { get; set; }
        // Transaction type

        public int intTransactionTypeID { get; set; }
        public int intBankNameID { get; set; }
        public int intBankBranchID { get; set; }
        public string strAccountNumber { get; set; }
        public int intComBankAccId { get; set; }
        public int intVisaObtained { get; set; }
        public string strCurrentWorklocation { get; set; }
        public int intVacationPolicyID { get; set; }

        public long ReportingTo { get; set; }

        public string strFirstNameArb {get;set;}
        public string strMiddleNameArb { get; set; }
        public string strLastNameArb { get; set; }
        public string strEmployeeFullNameArb { get; set; }

        public int intCountryID  {get;set;}

        public string strEmergencyAddress { get; set; }
        public string strEmergencyPhone { get; set; }

        public string strLocalAddress { get; set; }
        public string strLocalPhone { get; set; }
        public int intGradeID { get; set; }
        public int intHODID { get; set; }
        public int NoticePeriod { get; set; }
        public int CommissionStructureID { get; set; }
        public int ProcessTypeID { get; set; } 
        public List<clsDTOLanuages> DTOLanuages { get; set; }


      

    }
    public class clsDTOLanuages
    {
        public int LanguageID { get; set; }
    }
}
