﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,28 Feb 2011>
Description:	<Description,,DTO for ConfigurationSettings>
 * 
 * 
 * Modified By :Rajesh
 * Modified On :12-Aug-2013
================================================
*/
namespace MyPayfriend
{
    public class clsDTOConfigurationSettings
    {
        public int ConfigurationID { get; set; }
        public string ConfigurationValue { get; set; }
    }



}
