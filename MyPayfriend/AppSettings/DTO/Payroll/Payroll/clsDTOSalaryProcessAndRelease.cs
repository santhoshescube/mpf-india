﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOSalaryProcessAndRelease
    {
        public List<clsEDSSIFDetails> lstClsEDSSIFDetails { get; set; }
    }
    public class clsEDSSIFDetails
    {
        public int EDRID { get; set; }
        public int SIFID { get; set; }
        public string RecordType { get; set; }
        public string PersonID { get; set; }
        public string AgentID { get; set; }
        public string EmpAccNo { get; set; }
        public string PayStartDate { get; set; }
        public string PayEndDate { get; set; }
        public int WorkDays { get; set; }
        public decimal FixedIncome { get; set; }
        public decimal VariableIncome { get; set; }
        public decimal LOPDays { get; set; }
    }
}
