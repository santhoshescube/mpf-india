﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
   public class clsDTOEmployeePayList
    {
        public int intPaymentID { get; set; }
        public int intEmployeeID { get; set; }
        public string strProcessDate { get; set; }
        public string strMachineName { get; set; }
    }
}
