﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOSalaryTemplate
    {
       public  int SalaryTempId{get;set;}
       public string SalaryTempName { get; set; }
       public decimal SalaryTempGrossAmount { get; set; }
       public int OTPolicyID { get; set; }
       public int AbsentPolicyID{get;set;}
       public int HolidayPolicyID{get;set;}
       public int EncashPolicyID{get;set;}
       public int SetPolicyID { get; set; }
       public List<clsDTOSalaryTemplateDetails> lstclsDTOSalaryTemplateDetails { get; set; }
       public List<clsDTOSalaryTemplateDeduction> lstclsDTOSalaryTemplateDeduction { get; set; }
          
    }
    public class clsDTOSalaryTemplateDetails
    {
  
        public int SalaryTempParticularsID { get; set; }
        public decimal SalaryTempPercentage { get; set; }
       
      
    }
    public class clsDTOSalaryTemplateDeduction
    {
        public int SalaryTempParticularsDedID { get; set; }
        public int PolicyID { get; set; }
        public decimal Amount { get; set; }
       
    }
    
}
