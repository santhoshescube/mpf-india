﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOGLCodeMapping
    {
        public List<clsDTOAdditionDeductionDetails> AdditionDeductionDetails { get; set; }
    }

    public class clsDTOAdditionDeductionDetails
    {
        public int AdditionDeductionID { get; set; }
        public string Code { get; set; }
        public string PRCode { get; set; }
    }
}
