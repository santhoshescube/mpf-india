﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOAddParicularsToAll
    {

        public int AmendmentCodeID { get; set; }

        public string AmendmentCode { get; set; }

        public int StatusID { get; set; }

        public int AmendmentStatus { get; set; }

        public List<clsDTOAddParicularsToAllDetail> DTOAddParicularsToAllDetail { get; set; }

        public List<clsDTOSalaryAmendmentDetail> DTOSalaryAmendmentDetail { get; set; }

        public List<clsDTOAccuralsDetail> DTOAccuralDetail { get; set; }
       
    }
    public class clsDTOAddParicularsToAllDetail
    {
        public int PaymentID { get; set; }
        public int CurrencyID { get; set; }
        public decimal Amount { get; set; }
        public int IsAddition { get; set; }
        public int AddDedID { get; set; }

    }

    public class clsDTOSalaryAmendmentDetail
    {
        public int intEmployee { get; set; }

        public string strEffectiveDate { get; set; }

        public int intRepetitive { get; set; }

        public int intParticular { get; set; }

        public Decimal intAmount { get; set; }

        public string strRemarks { get; set; }
    }

    public class clsDTOAccuralsDetail
    {
        public int intEmployee { get; set; }

        public string dtMonthdate { get; set; }

        public int intGratuvityAmt { get; set; }

        public int intLeavepassage { get; set; }

        public Decimal intGratuvityDays { get; set; }

        public string intCompanyid { get; set; }
    }
}
