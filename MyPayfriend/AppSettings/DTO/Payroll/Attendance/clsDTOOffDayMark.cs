﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOOffDayMark
    {
        public int intCompanyID { get; set; }
        public int intEmployeeID { get; set; }
        public string strRosterDate { get; set; }
        public int intRosterStatusID { get; set; }
        public int intDepartmentID { get; set; }

    }
}
