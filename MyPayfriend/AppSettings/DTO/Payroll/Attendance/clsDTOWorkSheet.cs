﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
   public class clsDTOWorkSheet
    {
       public int iCompanyID { get; set; }
       public int iDepartmentID { get; set; }
       public string strFromDate { get; set; }
       public IList<clsDTOWorkSheetDetail> lstDTOWorkSheetDetail { get; set; }
    }

   public class clsDTOWorkSheetDetail
   {
       public int iEmployeeID { get; set; }
       public double dblWorkedDays { get; set; }
       public decimal decAbsentDays { get; set; }
       public decimal decOTHour { get; set; }
       public decimal decTotalDays { get; set; }
       public string  strRemarks { get; set; }
       public string strToDate{ get; set; }
       public string strDateRange { get; set; }
       public int iCompanyID { get; set; }
       public int iDepartmentID { get; set; }
       public string strFromDate { get; set; }
       public decimal decHolidayWorked { get; set; }
       public string strDateOfJoining { get; set; }
   }

}

