﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class clsDTOMapping
    {

        // AttendanceTemplateMaster

        public int TemplateMasterID { get; set; }
        public string  TemplateName { get; set; }
        public bool HeaderExists { get; set; }
        public string  Punchingcount { get; set; }
        public string Dateformat { get; set; }
        public bool Timewithdate { get; set; }
        public int intFileTypeID { get; set; }
        public string strUserName { get; set; }
        public string strPassword { get; set; }
        // AttendanceTemplatedetail

        public string Templatefield { get; set; }
        public string Actualfield { get; set; }


    }
}
