﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOEmployeeLoan
    {

        public int intLoanID { get; set; }
        public string strLoanNumber { get; set; }
        public int intEmployeeID { get; set; }
        public int intLoanTypeID { get; set; }
        public DateTime dtLoanDate { get; set; }
        public decimal decAmount { get; set; }
        public int intInterestTypeID { get; set; }
        public decimal decInterestRate { get; set; }
        public decimal decPaymentAmount { get; set; }
        public DateTime dtDeductStartDate { get; set; }
        public DateTime dtDeductEndDate { get; set; }
        public int intNoOfInstallments{get;set;}
        public int intCurrencyID { get; set; }
        public string strReason { get; set; }
        public int intCompanyID { get; set; }
        public decimal decCompanyAmount { get; set; }
        public bool blnClosed { get; set; }
        public DateTime dtClosedDate { get; set; }
        public int intClosedBy { get; set; }
        public decimal decBalanceAmount { get; set; }

        public List<clsDTOEmployeeLoanInstallmentDetails> objClsDTOEmployeeLoanInstallmentDetails { get; set; }

        public clsDTOEmployeeLoan()
        {
            objClsDTOEmployeeLoanInstallmentDetails = new List<clsDTOEmployeeLoanInstallmentDetails>();
        }

    }

    public class clsDTOEmployeeLoanInstallmentDetails
    {
        //public string strInstallmentNo{get;set;}
        public int InstallmentNo { get; set; }
        public DateTime dtInstallmentDate{get;set;}
        public decimal decAmount{get;set;}
        public decimal decInterestAmount{get;set;}
        public bool blnIsPaid { get; set; }
    }
}
