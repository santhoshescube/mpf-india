﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
 * Created By       : Tijo
 * Created Date     : 14 Aug 2013
 * Purpose          : For Employee Deposit
*/
namespace MyPayfriend
{
    public class clsDTODeposit
    {
        public long lngDepositID { get; set; }
        public long lngDepositNo { get; set; }
        public long lngEmployeeID { get; set; }
        public int intDepositTypeID { get; set; }
        public decimal decDepositAmount { get; set; }
        public int intCurrencyID { get; set; }
        public DateTime dtDepositDate { get; set; }
        public decimal decCompanyAmount { get; set; }
        public bool blnIsRefundable { get; set; }
        public DateTime dtRefundableDate { get; set; }
        public bool blnIsRefunded { get; set; }
        public DateTime dtRefundDate { get; set; }
        public string strRemarks { get; set; }
        public int intCreatedBy { get; set; }
        public string strSearchKey { get; set; }
    }
}
