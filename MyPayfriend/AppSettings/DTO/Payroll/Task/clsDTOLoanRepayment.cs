﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
 * Created By       : Tijo
 * Created Date     : 19 Aug 2013
 * Purpose          : For Loan Repayment
*/
namespace MyPayfriend
{
    public class clsDTOLoanRepayment
    {
        public long lngRepaymentID { get; set; }
        public long lngEmployeeID { get; set; }
        public int intLoanID { get; set; }
        public DateTime dtDate { get; set; }
        public decimal decAmount { get; set; }
        public bool blnFromSalary { get; set; }
        public string strRemarks { get; set; }
        public int intCurrencyID { get; set; }
        public decimal decPaymentID { get; set; }
        public decimal decCompanyAmount { get; set; }
        public string strSearchKey { get; set; }
        public bool blnIsClosed { get; set; }
    }
}
