﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOEmployeeExpense
    {
        public long lngEmployeeExpenseID { get; set; }
        public long lngExpenseNo { get; set; }
        public long lngEmployeeID { get; set; }
        public int intExpenseHeadID { get; set; }
        public DateTime dtExpenseFromDate { get; set; }
        public DateTime dtExpenseToDate { get; set; }
        public DateTime dtExpenseDate { get; set; }
        public decimal decExpenseAmount { get; set; }
        public int intExpenseTransID { get; set; }
        public decimal decActualAmount { get; set; }
        public int intCurrencyID { get; set; }
        public string strRemarks { get; set; }
        public string strSearchKey { get; set; }
    }
}
