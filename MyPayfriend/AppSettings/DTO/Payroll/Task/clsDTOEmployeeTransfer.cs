﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOEmployeeTransfer
    {
      public int TransferID { get; set; }
      public int TransferTypeID  { get; set; }
      public int EmployeeID { get; set; }
      public int TransferFromID { get; set; }
      public int TransferToID { get; set; }
      public string OtherInfo { get; set; }
      public DateTime TransferDate { get; set; }
      public int IsFromRequest { get; set; }
      public int RequestId { get; set; }  

      public int TransactionTypeID { get; set; }
      public int PolicyID { get; set; }
      public int LeavePolicyID { get; set; }
      public int EmployerBankID { get; set; }
      public int EmployerAccountID { get; set; }
      public int EmployeeBankID { get; set; }
      public string  EmployeeAccountNo { get; set; }
      public string MachineName { get; set; }
      public DateTime FinYearStartDate { get; set; }
      public int CompanyID { get; set; }
      public int DepartmentID { get; set; }
      public int DesignationID { get; set; }
      public int EmploymentTypeID { get; set; }
      public int LocationID { get; set; }
      public int iTemp { get; set; }
    }
}
