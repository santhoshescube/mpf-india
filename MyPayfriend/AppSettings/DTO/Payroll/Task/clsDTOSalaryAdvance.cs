﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    /*********************************************
      * Author       : Arun
      * Created On   : 12 Apr 2012
      * Purpose      : Salary Advance Properties 
      * ******************************************/
    public class clsDTOSalaryAdvance
    {
        public int intSalaryAdvanceID { get; set; }
        public long lngEmployeeID { get; set; }
        public DateTime dtDate { get; set; }
        public decimal decAmount { get; set; }
        public int intCurrencyID { get; set; }
        public string strRemarks { get; set; }
        public bool blnIsSettled { get; set; }
        public int intCreatedBy { get; set; }
        public string strSearchKey { get; set; }
    }
}
