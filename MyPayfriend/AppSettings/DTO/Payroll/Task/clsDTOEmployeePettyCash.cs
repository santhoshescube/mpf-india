﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOEmployeePettyCash
    {        
        public long lngPettyCashID { get; set; }
        public long lngEmployeeID { get; set; }
        public string strPettyCashNo { get; set; }
        public DateTime dtDate { get; set; }
        public decimal decAmount { get; set; }
        public decimal decActualAmount { get; set; }
        public string strEmployeeRemarks { get; set; }
        public string strDescription { get; set; }
    }
}
