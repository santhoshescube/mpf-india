﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{

/*==============================================
Description :	     <Document Transactions>
Modified By :		 <Arun OK>
Modified Date :      <20 June 2013>
===============================================*/

    public class clsDTOInsuranceCard
    {
        public int InsuranceCardID { get; set; }
        public long EmployeeID { get; set; }
        public string CardNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyName { get; set; }
        public int InsuranceCompanyID { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime RenewalDate { get; set; }
        public string Remarks { get; set; }
        public int? IssueID { get; set; }
        public bool IsRenewed { get; set; }
    }
}
