﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyPayfriend;



    public class clsPassport:IDisposable
    {
        /// <summary>
        /// Gets or sets Unique ID of the visa (Identity Field)
        /// </summary>
        public int? PassportID { get; set; }
        /// <summary>
        /// Gets or sets Employee ID associated with the visa
        /// </summary>
        public long? EmployeeID { get; set; }
        /// <summary>
        /// Gets or sets Visa Number
        /// </summary>
        public string NameinPassport { get; set; }
        /// <summary>
        /// Gets or sets the date on which visa is issued
        /// </summary> 
        public string PassportNumber { get; set; }
        /// <summary>
        /// Gets or sets the date on which visa is issued
        /// </summary>
        public DateTime? IssueDate { get; set; }
        /// <summary>
        /// Gets or sets the place where visa is issued
        /// </summary>
        public string PlaceofIssue	{ get; set; }
        /// <summary>
        /// Gets or sets expiry date of the visa
        /// </summary> 
        public string PlaceOfBirth { get; set; }
        /// <summary>
        /// Gets or sets expiry date of the visa
        /// </summary>
        public DateTime? ExpiryDate { get; set; }
        /// <summary>
        /// Gets or sets the countryID from where visa is issued
        /// </summary>
        public int? CountryID { get; set; }

        public int? DocumentID { get; set; }
        /// <summary>
        /// Gets or sets Renewal date of the visa
        /// </summary>
        public DateTime? RenewDate { get; set; }
        public bool PassportPagesAvailable { get; set; }
        public bool IsRenew { get; set; }
        public string OldPassportNumbers { get; set; }
        public string EntryRestrictions { get; set; }
        public DateTime? RestrictionEndDate { get; set; }
        public string ResidencePermits { get; set; }
        public string Remarks { get; set; }

       public string ReferenceNumber{get;set;}
       public int OperationTypeID {get;set;}
       public int DocumentTypeID{get;set;}

        /// <summary>
        /// Gets or sets expiry date of the visa
        /// </summary>
       // public int? MultipleEntryCountryID { get; set; }
        //public DateTime? MultipleEntryExpiryDate { get; set; }
        /// <summary>
        /// Gets or sets Remarks
        /// </summary>
        ///public string Remarks { get; set; }


        public clsPager Pager { get; set; }

        #region Dispose

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        /// <summary>
        /// This method will be called by user code.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                // check to see if object is disposed.
                if (!this.disposed)
                {
                    // dispose unmanaged resources here.
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~clsPassport()
        {
            // This dispose method will only be called at runtime.
            Dispose(false);
        }
        #endregion
    }


    

