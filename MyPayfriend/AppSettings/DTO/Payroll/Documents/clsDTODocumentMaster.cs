﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyPayfriend
{


    public class clsDTODocumentMaster
    {
        public int DocumentTypeID { get; set; }

        public string DocumentNumber { get; set; }

        public int OperationTypeID { get; set; }

        public long ReferenceID { get; set; }

        public string ReferenceNumber { get; set; }


        public DateTime IssuedDate { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public bool IsReceipted { get; set; }

        public int DocumentID { get; set; }

        public int OrderNo { get; set; }

        public short DocumentStatusID { get; set; }

        public short BinID { get; set; }

        public DateTime ReceiptIssueDate { get; set; }

        public int Custodian { get; set; }

        public int ReceiptIssueReasonID { get; set; }

        public string Remarks { get; set; }

        public DateTime? ExpectedReturnDate { get; set; }

        public int ReceivedFrom { get; set; }

        public bool IsRenew { get; set; }
    }

  
}

