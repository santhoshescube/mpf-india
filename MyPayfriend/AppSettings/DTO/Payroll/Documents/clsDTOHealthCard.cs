﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOHealthCard
    {
        public int CardID { get; set; }
        public long EmployeeID { get; set; }
        public string CardNumber { get; set; }
        public string CardPersonalIDNumber { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime RenewalDate { get; set; }
        public string Remarks { get; set; }
        public bool IsRenewed { get; set; }
    }
}
