﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOEmployeeLabourCard
    {
        public int intLabourCardID { get; set; }
        public int intEmployeeID { get; set; }
        public string strCardNumber { get; set; }
        public string strCardPersonalNumber { get; set; }
        public DateTime dtIssueDate { get; set; }
        public DateTime dtExpiryDate { get; set; }
        public DateTime dtRenewalDate { get; set; }
        public string strRemarks { get; set; }
        public bool blnIsRenewed { get; set; }
    }
}
