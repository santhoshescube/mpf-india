﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOProcessingTimeMaster
    {
        
        public int intDocumentTypeID { get; set; }
        public int intUserID { get; set; }
        public int intProcessingtime { get; set; }
        public int intReturnTime { get; set; }
       
       
        
        
       
    public List<clsDTOProcessingTimeMasterDetail> DTOProcessingTimeMasterDetail { get; set; }

    }
    public class clsDTOProcessingTimeMasterDetail
    {
        public int intProcessingID { get; set; }
        public int intProcessingTime { get; set; }

        public int intAlertInterval { get; set; }
        public int intReturnTime { get; set; }
    }
}
