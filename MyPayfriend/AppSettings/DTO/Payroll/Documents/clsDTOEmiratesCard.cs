﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOEmiratesCard
    {
        public int iRecordCount { get; set; }
        public int EmiratesID { get; set; }
        public long EmployeeID { get; set; }
        public string CardNumber { get; set; }
        public string CardPersonalIDNumber { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Remarks { get; set; }
        public bool IsRenewed { get; set; }
        public int CompanyID { get; set; }
        public DateTime dtDateOfJoining { get; set; }
        public DateTime dtRenewalDate { get; set; }
        public string EmployeeNumber { get; set; }
    }
}
