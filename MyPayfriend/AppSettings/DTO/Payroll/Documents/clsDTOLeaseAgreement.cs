﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{

    public class clsDTOLeaseAgreement
    {
        public  int      LeaseID        { get; set; }
        public  int      CompanyID      { get; set; }
        public  DateTime DocumentDate { get; set; }
        public  string   AgreementNo   { get; set; }
        public  string   Lessor         { get; set; }
        public  DateTime StartDate { get; set; }
        public  DateTime EndDate { get; set; }
        public  decimal  RentalValue    { get; set; }
        public  bool     DepositHeld    { get; set; }
        public  string   Tenant         { get; set; }
        public  string   ContactNumbers { get; set; }
        public  string   Address        { get; set; }
        public  string   LandLord       { get; set; }
        public  string   UnitNumber     { get; set; }
        public  int?      LeaseUnitTypeID { get; set; }
        public  string   BuildingNumber { get; set; }
        public  string   Location       { get; set; }
        public  string   PlotNumber     { get; set; }
        public  decimal  Total           { get; set; }
        public  string   Remarks        { get; set; }
        public  string   LeasePeriod    { get; set; }
        public bool IsRenewed { get; set; }

    }

}

