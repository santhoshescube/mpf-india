﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{

/*==============================================
Description :	     <Document Transactions>
Modified By :		 <Arun OK>
Modified Date :      <20 June 2013>
===============================================*/

    public class clsDTOInsuranceDetails
    {
        public int InsuranceID { get; set; }
        public int CompanyID { get; set; }
        public int CompanyAssetID { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyName { get; set; }
        public decimal PolicyAmount { get; set; }
        public int InsuranceCompanyID { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime RenewalDate { get; set; }
        public string Remarks { get; set; }
        public bool IsRenewed { get; set; }
    }
}
