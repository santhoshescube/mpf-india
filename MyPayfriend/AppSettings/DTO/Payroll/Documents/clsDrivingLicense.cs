﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    /****************************************************
  * Author        : Thasni
  * Creation Date : 12 Apr 2011
  * Description   : 
  ****************************************************/
   public class clsDrivingLicense:IDisposable
    {
      
        /// <summary>
        /// Unique ID of the License (Identity)
        /// </summary>
        public int ? LicenseID { get; set; }
        /// <summary>
        /// EmployeeID  
        /// </summary>
        public long ? EmployeeID { get; set; }
        /// <summary>
        /// LicenceNumber 
        /// </summary>
        public string  LicenceNumber { get; set; }
        /// <summary>
        /// Name of LicenceHolder
        /// </summary>
        public string LicenceHoldersName { get; set; }
        /// <summary>
        /// LicenceTypeID like heavy,lite etc
        /// </summary>
        public int ? LicenceTypeID { get; set; }
        /// <summary>
        ///CountryID from which the licence is issued
        /// </summary>
        public int ? LicencedCountryID { get; set; }
         /// <summary>
        /// IssueDate of license
        /// </summary>
        public DateTime ? IssueDate { get; set; }
          /// <summary>
        /// ExpiryDate of license
        /// </summary>
        public DateTime ? ExpiryDate { get; set; }

        public bool IsRenew { get; set; }

        public string Remarks { get; set; }
        public clsPager Pager { get; set; }
        public int? DocumentID { get; set; }
        public string ReferenceNumber { get; set; }
        public int OperationTypeID { get; set; }
        public int DocumentTypeID { get; set; }
        /// <summary>
      
      
      
       #region Dispose

        // Track whether Dispose method has already been called.
        private bool disposed = false;

        /// <summary>
        /// This method will be called by user code.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                // check to see if object is disposed.
                if (!this.disposed)
                {
                    // dispose unmanaged resources here.
                }
            }
            this.disposed = true;
        }

        #endregion

        #region Destructor
        ~clsDrivingLicense()
        {
            // This dispose method will only be called at runtime.
            Dispose(false);
        }
        #endregion
    }
}
