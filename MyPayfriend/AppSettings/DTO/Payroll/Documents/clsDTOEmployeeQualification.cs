﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOEmployeeQualification
    {
        public int QualificationID { get; set; }
        public long EmployeeID { get; set; }
        public string SchoolOrCollege { get; set; }
        public int? UniversityID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int DegreeID { get; set; }
        public string DegreeOther { get; set; }
        public string CertificateTitle { get; set; }
        public string GradeorPercentage { get; set; }
        public int CertificatesAttested { get; set; }
        public int CertificatesVerified { get; set; }
        public int UniversityClearenceRequired { get; set; }
        public int ClearenceCompleted { get; set; }
        public string Remarks { get; set; }
        public string ReferenceNumber { get; set; }
    }
}
