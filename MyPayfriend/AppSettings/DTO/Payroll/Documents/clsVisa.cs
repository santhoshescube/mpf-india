﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyPayFriend;

public class clsVisa : IDisposable
{
    public int? DocumentID { get; set; }
    /// <summary>
    /// Gets or sets Unique ID of the visa (Identity Field)
    /// </summary>
    public int? VisaID { get; set; }
    /// <summary>
    /// Gets or sets Employee ID associated with the visa
    /// </summary>
    public long? EmployeeID { get; set; }
    /// <summary>
    /// Gets or sets Visa Number
    /// </summary>
    public string VisaNumber { get; set; }
    /// <summary>
    /// Gets or sets the date on which visa is issued
    /// </summary>
    public DateTime? VisaIssueDate { get; set; }
    /// <summary>
    /// Gets or sets the place where visa is issued
    /// </summary>
    public string PlaceofIssue { get; set; }
    /// <summary>
    /// Gets or sets expiry date of the visa
    /// </summary>
    public DateTime? VisaExpiryDate { get; set; }
    /// <summary>
    /// Gets or sets the countryID from where visa is issued
    /// </summary>
    public int? IssueCountryID { get; set; }

    public bool IsRenew { get; set; }
    public int DocumentTypeID { get; set; }
    public int OperationTypeID { get; set; }
    public int ReferenceID { get; set; }
    public int VisaTypeID { get; set; }
    public string  ReferenceNumber { get; set; }


    /// <summary>
    /// Gets or sets Renewal date of the visa
    /// </summary>
    // public DateTime? VisaRenewDate { get; set; }
    //public bool MultipleEntry { get; set; }
    // public int? MultipleEntryCountryID { get; set; }
    //public DateTime? MultipleEntryExpiryDate { get; set; }
    /// <summary>
    /// Gets or sets Remarks
    /// </summary>
    public string Remarks { get; set; }
    public string UID { get; set; }
    public int? visaDesignationID { get; set; }
    public int? visaCompanyID { get; set; }

    public clsPager Pager { get; set; }

    #region Dispose

    // Track whether Dispose method has already been called.
    private bool disposed = false;

    /// <summary>
    /// This method will be called by user code.
    /// </summary>
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    public void Dispose(bool disposing)
    {
        if (disposing)
        {
            // check to see if object is disposed.
            if (!this.disposed)
            {
                // dispose unmanaged resources here.
            }
        }
        this.disposed = true;
    }

    #endregion

    #region Destructor
    ~clsVisa()
    {
        // This dispose method will only be called at runtime.
        Dispose(false);
    }
    #endregion
}

