﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{

    public class clsDTOTradeLicense
    {
        public int TradeLicenseID { get; set; }
        public int CompanyID { get; set; }
        public string Sponsor { get; set; }
        public decimal SponserFee { get; set; }
        public string LicenceNumber { get; set; }
        public string City { get; set; }
        public int? ProvinceID { get; set; }
        public string Partner { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Remarks { get; set; }
        public bool IsRenewed { get; set; }
    }
}
