﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOLabourCard
    {
        public clsDTOEmiratesCard objDTOEmiratesCard { get; set; }
        public int CardID { get; set; }
        public long EmployeeID { get; set; }
        public int CardNumber { get; set; }
        public int CardPersonalIDNumber { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime RenewalDate { get; set; }
        public string Remarks { get; set; }
        public bool IsRenewed { get; set; }

        private clsDTOLabourCard()
        {
            objDTOEmiratesCard = new clsDTOEmiratesCard();
        }
    }
    
}
