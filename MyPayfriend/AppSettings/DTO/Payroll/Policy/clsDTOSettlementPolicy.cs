﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class clsDTOSettlementPolicy
    {
        public int ModeAddEdit { get; set; }
        public int SetPolicyID { get; set; }
        public string DescriptionPolicy { get; set; }
        public bool blnIsIncludeLeave{ get; set; }
        public int CalculationID { get; set; }
        public bool blnIsIncludeLeaveTer { get; set; }
        public bool blnIsIncludeLeavePro { get; set; }
        public int CalculationIDTer { get; set; }
        public int CalculationIDPro { get; set; }
        public bool CompanyBasedN { get; set; }
        public bool CompanyBasedT { get; set; }
        public bool CompanyBasedP { get; set; }

        public List<clsDTOSettlementPolicyDetail> lstclsDTOSettlementPolicyDetail { get; set; }
        public List<clsDTOSettlementPolicyDetailFive> lstclsDTOSettlementPolicyDetailFive { get; set; }
        public List<clsDTOInExGra> lstclsDTOInExGra { get; set; }

        public List<clsDTOSettlementPolicyDetailTer> lstclsDTOSettlementPolicyDetailTer { get; set; }
        public List<clsDTOSettlementPolicyDetailFiveTer> lstclsDTOSettlementPolicyDetailFiveTer { get; set; }
        public List<clsDTOInExGraTer> lstclsDTOInExGraTer { get; set; }


        public List<clsDTOSettlementPolicyDetailPro> lstclsDTOSettlementPolicyDetailPro { get; set; }
        public List<clsDTOSettlementPolicyDetailFivePro> lstclsDTOSettlementPolicyDetailFivePro { get; set; }
        public List<clsDTOInExGraPro> lstclsDTOInExGraPro { get; set; }

    }

        public class clsDTOSettlementPolicyDetail
        {
            public int DtlsID { get; set; }
            public int ParameterID { get; set; }
            public decimal NoOfMonths { get; set; }
            public decimal NoOfDays { get; set; }
        }

        public class clsDTOSettlementPolicyDetailFive
        {
            public int DtlsID { get; set; }
            public int ParameterID { get; set; }
            public decimal NoOfMonths { get; set; }
            public decimal NoOfDays { get; set; }
        }
       public class clsDTOInExGra
       {
            public int AdditionDeductionID { get; set; }
            public string DescriptionStr { get; set; }
            public bool Sel { get; set; }
       }

       public class clsDTOSettlementPolicyDetailTer
       {
           public int DtlsID { get; set; }
           public int ParameterID { get; set; }
           public decimal NoOfMonths { get; set; }
           public decimal NoOfDays { get; set; }
       }

       public class clsDTOSettlementPolicyDetailFiveTer
       {
           public int DtlsID { get; set; }
           public int ParameterID { get; set; }
           public decimal NoOfMonths { get; set; }
           public decimal NoOfDays { get; set; }
       }
       public class clsDTOInExGraTer
       {
           public int AdditionDeductionID { get; set; }
           public string DescriptionStr { get; set; }
           public bool Sel { get; set; }
       }





       public class clsDTOSettlementPolicyDetailPro
       {
           public int DtlsID { get; set; }
           public int ParameterID { get; set; }
           public decimal NoOfMonths { get; set; }
           public decimal NoOfDays { get; set; }
       }

       public class clsDTOSettlementPolicyDetailFivePro
       {
           public int DtlsID { get; set; }
           public int ParameterID { get; set; }
           public decimal NoOfMonths { get; set; }
           public decimal NoOfDays { get; set; }
       }

       public class clsDTOInExGraPro
       {
           public int AdditionDeductionID { get; set; }
           public string DescriptionStr { get; set; }
           public bool Sel { get; set; }
       }


}
