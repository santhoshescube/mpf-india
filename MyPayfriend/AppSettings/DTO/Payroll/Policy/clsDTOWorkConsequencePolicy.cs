﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
     /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : WorkConsequence Policy Properties 
       * ******************************************/
    public class clsDTOWorkConsequencePolicy
    {
        /// <summary>
        /// Unique ID of the WorkConsequence Policy. Primary Key.
        /// </summary>
       public int WorkConsequencePolicyID{get;set;}
        /// <summary>
        /// Name of the WorkConsequence Policy
        /// </summary>
         public string WorkConsequencePolicy{get;set;}
        /// <summary>
        /// check WorkConsequence and shortage shown in salary slip separatly or together
        /// </summary>
        public int CalculationID { get; set; }
        /// <summary>
        /// checking Company policy depend
        /// </summary>
        public int CompanyBasedOn { get; set; }
        /// <summary>
        /// checking rate only
        /// </summary>
        public bool IsRateOnly { get; set; }
        /// <summary>
        /// coressponding WorkConsequence rate
        /// </summary>
        public decimal WorkConsequenceRate { get; set; }
        /// <summary>
        /// specifing rate is based on Hour
        /// </summary>
        //public bool IsRateBasedOnHour { get; set; }
        /// <summary>
        /// 
        /// hours working per day
        /// </summary>
        //public decimal HoursPerDay { get; set; }
        /// <summary>
        /// Specifing WorkConsequence or Shortage 1 for WorkConsequence, 2 for Shortage
        /// </summary>
        /// 

        public double Percentage { get; set; }
        public int WorkConsequenceType { get; set; }

        public List<clsDTOSalaryPolicyDetailWorkConsequence> DTOSalaryPolicyDetailWorkConsequence { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public clsDTOWorkConsequencePolicy()
        {
            this.DTOSalaryPolicyDetailWorkConsequence = new List<clsDTOSalaryPolicyDetailWorkConsequence>();
        }

      
    }

    /// <summary>
    /// Salary policy Details for addition deduction
    /// </summary>
    public class clsDTOSalaryPolicyDetailWorkConsequence
    {
        /// <summary>
        /// salpolicy Details ID
        /// </summary>
        public int SerialNo { get; set; }
      //public int SalPolicyID{get;set;}
        /// <summary>
        /// Corresponding Addition deduction ID for Excluding
        /// </summary>
      public int AdditionDeductionID{get;set;}
        /// <summary>
        /// WorkConsequence Or Shortage
        /// </summary>
      public int PolicyType { get; set; }

      
    }
}
