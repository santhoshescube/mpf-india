﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
     /*********************************************
       * Author       : Arun
       * Created On   : 10 Apr 2012
       * Purpose      : LeaveConsequence Policy Properties 
       * ******************************************/
    public class clsDTOLeaveConsequencePolicy
    {
        /// <summary>
        /// Unique ID of the LeaveConsequence Policy. Primary Key.
        /// </summary>
       public int LeaveConsequencePolicyID{get;set;}
        /// <summary>
        /// Name of the LeaveConsequence Policy
        /// </summary>
         public string LeaveConsequencePolicy{get;set;}
        /// <summary>
        /// check LeaveConsequence and shortage shown in salary slip separatly or together
        /// </summary>
        public int CalculationID { get; set; }
        /// <summary>
        /// checking Company policy depend
        /// </summary>
        public int CompanyBasedOn { get; set; }
        /// <summary>
        /// checking rate only
        /// </summary>
        public bool IsRateOnly { get; set; }
        /// <summary>
        /// coressponding LeaveConsequence rate
        /// </summary>
        public decimal LeaveConsequenceRate { get; set; }
        /// <summary>
        /// specifing rate is based on Hour
        /// </summary>
        //public bool IsRateBasedOnHour { get; set; }
        /// <summary>
        /// 
        /// hours working per day
        /// </summary>
        //public decimal HoursPerDay { get; set; }
        /// <summary>
        /// Specifing LeaveConsequence or Shortage 1 for LeaveConsequence, 2 for Shortage
        /// </summary>
        /// 

        public double Percentage { get; set; }
        public int LeaveConsequenceType { get; set; }

        public List<clsDTOSalaryPolicyDetailLeaveConsequence> DTOSalaryPolicyDetailLeaveConsequence { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public clsDTOLeaveConsequencePolicy()
        {
            this.DTOSalaryPolicyDetailLeaveConsequence = new List<clsDTOSalaryPolicyDetailLeaveConsequence>();
        }

      
    }

    /// <summary>
    /// Salary policy Details for addition deduction
    /// </summary>
    public class clsDTOSalaryPolicyDetailLeaveConsequence
    {
        /// <summary>
        /// salpolicy Details ID
        /// </summary>
        public int SerialNo { get; set; }
      //public int SalPolicyID{get;set;}
        /// <summary>
        /// Corresponding Addition deduction ID for Excluding
        /// </summary>
      public int AdditionDeductionID{get;set;}
        /// <summary>
        /// LeaveConsequence Or Shortage
        /// </summary>
      public int PolicyType { get; set; }

      
    }
}
