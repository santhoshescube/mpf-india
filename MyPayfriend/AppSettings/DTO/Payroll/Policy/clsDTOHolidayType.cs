﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDTOHolidayType
    {

        public int intHolidayTypeID { get; set; }
        public string strHolidayType { get; set; }
        public string strColor { get; set; }
        
    }
}
