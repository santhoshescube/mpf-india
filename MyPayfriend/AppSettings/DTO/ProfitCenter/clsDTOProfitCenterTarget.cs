﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOProfitCenterTarget
    {
        public int TargetID { get; set; }
        public int CompanyID { get; set; }
        public int ProfitCenterID { get; set; }
        public DateTime MonthFrom { get; set; }
        public DateTime MonthTo { get; set; }
        public double Target { get; set; }
        public string Remarks { get; set; }

        public List<clsDTOProfitCenterTargetDetails> ProfitCenterTargetDetails { get; set; }
    }

    public class clsDTOProfitCenterTargetDetails
    {
        public int EmployeeID { get; set; }
        public double Target { get; set; }
    }
}
