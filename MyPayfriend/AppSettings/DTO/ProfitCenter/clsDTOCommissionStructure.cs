﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOCommissionStructure
    {
        public int CommissionStructureID { get; set; }
        public string StructureName { get; set; }
        public double Deduction { get; set; }

        public List<clsDTOCommissionStructureGroupDetails> CommissionStructureGroupDetails { get; set; }
        public List<clsDTOCommissionStructureExcessTarget> CommissionStructureExcessTarget { get; set; }
    }

    public class clsDTOCommissionStructureGroupDetails
    {
        public double FromPercentage { get; set; }
        public double ToPercentage { get; set; }
        public double Amount { get; set; }
    }

    public class clsDTOCommissionStructureExcessTarget
    {
        public int ParameterID { get; set; }
        public double ExcessTarget { get; set; }
        public double Percentage { get; set; }
    }
}
