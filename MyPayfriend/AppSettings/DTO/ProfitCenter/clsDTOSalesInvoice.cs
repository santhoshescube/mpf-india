﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public class clsDTOSalesInvoice
    {
        public long SalesInvoiceID { get; set; }
        public int CompanyID { get; set; }
        public int ProfitCenterID { get; set; }
        public DateTime Month { get; set; }
        public string Remarks { get; set; }

        public List<clsDTOSalesInvoiceDetails> SalesInvoiceDetails { get; set; }
    }

    public class clsDTOSalesInvoiceDetails
    {
        public int EmployeeID { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public double Amount { get; set; }
        public string Remarks { get; set; }
    }
}