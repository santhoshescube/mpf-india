﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace Utilities
{
    /************************************************
     * Created By : Ratheesh
     * Created On : 30 June 2011
     ***********************************************/

    public class clsNumberToWords
    {
        private static List<NumberTable> numberTable = new List<NumberTable>() { 
                new NumberTable( "01", "one"),      new NumberTable("02", "two"),
                new NumberTable("03", "three"),     new NumberTable("04", "four"),
                new NumberTable("05", "five"),      new NumberTable("06", "six"),
                new NumberTable("07", "seven"),     new NumberTable("08", "eight"),
                new NumberTable("09", "nine"),      new NumberTable("10", "ten"),
                new NumberTable("11", "eleven"),    new NumberTable("12", "twelve"),
                new NumberTable("13", "thirteen"),  new NumberTable("14", "fourteen"),
                new NumberTable("15", "fiftieen"),  new NumberTable("16", "sixteen"),
                new NumberTable("17", "seventeen"), new NumberTable("18", "eighteen"),
                new NumberTable("19", "nineteen"),  new NumberTable("20", "twenty"),
                new NumberTable("30", "thirty"),    new NumberTable("40", "fourty"),
                new NumberTable("50", "fifty"),     new NumberTable("60", "sixty"),
                new NumberTable("70", "seventy"),   new NumberTable("80", "eighty"),
                new NumberTable("90", "ninety")
            };

        private static string NumberToWords(string strNumberToConvert)
        {
            Int64 lngNumber = 0;
            Int64.TryParse(strNumberToConvert, out lngNumber);

            if (lngNumber == 0)
                return "Zero";
            
            string strNumber = lngNumber.ToString();
            string strOutputString = string.Empty;
            string strChunk = string.Empty;
            string strTensOnes, strHundreds, strTens, strOnes, strWord;
            string strTempTens, strTempOnes, strTemp;

            int intLength, intPosition, intLoops, intCounter = 1;

            intLength = strNumber.Length;
            intPosition = strNumber.Length - 2;
            intLoops = (int)lngNumber / 3;

            // make sure there is an extra loop added for the remaining numbers
            intLoops += lngNumber % 3 == 0 ? 0 : 1;

            while (intCounter <= intLoops)
            {
                // get chunks of 3 charaters at a time padded with leading zeros
                strChunk = "000" + strNumber.Substring(intPosition, 3).Substring(2, 3);

                if (strChunk != "000")
                {
                    strTens = strChunk.Substring(2, 1);
                    strOnes = strChunk.Substring(3, 1);
                    strHundreds = strChunk.Substring(1, 1);
                    strTensOnes = strChunk.Substring(2, 2);

                    // if twenty or less, use the word directly from numbersTable
                    if (Convert.ToInt32(strTensOnes) <= 20 || strOnes == "0")
                    {
                        strTemp = GetWordFromNumberTable(strTensOnes);
                        strWord = GetWord(intCounter);
                        strOutputString = string.Format("{0}{1}{2}", strTemp, strWord, strOutputString);
                    }
                    else
                    {
                        // break down the ones and the tens separately
                        strTempOnes = GetWordFromNumberTable(strOnes);
                        strTempTens = GetWordFromNumberTable(strTens);
                        strWord = GetWord(intCounter);
                        strOutputString = string.Format(" {0}-{1}{2}{3}", strTempTens, strTempOnes, strWord, strOutputString);
                    }
                    // now get hundreds
                    if (strHundreds != "0")
                    {
                        strWord = GetWordFromNumberTable("0" + strHundreds);
                        strOutputString = string.Format(" {0} hundred {1}", strWord, strOutputString);
                    }
                }

                // increment counter by 1
                intCounter += 1;
                // decrement position by 3
                intPosition -= 3;
            }

            // remove any double spaces
            strOutputString.Replace("  ", " ").Trim();

            if(strOutputString.Length > 0)
                // Capitalise first character
                strOutputString = strOutputString.Substring(0, 1).ToUpper() + strOutputString.Substring(1, strOutputString.Length);

            return strOutputString;
        }
        private static string GetRightFraction(string number)
        {
            string outputString = string.Empty;
            string strFirstChar = number.Substring(0, 1);
            if (strFirstChar == "0")
            {
                int length = number.Length;
                int counter = 1;
                string strOneChar;
                while (counter <= length)
                {
                    strOneChar = number.Substring(counter, 1);
                    outputString += strOneChar == "0" ? "zero" : GetWordFromNumberTable("0" + strOneChar);
                    counter += 1;
                }

                // remove any double spaces
                outputString.Replace("  ", " ").Trim();

                if (outputString.Length > 0)
                    // Capitalise first character
                    outputString = outputString.Substring(0, 1).ToUpper() + outputString.Substring(1, outputString.Length);
            }
            else
            {
                outputString = NumberToWords(number);
            }

            return outputString;
        }
        public static string ConvertNumberToWords(string stringNumber)
        {
            double dblNumber = 0;
            double.TryParse(stringNumber, out dblNumber);

            if (dblNumber == 0)
                return "Zero";

            string strNumber = dblNumber.ToString();

            string[] strArrNumbers = strNumber.Split('.');

            string LeftPart = string.Empty, RightPart = string.Empty;
            LeftPart = strArrNumbers[0];
            RightPart = strArrNumbers.Length == 2 ? strArrNumbers[1] : string.Empty;

            string outputString = NumberToWords(LeftPart) + (RightPart == string.Empty ? string.Empty : " " + GetRightFraction(RightPart));

            return outputString;
        }
        private static string GetWordFromNumberTable(string strNumber)
        {
            string returnValue = string.Empty;
            if (numberTable != null && numberTable.Count > 0) 
            {
                var temp = numberTable.Find(n => n.Number == strNumber);
                if (temp != null)
                {
                    returnValue = temp.NumberInWord;
                    temp = null;
                }
            }
            return returnValue;
        }
        private static string GetWord(int position)
        {
            string strReturnValue = string.Empty;

            switch (position)
            {
                case 1:
                    strReturnValue = string.Empty;
                    break;
                case 2:
                    strReturnValue = " thousand ";
                    break;
                case 3:
                    strReturnValue = " million ";
                    break;
                case 4:
                    strReturnValue = " billion ";
                    break;
                case 5:
                    strReturnValue = " trillion ";
                    break;
                case 6:
                    strReturnValue = " quadrillion ";
                    break;
                case 7:
                    strReturnValue = " quintillion ";
                    break;
                case 8:
                    strReturnValue = " sextillion ";
                    break;
                case 9:
                    strReturnValue = " septillion ";
                    break;
                case 10:
                    strReturnValue = " octillion ";
                    break;
                case 11:
                    strReturnValue = " nonillion ";
                    break;
                case 12:
                    strReturnValue = " decillion ";
                    break;
                case 13:
                    strReturnValue = " undecillion ";
                    break;
            }

            return strReturnValue;
        }
    }

    public class NumberTable
    {
        public string Number { get; set; }
        public string NumberInWord { get; set; }

        public NumberTable(string number, string numberInWord)
        {
            this.Number = number;
            this.NumberInWord = NumberInWord;
        }
    }


}
