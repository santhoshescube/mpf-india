﻿/// <summary>
/// Enumerator variables for Form Identifications
/// </summary>
public enum FormID
{
    CommonMessage = 1,
    CompanyInformation = 2,     // 201-250
    Employee = 5,               // 401 - 450
    Currency = 8,               // 551 - 600
    Warehouse = 9,              // 601 - 650
    BankInformation = 10,       // 651 - 700
    Documents = 17,  
    Configuration =41,// 1051 - 1100
    User = 50,
    RoleSettings = 51,
    MainFormRibbon = 59,
    EmailSettings = 61,     // 4001 - 4050
    EmailPopup = 62,   // 4051 - 4100
    employee = 63,        //4101 - 4150
    Expense = 64,       //7251 - 7300
    Scanning = 68, // 7301-7330
    ExchangeCurrency = 79, // 7361 -7381
    Vehicle = 94,
    ChangePassword = 95,
    Projects = 101,
    LeavePolicy = 105,
    ShiftPolicy = 107, // 201-250
    WorkPolicy = 108, // 201-250
    OvertimePolicy = 111, //8000-8090
    AbsentPolicy = 112,
    HolidayCalendar = 113,
    AdditionDeduction = 115, //8100 -8130
    SalaryProcess = 116, //8150-
    HolidayType = 118,
    SalaryAdvance = 119,
    Attendance = 121,
    SalaryStructure = 122,  //9600 - 9700
    AttendanceDeviceSttings=124,
    AttendanceMapping=125,
    AttendanceOtTemplate=126,
    AttendanceUpdate=127,
    LeaveFromAttendance=128,
    SalaryRelease=129, // 1800
    SalaryPayment=130,  // 1850
    AttendanceReport=133, //Attendance Summary report 9700 -9710
    SalaryStructureMISReport =134,
    EmployeeProfileMISReport = 135,
    PaymentsMISReport = 136,
    SalarySlipMISReport = 137,
    LeaveSummaryMISReport = 138,
    SalaryAdvanceSummaryReport=139,
    ExpenseMISReport = 140,
    VacationPolicy =144,
    VacationEntry = 145,
    ShiftSchedule = 147,
    HolidayPolicy=148,
    WorkLocation=149,
    LeaveEntry = 150,
    EmployeeLeaveStructure =151,
    AttendanceManual = 152,
    LocationSchedule=153,
    DeductionPolicy=154,
    EmployeeDeductionReport = 155,
    LeaveExtension=156,//6050-6054,6999-7014
    PFPolicy=157,
    SettlementPolicy=158,
    Passport=159,
    EncashPolicy=160,
    CompanyAssets =161,
    EmployeeTransfer=162,
    LeaveOpening=163,
    SettlementProcess=164,
    EmployeeLoan = 165,
    Qualification = 166,
    Visa=167,
    DrivingLicense=168 ,
    ProcessingTimeMaster=169,
    DocumentIssueReceipt = 170,
    HealthCard=171,
    EmirateHealthCard = 172,
    InsuranceCard=173,
    LeaveRequest = 174,  
    DocumentReport=175,
    LabourCard=176,
    AlertInfo=177,
    UnearnedPolicy =178,
    AddParicularsToAll = 179,
    Deposit = 180,
    LoanRepayment = 181,
    CompanyAssetReport=182,
    VacationReport=183,
    TradeLicense =184,
    LabourCostReport = 185,
    AssetHandover = 186,
    LeaseAgreement =187,
    InsuranceDetails = 190,
    HolidayReport = 191,
    SIF = 192,
    VacationExtension = 193,
    CommonReference = 194,
    Navigator = 195,
    AttendanceWizard = 196,
    StockRegister = 197,
    SettlementReport = 198,
    CTCReport = 199,
    EmployeeTransferReport = 200,
    ProjectCostReport = 201,
    PettyCash=202,
    WorkSheet = 203,
    WorkSheetReport=204,
    OffDayMarking=205,
    CompanySettings=206,
    SalaryStructureList=207,
    AttendanceReportLive = 208,
    OffDayMarkingReport=209,
    RptUnreleased=210,
    AccrualReport = 211,
    EmployeeHistoryReport = 212,
    LeavePolicyConsequence = 213,
    ProfitCenterTarget = 214,
    CommissionStructure = 215,
    SalesInvoice = 216,
    WorkPolicyConsequence=217,
    SalaryTemplate=218,
    SalaryAmendment=219
}

public enum EmailFormID
{
    Company = 1,
    Bank = 2,
    Employee = 3,
    Configuration = 13,
    RoleSettings = 16,
    Expense = 24,
    Documents = 34,
    ExchangeCurrency = 38,
    Projects = 55,
    SalaryStructure =62,
    ShiftPolicy =63,
    WorkPolicy =64,
    LeavePolicy =65,
    SalaryRelease=66,
    SalaryPayment=67,
    Attendance=70,
    WorkLocation=71,
    LocationSchedule=72,
    DeductionPolicy=73,
    LeaveEntry=74,
    LeaveExtension=75,
    PFPolicy=76,
    SettlementProcess=77,
    Qualification = 78,
    Passport=79,
    Visa = 80,
    DrivingLicense=81,
    EmployeeLoan = 82,
    EmirateHealthCard = 83,
    HealthCard=84,
    InsuranceCardID=85,
    LabourCard=86,
    LeaveRequest=87,
    ShiftSchedule = 88,
    Deposit = 180,
    LoanRepayment = 181,
    SalaryAdvance = 182,
    LeaveStructure = 183,
    TradeLicense   = 184,
    VacationProcess = 185,
    VacationPolicy  =186,
    LeaseAgreement  =187,
    AssetHandover = 188,
    InsuranceDetails = 190,
    CompanyAsset = 191,
    PettyCash=192
}

public enum eMessageType
{
    Error = 1,
    Warning = 3,
    Information = 2,
    Question = 4
}



public enum OperationType
{
   Company=1,
   Employee = 2
   
}

public enum PaymentModes
{
    Bank = 1,
    Cash = 2,
    Cheque = 3,
    DD = 4
}



public enum PaymentTransactionType
{
    Bank = 1,
    Cash = 2,
    Wps = 3,
    Cheque=4
}





public enum AlertStatus
{
    Open = 86,
    Close = 87,
    Read = 98,
    Blocked = 99
}

public enum eMenuID
{
    NewCompany = 1,
    Bank = 2,
    ExchangeCurrency = 3,
    WorkPolicy = 4,
    ShiftPolicy = 5,
    LeavePolicy = 6,
    HolidayCalender = 7,
    WorkLocation = 8,
    Currency = 9,
    DeductionPolicy = 10,
    AdditionDeduction = 11,
    OvertimePolicy = 12,
    AbsentPolicy = 13,
    CompanyAssets = 14,
    VacationPolicy = 15,
    SettlementPolicy = 16,
    HolidayPolicy = 17,
    EncashPolicy = 18,
    UnearnedPolicy = 19,
    Employee = 20,
    LeaveStructure = 21,
    SalaryStructure = 22,
    LeaveOpening  = 23,
    Loan = 24,
    Attendance = 25,
    SalaryProcess = 26,
    SalaryRelease = 27,
    SalaryPayment = 28,
    SettlementProcess = 29,
    VacationProcess = 30,
    AttendanceMapping = 31,
    VacationExtension = 32,
    ChangePassword = 33,
    ConfigurationSetting = 34,
    EmailSettings = 35,
    Role = 36,
    User = 37,
    Backup = 38,
    AttendanceWizard = 39,
    SalaryStructureReport = 40,
    AttendanceReport = 41,
    PaymentReport = 42,
    LeaveSummaryReport = 43,
    PaySlip = 44,
    EmployeeDeduction = 45,
    SalaryAdvanceSummaryReport = 46,
    EmployeeProfileReport = 47,
    DocumentReport = 48,
    AssetReport = 49,
    LabourCostReport = 50,
    VacationReport = 51,
    SettlementReport = 52,
    ExpenseReport = 53,
    CTCReport = 54,
    AlertReport = 55,
    HolidayReport = 56,
    EmployeeTranferReport = 57,
    Qualification = 58,
    Visa  = 59,
    EmployeeDocumentReceipt = 60,
    OtherDocuments = 61,
    EmployeeDocumentIssue = 62,
    DocumentRegister = 63,
    Passport = 64,
    DrivingLicense = 65,
    HealthCard = 66,
    LabourCard = 67,
    NationalIDCard = 68,
    InsuranceCard = 69,
    ProcessingTime = 70,
    TradeLicense = 71,
    LeaseAgreement = 72,
    Insurance = 73,
    DocumentRenew = 74,
    LeaveExtension = 75,
    LeaveEntry = 76,
    SalaryAdvance = 77,
    EmployeeTransfer = 78,
    AssetHandover = 79,
    ShiftSchedule = 80,
    Expense = 81,
    Deposit = 82,
    LoanRepayment = 83,
    DepositRefund = 84,
    NavigationCompany = 85,
    NavigationEmployee = 86,
    NavigationDocuments = 87,
    NavigationEasyView = 88,
    ViewEmployeeDocuments = 89,
    ViewCompanyDocuments = 90,
    CompanyDocumentReceipt = 135,
    CompanyDocumentIssue = 136,
    DutyRoast = 141,
    WorkSheet = 146,
    AttendanceReportLive = 147,
    WorkSheetRpt = 148,
    OffDayMarkReport = 149,
    PaymentReportProcess = 150,
    SalaryStructureList = 151,
    CompanySettings=152,
    GeneralExpense=153,
    PettyCash = 153,
    AccrualReport = 154,
    EmployeeHistory = 155,
    LeaveConsequencePolicy=157,
    ProfitCenterTarget = 158,
    CommissionStructure = 159,
    SalesInvoice = 160,
    WorkConsequencePolicy=161,
    SalaryTemplate=162

}

public enum eModuleID
{
    Masters = 1,
    Employee = 2,
    Payroll = 3,
    Setings = 4,
    Reports = 5,
    Documents = 6,
    Task=7,
    Navigator=8
}


public enum SqlErrorCodes
{
    UniqueKeyErrorNumber = 2627,
    ForeignKeyErrorNumber = 547
}

public enum ReasonReferences
{
    Damaged = 1,
    Returned = 6
}

public enum ControlState
{
    Enable,
    Disable
}





public enum PredefinedCompany
{
    DefaultCompany = 1
}



public enum eMode
{
    /// <summary>
    /// Indicates Insert Operation
    /// </summary>
    Insert = 1,
    /// <summary>
    /// Indicates Update Operation
    /// </summary>
    Update = 2,
    /// <summary>
    /// Indicates Delete Operation
    /// </summary>
    Delete = 3,
    /// <summary>
    /// Indicates Unknown operation (default)
    /// </summary>
    Unknown = 4
}
















public enum Periods
{
    Yearly = 0,
    Monthly = 1,
    Daily = 2,
    Summary = 3
}



public enum AttendanceStatus
{
    Present = 1,
    Rest = 2,
    Leave = 3,
    Absent = 4
}

public enum AttendanceMode
{
    AttendanceAuto = 1,
    AttendanceManual = 2
}

public enum AttendnaceDevices
{

    FingerTech = 1,
    ZKFingerprint = 2,
    HandPunch = 3,
    ActaTEK = 4
}

public enum AttendanceFiletype
{
    Csv=1,
    Excel=2,
    AccessDB=3,
    SqlDataBase=4
}

public enum PolicyType
{
    Absent = 1,
    Shortage = 2,
    Overtime = 3,
    Holiday = 4,
    Encash = 5,
    Vacation = 6,
    Settlement = 7,
    UnEarnedAmt = 8,
    LeavePolicy = 9,
    Termination=10,
    WorkConPolicy = 11,
    SettlementProvision = 12
}

public enum AbsentType
{
    Absent = 1,
    Shortage = 2
}

public enum CalculationType
{
    BasicPay = 1,
    GrossSalary = 2,
    NetAmount = 3,
    WorkDays = 4 
}



public enum DocDocumentStatus
{
    Receipt = 48,
    Issue = 50
}



public enum Locations
{
    BOTTOMCENTER = 1,
    BOTTOMLEFT=2,
    BOTTOMRIGHT=3,
    TOPCENTER=4,
    TOPLEFT=5,
    TOPRIGHT=6
}

public enum Allignment
{
    CENTER=1,
    LEFT = 2,
    RIGHT = 3
    
}


public enum SearchBy
{
    // these indices are passing to stored procedure.
    // if you change any index, you must update it in the data access layer as well.
    Department = 1,
    Designation = 2,
    EmployeeNumber = 3,
    FirstName = 4,
    EmployeeFullName = 5,
    CardNumber = 6
}



public enum ReferenceTables
{
    VehicleModel = 0,
    VehicleCategory = 1,
    LicenseType = 2,
    Manufacturer = 3,
    VehicleLocation = 4,
    Country = 5,
    Nationality = 6,
    Religion = 7,
    Language = 11,
    InsuranceCompany = 12,
}
public enum Mode
{
    /// <summary>
    /// Indicates Insert Operation
    /// </summary>
    Insert = 1,
    /// <summary>
    /// Indicates Update Operation
    /// </summary>
    Update = 2,
    /// <summary>
    /// Indicates Delete Operation
    /// </summary>
    Delete = 3
}
public enum CommonMessages
{
    SaveConfirm = 1,
    Saved = 2,
    UpdateConfirm = 3,
    Updated = 21,
    DeleteConfirm = 13,
    Deleted = 4,
    FormClose = 8,  
    SaveFailed = 9,
    NextRecord = 11,
    LastRecord = 12,
    PreviousRecord = 10,
    FirstRecord = 9,
    NewRecord = 655,
    DetailExists = 19,
    ConfirmPhotoRemoval = 20,
    PhotoRemoved = 21,
    SearchNotFound = 23,
}
public enum DocumentType
{
    Passport = 1,
    Visa = 2,
    Driving_License = 3,
    Health_Card = 4,
    Labour_Card = 5,
    National_ID_Card = 6,
    InsuranceDetails = 7,
    Qualification = 8,
    Insurance_Card = 9,
    Expense = 10,
    Deposit = 11,
    Lease_Agreement = 12,
    Trading_License = 13,
    Leave_Entry = 14,
    Probation = 15,
    Asset = 16
}
public enum LogSeverity
{
    Info = 0,
    Warning = 1,
    Error = 2,
    Fatal = 3
}

public enum ePaymentClassification
{
    Monthly = 1
}

public enum eInterestType
{
    None = 1,
    Simple = 2,
    Compound = 3
}

public enum EmployeeWorkStatus
{
    Absconding = 1,
    Expired = 2,
    Retired = 3,
    Resigned = 4,
    Terminated = 5,
    InService = 6,
    Probation = 7
}

public enum EmployeeLeaveType
{
    Vacation = 0,
    Sick = 1,
    Casual = 2,
    Maternity = 3,
    RH = 4    
}

public enum LeaveRequestStatus
{
    Requested = 1,
    Approved = 2,
    Rejected = 3
}

public enum EmployeeExpenseTransType
{
    None = 1,
    Reimbursement = 2,
    Deduction = 3
}
public enum LeaveExtensionType
{
    LeaveExtension = 1,
    TimeExtensionDay = 2,
    TimeExtensionMonth = 3

}

public enum AssetStausType
{
    OnHand = 1,
    Issued = 2,
    Blocked = 3
}