﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class ClsCommonSettings
{
    public static string strEncryptionKey = "MAKE-IT-POSSIBLE";

    public static int ParentCompanyID { get; set; }
    public static string CurrentCompany { get; set; }
    public static int CurrentCompanyID { get; set; }
    public static int ModuleID { get; set; }
    public static int ProductID { get { return 9; } }   
    public static int UserID { get; set; }
    public static int intEmployeeID { get; set; }
    public static int RoleID { get; set; }
    public static int TimerInterval { get; set; }
    public static int Scale { get; set; }
    public static bool AttendanceAutofillForSinglePunch { get; set; }
    //public static bool AttendanceAutofillForSinglePunch { get { return true; } }
    public static bool AttendanceLiveEnable { get; set; }
    public static int intBackupInterval { get; set; }
    public static int CurrencyID { get; set; }
    public static string Currency { get; set; }
    public static bool SIFFile50Percentage { get; set; }
    public static int intDepartmentID { get; set; }

    public static bool SendMailToAll { get; set; }
    public static bool ShowErrorMess { get { return false; } }

    public static string DbName { get; set; }
    public static string strCompanyName { get; set; }
    public static string NationalityCard { get; set; }
    
    public static string strUserName { get; set; }
    public static string strrole { get; set; }
    public static string strEmployeeName { get; set; }
    public static string ServerName { get; set; }
    public static string MessageCaption { get; set; }
    public static string ReportFooter { get; set; }
    

    //public static string strEmployeeNumberPrefix { get; set; }
    //public static bool blnEmployeeNumberAutogenerate { get; set; }
    public static string strAgentBankCodePrefix { get; set; }
  
    public static string strServerPath { get; set; }
   
    public static System.Drawing.Color MandatoryColor { get { return System.Drawing.SystemColors.Info; } }
   
   

    public static bool DocumentModuleExists { get; set; }

    public static int intLoanAmountPercentage { get; set; }
  
    public static bool Glb24HourFormat { get; set; }
    public static bool GlbSalaryDayIsEditable { get; set; }
    public static bool GlSalarySlipIsEditable { get; set; }
    public static bool GlDisplayLeaveSummaryInSalarySlip { get; set; }

    public static bool IsArabicView { get; set; }
    public static bool IsAmountRoundByZero { get; set; } 
    public static bool IsHrPowerEnabled { get; set; }
    //public static bool IsVacationBasedOnActualRejoinDate { get; set; }
    //public static bool IsMultiCurrencyEnabled { get; set; }

    public static bool IsArabicViewEnabled { get; set; }

    public static bool ThirdPartyIntegration { get; set; }
    //    /// <summary>
    //    /// Gets DateTime value that represents its null equivalent
    //    /// </summary>
    public static DateTime? NullDate = null;
    //    /// <summary>
    //    /// Gets string value that represents its null equivalent
    //    /// </summary>
    public static string NullString = null;
    //    /// <summary>
    //    /// Gets integer value that represents its null equivalent
    //    /// </summary>
    public static int? NullInt = null;

    public static bool IsVacationBasedOnActualRejoinDate { get; set; }
    public static DateTime GetServerDate()
    {
        DataLayer objDataLayer = new DataLayer();
        object objServerDate = objDataLayer.ExecuteScalar("spGetServerDate");
        return Convert.ToDateTime(objServerDate).Date;
    }

    public static DateTime GetServerDateTime()
    {
        DataLayer objDataLayer = new DataLayer();
        object objServerDate = objDataLayer.ExecuteScalar("spGetServerDate");
        return Convert.ToDateTime(objServerDate);
    }

    
}
