﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Runtime.InteropServices;
using System.IO;



public class ClsExportDatagridviewToExcel
    {

        public string ExcelFilePath ;
        [DllImport("Shell32dll",EntryPoint = "ShellexCuteA")]
        private static extern int ShellEx(int hWnd , string lpOperation , string lpFile , string lpParameters , string lpDirectory, int nShowCmd );

    /// <summary>
    ///  Attendance GridView Exporting
    /// </summary>
    /// <param name="grdView"> Exporting Grid View</param>
    /// <param name="fileName"> File name for excel</param>
    /// <param name="iStartCol"> strating column </param>
    /// <param name="iEndcol"> End Column</param>
        /// <param name="blnAttendanceStatusShow"> Show the status of attendance </param>
        public void ExportToExcel(System.Windows.Forms.DataGridView  grdView , string fileName 
             , int iStartCol , int iEndcol ,bool blnAttendanceStatusShow)
        {
            if (grdView.Rows.Count > 0)
            {
                // ' Choose the path, name, and extension for the Excel file
                string myFile = fileName;
                ExcelFilePath = myFile;
                // ' Open the file and write the headers
                // Dim fs As New IO.StreamWriter(myFile, False)
                StreamWriter fs = new StreamWriter(myFile, false);
                fs.WriteLine("<?xml version=\"1.0\"?>");

                fs.WriteLine("<?mso-application progid=\"Excel.Sheet\"?>");
                fs.WriteLine("<ss:Workbook xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">");

                //' Create the styles for the worksheet
                fs.WriteLine("  <ss:Styles>");
                //' Style for the column headers
                fs.WriteLine("    <ss:Style ss:ID=\"1\">");
                fs.WriteLine("      <ss:Font ss:Bold=\"1\"/>");
                fs.WriteLine("      <ss:Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");

                fs.WriteLine("      <ss:Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
                fs.WriteLine("    </ss:Style>");
                //' Style for the column information
                fs.WriteLine("    <ss:Style ss:ID=\"2\">");
                fs.WriteLine("      <ss:Alignment ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");
                fs.WriteLine("    </ss:Style>");

                fs.WriteLine("    <ss:Style ss:ID=\"3\">");
                fs.WriteLine("    <NumberFormat ss:Format=\"0.00\" /> ");
                fs.WriteLine("    </ss:Style>");


                fs.WriteLine("  </ss:Styles>");

                //' Write the worksheet contents
                fs.WriteLine("<ss:Worksheet ss:Name=\"Sheet1\">");
                fs.WriteLine("  <ss:Table>");

                for (int i = iStartCol; i <= iEndcol; i++)//' grdView.Columns.Count - 1
                {
                    fs.WriteLine(String.Format("    <ss:Column ss:Width=\"{0}\"/>",
                             grdView.Columns[i].Width));
                }
                fs.WriteLine("    <ss:Row>");
                for (int j = iStartCol; j <= iEndcol; j++)// ' grdView.Columns.Count - 1
                {
                    fs.WriteLine(String.Format("      <ss:Cell ss:StyleID=\"1\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                          grdView.Columns[j].HeaderText));
                }
                fs.WriteLine("    </ss:Row>");

                //' Check for an empty row at the end due to Adding allowed on the DataGridView
                int subtractBy = 0;
                string cellText = "";
                if (grdView.AllowUserToAddRows == true)
                    subtractBy = 2;
                else
                    subtractBy = 1;

                //' Write contents for each cell
                for (int k = 0; k <= grdView.RowCount - subtractBy; k++)
                {
                    fs.WriteLine(String.Format("    <ss:Row ss:Height=\"{0}\">", grdView.Rows[k].Height));
                    for (int intCol = iStartCol; intCol <= iEndcol; intCol++)// ' grdView.Columns.Count - 1
                    {
                        if (blnAttendanceStatusShow)
                        {
                            if (grdView.Columns[intCol].Name == "ColDgvStatus")
                            {
                                if (grdView[intCol, k].Value.ToStringCustom() != string.Empty)
                                {

                                    if (grdView[intCol, k].Value.ToInt32()==(int)AttendanceStatus.Present)
                                    {
                                    cellText = "Present";
                                    }
                                    else if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Rest)
                                    {
                                        cellText = "Rest";
                                    }
                                    else if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Leave)
                                    {
                                        cellText = "Leave";
                                    }
                                    else if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Absent)
                                    {
                                        cellText = "Absent";
                                    }
                                    else
                                    {
                                        cellText = "";
                                    }
                                }
                                else
                                {
                                    cellText = "";
                                }
                            }
                            else
                            { cellText = (Convert.ToString(grdView[intCol, k].Value)) == "" ? "" : Convert.ToString((grdView[intCol, k].Value)); }

                        }
                        else
                        {
                            cellText = (Convert.ToString(grdView[intCol, k].Value)) == "" ? "" : Convert.ToString((grdView[intCol, k].Value));
                        }
                        //' Check for null cell and change it to empty to avoid error
                        if (Convert.ToString(cellText) == "")
                            cellText = "";
                        if (cellText.IsDecimal())
                            fs.WriteLine("      <ss:Cell ss:StyleID=\"3\"><ss:Data ss:Type=\"Number\">" + cellText + "</ss:Data> <NamedCell ss:Name=\"Total\"/></ss:Cell>");
                        else
                            fs.WriteLine(String.Format("      <ss:Cell ss:StyleID=\"2\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>", cellText.ToString()));
                        
                    }
                    fs.WriteLine("    </ss:Row>");
                }

                //' Close up the document
                fs.WriteLine("  </ss:Table>");
                fs.WriteLine("</ss:Worksheet>");
                fs.WriteLine("</ss:Workbook>");
                fs.Close();
            }// if 
        // ' Open the file in Microsoft Excel
        // ' 10 = SW_SHOWDEFAULT
        // ' ShellEx(FormName.Handle, "Open", myFile, "", "", 10)
        }


        public void ExportToExcel(System.Windows.Forms.DataGridView grdView, string fileName
           , int iStartCol, int iEndcol, bool blnAttendanceStatusShow,DataTable CompanyHead,string[] HeaderRows)
        {
            if (grdView.Rows.Count > 0)
            {
                // ' Choose the path, name, and extension for the Excel file
                int ColCount = iEndcol - iStartCol;
                string myFile = fileName;
                ExcelFilePath = myFile;
                fileName = Path.GetFileNameWithoutExtension(fileName);
                // ' Open the file and write the headers
                // Dim fs As New IO.StreamWriter(myFile, False)
                StreamWriter fs = new StreamWriter(myFile, false);
                fs.WriteLine("<?xml version=\"1.0\"?>");

                fs.WriteLine("<?mso-application progid=\"Excel.Sheet\"?>");
                fs.WriteLine("<ss:Workbook xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">");

                //' Create the styles for the worksheet
                fs.WriteLine("  <ss:Styles>");
                //' Style for the column headers
                fs.WriteLine("    <ss:Style ss:ID=\"1\">");
                fs.WriteLine("      <ss:Font ss:Bold=\"1\"/>");
                fs.WriteLine("      <ss:Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");
                fs.WriteLine("      <ss:Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
                fs.WriteLine("    </ss:Style>");
                //' Style for the column information
                fs.WriteLine("    <ss:Style ss:ID=\"2\">");
                fs.WriteLine("      <ss:Alignment ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");
                fs.WriteLine("    </ss:Style>");
                //' Style for the company information
                fs.WriteLine("    <ss:Style ss:ID=\"3\">");
                fs.WriteLine("      <ss:Font ss:Bold=\"1\" ss:Size=\"14\"/>");
                fs.WriteLine("      <ss:Alignment ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");
                fs.WriteLine("    </ss:Style>");
                fs.WriteLine("  </ss:Styles>");
               
                //' Write the worksheet contents
                fs.WriteLine("<ss:Worksheet ss:Name=\"Sheet1\">");

                fs.WriteLine("  <ss:Table>");

                fs.WriteLine(String.Format("    <ss:Row ss:Height=\"{0}\">", 20));
                fs.WriteLine(String.Format("      <ss:Cell ss:MergeAcross=\"" + ColCount + "\" ss:StyleID=\"3\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                          CompanyHead.Rows[0]["CompanyName"].ToString()));
                fs.WriteLine("    </ss:Row>");
                fs.WriteLine("    <ss:Row>");
                fs.WriteLine(String.Format("      <ss:Cell ss:MergeAcross=\"" + ColCount + "\" ss:StyleID=\"2\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                          CompanyHead.Rows[0]["HPOBox"].ToString()));
                fs.WriteLine("    </ss:Row>");
                fs.WriteLine("    <ss:Row>");
                fs.WriteLine(String.Format("      <ss:Cell ss:MergeAcross=\"" + ColCount + "\" ss:StyleID=\"2\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                          CompanyHead.Rows[0]["RoadArea"].ToString()));
                fs.WriteLine("    </ss:Row>");
                fs.WriteLine("    <ss:Row>");
                fs.WriteLine(String.Format("      <ss:Cell ss:MergeAcross=\"" + ColCount + "\" ss:StyleID=\"2\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                          CompanyHead.Rows[0]["PhonePABXNo"].ToString()));
                fs.WriteLine("    </ss:Row>");
                fs.WriteLine("    <ss:Row>");
                fs.WriteLine(String.Format("      <ss:Cell ss:MergeAcross=\"" + ColCount + "\" ss:StyleID=\"1\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                         fileName));
                fs.WriteLine("    </ss:Row>");
                for (int i = 0; i < HeaderRows.Length; i++)
                {
                    fs.WriteLine("    <ss:Row>");
                    fs.WriteLine(String.Format("      <ss:Cell ss:MergeAcross=\"" + ColCount + "\" ss:StyleID=\"2\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                              HeaderRows[i]));
                    fs.WriteLine("    </ss:Row>");
                }
                //fs.WriteLine("  </ss:Table>");

                //fs.WriteLine("  <ss:Table>");

                //for (int i = iStartCol; i <= iEndcol; i++)//' grdView.Columns.Count - 1
                //{
                //    fs.WriteLine(String.Format("    <ss:Column ss:Width=\"{0}\"/>",
                //             grdView.Columns[i].Width));
                //}
                fs.WriteLine("    <ss:Row>");
                for (int j = iStartCol; j <= iEndcol; j++)// ' grdView.Columns.Count - 1
                {
                    fs.WriteLine(String.Format("      <ss:Cell ss:StyleID=\"1\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                          grdView.Columns[j].HeaderText));
                }
                fs.WriteLine("    </ss:Row>");

                //' Check for an empty row at the end due to Adding allowed on the DataGridView
                int subtractBy = 0;
                string cellText = "";
                if (grdView.AllowUserToAddRows == true)
                    subtractBy = 2;
                else
                    subtractBy = 1;

                //' Write contents for each cell
                for (int k = 0; k <= grdView.RowCount - subtractBy; k++)
                {
                    fs.WriteLine(String.Format("    <ss:Row ss:Height=\"{0}\">", grdView.Rows[k].Height));
                    for (int intCol = iStartCol; intCol <= iEndcol; intCol++)// ' grdView.Columns.Count - 1
                    {
                        if (blnAttendanceStatusShow)
                        {
                            if (grdView.Columns[intCol].Name == "ColDgvStatus")
                            {
                                if (grdView[intCol, k].Value.ToStringCustom() != string.Empty)
                                {

                                    if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Present)
                                    {
                                        cellText = "Present";
                                    }
                                    else if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Rest)
                                    {
                                        cellText = "Rest";
                                    }
                                    else if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Leave)
                                    {
                                        cellText = "Leave";
                                    }
                                    else if (grdView[intCol, k].Value.ToInt32() == (int)AttendanceStatus.Absent)
                                    {
                                        cellText = "Absent";
                                    }
                                    else
                                    {
                                        cellText = "";
                                    }
                                }
                                else
                                {
                                    cellText = "";
                                }
                            }
                            else
                            { cellText = (Convert.ToString(grdView[intCol, k].Value)) == "" ? "" : Convert.ToString((grdView[intCol, k].Value)); }

                        }
                        else
                        {
                            cellText = (Convert.ToString(grdView[intCol, k].Value)) == "" ? "" : Convert.ToString((grdView[intCol, k].Value));
                        }
                        //' Check for null cell and change it to empty to avoid error
                        if (Convert.ToString(cellText) == "")
                            cellText = "";

                        fs.WriteLine(String.Format("      <ss:Cell ss:StyleID=\"2\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>", cellText.ToString()));
                    }
                    fs.WriteLine("    </ss:Row>");
                }

                //' Close up the document
                fs.WriteLine("  </ss:Table>");
                fs.WriteLine("</ss:Worksheet>");
                fs.WriteLine("</ss:Workbook>");
                fs.Close();
            }// if 
            // ' Open the file in Microsoft Excel
            // ' 10 = SW_SHOWDEFAULT
            // ' ShellEx(FormName.Handle, "Open", myFile, "", "", 10)
        }

        public void ExportToExcel(System.Windows.Forms.DataGridView grdView, string fileName
             , int iStartCol, int iEndcol, int[] displayCol)
        {
            if (grdView.Rows.Count > 0)
            {
                // ' Choose the path, name, and extension for the Excel file
                string myFile = fileName;
                ExcelFilePath = myFile;
                // ' Open the file and write the headers
                // Dim fs As New IO.StreamWriter(myFile, False)
                StreamWriter fs = new StreamWriter(myFile, false);
                fs.WriteLine("<?xml version=\"1.0\"?>");

                fs.WriteLine("<?mso-application progid=\"Excel.Sheet\"?>");
                fs.WriteLine("<ss:Workbook xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">");

                //' Create the styles for the worksheet
                fs.WriteLine("  <ss:Styles>");
                //' Style for the column headers
                fs.WriteLine("    <ss:Style ss:ID=\"1\">");
                fs.WriteLine("      <ss:Font ss:Bold=\"1\"/>");
                fs.WriteLine("      <ss:Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");

                fs.WriteLine("      <ss:Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
                fs.WriteLine("    </ss:Style>");
                //' Style for the column information
                fs.WriteLine("    <ss:Style ss:ID=\"2\">");
                fs.WriteLine("      <ss:Alignment ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");
                fs.WriteLine("    </ss:Style>");
                fs.WriteLine("  </ss:Styles>");

                //' Write the worksheet contents
                fs.WriteLine("<ss:Worksheet ss:Name=\"Sheet1\">");
                fs.WriteLine("  <ss:Table>");

                for (int i = iStartCol; i <= iEndcol; i++)//' grdView.Columns.Count - 1
                {
                    if (displayCol.Contains(i))
                    {
                        fs.WriteLine(String.Format("    <ss:Column ss:Width=\"{0}\"/>",
                                 grdView.Columns[i].Width));
                    }
                }
                fs.WriteLine("    <ss:Row>");
                for (int j = iStartCol; j <= iEndcol; j++)// ' grdView.Columns.Count - 1
                {
                    if (displayCol.Contains(j))
                    {
                        fs.WriteLine(String.Format("      <ss:Cell ss:StyleID=\"1\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>",
                              grdView.Columns[j].HeaderText));
                    }
                }
                fs.WriteLine("    </ss:Row>");

                //' Check for an empty row at the end due to Adding allowed on the DataGridView
                int subtractBy = 0;
                string cellText = "";
                if (grdView.AllowUserToAddRows == true)
                    subtractBy = 2;
                else
                    subtractBy = 1;

                //' Write contents for each cell
                for (int k = 0; k <= grdView.RowCount - subtractBy; k++)
                {
                    fs.WriteLine(String.Format("    <ss:Row ss:Height=\"{0}\">", grdView.Rows[k].Height));
                    for (int intCol = iStartCol; intCol <= iEndcol; intCol++)// ' grdView.Columns.Count - 1
                    {
                        if (displayCol.Contains(intCol))
                        {
                            cellText = (Convert.ToString(grdView[intCol, k].Value)) == "" ? "" : Convert.ToString((grdView[intCol, k].Value));

                            //' Check for null cell and change it to empty to avoid error
                            if (Convert.ToString(cellText) == "")
                                cellText = "";

                            fs.WriteLine(String.Format("      <ss:Cell ss:StyleID=\"2\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>", cellText.ToString()));
                        }
                    }
                    fs.WriteLine("    </ss:Row>");
                }

                //' Close up the document
                fs.WriteLine("  </ss:Table>");
                fs.WriteLine("</ss:Worksheet>");
                fs.WriteLine("</ss:Workbook>");
                fs.Close();
            }// if 
            // ' Open the file in Microsoft Excel
            // ' 10 = SW_SHOWDEFAULT
            // ' ShellEx(FormName.Handle, "Open", myFile, "", "", 10)
        }


        public void ExportToExcelForAmendment(System.Windows.Forms.DataGridView grdView,string fileName,int iStartCol,int iEndcol,bool blnAttendanceStatusShow)
        {
            if (grdView.Rows.Count <= 0)
                return;
            string path = fileName;
            this.ExcelFilePath = path;
            StreamWriter streamWriter = new StreamWriter(path, false);
            streamWriter.WriteLine("<?xml version=\"1.0\"?>");
            streamWriter.WriteLine("<?mso-application progid=\"Excel.Sheet\"?>");
            streamWriter.WriteLine("<ss:Workbook xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">");
            streamWriter.WriteLine("  <ss:Styles>");
            streamWriter.WriteLine("    <ss:Style ss:ID=\"1\">");
            streamWriter.WriteLine("      <ss:Font ss:Bold=\"1\"/>");
            streamWriter.WriteLine("      <ss:Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");
            streamWriter.WriteLine("      <ss:Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
            streamWriter.WriteLine("    </ss:Style>");
            streamWriter.WriteLine("    <ss:Style ss:ID=\"n3\">");
            streamWriter.WriteLine("    <NumberFormat ss:Format=\"&quot;$&quot;#,##0.00\" /> ");
            streamWriter.WriteLine("    </ss:Style>");
            streamWriter.WriteLine("    <ss:Style ss:ID=\"3\">");
            streamWriter.WriteLine("    <NumberFormat ss:Format=\"0.00\" /> ");
            streamWriter.WriteLine("    </ss:Style>");
            streamWriter.WriteLine("    <ss:Style ss:ID=\"2\">");
            streamWriter.WriteLine("      <ss:Alignment ss:Vertical=\"Center\" ss:WrapText=\"1\"/>");
            streamWriter.WriteLine("    </ss:Style>");
            streamWriter.WriteLine("  </ss:Styles>");
            streamWriter.WriteLine("  <Names>");
            streamWriter.WriteLine("  <NamedRange ss:Name=\"Total\" ss:ReferesTo=\"Sheet1!R12C5\"/>");
            streamWriter.WriteLine("  </Names>");
            streamWriter.WriteLine("<ss:Worksheet ss:Name=\"Sheet1\">");
            streamWriter.WriteLine("  <ss:Table>");
            for (int index = iStartCol; index <= iEndcol; ++index)
                streamWriter.WriteLine(string.Format("    <ss:Column ss:Width=\"{0}\"/>", (object)grdView.Columns[index].Width));
            streamWriter.WriteLine("    <ss:Row>");
            for (int index = iStartCol; index <= iEndcol; ++index)
                streamWriter.WriteLine(string.Format("      <ss:Cell ss:StyleID=\"1\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>", (object)grdView.Columns[index].HeaderText));
            streamWriter.WriteLine("    </ss:Row>");
            int num = !grdView.AllowUserToAddRows ? 1 : 2;
            for (int index1 = 0; index1 <= grdView.RowCount - num; ++index1)
            {
                streamWriter.WriteLine(string.Format("    <ss:Row ss:Height=\"{0}\">", (object)grdView.Rows[index1].Height));
                for (int index2 = iStartCol; index2 <= iEndcol; ++index2)
                {
                    string source;
                    switch (index2)
                    {
                        case 0:
                        case 3:
                            source = Convert.ToString(grdView[index2, index1].FormattedValue) == "" ? "" : Convert.ToString(grdView[index2, index1].FormattedValue);
                            break;
                        case 1:
                            source = Convert.ToString(grdView[index2, index1].Value) == "" ? "" : grdView[index2, index1].Value.ToDateTime().ToString("dd MMM yyyy");
                            break;
                        default:
                            source = Convert.ToString(grdView[index2, index1].Value) == "" ? "" : Convert.ToString(grdView[index2, index1].Value);
                            break;
                    }
                    if (Convert.ToString(source) == "")
                        source = "";
                    if (source.IsDecimal())
                        streamWriter.WriteLine("      <ss:Cell ss:StyleID=\"3\"><ss:Data ss:Type=\"Number\">" + source + "</ss:Data> <NamedCell ss:Name=\"Total\"/></ss:Cell>");
                    else
                        streamWriter.WriteLine(string.Format("      <ss:Cell ss:StyleID=\"2\"><ss:Data ss:Type=\"String\">{0}</ss:Data></ss:Cell>", (object)source.ToString()));
                }
                streamWriter.WriteLine("    </ss:Row>");
            }
            streamWriter.WriteLine("  </ss:Table>");
            streamWriter.WriteLine("</ss:Worksheet>");
            streamWriter.WriteLine("</ss:Workbook>");
            streamWriter.Close();
        }



    }
        



