﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPayfriend
{
    public enum LogSeverity
    {
        Info =0,
        Warning = 1,
        Error = 2,
        Fatal = 3
    }
}
