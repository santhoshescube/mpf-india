﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace MyPayfriend
{
    public class clsMessageDAL
    {
        public static DataTable GetMessages(int FormID, int ProductID)
        {
            string strMode = "SEL";

            if (ClsCommonSettings.IsArabicView)
            {
                 strMode = "ARB";
            }
            return new DataLayer().ExecuteDataTable("spMessage", new List<SqlParameter> { 
                new SqlParameter("@Mode", strMode),
                new SqlParameter("@FormID", FormID)
            });
        }
    }
}
