﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class ClsDALArabicConverter
    {
        DataTable dtArabic = null;
        #region GetControls
        public void GetControls(Control cntrl, int intFormID)
        {
            if (cntrl != null && cntrl.Name != null)
            {
                string strName = "", strToolTip = ""; ;
                string strEnglishText = "";
                int intLocationX = 0, intLocationY = 0;

                if (cntrl.GetType().ToString().Equals("System.Windows.Forms.BindingNavigator"))
                {
                    BindingNavigator CntrlBN = cntrl as BindingNavigator;

                    for (int i = 0; i < CntrlBN.Items.Count; i++)
                    {
                        strName = "";
                        strToolTip = ""; ;
                        strEnglishText = "";
                        intLocationX = 0;
                        intLocationY = 0;

                        if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripButton")
                        {
                            ToolStripButton Btn = CntrlBN.Items[i] as ToolStripButton;
                            strName = Btn.Name;
                            strToolTip = Btn.ToolTipText;
                            strEnglishText = Btn.Text;
                            intLocationX = intLocationY = 0;
                        }
                        else if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripLabel")
                        {
                            ToolStripLabel Lbl = CntrlBN.Items[i] as ToolStripLabel;
                            strName = Lbl.Name;
                            strToolTip = Lbl.ToolTipText;
                            strEnglishText = Lbl.Text;
                            intLocationX = intLocationY = 0;
                        }
                        else if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripTextBox")
                        {
                            ToolStripTextBox Txt = CntrlBN.Items[i] as ToolStripTextBox;
                            strName = Txt.Name;
                            strToolTip = Txt.ToolTipText;
                            strEnglishText = "";
                            intLocationX = intLocationY = 0;
                        }
                        InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
                    }
                }
                else if (cntrl.GetType().ToString().Equals("System.Windows.Forms.ToolStrip"))
                {
                    ToolStrip CntrlBN = cntrl as ToolStrip;

                    for (int i = 0; i < CntrlBN.Items.Count; i++)
                    {
                        strName = "";
                        strToolTip = ""; ;
                        strEnglishText = "";
                        intLocationX = 0;
                        intLocationY = 0;

                        if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripButton")
                        {
                            ToolStripButton Btn = CntrlBN.Items[i] as ToolStripButton;
                            strName = Btn.Name;
                            strToolTip = Btn.ToolTipText;
                            strEnglishText = Btn.Text;
                            intLocationX = intLocationY = 0;
                        }
                        else if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripLabel")
                        {
                            ToolStripLabel Lbl = CntrlBN.Items[i] as ToolStripLabel;
                            strName = Lbl.Name;
                            strToolTip = Lbl.ToolTipText;
                            strEnglishText = Lbl.Text;
                            intLocationX = intLocationY = 0;
                        }
                        else if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripTextBox")
                        {
                            ToolStripTextBox Txt = CntrlBN.Items[i] as ToolStripTextBox;
                            strName = Txt.Name;
                            strToolTip = Txt.ToolTipText;
                            strEnglishText = "";
                            intLocationX = intLocationY = 0;
                        }
                        InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
                    }
                }
                else if (cntrl.GetType().ToString().Equals("System.Windows.Forms.TabControl"))
                {
                    TabControl Tb = cntrl as TabControl;

                    strName = Tb.Name;
                    intLocationX = Tb.Location.X;
                    intLocationY = Tb.Location.Y;
                    InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);

                    for (int i = 0; i < Tb.TabPages.Count; i++)
                    {
                        strName = Tb.TabPages[i].Name;
                        strEnglishText = Tb.TabPages[i].Text;
                        intLocationX = 0;
                        intLocationY = 0;
                        InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);

                        for (int j = 0; j < Tb.TabPages[i].Controls.Count; j++)
                        {
                            if (Tb.TabPages[i].Controls[j].GetType().FullName != "System.Windows.Forms.Panel" && Tb.TabPages[i].Controls[j].GetType().FullName != "System.Windows.Forms.TabControl" && Tb.TabPages[i].Controls[j].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && Tb.TabPages[i].Controls[j].GetType().FullName != "System.Windows.Forms.GroupBox")
                                GetBaseControls(Tb.TabPages[i].Controls[j], intFormID);
                            else
                                GetControls(Tb.TabPages[i].Controls[j], intFormID);
                        }
                    }
                }
                else if (cntrl.GetType().FullName.Equals("System.Windows.Forms.Panel"))
                {
                    Panel Pnl = cntrl as Panel;

                    for (int i = 0; i < Pnl.Controls.Count; i++)
                    {
                        if (Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.Panel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.TabControl" && Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                            GetBaseControls(Pnl.Controls[i], intFormID);
                        else
                            GetControls(Pnl.Controls[i], intFormID);
                    }
                }
                else if (cntrl.GetType().FullName.Equals("DevComponents.DotNetBar.PanelEx"))
                {
                    DevComponents.DotNetBar.PanelEx Pnl = cntrl as DevComponents.DotNetBar.PanelEx;
                    for (int i = 0; i < Pnl.Controls.Count; i++)
                    {
                        if (Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.ExpandablePanel" &&  Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.Panel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.TabControl" && Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                            GetBaseControls(Pnl.Controls[i], intFormID);
                        else
                            GetControls(Pnl.Controls[i], intFormID);
                    }
                }
                else if (cntrl.GetType().FullName.Equals("DevComponents.DotNetBar.ExpandablePanel"))
                {
                    DevComponents.DotNetBar.ExpandablePanel Pnl = cntrl as DevComponents.DotNetBar.ExpandablePanel;

                    strName = Pnl.Name;
                    strEnglishText = Pnl.TitleText;
                    intLocationX = 0;
                    intLocationY = 0;
                    strToolTip = "";

                    InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
                    for (int i = 0; i < Pnl.Controls.Count; i++)
                    {
                        if (Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.ExpandablePanel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.Panel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.TabControl" && Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx")
                            GetBaseControls(Pnl.Controls[i], intFormID);
                        else
                            GetControls(Pnl.Controls[i], intFormID);
                    }
                }
                else if (cntrl.GetType().FullName.Equals("System.Windows.Forms.GroupBox"))
                {
                    System.Windows.Forms.GroupBox GBx = cntrl as System.Windows.Forms.GroupBox;
                    for (int i = 0; i < GBx.Controls.Count; i++)
                    {
                        if (GBx.Controls[i].GetType().FullName != "System.Windows.Forms.Panel" && GBx.Controls[i].GetType().FullName != "System.Windows.Forms.TabControl" && GBx.Controls[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && GBx.Controls[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                            GetBaseControls(GBx.Controls[i], intFormID);
                        else
                            GetControls(GBx.Controls[i], intFormID);
                    }
                }

                //else if (cntrl.GetType().FullName.Equals("DevComponents.DotNetBar.Bar"))
                //{
                //    DevComponents.DotNetBar.Bar br = cntrl as DevComponents.DotNetBar.Bar;

                //    for (int i = 0; i < br.Items.Count; i++)
                //    {
                //        if (br.Items[i].GetType().FullName != "System.Windows.Forms.Panel" && br.Items[i].GetType().FullName != "System.Windows.Forms.TabControl" && br.Items[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && br.Items[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                //        {
                //            if (br.Items[i].GetType().FullName == "DevComponents.DotNetBar.LabelItem")
                //            {
                //                strName = br.Items[i].Name;
                //                strEnglishText = br.Items[i].Text;
                //                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
                //            }
                //        }
                //        else
                //            GetControls(br.Controls[i], intFormID);
                //    }
                //}

                else
                {
                    GetBaseControls(cntrl, intFormID);
                }
            }
        }
        #endregion GetControls

        #region GetBaseControls
        public void GetBaseControls(Control control, int intFormID)
        {
            string strName = "", strToolTip = ""; ;
            string strEnglishText = "";
            int intLocationX = 0, intLocationY = 0;

            if (control.GetType().FullName == "System.Windows.Forms.Button")
            {
                Button btn = control as Button;
                strName = btn.Name;
                strEnglishText = btn.Text;
                intLocationX = btn.Location.X;
                intLocationY = btn.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.ButtonX")
            {
                DevComponents.DotNetBar.ButtonX btn = control as DevComponents.DotNetBar.ButtonX;
                strName = btn.Name;
                strEnglishText = btn.Text;
                strToolTip = btn.Tooltip;
                intLocationX = btn.Location.X;
                intLocationY = btn.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "System.Windows.Forms.Label")
            {
                Label lbl = control as Label;
                strName = lbl.Name;
                strEnglishText = lbl.Text;
                intLocationX = lbl.Location.X;
                intLocationY = lbl.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.LabelX")
            {
                DevComponents.DotNetBar.LabelX lbl = control as DevComponents.DotNetBar.LabelX;
                strName = lbl.Name;
                strEnglishText = lbl.Text;
                intLocationX = lbl.Location.X;
                intLocationY = lbl.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "System.Windows.Forms.TextBox")
            {
                TextBox txt = control as TextBox;
                strName = txt.Name;
                strEnglishText = "";
                intLocationX = txt.Location.X;
                intLocationY = txt.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.Controls.TextBoxX")
            {
                DevComponents.DotNetBar.Controls.TextBoxX txt = control as DevComponents.DotNetBar.Controls.TextBoxX;
                strName = txt.Name;
                intLocationX = txt.Location.X;
                intLocationY = txt.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "System.Windows.Forms.ComboBox")
            {
                ComboBox cbo = control as ComboBox;
                strName = cbo.Name;
                intLocationX = cbo.Location.X;
                intLocationY = cbo.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.Controls.ComboBoxEx")
            {
                DevComponents.DotNetBar.Controls.ComboBoxEx cbo = control as DevComponents.DotNetBar.Controls.ComboBoxEx;
                strName = cbo.Name;
                intLocationX = cbo.Location.X;
                intLocationY = cbo.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "System.Windows.Forms.DateTimePicker")
            {
                DateTimePicker dtp = control as DateTimePicker;
                strName = dtp.Name;
                intLocationX = dtp.Location.X;
                intLocationY = dtp.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "System.Windows.Forms.RichTextBox")
            {
                RichTextBox rTxt = control as RichTextBox;
                strName = rTxt.Name;
                intLocationX = rTxt.Location.X;
                intLocationY = rTxt.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "System.Windows.Forms.CheckBox")
            {
                CheckBox chk = control as CheckBox;
                strName = chk.Name;
                strEnglishText = chk.Text;
                intLocationX = chk.Location.X;
                intLocationY = chk.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "System.Windows.Forms.RadioButton")
            {
                RadioButton rBtn = control as RadioButton;
                strName = rBtn.Name;
                strEnglishText = rBtn.Text;
                intLocationX = rBtn.Location.X;
                intLocationY = rBtn.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.Controls.CheckBoxX")
            {
                DevComponents.DotNetBar.Controls.CheckBoxX chk = control as DevComponents.DotNetBar.Controls.CheckBoxX;
                strName = chk.Name;
                strEnglishText = chk.Text;
                intLocationX = chk.Location.X;
                intLocationY = chk.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName == "System.Windows.Forms.CheckedListBox")
            {
                CheckedListBox chk = control as CheckedListBox;
                strName = chk.Name;
                strEnglishText = chk.Text;
                intLocationX = chk.Location.X;
                intLocationY = chk.Location.Y;
                InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);
            }
            else if (control.GetType().FullName.Equals("DevComponents.DotNetBar.Bar"))
            {
                DevComponents.DotNetBar.Bar br = control as DevComponents.DotNetBar.Bar;

                for (int i = 0; i < br.Items.Count; i++)
                {
                    strName = "";
                    strEnglishText = "";
                    strToolTip = "";
                    intLocationX = 0;
                    intLocationY = 0;

                    if (br.Items[i].GetType().FullName != "DevComponents.DotNetBar.ExpandablePanel" && br.Items[i].GetType().FullName != "System.Windows.Forms.Panel" && br.Items[i].GetType().FullName != "System.Windows.Forms.TabControl" && br.Items[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && br.Items[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                    {
                        if (br.Items[i].GetType().FullName == "DevComponents.DotNetBar.LabelItem")
                        {
                            strName = br.Items[i].Name;
                            strEnglishText = br.Items[i].Text;
                        }
                        else if (br.Items[i].GetType().FullName == "DevComponents.DotNetBar.ButtonItem")
                        {
                            strName = br.Items[i].Name;
                            strEnglishText = br.Items[i].Text;
                            strToolTip = br.Items[i].Tooltip;
                        }
                        InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);

                    }
                    else
                        GetControls(br.Controls[i], intFormID);
                }
            }
            else if (control.GetType().FullName.Equals("System.Windows.Forms.DataGridView"))
            {
                DataGridView Dgv = control as DataGridView;
                strName = Dgv.Name;
                intLocationX = Dgv.Location.X;
                intLocationY = Dgv.Location.Y;
                InsertArabicVersion(strName, "", "", intLocationX, intLocationY, intFormID);
                for (int i = 0; i < Dgv.Columns.Count; i++)
                {
                    strName = Dgv.Columns[i].Name;
                    strEnglishText = Dgv.Columns[i].HeaderText;
                    InsertArabicVersion(strName, "", strEnglishText, 0, 0, intFormID);
                }
            }
            //InsertArabicVersion(strName, strToolTip, strEnglishText, intLocationX, intLocationY, intFormID);

        }
        #endregion GetBaseControls

        public void InsertArabicVersion(string strName, string strToolTip, string strText, int intLocX, int intLocY, int intFormID)
        {
            if (strName != "")
            {
                ArrayList sqlParam = new ArrayList();
                sqlParam.Add(new SqlParameter("@Mode", 1));
                sqlParam.Add(new SqlParameter("@FormID", intFormID));
                sqlParam.Add(new SqlParameter("@ControlName", strName));
                sqlParam.Add(new SqlParameter("@EnglishText", strText));
                sqlParam.Add(new SqlParameter("@EnglishTooltip", strToolTip));
                sqlParam.Add(new SqlParameter("@LocationX", intLocX));
                sqlParam.Add(new SqlParameter("@LocationY", intLocY));
                new clsConnection().ExecutesQuery(sqlParam, "spArabicConversion");
            }
        }

        public void SetArabicVersion(int intFormID, Form objFrm)
        {
            dtArabic = GetArabicConrols(intFormID);
            SetArabicForms(objFrm);
        }

        private void SetArabicForms(Form objFrm)
        {
            DataRow[] dr = dtArabic.Select("ControlName='" + objFrm.Name + "'");
            if (dr != null)
            {
                if (dr.Length > 0)
                    objFrm.Text = Convert.ToString(dr[0]["ArabicText"]);
            }
            objFrm.RightToLeftLayout = true;
            objFrm.RightToLeft = RightToLeft.Yes;

            foreach (Control cntrl in objFrm.Controls)
                SetControls(cntrl);
        }

        public void SetControls(Control cntrl)
        {

            if (cntrl != null && cntrl.Name != null)
            {
                DataRow[] DataRow = null;

                if (cntrl.GetType().ToString().Equals("System.Windows.Forms.BindingNavigator"))
                {
                    BindingNavigator CntrlBN = cntrl as BindingNavigator;

                    for (int i = 0; i < CntrlBN.Items.Count; i++)
                    {
                        if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripButton")
                        {
                            ToolStripButton Btn = CntrlBN.Items[i] as ToolStripButton;
                            DataRow = dtArabic.Select("ControlName='" + Btn.Name + "'");
                            if (DataRow != null)
                            {
                                if (DataRow.Length > 0)
                                {
                                    Btn.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                                    Btn.ToolTipText = Convert.ToString(DataRow[0]["ArabicToolTip"]);
                                }
                            }
                        }
                        else if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripLabel")
                        {
                            ToolStripLabel Lbl = CntrlBN.Items[i] as ToolStripLabel;
                            DataRow = dtArabic.Select("ControlName='" + Lbl.Name + "'");
                            if (DataRow != null)
                            {
                                if (DataRow.Length > 0)
                                {
                                    Lbl.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                                    Lbl.ToolTipText = Convert.ToString(DataRow[0]["ArabicToolTip"]);
                                }
                            }
                        }
                        else if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripTextBox")
                        {
                            ToolStripTextBox Txt = CntrlBN.Items[i] as ToolStripTextBox;
                            DataRow = dtArabic.Select("ControlName='" + Txt.Name + "'");
                            if (DataRow != null)
                            {
                                if (DataRow.Length > 0)
                                {
                                    Txt.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                                    Txt.ToolTipText = Convert.ToString(DataRow[0]["ArabicToolTip"]);
                                }
                            }
                        }
                    }
                }
                else if (cntrl.GetType().ToString().Equals("System.Windows.Forms.ToolStrip"))
                {
                    ToolStrip CntrlBN = cntrl as ToolStrip;

                    for (int i = 0; i < CntrlBN.Items.Count; i++)
                    {
                        if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripButton")
                        {
                            ToolStripButton Btn = CntrlBN.Items[i] as ToolStripButton;
                            DataRow = dtArabic.Select("ControlName='" + Btn.Name + "'");
                            if (DataRow != null)
                            {
                                if (DataRow.Length > 0)
                                {
                                    Btn.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                                    Btn.ToolTipText = Convert.ToString(DataRow[0]["ArabicToolTip"]);
                                }
                            }
                        }
                        else if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripLabel")
                        {
                            ToolStripLabel Lbl = CntrlBN.Items[i] as ToolStripLabel;
                            DataRow = dtArabic.Select("ControlName='" + Lbl.Name + "'");
                            if (DataRow != null)
                            {
                                if (DataRow.Length > 0)
                                {
                                    Lbl.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                                    Lbl.ToolTipText = Convert.ToString(DataRow[0]["ArabicToolTip"]);
                                }
                            }
                        }
                        else if (CntrlBN.Items[i].GetType().ToString() == "System.Windows.Forms.ToolStripTextBox")
                        {
                            ToolStripTextBox Txt = CntrlBN.Items[i] as ToolStripTextBox;
                            DataRow = dtArabic.Select("ControlName='" + Txt.Name + "'");
                            if (DataRow != null)
                            {
                                if (DataRow.Length > 0)
                                {
                                    Txt.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                                    Txt.ToolTipText = Convert.ToString(DataRow[0]["ArabicToolTip"]);
                                }
                            }
                        }
                    }
                }
                else if (cntrl.GetType().ToString().Equals("System.Windows.Forms.TabControl"))
                {
                    TabControl Tb = cntrl as TabControl;
                    Tb.RightToLeft = RightToLeft.Yes;
                    Tb.RightToLeftLayout = true;
                    DataRow = dtArabic.Select("ControlName='" + Tb.Name + "'");

                    if (DataRow != null)
                    {
                        if (DataRow.Length > 0)
                            Tb.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }

                    for (int i = 0; i < Tb.TabPages.Count; i++)
                    {
                        DataRow = dtArabic.Select("ControlName='" + Tb.TabPages[i].Name + "'");
                        if (DataRow != null)
                        {
                            if (DataRow.Length > 0)
                                Tb.TabPages[i].Text = Convert.ToString(DataRow[0]["ArabicText"]);
                        }
                        for (int j = 0; j < Tb.TabPages[i].Controls.Count; j++)
                        {
                            if (Tb.TabPages[i].Controls[j].GetType().FullName != "DevComponents.DotNetBar.ExpandablePanel" && Tb.TabPages[i].Controls[j].GetType().FullName != "System.Windows.Forms.Panel" && Tb.TabPages[i].Controls[j].GetType().FullName != "System.Windows.Forms.TabControl" && Tb.TabPages[i].Controls[j].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && Tb.TabPages[i].Controls[j].GetType().FullName != "System.Windows.Forms.GroupBox")
                                SetBaseControls(Tb.TabPages[i].Controls[j]);
                            else
                                SetControls(Tb.TabPages[i].Controls[j]);
                        }
                    }
                }
                else if (cntrl.GetType().FullName.Equals("System.Windows.Forms.Panel"))
                {
                    Panel Pnl = cntrl as Panel;
                    DataRow = dtArabic.Select("ControlName='" + Pnl.Name + "'");
                    if (DataRow != null)
                    {
                        if (DataRow.Length != 0)
                            Pnl.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }
                    for (int i = 0; i < Pnl.Controls.Count; i++)
                    {
                        if (Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.ExpandablePanel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.Panel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.TabControl" && Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                            SetBaseControls(Pnl.Controls[i]);
                        else
                            SetControls(Pnl.Controls[i]);
                    }
                }
                else if (cntrl.GetType().FullName.Equals("DevComponents.DotNetBar.PanelEx"))
                {
                    DevComponents.DotNetBar.PanelEx Pnl = cntrl as DevComponents.DotNetBar.PanelEx;
                    DataRow = dtArabic.Select("ControlName='" + Pnl.Name + "'");
                    if (DataRow != null)
                    {
                        if (DataRow.Length != 0)
                            Pnl.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }
                    for (int i = 0; i < Pnl.Controls.Count; i++)
                    {
                        if (Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.ExpandablePanel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.Panel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.TabControl" && Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                            SetBaseControls(Pnl.Controls[i]);
                        else
                            SetControls(Pnl.Controls[i]);
                    }
                }
                else if (cntrl.GetType().FullName.Equals("System.Windows.Forms.GroupBox"))
                {
                    System.Windows.Forms.GroupBox Pnl = cntrl as System.Windows.Forms.GroupBox;
                    DataRow = dtArabic.Select("ControlName='" + Pnl.Name + "'");
                    if (DataRow != null)
                    {
                        if (DataRow.Length != 0)
                        {
                            Pnl.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                            Pnl.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                        }
                    }
                    for (int i = 0; i < Pnl.Controls.Count; i++)
                    {
                        if (Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.ExpandablePanel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.Panel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.TabControl" && Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                            SetBaseControls(Pnl.Controls[i]);
                        else
                            SetControls(Pnl.Controls[i]);
                    }
                }
                else if (cntrl.GetType().FullName.Equals("DevComponents.DotNetBar.ExpandablePanel"))
                {
                    DevComponents.DotNetBar.ExpandablePanel Pnl = cntrl as DevComponents.DotNetBar.ExpandablePanel;
                    DataRow = dtArabic.Select("ControlName='" + Pnl.Name + "'");
                    if (DataRow != null)
                    {
                        if (DataRow.Length > 0)
                            Pnl.TitleText = Convert.ToString(DataRow[0]["EnglishText"]);
                    }
                    for (int i = 0; i < Pnl.Controls.Count; i++)
                    {
                        if (Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.ExpandablePanel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.Panel" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.TabControl" && Pnl.Controls[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && Pnl.Controls[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                            SetBaseControls(Pnl.Controls[i]);
                        else
                            SetControls(Pnl.Controls[i]);
                    }
                }
                else
                {
                    SetBaseControls(cntrl);
                }
            }
        }

        private void SetBaseControls(Control control)
        {
            DataRow[] DataRow = dtArabic.Select("ControlName='" + control.Name + "'");

            if (control.GetType().FullName == "System.Windows.Forms.Button")
            {
                Button btn = control as Button;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                    {
                        btn.Text = Convert.ToString(DataRow[0]["ArabicText"]).Trim();
                        btn.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }
                }
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.ButtonX")
            {
                DevComponents.DotNetBar.ButtonX btn = control as DevComponents.DotNetBar.ButtonX;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                    {
                        btn.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                        btn.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                        btn.Tooltip = Convert.ToString(DataRow[0]["ArabicToolTip"]);
                    }
                }
            }
            else if (control.GetType().FullName == "System.Windows.Forms.Label")
            {
                Label lbl = control as Label;
                lbl.TextAlign = System.Drawing.ContentAlignment.TopLeft;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                    {
                        lbl.Text = Convert.ToString(DataRow[0]["ArabicText"]).Trim();
                        lbl.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }
                }
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.LabelX")
            {
                DevComponents.DotNetBar.LabelX lbl = control as DevComponents.DotNetBar.LabelX;
                lbl.TextAlignment = System.Drawing.StringAlignment.Near;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                    {
                        lbl.Text = Convert.ToString(DataRow[0]["ArabicText"]).Trim();
                        lbl.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }
                }
            }
            else if (control.GetType().FullName == "System.Windows.Forms.TextBox")
            {
                TextBox txt = control as TextBox;
                //txt.TextAlign = HorizontalAlignment.Right;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                        txt.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                }
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.Controls.TextBoxX")
            {
                DevComponents.DotNetBar.Controls.TextBoxX txt = control as DevComponents.DotNetBar.Controls.TextBoxX;
                // txt.TextAlign = HorizontalAlignment.Right;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                        txt.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                }
            }
            else if (control.GetType().FullName == "System.Windows.Forms.ComboBox")
            {
                ComboBox cbo = control as ComboBox;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                        cbo.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                }
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.Controls.ComboBoxEx")
            {
                DevComponents.DotNetBar.Controls.ComboBoxEx cbo = control as DevComponents.DotNetBar.Controls.ComboBoxEx;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                        cbo.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                }
            }
            else if (control.GetType().FullName == "System.Windows.Forms.DateTimePicker")
            {
                DateTimePicker dtp = control as DateTimePicker;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                        dtp.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                }
            }
            else if (control.GetType().FullName == "System.Windows.Forms.RichTextBox")
            {
                RichTextBox rTxt = control as RichTextBox;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                        rTxt.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                }
            }
            else if (control.GetType().FullName == "System.Windows.Forms.CheckBox")
            {
                CheckBox chk = control as CheckBox;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                    {
                        chk.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                        chk.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }
                }
            }
            else if (control.GetType().FullName == "System.Windows.Forms.CheckedListBox")
            {
                CheckedListBox chk = control as CheckedListBox;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                    {
                        chk.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                        chk.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }
                }
            }
            else if (control.GetType().FullName == "System.Windows.Forms.RadioButton")
            {
                RadioButton rBtn = control as RadioButton;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                    {
                        rBtn.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                        rBtn.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }
                }
            }
            else if (control.GetType().FullName == "DevComponents.DotNetBar.Controls.CheckBoxX")
            {
                DevComponents.DotNetBar.Controls.CheckBoxX chk = control as DevComponents.DotNetBar.Controls.CheckBoxX;
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                    {
                        chk.Text = Convert.ToString(DataRow[0]["ArabicText"]);
                        chk.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                    }
                }
            }
            else if (control.GetType().FullName.Equals("DevComponents.DotNetBar.Bar"))
            {
                DevComponents.DotNetBar.Bar br = control as DevComponents.DotNetBar.Bar;

                for (int i = 0; i < br.Items.Count; i++)
                {
                    if (br.Items[i].GetType().FullName != "DevComponents.DotNetBar.ExpandablePanel" && br.Items[i].GetType().FullName != "System.Windows.Forms.Panel" && br.Items[i].GetType().FullName != "System.Windows.Forms.TabControl" && br.Items[i].GetType().FullName != "DevComponents.DotNetBar.PanelEx" && br.Items[i].GetType().FullName != "System.Windows.Forms.GroupBox")
                    {
                        if (br.Items[i].GetType().FullName == "DevComponents.DotNetBar.LabelItem")
                        {
                            DataRow = dtArabic.Select("ControlName='" + br.Items[i].Name + "'");
                            if (DataRow != null)
                            {
                                if (DataRow.Length > 0)
                                    br.Items[i].Text = Convert.ToString(DataRow[0]["ArabicText"]);
                            }
                        }
                        else if (br.Items[i].GetType().FullName == "DevComponents.DotNetBar.ButtonItem")
                        {
                            DataRow = dtArabic.Select("ControlName='" + br.Items[i].Name + "'");
                            if (DataRow != null)
                            {
                                if (DataRow.Length > 0)
                                {
                                    br.Items[i].Text = Convert.ToString(DataRow[0]["ArabicText"]);
                                    br.Items[i].Tooltip = Convert.ToString(DataRow[0]["ArabicToolTip"]);
                                }
                            }
                        }
                    }
                    else
                        SetControls(br.Controls[i]);
                }
            }
            else if (control.GetType().FullName.Equals("System.Windows.Forms.DataGridView"))
            {
                DataGridView Dgv = control as DataGridView;
                DataRow = dtArabic.Select("ControlName='" + Dgv.Name + "'");
                if (DataRow != null)
                {
                    if (DataRow.Length > 0)
                        Dgv.Location = new System.Drawing.Point(DataRow[0]["LocationX"].ToInt32(), DataRow[0]["LocationY"].ToInt32());
                }

                for (int i = 0; i < Dgv.Columns.Count; i++)
                {
                    DataRow = dtArabic.Select("ControlName='" + Dgv.Columns[i].Name + "'");
                    if (DataRow != null)
                    {
                        if (DataRow.Length > 0)
                            Dgv.Columns[i].HeaderText = Convert.ToString(DataRow[0]["ArabicText"]);
                    }
                }
            }
        }

        private DataTable GetArabicConrols(int intFormID)
        {
            ArrayList sqlParam = new ArrayList();
            sqlParam.Add(new SqlParameter("@Mode", 2));
            sqlParam.Add(new SqlParameter("@FormID", intFormID));
            return new clsConnection().FillDataTable("spArabicConversion", sqlParam);
        }
    }
}
