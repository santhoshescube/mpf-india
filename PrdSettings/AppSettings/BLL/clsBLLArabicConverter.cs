﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public class clsBLLArabicConverter
    {
        ClsDALArabicConverter objDALArabicConverter;

        public clsBLLArabicConverter()
        {
            objDALArabicConverter = new ClsDALArabicConverter();
        }
        public void SetArabicVersion(int intFormID, Form objFrm)
        {
            objDALArabicConverter.SetArabicVersion(intFormID, objFrm);
        }

        public void GetControls(Control cntrl, int intFormID)
        {
            objDALArabicConverter.GetControls(cntrl, intFormID);
        }

        public void InsertArabicVersion(string strName, string strToolTip, string strText, int intLocX, int intLocY, int intFormID)
        {
            objDALArabicConverter.InsertArabicVersion(strName, strToolTip, strText, intLocX, intLocY, intFormID);
        }
    }
}
