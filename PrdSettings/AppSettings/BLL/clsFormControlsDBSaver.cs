﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public class clsFormControlsDBSaver
    {

        public void ConvertAndSave(Form obj, string strName, string strText,int intFormID)
        {
            ////FrmCompanyArb objC = new FrmCompanyArb();
            //FrmTemplateDesignArb.ControlCollection obj = new ControlCollection(this);
            ////int i = ((System.Windows.Forms.Layout.ArrangedElementCollection)(((System.Windows.Forms.Control.ControlCollection)(obj)).Owner.Controls)).Count;
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.InsertArabicVersion(strName, "", strText, 0, 0, intFormID);
            foreach (Control cntrl in obj.Controls)
            {
                objDAL.GetControls(cntrl, intFormID);
            }
        }
    }
}
