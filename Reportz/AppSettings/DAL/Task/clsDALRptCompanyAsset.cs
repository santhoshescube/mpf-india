﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 22 AUG 2013
* Description      : Company Asset Report DAL

* ******************************************************/
namespace MyPayfriend
{
    public class clsDALRptCompanyAsset
    {

        public static DataSet DisplayCompanyAssetReport(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, int AssetType, int CompanyAsset, int StatusType, DateTime FromDate, DateTime ToDate, bool IncludeComp) 
        {
            ArrayList alParameters = new ArrayList();
            if (CompanyAsset > 0)
            {
                alParameters.Add(new SqlParameter("@Mode", 2));
            }
            else
            {
                alParameters.Add(new SqlParameter("@Mode", 1));
            }
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@BranchID", BranchID));
            alParameters.Add(new SqlParameter("@DepartmentID",DepartmentID));
            alParameters.Add(new SqlParameter("@WorkStatusID",WorkStatusID)); 
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@BenefitTypeID", AssetType));
            alParameters.Add(new SqlParameter("@CompanyAssetID", CompanyAsset));
            alParameters.Add(new SqlParameter("@StatusTypeID", StatusType));
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
            alParameters.Add(new SqlParameter("@IncludeComp", IncludeComp));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataSet("spPayRptAssets", alParameters);
        }

        public static DataTable GetAllCompanyAssets(int AssetType, int CompanyAsset, int StatusType, DateTime FromDate, DateTime ToDate, int CompanyID, int BranchID, bool IncludeComp)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 3));
            alParameters.Add(new SqlParameter("@BenefitTypeID", AssetType));
            alParameters.Add(new SqlParameter("@CompanyAssetID", CompanyAsset));
            alParameters.Add(new SqlParameter("@StatusTypeID", StatusType));
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@BranchID", BranchID));
            alParameters.Add(new SqlParameter("@IncludeComp", IncludeComp));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("spPayRptAssets", alParameters);
        }

    }
}
