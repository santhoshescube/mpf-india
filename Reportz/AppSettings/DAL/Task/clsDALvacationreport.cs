﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 22 AUG 2013
* Description      : Vacation Report DAL

* ******************************************************/
namespace MyPayfriend
{
    public class clsDALvacationreport
    {/**/
        public clsDALvacationreport() { }
        public static DataTable Getvacation(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate, int SearchType)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       
                                new SqlParameter("@Mode", 1),
                      
                        new SqlParameter("@CompanyID",CompanyID), 
                        new SqlParameter("@BranchID",BranchID),
                        new SqlParameter("@DepartmentID",DepartmentID),
                        new SqlParameter("@WorkStatusID",WorkStatusID) ,
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@IncludeComp",IncludeCompany),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                        new SqlParameter("@SearchType",SearchType),
  new SqlParameter("@UserID",ClsCommonSettings.UserID)

                    
                        };
            return new DataLayer().ExecuteDataTable("spPayRptVacationReport", sqlParameter);
        }
        public static DataTable GetvacationDetails(int EmployeeID, DateTime FromDate, DateTime ToDate)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       
               
                       new SqlParameter("@Mode",2), 
                      
                        new SqlParameter("@EmployeeID",EmployeeID),
                       
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                       
                        
                        };
            return new DataLayer().ExecuteDataTable("spPayRptVacationReport", sqlParameter);
        }
        public static DataTable GetvacationDetailsForGrid(int EmployeeID, DateTime FromDate, DateTime ToDate, int WorkStatusID, int CompanyID, int BranchID, int DepartmentID, bool IncludeCompany)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       
               
                       new SqlParameter("@Mode",3), 
                        new SqlParameter("@CompanyID",CompanyID), 
                        new SqlParameter("@BranchID",BranchID),
                        new SqlParameter("@DepartmentID",DepartmentID),
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@IncludeComp",IncludeCompany),
                        new SqlParameter("@WorkStatusID",WorkStatusID) ,
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                       new SqlParameter("@UserID",ClsCommonSettings.UserID)
                        
                        };
            return new DataLayer().ExecuteDataTable("spPayRptVacationReport", sqlParameter);
        }
    }
}
