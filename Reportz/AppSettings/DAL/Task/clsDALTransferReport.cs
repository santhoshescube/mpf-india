﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 22 AUG 2013
* Description      : transfer Report DAL

* ******************************************************/
namespace MyPayfriend
{
  public  class clsDALTransferReport
    {
      public clsDALTransferReport() { }

      public static DataTable GetTransferDetails( int EmployeeID, DateTime FromDate, DateTime ToDate)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       
                        new SqlParameter("@Mode", 1),
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@UserID",ClsCommonSettings.UserID),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate)
                       
                        };
            return new DataLayer().ExecuteDataTable("spPayRptTransferReport", sqlParameter);
        }
    }
}
