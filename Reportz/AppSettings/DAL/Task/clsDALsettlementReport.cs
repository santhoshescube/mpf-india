﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 24 AUG 2013
* Description      : Settlement Report DAL

* ******************************************************/
namespace MyPayfriend
{
    public class clsDALsettlementReport
    {
        public static DataTable GetSettlement(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate,int SettlementType)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       
                        new SqlParameter("@Mode", 1),
                        new SqlParameter("@CompanyID",CompanyID), 
                        new SqlParameter("@BranchID",BranchID),
                        new SqlParameter("@DepartmentID",DepartmentID),
                        new SqlParameter("@DesignationID",DesignationID),
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@SettlementType",SettlementType),
                        new SqlParameter("@IncludeComp",IncludeCompany),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                          new SqlParameter("@UserID",ClsCommonSettings.UserID)

                       
                        };
            return new DataLayer().ExecuteDataTable("spPayRptSettlement", sqlParameter);
        }
        public static DataTable GetSettlementDetails(int EmployeeID, DateTime FromDate, DateTime ToDate,int SettlementType)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       new SqlParameter("@Mode",2), 
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@SettlementType",SettlementType),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                        };
            return new DataLayer().ExecuteDataTable("spPayRptSettlement", sqlParameter);
        }
    }
}
