﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 22 AUG 2013
* Description      : Expense Report DAL

* ******************************************************/
namespace MyPayfriend
{
  public  class clsDALexpenseReport
    {
      public clsDALexpenseReport() { }
        
        public static DataTable GetExpenseDetails(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID ,int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate,int ExpenseType)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       
                        new SqlParameter("@Mode", 1),
                        new SqlParameter("@CompanyID",CompanyID), 
                        new SqlParameter("@BranchID",BranchID),
                        new SqlParameter("@DepartmentID",DepartmentID),
                        new SqlParameter("@WorkStatusID",WorkStatusID) ,
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@IncludeComp",IncludeCompany),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                        new SqlParameter("@ExpenseType",ExpenseType),
                         new SqlParameter("@UserID",ClsCommonSettings.UserID)
                        };
            return new DataLayer().ExecuteDataTable("spPayRptExpenseReport", sqlParameter);
        }
    }
}
