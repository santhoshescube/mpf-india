﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    /*****************************************************
    * Created By       : HIMA
    * Creation Date    : 22 AUG 2013
    * Description      : Salary Advance Report DAL

    * ******************************************************/
   public class clsDALSalaryAdvanceReport
    {
        public clsDALSalaryAdvanceReport() { }
        public static DataTable GetSalaryAdvanceOrLoanSummary(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate, int IsSalaryReport, bool IsClosed)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                        IsSalaryReport == 0?new SqlParameter("@Mode", 1):new SqlParameter("@Mode", 2),
                        new SqlParameter("@CompanyID",CompanyID), 
                        new SqlParameter("@BranchID",BranchID),
                        new SqlParameter("@DepartmentID",DepartmentID),
                       
                        new SqlParameter("@WorkStatusID",WorkStatusID) ,
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@IncludeCompany",IncludeCompany),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                        new SqlParameter("@IsClosed",IsClosed),
                        new SqlParameter("@UserID",ClsCommonSettings.UserID),
                      
                        };
            return new DataLayer().ExecuteDataTable("spPayRptSalaryAdvanceSummary", sqlParameter);
        }
        public static DataTable GetLoanDetails(int EmployeeID, DateTime FromDate, DateTime ToDate, bool IsClosed)
        {
            List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                        new SqlParameter("@Mode", 3),
                        
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@IsClosed",IsClosed),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate)
                        
                        };
            return new DataLayer().ExecuteDataTable("spPayRptSalaryAdvanceSummary", sqlParameter);
        }




     
    }
}
