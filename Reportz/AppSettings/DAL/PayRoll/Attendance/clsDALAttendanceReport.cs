﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;



namespace MyPayfriend
{
/*****************************************************
* Created By       : Arun
* Creation Date    : 24 Apr 2012
* Description      : Handle Attendance Reports Summary

* ******************************************************/
    public class clsDALAttendanceReport
    {

        public clsDALAttendanceReport() { }
       
        public static DataTable GetReport(int CompanyID, int BranchID, bool IncludeCompany,int DepartmentID,int DesignationID, int WorkStatusID, int EmployeeID, bool IsMonth, DateTime AttendanceDate,string From,string To)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@CompanyID",CompanyID));
            alParameters.Add(new SqlParameter("@BranchID",BranchID));
            alParameters.Add(new SqlParameter("@IncludeCompany",IncludeCompany));
            alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
            alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
            alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            alParameters.Add(new SqlParameter("@EmployeeID",EmployeeID));
            alParameters.Add(new SqlParameter("@IsMonth",IsMonth));
            //alParameters.Add(new SqlParameter("@Date",AttendanceDate));
            alParameters.Add(new SqlParameter("@Date", From));
            alParameters.Add(new SqlParameter("@ToDate",To));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("spPayRptAttendace", alParameters);
        }

        public static DataSet GetPunchingReport(int EmployeeID,string From,string To)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 2));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@Date", From));
            alParameters.Add(new SqlParameter("@ToDate", To));
            return new DataLayer().ExecuteDataSet("spPayRptAttendace", alParameters);
        }
        public static DataSet GetPunchingLogReport(string AttendanceDate, string AttendanceDateTo, int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "AGEP"));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@AttDate1", AttendanceDate));
            alParameters.Add(new SqlParameter("@AttDateTo1", AttendanceDateTo));
            alParameters.Add(new SqlParameter("@BranchID", BranchID));
            alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
            alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
            alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataSet("HRSpAttDailyReport", alParameters);
        }
        public static DataSet GetPunchingLogReport(int EmployeeID, string AttendanceDate, string AttendanceDateTo, int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "AGEP"));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@AttDate1", AttendanceDate));
            alParameters.Add(new SqlParameter("@AttDateTo1", AttendanceDateTo));
            alParameters.Add(new SqlParameter("@BranchID", BranchID));
            alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
            alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
            alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataSet("HRSpAttDailyReport", alParameters);
        }
       public static DataTable GetSummaryReport(int CompanyID, int BranchID, bool IncludeCompany,int DepartmentID,int DesignationID, int WorkStatusID, int EmployeeID,string From,string To)
       {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 3));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@BranchID", BranchID));
            alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
            alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
            alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@Date", From));
            alParameters.Add(new SqlParameter("@ToDate", To));
            alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            return new DataLayer().ExecuteDataTable("spPayRptAttendace", alParameters);
        }

       public static DataTable GetEmployeeAttendnaceAllEmployee(int intMode, int intCompanyID, int intBranchID, int intEmployeeID, int intIncludeCompany, int intLocationID,string From, string To)
       {
           //intMode=5 All Employee in one month
           //intMode=9 All Employee in one Day
            List<SqlParameter> sqlParameters = null;
            sqlParameters = new List<SqlParameter> 
            { 
                new SqlParameter("@Mode", intMode),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID", intBranchID) ,
                new SqlParameter("@EmployeeID", intEmployeeID) ,
                new SqlParameter("@IncludeComp", intIncludeCompany),
                new SqlParameter("@LocationID", intLocationID),
                new SqlParameter("@StartDate", From),
                new SqlParameter("@EndDate",To),
                new SqlParameter("@UserID", ClsCommonSettings.UserID)
           };
           return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", sqlParameters);

       }
       /// <summary>
       /// Getting day Summary of a prticular day
       /// </summary>
       /// <param name="intCompanyID"></param>
       /// <param name="intEmployeeID"></param>
       /// <param name="intDayID"></param>
       /// <param name="strDate"></param>
       /// <returns></returns>
       public static DataTable GetEmployeeDetailsAttendance(int intEmployeeID, int intDayID, string strDate, int intLocationID)
       {
           //new SqlParameter("@CompanyID",intCompanyID),
           List<SqlParameter> sqlParameters = null;
           sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 6),
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@DayID", intDayID) ,
                new SqlParameter("@Date", strDate),
                     new SqlParameter("@LocationID", intLocationID)
                 
                  };
           return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", sqlParameters);

       }
       /// <summary>
       /// Entry exit details of a particular day
       /// </summary>
       /// <param name="intCompanyID"></param>
       /// <param name="intEmployeeID"></param>
       /// <param name="intDayID"></param>
       /// <param name="strDate"></param>
       /// <returns></returns>
       public static DataTable GetEmployeeDayEntryExitDetails(int intEmployeeID, string strDate, int intLocationID)
       {//new SqlParameter("@CompanyID",intCompanyID), 
           List<SqlParameter> sqlParameters = null;
           sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 7),
                new SqlParameter("@EmployeeID", intEmployeeID),
                 new SqlParameter("@Date", strDate),
                    new SqlParameter("@LocationID", intLocationID)
                  };
           return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", sqlParameters);
       }

       public static DataTable GetEmployeeSummary(int intCompanyID, int intEmployeeID, int intBranchID, string strStartDate, string strEndDate, int intIncludeCompany, int intLocationID)
       {
           List<SqlParameter> sqlParameters = null;
           sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 8),
                new SqlParameter("@CompanyID",intCompanyID), 
                 new SqlParameter("@EmployeeID", intEmployeeID),
                  new SqlParameter("@BranchID", intBranchID),
                 new SqlParameter("@StartDate", strStartDate),
                 new SqlParameter("@EndDate", strEndDate),
                  new SqlParameter("@IncludeComp", intIncludeCompany),
                    new SqlParameter("@LocationID", intLocationID),
                    new SqlParameter("@UserID", ClsCommonSettings.UserID),
                                   };
           return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", sqlParameters);
       }

       public static DataTable GetAttendanceDraft(int intCompanyID, int intEmployeeID, int intBranchID, string strStartDate, string strEndDate, int intIncludeCompany, int DesignationID, int DepartmentID)
       {
           List<SqlParameter> sqlParameters = null;
           sqlParameters = new List<SqlParameter>
           { 
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@BranchID", intBranchID),
                new SqlParameter("@FromDate", strStartDate),
                new SqlParameter("@ToDate", strEndDate),
                new SqlParameter("@IncludeComp", intIncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@DepartmentID", DepartmentID),
                new SqlParameter("@Mode", 1),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),

            };
           return new DataLayer().ExecuteDataTable("spPayRptEmployeeDraft", sqlParameters);
       }


       public static DataTable GetAttendanceDraft2(int intCompanyID, int intEmployeeID, int intBranchID, string strStartDate, string strEndDate, int intIncludeCompany, int DesignationID, int DepartmentID)
       {
           List<SqlParameter> sqlParameters = null;
           sqlParameters = new List<SqlParameter>
           { 
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@BranchID", intBranchID),
                new SqlParameter("@FromDate", strStartDate),
                new SqlParameter("@ToDate", strEndDate),
                new SqlParameter("@IncludeComp", intIncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@DepartmentID", DepartmentID),
                new SqlParameter("@Mode", 2),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),

            };
           return new DataLayer().ExecuteDataTable("spPayRptEmployeeDraft", sqlParameters);
       }

       public static DataSet GetPunchingLogReportGrid(int EmployeeID, string AttendanceDate, string AttendanceDateTo, 
           int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
       {
           ArrayList alParameters = new ArrayList();
           alParameters.Add(new SqlParameter("@Mode", "AGEP"));
           alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
           alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
           alParameters.Add(new SqlParameter("@AttDate1", AttendanceDate));
           alParameters.Add(new SqlParameter("@AttDateTo1", AttendanceDateTo));
           alParameters.Add(new SqlParameter("@BranchID", BranchID));
           alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
           alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
           alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
           alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
           alParameters.Add(new SqlParameter("@ReportView", 2)); // Grid Format
           alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
           return new DataLayer().ExecuteDataSet("HRSpAttDailyReport", alParameters);
       }

       public static DataSet GetPunchingLogReportGrid(string AttendanceDate, string AttendanceDateTo, 
           int CompanyID, int BranchID,bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
       {
           ArrayList alParameters = new ArrayList();
           alParameters.Add(new SqlParameter("@Mode", "AGEP"));
           alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
           alParameters.Add(new SqlParameter("@AttDate1", AttendanceDate));
           alParameters.Add(new SqlParameter("@AttDateTo1", AttendanceDateTo));
           alParameters.Add(new SqlParameter("@BranchID", BranchID));
           alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
           alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
           alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
           alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
           alParameters.Add(new SqlParameter("@ReportView", 2)); // Grid Format
           alParameters.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
           return new DataLayer().ExecuteDataSet("HRSpAttDailyReport", alParameters);
       }
    }
}
