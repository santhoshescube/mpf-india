﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALRptLocationSchedule
    {
        private List<SqlParameter> SqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALRptLocationSchedule() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetReport(int intCompanyID, int intBranchID, int intIncludeCompany, int intEmployeeID, int intLocationID , DateTime FromDate,DateTime ToDate)
        {
            this.SqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID",intBranchID),
                new SqlParameter("@IncludeCompany",intIncludeCompany),
                new SqlParameter("@WorkLocationID",intLocationID),
                new SqlParameter("@EmployeeID",intEmployeeID),
                new SqlParameter("@FromDate",FromDate),
                new SqlParameter("@ToDate",ToDate),
                };
            return DataLayer.ExecuteDataTable("spPayRptLocationSchedule", this.SqlParameters);
        }

        public DataTable GetCompanyHeader(int intCompanyID)
        {
            this.SqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",intCompanyID) 
           };
            return DataLayer.ExecuteDataTable("spPayRptLocationSchedule", this.SqlParameters);
        }

      

    }
}
