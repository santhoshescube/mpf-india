﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALRptPayments
    {
        public clsDALRptPayments() { }


        public static DataTable GetMonthlyPaymentsOne(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, int IsReleased, string fromdate, string todate, int ReportView, bool ExcludeSettlement)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@FrmDate", fromdate),
                new SqlParameter("@TooDate", todate),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@IsReleased", IsReleased) ,
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                 new SqlParameter("@ReportView", ReportView) 
            };
            return new DataLayer().ExecuteDataTable("spPayRptPaymentsOne", sqlParameters);
        }


        public static DataTable GetMonthlyPaymentsOne(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate, int IsReleased, int ReportView, bool ExcludeSettlement)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                new SqlParameter("@Year", Year),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@PaymentDate1", PaymentDate),
                new SqlParameter("@IsReleased", IsReleased), 
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                new SqlParameter("@ReportView", ReportView) 
            };
            return new DataLayer().ExecuteDataTable("spPayRptPaymentsOne", sqlParameters);
        }

        public static DataTable GetMonthlyPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate, int IsReleased, bool ExcludeSettlement)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                new SqlParameter("@Year", Year),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@PaymentDate1", PaymentDate),
                new SqlParameter("@IsReleased", IsReleased),
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                new SqlParameter("@IsPayStructureOnly", IsPayStructureOnly),                  
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }

        public static DataTable GetMonthlyPaymentSummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate, int IsReleased, bool ExcludeSettlement)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 5),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                new SqlParameter("@Year", Year),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@PaymentDate1", PaymentDate),
                new SqlParameter("@IsReleased", IsReleased),
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                new SqlParameter("@IsPayStructureOnly", IsPayStructureOnly),                  
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }

        public static DataTable GetMonthlyPaymentSummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate, int IsReleased, bool ExcludeSettlement, string FromDate, string ToDate)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 5),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                new SqlParameter("@Year", Year),
                new SqlParameter("@FromDate", FromDate),
                new SqlParameter("@ToDate", ToDate),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@PaymentDate1", PaymentDate),
                new SqlParameter("@IsReleased", IsReleased),
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                new SqlParameter("@IsPayStructureOnly", IsPayStructureOnly),                  
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }



        public static DataTable GetSummaryPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate, bool IncludeCompany, int TransactionTypeID, string PaymentDate, int IsReleased, bool ExcludeSettlement)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@FromDate", FromDate.AddDays(-FromDate.Day+1)),
                new SqlParameter("@ToDate", ToDate.AddMonths(1).AddDays(-ToDate.Day)),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@PaymentDate1", PaymentDate),
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                new SqlParameter("@IsReleased", IsReleased),
                 new SqlParameter("@UserID", ClsCommonSettings.UserID)
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }

        public static DataTable GetMonthlyPaymentsProcessed(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string FromDate, string ToDate, int Istransfer, bool ExcludeSettlement)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 4),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                new SqlParameter("@Year", Year),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@FromDate", FromDate),
                new SqlParameter("@ToDate", ToDate),
                new SqlParameter("@IsReleased", 0),
                new SqlParameter("@IsPayStructureOnly", IsPayStructureOnly),
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                new SqlParameter("@Istransfer", Istransfer),
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }

        public static DataTable GetSalaryDetails(int intCompanyID, int intBranchID, bool intIncludeCompany, int intEmployeeID, string date)
         {
            List<SqlParameter> sqlParameters = new List<SqlParameter>{ 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID",intBranchID),
                new SqlParameter("@IncludeComp",intIncludeCompany),               
                new SqlParameter("@EmployeeID",intEmployeeID),  
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                 new SqlParameter("@Date", date)
                };
            return new DataLayer().ExecuteDataTable("spPayRptAccrual", sqlParameters);
        }

        public static DataSet GetGratuityDetails(int intCompanyID, int intEmployeeID,string FromDate,string ToDate,int SettlementExp,int SepType)
        {            
            List<SqlParameter> sqlParameters = new List<SqlParameter>{ 
               
                new SqlParameter("@CompID",intCompanyID),                              
                new SqlParameter("@EmployeeID",intEmployeeID),               
                new SqlParameter("@FromDate1",FromDate), 
                new SqlParameter("@ToDate1",ToDate), 
                new SqlParameter("@SepType",SepType), 
                new SqlParameter("@SettlementExp",SettlementExp), 
                new SqlParameter("@BalanceLeavePay", "0")
                };
            return new DataLayer().ExecuteDataSet("spPayFinalSettlementProcess", sqlParameters);
        }


        public static DataSet GetGratuityDetailsAccural(int intCompanyID, int intEmployeeID, string FromDate, string ToDate, int SettlementExp, int SepType)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>{ 
               
                new SqlParameter("@CompID",intCompanyID),                              
                new SqlParameter("@EmployeeID",intEmployeeID),               
                new SqlParameter("@FromDate1",FromDate), 
                new SqlParameter("@ToDate1",ToDate), 
                new SqlParameter("@SepType",SepType), 
                new SqlParameter("@SettlementExp",SettlementExp), 
                new SqlParameter("@BalanceLeavePay", "0")
                };
            return new DataLayer().ExecuteDataSet("spRptFinalSettlement", sqlParameters);
        }


        public static DataSet GetVacationDetails(int intCompanyID,int intEmployeeID,string date)
        {
             List<SqlParameter> sqlParameters = new List<SqlParameter>{ 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",intCompanyID),                    
                new SqlParameter("@EmployeeID",intEmployeeID),  
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@Date", date)
                };
             return new DataLayer().ExecuteDataSet("spPayRptAccrual", sqlParameters);

                   
        }

        public static DataTable GetMonthlyPaymentsProcessedGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, 
            int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string FromDate,
            string ToDate, int Istransfer, bool ExcludeSettlement)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 4),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                new SqlParameter("@Year", Year),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@FromDate", FromDate),
                new SqlParameter("@ToDate", ToDate),
                new SqlParameter("@IsReleased", 0),
                new SqlParameter("@IsPayStructureOnly", IsPayStructureOnly),
                new SqlParameter("@Istransfer", Istransfer),
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                new SqlParameter("@ReportView", 2), // Grid Format
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }

        public static DataTable GetMonthlyPaymentsGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, 
            int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate,
            int IsReleased, bool ExcludeSettlement)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                new SqlParameter("@Year", Year),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@PaymentDate1", PaymentDate),
                new SqlParameter("@IsReleased", IsReleased),
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                new SqlParameter("@IsPayStructureOnly", IsPayStructureOnly),
                new SqlParameter("@ReportView", 2), // Grid Format  
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }

        public static DataTable GetSummaryPaymentsGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID,
            int EmployeeID, DateTime FromDate, DateTime ToDate, bool IncludeCompany, int TransactionTypeID, string PaymentDate, int IsReleased, bool ExcludeSettlement)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@FromDate", FromDate.AddDays(-FromDate.Day+1)),
                new SqlParameter("@ToDate", ToDate.AddMonths(1).AddDays(-ToDate.Day)),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@PaymentDate1", PaymentDate),
                new SqlParameter("@IsReleased", IsReleased),
                new SqlParameter("@ExcludeSettlement", ExcludeSettlement), 
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                new SqlParameter("@ReportView", 2), // Grid Format  
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }
        public static DataTable GetSalaryDetailsFromTo(int intCompanyID, int intBranchID, bool intIncludeCompany, int intEmployeeID, string Fromdate, string Todate)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>{ 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID",intBranchID),
                new SqlParameter("@IncludeComp",intIncludeCompany),               
                new SqlParameter("@EmployeeID",intEmployeeID),  
                new SqlParameter("@UserID", ClsCommonSettings.UserID),
                 new SqlParameter("@FromDateRpt", Fromdate),
                 new SqlParameter("@ToDateRpt", Todate)
                };
            return new DataLayer().ExecuteDataTable("spPayRptAccrual", sqlParameters);
        }

        public static DataSet GetGratuityDetailsAccural(int intCompanyID,int intEmployeeID, string FromDate, string ToDate,int SettlementExp,int SepType,int IsUpdate)
        {
            return new DataLayer().ExecuteDataSet("spRptFinalSettlement", new List<SqlParameter>()
              {
                new SqlParameter("@CompID", intCompanyID),
                new SqlParameter("@EmployeeID",  intEmployeeID),
                new SqlParameter("@FromDate1",  FromDate),
                new SqlParameter("@ToDate1",  ToDate),
                new SqlParameter("@SepType",  SepType),
                new SqlParameter("@SettlementExp", SettlementExp),
                new SqlParameter("@IsActuralUpdate",  IsUpdate),
                new SqlParameter("@BalanceLeavePay",  "0")
              });
        }

        public static DataSet GetVacationDetails(int intCompanyID, int intEmployeeID, string date,int IsUpdate)
        {
            return new DataLayer().ExecuteDataSet("spPayRptAccrual", new List<SqlParameter>()
              {
                new SqlParameter("@Mode",  2),
                new SqlParameter("@CompanyID",  intCompanyID),
                new SqlParameter("@EmployeeID",  intEmployeeID),
                new SqlParameter("@UserID",  ClsCommonSettings.UserID),
                new SqlParameter("@IsActuralUpdate",  IsUpdate),
                new SqlParameter("@Date",  date)
              });
        }
    }
}