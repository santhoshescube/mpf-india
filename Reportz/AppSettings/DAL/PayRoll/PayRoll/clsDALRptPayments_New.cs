﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALRptPayments_New
    {
         private List<SqlParameter> sqlParameters = null;
        DataLayer MobjDataLayer = null;

        public clsDALRptPayments_New() { }

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable DisplayCompanyReportHeader(int intCompanyID) //CompnayReportHeader
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 31),
                new SqlParameter("@CompanyID",intCompanyID)
                };
            return DataLayer.ExecuteDataTable("spPayModuleFunctions", this.sqlParameters);

        }

        public DataTable GetMonthlyPayments(int intCompanyID, int intBranchID, int intDepartmentID, int intEmployeeID, int intMonth, int intYear, int intIncludeComp, int intLocationID)
        {
                this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID", intBranchID) ,
                new SqlParameter("@DepartmentID", intDepartmentID) ,
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@Month", intMonth),
                new SqlParameter("@Year", intYear),
                new SqlParameter("@EmpVenFlag",1),
                new SqlParameter("@IncludeComp", intIncludeComp),
                new SqlParameter("@WorkLocationID", intLocationID)
                  
                     };
                return DataLayer.ExecuteDataTable("spPayRptPayments_New", this.sqlParameters);
        }

        public DataTable GetSummaryPayments(int intCompanyID, int intBranchID, int intDepartmentID, int intEmployeeID, string strFromDate, string strToDate, int intIncludeComp, int intLocationID)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID", intBranchID) ,
                new SqlParameter("@DepartmentID", intDepartmentID) ,
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@FromDate1", strFromDate),
                new SqlParameter("@ToDate1", strToDate),
               // new SqlParameter("@EmpVenFlag",1),
                new SqlParameter("@IncludeComp", intIncludeComp),
                 new SqlParameter("@WorkLocationID", intLocationID)
                  
                     };
            return DataLayer.ExecuteDataTable("spPayRptPayments_New", this.sqlParameters);
        }

        public DataTable GetFinancialYear(int intEmployeeID, DateTime dteFrom, DateTime dteTo)
        {
            this.sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@EmployeeID",intEmployeeID), 
                new SqlParameter("@From",dteFrom),
                new SqlParameter("@To", dteTo)
                  
                     };
            return DataLayer.ExecuteDataTable("spPayRptPayments_New", this.sqlParameters);
        }




    }
}
