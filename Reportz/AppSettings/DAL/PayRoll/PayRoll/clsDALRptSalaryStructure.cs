﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    class clsDALRptSalaryStructure
    {
       
        public static DataTable GetSalaryStructureSummary(int CompanyID, int BranchID, int DepartmentID,int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp,int EmployeeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@DesignationID", DesignationID) ,
                new SqlParameter("@WorkStatusID", WorkStatusID) ,
                new SqlParameter("@EmploymentTypeID", EmploymentTypeID),
                new SqlParameter("@IncludeComp", IncludeComp),
                new SqlParameter("@EmployeeID",EmployeeID),
                 new SqlParameter("@UserID",ClsCommonSettings.UserID)
                  
                     };
            return new  DataLayer().ExecuteDataTable("spPayRptSalaryStructure", sqlParameters);
        }

        public static DataTable GetSalaryStructureHistorySummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@DesignationID", DesignationID) ,
                new SqlParameter("@WorkStatusID", WorkStatusID) ,
                new SqlParameter("@EmploymentTypeID", EmploymentTypeID),
                new SqlParameter("@IncludeComp", IncludeComp),
                new SqlParameter("@EmployeeID",EmployeeID),
                new SqlParameter("@UserID",ClsCommonSettings.UserID)
                  
                     };
            return new DataLayer().ExecuteDataTable("spPayRptSalaryStructure", sqlParameters);
        }


        public static DataTable GetSalaryStructureSummaryGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@DesignationID", DesignationID) ,
                new SqlParameter("@WorkStatusID", WorkStatusID) ,
                new SqlParameter("@EmploymentTypeID", EmploymentTypeID),
                new SqlParameter("@IncludeComp", IncludeComp),
                new SqlParameter("@EmployeeID",EmployeeID),
                 new SqlParameter("@UserID",ClsCommonSettings.UserID)
                  
                     };
            return new DataLayer().ExecuteDataTable("spPayRptSalaryStructureGrid", sqlParameters);
        }
        public static DataTable GetSalaryStructureSummaryGridHistory(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@DesignationID", DesignationID) ,
                new SqlParameter("@WorkStatusID", WorkStatusID) ,
                new SqlParameter("@EmploymentTypeID", EmploymentTypeID),
                new SqlParameter("@IncludeComp", IncludeComp),
                new SqlParameter("@EmployeeID",EmployeeID),
                 new SqlParameter("@UserID",ClsCommonSettings.UserID)
                  
                     };
            return new DataLayer().ExecuteDataTable("spPayRptSalaryStructureGrid", sqlParameters);
        }
        public static DataTable GetSalaryStructureDetailsGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@DesignationID", DesignationID) ,
                new SqlParameter("@WorkStatusID", WorkStatusID) ,
                new SqlParameter("@EmploymentTypeID", EmploymentTypeID),
                new SqlParameter("@IncludeComp", IncludeComp),
                new SqlParameter("@EmployeeID",EmployeeID),
                 new SqlParameter("@UserID",ClsCommonSettings.UserID)
                  
                     };
            return new DataLayer().ExecuteDataTable("spPayRptSalaryStructureGrid", sqlParameters);
        }
    }

}
