﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MyPayfriend
{
    class clsDALRptEmployeeDeduction
    {

        public static DataTable GetReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int DeductionID, string Month, int Year)
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 1));
            alParameters.Add(new SqlParameter("@CompanyID",CompanyID));
            alParameters.Add(new SqlParameter("@BranchID",BranchID));
            alParameters.Add(new SqlParameter("@IncludeCompany",IncludeCompany));
            alParameters.Add(new SqlParameter("@DepartmentID",DepartmentID));
            alParameters.Add(new SqlParameter("@DesignationID",DesignationID));
            alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
            alParameters.Add(new SqlParameter("@EmployeeID",EmployeeID));
            alParameters.Add(new SqlParameter("@AdditionDeductionID",DeductionID));
            alParameters.Add(new SqlParameter("@Month",Month));
            alParameters.Add(new SqlParameter("@Year",Year));
            alParameters.Add(new SqlParameter("@UserID",ClsCommonSettings.UserID));
             
            return new DataLayer().ExecuteDataTable("spRptEmployeeDeduction", alParameters);
        }
    }
}
