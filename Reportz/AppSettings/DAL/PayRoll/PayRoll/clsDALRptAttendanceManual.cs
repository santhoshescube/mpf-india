﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    public class clsDALRptAttendanceManual
    {
        DataLayer MobjDataLayer = null;
        List<SqlParameter> SqlParameters = null;

        public DataLayer DataLayer
        {
            get
            {
                if (this.MobjDataLayer == null)
                    this.MobjDataLayer = new DataLayer();

                return this.MobjDataLayer;
            }
            set
            {
                this.MobjDataLayer = value;
            }
        }

        /// <summary>
        /// 
        /// return datatable  for filling combos
        /// </summary>
        /// <param name="saFieldValues"></param>
        /// <returns></returns>
        public DataTable FillCombos(string[] saFieldValues)
        {
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = DataLayer;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }

        public DataTable GetAttendanceMonths(int intCompanyID, int intBranchID, int intIncludeCompany)
        {
                this.SqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID",intBranchID),
                new SqlParameter("@IncludeCompany",intIncludeCompany)
                };
                return DataLayer.ExecuteDataTable("spPayRptAttendanceManual", this.SqlParameters);
        }

        public DataTable GetReport(int intCompanyID, int intBranchID, int intIncludeCompany, int intLocationID, int intEmployeeID, int intMonth, int intYear, DateTime dtAttendanceDate)
        {
            this.SqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID",intBranchID),
                new SqlParameter("@IncludeCompany",intIncludeCompany),
                new SqlParameter("@WorkLocationID",intLocationID),
                new SqlParameter("@EmployeeID",intEmployeeID),
                new SqlParameter("@Month",intMonth),
                new SqlParameter("@Year",intYear),
                new SqlParameter("@AttendanceDate",dtAttendanceDate)
                };
            return DataLayer.ExecuteDataTable("spPayRptAttendanceManual", this.SqlParameters);
        }

        public DataTable GetCompanyHeader(int intCompanyID)
        {
            this.SqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 3),
                new SqlParameter("@CompanyID",intCompanyID) 
           };
            return DataLayer.ExecuteDataTable("spPayRptAttendanceManual", this.SqlParameters);
        }


        public DataTable GetReportWorkSheet(int intCompanyID, int intBranchID, int intIncludeCompany, int intDepartmentID, int intEmployeeID, int intMonth, int intYear, DateTime dtAttendanceDate)
        {
            this.SqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID",intBranchID), 
                new SqlParameter("@DepartmentID",intDepartmentID),
                new SqlParameter("@EmployeeID",intEmployeeID),
                new SqlParameter("@IncludeCompany",intIncludeCompany),
                new SqlParameter("@AttendanceDate",dtAttendanceDate.ToString("dd-MMM-yyyy"))
                };
            return DataLayer.ExecuteDataTable("spPayRptAttendanceWorkSheet", this.SqlParameters);
        }

        public DataTable GetOffDayMarkDetails(int intCompanyID, string strRosterDate, int intDepartmentID,int intBranchID,int intIncludeCompany,int intEmployeeID)
        {
            this.SqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 4),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@RosterDate1",strRosterDate),
                new SqlParameter("@DepartmentID",intDepartmentID),
                new SqlParameter("@BranchID",intBranchID),
                new SqlParameter("@IncludeCompany",intIncludeCompany),
                new SqlParameter("@EmployeeID",intEmployeeID),
                };
            return DataLayer.ExecuteDataTable("spPayOffDayMark", this.SqlParameters);
        }
        public DataSet GetEmployeeHistoryDetail(int intEmployeeID)
        {
            this.SqlParameters = new List<SqlParameter> { 
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView)
                };
            return DataLayer.ExecuteDataSet("spEmployeeHistory", this.SqlParameters);
        }

        public DataSet GetEmployeeLedgerDetail(int intEmployeeID,DateTime fromDate,DateTime toDate)
        {
            this.SqlParameters = new List<SqlParameter> { 
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@FromDate", fromDate),
                new SqlParameter("@ToDate", toDate),
                new SqlParameter("@IsArabic", ClsCommonSettings.IsArabicView)
                };
            return DataLayer.ExecuteDataSet("spEmployeeLedger", this.SqlParameters);
        }

    }
}
