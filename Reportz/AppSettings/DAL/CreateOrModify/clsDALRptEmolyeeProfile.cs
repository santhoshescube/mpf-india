﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MyPayfriend
{
    class clsDALRptEmolyeeProfile
    {

        public static DataTable GetEmployeeInformation(int intCompanyID, int intBranchID, int intDepartmentID, int intDesignationID, int intEmploymentTypeID, int intEmployeeID, bool intIncludeComp, int intLocation, int intWorkStatus, int intNationalityID, int intReligionID, string sDateOfJoining, string sDateOfJoiningTo, int VisaObtainedFrom)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>
            { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID", intBranchID) ,
                new SqlParameter("@DepartmentID", intDepartmentID) ,
                new SqlParameter("@DesignationID", intDesignationID) ,
                new SqlParameter("@EmploymentTypeID", intEmploymentTypeID),
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@IncludeComp", intIncludeComp),
                new SqlParameter("@LocationID", intLocation),
                new SqlParameter("@WorkStatusID", intWorkStatus),
                new SqlParameter("@NationalityID", intNationalityID),
                new SqlParameter("@ReligionID", intReligionID),
                new SqlParameter("@DOJ",sDateOfJoining=="01-Jan-1900"?null:sDateOfJoining),
                new SqlParameter("@CountryID", -1),
                new SqlParameter("@DOJTo", sDateOfJoiningTo),
                new SqlParameter("@VisaObtainedFrom", VisaObtainedFrom),
                new SqlParameter("@UserID",ClsCommonSettings.UserID)
            };
            return new DataLayer().ExecuteDataTable("spPayRptEmployeeProfileSummary", sqlParameters);
        }

        public static DataTable DisplaySingleEmployeeReport(int intEmployeeID, int VisaObtainedFrom)  //Employee  Report
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>
            { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@VisaObtainedFrom", VisaObtainedFrom),
                new SqlParameter("@EmployeeID", intEmployeeID)
            };
            return new DataLayer().ExecuteDataTable("spPayRptEmployeeProfileSummary", sqlParameters);
        }
    }
}