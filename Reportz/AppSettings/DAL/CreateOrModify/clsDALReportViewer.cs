﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Collections;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,25 Feb 2011>
Description:	<Description,,DAL for ReportViewer>
================================================
*/
namespace MyPayfriend
{
    public class clsDALReportViewer : IDisposable
    {
        ArrayList prmReportDetails;
        DataTable dtpreportTable;

        public clsDTOReportviewer objclsDTOReportviewer { get; set; }
        public DataLayer objclsconnection { get; set; }


        public DataTable DisplayCompanyReport() // for company report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 17));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spCompany", prmReportDetails);
        }

        public DataTable DisplayCompanyBankReport() //company Bank Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 18));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spCompany", prmReportDetails);
        }

        public DataTable DisplayBankReport()  //Bank Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@BankBranchID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spBankDetails", prmReportDetails);
        }

        public DataTable DisplayEmployeeReport()  //Employee  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spEmployee", prmReportDetails);
        }

        public DataSet DisplayAlertSettingsReport() //  AlertSetting Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@AlertSettingID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAlertSetting", prmReportDetails);
        }


        public DataSet DisplayExchangeCurrencyReport() //Exchange currency Details Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spExchangeCurrency", prmReportDetails);
        }

        public DataSet DisplayShiftScheduleDetails()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 11));
            prmReportDetails.Add(new SqlParameter("@ShiftScheduleID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayShiftScheduleTransaction", prmReportDetails);
        }

        public DataSet DisplayCompanyReportHeader(int intCompanyID) //CompnayReportHeader
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 25));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            return objclsconnection.ExecuteDataSet("spCompany", prmReportDetails);
        }

        public DataTable DisplayCompanyDefaultHeader() //Company Default Header
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 22));
            return objclsconnection.ExecuteDataTable("spCompany", prmReportDetails);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3)
            {
                using (ClsCommonUtility objCommonUtility = new ClsCommonUtility())
                {
                    objCommonUtility.PobjDataLayer = objclsconnection;
                    return objCommonUtility.FillCombos(saFieldValues);
                }
            }
            return null;
        }
        public DataSet DisplayLeavePolicy() //Leave Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 20));
            prmReportDetails.Add(new SqlParameter("@LeavePolicyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayLeavePolicy", prmReportDetails);

        }

        public DataSet DisplayShiftPolicy() //Shift Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 13));
            prmReportDetails.Add(new SqlParameter("@ShiftID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayShiftPolicy", prmReportDetails);

        }
        public DataSet DisplayWorkPolicy() //Work Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "WP"));
            prmReportDetails.Add(new SqlParameter("@PolicyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayWorkPolicy", prmReportDetails);

        }

        public DataSet DisplayOvertimePolicy() //OvertimePolicy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "11"));
            prmReportDetails.Add(new SqlParameter("@OTPolicyID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayOvertimePolicy", prmReportDetails);

        }



        public static DataTable GetDocumentReceiptIssueDetails(int OperationTypeID, int DocumentTypeID, int DocumentID,int OrderNo)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 23));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            prmDocuments.Add(new SqlParameter("@OrderNo", OrderNo));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }


        public static DataTable GetOtherDocumentDetails(int CurrentIndex, int DocumentTypeID, int DocumentID, int OperationTypeID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 24));
            prmDocuments.Add(new SqlParameter("@RowIndex", CurrentIndex));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }





        public DataSet DisplayAccRecurringJournalSetup() //AccRecurringJournalSetup
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "6"));
            prmReportDetails.Add(new SqlParameter("@RecurringJournalSetupID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spAccJournalRecurrenceSetup", prmReportDetails);

        }

        public DataSet DisplaySalaryStructure() //OvertimePolicy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "19"));
            prmReportDetails.Add(new SqlParameter("@SalaryStructureID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPaySalaryStructure", prmReportDetails);

        }

        public DataSet DisplayLeaveStructure(int intEmployeeID, int intCompanyID, string date1, string CurrentFinYear)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 37));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
            prmReportDetails.Add(new SqlParameter("@Date1", Convert.ToDateTime(date1)));
            prmReportDetails.Add(new SqlParameter("@CurrentFinYear", CurrentFinYear));
            return objclsconnection.ExecuteDataSet("spPayEmployeeLeave", prmReportDetails);
        }


        public DataSet DisplaySalaryRelease() //SalaryRelease Form
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@ProcessDate1", objclsDTOReportviewer.strDate));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompany));
            return objclsconnection.ExecuteDataSet("spPayEmployeePaymentDetailRelease", prmReportDetails);

        }

        public DataSet DisplayCompanyHeader()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 31));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompany));
            return objclsconnection.ExecuteDataSet("spPayModuleFunctions", prmReportDetails);
        }
        public DataSet DisplayRoleSettingsReport() //  CompanySettings Report
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 3));
            prmReportDetails.Add(new SqlParameter("@RoleID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("STRoleDetailsTransaction", prmReportDetails);
        }

        public DataTable DisplayWorkLocation()  //Work Location
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@WorkLocationID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayWorkLocation", prmReportDetails);
        }

        public DataSet DisplayocationScheduleReport()  //Location Schedule
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 12));
            prmReportDetails.Add(new SqlParameter("@ScheduleID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayLocationSchedule", prmReportDetails);
        }
        public DataSet DisplayDeductionPolicy() //Work Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@AdditionDeductionPolicyId", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayDeductionPolicyTransaction", prmReportDetails);

        }
        public DataSet DisplayEmployeeLeaveEntry()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 36));
            prmReportDetails.Add(new SqlParameter("@LeaveID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayEmployeeLeave", prmReportDetails);
        }

        public DataSet DisplayPFPolicy() //PF Policy
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@AdditionDeductionPolicyId", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayPFPolicyTransaction", prmReportDetails);
        }
        public DataTable DisplayCompanyAssetReport()  //Comapny Assets  Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@BenefitTypeID", objclsDTOReportviewer.intRecId));
            prmReportDetails.Add(new SqlParameter("@CompanyID", objclsDTOReportviewer.intCompanyID));
            return objclsconnection.ExecuteDataTable("PayCompanyAssets", prmReportDetails);
        }
        public DataSet VacationProcessReport() // for Vacation Process Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 9));
            prmReportDetails.Add(new SqlParameter("@VacationID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayVacationProcessTransactions", prmReportDetails);
        }
        public DataTable DisplayVisaReport()  //Visa Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "EVR"));
            prmReportDetails.Add(new SqlParameter("@VisaID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spDocumentReport", prmReportDetails);
        }
        public DataTable DisplayEmirateHealthReport()  //EmirateHealth Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "6"));
            prmReportDetails.Add(new SqlParameter("@CardID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("MvfEmployeeEmiratesCardTransactions", prmReportDetails);
        }
        public DataTable DisplaHealthCardReport()  //EmirateHealth Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "9"));
            prmReportDetails.Add(new SqlParameter("@CardID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("MvfEmployeeHealthCardTransactions", prmReportDetails);
        }
        public DataTable QualificationDetails()
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 8));
            prmReportDetails.Add(new SqlParameter("@QualificationID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayQualification", prmReportDetails);
        }
        public  static DataTable GetEmployeeLicenseReport(int LicenseID)
        {
             ArrayList  prmReportDetails = new ArrayList();

            prmReportDetails.Add(new SqlParameter("@Mode", "EDR"));
            prmReportDetails.Add(new SqlParameter("@LicenseID", LicenseID));

            return  new clsConnection().ExecuteDataTable("spDocumentReport", prmReportDetails);
        }


        public DataTable EmpSettlementDetails() //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@Mode", 36));
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

        }

        public DataSet EmpGratuityReport() //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", objclsDTOReportviewer.intRecId));
            prmEmpSettlement.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return objclsconnection.ExecuteDataSet("spPayGratuityReport", prmEmpSettlement);

        }


        public DataSet spPayGratuityReportNewVerOne() //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", objclsDTOReportviewer.intRecId));
            prmEmpSettlement.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return objclsconnection.ExecuteDataSet("spPayGratuityReportNewVerOne", prmEmpSettlement);

        }


        public DataSet spPayGratuityReportNewVerOne5() //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", objclsDTOReportviewer.intRecId));
            prmEmpSettlement.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            prmEmpSettlement.Add(new SqlParameter("@DedFlg", "1"));
            return objclsconnection.ExecuteDataSet("spPayGratuityReportNewVerOne", prmEmpSettlement);

        }

        public DataSet EmpGratuityReportNew() //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", objclsDTOReportviewer.intRecId));
            prmEmpSettlement.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return objclsconnection.ExecuteDataSet("spPayGratuityReportNew", prmEmpSettlement);

        }
        public static  DataTable EmpSettlementDetails(int SettlementID) //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@Mode", 36));
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", SettlementID));
            return new DataLayer().ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

        }


        public DataTable EmpSettlement() //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@Mode", 37));
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

        }


        public static  DataTable EmpSettlement(int SettlementID) //
        {
            ArrayList prmEmpSettlement = new ArrayList();
            prmEmpSettlement.Add(new SqlParameter("@Mode", 37));
            prmEmpSettlement.Add(new SqlParameter("@SettlementID", SettlementID));
            return new DataLayer().ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

        }

        public DataSet GetEmployeeLoanReport()
        {
            ArrayList prmEmployeeLoan = new ArrayList();
            prmEmployeeLoan.Add(new SqlParameter("@Mode", 8));
            prmEmployeeLoan.Add(new SqlParameter("@LoanID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayEmployeeLoan", prmEmployeeLoan);
        }
        public DataTable DisplayInsuranceCardReport()  //Healthcard Report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@InsuranceCardID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayInsuranceCard", parameters);
        }

        public DataTable DisplayInsuranceDetailsReport()  //Insurance details Report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@InsuranceID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayInsuranceDetails", parameters);
        }
        public DataTable DisplayLabourCardReport()  //EmirateHealth Report
        {
            dtpreportTable = new DataTable();
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 8));
            prmReportDetails.Add(new SqlParameter("@LabourCardID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spEmployeeLabourCard", prmReportDetails);
        }

        public DataSet GetLeaveRequestDetials()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@LeaveRequestID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayLeaveRequest", parameters);
        }
        public DataSet GetAlerts()
        {
            ArrayList prmAlertreport = new ArrayList();
            prmAlertreport.Add(new SqlParameter("@Mode", 5));
            prmAlertreport.Add(new SqlParameter("@SearchKey", objclsDTOReportviewer.strSearchKey));
            prmAlertreport.Add(new SqlParameter("@UserID", ClsCommonSettings.UserID));
            prmAlertreport.Add(new SqlParameter("@FromDate", objclsDTOReportviewer.strFromDate));
            prmAlertreport.Add(new SqlParameter("@ToDate", objclsDTOReportviewer.strToDate));
            prmAlertreport.Add(new SqlParameter("@AlertTypeID", objclsDTOReportviewer.intAlertType));
          

            return objclsconnection.ExecuteDataSet("spAlertReport", prmAlertreport);
        }
        public DataTable GetPrintExpenseDetails()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@EmployeeExpenseID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayEmployeeExpense", parameters);
        }

        public DataTable GetPrintDepositDetails()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 6));
            parameters.Add(new SqlParameter("@DepositID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPayDeposit", parameters);
        }

        public DataSet GetPrintLoanRepaymentDetails()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@RepaymentID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataSet("spPayLoanRepayment", parameters);
        }

        public DataTable GetPrintSalaryAdvanceDetails()
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@SalaryAdvanceID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spPaySalaryAdvance", parameters);
        }

        #region DisplayLeaveExtension
        public DataTable DisplayLeaveExtension(int EmployeeID, int CompanyID)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", "ShowReport"));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            return objclsconnection.ExecuteDataTable("spEmployeeLeaveExtension", prmReportDetails);
        }
        #endregion DisplayLeaveExtension

        public static DataTable DisplayTradeLicenseReport(long TradeLicenseID)  //Tradelicense report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
            return new clsConnection().ExecuteDataTable ("spPayTradeLicense", parameters);
        }

        public static DataTable DisplayLeaseAgreementReport(long LeaseAgreementID)  //Tradelicense report
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "7"));
            parameters.Add(new SqlParameter("@LeaseID", LeaseAgreementID));
            return new clsConnection().ExecuteDataTable("spPayLeaseAgreement", parameters);
        }

        public DataSet DisplayAssetHandoverDetails()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 14));
            prmReportDetails.Add(new SqlParameter("@EmpBenefitID", objclsDTOReportviewer.intRecId));
            prmReportDetails.Add(new SqlParameter("@IsArabicView", ClsCommonSettings.IsArabicView));
            return objclsconnection.ExecuteDataSet("spPayAssetHandover", prmReportDetails);
        }

        public DataTable DisplaySalesInvoiceDetails()
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 6));
            prmReportDetails.Add(new SqlParameter("@SalesInvoiceID", objclsDTOReportviewer.intRecId));
            return objclsconnection.ExecuteDataTable("spSalesInvoice", prmReportDetails);
        }

        public DataTable DisplaySalesInvoiceSummary(int CompanyID, bool IncludeCompany, int BranchID, int DepartmentID, int DesignationID,
            int ProfitCenterID, int EmployeeID, DateTime MonthFrom, DateTime MonthTo)
        {
            prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@Mode", 7));
            prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
            prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
            prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
            prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
            prmReportDetails.Add(new SqlParameter("@ProfitCenterID", ProfitCenterID));
            prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
            prmReportDetails.Add(new SqlParameter("@MonthFrom", MonthFrom));
            prmReportDetails.Add(new SqlParameter("@MonthTo", MonthTo));
            return objclsconnection.ExecuteDataTable("spSalesInvoice", prmReportDetails);
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            if (prmReportDetails != null)
                prmReportDetails = null;

            if (dtpreportTable != null)
                dtpreportTable.Dispose();


        }

        #endregion
    }
}