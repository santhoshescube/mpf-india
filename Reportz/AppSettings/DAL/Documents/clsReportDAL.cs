﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


public class clsReportDAL
{
    private static List<SqlParameter> param;
   
    public clsReportDAL()
    {
        
    }

    public static DataTable GetcompanyInfo(int CompanyID)
    {
        param = new List<SqlParameter>();
        param.Add(new SqlParameter("@Mode", 8));
        param.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteDataTable("STRptSales", param);

    }

    public static  DataTable GetEmployeeVisaReport(int VisaID)
    {
        param = new List<SqlParameter>();

        param.Add(new SqlParameter("@Mode", "EVR"));
        param.Add(new SqlParameter("@VisaID", VisaID));

        return new  DataLayer().ExecuteDataTable("spDocumentReport", param);

    }


    public static DataSet  GetEmployeeVisaReportD(int VisaID)
    {
        param = new List<SqlParameter>();

        param.Add(new SqlParameter("@Mode", "EVR"));
        param.Add(new SqlParameter("@VisaID", VisaID));

        return new DataLayer().ExecuteDataSet("spDocumentReport", param);

    }

    public static DataTable GetEmployeePassportReport(int PassportID)
    {
        param = new List<SqlParameter>();

        param.Add(new SqlParameter("@Mode", "EPD"));
        param.Add(new SqlParameter("@PassportID", PassportID));

        return new DataLayer().ExecuteDataTable("spDocumentReport", param);

    }

    public static DataSet  GetEmployeePassportReportD(int PassportID)
    {
        param = new List<SqlParameter>();

        param.Add(new SqlParameter("@Mode", "EPD"));
        param.Add(new SqlParameter("@PassportID", PassportID));

        return new DataLayer().ExecuteDataSet("spDocumentReport", param);

    }
}
   

