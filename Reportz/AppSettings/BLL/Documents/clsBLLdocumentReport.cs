﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System;

namespace MyPayfriend
{
    public class clsBLLdocumentReport
    {
        clsDALdocumentReport MobjclsDALdocumentReport;
         private DataLayer MobjDataLayer;

         public clsBLLdocumentReport()
        {
            MobjDataLayer = new DataLayer();
            MobjclsDALdocumentReport = new clsDALdocumentReport(MobjDataLayer);
           
        }
         public DataTable FillCombos(string[] saFieldValues)
         {
             //function for getting datatable for filling combo
             return MobjclsDALdocumentReport.FillCombos(saFieldValues);
         }

         public DataTable FillCombos(string sQuery)
         {
             //function for getting datatable for filling combo
             return MobjclsDALdocumentReport.FillCombos(sQuery);
         }
         public DataTable GetDocForAllEmp(int intDocTypeID, bool blnIsPredefined, int intEmpID)
         {
             return MobjclsDALdocumentReport.GetDocForAllEmp(intDocTypeID,blnIsPredefined,intEmpID);

         }
         public DataSet GetDocForSingleEmp( int intEmpID)
         {
             return MobjclsDALdocumentReport.GetDocForSingleEmp( intEmpID);
         }

         public static  DataSet  AllDocuments(int CompanyID,int BranchID,bool IncludeCompany, int StatusID,int DocumentTypeID,DateTime? FromDate,DateTime? ToDate )
         {
             return clsDALdocumentReport.AllDocuments(CompanyID, BranchID, IncludeCompany, StatusID, DocumentTypeID, FromDate, ToDate); 
         }
         public static DataSet AllDocumentsGrid(int CompanyID, int BranchID, bool IncludeCompany, int StatusID, int DocumentTypeID, DateTime? FromDate, DateTime? ToDate, int EmployeeID, string DocumentNr,int reportType)
         {
             return clsDALdocumentReport.AllDocumentsGrid(CompanyID, BranchID, IncludeCompany, StatusID, DocumentTypeID, FromDate, ToDate, EmployeeID, DocumentNr, reportType);
         }

         public static DataSet GetAllDocumentsByEmployee(int EmployeeID,int DocumentTypeID,string DocumentNumber,DateTime?FromDate,DateTime?ToDate)
         {
             return clsDALdocumentReport.GetAllDocumentsByEmployee(EmployeeID,DocumentTypeID,DocumentNumber,FromDate,ToDate   );
         }

         public static DataSet GetSingleDocumentDetails(int CompanyID,int BranchID,bool IncludeCompany,int DocumentTypeID,string DocumentNumber,DateTime? FromDate,DateTime? ToDate)
         {
             return clsDALdocumentReport.GetSingleDocumentDetails(CompanyID,BranchID,IncludeCompany,   DocumentTypeID,DocumentNumber,FromDate ,ToDate);
         }


    }
}
