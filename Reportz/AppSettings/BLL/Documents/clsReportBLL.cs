﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;




public class clsReportBLL
{

    public clsReportBLL()
    {
       
    }

    public static DataTable GetCompanyInfo(int CompanyID)
    {
        return clsReportDAL.GetcompanyInfo(CompanyID);
    }


    public static DataSet  GetEmployeeVisaReportD(int VisaId)
    {
        return clsReportDAL.GetEmployeeVisaReportD(VisaId);
    }


    public static DataTable GetEmployeeVisaReport(int VisaId)
    {
        return clsReportDAL.GetEmployeeVisaReport(VisaId);
    }

    public static DataTable GetEmployeePassportReport(int PassportID)
    {
        return clsReportDAL.GetEmployeePassportReport(PassportID);
    }


    public static DataSet  GetEmployeePassportReportD(int PassportID)
    {
        return clsReportDAL.GetEmployeePassportReportD(PassportID);
    }


}