﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 22 AUG 2013
* Description      : transfer  Report BLL

* ******************************************************/
namespace MyPayfriend
{
   public  class clsBLLTransferReport
    {
        public clsBLLTransferReport() { }
        public static DataTable GetTransferDetails( int EmployeeID, DateTime FromDate, DateTime ToDate)
         {
             return clsDALTransferReport.GetTransferDetails( EmployeeID, FromDate, ToDate);
         }
    }
}
