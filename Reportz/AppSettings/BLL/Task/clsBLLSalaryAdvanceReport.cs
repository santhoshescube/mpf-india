﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyPayfriend
{
    /*****************************************************
  * Created By       : HIMA
  * Creation Date    : 20 AUG 2013
  * Description      : Salary Advance and Loan Report BLL

  * ******************************************************/

   public class clsBLLSalaryAdvanceReport
    {
        public clsBLLSalaryAdvanceReport(){}
        public static DataTable GetSalaryAdvanceOrLoanSummary(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate, int IsSalaryAdvanceReport, bool IsClosed)
        {
            return clsDALSalaryAdvanceReport.GetSalaryAdvanceOrLoanSummary(CompanyID, BranchID, DepartmentID, WorkStatusID, EmployeeID, IncludeCompany, FromDate, ToDate, IsSalaryAdvanceReport, IsClosed);
        }
        public static DataTable GetLoanDetails(int EmployeeID, DateTime FromDate, DateTime ToDate, bool IsClosed)
        {
            return clsDALSalaryAdvanceReport.GetLoanDetails(EmployeeID, FromDate, ToDate, IsClosed);
  
        }
    }
}
