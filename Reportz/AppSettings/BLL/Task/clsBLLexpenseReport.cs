﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 22 AUG 2013
* Description      : Expense Report BLL

* ******************************************************/
namespace MyPayfriend
{
   public  class clsBLLexpenseReport
    {
        public clsBLLexpenseReport() { }
         public static DataTable GetExpenseDetails(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate,int ExpenseType)
         {
             return clsDALexpenseReport.GetExpenseDetails(CompanyID, BranchID, DepartmentID, WorkStatusID, EmployeeID, IncludeCompany, FromDate, ToDate, ExpenseType);
         }
    }
}
