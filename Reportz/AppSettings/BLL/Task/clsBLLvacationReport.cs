﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 15 AUG 2013
* Description      : Vacation Report BLL

* ******************************************************/
namespace MyPayfriend
{
   public  class clsBLLvacationReport
    {
       public clsBLLvacationReport() { }
         public static DataTable Getvacation(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate, int SearchType)
         {
             return clsDALvacationreport.Getvacation(CompanyID, BranchID, DepartmentID,WorkStatusID, EmployeeID, IncludeCompany, FromDate, ToDate, SearchType);
         }
         public static DataTable GetvacationDetails(int EmployeeID, DateTime FromDate, DateTime ToDate)
         {
             return clsDALvacationreport.GetvacationDetails(EmployeeID, FromDate, ToDate);
         }
         public static DataTable GetvacationDetailsForGrid(int EmployeeID, DateTime FromDate, DateTime ToDate, int WorkStatusID, int CompanyID, int BranchID, int DepartmentID, bool IncludeCompany)
         {
             return clsDALvacationreport.GetvacationDetailsForGrid(EmployeeID, FromDate, ToDate, WorkStatusID, CompanyID,  BranchID,  DepartmentID,  IncludeCompany);
         }
    }
}
