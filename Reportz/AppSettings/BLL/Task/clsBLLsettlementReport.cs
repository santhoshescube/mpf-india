﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 24 AUG 2013
* Description      : Settlement Report BLL

* ******************************************************/
  namespace MyPayfriend
{
     public class clsBLLsettlementReport
    {
         public static DataTable GetSettlement(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate,int SettlementType)
         {
             return clsDALsettlementReport.GetSettlement(CompanyID, BranchID, DepartmentID, DesignationID, EmployeeID, IncludeCompany, FromDate, ToDate,SettlementType);
         }
         public static DataTable GetSettlementDetails(int EmployeeID, DateTime FromDate, DateTime ToDate, int SettlementType)
         {
             return clsDALsettlementReport.GetSettlementDetails(EmployeeID, FromDate, ToDate,SettlementType);
         }
    }
}
