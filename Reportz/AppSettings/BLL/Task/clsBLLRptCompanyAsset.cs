﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
/*****************************************************
* Created By       : HIMA
* Creation Date    : 20 AUG 2013
* Description      : Company Asset Report BLL

* ******************************************************/
namespace MyPayfriend
{
    class clsBLLRptCompanyAsset
    {

        public static DataSet DisplayCompanyAssetReport(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, int AssetType, int CompanyAsset, int StatusType, DateTime FromDate, DateTime ToDate, bool IncludeComp)
        {
            return clsDALRptCompanyAsset.DisplayCompanyAssetReport(CompanyID,BranchID,  DepartmentID,  WorkStatusID, EmployeeID, AssetType, CompanyAsset,StatusType, FromDate, ToDate, IncludeComp);
        }


        public static DataTable GetAllCompanyAssets(int AssetType, int CompanyAsset, int StatusType, DateTime FromDate, DateTime ToDate, int CompanyID, int BranchID, bool IncludeComp)
        {
            return clsDALRptCompanyAsset.GetAllCompanyAssets( AssetType, CompanyAsset,StatusType,FromDate,ToDate,CompanyID,BranchID,IncludeComp); 
        }
    }
}
