﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLRptEmployeeProfile
    {
        public static DataTable GetEmployeeInformation(int intCompanyID, int intBranchID, int intDepartmentID, int intDesignationID, int intEmploymentTypeID, int intEmployeeID, bool intIncludeComp, int intLocation, int intWorkStatus, int intNationalityID, int intReligionID, string sDateOfJoining, string DateOfJoiningTo, int VisaObtainedFrom)
        {
            return clsDALRptEmolyeeProfile.GetEmployeeInformation(intCompanyID, intBranchID, intDepartmentID, intDesignationID, intEmploymentTypeID, intEmployeeID, intIncludeComp, intLocation, intWorkStatus, intNationalityID, intReligionID, sDateOfJoining, DateOfJoiningTo, VisaObtainedFrom);
        }

        public static DataTable DisplaySingleEmployeeReport(int intEmployeeID, int VisaObtainedFrom)
        {
            return clsDALRptEmolyeeProfile.DisplaySingleEmployeeReport(intEmployeeID, VisaObtainedFrom);
        }

    }
}