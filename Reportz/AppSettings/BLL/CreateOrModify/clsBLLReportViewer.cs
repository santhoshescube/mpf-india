﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data;
using System.Data.SqlClient;
/* 
=================================================
Author:		<Author,,Sawmya>
Create date: <Create Date,25 Feb 2011>
Description:	<Description,BLL for ReportViewer>
================================================
*/
namespace MyPayfriend
{
    public class clsBLLReportViewer : IDisposable
    {
        clsDTOReportviewer objclsDTOReportviewer;
        clsDALReportViewer objclsDALReportViewer;
        private DataLayer objclsconnection;

        public clsBLLReportViewer()
        {

            objclsconnection = new DataLayer();
            objclsDALReportViewer = new clsDALReportViewer();
            objclsDTOReportviewer = new clsDTOReportviewer();
            objclsDALReportViewer.objclsDTOReportviewer = objclsDTOReportviewer;
        }

        public clsDTOReportviewer clsDTOReportviewer
        {
            get { return objclsDTOReportviewer; }
            set { objclsDTOReportviewer = value; }
        }

        public DataTable DisplayCompanyReport()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayCompanyReport();
        }

        public DataTable DisplayCompanyBankReport()   //company Bank Report
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayCompanyBankReport();
        }

        public DataTable DisplayBankReport()  //Bank Report
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayBankReport();
        }


        public DataSet DisplayCompanyReportHeader(int intCompanyID)  //Company  Header
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayCompanyReportHeader(intCompanyID);
        }

        public DataTable DisplayCompanyDefaultHeader()  //Company Default Header
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayCompanyDefaultHeader();
        }


        public DataTable DisplayEmployeeReport()  //Employee  Report
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayEmployeeReport();

        }


        public DataSet DisplayExchangeCurrencyReport() //ExchangeCurrency Report
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayExchangeCurrencyReport();
        }

        public DataSet DisplayAlertSettingsReport()// AlertSettings Report
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayAlertSettingsReport();
        }

        public DataSet DisplayShiftScheduleDetails()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayShiftScheduleDetails();
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.FillCombos(saFieldValues);
        }
        public DataSet DisplayLeavePolicy() // LEave Policy
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayLeavePolicy();
        }
        public DataSet DisplayShiftPolicy() // Shift Policy
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayShiftPolicy();
        }
        public DataSet DisplayWorkPolicy() // Shift Policy
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayWorkPolicy();
        }

        public DataSet DisplayOvertimePolicy() // 
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayOvertimePolicy();
        }

        public static  DataTable  GetDocumentReceiptIssueDetails(int OperationTypeID,int DocumentTypeID,int DocumentID,int OrderNo) // 
        {
            return clsDALReportViewer.GetDocumentReceiptIssueDetails(OperationTypeID,DocumentTypeID,DocumentID,OrderNo );
        }

        public static DataTable GetOtherDocumentDetails(int CurrentIndex, int DocumentTypeID, int DocumentID, int OperationTypeID)
        {
            return clsDALReportViewer.GetOtherDocumentDetails(CurrentIndex, DocumentTypeID, DocumentID, OperationTypeID);
        }
      
        public DataSet DisplayAccRecurringJournalSetup()//AccRecurringJournalSetup
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayAccRecurringJournalSetup();
        }
        public DataSet DisplaySalaryStructure() // 
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplaySalaryStructure();
        }
        public DataSet DisplayLeaveStructure(int intEmployeeID, int intCompanyID, string date1, string CurrentFinYear) // 
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayLeaveStructure( intEmployeeID,  intCompanyID,  date1,  CurrentFinYear);
        }

        public DataSet DisplaySalaryRelease()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplaySalaryRelease();
        }

        public DataSet DisplayCompanyHeader()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayCompanyHeader();
        }
        public DataSet DisplayRoleSettingsReport() //Role Settings Report
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayRoleSettingsReport();
        }

        public DataTable DisplayWorkLocation()  //Worl Location
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayWorkLocation();
        }
        public DataSet DisplayocationScheduleReport()  //Location 
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayocationScheduleReport();
        }
        public DataSet DisplayDeductionPolicy() // Shift Policy
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayDeductionPolicy();
        }

        public DataSet DisplayEmployeeLeaveEntry() // Leave Entry
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayEmployeeLeaveEntry();
        }
        public DataTable DisplayVisaReport()  //Visa Report
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayVisaReport();
        }
 
        public DataTable DisplayEmirateHealthReport()  //Visa Report
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayEmirateHealthReport();
        }
        public DataTable DisplayCompanyAssetReport() //company Assets
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayCompanyAssetReport();
        }
        public DataSet DisplayPFPolicy() // PF Policy
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayPFPolicy();
        }
        public DataSet VacationProcessReport()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.VacationProcessReport();
        }
        public DataTable DisplaHealthCardReport()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplaHealthCardReport();
        }
        public DataTable QualificationDetails() // Employee Qualification details
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.QualificationDetails();
        }
        public static DataTable DrivingLicenseDetails(int LicenseID) // Get Employee driving lecense details
        {
            return clsDALReportViewer.GetEmployeeLicenseReport(LicenseID);
        }
        public DataTable EmpSettlementDetails() //
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.EmpSettlementDetails();
        }
        public DataSet EmpGratuityReport() //
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.EmpGratuityReport();
            
        }
        public DataSet EmpGratuityReportNew() //
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.EmpGratuityReportNew();
        }

        public DataSet spPayGratuityReportNewVerOne() //
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.spPayGratuityReportNewVerOne();

        }
        public DataSet spPayGratuityReportNewVerOne5() //
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.spPayGratuityReportNewVerOne5();

        }

        public static  DataTable GetEmpSettlementDetails(int SettlementID) //
        {
            return clsDALReportViewer.EmpSettlementDetails(SettlementID);
        }

        public DataTable EmpSettlement() //
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.EmpSettlement();
        }

        public static  DataTable GetEmpSettlement(int SettlementID) //
        {
          return clsDALReportViewer.EmpSettlement(SettlementID );
        }


        public DataSet GetEmployeeLoanReport() 
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.GetEmployeeLoanReport();
        }
        public DataTable DisplayInsuranceCardReport() // Employee Insurancecard details
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayInsuranceCardReport();
        }
        public DataTable DisplayInsuranceDetailsReport() // company Insurance details
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayInsuranceDetailsReport();
        }

        public DataTable DisplayLabourCardReport()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayLabourCardReport();
        }

        public DataSet GetLeaveRequestDetials()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.GetLeaveRequestDetials();
        }
        public DataSet GetAlerts()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.GetAlerts();
        }

        public DataTable GetPrintExpenseDetails()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.GetPrintExpenseDetails();
        }

        public DataTable GetPrintDepositDetails()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.GetPrintDepositDetails();
        }

        public DataSet GetPrintLoanRepaymentDetails()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.GetPrintLoanRepaymentDetails();
        }

        public DataTable GetPrintSalaryAdvanceDetails()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.GetPrintSalaryAdvanceDetails();
        }

        #region DisplayLeaveExtension
        public DataTable DisplayLeaveExtension(int EmployeeID, int CompanyID)//Leave Extension
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayLeaveExtension(EmployeeID, CompanyID);
        }
        #endregion DisplayLeaveExtension
        /// <summary>
        /// Created by laxmi for showing tradelicense report
        /// </summary>
        /// <returns></returns>
        public static DataTable DisplayTradeLicenseReport(long TradeLicenseID)
        {
            return clsDALReportViewer.DisplayTradeLicenseReport(TradeLicenseID);
        }

        public static DataTable DisplayLeaseAgreementReport(long LeaseID)
        {
            return clsDALReportViewer.DisplayLeaseAgreementReport(LeaseID);
        }

        public DataSet DisplayAssetHandoverDetails()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplayAssetHandoverDetails();
        }

        public DataTable DisplaySalesInvoiceDetails()
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplaySalesInvoiceDetails();
        }

        public DataTable DisplaySalesInvoiceSummary(int CompanyID, bool IncludeCompany, int BranchID, int DepartmentID, int DesignationID,
            int ProfitCenterID, int EmployeeID, DateTime MonthFrom, DateTime MonthTo)
        {
            objclsDALReportViewer.objclsconnection = objclsconnection;
            return objclsDALReportViewer.DisplaySalesInvoiceSummary(CompanyID, IncludeCompany, BranchID, DepartmentID, DesignationID,
                ProfitCenterID, EmployeeID, MonthFrom, MonthTo);
        }
        #region IDisposable Members

        public void Dispose()
        {

            if (objclsDALReportViewer != null)
                objclsDALReportViewer = null;

            if (objclsDTOReportviewer != null)
                objclsDTOReportviewer = null;
        }

        #endregion
    }
}