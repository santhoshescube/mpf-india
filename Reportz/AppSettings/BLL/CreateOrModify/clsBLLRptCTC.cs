﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

public class clsBLLRptCTC
{

    public static DataSet GetEmployeeCTCReport(int CompanyID,int BranchID,bool IncludeCompany, int EmployeeID,DateTime ?Date,String FinYear)
    {
        return clsDALRptCTC.GetEmployeeCTCReport( CompanyID,BranchID,IncludeCompany,  EmployeeID,Date ,FinYear);
    }
}

