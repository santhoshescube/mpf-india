﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    class clsBLLRptEmployeeDeduction
    {
        
        public static DataTable GetReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int DeductionID, string Month, int Year)
        {
            return clsDALRptEmployeeDeduction.GetReport(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID,WorkStatusID, EmployeeID, DeductionID, Month, Year);
        }
      
    }
}
