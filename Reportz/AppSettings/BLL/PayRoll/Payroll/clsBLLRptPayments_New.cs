﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLRptPayments_New
    {
        clsDALRptPayments_New MobjDALRptEmolyeeProfile = null;
        public clsBLLRptPayments_New() { }

        private clsDALRptPayments_New DALRptPayments  
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALRptEmolyeeProfile == null)
                    this.MobjDALRptEmolyeeProfile = new clsDALRptPayments_New();

                return this.MobjDALRptEmolyeeProfile;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALRptPayments.FillCombos(saFieldValues);
        }
        public DataTable DisplayCompanyReportHeader(int intCompanyID)  //Company  Header
        {
            return DALRptPayments.DisplayCompanyReportHeader(intCompanyID);  
        }
        public DataTable GetMonthlyPayments(int intCompanyID, int intBranchID, int intDepartmentID, int intEmployeeID, int intMonth, int intYear, int intIncludeComp, int intLocationID)
        {
            return DALRptPayments.GetMonthlyPayments(intCompanyID, intBranchID, intDepartmentID, intEmployeeID, intMonth, intYear, intIncludeComp, intLocationID);
        }

        public DataTable GetSummaryPayments(int intCompanyID, int intBranchID, int intDepartmentID, int intEmployeeID, string strFromDate, string strToDate, int intIncludeComp, int intLocationID)
        {
            return DALRptPayments.GetSummaryPayments(intCompanyID, intBranchID, intDepartmentID, intEmployeeID, strFromDate, strToDate, intIncludeComp, intLocationID);
        }
        public DataTable GetFinancialYear(int intEmployeeID, DateTime dteFrom, DateTime dteTo)
        {
            return DALRptPayments.GetFinancialYear(intEmployeeID, dteFrom, dteTo);
        }



    }
}
