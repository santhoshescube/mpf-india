﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLRptSalaryStructure
    {

        public static DataTable GetSalaryStructureSummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {

            return clsDALRptSalaryStructure.GetSalaryStructureSummary(CompanyID, BranchID, DepartmentID,DesignationID, WorkStatusID, EmploymentTypeID, IncludeComp, EmployeeID);
        }

        public static DataTable GetSalaryStructureHistorySummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {

            return clsDALRptSalaryStructure.GetSalaryStructureHistorySummary(CompanyID, BranchID, DepartmentID,DesignationID, WorkStatusID, EmploymentTypeID, IncludeComp, EmployeeID);
        }
        public static DataTable GetSalaryStructureSummaryGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {
            return clsDALRptSalaryStructure.GetSalaryStructureSummaryGrid(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmploymentTypeID, IncludeComp, EmployeeID);
        }
        public static DataTable GetSalaryStructureSummaryGridHistory(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {
            return clsDALRptSalaryStructure.GetSalaryStructureSummaryGridHistory(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmploymentTypeID, IncludeComp, EmployeeID);
        }
        public static DataTable GetSalaryStructureDetailsGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID)
        {
            return clsDALRptSalaryStructure.GetSalaryStructureDetailsGrid(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmploymentTypeID, IncludeComp, EmployeeID);
        }
    }
}
