﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLRptPayments
    {
        public clsBLLRptPayments() { }

        public static DataTable GetMonthlyPaymentsOne(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, int IsReleased, string fromdate, string todate, int ReportView,bool ExcludeSettlement)
        {
            return clsDALRptPayments.GetMonthlyPaymentsOne(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, IncludeCompany, TransactionTypeID, IsPayStructureOnly, IsReleased, fromdate, todate, ReportView, ExcludeSettlement);
        }
        public static DataTable GetMonthlyPaymentsOne(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate, int IsReleased, int ReportView, bool ExcludeSettlement)
        {
            return clsDALRptPayments.GetMonthlyPaymentsOne(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, Month, Year, IncludeCompany, TransactionTypeID, IsPayStructureOnly, PaymentDate, IsReleased, ReportView, ExcludeSettlement);
        }
        public static DataTable GetMonthlyPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate, int IsReleased, bool ExcludeSettlement)
        {
            return clsDALRptPayments.GetMonthlyPayments(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, Month, Year, IncludeCompany, TransactionTypeID, IsPayStructureOnly, PaymentDate, IsReleased, ExcludeSettlement);
        }
        public static DataTable GetSummaryPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate, bool IncludeCompany, int TransactionTypeID, string PaymentDate, int IsReleased, bool ExcludeSettlement)
        {
            return clsDALRptPayments.GetSummaryPayments(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, FromDate, ToDate, IncludeCompany, TransactionTypeID, PaymentDate, IsReleased, ExcludeSettlement);
        }
        public static DataTable GetMonthlyPaymentsProcessed(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string FromDate, string ToDate, int Istransfer, bool ExcludeSettlement)
        {
            return clsDALRptPayments.GetMonthlyPaymentsProcessed(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, Month, Year, IncludeCompany, TransactionTypeID, IsPayStructureOnly, FromDate, ToDate, Istransfer, ExcludeSettlement);
        }
        public static DataTable GetSalaryDetails(int intCompanyID, int intBranchID, bool intIncludeCompany, int intEmployeeID, string date)
        {
            return clsDALRptPayments.GetSalaryDetails(intCompanyID, intBranchID, intIncludeCompany, intEmployeeID, date);
        }
        public static DataSet GetGratuityDetails(int intCompanyID, int intEmployeeID, string FromDate, string ToDate, int SettlementExp, int SepType)
        {
            return clsDALRptPayments.GetGratuityDetails(intCompanyID, intEmployeeID, FromDate, ToDate, SettlementExp, SepType);
        }

        public static DataSet GetGratuityDetailsAccural(int intCompanyID, int intEmployeeID, string FromDate, string ToDate, int SettlementExp, int SepType)
        {
            return clsDALRptPayments.GetGratuityDetailsAccural(intCompanyID, intEmployeeID, FromDate, ToDate, SettlementExp, SepType);
        }


        public static DataSet GetVacationDetails(int intCompanyID, int intEmployeeID, string date)
        {
            return clsDALRptPayments.GetVacationDetails(intCompanyID, intEmployeeID, date);

        }

        public static DataTable GetMonthlyPaymentsProcessedGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, 
            int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string FromDate, 
            string ToDate, int Istransfer,bool ExcludeSettlement)
        {
            return clsDALRptPayments.GetMonthlyPaymentsProcessedGrid(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, 
                EmployeeID, Month, Year, IncludeCompany, TransactionTypeID, IsPayStructureOnly, FromDate, ToDate, Istransfer, ExcludeSettlement);
        }

        public static DataTable GetMonthlyPaymentsGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, 
            int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate, 
            int IsReleased,bool ExcludeSettlement)
        {
            return clsDALRptPayments.GetMonthlyPaymentsGrid(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, 
                EmployeeID, Month, Year, IncludeCompany, TransactionTypeID, IsPayStructureOnly, PaymentDate, IsReleased, ExcludeSettlement);
        }

        public static DataTable GetSummaryPaymentsGrid(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID,
            int EmployeeID, DateTime FromDate, DateTime ToDate, bool IncludeCompany, int TransactionTypeID, string PaymentDate, int IsReleased, bool ExcludeSettlement)
        {
            return clsDALRptPayments.GetSummaryPaymentsGrid(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, 
                EmployeeID, FromDate, ToDate, IncludeCompany, TransactionTypeID, PaymentDate, IsReleased, ExcludeSettlement);
        }
        public static DataTable GetMonthlyPaymentSummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate, int IsReleased, bool ExcludeSettlement)
        {
            return clsDALRptPayments.GetMonthlyPaymentSummary(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, Month, Year, IncludeCompany, TransactionTypeID, IsPayStructureOnly, PaymentDate, IsReleased, ExcludeSettlement);
        }
        public static DataTable GetSalaryDetailsFromTo(int intCompanyID, int intBranchID, bool intIncludeCompany, int intEmployeeID, string Fromdate, string Todate)
        {
            return clsDALRptPayments.GetSalaryDetailsFromTo(intCompanyID, intBranchID, intIncludeCompany, intEmployeeID, Fromdate, Todate);
        }
        public static DataTable GetMonthlyPaymentSummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string PaymentDate, int IsReleased, bool ExcludeSettlement, string FromDate, string ToDate)
        {
            return clsDALRptPayments.GetMonthlyPaymentSummary(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, Month, Year, IncludeCompany, TransactionTypeID, IsPayStructureOnly, PaymentDate, IsReleased, ExcludeSettlement, FromDate, ToDate);
        }

        public static DataSet GetGratuityDetailsAccural(int intCompanyID, int intEmployeeID,string FromDate,string ToDate,int SettlementExp, int SepType, int IsUpdate)
        {
            return clsDALRptPayments.GetGratuityDetailsAccural(intCompanyID, intEmployeeID, FromDate, ToDate, SettlementExp, SepType, IsUpdate);
        }

        public static DataSet GetVacationDetails(int intCompanyID,int intEmployeeID,string date,int IsUpdate)
        {
            return clsDALRptPayments.GetVacationDetails(intCompanyID, intEmployeeID, date, IsUpdate);
        }

    }
}
