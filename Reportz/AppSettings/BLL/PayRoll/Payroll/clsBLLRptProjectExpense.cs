﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLRptProjectExpense
    {
        public clsBLLRptProjectExpense() { }

        public static DataTable GetProjectPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate, bool IncludeCompany, int ProjectID)
        {
            return clsDALRptProjectExpense.GetProjectPayments(CompanyID,BranchID,DepartmentID,DesignationID,WorkStatusID,EmployeeID,FromDate,ToDate,IncludeCompany,ProjectID);
        }

        public static DataTable GetProjectPaymentsMonthYear(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int ProjectID)
        {
            return clsDALRptProjectExpense.GetProjectPaymentsMonthYear(CompanyID, BranchID, DepartmentID, DesignationID, WorkStatusID, EmployeeID, Month, Year, IncludeCompany, ProjectID);
        }
    }
}
