﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLRptAttendanceManual
    {
        clsDALRptAttendanceManual MobjDALRptAttendanceManual = null;

        private clsDALRptAttendanceManual DALRptAttendanceManual
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALRptAttendanceManual == null)
                    this.MobjDALRptAttendanceManual = new clsDALRptAttendanceManual();

                return this.MobjDALRptAttendanceManual;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALRptAttendanceManual.FillCombos(saFieldValues);
        }

        public DataTable GetAttendanceMonths(int intCompanyID, int intBranchID, int intIncludeCompany)
        {
            return this.DALRptAttendanceManual.GetAttendanceMonths(intCompanyID, intBranchID, intIncludeCompany);
        }

        public DataTable GetReport(int intCompanyID, int intBranchID, int intIncludeCompany, int intLocationID, int intEmployeeID, int intMonth, int intYear, DateTime dtAttendanceDate)
        {
            return this.DALRptAttendanceManual.GetReport(intCompanyID, intBranchID, intIncludeCompany, intLocationID, intEmployeeID, intMonth, intYear, dtAttendanceDate);
        }
        public DataTable GetCompanyHeader(int intCompanyID)
        {
            return this.DALRptAttendanceManual.GetCompanyHeader(intCompanyID);
        }
        public DataTable GetReportWorkSheet(int intCompanyID, int intBranchID, int intIncludeCompany, int intDepartmentID, int intEmployeeID, int intMonth, int intYear, DateTime dtAttendanceDate)
        {
            return this.DALRptAttendanceManual.GetReportWorkSheet(intCompanyID, intBranchID, intIncludeCompany, intDepartmentID, intEmployeeID, intMonth, intYear, dtAttendanceDate);
        }
        public DataTable GetOffDayMarkDetails(int intCompanyID, string strRosterDate, int intDepartmentID,int intBranchID,int  intIncludeCompany, int intEmployeeID)
        {
            return this.DALRptAttendanceManual.GetOffDayMarkDetails(intCompanyID, strRosterDate, intDepartmentID,intBranchID, intIncludeCompany, intEmployeeID);
        }
        public DataSet  GetEmployeeHistoryDetail(int intEmployeeID)
        {
            return this.DALRptAttendanceManual.GetEmployeeHistoryDetail(intEmployeeID);
        }

        public DataSet GetEmployeeLedgerDetail(int intEmployeeID,DateTime fromDate,DateTime toDate)
        {
            return this.DALRptAttendanceManual.GetEmployeeLedgerDetail(intEmployeeID, fromDate, toDate);
        }
    }
}
