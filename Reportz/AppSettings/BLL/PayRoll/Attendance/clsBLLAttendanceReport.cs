﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace MyPayfriend
{

    /*****************************************************  
* Created By       : Arun
* Creation Date    : 24 Apr 2012
* Description      : Handle Attendance Reports Summary

* ******************************************************/
    public class clsBLLAttendanceReport
    {

        public clsBLLAttendanceReport()
        {

        }

        public static DataTable GetReport(int CompanyID, int BranchID, bool IncludeCompany,int DepartmentID,int DesignationID, int WorkStatusID, int EmployeeID, bool IsMonth, DateTime AttendanceDate,string From,string To)
        {
            return clsDALAttendanceReport.GetReport(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, WorkStatusID, EmployeeID, IsMonth, AttendanceDate, From, To);
        }
        public static DataSet GetPunchingReport(int EmployeeID, string From, string To)
        {
            return clsDALAttendanceReport.GetPunchingReport(EmployeeID, From, To); 
        }
        public static DataSet GetPunchingLogReport(string AttendanceDate, string AttendanceDateTo, int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
        {
            return clsDALAttendanceReport.GetPunchingLogReport(AttendanceDate,AttendanceDateTo, CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, WorkStatusID);
        }
        public static DataSet GetPunchingLogReport(int EmployeeID, string AttendanceDate, string AttendanceDateTo, int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
        {
            return clsDALAttendanceReport.GetPunchingLogReport(EmployeeID, AttendanceDate, AttendanceDateTo, CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, WorkStatusID);
        }
        public static DataTable GetSummaryReport(int CompanyID, int BranchID, bool IncludeCompany,int DepartmentID,int DesignationID, int WorkStatusID, int EmployeeID, string From,string To)
        {
            return clsDALAttendanceReport.GetSummaryReport(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, WorkStatusID, EmployeeID, From, To);  
        }
        public DataTable GetEmployeeDetailsAttendance(int intEmployeeID, int intDayID, string strDate, int intLocationID)
        {
            return clsDALAttendanceReport.GetEmployeeDetailsAttendance(intEmployeeID, intDayID, strDate, intLocationID);
        }
        public DataTable GetEmployeeDayEntryExitDetails(int intEmployeeID, string strDate, int intLocationID)
        {
            return clsDALAttendanceReport.GetEmployeeDayEntryExitDetails(intEmployeeID, strDate, intLocationID);
        }
        public DataTable GetEmployeeSummary(int intCompanyID, int intEmployeeID, int intBranchID, string strStartDate, string strEndDate, int intIncludeCompany, int intLocationID)
        {
            return clsDALAttendanceReport.GetEmployeeSummary(intCompanyID, intEmployeeID, intBranchID, strStartDate, strEndDate, intIncludeCompany, intLocationID);
        }
        public DataTable GetAttendanceDraft(int intCompanyID, int intEmployeeID, int intBranchID, string strStartDate, string strEndDate, int intIncludeCompany, int DesignationID, int DepartmentID)
        {
            return clsDALAttendanceReport.GetAttendanceDraft(intCompanyID, intEmployeeID, intBranchID, strStartDate, strEndDate, intIncludeCompany, DesignationID, DepartmentID);
        }
        public DataTable GetAttendanceDraft2(int intCompanyID, int intEmployeeID, int intBranchID, string strStartDate, string strEndDate, int intIncludeCompany, int DesignationID, int DepartmentID)
        {
            return clsDALAttendanceReport.GetAttendanceDraft2(intCompanyID, intEmployeeID, intBranchID, strStartDate, strEndDate, intIncludeCompany, DesignationID, DepartmentID);
        }
        public  DataTable GetEmployeeAttendnaceAllEmployee(int intMode, int intCompanyID, int intBranchID, int intEmployeeID, int intIncludeCompany, int intLocationID, string From, string To)
        {
            return clsDALAttendanceReport.GetEmployeeAttendnaceAllEmployee(intMode, intCompanyID, intBranchID, intEmployeeID, intIncludeCompany, intLocationID, From, To);
        }
        public static DataSet GetPunchingLogReportGrid(int EmployeeID, string AttendanceDate, string AttendanceDateTo, 
            int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
        {
            return clsDALAttendanceReport.GetPunchingLogReportGrid(EmployeeID, AttendanceDate, AttendanceDateTo, 
                CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, WorkStatusID);
        }
        public static DataSet GetPunchingLogReportGrid(string AttendanceDate, string AttendanceDateTo, int CompanyID, int BranchID, bool IncludeCompany, 
            int DepartmentID, int DesignationID, int WorkStatusID)
        {
            return clsDALAttendanceReport.GetPunchingLogReportGrid(AttendanceDate, AttendanceDateTo,  CompanyID, BranchID, 
                IncludeCompany, DepartmentID, DesignationID, WorkStatusID);
        }
    }
}
