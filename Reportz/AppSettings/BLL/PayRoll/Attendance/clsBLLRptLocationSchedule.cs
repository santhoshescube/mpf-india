﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyPayfriend
{
    public class clsBLLRptLocationSchedule
    {

        clsDALRptLocationSchedule MobjDALRptLocationSchedule = null;
        public clsBLLRptLocationSchedule() { }

        private clsDALRptLocationSchedule DALRptLocationSchedule
        {
            get
            {
                // Create instance of data access layer if null
                if (this.MobjDALRptLocationSchedule == null)
                    this.MobjDALRptLocationSchedule = new clsDALRptLocationSchedule();

                return this.MobjDALRptLocationSchedule;
            }
        }

        public DataTable FillCombos(string[] saFieldValues)
        {
            return DALRptLocationSchedule.FillCombos(saFieldValues);
        }

        public DataTable GetReport(int intCompanyID, int intBranchID, int intIncludeCompany, int intEmployeeID, int intLocationID, DateTime FromDate,DateTime ToDate)
        {
            return this.DALRptLocationSchedule.GetReport(intCompanyID, intBranchID, intIncludeCompany, intEmployeeID, intLocationID ,FromDate, ToDate);
        }

        public DataTable GetCompanyHeader(int intCompanyID)
        {
            return this.DALRptLocationSchedule.GetCompanyHeader(intCompanyID);
        }
   

    }
}

