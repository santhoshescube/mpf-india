﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;
using System.Data.SqlClient;

using System.IO;
using System.Drawing.Imaging;
using Microsoft.VisualBasic;

namespace MyPayfriend
{
    public partial class FrmReportviewer : Form
    {
        public int PiFormID;
        public Int64 PiRecId = 0;
        public int PintExecutive;
        public string PsExecutive;
        public string PsPeriod;
        public int ITem;
        public int PintoperationType;
        public bool PblnCustomerWiseDeliveryNote;//From Item ISsue Form HAndling 2 Type Print
        public bool PblnPickList;//For Hamdling Location Wise Print Report
        public int PiDiscountType;
        public int No;
        public string PintNo;
        public int PintCompany;
        public DataSet dt;
        int PiScale;

        public int PiVendorMode;
        public int PiVendorID;
        public string PsFromDate;
        public string PsToDate;
        public int PiStatus;
        public bool PbFinYearStart;

        public string sItem;
        public string PsReportPath;
        public string PsFormName;
        string PsReportFooter;
        public string Type;
        public string sDate;
        public string strAmountInWord;

        public double PTax;
        public double STax;
        public double TTax;
        public double Totals;
        public DateTime dteFromDate;
        public DateTime dteToDate;
        public int intMenuID;
        public Object objTemp;
        public string strAccHeader;
        private string MsReportPath;
        clsBLLReportViewer MobjclsBLLReportViewer;

        // Vendor/customer history related fields
        public int MiVendorMode;
        public DataSet CompanyHeader;
        public int intAccountID;
        public bool blnIsGroup;

        public bool IsReceipted { get; set; }
        public int OperationTypeID { get; set; }
        public int DocumentTypeID { get; set; }
        public int DocumentID { get; set; }
        public int OrderID { get; set; }
        public int CurrentIndex { get; set; }
        public int intAlertType { get; set; }
        public string strFromDate;
        public string strToDate;
        public int intCompanyID { get; set; }
        public string strSearchKey { get; set; }
        public int intPageSize { get; set; }
        public int intPageIndex { get; set; }

        public FrmReportviewer()
        {
            InitializeComponent();

            PsReportFooter = ClsCommonSettings.ReportFooter;
            PiScale = ClsCommonSettings.Scale;
            MobjclsBLLReportViewer = new clsBLLReportViewer();
        }

        private void FrmReportviewer_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.Text = PsFormName;
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                switch (PiFormID)
                {
                    case (int)FormID.CompanyInformation:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyFormArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyForm.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_GaReportCompanyForm", MobjclsBLLReportViewer.DisplayCompanyReport()));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyAccountBankDetails", MobjclsBLLReportViewer.DisplayCompanyBankReport()));
                            break;
                        }
                    case (int)FormID.BankInformation:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptBankInformationFormArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptBankInformationForm.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_BankNameReference", MobjclsBLLReportViewer.DisplayBankReport()));
                            break;
                        }
                    case (int)FormID.Employee:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeInformationArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeInformation.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;

                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataTable dt = MobjclsBLLReportViewer.DisplayEmployeeReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsBLLReportCommon.GetCompanyHeader(PintCompany, -1, true)));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_EmployeeMaster", dt));
                            break;
                        }
                    case (int)FormID.ShiftSchedule:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptShiftScheduleArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptShiftSchedule.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;

                            DataSet dsShiftInfo = MobjclsBLLReportViewer.DisplayShiftScheduleDetails();
                            DataTable datCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                            ReportParameter[] ReportParameter = new ReportParameter[2];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);

                            string strTemp = "";

                            if (ClsCommonSettings.Glb24HourFormat)
                            {
                                strTemp = Strings.Format(Convert.ToDateTime(dsShiftInfo.Tables[0].Rows[0]["FromTime"]), "HH:mm ") + " To " +
                                     "" + Strings.Format(Convert.ToDateTime(dsShiftInfo.Tables[0].Rows[0]["ToTime"]), "HH:mm tt") +
                                     " [Duration = " + dsShiftInfo.Tables[0].Rows[0]["Duration"] + "," +
                                     " Min.WorkHrs = " + dsShiftInfo.Tables[0].Rows[0]["MinWorkingHours"] + "]";
                            }
                            else
                            {
                                strTemp = Strings.Format(Convert.ToDateTime(dsShiftInfo.Tables[0].Rows[0]["FromTime"]), "hh:mm tt") + " To " +
                                      "" + Strings.Format(Convert.ToDateTime(dsShiftInfo.Tables[0].Rows[0]["ToTime"]), "hh:mm tt") +
                                      " [Duration = " + dsShiftInfo.Tables[0].Rows[0]["Duration"] + "," +
                                      " Min.WorkHrs = " + dsShiftInfo.Tables[0].Rows[0]["MinWorkingHours"] + "]";
                            }

                            ReportParameter[1] = new ReportParameter("HourFormat", strTemp, false);

                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetShiftSchedule_ShiftScheduleMaster", dsShiftInfo.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", datCompany));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetShiftSchedule_ShiftScheduleDetails", dsShiftInfo.Tables[1]));
                            break;
                        }

                    case (int)FormID.ExchangeCurrency:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptExchangeCurrencyFormArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptExchangeCurrencyForm.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayExchangeCurrencyReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_STCompanyCurrency", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_STExchangeCurrency", dtSet.Tables[1]));
                            break;
                        }
                    //case (int)FormID.AlertSetting:
                    //    {
                    //        MsReportPath = Application.StartupPath + "\\MainReports\\RptAlertForm.rdlc";
                    //        MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                    //        reportViewer1.LocalReport.ReportPath = MsReportPath;
                    //        ReportParameter[] ReportParameter = new ReportParameter[1];
                    //        ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                    //        reportViewer1.LocalReport.SetParameters(ReportParameter);
                    //        reportViewer1.LocalReport.DataSources.Clear();
                    //        DataSet dtSet = MobjclsBLLReportViewer.DisplayAlertSettingsReport();
                    //        DataTable dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                    //        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    //        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STAlertSettings", dtSet.Tables[0]));
                    //        reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STAlertRoleSettings", dtSet.Tables[1]));
                    //        break;
                    //    }
                    case (int)FormID.RoleSettings:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptRoleSettingsFormArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptRoleSettingsForm.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayRoleSettingsReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSettingsFormReport_STRoleSettings", dtSet.Tables[0]));
                            break;
                        }
                    case (int)FormID.LeavePolicy:
                        {
                            //

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\rptCompanyLeaveDetailArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\rptCompanyLeaveDetail.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayLeavePolicy();
                            //int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                            //dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_LeavePolicyMaster", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_LeavePolicyDetail", dtSet.Tables[1]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_LeavePolicyConsequences", dtSet.Tables[2]));

                            break;
                        }
                    case (int)FormID.ShiftPolicy:
                        {
                            //

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyShiftDetailsArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyShiftDetails.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayShiftPolicy();
                            //int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                            DataTable dtComp = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                            if (ClsCommonSettings.Glb24HourFormat == false && dtSet.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < dtSet.Tables[0].Rows.Count; i++)
                                {
                                    if (dtSet.Tables[0].Rows[i]["FromTime"] != DBNull.Value)
                                        dtSet.Tables[0].Rows[i]["FromTime"] = ConvertTo12Hr(Convert.ToString(dtSet.Tables[0].Rows[i]["FromTime"]));
                                    if (dtSet.Tables[0].Rows[i]["ToTime"] != DBNull.Value)
                                        dtSet.Tables[0].Rows[i]["ToTime"] = ConvertTo12Hr(Convert.ToString(dtSet.Tables[0].Rows[i]["ToTime"]));
                                    if (Convert.ToInt32(dtSet.Tables[0].Rows[i]["ShiftTypeID"]) == 4)
                                    {
                                        if (dtSet.Tables[0].Rows[i]["FromTimeD"] != DBNull.Value)
                                            dtSet.Tables[0].Rows[i]["FromTimeD"] = ConvertTo12Hr(Convert.ToString(dtSet.Tables[0].Rows[i]["FromTimeD"]));
                                        if (dtSet.Tables[0].Rows[i]["ToTimeD"] != DBNull.Value)
                                            dtSet.Tables[0].Rows[i]["ToTimeD"] = ConvertTo12Hr(Convert.ToString(dtSet.Tables[0].Rows[i]["ToTimeD"]));
                                    }
                                }
                            }
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtComp));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_ShiftPolicy", dtSet.Tables[0]));
                            //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_ShiftPolicyDetail", dtSet.Tables[1]));
                            break;
                        }
                    case (int)FormID.WorkPolicy:
                        {
                            //

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptPolicyMasterArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptPolicyMaster.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayWorkPolicy();
                            int companyid = ClsCommonSettings.CurrentCompanyID;
                            dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_WorkPolicy", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_WorkPolicyDetail", dtSet.Tables[1]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_WorkPolicyConsequence", dtSet.Tables[2]));
                            break;
                        }
                    case (int)FormID.OvertimePolicy:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptOTPolicyArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptOTPolicy.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSetOTPolicy = MobjclsBLLReportViewer.DisplayOvertimePolicy();

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetOTPolicy.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPolicy_OvertimePolicy", dtSetOTPolicy.Tables[1]));
                            break;
                        }


                    case (int)FormID.DocumentIssueReceipt:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                if (IsReceipted)
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptDocumentReceiptArb.rdlc";
                                }
                                else
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptDocumentIssueArb.rdlc";
                                }
                            }
                            else
                            {
                                if (IsReceipted)
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptDocumentReceipt.rdlc";
                                }
                                else
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptDocumentIssue.rdlc";
                                }
                            }

                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intStatusID = PiStatus;

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();




                            // MobjclsBLLReportViewer.DisplayReceiptIssue();

                            DataTable dt = clsBLLReportViewer.GetDocumentReceiptIssueDetails(OperationTypeID, DocumentTypeID, DocumentID, OrderID);
                            int companyid = 0;
                            DataTable dtCompany = new DataTable();
                            if (dt.Rows.Count > 0)
                            {
                                companyid = Convert.ToInt32(dt.Rows[0]["CompanyID"]);
                            }
                            if (companyid != 0)
                                dtCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid).Tables[0];
                            else
                            {
                                dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                            //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetOTPolicy.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtDocument", dt));
                            break;
                        }

                    case (int)FormID.Documents:
                        {

                            int StatusID = -2;
                            string Currentstatus = "";
                            string ReferenceNo = "";

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptOtherDocumentArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptOtherDocument.rdlc";

                            }

                            DataTable dt = clsBLLReportViewer.GetOtherDocumentDetails(CurrentIndex, DocumentTypeID, DocumentID, OperationTypeID);

                            if (dt.Rows.Count > 0)
                            {
                                DataRow dr = dt.Rows[0];

                                StatusID = dr["StatusID"].ToInt16();



                                if (StatusID == -1)
                                    Currentstatus = "New";
                                else if (StatusID == 0)
                                    Currentstatus = "Issued";
                                else
                                    Currentstatus = "Received";




                                int companyid = 0;
                                DataTable dtCompany = new DataTable();
                                if (dt.Rows.Count > 0)
                                {
                                    companyid = Convert.ToInt32(dt.Rows[0]["CompanyID"]);
                                }
                                if (companyid != 0)
                                    dtCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid).Tables[0];
                                else
                                {
                                    dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                                }


                                if (OperationTypeID == (int)OperationType.Company)
                                    ReferenceNo = "Company Name";
                                else
                                    ReferenceNo = "Employee Name";

                                reportViewer1.LocalReport.ReportPath = MsReportPath;
                                ReportParameter[] ReportParameter = new ReportParameter[3];
                                ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                ReportParameter[1] = new ReportParameter("CurrentStatus", Currentstatus, false);
                                ReportParameter[2] = new ReportParameter("ReferenceNo", ReferenceNo, false);
                                reportViewer1.LocalReport.SetParameters(ReportParameter);
                                reportViewer1.LocalReport.DataSources.Clear();

                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                                //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtSetOTPolicy.Tables[0]));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtOtherDocument", dt));
                            }
                            break;
                        }

                    case (int)FormID.SalaryStructure:
                        {
                            decimal decAddAmt = 0, decDedAmt = 0;

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptSalaryStructureArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptSalaryStructure.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            DataSet dtSet = MobjclsBLLReportViewer.DisplaySalaryStructure();
                            if (dtSet.Tables[1].Rows.Count > 0)
                            {
                                decAddAmt = dtSet.Tables[1].Compute("SUM(Amount)", "IsAddtion ='true'").ToDecimal();
                                decDedAmt = dtSet.Tables[1].Compute("SUM(Amount)", "IsAddtion ='false'").ToDecimal();
                            }
                            ReportParameter[] ReportParameter = new ReportParameter[3];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("AdditionAmount", decAddAmt.ToString(), false);
                            ReportParameter[2] = new ReportParameter("DeductionAmount", decDedAmt.ToString(), false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                            dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructure", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructureDetail", dtSet.Tables[1]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_OtherRemuneration", dtSet.Tables[2]));
                            break;
                        }
                    case (int)FormID.EmployeeLeaveStructure:
                        {
                            string strFromDate = PsPeriod.Split(Convert.ToChar("-"))[0];

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeLeaveStructureArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeLeaveStructure.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            DataSet dtSet = MobjclsBLLReportViewer.DisplayLeaveStructure(PintExecutive, PintCompany, strFromDate, PsPeriod);

                            ReportParameter[] ReportParameter = new ReportParameter[3];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("Employee", PsExecutive, false);
                            ReportParameter[2] = new ReportParameter("Period", PsPeriod, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(PintCompany);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveSummary_dtLeaveSummary", dtSet.Tables[1]));

                            break;
                        }

                    case (int)FormID.SalaryRelease:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeePaymentReleaseArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeePaymentRelease.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompany = PintCompany;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.strDate = sDate;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;

                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);

                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSet = MobjclsBLLReportViewer.DisplaySalaryRelease();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeepaymentRelease_EmployeePaymentDetailRelease", dtSet.Tables[0]));
                            break;
                        }

                    case (int)FormID.WorkLocation:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptWorkLocationArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptWorkLocation.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(intCompanyID); ;
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtWorkLocation", MobjclsBLLReportViewer.DisplayWorkLocation()));
                            break;
                        }
                    case (int)FormID.DeductionPolicy:
                        {
                            //

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptDeductionPolicyArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptDeductionPolicy.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                            DataSet dtSet = MobjclsBLLReportViewer.DisplayDeductionPolicy();
                            //int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                            //dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetDeductionPolicy_DeductionPolicy", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetDeductionPolicy_DeductionParticulars", dtSet.Tables[1]));


                            break;
                        }
                    case (int)FormID.LeaveEntry:
                        {
                            //

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptLeaveDetailsArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptLeaveDetails.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                            DataSet dtSet = MobjclsBLLReportViewer.DisplayEmployeeLeaveEntry();
                            //int companyid = Convert.ToInt32(dtSet.Tables[0].Rows[0]["CompanyID"]);
                            //dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(companyid);
                            //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_EmployeeLeaveDetails", dtSet.Tables[0]));
                            //  reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_LeaveOpening", dtSet.Tables[0]));


                            break;
                        }
                    case (int)FormID.LeaveExtension:
                        {
                            //

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptLeaveExtensionSummaryArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptLeaveExtensionSummary.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[2];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("EmployeeName", PsExecutive, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataTable dtCompany = new DataTable();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompany = PintCompany;
                            ////dtCompany = MobjclsBLLReportViewer.DisplayCompanyHeader().Tables[0];
                            ////if(dtCompany.Rows.Count==0)
                            //   dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                            DataTable dtLeave = MobjclsBLLReportViewer.DisplayLeaveExtension(PintExecutive, PintCompany);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtLeave));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveExtension_DtSetLeaveExtensionSummary", dtLeave));
                            break;
                        }
                    case (int)FormID.CompanyAssets:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyAssetsArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyAssets.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsBLLReportCommon.GetCompanyHeader(PintCompany, -1, true)));
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = PintCompany;
                            DataTable dtcompany = MobjclsBLLReportViewer.DisplayCompanyAssetReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyAssets", dtcompany));
                            break;
                        }
                    case (int)FormID.Visa:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptVisaArb.rdlc";


                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptVisa.rdlc";


                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;

                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            DataTable dtcompany = MobjclsBLLReportViewer.DisplayVisaReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeVisaDetails", dtcompany));
                            break;
                        }
                    case (int)FormID.EmirateHealthCard:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmirateHealthCardArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmirateHealthCard.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[2];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            ReportParameter[1] = new ReportParameter("HeaderName", ClsCommonSettings.NationalityCard, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;

                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            //DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            DataTable dtcompany = MobjclsBLLReportViewer.DisplayEmirateHealthReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmiratesHealthCard", dtcompany));
                            break;
                        }
                    case (int)FormID.HealthCard:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptHealthCardArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptHealthCard.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;

                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            DataTable dtcompany = MobjclsBLLReportViewer.DisplaHealthCardReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtHealthCard", dtcompany));
                            break;
                        }
                    case (int)FormID.LabourCard:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptLabourCardArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptLabourCard.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;

                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            DataTable dtcompany = MobjclsBLLReportViewer.DisplayLabourCardReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtLabourCard", dtcompany));
                            break;
                        }
                    case (int)FormID.VacationEntry:
                        {
                            DataSet DST;
                            DataTable T1;
                            DataTable T2;

                            if (OperationTypeID == 1000)
                            {
                                if (ClsCommonSettings.IsArabicView)
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeVacationProcessArb.rdlc";
                                else
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeVacationProcess.rdlc";
                            }
                            else if (OperationTypeID == 2000)
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeVacationProcess2.rdlc";

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            DST = MobjclsBLLReportViewer.VacationProcessReport();
                            T1 = DST.Tables[0];
                            T2 = DST.Tables[1];
                            DataSet dtSet = new DataSet();                            

                            if (OperationTypeID == 1000)
                            {
                                ReportParameter[] ReportParameter = new ReportParameter[3];
                                ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                ReportParameter[1] = new ReportParameter("Scale", ClsCommonSettings.Scale.ToStringCustom(), false);
                                ReportParameter[2] = new ReportParameter("bComDisplay", "true", true);
                                reportViewer1.LocalReport.SetParameters(ReportParameter);
                            }
                            else if (OperationTypeID == 2000)
                            {
                                MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = T1.Rows[0]["SalaryStructureID"].ToInt64();
                                dtSet = MobjclsBLLReportViewer.DisplaySalaryStructure();
                                decimal decAddAmt = 0, decDedAmt = 0;

                                ReportParameter[] ReportParameter = new ReportParameter[5];
                                ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                ReportParameter[1] = new ReportParameter("Scale", ClsCommonSettings.Scale.ToStringCustom(), false);
                                ReportParameter[2] = new ReportParameter("bComDisplay", "true", true);

                                if (dtSet.Tables[1].Rows.Count > 0)
                                {
                                    decAddAmt = dtSet.Tables[1].Compute("SUM(Amount)", "IsAddtion ='true'").ToDecimal();
                                    decDedAmt = dtSet.Tables[1].Compute("SUM(Amount)", "IsAddtion ='false'").ToDecimal();
                                }

                                ReportParameter[3] = new ReportParameter("AdditionAmount", decAddAmt.ToString(), false);
                                ReportParameter[4] = new ReportParameter("DeductionAmount", decDedAmt.ToString(), false);
                                reportViewer1.LocalReport.SetParameters(ReportParameter);
                            }
                            
                            reportViewer1.LocalReport.DataSources.Clear();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeVacationProcess_VacationMaster", T1));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeVacationProcess_VacationDetail", T2));

                            // Salary Structure
                            if (OperationTypeID == 2000)
                            {                                
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructureDetail", dtSet.Tables[1]));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_OtherRemuneration", dtSet.Tables[2]));
                            }
                            // End
                            break;
                        }
                    case (int)FormID.Qualification:
                        {

                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeQualificationdetailsArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeQualificationdetails.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;
                            // DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            DataTable dtQualifiacation = MobjclsBLLReportViewer.QualificationDetails();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_Qualification", dtQualifiacation));
                            break;
                        }
                    case (int)FormID.DrivingLicense:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptDrivingLicenceArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptDrivingLicence.rdlc";


                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();

                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;

                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            DataTable dtLicenseDetails = clsBLLReportViewer.DrivingLicenseDetails(PiRecId.ToInt32());

                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeLicenceDetails", dtLicenseDetails));
                            break;
                        }
                    case (int)FormID.SettlementProcess:
                        {

                            int HasSalary;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;

                            if (OperationTypeID <=0)
                            {
                                if (ClsCommonSettings.IsArabicView)
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeSettlementArb.rdlc";

                                }
                                else
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeSettlement.rdlc";
                                }

                     

                                reportViewer1.LocalReport.DataSources.Clear();
                                reportViewer1.LocalReport.ReportPath = MsReportPath;

                                DataTable datDetail;

                                datDetail = MobjclsBLLReportViewer.EmpSettlementDetails();
                                HasSalary = 0;

                                DataTable datmaster; int Pscale = 0;
                                datmaster = MobjclsBLLReportViewer.EmpSettlement();
                                if (datmaster.Rows[0]["Scale"] == System.DBNull.Value)
                                {
                                    Pscale = 2;
                                }
                                else
                                {
                                    Pscale = Convert.ToInt32(datmaster.Rows[0]["Scale"]);
                                }
                                ReportParameter[] RepParam = new ReportParameter[4];
                                RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                RepParam[1] = new ReportParameter("Scale", Pscale.ToString(), false);
                                RepParam[2] = new ReportParameter("HasSalary", HasSalary.ToString(), false);
                                RepParam[3] = new ReportParameter("bComDisplay", "true", false);
                                reportViewer1.LocalReport.SetParameters(RepParam);

                                DataTable dtCompany = Report.GetReportHeader(datmaster.Rows[0]["CompanyID"].ToInt32(), 0, true);

                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmployeeSettlement", datmaster));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmployeeSettlementDetail", datDetail));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                            }
                            else if (OperationTypeID == 1000)
                            {
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                                if (ClsCommonSettings.IsArabicView)
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptGratuityReportArb.rdlc";
                                }
                                else
                                {
                                    MsReportPath = Application.StartupPath + "\\MainReports\\RptGratuityReport.rdlc";
                                }

                                reportViewer1.LocalReport.DataSources.Clear();
                                reportViewer1.LocalReport.ReportPath = MsReportPath;

                                DataTable datSettDtls;
                                DataTable datLeaveDtls;
                                DataTable datGLADtls;
                                DataTable datPayEmployeeSettlementGratuity;
                                DataTable datLastDtls;
                                DataSet DTSet;

                                DTSet = MobjclsBLLReportViewer.EmpGratuityReport();

                                HasSalary = 0;

                                datSettDtls = DTSet.Tables[0];
                                datLeaveDtls = DTSet.Tables[1];
                                datGLADtls = DTSet.Tables[2];
                                datPayEmployeeSettlementGratuity = DTSet.Tables[3];
                                datLastDtls = DTSet.Tables[4];



                                ReportParameter[] RepParam = new ReportParameter[1];
                                RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                reportViewer1.LocalReport.SetParameters(RepParam);

                                DataTable dtCompany = Report.GetReportHeader(datSettDtls.Rows[0]["CompanyID"].ToInt32(), 0, true);

                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_SettDtls", datSettDtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_LeaveDtls", datLeaveDtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_GLADtls", datGLADtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_PayEmployeeSettlementGratuity", datPayEmployeeSettlementGratuity));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_LastDtls", datLastDtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));

                                //////////////////////////////////////////////////////////////////////////////////////////////////////////

                            }
                            else if (OperationTypeID == 2000)
                            {
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeSettlnt1.rdlc";

                                reportViewer1.LocalReport.DataSources.Clear();
                                reportViewer1.LocalReport.ReportPath = MsReportPath;
                                string NetAmt="0";
                                DataTable datSettDtls;
                                DataTable datLeaveDtls;
                                DataTable datGLADtls;
                                DataTable datPayEmployeeSettlementGratuity;
                                DataSet DTSet;

                                DTSet = MobjclsBLLReportViewer.EmpGratuityReportNew();

                                HasSalary = 0;

                                datSettDtls = DTSet.Tables[0];
                                datLeaveDtls = DTSet.Tables[1];
                                datGLADtls = DTSet.Tables[2];
                                datPayEmployeeSettlementGratuity = DTSet.Tables[3];


                                if (datPayEmployeeSettlementGratuity.Rows.Count > 0)
                                {
                                    string sFilterExpression = "AdditionDeduction='1000'";
                                    DataRow[] DataRow;
                                    DataRow = datPayEmployeeSettlementGratuity.Select(sFilterExpression);
                                     if (DataRow.Count() > 0)
                                     {
                                         NetAmt = DataRow[0]["Credit"].ToString();
                                     }
                                }

                                ReportParameter[] RepParam = new ReportParameter[4];
                                RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                RepParam[1] = new ReportParameter("NetAmt", NetAmt, false);
                                RepParam[2] = new ReportParameter("HasSalary", HasSalary.ToString(), false);
                                RepParam[3] = new ReportParameter("bComDisplay", "true", false);
                                reportViewer1.LocalReport.SetParameters(RepParam);


                                DataTable dtCompany = Report.GetReportHeader(datSettDtls.Rows[0]["CompanyID"].ToInt32(), 0, true);

                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlementService", datSettDtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlementSalary", datLeaveDtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlementVacation", datGLADtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlementDlts", datPayEmployeeSettlementGratuity));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));

                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                            }
                            else if (OperationTypeID == 3000)
                            {
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeSettlnt2.rdlc";

                                reportViewer1.LocalReport.DataSources.Clear();
                                reportViewer1.LocalReport.ReportPath = MsReportPath;
                                string NetAmt = "0";
                                DataTable datSettDtls;
                                DataTable datLeaveDtls;
                                DataTable datGLADtls;
                                DataTable datPayEmployeeSettlementGratuity;
                                DataSet DTSet;

                                DTSet = MobjclsBLLReportViewer.spPayGratuityReportNewVerOne();

                                HasSalary = 0;

                                datSettDtls = DTSet.Tables[0];
                                datLeaveDtls = DTSet.Tables[1];
                                datGLADtls = DTSet.Tables[2];
                                datPayEmployeeSettlementGratuity = DTSet.Tables[3];


                                //if (datPayEmployeeSettlementGratuity.Rows.Count > 0)
                                //{

                                //    DataRow[] DataRow;
                                //    DataRow = datPayEmployeeSettlementGratuity.Select(sFilterExpression);
                                //    if (DataRow.Count() > 0)
                                //    {
                                //        NetAmt = DataRow[0]["Credit"].ToString();
                                //    }
                                //}

                                ReportParameter[] RepParam = new ReportParameter[4];
                                RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                RepParam[1] = new ReportParameter("NetAmt", NetAmt, false);
                                RepParam[2] = new ReportParameter("HasSalary", HasSalary.ToString(), false);
                                RepParam[3] = new ReportParameter("bComDisplay", "true", false);
                                reportViewer1.LocalReport.SetParameters(RepParam);


                                DataTable dtCompany = Report.GetReportHeader(datSettDtls.Rows[0]["CompanyID"].ToInt32(), 0, true);

                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlement1", datSettDtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlement2", datLeaveDtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlement3", datGLADtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlement4", datPayEmployeeSettlementGratuity));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));

                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                            }
                            else if (OperationTypeID == 4000)
                            {
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeSettlnt2.rdlc";

                                reportViewer1.LocalReport.DataSources.Clear();
                                reportViewer1.LocalReport.ReportPath = MsReportPath;
                                string NetAmt = "0";
                                DataTable datSettDtls;
                                DataTable datLeaveDtls;
                                DataTable datGLADtls;
                                DataTable datPayEmployeeSettlementGratuity;
                                DataSet DTSet;

                                DTSet = MobjclsBLLReportViewer.spPayGratuityReportNewVerOne5();

                                HasSalary = 0;

                                datSettDtls = DTSet.Tables[0];
                                datLeaveDtls = DTSet.Tables[1];
                                datGLADtls = DTSet.Tables[2];
                                datPayEmployeeSettlementGratuity = DTSet.Tables[3];


                                //if (datPayEmployeeSettlementGratuity.Rows.Count > 0)
                                //{

                                //    DataRow[] DataRow;
                                //    DataRow = datPayEmployeeSettlementGratuity.Select(sFilterExpression);
                                //    if (DataRow.Count() > 0)
                                //    {
                                //        NetAmt = DataRow[0]["Credit"].ToString();
                                //    }
                                //}

                                ReportParameter[] RepParam = new ReportParameter[4];
                                RepParam[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                                RepParam[1] = new ReportParameter("NetAmt", NetAmt, false);
                                RepParam[2] = new ReportParameter("HasSalary", HasSalary.ToString(), false);
                                RepParam[3] = new ReportParameter("bComDisplay", "true", false);
                                reportViewer1.LocalReport.SetParameters(RepParam);


                                DataTable dtCompany = Report.GetReportHeader(datSettDtls.Rows[0]["CompanyID"].ToInt32(), 0, true);

                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlement1", datSettDtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlement2", datLeaveDtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlement3", datGLADtls));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmpSettlement4", datPayEmployeeSettlementGratuity));
                                reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));

                                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                            }
                            /*******************************************************************************************/
                            break;
                        }

                    case (int)FormID.EmployeeLoan:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeLoanArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeLoan.rdlc";

                            }
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            DataSet dtSet = MobjclsBLLReportViewer.GetEmployeeLoanReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtSetEmployeeLoan_dtLoan", dtSet.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtSetEmployeeLoan_dtInstallmentDetails", dtSet.Tables[1]));
                            break;
                        }
                    case (int)FormID.InsuranceCard:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeInsuranceCardArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeInsuranceCard.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;
                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            //DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            DataTable dtcompany = MobjclsBLLReportViewer.DisplayInsuranceCardReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtInsuranceCard", dtcompany));
                            break;
                        }
                    case (int)FormID.InsuranceDetails:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyInsuranceArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyInsurance.rdlc";


                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;
                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            //DataTable dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            DataTable dtcompany = MobjclsBLLReportViewer.DisplayInsuranceDetailsReport();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtInsuranceDetails", dtcompany));
                            break;
                        }
                    case (int)FormID.AlertInfo:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\rptAlertInfoReportArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\rptAlertInfoReport.rdlc";


                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.strSearchKey = strSearchKey;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intAlertType = intAlertType;
                            MobjclsBLLReportViewer.clsDTOReportviewer.strFromDate = strFromDate;
                            MobjclsBLLReportViewer.clsDTOReportviewer.strToDate = strToDate;
                            DataTable dtCompany = new clsBLLReportViewer().DisplayCompanyDefaultHeader();
                            DataSet dsAlerts = MobjclsBLLReportViewer.GetAlerts();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtAlertReport", dsAlerts.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtProbationAlert", dsAlerts.Tables[1]));
                            break;
                        }

                    case (int)FormID.Expense:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeExpenseArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeExpense.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = PintCompany;
                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            DataTable dtExpense = MobjclsBLLReportViewer.GetPrintExpenseDetails();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_EmployeeExpense", dtExpense));
                            break;
                        }

                    case (int)FormID.Deposit:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptDepositArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptDeposit.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = PintCompany;
                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            DataTable dtDeposit = MobjclsBLLReportViewer.GetPrintDepositDetails();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_Deposit", dtDeposit));
                            break;
                        }

                    case (int)FormID.LoanRepayment:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptLoanRepaymentArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptLoanRepayment.rdlc";


                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = PintCompany;
                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            DataSet dsLoanRepayment = MobjclsBLLReportViewer.GetPrintLoanRepaymentDetails();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_LoanRepayment", dsLoanRepayment.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_LoanRepaymentDetails", dsLoanRepayment.Tables[1]));
                            break;
                        }

                    case (int)FormID.SalaryAdvance:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptSalaryAdvanceArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptSalaryAdvance.rdlc";

                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = PintCompany;
                            DataTable dt = new DataTable();
                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dt = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dt = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }
                            DataTable dtSalaryAdvance = MobjclsBLLReportViewer.GetPrintSalaryAdvanceDetails();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dt));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_SalaryAdvance", dtSalaryAdvance));
                            break;
                        }
                    case (int)FormID.TradeLicense:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyTradeLicenseArb.rdlc";
                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyTradeLicense.rdlc";

                            }

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;
                            DataTable dtCompany = new DataTable();

                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dtCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }

                            DataTable dt = clsBLLReportViewer.DisplayTradeLicenseReport(PiRecId);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtTradeLicense", dt));
                            break;
                        }
                    case (int)FormID.LeaseAgreement:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyLeaseAgreementArb.rdlc";
                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptCompanyLeaseAgreement.rdlc";
                            }

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = intCompanyID;
                            DataTable dtCompany = new DataTable();

                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                dtCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                            {
                                dtCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();
                            }

                            DataTable dt = clsBLLReportViewer.DisplayLeaseAgreementReport(PiRecId);
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtLeaseAgreement", dt));
                            break;
                        }

                    case (int)FormID.AssetHandover:
                        {
                            if (ClsCommonSettings.IsArabicView)
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptAssetHandoverArb.rdlc";

                            }
                            else
                            {
                                MsReportPath = Application.StartupPath + "\\MainReports\\RptAssetHandover.rdlc";


                            }
                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;

                            DataSet dsAssetHandover = MobjclsBLLReportViewer.DisplayAssetHandoverDetails();
                            DataTable datCompany = new DataTable();

                            if (MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID != 0)
                                datCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            else
                                datCompany = MobjclsBLLReportViewer.DisplayCompanyDefaultHeader();

                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_AssetHandover", dsAssetHandover.Tables[0]));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", datCompany));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayEmployeeLeaveEntry_AssetHandoverDetails", dsAssetHandover.Tables[1]));
                            break;
                        }

                    case (int)FormID.SalesInvoice:
                        {
                            if (ClsCommonSettings.IsArabicView)
                                MsReportPath = Application.StartupPath + "\\MainReports\\rptSalesInvoiceArb.rdlc";
                            else
                                MsReportPath = Application.StartupPath + "\\MainReports\\rptSalesInvoice.rdlc";

                            reportViewer1.LocalReport.ReportPath = MsReportPath;
                            MobjclsBLLReportViewer.clsDTOReportviewer.intRecId = PiRecId;

                            DataTable datSalesInvoice = MobjclsBLLReportViewer.DisplaySalesInvoiceDetails();

                            if (datSalesInvoice != null)
                            {
                                if (datSalesInvoice.Rows.Count > 0)
                                    MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID = datSalesInvoice.Rows[0]["CompanyID"].ToInt32();
                            }

                            DataTable datCompany = MobjclsBLLReportViewer.DisplayCompanyReportHeader(MobjclsBLLReportViewer.clsDTOReportviewer.intCompanyID).Tables[0];
                            
                            ReportParameter[] ReportParameter = new ReportParameter[1];
                            ReportParameter[0] = new ReportParameter("GeneratedBy", PsReportFooter, false);
                            reportViewer1.LocalReport.SetParameters(ReportParameter);
                            reportViewer1.LocalReport.DataSources.Clear();
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSalaryAdvanceSummary_dtSalesInvoice", datSalesInvoice));
                            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", datCompany));
                            break;
                        }
                }
                reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                reportViewer1.ZoomMode = ZoomMode.Percent;
                this.Cursor = Cursors.Default;
            }
            catch (Exception) { }
        }

        public void SubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", CompanyHeader.Tables[0]));
        }

        private string ConvertTo12Hr(string Time)
        {
            dtpHourFormat12.CustomFormat = "hh:mm tt";
            string msTime = Time;
            string ConvertedTime = "";
            dtpHourFormat12.Text = Time;
            ConvertedTime = dtpHourFormat12.Text;
            return ConvertedTime;
        }
    }
}
