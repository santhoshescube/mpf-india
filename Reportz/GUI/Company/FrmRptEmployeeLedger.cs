﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{
    public partial class FrmRptEmployeeLedger : Report
    {
        public int PiCompanyID;

        clsBLLRptAttendanceManual MobjclsBLLRptAttendanceManual = null;


        public FrmRptEmployeeLedger()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                expandablePanel1.TitleText = "تصفية حسب";
                lblCompany.Text = "شركة";
                lblBranch.Text = "الفرع";
                labelX1.Text = "عامل";
                chkIncludeCompany.Text = "وتشمل الشركة";
                btnShow.Text = "عرض";
            }
        }

        private void FrmRptEmployeeHistory_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
            if (PiCompanyID > 0)
            {
                cboCompany.SelectedValue = PiCompanyID;
            }
        }

       private clsBLLRptAttendanceManual BLLReportViewerManual
        {
            get
            {
                if (this.MobjclsBLLRptAttendanceManual == null)
                    this.MobjclsBLLRptAttendanceManual = new clsBLLRptAttendanceManual();

                return this.MobjclsBLLRptAttendanceManual;
            }
        }

          private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;

            string strCondition = string.Empty;

            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0 || intType == 1)
                {
                    datCombos = BLLReportViewerManual.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "ParentID = 0" });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                    blnRetvalue = true;
                }

                if (intType == 0 || intType == 2)
                {
                    datCombos = BLLReportViewerManual.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "ParentID = " + cboCompany.SelectedValue });
                    cboBranch.ValueMember = "CompanyID";
                    cboBranch.DisplayMember = "CompanyName";
                    DataRow dtRow;

                    dtRow = datCombos.NewRow();
                    dtRow["CompanyID"] = -1;
                    dtRow["CompanyName"] = "NONE";
                    datCombos.Rows.InsertAt(dtRow, 0);

                    if (datCombos.Rows.Count > 1)
                    {
                        dtRow = datCombos.NewRow();
                        dtRow["CompanyID"] = 0;
                        dtRow["CompanyName"] = "ALL";
                        datCombos.Rows.InsertAt(dtRow, 1);
                    }

                    cboBranch.DataSource = datCombos;
                    blnRetvalue = true;
                    cboEmployee.DataSource = null; 
                    cboEmployee.Items.Clear();
                    cboEmployee.Text = "";
                }



                if (intType == 0 || intType == 3 || intType == 2)
                {
                    strCondition = SetCompanySearchCondition();
               
                    datCombos = BLLReportViewerManual.FillCombos(new string[] { " DISTINCT EM.EmployeeID,EM.EmployeeFullName AS EmployeeName", "EmployeeMaster AS EM ", strCondition });

                    //DataRow dr = datCombos.NewRow();
                    //dr["EmployeeID"] = 0;
                    //dr["EmployeeName"] = "ALL";
                    //datCombos.Rows.InsertAt(dr, 0);

                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DisplayMember = "EmployeeName";
                    cboEmployee.DataSource = datCombos;

                }

              
            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }
          private void ShowReport()
          {
              DataSet DtSet;
              int intCompanyID = cboCompany.SelectedValue.ToInt32();
              int intIncludeCompany = chkIncludeCompany.Checked ? 1 : 0;
              int intBranchID = cboBranch.SelectedValue.ToInt32();
              int intEmployeeID = cboEmployee.SelectedValue.ToInt32();

              ReportViewer.Clear();
              ReportViewer.Refresh();

              DtSet = this.BLLReportViewerManual.GetEmployeeLedgerDetail(intEmployeeID,dtpFromDate.Value,dtpToDate.Value);

              if (DtSet.Tables[0].Rows.Count <= 0)
              {
                  UserMessage.ShowMessage(20075);
                  return;
              }
              else
              {

                  DataTable dtCompanyHeader = clsBLLReportCommon.GetCompanyHeader(intCompanyID, intBranchID, chkIncludeCompany.Checked);
                  DataTable EmployeeInfo = DtSet.Tables[0]; 
                  DataTable EmployeeVacation = DtSet.Tables[1];
                  DataTable EmployeeSalary = DtSet.Tables[2];


                this.ReportViewer.LocalReport.DataSources.Clear();
                  string strMReportPath = "";

                
             strMReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeLedger.rdlc";

              ReportParameter[] Parameters = new ReportParameter[2];
              Parameters[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
              Parameters[1] = new ReportParameter("IsDateSelected", "0", false);

                  this.ReportViewer.ProcessingMode = ProcessingMode.Local;
                  this.ReportViewer.LocalReport.ReportPath = strMReportPath;
                  this.ReportViewer.LocalReport.SetParameters(Parameters);
                  this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtsetRptEmployeeHistory_EmployeeInfo", EmployeeInfo));
                  this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtsetRptEmployeeHistory_EmployeeVacation", EmployeeVacation));
                  this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtsetRptEmployeeHistory_EmployeeSalary", EmployeeSalary));
                  this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompanyHeader));
                  this.ReportViewer.SetDisplayMode(DisplayMode.PrintLayout);
                  this.ReportViewer.ZoomMode = ZoomMode.Percent;
                  this.ReportViewer.ZoomPercent = 100;
              }
          }
          private string SetCompanySearchCondition()
          {
              string strCondition = string.Empty;

              if (cboCompany.SelectedValue != null && chkIncludeCompany.Checked)
              {
                  strCondition = strCondition + "EM.CompanyID = " + cboCompany.SelectedValue.ToInt32();
              }
              else if (cboCompany.SelectedValue.ToInt32() > 0)
              {
                  strCondition = strCondition + "EM.CompanyID = " + cboCompany.SelectedValue.ToInt32();
              }
              if (cboBranch.SelectedValue != null && cboBranch.SelectedValue.ToInt32() != -1)
              {
                  if (strCondition != string.Empty)
                      strCondition = strCondition + " OR ";

                  if (cboBranch.SelectedValue.ToInt32() == 0)
                      strCondition = strCondition + "EM.CompanyID IN (SELECT CompanyID FROM CompanyMaster WHERE ParentID = " + cboCompany.SelectedValue.ToInt32() + ")";
                  else
                      strCondition = strCondition + "EM.CompanyID = " + cboBranch.SelectedValue.ToInt32();
              }

              return strCondition;

          }

          private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
          {
              try
              {
                  if (cboCompany.SelectedValue != null)
                      LoadCombos(2);
              }
              catch
              { }
          }

          private void cboBranch_SelectedValueChanged(object sender, EventArgs e)
          {
              try
              {

                  LoadCombos(3);

              }
              catch
              {
              }
          }

          private void btnShow_Click(object sender, EventArgs e)
          {
              if (ValidateReport())
                  ShowReport();
          }

          private bool ValidateReport()
          {
              if (cboCompany.SelectedValue.ToInt32() <= 0)
              {
                  UserMessage.ShowMessage(20078);
                  cboCompany.Focus();
                  return false;
              }
              else if (cboBranch.SelectedValue.ToStringCustom() == string.Empty)
              {
                  UserMessage.ShowMessage(20079);
                  cboBranch.Focus();
                  return false;
              }

              else if (cboEmployee.SelectedValue.ToStringCustom() == string.Empty)
              {
                  UserMessage.ShowMessage(20081);
                  cboEmployee.Focus();
                  return false;
              }

              return true;
          }
          private clsMessage ObjUserMessage = null;

          private clsMessage UserMessage
          {
              get
              {
                  if (this.ObjUserMessage == null)
                      this.ObjUserMessage = new clsMessage(FormID.WorkSheetReport);
                  return this.ObjUserMessage;
              }
          }

          private void cboEmployee_KeyDown(object sender, KeyEventArgs e)
          {
              cboEmployee.DroppedDown = false;
          }
    }
}
