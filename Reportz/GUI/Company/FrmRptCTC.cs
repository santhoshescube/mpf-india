﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;


namespace MyPayfriend
{
    /*
       CREATED BY RAJESH
       CREATED ON 31-AUG-2013
       DESCRIPTION EMPLOYEE CTC REPORT
    */
    public partial class FrmRptCTC : Report
    {
        #region Properties
        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.EmployeeProfileMISReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor
        public FrmRptCTC()
        {
            InitializeComponent();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }

        }
        #endregion
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.CTCReport, this);
        }
        #endregion SetArabicControls

        #region FormEvents
        private void FrmRptEmployeeProfile_Load(object sender, EventArgs e)
        {
            
            LoadCombos();
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
            dtpMonth.Value = ("01/" + GetCurrentMonth(dtpMonth.Value.Month) + "/" + (dtpMonth.Value.Year) + "").ToDateTime();
            cboCompany_SelectedIndexChanged(null, null);
        }
        #endregion
        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }

        #region Functions

        /// <summary>
        /// Enable or disable the include company based on the branch value
        /// </summary>
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }

        /// <summary>
        /// Fill all company 
        /// </summary>
        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Fill all branch based on the company value
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

        }

        /// <summary>
        /// Fill all departments
        /// </summary>
        private void FillAllDeparments()
        {
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
        }

        /// <summary>
        /// Fill all employee work status
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
        }

        /// <summary>
        /// Fill all employees based on the filteration
        /// </summary>
        private void FillAllEmployees()
        {
            DataTable dtEmployee = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                            chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), -1, -1, cboWorkStatus.SelectedValue.ToInt32(), -1);
            dtEmployee.Rows.RemoveAt(0);
            cboEmployee.DataSource = dtEmployee;
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";

        }

        /// <summary>
        /// Fill all types
        /// 0->Monthly 
        /// 1->Financial year
        /// </summary>
        private void FillAllTypes()
        {
            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add("Type");
            dt.Columns.Add("TypeID");

            dr = dt.NewRow();
            dr["Type"] = "Month";
            dr["TypeID"] = 1;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Type"] = "Financial Year";
            dr["TypeID"] = 2;
            dt.Rows.Add(dr);


            cboType.DataSource = dt;
            cboType.DisplayMember = "Type";
            cboType.ValueMember = "TypeID";
        }

        /// <summary>
        /// Fill all financial year
        /// </summary>
        private void FillAllFinancialYear()
        {
            cboFinYear.DataSource = clsBLLReportCommon.GetAllFinancialYears(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            cboFinYear.DisplayMember = "FinYearPeriod";
            cboFinYear.ValueMember = "FinYearStartDate";
        }

        /// <summary>
        /// Enable or disable the financial year and month control based on the selected type
        /// </summary>
        private void EnableDisableFinancialYear()
        {
            dtpMonth.Enabled = cboFinYear.Enabled = false;
            if (cboType.SelectedIndex == 0)
                dtpMonth.Enabled = true;
            else
                cboFinYear.Enabled = true;
        }

        private void LoadCombos()
        {
            FillAllCompany();
            FillAllDeparments();
            FillAllWorkStatus();
            FillAllTypes();
            FillAllFinancialYear();

        }

        /// <summary>
        /// Validate all the controls in the form
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9708);
                return false;
            }

            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9709);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9710);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9711);
                return false;
            }

            if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9716);
                return false;

            }

            if (cboType.SelectedIndex == 1 && cboFinYear.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9717);
                return false;

            }


            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9713);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Show report function
        /// </summary>
        public void ShowReport()
        {

            try
            {
                DataSet dsEmployeeCTC;
                DateTime? Date;

                if (cboType.SelectedIndex == 1)
                    Date = null;
                else
                    Date = dtpMonth.Value.Date.AddMonths(1).AddDays(-dtpMonth.Value.Date.Day);

                this.rvEmployeeProfile.Reset();

                //Setting the report header 
                SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
                if (ClsCommonSettings.IsArabicView)
                {
                    this.rvEmployeeProfile.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeCTCArb.rdlc";
                }
                else
                {
                    this.rvEmployeeProfile.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeCTC.rdlc";
                }

                if (cboEmployee.SelectedValue.ToInt32() > 0)
                {
                    rvEmployeeProfile.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                    new ReportParameter("Department", cboDepartment.Text, false),
                    new ReportParameter("Employee",cboEmployee.Text, false),
                    new ReportParameter("Period",cboType.SelectedIndex == 0?"Month":"Financial Year", false),
                    new ReportParameter("PeriodValue",cboType.SelectedIndex == 0?dtpMonth.Text:cboFinYear.Text, false)
                   
                });

                    this.rvEmployeeProfile.LocalReport.DataSources.Clear();

                    dsEmployeeCTC = clsBLLRptCTC.GetEmployeeCTCReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboEmployee.SelectedValue.ToInt32(), Date, (cboType.SelectedIndex == 0 ? "" : cboFinYear.Text));

                    if (dsEmployeeCTC.Tables[0].Rows.Count > 0)
                    {
                        rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("dtSetCTC_dtCTC", dsEmployeeCTC.Tables[0]));
                        rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("dtSetCTC_dtAssetDetails", dsEmployeeCTC.Tables[1]));
                    }
                    else
                    {
                        UserMessage.ShowMessage(9707);
                        return;
                    }
                }
                SetReportDisplay(rvEmployeeProfile);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Loading the employees based on the filteration
        /// </summary>
        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        #endregion

        #region ControlEvents

        /// <summary>
        /// CboBranch Selected index change event
        /// </summary>
        /// 
        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Enable or disable the include company based on the branch value
            EnableDisableIncludeCompany();

            //Fill All Financial Year based on Company
            FillAllFinancialYear();
            //Fill all employees based on the branch value
            FillAllEmployees();

        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                rvEmployeeProfile.Reset();
                //validate the controls
                if (ValidateForm())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
            this.Cursor = Cursors.Default;
        }

        /// <summary>
        /// CboCompany selected index change event
        /// </summary>
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
        }

        /// <summary>
        /// chkIncludeCompany_CheckedChanged event
        /// </summary>
        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            //Fill All Financial Year based on Company
            FillAllFinancialYear();
            // Fill All Employees based on Company
            FillAllEmployees();
        }

        public void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }

        /// <summary>
        /// Enable or disable the financial year and month controls based on the type value
        /// </summary>
        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableFinancialYear();
        }
        #endregion

        #region ReportViewer Events
        private void rvEmployeeProfile_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvEmployeeProfile_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {

        }

        private void cboFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void labelX4_Click(object sender, EventArgs e)
        {

        }

        private void labelX3_Click(object sender, EventArgs e)
        {

        }

    }
}
