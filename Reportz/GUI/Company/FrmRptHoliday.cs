﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyPayfriend;
using Microsoft.Reporting.WinForms;

/*****************************************************
   * Created By       : Sruthy K
   * Creation Date    : 11 Sep 2013
   * Description      : Company Holiday Report
   * FormID           : 191
   * Message Code     : 20011
   * ******************************************************/

namespace MyPayfriend
{
    public partial class FrmRptHoliday : Report
    {
        #region Properties
        private clsMessage ObjUserMessage = null;
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.HolidayReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor
        public FrmRptHoliday()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }
        #endregion

        #region FormLoad
        private void FrmRptHoliday_Load(object sender, EventArgs e)
        {
            LoadCompany();
            
           // cboCompany_SelectedIndexChanged(null, null);

        }
        #endregion

        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.VacationExtension, this);
        }
        #endregion SetArabicControls



        #region Functions
        private void LoadCompany()
        {
            cboCompany.DisplayMember = "CompanyName";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetCompaniesandBranches();
        }

        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(9100);
                return false;
            }
            if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
            {
                UserMessage.ShowMessage(1761);
                return false;
            }
            return true;
        }

        private void ShowReport()
        {
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), -1, true);

            DataTable dtReport = clsBLLRptHoliday.GetReport(cboCompany.SelectedValue.ToInt32(), dtpFromDate.Value.Date, dtpToDate.Value.Date);

            //Set Report Path

            if (ClsCommonSettings.IsArabicView)
            {
                rvHoliday.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptHolidayArb.rdlc";

            }
            else
            {
                rvHoliday.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptHoliday.rdlc";

            }

            //Set Report Parameters
            rvHoliday.LocalReport.SetParameters(new ReportParameter[] 
            {
                new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter),
                new ReportParameter("Company",cboCompany.Text.Trim()),
                new ReportParameter("Period",dtpFromDate.Value.ToString("dd MMM yyyy")+" - "+dtpToDate.Value.ToString("dd MMM yyyy"))
            });

            //Clear Report DataSources
            rvHoliday.LocalReport.DataSources.Clear();

            //Set Report DataSource if Record Exists
            if (dtReport.Rows.Count > 0)
            {
                rvHoliday.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                rvHoliday.LocalReport.DataSources.Add(new ReportDataSource("dtSetHoliday_dtHoliday", dtReport));
            }
            else
            {
                UserMessage.ShowMessage(20011); // Show 'No Records Found' Message to User
                return;
            }
            SetReportDisplay(rvHoliday);
        }

        #endregion

        #region ControlEvents
        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                rvHoliday.Reset();
                if (FormValidations())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region Control KeyDown Event
        private void cboCompany_KeyDown(object sender, KeyEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rvHoliday_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvHoliday_RenderingComplete(object sender, Microsoft.Reporting.WinForms.RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion

   }
}
