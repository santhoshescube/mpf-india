﻿namespace MyPayfriend
{
    partial class FrmRptCTC:Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptCTC));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.cboWorkStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.cboFinYear = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboEmployee = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.lblEmployee = new DevComponents.DotNetBar.LabelX();
            this.cboDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDepartment = new DevComponents.DotNetBar.LabelX();
            this.chkIncludeCompany = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cboBranch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblBranch = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.rvEmployeeProfile = new Microsoft.Reporting.WinForms.ReportViewer();
            this.expandablePanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.dtpMonth);
            this.expandablePanel1.Controls.Add(this.btnShow);
            this.expandablePanel1.Controls.Add(this.cboWorkStatus);
            this.expandablePanel1.Controls.Add(this.labelX1);
            this.expandablePanel1.Controls.Add(this.cboFinYear);
            this.expandablePanel1.Controls.Add(this.cboType);
            this.expandablePanel1.Controls.Add(this.cboEmployee);
            this.expandablePanel1.Controls.Add(this.labelX3);
            this.expandablePanel1.Controls.Add(this.labelX4);
            this.expandablePanel1.Controls.Add(this.labelX2);
            this.expandablePanel1.Controls.Add(this.lblEmployee);
            this.expandablePanel1.Controls.Add(this.cboDepartment);
            this.expandablePanel1.Controls.Add(this.lblDepartment);
            this.expandablePanel1.Controls.Add(this.chkIncludeCompany);
            this.expandablePanel1.Controls.Add(this.cboBranch);
            this.expandablePanel1.Controls.Add(this.lblBranch);
            this.expandablePanel1.Controls.Add(this.cboCompany);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(1252, 114);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 2;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By  ";
            // 
            // dtpMonth
            // 
            this.dtpMonth.CustomFormat = "MMM-yyyy";
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Location = new System.Drawing.Point(924, 38);
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ShowUpDown = true;
            this.dtpMonth.Size = new System.Drawing.Size(184, 20);
            this.dtpMonth.TabIndex = 143;
            this.dtpMonth.ValueChanged += new System.EventHandler(this.dtpMonth_ValueChanged);
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(1163, 36);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(77, 49);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 142;
            this.btnShow.Text = "Show";
            this.btnShow.Tooltip = "Show Report";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cboWorkStatus
            // 
            this.cboWorkStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWorkStatus.DropDownHeight = 134;
            this.cboWorkStatus.FormattingEnabled = true;
            this.cboWorkStatus.IntegralHeight = false;
            this.cboWorkStatus.ItemHeight = 14;
            this.cboWorkStatus.Location = new System.Drawing.Point(356, 65);
            this.cboWorkStatus.Name = "cboWorkStatus";
            this.cboWorkStatus.Size = new System.Drawing.Size(155, 20);
            this.cboWorkStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboWorkStatus.TabIndex = 35;
            this.cboWorkStatus.SelectedIndexChanged += new System.EventHandler(this.LoadEmployee);
            this.cboWorkStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(269, 65);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(62, 15);
            this.labelX1.TabIndex = 34;
            this.labelX1.Text = "Work Status";
            // 
            // cboFinYear
            // 
            this.cboFinYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFinYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFinYear.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboFinYear.DropDownHeight = 134;
            this.cboFinYear.FormattingEnabled = true;
            this.cboFinYear.IntegralHeight = false;
            this.cboFinYear.ItemHeight = 14;
            this.cboFinYear.Location = new System.Drawing.Point(924, 62);
            this.cboFinYear.Name = "cboFinYear";
            this.cboFinYear.Size = new System.Drawing.Size(184, 20);
            this.cboFinYear.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboFinYear.TabIndex = 31;
            this.cboFinYear.SelectedIndexChanged += new System.EventHandler(this.cboFinYear_SelectedIndexChanged);
            this.cboFinYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // cboType
            // 
            this.cboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboType.DropDownHeight = 134;
            this.cboType.FormattingEnabled = true;
            this.cboType.IntegralHeight = false;
            this.cboType.ItemHeight = 14;
            this.cboType.Location = new System.Drawing.Point(611, 39);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(208, 20);
            this.cboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboType.TabIndex = 31;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            this.cboType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.ItemHeight = 14;
            this.cboEmployee.Location = new System.Drawing.Point(611, 65);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(208, 20);
            this.cboEmployee.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboEmployee.TabIndex = 31;
            this.cboEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // labelX3
            // 
            this.labelX3.AutoSize = true;
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.Class = "";
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(844, 39);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(33, 15);
            this.labelX3.TabIndex = 30;
            this.labelX3.Text = "Month";
            this.labelX3.Click += new System.EventHandler(this.labelX3_Click);
            // 
            // labelX4
            // 
            this.labelX4.AutoSize = true;
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.Class = "";
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(844, 67);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(44, 15);
            this.labelX4.TabIndex = 30;
            this.labelX4.Text = "Fin Year";
            this.labelX4.Click += new System.EventHandler(this.labelX4_Click);
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(535, 40);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(27, 15);
            this.labelX2.TabIndex = 30;
            this.labelX2.Text = "Type";
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployee.BackgroundStyle.Class = "";
            this.lblEmployee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployee.Location = new System.Drawing.Point(535, 65);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(51, 15);
            this.lblEmployee.TabIndex = 30;
            this.lblEmployee.Text = "Employee";
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.DisplayMember = "Text";
            this.cboDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDepartment.DropDownHeight = 134;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.IntegralHeight = false;
            this.cboDepartment.ItemHeight = 14;
            this.cboDepartment.Location = new System.Drawing.Point(356, 39);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(155, 20);
            this.cboDepartment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDepartment.TabIndex = 23;
            this.cboDepartment.SelectionChangeCommitted += new System.EventHandler(this.LoadEmployee);
            this.cboDepartment.SelectedIndexChanged += new System.EventHandler(this.LoadEmployee);
            this.cboDepartment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            // 
            // 
            // 
            this.lblDepartment.BackgroundStyle.Class = "";
            this.lblDepartment.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDepartment.Location = new System.Drawing.Point(269, 39);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(60, 15);
            this.lblDepartment.TabIndex = 22;
            this.lblDepartment.Text = "Department";
            // 
            // chkIncludeCompany
            // 
            this.chkIncludeCompany.AutoSize = true;
            // 
            // 
            // 
            this.chkIncludeCompany.BackgroundStyle.Class = "";
            this.chkIncludeCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkIncludeCompany.Location = new System.Drawing.Point(12, 88);
            this.chkIncludeCompany.Name = "chkIncludeCompany";
            this.chkIncludeCompany.Size = new System.Drawing.Size(108, 15);
            this.chkIncludeCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkIncludeCompany.TabIndex = 18;
            this.chkIncludeCompany.Text = "Include Company";
            this.chkIncludeCompany.CheckedChanged += new System.EventHandler(this.chkIncludeCompany_CheckedChanged);
            // 
            // cboBranch
            // 
            this.cboBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBranch.DisplayMember = "Text";
            this.cboBranch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboBranch.DropDownHeight = 134;
            this.cboBranch.FormattingEnabled = true;
            this.cboBranch.IntegralHeight = false;
            this.cboBranch.ItemHeight = 14;
            this.cboBranch.Location = new System.Drawing.Point(63, 62);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(188, 20);
            this.cboBranch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboBranch.TabIndex = 7;
            this.cboBranch.SelectedIndexChanged += new System.EventHandler(this.cboBranch_SelectedIndexChanged);
            this.cboBranch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            // 
            // 
            // 
            this.lblBranch.BackgroundStyle.Class = "";
            this.lblBranch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBranch.Location = new System.Drawing.Point(12, 65);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(37, 15);
            this.lblBranch.TabIndex = 5;
            this.lblBranch.Text = "Branch";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(63, 36);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(188, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 4;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboKeyPress);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(12, 39);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 2;
            this.lblCompany.Text = "Company";
            // 
            // rvEmployeeProfile
            // 
            this.rvEmployeeProfile.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rvEmployeeProfile.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetCompanyFormReport_CompanyHeader";
            reportDataSource1.Value = null;
            this.rvEmployeeProfile.LocalReport.DataSources.Add(reportDataSource1);
            this.rvEmployeeProfile.LocalReport.ReportEmbeddedResource = "MindSoftERP.bin.Debug.MainReports.RptCompanyHeader.rdlc";
            this.rvEmployeeProfile.Location = new System.Drawing.Point(0, 114);
            this.rvEmployeeProfile.Name = "rvEmployeeProfile";
            this.rvEmployeeProfile.Size = new System.Drawing.Size(1252, 259);
            this.rvEmployeeProfile.TabIndex = 5;
            this.rvEmployeeProfile.RenderingComplete += new Microsoft.Reporting.WinForms.RenderingCompleteEventHandler(this.rvEmployeeProfile_RenderingComplete);
            this.rvEmployeeProfile.RenderingBegin += new System.ComponentModel.CancelEventHandler(this.rvEmployeeProfile_RenderingBegin);
            // 
            // FrmRptCTC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1252, 373);
            this.Controls.Add(this.rvEmployeeProfile);
            this.Controls.Add(this.expandablePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptCTC";
            this.Text = "CTC Report";
            this.Load += new System.EventHandler(this.FrmRptEmployeeProfile_Load);
            this.expandablePanel1.ResumeLayout(false);
            this.expandablePanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDepartment;
        private DevComponents.DotNetBar.LabelX lblDepartment;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkIncludeCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboBranch;
        private DevComponents.DotNetBar.LabelX lblBranch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private Microsoft.Reporting.WinForms.ReportViewer rvEmployeeProfile;
        private DevComponents.DotNetBar.LabelX lblEmployee;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboEmployee;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboWorkStatus;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btnShow;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboType;
        private DevComponents.DotNetBar.LabelX labelX2;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboFinYear;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        internal System.Windows.Forms.DateTimePicker dtpMonth;
    }
}