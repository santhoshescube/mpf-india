﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

/*****************************************************
   * Created By       : Sruthy K
   * Creation Date    : 27 Aug 2013
   * Description      : Labour Cost Report
   * FormID           : 185
   * Message Code     : 17624 - 17625
   * ******************************************************/

namespace MyPayfriend
{
    public partial class FrmRptLabourCost : Report
    {
        #region Properties

        private clsMessage ObjUserMessage = null;
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.LabourCostReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor
        public FrmRptLabourCost()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }
        #endregion

        #region FormLoad
        private void FrmRptLabourCost_Load(object sender, EventArgs e)
        {
            LoadCombos();
            cboType.SelectedIndex = 0;
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
            dtpMonth.Value = ("01/" + GetCurrentMonth(dtpMonth.Value.Month) + "/" + (dtpMonth.Value.Year) + "").ToDateTime();
            cboCompany_SelectedIndexChanged(null, null);
        }
        #endregion



        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.LabourCostReport, this);
        }
        #endregion SetArabicControls

        #region Functions
        /// <summary>
        /// Load All Combos
        /// </summary>
        private void LoadCombos()
        {
            FillCompany();
            FillAllDepartments();
            FillAllWorkStatus();
        }

        /// <summary>
        /// Load Company
        /// </summary>
        private void FillCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Load Branches by Company
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());
        }

        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }

        /// <summary>
        /// Load Employees by Company,Department,Designation
        /// </summary>
        private void FillAllEmployees()
        {
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(),-1,-1,cboWorkStatus.SelectedValue.ToInt32(),-1);
        }

        /// <summary>
        /// Load Financial Year by Company,Employee
        /// </summary>
        private void FillFinancialYears()
        {
            cboFinYear.DisplayMember = "FinYearPeriod";
            cboFinYear.ValueMember = "FinYearStartDate";
            cboFinYear.DataSource = clsBLLReportCommon.GetAllFinancialYears(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked);
        }

        /// <summary>
        /// Load WorkStatus 
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }

        /// <summary>
        /// Enable or Disable Include Company checkbox
        /// </summary>
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }

        /// <summary>
        /// Validate User Inputs
        /// </summary>
        /// <returns></returns>
        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0) 
            {
                UserMessage.ShowMessage(9100);
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9109);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                return false;
            }
            if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9102);
                return false;
            }
            if (cboType.SelectedIndex == 1 && cboFinYear.SelectedIndex == -1) // Type is Financial Year and Month Not Selected
            {
                UserMessage.ShowMessage(17625);
                return false;
            }
            return true;
        }


        private void ShowReport()
        {
            DateTime dtFinYearStartDate = DateTime.Today;
            DateTime dtFinYearEndDate = DateTime.Today;
            bool blnIsMonth = cboType.SelectedIndex == 0 ? true : false;
            int intMonth = 0;
            int intYear = 0;
            DateTime dtMonthEndDate = DateTime.Today;

            // Check whether Month is Selected
            if (blnIsMonth)
            {
                intMonth = dtpMonth.Value.Date.Month;
                intYear = dtpMonth.Value.Date.Year;
                dtMonthEndDate = new DateTime(intYear, intMonth, DateTime.DaysInMonth(intYear, intMonth)).Date;
            }
            else if (cboFinYear.SelectedValue != null && cboFinYear.Text != string.Empty)
            {
                dtFinYearStartDate = cboFinYear.Text.Split('-')[0].ToDateTime();
                dtFinYearEndDate = cboFinYear.Text.Split('-')[1].ToDateTime();
            }

            //Set Company Header For Report
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            //Get Report Data
            DataSet dsReport = clsBLLRptLabourCost.GetReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(),cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), blnIsMonth, intMonth, intYear, dtFinYearStartDate, dtFinYearEndDate,dtMonthEndDate);

            //Set Report Path

            if (ClsCommonSettings.IsArabicView)
            {

                rvLabourCost.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptLabourCostArb.rdlc";

            }
            else
            {
                rvLabourCost.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptLabourCost.rdlc";

            }

            //Set Report Parameters
            rvLabourCost.LocalReport.SetParameters(new ReportParameter[] 
            {
                new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter),
                new ReportParameter("Company",cboCompany.Text.Trim()),
                new ReportParameter("Branch",cboBranch.Text.Trim()),
                new ReportParameter("Department",cboDepartment.Text.Trim()),
                new ReportParameter("WorkStatus",cboWorkStatus.Text.Trim()),
                new ReportParameter("Employee",cboEmployee.Text.Trim()),
                new ReportParameter("IsMonth",blnIsMonth ? "1" : "0"),
                new ReportParameter("Period",blnIsMonth ? dtpMonth.Value.ToString("MMM yyyy") : cboFinYear.Text.Trim())
            });

            //Clear Report DataSources
            rvLabourCost.LocalReport.DataSources.Clear();

            //Set Report DataSource if Record Exists
            if (dsReport.Tables[0].Rows.Count > 0 || dsReport.Tables[1].Rows.Count > 0)
            {
                rvLabourCost.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                rvLabourCost.LocalReport.DataSources.Add(new ReportDataSource("dtSetLabourCost_dtLabourCost", dsReport.Tables[0]));
                rvLabourCost.LocalReport.DataSources.Add(new ReportDataSource("dtSetLabourCost_dtSettlement", dsReport.Tables[1]));
            }
            else
            {
                UserMessage.ShowMessage(17624); // Show 'No Records Found' Message to User
                return;
            }
            SetReportDisplay(rvLabourCost);
        }
        #endregion

        #region ControlEvents
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Enable or Disable Include Company CheckBox Based On Branch Value
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboType.SelectedIndex == 0)
            {
                cboFinYear.Enabled = false;
                dtpMonth.Enabled = true;
            }
            else
            {
                cboFinYear.Enabled = true;
                dtpMonth.Enabled = false;
                FillFinancialYears();
            }
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            dtpMonth.Value = Convert.ToDateTime("01-" + dtpMonth.Value.ToString("MMMM") + "-" + dtpMonth.Value.Year.ToString());
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                // Clear Report
                rvLabourCost.Reset();
                if (FormValidations())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region Control KeyDown Event
        private void cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cboSender = (ComboBox)sender;
            if (cboSender != null)
                cboSender.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rvLabourCost_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvLabourCost_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion
      
    }
}
