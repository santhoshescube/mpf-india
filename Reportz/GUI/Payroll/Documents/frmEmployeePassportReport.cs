﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;



namespace MyPayfriend
{
    public partial class frmEmployeePassportReport : DevComponents.DotNetBar.Office2007Form
    {

        public int PassportID { get; set; }
        public int CompanyID { get; set; }
        public frmEmployeePassportReport()
        {
            InitializeComponent();
        }

        private void frmEmployeePassportReport_Load(object sender, EventArgs e)
        {
            ShowReport();
        }

        private void ShowReport()
        {
           


            DataTable dtPassportDetails = clsReportBLL.GetEmployeePassportReport(PassportID);       
           // DataTable dtCompany = new clsBLLReportViewer().DisplayCompanyDefaultHeader();
            DataTable dtCompany = new DataTable();
            if (CompanyID != 0)
                dtCompany = new clsBLLReportViewer().DisplayCompanyReportHeader(CompanyID).Tables[0];
            else
            {
                dtCompany = new clsBLLReportViewer().DisplayCompanyDefaultHeader();
            }
            this.reportViewer1.ProcessingMode = ProcessingMode.Local;

            if (ClsCommonSettings.IsArabicView)
            {
                this.reportViewer1.LocalReport.ReportPath = Application.StartupPath + @"/MainReports/RptEmployeePassportArb.rdlc";

            }
            else
            {
                this.reportViewer1.LocalReport.ReportPath = Application.StartupPath + @"/MainReports/RptEmployeePassport.rdlc";

            }

            // Report parameters
            ReportParameter[] RepParam = new ReportParameter[1];
            RepParam[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);

            reportViewer1.LocalReport.SetParameters(RepParam);
            //----------------

            this.reportViewer1.LocalReport.DataSources.Clear();

            ReportDataSource rdsReportData = new ReportDataSource("dtDocuments_dtEmployeePassport", dtPassportDetails);

            ReportDataSource rdsReportHeader = new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany);

            this.reportViewer1.LocalReport.DataSources.Add(rdsReportData);

            this.reportViewer1.LocalReport.DataSources.Add(rdsReportHeader);

            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
            this.reportViewer1.ZoomMode = ZoomMode.Percent;

            dtPassportDetails.Dispose();

            dtCompany.Dispose();


        }
    }

}