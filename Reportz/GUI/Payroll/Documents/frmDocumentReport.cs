﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{
    public partial class frmDocumentReport : Report
    {
        #region Properties
        private clsMessage ObjUserMessage = null;
        ClsExportDatagridviewToExcel MobjExportToExcel;
        DataSet DsDocuments;
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.DocumentReport);

                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor
        public frmDocumentReport()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
            MobjExportToExcel = new ClsExportDatagridviewToExcel();

        }
        #endregion

        #region FormLoad
        private void frmDocumentReport_Load(object sender, EventArgs e)
        {
            Initialize();
            cboCompany_SelectedIndexChanged(null, null);
            FillReportFormat();
            cboType.SelectedIndex = 0;
        }
        #endregion

        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.DocumentReport, this);
        }
        #endregion SetArabicControls

        #region Functions

        /// <summary>
        /// Initialize the form
        /// </summary>
        private void Initialize()
        {
            chkIncludeCompany.Checked = true;
            LoadCombos();
            SetDefaultBranch(cboBranch);
            chkExpiryDate.Checked = false;
            chkExpiryDate_CheckedChanged(new object(), new EventArgs());
        }
        private void FillReportFormat()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("ReportFormatID", typeof(int));
            datTemp.Columns.Add("ReportFormat", typeof(string));

            datTemp.Rows.Add(1, "Graphics Format");
            datTemp.Rows.Add(1, "Grid Format");

            cboReportFormat.DataSource = datTemp;
            cboReportFormat.ValueMember = "ReportFormatID";
            cboReportFormat.DisplayMember = "ReportFormat";

            cboReportFormat.SelectedIndex = 0;
        }

        /// <summary>
        /// Fill all the combos
        /// </summary>
        private void LoadCombos()
        {
            FillAllDocumentTypes();
            FillAllCompany();
            FillDocumentStatus();
        }

        /// <summary>
        /// Load Employee based on the filteration
        /// </summary>
        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        /// <summary>
        /// Load all the companies
        /// </summary>
        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Load all the branches based on the company
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

        }

        /// <summary>
        /// Load all document types
        /// </summary>
        private void FillAllDocumentTypes()
        {
            CboDocumentTypeID.DisplayMember = "DocumentType";
            CboDocumentTypeID.ValueMember = "DocumentTypeID";
            CboDocumentTypeID.DataSource = clsBLLReportCommon.GetAllDocumentTypes();

        }

        /// <summary>
        /// Load all document status
        /// </summary>
        private void FillDocumentStatus()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DocumentStatusID");
            dt.Columns.Add("DocumentStatus");

            DataRow dr = null;
            dr = dt.NewRow();
            dr["DocumentStatusID"] = -1;
            dr["DocumentStatus"] = "Any";

            dt.Rows.Add(dr);


            dr = dt.NewRow();
            dr["DocumentStatusID"] = 2;
            dr["DocumentStatus"] = "New";

            dt.Rows.Add(dr);



            dr = dt.NewRow();
            dr["DocumentStatusID"] = 1;
            dr["DocumentStatus"] = "Received";

            dt.Rows.Add(dr);


            dr = dt.NewRow();
            dr["DocumentStatusID"] = 0;
            dr["DocumentStatus"] = "Issued";

            dt.Rows.Add(dr);


            CboDocumentStatus.DataSource = dt;
            CboDocumentStatus.DisplayMember = "DocumentStatus";
            CboDocumentStatus.ValueMember = "DocumentStatusID";
        }

        /// <summary>
        /// Load all document numbers based on the document typeid
        /// </summary>
        private void FillAllDocumentNumbers()
        {

            DataTable dtDistinctDocumentNumber = clsBLLReportCommon.GetAllDocumentNumbers(cboCompany.SelectedValue.ToInt32(),cboBranch.SelectedValue.ToInt32(),chkIncludeCompany.Checked, CboDocumentTypeID.SelectedValue.ToInt32()).DefaultView.ToTable(true, "DocumentNumber");
            DataRow dr = dtDistinctDocumentNumber.NewRow();
            //dr[0] = -1;
            dr[0] = "Any";
            //dr[2] = -1;
            //dr[3] = DateTime.Now;
            //dr[4] = 1;
            //dr[5] = 200;

            dtDistinctDocumentNumber.Rows.InsertAt(dr, 0);

            CboDocumentNumber.DisplayMember = "DocumentNumber";
           // CboDocumentNumber.ValueMember = "DocumentID";
            CboDocumentNumber.DataSource = dtDistinctDocumentNumber;

        }

        /// <summary>
        /// Enable or disable include company check box based on the company ,branch and chkinclude company values
        /// </summary>
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }
        /// <summary>
        /// LOad all employees
        /// </summary>
        private void FillAllEmployees()
        {
            CboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                chkIncludeCompany.Checked, CboDocumentTypeID.SelectedValue.ToInt32(), CboDocumentNumber.SelectedValue.ToInt32() > 0 ? CboDocumentNumber.Text : "-1");
            CboEmployee.DisplayMember = "EmployeeFullName";
            CboEmployee.ValueMember = "EmployeeID";



           
        }

        #region ControlEvents
        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllDocumentNumbers();
            FillAllEmployees();
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();

            FillAllDocumentNumbers();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void CboDocumentTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllDocumentNumbers();
        }

        private void CboDocumentNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Load all the employees based on the document types and document number
            FillAllEmployees();


            if (CboDocumentNumber.SelectedIndex > 0)
            {
                CboEmployee.SelectedIndex = 0;
                CboDocumentStatus.SelectedIndex = 0;

                CboEmployee.Enabled = CboDocumentStatus.Enabled = false;
                chkExpiryDate.Checked = false;
                chkExpiryDate.Enabled = false;
            }
            else
            {
                CboEmployee.Enabled = CboDocumentStatus.Enabled = true;
                chkExpiryDate.Enabled = true;
            }

           
        }

        private void chkExpiryDate_CheckedChanged(object sender, EventArgs e)
        {
            EnableDisableDate();
        }



        private void ShowGridReport()
        {
            try
            {

                DateTime? FromDate, ToDate;
                FromDate = ToDate = null;
                dgvReport.DataSource = null;
                if (chkExpiryDate.Checked)
                {
                    if (dtpFromDate.Text != "")
                        FromDate = dtpFromDate.Value;

                    if (dtpToDate.Text != "")
                        ToDate = dtpToDate.Value;
                }

                btnExcelExport.Visible = dgvReport.Visible = true;
                DataSet datTemp = new DataSet();

                datTemp = clsBLLdocumentReport.AllDocumentsGrid(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, CboDocumentStatus.SelectedValue.ToInt32(), CboDocumentTypeID.SelectedValue.ToInt32(), FromDate, ToDate,CboEmployee.SelectedValue.ToInt32(),CboDocumentNumber.Text.ToString(),cboType.SelectedIndex.ToInt32());

                dgvReport.DataSource = datTemp.Tables[0];
               
            }
            catch
            {
            }
        }
        public void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }

        #endregion

        /// <summary>
        /// Show report function
        /// </summary>
        public void ShowReport()
        {
            DateTime? FromDate, ToDate;
            string strMReportPath = string.Empty;

            //Setting the company header logo
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            RptView.LocalReport.DataSources.Clear();
            RptView.Reset();

            FromDate = ToDate = null;
            //setting the from date and to date based on the expiry check box
            if (chkExpiryDate.Checked)
            {
                if (dtpFromDate.Text != "")
                    FromDate = dtpFromDate.Value;

                if (dtpToDate.Text != "")
                    ToDate = dtpToDate.Value;
            }

            if (CboDocumentStatus.SelectedIndex > 0)
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    if (CboDocumentStatus.SelectedValue.ToInt32() == 1)
                        strMReportPath = Application.StartupPath + "\\MainReports\\RptDocumentsReceivedDetailsArb.rdlc";

                    else if (CboDocumentStatus.SelectedValue.ToInt32() == 0)
                        strMReportPath = Application.StartupPath + "\\MainReports\\RptDocumentsIssuedDetailsArb.rdlc";
                }
                else
                {
                    if (CboDocumentStatus.SelectedValue.ToInt32() == 1)
                        strMReportPath = Application.StartupPath + "\\MainReports\\RptDocumentsReceivedDetails.rdlc";

                    else if (CboDocumentStatus.SelectedValue.ToInt32() == 0)
                        strMReportPath = Application.StartupPath + "\\MainReports\\RptDocumentsIssuedDetails.rdlc";
                }




                DsDocuments = clsBLLdocumentReport.AllDocuments(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, CboDocumentStatus.SelectedValue.ToInt32(), CboDocumentTypeID.SelectedValue.ToInt32(),
                   FromDate, ToDate
                   );

                if (DsDocuments.Tables[0].Rows.Count > 0)
                {
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtAllDocuments", DsDocuments.Tables[0]));
                }
            }
            else
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptDocumentreportArb.rdlc";

                }
                else
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptDocumentreport.rdlc";

                }

                DsDocuments = clsBLLdocumentReport.GetAllDocumentsByEmployee(CboEmployee.SelectedValue.ToInt32(), CboDocumentTypeID.SelectedValue.ToInt32(), CboDocumentNumber.SelectedIndex == 0 ? "" : CboDocumentNumber.Text, FromDate, ToDate);
                RptView.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));


                if (DsDocuments.Tables[0].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeePassport", ""));//Passport
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeePassport", DsDocuments.Tables[0]));//Passport



                if (DsDocuments.Tables[1].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeVisaDetails", ""));//Visa
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeVisaDetails", DsDocuments.Tables[1]));//Visa




                if (DsDocuments.Tables[2].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtHealthCard", ""));//Health Card
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtHealthCard", DsDocuments.Tables[2]));//Health Card



                if (DsDocuments.Tables[3].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmiratesHealthCard", ""));//Emirated Card
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmiratesHealthCard", DsDocuments.Tables[3]));//Emirated Card



                if (DsDocuments.Tables[4].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtLabourCard", ""));//Labour Card
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtLabourCard", DsDocuments.Tables[4]));//Labour Card



                if (DsDocuments.Tables[5].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtInsuranceCard", ""));//Insurance Card
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtInsuranceCard", DsDocuments.Tables[5]));//Insurance Card



                if (DsDocuments.Tables[6].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeLicenceDetails", ""));//License Details

                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeLicenceDetails", DsDocuments.Tables[6]));//License Details


                if (DsDocuments.Tables[7].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_Qualification", ""));//Qualification Details
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_Qualification", DsDocuments.Tables[7]));//Qualification Details


                if (DsDocuments.Tables[8].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtOtherDocs", ""));//Other Documents
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtOtherDocs", DsDocuments.Tables[8]));//Other Documents



                if (DsDocuments.Tables[9].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeDet", ""));//Other Documents
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeDet", DsDocuments.Tables[9]));//Other Documents


                if (DsDocuments.Tables[10].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dataTabTradeLicense", ""));//Trade License
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dataTabTradeLicense", DsDocuments.Tables[10]));//Trade License


                if (DsDocuments.Tables[11].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dataTabCompanyLeaseAgreement", ""));//Lease Agreement
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dataTabCompanyLeaseAgreement", DsDocuments.Tables[11]));//Lease Agreement


                if (DsDocuments.Tables[12].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dataTabInsuranceDetails", ""));//Insurance Details
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dataTabInsuranceDetails", DsDocuments.Tables[12]));//Insurance Details


                if (DsDocuments.Tables[13].Columns.Count == 1)
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dataTabOtherDocs", ""));//Other Documents
                else
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dataTabOtherDocs", DsDocuments.Tables[13]));//Other Documents

            }


            if (CboDocumentNumber.SelectedIndex > 0)
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptSingleDocumentArb.rdlc";

                }
                else
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptSingleDocument.rdlc";

                }

                DsDocuments = clsBLLdocumentReport.GetSingleDocumentDetails(cboCompany.SelectedValue.ToInt16(),cboBranch.SelectedValue.ToInt32(),chkIncludeCompany.Checked, CboDocumentTypeID.SelectedValue.ToInt32(), CboDocumentNumber.SelectedIndex > 0 ? CboDocumentNumber.Text : "-1",FromDate,ToDate );

                if (DsDocuments.Tables[0].Rows.Count > 0)
                {
                    RptView.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                    RptView.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtSingleDocument", DsDocuments.Tables[0]));
                    RptView.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtAllReceiptIssues", DsDocuments.Tables[1]));

                }

            }

            //No record found message 
            if (DsDocuments.Tables.Count > 8)
            {
                if (DsDocuments.Tables[0].Rows.Count == 0 && DsDocuments.Tables[1].Rows.Count == 0 && DsDocuments.Tables[2].Rows.Count == 0 && DsDocuments.Tables[3].Rows.Count == 0 && DsDocuments.Tables[4].Rows.Count == 0 && DsDocuments.Tables[5].Rows.Count == 0 && DsDocuments.Tables[6].Rows.Count == 0 && DsDocuments.Tables[7].Rows.Count == 0 && DsDocuments.Tables[8].Rows.Count == 0)
                {
                    UserMessage.ShowMessage(17620);
                    return;
                }
            }
            else if (DsDocuments.Tables[0].Rows.Count == 0)
            {
                UserMessage.ShowMessage(17620);
                return;
            }


            //Setting the report parameter for all the reports since it s common for all reports
            RptView.LocalReport.ReportPath = strMReportPath;
            RptView.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false)
                });


            //Setting the report display
            SetReportDisplay(RptView);

        }

        /// <summary>
        /// Sub report event
        /// </summary>
        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("dtDocuments_dtAllReceiptIssues", DsDocuments.Tables[1]));
        }

        /// <summary>
        /// Validate the report
        /// </summary>
        private bool ValidateReport()
        {
            if (cboCompany.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(17612);
                return false;
            }
            else if (cboBranch.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(17613);
                return false;
            }
            else if (CboDocumentTypeID.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(10251);
                return false;
            }
            else if (CboDocumentNumber.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(17615);
                return false;
            }
            else if (CboEmployee.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(10250);
                return false;
            }
            else if (CboDocumentStatus.SelectedIndex < 0)
            {
                UserMessage.ShowMessage(17617);
                return false;
            }

            else if (chkExpiryDate.Checked)
            {
                if (dtpFromDate.Text == "")
                {
                    UserMessage.ShowMessage(17618);
                    return false;
                }
                else if (dtpFromDate.Text != "" && dtpToDate.Text != "" && dtpToDate.Value.Date < dtpFromDate.Value.Date)
                {
                    UserMessage.ShowMessage(1761);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Enabled or disable the date controls based on the checkbox expiry date value
        /// </summary>
        private void EnableDisableDate()
        {
            if (chkExpiryDate.Checked)
            {
                dtpFromDate.Enabled = dtpToDate.Enabled = true;

            }
            else
            {
                dtpFromDate.Enabled = dtpToDate.Enabled = false;
                dtpFromDate.Text = dtpToDate.Text = "";
            }
        }

        #endregion

        #region ReportViewer Events
        private void RptView_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;

        }

        private void RptView_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;

        }
        #endregion

        private void cboReportFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReportFormat.SelectedIndex == 0)
            {
                dgvReport.Visible =  btnExcelExport.Visible = false;
                RptView.Visible = true;
                cboType.Visible = false;
                 
            }
            else
            {
                dgvReport.Visible = btnExcelExport.Visible = true;
                RptView.Visible = false;
                cboType.Visible = true;
                cboType.SelectedIndex = 0;
            }
        }

        private void btnExcelExport_Click(object sender, EventArgs e)
        {
            string sFileName = "";

            if (dgvReport.RowCount <= 0)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";

                if (sfd.ShowDialog() == DialogResult.OK)
                    sFileName = sfd.FileName;
                else
                    return;
            }

            try
            {
                if (sFileName != "")
                {
                    if (dgvReport.RowCount > 0)
                        MobjExportToExcel.ExportToExcel(dgvReport, sFileName, 0, dgvReport.Columns.Count - 1, true);
                }
                else
                    MessageBox.Show("Please Enter Name");
            }
            catch { }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateReport())
                {
                    if (cboReportFormat.SelectedIndex == 0)
                    {
                        ShowReport();
                        dgvReport.DataSource = null;
                    }
                    else
                    {
                        ShowGridReport();
                    }
                }
                if (dgvReport.Columns.Contains("ReferenceName"))
                    dgvReport.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                else
                    dgvReport.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dgvReport.Refresh();
            }
            catch
            {
            }
        }
    }
}