﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using Microsoft.VisualBasic;
using System.IO;

/*****************************************************
   * Created By       : Sruthy K
   * Creation Date    : 06 Aug 2013
   * Description      : Attendance Report
   * FormID           : 133
   * Message Code     : 3002 - 3003
   * ******************************************************/

namespace MyPayfriend
{
    public partial class FrmRptAttendanceManual : Report
    {

        #region Properties
        ClsExportDatagridviewToExcel MobjExportToExcel;
        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AttendanceReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor

        public FrmRptAttendanceManual()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
            MobjExportToExcel = new ClsExportDatagridviewToExcel();
        }
        #endregion
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.AttendanceReport, this);
        }
        #endregion SetArabicControls

        #region FormLoad

        private void FrmRptAttendanceManual_Load(object sender, EventArgs e)
        {
            try
            {
                dtpAttDateFrom.Value = ClsCommonSettings.GetServerDate();
                FillReportFormat();
                LoadCombos();
                SetDefaultBranch(cboBranch);
                SetDefaultWorkStatus(cboWorkStatus);
                chkMonth.Checked = true;
                chkMonth.Checked = false;
            }
            catch
            {
            }


        }


        #endregion

        #region Functions

        private void FillReportFormat()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("ReportFormatID", typeof(int));
            datTemp.Columns.Add("ReportFormat", typeof(string));

            datTemp.Rows.Add(1, "Graphics Format-1");
            datTemp.Rows.Add(1, "Graphics Format-2");
            datTemp.Rows.Add(1, "Graphics Format-3");
            datTemp.Rows.Add(1, "Grid Format");

            cboReportFormat.DataSource = datTemp;
            cboReportFormat.ValueMember = "ReportFormatID";
            cboReportFormat.DisplayMember = "ReportFormat";

            cboReportFormat.SelectedIndex = 0;
        }

        /// <summary>
        /// Load All Combos
        /// </summary>
        private void LoadCombos()
        {
            FillCompany();
            FillAllDepartments();
            FillAllDesignations();
            FillAllWorkStatus();
        }

        /// <summary>
        /// Load Company
        /// </summary>
        private void FillCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Load Branches by Company
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());
        }


        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }



        /// <summary>
        /// Load All Designations
        /// </summary>
        private void FillAllDesignations()
        {
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation();
        }

        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }


        /// <summary>
        /// Load Employees by Company,Department,Designation
        /// </summary>
        private void FillAllEmployees()
        {
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), -1, cboWorkStatus.SelectedValue.ToInt32(), -1
                                );
        }
        /// <summary>
        /// Load Month 
        /// </summary>
        private void FillAllMonths()
        {
            DataTable dT = new DataTable();
            
            dT = clsBLLReportCommon.GetMonthForAttendance(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32()
                                );
           
            if (dT.Rows.Count > 0)
            {
                cboMonth.DataSource = dT;
            }
            else
            {
                cboMonth.DataSource = dT;
                cboMonth.SelectedIndex = -1;
                cboMonth.Text = "";
            }

            cboMonth.DisplayMember = "Month";
            cboMonth.ValueMember = "MDate";
        }

        private void ShowAttendanceSummaryReport()
        {
            string strMReportPath = "";
            if (FormValidations() == false)
                return;


            this.rvAttendance.Reset();
            decimal decTotalAmt = 0;
            int sChecked = 1;
            string strDateRange = dtpAttDateFrom.Value.ToString("dd-MMM-yyyy") + "  To  " + dtpAttDateTo.Value.ToString("dd-MMM-yyyy"); 

            string strDate = "";
            string strPerid = "";
            string strEndDate = "";
            string[] MonthYear = new string[2];

            DataTable dtEmployeeAttendance = null; ;
            DataTable dtCompanyDetails = null;
            DataTable dtEntryExitDetails = null;

            int iMonth = 0;int OneCompany =0;
            int iYear = 0;
            int intIncludeComp = chkIncludeCompany.Checked ? 1 : 0;
            int intBranchID = 0;
            int intCompanyID = cboCompany.SelectedValue.ToInt32();
            int intEmployeeID = cboEmployee.SelectedValue.ToInt32();
            int DesignationID = cboDesignation.SelectedValue.ToInt32();
            int DepartmentID = cboDepartment.SelectedValue.ToInt32();   
            int intLocationID = 0;
            string strTotalWorkTime = "00:00:00";
            string strTotalBreakTime = "00:00:00";
            string strTotalOt = "00:00:00";
            string strTotalAbsentTime = "00:00:00";
            string strTotalFoodBreakTime = "00:00:00";
            int intDayID = 0;
            this.rvAttendance.Reset();
            if (cboBranch.SelectedValue.ToInt32() == -2)
                intBranchID = 0;
            else
                intBranchID = cboBranch.SelectedValue.ToInt32();


            if (chkMonth.Checked)
            {
                iMonth = dtpAttDateFrom.Value.Month;
                iYear = dtpAttDateFrom.Value.Year;
                strDate = dtpAttDateFrom.Value.ToString("dd-MMM-yyyy");
                strPerid = strDate;


                //blnIsMonth = true;
                dtpAttDateFrom.Value = ("01-" + iMonth.ToString() + "-" + iYear.ToString()).ToDateTime();
                dtpAttDateTo.Value = dtpAttDateFrom.Value.AddMonths(1).AddDays(-1);
            }
            else
            {
                strDate = dtpAttDateFrom.Value.ToString("dd-MMM-yyyy");  
            }

            intDayID = (int)Convert.ToDateTime(strDate).DayOfWeek + 1;

            if (cboEmployee.Text != "ALL")
            {
                if (chkMonth.Checked == false)
                    sChecked = 0;

            }


            this.rvAttendance.LocalReport.DataSources.Clear();

            int iCmpID = cboBranch.SelectedValue.ToInt32() > 0 ? cboBranch.SelectedValue.ToInt32() : cboCompany.SelectedValue.ToInt32();
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            dtCompanyDetails = dtCompany;

            if (chkSummary.Checked || cboReportFormat.SelectedIndex == 1 || cboReportFormat.SelectedIndex == 2)
            {


                ReportParameter[] RepParam;

                if (cboReportFormat.SelectedIndex == 1 || cboReportFormat.SelectedIndex == 2)
                {
                    if (cboReportFormat.SelectedIndex == 2)
                    {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceDraft2.rdlc";
                    }
                    else
                    {
                         strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceDraft.rdlc";
                    }


                    strDate = dtpAttDateFrom.Value.ToString("dd-MMM-yyyy"); 
                    strEndDate = dtpAttDateTo.Value.ToString("dd-MMM-yyyy");

                    if (cboReportFormat.SelectedIndex == 2)
                    {
                        dtEmployeeAttendance = new clsBLLAttendanceReport().GetAttendanceDraft2(intCompanyID, intEmployeeID, intBranchID, strDate, strEndDate, intIncludeComp, DesignationID, DepartmentID);
                    }
                    else
                    {
                        dtEmployeeAttendance = new clsBLLAttendanceReport().GetAttendanceDraft(intCompanyID, intEmployeeID, intBranchID, strDate, strEndDate, intIncludeComp, DesignationID, DepartmentID);
                    }



                    RepParam = new ReportParameter[9];
                    RepParam[0] = new ReportParameter("Company", cboCompany.Text.Trim(), false);
                    RepParam[1] = new ReportParameter("Branch", cboBranch.Text.Trim(), false);
                    RepParam[2] = new ReportParameter("Department", cboDepartment.Text.Trim(), false);
                    RepParam[3] = new ReportParameter("Designation", cboDesignation.Text.Trim(), false);
                    RepParam[4] = new ReportParameter("WorkStatus", cboWorkStatus.Text.Trim(), false);
                    RepParam[5] = new ReportParameter("Employee", cboEmployee.Text, false);
                    RepParam[6] = new ReportParameter("Period", strDateRange, false);
                    RepParam[7] = new ReportParameter("IsMonth", "", true);
                    RepParam[8] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);

                    this.rvAttendance.ProcessingMode = ProcessingMode.Local;
                    this.rvAttendance.LocalReport.ReportPath = strMReportPath;
                    this.rvAttendance.LocalReport.SetParameters(RepParam);
                    if (cboReportFormat.SelectedIndex == 2)
                    {
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_RptAttedanceDraft2", dtEmployeeAttendance));
                    }
                    else
                    {
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_RptAttendanceDraft", dtEmployeeAttendance));
                    }
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));

                }
                else
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendancesummaryForaMonth.rdlc";

                    if (chkMonth.Checked)
                        strEndDate = strDate;
                    else
                        strEndDate = Convert.ToDateTime(strDate).AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");


                    dtEmployeeAttendance = new clsBLLAttendanceReport().GetEmployeeSummary(intCompanyID, intEmployeeID, intBranchID, strDate, strEndDate, intIncludeComp, 0);

                    RepParam = new ReportParameter[2];
                    RepParam[0] = new ReportParameter("Reporton", strPerid, false);
                    RepParam[1] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);


                    this.rvAttendance.ProcessingMode = ProcessingMode.Local;
                    this.rvAttendance.LocalReport.ReportPath = strMReportPath;
                    this.rvAttendance.LocalReport.SetParameters(RepParam);
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_AttendanceForaMonth", dtEmployeeAttendance));

                }

            }

            {

                if (cboEmployee.SelectedValue.ToInt32() > 0 && ChkDaily.Checked && cboReportFormat.SelectedIndex   ==0)
                {
                     
                    DataSet dsReport = clsBLLAttendanceReport.GetPunchingReport(cboEmployee.SelectedValue.ToInt32(), dtpAttDateFrom.Value.ToString("dd-MMM-yyyy"), dtpAttDateTo.Value.ToString("dd-MMM-yyyy"));
                    dtEmployeeAttendance = dsReport.Tables[1];
                    OneCompany = 1;
                    if (ClsCommonSettings.IsArabicView)
                    {
                        strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceDailyPunchingArb.rdlc";
                    }
                    else
                    {
                        strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceDailyPunching.rdlc";

                    }
                    this.rvAttendance.LocalReport.ReportPath = strMReportPath;
                    rvAttendance.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter)
                    });
                    if (dsReport.Tables[1].Rows.Count > 0)
                    {
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingGeneral", dsReport.Tables[0]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunching", dsReport.Tables[1]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingConsequences", dsReport.Tables[2]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    }
                    else
                    {
                        UserMessage.ShowMessage(3003);
                        return;
                    }

                }
                else if (cboReportFormat.SelectedIndex    == 0)
                {

                    int iMode = chkMonth.Checked ? 9 : 5;
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummary.rdlc";

                    if (iMode == 9)
                    {
                        dtpAttDateTo.Value = dtpAttDateFrom.Value;
                    }



                    dtEmployeeAttendance = new clsBLLAttendanceReport().GetEmployeeAttendnaceAllEmployee(iMode, intCompanyID, intBranchID, intEmployeeID, intIncludeComp, intLocationID, dtpAttDateFrom.Value.ToString("dd-MMM-yyyy"), dtpAttDateTo.Value.ToString("dd-MMM-yyyy"));

                    string strIsOTShown = "1";

                    if (cboEmployee.Text == "ALL" && chkMonth.Checked && !chkSummary.Checked)
                        strIsOTShown = "0";

                    ReportParameter[] RepParam = new ReportParameter[18];
                    RepParam[0] = new ReportParameter("CompanyID", Convert.ToString(intCompanyID), false);
                    RepParam[1] = new ReportParameter("Month", Convert.ToString(iMonth), false);
                    RepParam[2] = new ReportParameter("Year", Convert.ToString(iYear), false);
                    RepParam[3] = new ReportParameter("Companyname", cboCompany.Text.Trim(), false);
                    RepParam[4] = new ReportParameter("EmployeeID", intEmployeeID.ToString(), false);
                    RepParam[5] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
                    RepParam[6] = new ReportParameter("Totalworktime", strTotalWorkTime, false);
                    RepParam[7] = new ReportParameter("Totalbreaktime", strTotalBreakTime, false);
                    RepParam[8] = new ReportParameter("Date", strDate, false);
                    RepParam[9] = new ReportParameter("totalfbreaktime", strTotalFoodBreakTime, false);
                    RepParam[10] = new ReportParameter("totalot", strTotalOt, false);
                    RepParam[11] = new ReportParameter("checked", Convert.ToString(sChecked), false);
                    RepParam[12] = new ReportParameter("sMonth", Convert.ToString(decTotalAmt), false);
                    RepParam[13] = new ReportParameter("Employee", cboEmployee.Text, false);
                    RepParam[14] = new ReportParameter("Reporton", strDateRange, false);
                    RepParam[15] = new ReportParameter("TotalShortage", strTotalAbsentTime, false);
                    RepParam[16] = new ReportParameter("IsOTShown", strIsOTShown, false);
                    RepParam[17] = new ReportParameter("Company", cboCompany.Text.Trim(), false);
                    this.rvAttendance.ProcessingMode = ProcessingMode.Local;
                    this.rvAttendance.LocalReport.ReportPath = strMReportPath;
                    this.rvAttendance.LocalReport.SetParameters(RepParam);
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_EmployeeAttendance", dtEmployeeAttendance));

                }
            }
            if (cboReportFormat.SelectedIndex == 0 && OneCompany==0)
            {
                 this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_CompanyDetails", dtCompanyDetails));//"DtSetCompanyFormReport_CompanyHeader", dtCompany)
            }
            if (dtEmployeeAttendance.Rows.Count <= 0)
            {

                UserMessage.ShowMessage(9015, null, 2);
                return;
            }

            if (!File.Exists(strMReportPath))
            {
                UserMessage.ShowMessage(9127, null, 2);
                return;
            }


            this.rvAttendance.SetDisplayMode(DisplayMode.PrintLayout);
            this.rvAttendance.ZoomMode = ZoomMode.Percent;
            this.rvAttendance.ZoomPercent = 100;



        }
       
        /// <summary>
        /// Show Report
        /// </summary>
        private void ShowReport()
        {
            bool blnIsMonth = false;
            if (chkMonth.Checked)
                blnIsMonth = true;
            int intMonth = 0;
            int intYear = 0;
            
            // Check whether Month is Selected
            if (cboMonth.SelectedIndex != -1)
            {
                if (chkMonth.Checked)
                {
                    intMonth = (cboMonth.SelectedValue.ToString().Split('@')[0]).ToInt32();
                    intYear = (cboMonth.SelectedValue.ToString().Split('@')[1]).ToInt32();
                    blnIsMonth = true;
                    string mn = new DateTime(intYear, intMonth, 1).ToString("MMM", System.Globalization.CultureInfo.InvariantCulture);
                    dtpAttDateFrom.Value = Convert.ToDateTime("01-" + mn + "-" + intYear.ToString());
                    dtpAttDateTo.Value = dtpAttDateFrom.Value.NextMonth();
                }
            }
            string strDateRange = dtpAttDateFrom.Value.ToString("dd-MMM-yyyy") + "  To  " + dtpAttDateTo.Value.ToString("dd-MMM-yyyy"); 

            //Set Company Header For Report
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            //Clear Report Datasources
            this.rvAttendance.Reset();
            this.rvAttendance.LocalReport.DataSources.Clear();
            this.rvAttendance.Refresh();

            if (chkSummary.Checked) // Attendance Summary Report
            {
                DataTable dtReport = clsBLLAttendanceReport.GetSummaryReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), dtpAttDateFrom.Value.ToString("dd-MMM-yyyy"), dtpAttDateTo.Value.ToString("dd-MMM-yyyy"));

                if (ClsCommonSettings.IsArabicView)
                {
                    this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummaryForaMonthArb.rdlc";

                }
                else
                {
                    this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummaryForaMonth.rdlc";

                }

                if (cboMonth.Text.Trim()=="")
                {
                    strDateRange=dtpAttDateFrom.Value.ToString("dd-MMM-yyyy")+"  To  " + dtpAttDateTo.Value.ToString("dd-MMM-yyyy");     
                }
                else
                {
                    strDateRange = cboMonth.Text.Trim();
                }
                

                rvAttendance.LocalReport.SetParameters(new ReportParameter[]
                {
                    new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter),
                    new ReportParameter("Company",cboCompany.Text.Trim()),
                    new ReportParameter("Branch",cboBranch.Text.Trim()),
                    new ReportParameter("Department",cboDepartment.Text.Trim()),
                    new ReportParameter("Designation",cboDesignation.Text.Trim()),
                    new ReportParameter("WorkStatus",cboWorkStatus.Text.Trim()),
                    new ReportParameter("Employee",cboEmployee.Text.Trim()),
                    new ReportParameter("Month",strDateRange)
                });
                if (dtReport.Rows.Count > 0)
                {
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtSummary", dtReport));
                    this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                }
                else
                {
                    UserMessage.ShowMessage(3003);
                    return;
                }
            }
            else
            {
                if (cboEmployee.SelectedValue.ToInt32() > 0 && ChkDaily.Checked && cboReportFormat.SelectedIndex == 0) // Punching Report
                {
                    DataSet dsReport = clsBLLAttendanceReport.GetPunchingReport(cboEmployee.SelectedValue.ToInt32(), dtpAttDateFrom.Value.ToString("dd-MMM-yyyy"), dtpAttDateTo.Value.ToString("dd-MMM-yyyy"));

                    if (ClsCommonSettings.IsArabicView)
                    {
                        this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceDailyPunchingArb.rdlc";

                    }
                    else
                    {
                        this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceDailyPunching.rdlc";

                    }
                    rvAttendance.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter)
                    });
                    if (dsReport.Tables[1].Rows.Count > 0)
                    {
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingGeneral", dsReport.Tables[0]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunching", dsReport.Tables[1]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingConsequences", dsReport.Tables[2]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    }
                    else
                    {
                        UserMessage.ShowMessage(3003);
                        return;
                    }
                }
                else // Attendance Month Wise Report
                {
                    DataTable dtReport = clsBLLAttendanceReport.GetReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), blnIsMonth, dtpAttDateFrom.Value.ToString("dd-MMM-yyyy").ToDateTime() , dtpAttDateFrom.Value.ToString("dd-MMM-yyyy"), dtpAttDateTo.Value.ToString("dd-MMM-yyyy"));

                    if (ClsCommonSettings.IsArabicView)
                    {
                        this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummaryArb.rdlc";
                    }
                    else
                    {
                        this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceSummary1.rdlc";
                    }
                    if (dtReport.Rows.Count > 0)
                    {
                        rvAttendance.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter),
                        new ReportParameter("Company",cboCompany.Text.Trim()),
                        new ReportParameter("Branch",cboBranch.Text.Trim()),
                        new ReportParameter("Department",cboDepartment.Text.Trim()),
                        new ReportParameter("Designation",cboDesignation.Text.Trim()),
                        new ReportParameter("WorkStatus",cboWorkStatus.Text.Trim()),
                        new ReportParameter("Employee",cboEmployee.Text.Trim()),
                        new ReportParameter("IsMonth",blnIsMonth ? "1" : "0"),
                        new ReportParameter("Period",strDateRange)
                        //new ReportParameter("Period",chkMonth.Checked ? dtpAttDateFrom.Value.ToString("dd MMM yyyy") : cboMonth.Text.Trim())
                    });


                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtAttendance", dtReport));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    }
                    else
                    {
                        UserMessage.ShowMessage(3003);
                        return;
                    }
                }
            }
            SetReportDisplay(rvAttendance);
        }

        /// <summary>
        /// Validate User Inputs
        /// </summary>
        /// <returns></returns>
        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(9100);
                cboCompany.Focus();
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                cboBranch.Focus();
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9109);
                return false;
            }
            if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9112);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                cboWorkStatus.Focus();
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                cboEmployee.Focus();
                return false;
            }
            if (chkMonth.Checked == true  && cboMonth.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9712);
                cboMonth.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Enable or Disable Include Company checkbox
        /// </summary>
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }

        #endregion

        #region ControlEvents

        private void btnShow_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (chkMonth.Checked)
                {
                    cboMonth.Enabled =dtpAttDateTo.Enabled=dtpAttDateFrom.Enabled = true;
                    if (cboMonth.SelectedIndex != -1)
                    {
                        int intMonth = (cboMonth.SelectedValue.ToString().Split('@')[0]).ToInt32();
                        int intYear = (cboMonth.SelectedValue.ToString().Split('@')[1]).ToInt32();
                        dtpAttDateFrom.Value = ("01-" + intMonth.ToString() + "-" + intYear.ToString()).ToDateTime();
                        dtpAttDateTo.Value = dtpAttDateFrom.Value.AddMonths(1).AddDays(-1);
                    }
                     dtpAttDateTo.Enabled=dtpAttDateFrom.Enabled = false;
                }
                else if (ChkDaily.Checked)
                {
                    dtpAttDateTo.Value = dtpAttDateFrom.Value;
                }
                else
                {
                    cboMonth.SelectedIndex = -1;
                    cboMonth.Enabled = false;
                    dtpAttDateFrom.Enabled = true;
                    dtpAttDateTo.Enabled = true;
                }

                if ((chkSummary.Checked == true || chkMonth.Checked == true) && cboReportFormat.SelectedIndex    == 0)
                {
                    if (FormValidations())
                    {
                        if (cboReportFormat.SelectedIndex == 0)
                            ShowReport();
                        else
                            ShowGridReport();

                    }
                }
                else
                {
                    if (FormValidations())
                    {
                        if (cboReportFormat.SelectedIndex != 3)
                            ShowAttendanceSummaryReport();
                        else
                            ShowGridReport();
                    }
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
            this.Cursor = Cursors.Default;
        }



        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
            //FillAllEmployees();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllMonths();
        }

        #endregion

        #region Control KeyDown Event

        private void cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cboSender = sender as ComboBox;
            if (cboSender != null)
                cboSender.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rvAttendance_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvAttendance_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion

        private void ShowGridReport()
        {
            try
            {
                bool blnIsMonth = true;
                if (chkMonth.Checked)
                    blnIsMonth = false;

                int intMonth = 0;
                int intYear = 0;
                string MonthYear="";
                if (cboMonth.SelectedIndex != -1)
                {
                    MonthYear = cboMonth.SelectedValue.ToString();
                }
                // Check whether Month is Selected
                if (cboMonth.SelectedIndex !=-1)
                {
                    intMonth = (cboMonth.SelectedValue.ToString().Split('@')[0]).ToInt32();
                    intYear = (cboMonth.SelectedValue.ToString().Split('@')[1]).ToInt32();

                    blnIsMonth = true;
                    dtpAttDateFrom.Value = ("01-" + intMonth.ToString() + "-" + intYear.ToString()).ToDateTime();
                    dtpAttDateTo.Value = dtpAttDateFrom.Value.AddMonths(1).AddDays(-1);
                }
                else
                {
                    blnIsMonth = false;
                }
                DataTable datTemp = new DataTable();
                datTemp = clsBLLAttendanceReport.GetReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), blnIsMonth, dtpAttDateFrom.Value, dtpAttDateFrom.Value.ToString("dd-MMM-yyyy"), dtpAttDateTo.Value.ToString("dd-MMM-yyyy"));
                if (datTemp.Rows.Count > 0)
                {
                    for (int i = 0; i < 7; i++)
                    {
                        datTemp.Columns.RemoveAt(12);
                        datTemp.AcceptChanges();
                    }
                    if (chkSummary.Checked)
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            datTemp.Columns.RemoveAt(2);
                            datTemp.AcceptChanges();
                        }
                        DataTable dtClone = datTemp.Clone();
                        DataRow dtNewRow;

                        double awt = 0, bt = 0, st = 0, ot = 0;
                        string empNo = "", empName = "";
                        var x = (from r in datTemp.AsEnumerable() select r["EmployeeNumber"]).Distinct().ToList();

                        foreach (var eno in x)
                        {
                            DataTable dttemp = new DataTable();
                            dttemp = datTemp.Clone();

                            DataRow[] rowsToCopy;
                            rowsToCopy = datTemp.Select("EmployeeNumber='" + eno + "'");

                            foreach (DataRow temp in rowsToCopy)
                            {
                                dttemp.ImportRow(temp);
                            }

                            for (int iCounter = 0; iCounter < dttemp.Rows.Count; iCounter++)
                            {
                                empNo = dttemp.Rows[iCounter]["EmployeeNumber"].ToString();
                                empName = dttemp.Rows[iCounter]["EmployeeName"].ToString();
                                awt = awt + (dttemp.Rows[iCounter]["ActualWorkTime"].ToString().Replace(":", ".")).ToDouble();
                                st = st + (dttemp.Rows[iCounter]["Shortage"].ToString().Replace(":", ".")).ToDouble();
                                bt = bt + (dttemp.Rows[iCounter]["BreakTime"].ToString().Substring(0, 5).Replace(":", ".")).ToDouble();
                                ot = ot + (dttemp.Rows[iCounter]["OT"].ToString().Substring(0, 5).Replace(":", ".")).ToDouble();
                            }

                            dtNewRow = dtClone.Rows.Add();
                            dtNewRow.SetField("EmployeeNumber", empNo);
                            dtNewRow.SetField("EmployeeName", empName);
                            dtNewRow.SetField("ActualWorkTime", awt);
                            dtNewRow.SetField("BreakTime", bt);
                            dtNewRow.SetField("Shortage", st);
                            dtNewRow.SetField("OT", ot);
                        }
                        datTemp = dtClone;
                    }

                    dgvReport.DataSource = datTemp;
                }
                else
                {
                    UserMessage.ShowMessage(3003);
                    return;
                }
            }
            catch { }
        }

        private void dgvReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void cboReportFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReportFormat.SelectedIndex != 3)
            {
                dgvReport.Visible = btnExcelExport.Visible = false;
                rvAttendance.Visible = true;
            }
            else
            {
                dgvReport.Visible = btnExcelExport.Visible = true;
                rvAttendance.Visible = false;
            }
        }

        private void btnExcelExport_Click(object sender, EventArgs e)
        {
            string sFileName = "";

            if (dgvReport.RowCount <= 0)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";

                if (sfd.ShowDialog() == DialogResult.OK)
                    sFileName = sfd.FileName;
                else
                    return;
            }

            try
            {
                if (sFileName != "")
                {
                    if (dgvReport.RowCount > 0)
                        MobjExportToExcel.ExportToExcel(dgvReport, sFileName, 0, dgvReport.Columns.Count - 1, true);
                }
                else
                    MessageBox.Show("Please Enter Name");
            }
            catch { }
        }

        private void chkDraft_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboReportFormat.SelectedIndex  ==1 )
                {
                    chkMonth.Checked = true;
                    dtpAttDateFrom.Enabled = false;
                    dtpAttDateTo.Enabled = false;
                    cboMonth.Enabled = true;
                    cboReportFormat.SelectedIndex = 0;
                }
                else
                {
                    dtpAttDateFrom.Enabled = true;
                    dtpAttDateTo.Enabled = true;
                    chkMonth.Checked = false;
                }
            }
            catch
            {
            }
        }

        private void chkMonth_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                if (chkMonth.Checked)
                {
                    cboMonth.Enabled = true;
                    dtpAttDateFrom.Enabled = false;
                    dtpAttDateTo.Enabled = false;
                    chkSummary.Checked = false;
                    chkSummary.Enabled = false;
                    ChkDaily.Checked = false; 
                }
                else
                {
                    cboMonth.Enabled = false;
                    dtpAttDateFrom.Enabled = true;
                    dtpAttDateTo.Enabled = true;
                    chkSummary.Checked = true;
                    chkSummary.Enabled = true;
                }



            }
            catch
            {
            }
        }

        private void ChkDaily_CheckedChanged(object sender, EventArgs e)
        {


            if (ChkDaily.Checked)
            {
                chkMonth.Checked = false;
                cboMonth.Enabled = false;
                dtpAttDateFrom.Enabled = true;
                dtpAttDateTo.Enabled = false;
                dtpAttDateTo.Value = dtpAttDateFrom.Value; 
                chkSummary.Checked = false;
                chkSummary.Enabled = true;
            }
            else
            {
                dtpAttDateFrom.Enabled = true;
                dtpAttDateTo.Enabled = true;
            }
        }

        private void dtpAttDateFrom_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (dtpAttDateFrom.Checked)
                {
                    dtpAttDateTo.Value = dtpAttDateFrom.Value;
                }
            }
            catch
            {
            }
        }

        private void dtpAttDateTo_ValueChanged(object sender, EventArgs e)
        {

        }


    }
}
