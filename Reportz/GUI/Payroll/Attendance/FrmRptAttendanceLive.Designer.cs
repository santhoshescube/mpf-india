﻿namespace MyPayfriend
{
    partial class FrmRptAttendanceLive
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptAttendanceLive));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.btnExcelExport = new DevComponents.DotNetBar.ButtonX();
            this.cboReportFormat = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.cboDesignation = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDesignation = new DevComponents.DotNetBar.LabelX();
            this.cboDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDepartment = new DevComponents.DotNetBar.LabelX();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.cboWorkStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWorkStatus = new DevComponents.DotNetBar.LabelX();
            this.chkIncludeCompany = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.dtpAttDate = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.cboEmployee = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboBranch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblEmployee = new DevComponents.DotNetBar.LabelX();
            this.lblBranch = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.rvAttendance = new Microsoft.Reporting.WinForms.ReportViewer();
            this.pbGridView = new System.Windows.Forms.ProgressBar();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.dgvReport = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.expandablePanel1.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.btnExcelExport);
            this.expandablePanel1.Controls.Add(this.cboReportFormat);
            this.expandablePanel1.Controls.Add(this.lblToDate);
            this.expandablePanel1.Controls.Add(this.dtpToDate);
            this.expandablePanel1.Controls.Add(this.cboDesignation);
            this.expandablePanel1.Controls.Add(this.lblDesignation);
            this.expandablePanel1.Controls.Add(this.cboDepartment);
            this.expandablePanel1.Controls.Add(this.lblDepartment);
            this.expandablePanel1.Controls.Add(this.btnShow);
            this.expandablePanel1.Controls.Add(this.cboWorkStatus);
            this.expandablePanel1.Controls.Add(this.lblWorkStatus);
            this.expandablePanel1.Controls.Add(this.chkIncludeCompany);
            this.expandablePanel1.Controls.Add(this.dtpAttDate);
            this.expandablePanel1.Controls.Add(this.lblFromDate);
            this.expandablePanel1.Controls.Add(this.cboEmployee);
            this.expandablePanel1.Controls.Add(this.cboBranch);
            this.expandablePanel1.Controls.Add(this.lblEmployee);
            this.expandablePanel1.Controls.Add(this.lblBranch);
            this.expandablePanel1.Controls.Add(this.cboCompany);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(1216, 114);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 1;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // btnExcelExport
            // 
            this.btnExcelExport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnExcelExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcelExport.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnExcelExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcelExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExcelExport.Image")));
            this.btnExcelExport.ImageTextSpacing = 5;
            this.btnExcelExport.Location = new System.Drawing.Point(1188, 91);
            this.btnExcelExport.Name = "btnExcelExport";
            this.btnExcelExport.Size = new System.Drawing.Size(16, 16);
            this.btnExcelExport.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnExcelExport.TabIndex = 161;
            this.btnExcelExport.Tooltip = "Export to Excel";
            this.btnExcelExport.Click += new System.EventHandler(this.btnExcelExport_Click);
            // 
            // cboReportFormat
            // 
            this.cboReportFormat.DisplayMember = "Text";
            this.cboReportFormat.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboReportFormat.FormattingEnabled = true;
            this.cboReportFormat.ItemHeight = 14;
            this.cboReportFormat.Location = new System.Drawing.Point(939, 36);
            this.cboReportFormat.Name = "cboReportFormat";
            this.cboReportFormat.Size = new System.Drawing.Size(121, 20);
            this.cboReportFormat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboReportFormat.TabIndex = 160;
            this.cboReportFormat.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            this.cboReportFormat.WatermarkFont = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic);
            this.cboReportFormat.WatermarkText = "Report Format";
            this.cboReportFormat.SelectedIndexChanged += new System.EventHandler(this.cboReportFormat_SelectedIndexChanged);
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(777, 70);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(15, 15);
            this.lblToDate.TabIndex = 159;
            this.lblToDate.Text = "To";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd MMM yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(820, 66);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(101, 20);
            this.dtpToDate.TabIndex = 158;
            // 
            // cboDesignation
            // 
            this.cboDesignation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDesignation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDesignation.DisplayMember = "Text";
            this.cboDesignation.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDesignation.DropDownHeight = 134;
            this.cboDesignation.FormattingEnabled = true;
            this.cboDesignation.IntegralHeight = false;
            this.cboDesignation.ItemHeight = 14;
            this.cboDesignation.Location = new System.Drawing.Point(325, 69);
            this.cboDesignation.Name = "cboDesignation";
            this.cboDesignation.Size = new System.Drawing.Size(175, 20);
            this.cboDesignation.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDesignation.TabIndex = 156;
            this.cboDesignation.SelectedIndexChanged += new System.EventHandler(this.cboDesignation_SelectedIndexChanged);
            this.cboDesignation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_KeyDown);
            // 
            // lblDesignation
            // 
            this.lblDesignation.AutoSize = true;
            // 
            // 
            // 
            this.lblDesignation.BackgroundStyle.Class = "";
            this.lblDesignation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDesignation.Location = new System.Drawing.Point(258, 72);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(61, 15);
            this.lblDesignation.TabIndex = 155;
            this.lblDesignation.Text = "Designation";
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.DisplayMember = "Text";
            this.cboDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDepartment.DropDownHeight = 134;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.IntegralHeight = false;
            this.cboDepartment.ItemHeight = 14;
            this.cboDepartment.Location = new System.Drawing.Point(325, 36);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(175, 20);
            this.cboDepartment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDepartment.TabIndex = 154;
            this.cboDepartment.SelectedIndexChanged += new System.EventHandler(this.cboDepartment_SelectedIndexChanged);
            this.cboDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_KeyDown);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            // 
            // 
            // 
            this.lblDepartment.BackgroundStyle.Class = "";
            this.lblDepartment.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDepartment.Location = new System.Drawing.Point(258, 39);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(60, 15);
            this.lblDepartment.TabIndex = 153;
            this.lblDepartment.Text = "Department";
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(939, 66);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(78, 30);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 152;
            this.btnShow.Text = "Show";
            this.btnShow.Tooltip = "Show Report";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cboWorkStatus
            // 
            this.cboWorkStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkStatus.DisplayMember = "Text";
            this.cboWorkStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWorkStatus.DropDownHeight = 134;
            this.cboWorkStatus.FormattingEnabled = true;
            this.cboWorkStatus.IntegralHeight = false;
            this.cboWorkStatus.ItemHeight = 14;
            this.cboWorkStatus.Location = new System.Drawing.Point(587, 36);
            this.cboWorkStatus.Name = "cboWorkStatus";
            this.cboWorkStatus.Size = new System.Drawing.Size(175, 20);
            this.cboWorkStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboWorkStatus.TabIndex = 23;
            this.cboWorkStatus.SelectedIndexChanged += new System.EventHandler(this.cboWorkStatus_SelectedIndexChanged);
            this.cboWorkStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_KeyDown);
            // 
            // lblWorkStatus
            // 
            this.lblWorkStatus.AutoSize = true;
            // 
            // 
            // 
            this.lblWorkStatus.BackgroundStyle.Class = "";
            this.lblWorkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWorkStatus.Location = new System.Drawing.Point(518, 36);
            this.lblWorkStatus.Name = "lblWorkStatus";
            this.lblWorkStatus.Size = new System.Drawing.Size(62, 15);
            this.lblWorkStatus.TabIndex = 22;
            this.lblWorkStatus.Text = "Work Status";
            // 
            // chkIncludeCompany
            // 
            this.chkIncludeCompany.AutoSize = true;
            // 
            // 
            // 
            this.chkIncludeCompany.BackgroundStyle.Class = "";
            this.chkIncludeCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkIncludeCompany.Checked = true;
            this.chkIncludeCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeCompany.CheckValue = "Y";
            this.chkIncludeCompany.Location = new System.Drawing.Point(68, 92);
            this.chkIncludeCompany.Name = "chkIncludeCompany";
            this.chkIncludeCompany.Size = new System.Drawing.Size(108, 15);
            this.chkIncludeCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkIncludeCompany.TabIndex = 18;
            this.chkIncludeCompany.Text = "Include Company";
            this.chkIncludeCompany.CheckedChanged += new System.EventHandler(this.chkIncludeCompany_CheckedChanged);
            // 
            // dtpAttDate
            // 
            this.dtpAttDate.CustomFormat = "dd MMM yyyy";
            this.dtpAttDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAttDate.Location = new System.Drawing.Point(820, 36);
            this.dtpAttDate.Name = "dtpAttDate";
            this.dtpAttDate.Size = new System.Drawing.Size(101, 20);
            this.dtpAttDate.TabIndex = 16;
            this.dtpAttDate.ValueChanged += new System.EventHandler(this.dtpAttDate_ValueChanged);
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(777, 37);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(28, 15);
            this.lblFromDate.TabIndex = 14;
            this.lblFromDate.Text = "From";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.DisplayMember = "Text";
            this.cboEmployee.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.ItemHeight = 14;
            this.cboEmployee.Location = new System.Drawing.Point(587, 66);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(175, 20);
            this.cboEmployee.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboEmployee.TabIndex = 8;
            this.cboEmployee.SelectedIndexChanged += new System.EventHandler(this.cboEmployee_SelectedIndexChanged);
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_KeyDown);
            // 
            // cboBranch
            // 
            this.cboBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBranch.DisplayMember = "Text";
            this.cboBranch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboBranch.DropDownHeight = 134;
            this.cboBranch.FormattingEnabled = true;
            this.cboBranch.IntegralHeight = false;
            this.cboBranch.ItemHeight = 14;
            this.cboBranch.Location = new System.Drawing.Point(68, 66);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(175, 20);
            this.cboBranch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboBranch.TabIndex = 7;
            this.cboBranch.SelectedIndexChanged += new System.EventHandler(this.cboBranch_SelectedIndexChanged);
            this.cboBranch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_KeyDown);
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployee.BackgroundStyle.Class = "";
            this.lblEmployee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployee.Location = new System.Drawing.Point(518, 69);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(51, 15);
            this.lblEmployee.TabIndex = 6;
            this.lblEmployee.Text = "Employee";
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            // 
            // 
            // 
            this.lblBranch.BackgroundStyle.Class = "";
            this.lblBranch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBranch.Location = new System.Drawing.Point(12, 69);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(37, 15);
            this.lblBranch.TabIndex = 5;
            this.lblBranch.Text = "Branch";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(68, 36);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(175, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 4;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_KeyDown);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(11, 36);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 2;
            this.lblCompany.Text = "Company";
            // 
            // rvAttendance
            // 
            this.rvAttendance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rvAttendance.Location = new System.Drawing.Point(0, 114);
            this.rvAttendance.Name = "rvAttendance";
            this.rvAttendance.Size = new System.Drawing.Size(1216, 425);
            this.rvAttendance.TabIndex = 2;
            this.rvAttendance.RenderingComplete += new Microsoft.Reporting.WinForms.RenderingCompleteEventHandler(this.rvAttendance_RenderingComplete);
            this.rvAttendance.RenderingBegin += new System.ComponentModel.CancelEventHandler(this.rvAttendance_RenderingBegin);
            // 
            // pbGridView
            // 
            this.pbGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbGridView.Location = new System.Drawing.Point(1116, 1);
            this.pbGridView.Name = "pbGridView";
            this.pbGridView.Size = new System.Drawing.Size(174, 20);
            this.pbGridView.TabIndex = 0;
            // 
            // pnlGrid
            // 
            this.pnlGrid.BackColor = System.Drawing.Color.Transparent;
            this.pnlGrid.Controls.Add(this.progressBar1);
            this.pnlGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlGrid.Location = new System.Drawing.Point(0, 517);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(1216, 22);
            this.pnlGrid.TabIndex = 8;
            this.pnlGrid.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(1042, 1);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(174, 20);
            this.progressBar1.TabIndex = 0;
            // 
            // dgvReport
            // 
            this.dgvReport.AllowUserToAddRows = false;
            this.dgvReport.AllowUserToDeleteRows = false;
            this.dgvReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReport.BackgroundColor = System.Drawing.Color.White;
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvReport.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReport.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvReport.Location = new System.Drawing.Point(0, 114);
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.ReadOnly = true;
            this.dgvReport.Size = new System.Drawing.Size(1216, 403);
            this.dgvReport.TabIndex = 9;
            this.dgvReport.Visible = false;
            this.dgvReport.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvReport_DataError);
            // 
            // FrmRptAttendanceLive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 539);
            this.Controls.Add(this.dgvReport);
            this.Controls.Add(this.pnlGrid);
            this.Controls.Add(this.rvAttendance);
            this.Controls.Add(this.expandablePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptAttendanceLive";
            this.Text = "Attendance ";
            this.Load += new System.EventHandler(this.FrmRptAttendanceLive_Load);
            this.expandablePanel1.ResumeLayout(false);
            this.expandablePanel1.PerformLayout();
            this.pnlGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkIncludeCompany;
        private System.Windows.Forms.DateTimePicker dtpAttDate;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboEmployee;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboBranch;
        private DevComponents.DotNetBar.LabelX lblEmployee;
        private DevComponents.DotNetBar.LabelX lblBranch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboWorkStatus;
        private DevComponents.DotNetBar.LabelX lblWorkStatus;
        private Microsoft.Reporting.WinForms.ReportViewer rvAttendance;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDesignation;
        private DevComponents.DotNetBar.LabelX lblDesignation;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDepartment;
        private DevComponents.DotNetBar.LabelX lblDepartment;
        private DevComponents.DotNetBar.LabelX lblToDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private DevComponents.DotNetBar.ButtonX btnExcelExport;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboReportFormat;
        private System.Windows.Forms.ProgressBar pbGridView;
        private System.Windows.Forms.Panel pnlGrid;
        private System.Windows.Forms.ProgressBar progressBar1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvReport;

    }
}