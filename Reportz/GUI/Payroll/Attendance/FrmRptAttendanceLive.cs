﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using Microsoft.VisualBasic;
using System.IO;

/*****************************************************
   * Created By       : Sruthy K
   * Creation Date    : 06 Aug 2013
   * Description      : Attendance Report
   * FormID           : 133
   * Message Code     : 3002 - 3003
   * ******************************************************/

namespace MyPayfriend
{
    public partial class FrmRptAttendanceLive : Report
    {
       
        #region Properties
        ClsExportDatagridviewToExcel MobjExportToExcel;
        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AttendanceReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor

        public FrmRptAttendanceLive()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }

            MobjExportToExcel = new ClsExportDatagridviewToExcel();
        }
        #endregion
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.AttendanceReportLive, this);
        }
        #endregion SetArabicControls

        #region FormLoad


        private void FrmRptAttendanceLive_Load(object sender, EventArgs e)
        {
            dtpAttDate.Value = ClsCommonSettings.GetServerDate();
            FillReportFormat();
            LoadCombos();
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
            dtpAttDate.Checked = true; 
        }

        #endregion

        private void FillReportFormat()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("ReportFormatID", typeof(int));
            datTemp.Columns.Add("ReportFormat", typeof(string));

            datTemp.Rows.Add(1, "Graphics Format");
            datTemp.Rows.Add(1, "Grid Format");

            cboReportFormat.DataSource = datTemp;
            cboReportFormat.ValueMember = "ReportFormatID";
            cboReportFormat.DisplayMember = "ReportFormat";

            cboReportFormat.SelectedIndex = 0;
        }

        #region Functions
        /// <summary>
        /// Load All Combos
        /// </summary>
        private void LoadCombos()
        {
            FillCompany();
            FillAllDepartments();
            FillAllDesignations();
            FillAllWorkStatus();
        }

        /// <summary>
        /// Load Company
        /// </summary>
        private void FillCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Load Branches by Company
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());
        }


        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }



        /// <summary>
        /// Load All Designations
        /// </summary>
        private void FillAllDesignations()
        {
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation();
        }

        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }


        /// <summary>
        /// Load Employees by Company,Department,Designation
        /// </summary>
        private void FillAllEmployees()
        {
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked,cboDepartment.SelectedValue.ToInt32(),cboDesignation.SelectedValue.ToInt32(),-1,cboWorkStatus.SelectedValue.ToInt32(),-1
                                );
        }
        /// <summary>
        /// Load Month 
        /// </summary>


       
        /// <summary>
        /// Show Report
        /// </summary>
        private void ShowReport()
        {


            //Set Company Header For Report
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            //Clear Report Datasources
            this.rvAttendance.Reset(); 
            this.rvAttendance.LocalReport.DataSources.Clear();
            this.rvAttendance.Refresh();
     


                    DataSet dsReport;

                    if (cboEmployee.SelectedIndex != 0)
                    {

                        dsReport = clsBLLAttendanceReport.GetPunchingLogReport(cboEmployee.SelectedValue.ToInt32(), dtpAttDate.Value.ToString("dd-MMM-yyyy"), dtpToDate.Value.ToString("dd-MMM-yyyy"), cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked.ToBoolean(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32());
                    }
                    else
                    {
                        dsReport = clsBLLAttendanceReport.GetPunchingLogReport(dtpAttDate.Value.ToString("dd-MMM-yyyy"), dtpToDate.Value.ToString("dd-MMM-yyyy"), cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked.ToBoolean(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32());
                        
                    }

                    this.rvAttendance.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceLogDailyPunching.rdlc";

                    rvAttendance.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter)
                    });
                    if (dsReport.Tables[0].Rows.Count > 0)
                    {
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_DtPunchingLog", dsReport.Tables[0]));
                        this.rvAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    }
                    else
                    {
                        UserMessage.ShowMessage(3003);
                        return;
                    }
          


         
            SetReportDisplay(rvAttendance);
        }

        /// <summary>
        /// Validate User Inputs
        /// </summary>
        /// <returns></returns>
        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(9100);
                cboCompany.Focus();
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                cboBranch.Focus();
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9109);
                return false;
            }
            if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9112);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                cboWorkStatus.Focus();
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                cboEmployee.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Enable or Disable Include Company checkbox
        /// </summary>
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }

        #endregion

        #region ControlEvents

        private void btnShow_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (FormValidations())
                {
                    if (cboReportFormat.SelectedIndex == 0)
                        ShowReport();
                    else
                        ShowGridReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
            this.Cursor = Cursors.Default;
        }

        private void dtpAttDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                dtpToDate.Enabled = false;
                if (cboEmployee.SelectedIndex == 0 || cboEmployee.SelectedIndex == -1)
                {
                    dtpToDate.Value  = dtpAttDate.Value.AddDays(7).AddDays(-1);  
                }
                else
                {
                    dtpToDate.Value = dtpAttDate.Value.AddMonths(1).AddDays(-1);
                }



                if (cboEmployee.SelectedIndex >0)
                {
                    dtpToDate.Enabled = true;
                }

            }
            catch
            {
            }
        }
       
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }
      
        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                dtpAttDate_ValueChanged(sender, e);
            }
            catch
            { }


        }

        #endregion

        #region Control KeyDown Event
       
        private void cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cboSender = sender as ComboBox;
            if (cboSender != null)
                cboSender.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rvAttendance_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvAttendance_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion


        private void ShowGridReport()
        {
            try
            {
                pnlGrid.Visible = true;
                DataTable datTemp = new DataTable();
                DataSet dtSet;
                if (cboEmployee.SelectedIndex != 0)
                {
                    dtSet = clsBLLAttendanceReport.GetPunchingLogReportGrid(cboEmployee.SelectedValue.ToInt32(),
                            dtpAttDate.Value.ToString("dd-MMM-yyyy"), dtpToDate.Value.ToString("dd-MMM-yyyy"),
                            cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked.ToBoolean(),
                            cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32());
                }
                else
                {
                    dtSet = clsBLLAttendanceReport.GetPunchingLogReportGrid(dtpAttDate.Value.ToString("dd-MMM-yyyy"), dtpToDate.Value.ToString("dd-MMM-yyyy"),
                            cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked.ToBoolean(),
                            cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32());
                }

                if (dtSet == null || dtSet.Tables.Count<=0)
                {
                    UserMessage.ShowMessage(3003);
                    return;
                }
                else if (dtSet.Tables[0].Rows.Count <= 0)
                {
                    UserMessage.ShowMessage(3003);
                    return;
                }
                else
                {
                    datTemp = dtSet.Tables[0];
                }

                dgvReport.Rows.Clear();
                dgvReport.Columns.Clear();
                bool IsEmpNoNumeric = true;

                foreach (DataColumn dc in datTemp.Columns)
                {
                    if (!(dc.ColumnName == "EmployeeID"))
                    {
                        dgvReport.Columns.Add(dc.ColumnName, dc.ColumnName);
                        dgvReport.Columns[dc.ColumnName].MinimumWidth = 100;
                    }

                    if (dc.ColumnName.Contains("Entry"))
                        dgvReport.Columns[dc.ColumnName].HeaderText = "Punch In";
                    else if (dc.ColumnName.Contains("Exit"))
                        dgvReport.Columns[dc.ColumnName].HeaderText = "Punch Out";


                    dgvReport.Columns[dc.ColumnName].SortMode = DataGridViewColumnSortMode.NotSortable;
                }

                pbGridView.Minimum = 0;
                pbGridView.Maximum = datTemp.Rows.Count;

                foreach (DataRow dr in datTemp.Rows)
                {
                    dgvReport.Rows.Add();

                    foreach (DataColumn dc in datTemp.Columns)
                    {
                        if (dc.ColumnName == "EmployeeNumber")
                        {
                            int intEmpSlNo = 0;
                            int.TryParse(dr["EmployeeNumber"].ToStringCustom(), out intEmpSlNo);

                            if (intEmpSlNo == 0)
                                IsEmpNoNumeric = false;

                            dgvReport.Rows[dgvReport.Rows.Count - 1].Cells["EmployeeSlNo"].Value = intEmpSlNo;
                        }

                        if (!(dc.ColumnName == "EmployeeID"))
                            dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value = dr[dc.ColumnName];
                    }

                    Application.DoEvents();
                    pbGridView.Value = dgvReport.Rows.Count - 1;
                }

                pbGridView.Value = datTemp.Rows.Count;

                if (IsEmpNoNumeric)
                {
                    dgvReport.Columns.Remove("EmployeeNumber");
                    dgvReport.Columns["EmployeeSlNo"].HeaderText = "EmployeeNumber";
                }
                else
                    dgvReport.Columns.Remove("EmployeeSlNo");

                pnlGrid.Visible = false;
            }
            catch
            {
                dgvReport.Rows.Clear();
                dgvReport.Columns.Clear();
            }
        }

        private void btnExcelExport_Click(object sender, EventArgs e)
        {
            string sFileName = "";

            if (dgvReport.RowCount <= 0)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";

                if (sfd.ShowDialog() == DialogResult.OK)
                    sFileName = sfd.FileName;
                else
                    return;
            }

            try
            {
                if (sFileName != "")
                {
                    if (dgvReport.RowCount > 0)
                        MobjExportToExcel.ExportToExcel(dgvReport, sFileName, 0, dgvReport.Columns.Count - 1, true);
                }
                else
                    MessageBox.Show("Please Enter Name");
            }
            catch { }
        }

        private void cboReportFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboReportFormat.SelectedIndex == 0)
                {
                    dgvReport.Rows.Clear();
                    dgvReport.Visible = pnlGrid.Visible = btnExcelExport.Visible = false;
                    rvAttendance.Visible = true;
                }
                else
                {
                    dgvReport.Visible = btnExcelExport.Visible = true;
                    rvAttendance.Visible = false;
                    this.rvAttendance.Reset();
                    this.rvAttendance.LocalReport.DataSources.Clear();
                    this.rvAttendance.Refresh();
                }
            }
            catch
            { }
        }

        private void dgvReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }
    }
}
