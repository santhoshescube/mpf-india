﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{
    public partial class FrmRptLocationSchedule : Form
    {
        clsBLLRptLocationSchedule MobjclsBLLRptLocationSchedule = null;
        public FrmRptLocationSchedule()
        {
            InitializeComponent();
        }

        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AttendanceReport);
                return this.ObjUserMessage;
            }
        }
        private clsBLLRptLocationSchedule BLLRptLocationSchedule
        {
            get
            {
                if (this.MobjclsBLLRptLocationSchedule == null)
                    this.MobjclsBLLRptLocationSchedule = new clsBLLRptLocationSchedule();

                return this.MobjclsBLLRptLocationSchedule;
            }
        }
        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;

            string strCondition = string.Empty;

            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0)
                {
                    datCombos = BLLRptLocationSchedule.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "ParentID = 0" });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                    blnRetvalue = true;
                }

                if (intType == 1)
                {
                    datCombos = BLLRptLocationSchedule.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "ParentID = " + cboCompany.SelectedValue });
                    cboBranch.ValueMember = "CompanyID";
                    cboBranch.DisplayMember = "CompanyName";
                    DataRow dtRow;

                    dtRow = datCombos.NewRow();
                    dtRow["CompanyID"] = -1;
                    dtRow["CompanyName"] = "NONE";
                    datCombos.Rows.InsertAt(dtRow, 0);

                    if (datCombos.Rows.Count > 1)
                    {
                        dtRow = datCombos.NewRow();
                        dtRow["CompanyID"] = 0;
                        dtRow["CompanyName"] = "ALL";
                        datCombos.Rows.InsertAt(dtRow, 1);
                    }

                    cboBranch.DataSource = datCombos;
                    blnRetvalue = true;
                }

                if (intType == 2)
                {
                    strCondition = SetCompanySearchCondition();

                    datCombos = BLLRptLocationSchedule.FillCombos(new string[] { " DISTINCT EM.EmployeeID,EM.EmployeeFullName AS EmployeeName", "EmployeeMaster EM", strCondition });

                    DataRow dr = datCombos.NewRow();
                    dr["EmployeeID"] = 0;
                    dr["EmployeeName"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);

                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DisplayMember = "EmployeeName";
                    cboEmployee.DataSource = datCombos;

                }

                if (intType == 3)
                {
                    datCombos = BLLRptLocationSchedule.FillCombos(new string[] { "WR.WorkLocationID,WR.LocationName", "WorkLocationReference WR INNER JOIN WorkLocationSchedule WS ON WR.WorkLocationID=WS.WorkLocationID INNER JOIN WorkLocationScheduleDetails WSD ON WS.ScheduleID=WSD.ScheduleID", " WSD.EmployeeID= " + cboEmployee.SelectedValue.ToInt32() + "" });
                    DataRow dr = datCombos.NewRow();
                    dr["WorkLocationID"] = 0;
                    dr["LocationName"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);

                    cboLocation.ValueMember = "WorkLocationID";
                    cboLocation.DisplayMember = "LocationName";
                    cboLocation.DataSource = datCombos;
                }
              

            }
            catch (Exception Ex)
            {
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }

        private string SetCompanySearchCondition()
        {
            string strCondition = string.Empty;

            //if (cboCompany.SelectedValue != null && chkIncludeCompany.Checked)
            //    strCondition = strCondition + "EM.CompanyID = " + cboCompany.SelectedValue.ToInt32();

            //if (cboBranch.SelectedValue != null && cboBranch.SelectedValue.ToInt32() != -1)
            //{
            //    if (strCondition != string.Empty)
            //        strCondition = strCondition + " OR ";

            //    if (cboBranch.SelectedValue.ToInt32() == 0)
            //        strCondition = strCondition + "EM.CompanyID IN (SELECT CompanyID FROM CompanyMaster WHERE ParentID = " + cboCompany.SelectedValue.ToInt32() + ")";
            //    else
            //        strCondition = strCondition + "EM.CompanyID = " + cboCompany.SelectedValue.ToInt32();
            //}
            strCondition = "";
            int intBranch = Convert.ToInt32(this.cboBranch.SelectedValue);
            if (intBranch == 0) //all branches
                strCondition = " EM.CompanyID IN(SELECT " + Convert.ToInt32(cboCompany.SelectedValue) + "  UNION SELECT CompanyID FROM CompanyMaster WHERE ParentId = " + Convert.ToInt32(cboCompany.SelectedValue) + ")";

            else if (intBranch == -1)//no branch
                strCondition = " EM.CompanyID = " + Convert.ToInt32(cboCompany.SelectedValue);

            else if (intBranch > 0 && chkIncludeCompany.Checked)//branch and include company 
                strCondition = " EM.CompanyID IN(" + Convert.ToInt32(cboCompany.SelectedValue) + "," + Convert.ToInt32(cboBranch.SelectedValue) + ")";

            else if (intBranch > 0 && !chkIncludeCompany.Checked)//only branch
                strCondition = " EM.CompanyID = " + intBranch;

            return strCondition;

        }

        private void FrmRptLocationSchedule_Load(object sender, EventArgs e)
        {
            LoadCombos(0);
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboCompany_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboCompany.SelectedValue != null)
                LoadCombos(1);
        }

        private void cboBranch_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadCombos(2);
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboBranch.SelectedValue.ToInt32() == -1)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = true;
            }
            if (cboBranch.SelectedValue.ToInt32() == -1)
                chkIncludeCompany.Checked = true;
            else
                chkIncludeCompany.Checked = false;
            ClearReport();
            LoadCombos(2);
        }

        private void cboEmployee_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadCombos(3);
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            if (ValidateReport())
                ShowReport();
        }

        private void ShowReport()
        {
            int intCompanyID = cboCompany.SelectedValue.ToInt32();
            int intIncludeCompany = chkIncludeCompany.Checked ? 1 : 0;
            int intBranchID = cboBranch.SelectedValue.ToInt32();
            int intLocationID = cboLocation.SelectedValue.ToInt32();
            int intEmployeeID = cboEmployee.SelectedValue.ToInt32();
            DateTime dtFromDate = dtpFromDate.Value.ToString("dd MMM yyyy").ToDateTime();
            DateTime dtToDate = dtpToDate.Value.ToString("dd MMM yyyy").ToDateTime();


            ClearReport();

            DataTable dtReport = this.MobjclsBLLRptLocationSchedule.GetReport(intCompanyID, intBranchID, intIncludeCompany, intEmployeeID, intLocationID, dtFromDate, dtToDate);

            if (dtReport.Rows.Count <= 0)
            {
                UserMessage.ShowMessage(3000);
                return;
            }
            else
            {
                intCompanyID = cboBranch.SelectedValue.ToInt32() > 0 ? cboBranch.SelectedValue.ToInt32() : cboCompany.SelectedValue.ToInt32();
                DataTable dtCompanyHeader = MobjclsBLLRptLocationSchedule.GetCompanyHeader(intCompanyID);

                this.rptLocationSchedule.LocalReport.DataSources.Clear();
                string strMReportPath;
                if (ClsCommonSettings.IsArabicView)
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptWorkLocationScheduleArb.rdlc";

                }
                else
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptWorkLocationSchedule.rdlc";


                }
                //string strMReportPath = Application.StartupPath + "\\MainReports\\Report4.rdlc";
                
                ReportParameter[] Parameters = new ReportParameter[2];
                Parameters[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
                Parameters[1] = new ReportParameter("Employee", cboEmployee.Text, false);


                this.rptLocationSchedule.ProcessingMode = ProcessingMode.Local;
                this.rptLocationSchedule.LocalReport.ReportPath = strMReportPath;
                this.rptLocationSchedule.LocalReport.SetParameters(Parameters);
                this.rptLocationSchedule.LocalReport.DataSources.Add(new ReportDataSource("DtSetLocationSchedule_dtLocationSchedule", dtReport));
                this.rptLocationSchedule.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_CompanyDetails", dtCompanyHeader));
                this.rptLocationSchedule.SetDisplayMode(DisplayMode.PrintLayout);
                this.rptLocationSchedule.ZoomMode = ZoomMode.Percent;
                this.rptLocationSchedule.ZoomPercent = 100;
            }

        }

        private void ClearReport()
        {
            rptLocationSchedule.Clear();
            rptLocationSchedule.Refresh();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private bool ValidateReport()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(3003);
                cboCompany.Focus();
                return false;
            }
            else if (cboBranch.SelectedValue.ToStringCustom() == string.Empty)
            {
                UserMessage.ShowMessage(3004);
                cboBranch.Focus();
                return false;
            }
            else if (cboLocation.SelectedValue.ToStringCustom() == string.Empty)
            {
                UserMessage.ShowMessage(3007);
                cboLocation.Focus();
                return false;
            }
            else if (cboEmployee.SelectedValue.ToStringCustom() == string.Empty)
            {
                UserMessage.ShowMessage(3006);
                cboEmployee.Focus();
                return false;
            }
            else if (dtpFromDate.Value.ToDateTime() > dtpToDate.Value.ToDateTime())
            {
                UserMessage.ShowMessage(19);
                dtpFromDate.Focus();
                return false;
            }
     
            return true;
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            ClearReport();
            LoadCombos(2);
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void cboBranch_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboBranch.DroppedDown = false;
        }

        private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        private void cboLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboLocation.DroppedDown = false;
        }

    }
}
