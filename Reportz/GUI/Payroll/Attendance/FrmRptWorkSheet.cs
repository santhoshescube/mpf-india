﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{
    public partial class FrmRptWorkSheet:Form 
    {
        clsBLLRptAttendanceManual MobjclsBLLRptAttendanceManual = null;
        
        public FrmRptWorkSheet()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }

        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.WorkSheetReport); 
                return this.ObjUserMessage;
            }
        }
        private clsBLLRptAttendanceManual BLLRptAttendanceManual
        {
            get
            {
                if (this.MobjclsBLLRptAttendanceManual == null)
                    this.MobjclsBLLRptAttendanceManual = new clsBLLRptAttendanceManual();

                return this.MobjclsBLLRptAttendanceManual;
            }
        }

        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }

        private void FrmRptWorkSheet_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCombos(0);
                dtpFromDate.Value = ("01/" + GetCurrentMonth(dtpFromDate.Value.Month) + "/" + dtpFromDate.Value.Year.ToString() + "").ToDateTime();

                dtpAttDate_ValueChanged(sender, e);
            }
            catch
            { }
        }
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.WorkSheetReport, this);
        }
        #endregion SetArabicControls

        private bool LoadCombos(int intType)
        {
            bool blnRetvalue = false;

            string strCondition = string.Empty;

            DataTable datCombos = new DataTable();
            try
            {

                if (intType == 0)
                {
                    datCombos = BLLRptAttendanceManual.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "ParentID = 0" });
                    cboCompany.ValueMember = "CompanyID";
                    cboCompany.DisplayMember = "CompanyName";
                    cboCompany.DataSource = datCombos;
                    blnRetvalue = true;
                }

                if (intType == 1)
                {
                    datCombos = BLLRptAttendanceManual.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "ParentID = " + cboCompany.SelectedValue });
                    cboBranch.ValueMember = "CompanyID";
                    cboBranch.DisplayMember = "CompanyName";
                    DataRow dtRow;

                    dtRow = datCombos.NewRow();
                    dtRow["CompanyID"] = -1;
                    dtRow["CompanyName"] = "NONE";
                    datCombos.Rows.InsertAt(dtRow, 0);

                    if (datCombos.Rows.Count > 1)
                    {
                        dtRow = datCombos.NewRow();
                        dtRow["CompanyID"] = 0;
                        dtRow["CompanyName"] = "ALL";
                        datCombos.Rows.InsertAt(dtRow, 1);
                    }

                    cboBranch.DataSource = datCombos;
                    blnRetvalue = true;
                }
                if (intType == 0)
                {
                    datCombos = BLLRptAttendanceManual.FillCombos(new string[] { "DepartmentID,Department", "DepartmentReference", "" });
                    DataRow dr = datCombos.NewRow();
                    dr["DepartmentID"] = 0;
                    dr["Department"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);

                    cboDepartment.ValueMember = "DepartmentID";
                    cboDepartment.DisplayMember = "Department";
                    cboDepartment.DataSource = datCombos;
                }

                if (intType == 2)
                {
                    strCondition = SetCompanySearchCondition();
                    

                    if (cboDepartment.SelectedValue != null && cboDepartment.SelectedValue.ToInt32() > 0)
                    {

                        strCondition = strCondition + " AND A.DepartmentID = " + cboDepartment.SelectedValue.ToInt32() + " And A.FromDate= '" + dtpFromDate.Value.ToString("dd-MMM-yyyy")  + "'";
      
                    }


                    datCombos = BLLRptAttendanceManual.FillCombos(new string[] { " DISTINCT EM.EmployeeID,EM.EmployeeFullName AS EmployeeName", "EmployeeMaster AS EM INNER JOIN WorkSheetDetails AS A ON A.EmployeeID = EM.EmployeeID", strCondition });

                    DataRow dr = datCombos.NewRow();
                    dr["EmployeeID"] = 0;
                    dr["EmployeeName"] = "ALL";
                    datCombos.Rows.InsertAt(dr, 0);

                    cboEmployee.ValueMember = "EmployeeID";
                    cboEmployee.DisplayMember = "EmployeeName";
                    cboEmployee.DataSource = datCombos;

                }
                if (intType == 3 || intType == 0)
                {
                    strCondition = "";
                    datCombos = null;

                    //cboMonth.Text = string.Empty;

                    if (cboEmployee.SelectedValue.ToInt32() > 0)
                    {
                        strCondition = "EmployeeID=" + cboEmployee.SelectedValue.ToInt32();
                        datCombos = BLLRptAttendanceManual.FillCombos(new string[] { "Distinct CAST(Month(Date) AS VARCHAR)+'@'+CAST(Year(Date) AS VARCHAR) As MDate,convert(varchar,datename(mm,Date)) " +
                                                                            " +' - ' +convert(varchar, year(Date)) As [Month]", "PayAttendanceManual", strCondition });

                    }
                    else
                    {
                        int intBranchID = 0;
                        if (cboBranch.SelectedValue.ToInt32() == -2)
                            intBranchID = 0;
                        else
                            intBranchID = cboBranch.SelectedValue.ToInt32();
                        int iIncludeCompany = chkIncludeCompany.Checked ? 1 : 0;
                        datCombos = BLLRptAttendanceManual.GetAttendanceMonths(cboCompany.SelectedValue.ToInt32(), intBranchID, iIncludeCompany);
                    }
                    //cboMonth.ValueMember = "MDate";
                    //cboMonth.DisplayMember = "Month";
                    //cboMonth.DataSource = datCombos;
                    blnRetvalue = true;

                }


            }
            catch (Exception Ex)
            {
                //ClsLogWriter.WriteLog(Ex, Log.LogSeverity.Error);
                blnRetvalue = false;
            }
            return blnRetvalue;
        }

        private void ClearReport()
        {
            rptAttendance.Clear();
            rptAttendance.Refresh();
        }

        private string SetCompanySearchCondition()
        {
            string strCondition = string.Empty;

            if (cboCompany.SelectedValue != null && chkIncludeCompany.Checked)
                strCondition = strCondition + "EM.CompanyID = " + cboCompany.SelectedValue.ToInt32();

            if (cboBranch.SelectedValue != null && cboBranch.SelectedValue.ToInt32() != -1)
            {
                if (strCondition != string.Empty)
                    strCondition = strCondition + " OR ";

                if (cboBranch.SelectedValue.ToInt32() == 0)
                    strCondition = strCondition + "EM.CompanyID IN (SELECT CompanyID FROM CompanyMaster WHERE ParentID = " + cboCompany.SelectedValue.ToInt32() + ")";
                else
                    strCondition = strCondition + "EM.CompanyID = " + cboBranch.SelectedValue.ToInt32();
            }

            return strCondition;

        }

        private void cboCompany_SelectedValueChanged(object sender, EventArgs e)
        {
            if(cboCompany.SelectedValue != null)
                LoadCombos(1);
        }

        private void cboLocation_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadCombos(2);
        }

        private void cboEmployee_SelectedValueChanged(object sender, EventArgs e)
        {
            //LoadCombos(3);
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            if (ValidateReport())
                ShowReport();
        }

        private bool ValidateReport()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(20078);
                cboCompany.Focus();
                return false;
            }
            else if (cboBranch.SelectedValue.ToStringCustom() == string.Empty)
            {
                UserMessage.ShowMessage(20079);
                cboBranch.Focus();
                return false;
            }
            else if (cboDepartment.SelectedValue.ToStringCustom() == string.Empty)
            {
                UserMessage.ShowMessage(9109);
                cboDepartment.Focus();
                return false;
            }
            else if (cboEmployee.SelectedValue.ToStringCustom() == string.Empty)
            {
                UserMessage.ShowMessage(20081);
                cboEmployee.Focus();
                return false;
            }
            //else if (dtpAttDate.Checked == false && cboMonth.SelectedValue.ToStringCustom() == string.Empty)
            //{
            //    UserMessage.ShowMessage(20080);
            //    cboMonth.Focus();
            //    return false;
            //}
            return true;
        }

        private void ShowReport()
        {
            int intCompanyID = cboCompany.SelectedValue.ToInt32();
            int intIncludeCompany = chkIncludeCompany.Checked ? 1 : 0;
            int intBranchID = cboBranch.SelectedValue.ToInt32();
            int intLocationID = cboDepartment.SelectedValue.ToInt32();
            int intEmployeeID = cboEmployee.SelectedValue.ToInt32();
            DateTime dtAttendanceDate = dtpFromDate.Value.ToString("dd MMM yyyy").ToDateTime();
            int intMonth = 0;
            int intYear = 0;

            //if (!dtpAttDate.Checked)
            //{
            //    intMonth = cboMonth.SelectedValue.ToString().Split('@').GetValue(0).ToInt32();
            //    intYear = cboMonth.SelectedValue.ToString().Split('@').GetValue(1).ToInt32();
            //}

            rptAttendance.Clear();
            rptAttendance.Refresh();

            DataTable dtReport = this.MobjclsBLLRptAttendanceManual.GetReportWorkSheet(intCompanyID, intBranchID, intIncludeCompany, intLocationID, intEmployeeID, intMonth, intYear, dtAttendanceDate);

            if (dtReport.Rows.Count <= 0)
            {
                UserMessage.ShowMessage(20075);
                return;
            }
            else
            {
                intCompanyID = cboBranch.SelectedValue.ToInt32() > 0 ? cboBranch.SelectedValue.ToInt32() : cboCompany.SelectedValue.ToInt32();
                DataTable dtCompanyHeader = MobjclsBLLRptAttendanceManual.GetCompanyHeader(intCompanyID);

                this.rptAttendance.LocalReport.DataSources.Clear();
                string strMReportPath = Application.StartupPath + "\\MainReports\\RptAttendanceWorkSheet.rdlc";

                ReportParameter[] Parameters = new ReportParameter[3];
                Parameters[0] = new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false);
                Parameters[1] = new ReportParameter("Employee", cboEmployee.Text, false);
                Parameters[2] = new ReportParameter("IsDateSelected", "0", false);

                this.rptAttendance.ProcessingMode = ProcessingMode.Local;
                this.rptAttendance.LocalReport.ReportPath = strMReportPath;
                this.rptAttendance.LocalReport.SetParameters(Parameters);
                this.rptAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtSetWorkSheet_spPayWorkSheetTransaction", dtReport));
                this.rptAttendance.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_CompanyDetails", dtCompanyHeader));
                this.rptAttendance.SetDisplayMode(DisplayMode.PrintLayout);
                this.rptAttendance.ZoomMode = ZoomMode.Percent;
                this.rptAttendance.ZoomPercent = 100;
            }

        }
        private string GetStratDate()
        {
            //'Getting Start date
            string sMonth = "";
            switch (dtpFromDate.Value.Month)
            {
                case 1:
                    sMonth = "Jan";
                    break;
                case 2:
                    sMonth = "Feb";
                    break;
                case 3:
                    sMonth = "Mar";
                    break;
                case 4:
                    sMonth = "Apr";
                    break;
                case 5:
                    sMonth = "May";
                    break;
                case 6:
                    sMonth = "Jun";
                    break;
                case 7:
                    sMonth = "Jul";
                    break;
                case 8:
                    sMonth = "Aug";
                    break;
                case 9:
                    sMonth = "Sep";
                    break;
                case 10:
                    sMonth = "Oct";
                    break;
                case 11:
                    sMonth = "Nov";
                    break;
                case 12:
                    sMonth = "Dec";
                    break;
            }
            return Convert.ToDateTime("01-" + sMonth + "-" + dtpFromDate.Value.Year).ToString("dd MMM yyyy");
        }
        private void dtpAttDate_ValueChanged(object sender, EventArgs e)
        {
            ClearReport();
            LoadCombos(2);
            //if (dtpAttDate.Checked)
            //{
            //    cboMonth.Enabled = false;
            //}
            //else
                //cboMonth.Enabled = true;

            

        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
            LoadCombos(3);
        }

        private void cboLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
        }

        private void cboMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearReport();
            LoadCombos(2);
        }

        private void cboBranch_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadCombos(2);
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false;
        }

        private void cboBranch_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboBranch.DroppedDown = false;
        }

        private void cboLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboDepartment.DroppedDown = false;
        }

        private void cboEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboEmployee.DroppedDown = false;
        }

        //private void cboMonth_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    cboMonth.DroppedDown = false;
        //}

        private void dtpFromDate_KeyDown(object sender, KeyEventArgs e)
        {
            dtpFromDate.Value = GetStratDate().ToDateTime();
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                dtpFromDate.Value = GetStratDate().ToDateTime();
            }
            catch
            {
            }
        }

        private void rptAttendance_Load(object sender, EventArgs e)
        {

        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCombos(3);
            }
            catch
            {
            }
        }
    }
}
