﻿namespace MyPayfriend
{
    partial class FrmExpenseReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExpenseReport));
            this.rptViewerExpense = new Microsoft.Reporting.WinForms.ReportViewer();
            this.exPanelVacation = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboWorkStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWorkStatus = new DevComponents.DotNetBar.LabelX();
            this.cboExpenseType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.cboDepartment = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblDepartment = new DevComponents.DotNetBar.LabelX();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.chkIncludeCompany = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cboEmployee = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboBranch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblEmployee = new DevComponents.DotNetBar.LabelX();
            this.lblBranch = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.exPanelVacation.SuspendLayout();
            this.SuspendLayout();
            // 
            // rptViewerExpense
            // 
            this.rptViewerExpense.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptViewerExpense.Location = new System.Drawing.Point(0, 108);
            this.rptViewerExpense.Name = "rptViewerExpense";
            this.rptViewerExpense.Size = new System.Drawing.Size(1235, 270);
            this.rptViewerExpense.TabIndex = 6;
            this.rptViewerExpense.RenderingComplete += new Microsoft.Reporting.WinForms.RenderingCompleteEventHandler(this.rptViewerExpense_RenderingComplete);
            this.rptViewerExpense.RenderingBegin += new System.ComponentModel.CancelEventHandler(this.rptViewerExpense_RenderingBegin);
            // 
            // exPanelVacation
            // 
            this.exPanelVacation.CanvasColor = System.Drawing.SystemColors.Control;
            this.exPanelVacation.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.exPanelVacation.Controls.Add(this.cboWorkStatus);
            this.exPanelVacation.Controls.Add(this.lblWorkStatus);
            this.exPanelVacation.Controls.Add(this.cboExpenseType);
            this.exPanelVacation.Controls.Add(this.labelX1);
            this.exPanelVacation.Controls.Add(this.dtpToDate);
            this.exPanelVacation.Controls.Add(this.dtpFromDate);
            this.exPanelVacation.Controls.Add(this.lblToDate);
            this.exPanelVacation.Controls.Add(this.lblFromDate);
            this.exPanelVacation.Controls.Add(this.cboDepartment);
            this.exPanelVacation.Controls.Add(this.lblDepartment);
            this.exPanelVacation.Controls.Add(this.BtnShow);
            this.exPanelVacation.Controls.Add(this.chkIncludeCompany);
            this.exPanelVacation.Controls.Add(this.cboEmployee);
            this.exPanelVacation.Controls.Add(this.cboBranch);
            this.exPanelVacation.Controls.Add(this.lblEmployee);
            this.exPanelVacation.Controls.Add(this.lblBranch);
            this.exPanelVacation.Controls.Add(this.cboCompany);
            this.exPanelVacation.Controls.Add(this.lblCompany);
            this.exPanelVacation.Dock = System.Windows.Forms.DockStyle.Top;
            this.exPanelVacation.Location = new System.Drawing.Point(0, 0);
            this.exPanelVacation.Name = "exPanelVacation";
            this.exPanelVacation.Size = new System.Drawing.Size(1235, 108);
            this.exPanelVacation.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.exPanelVacation.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.exPanelVacation.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.exPanelVacation.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.exPanelVacation.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.exPanelVacation.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.exPanelVacation.Style.GradientAngle = 90;
            this.exPanelVacation.TabIndex = 5;
            this.exPanelVacation.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.exPanelVacation.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.exPanelVacation.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.exPanelVacation.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.exPanelVacation.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.exPanelVacation.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.exPanelVacation.TitleStyle.GradientAngle = 90;
            this.exPanelVacation.TitleText = "Filter By";
            // 
            // cboWorkStatus
            // 
            this.cboWorkStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWorkStatus.DropDownHeight = 134;
            this.cboWorkStatus.FormattingEnabled = true;
            this.cboWorkStatus.IntegralHeight = false;
            this.cboWorkStatus.ItemHeight = 14;
            this.cboWorkStatus.Location = new System.Drawing.Point(346, 59);
            this.cboWorkStatus.Name = "cboWorkStatus";
            this.cboWorkStatus.Size = new System.Drawing.Size(193, 20);
            this.cboWorkStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboWorkStatus.TabIndex = 152;
            this.cboWorkStatus.SelectedIndexChanged += new System.EventHandler(this.LoadEmployee);
            this.cboWorkStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_Keydown);
            // 
            // lblWorkStatus
            // 
            this.lblWorkStatus.AutoSize = true;
            // 
            // 
            // 
            this.lblWorkStatus.BackgroundStyle.Class = "";
            this.lblWorkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWorkStatus.Location = new System.Drawing.Point(278, 62);
            this.lblWorkStatus.Name = "lblWorkStatus";
            this.lblWorkStatus.Size = new System.Drawing.Size(62, 15);
            this.lblWorkStatus.TabIndex = 151;
            this.lblWorkStatus.Text = "Work Status";
            // 
            // cboExpenseType
            // 
            this.cboExpenseType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboExpenseType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboExpenseType.DisplayMember = "Text";
            this.cboExpenseType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboExpenseType.DropDownHeight = 134;
            this.cboExpenseType.FormattingEnabled = true;
            this.cboExpenseType.IntegralHeight = false;
            this.cboExpenseType.ItemHeight = 14;
            this.cboExpenseType.Location = new System.Drawing.Point(638, 61);
            this.cboExpenseType.Name = "cboExpenseType";
            this.cboExpenseType.Size = new System.Drawing.Size(193, 20);
            this.cboExpenseType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboExpenseType.TabIndex = 139;
            this.cboExpenseType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_Keydown);
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(558, 62);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(73, 15);
            this.labelX1.TabIndex = 138;
            this.labelX1.Text = "Expense Type";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(911, 59);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(82, 20);
            this.dtpToDate.TabIndex = 32;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(910, 33);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(82, 20);
            this.dtpFromDate.TabIndex = 31;
            // 
            // lblToDate
            // 
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(852, 63);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(40, 15);
            this.lblToDate.TabIndex = 29;
            this.lblToDate.Text = "To ";
            // 
            // lblFromDate
            // 
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(850, 37);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(54, 15);
            this.lblFromDate.TabIndex = 28;
            this.lblFromDate.Text = "From ";
            // 
            // cboDepartment
            // 
            this.cboDepartment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDepartment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDepartment.DisplayMember = "Text";
            this.cboDepartment.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboDepartment.DropDownHeight = 134;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.IntegralHeight = false;
            this.cboDepartment.ItemHeight = 14;
            this.cboDepartment.Location = new System.Drawing.Point(346, 33);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(193, 20);
            this.cboDepartment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboDepartment.TabIndex = 135;
            this.cboDepartment.SelectionChangeCommitted += new System.EventHandler(this.LoadEmployee);
            this.cboDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_Keydown);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            // 
            // 
            // 
            this.lblDepartment.BackgroundStyle.Class = "";
            this.lblDepartment.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDepartment.Location = new System.Drawing.Point(278, 34);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(60, 15);
            this.lblDepartment.TabIndex = 134;
            this.lblDepartment.Text = "Department";
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(1019, 35);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(64, 42);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 132;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // chkIncludeCompany
            // 
            this.chkIncludeCompany.AutoSize = true;
            // 
            // 
            // 
            this.chkIncludeCompany.BackgroundStyle.Class = "";
            this.chkIncludeCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkIncludeCompany.Location = new System.Drawing.Point(69, 87);
            this.chkIncludeCompany.Name = "chkIncludeCompany";
            this.chkIncludeCompany.Size = new System.Drawing.Size(108, 15);
            this.chkIncludeCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkIncludeCompany.TabIndex = 18;
            this.chkIncludeCompany.Text = "Include Company";
            this.chkIncludeCompany.CheckedChanged += new System.EventHandler(this.LoadEmployee);
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.DisplayMember = "Text";
            this.cboEmployee.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.ItemHeight = 14;
            this.cboEmployee.Location = new System.Drawing.Point(638, 33);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(193, 20);
            this.cboEmployee.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboEmployee.TabIndex = 8;
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_Keydown);
            // 
            // cboBranch
            // 
            this.cboBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBranch.DisplayMember = "Text";
            this.cboBranch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboBranch.DropDownHeight = 134;
            this.cboBranch.FormattingEnabled = true;
            this.cboBranch.IntegralHeight = false;
            this.cboBranch.ItemHeight = 14;
            this.cboBranch.Location = new System.Drawing.Point(69, 60);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(193, 20);
            this.cboBranch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboBranch.TabIndex = 7;
            this.cboBranch.SelectedIndexChanged += new System.EventHandler(this.cboBranch_SelectedIndexChanged);
            this.cboBranch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_Keydown);
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployee.BackgroundStyle.Class = "";
            this.lblEmployee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployee.Location = new System.Drawing.Point(558, 34);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(51, 15);
            this.lblEmployee.TabIndex = 6;
            this.lblEmployee.Text = "Employee";
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            // 
            // 
            // 
            this.lblBranch.BackgroundStyle.Class = "";
            this.lblBranch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBranch.Location = new System.Drawing.Point(13, 60);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(37, 15);
            this.lblBranch.TabIndex = 5;
            this.lblBranch.Text = "Branch";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(69, 33);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(193, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 4;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_Keydown);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(13, 34);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 2;
            this.lblCompany.Text = "Company";
            // 
            // FrmExpenseReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1235, 378);
            this.Controls.Add(this.rptViewerExpense);
            this.Controls.Add(this.exPanelVacation);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmExpenseReport";
            this.Text = "Expense Report";
            this.Load += new System.EventHandler(this.FrmExpenseReport_Load);
            this.exPanelVacation.ResumeLayout(false);
            this.exPanelVacation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptViewerExpense;
        private DevComponents.DotNetBar.ExpandablePanel exPanelVacation;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private DevComponents.DotNetBar.LabelX lblToDate;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboDepartment;
        private DevComponents.DotNetBar.LabelX lblDepartment;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkIncludeCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboEmployee;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboBranch;
        private DevComponents.DotNetBar.LabelX lblEmployee;
        private DevComponents.DotNetBar.LabelX lblBranch;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboExpenseType;
        private DevComponents.DotNetBar.LabelX labelX1;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboWorkStatus;
        private DevComponents.DotNetBar.LabelX lblWorkStatus;

    }
}