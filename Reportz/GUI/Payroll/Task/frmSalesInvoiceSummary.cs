﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public partial class frmSalesInvoiceSummary : Form
    {
        clsBLLReportViewer MobjclsBLLReportViewer;
        ClsExportDatagridviewToExcel MobjExportToExcel;
        ClsLogWriter MobjClsLogs;
        string[] Months = null;

        public frmSalesInvoiceSummary()
        {
            InitializeComponent();
            MobjclsBLLReportViewer = new clsBLLReportViewer();
            MobjExportToExcel = new ClsExportDatagridviewToExcel();
            MobjClsLogs = new ClsLogWriter(Application.StartupPath);
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
            }

        }

        private void frmSalesInvoiceSummary_Load(object sender, EventArgs e)
        {
            LoadCombos();

            this.dgvReport.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgvReport.ColumnHeadersHeight = this.dgvReport.ColumnHeadersHeight * 2;
            this.dgvReport.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            this.dgvReport.CellPainting += new DataGridViewCellPaintingEventHandler(dgvReport_CellPainting);
            this.dgvReport.Paint += new PaintEventHandler(dgvReport_Paint);
            this.dgvReport.Scroll += new ScrollEventHandler(dgvReport_Scroll);
            this.dgvReport.ColumnWidthChanged += new DataGridViewColumnEventHandler(dgvReport_ColumnWidthChanged);
        }

        private void LoadCombos()
        {
            FillAllCompany();
            FillAllDeparments();
            FillAllDesignation();
            FillAllProfitCenter();
        }

        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());
        }

        private void FillAllDeparments()
        {
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
        }

        private void FillAllDesignation()
        {
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation();
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
        }

        private void FillAllProfitCenter()
        {
            DataTable datTemp = new ClsCommonUtility().FillCombos(new string[] { "ProfitCenterID," + 
                    (ClsCommonSettings.IsArabicView ? "ProfitCenterArb" : "ProfitCenter") + " AS ProfitCenter", "ProfitCenterReference", "" });

            DataRow dr = datTemp.NewRow();
            dr["ProfitCenterID"] = -1;

            if (ClsCommonSettings.IsArabicView)
                dr["ProfitCenter"] = "أي";
            else
                dr["ProfitCenter"] = "All";

            datTemp.Rows.InsertAt(dr, 0);

            cboProfitCenter.DataSource = datTemp;
            cboProfitCenter.DisplayMember = "ProfitCenter";
            cboProfitCenter.ValueMember = "ProfitCenterID";
        }

        private void FillAllEmployees()
        {
            DataTable datTemp = clsBLLReportCommon.GetAllEmployeesProfitCenter(cboCompany.SelectedValue.ToInt32(),
                cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(),
                cboDesignation.SelectedValue.ToInt32(), cboProfitCenter.SelectedValue.ToInt32());

            DataRow dr = datTemp.NewRow();
            dr["EmployeeID"] = -1;

            if (ClsCommonSettings.IsArabicView)
                dr["EmployeeFullName"] = "أي";
            else
                dr["EmployeeFullName"] = "Any";

            datTemp.Rows.InsertAt(dr, 0);

            cboEmployee.DataSource = datTemp;
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
        }

        private void EnableDisableIncludeCompany()
        {
            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
                cboBranch.Enabled = true;

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

        private void cbo_KeyPress(object sender, KeyPressEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllBranches();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void dgvReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            string sFileName = "";

            if (dgvExcel.RowCount <= 0)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";

                if (sfd.ShowDialog() == DialogResult.OK)
                    sFileName = sfd.FileName;
                else
                    return;
            }

            try
            {
                if (sFileName != "")
                {
                    if (dgvExcel.RowCount > 0)
                        MobjExportToExcel.ExportToExcel(dgvExcel, sFileName, 0, dgvExcel.Columns.Count - 1, true);
                }
                else
                    MessageBox.Show("Please Enter Name");
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            }
        }
        
        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime selectedDate = dtpToDate.Value;
                DateTime LastDayOfMonth=new DateTime(selectedDate.Year,selectedDate.Month,DateTime.DaysInMonth(selectedDate.Year,selectedDate.Month));


                dtpFromDate.Value = new DateTime(dtpFromDate.Value.Year, dtpFromDate.Value.Month, 1);
               
                dgvReport.DataSource = dgvExcel.DataSource = null;
                DataTable datReport = MobjclsBLLReportViewer.DisplaySalesInvoiceSummary(cboCompany.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                    cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(),
                    cboProfitCenter.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(),
                    dtpFromDate.Value.ToString("dd MMM yyyy").ToDateTime(), LastDayOfMonth.ToString("dd MMM yyyy").ToDateTime());

                datReport.Rows.Add();

                foreach (DataColumn col in datReport.Columns)
                {
                    if (col.ColumnName == "Employee")
                        datReport.Rows[datReport.Rows.Count - 1][col] = "Total";
                    else
                        datReport.Rows[datReport.Rows.Count - 1][col] = datReport.Compute("SUM([" + col.ColumnName + "])", "").ToDecimal();
                }

                dgvReport.DataSource = dgvExcel.DataSource = datReport;
                MergeColumnSetUp();
            }
            catch { }
        }

        private void MergeColumnSetUp()
        {
            try
            {
                Months = new string[(dgvReport.Columns.Count - 1) / 2];
                int MonthCount = 0;

                for (int i = 0; i < this.dgvReport.ColumnCount; )
                {
                    if (dgvReport.Columns[i].HeaderText == "Employee")
                        i++;
                    else
                    {
                        Months[MonthCount] = dgvReport.Columns[i].HeaderText.Split('(').First();
                        i += 2;
                        MonthCount++;
                    }
                }

                for (int j = 0; j < this.dgvReport.ColumnCount; j++)
                {
                    if (dgvReport.Columns[j].HeaderText.Contains("(Incentive)"))
                        dgvReport.Columns[j].HeaderText = "Incentive";
                    else if (dgvReport.Columns[j].HeaderText.Contains("(Invoice)"))
                        dgvReport.Columns[j].HeaderText = "Invoice";
                }
            }
            catch { }
        }

        void dgvReport_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                Rectangle rtHeader = this.dgvReport.DisplayRectangle;
                rtHeader.Height = this.dgvReport.ColumnHeadersHeight / 2;
                this.dgvReport.Invalidate(rtHeader);
            }
            catch { }
        }

        void dgvReport_Scroll(object sender, ScrollEventArgs e)
        {
            try
            {
                Rectangle rtHeader = this.dgvReport.DisplayRectangle;
                rtHeader.Height = this.dgvReport.ColumnHeadersHeight / 2;
                this.dgvReport.Invalidate(rtHeader);
            }
            catch { }
        }

        void dgvReport_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                if (dgvReport.Columns.Count > 0)
                {
                    int i = dgvReport.Columns.Count - 1;

                    for (int j = 1; j < i; )
                    {
                        Rectangle r1 = this.dgvReport.GetCellDisplayRectangle(j, -1, true);
                        int w2 = this.dgvReport.GetCellDisplayRectangle(j, -1, true).Width;
                        r1.X += 1;
                        r1.Y += 1;
                        r1.Width = r1.Width + w2 - 2;
                        r1.Height = r1.Height / 2 - 2;
                        e.Graphics.FillRectangle(new SolidBrush(this.dgvReport.ColumnHeadersDefaultCellStyle.BackColor), r1);
                        StringFormat format = new StringFormat();
                        format.Alignment = StringAlignment.Center;
                        format.LineAlignment = StringAlignment.Center;
                        e.Graphics.DrawString(Months[j / 2],
                            this.dgvReport.ColumnHeadersDefaultCellStyle.Font,
                            new SolidBrush(this.dgvReport.ColumnHeadersDefaultCellStyle.ForeColor),
                            r1,
                            format);
                        j += 2;
                    }
                }
            }
            catch { }
        }

        void dgvReport_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1 && e.ColumnIndex > -1)
                {
                    Rectangle r2 = e.CellBounds;
                    r2.Y += e.CellBounds.Height / 2;
                    r2.Height = e.CellBounds.Height / 2;

                    e.PaintBackground(r2, true);

                    e.PaintContent(r2);
                    e.Handled = true;
                }
            }
            catch { }
        }
    }
}