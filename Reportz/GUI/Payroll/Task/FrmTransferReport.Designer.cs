﻿namespace MyPayfriend
{
    partial class FrmTransferReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTransferReport));
            this.exPanelTransfer = new DevComponents.DotNetBar.ExpandablePanel();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.cboEmployee = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblEmployee = new DevComponents.DotNetBar.LabelX();
            this.rptViewerTransfer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.exPanelTransfer.SuspendLayout();
            this.SuspendLayout();
            // 
            // exPanelTransfer
            // 
            this.exPanelTransfer.CanvasColor = System.Drawing.SystemColors.Control;
            this.exPanelTransfer.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.exPanelTransfer.Controls.Add(this.dtpToDate);
            this.exPanelTransfer.Controls.Add(this.dtpFromDate);
            this.exPanelTransfer.Controls.Add(this.lblToDate);
            this.exPanelTransfer.Controls.Add(this.lblFromDate);
            this.exPanelTransfer.Controls.Add(this.BtnShow);
            this.exPanelTransfer.Controls.Add(this.cboEmployee);
            this.exPanelTransfer.Controls.Add(this.lblEmployee);
            this.exPanelTransfer.Dock = System.Windows.Forms.DockStyle.Top;
            this.exPanelTransfer.ExpandButtonVisible = false;
            this.exPanelTransfer.Location = new System.Drawing.Point(0, 0);
            this.exPanelTransfer.Name = "exPanelTransfer";
            this.exPanelTransfer.Size = new System.Drawing.Size(1235, 100);
            this.exPanelTransfer.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.exPanelTransfer.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.exPanelTransfer.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.exPanelTransfer.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.exPanelTransfer.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.exPanelTransfer.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.exPanelTransfer.Style.GradientAngle = 90;
            this.exPanelTransfer.TabIndex = 7;
            this.exPanelTransfer.ThemeAware = true;
            this.exPanelTransfer.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.exPanelTransfer.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.exPanelTransfer.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.exPanelTransfer.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.exPanelTransfer.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.exPanelTransfer.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.exPanelTransfer.TitleStyle.GradientAngle = 90;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(391, 68);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(100, 20);
            this.dtpToDate.TabIndex = 32;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(391, 38);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(100, 20);
            this.dtpFromDate.TabIndex = 31;
            // 
            // lblToDate
            // 
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(331, 68);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(40, 15);
            this.lblToDate.TabIndex = 29;
            this.lblToDate.Text = "To ";
            // 
            // lblFromDate
            // 
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(331, 40);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(35, 15);
            this.lblFromDate.TabIndex = 28;
            this.lblFromDate.Text = "From ";
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(526, 41);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(64, 42);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 132;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.DisplayMember = "Text";
            this.cboEmployee.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.ItemHeight = 14;
            this.cboEmployee.Location = new System.Drawing.Point(83, 40);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(212, 20);
            this.cboEmployee.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboEmployee.TabIndex = 8;
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbo_Keydown);
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployee.BackgroundStyle.Class = "";
            this.lblEmployee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployee.Location = new System.Drawing.Point(17, 40);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(51, 15);
            this.lblEmployee.TabIndex = 6;
            this.lblEmployee.Text = "Employee";
            // 
            // rptViewerTransfer
            // 
            this.rptViewerTransfer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptViewerTransfer.Location = new System.Drawing.Point(0, 100);
            this.rptViewerTransfer.Name = "rptViewerTransfer";
            this.rptViewerTransfer.Size = new System.Drawing.Size(1235, 278);
            this.rptViewerTransfer.TabIndex = 8;
            // 
            // FrmTransferReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1235, 378);
            this.Controls.Add(this.rptViewerTransfer);
            this.Controls.Add(this.exPanelTransfer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmTransferReport";
            this.Text = "Transfer Report";
            this.Load += new System.EventHandler(this.FrmTransferReport_Load);
            this.exPanelTransfer.ResumeLayout(false);
            this.exPanelTransfer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel exPanelTransfer;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private DevComponents.DotNetBar.LabelX lblToDate;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboEmployee;
        private DevComponents.DotNetBar.LabelX lblEmployee;
        private Microsoft.Reporting.WinForms.ReportViewer rptViewerTransfer;
    }
}