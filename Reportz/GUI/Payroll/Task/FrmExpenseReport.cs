﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{
    /*****************************************************
   * Created By       : HIMA
   * Creation Date    : 22 AUG 2013
   * Description      : Handle EXPENSE Reports 
   * FormID           : 
   * ******************************************************/
    public partial class FrmExpenseReport : Report
    {
        #region Properties
        private string strMReportPath = "";
        private clsMessage ObjUserMessage = null;
        #endregion

        #region Constructor
        public FrmExpenseReport()
        {
            InitializeComponent();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }

        }
        #endregion
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.ExpenseMISReport, this);
        }
        #endregion SetArabicControls

        #region Message
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.ExpenseMISReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Functions
        #region EnableDisableIncludeCompany
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }
        #endregion
        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

        }
        private void FillAllDeparments()
        {
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
        }
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }
        private void FillAllEmployees()
        {
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                            chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), -1, -1, cboWorkStatus.SelectedValue.ToInt32(), -1
                                            );
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
        }
        private void FillAllExpenseTypes()
        {
            cboExpenseType.DataSource = clsBLLReportCommon.GetAllExpenseTypes();
            cboExpenseType.DisplayMember = "Description";
            cboExpenseType.ValueMember = "ExpenseHeadID";
        }
        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }
        private void LoadCombos()
        {
            FillAllCompany();
            FillAllDeparments();
            FillAllWorkStatus();
            FillAllExpenseTypes();
        }
        #region Validations
        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(9100);
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9109);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                return false;
            }
            if (cboExpenseType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9102);
                return false;
            }
            if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
            {
                UserMessage.ShowMessage(1761);
                return false;
            }
            return true;
        }
        #endregion
        private void ShowExpenseReport()
        {
            if (ClsCommonSettings.IsArabicView)
            {
                strMReportPath = cboEmployee.SelectedValue.ToInt32() == -1 ? Application.StartupPath + "\\MainReports\\RptExpenseSummaryArb.rdlc" :
                                                                                  Application.StartupPath + "\\MainReports\\RptExpenseArb.rdlc";

            }
            else
            {
                strMReportPath = cboEmployee.SelectedValue.ToInt32() == -1 ? Application.StartupPath + "\\MainReports\\RptExpenseSummary.rdlc" :
                                                                                  Application.StartupPath + "\\MainReports\\RptExpense.rdlc";

            }
            DataTable dtExpense = null;

            //Set ReportPath
            this.rptViewerExpense.LocalReport.ReportPath = strMReportPath;
            //Set Report Header
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            //Expense Report
            dtExpense = clsBLLexpenseReport.GetExpenseDetails(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), chkIncludeCompany.Checked, dtpFromDate.Value, dtpToDate.Value, cboExpenseType.SelectedValue.ToInt32());



            if (dtExpense != null)
            {
                if (dtExpense.Rows.Count > 0)
                {
                    rptViewerExpense.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                                 new ReportParameter("Company", cboCompany.Text, false),
                                 new ReportParameter("Branch", cboBranch.Text, false),
                                 new ReportParameter("Department", cboDepartment.Text, false),
                                 new ReportParameter("WorkStatus", cboWorkStatus.Text, false),
                                 new ReportParameter("Employee",cboEmployee.Text, false),
                                 new ReportParameter("Type",cboExpenseType.Text, false),
                                 new ReportParameter("FromDate",dtpFromDate.Value.ToString("dd MMM yyy"), false),
                                 new ReportParameter("ToDate",dtpToDate.Value.ToString("dd MMM yyy"), false)
                               });

                    this.rptViewerExpense.LocalReport.DataSources.Add(new ReportDataSource("dtSetExpense_dtExpenseAll", dtExpense));
                    this.rptViewerExpense.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                }
                else
                {
                    UserMessage.ShowMessage(9700);//NO INFORMATION FOUND 
                    return;
                }
            }
            else
            {
                UserMessage.ShowMessage(9700);//NO INFORMATION FOUND 
                return;
            }





            SetReportDisplay(rptViewerExpense);
        }
        #endregion

        #region FormLoad
        private void FrmExpenseReport_Load(object sender, EventArgs e)
        {
            LoadCombos();
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
            cboCompany_SelectedIndexChanged(null, null);




        }
        #endregion

        #region Control KeydownEvents
        private void cbo_Keydown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = sender as ComboBox;
            cbo.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rptViewerExpense_RenderingBegin(object sender, CancelEventArgs e)
        {
            BtnShow.Enabled = false;
        }

        private void rptViewerExpense_RenderingComplete(object sender, Microsoft.Reporting.WinForms.RenderingCompleteEventArgs e)
        {
            BtnShow.Enabled = true;
        }
        #endregion

        #region ControlEvents
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllBranches();
        }
        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }
        private void BtnShow_Click(object sender, EventArgs e)
        {
            this.rptViewerExpense.Reset();
            try
            {

                if (FormValidations())
                    ShowExpenseReport();
            }
            catch (Exception Ex)
            {
                BtnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;
        }

        #endregion
    }
}
