﻿namespace MyPayfriend
{
    partial class FrmRptCompanyAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptCompanyAsset));
            this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
            this.cboWorkStatus = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblWorkStatus = new DevComponents.DotNetBar.LabelX();
            this.cboType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.lblType = new DevComponents.DotNetBar.LabelX();
            this.chkIncludeCompany = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cboBranch = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblBranch = new DevComponents.DotNetBar.LabelX();
            this.BtnShow = new DevComponents.DotNetBar.ButtonX();
            this.cboStatusType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboAssets = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblAssets = new DevComponents.DotNetBar.LabelX();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblToDate = new DevComponents.DotNetBar.LabelX();
            this.lblFromDate = new DevComponents.DotNetBar.LabelX();
            this.cboEmployee = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cboAssetType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblEmployee = new DevComponents.DotNetBar.LabelX();
            this.lblAssetType = new DevComponents.DotNetBar.LabelX();
            this.cboCompany = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblCompany = new DevComponents.DotNetBar.LabelX();
            this.lblStatus = new DevComponents.DotNetBar.LabelX();
            this.rptCompanyAsset = new Microsoft.Reporting.WinForms.ReportViewer();
            this.expandablePanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // expandablePanel1
            // 
            this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.expandablePanel1.Controls.Add(this.cboWorkStatus);
            this.expandablePanel1.Controls.Add(this.lblWorkStatus);
            this.expandablePanel1.Controls.Add(this.cboType);
            this.expandablePanel1.Controls.Add(this.lblType);
            this.expandablePanel1.Controls.Add(this.chkIncludeCompany);
            this.expandablePanel1.Controls.Add(this.cboBranch);
            this.expandablePanel1.Controls.Add(this.lblBranch);
            this.expandablePanel1.Controls.Add(this.BtnShow);
            this.expandablePanel1.Controls.Add(this.cboStatusType);
            this.expandablePanel1.Controls.Add(this.cboAssets);
            this.expandablePanel1.Controls.Add(this.lblAssets);
            this.expandablePanel1.Controls.Add(this.dtpToDate);
            this.expandablePanel1.Controls.Add(this.dtpFromDate);
            this.expandablePanel1.Controls.Add(this.lblToDate);
            this.expandablePanel1.Controls.Add(this.lblFromDate);
            this.expandablePanel1.Controls.Add(this.cboEmployee);
            this.expandablePanel1.Controls.Add(this.cboAssetType);
            this.expandablePanel1.Controls.Add(this.lblEmployee);
            this.expandablePanel1.Controls.Add(this.lblAssetType);
            this.expandablePanel1.Controls.Add(this.cboCompany);
            this.expandablePanel1.Controls.Add(this.lblCompany);
            this.expandablePanel1.Controls.Add(this.lblStatus);
            this.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.expandablePanel1.Location = new System.Drawing.Point(0, 0);
            this.expandablePanel1.Name = "expandablePanel1";
            this.expandablePanel1.Size = new System.Drawing.Size(1241, 107);
            this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandablePanel1.Style.GradientAngle = 90;
            this.expandablePanel1.TabIndex = 2;
            this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
            this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
            this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.expandablePanel1.TitleStyle.GradientAngle = 90;
            this.expandablePanel1.TitleText = "Filter By";
            // 
            // cboWorkStatus
            // 
            this.cboWorkStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboWorkStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboWorkStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboWorkStatus.DropDownHeight = 134;
            this.cboWorkStatus.FormattingEnabled = true;
            this.cboWorkStatus.IntegralHeight = false;
            this.cboWorkStatus.ItemHeight = 14;
            this.cboWorkStatus.Location = new System.Drawing.Point(307, 63);
            this.cboWorkStatus.Name = "cboWorkStatus";
            this.cboWorkStatus.Size = new System.Drawing.Size(149, 20);
            this.cboWorkStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboWorkStatus.TabIndex = 154;
            this.cboWorkStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCombo_KeyDown);
            // 
            // lblWorkStatus
            // 
            this.lblWorkStatus.AutoSize = true;
            // 
            // 
            // 
            this.lblWorkStatus.BackgroundStyle.Class = "";
            this.lblWorkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWorkStatus.Location = new System.Drawing.Point(237, 66);
            this.lblWorkStatus.Name = "lblWorkStatus";
            this.lblWorkStatus.Size = new System.Drawing.Size(62, 15);
            this.lblWorkStatus.TabIndex = 153;
            this.lblWorkStatus.Text = "Work Status";
            // 
            // cboType
            // 
            this.cboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboType.DisplayMember = "Text";
            this.cboType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboType.DropDownHeight = 134;
            this.cboType.FormattingEnabled = true;
            this.cboType.IntegralHeight = false;
            this.cboType.ItemHeight = 14;
            this.cboType.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.cboType.Location = new System.Drawing.Point(307, 36);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(149, 20);
            this.cboType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboType.TabIndex = 152;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            this.cboType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCombo_KeyDown);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "Company";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Employee";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            // 
            // 
            // 
            this.lblType.BackgroundStyle.Class = "";
            this.lblType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblType.Location = new System.Drawing.Point(237, 37);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(27, 15);
            this.lblType.TabIndex = 151;
            this.lblType.Text = "Type";
            // 
            // chkIncludeCompany
            // 
            this.chkIncludeCompany.AutoSize = true;
            // 
            // 
            // 
            this.chkIncludeCompany.BackgroundStyle.Class = "";
            this.chkIncludeCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkIncludeCompany.Location = new System.Drawing.Point(64, 86);
            this.chkIncludeCompany.Name = "chkIncludeCompany";
            this.chkIncludeCompany.Size = new System.Drawing.Size(108, 15);
            this.chkIncludeCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkIncludeCompany.TabIndex = 134;
            this.chkIncludeCompany.Text = "Include Company";
            this.chkIncludeCompany.CheckedChanged += new System.EventHandler(this.chkIncludeCompany_CheckedChanged);
            // 
            // cboBranch
            // 
            this.cboBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBranch.DisplayMember = "Text";
            this.cboBranch.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboBranch.DropDownHeight = 134;
            this.cboBranch.FormattingEnabled = true;
            this.cboBranch.IntegralHeight = false;
            this.cboBranch.ItemHeight = 14;
            this.cboBranch.Location = new System.Drawing.Point(68, 62);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(149, 20);
            this.cboBranch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboBranch.TabIndex = 133;
            this.cboBranch.SelectedIndexChanged += new System.EventHandler(this.cboBranch_SelectedIndexChanged);
            this.cboBranch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCombo_KeyDown);
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            // 
            // 
            // 
            this.lblBranch.BackgroundStyle.Class = "";
            this.lblBranch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblBranch.Location = new System.Drawing.Point(12, 63);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(37, 15);
            this.lblBranch.TabIndex = 132;
            this.lblBranch.Text = "Branch";
            // 
            // BtnShow
            // 
            this.BtnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.BtnShow.Location = new System.Drawing.Point(1169, 38);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(64, 42);
            this.BtnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BtnShow.TabIndex = 131;
            this.BtnShow.Text = "Show";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // cboStatusType
            // 
            this.cboStatusType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboStatusType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboStatusType.DisplayMember = "Text";
            this.cboStatusType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboStatusType.DropDownHeight = 134;
            this.cboStatusType.FormattingEnabled = true;
            this.cboStatusType.IntegralHeight = false;
            this.cboStatusType.ItemHeight = 14;
            this.cboStatusType.Location = new System.Drawing.Point(784, 64);
            this.cboStatusType.Name = "cboStatusType";
            this.cboStatusType.Size = new System.Drawing.Size(149, 20);
            this.cboStatusType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboStatusType.TabIndex = 130;
            this.cboStatusType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCombo_KeyDown);
            // 
            // cboAssets
            // 
            this.cboAssets.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAssets.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAssets.DisplayMember = "Text";
            this.cboAssets.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboAssets.DropDownHeight = 134;
            this.cboAssets.FormattingEnabled = true;
            this.cboAssets.IntegralHeight = false;
            this.cboAssets.ItemHeight = 14;
            this.cboAssets.Location = new System.Drawing.Point(784, 36);
            this.cboAssets.Name = "cboAssets";
            this.cboAssets.Size = new System.Drawing.Size(149, 20);
            this.cboAssets.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboAssets.TabIndex = 129;
            this.cboAssets.SelectedIndexChanged += new System.EventHandler(this.cboAssets_SelectedIndexChanged);
            this.cboAssets.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCombo_KeyDown);
            // 
            // lblAssets
            // 
            this.lblAssets.AutoSize = true;
            // 
            // 
            // 
            this.lblAssets.BackgroundStyle.Class = "";
            this.lblAssets.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAssets.Location = new System.Drawing.Point(710, 37);
            this.lblAssets.Name = "lblAssets";
            this.lblAssets.Size = new System.Drawing.Size(30, 15);
            this.lblAssets.TabIndex = 128;
            this.lblAssets.Text = "Asset";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd MMM yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(1028, 65);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(104, 20);
            this.dtpToDate.TabIndex = 13;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd MMM yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(1028, 34);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(104, 20);
            this.dtpFromDate.TabIndex = 12;
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            // 
            // 
            // 
            this.lblToDate.BackgroundStyle.Class = "";
            this.lblToDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblToDate.Location = new System.Drawing.Point(957, 67);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(41, 15);
            this.lblToDate.TabIndex = 11;
            this.lblToDate.Text = "To Date";
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            // 
            // 
            // 
            this.lblFromDate.BackgroundStyle.Class = "";
            this.lblFromDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblFromDate.Location = new System.Drawing.Point(957, 37);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(54, 15);
            this.lblFromDate.TabIndex = 9;
            this.lblFromDate.Text = "From Date";
            // 
            // cboEmployee
            // 
            this.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEmployee.DisplayMember = "Text";
            this.cboEmployee.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboEmployee.DropDownHeight = 134;
            this.cboEmployee.FormattingEnabled = true;
            this.cboEmployee.IntegralHeight = false;
            this.cboEmployee.ItemHeight = 14;
            this.cboEmployee.Location = new System.Drawing.Point(543, 36);
            this.cboEmployee.Name = "cboEmployee";
            this.cboEmployee.Size = new System.Drawing.Size(149, 20);
            this.cboEmployee.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboEmployee.TabIndex = 8;
            this.cboEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCombo_KeyDown);
            // 
            // cboAssetType
            // 
            this.cboAssetType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAssetType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAssetType.DisplayMember = "Text";
            this.cboAssetType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboAssetType.DropDownHeight = 134;
            this.cboAssetType.FormattingEnabled = true;
            this.cboAssetType.IntegralHeight = false;
            this.cboAssetType.ItemHeight = 14;
            this.cboAssetType.Location = new System.Drawing.Point(543, 65);
            this.cboAssetType.Name = "cboAssetType";
            this.cboAssetType.Size = new System.Drawing.Size(149, 20);
            this.cboAssetType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboAssetType.TabIndex = 7;
            this.cboAssetType.SelectedIndexChanged += new System.EventHandler(this.cboAssetType_SelectedIndexChanged);
            this.cboAssetType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCombo_KeyDown);
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            // 
            // 
            // 
            this.lblEmployee.BackgroundStyle.Class = "";
            this.lblEmployee.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblEmployee.Location = new System.Drawing.Point(475, 37);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(51, 15);
            this.lblEmployee.TabIndex = 6;
            this.lblEmployee.Text = "Employee";
            // 
            // lblAssetType
            // 
            this.lblAssetType.AutoSize = true;
            // 
            // 
            // 
            this.lblAssetType.BackgroundStyle.Class = "";
            this.lblAssetType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAssetType.Location = new System.Drawing.Point(475, 66);
            this.lblAssetType.Name = "lblAssetType";
            this.lblAssetType.Size = new System.Drawing.Size(54, 15);
            this.lblAssetType.TabIndex = 5;
            this.lblAssetType.Text = "AssetType";
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.DisplayMember = "Text";
            this.cboCompany.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboCompany.DropDownHeight = 134;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.IntegralHeight = false;
            this.cboCompany.ItemHeight = 14;
            this.cboCompany.Location = new System.Drawing.Point(68, 34);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(149, 20);
            this.cboCompany.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cboCompany.TabIndex = 4;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboCombo_KeyDown);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            // 
            // 
            // 
            this.lblCompany.BackgroundStyle.Class = "";
            this.lblCompany.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCompany.Location = new System.Drawing.Point(12, 36);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(50, 15);
            this.lblCompany.TabIndex = 2;
            this.lblCompany.Text = "Company";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            // 
            // 
            // 
            this.lblStatus.BackgroundStyle.Class = "";
            this.lblStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStatus.Location = new System.Drawing.Point(708, 67);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(64, 15);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "Asset Status";
            // 
            // rptCompanyAsset
            // 
            this.rptCompanyAsset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptCompanyAsset.Location = new System.Drawing.Point(0, 107);
            this.rptCompanyAsset.Name = "rptCompanyAsset";
            this.rptCompanyAsset.Size = new System.Drawing.Size(1241, 415);
            this.rptCompanyAsset.TabIndex = 3;
            this.rptCompanyAsset.RenderingComplete += new Microsoft.Reporting.WinForms.RenderingCompleteEventHandler(this.rptCompanyAsset_RenderingComplete);
            this.rptCompanyAsset.Load += new System.EventHandler(this.rptCompanyAsset_Load);
            this.rptCompanyAsset.RenderingBegin += new System.ComponentModel.CancelEventHandler(this.rptCompanyAsset_RenderingBegin);
            // 
            // FrmRptCompanyAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1241, 522);
            this.Controls.Add(this.rptCompanyAsset);
            this.Controls.Add(this.expandablePanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptCompanyAsset";
            this.Text = "FrmRptCompanyAsset";
            this.Load += new System.EventHandler(this.FrmRptCompanyAsset_Load);
            this.expandablePanel1.ResumeLayout(false);
            this.expandablePanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboAssets;
        private DevComponents.DotNetBar.LabelX lblAssets;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private DevComponents.DotNetBar.LabelX lblToDate;
        private DevComponents.DotNetBar.LabelX lblFromDate;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboEmployee;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboAssetType;
        private DevComponents.DotNetBar.LabelX lblEmployee;
        private DevComponents.DotNetBar.LabelX lblAssetType;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboCompany;
        private DevComponents.DotNetBar.LabelX lblCompany;
        private DevComponents.DotNetBar.LabelX lblStatus;
        private Microsoft.Reporting.WinForms.ReportViewer rptCompanyAsset;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboStatusType;
        private DevComponents.DotNetBar.ButtonX BtnShow;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboBranch;
        private DevComponents.DotNetBar.LabelX lblBranch;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkIncludeCompany;
        internal DevComponents.DotNetBar.Controls.ComboBoxEx cboWorkStatus;
        private DevComponents.DotNetBar.LabelX lblWorkStatus;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboType;
        private DevComponents.DotNetBar.LabelX lblType;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
    }
}