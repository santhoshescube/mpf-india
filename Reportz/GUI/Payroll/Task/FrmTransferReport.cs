﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{
    public partial class FrmTransferReport : Report
    {
        #region Properties
        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.AttendanceReport);
                return this.ObjUserMessage;
            }
        }
        #endregion
        #region Constructor
        public FrmTransferReport()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                lblEmployee.Text = "عامل";
                lblFromDate.Text = "من";
                lblToDate.Text = "إلى";
                BtnShow.Text = "عرض";
            }
            exPanelTransfer.TitleText = "";
        }
        #endregion


        #region Functions
        /// <summary>
        /// Load All Combos
        /// </summary>
        private void LoadCombos()
        {
            FillAllEmployees();
        }
        /// <summary>
        /// Load Company
        /// </summary>
     
        private void FillAllEmployees()
        {
            cboEmployee.DisplayMember = "EmployeeName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = clsBLLReportCommon.GetEmployeeForTransfer();
        }
   
    
        #endregion
      
        #region Control KeyDown Event
        private void cbo_Keydown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = sender as ComboBox;
            cbo.DroppedDown = false;
        }
        #endregion
        #region Validations
        private bool FormValidations()
        {
           
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                return false;
            }
          
            if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
            {
                UserMessage.ShowMessage(1761);
                return false;
            }
            return true;
        }
        #endregion
        #region SHOW
        private void ShowReport()
        {
           

            //Set Company Header For Report
            SetReportHeader(ClsCommonSettings.CurrentCompanyID,-1, true);

            //Clear Report Datasources
            this.rptViewerTransfer.LocalReport.DataSources.Clear();

            DataTable dtReport = clsBLLTransferReport.GetTransferDetails(cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value);
            if (ClsCommonSettings.IsArabicView)
            {
                this.rptViewerTransfer.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptTransferReportArb.rdlc";

            }
            else
            {
                this.rptViewerTransfer.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptTransferReport.rdlc";

            }

                rptViewerTransfer.LocalReport.SetParameters(new ReportParameter[]
                {
                    new ReportParameter("GeneratedBy",ClsCommonSettings.ReportFooter),
                   
                    new ReportParameter("Employee",cboEmployee.Text.Trim()),
                    
                });
                if (dtReport.Rows.Count > 0)
                {
                    this.rptViewerTransfer.LocalReport.DataSources.Add(new ReportDataSource("dtSetTransfer_dtTransfer", dtReport));
                    this.rptViewerTransfer.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                }
                else
                {
                    UserMessage.ShowMessage(9015);
                    return;
                }

                SetReportDisplay(rptViewerTransfer);
            
        }
        #endregion
        private void FrmTransferReport_Load(object sender, EventArgs e)
        {
            LoadCombos();
            
           
        }
        #region ReportViewerEvents
        private void rptViewerTransfer_RenderingBegin(object sender, CancelEventArgs e)
        {
            BtnShow.Enabled = false;
        }

        private void rptViewerTransfer_RenderingComplete(object sender, Microsoft.Reporting.WinForms.RenderingCompleteEventArgs e)
        {
            BtnShow.Enabled = true;
        }
        #endregion



        private void BtnShow_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                //Clear Report
                this.rptViewerTransfer.Reset();

                if (FormValidations())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                BtnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
            this.Cursor = Cursors.Default;
        }
    }
}
