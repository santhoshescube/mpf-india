﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{

    /* 
=================================================
Author     : Ranju Mathew
Create date:  23 July 2013>
Description: CompanyAssets Operations Report
   
Modified By      : HIMA
Modified Date    : 22 AUG 2013
  
  
================================================
*/

    public partial class FrmRptCompanyAsset :Report
    {
        #region Properties
        private string strMReportPath = "";
        private clsMessage ObjUserMessage = null;
        DataTable dtCompanyAsset = null;
        DataSet datTemp = null;
        #endregion

        #region Constructor
        public FrmRptCompanyAsset()
        {
            InitializeComponent();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }


        }
        #endregion
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.CompanyAssetReport, this);
        }
        #endregion SetArabicControls


        #region NotificationMessages
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.CompanyAssetReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Functions
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }
        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

        }
       
        /// <summary>
        /// Load WorkStatus 
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
            cboWorkStatus.SelectedValue = (int)EmployeeWorkStatus.InService;
        }
        private void FillAllEmployees()
        {
            //cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
            //                                chkIncludeCompany.Checked, -1, -1, -1, cboWorkStatus.SelectedValue.ToInt32(), -1
            //                                );
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployeesInCurrentCompany(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                            chkIncludeCompany.Checked, -1, -1, -1, cboWorkStatus.SelectedValue.ToInt32()
                                            );
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
        }
        private void FillAssetTypes()
        {
            cboAssetType.DisplayMember = "BenefitTypeName";
            cboAssetType.ValueMember = "BenefitTypeID";
            cboAssetType.DataSource = clsBLLReportCommon.GetAllAssetTypes();
        }
        private void FillAssets()
        {
            cboAssets.DisplayMember = "Description";
            cboAssets.ValueMember = "CompanyAssetID";
            cboAssets.DataSource = clsBLLReportCommon.GetAllAssets(cboAssetType.SelectedValue.ToInt32(),cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),chkIncludeCompany.Checked);
        }
        private void FillAssetStatus()
        {
  
                cboStatusType.DisplayMember = "AssetStatus";
                cboStatusType.ValueMember = "AssetStatusID";
                cboStatusType.DataSource = clsBLLReportCommon.GetAllAssetStatus(cboType.SelectedIndex);
      
        }
        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }
        private void LoadCombos()
        {
            FillAllCompany();
            FillAllWorkStatus();
            FillAssetTypes();
            FillAssetStatus();

        }
        #region Validation
        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(9100);
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                return false;
            }
            if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9102);
                return false;
            }

            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                return false;
            }
            if (cboAssetType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(17220);
                return false;
            }
            if (cboAssets.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(17221);
                return false;
            }
            if (cboStatusType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9103);
                return false;
            }
            return true;
        }
        #endregion
        private void ShowAssetReport()
        {
            

            if (cboCompany.SelectedIndex >= 0)
            {

                DateTime FromDate = Convert.ToDateTime(dtpFromDate.Value.ToShortDateString());
                DateTime ToDate = Convert.ToDateTime(dtpToDate.Value.ToShortDateString());

                if (cboType.SelectedIndex == 0)
                {
                    if (ClsCommonSettings.IsArabicView)
                    {
                        strMReportPath = Application.StartupPath + "\\MainReports\\RptCompanyAssetDetailsArb.rdlc";

                    }
                    else
                    {
                        strMReportPath = Application.StartupPath + "\\MainReports\\RptCompanyAssetDetails.rdlc";

                    }

                    dtCompanyAsset = clsBLLRptCompanyAsset.GetAllCompanyAssets(cboAssetType.SelectedValue.ToInt32(), cboAssets.SelectedValue.ToInt32(), cboStatusType.SelectedValue.ToInt32(), FromDate, ToDate, cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
                }
                else
                {
                    if (ClsCommonSettings.IsArabicView)
                    {
                        if ((cboEmployee.SelectedValue.ToInt32() != -1) && (cboAssets.SelectedValue.ToInt32() == -1))
                            strMReportPath = Application.StartupPath + "\\MainReports\\RptAssetHistorySingleEmployArb.rdlc";
                        else if ((cboEmployee.SelectedValue.ToInt32() == -1) && (cboAssets.SelectedValue.ToInt32() == -1))
                            strMReportPath = Application.StartupPath + "\\MainReports\\RptCompanyAssetHandOverArb.rdlc";
                        else if (cboAssets.SelectedValue.ToInt32() != -1)
                            strMReportPath = Application.StartupPath + "\\MainReports\\RptCompanyAssetRetailsArb.rdlc";
                    }
                    else
                    {
                        if ((cboEmployee.SelectedValue.ToInt32() != -1) && (cboAssets.SelectedValue.ToInt32() == -1))
                            strMReportPath = Application.StartupPath + "\\MainReports\\RptAssetHistorySingleEmploy.rdlc";
                        else if ((cboEmployee.SelectedValue.ToInt32() == -1) && (cboAssets.SelectedValue.ToInt32() == -1))
                            strMReportPath = Application.StartupPath + "\\MainReports\\RptCompanyAssetHandOver.rdlc";
                        else if (cboAssets.SelectedValue.ToInt32() != -1)
                            strMReportPath = Application.StartupPath + "\\MainReports\\RptCompanyAssetRetails.rdlc";
                    }

               

                    //strMReportPath = cboAssets.SelectedValue.ToInt32() == -1 ? Application.StartupPath + "\\MainReports\\RptCompanyAssetHandOver.rdlc" :
                    //                                                        Application.StartupPath + "\\MainReports\\RptCompanyAssetRetails.rdlc";
                     datTemp = clsBLLRptCompanyAsset.DisplayCompanyAssetReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboType.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), cboAssetType.SelectedValue.ToInt32(), cboAssets.SelectedValue.ToInt32(), cboStatusType.SelectedValue.ToInt32(), FromDate, ToDate, chkIncludeCompany.Checked);
                }
                rptCompanyAsset.LocalReport.ReportPath = strMReportPath;

               
               
                rptCompanyAsset.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                                 new ReportParameter("Company", cboCompany.Text, false),
                                 new ReportParameter("Branch", cboBranch.Text, false),
                                 new ReportParameter("Department", cboType.Text, false),
                                 new ReportParameter("Workstatus", cboWorkStatus.Text, false),
                                 new ReportParameter("Employee", cboEmployee.Text, false),
                                 new ReportParameter("AssetType",cboAssetType.Text, false),
                                 new ReportParameter("CompanyAsset",cboAssets.Text, false),
                                 new ReportParameter("Status",cboStatusType.Text, false),
                                 new ReportParameter("FromDate",dtpFromDate.Value.ToString("dd MMM yyyy"), false),
                                 new ReportParameter("ToDate",dtpToDate.Value.ToString("dd MMM yyyy"), false)
                               });
                
                SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
                rptCompanyAsset.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                if (cboType.SelectedIndex == 0)
                {
                    if (dtCompanyAsset.Rows.Count > 0)
                    {
                        rptCompanyAsset.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_CompanyAssetInfo", dtCompanyAsset));
                    }
                    else
                    {
                        UserMessage.ShowMessage(9015);//NO INFORMATION FOUND 
                        return;
                    }

                }
                else
                {
                   if (datTemp.Tables[0].Rows.Count > 0)
                   {
                        if (cboAssets.SelectedValue.ToInt32() > 0)
                        {
                            rptCompanyAsset.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                            rptCompanyAsset.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                            rptCompanyAsset.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_CompanyAssetDetails", datTemp.Tables[0]));
                            rptCompanyAsset.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_CompanyAssetInfo", datTemp.Tables[1]));
                            rptCompanyAsset.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_AssetUsageDamageInfo", datTemp.Tables[2]));
                        }
                        else
                        {
                            rptCompanyAsset.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_CompanyAsset", datTemp.Tables[0]));
                        }
                   }
                   else
                   {
                       UserMessage.ShowMessage(9015);//NO INFORMATION FOUND 
                       return;
                   }

                }
                SetReportDisplay(rptCompanyAsset);
            }
        }
        #endregion

        /// <summary>
        /// Sub report event
        /// </summary>
        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_AssetUsageDamageInfo", datTemp.Tables[2]));
        }
        private void rptCompanyAsset_Load(object sender, EventArgs e)
        {
            
        }
     
      
       

        #region ControlKeyDownEvent
        private void cboCombo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            cbo.DroppedDown = false;
        }
        #endregion

        #region ControlEvents
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllBranches();

        }
        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
            FillAssets();
        }
        private void cboAssets_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cboAssets.SelectedValue.ToInt32() > 0)
            //{

            //    //cboStatusType.Enabled = false;
            //    //lblStatus.Enabled = false;
            //    cboStatusType.Visible = false;
            //    lblStatus.Visible = false;
            //}
            //else
            //{
            //    //cboStatusType.Enabled = true;
            //    //lblStatus.Enabled = true;
            //    cboStatusType.Visible = true;
            //    lblStatus.Visible = true;
            //    FillAssetStatus();
            //}
        }
        private void cboAssetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAssets();
        }
        private void BtnShow_Click(object sender, EventArgs e)
        {
            rptCompanyAsset.Reset();
            try
            {
               
                if (FormValidations())
                    ShowAssetReport();
            }
            catch (Exception Ex)
            {
                BtnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region ReportViewerEvents
        private void rptCompanyAsset_RenderingBegin(object sender, CancelEventArgs e)
        {
            BtnShow.Enabled = false;
        }

        private void rptCompanyAsset_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            BtnShow.Enabled = true;
        }
        #endregion

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAssetStatus();
            if (cboType.SelectedIndex == 0)
            {
                cboWorkStatus.Enabled = false;
                cboEmployee.Enabled = false;
            }
            else
            {
                cboWorkStatus.Enabled = true;
                cboEmployee.Enabled = true;
            }
        }
         #region FormLoad
        private void FrmRptCompanyAsset_Load(object sender, EventArgs e)
        {
            LoadCombos();
            SetDefaultBranch(cboBranch);
            chkIncludeCompany.Checked = true;
            cboType.SelectedIndex = 0;
            cboCompany_SelectedIndexChanged(null, null);
        }
         #endregion

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
            FillAssets();
            //cboCompany_SelectedIndexChanged(null, null);
        }
    }
}
