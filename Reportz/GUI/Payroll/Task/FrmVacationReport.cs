﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{
    /*****************************************************
  * Created By       : HIMA
  * Creation Date    : 20 AUG 2013
  * Description      : Handle vacation Reports 
  * ******************************************************/
    public partial class FrmVacationReport : Report
    {
        #region Properties
        private string strMReportPath = "";
        private clsMessage ObjUserMessage = null;
        int SearchType = 0;
        ClsExportDatagridviewToExcel MobjExportToExcel;
        #endregion

        #region Constructor
        public FrmVacationReport()
        {
            InitializeComponent();
            MobjExportToExcel = new ClsExportDatagridviewToExcel();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }

        }
        #endregion
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.VacationReport, this);
        }
        #endregion SetArabicControls

        #region Message
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.VacationReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Functions
        private void LoadCombos()
        {
            FillAllCompany();
            FillAllDeparments();
            FillAllWorkStatus();
        }
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }
        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

        }
        private void FillAllDeparments()
        {
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
        }
    
        /// <summary>
        /// Load WorkStatus 
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }
        private void FillAllEmployees()
        {
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                            chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(),-1,-1,cboWorkStatus.SelectedValue.ToInt32(),-1
                                            );
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
        }

        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }
        #region Validations
        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(9100);
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9109);
                return false;
            }
           
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                return false;
            }
            if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(17225);
                return false;
            }
            if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
            {
                UserMessage.ShowMessage(1761);
                return false;
            }
            return true;
        }
        #endregion
        #region SHOW
        private void ShowvacationReport()
        {
            
           
            //Search Type:1-Vacation Info,SearchType = 2-vacation rejoining
            if (cboType.SelectedIndex == 0)
            {
                SearchType = 1;
                if (ClsCommonSettings.IsArabicView)
                {
                    strMReportPath = cboEmployee.SelectedValue.ToInt32() == -1 ? Application.StartupPath + "\\MainReports\\RptVacationSummaryArb.rdlc" :
                                                                                           Application.StartupPath + "\\MainReports\\RptVacationDetailArb.rdlc";

                }
                else
                {
                    strMReportPath = cboEmployee.SelectedValue.ToInt32() == -1 ? Application.StartupPath + "\\MainReports\\RptVacationSummary.rdlc" :
                                                                                           Application.StartupPath + "\\MainReports\\RptVacationDetail.rdlc";

                }

            }
            else if (cboType.SelectedIndex == 1)
            {
                SearchType = 2;
                if (ClsCommonSettings.IsArabicView)
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptVacationRejoiningSummaryArb.rdlc";
                }
                else
                {
                    strMReportPath = Application.StartupPath + "\\MainReports\\RptVacationRejoiningSummary.rdlc";
                }



            }
            DataTable dtVacation = null;
            DataTable dtVacationDetails = null;
            
            //Set Reportpath
            this.rptViewerVacation.LocalReport.ReportPath = strMReportPath;
            //Company Header
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            //Summary report
            dtVacation = clsBLLvacationReport.Getvacation(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(),cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), chkIncludeCompany.Checked, dtpFromDate.Value, dtpToDate.Value, SearchType);
            //Detailed Report for a specific employee
            if ((cboEmployee.SelectedValue.ToInt32() > 0) & SearchType == 1)
                dtVacationDetails = clsBLLvacationReport.GetvacationDetails(cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value);
            if (dtVacation != null)
            {
                if (dtVacation.Rows.Count > 0)
                {
                    rptViewerVacation.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                                 new ReportParameter("Company", cboCompany.Text, false),
                                 new ReportParameter("Branch", cboBranch.Text, false),
                                 new ReportParameter("Department", cboDepartment.Text, false),
                                 new ReportParameter("Workstatus", cboWorkStatus.Text, false),
                                 new ReportParameter("Employee",cboEmployee.Text, false),
                                 new ReportParameter("Type",cboType.Text, false),
                                 new ReportParameter("FromDate",dtpFromDate.Value.ToString("dd MMM yyyy"), false),
                                 new ReportParameter("ToDate",dtpToDate.Value.ToString("dd MMM yyyy"), false)
                               });

                    this.rptViewerVacation.LocalReport.DataSources.Add(new ReportDataSource("dtSetVacationReport_dtVacationSummary", dtVacation));
                    if (cboEmployee.SelectedValue.ToInt32() > 0)
                    {
                        this.rptViewerVacation.LocalReport.DataSources.Add(new ReportDataSource("dtSetVacationReport_dtVacationDetails", dtVacationDetails));
                    }
                    this.rptViewerVacation.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                }
                else
                {
                    UserMessage.ShowMessage(17226);//NO INFORMATION FOUND 
                    return;
                }
            }
         
            SetReportDisplay(rptViewerVacation);
        }
        #endregion
        #endregion

        #region FormLoad
        private void FrmVacationReport_Load(object sender, EventArgs e)
        {
            LoadCombos();
            cboType.SelectedIndex = 0;
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
            cboCompany_SelectedIndexChanged(null, null);
            cboReportFormat.SelectedIndex = 1;
        }
        #endregion

        #region Control KeyDown Event
        private void cbo_Keydown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = sender as ComboBox;
            cbo.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rptViewerVacation_RenderingBegin(object sender, CancelEventArgs e)
        {
            BtnShow.Enabled = false;
        }
        private void rptViewerVacation_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            BtnShow.Enabled = true;
        }
        #endregion

        #region ControlEvents
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllBranches();
        }
        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }
      
        private void BtnShow_Click(object sender, EventArgs e)
        {
            this.rptViewerVacation.Reset();
            try
            {
                if (cboType.SelectedIndex == 0 || cboType.SelectedIndex == 1)
                {
                    cboReportFormat.SelectedIndex = 0; dgvReport.DataSource = null;
                    Application.DoEvents();
                }
                else
                {
                    cboReportFormat.SelectedIndex = 1;
                }
                if (FormValidations())
                {
                    if (cboType.SelectedIndex == 0 || cboType.SelectedIndex == 1)
                        ShowvacationReport();
                    else
                    {
                        dgvReport.DataSource = null;
                        dgvReport.DataSource = clsBLLvacationReport.GetvacationDetailsForGrid(cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value, cboWorkStatus.SelectedValue.ToInt32(), cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(),chkIncludeCompany.Checked);
                    }
                }
            }
            catch (Exception Ex)
            {
                BtnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
            this.Cursor = Cursors.Default;
        }
        #endregion

        private void cboReportFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReportFormat.SelectedIndex == 0)
            {
                dgvReport.Visible = btnExcelExport.Visible = false;
                rptViewerVacation.Visible = true;

            }
            else
            {
                dgvReport.Visible = btnExcelExport.Visible = true;
                rptViewerVacation.Visible = false;
            }
        }

        private void btnExcelExport_Click(object sender, EventArgs e)
        {
            string sFileName = "";

            if (dgvReport.RowCount <= 0)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";

                if (sfd.ShowDialog() == DialogResult.OK)
                    sFileName = sfd.FileName;
                else
                    return;
            }

            try
            {
                if (sFileName != "")
                {
                    if (dgvReport.RowCount > 0)
                        MobjExportToExcel.ExportToExcel(dgvReport, sFileName, 0, dgvReport.Columns.Count - 1, true);
                }
                else
                    MessageBox.Show("Please Enter Name");
            }
            catch { }
        }

       
    }
}
