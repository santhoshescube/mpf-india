﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
namespace MyPayfriend
{
    /*****************************************************
* Created By       : HIMA
* Creation Date    : 24 AUG 2013
* Description      : Settlement Reports 
* ******************************************************/
    public partial class FrmSettlementReport : Report
    {
        #region Properties
        private string strMReportPath = "";
        private clsMessage ObjUserMessage = null;
        #endregion

        #region Constructor
        public FrmSettlementReport()
        {
            InitializeComponent();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }

        }
        #endregion

        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.SettlementReport, this);
        }
        #endregion SetArabicControls


        #region NotificationMessage
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.VacationReport);
                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Functions
        private void LoadCombos()
        {
            FillAllCompany();
            FillAllDeparments();
            FillAllDesignation();
            FillAllSettlementTypes();
        }
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }
        private void FillAllCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

        }
        private void FillAllDeparments()
        {
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
        }
        private void FillAllDesignation()
        {
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation();
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
        }
        private void FillAllEmployees()
        {
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                            chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32()
                                            );
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
        }
        private void FillAllSettlementTypes()
        {
            cboType.DataSource = clsBLLReportCommon.GetAllSettlementTypes();
            cboType.DisplayMember = "WorkStatus";
            cboType.ValueMember = "WorkStatusID";
        }
        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }
        #region Validations
        private bool FormValidations()
        {
            if (cboCompany.SelectedValue.ToInt32() <= 0)
            {
                UserMessage.ShowMessage(9100);
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9141);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9109);
                return false;
            }
            if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9112);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9124);
                return false;
            }
            if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9102);
                return false;
            }
            if (dtpFromDate.Value.Date > dtpToDate.Value.Date)
            {
                UserMessage.ShowMessage(1761);
                return false;
            }
            return true;
        }
        #endregion
        private void ShowSettlementReport()
        {
            if (ClsCommonSettings.IsArabicView)
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\RptSettlementArb.rdlc";

            }
            else
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\RptSettlement.rdlc";

            }

            DataTable dtsettlement = null;
            DataTable dtsettlementDetails = new DataTable();

            //Setting ReportPath
            this.rptViewerSettlement.LocalReport.ReportPath = strMReportPath;
            //Company Header
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            //Main Report
            dtsettlement = clsBLLsettlementReport.GetSettlement(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), chkIncludeCompany.Checked, dtpFromDate.Value, dtpToDate.Value, cboType.SelectedValue.ToInt32());
            //Detailed Report for a specific employee
            if (cboEmployee.SelectedValue.ToInt32() > 0)
                 dtsettlementDetails = clsBLLsettlementReport.GetSettlementDetails(cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value, cboType.SelectedValue.ToInt32());
           
            if (dtsettlement != null)
            {
                if (dtsettlement.Rows.Count > 0)
                {
                    rptViewerSettlement.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                                 new ReportParameter("Company", cboCompany.Text, false),
                                 new ReportParameter("Branch", cboBranch.Text, false),
                                 new ReportParameter("Department", cboDepartment.Text, false),
                                 new ReportParameter("Designation", cboDesignation.Text, false),
                                 new ReportParameter("Employee",cboEmployee.Text, false),
                                 new ReportParameter("Type",cboType.Text, false),
                                 new ReportParameter("FromDate",dtpFromDate.Value.ToString("dd MMM yyy"), false),
                                 new ReportParameter("ToDate",dtpToDate.Value.ToString("dd MMM yyy"), false)
                              
                               });

                    this.rptViewerSettlement.LocalReport.DataSources.Add(new ReportDataSource("dtSetSettlement_Settlement", dtsettlement));
                    this.rptViewerSettlement.LocalReport.DataSources.Add(new ReportDataSource("dtSetSettlement_dtSettlementDetails", dtsettlementDetails));
                    this.rptViewerSettlement.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                }
                else
                {
                    UserMessage.ShowMessage(17226);//NO INFORMATION FOUND 
                    return;
                }
            }

            SetReportDisplay(rptViewerSettlement);
        }
        #endregion

        #region Control KeyDown Event
        private void cbo_Keydown(object sender, KeyEventArgs e)
        {
            ComboBox cbo = sender as ComboBox;
            cbo.DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents
        private void rptViewerSettlement_RenderingBegin(object sender, CancelEventArgs e)
        {
            BtnShow.Enabled = false;
        }

        private void rptViewerSettlement_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            BtnShow.Enabled = true;
        }
        #endregion

        #region ControlEvents
        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllBranches();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            this.rptViewerSettlement.Reset();
            try
            {

                if (FormValidations())
                {
                    ShowSettlementReport();
                }
            }
            catch (Exception ex)
            {
                BtnShow.Enabled = true;
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region FormLoad
        private void FrmSettlementReport_Load(object sender, EventArgs e)
        {
            LoadCombos();
            SetDefaultBranch(cboBranch);
            cboCompany_SelectedIndexChanged(null, null);
        }
        #endregion



    }
}
