﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;

namespace MyPayfriend
{
    public partial class FrmBankTransfer : Form
    {

        public int PiCompanyID;
        public int PiCurrencyID;
        public int PiCurrencyIDFilter;
        public string PstrType;
        public string PdtProcessedDate;
        public int FlgEmployeeVendor;
        public string PstrCompanyName;
        public string MachineName;
        public string pFormName;
        string MsReportPath;
        public int TemplateNo;
        private ArrayList MsMessageArray;  // Error Message display
        ClsLogWriter MObjClsLogWriter;      // Object of the LogWriter class
        ClsNotification MObjClsNotification; // Object of the Notification class
        MessageBoxIcon MsMessageBoxIcon;
        clsBLLSalarySlip MobjclsBLLSalarySlip;
        FrmBankPaymentRptSettings MObjFrmBankPaymentRptSettings = new FrmBankPaymentRptSettings();
        private string MsMessageCommon;
        private string MsMessageCaption;
        public FrmBankTransfer()
        {
            InitializeComponent();
            MobjclsBLLSalarySlip = new clsBLLSalarySlip();
            MObjClsNotification = new ClsNotification();
        }
        private void LoadMessage()
        {
            MsMessageArray = new ArrayList();
            MsMessageArray = MObjClsNotification.FillMessageArray(0, ClsCommonSettings.ProductID);
        }
        private void ReportSettings()
        {
            MObjFrmBankPaymentRptSettings.PintTemplateNo = TemplateNo;
            if (TemplateNo == 1)
            {
                MObjFrmBankPaymentRptSettings.PintCompanyID = PiCompanyID;
                MObjFrmBankPaymentRptSettings.PstrSubject = "Transfer Of Salary Account";
                MObjFrmBankPaymentRptSettings.PstrMatter = "Please find here with the attachment of salary amount to be credited.";
                MObjFrmBankPaymentRptSettings.Text = "Bank Transfer Letter";// +" - " + Template1ToolStripMenuItem.Text;
            }
            else if (TemplateNo == 2)
            {
                MObjFrmBankPaymentRptSettings.PintCompanyID = PiCompanyID;
                MObjFrmBankPaymentRptSettings.PstrSubject = "Transfer Of Salary Account";
                MObjFrmBankPaymentRptSettings.PstrAnnexure = "Annexure to Bank Transfer Letter";
                MObjFrmBankPaymentRptSettings.PstrMatter = "Please find here with the attachment of salary amount to be credited.";
                MObjFrmBankPaymentRptSettings.Text = "Bank Transfer Letter";// +" - " + Template2ToolStripMenuItem.Text;
            }
            else
            {
                MObjFrmBankPaymentRptSettings.PintCompanyID = PiCompanyID;
                MObjFrmBankPaymentRptSettings.PstrSubject = "Transfer Of Salary Account";
                MObjFrmBankPaymentRptSettings.PstrAnnexure = "Annexure to Bank Transfer Letter";
                MObjFrmBankPaymentRptSettings.PstrMatter = "Please find here with the attachment of salary amount to be credited.";
                MObjFrmBankPaymentRptSettings.Text = "Bank Transfer Letter";// +" - " + Template3ToolStripMenuItem.Text;
            }
        }


        private void LoadCombos(int TypeID)
        {

            DataTable datCombos = new DataTable();
            if (TypeID == 0 || TypeID == 1)
            {
                datCombos = null;
                datCombos = MobjclsBLLSalarySlip.FillCombos(new string[] { "CompanyID,CompanyName", "CompanyMaster", "", "CompanyID", "CompanyName" });
                cboCompany.ValueMember = "CompanyID";
                cboCompany.DisplayMember = "CompanyName";
                cboCompany.DataSource = datCombos;
                cboCompany.SelectedValue = PiCompanyID;
            }

            if ((TypeID == 0 || TypeID == 2) && (cboCompany.SelectedIndex !=-1))
            {
                datCombos = MobjclsBLLSalarySlip.getDetailsInComboBox(24, cboCompany.SelectedValue.ToInt32()); // Get Bank
                cboBank.ValueMember = "BankID";
                cboBank.DisplayMember = "Bank";
                cboBank.DataSource = datCombos;
                cboBank.SelectedIndex = -1;
            }
        }


        private void btnPreview_Click(System.Object sender, System.EventArgs e)
        {
            try
            {
                string StrBankName = ""; string StrBranchName = ""; string StrAddress = ""; string StrTelephone = "";
                ReportViewerBank.Reset();
                ReportViewerBank.Clear();

                DataTable datComp;
                DataTable datSalarySlip;
                DataTable datSalarySlipDetails;
                if (TemplateNo == 1)
                {

                    this.ReportViewerBank.LocalReport.DataSources.Clear();

                    if (ClsCommonSettings.IsArabicView)
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptBankPaymentArb.rdlc";

                    }
                    else
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptBankPayment.rdlc";


                    }

                    if (!System.IO.File.Exists(MsReportPath))
                    {
                        MessageBox.Show("Report File Not Found");
                        return;
                    }


                    this.ReportViewerBank.ProcessingMode = ProcessingMode.Local;
                    this.ReportViewerBank.LocalReport.ReportPath = MsReportPath;

                    ReportParameter[] RepParam = new ReportParameter[8];
                    RepParam[0] = new ReportParameter("subject", MObjFrmBankPaymentRptSettings.PstrSubject, false);
                    RepParam[1] = new ReportParameter("matterPara", MObjFrmBankPaymentRptSettings.PstrMatter, false);
                    RepParam[2] = new ReportParameter("BankName", StrBankName, false);
                    RepParam[3] = new ReportParameter("BranchName", StrBranchName, false);
                    RepParam[4] = new ReportParameter("Address", StrAddress, false);
                    RepParam[5] = new ReportParameter("Telephone", StrTelephone, false);
                    RepParam[6] = new ReportParameter("CompanyName", PstrCompanyName, true);
                    RepParam[7] = new ReportParameter("chHeader", "true", false);


                    //RepParam[7] = new ReportParameter("chHeader", MObjFrmBankPaymentRptSettings.PblnHead.ToString().Trim()  , false);


                    this.ReportViewerBank.LocalReport.SetParameters(RepParam);



                    MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = PiCompanyID;
                    MobjclsBLLSalarySlip.clsDTOSalarySlip.strProcessDate = PdtProcessedDate;
                    datComp = MobjclsBLLSalarySlip.GetCompanyDetail();
                    datSalarySlip = MobjclsBLLSalarySlip.GetPayment();

                    datSalarySlipDetails = datSalarySlip;

                    if (datSalarySlip == null || datSalarySlip.Rows.Count < 1)
                    {
                        MessageBox.Show("No Data found");
                        return;
                    }

                    this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetCompanyCommon_GaReportCompanyForm", datComp));
                    this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("DtSetDisplayPayment_DisplayWordReportMaster", datSalarySlip));
                    this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("DtSetDisplayPayment_DisplayWordReport", datSalarySlipDetails));

                }
                else if (TemplateNo == 2)
                {

                    this.ReportViewerBank.LocalReport.DataSources.Clear();

                    if (ClsCommonSettings.IsArabicView)
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptBankPayment1Arb.rdlc";

                    }
                    else
                    {
                        MsReportPath = Application.StartupPath + "\\MainReports\\RptBankPayment1.rdlc";


                    }
                    if (!System.IO.File.Exists(MsReportPath))
                    {
                        MessageBox.Show("Report File Not Found");
                        return;
                    }


                    this.ReportViewerBank.ProcessingMode = ProcessingMode.Local;
                    this.ReportViewerBank.LocalReport.ReportPath = MsReportPath;

                    ReportParameter[] RepParam = new ReportParameter[9];
                    RepParam[0] = new ReportParameter("subject", MObjFrmBankPaymentRptSettings.PstrSubject, false);
                    RepParam[1] = new ReportParameter("matterPara", MObjFrmBankPaymentRptSettings.PstrMatter, false);
                    RepParam[2] = new ReportParameter("BankName", StrBankName, false);
                    RepParam[3] = new ReportParameter("BranchName", StrBranchName, false);
                    RepParam[4] = new ReportParameter("Address", StrAddress, false);
                    RepParam[5] = new ReportParameter("Telephone", StrTelephone, false);
                    RepParam[6] = new ReportParameter("CompanyName", PstrCompanyName, true);
                    RepParam[7] = new ReportParameter("chHeader", MObjFrmBankPaymentRptSettings.PblnHead, false);
                    RepParam[8] = new ReportParameter("Annexure", MObjFrmBankPaymentRptSettings.PstrAnnexure, false);


                    this.ReportViewerBank.LocalReport.SetParameters(RepParam);



                    MobjclsBLLSalarySlip.clsDTOSalarySlip.intCompanyID = PiCompanyID;
                    MobjclsBLLSalarySlip.clsDTOSalarySlip.strProcessDate = PdtProcessedDate;
                    datComp = MobjclsBLLSalarySlip.GetCompanyDetail();
                    datSalarySlipDetails = MobjclsBLLSalarySlip.GetPayment();
                    datSalarySlip = MobjclsBLLSalarySlip.GetPayment();

                    if (datSalarySlip == null || datSalarySlip.Rows.Count < 1)
                    {
                        MessageBox.Show("No Data found");
                        return;
                    }
                    this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetCompanyCommon_GaReportCompanyForm", datComp));
                    this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("DtSetDisplayPayment_DisplayWordReportMaster", datSalarySlip));
                    this.ReportViewerBank.LocalReport.DataSources.Add(new ReportDataSource("DtSetDisplayPayment_DisplayWordReport", datSalarySlipDetails));

                }
                this.ReportViewerBank.LocalReport.Refresh();
                this.ReportViewerBank.SetDisplayMode(DisplayMode.PrintLayout);
                this.ReportViewerBank.ZoomMode = ZoomMode.Percent;
                this.ReportViewerBank.ZoomPercent = 100;
            }
            catch
            {
            }

        }

      



        private void FrmBankTransfer_Load(object sender, EventArgs e)
        {
            try
            {
                CboTemplate.SelectedIndex = 0;
                LoadMessage();
                LoadCombos(0);
                //Template1ToolStripMenuItem_Click(sender, e);
            }
            catch
            {
            }
        }

        private void cboCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboCompany.DroppedDown = false; 
        }

        private void cboBank_KeyPress(object sender, KeyPressEventArgs e)
        {
            cboBank.DroppedDown = false;
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCombos(2);
            }
            catch
            {
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                if (CboTemplate.SelectedIndex == 0 || CboTemplate.SelectedIndex == -1)
                {
                    TemplateNo = 1;
                }
                else
                {
                    TemplateNo = 2;
                }


                if (cboCompany.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 14, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    return;
                }
                if (cboBank.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 15, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    return;
                }

              
                
                if (cboCompany.SelectedIndex != -1)
                {
                    PstrCompanyName = cboCompany.Text.Trim();
                    PiCompanyID = cboCompany.SelectedValue.ToInt32();  
                    ReportSettings();
                    btnPreview_Click(sender, e);
                }

            }
            catch
            {
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboCompany.SelectedIndex == -1)
                {
                    MsMessageCommon = MObjClsNotification.GetErrorMessage(MsMessageArray, 14, out MsMessageBoxIcon);
                    MessageBox.Show(MsMessageCommon.Replace("#", "").Trim(), ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MsMessageBoxIcon);
                    return;
                }
                PstrCompanyName = cboCompany.Text.Trim();
                if (cboCompany.SelectedIndex != -1)
                {

                    MObjFrmBankPaymentRptSettings.PintCompanyID = cboCompany.SelectedValue.ToInt32();
                    MObjFrmBankPaymentRptSettings.ShowDialog();
                    ReportViewerBank.Reset();
                    ReportViewerBank.Clear();
                    btnPreview_Click(sender, e);
                }
            }
            catch
            {
            }
        }

    }
}