﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
/*****************************************************
* Created By       : SRUTHY K
* Creation Date    : 20 AUG 2013
* Description      :Employee Deduction Reports 
* ******************************************************/
namespace MyPayfriend
{
    public partial class FrmRptEmployeeDeduction : Report
    {
        #region Properties
        private clsMessage ObjUserMessage;

        /// <summary>
        /// gets message
        /// </summary>
        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.EmployeeDeductionReport);

                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public FrmRptEmployeeDeduction()
        {
            InitializeComponent();

            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }

        }
        #endregion


        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.EmployeeDeductionReport, this);
        }
        #endregion SetArabicControls


        #region FormLoad
        private void FrmRptEmployeeDeduction_Load(object sender, EventArgs e)
        {
            dtpMonth.Value = ClsCommonSettings.GetServerDate();
            LoadCombos();
            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus);
            cboCompany_SelectedIndexChanged(null, null);

        }
        #endregion

        #region Functions

        /// <summary>
        /// Load All Combos
        /// </summary>
        private void LoadCombos()
        {
            FillCompany();
            FillAllDepartments();
            FillAllDesignations();
            FillAllWorkStatus();
            FillAllDeductions();
        }
        /// <summary>
        /// Load Company
        /// </summary>
        private void FillCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }
        /// <summary>
        /// Load Branches by Company
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());
        }
        /// <summary>
        /// Load All Departments
        /// </summary>
        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }
        /// <summary>
        /// Load All Designations
        /// </summary>
        private void FillAllDesignations()
        {
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation();
        }
        /// <summary>
        /// Load Employees by Company,Department & Designation
        /// </summary>
        private void FillAllEmployees()
        {
            cboEmployee.DisplayMember = "EmployeeFullName";
            cboEmployee.ValueMember = "EmployeeID";
            cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), -1, cboWorkStatus.SelectedValue.ToInt32(), -1
                                );
        }
        /// <summary>
        /// Load Deduction by Company & Employee
        /// </summary>
        private void FillAllDeductions()
        {
            cboDeduction.DisplayMember = "AdditionDeduction";
            cboDeduction.ValueMember = "AdditionDeductionID";
            cboDeduction.DataSource = clsBLLReportCommon.GetAllDeductions();
        }
        /// <summary>
        /// Load WorkStatus 
        /// </summary>
        private void FillAllWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }


        /// <summary>
        /// Enable or Disable Include Company checkbox
        /// </summary>
        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }
        /// <summary>
        /// Validates form control
        /// </summary>
        /// <returns>bool</returns>
        private bool ValidateReport()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(2504);
                return false;
            }
            if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(2505);
                return false;
            }
            if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(2506);
                return false;
            }
            if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(2507);
                return false;
            }
            if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }
            if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(2501);
                return false;
            }
            if (cboDeduction.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(2502);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Gets Employee deduction details
        /// </summary>
        private void ShowReport()
        {
            string[] saMonthYear = new string[2];
            saMonthYear = dtpMonth.Value.ToString("MMM-yyyy").Split('-');
            string strMonth = saMonthYear[0];
            int intYear = saMonthYear[1].ToInt32();

            DataTable datEmployeeDeduction = clsBLLRptEmployeeDeduction.GetReport(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), cboDeduction.SelectedValue.ToInt32(), strMonth, intYear);
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            if (datEmployeeDeduction != null && datEmployeeDeduction.Rows.Count > 0)
            {
                if (ClsCommonSettings.IsArabicView)
                {
                    this.rvEmployeeDeduction.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeDeductionArb.rdlc";

                }
                else
                {
                    this.rvEmployeeDeduction.LocalReport.ReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeDeduction.rdlc";

                }
                this.rvEmployeeDeduction.LocalReport.SetParameters(new ReportParameter[]
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter),
                    new ReportParameter("Company", cboCompany.Text.Trim()),
                    new ReportParameter("Branch", cboBranch.Text.Trim()),
                    new ReportParameter("Department", cboDepartment.Text.Trim()),
                    new ReportParameter("Designation", cboDesignation.Text.Trim()),
                    new ReportParameter("WorkStatus", cboWorkStatus.Text.Trim()),
                    new ReportParameter("Employee", cboEmployee.Text.Trim()),
                    new ReportParameter("Deduction", cboDeduction.Text.Trim()),
                    new ReportParameter("Period",dtpMonth.Value.ToString("MMM-yyyy"))
                });

                this.rvEmployeeDeduction.LocalReport.DataSources.Clear();
                this.rvEmployeeDeduction.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_EmployeeDeduction", datEmployeeDeduction));
                this.rvEmployeeDeduction.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                this.rvEmployeeDeduction.SetDisplayMode(DisplayMode.PrintLayout);
                this.rvEmployeeDeduction.ZoomMode = ZoomMode.Percent;
                this.rvEmployeeDeduction.ZoomPercent = 100;
            }
            else
            {
                UserMessage.ShowMessage(2503);
                return;
            }
        }
        private void LoadEmployee(object sender, EventArgs e)
        {
            FillAllEmployees();
        }
        #endregion

        #region ControlEvents

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
        }
        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany();
            FillAllEmployees();
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            dtpMonth.Value = Convert.ToDateTime("01-" + dtpMonth.Value.ToString("MMMM") + "-" + dtpMonth.Value.Year.ToString());
        }


        private void btnShow_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                rvEmployeeDeduction.Reset();

                if (ValidateReport())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);
            }
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region Control KeyDown Event
        private void cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ((ComboBox)(sender)).DroppedDown = false;
        }
        #endregion

        #region ReportViewerEvents

        private void rvEmployeeDeduction_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvEmployeeDeduction_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion


    }
}
