﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using Microsoft.VisualBasic;
namespace MyPayfriend
{
    public partial class FrmRptEmployeeExpense : Report
    {
        #region Properties
        private string strMReportPath = "";

        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.PaymentsMISReport);

                return this.ObjUserMessage;
            }
        }
        #endregion

        #region Constructor
        public FrmRptEmployeeExpense()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                SetArabicControls();
            }
        }
        #endregion
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.ProjectCostReport, this);
        }
        #endregion SetArabicControls

        #region FormLoad
        private void FrmRptPayments_Load(object sender, EventArgs e)
        {
            try
            {
                    //Fill all the combos
                    FillCombos();

                    SetDefaultBranch(cboBranch);
                    SetDefaultWorkStatus(cboWorkStatus); 
                    //Enable or disable the month combo ,from date and to date combo based on the type value
                    EnableDisableDates(); 

                    chkIncludeCompany.Checked = true;
                    //-------------------
                    lblFromDate.Enabled = true;
                    lblToDate.Enabled = true;
                    dtpFromDate.Enabled = true;
                    dtpToDate.Enabled = true;
                    //-------------------

                    //if (ClsCommonSettings.IsMultiCurrencyEnabled == true)
                    //{
                    //    lblBranch.Enabled  =false;
                    //    cboBranch.Enabled = false;
                    //    chkIncludeCompany.Visible  = false;
                    //    cboBranch.SelectedValue = 0;
                    //}


                   
                        dtpFromDate.Value = ("01/" + GetCurrentMonth(dtpFromDate.Value.Month) + "/" + (dtpFromDate.Value.Year) + "").ToDateTime();
                        dtpToDate.Value = ("01/" + GetCurrentMonth(dtpToDate.Value.Month) + "/" + (dtpToDate.Value.Year) + "").ToDateTime();
            }
            catch
            {
            }



        }
        #endregion



        #region Methods

        private void FillCombos()
        {

            //if (ClsCommonSettings.IsMultiCurrencyEnabled)
            //{
            //    GetAllCompaniesMulti();
            //}
            //else
            //{
                FillAllCompanies();
            //}
            FillAllDepartments(); 
            FillPaymentTypes();
            FillDesignation();
            FillWorkStatus();
            FillProject();
        }

        private void FillProject()
        {
            cboProject.DisplayMember = "ProjectCode";
            cboProject.ValueMember = "ProjectID";
            cboProject.DataSource = clsBLLReportCommon.GetProject();
        }

        private void FillAllCompanies()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        private void GetAllCompaniesMulti()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompaniesMulti();
        }

        private void FillAllBranch()
        {
            try
            {
                cboBranch.DisplayMember = "Branch";
                cboBranch.ValueMember = "BranchID";
                cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

                //if (ClsCommonSettings.IsMultiCurrencyEnabled == true)
                //{
                //    cboBranch.SelectedValue = 0;
                //}
            }
            catch
            {
            }


        }

        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }

        private void FillAllEmployees()
        {
            if (ClsCommonSettings.IsArabicView)
            {

                cboEmployee.DisplayMember = "EmployeeFullName";
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployeesArb(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                                            cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), -1, cboWorkStatus.SelectedValue.ToInt32(), -1);
            }
            else
            {
                cboEmployee.DisplayMember = "EmployeeFullName";
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                                            cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), -1, cboWorkStatus.SelectedValue.ToInt32(), -1);

            }

        }

        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedIndex = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }

        private void FillDesignation()
        {
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation(); 
        }

        private void FillWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
            
        }

        private void ShowReport()
        {
           
            DataTable DtPayments = null;

            //setting the report path based on the payment type value

            if (ClsCommonSettings.IsArabicView)
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeProjectCostArb.rdlc";
            }
            else
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeProjectCost.rdlc";
            }
    

            //Setting the report header 
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            rvPayments.Reset();
            rvPayments.LocalReport.DataSources.Clear();
            rvPayments.LocalReport.ReportPath = strMReportPath;

                DtPayments = clsBLLRptProjectExpense.GetProjectPayments(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value, chkIncludeCompany.Checked, cboProject.SelectedValue.ToInt32());

                DataTable dtCompanies =  DtPayments.DefaultView.ToTable(true, "CompanyName");  


                rvPayments.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                    new ReportParameter("CompanyName", cboCompany.Text , false),
                    new ReportParameter("BranchName",cboBranch.Text , false),
                    new ReportParameter("Department", cboDepartment.Text , false),
                    new ReportParameter("EmployeeName",cboEmployee.Text, false),
                    new ReportParameter("FromDate",  dtpFromDate.Text, false),
                    new ReportParameter("ToDate", dtpToDate.Text  , false),
                    new ReportParameter("Designation",cboDesignation.Text, false),
                    new ReportParameter("WorkStatus",  cboWorkStatus.Text , false)
                    

                });


                if (DtPayments.Rows.Count > 0)
                {
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayRptEmployeeProjectExpense_PayRptEmployeeProjectExpense", DtPayments));
                }

            //}

            //Show no record found
            if (DtPayments.Rows.Count == 0)
            {
                UserMessage.ShowMessage(9707);
                return;
            }

            //Set the report display mode and zoom percentage
            SetReportDisplay(rvPayments);

        }
       
        public bool FormValidation()
        {
            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9708);
                return false;
            }
            else if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9709);
                return false;
            }
            else if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9710);
                return false;
            }

            else if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9112);
                return false;
            }
            else if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }

            else if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9711);
                return false;
            }

        
            else if ( (dtpFromDate.Value.Date > dtpToDate.Value.Date))
            {
                UserMessage.ShowMessage(1761);
                return false;
            }

            return true;
        }

        private void FillPaymentTypes()
        {
            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add("TypeID");
            dt.Columns.Add("Type");

            dr = dt.NewRow();
            dr["TypeID"] = 0;
            dr["Type"] = "Monthly";
            dt.Rows.InsertAt(dr, 0);


            dr = dt.NewRow();
            dr["TypeID"] = 1;
            dr["Type"] = "Summary";
            dt.Rows.InsertAt(dr, 1);



        


        }

        #endregion

        #region ControlEvents

        private void btnShow_Click(object sender, EventArgs e)
        {
           
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (FormValidation())
                {
                    ShowReport();
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                this.Cursor = Cursors.WaitCursor;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;

        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranch();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany(); 
            FillAllEmployees();
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableDates(); 
        }

        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

       
        private void EnableDisableDates()
        {
            lblFromDate.Enabled = true;
            lblToDate.Enabled = true;
            dtpFromDate.Enabled = true;
            dtpToDate.Enabled = true;
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }

        #endregion

        #region ReportViewer Events
        private void rvPayments_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvPayments_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion

        private void labelX3_Click(object sender, EventArgs e)
        {

        }

        private void cboProject_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                dtpFromDate.Value = ("01/" + GetCurrentMonth(dtpFromDate.Value.Month) + "/" + (dtpFromDate.Value.Year) + "").ToDateTime();
            }
            catch
            {
            }

        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                dtpToDate.Value = ("01/" + GetCurrentMonth(dtpToDate.Value.Month) + "/" + (dtpToDate.Value.Year) + "").ToDateTime();
               // dtpToDate.Value = dtpToDate.Value.AddDays(-1); 
            }
            catch
            {
            }
        }


        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }



    }
}
