﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPayfriend
{
    public partial class frmRptAccrual : DevComponents.DotNetBar.Office2007Form
    {
        ClsLogWriter MobjClsLogs;
        ClsExportDatagridviewToExcel MobjExportToExcel;
        public frmRptAccrual()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                //lblEmployee.Text = "عامل";
                lblCompany.Text = "شركة";
                lblBranch.Text = "فرع";
                chkIncludeCompany.Text = "وتشمل الشركة";
                lblDate.Text = "كما في التاريخ";
                btnExport.Text = "تصدير";
                btnShow.Text = "عرض";
                lblStatus.Text = "حالة";
                btnShow.Tooltip = "عرض تقرير ";
                btnExport.Tooltip = "تصدير إلى Excel";
                lblEmployee.Text = "أجير";
            }
            MobjClsLogs = new ClsLogWriter(Application.StartupPath);
            MobjExportToExcel = new ClsExportDatagridviewToExcel();
           
        }
        private void LoadCombos()
        {
            FillCompany();
          
        }

        private void FillAllEmployees()
        {
            if (ClsCommonSettings.IsArabicView)
            {

                cboEmployee.DisplayMember = "EmployeeFullName";
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployeesArb(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                                            -1, -1, -1, -1, -1);
            }
            else
            {
                cboEmployee.DisplayMember = "EmployeeFullName";
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                                            -1, -1, -1, -1, -1);

            }

        }
        /// <summary>
        /// Load Company
        /// </summary>
        private void FillCompany()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        /// <summary>
        /// Load Branches by Company
        /// </summary>
        private void FillAllBranches()
        {
            cboBranch.DisplayMember = "Branch";
            cboBranch.ValueMember = "BranchID";
            cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranches();
            FillAllEmployees();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                dgvAccrual.DataSource = clsBLLReportCommon.GetEmployeeAccural(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value);
                            
                 btnExport.Visible = true;
                              
            }
            catch(Exception ex)
            {
                ClsLogWriter.WriteLog(ex, LogSeverity.Error);
            }

        }

        private void frmRptAccrual_Load(object sender, EventArgs e)
        {
            LoadCombos();
           
        }
        private void InitializeProgressBar(int iRowCount)
        {
            //Initializing the progress bar
            TlSProgressBarAttendance.Visible = true;
            TlSProgressBarAttendance.Value = 0;
            TlSProgressBarAttendance.Minimum = 0;
            if (iRowCount > 0)
            {
                TlSProgressBarAttendance.Maximum = iRowCount + 1;
            }
        }
        private void ResetProgressBar()
        {
            
         TlSProgressBarAttendance.Value = TlSProgressBarAttendance.Maximum;
         TlSProgressBarAttendance.Visible = false;
         
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            int iStartColum = 0;
            int iEndColumn = 0;
            string sFileName = "";

            if (dgvAccrual.RowCount <= 0)
            {
                return;
            }
           // MobjClsLogs.WriteLog("btnExportToExcel", 2);
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";
                if (sfd.ShowDialog() == DialogResult.OK)
                {

                    sFileName = sfd.FileName;
                   // MobjClsLogs.WriteLog("btnExportToExcel_Click(object sender, EventArgs e)--FileName=" + sFileName, 2);
                }
                else
                {
                    return;
                }
            }
            //MobjClsLogs.WriteLog("btnExportToExcel_Click_Click(object sender, EventArgs e)", 2);
            try
            {
                if (sFileName != "")
                {

                    if (dgvAccrual.RowCount > 0)
                    {
                       
                        MobjExportToExcel.ExportToExcel(dgvAccrual, sFileName, 0, dgvAccrual.Columns.Count-1, true);
                        //reportHeader, ".xls", My.Computer.FileSystem.SpecialDirectories.Temp)DgvFetchData,"Data",".xls",Application.StartupPath,0,9);
                    }
                    
                }
                else
                {
                    MessageBox.Show("Please Enter Name");
                }
            }
            catch (Exception Ex)
            {
                MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

            }
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                FillAllEmployees();
            }
            catch
            {
            }
        }

        private void dgvAccrual_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

      
        private void cbo_KeyDown(object sender, KeyEventArgs e)
        {
            ((ComboBox)(sender)).DroppedDown = false;
        }

        private void cboEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            cboEmployee.DroppedDown = false; 
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillAllEmployees();
            }
            catch
            { }
        }

        private void expandablePanel1_Click(object sender, EventArgs e)
        {

        }

     

        
    }
}
