﻿namespace MyPayfriend
{
    partial class FrmBankTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboBank = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ReportViewerBank = new Microsoft.Reporting.WinForms.ReportViewer();
            this.label2 = new System.Windows.Forms.Label();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.CboTemplate = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboCompany
            // 
            this.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(60, 14);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(285, 21);
            this.cboCompany.TabIndex = 70;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            this.cboCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboCompany_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 71;
            this.label1.Text = "Bank";
            // 
            // cboBank
            // 
            this.cboBank.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBank.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBank.FormattingEnabled = true;
            this.cboBank.Location = new System.Drawing.Point(60, 45);
            this.cboBank.Name = "cboBank";
            this.cboBank.Size = new System.Drawing.Size(285, 21);
            this.cboBank.TabIndex = 72;
            this.cboBank.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboBank_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.ReportViewerBank);
            this.panel1.Location = new System.Drawing.Point(6, 85);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(749, 449);
            this.panel1.TabIndex = 73;
            // 
            // ReportViewerBank
            // 
            this.ReportViewerBank.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportViewerBank.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DtSetDisplayPayment_DisplayWordReportMaster";
            reportDataSource1.Value = null;
            this.ReportViewerBank.LocalReport.DataSources.Add(reportDataSource1);
            this.ReportViewerBank.LocalReport.ReportEmbeddedResource = "MindsoftERP.bin.Debug.MainReports.RptBankPayment1.rdlc";
            this.ReportViewerBank.Location = new System.Drawing.Point(0, 0);
            this.ReportViewerBank.Name = "ReportViewerBank";
            this.ReportViewerBank.Size = new System.Drawing.Size(749, 449);
            this.ReportViewerBank.TabIndex = 70;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 74;
            this.label2.Text = "Company";
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(455, 48);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(99, 31);
            this.btnShow.TabIndex = 75;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Location = new System.Drawing.Point(560, 48);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(99, 31);
            this.btnSettings.TabIndex = 76;
            this.btnSettings.Text = "Report Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(397, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 77;
            this.label3.Text = "Template";
            // 
            // CboTemplate
            // 
            this.CboTemplate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboTemplate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboTemplate.FormattingEnabled = true;
            this.CboTemplate.Items.AddRange(new object[] {
            "Template 1",
            "Template 2"});
            this.CboTemplate.Location = new System.Drawing.Point(454, 15);
            this.CboTemplate.Name = "CboTemplate";
            this.CboTemplate.Size = new System.Drawing.Size(285, 21);
            this.CboTemplate.TabIndex = 78;
            // 
            // FrmBankTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 526);
            this.Controls.Add(this.CboTemplate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cboBank);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboCompany);
            this.Name = "FrmBankTransfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bank Transfer";
            this.Load += new System.EventHandler(this.FrmBankTransfer_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboCompany;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboBank;
        private System.Windows.Forms.Panel panel1;
        internal Microsoft.Reporting.WinForms.ReportViewer ReportViewerBank;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CboTemplate;
        //private global::MindsoftERP.MindsoftERP.Reports.Payroll.Payroll.DtSetDisplayPayment dtSetDisplayPayment;
        //private global::MindsoftERP.MindsoftERP.Reports.Payroll.Payroll.RptDtSetCompanyCommon rptDtSetCompanyCommon;
    }
}