﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace MyPayfriend
{
    public partial class FrmRptPayments : Report
    {
        #region Properties
        private string strMReportPath = "";

        private clsMessage ObjUserMessage = null;

        private clsMessage UserMessage
        {
            get
            {
                if (this.ObjUserMessage == null)
                    this.ObjUserMessage = new clsMessage(FormID.PaymentsMISReport);

                return this.ObjUserMessage;
            }
        }

        ClsExportDatagridviewToExcel MobjExportToExcel;
        #endregion

        #region Constructor
        public FrmRptPayments()
        {
            InitializeComponent();
            if (ClsCommonSettings.IsArabicView)
            {
                this.RightToLeftLayout = true;
                this.RightToLeft = RightToLeft.Yes;
                btnExcelExport.Text = "تصدير";
                btnExcelExport.Tooltip = "تصدير إلى Excel";
                SetArabicControls();
            }

            MobjExportToExcel = new ClsExportDatagridviewToExcel();
        }
        #endregion
        #region SetArabicControls
        private void SetArabicControls()
        {
            ClsDALArabicConverter objDAL = new ClsDALArabicConverter();
            objDAL.SetArabicVersion((int)FormID.PaymentsMISReport, this);
        }
        #endregion SetArabicControls

        #region FormLoad
        private void FrmRptPayments_Load(object sender, EventArgs e)
        {
            //Fill all the combos
            FillReportFormat();
            FillCombos();

            SetDefaultBranch(cboBranch);
            SetDefaultWorkStatus(cboWorkStatus); 
            //Enable or disable the month combo ,from date and to date combo based on the type value
            EnableDisableDates(); 

            chkIncludeCompany.Checked = true;


            //if (ClsCommonSettings.IsMultiCurrencyEnabled == true)
            //{
            //    lblBranch.Enabled  =false;
            //    cboBranch.Enabled = false;
            //    chkIncludeCompany.Visible  = false;
            //    cboBranch.SelectedValue = 0;
            //}

            dtpFromDate.Value = ("01/" + GetCurrentMonth(dtpFromDate.Value.Month) + "/" + (dtpFromDate.Value.Year) + "").ToDateTime();
            dtpToDate.Value = ("01/" + GetCurrentMonth(dtpToDate.Value.Month) + "/" + (dtpToDate.Value.Year) + "").ToDateTime();

            cboCompany_SelectedIndexChanged(null, null);
        }
        #endregion

        private void FillReportFormat()
        {
            DataTable datTemp = new DataTable();
            datTemp.Columns.Add("ReportFormatID", typeof(int));
            datTemp.Columns.Add("ReportFormat", typeof(string));

            datTemp.Rows.Add(1, "Graphics Format-1");
            datTemp.Rows.Add(1, "Graphics Format-2");
            datTemp.Rows.Add(1, "Graphics-Format-3");
            datTemp.Rows.Add(1, "Grid Format-1");
            datTemp.Rows.Add(1, "Grid Format-2");

            cboReportFormat.DataSource = datTemp;
            cboReportFormat.ValueMember = "ReportFormatID";
            cboReportFormat.DisplayMember = "ReportFormat";

            cboReportFormat.SelectedIndex = 0;
        }

        private string GetCurrentMonth(int MonthVal)
        {
            string Months = "";

            switch (MonthVal)
            {
                case 1:
                    Months = "Jan";
                    break;
                case 2:
                    Months = "Feb";
                    break;
                case 3:
                    Months = "Mar";
                    break;
                case 4:
                    Months = "Apr";
                    break;
                case 5:
                    Months = "May";
                    break;
                case 6:
                    Months = "Jun";
                    break;
                case 7:
                    Months = "Jul";
                    break;
                case 8:
                    Months = "Aug";
                    break;
                case 9:
                    Months = "Sep";
                    break;
                case 10:
                    Months = "Oct";
                    break;
                case 11:
                    Months = "Nov";
                    break;
                case 12:
                    Months = "Dec";
                    break;
            }

            return Months;
        }


        #region Methods

        private void FillCombos()
        {

            //if (ClsCommonSettings.IsMultiCurrencyEnabled)
            //{
            //    GetAllCompaniesMulti();
            //}
            //else
            //{
                FillAllCompanies();
            //}
            FillAllDepartments(); 
            FillPaymentTypes();
            FillDesignation();
            FillWorkStatus();
            FillTransactionType();
        }

        private void FillTransactionType()
        {
            cboTransactionType.DisplayMember = "TransactionType";
            cboTransactionType.ValueMember = "TransactionTypeID";
            cboTransactionType.DataSource = clsBLLReportCommon.GetTransactionType();
        }

        private void FillAllCompanies()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompanies();
        }

        private void GetAllCompaniesMulti()
        {
            cboCompany.DisplayMember = "Company";
            cboCompany.ValueMember = "CompanyID";
            cboCompany.DataSource = clsBLLReportCommon.GetAllCompaniesMulti();
        }

        private void FillAllBranch()
        {
            try
            {
                cboBranch.DisplayMember = "Branch";
                cboBranch.ValueMember = "BranchID";
                cboBranch.DataSource = clsBLLReportCommon.GetAllBranchesByCompanyID(cboCompany.SelectedValue.ToInt32());

                //if (ClsCommonSettings.IsMultiCurrencyEnabled == true)
                //{
                //    cboBranch.SelectedValue = 0;
                //}
            }
            catch
            {
            }
        }

        private void FillAllDepartments()
        {
            cboDepartment.DisplayMember = "Department";
            cboDepartment.ValueMember = "DepartmentID";
            cboDepartment.DataSource = clsBLLReportCommon.GetAllDepartments();
        }

        private void FillAllEmployees()
        {
            if (ClsCommonSettings.IsArabicView)
            {

                cboEmployee.DisplayMember = "EmployeeFullName";
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployeesArb(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                                            cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), -1, cboWorkStatus.SelectedValue.ToInt32(), -1);
            }
            else
            {
                cboEmployee.DisplayMember = "EmployeeFullName";
                cboEmployee.ValueMember = "EmployeeID";
                cboEmployee.DataSource = clsBLLReportCommon.GetAllEmployees(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                                            cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), -1, cboWorkStatus.SelectedValue.ToInt32(), -1);

            }
        }

        private void FillPaymentMonthYear()
        {
         
            cboMonthAndYear.ValueMember = "Value";
            cboMonthAndYear.DisplayMember = "Description";
            cboMonthAndYear.DataSource = clsBLLReportCommon.GetPayreleasedMonthYear(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32());

            if (cboMonthAndYear.Items.Count > 0)
                cboMonthAndYear.SelectedIndex = 0;
            else
                cboMonthAndYear.Text = "";
            
        }

        private void EnableDisableIncludeCompany()
        {

            //Checking if the user is having access to any branch
            if (cboBranch.Items.Count == 2)
            {
                cboBranch.SelectedValue = 0;
                cboBranch.Enabled = false;
            }
            else
            {
                cboBranch.Enabled = true;
            }

            //Checking if the users is having access to the selected company
            if (!(clsBLLReportCommon.IsCompanyPermissionExists(cboCompany.SelectedValue.ToInt32())))
            {
                chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
            }

            else
            {
                //No branch selected
                if (cboBranch.SelectedValue.ToInt32() == 0 || cboBranch.Items.Count == 2)
                {
                    chkIncludeCompany.Checked = true;
                    chkIncludeCompany.Enabled = false;
                }
                else
                {
                    chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
                }
            }

        }

        private void FillDesignation()
        {
            cboDesignation.DisplayMember = "Designation";
            cboDesignation.ValueMember = "DesignationID";
            cboDesignation.DataSource = clsBLLReportCommon.GetAllDesignation(); 
        }

        private void FillWorkStatus()
        {
            cboWorkStatus.DisplayMember = "WorkStatus";
            cboWorkStatus.ValueMember = "WorkStatusID";
            cboWorkStatus.DataSource = clsBLLReportCommon.GetAllWorkStatus();
        }

        public void ShowPaymentReport()
        {
            int Month = 0;
            int Year = 0;
            DataTable DtPayments = null;
            string PaymentDate = "01-Jan-1900";
            //string ProcessDate = "01-Jan-1900";
            //setting the report path based on the payment type value


        


        }

        private void ShowReport()
        {
            int Month = 0;
            int Year = 0;
            DataTable DtPayments = null;
            string PaymentDate = "01-Jan-1900";
            //string ProcessDate = "01-Jan-1900";
            //setting the report path based on the payment type value


            if (cboReportFormat.SelectedIndex == 1 && cboType.SelectedIndex == 0)
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\RptEmployeeMonthlyPayment1.rdlc";
            }
            else if (cboReportFormat.SelectedIndex == 2 && cboType.SelectedIndex == 0)
            {
                strMReportPath = Application.StartupPath + "\\MainReports\\RptEmployeePaymentSummary.rdlc";
            }
            else if (ClsCommonSettings.IsArabicView)
            {
                strMReportPath = cboType.SelectedIndex == 0 ? Application.StartupPath + "\\MainReports\\RptEmployeeMonthlyPaymentArb.rdlc"
                    : Application.StartupPath + "\\MainReports\\RptEmployeeYearlyPaymentArb.rdlc";
            }
            else
            {
                strMReportPath = cboType.SelectedIndex == 0 ? Application.StartupPath + "\\MainReports\\RptEmployeeMonthlyPayment.rdlc"
                    : Application.StartupPath + "\\MainReports\\RptEmployeeYearlyPayment.rdlc";
            }


            //if (dtpPaymentDate.Checked)
            //{
            //    PaymentDate = dtpPaymentDate.Value.ToString("dd-MMM-yyyy");  
            //}

            //Setting the report header 
            SetReportHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            rvPayments.Reset();
            rvPayments.LocalReport.DataSources.Clear();
            rvPayments.LocalReport.ReportPath = strMReportPath;

            //Monthly Report
            if (cboType.SelectedIndex == 0)
            {
                Month = cboMonthAndYear.SelectedValue.ToString().Split('@')[0].ToInt16();
                Year = cboMonthAndYear.SelectedValue.ToString().Split('@')[1].ToInt16();
               
                    rvPayments.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                    new ReportParameter("CompanyName", cboCompany.Text , false),
                    new ReportParameter("BranchName",cboBranch.Text , false),
                    new ReportParameter("Department", cboDepartment.Text , false),
                    new ReportParameter("EmployeeName",cboEmployee.Text, false),
                    new ReportParameter("MonthAndYear",  cboMonthAndYear.Text , false),
                    new ReportParameter("Designation",cboDesignation.Text, false),
                    new ReportParameter("WorkStatus",  cboWorkStatus.Text , false),
                    new ReportParameter("TransactionType",  cboTransactionType.Text, false)

                });

            

                if (cboReportFormat.SelectedIndex == 1 && cboType.SelectedIndex == 0)
                {
                    DtPayments = clsBLLRptPayments.GetMonthlyPaymentsOne(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), Month, Year, chkIncludeCompany.Checked, cboTransactionType.SelectedValue.ToInt32(), chkIsPayStructureOnly.Checked, PaymentDate, 1,1,ChkExcludeSettlement.Checked.ToBoolean());
                    if (DtPayments.Rows.Count > 0)
                    {
                        rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayments_EmployeePayment", DtPayments));
                    }
                }
                else if (cboReportFormat.SelectedIndex == 2 && cboType.SelectedIndex == 0)
                {
                    DtPayments = clsBLLRptPayments.GetMonthlyPaymentSummary(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), Month, Year, chkIncludeCompany.Checked, cboTransactionType.SelectedValue.ToInt32(), chkIsPayStructureOnly.Checked, PaymentDate, 1, ChkExcludeSettlement.Checked.ToBoolean(),dtpFromDate.Value.ToString("dd-MMM-yyyy"),dtpToDate.Value.ToString("dd-MMM-yyyy"));
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayments_EmployeePaymentSummary", DtPayments));
                }
                else
                {
                    DtPayments = clsBLLRptPayments.GetMonthlyPayments(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), Month, Year, chkIncludeCompany.Checked, cboTransactionType.SelectedValue.ToInt32(), chkIsPayStructureOnly.Checked, PaymentDate, 1, ChkExcludeSettlement.Checked.ToBoolean());
                    if (DtPayments.Rows.Count > 0)
                    {
                        rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayments_EmployeeSalarySummary", DtPayments));
                    }
                }
            }

            else if (cboType.SelectedIndex == 1)
            {

                DtPayments = clsBLLRptPayments.GetSummaryPayments(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(), cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value, chkIncludeCompany.Checked, cboTransactionType.SelectedValue.ToInt32(), PaymentDate, 1, ChkExcludeSettlement.Checked.ToBoolean());

                DataTable dtCompanies =  DtPayments.DefaultView.ToTable(true, "CompanyName");  


                rvPayments.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", ClsCommonSettings.ReportFooter, false),
                    new ReportParameter("CompanyName", cboCompany.Text , false),
                    new ReportParameter("BranchName",cboBranch.Text , false),
                    new ReportParameter("Department", cboDepartment.Text , false),
                    new ReportParameter("EmployeeName",cboEmployee.Text, false),
                    new ReportParameter("FromDate",  dtpFromDate.Text, false),
                    new ReportParameter("ToDate", dtpToDate.Text  , false),
                       new ReportParameter("Designation",cboDesignation.Text, false),
                    new ReportParameter("WorkStatus",  cboWorkStatus.Text , false),
                    new ReportParameter("Count",  dtCompanies.Rows.Count.ToString() , false),

                });


                if (DtPayments.Rows.Count > 0)
                {
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    rvPayments.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayments_EmployeeYearlyPayment", DtPayments));
                }

            }

            //Show no record found
            if (DtPayments.Rows.Count == 0)
            {
                UserMessage.ShowMessage(9707);
                return;
            }

            //Set the report display mode and zoom percentage
            SetReportDisplay(rvPayments);

        }
       
        public bool FormValidation()
        {


            if (cboReportFormat.SelectedIndex == 1 && cboType.SelectedIndex == 1)
            {
                cboReportFormat.SelectedIndex = 0;
            }


            if (cboCompany.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9708);
                return false;
            }
            else if (cboBranch.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9709);
                return false;
            }
            else if (cboDepartment.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9710);
                return false;
            }

            else if (cboDesignation.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9112);
                return false;
            }
            else if (cboWorkStatus.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9113);
                return false;
            }

            else if (cboEmployee.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9711);
                return false;
            }
            else if (cboType.SelectedIndex == -1)
            {
                UserMessage.ShowMessage(9728);
                return false;
            }

            else if ((cboType.SelectedIndex ==0) && (cboMonthAndYear.SelectedIndex == -1))
            {
                UserMessage.ShowMessage(9712);
                return false;
            }
            else if ((cboType.SelectedIndex == 1) && (dtpFromDate.Value.Date > dtpToDate.Value.Date))
            {
                UserMessage.ShowMessage(1761);
                return false;
            }




            return true;
        }

        private void FillPaymentTypes()
        {
            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add("TypeID");
            dt.Columns.Add("Type");

            dr = dt.NewRow();
            dr["TypeID"] = 0;
            dr["Type"] = "Monthly";
            dt.Rows.InsertAt(dr, 0);


            dr = dt.NewRow();
            dr["TypeID"] = 1;
            dr["Type"] = "Summary";
            dt.Rows.InsertAt(dr, 1);



            cboType.DataSource = dt;
            cboType.DisplayMember = "Type";
            cboType.ValueMember = "TypeID";


        }

        #endregion

        #region ControlEvents

        private void btnShow_Click(object sender, EventArgs e)
        {
           
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (FormValidation())
                {
                    if (cboReportFormat.SelectedIndex == 0 || cboReportFormat.SelectedIndex == 1)
                    {
                        ShowReport();
                    }
                    else if (cboReportFormat.SelectedIndex == 2 )
                    {
                        ShowReport();
                    }
                    else
                    {
                        ShowGridReport();
                    }
                  
                }
            }
            catch (Exception Ex)
            {
                btnShow.Enabled = true;
                this.Cursor = Cursors.WaitCursor;
                ClsLogWriter.WriteLog(Ex, LogSeverity.Error);

            }
            this.Cursor = Cursors.Default;

        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllBranch();
        }

        private void cboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableIncludeCompany(); 
            FillAllEmployees();
        }

        private void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableDates(); 
        }

        private void cboWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAllEmployees();
        }

        private void cboEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillPaymentMonthYear();
        }
        private void EnableDisableDates()
        {
            if (cboType.SelectedIndex == 0)
            {
                lblFromDate.Enabled = false;
                lblToDate.Enabled = false;
                dtpFromDate.Enabled = false;
                dtpToDate.Enabled = false;
                lblMonthAndYear.Enabled = true;
                cboMonthAndYear.Enabled = true;
                chkIsPayStructureOnly.Enabled = true; 

            }
            else
            {
                lblFromDate.Enabled = true;
                lblToDate.Enabled = true;
                dtpFromDate.Enabled = true;
                dtpToDate.Enabled = true;
                lblMonthAndYear.Enabled = false;
                cboMonthAndYear.Enabled = false;
                chkIsPayStructureOnly.Enabled = false;
                chkIsPayStructureOnly.Checked = false;

            }
        }

        private void ComboKeyPress(object sender, KeyPressEventArgs e)
        {
            ((ComboBox)sender).DroppedDown = false;
        }

        #endregion

        #region ReportViewer Events
        private void rvPayments_RenderingBegin(object sender, CancelEventArgs e)
        {
            btnShow.Enabled = false;
        }

        private void rvPayments_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            btnShow.Enabled = true;
        }
        #endregion

        private void cboReportFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReportFormat.SelectedIndex == 0 || cboReportFormat.SelectedIndex == 1 || cboReportFormat.SelectedIndex == 2)
            {
                dgvReport.Visible = pnlGrid.Visible = btnExcelExport.Visible = false;
                rvPayments.Visible = true;
            }
           
            else
            {
                dgvReport.Visible = btnExcelExport.Visible = true;
                rvPayments.Visible = false;
            }
            if (cboReportFormat.SelectedIndex == 1 && cboType.SelectedIndex == 1)
            {
                cboReportFormat.SelectedIndex = 0;
            }
        }

        private void ShowGridReport()
        {
            pnlGrid.Visible = true;
            if (cboReportFormat.SelectedIndex == 3)
            {
                if (cboType.SelectedIndex == 0) //Monthly Report
                    ShowGridReportMonthly2();
                else
                    ShowGridReportSummary();
            }
            else
            {
                if (cboType.SelectedIndex == 0) //Monthly Report
                    ShowGridReportMonthly();
                else
                    ShowGridReportSummary();
            }
        }

        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>

        private void ShowGridReportMonthly2()
        {
            int Month = 0;
            int Year = 0;
            string PaymentDate = "01-Jan-1900";
            DataTable datTemp = new DataTable();

            //if (dtpPaymentDate.Checked)
            //    PaymentDate = dtpPaymentDate.Value.ToString("dd-MMM-yyyy");

            Month = cboMonthAndYear.SelectedValue.ToString().Split('@')[0].ToInt16();
            Year = cboMonthAndYear.SelectedValue.ToString().Split('@')[1].ToInt16();

            datTemp = clsBLLRptPayments.GetMonthlyPaymentsOne(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                    cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(),
                    cboEmployee.SelectedValue.ToInt32(), Month, Year, chkIncludeCompany.Checked, cboTransactionType.SelectedValue.ToInt32(),
                    chkIsPayStructureOnly.Checked, PaymentDate, 1, 2, ChkExcludeSettlement.Checked.ToBoolean());

            dgvReport.Rows.Clear();
            dgvReport.Columns.Clear();
            bool IsEmpNoNumeric = true;

            foreach (DataColumn dc in datTemp.Columns)
            {
                if (!(dc.ColumnName == "PaymentID" || dc.ColumnName == "CompanyID" || dc.ColumnName == "EmployeeID"))
                {
                    dgvReport.Columns.Add(dc.ColumnName, dc.ColumnName);
                    dgvReport.Columns[dc.ColumnName].MinimumWidth = 100;
                    dgvReport.Columns[dc.ColumnName].SortMode = DataGridViewColumnSortMode.NotSortable;  
                }
            }

            pbGridView.Minimum = 0;
            pbGridView.Maximum = datTemp.Rows.Count;

            foreach (DataRow dr in datTemp.Rows)
            {
                dgvReport.Rows.Add();

                foreach (DataColumn dc in datTemp.Columns)
                {
                    if (dc.ColumnName == "EmployeeNo")
                    {
                        int intEmpSlNo = 0;
                        int.TryParse(dr["EmployeeNo"].ToStringCustom(), out intEmpSlNo);

                        if (intEmpSlNo == 0)
                            IsEmpNoNumeric = false;

                        dgvReport.Rows[dgvReport.Rows.Count - 1].Cells["EmployeeSlNo"].Value = intEmpSlNo;
                    }

                    if (!(dc.ColumnName == "PaymentID" || dc.ColumnName == "CompanyID" || dc.ColumnName == "EmployeeID"))
                    {
                        dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value = dr[dc.ColumnName];

                        if (Convert.ToString(dr[dc.ColumnName]) == string.Empty)
                            dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value = 0.00;
                    }
                }

                Application.DoEvents();
                pbGridView.Value = dgvReport.Rows.Count - 1;
            }

            dgvReport.Rows.Add();
            dgvReport.Rows[dgvReport.Rows.Count - 1].Cells["Employee"].Value = "Total";

            foreach (DataColumn dc in datTemp.Columns)
            {
                if (!(dc.ColumnName == "CompanyName" || dc.ColumnName == "EmployeeSlNo" || dc.ColumnName == "EmployeeNo" ||
                    dc.ColumnName == "Employee" || dc.ColumnName == "Particular" || dc.ColumnName == "Amount" || dc.ColumnName == "PaymentID" ||
                    dc.ColumnName == "CompanyID" || dc.ColumnName == "EmployeeID"))
                {
                    dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value =
                        datTemp.Compute("SUM([" + dc.ColumnName + "])", "").ToDouble();
                }
            }

            pbGridView.Value = datTemp.Rows.Count;

            if (IsEmpNoNumeric)
            {
                dgvReport.Columns.Remove("EmployeeNo");
                dgvReport.Columns["EmployeeSlNo"].HeaderText = "EmployeeNumber";
            }
            else
                dgvReport.Columns.Remove("EmployeeSlNo");

            RowStyle(dgvReport.Rows[dgvReport.Rows.Count - 1], dgvReport.Font);
            pnlGrid.Visible = false;
        }

        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        private void ShowGridReportMonthly()
        {
            int Month = 0;
            int Year = 0;
            string PaymentDate = "01-Jan-1900";
            DataTable datTemp = new DataTable();

            //if (dtpPaymentDate.Checked)
            //    PaymentDate = dtpPaymentDate.Value.ToString("dd-MMM-yyyy");

            Month = cboMonthAndYear.SelectedValue.ToString().Split('@')[0].ToInt16();
            Year = cboMonthAndYear.SelectedValue.ToString().Split('@')[1].ToInt16();

            datTemp = clsBLLRptPayments.GetMonthlyPaymentsGrid(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                    cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(),
                    cboEmployee.SelectedValue.ToInt32(), Month, Year, chkIncludeCompany.Checked, cboTransactionType.SelectedValue.ToInt32(),
                    chkIsPayStructureOnly.Checked, PaymentDate, 1, ChkExcludeSettlement.Checked.ToBoolean());

            dgvReport.Rows.Clear();
            dgvReport.Columns.Clear();
            bool IsEmpNoNumeric = true;

            foreach (DataColumn dc in datTemp.Columns)
            {
                if (!(dc.ColumnName == "PaymentID" || dc.ColumnName == "CompanyID" || dc.ColumnName == "EmployeeID"))
                {
                    dgvReport.Columns.Add(dc.ColumnName, dc.ColumnName);
                    dgvReport.Columns[dc.ColumnName].MinimumWidth = 100;
                }
            }

            pbGridView.Minimum = 0;
            pbGridView.Maximum = datTemp.Rows.Count;

            foreach (DataRow dr in datTemp.Rows)
            {
                dgvReport.Rows.Add();

                foreach (DataColumn dc in datTemp.Columns)
                {
                    if (dc.ColumnName == "EmployeeNumber")
                    {
                        int intEmpSlNo = 0;
                        int.TryParse(dr["EmployeeNumber"].ToStringCustom(), out intEmpSlNo);

                        if (intEmpSlNo == 0)
                            IsEmpNoNumeric = false;

                        dgvReport.Rows[dgvReport.Rows.Count - 1].Cells["EmployeeSlNo"].Value = intEmpSlNo;
                    }

                    if (!(dc.ColumnName == "PaymentID" || dc.ColumnName == "CompanyID" || dc.ColumnName == "EmployeeID"))
                    {
                        dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value = dr[dc.ColumnName];

                        if (Convert.ToString(dr[dc.ColumnName]) == string.Empty)
                            dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value = 0.00;
                    }
                }

                Application.DoEvents();
                pbGridView.Value = dgvReport.Rows.Count - 1;
            }

            dgvReport.Rows.Add();
            dgvReport.Rows[dgvReport.Rows.Count - 1].Cells["Designation"].Value = "Total";

            foreach (DataColumn dc in datTemp.Columns)
            {
                if (!(dc.ColumnName == "CompanyName" || dc.ColumnName == "EmployeeSlNo" || dc.ColumnName == "EmployeeNumber" ||
                    dc.ColumnName == "Employee" || dc.ColumnName == "Department" || dc.ColumnName == "Designation" || dc.ColumnName == "Amount" ||
                    dc.ColumnName == "Description" || dc.ColumnName == "EmployeeID" || dc.ColumnName == "CompanyID" || dc.ColumnName == "PaymentID"))
                {
                    dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value =
                        datTemp.Compute("SUM([" + dc.ColumnName + "])", "").ToDouble();
                }
            }

            pbGridView.Value = datTemp.Rows.Count;

            if (IsEmpNoNumeric)
            {
                dgvReport.Columns.Remove("EmployeeNumber");
                dgvReport.Columns["EmployeeSlNo"].HeaderText = "EmployeeNumber";
            }
            else
                dgvReport.Columns.Remove("EmployeeSlNo");

            RowStyle(dgvReport.Rows[dgvReport.Rows.Count - 1], dgvReport.Font);
            pnlGrid.Visible = false;
        }

        private void ShowGridReportSummary()
        {
            string PaymentDate = "01-Jan-1900";
            DataTable datTemp = new DataTable();

            //if (dtpPaymentDate.Checked)
            //    PaymentDate = dtpPaymentDate.Value.ToString("dd-MMM-yyyy");

            datTemp = clsBLLRptPayments.GetSummaryPaymentsGrid(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(),
                    cboDepartment.SelectedValue.ToInt32(), cboDesignation.SelectedValue.ToInt32(), cboWorkStatus.SelectedValue.ToInt32(),
                    cboEmployee.SelectedValue.ToInt32(), dtpFromDate.Value, dtpToDate.Value, chkIncludeCompany.Checked,
                    cboTransactionType.SelectedValue.ToInt32(), PaymentDate, 1, ChkExcludeSettlement.Checked.ToBoolean());

            dgvReport.Rows.Clear();
            dgvReport.Columns.Clear();
            bool IsEmpNoNumeric = true;

            foreach (DataColumn dc in datTemp.Columns)
            {
                dgvReport.Columns.Add(dc.ColumnName, dc.ColumnName);
                dgvReport.Columns[dc.ColumnName].MinimumWidth = 100;
                dgvReport.Columns[dc.ColumnName].SortMode = DataGridViewColumnSortMode.NotSortable;  
            }

            pbGridView.Minimum = 0;
            pbGridView.Maximum = datTemp.Rows.Count;

            foreach (DataRow dr in datTemp.Rows)
            {
                dgvReport.Rows.Add();

                foreach (DataColumn dc in datTemp.Columns)
                {
                    if (dc.ColumnName == "EmployeeNumber")
                    {
                        int intEmpSlNo = 0;
                        int.TryParse(dr["EmployeeNumber"].ToStringCustom(), out intEmpSlNo);

                        if (intEmpSlNo == 0)
                            IsEmpNoNumeric = false;

                        dgvReport.Rows[dgvReport.Rows.Count - 1].Cells["EmployeeSlNo"].Value = intEmpSlNo;
                    }

                    dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value = dr[dc.ColumnName];

                    if (Convert.ToString(dr[dc.ColumnName]) == string.Empty)
                        dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value = 0.00;
                }

                Application.DoEvents();
                pbGridView.Value = dgvReport.Rows.Count - 1;
            }

            dgvReport.Rows.Add();
            dgvReport.Rows[dgvReport.Rows.Count - 1].Cells["EmployeeName"].Value = "Total";

            foreach (DataColumn dc in datTemp.Columns)
            {
                if (!(dc.ColumnName == "CompanyName" || dc.ColumnName == "EmployeeSlNo" || dc.ColumnName == "EmployeeNumber" ||
                    dc.ColumnName == "EmployeeName" || dc.ColumnName == "Period" || dc.ColumnName == "PeriodFrom"))
                {
                    dgvReport.Rows[dgvReport.Rows.Count - 1].Cells[dc.ColumnName].Value =
                        datTemp.Compute("SUM([" + dc.ColumnName + "])", "").ToDouble();
                }
            } 
           
            pbGridView.Value = datTemp.Rows.Count;

            if (IsEmpNoNumeric)
            {
                dgvReport.Columns.Remove("EmployeeNumber");
                dgvReport.Columns["EmployeeSlNo"].HeaderText = "EmployeeNumber";
            }
            else
                dgvReport.Columns.Remove("EmployeeSlNo");

            RowStyle(dgvReport.Rows[dgvReport.Rows.Count - 1], dgvReport.Font);
            pnlGrid.Visible = false;
        }

        private void btnExcelExport_Click(object sender, EventArgs e)
        {
            string sFileName = "";

            if (dgvReport.RowCount <= 0)
                return;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Title = "Save ";
                sfd.Filter = " Excel File (*.xls)|*.xls|  Supported Files (*.xls)|*.xls;";

                if (sfd.ShowDialog() == DialogResult.OK)
                    sFileName = sfd.FileName;
                else
                    return;
            }

            try
            {
                if (sFileName != "")
                {
                    if (dgvReport.RowCount>0)
                    {
                        dtCompany = clsBLLReportCommon.GetCompanyHeader(cboCompany.SelectedValue.ToInt32(), cboBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked );
                        string[] RowHeader = new string[3];
                        RowHeader[0] = "Branch : " + cboBranch.Text + " Transaction Type : " + cboTransactionType.Text;
                        RowHeader[1] = "Department : " + cboDepartment.Text + " Designation : " + cboDesignation.Text;
                        RowHeader[2] = "Work Status :" + cboWorkStatus.Text + " Month && Year: " + cboMonthAndYear.Text;
      
                        MobjExportToExcel.ExportToExcel(dgvReport, sFileName, 0, dgvReport.Columns.Count - 1, true, dtCompany, RowHeader);
                    }
                }
                else
                    MessageBox.Show("Please Enter Name");
            }
            catch { }
        }

        private void dgvReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try { }
            catch { }
        }

        private static void RowStyle(DataGridViewBand RowBand, Font NewFont)
        {
            DataGridViewCellStyle style = new DataGridViewCellStyle();
            style.BackColor = Color.LightGray;
            style.Font = new Font(NewFont, FontStyle.Bold);
            RowBand.DefaultCellStyle = style;
        }

        private void dgvReport_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
